# AERIUS High level architecture

This document describes the components that make up the high level architecture of the AERIUS application.
These components are broadly divided into web layer components (shown in green in the image below), business layer components (blue) and data layer components (orange).
![high-level-architecture](images/aerius-calculator-architecture.png)

The following sections will discuss each component in a bit more detail; outlining the functional reason for its connections to other components, a selection of the most relevant technologies it uses and a brief description of how it functions.

## Web Layer applications

### Calculator HTML page server

Serves all web content used in the AERIUS web application, including the application itself.

#### Connections:

- Database: the static web server retrieves configuration of the user interface on startup, and continues to serve the application from memory.

### Connect service

The connect service is a REST API that exposes calculation and file storage capabilities and is used by authenticated API users or anonymous web application users.

#### Connections:

- Fileservice: storage of uploaded files.
- Database: tracking job statuses, validating API user authentication, retrieving calculation results.
- Messagebus: sending jobs to be processed asynchronously.
- Chrome headless: request web pages to be rendered to pdf for report generation.
- File download service: deposits generated data (e.g. pdf reports, result xmls, or archives of these) for public download.

### WMS Map layers

Geoserver that serves map layers to the web UI with relatively static map overlays (e.g. natura 2000 areas) and calculation results.

#### Connections:

- Database: retrieval of geo data. Examples are domain knowledge (e.g. background depositions and natura 2000 areas) and calculation results.

### Search service

This service provides geographic search capabilities to the UI, queried in free text it provides results in certain categories such as addresses, natura 2000 areas, coordinates, etc.

#### Connections:

- PDOK location service: forwards search queries to PDOK to get locations for addresses, cities, zip codes, etc.

### File service

An internally used central storage service for input files and their metadata.
Used for storage of user-uploaded files for validation by the connect API and while importing data into the browser GWT application.
Used by the chunker/export worker stores job input (for calculations in the chunker, and source data in the pdf) and results (e.g. gml, pdf).

## Business layer applications

### Messagebus

Central service for asynchronous messaging between business layer applications.

### Chrome headless

This headless browser is used for PDF report generation.

#### Connections:

Note: to prevent outside connections being made from this component these connections are routed internally.
- Static HTML web server: render specific pages for pdf generation.
- Connect service: retrieve calculation results
- WMS Map Layers: retrieve map layers

### Chunker/Export worker

The chunker/export worker handles jobs from the Calculator web application.
These jobs can entail performing a calculation, generating a pdf calculation report, or creating an IMAER gml file.
The calculation worker uses the calculator database and updates the status of running jobs there.

#### Connections:

- Messagebus: listen for jobs and divide into smaller tasks to be sent as new messages.
- Fileservice: retrieve input data that is too large to be put on the messagebus.

### OPS Worker

The OPS worker calculates results with the 'Operationele Prioritaire Stoffen' (OPS) model.
This model is a native application developed and maintained by the RIVM and is wrapped by the OPS Worker.
The worker generates the required input files, runs the OPS executable, parses the result files, and sends the results back.

#### Connections:

- Messagebus: listen for calculation tasks and send calculation results.

### SRM Worker

The SRM worker calculates results with the  Standaardrekenmethode 1 (SRM1) and Standaardrekenmethode 2 (SRM2) from the "Regeling beoordeling luchtkwaliteit 2007" (a model specific for road traffic).
The SRM2 model itself is implemented in OpenCL and is integrated in the SRM Worker using the Java JOCL library.
See the [SRM readme](../source/commons-aerius/aerius-commons-srm/README.md) for more details on the SRM worker.

#### Connections:

- Messagebus: listen for calculation tasks and send calculation results.

### ADMS Worker

The ADMS worker calculates results with the Atmospheric Dispersion Modeling System.
The model is a native application developed by [Cambridge Environmental Research Consultants (CERC)](https://cerc.co.uk) and wrapped by the ADMS worker.
The worker generates the required input files, runs the ADMS executable, parses the result files, and sends the results back.

#### Connections:

- Messagebus: listen for calculation tasks and send calculation results.

### Message worker

Sends e-mails to end users with their in- and output data, usually and a download link to a result file. But in case of failure include input as attachment.

#### Connections:

- Messagebus: listen for messages to be sent via email

### Task manager

The task manager operates as a gatekeeper to the workers.
It sets priorities on the different tasks and it guarantees that no matter how many tasks of a certain type are in the queue there is always room for other types of tasks.
The taskmanager can be found in the repository https://github.com/aerius/taskmanager

#### Connections:

- Messagebus: distribute calculation tasks across other message queues according to its configuration.

## Data layer applications

### PostgreSQL database

A central datastore for:
- domain specific information (e.g. background depositions, emission factors, etc.),
- calculation statuses and results,
- connect user authentication information,
- application configuration,
- translations for domain specific information
