# Remote testing a GWT application

Sometimes it's required to test a GWT application in Super Development Mode on a device other than the host machine. For example debugging the application on mobile
devices or tablets, which cannot run a code server and web container - and even if they could would make for a particularly nasty debugging experience.

This guide shows the steps required to do that.

The TL;DR concept is as follows: By default, GWT SDM runs on localhost, and ignores any requests originating from anywhere other than localhost, essentially allowing
only the host machine to be served the application and trigger an SDM compilation run. The steps given intend to run both the serving web container aswell as the SDM
code server to run on the local machine's local IP in the LAN, rather than localhost - and have both of those be aware of eachother's existence.

It's pretty straightforward, but cumbersome to figure out - which is the reason for this supershort guide.

[1] Find your local IP. [ipconfig/ifconfig]
	
[2] In your SDM config, add the following to Program Arguments:

````
-bindAddress [local_ip]
````
	
   This makes the code server run on your local ip rather than localhost.
	
[3] For *each servlet*, you need to redirect the serialisation policy URL to the remote code server.

  This defaults to localhost and for some godforsaken reason is hardcoded. If the product you're debugging employs the dispatcher pattern then
  this will be easy, otherwise you need to adapt every service to the following:
	
  Override 'getCodeServerPolicyUrl(final String strongName)' with the following:

````java
	@Override
	protected String getCodeServerPolicyUrl(final String strongName) {
	  String localIp = [local_ip];
	
	  return "http://" + localIp + ":" + 9876 + "/policies/" + strongName + ".gwt.rpc";
	}
````

   For example for Register, this will be the DispatcherServlet and the RegisterContextServlet.
	
[4] Navigate to [local_ip]:[port]/[product], ie. http://192.168.1.42:8080/calculator and start testing.

Make sure not to commit these changes into the repo!

# Problems

- Connection refused means your J2EE container isn't exposed on LAN - this should be default in Tomcat, so start Googling.

- Serialization exceptions means the J2EE container can't find the code server. If [local_ip]:9876 doesn't exist, see step 2, else see step 3.
