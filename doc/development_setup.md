# Setting up the development environment

## Introduction

The AERIUS application is written in JAVA and PostgreSQL.
The project is build with Maven 3.
The used development environment is Eclipse.
This page describes the installation of the development environment, how to build the project and how to test and debug the project.
This document will help setup AERIUS in Eclipse, using `AERIUS Calculator` as an example.

NOTE: This document is made on the assumption you're using Windows. If you're not, you may have to deviate from these steps slightly.

## Prerequisites

To start developing several tools need to be installed.
To follow this tutorial you'll need to have the following programs installed:
* A recent version of Eclipse (f.e. 2023-12) [Eclipse for Java EE Developers](http://www.eclipse.org/downloads/)
  * If you're using the Eclipse installer, make sure to select a JDK version 17 during installation
* [PostgreSQL (12+)](https://www.postgresql.org/download/)
* [Maven 3](http://maven.apache.org/download.cgi)
* [Java JDK (17)](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
* [Git](https://git-scm.com/downloads)
* [Tomcat 8.5](http://tomcat.apache.org/) (if using Tomcat to run the webserver) (Note: It's planned to use the embedded tomcat, so this is not required to be installed separately)
* [Ruby 2.4+](https://www.ruby-lang.org/en/downloads/) Only required for building the databases.

Then fork and/or clone the source code:
* A git clone from (a fork from) https://github.com/aerius/AERIUS-II
* A git clone from (a fork from) https://github.com/aerius/tools

## Getting the PostgreSQL database set up

The database behind the whole AERIUS application. Without it you can't run the application. To get the database setup follow these steps:

1. Open pgAdmin
2. Create a new Login/Group Role called `aerius` in the same server
you want the database to be
3. Create a new database `calculator`
4. Get the database in one of the following ways:
    * Backup:
      1. Get a backup from a colleague, or from SFTP and restore it:
      2. Right click `calculator` > `Restore...` > `Filename` > `Restore`
      3. Further instructions can be found found [here](database/buildsysteem_en_databronnen.md)
    * Build the DB using Docker

NOTE: Another way to get the database set up, without using pgAdmin, can be found [here](https://github.com/aerius/AERIUS-II/tree/main/scripts/database)
NOTE2: Make sure, if you're building the database for multiple customer profiles (f.e. for NL and UK), to save them under different database names.

## Installing the right Eclipse plugins

AERIUS makes use of several plugins. These can be installed using the Eclipse marketplace.
1. Go to `Help` > `Eclipse Marketplace...`
2. Install the following plugins:
    * m2e-apt 1.5.3+
    * Vue GWT 0.0.2+
    * GWT Eclipse Plugin 3.0.0+
    * SonarLint 5.6+

## Using the right JDK in Eclipse

This project uses JDK 11+ (currently >=11 && <15).
To get JDK installed correctly follow these steps:
1. Go to `Window` > `Preferences` > `Java` > `Installed JREs`
2. Add the link to the root of the JDK installation and make sure it's selected
3. `Apply` and `Close`
4. Go to the folder in which Eclipse is installed
5. Open `eclipse.ini`
6. Replace the line under `-vm` with the link to the bin of the JDK you added in Eclipse (f.e. `c:/Program Files/Java/jdk-13/bin` (without quotes!))
7. Save the file
8. Restart Eclipse

NOTE: Currently the vue-gwt dependency we are using does not support Java 15+, since it depends on Nashorn which has been removed in that version.
Newer versions of eclipse (at least the 2020-12 version) use an embedded java version of 15, causing some code to not be generated during development.

## Get the code styles setup

This project follows a certain codestyle. To help you with adhering to this style, you can let Eclipse help you with that.

NOTE: All XML files in these steps can be found in `.\[root]\src\main\resources` from the [Tools project](https://github.com/aerius/tools)
1. Go to `Window` > `Preferences` > `Maven` > `Lifecycle Mappings` and import `lifecycle-mapping-metadata.xml`
2. Go to `Java` > `Code Style`
    * Import `eclipse_clean_up_profile.xml` in `Clean up`
    * Import `eclipse_code_templates.xml` in `Code templates`
    * Import `eclipse_code_formatter_profile.xml` in `Formatter`
    * Import `eclipse.importorder` in `Organize imports`
3. Go to `Java` > `Editor` > `Save Actions` and click on `Configure`
    * On the tab `Code Organizing`
      * Check `Formatter` > `Remove trailing whitespace` (`all lines`)
      * Make sure everything else is turned off
    * On the tab `Code Style`
      * Check `Use modifier 'final' when possible`, with all underlying checkboxes enabled
      * Make sure everything else is turned off
    * On the tab `Member Accesses`
      * Everything should be turned off
    * On the tab `Missing Code`
      * Everything should be turned on
    * On the tab `Optimization` the following options should be turned on:
      * `Exit loop earlier`
      * `Use lazy logical operator`
      * `Primitive serialization`
      * `Precompile reused regular expressions`
      * `Remove redundant string creation`
      * `Prefer boolean literals`
    * On the tab `Unnecessary Code` the following checkboxes should be turned on:
      * `Remove unused imports`
      * `Remove unnecessary casts`
4. Go to `General` > `Editors` > `Text Editors`
    * Set the `Displayed tab width` to 2
5. Go to `Web` > `HTML Files` > `Editor`
    * Select `Indent using spaces`
    * Set the indentation size to `2`
    * `Apply and Close`

## Importing the AERIUS project in Eclipse.

AERIUS is build with maven. To get the AERIUS project into Eclipse it can
simply be imported with the maven import.

1. `Import...` > `Existing Maven Projects`
2. Select the root directory of `./calculator` (root folder from Github/calculator) as the `Root Directory` and press `Finish`

## Maven build

The project should be built automatically after importing, but some projects will not work out of the box.
To fix this, you should do a `maven clean install`. 
When building the UK application use the appropriate profile: `maven clean install -P JNCC`.

## Preparing the CodeServer (client)

The CodeServer takes care of compiling the client code of an AERIUS module during development.
It also supplies source maps which can be used in a browsers' development tools to debug.

Before starting the CodeServer you need to make sure the wui client project has been compiled.
This is needed to compile the GWT-Vue classes.

* To compile the client using a CLI
  1. navigate to the `./calculator` root of the GIT folder in the CLI
  2. Run `mvn clean install -am -DskipTests -Dgwt.skipCompilation=true -pl :aerius-calculator-wui-server`
* To compile the client using Eclipse (Same as above but in a console in Eclipse)
  1. Right click `aerius-parent` > `Run as` > `Maven build...`
  2. Fill in the following options:
    * Base directory: `${project_loc:aerius-parent}`
    * Name: `calculator-wui-server compile`
    * Goals: `clean install -am -Dgwt.skipCompilation=true -pl :aerius-calculator-wui-server`
    * `Skip Tests` checked
  3. `Apply` and `Run`

NOTE: After compiling you may have to refresh `aerius-parent`.

Next is to start the CodeServer:

* Start using a CLI (May keep the port used, but faster)
  1. navigate to the `./calculator` root of the GIT folder in the CLI
  2. navigate to the `source` folder
  3. Run `mvn gwt:codeserver -B -Denv=dev -am -Penv-dev -Dmaven.buildNumber.skip=true -pl :aerius-calculator-wui-client`
* Start using Eclipse (Same as above but in a console in Eclipse)
  1. Right click `aerius-source` > `Run as` > `Maven build...`
  2. Fill in the following options:
    * Base directory: `${project_loc:aerius-source}`
    * Name: `calculator-wui-server CodeServer`
    * Goals: `gwt:codeserver -B -Denv=dev -am -Dmaven.buildNumber.skip=true -pl :aerius-calculator-wui-client`
    * Profiles: `env-dev`
  3. `Apply`
* Start using Eclipse (Closes the application right, but slower)
  1. Right click `aerius-source` > `Run as` > `Maven build...`
  2. Fill in the following options:
    * Base directory: `${project_loc:aerius-source}`
    * Name: `AERIUS CodeServer`
    * Goals: `net.ltgt.gwt.maven:gwt-maven-plugin:codeserver -am`
    * Profiles: `env-dev`
  3. `Apply`

NOTE: The CodeServer should be started before the Jetty/Tomcat webserver

## Preparing the webserver

The webserver runs the server code of an AERIUS module.
There are multiple ways to run the webserver:

1. Tomcat 8.5
    * TODO
2. Jetty
    1. Copy `source/calculator/aerius-calculator-wui-server/src/main/jettyconf/context.template` to `source/calculator/aerius-calculator-wui-server/src/main/jettyconf/context.xml` and fill in your local database connection details.   
    2. Open `aerius-source` > `aerius-calculator`
    3. Right click `aerius-calculator-wui-server` in this folder
    4. Select `Run as` > `Maven build...`
    5. Fill in the following options:
      * Base directory: `${project_loc:aerius-calculator-wui-server}`
      * Name: `AERIUS Jetty Webserver`
      * Goals: `jetty:run`
      * Parameters:
        - `jetty.port`=`8085`
      * Profiles: `env-dev`
    6. Apply

## Running the development gateway

To make switching between (parts of) environments easy, and allow (partial) use of remote environments, a development gateway service is available.

To run this in Eclipse:

1. Right click `aerius-calculator-development-gateway`
2. Select `Run as` > `Maven build...`
  * Name: `AERIUS Development Gateway`
  * Goals: `spring-boot:run`
3. Apply

To run from a shell:

1. `mvn spring-boot:run -pl :aerius-calculator-development-gateway`

By default, this gateway will point to all services described in this document on localhost.

To configure it to have parts of it point to remote services instead, set up the following:

1. Rename the file in `source/calculator/aerius-calculator-development-gateway/config/application.yaml.template` to `source/calculator/aerius-calculator-development-gateway/config/application.yaml`
2. Comment in or out, or otherwise edit, the services you wish to point to remote services.
  * For example, by using a remote `connect` and `search` gateway, it is possible to do a calculation on a local UI without running those services.

## Switching the development environment between customers

AERIUS has multiple customers. To switch the development environment between them, follow these steps:

1. Make sure to build the application for the correct customer profile (see [Maven build](#maven-build)).
2. Make sure the database of the customer you wish to switch to is selected in the Connect and Jetty configuration files.
3. If you're using the gateway: Make sure the URL's either link to localhost or to the API's of the the correct customer.

## Debugging AERIUS GWT web application

To run and debug the AERIUS web application 2 applications must be started:
1. The `CodeServer` (for calculator: `aerius-calculator-wui-client`)
2. The webserver using `Tomcat` or `Jetty` (for calculator: `aerius-calculator-wui-server`)

The CodeServer dynamically compiles the Java code to JavaScript for debugging.
The webserver is used to serve static content and some basic initialization.

After running both the Codeserver and the Jetty/Tomcat webserver, the application should be accessible on `localhost:8080`.

## Adding users to GeoServer

When developing locally you might need to use the `GeoServer UI`. To get there you need an user account.
These can be configured by creating `source/geoserver/aerius-geoserver-calculator/src/main/webapp/data/security/usergroup/default/users.xml` (adjust path for other `GeoServer` instances). Use the snippet below as a template:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<userRegistry xmlns="http://www.geoserver.org/security/users" version="1.0">
  <users>
    <user enabled="true" name="admin" password="plain:<addYourPasswordHere>"/>
  </users>
<groups/>
</userRegistry>
```

This file will match an entry in the `.gitignore` and as such not be committed.
If this file is not present it will default to the `users.xml` in `geoserver-core`.
Whatever that might contain.

## Running GeoServer

Running contains 2 steps. Compiling and starting it. As an example I will use `geoserver-calculator`, but this should work for all `geoserver-*` products.

### Compile GeoServer

* Compile using Eclipse
  1. Right click `aerius-geoserver-parent` > `Run as` > `Maven build...`
  2. Fill in the following options:
    * Base directory: `${project_loc:aerius-geoserver-parent}`
    * Name: `geoserver-calculator Compile`
    * Goals: `clean install -pl :aerius-geoserver-calculator`
  3. `Apply`

### Start GeoServer

  * Start using Eclipse
    1. Click on the drop down next to the `External Tools Configurations` > `External Tools Configurations..` > double-click on `Program`
    2. Fill in the following options:
      * Location: `/usr/bin/java` (or wherever your Java binary can be found)
      * Name: `geoserver-calculator Start`
      * Working directory: `${workspace_loc:/aerius-geoserver-calculator/target/}`
      * Arguments: `-DTOMCAT_STANDALONE_CONTEXT_PATH=/geoserver-calculator -DTOMCAT_STANDALONE_PORT=8082 -DCALCULATOR_DB_HOST=localhost -DCALCULATOR_DB_NAME=calculator -DCALCULATOR_DB_USER=aerius -DCALCULATOR_DB_PASS=aerius -DGEOSERVER_CONSOLE_DISABLED=false -jar ${workspace_loc:/aerius-geoserver-calculator/target/geoserver-calculator.war}`
      These all should be on a single line and should be configured to use your local setup.
    3. `Apply`

Using the settings above the UI can be reached using `http://localhost:8082/geoserver-calculator/`.

## Running the Taskmanager

There are basically 3 ways to use the taskmanager for development purposes:
- Use the docker container from our nexus repository (it's named `taskmanager`).
- Download the jar and run it with correct configuration. To download it you can use:
    - the latest snapshot (https://nexus.aerius.nl/service/rest/v1/search/assets/download?sort=version&maven.groupId=nl.aerius&maven.artifactId=taskmanager&repository=maven-snapshots&maven.extension=jar&version=&maven.classifier=)
    - a specific released version, for instance 1.0.0 (https://nexus.aerius.nl/service/rest/v1/search/assets/download?sort=version&maven.groupId=nl.aerius&maven.artifactId=taskmanager&repository=maven-releases&maven.extension=jar&version=1.0.0&maven.classifier=)
- Check-out the repository from git and run/debug the application as you would with a normal java project in your favorite development environment.
More information about how to run the taskmanager is available in the taskmanager repository.
  
## Running the Worker(s)

There are several different type of workers. 
All workers are available in the same entry point, but a specific worker can be enabled using the configuration. 
This means you either can start all workers as a single program or each type worker as a different program.
Workers are enabled by specifying the `processes` configuration.
If the `processes` configuration is not set the specific worker is not started.
In general the configuration process is the following:
1. Copy template the template `source/calculator/aerius-worker/src/main/config/worker.template.properties` to the git-ignored location: `source/calculator/aerius-worker/src/test/config/worker.properties`.
2. Configure RabbitMQ:
```
# RabbitMQ configuration. Commented out configuration has a default value.
broker.host = [host]
broker.username = [username]
broker.password = [password]
```
2. Create a run configuration for the main class `nl.overheid.aerius.worker.main.Main`
3. Pass the absolute path to `worker.properties` created in step 1 via the `-config` argument

For specific worker configuration properties, see required configuration below: 

### OPS worker

1. Configure `ops.processes` to at least 1
2. Configure `ops.root` to the directory where you want to store versioned OPS folders.
3. There are three options for retrieving model data:
   1. Manually download and extract the latest OPS version from [nexus](https://nexus.aerius.nl/#browse/search=keyword%3Dops)
   2. Configure `ops.model.preload.versions` to match the version you want to load from `ops.model.data.url` on startup with an [OPSVersion](../source/commons-aerius/aerius-commons-ops/src/main/java/nl/overheid/aerius/ops/version/OPSVersion.java)
   3. Don't do anything and let the application download data when the first calculation job is received.

An example of the directory structure is the following, where `OPS` is the `ops.root`: 
```
OPS
└── 5.0.1.1_26082021
    ├── bin
    ├── data
    ├── meteo
    └── receptors
```    

### SRM worker

1. Configure `srm.processes` to at least 1
2. Configure `srm.own2000.root` and/or `srm.nsl.root` to the directory where you want to store versioned SRM folders.
3. There are three options for retrieving model data:
   1. Manually download and extract the latest presrm, presrm-lu and depositionvelocity versions from [nexus](https://nexus.aerius.nl/#browse/search=keyword%3Dsrm)
   2. Configure `srm.[nsl|own2000].model.preload.versions` to match the version you want to load from `srm.model.data.url` on startup with an [AeriusSRMVersion](../source/commons-aerius/aerius-commons-srm/src/main/java/nl/overheid/aerius/srm/version/AeriusSRMVersion.java)
   3. Don't do anything and let the application download data when the first calculation job is received.

An example of the directory structure is the following, where `SRM` is the `srm.[nsl|own2000].root`:
```
SRM
├── aerius-presrm-2.103-1
│   └── ...
├── depositionvelocity
│   └── ...
└── presrm-lu-2.103
    └── ...
```

### ADMS worker

1. Configure `adms.processes` to at least 1
2. Configure `adms.root` to the directory where you want to store versioned SRM folders.
3. There are three options for retrieving model data:
    1. Manually download and extract the latest ADMS data from [nexus](https://nexus.aerius.nl/#browse/search=keyword%3Dsrm)
    2. Configure `adms.model.preload.versions` to match the version you want to load from `adms.model.data.url` on startup with an [ADMSVersion](../source/commons-aerius/aerius-commons-adms/src/main/java/nl/aerius/adms/version/ADMSVersion.java)
    3. Don't do anything and let the application download data when the first calculation job is received.
4. Configure `adms.license.base64` with a valid license for the used ADMS version.

An example of the directory structure is the following, where `ADMS` is the `adms.root`:
```
ADMS
├── 5.0.0.1
│   └── ...
├── deposition_velocity_2022
│   └── ...
└── deposition_velocity_plume_depletion_2022
    └── ...
└── spatially_varying_dry_deposition_velocity_500m_20220614
    └── ...
└── spatially_varying_roughness_500m_20220614
    └── ...
└── uk_terrain_20220603
    └── ...
```

### Chunker worker

1. Configure `chunker.processes` to at least 1.
2. Configure `database.username`, `database.password` with the account details of your local database.
3. Configure `database.url`  point to your local calculator database.

### PDF worker

Configure pdf worker:
1. Set `pdf.processes=1`
2. Set `pdf.webserver.url = http://localhost:8080/`

Configure to run local Chrome executable, Use Chrome version 112 or newer.
1. Set `chrome.mode=HEADLESS`
2. Set `chrome.root=[directory to chrome executable]`

### Debugging emails sent by the message worker

The message worker sends emails to users.
The message worker can be run by configuring the message worker in te properties file:
1. Configure `email.processes = 1`
2. Add database configuration as with chunker worker
3. If you need configurations for sending emails you can add these in the properties file.

During development it is convenient to intercept the emails instead of actually sending them.
This can be achieved by using a fake smtp server.
There are several solutions available for fake smtp servers.
The following steps describe one such solution.
This describes a Java based fake smtp server that can be found on GitHub: https://github.com/gessnerfl/fake-smtp-server
The GitHub readme describes in detail how to use this fake smtp server, but here is a short tutorial:

  1. Download the jar file from https://github.com/gessnerfl/fake-smtp-server/releases/latest
  2. Create an `application.properties` file with the content `fakesmtp.port=25` to use port 25, which is used by the message worker.
  3. Start the jar with `java -jar fake-smtp-server-<version>.jar -Dspring.config.location=<path to, including application.properties file>`
  4. Open browser at `http://localhost:5080` to read and manage the emails that are send.

When using port 25 you might need to run the jar file as root, via `sudo` to be allowed to listen to port 25.

## Fixes

The debug may not work for various reasons. Here's how to fix some that we've encountered previously:

### Problem: Postgres restore doesn't work

If you've just updated Postgres versions, you may still use old version for restores and backups.
To use the new version instead follow these steps:
1. Navigate to `File` > `Preferences` > `Paths` > `Binary paths`
2. Make sure `PostgreSQL Binary Path` is set to the `bin` folder of the correct Postgres installation


### Problem: Maven clean build/install gives an error

You may get this error if you're using a custom maven proxy. The maven downloads from this project should come from `https://nexus.aerius.nl/`.

* Some companies use their own maven proxy repository, and define this repository in the global maven settings of their environment. Since not all repositories that AERIUS uses might be proxied by this internal repository, AERIUS works better without these global settings. Hence it's advisable to (temporarily) disable these. That can be achieved with the following steps:
  1. Navigate to `%USERPROFILE%/.m2`
  2. Rename the file `settings.xml` to `settings.xml.backup` or something similar.

### Problem: After compiling just a white page is shown

You may be getting a blank page when you test the web application in the browser.

* If there are no errors, the main page may just not have any content. Try `localhost:8080/own2000/sources`.
* Make sure the webserver is running. A blank page indicates the server files are
not correctly deployed to the webserver, or the webserver crashed during startup.
* Make sure you have your database setup correctly.
* Check the error log.
* Fix generated sources:
  1. Right click `aerius-calculator-wui-client` > `Properties`
  2. Go to `Java Compiler` > `Annotation Processing`
  3. Make sure `Enable project specific settings`, `Enable annotation processing` and `Enable processing in editor` are checked
  4. Fill in `target\generated-sources\annotations` for `Generated source directory`
  5. Fill in `target\generated-test-sources\test-annotations` for `Generated test source directory`
  6. `Apply and Close`

### Problem: The CSS doesn't work

* Refresh the site (`F5` in browser)
* Fix using Maven Build:
  1. Close all running Java applications in Java
  2. Right click `aerius-parent` > `Run as` > `Maven build...`
  3. Fill in the following values:
      * Name: `calculator Fix CSS`
      * Goals: `-pl :aerius-calculator-wui-client resources:resources compiler:compile -am`
  4. `Apply` and `Run`
* TODO: Find an easier/less time consuming fix

### Problem: The splash screen indicates an error has occurred.

This occurs most often due to failed serialization of a domain object (contexts most likely).
It happens when domain objects have been changed and the webserver still has old files,
or the first time the webserver starts up after a clean without client code deployed.

- Refresh common/shared
- Refresh wui-server
- Refresh wui-[*product*]-server.

At this point, if you're using Tomcat, the webserver dialog should indicate [*started, restart*] or
[*stopped, republish*]. If so, restart/start the webserver.

If not, you need to either keep refreshing until it does, or you're dealing
with some other problem not commonly encountered.

### Problem: Something to do with Ginjector

This error is utterly absurd, it shows up out of nowhere, for no reason, and can ruin an entire day and your mood.
They're not unlike parking tickets in this sense.
It will appear as though you caused it by a code change, but nothing could be further from the truth: you are not at fault, the error is only trying to trick you into thinking that you are like the deceiving little pest it is.
To solve it you need to do a full maven rebuild of the project, refresh everything (shared first, then clients, then servers), clean the webserver, redeploy to webserver, restart the code server and webserver, pray to jesus and all his apostles, start the application, and all should be well.

### Problem: gml:AbstractFeature

Importing GML files doesn't work out of the box when running from eclipse.
This is a workaround to make it work:
1. Run `maven install` on `imaer-gml`. This will extract the xsd files to your project build directory.
2. Open `gml-catalog.xml`
3. Change the `uri` of `system` values to your local path, for example:
  - `<system systemId="http://schemas.opengis.net/gml/3.2.1/gml.xsd" uri="C:\repositories\GitHub\IMAER-java\source\imaer-gml\target\classes\gml\3.2.1\gml.xsd" />`
  - `<system systemId="http://schemas.opengis.net/gmlsfProfile/2.0/gmlsfLevels.xsd" uri="C:\repositories\GitHub\IMAER-java\source\imaer-gml\target\classes\gmlsfProfile\2.0gmlsfLevels.xsd" />`
4. Make sure these files actually exist. If they do not, you may have to run `maven install` again
5. Save and rebuild

### Problems: In general

* Make sure everything is built. If you're not sure, run the `AERIUS parent clean install` (maven clean install) again
* Make sure the webserver is fresh and up-to-date. The webserver is not up to date when it indicates you need to restart:
  > [*Started, Restart*] or [*Stopped, Republish*]

  After refreshing (web application) projects, changes are compiled into target dir(s), the webserver listens for changes on this dir and whenever it encounters something that cannot be dynamically reloaded, it will indicate you need to issue a cold restart using the above indicators. Usually, only servlets and classes that are _not_ Serializable can be  dynamically reloaded.

  Note this: Server projects (eg. wui-server) depend on projects like -common and -shared. Refreshing those last 2 does not automatically cause the server (EE container) at which the server _project_ is deployed to refresh/restart. Only after explicitly refreshing the server project will it receive the changed files in its /webapps (or similar) directory. Because of this, the order in which you refresh projects also matters.

* When in doubt, refresh everything 3+ times and you should be fine. ;)

## dependency check

There are currently 2 ways to check our dependencies: checking for known vulnerabilities and checking for newer versions.

### Check for known vulnerabilities

To check for known vulnerabilities we use the owasp `dependency-check-maven` plugin to generate reports.
This can be triggered by running a maven verify action with the sonar profile: `mvn clean verify -DskipTests -Psonar`.
This is also done automatically by our build on main branch, and the results are added to sonarcloud as well.

### Check for newer versions

To check for newer versions we can use the `versions-maven-plugin` plugin.
This plugin has been configured to use a XML file to ignore certain versions (from the [Tools project](https://github.com/aerius/tools)).
This plugin can be used locally to check for newer versions, and can be used to automatically update versions as well.

There are 3 ways to check for newer versions (well, the plugin supports more, read the manual of the plugin if you want to know more):

* `mvn versions:display-property-updates` shows a list of all properties that can be updated to get newer versions.
* `mvn versions:display-plugin-updates` shows a list for each module with possible newer versions for maven plugins.
* `mvn versions:display-dependency-updates` shows a list of possible newer versions for all dependencies in the project (including transitive dependencies).

To automatically update versions, `mvn versions:use-latest-versions` can be used. Do note that this probably needs some testing to figure out if everything can actually be updated, as it will update major versions as well with the current configuration.
