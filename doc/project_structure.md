# AERIUS Technical Project Structure

The technical project structure describes how the directory and files are organizes within this project repository.

## The Directory Structure

In the top level of the project the following directories are available:

### bom

The `bom` directory contains maven bom files used our Java modules.
In bom files dependencies and version information of external libraries are declared.
Bom files are used to group multiple libraries for specific functionality.
This allows to manage dependencies in a single location.

### doc

The technical documentation.

### docker

The docker templates and configuration to run the AERIUS applications in docker.
See (docker readme)[docker/README.md] for more details.

### scripts

A set of convenience scripts to help during development.
For general development information see [development_setup.md](development_setup.md).

### source

The location of the Java and PostgreSQL source code of the project.
This directory will be explained in more detail:

## The source Directory

The `source` directory is where the source code of the project can be found.
The `source` directory contains several high-level directories that group specific modules.
The `calculator`, `commons-aerius` and `commons` directories contain the Java source code.
The modules also depend on external libraries developed in the AERIUS project.
These modules contain functionality also used or usable in other products.
The list of dependencies to AERIUS libraries can be found on the (dependencies)[dependencies.md] page.

### calculator

The `calculator` directory contains the top-level modules that contain the main entries for the applications.
The build artifacts of these modules are applications of AERIUS.
The directory also contains the modules for the user interface integration tests.

### commons-aerius

The `commons-aerius` directory contains all sub modules that cover certain AERIUS specific functionality.
This includes, among other things, the implementations of the pollutant model wrappers, database queries and domain specific utils.

*The modules in `commons-aerius` should never have a dependency on modules in the `calculator` directory.*

### commons

The `commons` directory contains a set of modules that have functionality that is not specific for AERIUS.
It contains functionality that is generic, not available in third-party libraries and can be used in multiple of our own modules.
Keeping code generic and not AERIUS specific keeps the overall code base simpler.

*The modules in `commons` should never have a dependency on modules in either the `commons-aerius` or `calculator` directory.*

### database

The `database` directory contains all `sql` code of the database.
There are 2 kinds of database files: structural and data.
Structural files are files including `tables`, `views`, etc. and are located in the `src/main/sql` directory.
Data files contain all the project specific data loaded in the database at build and are located in the in `src/data/sql` directory.
It's possible to build only the structural part of the database, without the data.

### geoserver

The `geoserver` directory contains the Geoserver modules.
The modules contain the geoserver configurations for AERIUS Calculator and the Open Data web services.
Geoserver artifacts can be build either NL or UK configurations.

### sonar-report

A module specific to collect multiple module coverage information by SonarQube.
Any Java module added to the project needs to be added to the pom.xml in the sonar-report module.

## Module dependencies

The interdependencies between the individual modules is visualized in the figure below:
![aerius_project_structure](images/aerius_project_structure.png)