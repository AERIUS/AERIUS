# Collaborative development

This document outlines some basic guidelines for doing efficient collaborative development,
initiating and dealing with reviews, and the process to getting a pull-request merged into Main.
It is by no means a template which must be followed to the letter,
or at all - it is merely a set of words combined into a sentence comprising a document from which one may derive some insight.

## Main branch, feature branches and pull requests

The main branch is where all the code lives.
Each change made to the main branch is created by a pull request to the main branch.
There are 2 type of branches from which pull requests can originate: Feature branches and personal branches.
Feature branches are used for changes that have high impact and are developed in multiple phases, possibly by multiple developers.
Personal branches are branches created on a fork the developer created of the project.
There are also some other ways merges can be made.
The following table shows the ways we identify and explain how branches and actions should work:

| Origin              | Target  | Action |
|---------------------|---------|--------|
| fork                | main    | squash |
| feature             | main    | merge  |
| fork                | feature | squash |
| main merge          | feature | merge  |
| main conflict merge | feature | merge  |

### Fork to Main

Smaller changes or features are made via pull requests from personal forks to the main branch.
These changes should preferable be small, such that the risk of breaking something is small.
The pull requests are to be reviewed following the [process described below](#reviewing-pull-requests).
When such a pull request is merged to main it should be done as a **squash** merge commit.
A squash commit is done because for the history we're only interested in the final commit as that represents the reviewed state.
Any in between commits might have been changed or there are undescriptive commits that would clutter the history on the main branch.
The merge comment should be updated to rewrite all squashed commit messages to a new single message explaining the commit clearly.
When available the commit message should contain the id of the issue from the issue management system. 

### Feature to Main

For larger features that include both front- and backend development a feature branch is created on the product repository.
A feature branch is a branch in the main repository and follows the naming convention `feature/AER-<id>-<description>`.
Any change for this feature branch is created through a pull request from a personal fork.
These pull request are to be reviewed following the [process described below](#reviewing-pull-requests), similar as a fork to main pull request.
When such a pull request is merged to main it should be done as a **merge** commit.
Because we do want to keep the commit history on the feature branch as it can be large and developed by multiple people.
There is no review on the code in the feature branch,
since all reviews have already been done on the pull requests to the feature branch.
Instead, to merge the feature branch it should be approved by someone from the QA/Test team.
This will be an indicator the tests are approved.
When approved the feature branch will be automatically merged by the merge bot.

### Fork to Feature

These pull requests are similar to the fork to main pull requests.
They follow the same process.
These pull requests are merged to main as a **squash** merge commit.

### Main merge to Feature

If there are changes in main that are also required in the feature branch a merge commit from main can be created.
This can be done in the main repo by creating a pull request from main to the feature branch.
The limitation is that there are no merge conflicts.
When such a pull request is merged to main it should be done as a **merge** commit.
There is no code review required as it only contains already approved code.

### Main conflict merge to Feature

When wanting to merge changes from main into a feature branch merge conflicts can be present.
In that case these conflicts need to be manually resolved before the code from main can be merged into the feature branch.
Therefor a branch from the feature branch called `feature-merge/AER-<id>-<description>` should be create in the root repository.
Locally this branch should be merged with main to resolve the conflicts.
The changes should be pushed to the root repository.
Then a pull request against the feature branch should be made.
When such a pull request is merged to main it should be done as a **merge** commit.
A code review should focus on the merge commit fixing the merge conflict and look for possible incorrectly fixed merge conflicts.

_NOTE: Changes to binary files should preferable be done in the main branch and merged back to the feature branch because binary changes can't be compared manually and are therefore difficult to resolve_

## Reviewing pull requests

When your feature is 'ready enough' to be reviewed by peers, be sure to make them aware of that responsibility.
Ping them on GitHub, assign a PR to them, or add the label `Review welcome!`.

Reviewing a feature can be very superficial or in-depth, entirely up to the reviewer and the feature in question.
Typically, if you've reviewed a PR, notify the owner of its condition using simple super-short key phrases such as:

- concept ACK (I'm OK with this idea, haven't looked at the code)
- ut ACK (untested ACK - I've looked at the code and can see nothing obviously fishy)
- tested ACK (I've actually pulled this PR, built the project, and did some digging - it works excellently!)
- NACK { elaborate } (This is utter garbage, and you should feel bad. Here's why [...])
- LGTM (Looks Good To Me)

## Collaborating on a single Pull-Request

When multiple people are contributing commits to a single PR,
it's important not to get commits mixed up into an incompatible tree.
Because maintaining a PR often involves forced pushes to the remote (as a result of rebasing against upstream/main),
it is not easily compatible with collaborative development out-of-the-box.

When multiple people are working on a single pull-request, be sure to be aware of this.
*Don't do any force pushes or rebases without the other person being aware and ready for it*.
It's pretty hard to lose a commit entirely, but there's no reason to make things complicated.

Some pointers:

- If there needs to be a rebase, let the PR owner do this: he proposed the damn thing, he's responsible for its merge-ability.
- If your local tree diverged from the PR, here's a reasonably safe way of migrating to it:

In any case, you should:

````
fetch upstream
stash your uncommitted (and untracked! git stash -u) changes
make a note of any unpushed commits
````

Then you can either:

````
checkout the PR in another branch
cherry-pick your commits
apply the stash
remove the old branch
rename the current branch
````

Alternatively (less safe, but faster):

````
hard reset to upstream PR
cherry-pick commits (they are in the cache)
apply stash
````

Then ping the other person to fetch your stuff, and not have it happen again.
