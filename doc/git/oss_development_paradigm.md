# OSS Development paradigm

First order of business is to correctly set up a local/personal forked repository of AERIUS in which development will take place.
AERIUS is an open-source project that has, for the most part, been privately developed.
Its development paradigm has historically catered to a privately sandboxed repository with a large number of maintainers.
Going forward - in order to improve quality of review and vetting of new code - new code will primarily move into the product by means of pull requests,
as is standard for most open-source paradigms.

This document outlines the preferred method of developing for AERIUS.
It describes how you need to set up a fork,
keep up with the main branch of AERIUS, and submit/maintain pull requests.
This paradigm is similar to the way many OSS projects are run,
so this description may sound familiar.

The steps below assume you are starting from scratch. To migrate an existing AERIUS repository to the setup assumed here,
see [migrate_local_pr_repo](migrate_local_pr_repo.md).

To set up an instant-push-to-upstream-hot-fix-branch,
see [hot fix branch](hot_fix_branch.md).

To squash a pull request, see [squashing a pr](squashing_a_pr.md).

To see some pointers on how to deal with PRs and those pesky other people
working see [collaborative development](collaborative_development.md).

## Setting up a bare project and fork

1 - Go to https://github.com/aerius/calculator and press fork (top right)

This will fork the project to your personal repository space on GitHub.

2 - Clone your personal fork

````
git clone https://github.com/[username]/calculator
````

3 - Set up an AERIUS upstream remote

````
git remote add upstream https://github.com/aerius/calculator
````

4 - Verify

````
git remote -v
````

Should return:

````
origin   https://github.com/[username]/calculator.git (fetch)
origin   https://github.com/[username]/calculator.git (push)
upstream https://github.com/aerius/calculator.git (fetch)
upstream https://github.com/aerius/calculator.git (push)
````

The following image depicts how changes should flow from aerius/main (upstream) to your forked main:

![Fork synchronisation scheme](scheme.png)

## Making changes

To make changes, you can work out of main, or create a branch specific to the feature you're working on. The following steps assume you are creating a feature in a branch called 'feature',

1 - Branch out

````
git checkout -b feature
````

2 - Do interesting development

````
touch feature
echo "code" >> feature
````

3 - Commit changes

````
git add feature
git commit -a -m "Feature"
````

4 - Push changes

````
git push
````

You have now pushed a commit into your personalized fork. To get this feature into the AERIUS repository, you need to create a pull request.

Oftentimes, some time will have passed while you have developed this feature, and the AERIUS main branch has branched away from your feature branch. Let's assume your new feature will not automatically merge into the AERIUS main branch anymore. In this situation, you need to rebase against AERIUS main and resolve any merge conflicts.

5 - Rebase against main

````
git fetch upstream
git rebase upstream/main
````

6 - Resolve any conflicts

7 - Force-push to your personalized fork's feature branch

Due to the rebase, your local branch will have diverged from the branch that lives on your remote origin. You need to force-push your changes to resolve this.

````
git push -f
````

Your feature is complete, and it is ready to be merged into AERIUS's main.

8 - Submit a pull request

In GitHub, view your feature branch, and click [pull request]

Review the pull request, verify it contains the correct commits, and submit it.

At this point, your pull request can be reviewed and vetted by other developers.

So long as the pull request is open can new commits be added to it, simply by pushing more commits into the branch from which it is being merged.

In this case, let's assume your feature is a complicated and big one, and is subject to more extensive review than is typical. By the time the PR has been fully reviewed, it will have diverged from AERIUS main such that it no longer automatically merges. In this case, you need to rebase it again, and push the result.

Refer to steps 5, 6 and 7 to do this.

9 - Pull request is being merged

After due process and review, your pull request will be merged into AERIUS main.
