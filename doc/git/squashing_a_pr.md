# Organizing your Pull-Request

If you're like me, and you commit wayyy too hastily, causing you to do another
3 or so fix-commits right after to account for your incompetence, it may be desirable
to squash those commits together in order not to bloat the git history.

Because the model we're using is already rebasing against main, doing this
is an absolute breeze.

The feature we're using here is squash, which you have access to if
interactively rebasing. You can do this while rebasing against upstream/main,
whatever/whatever, or against nothing at all.

Squashing through an interactive rebase allows you to go back through your
history, and apply each commit in some way which you can specify. You can make
changes, remove commits, add them on top, or merge/squash them into the
previous.

````
# Rebase xxx..zzz onto xxx
#
# Commands:
#  p, pick = use commit
#  e, edit = use commit, but stop for amending
#  s, squash = use commit, but meld into previous commit
#
# If you remove a line here THAT COMMIT WILL BE LOST.
# However, if you remove everything, the rebase will be aborted.
#
````

The basic steps are these:

````
# rebasing against another branch
git rebase -i upstream/main
# rebasing against itself
git rebase -i HEAD~3
# where 3 is any number of commits in local history you want to squash.
````

This will give you something like:

````
pick xxx Did a thing
pick yyy Woops, corrected a thing
pick zzz Damnit, should pay more attention to things
````

Changing that into this:

````
pick xxx Did a thing
squash yyy Woops, corrected a thing
squash zzz Damnit, should pay more attention to things
````

And submitting the result, will result in all 3 commits being squashed into xxx
- pruning rotten apples from your history while none will be the wiser.

Happy squashing!