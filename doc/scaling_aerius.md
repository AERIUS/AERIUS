# Scaling AERIUS

The engine of AERIUS is to make air quality calculations using scientific model implementations, like OPS and SRM2.
These calculations are CPU intensive and can take a long time to run.
On the other hand these calculations can easily be parallelized.
The challenge here is to keep the system operational while handling multiple users.
It is very easy to create a system that becomes unusable in a heartbeat because the calculation of a single user blocks all other users.

To manage such a system there are a number of [non-functional requirements](non_functional_requirements.md) specified.
To build a system that can adhere to these requirements a dedicated task manager is used that works in cooperation with the workers.

## Task Manager

The task manager is the central application through which all jobs are passed and the task manager manages the priority and workload on the workers.
Jobs are organized by the type of work that is performed.
For example OPS calculations are grouped.
Each type of work has its own scheduler in the taskmanager and works independently from other work types.
More information about the AERIUS taskmanager can be found in the repository of the taskmanager: https://github.com/aerius/taskmanager

## Workers

While the taskmanager determines the priority of the tasks.
The worker, or specially, the calculator worker makes sure the single model calculating tasks are not too large.
By limiting the task size to a run time of about a number of seconds single tasks can be handled quickly.
As a result model workers are never claimed too long, and therefore should not cause a situation where no higher priority tasks can be performed because all workers are busy.
As a second measure the calculation worker that breaks up a job in several tasks limits the number of tasks put on the queues at once.
The calculation worker monitors its tasks running and only puts a new task on the queue when a previous task is finished.
This will prevent a single worker from stuffing the queue with all tasks to do.
The calculation worker also monitors the work queue to see if the amount of tasks that can be put on the queue is still right.
If the queue is empty it means all tasks are picked up by workers, which is an indication the workers can pick up more work.
The calculation worker will then increase the number of tasks done at once.
This is a continuous process of increasing or decreasing the number of tasks that can be done at once depending on the queue size.
This dynamic process makes it possible to better use the available resources to calculate jobs in both cases when the system is either busy or quiet.
