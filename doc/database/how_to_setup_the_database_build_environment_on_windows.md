# How to setup the database-build-environment on Windows

## Prepare PostgreSQL and PostGIS

Download the latest PostgreSQL 15 installer.
The current version used on DEV is `15`, newer will probably also work but is at-own-risk.

* Run the installer and use the default configuration.
* Use Stack Builder to install the latest (spatial extention) PostGIS bundle. The current version used on DEV is 3.4.

Open pgAdmin4 and connect to the database as user `postgres`.
Create a new Login Role called `aerius` and make sure all Role privileges are checked.
Reconnect to the database as user `aerius` to make sure this works.

## Prepare Ruby

Download the latest RubyInstaller from https://rubyinstaller.org/downloads/.
A version that is reported to work is 3.3 with Devkit. Newer versions, or versions without Devkit, might also work but are at-own-risk.

Run the installer as Administrator. Check *Add Ruby executables to you PATH* during the install.

In a new commandprompt, install the following Ruby gems like so (make sure to run it as Administrator):

```
gem install net-ssh
gem install net-sftp
gem install clbustos-rtf
```

At the time of writing `net-ssh` version 7.2.0 and `net-sftp` version 4.0.0 are used. If there are reasons to use those exact versions, use a command like `gem install net-ssh -v 7.2.0` instead.

## Prepare Git

* Download the latest Git from https://git-scm.com/download/win, and run the installer 
* Checkout the **Calculator** git repository from https://github.com/aerius/calculator.git
* Checkout the **aerius-database-build** git repository from git@github.com:aerius/database-build.git (at sibling level of AERIUS-II, so same parent folder)

## Prepare database build script

Define your own settings in `\AERIUS-II\source\database\src\build\config\AeriusSettings.User.rb`.

Specify at least the `dbdata` folder and create that path on your machine. Also specify the password for Nexus and for the `aerius` postgres user that you created earlier.

Example:

```Ruby
# Depends on your installation of postgres, default user is 'aerius'
#$pg_username = 'postgres'
$pg_password = '...'

# On Windows, path to postgres bin directory if not in PATH
$pg_bin_path = 'C:/Program Files/PostgreSQL/15/bin'
# Postgres port, if nondefault 5432.
#$pg_port = 5433   

# On Windows, path to git executable if not on PATH
#$git_bin_path = 'c:/Users/xyz/AppData/Local/Atlassian/SourceTree/git_local/bin/'

# Location where the database files will be synced to/looked for when building the full database.
# This location has to exist before syncing.
$dbdata_path = 'c:/Database/db-data/aeriusII/'
# Alternative automatically determining the last part
#$dbdata_path = File.expand_path('D:/aerius/' + $dbdata_dir).fix_pathname
# Default value, relative to current location which isn't often ideal
#$dbdata_path = File.expand_path(File.dirname(__FILE__) + '/../../../../../../db-data-test/aeriusII/').fix_pathname

# Your Nexus credentials should go into the following
$https_data_username = '...'
$https_data_password = '...'

# Option if you want to build on a nondefault tablespace, ensure tablespace is present first
#$database_tablespace = 'ssd'
```

## Run database build scripts

The following commands are most important. They need to run from the AERIUS database root folders, e.g. `source\database`.

It is recommended to make `.bat` files to easily run them in the future. 
If you do, then add `%*` at the end of the commands below to pass on any parameters supplied when running the `.bat` file.
Replace `calculator_nl_latest.rb` with the file for the product that needs to be build.

* **Test database structure only** (no data, very quick):<br>
[`test_structure.bat`]<br>
`ruby ..\..\..\aerius-database-build\bin\Build.rb test_structure.rb src\build\versions\calculator_nl_latest.rb`
  * To include a check for missing data files, use `test_build.rb` instead of `test_structure.rb`.
  * Use `-v structure` to get an automatically generated database name including the version tested.

* **Download all needed data files from https** to `db-data`:<br>
[`sync_db.bat`]<br>
`ruby ..\..\..\aerius-database-build\bin\SyncDBData.rb src\build\versions\calculator_nl_latest.rb`

* **Build full database**:<br>
[`build_quick.bat`]<br>
`ruby ..\..\..\aerius-database-build\bin\Build.rb default.rb src\build\versions\calculator_nl_latest.rb --flags QUICK -v quick`
  * To include creation of a database dump and vacuuming, remove `--flags QUICK`.
  * Using `-v quick` will set the version to 'quick', which is also included in the default database name.
  * You can use `-v #` to set the version instead. Character `#` is substituted with the git hash.
  * You can override the default database name with parameter `--database-name calculator`. 
 
Once the bare scripts work, you can also use [Maven](/doc/development_build_test.md) for building your databases.
