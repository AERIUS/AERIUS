# Custom database builds using buildflags

## Generate nature

If new nature data arrives, the derived tables which depend on it, must be generated from this data. This process was previously performed by the actualisation team through a FME-script, since 2021 this has been included in the build script and now can be triggered through a build flag.
Every time new nature data is provided, these derived tables have to be generated again. The data for the following tables are generated:

* habitats
* hexagons
* receptors
* receptors_to_assessment_areas
* receptors_to_critical_deposition_areas
* relevant_habitat_areas
* relevant_habitats
* setup.geometry_of_interests

Run this command from `source/database` directory.
The call to generate the data using a commandline :

```
ruby ../../../aerius-database-build/bin/Build.rb default src/build/versions/calculator_nl_latest.rb -l generate_nature -v #
```

Explanation: 
* **../../../aerius-database-build/bin/Build.rb**: is the location to the aerius-database-build repository.
* **default**: the call of the default- buildscript.
* **src/build/versions/calculator_nl_latest.rb**: the settings- file used.
* **-l generate_nature**: the buildflag to generate the derived tables.
* **-v #**: the version, "#" gives the value of **$database_name_prefix** and adds the current git-hash

After generating the derived tables an export is done to the **{data_folder}/export/**- folder.
