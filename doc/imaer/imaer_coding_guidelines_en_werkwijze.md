# IMAER coding guidelines & werkwijze

## Coding guidelines

##### Naamgeving
- Als Java standaard:
	- Classes met mixed case, de eerste letter van elk woord uppercase. Gebruik geen underscores of andere rare karakters. Voorbeeld: AeriusCalculatorMetaData.
	- Variables met mixed case, de allereerste letter lowercase, de overige woorden beginnend met uppercase. Gebruik geen underscores of andere rare karakters. Voorbeeld: situationName.
	- Waardes van enumeraties met volledig uppercase. Underscores tussen woorden. Voorbeeld: INDUSTRIAL_ACTIVITY.
- In het Enterprise Architect document zijn er verschillende type classes:
	- \<\<enumeration\>\> voor enumeraties
	- \<\<dataType\>\> voor normale data classes
	- \<\<featureType\>\> voor alle classes met geo informatie (voorzien van geometry). Bij IMAER zijn dat alle EmissionSourceType en afgeleide classes, en CalculationPointType en afgeleide classes.
	Deze moeten een NEN3610ID bevatten, direct of doordat ze een class extenden.
- Daarnaast moet zoveel mogelijk gebruik worden gemaakt (extenden) van een paar basis classes:
	- EmissionSourceType voor classes die een bron vertegenwoordigen.
	- EmissionValueType voor classes die een emissiewaarde vertegenwoordigen. Hierbij kan direct de emissie opgegeven zijn of kan er aan de hand van de properties van de class door middel van een berekening de emissie bepaald worden.
	- CalculationPointType voor classes die een rekenpunt vertegenwoordigen.
- Abstracte classes en enumeraties mogen als suffix Type gebruiken.
- Indien een class een andere class extend, en dit wordt meegenomen in de naam, doe dit door middel van een prefix. Voorbeeld: LodgingSystem -> AdditionalLodgingSystem.
- Gebruik met hoge uitzondering afkortingen in de naamgevingen.
- Namen van classes/variables zijn in het Engels.

##### Enums/domeintabellen
- Enum waardes die vastliggen in de code worden een enum in IMAER. Voorbeeld: Substance en VehicleType.
- Overige lijsten zijn domeintabellen, staan in de en behoren bij een database en worden als CSV ge-exporteerd als bijlage. Deze zitten niet als enum in IMAER en kunnen zelfs gewijzigd worden binnen een versie. 
Voorbeeld: de RAV-codes zoals gebruikt voor farmLodgingType bij StandardFarmLodging.
- Gebruik de setup.ae_export_all_imaer_data(path) functie om de juiste CSV-bestanden te genereren. Pas deze functie aan indien er nieuwe domeintabellen ontstaan.

##### Overig
- Gebruik alleen GM_Point (punten), GM_Curve(lijnen) of GM_Surface(polygonen) voor geometries (of een union van deze zoals EmissionSourceGeometry).
- Gebruik CharacterString voor String waardes, Real voor doubles, Integer voor integers.

## Werkwijze

##### Huidige werkwijze
- Aanpassingen worden eerst in Enterprise Architect doorgevoerd. Ga dus niet direct in de XSD dingen lopen aanpassen, dit kan voor inconsistentie zorgen. Verder zorg je door in EA te beginnen ervoor dat IMAER NEN3610 conform blijft.
- Vervolgens wordt met ShapeChange een xsd bestand en feature catalogue html bestand gegenereerd.
- Daarna volgt nog een handmatige aanpassing in het xsd bestand om ervoor te zorgen dat het IMAER specifieke FeatureCollection type wordt toegevoegd.
Zie ook [Instructions_shapechange_EA.txt](../../aerius-tools/src/main/tools/ShapeChange/Instructions_shapechange_EA.txt).
- Vervolgens worden xsd en EA documenten toegevoegd aan aerius-common (/aerius-common/src/main/resources/imaer/x.y/ voor xsd, /aerius-common/src/main/eap/x.y/ voor EA document).
- Wijzigingen in code en unittests worden doorgevoerd (voor een ophoging in x.y betekent dit code kopieren+aanpassen versienr en opnieuw genereren unittest bestanden).
- Na alle wijzigingen worden aangepaste xsd, EA, feature catalogue, PDF-print van IMAER_FULL via Enterprise Architect en een export van de DB voor de domein tabellen verstuurd naar beheerder.

#### Versienummering

- Versies worden weergegeven als x.y.z, waarbij x de major, y de minor en z de incremental version is.
- X versie wordt opgehoogd bij een grote structurele aanpassing waarbij de backwards compatibility ook niet meer werkt.
- Y versie wordt opgehoogd bij een aanpassing waarbij backwards compatibility zoveel mogelijk behouden moet blijven (wel nieuwe namespace, maar geen verwijdering van objecten of iets dergelijks).
- Z versie wordt opgehoogd bij kleine aanpassingen waar bij een 'oude' versie nog steeds geldig is. Denk aan documentatie wijziging of eventueel toevoeging van een optioneel element)
- Er wordt alleen een xsd met namespace gebruikt per x.y. De x.y zijn hierbij ook in de namespace verwerkt. 
Een wijziging in z levert waarschijnlijk wel een nieuwe xsd op, maar dit is alleen zichtbaar in het 'version' attribuut van 'schema'.

#### Release
- De juiste bestanden (EAP, XSD, Domeintabellen) moeten worden toegevoegd aan de IMAER repository (https://github.com/aerius/IMAER) op het moment dat ze publiek beschikbaar gemaakt kunnen worden. 
- Kijk voor de juiste namen van de folders en waar alles hoort in de Technisch Register documentatie: https://github.com/Geonovum/technisch-register/tree/master/documentatie.
- Maak een nieuwe release op GitHub aan voor deze repository via release -> Draft a new release. Een pre-release is hierbij mogelijk, daarbij wordt het (zover ik weet) niet meegenomen op register.geostandaarden.nl. Op het moment dat een gewone release wordt gedaan, wordt een webhook getriggered waardoor http://register.geostandaarden.nl/imaer/index.html automagisch geupdatet zal worden.

#### Overig
- Zorg voor documentatie bij classes en variables (via Notes in EA). Indien de variable een eenheid heeft, vermeld deze dan (bijvoorbeeld snelheid in km/h).
- De IMAER documentatie is in het Nederlands.
- Zorg er altijd voor dat de IMAER_full view in Enterprise Architect overzichtelijk blijft.

#### Tips
- Aangeven dat een variabele optioneel is in EA kan via rechtermuisklik op property -> View Properties -> Detail. 
- Extra opties zoals van een variabele een attribuut maken kan via hetzelfde, maar dan Tagged Values i.p.v. Detail. Zie bestaande variabelen/ShapeChange documentatie voor verschillende opties.
- Opties van schema kunnen aangepast worden door rechtermuisklik op \<\<applicationSchema\>\> IMAER -> Properties -> Tagged Values.