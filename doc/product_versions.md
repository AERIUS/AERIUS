# AERIUS Releases, Themes and Versions

## Releases and Branches

When a new release is made the code must be properly versioned.
Therefore the following protocol is described on how to version.
This versioning applies to the versions in the maven pom files and git tagging/branches.
Proper versioning is needed to make sure a specific commit of code can be uniquely identified, also in binaries.
All other commits are work-in-progress versions that have a version with the extension `-SNAPSHOT`.
When creating a release, a single commit will get a unique version name, which should only be present in that commit.

### Preparations

Once it is decided a release is to be made all libraries that we maintain and are used with a `SNAPSHOT` version need to be released.
This means no `SNAPSHOT` versions should be present in dependencies anymore.

### Branching

When all commits are merged that need to be included in the release a branch is created with the name `<version>_fixes`.
The release will be made from this branch.
Preferably the main branch version will be increment to use a future release version.
After creating the `fixes` release branch all patches to the release branch should be cherry-picked from the main branch.
If the release is a new major release the main branch version should become something like `<version>.1.0-SNAPSHOT` or increment if this is not the first update.
This is based on the assumption a future release will be an increment of the major release.

### Release Candidate

When a release is to be made it should be decided if it is the official release that will be publicly available or if it is a release for external testing.
Before the official release 1 or more moments take place that other parties will test the new release.
When these external tests take place a release candidate will be created.
This is done by setting the version to `<version>-RC<number>` on the release branch.
For the first release candidate this will be `<version>-RC1`.
The next commit after that should set the version to the release `SNAPSHOT` version again.
This process can be repeated each time an external test session will be held.

### Official Release

Once the official release date/moment is established the final release version is committed on the release branch.
This should be done shortly before the official release moment.
This version should get the official release version number.
In general the major release number contains the year in the version name.
The next commit after the release commit should set the version a patch increment of the release `<version>.0.1-SNAPSHOT` version.
The main branch will than be updated to the next minor SNAPSHOT version version: `<version>.1.0-SNAPSHOT`.

### Minor/Patch Release

When updates/patches are done/needed after the release they will get an increment to the release version.
This can be either minor (e.g. `.1.0`) or patch (e.g. `.0.1`) release, whatever is deemed appropriate.
In general a patch release is expected to be a hotfix that is cherry-picked from a commit from the main branch to the release branch.
A minor release is a release when in general no data changes has been made that have effect on the calculations.
This means the database `STABLE` is not updated to get the content of `LATEST`.
The minor release is done from the main branch.
A minor release will start with its own fixes release branch with the minor version in the branch name.
After each release the fixes release branch should be set to `SNAPSHOT` version again.

### The Release Step-by-Step

* Initially development of a new major release starts with a SNAPSHOT version (e.g. `2023.2.0-SNAPSHOT`) on the main branch.
* When a release is about to be made the release branch is created from the main branch including the new major version name. e.g. `calculator2024_fixes`.
* The releases branch will get a version where the major version is appended with `SNAPSHOT`, e.g. `2024-SNAPSHOT`.
* When it is time to create a release for external testing, the version on the `_fixes` branch will be updated to contain the `RC<number>`, e.g. `2024-RC1`
* Afterwards, the release branch will get the SNAPSHOT version again, e.g. `2024-SNAPSHOT`.
* This can be repeated as many times as additional test releases are needed.
* Each time the release candidate number is incremented.
* Once the final release is done it will get the plain release version number, e.g. `2024`.
* The fixes branch will continue with a patch increment version on the major version number, e.g.`2024.0.1-SNAPSHOT`.
* The main branch will be continue as the next minor release version based on the released version number, e.g.`2024.1.0-SNAPSHOT`.

## From LATEST to STABLE

When a new major product release is done where the data and model have been updated the LATEST version is set as the STABLE version.
These are the changes to be made:

### Database

In `source/database/src/data/sql` the directory of the latest version should be copied to the stable version.
Best is to first remove the stable directory and than copy the latest directory content in the stable directory.
This to avoid keeping possible unused files in the old stable branch.

In the database the version of the calculation models is also set.
Because latest was copied to stable, these values are at latest and need to be set to stable.
The constants are stored in the table `system.constants` and this can be found in the `version_constants.sql` file.

### Pollution model versions

Each pollution model implementation has specific version information of tools and/or data used.
They also maintain a stable and latest version.
When creating a new stable version from the latest, the content of these version information should be updated for stable.

This is the information for the implemented models:

#### OPS

OPS version information is stored in `OPSVersion.java`, that is located in
`source/commons-aerius/aerius-commons-ops/src/main/java/nl/overheid/aerius/ops/version/OPSVersion.java`.

#### SRM

SRM version information is stored in `AeriusSRMVersion.java` that is located in `source/commons-aerius/aerius-commons-srm/src/main/java/nl/overheid/aerius/srm/version/AeriusSRMVersion.java`.

#### ADMS

ADMS version information is stored in `ADMSVersion.java` that is located in `source/commons-aerius/aerius-commons-adms/src/main/java/nl/aerius/adms/version/ADMSVersion.java`.
