# Non-functional requirements (NFR)

To have a functional working application, criteria must be specified to judge the operation of the system.
For AERIUS the following non-functional requirements are specified based on the intentions and operating of the system.
[Non-functional requirements (NFRs) implementations](nfr_implementation.md) explains how these non-functional requirements are implemented in AERIUS.

## Performance Efficiency (PE)

These requirements specify the amount of work and speed the system needs to meet.

| Nr. | NFR                                                                                                                                   |
|-----|---------------------------------------------------------------------------------------------------------------------------------------|
| PE1 | The web interface must be responsive to the user                                                                                      |
| PE2 | Jobs which can possibly claim large resources over a longer period should be performed in workers and not in the web server           |
| PE3 | A single job may not claim all workers                                                                                                |
| PE4 | A specific type of job may not claim all workers                                                                                      |
| PE5 | When more capacity is added to the system it should automatically scale up                                                            |
| PE6 | Jobs for calculation workers should be split in small enough tasks such that a single calculation doesn't take longer than 30 seconds |
| PE7 | Interactive calculation jobs are split into smaller tasks to improve interactivity. A task should run around 2 seconds                |
| PE8 | Background calculation jobs may be split into tasks of bigger size, but not too big to violate PE6                                    |

## Usability (U)

These requirements specify in non-functional terms what can be said about how the user should be able to use the application.

| Nr. | NFR                                                                                                                                        |
|-----|--------------------------------------------------------------------------------------------------------------------------------------------|
| U1  | End users should be able to use the web interface with minimal training                                                                    |
| U2  | The web interface should be accessible. Due to the use of complex map features, the map does not have be accessible conform web guidelines |

## Reliability (R)

These requirements specify the ability of a system or component to function under stated conditions for a specified period of time and resistance to failure.

| Nr. | NFR                                                                                            |
|-----|------------------------------------------------------------------------------------------------|
| R1  | Interactive jobs should have higher priorities than background jobs                            |
| R2  | The system should be able to recover (easily) after a crash                                    |
| R3  | When a worker crashes it should shut itself down so the job can be performed by another worker |
| R4  | Jobs that fail in execution may not stop a worker from executing other jobs                    |

## Maintainability (M)

This requirement specifies the guidelines regarding the development of the source code.

| Nr. | NFR                                                                                                                      |
|-----|--------------------------------------------------------------------------------------------------------------------------|
| M1  | New source code should not add new SonarQube violations and not break unit tests                                         |
| M2  | All new source code should be reviewed and approved by at least 1 other developer before merging it into the main branch |
| M3  | All source code should be tested after each commit in the main branch                                                    |
| M4  | Server/worker code should also be unit tested with a focus on business logic                                             |
| M5  | User interface code should be tested with UI tests                                                                       |
| M6  | The number of third-party libraries should be limited as much as possible                                                |
