# Non-functional requirements (NFRs) implementations
This page describes how the [NFRs](non_functional_requirements.md) are implemented in AERIUS.
The description is broken down per NFR.

## Performance Efficiency (PE)

These requirements specify the amount of work and speed the system needs to meet.

### PE1

> The web interface must be responsive to the user.
* When possible business logic should be done in the web interface not requiring a round trip to the server.
* Regarding calculations, huge amounts of data transferred from the server to the client may not block the user interface or take to long to render such that the user interfaces freezes.

### PE2

> Jobs which can possibly claim large resources over a longer period should be performed in workers and not in the web server.
* Any action requiring numerous calls and/or actions taking a long while - like calculations - are performed in the workers.

### PE3

> A single job may not claim all workers.
* The amount of concurrent tasks performed by a worker is limited.
  These limits are defined in the configuration of the task manager.

### PE4

> A specific type of job may not claim all workers.
* The task manager doesn't schedule a job when a specific job is already performed and only 1 free worker is available.
Additionally, this can be made stricter by limiting the maximum capacity a specific job may claim defined as a percentage of the total available workers in the task manager configuration.

### PE5

> When more capacity is added to the system it should automatically scale up.
* When new workers are started they connect to the RabbitMQ message bus with 1 channel per available process.
  The task manager monitors the queues and the number of channels and automatically scales the availability according to the number of available channels.

### PE6, PE7, PE8

> * Jobs for calculation workers should be split in small enough tasks such that a single calculation doesn't take longer than 30 seconds.
> * Interactive calculation jobs are split into smaller tasks to improve interactivity. A task should run around 2 seconds.
> * Background calculation jobs may be split into tasks of bigger size, but not too big to violate PE6.
* The amount of time an average OPS task takes based on the number of input sources and receptors has been determined.
This information is used to split ops calculation tasks into smaller tasks by calculating how many receptors could be calculated given the number of input sources and the maximum desired calculation time.
This is defined in ops units, where 1 ops unit is the average time it takes to calculate 1 source and 1 receptor (minus overhead of starting/ending ops).
In the database system.constants table a distinction is made for the max amount of OPS units for background calculation jobs (i.e. PDF/GML export/Connect jobs) and the max amount of OPS unit for interactive calculation jobs (i.e. calculations for the web interface).

## Usability (U)

These requirements specify in non-functional terms what can be said about how the user should be able to use the application.

### U1

> End users should be able to use the web interface with minimal training.
* The user interface is designed in cooperation with end users and uses terminology and data familiar to the end user. E.g. the input for farming sources uses industry known RAV codes in combination with an amount instead of raw emissions.

### U2

> The web interface should be accessible. Due to the use of complex map features, the map does not have be accessible conform web guidelines.
* The user interface is designed to be compliant with WCAG accessibility guidelines.
* The map is excluded from this requirement.

## Reliability (R)

These requirements specify the ability of a system or component to function under stated conditions for a specified period of time and resistance to failure.

### R1

> Interactive jobs should have higher priorities than background jobs.
* In the taskmanager a priority can be set for specific job types. The priority of interactive jobs (i.e. for the web interface) is set higher than background
jobs to guarantee this requirement.

### R2

> The system should be able to recover (easily) after a crash.
* The jobs are placed in the RabbitMQ queue in a persistent configuration.
This means when RabbitMQ goes down the queue state is preserved and restored when RabbitMQ is restarted.
Furthermore, any job picked up by a worker is not acknowledged until the task is finished.
If the worker crashes during the run the unacknowledged task is put back on the queue to be picked up by another worker guaranteeing eventual execution.
The task manager also picks up unacknowledged tasks and acknowledges them when they are passed to the specific worker queue.
This makes it possible to restart the task manager without losing jobs.

### R3

> When a worker crashes it should shut itself down so the job can be performed by another worker.
* The worker might crash due to a bug or for example a full file system.
In the later case the job will simply be picked up by another worker.
In case of a bug it can cause a cascading effect, i.e.: the job is picked up again, the new worker fails, puts it back on the queue, the next worker picks it up and so on until there are no working left causing the system to halt.
This is a failsafe mechanism based on the assumption that if a worker would fail it is more likely because of a circumstantial issue specific for that worker/server and that the job can be executed by another worker without any issues.
If all workers would shutdown due to a bug there is a fair chance that a bug fix is needed until the workers can be started again.
More likely the offending job will be removed from the queue so the system can continue to operate while fixing the issue in the meantime and giving the user a work-around if one is available for the time being.
In a case messages need to be (re)moved the [intercept](intercept_cli.md) command line tool can help.

### R4

> Jobs that fail in execution may not stop a worker from executing other jobs
* When a job is failing (e.g. it contains illegal input), the worker should mark the job as failed and continue processing other jobs, to prevent workers from getting stuck.
* There should be a difference in a worker problem (R3) and a job problem (R4).

## Maintainability (M)

This requirement specifies the guidelines regarding the development of the source code.

### M1

> New source code should not add new SonarQube violations and not break unit tests.
* Each new pull request is checked against SonarQube using a comparison with the base branch.
* Unit tests are being run after each commit in the main branch.

### M2

> All new source code should be reviewed and approved by at least 1 other developer before merging it into the main branch.
* GitHub is configured to only allow merging pull requests after at least one developer approving the merge.

### M3

> All source code should be tested after each commit in the main branch
* After each commit in the main branch, the code is checked out, compiled, unit tested and deployed onto the development environment.
* After deployment on the development environment, UI tests are executed on the web interface.

### M4

> Server/worker code should also be unit tested with a focus on business logic.

### M5

> User interface code should be tested with UI tests
