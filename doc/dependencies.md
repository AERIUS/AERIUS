# Dependencies

AERIUS is built using several in-house tooling and dependencies.
These parts of the code reside in separate repositories to maintain reusability in other (AERIUS) products.
This table refers to the other (in-house) dependencies and their README files / documentation.

| Project                                                                             | Description                                |
|-------------------------------------------------------------------------------------|--------------------------------------------|
| [database-build](https://github.com/aerius/database-build/)                         | Database build scripts                     |
| [IMAER-java](https://github.com/aerius/IMAER-java/)                                 | IMAER including import, export, validation |
| [geoserver-core](https://github.com/aerius/geoserver-core)                          | Geoserver core configuration               |
| [gwt-client-common](https://github.com/aerius/gwt-client-common/)                   | Shared GWT client code                     |
| [gwt-client-common-json](https://github.com/aerius/gwt-client-common-json/)         | Shared GWT client code on parsing JSON     |
| [search](https://github.com/aerius/search/)                                         | Shared search service                      |
| [sld-generator-maven-plugin](https://github.com/aerius/sld-generator-maven-plugin/) | Maven plugin to generate SLDs              |
| [taskmanager](https://github.com/aerius/taskmanager/)                               | Task manager                               |
| [tomcat-standalone](https://github.com/aerius/tomcat-standalone)                    | Tomcat standalone                          |
| [tools](https://github.com/aerius/tools/)                                           | Shared AERIUS IDE tooling                  |
| [webdocument-exporter](https://github.com/aerius/webdocument-exporter/)             | HTML to PDF generator                      |
