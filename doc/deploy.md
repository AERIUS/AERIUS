# Production 

Running AERIUS requires several products to be installed and configuration of 
those products as well as configuration of the AERIUS products.

## Required software
To run AERIUS the following external software needs to be installed on the
servers. Both Tomcat and the AERIUS applications require Java to be installed: 
[Java 8+ SDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

PostgreSQL needs to be installed on the database server:
[PostgreSQL 9 + PostGIS](http://www.postgresql.org/download/), install PostGIS 

Tomcat needs to installed on the servers hosting the web pages. It's not
uncommon to install an Apache webserver as a proxy before Tomcat. The
Apache web server can then be used to provide additional security.
[Tomcat 7+] (https://tomcat.apache.org/download-70.cgi)

The messages is done via RabbitMQ. RabbitMQ needs to be installed on 1 server:
[RabbitMQ] (https://www.rabbitmq.com/download.html)

After installing RabbitMQ, the management plugin needs to be enabled. For this 
run the following command from the RabbitMQ installation directory:
`sbin\rabbitmq-plugins.bat enable rabbitmq_management`.

## AERIUS products
The AERIUS products are the applications part of the AERIUS products. They are 
build with maven and result in war and jar files.

### Web server
The web server host the web application. This can be any Java application
server. AERIUS was developed using Tomcat, thus that is what is used here.
To install the AERIUS web applications the war files for the product are
deployed in Tomcat. The war doesn't contain a contex.xml, therefore a
context.xml must be installed in Tomcat. The template can be found
in the server project: [Context.xml](/aerius-wui-server/src/main/config/META-INF/context.xml).
In a local development environment this file should be placed in the webapps
directory of the specific server project. The *META-INF* directory is excluded
from git, so it can't be committed by accident.

Next to product specific web applications there is a Geoserver web application.
The Geoserver web application in the AERIUS project is a complete war including
the AERIUS needed configuration. Installing the war is sufficient.
To use a Geoserver instance some constants in the system database table must be
configured. See system constants below. 

### Task Manager
The task manager application manages the task priorities. There should be one
instance running of the task manager. The task manager is a java application
that can be start as follows:

> java -jar aerius-taskmanager-{version}.jar -Dlogback.configurationFile=logback.xml -config taskmanager.properties

Examples of the property files can be found in the aerius-taskmanager project
in the [src/main/config](/aerius-taskmanager/src/main/config) directory:

[example taskmanager.properties](/aerius-taskmanager/src/main/config/taskmanager.properties)

[example logback.xml](/aerius-taskmanager/src/main/config/logback.xml)

The configuration of the queues is read from the database as configured in
the taskmanager.properties file. By default the calculator database is 
loaded with the required queue configuration and doesn't need to be modified
unless new queues are needed.

If new queues would be needed or if priority changes would be made the task
manager must be restarted. The task manager doesn't automatically pick up
changes and there is no other way then editing the database to make changes. 

### Workers
The worker applications perform the long running tasks. Some workers listen to
the queue for new tasks, other workers run on their own schedule. Technically
there is one worker jar file. Via the configuration file passed to the worker
the specific worker is started. The worker is a java application that can be
start as follows:

> java -jar aerius-tasks-{version}.jar -Dlogback.configurationFile=logback.xml -config worker.properties

Examples of the property files can be found in the aerius-tasks project
in the [src/main/config](/aerius-tasks/src/main/config) directory:

[example worker.properties](/aerius-tasks/src/main/config/worker.properties)

[example logback.xml](/aerius-tasks/src/main/config/logback.xml)

## System Management and Configuration.

To manage the system and the configuration there are several places to go.

In the database a list of constants are available that need to be set in
order to get it to work in a specific configuration. More information on
these constants can be found in the file: [ConstantsEnum](/aerius-core-repository/src/main/java/nl/overheid/aerius/enums/ConstantsEnum.java)

To get the workers up and running they need to be configured with specific
options. In the default [worker.properties] (/aerius-tasks/src/main/config/worker.properties)
file in the config directory all options possible are put. This file can
be used as starting point for worker configuration, and also contains some
explanation on what each option does.

### Worker job management

On some occasions intercepting these jobs may be warranted. See the
[Intercept Command](intercept_cli.md)  page for more info.

