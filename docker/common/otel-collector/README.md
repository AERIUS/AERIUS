### OpenTelemetry collector

The [OpenTelemetry collector](https://github.com/open-telemetry/opentelemetry-collector-releases) Docker image will be used. We'll only add a default config for your convenience.
Check out it's documentation for the specifics.

##### Example build
```shell
docker buildx build -t aerius-otel-collector:latest .
```

##### Example run
```shell
docker run --rm -it --network host \
  aerius-otel-collector:latest
```
