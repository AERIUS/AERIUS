#!/usr/bin/env bash

echo '[PRESCRIPT] Looking for Chrome installation..'
AERIUS_CHROME_BROWSER_PATH=$(/tmp/selenium-manager --offline --browser chrome --browser-version ${AERIUS_CHROME_VERSION} --output SHELL | \
   grep '^Browser path: ' | \
   cut -d ':' -f 2)

if [[ -z "${AERIUS_CHROME_BROWSER_PATH}" ]]; then
  echo '[PRESCRIPT] Could not find Chrome installation. Crashing hard..'
  exit 1
fi

echo '[PRESCRIPT] Found Chrome installation: '"${AERIUS_CHROME_BROWSER_PATH}"
export AERIUS_CHROME_ROOT=$(dirname "${AERIUS_CHROME_BROWSER_PATH}")
echo '[PRESCRIPT] AERIUS_CHROME_ROOT set to: '"${AERIUS_CHROME_ROOT}"

echo '[PRESCRIPT] Launching Chrome headless'
# Run in background, it's output will be send to stdout, which is what we want
${AERIUS_CHROME_BROWSER_PATH} \
  --headless \
  --disable-dev-shm-usage \
  --no-sandbox \
  --disable-gpu \
  --disable-features=Vulkan \
  --disable-search-engine-choice-screen \
  --disable-features=OptimizationGuideModelDownloading,OptimizationHintsFetching,OptimizationTargetPrediction,OptimizationHints \
  --remote-debugging-port=9222 \
  --remote-allow-origins='*' \
  --window-size=1920,1080 &

# If remote debugging is requested, launch a proxy on port 9221 to 9222.
# Do note that the CDP will point you to port 9222 in some calls, so for it to properly work
#  you will need to setup something like a load balancer that will redirect localhost:9222
#  to this container on 9221.
# If using Traefik this is also present in the current configuration.
if [[ "${AERIUS_CHROME_ENABLE_REMOTE_DEBUGGING}" == 'true' ]]; then
  echo '[PRESCRIPT] Launching socat proxy to enable Remote Debugging'
  socat tcp-listen:9221,fork tcp:127.0.0.1:9222 &
fi
