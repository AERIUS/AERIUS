### Base for GeoServer images

Handles enable/disabling the GeoServer UI Console.
Adds generic fonts to be available for all GeoServer images.

Only for use in other images as a template.

##### ENV variables for running images based on this
- `GEOSERVER_CONSOLE_DISABLED`: If set to true will disable the console. By default the console is enabled.
- `GEOSERVER_EXTERNAL_URL`: Specify the external URL that is used to access this GeoServer (optional)
