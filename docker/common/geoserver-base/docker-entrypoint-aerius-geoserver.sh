#!/bin/sh

set -e

# if GEOSERVER_CONSOLE_DISABLED is set to true, disable the web interface - this also speeds up startup
if [ "${GEOSERVER_CONSOLE_DISABLED}" = "true" ]; then
  export JAVA_OPTIONS="${JAVA_OPTIONS} -DGEOSERVER_CONSOLE_DISABLED=true"
fi

# set proxy base url to external url
export JAVA_OPTIONS="${JAVA_OPTIONS} -DPROXY_BASE_URL=$GEOSERVER_EXTERNAL_URL"

# execute the CMD
exec "${@}"
