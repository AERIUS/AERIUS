### Connect API image

The webapp jar should be placed in this directory as `app.jar` while building the image.

##### Example build
```shell
docker buildx build -t aerius-connect-api:latest .
```

##### Example run
```shell
docker run --rm --network host \
  -p 8080:8080 \
  aerius-connect-api:latest
```

##### Example run with different DB password
```shell
docker run --rm --network host \
  -e SPRING_DATASOURCE_PASSWORD="password" \
  -p 8080:8080 \
  aerius-connect-api:latest
```
