#!/usr/bin/env bash

# Per property check if they are configured and if so add them to the list
#  of services to check before starting up
SERVICES_PROCESSED=0
for CONFIG_PROPERTY in AERIUS_CHUNKER_PROCESSES AERIUS_EMAIL_PROCESSES AERIUS_IMPORTER_PROCESSES AERIUS_JOBCLEANUP_ENABLED AERIUS_PDF_PROCESSES AERIUS_NOTIFY_PROCESSES; do
  CONFIG_PROPERTY_VALUE="${!CONFIG_PROPERTY}"

  # If DB URL is not set, skip
  if [[ -z "${AERIUS_DATABASE_URL}" ]]; then
    continue
  fi

  # If configuration name ends with _PROCESSES and isn't higher than 0, skip it.
  if [[ "${CONFIG_PROPERTY}" == *"_PROCESSES" ]] \
      && ([[ -z "${CONFIG_PROPERTY_VALUE}" ]] \
          || ! (( "${CONFIG_PROPERTY_VALUE}" > 0 ))); then
    continue
  # If configuration name ends with _ENABLED and isn't set to true, skip it
  elif [[ "${CONFIG_PROPERTY}" == *"_ENABLED" ]] \
      && ! [[ "${CONFIG_PROPERTY_VALUE}" == 'true' ]]; then
    continue
  fi

  echo '[PRESCRIPT] Found '"${CONFIG_PROPERTY}"' that needs database configuration.'
  (( SERVICES_PROCESSED++ )) || true
done

if (( "${SERVICES_PROCESSED}" > 0 )); then
  # Get service entry
  SERVICE_ENTRY="${AERIUS_DATABASE_URL##*://}"
  SERVICE_ENTRY="${SERVICE_ENTRY%%/*}"

  echo '[PRESCRIPT] Adding wrapper service check for: '"${SERVICE_ENTRY}"
  export AERIUS_WRAPPER_CHECK_SERVICES="${AERIUS_WRAPPER_CHECK_SERVICES},${SERVICE_ENTRY}"
# Add a warning in case of no validations
else
  echo '[PRESCRIPT] WARNING! Nothing found to do a validation for. In most cases this probably means you configured something wrong and you should fix that.'
  echo '[PRESCRIPT] Or you are using this container image in an advanced way and if so I salute you and you can ignore this.'
fi
