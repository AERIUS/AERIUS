### (Base) Worker image

Contains everything for a non-specific worker to run. For specific workers look for extending images like `aerius-worker-ops`.
Will only log to console as advised for Docker images.

Contains a dummy `worker.properties`.
All extra `worker.properties` configurations should by provided by using ENV variables.
Use `AERIUS_[PROPERTY NAME AS FOUND IN worker.properties IN UPPERCASE AND DOTS REPLACED BY UNDERSCORES]`.
i.e.: `database.calculator.processes` becomes `AERIUS_DATABASE_CALCULATOR_PROCESSES`.

Another way is to provide your own `worker.properties` and using volumes to map it to `/worker.properties`.
Do note though that ENV variables will take precedence.
Can be used standalone or for use in other images.

The worker jar should be placed in this directory as `app.jar` while building the image.
`monitorsrm2` directory should be placed in this directory while building the image.

##### ENV variables that are required or set by default for convenience
- `AERIUS_BROKER_HOST`: Defaults to `localhost`.
- `AERIUS_BROKER_USERNAME`: [REQUIRED]
- `AERIUS_BROKER_PASSWORD`: [REQUIRED]
- `AERIUS_BROKER_PORT`: Defaults to `5672`.
- `AERIUS_BROKER_VIRTUALHOST`: Defaults to `/`.

##### Example build
```shell
docker buildx build -t aerius-worker:latest .
```

##### Example run
```shell
docker run --rm -it --network host \
  -e AERIUS_BROKER_USERNAME=user \
  -e AERIUS_BROKER_PASSWORD=password \
  -e AERIUS_DATABASE_URL=jdbc:postgresql://localhost/calculator \
  -e AERIUS_DATABASE_USERNAME=username \
  -e AERIUS_DATABASE_PASSWORD=password \
  -e AERIUS_CHUNKER_PROCESSES=4 \
  aerius-worker:latest
```

##### Example run with own worker.properties
```shell
docker run --rm -it --network host \
  -e AERIUS_BROKER_USERNAME=user \
  -e AERIUS_BROKER_PASSWORD=password \
  -v path/to/my/Custom/worker.propties:/worker.properties \
  aerius-worker:latest
```
