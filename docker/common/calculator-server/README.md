### calculator-server image

The standalone jar should be placed in this directory as `app.jar` while building the image.

##### Example build
```shell
docker buildx build -t aerius-calculator-wui-server:latest .
```

##### Example run
```shell
docker run --rm --network host \
  -p 8080:8080 \
  aerius-calculator-wui-server:latest
```

