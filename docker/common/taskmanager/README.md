### Taskmanager image

The [open source taskmanager](https://github.com/aerius/taskmanager) Docker image will be used. We'll only add our default queue configuration for your convenience.
Check out it's documentation for the specifics.

##### Example build
```shell
docker buildx build -t aerius-taskmanager:latest .
```

##### Example run
```shell
docker run --rm -it --network host \
  -e BROKER_PASSWORD=password \
  aerius-taskmanager:latest
```
