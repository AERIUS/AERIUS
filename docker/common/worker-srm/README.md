### SRM Worker image

`srm` directory should be placed in this directory while building the image.

##### Example build
```shell
docker buildx build -t aerius-worker-srm:latest .
```

##### Example run
```shell
docker run --rm -it --network host \
  -e AERIUS_BROKER_USERNAME=user \
  -e AERIUS_BROKER_PASSWORD=password \
  aerius-worker-srm:latest
```

##### Example run with 4 SRM2 processes (defaults to 1)
```shell
docker run --rm -it --network host \
  -e AERIUS_BROKER_USERNAME=user \
  -e AERIUS_BROKER_PASSWORD=password \
  -e AERIUS_SRM_PROCESSES=4 \
  aerius-worker-srm:latest
```
