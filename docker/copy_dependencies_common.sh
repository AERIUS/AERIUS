#!/usr/bin/env bash

# Do not execute this script yourself, make use of the 'copy_dependencies.sh' scripts as present in the profile directories. e.g.: in directory 'nature'.

SOURCE_DIR='..'
DOCKER_COMMON_DIR='common'

# Exit on error
set -e

# Change current directory to directory of script so it can be called from everywhere
SCRIPT_PATH=$(readlink -f "${0}")
SCRIPT_DIR=$(dirname "${SCRIPT_PATH}")
cd "${SCRIPT_DIR}"

# include functions
source ./include.functions.sh

# database
# create source directory and put all sources in there so we can use a single COPY statement in the Dockerfile
# (make sure it's isolated in 1 layer instead of multiple)
if _is_module_enabled "${1}" 'database-calculator' || _is_module_enabled "${1}" 'database-calculator-uk' || _is_module_enabled "${1}" 'database-check' || _is_module_enabled "${1}" 'database-integrationtests'; then
  mkdir -p "${DOCKER_COMMON_DIR}"/database/source
  cp -Rauv "${SOURCE_DIR}"/source/database \
           "${DOCKER_COMMON_DIR}"/database/source/
fi

# worker
if _is_module_enabled "${1}" 'worker'; then
  WORKER_JAR_FILE=$(find "${SOURCE_DIR}"/source/calculator/aerius-worker/target/ -name aerius-worker-'*'.jar -not -name '*'tests'*')
  cp -auv "${WORKER_JAR_FILE}" \
          "${DOCKER_COMMON_DIR}"/worker/app.jar
fi

# worker-ops
if _is_module_enabled "${1}" 'worker-ops'; then
  cp -auv "${DOCKER_COMMON_DIR}"/worker/worker.properties \
          "${DOCKER_COMMON_DIR}"/worker/app.jar \
          "${DOCKER_COMMON_DIR}"/worker-ops/
fi

# worker-srm
if _is_module_enabled "${1}" 'worker-srm'; then
  cp -auv "${DOCKER_COMMON_DIR}"/worker/worker.properties \
          "${DOCKER_COMMON_DIR}"/worker/app.jar \
          "${DOCKER_COMMON_DIR}"/worker-srm/
fi

# calculator-server
if _is_module_enabled "${1}" 'calculator-server'; then
  cp -auv "${SOURCE_DIR}"/source/calculator/aerius-calculator-wui-server-standalone/target/aerius-calculator-wui-server-standalone-*.war \
          "${DOCKER_COMMON_DIR}"/calculator-server/app.jar
fi

# api
if _is_module_enabled "${1}" 'api'; then
  cp -auv "${SOURCE_DIR}"/source/calculator/aerius-connect-service/target/aerius-connect-service-*.jar \
          "${DOCKER_COMMON_DIR}"/api/app.jar
fi

# geoserver-calculator
if _is_module_enabled "${1}" 'geoserver-calculator'; then
  cp -auv "${SOURCE_DIR}"/source/geoserver/aerius-geoserver-calculator/target/geoserver-calculator\#\#*.war \
          "${DOCKER_COMMON_DIR}"/geoserver-calculator/app.jar
fi

# geoserver-opendata
if _is_module_enabled "${1}" 'geoserver-opendata'; then
  cp -auv "${SOURCE_DIR}"/source/geoserver/aerius-geoserver-opendata/target/geoserver-opendata\#\#*.war \
          "${DOCKER_COMMON_DIR}"/geoserver-opendata/app.jar
fi

# Change current directory to previous one so scripts calling this one can function properly
cd - > /dev/null
