# Docker

## Steps to run the application on a local Docker Swarm
```bash
### Needs to be done once to setup Docker Swarm
# Init Docker Swarm locally
$ docker swarm init
# Create web network to be used by traefik
$ docker network create -d overlay web

### Full flow when starting from scratch
# Export SERVICE_THEME so we can re-use which environment we are trying to build and deploy
$ export SERVICE_THEME=[NL / UK / LBV]
# Build applications (add `-PJNCC` to build UK artifacts)
$ mvn clean package -Pdeploy -DskipTests
# Go to Docker directory
$ cd docker
# Prepare docker environment (will copy dependencies build and more) *
$ ./prepare.sh
# Build the containers *
$ ./build.sh
# Deploy environment
$ docker stack deploy -c generated/docker-compose.yaml -c "${SERVICE_THEME}"/docker-compose.traefik.yaml "${SERVICE_THEME}"
# Get to it using http://localhost or http://127.0.0.1.
```

##### * Some environment variables are required. The scripts will tell you which ones. I'd recommend to set them system-wide as they can be used in other products as well.
