### ADMS Worker image

##### Example build
```shell
docker buildx build -t aerius-worker-adms:latest .
```

##### Example run
```shell
docker run --rm -it --network host \
  -e AERIUS_BROKER_USERNAME=user \
  -e AERIUS_BROKER_PASSWORD=password \
  aerius-worker-adms:latest
```

##### Example run with 4 ADMS processes (defaults to 1)
```shell
docker run --rm -it --network host \
  -e AERIUS_BROKER_USERNAME=user \
  -e AERIUS_BROKER_PASSWORD=password \
  -e AERIUS_ADMS_PROCESSES=4 \
  aerius-worker-adms:latest
```