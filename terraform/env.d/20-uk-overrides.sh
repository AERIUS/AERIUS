#!/usr/bin/env bash

# Crash on error
set -e

if [[ "${SERVICE_THEME}" == 'UK' ]]; then
cat << EOF
  ecs_ctr_ebs_size = "100"
EOF
fi
