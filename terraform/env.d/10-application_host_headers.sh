#!/usr/bin/env bash

# Crash on error
set -e

cat << EOF
  application_host_headers = {
    "CALCULATOR" = "${DEPLOY_WEBHOST}",
    "CHECK"      = "${DEPLOY_WEBHOST}",
    "CONNECT"    = "${DEPLOY_WEBHOST}",
    "OPENDATA"   = "${DEPLOY_WEBHOST}",
  }
EOF
