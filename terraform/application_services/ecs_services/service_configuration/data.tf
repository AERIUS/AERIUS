#############################
# COLLECT REMOTE STATE DATA #
#############################
locals {
  theme_arr = split(",", var.service["theme"])
}

## RDS ENDPOINT
data "aws_ssm_parameter" "rds_endpoint" {
  count  = var.rds_count > 0 ? 1 : 0

  name = "${lower(local.name_ssm_prefix)}/rds/endpoint_0"
}

## Mail USERNAME
data "aws_ssm_parameter" "username_mail" {
  name = "/shared/mail/username"
}

## Mail PWD
data "aws_ssm_parameter" "password_mail" {
  name = "/shared/mail/password"
}

## ADMS License
data "aws_ssm_parameter" "license_adms" {
  count  = contains(local.theme_arr, "UK") ? 1 : 0

  name   = "/shared/licenses/adms"
}
