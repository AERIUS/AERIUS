locals {
  queue_config_per_queue = { for queue in local.queue_configs : queue => fileexists("${path.module}/taskmanager_queue_configs/${local.taskmanager_queue_config_key}_${lower(queue)}.json") ? jsonencode(replace(file("${path.module}/taskmanager_queue_configs/${local.taskmanager_queue_config_key}_${lower(queue)}.json"), "\n", "")) : "" }
}

output "services" {
  description = "The services object containing all configuration"
  value = yamldecode(templatefile("${path.module}/services.yaml.tftpl", {
      APP_TIMEZONE  = var.app_timezone,
      APP_VERSION   = var.app_version,
      PRODUCT_THEME = var.service["theme"],
      PRODUCT_TYPE  = var.service["type"],

      THEME_NL      = contains(local.theme_arr, "NL") ? "true" : "false",
      THEME_UK      = contains(local.theme_arr, "UK") ? "true" : "false",
      THEME_LBV     = contains(local.theme_arr, "LBV") ? "true" : "false",
      THEME_ALPHA   = contains(local.theme_arr, "ALPHA") ? "true" : "false",

      DEPLOY_DATABASE_CONTAINER = var.rds_count > 0 ? "false" : "true",
      POST_STARTUP_SQL          = replace(var.post_startup_sql, "\n", " "),

      REGISTRY_URL               = var.ecr_directory == null ? "${var.ecr_repo}/${lower(var.environment)}" : "${var.ecr_repo}/${var.ecr_directory}",
      NEXUS_RESOURCES_REPOSITORY = "https://nexus.aerius.nl/repository/resources",

      DATABASE_HOSTNAME = var.rds_count > 0 ? (can(data.aws_ssm_parameter.rds_endpoint[0]) ? nonsensitive(data.aws_ssm_parameter.rds_endpoint[0].value) : "") : (contains(local.theme_arr, "LBV") ? "database-check.${lower(local.name_prefix)}-int.local" : "database-calculator.${lower(local.name_prefix)}-int.local"),
      DATABASE_NAME     = var.rds_count > 0 ? replace(lower(var.environment), "-", "") : (contains(local.theme_arr, "UK") ? "calculator-uk" : (contains(local.theme_arr, "LBV") ? "check" : "calculator")),
      DATABASE_USERNAME = var.rds_count > 0 ? var.rds_username : "aerius",
      DATABASE_PASSWORD = var.rds_count > 0 ? (can(var.ssm_passwords["rds"]) ? jsonencode(nonsensitive(var.ssm_passwords["rds"])) : "") : "aerius",

      RABBITMQ_USERNAME = var.rabbitmq_username,
      RABBITMQ_PASSWORD = jsonencode(nonsensitive(var.ssm_passwords["rabbitmq"])),

      MAILHOST_URL        = var.mailhost_url,
      MAILHOST_USERNAME   = jsonencode(nonsensitive(data.aws_ssm_parameter.username_mail.value)),
      MAILHOST_PASSWORD   = jsonencode(nonsensitive(data.aws_ssm_parameter.password_mail.value)),
      MAILHOST_SSL_ENABLE = var.mailhost_ssl,

      NOTIFY_APIKEY                     = var.notify_apikey,
      NOTIFY_TEMPLATEID_CONNECT_API_KEY = var.notify_templateid_connect_api_key,
      NOTIFY_TEMPLATEID_ENGINE_INPUT    = var.notify_templateid_engine_input,
      NOTIFY_TEMPLATEID_ERROR           = var.notify_templateid_error,
      NOTIFY_TEMPLATEID_GML_DOWNLOAD    = var.notify_templateid_gml_download,
      NOTIFY_TEMPLATEID_PDF_DOWNLOAD    = var.notify_templateid_pdf_download,

      GEOSERVER_CONSOLE_DISABLED = var.geoserver_console_disabled,
      GEOSERVER_CALCULATOR_URL   = "https://${local.app_domain_calculator}",
      GEOSERVER_OPEN_DATA_URL    = "https://${local.app_domain_opendata}",

      SEARCH_URL                 = var.service["type"] == "DEV" ? "https://search-dev.aerius.nl/api/" : "https://search.aerius.nl/api/",
      MANUAL_BASE_URL            = contains(local.theme_arr, "UK") && var.service["type"] == "DEV" ? "https://docs-dev.aerius.uk" : "",
      MANUAL_BASE_URL_CALCULATOR = contains(local.theme_arr, "NL") && var.service["type"] == "DEV" ? "https://docs-dev.aerius.nl" : "",
      MANUAL_BASE_URL_LBV_POLICY = contains(local.theme_arr, "LBV") && var.service["type"] == "DEV" ? "https://docs-dev.aerius.nl" : "",

      ARCHIVE_API_HOST = var.archive_api_host,
      ARCHIVE_API_KEY  = var.archive_api_key,
      REGISTER_HOST    = var.register_host,

      FILE_STORAGE_S3_BUCKETNAME         = var.file_storage_s3_bucketname,
      FILE_STORAGE_AWS_REGION            = var.file_storage_aws_region,
      FILE_STORAGE_AWS_ACCESS_KEY_ID     = var.file_storage_aws_access_key_id,
      FILE_STORAGE_AWS_SECRET_ACCESS_KEY = var.file_storage_aws_secret_access_key,

      # First host in the list is the leading domain
      FILESERVER_EXTERNAL_URL            = "https://${local.app_domain_connect}/",

      SERVICE_DISCOVERY_PRIVATE_DNS = "${lower(local.name_prefix)}-int.local",

      # Product queue config
      QUEUE_CONFIGS  = local.queue_config_per_queue,

      # worker config
      SCALE_BASED_ON_CRON = var.scale_based_on_cron,

      OPS_MODEL_PRELOAD_VERSIONS  = var.service["type"] == "PRERELEASE" || var.service["type"] == "DEV" ? "latest" : "stable",
      SRM_MODEL_PRELOAD_VERSIONS  = var.service["type"] == "PRERELEASE" || var.service["type"] == "DEV" ? "latest" : "stable",
      ADMS_MODEL_PRELOAD_VERSIONS = var.service["type"] == "PRERELEASE" || var.service["type"] == "DEV" ? "nca_latest" : "nca_latest",

      ADMS_LICENSE_BASE64 = can(data.aws_ssm_parameter.license_adms[0]) ? nonsensitive(data.aws_ssm_parameter.license_adms[0].value) : "",

      AERIUS_OTEL_COL_NAMESPACE                    = lower(var.environment),
      AERIUS_OTEL_COL_EXPORTER_METRICS_LOGGROUP    = var.otel_loggroup,
      AERIUS_OTEL_COL_EXPORTER_METRICS_LOGSTREAM   = lower(var.environment),
      AERIUS_OTEL_COL_EXPORTER_DEBUG_LOGLEVEL      = var.otel_exporter_debug_loglevel,

      CHUNKER_PROCESSES = var.chunker_processes,

      WORKER_JOBCLEANUP_AFTER_DAYS = var.jobcleanup_after_days,

      # OTA Connect API keys - will only be used if no RDS is used
      CONNECT_API_KEY_QA       = local.connect_api_key_qa,
      CONNECT_API_KEY_REGISTER = local.connect_api_key_register
  })).services
}

output "flags" {
  description = "The dynamic flags to be exposed"
  value       = [
    {name = "CONNECT_API", key = "CONNECT_API_HOST",          value = local.app_domain_connect},
    {name = "CONNECT_API", key = "CONNECT_API_KEY_QA",        value = local.connect_api_key_qa},
    {name = "CONNECT_API", key = "CONNECT_API_KEY_REGISTER",  value = local.connect_api_key_register},
  ]
}
