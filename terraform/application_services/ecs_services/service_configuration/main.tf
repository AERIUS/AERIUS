locals {
  name_prefix                 = var.environment
  name_ssm_prefix             = "/${lower(var.environment)}"

  taskmanager_queue_config_key = (
    contains(local.theme_arr, "ALPHA") ?
    "alpha" :

    contains(local.theme_arr, "LBV") ?
    "lbv" :

    ""
  )

  queue_configs = ["ADMS", "ASRM", "CONNECT", "IMPORTER", "MESSAGE", "OPS", "PDF"]

  app_domain_calculator = split(",", var.application_host_headers["CALCULATOR"])[0]
  app_domain_connect    = split(",", var.application_host_headers["CONNECT"])[0]
  app_domain_opendata   = split(",", var.application_host_headers["OPENDATA"])[0]

  connect_api_key_qa       = coalesce(var.ota_connect_api_key_qa, replace(random_uuid.random_api_key_qa.result, "-", ""))
  connect_api_key_register = coalesce(var.ota_connect_api_key_register, replace(random_uuid.random_api_key_register.result, "-", ""))
}
