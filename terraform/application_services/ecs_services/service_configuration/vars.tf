####################
# MODULE VARIABLES #
####################

#--------------------
# Standard Variables
#--------------------

variable "account_id" {
  type        = string
  description = "The AWS account ID in which to provision the infrastructure"
}

variable "service" {
  type        = map(string)
  description = "A map of service details, e.g.: name, type, theme etc."
}

variable "environment" {
  type        = string
  description = "A string for the Environment used in the terragrunt repository directory structure. E.g development, test, apps_shared_service. etc"
}

#==========================
# ECS Service Variables
#==========================

variable "app_version" {
  type        = string
  description = "The version of the application being deployed"
}

variable "app_timezone" {
  type    = string
  description = "Timezone configuration for the application"
}

variable "ecr_repo" {
  type        = string
  description = "Name of the ECR repository hosting the images"
}

variable "ecr_directory" {
  type        = string
  description = "Name of the directory hosting the images in the ECR repository"
  default     = null
}

variable "asg_name_suffix" {
  type        = string
  description = "A string containing the suffix name for the ASG to support ECS cluster"
}

variable "jobcleanup_after_days" {
  type        = string
  description = "Retention days for the jobcleanup service"
}

variable "ssm_passwords" {
  type        = map(string)
  sensitive   = true
  description = "The generated SSM passwords"
}

variable "mailhost_url" {
  type        = string
  description = "URL for the mailhost"
}

variable "mailhost_ssl" {
  type        = string
  description = "SSL support for mailhost"
}

variable "notify_apikey" {
  type        = string
  description = "Notify Api Key"
  default     = ""
}

variable "notify_templateid_connect_api_key" {
  type        = string
  description = "Notify template id for template requesting a connect api key"
  default     = "d4a25780-210e-437d-818b-b9308e3818ae"
}

variable "notify_templateid_engine_input" {
  type        = string
  description = "Notify template id for model engine input files download"
  default     = "f6f8b9fe-91e8-49da-bced-7fca5758d854"
}

variable "notify_templateid_error" {
  type        = string
  description = "Notify template id for template receiving an error"
  default     = "abd6a624-9788-4cf7-a3dc-6cb7caa5fab8"
}

variable "notify_templateid_gml_download" {
  type        = string
  description = "Notify template id for gml download"
  default     = "f6f8b9fe-91e8-49da-bced-7fca5758d854"
}

variable "notify_templateid_pdf_download" {
  type        = string
  description = "Notify template id for pdf download"
  default     = "f6f8b9fe-91e8-49da-bced-7fca5758d854"
}

variable "rds_username" {
  type        = string
  description = "RDS Cluster Database Username"
}

## RabbitMQ Details
variable "rabbitmq_username" {
  type        = string
  description = "RabbitMQ Username"
}

variable "file_storage_s3_bucketname" {
  type        = string
  description = "File storage S3 Bucket name"
  default     = ""
}

variable "file_storage_aws_region" {
  type        = string
  description = "File storage AWS region"
  default     = ""
}

variable "file_storage_aws_access_key_id" {
  type        = string
  description = "File storage AWS access key ID"
  default     = ""
}

variable "file_storage_aws_secret_access_key" {
  type        = string
  description = "File storage AWS secret access key"
  default     = ""
}

# GeoServer Details
variable "geoserver_console_disabled" {
  type        = string
  description = "GeoServer Disable Console"
}

variable "application_host_headers" {
  type        = map
  description = "Map containing host headers for specific applications"
}

# Worker details
variable "archive_api_host" {
  type        = string
  description = "Archive service API host"
  default     = ""
}

variable "archive_api_key" {
  type        = string
  description = "Archive service API key"
  default     = ""
}

variable "register_host" {
  type        = string
  description = "Hostname for the Register UI"
  default     = ""
}

# Schedule vars
variable "scale_down_cron" {}
variable "scale_up_cron"   {}

# Scaling based on cron
variable "scale_based_on_cron" {
  type        = bool
  description = "Enable or disable scaling based on cron."
}

variable "rds_count" {
  type        = number
  description = "RDS Cluster Instances Amount"
}

variable "chunker_processes" {
  type        = string
  description = "Number of chunker processes per service"
  default     = ""
}

variable "post_startup_sql" {
  type        = string
  description = "SQL to execute on the internal DB container if it is deployed"
  default     = ""
}

# OTEL details
variable "otel_exporter_debug_loglevel" {
  type        = string
  description = "Loglevel for OTEL exporter debug"
  default     = "basic"
}

variable "otel_loggroup" {
  type        = string
  description = "Loggroup for OTEL metrics"
  default     = ""
}

# Used for the DEV environment for various reasons, like inserting temporary API keys and such
variable "ota_connect_api_key_qa" {
  type        = string
  description = "Predefined API key to use when setting up environment for QA - if not set generates a random one"
  default     = ""
}

variable "ota_connect_api_key_register" {
  type        = string
  description = "Predefined API key to use when setting up environment for Register - if not set generates a random one"
  default     = ""
}
