locals {
  # Repository defaults
  basicinfra_version                       = "v1"
  ecr_repo                                 = "028339422996.dkr.ecr.eu-west-1.amazonaws.com"

  # ECS Services defaults
  scale_based_on_cron                      = false

  jobcleanup_after_days                    = "3"
  mailhost_url                             = "email-smtp.eu-west-1.amazonaws.com"
  mailhost_ssl                             = "true"

  rabbitmq_username                        = "stikstofje"

  geoserver_console_disabled               = "true"

  scale_down_cron                          = "cron(0 17 ? * MON-FRI *)"
  scale_up_cron                            = "cron(0 6 ? * MON-FRI *)"

  ecs_cluster_cap_provider_suffix_spot     = "FESCAP002"

  # ALB defaults
  target_groups = {
    "tg1" = {name = "api",  protocol = "HTTP", port = "8080", path = "/api/actuator/health",             matcher = "200-399"},
    "tg2" = {name = "geoc", protocol = "HTTP", port = "8080", path = "/geoserver-calculator/index.html", matcher = "200-399"},
    "tg3" = {name = "odat", protocol = "HTTP", port = "8080", path = "/opendata/index.html",             matcher = "200-399"},
    "tg9" = {name = "cal",  protocol = "HTTP", port = "8080", path = "/system-info",                     matcher = "200-399"},
  }

  listener_rules = {
    "rule1"        = {tg = "tg1", application_type = "CONNECT",    path_pattern = "/api/*",                  cognito = false},

    "rule2"        = {tg = "tg3", application_type = "OPENDATA",   path_pattern = "/opendata/*",             cognito = true},

    "rule3"        = {tg = "tg1", application_type = "CHECK",      path_pattern = "/api/*",                  cognito = false},
    "rule4"        = {tg = "tg2", application_type = "CHECK",      path_pattern = "/geoserver-calculator/*", cognito = true},
    "fallback_chk" = {tg = "tg9", application_type = "CHECK",      path_pattern = "/*",                      cognito = true},

    "rule6"        = {tg = "tg1", application_type = "CALCULATOR", path_pattern = "/api/*",                  cognito = false},
    "rule7"        = {tg = "tg2", application_type = "CALCULATOR", path_pattern = "/geoserver-calculator/*", cognito = true},
    "rule8"        = {tg = "tg9", application_type = "CALCULATOR", path_pattern = "/system-info",            cognito = false},
    "fallback_cal" = {tg = "tg9", application_type = "CALCULATOR", path_pattern = "/*",                      cognito = true},
  }

  # ECS cluster defaults
  ecs_ctr_fes_1_max_instance_size     = "100"

  ecs_capacity_providers = {
    "01" = {description = "On_demand_capacity", number = "001", asg = "01"},
    "02" = {description = "Spot_capacity",      number = "002", asg = "02"},
  }

  ecs_autoscaling_group = {
    "01" = {description = "On_demand_capacity", number = "001", spot = false},
    "02" = {description = "Spot_capacity",      number = "002", spot = true},
  }

  efs_shares = {
    "rabbitmq"      = {name = "rabbitmq",     throughput_mode = "provisioned", provisioned_throughput_in_mibps = 32 }
  }

  # RDS defaults
  rds_name                = "BESRDS001"
  rds_username            = "aerius"
  cluster_identifier      = "aurora-cluster-aerius"
  engine                  = "aurora-postgresql"
  engine_version          = "15.4"
  rds_count               = 1
  database_name           = "aerius"
  master_username         = "aerius"
  backup_retention_period = 5
  preferred_backup_window = "01:00-03:00"
  instance_class          = "db.r5.2xlarge"

  # Password defaults
  ssm_passwords = {
    "rds"        = {name = "rds",      description = "RDS Master Password"},
    "rabbitmq"   = {name = "rabbitmq", description = "Rabbitmq node Password"},
  }
}
