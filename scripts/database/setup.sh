psql -U postgres postgres <<OMG
  drop role aerius;
  create user aerius with encrypted password 'aerius';
  alter role aerius superuser;
  alter user aerius createdb;
OMG
