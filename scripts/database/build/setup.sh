#!/usr/bin/env sh
set -e

gem install net-ssh -v 4.2.0
gem install net-sftp -v 2.1.2
gem install clbustos-rtf
