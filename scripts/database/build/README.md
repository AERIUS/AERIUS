# Manual (calculator) db building

Note the scripts in this directory are both the documentation of what to do as well as usable scripts to manage your environment.

## Docker
Requirements: Docker (duh!).
Assumes `SFTP_READONLY_PASSWORD` environment variable is set. Can also be supplied using `SFTP_READONLY_PASSWORD=... <command>`.

To build the latest database, run `docker-build.sh`.

To run the latest database, run `docker-run.sh`.

## Non-Docker
To install ruby gems, run `setup.sh`

To configure database user settings, run `settings-configure.sh` and input things like the db-data dir.

Optional: to (later) edit the user settings, run `settings-edit.sh`.

To sync remote files, run `nl-sync.sh` or `uk-sync.sh`.

To build the database, run `nl-build.sh` or `uk-build.sh`. The database will be named `calculator` or `calculator-uk`.

To generate new derived tables with nature information, run `nl-generate.sh` or `uk-generate.sh`. The database will be named `calculator` or `calculator-uk`.

To check the database, run `nl-check.sh` or `uk-check.sh`.

