#!/usr/bin/env sh
set -e

# $1 = name of the ruby script in src/build/versions directory

. ./setenv.sh

cd ${DATABASE_SOURCE}

ruby ${AERIUS_DATABASE_BUILD_DIR}/bin/SyncDBData.rb src/build/versions/$1_$DATABASE_PROFILE.rb
