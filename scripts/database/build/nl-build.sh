#!/usr/bin/env sh
set -e

./db-build.sh "calculator" "calculator_nl"
# Alternatively: mvn package -p 'databases,build,database-latest,RIVM'
