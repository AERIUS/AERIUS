#!/usr/bin/env sh
set -e

SCRIPT_PATH=$(readlink -f "${0}")
SCRIPT_DIR=$(dirname "${SCRIPT_PATH}")

: ${AERIUS_DATABASE_BUILD_DIR:="${SCRIPT_DIR}/../../../aerius-database-build"}

if [ -f "${AERIUS_DATABASE_BUILD_DIR}/VERSION" ]; then
  echo Running with aerius-database-build version `cat "${AERIUS_DATABASE_BUILD_DIR}/VERSION"`
else
  echo Could not find aerius-database-build project in "${AERIUS_DATABASE_BUILD_DIR}}"
  echo Either checkout or set environment variable AERIUS_DATABASE_BUILD_DIR to point to the aerius-database-build project.
  exit 1;
fi

: ${DATABASE_PROFILE:="latest"}

DATABASE_SOURCE=$(dirname $(dirname $(dirname "${SCRIPT_DIR}")))/source/database
CONFIG_FILE="${DATABASE_SOURCE}/src/build/config/AeriusSettings.User.rb"
