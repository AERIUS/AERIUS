#!/usr/bin/env sh
set -e

. ./setenv.sh

if [ -e $CONFIG_FILE ]; then
  echo Configuration file already exist in $CONFIG_FILE.
else
  echo cp "${SCRIPT_DIR}/AeriusSettings.User.rb.template" $CONFIG_FILE
fi

sh ${SCRIPT_DIR}/settings-edit.sh
