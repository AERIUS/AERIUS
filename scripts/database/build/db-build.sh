#!/usr/bin/env sh
set -e

# $1 = name of the database
# $2 = name of the ruby script in src/build/versions directory

. ./setenv.sh

cd ${DATABASE_SOURCE}

ruby ${AERIUS_DATABASE_BUILD_DIR}/bin/Build.rb default.rb ${DATABASE_SOURCE}/src/build/versions/$2_$DATABASE_PROFILE.rb --flags QUICK -v $1 --database-name $1-builder &&
  cd ${SCRIPT_DIR} && ${SCRIPT_DIR}/db-rename.sh $1
