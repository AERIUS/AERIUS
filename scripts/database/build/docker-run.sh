#!/usr/bin/env bash

# Exit on error
set -e

# Run database container
# --rm   = remove container after it's stopped/killed
# -d     = run in the background
# --name = name of the container
# -p ... = allow access via port 5432 - for various tools/webapps etc.
# -v ... = allow access via Socket file (default for psql/pg_dump etc)
docker run \
  --rm \
  -d \
  --name database-calculator \
  -p 5432:5432 \
  -v /var/run/postgresql:/var/run/postgresql \
  database-calculator:latest
