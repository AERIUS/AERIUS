#!/usr/bin/env sh
set -e

# $1 = name of the database
. ./setenv.sh

pg_username=$(grep "\$pg_username" $CONFIG_FILE | sed "s/\$pg_username\s*=\s*'\([^']*\)'/\1/")
if [ -z "$pg_username" ]; then
    pg_username="postgres"
fi

psql -U $pg_username -c "DROP DATABASE \"$1\";" postgres || true
psql -U $pg_username -c "ALTER DATABASE \"$1-builder\" RENAME TO \"$1\";" postgres
