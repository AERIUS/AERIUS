#!/usr/bin/env sh
set -e

./db-build.sh "calculator-uk" "calculator_uk"
# Alternatively: mvn package -p 'databases,build,database-latest,JNCC'
