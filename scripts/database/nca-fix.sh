api_key=$(cat api_key.txt)

psql -U aerius calculator-uk <<OMG

  UPDATE system.bing_layer_properties SET api_key = '$api_key' WHERE api_key like 'BING_API_KEY';
  SELECT layer_properties_id,title,bundle_id,api_key FROM system.bing_layer_properties;

OMG
