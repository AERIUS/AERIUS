#!/bin/bash

# Define the database connection details
DATABASE="calculator"
USER="aerius"
PASSWORD="aerius"
HOST="localhost"
TABLE="users.users"

# Connect to the database, run the query, and print the output
PGPASSWORD=$PASSWORD psql -q -h $HOST -U $USER -d $DATABASE <<OMG
SELECT * FROM $TABLE;
OMG
