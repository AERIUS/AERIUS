#!/bin/sh

# This script attempts to free a specified port by:
# 1. Logging the current state of the port.
# 2. Killing processes using the specified port.
# 3. Killing stale connections in CLOSE_WAIT and FIN_WAIT2 states.

if [ $# -eq 0 ]; then
    echo "Usage: $0 <port>"
    exit 1
fi

PORT=$1
CHECK_INTERVAL=5

# Function to log the current state of the port
log_port_state() {
    echo "$(date '+%Y-%m-%d %H:%M:%S') - Checking port $PORT"
    echo "Processes using port $PORT:"
    lsof -i:$PORT
    echo "Stale sockets for port $PORT:"
    ss -antp | grep ":$PORT"
    echo "Note it may take about a minute for a TIME_WAIT state to resolve"
}

# Function to kill processes using the port
kill_processes() {
    for pid in $(lsof -t -i:$PORT); do
        process_name=$(ps -p $pid -o comm=)
        echo "Killing process $pid ($process_name) using port $PORT"
        kill -9 $pid
    done
}

# Function to kill stale connections
kill_stale_connections() {
    echo "Killing stale connections for port $PORT"

    # Get the PIDs of the processes holding the connections in CLOSE_WAIT state
    pids=$(ss -antp | grep ":$PORT" | grep 'CLOSE_WAIT' | awk '{print $6}' | sed 's/.*pid=\([0-9]*\),.*/\1/')

    for pid in $pids; do
        process_name=$(ps -p $pid -o comm=)
        echo "Killing process $pid ($process_name) holding a stale connection"
        kill -9 $pid
    done

    # Get the socket connections in FIN_WAIT2 state and kill them
    fin_wait_sockets=$(ss -antp | grep ":$PORT" | grep 'FIN_WAIT2' | awk '{print $4,$5}' | sed 's/::ffff://g')

    for conn in $fin_wait_sockets; do
        src=$(echo $conn | awk '{print $1}')
        dst=$(echo $conn | awk '{print $2}')
        echo "Killing connection from $src to $dst"
        ss -K src "$src" dst "$dst"
    done
}

# Main loop to free the port
while true; do
    log_port_state
    kill_processes
    kill_stale_connections

    # Check if the port is free
    if ! lsof -i:$PORT && ! ss -antp | grep -q ":$PORT"; then
        echo "Port $PORT is now free."
        break
    else
        echo "Port $PORT is still in use or in TIME_WAIT. Retrying..."
        sleep $CHECK_INTERVAL
    fi
done

echo "Port freeing completed."
