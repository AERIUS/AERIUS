#!/usr/bin/env sh
set -e

. ./setenv.sh

cd "${SCRIPT_DIR}"

# Begin by killing the port
sh kill-socket-connections.sh 9876

# Optional: include search client sources in codeserver recompiler
# Create 'search-repo-dir.txt' and put the relative url to the
# search repository in there (relative to `source/`) in order
# to have the codeserver recompile those sources.
if [ -f search-repo-dir.txt ]; then
  searchDir=$(readlink -f "$(< search-repo-dir.txt)")
fi

cd "${AERIUS_PROJECT_DIR}"

LAUNCHER_DIR="${AERIUS_PROJECT_DIR}/source/target/gwt/launcherDir"
if [ -d "${LAUNCHER_DIR}" ]; then
  rm -r "${AERIUS_PROJECT_DIR}/source/target/gwt/launcherDir"
fi

if [ "$1" = "build" ]; then
  mvn clean install \
   -DskipTests \
   -pl :aerius-calculator-wui-server \
   -am \
   -Dgwt.skipCompilation=true \
   -Pdeploy,${AERIUS_CUSTOMER}
fi

sh scripts/quickfix-templates.sh

cd source

echo "Starting code server in: $(pwd)"

# Run Maven in the background and store its PID
mvn gwt:codeserver \
  -pl :aerius-calculator-wui-client \
  -am \
  -B \
  -Penv-dev \
  -Denv=dev \
  -Dgwt.style=DETAILED \
  -Dmaven.buildNumber.skip=true \
  -Dgwt.modules=nl.overheid.aerius.AeriusDebug \
  -Dgwt.additional.sources="${searchDir:-/tmp/this-does-not-exist/}"
# -Dgwt.draftCompile=true -> geen source maps.
