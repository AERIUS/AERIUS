#!/usr/bin/env sh
set -e

# Geoserver is always built and run as war file

. ./setenv.sh

cd "${AERIUS_PROJECT_DIR}/source/geoserver"

mvn clean install -am -pl :aerius-geoserver-calculator -P`echo ${AERIUS_CUSTOMER} | tr '[:lower:]' '[:upper:]'` -Penv-dev

# Kill any existing geoserver
if fuser -k ${CALCULATOR_GEOSERVER_PORT}/tcp; then
  echo "Existing geoserver process terminated."
else
  echo "No existing geoserver process found on port ${CALCULATOR_GEOSERVER_PORT}."
fi

TARGET="${AERIUS_PROJECT_DIR}/source/geoserver/aerius-geoserver-calculator/target/"

cd "${TARGET}"

java \
  -DTOMCAT_STANDALONE_CONTEXT_PATH=/geoserver-calculator \
  -DTOMCAT_STANDALONE_PORT=${CALCULATOR_GEOSERVER_PORT} \
  -DCALCULATOR_DB_HOST=localhost \
  -DCALCULATOR_DB_NAME=${AERIUS_DB_NAME} \
  -DCALCULATOR_DB_USER=aerius \
  -DCALCULATOR_DB_PASS=aerius \
  -DGEOSERVER_CONSOLE_DISABLED=false \
  -jar "${TARGET}geoserver-calculator.war"
