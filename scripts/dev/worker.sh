#!/usr/bin/env sh
set -e

. ./setenv.sh

cd "${AERIUS_PROJECT_DIR}"

# Create specific worker settings by passing a string on the commandline, for example: model, or chunker
WORKER_TYPE=$1

# Copy the template worker config to here for usage with the worker
CONFIG_FILE=${SCRIPT_DIR}/config/worker-${WORKER_TYPE}.properties;
if [ ! -e "${CONFIG_FILE}" ]; then
  cp source/calculator/aerius-worker/src/main/config/worker.template.properties "${CONFIG_FILE}"
  echo Created configuration file "${CONFIG_FILE}"
  echo Modify this file with you own configuration settings
fi

mvn exec:java \
  -pl :aerius-worker \
  -Dexec.mainClass="nl.overheid.aerius.worker.main.Main" \
  -Dexec.args="-config ${CONFIG_FILE}"
