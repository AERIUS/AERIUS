#!/usr/bin/env sh
# Run this script from the directory this script is in:
# source setenv.sh

set -e

SCRIPT_PATH=$(readlink -f "${0}")
SCRIPT_DIR=$(dirname "${SCRIPT_PATH}")
CONFIG_FILE_DIR="${SCRIPT_DIR}/config"
if [ ! -e "${CONFIG_FILE_DIR}" ]; then
  mkdir "${CONFIG_FILE_DIR}"
fi

: ${AERIUS_PROJECT_DIR:="${SCRIPT_DIR}/../.."}

cd ${AERIUS_PROJECT_DIR}
AERIUS_VERSION=$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout)
echo AERIUS POM version: ${AERIUS_VERSION}
cd -

# Write env settings if not present
AERIUS_ENV_SETTINGS="${CONFIG_FILE_DIR}/env.sh"

if [ ! -f "${AERIUS_ENV_SETTINGS}" ]; then
  cat << EOF > "${AERIUS_ENV_SETTINGS}"
#!/usr/bin/env sh
set -e

# NL
export AERIUS_DB_NAME=calculator
export AERIUS_CUSTOMER=rivm

# UK
#export AERIUS_DB_NAME=calculator-uk
#export AERIUS_CUSTOMER=jncc
EOF
fi

. ${AERIUS_ENV_SETTINGS}

# Ports
: ${CONNECT_PORT:=8081}
: ${CALCULATOR_GEOSERVER_PORT:=8082}
: ${CODESERVER_PORT:=8085}
: ${CONNECT_DEBUG_PORT:=8181}

# Print current configuration
echo "
Current Configuration:
---------------------
Database Name: ${AERIUS_DB_NAME}
Customer: ${AERIUS_CUSTOMER}
Connect Port: ${CONNECT_PORT}
Connect Debug Port: ${CONNECT_DEBUG_PORT}
GeoServer Port: ${CALCULATOR_GEOSERVER_PORT}
CodeServer Port: ${CODESERVER_PORT}

To change these settings:
1. Edit ${AERIUS_ENV_SETTINGS}
2. Re-run: source setenv.sh

Note: The env.sh file is created in the config directory if it doesn't exist.
      It contains the main configuration for database and customer settings.
"
