#!/usr/bin/env sh
set -e

. ./setenv.sh

if [ "$1" = "connect" ]; then
  project="connect-service"
else
  project="calculator-wui-client"
fi

cd "${AERIUS_PROJECT_DIR}/source/calculator/aerius-${project}-test/src/test/e2e/"

echo Running Cypress from $PWD
echo $CYPRESS_LOCALE

if [ "$2" = "install" ]; then
  npm install
elif [ "$2" = "update" ]; then
  npm update
elif [ "$2" = "report" ]; then
  node ./cypress/reports/cucumber-html-report.js
else
  AERIUS_CYPRESS_SETTINGS="${CONFIG_FILE_DIR}/cypress-env.sh"

  if [ ! -f "${AERIUS_CYPRESS_SETTINGS}" ]; then
  cat << EOF > "${AERIUS_CYPRESS_SETTINGS}"
#!/usr/bin/env sh
set -e

# Cypress settings
# For Calculator Cypress testing see for complete list of options:
#  source/calculator/aerius-calculator-wui-client-test/README.md
# For Connect Cypress testing see for complete list of options:
#  source/calculator/aerius-connect-service-test/README.md

export CYPRESS_BASE_URL=http://localhost:8080
# NL
export CYPRESS_LOCALE=nl
export CYPRESS_ENV=
# Run with username and password, needed to running agains external test server.
#export CYPRESS_ENV="--env USERNAME=<username>,PASSWORD=<password>"

# UK
#export CYPRESS_LOCALE=en
#export CYPRESS_ENV="--env TEST_PROFILE=UK,SKIP_COOKIES_CHECK=true"
EOF
  fi

  . ${AERIUS_CYPRESS_SETTINGS}

  npx cypress open --config baseUrl=${CYPRESS_BASE_URL}?locale=${CYPRESS_LOCALE} ${CYPRESS_ENV}
fi
