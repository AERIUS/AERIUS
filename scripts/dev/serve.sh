#!/usr/bin/env sh
set -e

# Check if the necessary tools are installed
command -v tmux >/dev/null 2>&1 || { echo >&2 "I require tmux but it's not installed.  Aborting."; exit 1; }
command -v lsof >/dev/null 2>&1 || { echo >&2 "I require lsof but it's not installed.  Aborting."; exit 1; }

SESSION=$USER

# When always triggering a build before starting a specific application
# pass 'build' parameter to this script.
: ${AERIUS_BUILD:=$1}

. ./setenv.sh

cd "${SCRIPT_DIR}"
echo $PWD

tmux kill-session -t calculator && { echo "Waiting for existing session to close"; sleep 2; }

tmux new-session -d -s calculator './gateway.sh'

# Let panes remain on exit, so that any crashes remain visible
tmux set-option remain-on-exit

tmux setenv AERIUS_DB_NAME "${AERIUS_DB_NAME:-}"
tmux setenv AERIUS_CUSTOMER "${AERIUS_CUSTOMER:-}"

tmux select-pane -T "Gateway"

tmux split-window -v './geoserver.sh ${AERIUS_BUILD}'
tmux select-pane -T "Geoserver"

tmux split-window -v './fake-email.sh'
tmux select-pane -T "Mail"

tmux split-window -h './connect.sh'
tmux select-pane -T "Connect"

tmux split-window -h './worker.sh model'
tmux select-pane -T "Model Worker"

# Select tiled layout at this halfway point because if the next pane doesn't have enough space then it won't appear and crash the script
tmux select-layout tiled

tmux split-window -h './jetty.sh'
tmux select-pane -T "Jetty"

tmux split-window -v './worker.sh chunker'
tmux select-pane -T "Chunker"

tmux setenv TASKMANAGER_DIR "${TASKMANAGER_DIR:-}"
tmux split-window -h './taskmanager.sh'
tmux select-pane -T  "Taskmanger"

# Select tiled layout at this halfway point because if the next pane doesn't have enough space then it won't appear and crash the script
tmux select-layout tiled

tmux split-window -v './codeserver.sh ${AERIUS_BUILD}'
tmux select-pane -T "Codeserver"

tmux select-layout tiled

tmux split-window -v './vue-processing.sh'
tmux select-pane -T "Component Annotation Processors"

tmux split-window -h './easter.sh'
tmux select-pane -T "Easter"

tmux select-layout tiled

tmux select-pane -t 8
tmux -2 attach-session -d
