#!/usr/bin/env sh
set -e

. ./setenv.sh

cd "${AERIUS_PROJECT_DIR}/source/calculator/aerius-calculator-development-gateway"

mvn spring-boot:run
