#!/usr/bin/env bash
set -e

. ./setenv.sh

cd "${AERIUS_PROJECT_DIR}"

if [ "$1" = "build" ]; then
  mvn clean install -pl :aerius-connect-service -DskipTests
fi

if fuser -k ${CONNECT_PORT}/tcp; then
  echo "Existing connect process terminated."
else
  echo "No existing connect process found on port ${CONNECT_PORT}."
fi

mvn spring-boot:run -pl :aerius-connect-service \
  -Dspring-boot.run.jvmArguments="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=${CONNECT_DEBUG_PORT}" \
  -Dspring-boot.run.arguments="--server.port=${CONNECT_PORT}"
