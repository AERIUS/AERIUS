#!/usr/bin/env sh

. ./setenv.sh

: ${TASKMANAGER_DIR:=${AERIUS_PROJECT_DIR}/../taskmanager}
TASKMANAGER_SOURCE_DIR="${TASKMANAGER_DIR}/source/taskmanager"

if [ ! -d "${TASKMANAGER_SOURCE_DIR}" ]; then
  echo Could not find taskmanager repository in ${TASKMANAGER_DIR}
  echo Set environment variable 'TASKMANAGER_DIR' to point to the taskmanager repository directory.
  exit
fi
# Create Taskmanager configuration if not exists

QUEUE_CONFIG="${CONFIG_FILE_DIR}/queue_config"
TASKMANAGER_PROPERTIES="${CONFIG_FILE_DIR}/taskmanager.properties"

if [ ! -d "${QUEUE_CONFIG}" ]; then
  mkdir "${QUEUE_CONFIG}"
  cp ${AERIUS_PROJECT_DIR}/docker/common/taskmanager/queue_config/* "${QUEUE_CONFIG}"
fi
if [ ! -f "${TASKMANAGER_PROPERTIES}" ]; then
  cat << EOF > "${TASKMANAGER_PROPERTIES}"
# RabbitMQ configuration.
broker.host = localhost
broker.username = aerius
broker.password = aerius

# Taskmanager configuration directory. Directory should contain the queue configurations.
# See the queue directory for an example.
taskmanager.configuration.directory = ${QUEUE_CONFIG}
EOF
fi

cd "${TASKMANAGER_SOURCE_DIR}"

mvn exec:java \
  -Dexec.mainClass="nl.aerius.taskmanager.Main" \
  -Dexec.args="-config ${TASKMANAGER_PROPERTIES}" \
  -Dexec.classpathScope=runtime
