#!/bin/bash

# Go to base dir for monitoring
cd "${1:-.}"

# Directory to monitor (current directory and subdirectories)
MONITOR_DIR="src/main/java"

# Debounce delay in seconds
DEBOUNCE_DELAY=2

# Associative array to store last processed times for each file
declare -A LAST_PROCESSED

# Function to process Java files when changes are detected
process_java_file() {
  local java_file=$1

  # Check if the file contains "@Component"
  if grep -q "@Component" "$java_file"; then
    #echo "Java file change detected and contains '@Component': $java_file"
    ./process.sh "$java_file"
  #else
    #echo "'@Component' not found in $java_file. Skipping..."
  fi
}

# Function to handle file change events
handle_file_change() {
  local file=$1

  case "$file" in
    *.java)
      process_java_file "$file"
    ;;
    *.html)
      local java_file="${file%.html}.java"
      if [ -f "$java_file" ]; then
        process_java_file "$java_file"
      else
        echo "Corresponding Java file $java_file not found for changed HTML file $file."
      fi
    ;;
    *)
    #echo "Change detected in non-target file $file. Ignoring..."
  ;;
  esac
}

# Main monitoring loop
main_loop() {
  inotifywait -m -r -e modify -e move -e create -e delete --format '%w%f' $MONITOR_DIR | while read file; do
    local current_time=$(date +%s)
    local last_processed_time=${LAST_PROCESSED[$file]:-0}

    # Check if sufficient time has elapsed since the last processing
    if (( current_time - last_processed_time > DEBOUNCE_DELAY )); then
      LAST_PROCESSED[$file]=$current_time
      handle_file_change "$file"
    #else
      #echo "Skipping processing for $file due to debounce."
    fi
  done
}

main_loop
