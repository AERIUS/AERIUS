#!/bin/bash

# Get the parent directory of the current project root
PARENT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")/../.." && cd .. && pwd)"
EASTER_DIR="${PARENT_DIR}/easter"

if [ ! -d "${EASTER_DIR}" ]; then
    echo "No easter project found at ${EASTER_DIR}"
    exit 1
fi

cd "${EASTER_DIR}/easter" || {
    echo "Failed to navigate to easter project directory"
    exit 1
}

echo "Building easter project..."
npm run build

echo "Build complete. Contents of dist directory:"
ls -la dist/

echo "Starting server on port 3250..."
npx -y serve dist -l 3250
