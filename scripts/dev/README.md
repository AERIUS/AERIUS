# Development Scripts

This directory contains the scripts to run Calculator.
With the `serve.sh` script all processes can be started with a single command.

If you run for the first time some configuration files are generated in the config directory here.
You need to adapt these configuration files to set correct usernames and/or passwords.
Configuration/scripts generated are:
* `env.sh` theme/build configuration settings
* `worker-chunker.properties` worker properties file
* `worker-model.properties` worker properties file
* `fake-email.properties` to intercept the sending of emails.
* `taskmanager.properties` and `queue_config` directory with taskmanager configuration

To run AERIUS Calculator you need the following applications to run:
* PostgreSQL database. Creation and filling of the database needs to done before starting the applications.
* RabbitMQ
* AERIUS file server

Before starting the serve script it needs at least once to compile everything.
This doesn't need to be a full compile for the wui client.
Therefor you can compile with the following maven parameters from the root of the project:
```
mvn clean install -P deploy -DskipTests -Dgwt.skipCompilation=true
```

# Running a different theme

To run the scripts for a different theme/database you can change the settings in the file `config/env.sh`.
This file is included and initially created when `setenv.sh` is run.
The file `setenv.sh` is included in the bash scripts, but you can also run it once initially to generate the default `config/env.sh` file.

# Running Cypress

With the script `cypress.sh` the Cypress integration tests can be run.
The Cypress tests can be controlled by adapting the variables in `env.sh`.
As default it runs the NL tests against local host.
With these settings it's also possible to run against another server.
Run the script with the following commands:
To run UI tests (if `client` omitted this will be assumed)
```
./cypress.sh client
```

To run Connect tests:
```
./cypress.sh connect
```
It's also possible to run npm commands `update` and `install`:
```
./cypress.sh client install
```
and
```
./cypress.sh client update
```
(Either run update/install with `connect` or `client` argument).

For Calculator Cypress testing see for complete list of options:
[source/calculator/aerius-calculator-wui-client-test/README.md](../../source/calculator/aerius-calculator-wui-client-test/README.md)

For Connect Cypress testing see for complete list of options:
[source/calculator/aerius-connect-service-test/README.md](../../source/calculator/aerius-connect-service-test/README.md)
