#!/usr/bin/env sh
set -e

. ./setenv.sh

echo Local website to read e-mails at: http://localhost:5080/

# Fake-email verison
FAKE_EMAIL_VERSION=1.9.3

# Copy template copy file to config directory for local editing
CONFIG_FILE=${CONFIG_FILE_DIR}/fake-email.properties;
if [ ! -e "${CONFIG_FILE}" ]; then
  cp "${SCRIPT_DIR}/fake-email-template.properties" "${CONFIG_FILE}"
  echo Created fake-mail configuration file "${CONFIG_FILE}"
  echo Modify this file if you need other configuration settings
fi

FAKE_EMAIL_JAR_FILE=fake-smtp-server-${FAKE_EMAIL_VERSION}.jar
FAKE_EMAIL_JAR="${CONFIG_FILE_DIR}/${FAKE_EMAIL_JAR_FILE}"

# Get jar from GitHub and store in config directory if not yet available

if [ ! -e "${FAKE_EMAIL_JAR}" ]; then
  wget -P "${CONFIG_FILE_DIR}" https://github.com/gessnerfl/fake-smtp-server/releases/download/${FAKE_EMAIL_VERSION}/${FAKE_EMAIL_JAR_FILE}
fi

# Needs to run as superuser (or user that is allowed to connect to port)
java -jar "${FAKE_EMAIL_JAR}" -Dspring.config.location="${CONFIG_FILE_DIR}/application.properties"
