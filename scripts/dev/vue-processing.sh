#!/usr/bin/env sh
set -e

. ./setenv.sh

cd "${SCRIPT_DIR}/common"

MONITOR_DIR=${AERIUS_PROJECT_DIR}/source/calculator/aerius-calculator-wui-client

echo "Obtaining and saving classpath"
sh generate-classpath.sh ${MONITOR_DIR}

echo "Begin monitoring ${MONITOR_DIR}"
sh monitor-vue-components.sh ${MONITOR_DIR}
