cd ../..

mvn clean install -am -pl :aerius-calculator-wui-server-standalone -DskipTests

cd source/calculator/aerius-calculator-wui-server-standalone/target

java -DTOMCAT_STANDALONE_PORT=8085 -DCALCULATOR_DB_USER=postgres -DCALCULATOR_DB_PASS=postgres -DCALCULATOR_DB_HOST=localhost:5432 -DCALCULATOR_DB_NAME=calculator -jar aerius-calculator-wui-server-standalone-3.0-SNAPSHOT.war

