#!/bin/sh

# This script is designed to streamline the process of updating i18n keys in properties files, making it easier to manage and modify translations.
#
# - Prompts user for base i18n key.
# - Loads existing translations from properties files.
# - Opens text editor for user to update translations.
# - Saves updated translations back to properties files.
# - Runs a sorting script on i18n files.

# Function to prompt user for input
prompt() {
  local prompt_message=$1
  read -p "$prompt_message" input
  echo $input
}

# Change current directory to directory of script so it can be called from everywhere
SCRIPT_PATH=$(readlink -f "${0}")
SCRIPT_DIR=$(dirname "${SCRIPT_PATH}")
cd "${SCRIPT_DIR}"

# Get the base i18n key from the user
base_key=$(prompt "Enter the base i18n key (e.g., 'unitTime'): ")

# Function to preload existing translations related to a base key from a properties file
preload_translation() {
  local file=$1
  local base_key=$2
  # Use grep to find all lines related to the base key
  grep "^$base_key" "$file"
}

# Function to open text editor for input
get_text_from_editor() {
  local temp_file=$1
  local current_translations=$2  # Current translations to preload

  # Write guidance and existing translations to the temporary file
  {
    echo "${current_translations}"
    echo "# Please enter or modify the translations for '${base_key}' above. Lines starting with '#' will be ignored."
    echo "# Ensure the entries are properly formatted."
  } > "$temp_file"
  
  # Set EDITOR to nano if not already set
  if [ -z "$EDITOR" ]; then
    EDITOR=nano
  fi

  # Open the editor
  $EDITOR "$temp_file"
}

# Function to update properties file with changes from editor
update_properties_file() {
  local file=$1
  local temp_file=$2
  local base_key=$3

  # Create a new temporary file for the updated properties
  local updated_temp_file=$(mktemp /tmp/updated_properties.XXXXXX)

  # Exclude existing entries related to the base key
  grep -v "^$base_key" "$file" > "$updated_temp_file"

  # Add updated entries, excluding comments
  grep -v '^#' "$temp_file" >> "$updated_temp_file"

  # Move the updated file back to the original file path
  mv "$updated_temp_file" "$file"
}

# Prepare the temporary file paths
dutch_temp_file=$(mktemp /tmp/i18n_dutch.XXXXXX)
english_temp_file=$(mktemp /tmp/i18n_english.XXXXXX)

# Define the file paths
dutch_file="../../source/calculator/aerius-calculator-wui-client/src/main/resources/nl/overheid/aerius/wui/application/i18n/ApplicationMessages.properties"
english_file="../../source/calculator/aerius-calculator-wui-client/src/main/resources/nl/overheid/aerius/wui/application/i18n/ApplicationMessages_en.properties"

# Preload existing translations if they exist
dutch_current=$(preload_translation "$dutch_file" $base_key)
english_current=$(preload_translation "$english_file" $base_key)

# Edit Dutch and English strings
get_text_from_editor "$dutch_temp_file" "$dutch_current"
update_properties_file "$dutch_file" "$dutch_temp_file" $base_key

get_text_from_editor "$english_temp_file" "$english_current"
update_properties_file "$english_file" "$english_temp_file" $base_key

# Cleanup temporary files
rm -f "$dutch_temp_file" "$english_temp_file"

echo "i18n keys have been updated in the properties files."

sh sort-i18n.sh
