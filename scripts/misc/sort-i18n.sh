export LC_ALL=C


# Change current directory to directory of script so it can be called from everywhere
SCRIPT_PATH=$(readlink -f "${0}")
SCRIPT_DIR=$(dirname "${SCRIPT_PATH}")
cd "${SCRIPT_DIR}"

cd ../../source/calculator/aerius-calculator-wui-client/src/main/resources/nl/overheid/aerius/wui/application/i18n/

sort ApplicationMessages.properties --output=ApplicationMessages.properties
sort ApplicationMessages_en.properties --output=ApplicationMessages_en.properties
