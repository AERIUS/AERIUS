username=$(cat username.txt)
password=$(cat password.txt)

cd ../../source/calculator/aerius-calculator-wui-client-test/src/test/e2e

baseUrl=${1:-http://localhost:8080?locale=nl}

npx cypress open --config baseUrl=$baseUrl \
  --env USERNAME=$username,PASSWORD=$password
