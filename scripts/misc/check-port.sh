#!/bin/sh

# This script monitors a specified port, logs its status, and reports on processes and stale sockets using the port.
# It accepts a port number as the first argument, defaulting to 9876 if no argument is provided.

PORT=${1:-9876}
CHECK_INTERVAL=5
HISTORY_FILE="port_check_history.log"

# Create the log file if it doesn't exist
touch $HISTORY_FILE

check_port() {
    while true; do
        if ss -ltn | grep -q ":$PORT "; then
            status="open"
        else
            status="closed"
        fi

        # Report on the status of the port
        echo "$(date '+%Y-%m-%d %H:%M:%S') - Port $PORT is $status" >> $HISTORY_FILE

        # Report on processes using the port
        echo "Processes using port $PORT:" >> $HISTORY_FILE
        lsof -i:$PORT >> $HISTORY_FILE

        # Report on stale sockets
        echo "Stale sockets for port $PORT:" >> $HISTORY_FILE
        netstat -anp | grep ":$PORT" >> $HISTORY_FILE

        sleep $CHECK_INTERVAL
    done
}

print_history() {
    tail -f $HISTORY_FILE
}

# Run the check_port function in the background
check_port &

# Display the history
print_history
