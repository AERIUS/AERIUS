/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db;

import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Abstract class to help with inserting batch-wise.
 *
 * @param <T> The type of the objects to insert.
 */
public abstract class BatchInserter<T> {

  private static final Logger LOGGER = LoggerFactory.getLogger(BatchInserter.class);

  /**
   * Batch size interval between batch commits to the database.
   *
   * A high value (>1000) will take a long time to complete, an even higher value will cause OutOfMemory,
   * a low value (<20) is inefficient because then batching would have no real effect.
   */
  private static final int DEFAULT_BATCH_SIZE = 500;

  private int batchSize = DEFAULT_BATCH_SIZE;

  /**
   * @param con The connection to use.
   * @param query The query to use when inserting.
   * @param collection The collection to insert.
   * @return Number of rows inserted.
   * @throws SQLException In case of database exceptions.
   */
  public int insertBatch(final Connection con, final String query, final Collection<T> collection) throws SQLException {
    int insertCount = 0;
    int batchCount = 0;
    try (final PreparedStatement ps = con.prepareStatement(query)) {
      for (final T t : collection) {
        setParameters(ps, t);
        ps.addBatch(); // Uses batches, faster than inserting one by one.
        if (++batchCount % batchSize == 0) {
          insertCount += sum(ps.executeBatch());
        }
      }
      // Ensures anything left in batch gets inserted.
      insertCount += sum(ps.executeBatch());
    } catch (final BatchUpdateException e) {
      LOGGER.error("insertBatch failed:", e);
      throw e.getNextException();
    }
    return insertCount;
  }

  /**
   * @param ps The preparedstatement to set the parameters on.
   * @param object The object to obtain the parameters from.
   * @throws SQLException In case of database exceptions.
   */
  public abstract void setParameters(PreparedStatement ps, T object) throws SQLException;

  private static int sum(final int[] toSum) {
    int result = 0;
    for (final int i : toSum) {
      result += i;
    }
    return result;
  }

  public int getBatchSize() {
    return batchSize;
  }

  public void setBatchSize(final int batchSize) {
    this.batchSize = batchSize;
  }
}
