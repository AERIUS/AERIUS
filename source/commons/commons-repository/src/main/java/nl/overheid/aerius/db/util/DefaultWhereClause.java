/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.util;

import java.text.MessageFormat;

/**
 * Where clause based on an Attribute. Will be used in the form "attribute = ?"
 * If a type is given it will cast the input to that type: "attribute = ?::type"
 */
public class DefaultWhereClause implements WhereClause {

  private static final String WHERE_PATTERN = "{0} = ?";
  private static final String WHERE_PATTERN_TYPE = WHERE_PATTERN + "::{1}";

  private final String type;
  private final Attribute attribute;

  /**
   * Constructs a where clause for the attribute
   *
   * @param attribute query attribute
   */
  public DefaultWhereClause(final Attribute attribute) {
    this(attribute, null);
  }

  /**
   * Constructs where clause with defined type to cast to.
   *
   * @param attribute query attribute
   * @param type type to cast input to
   */
  public DefaultWhereClause(final Attribute attribute, final String type) {
    this.type = type;
    this.attribute = attribute;
  }

  @Override
  public String toWherePart() {
    if (type == null) {
      return MessageFormat.format(WHERE_PATTERN, attribute.attribute());
    } else {
      return MessageFormat.format(WHERE_PATTERN_TYPE, attribute.attribute(), type);
    }
  }

  @Override
  public Attribute[] getParamAttributes() {
    return new Attribute[] {attribute };
  }

}
