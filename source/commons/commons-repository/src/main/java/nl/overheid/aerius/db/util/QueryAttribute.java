/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.util;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Query attribute enum. Makes it harder to create typos in queries (once defined properly). Can be used in combination with {@link QueryBuilder}.
 */
public enum QueryAttribute implements Attribute {

  /**
   * The id of a receptor (or hexagon).
   */
  RECEPTOR_ID,
  /**
   * The id of a critical deposition area (area with a certain critical deposition value, like habitat type areas).
   */
  CRITICAL_DEPOSITION_AREA_ID,
  /**
   * A type, e.g.: Habitat type.
   */
  TYPE,
  /**
   * The id of a sector.
   */
  SECTOR_ID,
  /**
   * The id of an assessment area (a n2k area).
   */
  ASSESSMENT_AREA_ID,
  /**
   * The id of a habitat type.
   */
  HABITAT_TYPE_ID,
  /**
   * The id of a goal habitat type.
   */
  GOAL_HABITAT_TYPE_ID,
  /**
   * The name of something (like a habitat type).
   */
  NAME,
  /**
   * The name of an assessment area.
   */
  ASSESSMENT_AREA_NAME,
  /**
   * The name of a habitat.
   */
  HABITAT_NAME,
  /**
   * The label of something (like a calculation point).
   */
  LABEL,
  /**
   * Boolean to indicate if a habitat type is designated or not.
   */
  DESIGNATED,
  /**
   * Boolean to indicate if a habitat type is considered relevant (nitrogen sensitive for instance) or not.
   */
  RELEVANT,
  /**
   * Boolean to indicate if a habitat type is pollutant-sensitive or not.
   */
  SENSITIVE,
  /**
   * The critical deposition value. The amount of deposition around which point a habitat type will start to be affected.
   */
  CRITICAL_DEPOSITION,
  /**
   * The critical level. The level at which point a habitat type will start to be affected. Level is specific to substance type.
   */
  CRITICAL_LEVEL,
  /**
   * A geometry.
   */
  GEOMETRY,
  /**
   * A year.
   */
  YEAR,
  /**
   * The id of a province.
   */
  PROVINCE_AREA_ID,
  /**
   * The average roughness (z0) on a receptor.
   */
  AVERAGE_ROUGHNESS,
  /**
   * The dominant land use on a receptor.
   */
  DOMINANT_LAND_USE,
  /**
   * The distribution of land uses on a receptor.
   */
  LAND_USES,
  /**
   * The distance.
   */
  DISTANCE,
  /**
   * The maximum distance.
   */
  MAX_DISTANCE,
  /**
   * The bounding box (square box containing a geometry).
   */
  BOUNDINGBOX,
  /**
   * The icon type (of a sector for instance).
   */
  ICON_TYPE,
  /**
   * The color of something.
   */
  COLOR,
  /**
   * A reference.
   */
  REFERENCE,
  /**
   * A code.
   */
  CODE,
  /**
   * A description.
   */
  DESCRIPTION,
  /**
   * The heat content of a source(source characteristic).
   */
  HEAT_CONTENT,
  /**
   * The height of a source (source characteristic).
   */
  HEIGHT,
  /**
   * The spread of a source, usually coupled with height (source characteristic).
   */
  SPREAD,
  /**
   * The particle size distribution of a source, used for particle matter calculations (source characteristic).
   */
  PARTICLE_SIZE_DISTRIBUTION,
  /**
   * The ID of a substance.
   */
  SUBSTANCE_ID,
  /**
   * The emission factor.
   */
  EMISSION_FACTOR,
  /**
   * The reduction emission factor.
   */
  REDUCTION_FACTOR, REDUCTION_FACTOR_FLOOR, REDUCTION_FACTOR_CELLAR, REDUCTION_FACTOR_TOTAL,
  /**
   * The proportion factor of the total ammonia emissions originating from the floor or cellar
   */
  PROPORTION_FLOOR, PROPORTION_CELLAR,
  /**
   * The ID of a farm animal category.
   */
  FARM_ANIMAL_CATEGORY_ID,
  /**
   * The farm animal type.
   */
  FARM_ANIMAL_TYPE,
  /**
   * The ID of a farm animal housing category.
   */
  FARM_ANIMAL_HOUSING_CATEGORY_ID,
  /**
   * The ID of a  basic (farm animal) housing category.
   */
  BASIC_HOUSING_CATEGORY_ID,
  /**
   * The ID of a farm animal additional housing system.
   */
  FARM_ADDITIONAL_HOUSING_SYSTEM_ID,
  /**
   * The ID of a farm lodging type.
   */
  FARM_LODGING_TYPE_ID,
  /**
   * The emission factor type
   */
  FARM_EMISSION_FACTOR_TYPE,
  /**
   * The ID of an additional farm lodging system.
   */
  FARM_ADDITIONAL_LODGING_SYSTEM_ID,
  /**
   * The ID of an reductive farm lodging system.
   */
  FARM_REDUCTIVE_LODGING_SYSTEM_ID,
  /**
   * The ID of a farm lodging fodder measure.
   */
  FARM_LODGING_FODDER_MEASURE_ID,
  /**
   * The ID of a farm lodging system definition (BWL-code).
   */
  FARM_LODGING_SYSTEM_DEFINITION_ID,
  /**
   * The ID of an 'other' farm lodging type.
   */
  FARM_OTHER_LODGING_TYPE_ID,
  /**
   * The ID of a farm source category.
   */
  FARM_SOURCE_CATEGORY_ID,
  /**
   * Whether a farm lodging type/system is an air scrubber.
   */
  SCRUBBER,
  /**
   * Whether a farm additional housing system is an air scrubber.
   */
  AIR_SCRUBBER,
  /**
   * The coverage of a habitat type.
   */
  COVERAGE,
  /**
   * The surface of an area. (or actually area I guess...)
   */
  SURFACE,
  /**
   * The authority in charge of something.
   */
  AUTHORITY,
  /**
   * The type of authority
   */
  AUTHORITY_TYPE,
  /**
   * The codes of the directives
   */
  DIRECTIVE_CODES,
  /**
   * The ID of a calculation.
   */
  CALCULATION_ID,
  /**
   * The ID of a custom calculation point (defined by the user, not a receptor).
   */
  CALCULATION_POINT_ID,
  /**
   * The ID of a calculation sub point.
   */
  CALCULATION_SUB_POINT_ID,
  /**
   * The level for a calculation sub point.
   */
  LEVEL,
  /**
   * The ID of a calculation result set.
   */
  CALCULATION_RESULT_SET_ID,
  /**
   * The type of a calculation result (deposition, concentration, etc).
   */
  RESULT_TYPE,
  /**
   * The type of a calculation result (deposition, concentration, etc).
   */
  EMISSION_RESULT_TYPE,
  /**
   * A calculation result value.
   */
  RESULT,
  /**
   * A total calculation result value, usually including background values.
   */
  TOTAL_RESULT,
  /**
   * The ID of a UserProfile.
   */
  USER_ID,
  /**
   * The ID of an Authority.
   */
  AUTHORITY_ID,
  /**
   * The proposed calculation id.
   */
  PROPOSED_CALCULATION_ID,
  /**
   * The reference calculation id.
   */
  REFERENCE_CALCULATION_ID,
  /**
   * The off site reduction calculation id.
   */
  OFF_SITE_REDUCTION_CALCULATION_ID,
  /**
   * The temporary calculation ids.
   */
  TEMPORARY_CALCULATION_IDS,
  /**
   * The current calculation id.
   */
  CURRENT_CALCULATION_ID,
  /**
   * The zoom level.
   */
  ZOOM_LEVEL,
  /**
   * The x coordinate.
   */
  X_COORD,
  /**
   * The y coordinate.
   */
  Y_COORD,
  /**
   * The buffer to use.
   */
  BUFFER,
  /**
   * Habitat quality goal.
   */
  QUALITY_GOAL,
  /**
   * Habitat extent goal.
   */
  EXTENT_GOAL,
  /**
   * Relevant coverage.
   */
  RELEVANT_COVERAGE,
  /**
   * Relevant surface.
   */
  RELEVANT_SURFACE,
  /**
   * Total cartographic surface.
   */
  CARTOGRAPHIC_SURFACE,
  /**
   * Relevant cartographic surface.
   */
  RELEVANT_CARTOGRAPHIC_SURFACE,
  /**
   * A value e.g. the permit threshold value
   */
  VALUE,
  /**
   * A count e.g. record count
   */
  COUNT,
  /**
   * Development space
   */
  SPACE,
  /**
   * Borrowed portion of reserved development space
   */
  BORROWED,
  /**
   * Development space segment
   */
  SEGMENT,
  /**
   * Development space status
   */
  STATUS,

  /**
   * User role ID.
   */
  USERROLE_ID,

  /**
   * Permission ID.
   */
  PERMISSION_ID,

  /**
   * Diurnal variation code.
   */
  EMISSION_DIURNAL_VARIATION_CODE,

  /**
   * Emission diurnal variation ID.
   */
  EMISSION_DIURNAL_VARIATION_ID,

  /**
   * Unique ID for a Job.
   */
  JOB_ID,

  /**
   * A key, e.g: Job key.
   */
  KEY,

  /**
   * Filename of a file.
   */
  FILENAME,

  /**
   * Generic ID.
   */
  ID,
  /**
   *
   */
  USER_CALCULATION_POINT_SET_ID,
  /**
   *
   */
  USER_CALCULATION_POINT_ID,
  /**
   *
   */
  CLASS_NAME,
  /**
   *
   */
  OPS_VERSION,
  /**
   *
   */
  PRIORITY_PROJECT_ID,
  /**
   *
   */
  PRIORITY_PROJECT_REFERENCE,
  /**
   *
   */
  INITIAL_AVAILABLE_SPACE,
  /**
   *
   */
  ASSIGNED_SPACE,
  /**
   *
   */
  AVAILABLE_SPACE,
  /**
   *
   */
  HEXAGON_TYPE,
  /**
   *
   */
  OVERLAPPING_HEXAGON_TYPE,
  /**
   * Reference of the situation
   */
  SITUATION_REFERENCE,
  /**
   * Type of the situation
   */
  SITUATION_TYPE,
  /**
   * Name of the situation
   */
  SITUATION_NAME,
  /**
   * Version of AERIUS used for the situation results.
   */
  SITUATION_VERSION,
  /**
   * Result from reference situation calculation
   */
  REFERENCE_RESULT,
  /**
   * Result from off site reduction situation calculation
   */
  OFF_SITE_REDUCTION_RESULT,
  /**
   * Result from project calculation OR max effect calculation
   */
  PROPOSED_RESULT,
  /**
   * Scenario result type (combined result of multiple situations within a job).
   */
  SCENARIO_RESULT_TYPE,
  /**
   * Area marker location
   */
  MARKER_LOCATION,
  /**
   * Metadata.
   */
  METADATA;

  /**
   * @return the attribute representation as used in most queries.
   */
  @Override
  public String attribute() {
    return name().toLowerCase();
  }

  /**
   * @param rs The resultset to get the value from.
   * @return The value corresponding to this enum in the resultset.
   * @throws SQLException In case of a query error.
   */
  public int getInt(final ResultSet rs) throws SQLException {
    return QueryUtil.getInt(rs, this);
  }

  /**
   * @param rs The resultset to get the value from.
   * @return The value corresponding to this enum in the resultset.
   * @throws SQLException In case of a query error.
   */
  public double getDouble(final ResultSet rs) throws SQLException {
    return QueryUtil.getDouble(rs, this);
  }

  /**
   * @param rs The resultset to get the value from.
   * @return The value corresponding to this enum in the resultset.
   * @throws SQLException In case of a query error.
   */
  public Double getNullableDouble(final ResultSet rs) throws SQLException {
    return QueryUtil.getNullableDouble(rs, this);
  }

  /**
   * @param rs The resultset to get the value from.
   * @return The value corresponding to this enum in the resultset.
   * @throws SQLException In case of a query error.
   */
  public String getString(final ResultSet rs) throws SQLException {
    return QueryUtil.getString(rs, this);
  }

  /**
   * @param rs The resultset to get the value from.
   * @return The value corresponding to this enum in the resultset.
   * @throws SQLException In case of a query error.
   */
  public <E extends Enum<E>> E getEnum(final ResultSet rs, final Class<E> enumClass) throws SQLException {
    return QueryUtil.getEnum(rs, this, enumClass);
  }

  /**
   * @param rs The resultset to get the value from.
   * @return The value corresponding to this enum in the resultset.
   * @throws SQLException In case of a query error.
   */
  public boolean getBoolean(final ResultSet rs) throws SQLException {
    return QueryUtil.getBoolean(rs, this);
  }

  /**
   * @param rs The resultset to get the value from.
   * @return The value corresponding to this enum in the resultset.
   * @throws SQLException In case of a query error.
   */
  public Object getObject(final ResultSet rs) throws SQLException {
    return QueryUtil.getObject(rs, this);
  }

  /**
   * @param rs The resultset to get the Array from.
   * @return The value corresponding to this Array in the resultset.
   * @throws SQLException In case of a query error.
   */
  public Array getArray(final ResultSet rs) throws SQLException {
    return QueryUtil.getArray(rs, this);
  }

  /**
   * @param rs The resultset to check the null value for.
   * @return Boolean whether the field is null.
   * @throws SQLException In case of a query error.
   */
  public boolean isNull(final ResultSet rs) throws SQLException {
    return QueryUtil.isNull(rs, this);
  }

}
