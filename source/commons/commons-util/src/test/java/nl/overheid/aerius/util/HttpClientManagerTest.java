/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import java.util.stream.Stream;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.protocol.HttpContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.util.HttpClientManager.ServiceUnavailableRetryStrategyImpl;

/**
 * Test class for {@link HttpClientManager}.
 */
class HttpClientManagerTest {

  final ServiceUnavailableRetryStrategyImpl retryStrategy = new ServiceUnavailableRetryStrategyImpl();

  private static Stream<Arguments> data() {
    return Stream.of(
        Arguments.of(200, HttpClientManager.RETRY_AMOUNT_MAX - 1, false),
        Arguments.of(200, HttpClientManager.RETRY_AMOUNT_MAX + 1, false),
        Arguments.of(200, HttpClientManager.RETRY_AMOUNT_MAX, false),
        Arguments.of(400, HttpClientManager.RETRY_AMOUNT_MAX - 1, false),
        Arguments.of(400, HttpClientManager.RETRY_AMOUNT_MAX + 1, false),
        Arguments.of(400, HttpClientManager.RETRY_AMOUNT_MAX, false),
        Arguments.of(503, HttpClientManager.RETRY_AMOUNT_MAX - 1, true),
        Arguments.of(503, HttpClientManager.RETRY_AMOUNT_MAX + 1, false),
        Arguments.of(503, HttpClientManager.RETRY_AMOUNT_MAX, true),
        Arguments.of(504, HttpClientManager.RETRY_AMOUNT_MAX - 1, true),
        Arguments.of(504, HttpClientManager.RETRY_AMOUNT_MAX + 1, false),
        Arguments.of(504, HttpClientManager.RETRY_AMOUNT_MAX, true));

  }

  @ParameterizedTest
  @MethodSource("data")
  void testRetryOK(final int statusCode, final int retry, final boolean expectedToRetry) {
    final StatusLine statusLine = mock(StatusLine.class);
    doReturn(statusCode).when(statusLine).getStatusCode();

    final HttpResponse response = mock(HttpResponse.class);
    doReturn(statusLine).when(response).getStatusLine();

    final boolean result = retryStrategy.retryRequest(response, retry, new MockedHttpContext());

    if (expectedToRetry) {
      assertTrue(result, "Retry unexpectedly failed");
    } else {
      assertFalse(result, "Retry unexpectedly succeeded");
    }
  }

  private class MockedHttpContext implements HttpContext {

    @Override
    public Object getAttribute(final String id) {
      return null;
    }

    @Override
    public void setAttribute(final String id, final Object obj) {
    }

    @Override
    public Object removeAttribute(final String id) {
      return null;
    }

  }

}
