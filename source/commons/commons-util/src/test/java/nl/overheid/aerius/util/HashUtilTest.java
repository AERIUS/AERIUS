/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;
import java.util.function.BiFunction;

import org.junit.jupiter.api.Test;

/**
 * Test class for {@link HashUtil}.
 */
class HashUtilTest {

  private static final String TEST_SALT = "this\ris\r\nmy\ntest\nsalt";
  private static final String TEST_STRING = "this\ris\r\nmy\ntest\nstring";

  @Test
  void testLegacyHashStrings() {
    assertHash(HashUtil::generateLegacySaltedHash, List.of(TEST_STRING), "J0bZEbqPFhnaTv8pvtKdRWHBMl0=");
  }

  @Test
  void testHashStrings() {
    assertHash(HashUtil::generateSaltedHash, TEST_STRING.getBytes(), "J0bZEbqPFhnaTv8pvtKdRWHBMl0=");
  }

  <T> void assertHash(final BiFunction<String, T, String> hashMethod, final T data, final String testHash) {
    // Create and test hash1
    final String hash1 = hashMethod.apply(TEST_SALT, data);
    assertNotNull(hash1, "Generating hash shouldn't return null");
    assertEquals(testHash, hash1, "Hash should be same as ever");

    // Create and test hash2
    final String hash2 = hashMethod.apply(TEST_SALT, data);
    assertNotNull(hash2, "Generating hash a second time shouldn't return null");
    assertEquals(testHash, hash2, "Hash should be same as ever");

    // Compare both hashes
    assertEquals(hash1, hash2, "Generated hashes should be the same");
  }

  @Test
  void testHashMultipleStrings() {
    final String testString2 = "this'then'wouldarrrr<be>dfasdf>/another>";
    final String testHash = "z9iXhb9YbloOneMUExcNzuQ+VI4=";
    final List<String> data = List.of(TEST_STRING, testString2);

    // Create and test hash1
    final String hash1 = HashUtil.generateLegacySaltedHash(TEST_SALT, data);
    assertNotNull(hash1, "Generating hash for multiple strings shouldn't be null");
    assertEquals(testHash, hash1, "Hash should be same as ever");

    // Create and test hash2
    final String hash2 = HashUtil.generateLegacySaltedHash(TEST_SALT, data);
    assertNotNull(hash2, "Generating hash for multiple strings a second time shouldn't be null");
    assertEquals(testHash, hash2, "Hash should be same as ever");

    // Compare both hashes
    assertEquals(hash1, hash2, "Generated hashes should be the same");

    //reverse order and try again. Hashes should NOT be equal now.
    final String hash3 = HashUtil.generateLegacySaltedHash(TEST_SALT, List.of(testString2, TEST_STRING));
    assertNotNull(hash3, "Generating hash with different string order shouldn't return null");
    assertNotEquals(testHash, hash3, "Generated hashes shouldn't be the same if strings were in different order");
  }
}
