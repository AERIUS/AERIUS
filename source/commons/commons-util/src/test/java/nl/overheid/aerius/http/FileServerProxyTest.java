/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.http;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doReturn;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.util.HttpClientManager;

/**
 * Test class for {@link FileServerProxy}.
 */
@ExtendWith(MockitoExtension.class)
class FileServerProxyTest {
  private static final String URL = "http://localhost";

  private @Mock HttpClientManager httpClientManager;
  private @Mock CloseableHttpClient closeableHttpClient;
  private @Mock CloseableHttpResponse closeableHttpResponse;
  private @Mock HttpEntity httpEntity;
  private @Mock StatusLine statusLine;
  private @Captor ArgumentCaptor<HttpUriRequest> httpUriRequestCaptor;

  private FileServerProxy fileServerProxy;

  @BeforeEach
  void beforeEach() throws ClientProtocolException, IOException {
    doReturn(closeableHttpClient).when(httpClientManager).getHttpClient(anyInt());
    doReturn(closeableHttpResponse).when(closeableHttpClient).execute(httpUriRequestCaptor.capture());
    doReturn(httpEntity).when(closeableHttpResponse).getEntity();
    doReturn(statusLine).when(closeableHttpResponse).getStatusLine();
    fileServerProxy = new FileServerProxy(httpClientManager, URL);
  }

  @Test
  void testGetContent() throws IOException, HttpException, URISyntaxException {
    final String content = "content";
    final String pathSegment = "1/2";

    doReturn(HttpStatus.SC_OK).when(statusLine).getStatusCode();
    doReturn(new ByteArrayInputStream(content.getBytes())).when(httpEntity).getContent();
    assertEquals(content, fileServerProxy.getContent(pathSegment), "Expects to return the mocked content");
    assertEquals(URL + "/" + pathSegment, httpUriRequestCaptor.getValue().getURI().toString(), "Expects to return the correct url");
  }
}
