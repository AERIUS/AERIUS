/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

/**
 * Test class for {@link WorkerThreadUtil}.
 */
class WorkerThreadUtilTest {

  private static final String PREFIX = "SOMEPRE";
  private static final String WORKER_TYPE = "WORKWORK";

  @Test
  void testSetThreadNameNullId() {
    final String oldName = Thread.currentThread().getName();
    WorkerThreadUtil.setThreadName(PREFIX, WORKER_TYPE);
    final String newName = Thread.currentThread().getName();
    assertNotEquals(oldName, newName, "Thread name should have changed");
    assertEquals("AERIUS-SOMEPRE-WORKWORK", newName, "Thread name should have changed");
  }

  @Test
  void testSetThreadNameOneCharacter() {
    final String oldName = Thread.currentThread().getName();
    WorkerThreadUtil.setThreadName(PREFIX, WORKER_TYPE, "o");
    final String newName = Thread.currentThread().getName();
    assertNotEquals(oldName, newName, "Thread name should have changed");
    assertEquals("AERIUS-SOMEPRE-WORKWORK-o", newName, "Thread name should have changed");
  }

  @Test
  void testSetThreadNameNearLimit() {
    final String oldName = Thread.currentThread().getName();
    WorkerThreadUtil.setThreadName(PREFIX, WORKER_TYPE, "123456");
    final String newName = Thread.currentThread().getName();
    assertNotEquals(oldName, newName, "Thread name should have changed");
    assertEquals("AERIUS-SOMEPRE-WORKWORK-123456", newName, "Thread name should have changed");
  }

  @Test
  void testSetThreadNameOverLimit() {
    final String oldName = Thread.currentThread().getName();
    WorkerThreadUtil.setThreadName(PREFIX, WORKER_TYPE, "1234567");
    final String newName = Thread.currentThread().getName();
    assertNotEquals(oldName, newName, "Thread name should have changed");
    assertEquals("AERIUS-SOMEPRE-WORKWORK-123456", newName, "Thread name should have changed");
  }
}
