/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

public final class WorkerThreadUtil {

  private static final String AERIUS = "AERIUS-";
  private static final int UNIQUE_ID_SHORT_LENGTH = 6;

  private WorkerThreadUtil() {
    // Util class
  }

  /**
   * Sets the name of the current thread. Use this to set the name of a thread that is unique with the given prefix and workerType.
   *
   * @param prefix prefixes workerType with
   * @param workerType workerType name
   */
  public static void setThreadName(final String prefix, final String workerType) {
    Thread.currentThread().setName(AERIUS + prefix + "-" + workerType);
  }

  /**
   * Sets the name of the current thread. Use this to set the name of a thread that can have multiple instances with the given prefix and worker
   * combination. It's distinguished with the uniqueId.
   *
   * @param prefix prefixes workerType with
   * @param workerType workerType name
   * @param uniqueId id for prefix and workerType combination
   */
  public static final void setThreadName(final String prefix, final String workerType, final String uniqueId) {
    final String uniqueIdPart = obtainUniqueIdPart(uniqueId);
    Thread.currentThread().setName(AERIUS + prefix + "-" + workerType + "-" + uniqueIdPart);
  }

  private static String obtainUniqueIdPart(final String uniqueId) {
    if (uniqueId == null || uniqueId.length() <= UNIQUE_ID_SHORT_LENGTH) {
      return uniqueId;
    } else {
      return uniqueId.substring(0, UNIQUE_ID_SHORT_LENGTH);
    }
  }
}
