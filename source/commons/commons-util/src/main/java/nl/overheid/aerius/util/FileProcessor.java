/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import java.io.File;

import nl.overheid.aerius.shared.FileServerExpireTag;

/**
 * Interface to classes that process a file. Generally used to process zip files and do something with those zip files.
 */
public interface FileProcessor {

  /**
   * Processes the given file.
   *
   * @param id id to identify the file
   * @param filename name of the file
   * @param file full path to the file
   * @param expire expire tag for the file
   * @return identifier for the processed file
   * @throws Exception
   */
  String processFile(final String id, final String filename, final File file, final FileServerExpireTag expire) throws Exception;
}
