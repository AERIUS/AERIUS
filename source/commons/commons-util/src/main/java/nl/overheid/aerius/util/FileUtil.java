/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Util class to store data to file.
 */
public final class FileUtil {

  static final int MAX_OPTIONAL_FILENAME_LENGTH = 128;

  private static final String UNSAFE_FILENAME_CHARACTERS_REGEX = "\\W+";
  private static final String DATEFORMAT_FILENAME = "yyyyMMddHHmmss";
  private static final String FILENAME_SEPARATOR = "_";

  private static final Logger LOG = LoggerFactory.getLogger(FileUtil.class);

  public interface FileMatcher<T> {
    T get(File dir, String name, Matcher matcher);
  }

  private FileUtil() {
    //util class
  }

  /**
   * Write the content to a file in the temp directory. The file name of the
   * file is prefixed with an unique string.
   *
   * @param prefix file prefix
   * @param filename name of the file
   * @param content content of the file
   * @throws IOException exception on write file error
   */
  public static void toFile(final String prefix, final String filename, final String content) throws IOException {
    try (final BufferedWriter writer = Files.newBufferedWriter(File.createTempFile(prefix, filename).toPath(), StandardCharsets.UTF_8)) {
      writer.write(content);
      writer.flush();
    }
  }

  /**
   * Creates a directory in the given parent directory. If the directory path doesn't exist it is created.
   *
   * @param parentDirectory parent directory
   * @param prefix optional prefix
   * @return created directory
   * @throws IOException
   */
  public static File createDirectory(final File parentDirectory, final String prefix) throws IOException {
    final String actualPrefix = prefix == null ? "" : (prefix.toLowerCase(Locale.ROOT) + "-");
    final String randomName = UUID.randomUUID().toString().replace("-", "");
    final File directory = new File(parentDirectory, actualPrefix + randomName);

    if (!directory.exists() && !directory.mkdirs()) {
      throw new IOException("Could not create directory " + directory);
    }
    return directory;
  }

  /**
   * Strips possibly invalid characters from the filename to ensure a valid filename.
   *
   * @param name The possibly invalid filename
   * @return Valid filename
   */
  public static String getSafeFilename(final String name) {
    return name == null ? null : name.replaceAll(UNSAFE_FILENAME_CHARACTERS_REGEX, "");
  }

  /**
   * Get an (Aerius) file name.
   * Format: prefix_datestring[_optionalName][.]extension
   * @param prefix Prefix to use in the filename.
   * @param extension Extension to use for the filename. Will be prefixed by a . if not in the string.
   * @param optionalName The optional name to use in the filename.
   * @param optionalDate The optional date to use for the datestring. If null, current time will be used.
   * @return The file name that can be used.
   */
  public static String getFilename(final String prefix, final String extension, final String optionalName,
      final Date optionalDate) {
    if (prefix == null || extension == null) {
      throw new IllegalArgumentException("Prefix or extension not allowed to be null. Prefix: " + prefix + ", extension: " + extension);
    }
    return getActualFileName(prefix, extension, optionalName, optionalDate);
  }

  private static String getActualFileName(final String prefix, final String extension, final String optionalName,
      final Date optionalDate) {
    final StringBuilder filenameBuilder = new StringBuilder();
    filenameBuilder.append(prefix);
    filenameBuilder.append(FILENAME_SEPARATOR);
    filenameBuilder.append(new SimpleDateFormat(DATEFORMAT_FILENAME).format(
        optionalDate == null ? Calendar.getInstance().getTime() : optionalDate));
    if (optionalName != null && !optionalName.isEmpty()) {
      filenameBuilder.append(FILENAME_SEPARATOR);
      filenameBuilder.append(getSafeOptionalName(optionalName));
    }
    if (extension != null && !extension.isEmpty()) {
      filenameBuilder.append(extension.charAt(0) == '.' ? extension : ('.' + extension));
    }
    return filenameBuilder.toString();
  }

  private static String getSafeOptionalName(final String optionalName) {
    //optional name can be user-input, so assure it's safe and not too long
    final String safeOptionalName = getSafeFilename(optionalName);
    return safeOptionalName != null && safeOptionalName.length() > MAX_OPTIONAL_FILENAME_LENGTH
        ? safeOptionalName.substring(0, MAX_OPTIONAL_FILENAME_LENGTH)
            : safeOptionalName;
  }

  /**
   * @param file The file to remove the extension from.
   * @return The file without extension (without .) or the file name if no extension.
   */
  public static String getFileWithoutExtension(final File file) {
    final String filename = file.getName();
    final int posOfDot = filename.lastIndexOf('.');
    return posOfDot == -1 ? filename : filename.substring(0, posOfDot);
  }

  /**
   * Returns a list of file objects. If a file matches the give filter the filter creates a object of that file name.
   *
   * @param path The directory to search for files.
   * @param pattern pattern to match the file names with
   * @param filter returns the object given the file and pattern matcher of the filename
   * @return List of objects created from files that matched the pattern
   * @throws FileNotFoundException
   */
  public static <T> List<T> getFilteredFiles(final File path, final Pattern pattern, final FileMatcher<T> filter) throws FileNotFoundException {
    if (!path.exists()) {
      throw new FileNotFoundException(path.toString());
    }
    final List<T> list = new ArrayList<>();

    path.list((dir, name) -> {
      final Matcher matcher = pattern.matcher(name);

      if (matcher.matches()) {
        list.add(filter.get(dir, name, matcher));
      }
      return false;
    });
    return list;
  }

  public static List<File> getFilesWithExtension(final File path, final String extension) throws IOException {
    return getFilteredFiles(path, f -> f.endsWith(extension));
  }

  /**
   * Finds all files with filename in the directory, recursively.
   *
   * @param path The directory to search for files.
   * @param filenameFilter Filter to only return files that match the filename filter
   * @return List of files in the directory that matched the filename filer.
   * @throws IOException When the directory does not exist or is not a directory.
   */
  public static List<File> getFilteredFiles(final File path, final Predicate<String> filenameFilter) throws IOException {
    try (final Stream<Path> walk = Files.walk(path.toPath())) {
      return walk
          .filter(f -> Files.isRegularFile(f) && (filenameFilter == null || filenameFilter.test(f.toFile().getName())))
          .map(Path::toFile)
          .collect(Collectors.toList());
    }
  }

  /**
   * Remove the directory and it's contents, recursively.
   * It's <strong>highly recommended</strong> to provide the <code>allowedFilenames</code> parameter
   * It could save lives, or <strong>at least</strong> your job.
   * @param path Path to the directory to remove.
   * @param allowedFilenames The filenames patterns that are allowed to be removed.
   * It's advised to use this as a failsafe, to make sure you are not deleting a lot of other stuff by specifying the wrong directory.
   * If no allowedFilenames are supplied, the whole directory will be removed.
   */
  public static void removeDirectoryRecursively(final Path path, final Pattern... allowedFilenames) {
    if (path != null
        && (path.toFile().getAbsolutePath().isEmpty()
            || (path.getRoot() != null && path.getRoot().toFile().getAbsoluteFile().equals(path.toFile().getAbsoluteFile())))) {
      LOG.error("Trying to delete root! for: {}", path);
    } else {
      try {
        Files.walkFileTree(path, new DeleteFileVisitor(allowedFilenames));
      } catch (final IOException e) {
        LOG.warn("Failed to delete directory {} with allowedFilenames {}. Error msg: {}", path, allowedFilenames, e.getMessage());
        LOG.debug("Exception when trying to delete directory", e);
      }
    }
  }
}
