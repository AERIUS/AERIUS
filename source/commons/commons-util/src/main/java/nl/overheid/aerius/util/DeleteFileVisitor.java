/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class DeleteFileVisitor extends SimpleFileVisitor<Path> {

  private final Pattern[] allowedFilenames;

  DeleteFileVisitor(final Pattern... allowedFilenames) {
    this.allowedFilenames = allowedFilenames;
  }

  @Override
  public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
    return delete(file);
  }

  @Override
  public FileVisitResult visitFileFailed(final Path file, final IOException exc) throws IOException {
    // Try to delete the file anyway, even if its attributes could not be read, since delete-only access is theoretically possible.
    return delete(file);
  }

  @Override
  public FileVisitResult postVisitDirectory(final Path dir, final IOException exc) throws IOException {
    if (exc != null) {
      // Directory iteration failed; propagate exception.
      throw exc;
    }

    return delete(dir);
  }

  private FileVisitResult delete(final Path path) throws IOException {
    if (!Files.isDirectory(path) && allowedFilenames != null && allowedFilenames.length > 0) {
      checkAllowedFilenames(path);
    }
    Files.delete(path);
    return FileVisitResult.CONTINUE;
  }

  private void checkAllowedFilenames(final Path path) throws IOException {
    boolean okayToDelete = false;
    final String filename = path.getName(path.getNameCount() - 1).toString();

    for (final Pattern allowedFilename : allowedFilenames) {
      if (matchesPattern(allowedFilename, filename)) {
        okayToDelete = true;
        break;
      }
    }

    if (!okayToDelete) {
      final StringBuilder error = new StringBuilder();
      error.append("Trying to remove files that we are not allowed to remove. Filename: ")
          .append(filename).append(" - allowed filenames: '");
      for (int i = 0; i < allowedFilenames.length; ++i) {
        if (i != 0) {
          error.append(',');
        }
        error.append(allowedFilenames[i]);
      }
      error.append('\'');

      throw new IOException(error.toString());
    }
  }

  /**
   * Match given pattern with given text. Special characters supported: * and ?.
   * @param pattern The pattern to match to.
   * @param text The text to match.
   * @return whether the given text matches given pattern
   */
  private static boolean matchesPattern(final Pattern pattern, final String text) {
    final Matcher matcher = pattern.matcher(text);
    return matcher.find();
  }
}
