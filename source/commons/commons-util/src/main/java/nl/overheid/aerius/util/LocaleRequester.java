/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.util;

import java.util.Locale;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nl.overheid.aerius.shared.Constants;

/**
 * Get locale settings from browser request cookie or default value.
 */
public class LocaleRequester {
  private static final String AERIUS_LOCALE_KEY = "AERIUS_Locale";

  public static Locale initLocale(final HttpServletRequest request, final HttpServletResponse response) {
    final Locale locale = LocaleRequester.getLocale(request, response);

    initSessionCookie(response, locale, request.isSecure());
    return locale;
  }

  public static Locale getLocale(final HttpServletRequest request, final HttpServletResponse response) {
    final String langParam = request.getParameter(Constants.LANGUAGE_PARAMETER);

    Locale locale;
    if (langParam == null) {
      // Get the locale from cookie if not present in the request
      locale = getLocaleFromCookie(request, null);

      // On failure, get a default locale based on request - or take default
      if (locale == null) {
        locale = getRequestLocaleOrDefault(request, response);
      }
    } else {
      // Take the requested language or default if it does not exist
      locale = LocaleUtils.getSupportedLocaleOrDefault(langParam);
    }

    return locale;
  }

  /**
   * Sets the locale in a cookie. The locale is taken from the browser or if not no locale sent by the browser the default locale will be taken.
   *
   * @param request request object
   * @param response response object
   * @return the locale set in the cookie
   */
  private static Locale getRequestLocaleOrDefault(final HttpServletRequest request, final HttpServletResponse response) {
    return request.getLocale() == null ? LocaleUtils.getDefaultLocale() : request.getLocale();
  }

  /**
   * Returns the locale from the cookie or the default if no locale set in cookie.
   *
   * @param request request
   * @return locale object
   */
  public static Locale getLocaleFromCookie(final HttpServletRequest request) {
    return getLocaleFromCookie(request, LocaleUtils.getDefaultLocale());
  }

  private static Locale getLocaleFromCookie(final HttpServletRequest request, final Locale defaultLocale) {
    Locale locale = defaultLocale;
    final Cookie[] cookies = request.getCookies();
    if (cookies != null) {
      for (final Cookie cookie : cookies) {
        if (AERIUS_LOCALE_KEY.equals(cookie.getName()) && (cookie.getValue() != null)) {
          locale = LocaleUtils.getLocale(cookie.getValue());
          break;
        }
      }
    }
    return locale;
  }

  private static void initSessionCookie(final HttpServletResponse response, final Locale locale, final boolean secure) {
    final Cookie cookie = new Cookie(AERIUS_LOCALE_KEY, locale.getLanguage());
    cookie.setHttpOnly(true);
    cookie.setMaxAge(Integer.MAX_VALUE);
    cookie.setSecure(secure);
    // We force the path to root so our dynamic URL strategy doesn't get in the way.
    cookie.setPath("/");

    response.addCookie(cookie);
  }
}
