/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Creates and sends the contents of the ZIP on the fly to the {@link OutputStream}
 *  without using external processes. Do not forget to call {@link #close()} when done adding entries.
 */
public class ZipFileMaker implements AutoCloseable {
  private static final Logger LOG = LoggerFactory.getLogger(ZipFileMaker.class);

  // The stream where we will send to zip contents to.
  private final ZipOutputStream out;

  // Whether the ZIP file is already closed.
  private boolean isClosed;

  /**
   * Create a {@link ZipFileMaker} for the given output stream.
   * @param out The outputstream to write zip to.
   */
  public ZipFileMaker(final OutputStream out) {
    assert out != null;

    // Wrap the given OutputStream with a ZipOutputStream.
    this.out = new ZipOutputStream(out);
  }

  /**
   * @param outputFile The zip file to write the files in.
   * @param files The files to save in the zip file.
   * @param base The base of the files, not to be included in the zip file but used for directories in the zip.
   * @param removeFiles Whether to remove the files after they're zipped. Use this if these are temporary files which will not be used after
   *  being zipped.
   * @throws IOException In case saving the file failed.
   */
  public static void files2ZipStream(final File outputFile, final List<File> files, final File base, final boolean removeFiles) throws IOException {
    try (FileOutputStream outputStream = new FileOutputStream(outputFile, true)) {
      files2ZipStream(outputStream, files, base, removeFiles);
    }
  }

  /**
   * @param outputStream The zip outputstream to write the files to.
   * @param files The files to save in the zip file.
   * @param base The base of the files, not to be included in the zip file but used for directories in the zip.
   * @param removeFiles Whether to remove the files after they're zipped. Use this if these are temporary files which will not be used after
   *  being zipped.
   * @throws IOException In case saving the file failed.
   */
  public static void files2ZipStream(final OutputStream outputStream, final List<File> files, final File base, final boolean removeFiles)
      throws IOException {
    try (ZipFileMaker fileMaker = new ZipFileMaker(outputStream)) {
      fileMaker.add(files, base);
    } finally {
      if (removeFiles) {
        for (final File file : files) {
          if (!Files.deleteIfExists(file.toPath())) {
            LOG.warn("Cleaning up generated file failed: {}", file.getAbsolutePath());
          }
        }
      }
    }
  }

  /**
   * Adds a new file entry to the ZIP.
   * @param filename The filename to add the file under in the ZIP file.
   * @param file The {@link File} to add.
   * @throws IOException on I/O error
   */
  public synchronized void add(final String filename, final File file) throws IOException {
    assert !isClosed;

    // Add ZIP entry to output stream.
    out.putNextEntry(new ZipEntry(filename));
    // Now add the contents of the input to the ZIP entry.
    Files.copy(file.toPath(), out);

    // Close the entry.
    out.closeEntry();
  }

  /**
   * Adds a list of files to the ZIP. The file will be stored under the same filename of the file and the same directory location.
   * Will try to do this recursively.
   * @param files The list of {@link File}s to add.
   * @param base The base directory of the files to use as base for the files.
   * Files will be placed in the zip file as they are relative to the base location.
   * @throws IOException on I/O error
   */
  public void add(final List<File> files, final File base) throws IOException {
    for (final File file : files) {
      if (file.isFile()) {
        add(base.toURI().relativize(file.toURI()).getPath(), file);
      } else if (file.isDirectory()) {
        add(Arrays.asList(file.listFiles()), base);
      }
    }
  }

  /**
   * Adds a new file entry to the ZIP. The file will be stored under the same filename of the file.
   * Use {@link #add(File, String)} if you want to specify another filename.
   * @param file The {@link File} to add.
   * @throws IOException on I/O error
   */
  public void add(final File file) throws IOException {
    add(file.getName(), file);
  }

  /**
   * @return Returns true if the zip stream is closed.
   */
  public boolean isClosed() {
    return isClosed;
  }

  /**
   * Will finish writing the ZIP file to the stream and will close the stream.
   * @throws IOException on I/O error
   */
  @Override
  public void close() throws IOException {
    if (isClosed()) {
      throw new IllegalArgumentException("ZIP stream is already closed using finish() before.");
    }
    // Close the stream.
    out.close();
    // Set the flag closed flag.
    isClosed = true;
  }
}
