/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.exec;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.util.OSUtils;
import nl.overheid.aerius.worker.WorkerThreadUtil;

/**
 * Utility class to correctly use output stream of a invoked process. Can be
 * needed for process to end gracefully
 *
 * @see http://www.javaworld.com/javaworld/jw-12-2000/jw-1229-traps.html
 */
public class StreamGobbler implements Runnable {
  private static final Logger LOG = LoggerFactory.getLogger(StreamGobbler.class);

  private final String type;
  private final String parentId;
  private final Predicate<String> logLineFunction;

  private InputStream is;

  /**
   * Constructor of the stream gobbler.
   *
   * @param type type of stream, ERROR, or OUTPUT
   * @param parentId id of the process using the StreamGobbler, used for reference
   * @param logLine function that checks if the line should be logged. If returns true the line is logged
   */
  public StreamGobbler(final String type, final String parentId, final Predicate<String> logLine) {
    this.type = type;
    this.parentId = parentId;
    this.logLineFunction = logLine;
  }

  protected InputStream getInputStream() {
    return is;
  }

  /**
   * Set the input steam.
   * @param is inputSteam of data
   */
  public void setInputStream(final InputStream is) {
    this.is = is;
  }

  /**
   * Process the stream output.
   */
  @Override
  public void run() {
    WorkerThreadUtil.setThreadName("Gobbler", type, parentId);
    final List<String> lines = new ArrayList<>();

    try (final BufferedReader br = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))) {
      String line = "";
      while ((line = br.readLine()) != null) { // eof
        if (!line.isEmpty() && logLineFunction.test(line)) {
          lines.add(line);
        }
      }
      if (!lines.isEmpty()) {
        logLine(String.join(OSUtils.NL, lines));
      }
    } catch (final IOException ioe) {
      LOG.error("Error gobbling stream", ioe);
    }
  }

  private void logLine(final String line) {
    LOG.error("{}: {}>{}", parentId, type, line);
  }
}
