/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius;

/**
 * Provides the AERIUS version number with git hash, date and all that.
 *
 * <p>
 * The version number is generated when Maven is triggered to start building a
 * release. Maven writes the version number to the manifest file.
 */
public final class AeriusVersion {

  // Not to be constructed.
  private AeriusVersion() {}

  public static String getFriendlyVersionNumber() {
    return getVersionNumber().split("_")[0];
  }

  /**
   * Get the AERIUS version number.
   *
   * @return version number
   */
  public static String getVersionNumber() {
    final String version = AeriusVersion.class.getPackage().getImplementationVersion();
    return version == null ? "DEV" : version;
  }
}
