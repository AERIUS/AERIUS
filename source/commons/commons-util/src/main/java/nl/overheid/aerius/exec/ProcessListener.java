/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.exec;

/**
 * Interface to monitor processes. Used by the {@link ExecuteWrapper} to register started processes, and removed the process when the process is
 * finished.
 */
public interface ProcessListener {
  /**
   * Register a new process.
   *
   * @param process to register
   */
  void registerProcess(Process process);

  /**
   * Remove the given process, it is finished.
   *
   * @param process process to remove
   */
  void removeProcess(Process process);
}
