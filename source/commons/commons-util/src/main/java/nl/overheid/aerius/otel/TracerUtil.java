/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.otel;

import io.opentelemetry.api.GlobalOpenTelemetry;
import io.opentelemetry.api.metrics.DoubleHistogram;
import io.opentelemetry.api.metrics.LongCounter;
import io.opentelemetry.api.metrics.Meter;
import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.SpanBuilder;
import io.opentelemetry.api.trace.SpanKind;
import io.opentelemetry.api.trace.StatusCode;
import io.opentelemetry.api.trace.Tracer;

import nl.overheid.aerius.AeriusVersion;

/**
 * Helper classes for usage of open telemetry.
 */
public final class TracerUtil {

  private static final String NAME = "aerius";
  private static final String VERSION = AeriusVersion.getVersionNumber();
  private static final Meter METER = GlobalOpenTelemetry.get().meterBuilder(NAME).setInstrumentationVersion(VERSION).build();

  private TracerUtil() {
    // Util class
  }

  /**
   * Returns a {@link SpanBuilder} for aerius, and sets the jobKey as Attribute.
   *
   * @param jobKey job key
   * @param spanName name of the span
   * @param spanKind kind of the span
   * @return returns span builder.
   */
  public static SpanBuilder getSpanBuilder(final String jobKey, final String spanName, final SpanKind spanKind) {
    final Tracer tracer = GlobalOpenTelemetry.get().getTracer(NAME, VERSION);
    return tracer
        .spanBuilder(spanName)
        .setSpanKind(spanKind)
        .setAttribute("aerius.job.key", jobKey);
  }

  /**
   * Returns a {@link LongCounter} for aerius, by the counterName and with a specific unit.
   *
   * @param counterName the name of the counter.
   * @return the LongCounter.
   */
  public static synchronized LongCounter getLongCounter(final String counterName) {
    return METER.counterBuilder(counterName).build();
  }

  /**
   * Returns a {@link DoubleHistogram}, by the histogramName and with a specific unit.
   *
   * @param histogramName the name of the counter.
   * @param unit the unit of the counter.
   * @return the DoubleHistogram.
   */
  public static synchronized DoubleHistogram getDoubleHistogram(final String histogramName, final String unit) {
    return METER.histogramBuilder(histogramName).setUnit(unit).build();
  }

  /**
   * Sets the status of the span to error with the exception.
   *
   * @param span span to set exception on
   * @param e exception to set on span.
   */
  public static void setException(final Span span, final Exception e) {
    span.setStatus(StatusCode.ERROR, e.getMessage());
    span.recordException(e);
  }
}
