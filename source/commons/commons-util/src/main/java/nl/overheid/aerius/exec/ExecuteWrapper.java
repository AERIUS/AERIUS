/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.exec;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Performs the actual execution of the CalculationMethod executable and wraps the output to be directed correctly.
 */
public final class ExecuteWrapper {

  /**
   * Wrapper to provide tool specific stream gobblers.
   */
  public interface StreamGobblers {
    StreamGobbler errorStreamGobbler(final String type, final String parentId);

    StreamGobbler outputStreamGobbler(final String type, final String parentId);
  }

  public static final ExecutorService EXECUTOR = Executors.newCachedThreadPool();
  private static final Logger LOG = LoggerFactory.getLogger(ExecuteWrapper.class);

  private static final String ERROR_LOGGING_PREFIX = "ERROR";
  private static final String OUTPUT_LOGGING_PREFIX = "OUTPUT";

  private final ExecuteParameters executeParameters;
  private final String binDir;
  private final StreamGobblers streamGobblers;
  private final Optional<ProcessListener> processListener;

  private Runtime runtime = Runtime.getRuntime();

  /**
   * Constructor.
   *
   * @param executeParameters the calculation method to use for this execution
   * @param binDir directory to the calculation method executable
   * @param streamGobblers streamGobbles to use
   */
  public ExecuteWrapper(final ExecuteParameters executeParameters, final String binDir, final StreamGobblers streamGobblers) {
    this(executeParameters, binDir, streamGobblers, null);
  }

  public ExecuteWrapper(final ExecuteParameters executeParameters, final String binDir, final StreamGobblers streamGobblers,
      final ProcessListener processListener) {
    this.executeParameters = executeParameters;
    this.binDir = binDir;
    this.streamGobblers = streamGobblers;
    this.processListener = Optional.ofNullable(processListener);
  }

  /**
   * Set other Runtime for testing purposes.
   *
   * @param runtime runtime to use for testing.
   */
  public void setRuntime(final Runtime runtime) {
    this.runtime = runtime;
  }

  /**
   * Execute the process with default stream gobblers.
   *
   * @param runId the ID that is only used when logging, so the process running can be identified
   * @param currentWorkingDirectory The current working directory to use (optional). If null, the current working directory of
   *  the process executing it will be used.
   * @return returns the process exit code
   * @throws IOException on I/O errors
   * @throws InterruptedException on interrupted streams
   */
  public int run(final String runId, final File currentWorkingDirectory) throws IOException, InterruptedException {
    return run(currentWorkingDirectory,
        streamGobblers.errorStreamGobbler(ERROR_LOGGING_PREFIX, runId),
        streamGobblers.outputStreamGobbler(OUTPUT_LOGGING_PREFIX, runId));
  }

  /**
   * Execute the process with the given stream gobblers.
   *
   * @param currentWorkingDirectory The current working directory to use (optional). If null, the current working directory of
   *  the process executing it will be used.
   * @param errorGobbler error stream handler
   * @param outputGobbler output stream handler
   * @return returns the process exit code
   * @throws IOException on I/O errors
   * @throws InterruptedException on interrupted streams
   */
  public int run(final File currentWorkingDirectory, final StreamGobbler errorGobbler, final StreamGobbler outputGobbler)
      throws IOException, InterruptedException {
    final String executeString = Paths.get(binDir, executeParameters.getExecuteableFilename()).toString();

    final String[] executeArray = new String[executeParameters.getArgs().length + 1];
    executeArray[0] = executeString;
    System.arraycopy(executeParameters.getArgs(), 0, executeArray, 1, executeParameters.getArgs().length);

    if (LOG.isDebugEnabled()) {
      LOG.debug("Executing: {}", String.join(" ", executeArray));
    }
    final Process process = runtime.exec(executeArray, null, currentWorkingDirectory);

    processListener.ifPresent(listener -> listener.registerProcess(process));
    // redirect streams to the gobbler to be able to let process.waitFor function correctly
    errorGobbler.setInputStream(process.getErrorStream());
    outputGobbler.setInputStream(process.getInputStream());
    final Future<?> errorFuture = EXECUTOR.submit(errorGobbler);
    final Future<?> outputFuture = EXECUTOR.submit(outputGobbler);

    try {
      final int exitCode = process.waitFor();
      errorFuture.get();
      outputFuture.get();
      return exitCode;
    } catch (final ExecutionException e) {
      LOG.trace("StreamGobbler execution exception:", e);
      return -1;
    } finally {
      processListener.ifPresent(listener -> listener.removeProcess(process));
      if (process.isAlive()) {
        process.destroyForcibly();
      }
    }
  }
}
