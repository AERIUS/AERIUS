/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.http;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.util.EntityUtils;

import nl.overheid.aerius.util.HttpClientManager;

/**
 * Wrapper to get content execute a http call and get the response body as String.
 */
public final class HttpClientProxy {

  private static final int TIMEOUT_SECONDS = 10;

  private final HttpClientManager httpClientManager;
  private final int timeoutSeconds;

  public HttpClientProxy(final HttpClientManager httpClientManager) {
    this(httpClientManager, TIMEOUT_SECONDS);
  }

  public HttpClientProxy(final HttpClientManager httpClientManager, final int timeoutSeconds) {
    this.httpClientManager = httpClientManager;
    this.timeoutSeconds = timeoutSeconds;
  }

  /**
   * Get content from a url, and write response entity to the target.
   *
   * Will use a retry mechanism on http errors of 500 and up.
   *
   * @param url the url
   * @param target the target file
   * @throws IOException
   * @throws HttpException
   */
  public void getContent(final String url, final File target) throws IOException, HttpException {
    final HttpGet httpGet = new HttpGet(url);

    try (final CloseableHttpResponse httpResponse = httpClientManager.getRetryingHttpClient(timeoutSeconds).execute(httpGet)) {
      final HttpEntity entity = httpResponse.getEntity();
      if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK && entity != null && (target.exists() || target.createNewFile())) {
        try (final FileOutputStream fileOutputStream = new FileOutputStream(target)) {
          entity.writeTo(fileOutputStream);
        }
      } else {
        throw new HttpException(httpResponse.getStatusLine().getStatusCode(), EntityUtils.toString(entity, StandardCharsets.UTF_8));
      }
    }
  }

  /**
   * Makes a http request for the given request. Returns the content or throws an exception if status was not ok.
   *
   * @param request request to make
   * @return Returns the content returned by the request
   * @throws IOException
   * @throws HttpException
   */
  public String execute(final HttpUriRequest request) throws IOException, HttpException {
    try (final CloseableHttpResponse httpResponse = httpClientManager.getHttpClient(timeoutSeconds).execute(request)) {
      final String content = EntityUtils.toString(httpResponse.getEntity(), StandardCharsets.UTF_8);

      if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
        return content;
      } else {
        throw new HttpException(httpResponse.getStatusLine().getStatusCode(), content);
      }
    }
  }

  /**
   * Makes a http request for the given request. Returns the an input stream or throws an exception if status was not ok.
   * The user of this input stream should take care closing the input stream.
   *
   * @param request request to make
   * @return input stream to returned content
   * @throws IOException
   * @throws HttpException
   */
  public InputStream executeToInputStream(final HttpUriRequest request) throws IOException, HttpException {
    final CloseableHttpResponse httpResponse = httpClientManager.getHttpClient(timeoutSeconds).execute(request);

    if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
      return httpResponse.getEntity().getContent();
    } else {
      throw new HttpException(httpResponse.getStatusLine().getStatusCode(), EntityUtils.toString(httpResponse.getEntity(), StandardCharsets.UTF_8));
    }
  }
}
