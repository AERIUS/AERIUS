/*
 * Copyright the State of the Netherlands
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;
import java.util.stream.Stream;

import nl.overheid.aerius.shared.FileServerExpireTag;
import nl.overheid.aerius.util.FileProcessor;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.util.ZipFileMaker;

/**
 * Class to zip files and post the zip to the file server. Use this class by creating an instances and adding content to the zip by directory.
 * When finished call post to send the zip file to the file server.
 */
public class ZipAndProcessChainer implements Closeable {

  private final FileProcessor zipPostProcessor;
  private final String zipFileName;
  private final File zipFile;
  private final ZipFileMaker zipFileMaker;
  private final boolean keepFiles;

  /**
   * Constructor.
   *
   * @param zipPostProcessor proxy object to the file server
   * @param zipFile name of the zip file the data is stored in
   * @param keepFiles if true doens't remove the files put in the zip and the zip when file processed.
   * @throws FileNotFoundException
   */
  public ZipAndProcessChainer(final FileProcessor zipPostProcessor, final File zipFile, final boolean keepFiles) throws FileNotFoundException {
    this.zipPostProcessor = zipPostProcessor;
    this.keepFiles = keepFiles;
    this.zipFileName = zipFile.getName();
    this.zipFile = zipFile;
    final FileOutputStream fos = new FileOutputStream(zipFile);
    final BufferedOutputStream bos = new BufferedOutputStream(fos);
    zipFileMaker = new ZipFileMaker(bos);
  }

  /**
   * Add the files of the given directory. The last directory name is included in the zip.
   *
   * @param directory directory to add to zip
   * @param includeDirectory if true the files are added to the zip in the directory name.
   * @throws IOException
   */
  public void addDirectoryContent(final File directory, final boolean includeDirectory) {
    final List<File> files = Stream.of(directory.list()).map(f -> new File(directory, f)).toList();

    try {
      try {
        zipFileMaker.add(files, includeDirectory ? directory.getParentFile() : directory);
      } finally {
        if (!keepFiles) {
          FileUtil.removeDirectoryRecursively(directory.toPath());
        }
      }
    } catch (final IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  /**
   * Passes the zip file to the given post processor, with optional request parameters.
   *
   * @param id to link the file to store with
   * @param expire the tag to indicate how long the import file should be stored
   * @return file code under which the zip file is stored in the remove file service
   * @throws Exception
   */
  public String put(final String id, final FileServerExpireTag expire) throws Exception {
    zipFileMaker.close();
    return zipPostProcessor.processFile(id, zipFileName, zipFile, expire);
  }

  @Override
  public void close() throws IOException {
    if (!zipFileMaker.isClosed()) {
      zipFileMaker.close();
    }
    if (!keepFiles) {
      FileUtil.removeDirectoryRecursively(zipFile.getParentFile().toPath());
    }
  }
}
