/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import nl.overheid.aerius.shared.Constants;

/**
 * Utility class to add in everything Locale-related.
 */
public final class LocaleUtils {
  // For some insane reason, Locale does not maintain the all-important dutch language among its defaults.
  public static final Locale DUTCH = new Locale(Constants.LOCALE_NL);

  // Known locales that should be loaded on init and database messages validation/tests.
  public static final Locale[] KNOWN_LOCALES = new Locale[] { DUTCH, Locale.ENGLISH };

  private static String defaultLocale = "nl";

  // Not to be constructed.
  private LocaleUtils() {}

  /**
   * Get locale for given locale string.
   * @param locale The locale string to parse
   * @return The locale
   */
  public static Locale getLocale(final String locale) {
    final Locale rightLocale;
    if (locale == null || locale.isEmpty()) {
      rightLocale = getDefaultLocale();
    } else {
      final String[] split = locale.toLowerCase(Locale.ENGLISH).split("_");
      rightLocale = new Locale(split[0], split.length > 1 ? split[1] : "");
    }
    return isValidLocale(rightLocale) ? rightLocale : getDefaultLocale();
  }

  public static Locale getSupportedLocaleOrDefault(final String localeString) {
    final Locale locale = LocaleUtils.getLocale(localeString);

    for (final Locale known : KNOWN_LOCALES) {
      // Only check for language because that's all we're interested in.
      if (known.getLanguage().equals(locale.getLanguage())) {
        // Return known rather than user-given because we know exactly what's in known.
        return known;
      }
    }

    return getDefaultLocale();
  }

  public static List<Locale> getLocales() {
    return Arrays.asList(KNOWN_LOCALES);
  }

  public static boolean isValidLocale(final Locale test) {
    for (final Locale locale : KNOWN_LOCALES) {
      if (locale.equals(test)) {
        return true;
      }
    }

    return false;
  }

  public static void setDefaultLocale(final String defaultLocale) {
    LocaleUtils.defaultLocale = defaultLocale;
  }

  /**
   * Get the default locale.
   * @return The locale
   */
  public static Locale getDefaultLocale() {
    return getLocale(defaultLocale);
  }

}
