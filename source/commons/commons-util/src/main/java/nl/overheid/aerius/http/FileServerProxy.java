/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.http;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;

import nl.overheid.aerius.shared.FileServerExpireTag;
import nl.overheid.aerius.util.HttpClientManager;
import nl.overheid.aerius.util.FileProcessor;

/**
 * Wrapper to get/delete/put content and return response body as String.
 */
public final class FileServerProxy implements FileProcessor {

  private static final int TIMEOUT_SECONDS = 10;

  private final HttpClientProxy httpClientProxy;
  private final String fileServerInternalUrl;
  private final String fileServerExternalUrl;

  public FileServerProxy(final HttpClientManager httpClientManager, final String fileServerUrl) {
    this(httpClientManager, fileServerUrl, fileServerUrl, TIMEOUT_SECONDS);
  }

  public FileServerProxy(final HttpClientManager httpClientManager, final String fileServerInternalUrl, final String fileServerExternalUrl,
      final int timeoutSeconds) {
    this.fileServerInternalUrl = fileServerInternalUrl;
    this.fileServerExternalUrl = fileServerExternalUrl;
    this.httpClientProxy = new HttpClientProxy(httpClientManager, timeoutSeconds);
  }

  public String deleteRemoteContent(final String pathSegment) throws IOException, HttpException, URISyntaxException {
    return httpClientProxy.execute(new HttpDelete(appendPathSegments(fileServerInternalUrl, pathSegment)));
  }

  /**
   * Puts the content of the file as binary data, sends it as filename, to the internal file service,
   * and returns the external url to the file.
   *
   * @param pathSegment pathSegment to append to the url
   * @param filename filename to store the file under
   * @param file file to read data from and send
   * @param expire the tag to indicate how long the import file should be stored
   * @return the external url to the file
   */
  @Override
  public String processFile(final String pathSegment, final String filename, final File file, final FileServerExpireTag expire)
      throws IOException, HttpException, URISyntaxException {
    return putFile(pathSegment, filename, new FileEntity(file, ContentType.APPLICATION_OCTET_STREAM), expire);
  }

  /**
   * Puts a json object on the file server, sends it as filename, to the internal file service,
   * and returns the external url to the file.
   *
   * @param pathSegment pathSegment to append to the url
   * @param filename filename to store the file under
   * @param jsonObject jsonObject as String to send
   * @param expire the tag to indicate how long the import file should be stored
   * @return the external url to the file
   */
  public String putFile(final String pathSegment, final String filename, final String jsonObject, final FileServerExpireTag expire)
      throws IOException, HttpException, URISyntaxException {
    return putFile(pathSegment, filename, new StringEntity(jsonObject, ContentType.APPLICATION_JSON), expire);
  }

  private String putFile(final String pathSegment, final String filename, final HttpEntity httpEntity, final FileServerExpireTag expire)
      throws IOException, HttpException, URISyntaxException {
    final BasicNameValuePair expireParam = new BasicNameValuePair(FileServerExpireTag.tagKey(), FileServerExpireTag.safeTagValue(expire));
    final URI url = appendPathSegments(fileServerInternalUrl, expireParam, pathSegment, filename);
    final HttpPut putRequest = new HttpPut(url);

    putRequest.setEntity(httpEntity);
    httpClientProxy.execute(putRequest);
    return appendPathSegments(fileServerExternalUrl, pathSegment, filename).toString();
  }

  public String getContent(final String pathSegment) throws IOException, HttpException, URISyntaxException {
    return httpClientProxy.execute(new HttpGet(appendPathSegments(fileServerInternalUrl, pathSegment)));
  }

  public InputStream getContentStream(final String pathSegment) throws IOException, HttpException, URISyntaxException {
    return httpClientProxy.executeToInputStream(new HttpGet(appendPathSegments(fileServerInternalUrl, pathSegment)));
  }

  public static URI appendPathSegments(final String url, final String... additionaPathSegments) throws URISyntaxException {
    return FileServerProxy.appendPathSegments(url, (NameValuePair) null, additionaPathSegments);
  }

  private static URI appendPathSegments(final String url, final NameValuePair nameValuePair, final String... additionaPathSegments)
      throws URISyntaxException {
    final URIBuilder uriBuilder = new URIBuilder(url);
    final List<String> pathSegments = new ArrayList<>(uriBuilder.getPathSegments());

    pathSegments.addAll(List.of(additionaPathSegments));
    uriBuilder.setPath(String.join("/", pathSegments));
    if (nameValuePair != null) {
      uriBuilder.setParameters(nameValuePair);
    }
    return uriBuilder.build();
  }

}
