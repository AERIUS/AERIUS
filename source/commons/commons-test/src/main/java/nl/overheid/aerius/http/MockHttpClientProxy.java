/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.http;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;

import nl.overheid.aerius.util.HttpClientManager;

/**
 * Class class to mock {@link HttpClientProxy}.
 */
public final class MockHttpClientProxy {

  private static final HttpUriRequestCaptor EMPTY_HTTP_URI_REQUEST_REFERENCE = new HttpUriRequestCaptor();
  private final HttpClientManager httpClientManager = mock(HttpClientManager.class);
  private final CloseableHttpClient httpClient = mock(CloseableHttpClient.class);
  private final HttpEntity httpEntity = mock(HttpEntity.class);
  private final StatusLine statusLine = mock(StatusLine.class);

  /**
   * Mocks a HttpClientManager to return a string when execute is called.
   *
   * @param response response to return
   * @return mocked HttpClientManager instance
   * @throws IOException
   * @throws ClientProtocolException
   */
  public HttpClientManager mockStringResponse(final String response) throws IOException {
    return mockStringResponse(response, EMPTY_HTTP_URI_REQUEST_REFERENCE);
  }

  /**
   * Mocks a HttpClientManager to return a string when execute is called, and captor to verify the http request sent.
   *
   * @param response response to return
   * @param httpUriRequestcaptor capture to verify the actual http request sent
   * @return mocked HttpClientManager instance
   * @throws IOException
   * @throws ClientProtocolException
   */
  public HttpClientManager mockStringResponse(final String response, final HttpUriRequestCaptor httpUriRequestcaptor) throws IOException {
    mockClientManager(HttpStatus.SC_OK, httpUriRequestcaptor);
    final InputStream fileCodeStream = new ByteArrayInputStream(response.getBytes());
    doReturn(fileCodeStream).when(httpEntity).getContent();
    return httpClientManager;
  }

  /**
   * Mocks a HttpClientManager to respond with http status 200, and writes file contents to the captured fileOutputStream
   *
   * @param file the file to copy
   * @return a mocked HttpClientManager instance
   * @throws IOException
   */
  public HttpClientManager mockFileResponse(final Path file) throws IOException {
    mockClientManager(HttpStatus.SC_OK, EMPTY_HTTP_URI_REQUEST_REFERENCE);
    doAnswer(invocation -> {
      final FileOutputStream outputStream = invocation.getArgument(0, FileOutputStream.class);
      try (final InputStream inputStream = new FileInputStream(file.toFile())) {
        inputStream.transferTo(outputStream);
      }
      return null;
    }).when(httpEntity).writeTo(any());

    return httpClientManager;
  }

  /**
   * Mocks a HttpClientManager to return a 404 response.
   *
   * @return mocked HttpClientManager instance
   * @throws IOException
   */
  public HttpClientManager mockHttpException() throws IOException {
    return mockClientManager(HttpStatus.SC_NOT_FOUND, EMPTY_HTTP_URI_REQUEST_REFERENCE);
  }

  private HttpClientManager mockClientManager(final int httpStatus, final HttpUriRequestCaptor captor) throws IOException {
    lenient().doReturn(httpClient).when(httpClientManager).getHttpClient(anyInt());
    lenient().doReturn(httpClient).when(httpClientManager).getRetryingHttpClient(anyInt());

    final CloseableHttpResponse httpResponse = mock(CloseableHttpResponse.class);
    when(httpClient.execute(any())).thenAnswer(invocation -> {
      captor.set(invocation.getArgument(0));
      return httpResponse;
    });
    doReturn(statusLine).when(httpResponse).getStatusLine();
    doReturn(httpEntity).when(httpResponse).getEntity();

    doReturn(httpStatus).when(statusLine).getStatusCode();
    return httpClientManager;
  }

  public CloseableHttpClient getHttpClient() {
    return httpClient;
  }

  public HttpClientManager getHttpClientManagerMock() {
    return httpClientManager;
  }

  public static class HttpUriRequestCaptor {

    private final List<HttpUriRequest> httpUriRequests = new ArrayList<>();

    public HttpUriRequest get() {
      return httpUriRequests.get(0);
    }

    public HttpUriRequest get(final int idx) {
      return httpUriRequests.get(idx);
    }

    public void set(final HttpUriRequest httpUriRequest) {
      httpUriRequests.add(httpUriRequest);
    }

  }
}
