/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared;

/**
 * Constants shared by the client and server.
 *
 * Contained in a concrete class to allow for static imports
 */
public final class Constants {

  /**
   * Locale The Netherlands.
   */
  public static final String LOCALE_NL = "nl";

  /**
   * The locale to use when nothing is specified. Fallback locale if you will.
   */
  public static final String DEFAULT_LOCALE = LOCALE_NL;

  /**
   * The querystring key to store the locale in.
   */
  public static final String LANGUAGE_PARAMETER = "locale";

  /**
   * Regex used to a correct emailaddress.
   */
  public static final String VALID_EMAIL_ADDRESS_REGEX =
      "^[\\w\\u00C0-\\u02AF-\\+]+(\\.[\\w\\u00C0-\\u02AF-]+)*@[\\w\\u00C0-\\u02AF-]+(\\.[\\w\\u00C0-\\u02AF]+)*(\\.[A-Za-z]{2,})$";

  /**
   * Sets the max-age for Strict-Transport-Security
   */
  public static final int HSTS_MAX_AGE = 60 * 60 * 24 * 365; // (almost) 1 year

  private Constants() {
    // Constants class.
  }
}
