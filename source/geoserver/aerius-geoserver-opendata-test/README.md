## Testing the opendata endpoints with Cypress

Running the tests of the web user interface with Cypress can be done in 2 different ways.
First run with Maven, and second with Node.js.
This page describes how to start the tests.
The tests require some parameters, and some are optional.
The following parameters are available:


| Parameter      | Description                                              | Required |
|----------------|----------------------------------------------------------|----------|
| `base_url`     | URL to the AERIUS instance you want to test              | Required |
| `username`     | Username if basic authentication is enabled              | Optional |
| `password`     | Password if basic authentication is enabled              | Optional |
| `max_duration` | Max time the test is expected to run. Default 30 minutes | Optional |

### Using Maven

#### Runs the unit tests in Docker container.

```bash
mvn test -DCYPRESS_BASE=<base_url> [-DCYPRESS_USERNAME=<username>] [-DCYPRESS_PASSWORD=<password>] [-DCYPRESS_MAX_TEST_DURATION_MINUTES=<max_duration>]
```

### Using Node.js

Execute the commands from the `src/test/e2e` directory.

#### The project is set up as a Node.js project. Install modules and library based on the data from package.json

```bash
npm install
```

#### Run Cypress

```bash
npx cypress run --config baseUrl=[base_url] --env USERNAME=[username],PASSWORD=[password]
```

#### Run Cypress interactive mode

```bash
npx cypress open --config baseUrl=[base_url] --env USERNAME=[username],PASSWORD=[password]
```

#### Show reports

```bash
node ./cypress/reports/cucumber-html-report.js
```
