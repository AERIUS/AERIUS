/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class GetFeature {
  static versionWMS = '1.1.0'
  static username = Cypress.env('USERNAME')
  static password = Cypress.env('PASSWORD')

  /**
   * Make a GetFeature request to the given endpoint
   * @param {String} endpoint - The endpoint for the request
   * @param {String} typeName - The type of layer to request
   * @param {String} outputFormat - The output format for the response of the request
   * @param {Number} maxFeatures - (optional) The type of features the response should have
   * @alias getFeatureRequest
   * @example
   * getFeature.getFeatureRequest('base_geometries/ows, base_geometries:hexagons, application/json, 10')
   */
  static getFeatureRequest(endpoint: string, typeName: string, outputFormat: string, maxFeatures = 1) {
    var queryString: any = {
      service: 'WFS',
      version: this.versionWMS,
      request: 'GetFeature',
      typeName: typeName,
      maxFeatures: maxFeatures,
      outputFormat: outputFormat
   }
   this.featureRequest(endpoint, queryString)
  }

  /**
   * Make a GetFeature request to the given endpoint with the given filter
   * @param {String} endpoint - The endpoint for the request
   * @param {String} typeName - The type of layer to request
   * @param {String} outputFormat - The output format for the response of the request
   * @param {String} filter - The cql filter for the request
   * @param {Number} maxFeatures - (optional) The type of features the response should have
   * @alias getFeatureRequest
   * @example
   * getFeature.getFeatureRequestFilter('base_geometries/ows, base_geometries:hexagons, application/json, natura2000_area_id=65')
   */
  static getFeatureRequestFilter(endpoint: string, typeName: string, outputFormat: string, filter: string, maxFeatures = 1) {
   var queryString: any = {
      service: 'WFS',
      version: this.versionWMS,
      request: 'GetFeature',
      typeName: typeName,
      maxFeatures: maxFeatures,
      outputFormat: outputFormat,
      cql_filter: filter
   }
   this.featureRequest(endpoint, queryString)
  }

  /**
   * Make a Feature request to the given endpoint with the given query string
   * @param {String} endpoint - The endpoint for the request
   * @param {String} queryString - The query object for the request
   * @alias featureRequest
   */
  static featureRequest(endpoint: string, queryString: any) {
    if (!this.isEmpty(this.username) && !this.isEmpty(this.password)) {
      // First do a request that can follow Cognito's redirect (if needed)
      // Might result in a 200 or 404 or whatever, but in this case we don't care, we only want to put in the username/password if redirected.
      cy.request({
        method: 'GET',
        url: endpoint,
        followRedirect: false,
        failOnStatusCode: false
      }).then((response: any) => {
        cy.log(response.status);
        if (response.status == 302) {
          cy.log("got a 302 redirect to coginito with redirect url");
          const redirectUrl = response.redirectedToUrl;
          cy.visit(redirectUrl);
          cy.get("body").then((body: any) => {
            if (body.find("input[name=username]:visible").length > 0) {
              cy.get("input[name=username]:visible").type(this.username);
              cy.get("input[name=password]:visible").type(this.password);
              cy.get("input[name=signInSubmitButton]:visible").click();
            } else {
              cy.log("cognito user credentials still in cache");
            }
          });
        }
      });
    }
    // The actual call, including query string.
    cy.request({
      method: 'GET',
      url: endpoint,
      qs: queryString,
      timeout: 60000
    }).as('getFeatureRequest');
  }

  /**
   * Is string empty
   * @param {*} str
   * @returns
   */
  static isEmpty(str: string) {
    return !str || str.length === 0;
  }

  /**
   * Make a GetFeature request to the given endpoint with the given filter taken from a fixture
   * @param {String} endpoint - The endpoint for the request
   * @param {String} typeName - The type of layer to request
   * @param {String} outputFormat - The output format for the response of the request
   * @param {String} filter - The cql filter for the request taken from a fixture file
   * @param {Number} maxFeatures - (optional) The type of features the response should have
   * @alias getFeatureRequest
   * @example
   * getFeature.getFeatureRequestFilter('base_geometries/ows, base_geometries:hexagons, application/json, natura2000_area_id=65')
   */
   static getFeatureRequestFixtureFilter(endpoint: string, typeName: string, outputFormat: string, filter: string, maxFeatures = 1) {
    cy.fixture(filter).as('filterFixture')
    cy.get('@filterFixture').then((filterFixture: any) => {
      this.getFeatureRequestFilter(endpoint, typeName, outputFormat, filterFixture, maxFeatures = 1)
    })
  }

}
