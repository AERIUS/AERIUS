/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";

import { GetFeature } from '../../api/GetFeature';

const endpoint = 'depositions/wfs'

Given(`I make a GetFeature request to get {string} in {string} with filter {string}`, (typeName: string, outputFormat: string, filter: string) => {
  GetFeature.getFeatureRequestFilter(endpoint, typeName, outputFormat, filter)
})

Given(`I make a GetFeature request to get {string} in {string} with filter fixture {string}`, (typeName: string, outputFormat: string, filter: string) => {
  GetFeature.getFeatureRequestFixtureFilter(endpoint, typeName, outputFormat, filter)
})

Given(`I make a GetFeature request to get {string} in {string}`, (typeName: string, outputFormat: string) => {
  GetFeature.getFeatureRequest(endpoint, typeName, outputFormat)
})
