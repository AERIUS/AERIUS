#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: AERIUS open data, test the base_geometries sources in different output formats

  Scenario: Validate the open data source base_geometries:extra_assessment_hexagons_to_habitats in application/json format
    Given I make a GetFeature request to get 'base_geometries:extra_assessment_hexagons_to_habitats' in 'application/json' with filter 'receptor_id=3265093'
    Then the GetFeature response status code is 200
    And the GetFeature JSON response contains the key 'type' with type 'string'
    And the GetFeature JSON response contains the key 'totalFeatures' with type 'number'
    And the GetFeature JSON response contains the key 'features' with type 'array'
    And the GetFeature JSON response contains the key 'crs' with type 'object'
    And the GetFeature JSON response contains the key 'bbox' with type 'array'
    And the GetFeature response properties are equal to 'json/base_geometries-extra_assessment_hexagons_to_habitats.json'
    And the GetFeature response properties contains the key 'receptor_id' with type 'number'
    And the GetFeature JSON response features column count to be 6

  Scenario: Validate the open data source base_geometries:extra_assessment_hexagons_to_habitats in GML2 format
    Given I make a GetFeature request to get 'base_geometries:extra_assessment_hexagons_to_habitats' in 'GML2' with filter 'receptor_id=3265093'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<gml:featureMember>'
    And the GetFeature response contains '<base_geometries:receptor_id>3265093</base_geometries:receptor_id>'
    And the GetFeature response contains '<base_geometries:zoom_level>'
    And the GetFeature response contains '<base_geometries:natura2000_area_id>'
    And the GetFeature response contains '<base_geometries:natura2000_area_name>'
    And the GetFeature response contains '<base_geometries:habitat_type_id>'
    And the GetFeature response contains '<base_geometries:habitat_type_name>'
    And the GetFeature response contains '<base_geometries:habitat_type_description>'
    And the GetFeature response contains '<base_geometries:critical_deposition>'
    And the GetFeature response contains '<base_geometries:geometry>'

  Scenario: Validate the open data source base_geometries:extra_assessment_hexagons_to_habitats in gml3 format
    Given I make a GetFeature request to get 'base_geometries:extra_assessment_hexagons_to_habitats' in 'gml3' with filter 'receptor_id=3265093'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<gml:featureMembers>'
    And the GetFeature response contains '<base_geometries:receptor_id>3265093</base_geometries:receptor_id>'
    And the GetFeature response contains '<base_geometries:zoom_level>'
    And the GetFeature response contains '<base_geometries:natura2000_area_id>'
    And the GetFeature response contains '<base_geometries:natura2000_area_name>'
    And the GetFeature response contains '<base_geometries:habitat_type_id>'
    And the GetFeature response contains '<base_geometries:habitat_type_name>'
    And the GetFeature response contains '<base_geometries:habitat_type_description>'
    And the GetFeature response contains '<base_geometries:critical_deposition>'
    And the GetFeature response contains '<base_geometries:geometry>'

  Scenario: Validate the open data source base_geometries:extra_assessment_hexagons_to_habitats in gml32 format
    Given I make a GetFeature request to get 'base_geometries:extra_assessment_hexagons_to_habitats' in 'gml32' with filter 'receptor_id=3265093'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<wfs:member>'
    And the GetFeature response contains '<base_geometries:receptor_id>3265093</base_geometries:receptor_id>'
    And the GetFeature response contains '<base_geometries:zoom_level>'
    And the GetFeature response contains '<base_geometries:natura2000_area_id>'
    And the GetFeature response contains '<base_geometries:natura2000_area_name>'
    And the GetFeature response contains '<base_geometries:habitat_type_id>'
    And the GetFeature response contains '<base_geometries:habitat_type_name>'
    And the GetFeature response contains '<base_geometries:habitat_type_description>'
    And the GetFeature response contains '<base_geometries:critical_deposition>'
    And the GetFeature response contains '<base_geometries:geometry>'

  Scenario: Validate the open data source base_geometries:extra_assessment_hexagons_to_habitats in KML format
    Given I make a GetFeature request to get 'base_geometries:extra_assessment_hexagons_to_habitats' in 'KML' with filter 'receptor_id=3265093'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<SchemaData schemaUrl="#extra_assessment_hexagons_to_habitats_1">'
    And the GetFeature response contains '<SimpleData name="receptor_id">3265093</SimpleData>'
    And the GetFeature response contains '<SimpleData name="zoom_level">'
    And the GetFeature response contains '<SimpleData name="natura2000_area_id">'
    And the GetFeature response contains '<SimpleData name="natura2000_area_name">'
    And the GetFeature response contains '<SimpleData name="habitat_type_id">'
    And the GetFeature response contains '<SimpleData name="habitat_type_name">'
    And the GetFeature response contains '<SimpleData name="habitat_type_description">'
    And the GetFeature response contains '<SimpleData name="critical_deposition">'
    And the GetFeature response contains '<Polygon>'

  Scenario: Validate the open data source base_geometries:extra_assessment_hexagons_to_habitats in CSV format
    Given I make a GetFeature request to get 'base_geometries:extra_assessment_hexagons_to_habitats' in 'CSV' with filter 'receptor_id=3265093'
    Then the GetFeature response status code is 200
    And the GetFeature response contains 'FID,receptor_id,zoom_level,natura2000_area_id,natura2000_area_name,habitat_type_id,habitat_type_name,habitat_type_description,critical_deposition,geometry'

  Scenario: Validate the open data source base_geometries:extra_assessment_hexagons_to_habitats in GML2 format for zoom_level 1
    Given I make a GetFeature request to get 'base_geometries:extra_assessment_hexagons_to_habitats' in 'GML2' with filter 'zoom_level=1'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<gml:featureMember>'
    And the GetFeature response contains '<base_geometries:receptor_id>2827720</base_geometries:receptor_id>'
    And the GetFeature response contains '<base_geometries:zoom_level>1</base_geometries:zoom_level>'
    And the GetFeature response contains '<base_geometries:natura2000_area_id>'
    And the GetFeature response contains '<base_geometries:natura2000_area_name>'
    And the GetFeature response contains '<base_geometries:habitat_type_id>'
    And the GetFeature response contains '<base_geometries:habitat_type_name>'
    And the GetFeature response contains '<base_geometries:habitat_type_description>'
    And the GetFeature response contains '<base_geometries:critical_deposition>'

  Scenario: Validate the open data source base_geometries:hexagons extra assessment in application/json format
    Given I make a GetFeature request to get 'base_geometries:hexagons' in 'application/json' with filter 'receptor_id=4277497'
    Then the GetFeature response status code is 200
    And the GetFeature JSON response contains the key 'type' with type 'string'
    And the GetFeature JSON response contains the key 'totalFeatures' with type 'number'
    And the GetFeature JSON response contains the key 'features' with type 'array'
    And the GetFeature JSON response contains the key 'crs' with type 'object'
    And the GetFeature JSON response contains the key 'bbox' with type 'array'
    And the GetFeature response properties are equal to 'json/base_geometries-hexagons_extra_assessment.json'
    And the GetFeature response properties contains the key 'receptor_id' with type 'number'
    And the GetFeature JSON response features column count to be 6

  Scenario: Validate the open data source base_geometries:hexagons in GML2 format
    Given I make a GetFeature request to get 'base_geometries:hexagons' in 'GML2' with filter 'receptor_id=291553'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<gml:featureMember>'
    And the GetFeature response contains '</base_geometries:hexagons>'
    And the GetFeature response contains '<base_geometries:receptor_id>291553</base_geometries:receptor_id>'
    And the GetFeature response contains '<base_geometries:zoom_level>'
    And the GetFeature response contains '<base_geometries:relevant>'
    And the GetFeature response contains '<base_geometries:exceeding>'
    And the GetFeature response contains '<base_geometries:above_cl>'
    And the GetFeature response contains '<base_geometries:geometry>'
    And the GetFeature response contains '<base_geometries:extra_assessment>'
    
  Scenario: Validate the open data source base_geometries:hexagons in gml3 format
    Given I make a GetFeature request to get 'base_geometries:hexagons' in 'gml3' with filter 'receptor_id=291553'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<gml:featureMembers>'
    And the GetFeature response contains '<base_geometries:receptor_id>291553</base_geometries:receptor_id>'
    And the GetFeature response contains '<base_geometries:zoom_level>'
    And the GetFeature response contains '<base_geometries:relevant>'
    And the GetFeature response contains '<base_geometries:exceeding>'
    And the GetFeature response contains '<base_geometries:above_cl>'
    And the GetFeature response contains '<base_geometries:geometry>'
    And the GetFeature response contains '<base_geometries:extra_assessment>'

  Scenario: Validate the open data source base_geometries:hexagons in gml32 format
    Given I make a GetFeature request to get 'base_geometries:hexagons' in 'gml32' with filter 'receptor_id=291553'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<wfs:member>'
    And the GetFeature response contains '</base_geometries:hexagons>'
    And the GetFeature response contains '<base_geometries:receptor_id>291553</base_geometries:receptor_id>'
    And the GetFeature response contains '<base_geometries:zoom_level>'
    And the GetFeature response contains '<base_geometries:relevant>'
    And the GetFeature response contains '<base_geometries:exceeding>'
    And the GetFeature response contains '<base_geometries:above_cl>'
    And the GetFeature response contains '<base_geometries:geometry>'
    And the GetFeature response contains '<base_geometries:extra_assessment>'

  Scenario: Validate the open data source base_geometries:hexagons in KML format
    Given I make a GetFeature request to get 'base_geometries:hexagons' in 'KML' with filter 'receptor_id=291553'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<SchemaData schemaUrl="#hexagons_1">'
    And the GetFeature response contains '<SimpleData name="receptor_id">291553</SimpleData>'
    And the GetFeature response contains '<SimpleData name="zoom_level">'
    And the GetFeature response contains '<SimpleData name="relevant">'
    And the GetFeature response contains '<SimpleData name="exceeding">'
    And the GetFeature response contains '<SimpleData name="above_cl">'
    And the GetFeature response contains '<SimpleData name="extra_assessment">'
    And the GetFeature response contains '<Polygon>'

  Scenario: Validate the open data source base_geometries:hexagons in CSV format
    Given I make a GetFeature request to get 'base_geometries:hexagons' in 'CSV' with filter 'receptor_id=291553'
    Then the GetFeature response status code is 200
    And the GetFeature response contains 'FID,receptor_id,zoom_level,relevant,exceeding,above_cl,extra_assessment,critical_deposition,geometry'

  Scenario: Validate the open data source base_geometries:hexagons in GML2 format for zoom_level 1
    Given I make a GetFeature request to get 'base_geometries:hexagons' in 'GML2' with filter 'zoom_level=1'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<gml:featureMember>'
    And the GetFeature response contains '</base_geometries:hexagons>'
    And the GetFeature response contains '<base_geometries:receptor_id>'
    And the GetFeature response contains '<base_geometries:zoom_level>1</base_geometries:zoom_level>'
    And the GetFeature response contains '<base_geometries:relevant>'
    And the GetFeature response contains '<base_geometries:geometry>'
    And the GetFeature response contains '<base_geometries:extra_assessment>'

  Scenario: Validate the open data source base_geometries:hexagons in GML2 format for zoom_level 2
    Given I make a GetFeature request to get 'base_geometries:hexagons' in 'GML2' with filter 'zoom_level=2'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<gml:featureMember>'
    And the GetFeature response contains '</base_geometries:hexagons>'
    And the GetFeature response contains '<base_geometries:receptor_id>'
    And the GetFeature response contains '<base_geometries:zoom_level>2</base_geometries:zoom_level>'
    And the GetFeature response contains '<base_geometries:geometry>'
    And the GetFeature response does not contains '<base_geometries:extra_assessment>'

  Scenario: Validate the open data source base_geometries:hexagons_to_relevant_habitats in application/json format
    Given I make a GetFeature request to get 'base_geometries:hexagons_to_relevant_habitats' in 'application/json' with filter 'natura2000_area_id=10'
    Then the GetFeature response status code is 200
    And the GetFeature JSON response contains the key 'type' with type 'string'
    And the GetFeature JSON response contains the key 'totalFeatures' with type 'number'
    And the GetFeature JSON response contains the key 'features' with type 'array'
    And the GetFeature JSON response contains the key 'crs' with type 'object'
    And the GetFeature JSON response contains the key 'bbox' with type 'array'
    And the GetFeature response properties are equal to 'json/base_geometries-hexagons_to_relevant_habitats.json'
    And the GetFeature response properties contains the key 'receptor_id' with type 'number'
    And the GetFeature response properties contains the key 'zoom_level' with type 'number'
    And the GetFeature response properties contains the key 'receptor_id' with type 'number'
    And the GetFeature response properties contains the key 'habitat_type_id' with type 'number'
    And the GetFeature response properties contains the key 'habitat_type_name' with type 'string'
    And the GetFeature response properties contains the key 'habitat_type_description' with type 'string'
    And the GetFeature response properties contains the key 'surface' with type 'number'
    And the GetFeature response properties contains the key 'coverage' with type 'number'
    And the GetFeature JSON response features column count to be 6

  Scenario: Validate the open data source base_geometries:hexagons_to_relevant_habitats in GML2 format
    Given I make a GetFeature request to get 'base_geometries:hexagons_to_relevant_habitats' in 'GML2' with filter 'natura2000_area_id=10'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<gml:featureMember>'
    And the GetFeature response contains '</base_geometries:hexagons_to_relevant_habitats>'
    And the GetFeature response contains '<base_geometries:receptor_id>'
    And the GetFeature response contains '<base_geometries:zoom_level>1</base_geometries:zoom_level>'
    And the GetFeature response contains '<base_geometries:natura2000_area_id>10</base_geometries:natura2000_area_id>'
    And the GetFeature response contains '<base_geometries:natura2000_area_name>Oudegaasterbrekken, Fluessen en omgeving</base_geometries:natura2000_area_name>'
    And the GetFeature response contains '<base_geometries:habitat_type_id>'
    And the GetFeature response contains '<base_geometries:habitat_type_name>'
    And the GetFeature response contains '<base_geometries:habitat_type_description>'
    And the GetFeature response contains '<base_geometries:critical_deposition>'
    And the GetFeature response contains '<base_geometries:surface>'
    And the GetFeature response contains '<base_geometries:coverage>'
    And the GetFeature response contains '<base_geometries:geometry>'

  Scenario: Validate the open data source base_geometries:hexagons_to_relevant_habitats in gml3 format
    Given I make a GetFeature request to get 'base_geometries:hexagons_to_relevant_habitats' in 'gml3' with filter 'natura2000_area_id=10'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<gml:featureMembers>'
    And the GetFeature response contains '</base_geometries:hexagons_to_relevant_habitats>'
    And the GetFeature response contains '<base_geometries:receptor_id>'
    And the GetFeature response contains '<base_geometries:zoom_level>1</base_geometries:zoom_level>'
    And the GetFeature response contains '<base_geometries:natura2000_area_id>10</base_geometries:natura2000_area_id>'
    And the GetFeature response contains '<base_geometries:natura2000_area_name>Oudegaasterbrekken, Fluessen en omgeving</base_geometries:natura2000_area_name>'
    And the GetFeature response contains '<base_geometries:habitat_type_id>'
    And the GetFeature response contains '<base_geometries:habitat_type_name>'
    And the GetFeature response contains '<base_geometries:habitat_type_description>'
    And the GetFeature response contains '<base_geometries:critical_deposition>'
    And the GetFeature response contains '<base_geometries:surface>'
    And the GetFeature response contains '<base_geometries:coverage>'
    And the GetFeature response contains '<base_geometries:geometry>'

  Scenario: Validate the open data source base_geometries:hexagons_to_relevant_habitats in gml32 format
    Given I make a GetFeature request to get 'base_geometries:hexagons_to_relevant_habitats' in 'gml32' with filter 'natura2000_area_id=10'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<wfs:member>'
    And the GetFeature response contains '</base_geometries:hexagons_to_relevant_habitats>'
    And the GetFeature response contains '<base_geometries:receptor_id>'
    And the GetFeature response contains '<base_geometries:zoom_level>1</base_geometries:zoom_level>'
    And the GetFeature response contains '<base_geometries:natura2000_area_id>10</base_geometries:natura2000_area_id>'
    And the GetFeature response contains '<base_geometries:natura2000_area_name>Oudegaasterbrekken, Fluessen en omgeving</base_geometries:natura2000_area_name>'
    And the GetFeature response contains '<base_geometries:habitat_type_id>'
    And the GetFeature response contains '<base_geometries:habitat_type_name>'
    And the GetFeature response contains '<base_geometries:habitat_type_description>'
    And the GetFeature response contains '<base_geometries:critical_deposition>'
    And the GetFeature response contains '<base_geometries:surface>'
    And the GetFeature response contains '<base_geometries:coverage>'
    And the GetFeature response contains '<base_geometries:geometry>'

  Scenario: Validate the open data source base_geometries:hexagons_to_relevant_habitats in KML format
    Given I make a GetFeature request to get 'base_geometries:hexagons_to_relevant_habitats' in 'KML' with filter 'natura2000_area_id=10'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<SchemaData schemaUrl="#hexagons_to_relevant_habitats_1">'
    And the GetFeature response contains '<SimpleData name="receptor_id">'
    And the GetFeature response contains '<SimpleData name="zoom_level">1</SimpleData>'
    And the GetFeature response contains '<SimpleData name="natura2000_area_id">10</SimpleData>'
    And the GetFeature response contains '<SimpleData name="natura2000_area_name">Oudegaasterbrekken, Fluessen en omgeving</SimpleData>'
    And the GetFeature response contains '<SimpleData name="habitat_type_id">'
    And the GetFeature response contains '<SimpleData name="habitat_type_name">'
    And the GetFeature response contains '<SimpleData name="habitat_type_description">'
    And the GetFeature response contains '<SimpleData name="critical_deposition">'
    And the GetFeature response contains '<SimpleData name="surface">'
    And the GetFeature response contains '<SimpleData name="coverage">'
    And the GetFeature response contains '<Polygon>'
    
  Scenario: Validate the open data source base_geometries:hexagons_to_relevant_habitats in CSV format
    Given I make a GetFeature request to get 'base_geometries:hexagons_to_relevant_habitats' in 'CSV' with filter 'natura2000_area_id=10'
    Then the GetFeature response status code is 200
    And the GetFeature response contains 'FID,receptor_id,zoom_level,natura2000_area_id,natura2000_area_name,habitat_type_id,habitat_type_name,habitat_type_description,critical_deposition,surface,coverage,geometry'

  Scenario: Validate the open data source base_geometries:relevant_habitats in application/json format
    Given I make a GetFeature request to get 'base_geometries:relevant_habitats' in 'application/json' with filter 'natura2000_area_id=65'
    Then the GetFeature response status code is 200
    And the GetFeature JSON response contains the key 'type' with type 'string'
    And the GetFeature JSON response contains the key 'totalFeatures' with type 'number'
    And the GetFeature JSON response contains the key 'features' with type 'array'
    And the GetFeature JSON response contains the key 'crs' with type 'object'
    And the GetFeature JSON response contains the key 'bbox' with type 'array'
    And the GetFeature response properties are equal to 'json/base_geometries-relevant_habitats.json'
    And the GetFeature response properties contains the key 'habitat_type_id' with type 'number'
    And the GetFeature response properties contains the key 'habitat_type_name' with type 'string'
    And the GetFeature response properties contains the key 'habitat_type_description' with type 'string'
    And the GetFeature JSON response features column count to be 6

  Scenario: Validate the open data source base_geometries:relevant_habitats in GML2 format
    Given I make a GetFeature request to get 'base_geometries:relevant_habitats' in 'GML2' with filter 'natura2000_area_id=65'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<gml:featureMember>'
    And the GetFeature response contains '</base_geometries:relevant_habitats>'
    And the GetFeature response contains '<base_geometries:natura2000_area_id>65</base_geometries:natura2000_area_id>'
    And the GetFeature response contains '<base_geometries:natura2000_area_name>Binnenveld</base_geometries:natura2000_area_name>'
    And the GetFeature response contains '<base_geometries:habitat_type_id>'
    And the GetFeature response contains '<base_geometries:habitat_type_name>'
    And the GetFeature response contains '<base_geometries:habitat_type_description>'
    And the GetFeature response contains '<base_geometries:critical_deposition>'
    And the GetFeature response contains '<base_geometries:coverage>'
    And the GetFeature response contains '<base_geometries:geometry>'

  Scenario: Validate the open data source base_geometries:relevant_habitats in gml3 format
    Given I make a GetFeature request to get 'base_geometries:relevant_habitats' in 'gml3' with filter 'natura2000_area_id=65'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '</base_geometries:relevant_habitats>'
    And the GetFeature response contains '<base_geometries:natura2000_area_id>65</base_geometries:natura2000_area_id>'
    And the GetFeature response contains '<base_geometries:natura2000_area_name>Binnenveld</base_geometries:natura2000_area_name>'
    And the GetFeature response contains '<base_geometries:habitat_type_id>'
    And the GetFeature response contains '<base_geometries:habitat_type_name>'
    And the GetFeature response contains '<base_geometries:habitat_type_description>'
    And the GetFeature response contains '<base_geometries:critical_deposition>'
    And the GetFeature response contains '<base_geometries:coverage>'
    And the GetFeature response contains '<base_geometries:geometry>'

  Scenario: Validate the open data source base_geometries:relevant_habitats in gml32 format
    Given I make a GetFeature request to get 'base_geometries:relevant_habitats' in 'gml32' with filter 'natura2000_area_id=65'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<wfs:member>'
    And the GetFeature response contains '</base_geometries:relevant_habitats>'
    And the GetFeature response contains '<base_geometries:natura2000_area_id>65</base_geometries:natura2000_area_id>'
    And the GetFeature response contains '<base_geometries:natura2000_area_name>Binnenveld</base_geometries:natura2000_area_name>'
    And the GetFeature response contains '<base_geometries:habitat_type_id>'
    And the GetFeature response contains '<base_geometries:habitat_type_name>'
    And the GetFeature response contains '<base_geometries:habitat_type_description>'
    And the GetFeature response contains '<base_geometries:critical_deposition>'
    And the GetFeature response contains '<base_geometries:coverage>'
    And the GetFeature response contains '<base_geometries:geometry>'

  Scenario: Validate the open data source base_geometries:relevant_habitats in KML format
    Given I make a GetFeature request to get 'base_geometries:relevant_habitats' in 'KML' with filter 'natura2000_area_id=65'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<Schema name="relevant_habitats_1" id="relevant_habitats_1">'
    And the GetFeature response contains '<SimpleField type="string" name="natura2000_area_name"/>'
    And the GetFeature response contains '<SimpleField type="int" name="habitat_type_id"/>'
    And the GetFeature response contains '<SimpleField type="string" name="habitat_type_name"/>'
    And the GetFeature response contains '<SimpleField type="string" name="habitat_type_description"/>'
    And the GetFeature response contains '<SimpleField type="float" name="critical_deposition"/>'
    And the GetFeature response contains 'SimpleField type="float" name="coverage"/>'
    And the GetFeature response contains '<name>relevant_habitats</name>'
    And the GetFeature response contains '<Polygon>'

  Scenario: Validate the open data source base_geometries:relevant_habitats in CSV format
    Given I make a GetFeature request to get 'base_geometries:relevant_habitats' in 'CSV' with filter 'natura2000_area_id=65'
    Then the GetFeature response status code is 200
    And the GetFeature response contains 'FID,natura2000_area_id,natura2000_area_name,habitat_type_id,habitat_type_name,habitat_type_description,critical_deposition,coverage,geometry'

  Scenario: Validate the open data source base_geometries:terrain_properties in application/json format
    Given I make a GetFeature request to get 'base_geometries:terrain_properties' in 'application/json' with filter 'receptor_id=5650680'
    Then the GetFeature response status code is 200
    And the GetFeature response properties contains the key 'receptor_id' with type 'number'
    And the GetFeature response properties contains the key 'average_roughness' with type 'number'
    And the GetFeature response properties contains the key 'dominant_land_use' with type 'string'
    And the GetFeature response properties contains the key 'land_uses' with type 'string'
    And the GetFeature JSON response features column count to be 6

  Scenario: Validate the open data source base_geometries:terrain_properties in GML2 format
    Given I make a GetFeature request to get 'base_geometries:terrain_properties' in 'GML2' with filter 'receptor_id=5650680'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<gml:featureMember>'
    And the GetFeature response contains '</base_geometries:terrain_properties>'
    And the GetFeature response contains '<base_geometries:receptor_id>'
    And the GetFeature response contains '<base_geometries:average_roughness>'
    And the GetFeature response contains '<base_geometries:dominant_land_use>'
    And the GetFeature response contains '<base_geometries:land_uses>'
    And the GetFeature response contains '<base_geometries:geometry>'

  Scenario: Validate the open data source base_geometries:terrain_properties in gml3 format
    Given I make a GetFeature request to get 'base_geometries:terrain_properties' in 'gml3' with filter 'receptor_id=5650680'
    Then the GetFeature response status code is 200
    # deze response klopt niet !!!
    
    And the GetFeature response contains '</base_geometries:terrain_properties>'
    And the GetFeature response contains '<base_geometries:receptor_id>'
    And the GetFeature response contains '<base_geometries:average_roughness>'
    And the GetFeature response contains '<base_geometries:dominant_land_use>'
    And the GetFeature response contains '<base_geometries:land_uses>'
    And the GetFeature response contains '<base_geometries:geometry>'

  Scenario: Validate the open data source base_geometries:terrain_properties in gml32 format
    Given I make a GetFeature request to get 'base_geometries:terrain_properties' in 'gml32' with filter 'receptor_id=5650680'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<wfs:member>'
    And the GetFeature response contains '</base_geometries:terrain_properties>'
    And the GetFeature response contains '<base_geometries:receptor_id>'
    And the GetFeature response contains '<base_geometries:average_roughness>'
    And the GetFeature response contains '<base_geometries:dominant_land_use>'
    And the GetFeature response contains '<base_geometries:land_uses>'
    And the GetFeature response contains '<base_geometries:geometry>'

  Scenario: Validate the open data source base_geometries:terrain_properties in KML format
    Given I make a GetFeature request to get 'base_geometries:terrain_properties' in 'KML' with filter 'receptor_id=5650680'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<Schema name="terrain_properties_1" id="terrain_properties_1">'
    And the GetFeature response contains '<SimpleField type="int" name="receptor_id"/>'
    And the GetFeature response contains '<SimpleField type="float" name="average_roughness"/>'
    And the GetFeature response contains '<SimpleField type="string" name="dominant_land_use"/>'
    And the GetFeature response contains '<SimpleField type="string" name="land_uses"/>'
    And the GetFeature response contains '<name>terrain_properties</name>'   
    And the GetFeature response contains '<Polygon>'

  Scenario: Validate the open data source base_geometries:terrain_properties in CSV format
    Given I make a GetFeature request to get 'base_geometries:terrain_properties' in 'CSV' with filter 'receptor_id=5650680'
    Then the GetFeature response status code is 200
    And the GetFeature response contains 'FID,receptor_id,average_roughness,dominant_land_use,land_uses,geometry'
    
  Scenario: Validate the open data source base_geometries:waterway_categories in application/json format
    Given I make a GetFeature request to get 'base_geometries:waterway_categories' in 'application/json' with filter 'shipping_inland_waterway_id=1'
    Then the GetFeature response status code is 200
    And the GetFeature response properties contains the key 'shipping_inland_waterway_id' with type 'number'
    And the GetFeature response properties contains the key 'code' with type 'string'
    And the GetFeature response properties contains the key 'name' with type 'string'
    And the GetFeature response properties contains the key 'description' with type 'string'
    And the GetFeature response properties contains the key 'flowing' with type 'boolean'
    And the GetFeature JSON response features column count to be 6

  Scenario: Validate the open data source base_geometries:waterway_categories in GML2 format
    Given I make a GetFeature request to get 'base_geometries:waterway_categories' in 'GML2' with filter 'shipping_inland_waterway_id=1'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<gml:featureMember>'
    And the GetFeature response contains '</base_geometries:waterway_categories>'
    And the GetFeature response contains '<base_geometries:shipping_inland_waterway_id>'
    And the GetFeature response contains '<base_geometries:code>'
    And the GetFeature response contains '<base_geometries:name>'
    And the GetFeature response contains '<base_geometries:description>'
    And the GetFeature response contains '<base_geometries:flowing>'
    And the GetFeature response contains '<base_geometries:geometry>'

  Scenario: Validate the open data source base_geometries:waterway_categories in gml3 format
    Given I make a GetFeature request to get 'base_geometries:waterway_categories' in 'gml3' with filter 'shipping_inland_waterway_id=1'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '</base_geometries:waterway_categories>'
    And the GetFeature response contains '<base_geometries:shipping_inland_waterway_id>'
    And the GetFeature response contains '<base_geometries:code>'
    And the GetFeature response contains '<gml:name>'
    And the GetFeature response contains '<gml:description>'
    And the GetFeature response contains '<base_geometries:flowing>'
    And the GetFeature response contains '<base_geometries:geometry>'

  Scenario: Validate the open data source base_geometries:waterway_categories in gml32 format
    Given I make a GetFeature request to get 'base_geometries:waterway_categories' in 'gml32' with filter 'shipping_inland_waterway_id=1'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<wfs:member>'
    And the GetFeature response contains '</base_geometries:waterway_categories>'
    And the GetFeature response contains '<base_geometries:shipping_inland_waterway_id>'
    And the GetFeature response contains '<base_geometries:code>'
    And the GetFeature response contains '<gml:name>'
    And the GetFeature response contains '<gml:description>'
    And the GetFeature response contains '<base_geometries:flowing>'
    And the GetFeature response contains '<base_geometries:geometry>'

  Scenario: Validate the open data source base_geometries:waterway_categories in KML format
    Given I make a GetFeature request to get 'base_geometries:waterway_categories' in 'KML' with filter 'shipping_inland_waterway_id=1'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<Schema name="waterway_categories_1" id="waterway_categories_1">'
    And the GetFeature response contains '<SimpleField type="int" name="shipping_inland_waterway_id"/>'
    And the GetFeature response contains '<SimpleField type="string" name="code"/>'
    And the GetFeature response contains '<SimpleField type="string" name="name"/>'
    And the GetFeature response contains '<SimpleField type="string" name="description"/>'
    And the GetFeature response contains '<SimpleField type="bool" name="flowing"/>'
    And the GetFeature response contains '<name>waterway_categories</name>'
    And the GetFeature response contains '<LineString>'

  Scenario: Validate the open data source base_geometries:waterway_categories in CSV format
    Given I make a GetFeature request to get 'base_geometries:waterway_categories' in 'CSV' with filter 'shipping_inland_waterway_id=1'
    Then the GetFeature response status code is 200
    And the GetFeature response contains 'FID,shipping_inland_waterway_id,code,name,description,flowing,geometry'
