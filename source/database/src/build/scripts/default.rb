clear_log

check_datasources

if has_build_flag :release_candidate then
  puts "#{'*' * 20} RELEASE CANDIDATE #{'*' * 20}"
  create_database # error if exists!
else
  create_database :overwrite_existing
end

begin

  import_database_structure
  update_comments

  load_data

  generate_html_documentation
  generate_rtf_documentation
  generate_datasources_json

  if has_build_flag :quick then
    analyze_vacuum_database :analyze
  else
    analyze_vacuum_database :vacuum, :analyze
  end

  # The following flags can be set to true by the load.rb, but for performance reasons we want to execute them *after* the vacuum/analyze.
  run_unit_tests if $do_run_unit_tests
  validate_contents if $do_validate_contents
  create_summary if $do_create_summary

  dump_database :overwrite_existing unless has_build_flag :quick

ensure

  drop_database_if_exists :aggressive if $on_build_server

end
