#
# Product specific settings.
#

$product = :check_nl_stable    # The product these settings are for.

#-------------------------------------

source_path = File.dirname(__FILE__) + '/../../'
sql_path = '/main/sql/'
data_path = '/data/sql/'
config_path = '/build/config/'
settings_file = 'AeriusSettings.rb'

#-------------------------------------

$project_settings_file = File.expand_path(source_path + config_path + settings_file).fix_pathname

$common_sql_paths =
  [
    File.expand_path(source_path + sql_path + '/common/').fix_pathname,                        # /src/main/sql/common/
    File.expand_path(source_path + sql_path + '/calculator_common/').fix_pathname              # /src/main/sql/calculator-common/
  ]
$product_sql_path = File.expand_path(source_path + sql_path + '/calculator_nl/').fix_pathname  # /src/main/sql/calculator_nl/

$common_data_paths =
  [
    File.expand_path(source_path + data_path + '/common/').fix_pathname,                       # /src/data/sql/common/
    File.expand_path(source_path + data_path + '/nl-common/').fix_pathname,                    # /src/data/sql/nl-common/
    File.expand_path(source_path + data_path + '/nl-check-stable/').fix_pathname,              # /src/data/sql/nl-check-stable/
  ]
$product_data_path = File.expand_path(source_path + data_path + '/nl-check-stable/').fix_pathname    # /src/data/sql/nl-check-stable/
