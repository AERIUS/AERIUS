#
# Product specific settings.
#

$product = :integrationtests_latest    # The product these settings are for.

#-------------------------------------

source_path = File.dirname(__FILE__) + '/../../'
sql_path = '/main/sql/'
data_path = '/data/sql/'
config_path = '/build/config/'
settings_file = 'AeriusSettings.rb'

#-------------------------------------

$project_settings_file = File.expand_path(source_path + config_path + settings_file).fix_pathname

$common_sql_paths = 
  [
    File.expand_path(source_path + sql_path + '/common/').fix_pathname,                             # /src/main/sql/common/
    File.expand_path(source_path + sql_path + '/calculator_common/').fix_pathname                   # /src/main/sql/calculator-common/
  ]
# For integration tests, use calculator_nl as base for structure for now.
# If required, we could always include a integration-tests specific structure.
$product_sql_path = File.expand_path(source_path + sql_path + '/calculator_nl/').fix_pathname       # /src/main/sql/calculator-nl/

$common_data_paths =
  [
    File.expand_path(source_path + data_path + '/common/').fix_pathname,                            # /src/data/sql/common/
    File.expand_path(source_path + data_path + '/integrationtests/').fix_pathname,                  # /src/data/sql/integrationtests/
  ]
$product_data_path = File.expand_path(source_path + data_path + '/integrationtests/').fix_pathname  # /src/data/sql/integrationtests/
