/*
 * emission_result_type
 * --------------------
 * Enum type for indications what type an emission result is.
 */
CREATE TYPE emission_result_type AS ENUM
	('concentration', 'direct_concentration', 'deposition', 'exceedance_days', 'exceedance_hours', 'dry_deposition', 'wet_deposition');


/*
 * point_weight
 * ------------
 * Type definition for a (geometrical) point with a weight.
 */
CREATE TYPE point_weight AS
(
	point geometry,
	weight fraction
);


/*
 * ae_color_range_rs
 * -----------------
 * Type that specifies what the totals (for example the number of receptors, total depositon, total surface) within a color range are.
 * This type is used by the aggregate function ae_color_range.
 */
CREATE TYPE ae_color_range_rs AS (
	lower_value real,
	color text,
	total numeric
);


/*
 * color_range_type
 * ----------------
 * Enum type for color ranges.
 * Should correspond to the 'ColorRangeType' enum in the Java code.
 */
CREATE TYPE color_range_type AS ENUM
	('contribution_deposition', 'contribution_concentration', 'contribution_concentration_nox', 'contribution_concentration_nh3',
	'difference_deposition', 'difference_concentration', 'difference_concentration_nox', 'difference_concentration_nh3',
	'in_combination_deposition', 'in_combination_concentration', 'in_combination_concentration_nox', 'in_combination_concentration_nh3',
	'background_deposition', 'background_concentration_nox', 'background_concentration_nh3',
	'project_calculation_percentage_critical_load', 'in_combination_percentage_critical_load', 'archive_contribution_percentage_critical_load',
	'road_max_speed', 'road_barrier_height', 'road_barrier_porosity', 'road_traffic_volume',
	'road_traffic_volume_light_traffic', 'road_traffic_volume_normal_traffic',
	'road_traffic_volume_heavy_freight', 'road_traffic_volume_auto_bus', 'road_traffic_volume_car', 'road_traffic_volume_tax',
	'road_traffic_volume_lgv', 'road_traffic_volume_hgv', 'road_traffic_volume_bus', 'road_traffic_volume_mot',
	'road_emission_nox', 'road_emission_nh3', 'road_elevation_height' ,'max_total_as_percentage_critical_load',
	'habitat_areas_sensitivity_level_deposition', 'habitat_areas_sensitivity_level_concentration_nox', 'habitat_areas_sensitivity_level_concentration_nh3',
	'road_tunnelfactor');


/*
 * color_item_type
 * ---------------
 * Enum type for color items.
 * Should correspond to the 'ColorItemType' enum in the Java code.
 */
CREATE TYPE color_item_type AS ENUM
	('road_type', 'road_barrier_type', 'road_elevation_type', 'natura2000_directive');


/*
 * critical_deposition_classification
 * ----------------------------------
 * Enum type for critical load (KDW, Kritische Depositie Waarde) classifications.
 */
CREATE TYPE critical_deposition_classification AS ENUM
	('high_sensitivity', 'normal_sensitivity', 'low_sensitivity');


/*
 * theme_type
 * ----------
 * Enum type for the different themes used in the application.
 */
CREATE TYPE theme_type AS ENUM
	('nca', 'own2000', 'rbl');
