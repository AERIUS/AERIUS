/*
 * ae_color_range_sfunc
 * --------------------
 * State function for color range function 'ae_color_range'.
 * Each 'v_value' will be placed within a range.
 * Uses an array with an element for each range for the supplied color range type,
 * and sums the incoming 'v_weight' values accordingly.
 */
CREATE OR REPLACE FUNCTION ae_color_range_sfunc(state ae_color_range_rs[], v_color_range_type color_range_type, v_value numeric, v_weight numeric)
	RETURNS ae_color_range_rs[] AS
$BODY$
DECLARE
	position integer;
	rec record;
BEGIN
	IF array_length(state, 1) IS NULL THEN
		FOR rec IN SELECT lower_value, color FROM system.color_ranges WHERE color_range_type = v_color_range_type ORDER BY lower_value LOOP
			state := state || (rec.lower_value, rec.color, 0)::ae_color_range_rs;
		END LOOP;
	END IF;

	position := rank FROM (SELECT lower_value, rank() OVER (order by lower_value)
			FROM system.color_ranges WHERE color_range_type = v_color_range_type
		) AS base
		WHERE lower_value = (SELECT (ae_color_range_lower_value(v_color_range_type, v_value)).lower_value);

	IF position IS NOT NULL THEN
		state[position] := (state[position].lower_value, state[position].color::text, state[position].total + v_weight)::ae_color_range_rs;
	END IF;
	RETURN state;
END;
$BODY$
LANGUAGE plpgsql STABLE RETURNS NULL ON NULL INPUT;


/*
 * ae_color_range
 * --------------
 * Aggregate function to sum (weighted if needed) the occurrences of valus within a range.
 * The first parameter is the color type range to use.
 * The second parameter is the value to determine the correct range.
 * The third parameter is the weight which should be added to the sum of the correct range.
 * For example, the weight can be 1 to count occurrences, or a 'surface' column to sum surfaces.
 *
 * The return value is an array with as many alements as there are ranges for the supplied color range type.
 * Each element contains the lower bound and the color of the range, as well as the total sum of the weights.
 * NULL values are skipped, and if there are no non-NULL values, NULL will be returned.
 */
CREATE AGGREGATE ae_color_range(color_range_type, numeric, numeric) (
	SFUNC = ae_color_range_sfunc,
	STYPE = ae_color_range_rs[],
	INITCOND = '{}'
);


/*
 * ae_color_range_lower_value
 * --------------------------
 * Function to determine the lower bound and color of a range which contains the supplied value for the supplied color range type.
 */
CREATE OR REPLACE FUNCTION ae_color_range_lower_value(v_color_range_type color_range_type, v_value real)
	RETURNS TABLE(lower_value real, color text) AS
$BODY$
BEGIN
	RETURN QUERY SELECT color_ranges.lower_value, color_ranges.color::text
		FROM system.color_ranges
		WHERE color_range_type = v_color_range_type
			AND v_value >= color_ranges.lower_value
		ORDER BY color_ranges.lower_value DESC
		LIMIT 1;
END;
$BODY$
LANGUAGE plpgsql STABLE;
