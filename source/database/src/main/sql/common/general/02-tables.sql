/*
 * substances
 * ----------
 * Substances available within the system.
 * The ID of a substance should match the 'Substance' enum in Java, or at least not clash with those.
 */
CREATE TABLE substances (
	substance_id smallint NOT NULL,
	name text NOT NULL,
	description text,

	CONSTRAINT substances_pkey PRIMARY KEY (substance_id)
);
