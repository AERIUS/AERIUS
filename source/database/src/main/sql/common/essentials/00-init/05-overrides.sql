/*
 * ae_geometry_typmod_in
 * ---------------------
 * Override function for geometry_typmod_in, which is the type modificator function of the type geometry.
 * This function will use the default SRID (as returned by ae_get_srid) instead of SRID 0,
 * whenever no SRID is specified when defining the geometry type.
 * The default SRID is defined in the constants table.
 */
CREATE OR REPLACE FUNCTION ae_geometry_typmod_in(v_args cstring[])
  RETURNS integer AS
$BODY$
DECLARE
	v_argscopy text[];
BEGIN
	-- Work with a text[] copy, because altering the cstring[] and forwarding it to the C function fails in newer PostgreSQL versions.
	v_argscopy := v_args;

	-- Check if srid is set
	IF (v_argscopy[2] IS NULL) THEN
		v_argscopy[2] := ae_get_srid()::text;
	END IF;

	RETURN geometry_typmod_in(v_argscopy::cstring[]);
END;
$BODY$
LANGUAGE plpgsql IMMUTABLE;

/*
 * Replace the default geometry_typmod_in function by ae_geometry_typmod_in.
 */
UPDATE pg_catalog.pg_type
	SET typmodin = 'ae_geometry_typmod_in'::regproc
	WHERE typmodin = 'geometry_typmod_in'::regproc
;
