/*
 * The setup schema contains basic tables, queries and functions to create derived AERIUS tables.
 * In the setup schema, several (derived) intermediate tables are created for performance reasons.
 */
CREATE SCHEMA setup;

/*
 * system
 * ------
 * The system schema contains basic tables for the AERIUS application.
 */
CREATE SCHEMA system;

/*
 * opendata
 * --------
 * The opendata schema contains all the base views that are used by the opendata services.
 * The opendata services currently consists for a geoserver instance which serve WMS/WFS.
 *
 * The opendata services can only use views from this schema.
 */
CREATE SCHEMA opendata;

/*
 * i18n
 * ----
 * The i18n schema contains tables with translations of database items, for example sectors.
 */
CREATE SCHEMA i18n;

/*
 * users
 * -----
 * The users schema contains all the (Connect) user-related data.
 */
CREATE SCHEMA users;

/*
 * jobs
 * ----
 * The jobs schema contains all tables with dynamic data related to jobs and calculations.
 */
CREATE SCHEMA jobs;
