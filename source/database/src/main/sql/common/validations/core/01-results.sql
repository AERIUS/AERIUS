/*
 * validation_runs
 * ---------------
 * Table to track validation runs.
 * Each validation run ends up as a record in this table.
 * A validation run always correponds to a specific backend connection.
 * When running the validations in a fresh connection, a new validation-run_id should be used.
 * This is automatically done by the validation logger function (ae_perform_and_report_validation).
 */
CREATE TABLE setup.validation_runs (
	validation_run_id serial NOT NULL,
	transaction_id bigint NOT NULL,

	CONSTRAINT validation_runs_pkey PRIMARY KEY (validation_run_id)
);


/*
 * validation_results
 * ------------------
 * Table to track validation run results.
 * Each validation executed ends up as a record in this table, along with the validation result.
 */
CREATE TABLE setup.validation_results (
	validaton_result_id serial NOT NULL,
	validation_run_id integer NOT NULL,
	name regproc NOT NULL,
	result setup.validation_result_type NOT NULL,

	CONSTRAINT validation_results_pkey PRIMARY KEY (validaton_result_id),
	CONSTRAINT validation_results_fkey_validaton_runs FOREIGN KEY (validation_run_id) REFERENCES setup.validation_runs,
	CONSTRAINT validation_results_unique_combination UNIQUE (validation_run_id, name)
);


/*
 * validation_logs
 * ---------------
 * Table for saving the validaton logs.
 * Each test executed within a validation ends up as a record in this table, along with the result.
 */
CREATE TABLE setup.validation_logs (
	validation_log_id serial NOT NULL,
	validation_run_id integer NOT NULL,
	name regproc NOT NULL,
	result setup.validation_result_type NOT NULL,
	object text,
	message text NOT NULL,

	CONSTRAINT validation_logs_pkey PRIMARY KEY (validation_log_id),
	CONSTRAINT validation_logs_fkey_validaton_runs FOREIGN KEY (validation_run_id) REFERENCES setup.validation_runs
);


/*
 * last_validation_run_results_view
 * --------------------------------
 * View returning the validation results of the last validation run.
 */
CREATE OR REPLACE VIEW setup.last_validation_run_results_view AS
SELECT
	validation_run_id,
	name,
	result

	FROM setup.validation_results
		INNER JOIN (SELECT validation_run_id FROM setup.validation_runs ORDER BY validation_run_id DESC LIMIT 1) AS last_run_id USING (validation_run_id)

	ORDER BY validation_run_id, result DESC, name
;


/*
 * last_validation_logs_view
 * -------------------------
 * View returning the validation logs of the last validation run.
 */
CREATE OR REPLACE VIEW setup.last_validation_logs_view AS
SELECT
	validation_run_id,
	name,
	run_results.result AS run_result,
	logs.result AS log_result,
	object,
	message

	FROM setup.last_validation_run_results_view AS run_results
		INNER JOIN setup.validation_logs AS logs USING (validation_run_id, name)

	ORDER BY validation_run_id, run_result DESC, log_result DESC, name, object
;


/*
 * last_validation_run_view
 * ------------------------
 * View returning the validation statistics of the last validation run.
 * The result of the validations within the run are aggregated by result type.
 */
CREATE OR REPLACE VIEW setup.last_validation_run_view AS
SELECT
	validation_run_id,
	result,
	COALESCE(COUNT(name), 0) AS number_of_tests

	FROM (SELECT validation_run_id FROM setup.validation_runs ORDER BY validation_run_id DESC LIMIT 1) AS last_run_id
		CROSS JOIN (SELECT unnest(enum_range(null::setup.validation_result_type)) AS result) AS result_types
		LEFT JOIN setup.validation_results USING (validation_run_id, result)

	GROUP BY validation_run_id, result

	ORDER BY validation_run_id, result DESC
;
