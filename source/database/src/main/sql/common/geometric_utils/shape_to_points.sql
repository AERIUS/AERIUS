/*
 * ae_line_to_point_sources
 * ------------------------
 * Function to split a line into point geometries.
 * Splits the line in equally sized segments, where the segment size won't exceed the supplied maximum segment size.
 * @param geom The input line geometry.
 * Should be a linestring. If a multi-linestring is supplied, merging as a linestring will be attempted.
 * If this fails, or if another type is supplied, an exception will be raised.
 * @param maxsegmentsize Maximum segment size.
 */
CREATE OR REPLACE FUNCTION ae_line_to_point_sources(geom geometry, maxsegmentsize float)
	RETURNS SETOF geometry AS
$BODY$
DECLARE
	line geometry;
	linelength float;
	numsegments float;
	segmentsize float;
	fractionstep float;
	startfraction float;
	endfraction float;
	segment integer;
BEGIN
	line := geom;
	IF GeometryType(line) = 'MULTILINESTRING' THEN
		line := ST_LineMerge(line); -- Might not do anything if it can't merge.
	END IF;
	IF GeometryType(line) <> 'LINESTRING' THEN
		RAISE EXCEPTION 'Only LINESTRING geometry supported';
	END IF;

	linelength := ST_Length(line);
	numsegments := ceil(linelength / maxsegmentsize);
	segmentsize := linelength / numsegments;
	fractionstep := segmentsize / linelength;

	startfraction := 0.0;
	FOR segment IN 1..(numsegments::integer) LOOP
		IF segment = numsegments THEN
			endfraction := 1.0;
		ELSE
			endfraction := startfraction + fractionstep;
		END IF;
		RETURN NEXT ST_LineInterpolatePoint(ST_LineSubstring(line, startfraction, endfraction), 0.5);
		startfraction := endfraction;
	END LOOP;
	RETURN;
END;
$BODY$
LANGUAGE plpgsql IMMUTABLE;
