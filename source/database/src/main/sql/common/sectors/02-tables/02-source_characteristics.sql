/*
 * emission_diurnal_variations
 * ---------------------------
 * Table containing the different types of diurnal (temporal) variations.
 */
CREATE TABLE emission_diurnal_variations (
	emission_diurnal_variation_id integer NOT NULL,
	code text NOT NULL UNIQUE,
	name text NOT NULL UNIQUE,
	description text,

	CONSTRAINT emission_diurnal_variations_pkey PRIMARY KEY (emission_diurnal_variation_id)
);


/*
 * sector_default_source_characteristics
 * -------------------------------------
 * Table containing the standard OPS characteristics per sector.
 */
CREATE TABLE sector_default_source_characteristics (
	sector_id integer NOT NULL,
	heat_content posreal NOT NULL,
	height posreal NOT NULL,
	spread posreal NOT NULL,
	emission_diurnal_variation_id integer NOT NULL,
	particle_size_distribution integer NOT NULL,

	CONSTRAINT sector_default_source_characteristics_pkey PRIMARY KEY (sector_id),
	CONSTRAINT sector_default_source_characteristics_fkey_sectors FOREIGN KEY (sector_id) REFERENCES sectors,
	CONSTRAINT sector_default_source_characteristics_fkey_emission_diurnal_variations FOREIGN KEY (emission_diurnal_variation_id) REFERENCES emission_diurnal_variations
);


/*
 * standard_time_varying_profiles
 * ------------------------------
 * Table containing the standard time-varying profiles.
 * Currently only used by UK version.
 */
CREATE TABLE standard_time_varying_profiles (
	standard_time_varying_profile_id integer NOT NULL,
	code text NOT NULL UNIQUE,
	name text NOT NULL UNIQUE,
	description text,

	CONSTRAINT standard_time_varying_profiles_pkey PRIMARY KEY (standard_time_varying_profile_id)
);


/*
 * standard_time_varying_profile_values
 * ------------------------------------
 * Table containing the values for a standard time-varying profile.
 * Currently only used by UK version.
 */
CREATE TABLE standard_time_varying_profile_values (
	standard_time_varying_profile_id integer NOT NULL,
	value_index integer NOT NULL,
	value posreal NOT NULL,

	CONSTRAINT standard_time_varying_profile_values_pkey PRIMARY KEY (standard_time_varying_profile_id, value_index),
	CONSTRAINT standard_time_varying_profile_values_fkey_profile FOREIGN KEY (standard_time_varying_profile_id) REFERENCES standard_time_varying_profiles
);


/*
 * sector_default_time_varying_profiles
 * ------------------------------------
 * Table containing default standard time-varying profiles per sector.
 * If not present, continuous emission is assumed as the default.
 * Currently only used by UK version.
 */
CREATE TABLE sector_default_time_varying_profiles (
	sector_id integer NOT NULL,
	code text NOT NULL,

	CONSTRAINT sector_default_time_varying_profiles_pkey PRIMARY KEY (sector_id),
	CONSTRAINT sector_default_time_varying_profiles_fkey_sectors FOREIGN KEY (sector_id) REFERENCES sectors,
	CONSTRAINT sector_default_time_varying_profiles_fkey_profiles FOREIGN KEY (code) REFERENCES standard_time_varying_profiles (code)
);


/*
 * sector_year_default_time_varying_profiles
 * -----------------------------------------
 * Table containing default standard time-varying profiles per sector and year combination.
 * If not present, the sector default will be used.
 * Currently only used by UK version.
 */
CREATE TABLE sector_year_default_time_varying_profiles (
	sector_id integer NOT NULL,
	year smallint NOT NULL,
	code text NOT NULL,

	CONSTRAINT sector_year_default_time_varying_profiles_pkey PRIMARY KEY (sector_id, year),
	CONSTRAINT sector_year_default_time_varying_profiles_fkey_sectors FOREIGN KEY (sector_id) REFERENCES sectors,
	CONSTRAINT sector_year_default_time_varying_profiles_fkey_profiles FOREIGN KEY (code) REFERENCES standard_time_varying_profiles (code)
);
