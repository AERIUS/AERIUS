/*
 * sectors
 * -------
 * Table containing AERIUS sectors.
 */
CREATE TABLE sectors (
	sector_id integer NOT NULL,
	description text NOT NULL,

	CONSTRAINT sectors_pkey PRIMARY KEY (sector_id)
);
