/*
 * emission_diurnal_variations_view
 * --------------------------------
 * View returning the different types of diurnal variation.
 */
CREATE OR REPLACE VIEW emission_diurnal_variations_view AS
SELECT
	emission_diurnal_variation_id,
	code AS emission_diurnal_variation_code,
	name AS emission_diurnal_variation_name,
	description AS_diurnal_variation_description

	FROM emission_diurnal_variations
;


/*
 * default_source_characteristics_view
 * -----------------------------------
 * View returning the emission characteristics per AERIUS sector.
 */
CREATE OR REPLACE VIEW default_source_characteristics_view AS
SELECT
	sector_id,
	heat_content,
	height,
	spread,
	emission_diurnal_variation_id,
	emission_diurnal_variation_code,
	particle_size_distribution

	FROM sector_default_source_characteristics
		INNER JOIN emission_diurnal_variations_view USING (emission_diurnal_variation_id)
;
