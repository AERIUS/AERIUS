/*
 * sectorgroup
 * -----------
 * Enum type for the AERIUS sector groups
 */
CREATE TYPE system.sectorgroup AS ENUM
	('energy', 'agriculture', 'farmland', 'live_and_work', 'industry', 'mobile_equipment', 'rail_transportation',
		'aviation', 'road_transportation', 'shipping', 'other', 'road_freeway');
