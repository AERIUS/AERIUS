/*
 * sectors_sectorgroup
 * -------------------
 * System table containing the sectorgroup for each sector.
 */
CREATE TABLE system.sectors_sectorgroup (
	sector_id integer NOT NULL,
	sectorgroup system.sectorgroup NOT NULL,

	CONSTRAINT sectors_sectorgroup_pkey PRIMARY KEY (sector_id),
	CONSTRAINT sectors_sectorgroup_fkey_sectors FOREIGN KEY (sector_id) REFERENCES sectors
);


/*
 * sector_calculation_properties
 * -----------------------------
 * System table containing the calculation properties, like emission calculation method and calculation engine per subsource.
 * If a sector is not present in this table, it can't be calculated.
 */
CREATE TABLE system.sector_calculation_properties (
	sector_id integer NOT NULL,
	emission_calculation_method text NOT NULL,
	calculation_engine text NOT NULL,

	CONSTRAINT sector_calculation_properties_pkey PRIMARY KEY (sector_id),
	CONSTRAINT sector_calculation_properties_fkey_sectors FOREIGN KEY (sector_id) REFERENCES sectors
);
