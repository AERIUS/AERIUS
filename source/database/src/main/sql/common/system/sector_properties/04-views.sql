/*
 * sector_properties_view
 * ----------------------
 * View returning all system properties for a sector.
 */
CREATE OR REPLACE VIEW system.sector_properties_view AS
SELECT
	sector_id,
	sectorgroup,
	emission_calculation_method,
	calculation_engine

	FROM system.sectors_sectorgroup
		INNER JOIN system.sector_calculation_properties USING (sector_id)
;
