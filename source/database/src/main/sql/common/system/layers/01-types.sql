/*
 * layer_legend_type
 * -----------------
 * Type used to indicate what sort of type a legend is.
 */
CREATE TYPE system.layer_legend_type AS ENUM
	('circle', 'hexagon', 'text');

/*
 * layer_type
 * ----------
 * Type to indicate what sort of type a layer is.
 */
CREATE TYPE system.layer_type AS ENUM
	('wms', 'wmts', 'bing');

/*
 * dynamic_wms_layer_type
 * ----------------------
 * Type to indicate what sort of dynamic layer a WMS layer is.
 */
CREATE TYPE system.dynamic_wms_layer_type AS ENUM
	('habitat_type');
