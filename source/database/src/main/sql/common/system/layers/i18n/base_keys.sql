/*
 * layers
 * ------
 * Table containing i18n localization for wms_layers, wmts_layers and layer_bundles.
 */
CREATE TABLE i18n.layers (
	layer_id integer NOT NULL,
	language_code i18n.language_code_type NOT NULL,
	description text NOT NULL,

	CONSTRAINT layers_pkey PRIMARY KEY (layer_id, language_code)
);
