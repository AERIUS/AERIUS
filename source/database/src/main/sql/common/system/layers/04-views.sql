/*
 * layers_view
 * -----------
 * View returning all layers with their properties.
 * Uses NULL for properties that are not of importance for the type of the layer.
 */
CREATE OR REPLACE VIEW system.layers_view AS
SELECT
	layer_id,
	layer_properties_id,
	layer_type,
	name,
	title,
	attribution,
	opacity,
	layer_legend_id,
	min_scale,
	max_scale,

	NULL AS image_type,
	NULL AS service_version,
	NULL AS tile_matrix_set,
	NULL AS projection,

	url,
	bundle_name,
	begin_year,
	end_year,
	dynamic_type,
	tile_size,

	NULL AS api_key

	FROM system.layers
		INNER JOIN system.wms_layer_properties USING (layer_properties_id)
		LEFT JOIN system.layer_bundles USING (bundle_id)
UNION ALL
SELECT
	layer_id,
	layer_properties_id,
	layer_type,
	name,
	title,
	attribution,
	opacity,
	layer_legend_id,
	min_scale,
	max_scale,

	image_type,
	service_version,
	tile_matrix_set,
	projection,
	url,
	bundle_name,

	NULL AS begin_year,
	NULL AS end_year,
	NULL AS dynamic_type,
	NULL AS tile_size,

	NULL AS api_key

	FROM system.layers
		INNER JOIN system.wmts_layer_properties USING (layer_properties_id)
		LEFT JOIN system.layer_bundles USING (bundle_id)
UNION ALL
SELECT
	layer_id,
	layer_properties_id,
	layer_type,
	name,
	title,
	attribution,
	opacity,
	layer_legend_id,
	min_scale,
	max_scale,

	NULL AS image_type,
	NULL AS service_version,
	NULL AS tile_matrix_set,
	NULL AS projection,
	NULL AS url,
	bundle_name,

	NULL AS begin_year,
	NULL AS end_year,
	NULL AS dynamic_type,
	NULL AS tile_size,

	api_key

	FROM system.layers
		INNER JOIN system.bing_layer_properties USING (layer_properties_id)
		LEFT JOIN system.layer_bundles USING (bundle_id)
;
