/*
 * layer_legends
 * -------------
 * Table containing legends for (WMS) layers.
 * Multiple layers can use the same legend.
 */
CREATE TABLE system.layer_legends (
	layer_legend_id integer NOT NULL,
	legend_type system.layer_legend_type NOT NULL,
	description text NOT NULL,

	CONSTRAINT layer_legends_pkey PRIMARY KEY (layer_legend_id)
);


/*
 * layer_legend_color_items
 * ------------------------
 * Table containing items for a legend for (WMS) layers.
 */
CREATE TABLE system.layer_legend_color_items (
	layer_legend_id integer NOT NULL,
	name text NOT NULL,
	color system.color NOT NULL,
	sort_order integer NOT NULL,

	CONSTRAINT layer_legend_color_items_pkey PRIMARY KEY (layer_legend_id, name),
	CONSTRAINT layer_legend_color_items_fkey_layer_legends FOREIGN KEY (layer_legend_id) REFERENCES system.layer_legends
);


/*
 * layer_bundles
 * -------------
 * Table containing bundles that can be used to bundle/group layers together.
 */
CREATE TABLE system.layer_bundles (
	bundle_id integer NOT NULL,
	bundle_name text NOT NULL,

	CONSTRAINT layer_bundles_pkey PRIMARY KEY (bundle_id)
);


/*
 * layer_properties
 * ----------------
 * Parent table for properties of layers. Does not contain physical records itself, but is meant for inheritance.
 */
CREATE TABLE system.layer_properties (
	layer_properties_id integer NOT NULL,
	title text,
	attribution text,
	opacity float NOT NULL,
	layer_legend_id integer,
	min_scale integer,
	max_scale integer,
	url text,
	bundle_id integer,

	CONSTRAINT layer_properties_pkey PRIMARY KEY (layer_properties_id),
	CONSTRAINT layer_properties_fkey_layer_legends FOREIGN KEY (layer_legend_id) REFERENCES system.layer_legends,
	CONSTRAINT layer_properties_fkey_layer_bundles FOREIGN KEY (bundle_id) REFERENCES system.layer_bundles
);


/*
 * wmts_layer_properties
 * ---------------------
 * Table containing properties for WMTS (Web Tile Map Service) layers.
 */
CREATE TABLE system.wmts_layer_properties (
	image_type text NOT NULL,
	service_version text NOT NULL,
	tile_matrix_set text NOT NULL,
	projection text NOT NULL,

	PRIMARY KEY (layer_properties_id)
) INHERITS (system.layer_properties);

CREATE UNIQUE INDEX idx_wmts_layers_layer_id ON system.wmts_layer_properties (layer_properties_id);


/*
 * wms_layer_properties
 * --------------------
 * Table containing properties for WMS (Web Map Service) layers.
 */
CREATE TABLE system.wms_layer_properties (
	begin_year integer,
	end_year integer,
	dynamic_type system.dynamic_wms_layer_type,
	tile_size integer,

	PRIMARY KEY (layer_properties_id)
) INHERITS (system.layer_properties);

CREATE UNIQUE INDEX idx_wms_layers_layer_properties_id ON system.wms_layer_properties (layer_properties_id);


/*
 * bing_layer_properties
 * ---------------------
 * Table containing properties for Microsoft Bing layers.
 */
CREATE TABLE system.bing_layer_properties (
	api_key text
) INHERITS (system.layer_properties);

CREATE UNIQUE INDEX idx_bing_layers_layer_properties_id ON system.bing_layer_properties (layer_properties_id);

/*
 * layers
 * ------
 * Parent table for layers.
 */
CREATE TABLE system.layers (
	layer_id integer NOT NULL,
	layer_properties_id integer NOT NULL,
	layer_type system.layer_type NOT NULL,
	name text NOT NULL,

	CONSTRAINT layers_pkey PRIMARY KEY (layer_id)
);


/*
 * theme_layers
 * ------------
 * Table to specify which layers should be loaded by default per theme, and in which order.
 */
CREATE TABLE system.theme_layers (
	layer_id integer NOT NULL,
	theme theme_type NOT NULL,
	sort_order integer NOT NULL,
	part_of_base_layer boolean,
	visible boolean,

	CONSTRAINT theme_layers_pkey PRIMARY KEY (layer_id, theme),
	CONSTRAINT theme_layers_fkey_layers FOREIGN KEY (layer_id) REFERENCES system.layers
);
