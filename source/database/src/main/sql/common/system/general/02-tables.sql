/*
 * color_ranges
 * ------------
 * Table containing ranges by lower bound and their colors.
 * Can be used to group receptors by range for example.
 */
CREATE TABLE system.color_ranges (
	color_range_type color_range_type NOT NULL,
	lower_value real NOT NULL,
	color system.color NOT NULL,

	CONSTRAINT color_ranges_pkey PRIMARY KEY (color_range_type, lower_value)
);


/*
 * color_items
 * -----------
 * Table containing individual items and their color.
 * Can be used to group road categories on a map for example.
 */
CREATE TABLE system.color_items (
	color_item_type color_item_type NOT NULL,
	item_value text NOT NULL,
	color system.color NOT NULL,
	line_width integer NOT NULL,
	sort_order integer NOT NULL,

	CONSTRAINT color_items_pkey PRIMARY KEY (color_item_type, item_value)
);
