/*
 * color
 * -----
 * Type for hexadecimal color code.
 */
CREATE DOMAIN system.color AS character varying(6)
	CHECK ((VALUE IS NULL) OR (VALUE::text ~ '[0-9a-fA-F]{6}'::text));
