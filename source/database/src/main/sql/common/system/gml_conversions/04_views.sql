/*
 * system.gml_plan_conversions_view
 * --------------------------------
 * View returning the conversion data for plans. This contains, among other things, the emission factor and emission characteristics.
 * See 'system.gml_plan_conversions' for more information.
 */
CREATE OR REPLACE VIEW system.gml_plan_conversions_view AS
SELECT
	gml_plan_conversions.old_code,
	gml_plan_conversions.name,
	gml_plan_conversions.emission_factor,
	gml_plan_conversions.heat_content,
	gml_plan_conversions.height,
	gml_plan_conversions.spread,
	gml_plan_conversions.particle_size_distribution,
	emission_diurnal_variations.emission_diurnal_variation_id,
	emission_diurnal_variations.code AS emission_diurnal_variation_code

	FROM system.gml_plan_conversions
		INNER JOIN emission_diurnal_variations USING (emission_diurnal_variation_id);
