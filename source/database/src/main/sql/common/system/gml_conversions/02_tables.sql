/*
 * gml_conversions
 * ---------------
 * Table containing conversions of adjusted IDs and codes used in older GML versions.
 */
CREATE TABLE system.gml_conversions (
	gml_version text NOT NULL,
	type text NOT NULL,
	old_value text NOT NULL,
	new_value text NOT NULL,
	issue_warning boolean NOT NULL,

	CONSTRAINT gml_conversions_pkey PRIMARY KEY (gml_version, type, old_value)
);


/*
 * gml_mobile_source_off_road_conversions
 * --------------------------------------
 * Table containing values necessary for converting mobile source off road properties from the old method (IMAER <= 3.1) to the new method (IMAER >= 4.0).
 * Based on the old off road mobile source category code and the values present in the GML an estimation of the total operating hours can be made.
 *
 * @column new_fuel_consumption liter fuel used per hour (l/hour)
 */
CREATE TABLE system.gml_mobile_source_off_road_conversions (
	old_code text NOT NULL,
	new_fuel_consumption posreal NOT NULL,

	CONSTRAINT gml_mobile_source_off_road_conversions_pkey PRIMARY KEY (old_code)
);


/*
 * gml_plan_conversions
 * --------------------
 * Table containing values needed to convert properties of (spatial) plan emission sources in older versions (IMAER <= 4.0) to generic sources.
 * Based on the old plan codes and the values present in the GML a generic source can be created.
 * The emission factors in this table are for the substance NOx, as that was the only substance having emission factors > 0.
 * To prevent characteristics getting changed should the GCN sector characteristics ever change, these are directly incorporated in this table.
 *
 * @column name The name of the category. Is not used in the conversion code.
 * @column emission_factor The emission factor for NOx in kg/y/[unit belonging to the category]
 */
CREATE TABLE system.gml_plan_conversions (
	old_code text NOT NULL,
	name text NOT NULL,
	emission_factor posreal NOT NULL,
	heat_content posreal NOT NULL,
	height posreal NOT NULL,
	spread posreal NOT NULL,
	emission_diurnal_variation_id integer NOT NULL,
	particle_size_distribution integer NOT NULL,

	CONSTRAINT gml_plan_conversions_pkey PRIMARY KEY (old_code),
	CONSTRAINT gml_plan_conversion_fkey_emission_diurnal_variations FOREIGN KEY (emission_diurnal_variation_id) REFERENCES emission_diurnal_variations
);


/*
 * gml_farm_lodging_conversions
 * ----------------------------
 * Table containing values necessary for converting farm lodgings from the old method (IMAER <= 5.1) to the new farm animal housing method (IMAER >= 6.0).
 *
 * @column animal_type_code The code of the animal type to convert to. 
 * @column animal_housing_code The code of the animal housing category to convert to.
 * @column additional_system_code The optional code of the additional system to add to the resulting animal housing.
 */
CREATE TABLE system.gml_farm_lodging_conversions (
	old_code text NOT NULL,
	animal_type_code text NOT NULL,
	animal_housing_code text NOT NULL,
	additional_system_code text,

	CONSTRAINT gml_farm_lodging_conversions_pkey PRIMARY KEY (old_code)
);
