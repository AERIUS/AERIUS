/*
 * uncalculated_receptors
 * ----------------------
 * Table containing receptors that have not been calculated.
 * In monitor for example only zoom_level 3 receptors have been calculated.
 */
CREATE TABLE setup.uncalculated_receptors (
	receptor_id integer NOT NULL,

	CONSTRAINT uncalculated_receptors_pkey PRIMARY KEY (receptor_id),
	CONSTRAINT uncalculated_receptors_fkey_receptors FOREIGN KEY (receptor_id) REFERENCES receptors
);
