/*
 * receptors
 * ---------
 * Table containing the receptors (standard calculation points).
 * Receptors represent the center point of the accompanying hexagons.
 */
CREATE TABLE receptors (
	receptor_id integer NOT NULL,
	geometry geometry(Point),

	CONSTRAINT receptors_pkey PRIMARY KEY (receptor_id)
);

CREATE INDEX idx_receptors_geometry_gist ON receptors USING GIST (geometry);


/*
 * hexagons
 * --------
 * Table containing the hexagons (including geometry) that belong to receptors.
 * These hexagons serve as a representation of the receptor at a certain zoom level.
 * For NL, the surface of a hexagon at zoom level 1 matches a hectare.
 * For UK, the surface of a hexagon at zoom level 1 matches 4 hectares.
 * At higher zoom levels the hexagons are aggregations.
 */
CREATE TABLE hexagons (
	receptor_id integer NOT NULL,
	zoom_level posint NOT NULL,
	geometry geometry(Polygon),

	CONSTRAINT hexagons_pkey PRIMARY KEY (receptor_id, zoom_level),
	CONSTRAINT hexagons_fkey_receptors FOREIGN KEY (receptor_id) REFERENCES receptors
);

CREATE INDEX idx_hexagons_geometry_gist ON hexagons USING GIST (geometry);
CREATE INDEX idx_hexagons_zoom_level ON hexagons (zoom_level);


/*
 * included_receptors
 * ------------------
 * Table containing the 'included' receptors.
 * A receptor is only included in overviews when present in this table.
 * The receptor can for example be excluded when they are to close to a source, which would make the results for this receptor point incorrect.
 * Another reason would be that the receptor (through zoom level 1 hexagon) did not cover a relevant habitat area.
 */
CREATE TABLE included_receptors (
	receptor_id integer NOT NULL,

	CONSTRAINT included_receptors_pkey PRIMARY KEY (receptor_id),
	CONSTRAINT included_receptors_fkey_receptors FOREIGN KEY (receptor_id) REFERENCES receptors
);


/*
 * critical_levels
 * ---------------
 * Table containing the critical levels per receptor per substance/result type.
 * Each habitat type can have critical levels for a substance/result type combination.
 * To determine the values in this table, the critical level for relevant habitat areas that intersect with the zoom level 1 hexagon are used.
 * Per receptor, the lowest (=most strict) critical level value is used, no matter the surface size that is covered.
 */
CREATE TABLE critical_levels (
	receptor_id integer NOT NULL,
	substance_id smallint NOT NULL,
	result_type emission_result_type NOT NULL,
	critical_level posreal NOT NULL CHECK (critical_level > 0),

	CONSTRAINT critical_levels_pkey PRIMARY KEY (receptor_id, substance_id, result_type),
	CONSTRAINT critical_levels_fkey_receptors FOREIGN KEY (receptor_id) REFERENCES receptors
);


/*
 * terrain_properties
 * ------------------
 * Table containing the average roughness and dominant land use per receptor and zoom level.
 * All values in this table are determined within the area of the hexagon at zoom level 1, corresponding to the receptor.
 * @column average_roughness The average roughness, in meters.
 * @column dominant_land_use The dominant land use, one of the values in land_use_classification enumeration.
 * @column land_uses The relative shares per receptor of each land use classification.
 * This is an array with 9 elements. The number 9 is equal to the number of values in the land_use_classification enum.
 */
CREATE TABLE terrain_properties (
	receptor_id integer NOT NULL,
	zoom_level integer NOT NULL,
	average_roughness real NOT NULL,
	dominant_land_use land_use_classification NOT NULL,
	land_uses integer ARRAY[9] NOT NULL,

	CONSTRAINT terrain_properties_pkey PRIMARY KEY (receptor_id, zoom_level)
);


/*
 * non_exceeding_receptors
 * -----------------------
 * Table containing the receptors for which the (nitrogen) deposition does NOT exceed or nearly exceeds the critical deposition value (KDW - 70 mol).
 */
CREATE TABLE non_exceeding_receptors (
	receptor_id integer NOT NULL,

	CONSTRAINT non_exceeding_receptors_policies_pkey PRIMARY KEY (receptor_id),
	CONSTRAINT non_exceeding_receptors_policies_fkey_receptors FOREIGN KEY (receptor_id) REFERENCES receptors
);


/*
 * hexagon_type_receptors
 * ----------------------
 * Table specifying per hexagon type which receptors belong to that type.
 */
CREATE TABLE hexagon_type_receptors (
  hexagon_type hexagon_type NOT NULL,
	receptor_id integer NOT NULL,

	CONSTRAINT hexagon_type_receptors_pkey PRIMARY KEY (hexagon_type, receptor_id),
	CONSTRAINT hexagon_type_receptors_fkey_receptors FOREIGN KEY (receptor_id) REFERENCES receptors
);

CREATE INDEX idx_hexagon_type_receptors_receptor_id ON hexagon_type_receptors (receptor_id);


/*
 * extra_assessment_receptors
 * --------------------------
 * Table specifying the receptors that require extra assessment.
 * These receptors were covered by a specific habitat in an earlier dataset (like T0), but are no longer. 
 * In some such cases, there are reasons to assess effects of projects on these receptors, for instance if the goal is to restore the habitat on this hexagon.
 * 
 * Note: these receptors might still be covered by other habitat types, making them relative still.
 * In that case, the habitat type(s) that are no longer present will most likely have a lower critical load.
 */
CREATE TABLE extra_assessment_receptors (
	receptor_id integer NOT NULL,
	assessment_area_id integer NOT NULL,
	habitat_type_id integer NOT NULL,

	CONSTRAINT extra_assessment_receptors_pkey PRIMARY KEY (receptor_id, assessment_area_id, habitat_type_id),
	CONSTRAINT extra_assessment_receptors_fkey_receptors FOREIGN KEY (receptor_id) REFERENCES receptors,
	CONSTRAINT extra_assessment_receptors_fkey_habitat_types FOREIGN KEY (habitat_type_id) REFERENCES habitat_types
);

CREATE INDEX idx_extra_assessment_receptors_assessment_area_id ON extra_assessment_receptors (assessment_area_id);
