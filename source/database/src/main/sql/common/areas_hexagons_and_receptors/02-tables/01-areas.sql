/*
 * countries
 * ---------
 * Table containing countries.
 * Contains a code (for use URL and such) and a more extensive name/description.
 */
CREATE TABLE countries (
	country_id integer NOT NULL,
	code text NOT NULL,
	name text NOT NULL,

	CONSTRAINT countries_pkey PRIMARY KEY (country_id),
	CONSTRAINT countries_code_unique UNIQUE (code)
);


/*
 * authorities
 * -----------
 * Table containing competent authorities.
 * Contains a code (for use URL and such), a more extensive name/description and an authority type.
 */
CREATE TABLE authorities (
	authority_id integer NOT NULL,
	country_id integer NOT NULL,
	code text NOT NULL,
	name text NOT NULL,
	type authority_type NOT NULL,

	CONSTRAINT authorities_pkey PRIMARY KEY (authority_id),
	CONSTRAINT authorities_code_unique UNIQUE (code),
	CONSTRAINT authorities_fkey_countries FOREIGN KEY (country_id) REFERENCES countries
);


/*
 * assessment_areas
 * ----------------
 * Parent table for assessment areas. Does not contain physical records itself, but is meant for inheritance.
 *
 * For NL:
 * 1..10000 = N2000 areas (in natura2000_areas) (1000+ = abroad)
 * 10001..20000 = N2000 directive areas (in natura2000_directive_areas)
 */
CREATE TABLE assessment_areas
(
	assessment_area_id integer NOT NULL,
	type assessment_area_type NOT NULL,
	name text NOT NULL,
	code text NOT NULL,
	authority_id integer NOT NULL,
	geometry geometry(MultiPolygon),

	CONSTRAINT assessment_areas_pkey PRIMARY KEY (assessment_area_id),
	CONSTRAINT assessment_areas_fkey_authorities FOREIGN KEY (authority_id) REFERENCES authorities,
	CONSTRAINT assessment_areas_code_unique UNIQUE (code)
);

CREATE INDEX idx_assessment_areas_geometry_gist ON assessment_areas USING GIST (geometry);
CREATE INDEX idx_assessment_areas_name ON assessment_areas (name);


/*
 * natura2000_areas
 * ----------------
 * Table containing nature sites (natura2000 areas).
 * The geometry used should be equal to the union of all directive areas (natura2000_directive_areas) of the same area.
 *
 * In the case of NL: the natura2000_area_id matches the official Natura 2000 area numbers as used in the netherlands.
 * In the case of UK: this table contains the nature sites.
 */
CREATE TABLE natura2000_areas
(
	natura2000_area_id integer NOT NULL,

	CONSTRAINT natura2000_areas_pkey PRIMARY KEY (natura2000_area_id)

) INHERITS (assessment_areas);

CREATE UNIQUE INDEX idx_natura2000_areas_assessment_area_id ON natura2000_areas (assessment_area_id);
CREATE INDEX idx_natura2000_areas_geometry_gist ON natura2000_areas USING GIST (geometry);
CREATE INDEX idx_natura2000_areas_name ON natura2000_areas (name);


/*
 * natura2000_area_properties
 * --------------------------
 * Table containing properties for a nature site (natura2000 area).
 *
 * @column registered_surface is the surface as it is registered in the designation decision (aanwijsbesluit)
 * @column design_status specifies the 'vaststellings-status' for the N2000 area.
 */
CREATE TABLE natura2000_area_properties (
	natura2000_area_id integer NOT NULL,
	registered_surface bigint NOT NULL,
	design_status design_status_type NOT NULL,

	CONSTRAINT natura2000_area_properties_pkey PRIMARY KEY (natura2000_area_id),
	CONSTRAINT natura2000_area_properties_fkey_natura2000_areas FOREIGN KEY (natura2000_area_id) REFERENCES natura2000_areas
);


/*
 * natura2000_directives
 * ---------------------
 * Table containing the possible directives for nature sites (natura2000 areas).
 *
 * Each directive specifies if it is a habitat directive and/or a species directive.
 * This influences in what way an area will be incorporated when it comes to relevant habitat areas.
 */
CREATE TABLE natura2000_directives
(
	natura2000_directive_id integer NOT NULL,
	directive_code text NOT NULL,
	directive_name text NOT NULL,
	habitat_directive boolean NOT NULL,
	species_directive boolean NOT NULL,

	CONSTRAINT natura2000_directives_pkey PRIMARY KEY (natura2000_directive_id),
	CONSTRAINT natura2000_directives_unique_code UNIQUE (directive_code)
);


/*
 * natura2000_directive_areas
 * --------------------------
 * Table containing the sections of the nature sites (natura2000 areas) with their own directive(s).
 */
CREATE TABLE natura2000_directive_areas
(
	natura2000_directive_area_id integer NOT NULL,
	natura2000_area_id integer NOT NULL,
	natura2000_directive_id integer NOT NULL,
	design_status_description text NOT NULL,

	CONSTRAINT natura2000_directive_areas_pkey PRIMARY KEY (natura2000_directive_area_id),
	CONSTRAINT natura2000_directive_areas_fkey_natura2000_areas FOREIGN KEY (natura2000_area_id) REFERENCES natura2000_areas,
	CONSTRAINT natura2000_directive_areas_fkey_natura2000_directives FOREIGN KEY (natura2000_directive_id) REFERENCES natura2000_directives
) INHERITS (assessment_areas);

CREATE UNIQUE INDEX idx_natura2000_directive_areas_assessment_area_id ON natura2000_directive_areas (assessment_area_id);
CREATE INDEX idx_natura2000_directive_areas_geometry_gist ON natura2000_directive_areas USING GIST (geometry);
CREATE INDEX idx_natura2000_directive_areas_name ON natura2000_directive_areas (name);


/*
 * habitat_types
 * -------------
 * Table containing the habitat types, identified by the name column and with an additional description.
 */
CREATE TABLE habitat_types
(
	habitat_type_id integer NOT NULL,
	name text NOT NULL,
	description text NOT NULL,

	CONSTRAINT habitat_types_pkey PRIMARY KEY (habitat_type_id)
);

CREATE UNIQUE INDEX idx_habitat_types_name ON habitat_types (name);


/*
 * habitat_type_critical_levels
 * ----------------------------
 * Table containing the critical levels for the habitat types per substance and emission result type.
 * NOTE: For critical nitrogen deposition (KDW) a value has to be used for substance ID 1711 and result type "deposition".
 */
CREATE TABLE habitat_type_critical_levels (
	habitat_type_id integer NOT NULL,
	substance_id smallint NOT NULL,
	result_type emission_result_type NOT NULL,
	critical_level posreal NULL,
	sensitive boolean NOT NULL DEFAULT FALSE,

	CONSTRAINT habitat_type_critical_levels_pkey PRIMARY KEY (habitat_type_id, substance_id, result_type),
	CONSTRAINT habitat_type_critical_levels_fkey_habitat_types FOREIGN KEY (habitat_type_id) REFERENCES habitat_types
);


/*
 * habitat_properties
 * ------------------
 * Table containing the properties of the goal/designated habitat types per assessment area, along with the status.
 * @column quality_goal The goal for quality (doelstelling kwaliteit).
 * @column extent_goal The goal for extent/surface (doelstelling oppervlakte).
 * @column design_status The status of this habitat type (vaststellings status).
 */
CREATE TABLE habitat_properties (
	goal_habitat_type_id integer NOT NULL,
	assessment_area_id integer NOT NULL,
	quality_goal habitat_goal_type NOT NULL,
	extent_goal habitat_goal_type NOT NULL,
	design_status design_status_type NOT NULL CHECK (design_status <> 'irrelevant'),

	CONSTRAINT habitat_properties_pkey PRIMARY KEY (goal_habitat_type_id, assessment_area_id),
	CONSTRAINT habitat_properties_fkey_habitat_types FOREIGN KEY (goal_habitat_type_id) REFERENCES habitat_types (habitat_type_id)
);


/*
 * habitat_type_relations
 * ----------------------
 * Table containing the relations between habitat types.
 * Habitat types can be sub-types of a goal/designated habitat type.
 * For instance, H1330A and ZGH1330A, which are both part of designated habitat type H1330A.
 * This parent-child relation is kept in this table.
 * A habitat type can only have 1 goal habitat type. Most habitat types have themselves as the goal habitat type.
 */
CREATE TABLE habitat_type_relations
(
	habitat_type_id integer NOT NULL UNIQUE,
	goal_habitat_type_id integer NOT NULL,

	CONSTRAINT habitat_type_relations_pkey PRIMARY KEY (habitat_type_id, goal_habitat_type_id),
	CONSTRAINT habitat_type_relations_fkey_habitat_types FOREIGN KEY (habitat_type_id) REFERENCES habitat_types,
	CONSTRAINT habitat_type_relations_fkey_goal_habitat_types FOREIGN KEY (goal_habitat_type_id) REFERENCES habitat_types (habitat_type_id)
);


/*
 * habitat_areas
 * -------------
 * Table containing the habitat areas per assessment area. A habitat area has a habitat type.
 *
 * @column coverage The coverage of the habitat over the area (Dekkingsgraad). This factor is used to determine the cartographic surface.
 */
CREATE TABLE habitat_areas
(
	assessment_area_id integer NOT NULL,
	habitat_area_id integer NOT NULL,
	habitat_type_id integer NOT NULL,
	coverage fraction NOT NULL,
	geometry geometry(MultiPolygon),

	CONSTRAINT habitat_areas_pkey PRIMARY KEY (habitat_area_id),
	CONSTRAINT habitat_areas_fkey_habitat_types FOREIGN KEY (habitat_type_id) REFERENCES habitat_types
);

CREATE INDEX idx_habitat_areas_geometry_gist ON habitat_areas USING GIST (geometry);
CREATE INDEX idx_habitat_areas_assessment_area_id ON habitat_areas (assessment_area_id);
CREATE INDEX idx_habitat_areas_habitat_type_id ON habitat_areas (habitat_type_id);


/*
 * relevant_habitat_areas
 * ----------------------
 * Table containing the relevant (parts of) habitat areas per assessment areas.
 * 
 * A habitat area either is or isn't relevant, but at the borders of assessment areas it can also be partly relevant.
 * In that case the geometry is the intersection of the assessment area and habitat area.
 * 
 * @column coverage Coverage of the whole habitat area, equal to {@link habitat_areas}.
 */
CREATE TABLE relevant_habitat_areas
(
	assessment_area_id integer NOT NULL,
	habitat_area_id integer NOT NULL,
	habitat_type_id integer NOT NULL,
	coverage fraction NOT NULL,
	geometry geometry(MultiPolygon),

	CONSTRAINT relevant_habitat_areas_pkey PRIMARY KEY (habitat_area_id),
	CONSTRAINT relevant_habitat_areas_fkey_habitat_types FOREIGN KEY (habitat_type_id) REFERENCES habitat_types
);

CREATE INDEX idx_relevant_habitat_areas_geometry_gist ON relevant_habitat_areas USING GIST (geometry);
CREATE INDEX idx_relevant_habitat_areas_assessment_area_id ON relevant_habitat_areas (assessment_area_id);
CREATE INDEX idx_relevant_habitat_areas_habitat_type_id ON relevant_habitat_areas (habitat_type_id);


/*
 * habitats
 * --------
 * Table containing combined habitat areas per assessment area and habitat type.
 * The geometry is the combination of all individual areas of the habitat type within the assessment area.
 *
 * @column habitat_coverage Average coverage for this habitat. Calculated based on the average of the individual habitat areas, 
 * weighted by surface of each area.
 */
CREATE TABLE habitats
(
	assessment_area_id integer NOT NULL,
	habitat_type_id integer NOT NULL,
	habitat_coverage fraction NOT NULL,
	geometry geometry(MultiPolygon),

	CONSTRAINT habitats_pkey PRIMARY KEY (assessment_area_id, habitat_type_id),
	CONSTRAINT habitats_fkey_habitat_types FOREIGN KEY (habitat_type_id) REFERENCES habitat_types
);

CREATE INDEX idx_habitats_geometry_gist ON habitats USING GIST (geometry);
CREATE INDEX idx_habitats_habitat_type_id ON habitats (habitat_type_id);


/*
 * relevant_habitats
 * -----------------
 * Table containing relevant (parts of) combined habitat areas.
 *
 * @column habitat_coverage Average coverage for this habitat. Calculated based on the average of the individual habitat areas, 
 * weighted by surface of each area. For partially relevant habitat areas, the full surface of the area is used as the weight.
 */
CREATE TABLE relevant_habitats
(
	assessment_area_id integer NOT NULL,
	habitat_type_id integer NOT NULL,
	habitat_coverage fraction NOT NULL,
	geometry geometry(MultiPolygon),

	CONSTRAINT relevant_habitats_pkey PRIMARY KEY (assessment_area_id, habitat_type_id),
	CONSTRAINT relevant_habitats_fkey_habitat_types FOREIGN KEY (habitat_type_id) REFERENCES habitat_types
);

CREATE INDEX idx_relevant_habitats_geometry_gist ON relevant_habitats USING GIST (geometry);
CREATE INDEX idx_relevant_habitats_habitat_type_id ON relevant_habitats (habitat_type_id);


/*
 * species
 * -------
 * Table containing species that can be present in a habitat.
 * This can be habitat species, breeding bird species and non breeding bird species.
 * A bird species can be present as both breeding and non-breeding in this table, as for some areas it is considered breeding while for others it is not.
 */
CREATE TABLE species
(
	species_id integer NOT NULL,
	name text NOT NULL,
	description text NOT NULL,
	species_type species_type NOT NULL,

	CONSTRAINT species_pkey PRIMARY KEY (species_id)
);

CREATE UNIQUE INDEX idx_species_name ON species (name, species_type);


/*
 * species_properties
 * ------------------
 * Table containing properties of designated species per assessment areas.
 * Contains the goals for each species, including the status:
 * Goal quality habitat, goal surface habitat, goal population.
 * The population goal for non breeding bird species is supplied as text. The population_goal must be 'specified' in that case.
 * In all other cases and goal types the goal shouldn't be 'specified'.
 */
CREATE TABLE species_properties (
	species_id integer NOT NULL,
	assessment_area_id integer NOT NULL,
	quality_goal habitat_goal_type NOT NULL CHECK (quality_goal <> 'specified'),
	extent_goal habitat_goal_type NOT NULL CHECK (extent_goal <> 'specified'),
	population_goal habitat_goal_type NOT NULL
		CHECK ((population_goal <> 'specified' AND population_goal_description IS NULL) OR (population_goal = 'specified' AND population_goal_description IS NOT NULL)),
	population_goal_description text, -- TODO: need some refactoring
	design_status design_status_type NOT NULL CHECK (design_status <> 'irrelevant'),

	CONSTRAINT species_properties_pkey PRIMARY KEY (species_id, assessment_area_id),
	CONSTRAINT species_properties_fkey_species FOREIGN KEY (species_id) REFERENCES species
);


/*
 * designated_species
 * ------------------
 * Table containing designated species per assessment areas.
 */
CREATE TABLE designated_species
(
	species_id integer NOT NULL,
	assessment_area_id integer NOT NULL,

	CONSTRAINT designated_species_pkey PRIMARY KEY (species_id, assessment_area_id),
	CONSTRAINT designated_species_fkey_assessment_areas FOREIGN KEY (assessment_area_id) REFERENCES natura2000_areas (assessment_area_id), -- Currently limited to N2000. Can't reference base table 'assessment_areas'.
	CONSTRAINT designated_species_fkey_species FOREIGN KEY (species_id) REFERENCES species
);


/*
 * species_to_habitats
 * -------------------
 * Table containing the species that can be present in a habitat type.
 */
CREATE TABLE species_to_habitats
(
	species_id integer NOT NULL,
	assessment_area_id integer NOT NULL,
	goal_habitat_type_id integer NOT NULL,

	CONSTRAINT species_to_habitats_pkey PRIMARY KEY (species_id, assessment_area_id, goal_habitat_type_id),
	CONSTRAINT species_to_habitats_fkey_species FOREIGN KEY (species_id) REFERENCES species,
	CONSTRAINT species_to_habitats_fkey_assessment_areas FOREIGN KEY (assessment_area_id) REFERENCES natura2000_areas (assessment_area_id), -- Currently limited to N2000. Can't reference base table 'assessment_areas'.
	CONSTRAINT species_to_habitats_fkey_habitat_types FOREIGN KEY (goal_habitat_type_id) REFERENCES habitat_types (habitat_type_id)
);


/*
 * province_areas
 * --------------
 * Table containing provinces.
 * Each province has a related authority, which is also defined in this table. 
 */
CREATE TABLE province_areas
(
	province_area_id integer NOT NULL,
	name text NOT NULL,
	authority_id integer NOT NULL,
	geometry geometry(MultiPolygon),

	CONSTRAINT province_areas_pkey PRIMARY KEY (province_area_id),
	CONSTRAINT province_areas_fkey_authorities FOREIGN KEY (authority_id) REFERENCES authorities
);

CREATE INDEX idx_province_areas_geometry_gist ON province_areas USING GIST (geometry);
CREATE INDEX idx_province_areas_name ON province_areas (name);
