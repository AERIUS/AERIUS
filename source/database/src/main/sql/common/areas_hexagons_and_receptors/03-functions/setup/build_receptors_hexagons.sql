/*
 * ae_assessment_area_geometry_of_interest
 * ---------------------------------------
 * Function returning the geometry of interest for an assessment area.
 * The geometry of interest is the geometry of the assessment area on land plus the geometry of the area where there are habitat areas.
 * To ensure everything is covered, a buffer is added for the section on water, and for the union of land and water sections this buffer is added as well.
 * For NL this buffer (as defined by a constant) is 170m.
 * For UK this buffer (as defined by a constant) is 850m.
 */
CREATE OR REPLACE FUNCTION setup.ae_assessment_area_geometry_of_interest(v_assessment_area_id integer, v_land_geometry geometry)
	RETURNS geometry AS
$BODY$
DECLARE
	v_on_land_geometry geometry;
	v_on_water_geometry geometry;
	v_habitat_on_water_geometry geometry;
	v_habitat_on_water_count integer;
	v_buffer integer = ae_constant('GEOMETRY_OF_INTEREST_BUFFER')::integer;
BEGIN
	-- Get the geometry of the assessment area on land and water
	v_on_land_geometry := (SELECT ST_Intersection(geometry, v_land_geometry) FROM assessment_areas WHERE assessment_area_id = v_assessment_area_id);
	v_on_water_geometry := (SELECT ST_Difference(geometry, v_land_geometry) FROM assessment_areas WHERE assessment_area_id = v_assessment_area_id);

	-- Habitat on land geometry must be set
	v_habitat_on_water_geometry := ST_GeomFromText('POLYGON EMPTY', ae_get_srid());

	-- Get the hatiat geometry on water
	IF (NOT ST_IsEmpty(v_on_water_geometry)) THEN
		-- Get the geometry of the habitat_areas within the on water geometry
		-- Use count because ST_Union(NULL) returns invalid geometry
		SELECT
			ST_Union(ST_Intersection(geometry, v_on_water_geometry)),
			COUNT(*)

			INTO v_habitat_on_water_geometry, v_habitat_on_water_count

			FROM habitats
				INNER JOIN habitat_type_sensitivity_view USING (habitat_type_id)

			WHERE
				assessment_area_id = v_assessment_area_id
				AND sensitive IS TRUE
				AND ST_Intersects(v_on_water_geometry, geometry)
		;

		IF (v_habitat_on_water_count = 0) THEN
			v_habitat_on_water_geometry := ST_GeomFromText('POLYGON EMPTY', ae_get_srid());
		END IF;
	END IF;

	RAISE NOTICE E'Assessment area %: % m\u00B2 land, % m\u00B2 water, % m\u00B2 habitat on water.', v_assessment_area_id, FLOOR(ST_Area(v_on_land_geometry)), FLOOR(ST_Area(v_on_water_geometry)), FLOOR(ST_Area(v_habitat_on_water_geometry));

	RETURN ST_Buffer(ST_Union(v_on_land_geometry, ST_Buffer(v_habitat_on_water_geometry, 2 * v_buffer)), v_buffer);
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_build_geometry_of_interests
 * ------------------------------
 * Function to determine (and fill) the geometry of interests for all assessment areas.
 * This function has to be run before creating receptors.
 */
CREATE OR REPLACE FUNCTION setup.ae_build_geometry_of_interests()
	RETURNS void AS
$BODY$
DECLARE
	v_land_geometry geometry;
BEGIN
	RAISE NOTICE '[%] Generating land geometry...', to_char(clock_timestamp(), 'DD-MM-YYYY HH24:MI:SS.MS');
	v_land_geometry := (SELECT ST_Union(geometry) FROM setup.province_land_borders);

	RAISE NOTICE '[%] Generating all geometry of interests...', to_char(clock_timestamp(), 'DD-MM-YYYY HH24:MI:SS.MS');
	INSERT INTO setup.geometry_of_interests(assessment_area_id, geometry)
	SELECT * FROM
		(SELECT
			assessment_area_id,
			ST_Multi(setup.ae_assessment_area_geometry_of_interest(assessment_area_id, v_land_geometry)) AS geometry

			FROM
				(SELECT assessment_area_id FROM assessment_areas WHERE type = 'natura2000_area' ORDER BY assessment_area_id) AS assessment_area_ids
		)AS geometry_of_interest

		WHERE NOT ST_IsEmpty(geometry)
	;

	RAISE NOTICE '[%] Done.', to_char(clock_timestamp(), 'DD-MM-YYYY HH24:MI:SS.MS');
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_build_receptors
 * ------------------
 * Function to fill the receptor table with all receptors within geometries of interests of the assessment areas.
 * This function has to be run before hexagons can be created.
 */
CREATE OR REPLACE FUNCTION setup.ae_build_receptors()
	RETURNS void AS
$BODY$
DECLARE
	v_geometry_of_interests geometry;
	v_outside_boundary geometry;
BEGIN
	IF (SELECT COUNT(*) FROM setup.geometry_of_interests) = 0 THEN
		RAISE EXCEPTION '"setup.geometry_of_interests" table is empty. You must generate geometry of interests before receptors. You can use "setup.ae_build_geometry_of_interests()".';
	END IF;

	IF EXISTS(SELECT receptor_id FROM hexagons LIMIT 1) THEN
		RAISE WARNING '"hexagons" table is not empty! You should generate receptors BEFORE hexagons!';
	END IF;

	RAISE NOTICE '[%] Merging geometry of interests...', to_char(clock_timestamp(), 'DD-MM-YYYY HH24:MI:SS.MS');
	v_geometry_of_interests := (SELECT ST_Union(geometry) FROM setup.geometry_of_interests);

	RAISE NOTICE '[%] Subtracting outside boundary...', to_char(clock_timestamp(), 'DD-MM-YYYY HH24:MI:SS.MS');
	v_outside_boundary := ST_SetSRID(ST_GeomFromText(ae_constant('CALCULATOR_BOUNDARY')), ae_get_srid());
	v_geometry_of_interests := ST_Difference(v_geometry_of_interests, v_outside_boundary);

	RAISE NOTICE '[%] Generating receptors...', to_char(clock_timestamp(), 'DD-MM-YYYY HH24:MI:SS.MS');
	CREATE TEMPORARY TABLE receptors_in_bb ON COMMIT DROP AS
	SELECT receptor_id, geometry FROM ae_determine_receptor_ids_in_geometry(v_geometry_of_interests);

	RAISE NOTICE '[%] Inserting receptors...', to_char(clock_timestamp(), 'DD-MM-YYYY HH24:MI:SS.MS');
	INSERT INTO receptors SELECT receptor_id, geometry FROM receptors_in_bb;

	DROP TABLE receptors_in_bb;

	RAISE NOTICE '[%] Done.', to_char(clock_timestamp(), 'DD-MM-YYYY HH24:MI:SS.MS');
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_build_hexagons
 * -----------------
 * Function to fill the hexagons table with all hexagons belonging to the receptors.
 * This function should only be called after receptors have been created/inserted into the receptors table.
 */
CREATE OR REPLACE FUNCTION setup.ae_build_hexagons()
	RETURNS void AS
$BODY$
DECLARE
	v_max_zoom_level integer = ae_constant('MAX_ZOOM_LEVEL')::integer;
	v_zoom_level integer;
BEGIN
	IF (SELECT COUNT(*) FROM receptors) = 0 THEN
		RAISE EXCEPTION '"receptors" table is empty! You must generate receptors before hexagons. You can use "setup.ae_build_receptors()".';
	END IF;

	RAISE NOTICE '[%] Generating hexagons...', to_char(clock_timestamp(), 'DD-MM-YYYY HH24:MI:SS.MS');

	FOR v_zoom_level IN 1..v_max_zoom_level LOOP
		INSERT INTO hexagons
		SELECT receptors.receptor_id, v_zoom_level, ae_create_hexagon(receptors.receptor_id, v_zoom_level)
			FROM receptors
			WHERE
				v_zoom_level = 1
				OR ae_is_receptor_id_available_on_zoomlevel(receptors.receptor_id, v_zoom_level);
	END LOOP;
	RAISE NOTICE '[%] Done.', to_char(clock_timestamp(), 'DD-MM-YYYY HH24:MI:SS.MS');
END;
$BODY$
LANGUAGE plpgsql VOLATILE;
