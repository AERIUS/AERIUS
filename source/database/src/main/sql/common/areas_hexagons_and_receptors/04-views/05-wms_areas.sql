/*
 * wms_habitat_areas_sensitivity_view
 * ----------------------------------
 * WMS view returning habitat areas including critical deposition classification and relevance.
 */
CREATE OR REPLACE VIEW wms_habitat_areas_sensitivity_view AS
SELECT
	habitat_area_id,
	habitat_type_id,
	critical_deposition,
	ae_critical_deposition_classification(critical_deposition) AS critical_deposition_classification,
	(relevant_habitat_areas.habitat_type_id IS NOT NULL) AS relevant,
	habitat_areas.geometry,
	relevant_habitat_areas.geometry AS relevant_geometry

	FROM habitat_areas
		INNER JOIN habitat_types USING (habitat_type_id)
		INNER JOIN habitat_type_critical_depositions_view USING (habitat_type_id)
		LEFT JOIN relevant_habitat_areas USING (habitat_area_id, assessment_area_id, habitat_type_id)
;


/*
 * wms_habitat_areas_sensitivity_level_view
 * ----------------------------------------
 * WMS view returning habitat areas including critical level, substance, emission result type and relevance.
 */
CREATE OR REPLACE VIEW wms_habitat_areas_sensitivity_level_view AS
SELECT
	habitat_areas.habitat_area_id,
	habitat_areas.habitat_type_id,
	habitat_type_critical_levels.critical_level AS critical_level,
	relevant_habitat_areas.habitat_type_id IS NOT NULL AS relevant,
	habitat_type_critical_levels.substance_id,
	habitat_type_critical_levels.result_type as emission_result_type,
	habitat_areas.geometry,
	relevant_habitat_areas.geometry AS relevant_geometry

	FROM habitat_areas
		JOIN habitat_types USING (habitat_type_id)
		JOIN habitat_type_critical_levels USING (habitat_type_id)
		LEFT JOIN relevant_habitat_areas USING (habitat_area_id, assessment_area_id, habitat_type_id)
;


/*
 * wms_nature_areas_view
 * ---------------------
 * WMS view returning the natura2000 directive areas, including name.
 * Selectie van natura2000_directive_areas (inclusief naam).
 */
CREATE OR REPLACE VIEW wms_nature_areas_view AS
SELECT
	natura2000_areas.assessment_area_id,
	country_id,
	directive_code,
	directive_name,
	design_status_description,
	natura2000_areas.name,
	natura2000_directive_areas.geometry

	FROM natura2000_directive_areas
		INNER JOIN natura2000_directives USING (natura2000_directive_id)
		INNER JOIN authorities USING (authority_id)
		INNER JOIN natura2000_areas USING (natura2000_area_id)
;


/*
 * wms_habitats_view
 * -----------------
 * WMS view returning the habitat area(s) within an assessment area.
 * Use at least 'assessment_area_id' in the where-clause, optionally 'habitat_type_id'.
 */
CREATE OR REPLACE VIEW wms_habitats_view AS
SELECT
	assessment_area_id,
	habitat_type_id,
	name,
	description,
	geometry,
	relevant_geometry

	FROM habitats_view
;


/*
 * wms_relevant_habitat_info_for_receptor_view
 * -------------------------------------------
 * WMS view returning the habitat areas that intersect with a receptor/hexagon.
 * Us 'receptor_id' and 'habitat_type_id' in the where-clause.
 *
 * The geometries are returned per assessment_area_id, to avoid having to use the slow ST_Union function.
 * Graphically this results in a line on the borders.
 */
CREATE OR REPLACE VIEW wms_relevant_habitat_info_for_receptor_view AS
SELECT
	receptor_id,
	assessment_area_id,
	habitat_type_id,
	geometry

	FROM receptors_to_relevant_habitats_view
		INNER JOIN relevant_habitats USING (assessment_area_id, habitat_type_id)
;


/*
 * wms_province_areas_view
 * -----------------------
 * WMS view for showing the province boundaries.
 */
CREATE OR REPLACE VIEW wms_province_areas_view AS
SELECT
	province_area_id,
	name,
	authority_id,
	geometry

	FROM province_areas
;


/*
 * wms_extra_assessment_hexagons_view
 * ----------------------------------
 * WMS view returning the extra assessment hexagons within an assessment area.
 * Use at least 'assessment_area_id' in the where-clause, optionally 'habitat_type_id'.
 */
CREATE OR REPLACE VIEW wms_extra_assessment_hexagons_view AS
SELECT
	receptor_id,
	assessment_area_id,
	habitat_type_id,
	zoom_level,
	geometry

	FROM extra_assessment_receptors
		INNER JOIN hexagons USING (receptor_id)
;


/*
 * wms_extra_assessment_hexagons_sensitivity_view
 * ----------------------------------------------
 * WMS view returning extra assessment hexagons including critical deposition classification and relevance.
 */
CREATE OR REPLACE VIEW wms_extra_assessment_hexagons_sensitivity_view AS
SELECT
	receptor_id,
	assessment_area_id,
	habitat_type_id,
	critical_deposition,
	ae_critical_deposition_classification(critical_deposition) AS critical_deposition_classification,
	zoom_level,
	geometry

	FROM extra_assessment_receptors
		INNER JOIN habitat_type_critical_depositions_view USING (habitat_type_id)
		INNER JOIN hexagons USING (receptor_id)
;
