/*
 * habitat_type_sensitivity_view
 * -----------------------------
 * View returning the (nitrogen) sensitiveness of a habitat type.
 */
CREATE OR REPLACE VIEW habitat_type_sensitivity_view AS
SELECT
	habitat_type_id,
	bool_or(sensitive) AS sensitive

	FROM habitat_type_critical_levels

	GROUP BY habitat_type_id
;


/*
 * habitat_type_critical_depositions_view
 * --------------------------------------
 * View returning the critical deposition value (KDW) of a habitat type.
 */
CREATE OR REPLACE VIEW habitat_type_critical_depositions_view AS
SELECT
	habitat_type_id,
	critical_level AS critical_deposition,
	sensitive

	FROM habitat_type_critical_levels

	WHERE
		substance_id= 1711
		AND result_type = 'deposition'
;


/*
 * habitat_properties_view
 * -----------------------
 * View returning the properties of a habitat type in an assessment area.
 * This view uses the parent habitat type properties based on the habitat_type_relations table.
 */
CREATE OR REPLACE VIEW habitat_properties_view AS
SELECT
	assessment_area_id,
	habitat_type_id,
	quality_goal,
	extent_goal

	FROM habitat_properties
		INNER JOIN habitat_type_relations USING (goal_habitat_type_id)
;


/*
 * species_to_habitats_view
 * ------------------------
 * View returning for each species in which assessment area/habitat type they are present.
 * This view uses the parent habitat type based on the habitat_type_relations table.
 */
CREATE OR REPLACE VIEW species_to_habitats_view AS
SELECT
	species_id,
	assessment_area_id,
	habitat_type_id

	FROM species_to_habitats
		INNER JOIN habitat_type_relations USING (goal_habitat_type_id)
;


/*
 * designated_habitats_view
 * ------------------------
 * View to determine the designated habitat types per assessment area.
 * Relationships based on the habitat_type_relations are taken into account.
 */
CREATE OR REPLACE VIEW designated_habitats_view AS
SELECT
	assessment_area_id,
	habitat_type_id

	FROM habitat_properties_view

	WHERE NOT (quality_goal = 'none' AND extent_goal = 'none')
;


/*
 * designated_species_to_habitats_view
 * -----------------------------------
 * View to determine the designated (bird) species per habitat type and assessment area.
 * Relationships based on the habitat_type_relations are taken into account.
 */
CREATE OR REPLACE VIEW designated_species_to_habitats_view AS
SELECT
	species_id,
	assessment_area_id,
	habitat_type_id

	FROM species_to_habitats_view
		INNER JOIN designated_species USING (species_id, assessment_area_id)
;


/*
 * critical_deposition_areas_view
 * ------------------------------
 * View to collect the critical deposition areas with associated type, critical deposition value, and whether or not they are designated.
 * This includes both the set of all habitat areas and the set of relevant habitat areas.
 */
CREATE OR REPLACE VIEW critical_deposition_areas_view AS
SELECT
	assessment_area_id,
	'habitat'::critical_deposition_area_type AS type,
	habitat_type_id AS critical_deposition_area_id,
	name,
	description,
	FALSE AS relevant, -- These are NOT the relevant_habitats
	geometry

	FROM habitats
		INNER JOIN habitat_types USING (habitat_type_id)
UNION ALL
SELECT
	assessment_area_id,
	'relevant_habitat'::critical_deposition_area_type AS type,
	habitat_type_id AS critical_deposition_area_id,
	name,
	description,
	TRUE AS relevant, -- These are the relevant_habitats
	geometry

	FROM relevant_habitats
		INNER JOIN habitat_types USING (habitat_type_id)
;


/*
 * critical_deposition_area_critical_levels_view
 * ---------------------------------------------
 * View returning the critical deposition values for critical deposition areas.
 * This view only returns records for which the habitat type is set to be 'sensitive'.
 */
CREATE OR REPLACE VIEW critical_deposition_area_critical_levels_view AS
SELECT
	habitat_type_id AS critical_deposition_area_id,
	substance_id,
	result_type,
	critical_level

	FROM habitat_type_critical_levels

	WHERE sensitive = TRUE
;
