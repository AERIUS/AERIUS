/*
 * assessment_area_directive_view
 * ------------------------------
 * View returning per assessment area the directives (aggregated) and design status).
 * These are properties of the directive areas of the N2000 area.
 * This information is combinated into a N2000 area property.
 * As a result, this view only works for entire N2000 areas.
 */
CREATE OR REPLACE VIEW assessment_area_directive_view AS
SELECT
	natura2000_areas.assessment_area_id,
	natura2000_directive_areas.natura2000_area_id,
	array_to_string(array_agg(DISTINCT directive_code ORDER BY directive_code), ', ') AS directive_codes,
	array_to_string(array_agg(DISTINCT directive_name ORDER BY directive_name), ', ') AS directive,
	array_to_string(array_agg(DISTINCT natura2000_directive_areas.design_status_description), ', ') AS design_status_description

	FROM natura2000_directive_areas
		INNER JOIN (
			SELECT natura2000_directive_area_id, UNNEST(string_to_array(directive_code, ', ')) AS directive_code, UNNEST(string_to_array(directive_name, ', ')) AS directive_name
				FROM natura2000_directive_areas
					INNER JOIN natura2000_directives USING (natura2000_directive_id)
			) AS directives USING (natura2000_directive_area_id)
		INNER JOIN natura2000_areas USING (natura2000_area_id)

	GROUP BY natura2000_areas.assessment_area_id, natura2000_area_id
;


/*
 * natura2000_area_info_view
 * -------------------------
 * View returning general information about the N2000 areas.
 * Use 'assessment_area_id', 'natura2000_area_id' or ST_Intersects(ST_SetSRID(ST_Point(218928, 486793), ae_get_srid()), geometry) in the where-clause.
 */
CREATE OR REPLACE VIEW natura2000_area_info_view AS
SELECT
	assessment_area_id,
	natura2000_area_id,
	natura2000_areas.code,
	natura2000_areas.name,
	assessment_area_directive_view.directive,
	assessment_area_directive_view.directive_codes,
	assessment_area_directive_view.design_status_description,
	authorities.name AS authority,
	COALESCE(registered_surface, ROUND(ST_Area(natura2000_areas.geometry))::bigint) AS surface,
	Box2D(natura2000_areas.geometry) AS boundingbox,
	natura2000_areas.geometry

	FROM natura2000_areas
		INNER JOIN assessment_area_directive_view USING (assessment_area_id, natura2000_area_id)
		INNER JOIN authorities USING (authority_id)
		LEFT JOIN natura2000_area_properties USING (natura2000_area_id)
;


/*
 * habitats_view
 * -------------
 * View returning for an assessment area which habitattypes are contained within: all habitat ares that intersect with the assessment area.
 * The union of the geometries of all areas of the type is also returned.
 * Use 'assessment_area_id' in the where-clause.
 */
CREATE OR REPLACE VIEW habitats_view AS
SELECT
	assessment_area_id,
	habitat_types.habitat_type_id,
	habitat_types.name,
	habitat_types.description,
	(relevant_habitats.habitat_type_id IS NOT NULL) AS relevant,
	(designated_habitats_view.habitat_type_id IS NOT NULL) AS designated,

	habitats.habitat_coverage,
	ST_Area(habitats.geometry) AS surface, -- ingetekend totaal oppervlak
	ST_Area(habitats.geometry) * habitats.habitat_coverage AS cartographic_surface, -- gekarteerd totaal oppervlak
	habitats.geometry,

	(COALESCE(relevant_habitats.habitat_coverage, 0))::real AS relevant_habitat_coverage,
	(COALESCE(ST_Area(relevant_habitats.geometry), 0))::real AS relevant_surface, -- ingetekend relevant oppervlak
	(COALESCE(ST_Area(relevant_habitats.geometry) * relevant_habitats.habitat_coverage, 0))::real AS relevant_cartographic_surface,
	relevant_habitats.geometry AS relevant_geometry

	FROM assessment_areas
		INNER JOIN habitats USING (assessment_area_id)
		INNER JOIN habitat_types USING (habitat_type_id)
		LEFT JOIN relevant_habitats USING (assessment_area_id, habitat_type_id)
		LEFT JOIN designated_habitats_view USING (assessment_area_id, habitat_type_id)
;


/*
 * relevant_habitat_info_for_receptor_view
 * ---------------------------------------
 * General information about habitat areas that intersect with a hexagon.
 * The cartographic_surface is the cartographic surface (gekarteerde oppervlakte) of the habitat type that intersects with a hexagon.
 * The coverage of each individual habitat area is taken into account.
 * Use 'receptor_id' in the where-clause.
 *
 * Note: No intermediate table is used because all intermediate tables that we use so far are assessment_area based.
 * For receptors containing multiple assessment areas with the same habitat type this wosuld cause duplications.
 * This view is fast enough, as long as 'receptor_id' is used in the where-clause.
 */
CREATE OR REPLACE VIEW relevant_habitat_info_for_receptor_view AS
SELECT
	receptor_id,
	habitat_type_id,
	name,
	description,
	substance_id,
	result_type,
	critical_level,
	SUM(surface * receptor_habitat_coverage)::posreal AS cartographic_surface

	FROM receptors_to_relevant_habitats_view
		INNER JOIN habitat_types USING (habitat_type_id)
		INNER JOIN habitat_type_critical_levels USING (habitat_type_id)

	GROUP BY receptor_id, habitat_type_id, name, description, substance_id, result_type, critical_level
;


/*
 * habitat_info_for_assessment_area_view
 * -------------------------------------
 * View returning the habitat types within an assessment area, including properties like coverage, surface and goals.
 * Used in the info-popup in Calculator when hovering over habitat type.
 * Use 'assessment_area_id' in the where-clause.
 */
CREATE OR REPLACE VIEW habitat_info_for_assessment_area_view AS
SELECT
	assessment_area_id,
	habitat_type_id,
	name AS habitat_type_name,
	description AS habitat_type_description,
	designated,
	relevant,
	substance_id,
	result_type,
	critical_level,
	habitat_coverage,
	surface, -- ingetekend totaal oppervlak
	cartographic_surface, -- gekarteerd totaal oppervlak
	relevant_habitat_coverage,
	relevant_surface, -- ingetekend relevant oppervlak
	relevant_cartographic_surface, -- gekarteerd relevant oppervlak
	quality_goal,
	extent_goal

	FROM habitats_view
		INNER JOIN habitat_type_critical_levels USING (habitat_type_id)
		LEFT JOIN habitat_properties_view USING (habitat_type_id, assessment_area_id)
;


/*
 * extra_assessment_habitat_info_for_receptor_view
 * -----------------------------------------------
 * General information about extra assessment habitats for a hexagon.
 * Use 'receptor_id' in the where-clause.
 */
CREATE OR REPLACE VIEW extra_assessment_habitat_info_for_receptor_view AS
SELECT DISTINCT
	receptor_id,
	habitat_type_id,
	name,
	description,
	substance_id,
	result_type,
	critical_level

	FROM extra_assessment_receptors
		INNER JOIN habitat_types USING (habitat_type_id)
		INNER JOIN habitat_type_critical_levels USING (habitat_type_id)
;


/*
 * extra_assessment_habitat_info_for_assessment_area_view
 * ------------------------------------------------------
 * View returning the extra assesment habitat types within an assessment area, including properties like goals.
 * Used in the info-popup in Calculator when hovering over habitat type.
 * Use 'assessment_area_id' in the where-clause.
 */
CREATE OR REPLACE VIEW extra_assessment_habitat_info_for_assessment_area_view AS
SELECT
	assessment_area_id,
	habitat_type_id,
	name AS habitat_type_name,
	description AS habitat_type_description,
	substance_id,
	result_type,
	critical_level,
	quality_goal,
	extent_goal

	FROM extra_assessment_receptors
		INNER JOIN habitat_types USING (habitat_type_id)
		INNER JOIN habitat_type_critical_levels USING (habitat_type_id)
		LEFT JOIN habitat_properties_view USING (habitat_type_id, assessment_area_id)
;
