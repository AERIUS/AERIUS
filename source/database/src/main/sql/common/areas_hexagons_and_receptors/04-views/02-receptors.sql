/*
 * permit_required_receptors_view
 * ------------------------------
 * View returning the (sub)set of receptors that are relevant for the 'Wet Natuurbescherming' (OwN2000) calculation.
 * This includes all nitrogen-relevant receptors (included_receptors).
 */
CREATE OR REPLACE VIEW permit_required_receptors_view AS
SELECT
	receptor_id

	FROM included_receptors
;


/*
 * critical_depositions_view
 * -------------------------
 * View returing the critical deposition value (KDW) per receptor.
 * Each critical depostion area has a KDW.
 * To determine the KDW per receptor, all relevant critical deposition areas within the assessment area intersecting with the hexagon at zoom level 1 are determined.
 * From this selection the lowest (=most critical) KDW value is used per receptor, independent of the surface size of the intersection.
 */
CREATE OR REPLACE VIEW critical_depositions_view AS
SELECT
	receptor_id,
	MIN(critical_level) AS critical_deposition

	FROM critical_levels

	WHERE substance_id= 1711
		AND result_type = 'deposition'

	GROUP BY receptor_id
;
