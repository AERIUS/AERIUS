/*
 * receptors_to_habitats_view
 * --------------------------
 * View on receptors_to_critical_deposition_areas returning only the type 'habitat', using habitat_type_id.
 */
CREATE OR REPLACE VIEW receptors_to_habitats_view AS
SELECT
	assessment_area_id,
	critical_deposition_area_id AS habitat_type_id,
	receptor_id,
	surface,
	receptor_habitat_coverage

	FROM receptors_to_critical_deposition_areas

	WHERE type = 'habitat'
;


/*
 * receptors_to_relevant_habitats_view
 * -----------------------------------
 * View on receptors_to_critical_deposition_areas returning only the type 'relevant_habitat', using habitat_type_id.
 */
CREATE OR REPLACE VIEW receptors_to_relevant_habitats_view AS
SELECT
	assessment_area_id,
	critical_deposition_area_id AS habitat_type_id,
	receptor_id,
	surface,
	receptor_habitat_coverage

	FROM receptors_to_critical_deposition_areas

	WHERE type = 'relevant_habitat'
;


/*
 * receptors_to_assessment_areas_on_critical_deposition_area_view
 * --------------------------------------------------------------
 * View reducing receptors_to_critical_deposition_areas to a link between assessment areas and receptors.
 * This only includes receptors within a critical deposition area.
 * The type of critical deposition area must be filtered by 'type'.
 * Surface and weight are based on the assessment area/receptor relation (critical deposition areas are combined).
 */
CREATE OR REPLACE VIEW receptors_to_assessment_areas_on_critical_deposition_area_view AS
SELECT
	assessment_area_id,
	receptor_id,
	type,
	SUM(surface * receptor_habitat_coverage / 10000.0)::real AS weight,  --coverage meenemen
	SUM(surface) AS surface,
	SUM(surface * receptor_habitat_coverage) AS cartographic_surface

	FROM receptors_to_critical_deposition_areas

	GROUP BY assessment_area_id, receptor_id, type
;


/*
 * receptors_to_assessment_areas_on_relevant_habitat_view
 * ------------------------------------------------------
 * View similar as receptors_to_assessment_areas_on_critical_deposition_area_view, but only where type = 'relevant_habitat'.
 */
CREATE OR REPLACE VIEW receptors_to_assessment_areas_on_relevant_habitat_view AS
SELECT
	assessment_area_id,
	receptor_id,
	weight,
	surface,
	cartographic_surface

	FROM receptors_to_assessment_areas_on_critical_deposition_area_view

	WHERE type = 'relevant_habitat'
;
