/*
 * build_critical_levels
 * ---------------------
 * View to fill the critical_levels table.
 */
CREATE OR REPLACE VIEW setup.build_critical_levels AS
SELECT
	receptor_id,
	substance_id,
	result_type,
	MIN(critical_level) AS critical_level

	FROM receptors_to_critical_deposition_areas
		INNER JOIN critical_deposition_areas_view USING (assessment_area_id, type, critical_deposition_area_id)
		INNER JOIN habitat_type_critical_levels ON (critical_deposition_area_id = habitat_type_id)

	WHERE type = 'relevant_habitat'
		AND habitat_type_critical_levels.sensitive = TRUE
		AND critical_level IS NOT NULL

	GROUP BY receptor_id, substance_id, result_type
;
