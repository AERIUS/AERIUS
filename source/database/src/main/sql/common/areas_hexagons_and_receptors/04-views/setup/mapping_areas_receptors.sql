/*
 * build_receptors_to_assessment_areas_view
 * ----------------------------------------
 * View to fill the receptors_to_assessment_areas table.
 */
CREATE OR REPLACE VIEW setup.build_receptors_to_assessment_areas_view AS
SELECT
	assessment_area_id,
	(setup.ae_determine_hexagon_intersections(geometry)).receptor_id,
	(setup.ae_determine_hexagon_intersections(geometry)).surface

	FROM assessment_areas

	WHERE assessment_areas.type = 'natura2000_area'
;


/*
 * build_included_receptors_view
 * -----------------------------
 * View to fill the included_receptors table.
 */
CREATE OR REPLACE VIEW setup.build_included_receptors_view AS
SELECT DISTINCT
	receptor_id

	FROM receptors_to_critical_deposition_areas

	WHERE type = 'relevant_habitat'
UNION
SELECT DISTINCT
	receptor_id
	
	FROM extra_assessment_receptors
;


/*
 * build_receptors_to_critical_deposition_areas_view
 * -------------------------------------------------
 * View to fill the receptors_to_critical_deposition_areas table.
 *
 * The coverage of the habitat on each receptor is calculated by looking at the intersection of the habitat areas that intersect the receptor.
 * See {@link setup.ae_determine_habitat_coverage_on_hexagon} for the calculation method.
 */
CREATE OR REPLACE VIEW setup.build_receptors_to_critical_deposition_areas_view AS
SELECT
	assessment_area_id,
	type,
	critical_deposition_area_id,
	receptor_id,
	surface,
	setup.ae_determine_habitat_coverage_on_hexagon(assessment_area_id, type, critical_deposition_area_id, receptor_id) AS receptor_habitat_coverage

	FROM
	(SELECT
		assessment_area_id,
		type,
		critical_deposition_area_id,
		(setup.ae_determine_hexagon_intersections(geometry)).receptor_id,
		(setup.ae_determine_hexagon_intersections(geometry)).surface

		FROM critical_deposition_areas_view
	) AS mapping_receptor_cda
;


/*
 * build_hexagon_type_receptors_view
 * ---------------------------------
 * View to fill the hexagon_type_receptors table.
 */
CREATE OR REPLACE VIEW setup.build_hexagon_type_receptors_view AS
SELECT DISTINCT
	'relevant_hexagons'::hexagon_type AS hexagon_type,
	receptor_id

	FROM receptors_to_critical_deposition_areas

	WHERE type = 'relevant_habitat'
UNION ALL
SELECT DISTINCT
	'exceeding_hexagons'::hexagon_type AS hexagon_type,
	receptor_id

	FROM receptors_to_critical_deposition_areas
		LEFT JOIN non_exceeding_receptors USING (receptor_id)

	WHERE type = 'relevant_habitat'
		AND non_exceeding_receptors.receptor_id IS NULL
UNION ALL
SELECT DISTINCT
	'extra_assessment_hexagons'::hexagon_type AS hexagon_type,
	receptor_id

	FROM extra_assessment_receptors
;
