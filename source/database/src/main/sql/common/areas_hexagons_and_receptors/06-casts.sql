/*
 * Cast definition for land_use_classification to integer
 */
CREATE CAST (land_use_classification AS integer) WITH FUNCTION ae_enum_to_index(anyenum);


/*
 * Cast definition for integer to land_use_classification
 */
CREATE CAST (integer AS land_use_classification) WITH FUNCTION ae_integer_to_land_use_classification(integer);
