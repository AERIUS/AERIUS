/*
 * assessment_area_type
 * --------------------
 * The type of an assesment area.
 */
CREATE TYPE assessment_area_type AS ENUM
	('natura2000_area', 'natura2000_directive_area');


/*
 * critical_deposition_area_type
 * -----------------------------
 * The type of a critical deposition area.
 */
CREATE TYPE critical_deposition_area_type AS ENUM
	('relevant_habitat', 'habitat');


/*
 * habitat_goal_type
 * -----------------
 * The goal for surface and/or quality, in the context of habitat types:
 * =       level
 * >       increase
 * = (>)   increase while maintaining properly developed locations
 * <       decrease is allowed, in favor of another specific habitat type
 * = (<)   decrease is allowed in favor of another habitat type
 * > (<)   surface should be increased, but is allowed to decrease in favor of another habitat type
 *
 * Goal for habitat and/or populace, in the context of species, breeding birds, non-breeding birds:
 * =       level
 * >       increase/improvement
 * <       decrease is allowed
 * = (<)   decrease in favor of another species is allowed
 */
CREATE TYPE habitat_goal_type AS ENUM
	('specified', 'none', 'level', 'increase', 'level_increase', 'decrease', 'level_decrease', 'increase_may_decrease');


/*
 * land_use_classification
 * -----------------------
 * The typ of classification for land use.
 */
CREATE TYPE land_use_classification AS ENUM
	('grasland', 'bouwland', 'vaste gewassen', 'naaldbos', 'loofbos', 'water', 'bebouwing', 'overige natuur', 'kale grond');


/*
 * species_type
 * ------------
 * The type of a (animal) species which are present in a habitat.
 */
CREATE TYPE species_type AS ENUM
	('habitat_species', 'breeding_bird_species', 'non_breeding_bird_species');


/*
 * authority_type
 * --------------
 * The typ of an authority.
 * Be aware that the order of this enum also dictates how the entries in the authorities table are sorted in the UI (for example in a dropdown box).
 */
CREATE TYPE authority_type AS ENUM
	('unknown', 'province', 'ministry', 'foreign');


/*
 * design_status_type
 * ------------------
 * The type of the design status (status van een doelstelling) of a habitat type or species.
 *
 */
CREATE TYPE design_status_type AS ENUM
	('aanmelding', 'ontwerp', 'definitief', 'irrelevant');

/*
 * hexagon_type
 * ------------
 * The type of a hexagon, which is used for statistics of a calculation for example.
 */
CREATE TYPE hexagon_type AS ENUM
	('relevant_hexagons', 'exceeding_hexagons', 'above_cl_hexagons', 'extra_assessment_hexagons');
