{import_common 'constants/general_constants.sql'}

/**
 * Threshold value ('Drempelwaarde'). Only show results for receptors that have a deposition above this threshold.
 */
INSERT INTO constants (key, value) VALUES ('PRONOUNCEMENT_THRESHOLD_VALUE', '0.0');

/**
 * Default SRID.
 */
INSERT INTO constants (key, value) VALUES ('SRID', 27700);

/**
 * The boundary (box) for the calculation grid.
 */
INSERT INTO constants (key, value) VALUES ('CALCULATOR_GRID_BOUNDARY_BOX', 'POLYGON((-4000 4000,-4000 1222000,660000 1222000,660000 4000,-4000 4000))');

/**
 * The boundary of the calculation grid in Calculator. This is the inverse of the normal calculation boundary.
 */
INSERT INTO constants (key, value) VALUES ('CALCULATOR_BOUNDARY', 'POLYGON((-1673115.34565 -1203285.0926,-1673115.34565 2447055.1144,2345704.73095 2447055.1144,2345704.73095 -1203285.0926,-1673115.34565 -1203285.0926),(-4000 4000,-4000 1222000,660000 1222000,660000 4000,-4000 4000))');

/**
 * Surface of a zoom level 1 hexagon (in m^2)
 */
INSERT INTO constants (key, value) VALUES ('SURFACE_ZOOM_LEVEL_1', 40000);

/**
 * Number of zoom levels.
 */
INSERT INTO constants (key, value) VALUES ('MAX_ZOOM_LEVEL', 7);

/**
 * The geometry of interest area buffer (in meters).
 */
INSERT INTO constants (key, value) VALUES ('GEOMETRY_OF_INTEREST_BUFFER', 850);

/**
 * The organisation that manages the implementation of AERIUS.
 */
INSERT INTO constants (key, value) VALUES ('CUSTOMER', 'JNCC');
