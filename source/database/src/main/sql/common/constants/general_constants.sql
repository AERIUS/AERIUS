/**
 * Values in meters for converting a line/polygon to point sources.
 */
INSERT INTO constants (key, value) VALUES ('CONVERT_LINE_TO_POINTS_SEGMENT_SIZE', '25');
INSERT INTO constants (key, value) VALUES ('CONVERT_POLYGON_TO_POINTS_GRID_SIZE', '100');
INSERT INTO constants (key, value) VALUES ('CONVERT_INLAND_SHIPPING_LINE_TO_POINTS_SEGMENT_SIZE', '25');
