/*
 * shipping_inland_ship_direction_type
 * -----------------------------------
 * Enum defining the direction of travl of a ship. Upstream is from sea and downstream is towards sea.
 * Irrelevant kan be used to specify that there is no distinction between up- and downstream, for example when there is hardly any current.
 */
CREATE TYPE shipping_inland_ship_direction_type AS ENUM
	('upstream', 'downstream', 'irrelevant');
