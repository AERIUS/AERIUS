/*
 * shipping_inland_waterway_categories
 * -----------------------------------
 * Table containing all waterway categories in the inland shipping network.
 * @column flowing For some types the current or flow of the waterway is of importance, this is indicated by the flowing boolean.
 */
CREATE TABLE shipping_inland_waterway_categories
(
	shipping_inland_waterway_category_id integer NOT NULL,
	code text NOT NULL UNIQUE,
	name text NOT NULL UNIQUE,
	description text,
	flowing boolean NOT NULL,

	CONSTRAINT shipping_inland_waterway_categories_pkey PRIMARY KEY (shipping_inland_waterway_category_id)
);

/*
 * shipping_inland_waterways
 * -------------------------
 * table containing all original waterways in the inland shipping network.
 * Current or flow can be of importance when it comes to emission factors. This depends on the category of the waterway.
 * When current or flow is important, the geometry should be coded along the direction of the flow.
 * For instance the Waal: start point of the geometry is in Germany, end point of the geometry is near the North Sea.
 */
CREATE TABLE shipping_inland_waterways
(
	shipping_inland_waterway_id integer NOT NULL,
	shipping_inland_waterway_category_id integer NOT NULL,
	geometry geometry(LineString),

	CONSTRAINT shipping_inland_waterways_pkey PRIMARY KEY (shipping_inland_waterway_id),
	CONSTRAINT shipping_inland_waterways_fkey_categories FOREIGN KEY (shipping_inland_waterway_category_id) REFERENCES shipping_inland_waterway_categories
);

CREATE INDEX idx_shipping_inland_waterways_geometry_gist ON shipping_inland_waterways USING GIST (geometry);

/*
 * shipping_inland_locks
 * ---------------------
 * Table containing all locks (sluizen) in the network.
 * This table contains the polygon where the lock factor should be applied, not the geometry of the lock itself.
 */
CREATE TABLE shipping_inland_locks
(
	shipping_inland_lock_id integer NOT NULL,
	lock_factor posreal NOT NULL,
	geometry geometry(Polygon),

	CONSTRAINT shipping_inland_locks_pkey PRIMARY KEY (shipping_inland_lock_id)
);

CREATE INDEX idx_shipping_inland_locks_geometry_gist ON shipping_inland_locks USING GIST (geometry);
