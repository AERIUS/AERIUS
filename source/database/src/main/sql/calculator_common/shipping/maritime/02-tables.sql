/*
 * shipping_maritime_nodes
 * -----------------------
 * View containing the nodes for the maritime shipping network.
 * @column snappable Specifies if the node can be used by users to snap their route to when drawing.
 */
CREATE TABLE shipping_maritime_nodes
(
	shipping_node_id integer NOT NULL,
	snappable boolean NOT NULL,
	geometry geometry(Point),

	CONSTRAINT shipping_maritime_nodes_pkey PRIMARY KEY (shipping_node_id)
);

CREATE INDEX idx_shipping_maritime_nodes_geometry_gist ON shipping_maritime_nodes USING GIST (geometry);


/*
 * shipping_maritime_maneuver_areas
 * --------------------------------
 * Table containing maneuver areas (for example near locks) for maritime shipping.
 * The maneuver factor should be applied for emissions within this area.
 * Should the inherent maneuver section of a route intersect with this area, then the highest maneuver factor should be used.
 */
CREATE TABLE shipping_maritime_maneuver_areas
(
	shipping_maritime_maneuver_area_id integer NOT NULL,
	maneuver_factor posreal NOT NULL,
	geometry geometry(Polygon),

	CONSTRAINT shipping_maritime_maneuver_areas_pkey PRIMARY KEY (shipping_maritime_maneuver_area_id)
);

CREATE INDEX idx_shipping_maritime_maneuver_areas_geometry_gist ON shipping_maritime_maneuver_areas USING GIST (geometry);
