/*
 * ae_standalone_maritime_shipping_route
 * -------------------------------------
 * Function to determine the points for a maritime shipping route, both inland and on sea.
 * @param v_route The route travelled by ships.
 * @param v_max_segment_size The maximum segment size that a point can represent.
 * @return The collection of points representing the route.
 * For each point the distance it represents within the route is returned, as well as the maneuvring factor (based on maneuver areas).
 */
CREATE OR REPLACE FUNCTION ae_standalone_maritime_shipping_route(v_route geometry, v_max_segment_size double precision)
	RETURNS TABLE(point_geometry geometry, length posreal, maneuver_factor posreal) AS
$BODY$
DECLARE
	length_per_point posreal;

BEGIN
	CREATE TEMPORARY TABLE tmp_shipping_emission_points (point geometry) ON COMMIT DROP;
	INSERT INTO tmp_shipping_emission_points SELECT ae_line_to_point_sources(v_route, v_max_segment_size);
	-- length per point = total length / # points
	SELECT ST_Length(v_route) / COUNT(*) INTO length_per_point FROM tmp_shipping_emission_points;

	RETURN QUERY SELECT points.*, length_per_point, COALESCE(maneuver_area.maneuver_factor, 1::posreal)
		FROM tmp_shipping_emission_points AS points
			LEFT JOIN shipping_maritime_maneuver_areas AS maneuver_area ON ST_Intersects(points.point, maneuver_area.geometry);

	DROP TABLE tmp_shipping_emission_points;
	RETURN;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_maneuvering_maritime_shipping_route
 * --------------------------------------
 * Function to determine the points for a maritime shipping route where there is some sort of maneuvring.
 * @param v_route The route travelled by ships.
 * @param v_max_segment_size The maximum segment size that a point can represent.
 * @param v_maneuver_factor The default maneuver factor that should be used for the points.
 * @return The collection of points representing the route.
 * For each point the distance it represents within the route is returned, as well as the maneuvring factor.
 */
CREATE OR REPLACE FUNCTION ae_maneuvering_maritime_shipping_route(v_route geometry, v_max_segment_size double precision, v_maneuver_factor posreal)
  RETURNS TABLE(point_geometry geometry, length posreal, maneuver_factor posreal) AS
$BODY$
DECLARE
	length_per_point posreal;
BEGIN
	CREATE TEMPORARY TABLE tmp_shipping_emission_points_maneuver (point geometry) ON COMMIT DROP;
	INSERT INTO tmp_shipping_emission_points_maneuver SELECT ae_line_to_point_sources(v_route, v_max_segment_size);
	-- length per point = total length / # points
	SELECT ST_Length(v_route) / COUNT(*) INTO length_per_point FROM tmp_shipping_emission_points_maneuver;
	RETURN QUERY SELECT
			points.*,
			length_per_point,
			GREATEST(v_maneuver_factor, COALESCE(maneuver_area.maneuver_factor, 0::posreal))
		FROM tmp_shipping_emission_points_maneuver AS points
			LEFT JOIN shipping_maritime_maneuver_areas AS maneuver_area ON ST_Intersects(points.point, maneuver_area.geometry);

	DROP TABLE tmp_shipping_emission_points_maneuver;
	RETURN;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_maritime_mooring_inland_shipping_route
 * -----------------------------------------
 * Function to determine the points of a inland shipping route in the case of mooring at a dock.
 * This function includes using a maneuver factor for the first section from the dock.
 * @param transfer_route The route the ship travels to arrive at the harbor entrance or at another dock.
 * @param v_max_segment_size The maximum segment size that a point can represent.
 * @param v_maneuver_length The length of the route where a maneuver factor should be applied when mooring at a dock.
 * @param v_maneuver_factor The factor that should be used when maneuvering from/to a dock.
 * @param v_mooring_on_a Boolean indicating if the start point of the route is at a dock (at the A side)
 * @param v_mooring_on_b Boolean indicating if the end point of the route is at a dock (at the B side)
 * @return The collection of points that represents the route.
 * For each point the distance it represents within the route is returned, as well as the maneuvring factor (based on both maneuver areas and maneuver part at a dock).
 */
CREATE OR REPLACE FUNCTION ae_maritime_mooring_inland_shipping_route(transfer_route geometry, v_max_segment_size double precision, v_maneuver_length posreal, v_maneuver_factor posreal, v_mooring_on_a boolean, v_mooring_on_b boolean)
  RETURNS TABLE(point_geometry geometry, length posreal, maneuver_factor posreal) AS
$BODY$
DECLARE
	remaining_part geometry;
	maneuver_a_part geometry;
	maneuver_a_fraction posreal;
	maneuver_b_part geometry;
	maneuver_b_fraction posreal;
BEGIN
	-- First determine the maneuvering partion of the route from side A.
	SELECT CASE WHEN v_maneuver_factor = 1 OR NOT v_mooring_on_a THEN 0 ELSE v_maneuver_length / ST_Length(transfer_route) END INTO maneuver_a_fraction;

	IF maneuver_a_fraction = 0 THEN
		SELECT transfer_route INTO remaining_part;
	ELSEIF maneuver_a_fraction > 1 THEN
		SELECT transfer_route INTO maneuver_a_part;
	ELSE
		SELECT ST_LineSubstring(transfer_route, 0, maneuver_a_fraction) INTO maneuver_a_part;
		SELECT ST_LineSubstring(transfer_route, maneuver_a_fraction, 1) INTO remaining_part;
	END IF;

	-- Then determine the maneuvering partion of the route from side B if there is any remaining part.
	IF remaining_part IS NOT NULL THEN
		SELECT CASE WHEN v_maneuver_factor = 1 OR NOT v_mooring_on_b THEN 0 ELSE v_maneuver_length / ST_Length(remaining_part) END INTO maneuver_b_fraction;

		IF maneuver_b_fraction = 0 THEN
			-- Basically no change, keep the remaining part as it is
			SELECT remaining_part INTO remaining_part;
		ELSEIF maneuver_b_fraction > 1 THEN
			-- Full remaining part is maneuvering, be sure to set remaining part to null
			SELECT remaining_part INTO maneuver_b_part;
			SELECT NULL INTO remaining_part;
		ELSE
			SELECT ST_LineSubstring(remaining_part, 1 - maneuver_b_fraction, 1) INTO maneuver_b_part;
			SELECT ST_LineSubstring(remaining_part, 0, 1 - maneuver_b_fraction) INTO remaining_part;
		END IF;
	END IF;

	IF maneuver_a_part IS NOT NULL THEN
		RETURN QUERY SELECT * FROM ae_maneuvering_maritime_shipping_route(maneuver_a_part, v_max_segment_size, v_maneuver_factor);
	END IF;

	IF remaining_part IS NOT NULL THEN
		RETURN QUERY SELECT * FROM ae_standalone_maritime_shipping_route(remaining_part, v_max_segment_size);
	END IF;

	IF maneuver_b_part IS NOT NULL THEN
		RETURN QUERY SELECT * FROM ae_maneuvering_maritime_shipping_route(maneuver_b_part, v_max_segment_size, v_maneuver_factor);
	END IF;

	RETURN;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_category_maritime_mooring_inland_shipping_route
 * --------------------------------------------------
 * Function to determine the points of a inland shipping route in the case of mooring at a dock for a specificy maritime shipping category.
 * These points can be used directly as emission sources.
 * @param v_category_id The ID of the maritime shipping category for which to determine the route, including the maneuvring section(s).
 * @param transfer_route The route the ship travels to arrive at the harbor entrance or at another dock.
 * @param v_max_segment_size The maximum segment size that a point can represent.
 * @param v_mooring_on_a Boolean indicating if the start point of the route is at a dock (at the A side)
 * @param v_mooring_on_b Boolean indicating if the end point of the route is at a dock (at the B side)
 * @return The collection of points that represents the route.
 * For each point the distance it represents within the route is returned, as well as the maneuvring factor for the supplied maritime shipping category.
 */
CREATE OR REPLACE FUNCTION ae_category_maritime_mooring_inland_shipping_route(v_category_id int, transfer_route geometry, v_max_segment_size double precision, v_mooring_on_a boolean, v_mooring_on_b boolean)
	RETURNS TABLE(point_geometry geometry, length posreal, maneuver_factor posreal) AS
$BODY$
DECLARE
	maneuver_length posreal;
	maneuver_factor posreal;
BEGIN
	SELECT category.maneuver_length, category.maneuver_factor INTO maneuver_length, maneuver_factor
		FROM shipping_maritime_category_maneuver_properties AS category
		WHERE category.shipping_maritime_category_id = v_category_id;

	RETURN QUERY SELECT * FROM ae_maritime_mooring_inland_shipping_route(transfer_route, v_max_segment_size, maneuver_length, maneuver_factor, v_mooring_on_a, v_mooring_on_b);
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_custom_maritime_mooring_inland_shipping_route
 * ------------------------------------------------
 * Function to determine the points of a inland shipping route in the case of mooring at a dock based on bruto tonnage of a ship.
 * These points can be used directly as emission sources.
 * @param v_tonnage The bruto tonnage of th ship for which to determine the points, including the maneuvering section.
 * @param transfer_route The route the ship travels to arrive at the harbor entrance or at another dock.
 * @param v_max_segment_size The maximum segment size that a point can represent.
 * @param v_mooring_on_a Boolean indicating if the start point of the route is at a dock (at the A side)
 * @param v_mooring_on_b Boolean indicating if the end point of the route is at a dock (at the B side)
 * @return The collection of points that represents the route.
 * For each point the distance it represents within the route is returned, as well as the maneuvring factor for the supplied tonnage.
 */
CREATE OR REPLACE FUNCTION ae_custom_maritime_mooring_inland_shipping_route(v_tonnage int, transfer_route geometry, v_max_segment_size double precision, v_mooring_on_a boolean, v_mooring_on_b boolean)
	RETURNS TABLE(point_geometry geometry, length posreal, maneuver_factor posreal) AS
$BODY$
DECLARE
	v_tonnage_category_id integer;
	maneuver_length posreal;
	maneuver_factor posreal;
BEGIN
	v_tonnage_category_id := tonnage_category_id FROM shipping_maritime_mooring_maneuver_factors WHERE tonnage_lower_threshold < v_tonnage ORDER BY tonnage_lower_threshold DESC LIMIT 1;

	SELECT mooring_maneuver_factors.maneuver_length, mooring_maneuver_factors.maneuver_factor INTO maneuver_length, maneuver_factor
		FROM shipping_maritime_mooring_maneuver_factors AS mooring_maneuver_factors
		WHERE tonnage_category_id = v_tonnage_category_id;

	RETURN QUERY SELECT * FROM ae_maritime_mooring_inland_shipping_route(transfer_route, v_max_segment_size, maneuver_length, maneuver_factor, v_mooring_on_a, v_mooring_on_b);
END;
$BODY$
LANGUAGE plpgsql VOLATILE;
