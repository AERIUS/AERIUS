/*
 * wms_shipping_maritime_network_view
 * ----------------------------------
 * WMS view returning the maritime shipping network. Can return both main routes and nodes to snap to.
 */
CREATE OR REPLACE VIEW wms_shipping_maritime_network_view AS
SELECT
	false AS is_line,
	geometry

	FROM shipping_maritime_nodes
	WHERE snappable = true

;
