/**
 * ae_trigger_job_status_update
 * ----------------------------
 * Trigger to add a new log entry when the status of a job changes.
 */
CREATE TRIGGER ae_trigger_job_status_update AFTER INSERT OR UPDATE ON jobs.jobs
	FOR EACH ROW
	EXECUTE FUNCTION jobs.ae_log_job_state_change();
