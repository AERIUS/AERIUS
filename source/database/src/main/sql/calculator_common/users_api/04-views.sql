/*
 * job_progress_view
 * -----------------
 * View to return the progress of all jobs of users.
 * Also returns progress properties like state, start time, etc.
 */
CREATE OR REPLACE VIEW jobs.job_progress_view AS
SELECT
	job_id,
	user_id,
	type,
	key,
	name,
	protected,
	state,
	jobs.ae_job_queue_position(job_id, state) AS queue_position,
	jobs.ae_job_state_timestamp(job_id, 'created') AS start_time,
	jobs.ae_job_state_timestamp(job_id, 'calculating') AS calculating_time,
	jobs.ae_job_end_time(job_id) AS end_time,
	number_of_points,
	number_of_points_calculated,
	result_url,
	error_message

	FROM jobs.jobs
;
