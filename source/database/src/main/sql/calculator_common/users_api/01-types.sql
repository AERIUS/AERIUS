/*
 * job_state_type
 * --------------
 * Enum specifying the state of a job.
 */
CREATE TYPE job_state_type AS ENUM
  ('created', 'queuing', 'preparing', 'calculating', 'post_processing', 'cancelled', 'completed', 'deleted', 'error');


/*
 * job_type
 * --------
 * Enum specifying the type of a job.
 */
CREATE TYPE job_type AS ENUM
	('calculation', 'report', 'insert_results', 'import_summary');


/*
 * situation_type
 * --------------
 * Enym specifying the type of a calculation.
 */
CREATE TYPE situation_type AS ENUM
	('combination_proposed', 'combination_reference', 'off_site_reduction', 'proposed', 'reference', 'temporary');
