/*
 * ae_delete_job
 * -------------
 * Function to use when a job can be removed, including related calculation results.
 *
 * @param v_job_id ID of the job to remove.
 * @param v_force Boolean to indicate that the job should be removed, even if it was protected.
 */
CREATE OR REPLACE FUNCTION jobs.ae_delete_job(v_job_id integer, v_force boolean)
	RETURNS void AS
$BODY$
BEGIN
	IF EXISTS(SELECT protected FROM jobs.jobs WHERE job_id = v_job_id AND (protected = FALSE OR v_force = TRUE))
		THEN
			PERFORM jobs.ae_delete_job_calculations(v_job_id, v_force);

			DELETE FROM jobs.jobs WHERE job_id = v_job_id;
	END IF;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_delete_job_calculations
 * --------------------------
 * Function to use to only delete the calculations related to a job.
 * The job itself  (and its progress) is retained.
 *
 * @param v_job_id The ID of the job for which to delete calculation results.
 * @param v_force Boolean to indicate that results should be removed, even if the job was protected.
 */
CREATE OR REPLACE FUNCTION jobs.ae_delete_job_calculations(v_job_id integer, v_force boolean)
	RETURNS boolean AS
$BODY$
DECLARE
	v_calculation_id integer;
BEGIN
	IF EXISTS(SELECT protected FROM jobs.jobs WHERE job_id = v_job_id AND (protected = FALSE OR v_force = TRUE))
		THEN
			FOR v_calculation_id IN
				(SELECT calculation_id FROM jobs.job_calculations WHERE job_id = v_job_id)
			LOOP
				-- First delete results before job_calculations because deleting calculations depends on number
				-- of job_calculations pointing to same calculation_id, and if more job_calculations point to the
				-- same calculation id the results should not be deleted.
				PERFORM jobs.ae_delete_calculation(v_calculation_id);
				DELETE FROM jobs.job_calculations WHERE job_id = v_job_id AND calculation_id = v_calculation_id;
			END LOOP;
			RETURN TRUE;
	ELSE
		RETURN FALSE;
	END IF;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_delete_user_calculation_point_set_and_points
 * -----------------------------------------------
 * Function to delete the user-private calculation point set, including the related points.
 *
 * @param v_set_id The ID of the receptor set to remove.
 */
CREATE OR REPLACE FUNCTION users.ae_delete_user_calculation_point_set_and_points(v_set_id integer)
	RETURNS void AS
$BODY$
BEGIN
	DELETE FROM users.user_calculation_points WHERE user_calculation_point_set_id = v_set_id;
	DELETE FROM users.user_calculation_point_sets WHERE user_calculation_point_set_id = v_set_id;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_update_job_status
 * --------------------
 * Function to alter the state of a job.
 * This function ensures that some status changes are not possible.
 * As an example: when the state is already 'completed', then the state can no longer be changed to 'cancelled'.
 * Incorrect status changes do not trigger an exception, but are not executed.
 *
 * @param v_job_id The ID of the job for which to change the state.
 * @param v_job_status The state to change the job to.
 * @returns true if status was updated
 */
CREATE OR REPLACE FUNCTION jobs.ae_update_job_status(v_job_key text, v_job_status job_state_type)
	RETURNS bool AS
$BODY$
DECLARE
	row_count int;
BEGIN
	UPDATE jobs.jobs
		SET state = v_job_status
		WHERE key = v_job_key
			AND ((state IN ('created', 'queuing', 'preparing', 'calculating', 'post_processing') AND v_job_status <> 'deleted'::job_state_type)
				OR (v_job_status = 'deleted'::job_state_type AND NOT protected));
	GET DIAGNOSTICS row_count = ROW_COUNT;
	RETURN row_count > 0;
END
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_log_job_state_change
 * -----------------------
 * Trigger function to add a state change to the job_progress_log
 */
CREATE OR REPLACE FUNCTION jobs.ae_log_job_state_change()
	RETURNS TRIGGER AS
$BODY$
BEGIN
		IF (TG_OP = 'INSERT' OR (TG_OP = 'UPDATE' AND OLD.state IS DISTINCT FROM NEW.state)) THEN
			INSERT INTO jobs.job_progress_log(job_id, state)
			VALUES (NEW.job_id, NEW.state);
		END IF;
		RETURN NEW;
END
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_scenario_get_calculation_id_of_situation_type
 * ------------------------------------------------
 * Function to obtain the calculation ID of a calculation within a specific job.
 * The calculation is determined based on the supplied situation type, and only 1 ID will be returned.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_get_calculation_id_of_situation_type(v_job_key text, v_situation_type situation_type)
	RETURNS int AS
$BODY$
	SELECT COALESCE(
		(
		SELECT calculation_id
		FROM jobs.job_calculations
			INNER JOIN jobs.jobs USING (job_id)
		WHERE key = v_job_key
			AND situation_type = v_situation_type
		LIMIT 1
		), 0)
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_get_calculation_ids_of_situation_type
 * -------------------------------------------------
 * Function to obtain the calculation IDs of calculations within a job.
 * The calculations are determined based on the supplied situation type.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_get_calculation_ids_of_situation_type(v_job_key text, v_situation_type situation_type)
	RETURNS int[] AS
$BODY$
	SELECT ARRAY(
		SELECT calculation_id
		FROM jobs.job_calculations
			INNER JOIN jobs.jobs USING (job_id)
		WHERE key = v_job_key
			AND situation_type = v_situation_type
		)
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_get_calculation_id_of_situation_reference
 * -----------------------------------------------------
 * Function to obtain the calculation ID of a situation within a job.
 * The calculation is determined based on the supplied situation reference, which should be unique within the job.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_get_calculation_id_of_situation_reference(v_job_key text, v_situation_reference text)
	RETURNS int AS
$BODY$
	SELECT COALESCE(
		(
		SELECT calculation_id
		FROM jobs.job_calculations
			INNER JOIN jobs.jobs USING (job_id)
		WHERE key = v_job_key
			AND situation_reference = v_situation_reference
		LIMIT 1
		), 0)
$BODY$
LANGUAGE SQL STABLE;

/*
 * ae_job_queue_position
 * ---------------------
 * Returns the position in the 'queue' for the given job_id, but if the job is not queuing it returns 0.
 * Position is determined by the number of jobs that have a lower job id, and are in state queuing.
 */
CREATE OR REPLACE FUNCTION jobs.ae_job_queue_position(v_job_id int, v_state job_state_type)
	RETURNS int AS
$BODY$
	SELECT CASE
		WHEN v_state <> 'queuing' OR v_state IS NULL THEN 0
		ELSE (SELECT count(job_id) FROM jobs.jobs WHERE job_id < v_job_id AND state = 'queuing')	
	END AS queue_position;
$BODY$
LANGUAGE SQL STABLE;

/**
 * ae_job_state_timestamp
 * ----------------------
 * Returns the last known timestamp for the given state.
 */
CREATE OR REPLACE FUNCTION jobs.ae_job_state_timestamp(v_job_id int, v_state job_state_type)
	RETURNS timestamp AS
$BODY$
	SELECT state_time
	FROM jobs.job_progress_log
	WHERE job_id = v_job_id
		AND state = v_state
	ORDER BY state_time DESC
	LIMIT 1;
$BODY$
LANGUAGE SQL STABLE;

/**
 * ae_job_end_time
 * ---------------
 * Returns the job end time if the job has ended or else returns nothing
 */
CREATE OR REPLACE FUNCTION jobs.ae_job_end_time(v_job_id int)
	RETURNS timestamp AS
$BODY$
	SELECT state_time
	FROM jobs.job_progress_log
	WHERE job_id = v_job_id
		AND state IN ('completed', 'cancelled', 'deleted', 'error')
	ORDER BY state_time DESC
	LIMIT 1;
$BODY$
LANGUAGE SQL STABLE;
