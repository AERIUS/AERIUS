/*
 * users
 * -----
 * Table containing the users for Connect. Each user is uniquely identified by email address or API-key.
 *
 * @column api_key The API-key is intended for users that do calculations through the Connect webservice.
 * Used by the system to determine the correct user_id.
 * @column max_concurrent_jobs The maximum number of concurrent jobs that the user is allowed to run.
 * @column period_job_rate_limit The maximum number of jobs that can be created per period (for example a day, system specific).
 */
CREATE TABLE users.users (
	user_id serial NOT NULL,
	api_key text NOT NULL,
	email_address text NOT NULL CHECK(email_address = lower(email_address)),
	enabled boolean NOT NULL DEFAULT TRUE,
	max_concurrent_jobs posint NOT NULL,
	period_job_rate_limit posint NULL,

	CONSTRAINT users_pkey PRIMARY KEY (user_id),
	CONSTRAINT users_unique_api_key UNIQUE (api_key),
	CONSTRAINT users_unique_email_address UNIQUE (email_address)
);


/*
 * user_calculation_point_sets
 * ---------------------------
 * Table containing user-private calculation point sets for Connect.
 *
 * A user can add private sets of calculation points, remove them or retrieve them in a list.
 * These sets can be used when calculating by specifying the name of the set through Connect.
*/
CREATE TABLE users.user_calculation_point_sets (
	user_calculation_point_set_id serial NOT NULL,
	user_id integer NOT NULL,
	name text NOT NULL,
	description text,

	CONSTRAINT user_calculation_point_sets_pkey PRIMARY KEY (user_calculation_point_set_id),
	CONSTRAINT user_calculation_point_sets_fkey_users FOREIGN KEY (user_id) REFERENCES users.users,
	CONSTRAINT user_calculation_point_sets_name_unique UNIQUE (user_id, name)
);


/*
 * user_calculation_points
 * -----------------------
 * Table containing the points within user-private calculation point sets.
 * A calculation point is identified by the combination of the calculation point set ID and a point ID. The point ID is unique within the context of a set.
 * @column nearest_receptor_id The ID of the nearest receptor in the default hexagon grid.
 * This ID is used to quickly assign terrain properties (roughness and such) when those were not supplied.
 */
CREATE TABLE users.user_calculation_points (
	user_calculation_point_set_id integer NOT NULL,
	user_calculation_point_id integer NOT NULL,
	label text,
	nearest_receptor_id integer NOT NULL,
	geometry geometry(Point,28992),
	average_roughness real,
	dominant_land_use integer,
	land_uses integer ARRAY[9],
	height real,
	class_name text,

 	CONSTRAINT user_calculation_points_pkey PRIMARY KEY (user_calculation_point_set_id, user_calculation_point_id),
 	CONSTRAINT user_calculation_points_fkey_user_calculation_point_sets FOREIGN KEY (user_calculation_point_set_id) REFERENCES users.user_calculation_point_sets
 );


/*
 * jobs
 * ----
 * Table containing (calculation) jobs that can be processed by a worker.
 * A job usually is the combination of some situations to be calculated or exported.
 * The job key is generated so the user has something to track the job by, and it's used to keep track of the job when the worker processes it.
 *
 * In the case of Connect, the user that created the job is tracked. Multiple jobs can be connected to the same user.
 *
 * @column user_id Optional id of the user when the user is logged in (e.g. used an api key).
 * @column key Unique identifier for the job.
 * @column name User given name of the job.
 * @column protected If true job should not be scheduled for regular cleanup.
 * @column type Type of job.
 * @column state Progress state of the job.
 * @column number_of_points Number of points(hexagons(representing hectares) and/or custom points) that need to be calculated.
 * @column number_of_points_calculated Number of points(hexagons(representing hectares) and/or custom points) that need have been calculated.
 * @column result_url URL to the file where calculation results can be found. NULL when job is not complete yet, or when there is no such file.
 * @column error_message Error message in case the job was stopped with an error.
 */
CREATE TABLE jobs.jobs (
	job_id serial NOT NULL,
	user_id integer,
	type job_type NOT NULL,
	key text NOT NULL,
	name text,
	protected boolean NOT NULL DEFAULT FALSE,
	state job_state_type NOT NULL DEFAULT 'created'::job_state_type,
	number_of_points integer NOT NULL DEFAULT 0,
	number_of_points_calculated posreal NOT NULL DEFAULT 0,
	result_url text,
	error_message text,

	CONSTRAINT jobs_pkey PRIMARY KEY (job_id),
	CONSTRAINT jobs_fkey_users FOREIGN KEY (user_id) REFERENCES users.users
);

CREATE UNIQUE INDEX idx_jobs_key ON jobs.jobs(key);
CREATE INDEX idx_jobs_user_id ON jobs.jobs(user_id);


/*
 * job_calculations
 * ----------------
 * Table linking (1:N) calculations and jobs.
 * A job can be linked to multiple calculations.
 */
CREATE TABLE jobs.job_calculations (
	job_id integer NOT NULL,
	calculation_id integer NOT NULL,
	situation_reference text,
	situation_type situation_type,
	situation_name text,
	situation_version text,

	CONSTRAINT job_calculations_pkey PRIMARY KEY (job_id, calculation_id),
	CONSTRAINT job_calculations_fkey_jobs FOREIGN KEY (job_id) REFERENCES jobs.jobs ON DELETE CASCADE,
	CONSTRAINT job_calculations_fkey_calculations FOREIGN KEY (calculation_id) REFERENCES jobs.calculations
);

CREATE INDEX idx_job_calculations_job_calculation_id ON jobs.job_calculations(calculation_id);


/*
 * job_progress_log
 * ----------------
 * Table to log the progress of a job by storing the timestamp with each state change.
 *
 * @column state_time Timestamp when the state of a job changed.
 * @column state The state that the job was changed to.
 *  */
CREATE TABLE jobs.job_progress_log (
	job_id integer NOT NULL,
	state_time timestamp with time zone NOT NULL default now(),
	state job_state_type NOT NULL,

	CONSTRAINT job_progress_log_pkey PRIMARY KEY (job_id, state_time, state),
	CONSTRAINT job_progress_log_fkey_jobs FOREIGN KEY (job_id) REFERENCES jobs.jobs ON DELETE CASCADE
);
