/*
 * wms_background_results_view
 * ---------------------------
 * WMS view returning background results (concentrations and depositions) for hexagons.
 */
CREATE OR REPLACE VIEW wms_background_results_view AS
SELECT
	year,
	emission_result_type,
	substance_id,
	receptor_id,
	result,
	zoom_level,
	geometry

	FROM receptor_background_results_view
		INNER JOIN hexagons USING (receptor_id)
;
