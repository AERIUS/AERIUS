/*
 * receptor_background_results_view
 * --------------------------------
 * View returning the background values for result type/substance combinations on receptor level for all years.
 *
 * Use at least 'year' in the where-clause.
 */
CREATE OR REPLACE VIEW receptor_background_results_view AS
SELECT
	receptor_id,
	year,
	emission_result_type,
	substance_id,
	result

	FROM receptor_background_results
		INNER JOIN target_background_years USING (background_year, emission_result_type, substance_id)
;
