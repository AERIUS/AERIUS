/*
 * background_years
 * ----------------
 * Table containing the background year(s).
 */
CREATE TABLE background_years (
	background_year year_type NOT NULL,

	CONSTRAINT background_years_pkey PRIMARY KEY (background_year)
);


/*
 * target_background_years
 * -----------------------
 * Table specifying which background year to use values from when looking at a year.
 * This can be different for different result type/substance combinations.
 */
CREATE TABLE target_background_years (
	year year_type NOT NULL,
	emission_result_type emission_result_type NOT NULL,
	substance_id smallint NOT NULL,
	background_year year_type NOT NULL,

	CONSTRAINT target_background_years_pkey PRIMARY KEY (year, emission_result_type, substance_id),
	CONSTRAINT target_background_years_fkey_background_years FOREIGN KEY (background_year) REFERENCES background_years,
	CONSTRAINT target_background_years_fkey_substances FOREIGN KEY (substance_id) REFERENCES substances
);


/*
 * receptor_background_results
 * ---------------------------
 * Table containing background results (concentrations, depositions, etc) per background year and per receptor.
 *
 * This table does not use a foreign key to the receptors table, as this can be a full grid covering map.
 */
CREATE TABLE receptor_background_results (
	background_year year_type NOT NULL,
	emission_result_type emission_result_type NOT NULL,
	substance_id smallint NOT NULL,
	receptor_id integer NOT NULL,
	result posreal NOT NULL,

	CONSTRAINT receptor_background_results_pkey PRIMARY KEY (background_year, emission_result_type, substance_id, receptor_id),
	CONSTRAINT receptor_background_results_fkey_background_years FOREIGN KEY (background_year) REFERENCES background_years,
	CONSTRAINT receptor_background_results_fkey_substances FOREIGN KEY (substance_id) REFERENCES substances
);

CREATE INDEX idx_receptor_background_results ON receptor_background_results (receptor_id, emission_result_type, substance_id, background_year);
