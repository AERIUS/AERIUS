/*
 * ae_delete_calculation
 * ---------------------
 * Function to use when calculation + results have to be removed.
 * 'ON DELETE CASCADE' is added to the foreign key constraints of the following tables:
 * calculation_results, calculation_result_sets, calculation_point results en calculation_points.
 * This will ensure that all records for the supplied calculation_id will be removed, without any foreign key constraint violations.
 * If the calculation is the only one using the calculation_point_set, that set and the related calculation_points will also be removed.
 */
CREATE OR REPLACE FUNCTION jobs.ae_delete_calculation(v_calculation_id integer)
	RETURNS void AS
$BODY$
DECLARE
	only_calculation_in_set boolean;
	no_more_than_1_reference boolean;
	v_calculation_point_set_id integer;
BEGIN
	no_more_than_1_reference := COUNT(*) < 2 FROM jobs.job_calculations WHERE calculation_id = v_calculation_id;

	IF no_more_than_1_reference THEN
		v_calculation_point_set_id := calculation_point_set_id FROM jobs.calculations WHERE calculation_id = v_calculation_id;

		only_calculation_in_set := COUNT(*) = 1 FROM jobs.calculations WHERE calculation_point_set_id = v_calculation_point_set_id;

		IF EXISTS(SELECT table_name FROM information_schema.tables WHERE table_name = 'job_calculations')
			THEN
				DELETE FROM jobs.job_calculations WHERE calculation_id = v_calculation_id;
		END IF;

		DELETE FROM jobs.calculations WHERE calculation_id = v_calculation_id;

		IF only_calculation_in_set THEN
			DELETE FROM jobs.calculation_point_sets WHERE calculation_point_set_id = v_calculation_point_set_id;
		END IF;
	END IF;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;
