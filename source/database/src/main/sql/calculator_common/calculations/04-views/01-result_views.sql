/*
 * calculation_results_view
 * ------------------------
 * View returning all emission results (concentrations, depositions and exceedance days) per calculation and receptor.
 * Use 'calculation_id' or 'calculation_result_set_id', 'receptor_id', 'substance_id' and 'result_type' in the where-clause.
 */
CREATE OR REPLACE VIEW jobs.calculation_results_view AS
SELECT
	calculation_id,
	calculation_result_set_id,
	receptor_id,
	substance_id,
	result_type,
	result

	FROM jobs.calculation_results
		INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)

	WHERE result_set_type = 'total'
;


/*
 * calculation_sector_results_view
 * -------------------------------
 * View returning all emission results (concentrations, depositions and exceedance days) per calculation, receptor and sector.
 * Use 'calculation_id', 'receptor_id', 'substance_id' and 'result_type' in the where-clause.
 */
CREATE OR REPLACE VIEW jobs.calculation_sector_results_view AS
SELECT
	calculation_id,
	receptor_id,
	substance_id,
	result_type,
	result_set_type_key AS sector_id,
	result

	FROM jobs.calculation_results
		INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)

	WHERE result_set_type = 'sector'
;


/*
 * calculation_point_results_view
 * ------------------------------
 * View returning all emission results (concentrations, depositions and exceedance days) per calculation and custom point.
 * Use 'calculation_id' or 'calculation_result_set_id', 'receptor_id', 'substance_id' and 'result_type' in the where-clause.
 */
CREATE OR REPLACE VIEW jobs.calculation_point_results_view AS
SELECT
	calculation_id,
	calculation_result_set_id,
	calculation_point_id,
	label,
	substance_id,
	result_type,
	result,
	calculation_points.geometry

	FROM jobs.calculation_point_results
		INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
		INNER JOIN jobs.calculations USING (calculation_id)
		INNER JOIN jobs.calculation_points USING (calculation_point_set_id, calculation_point_id)

	WHERE result_set_type = 'total'
;


/*
 * calculation_point_sector_results_view
 * -------------------------------------
 * View returning all emission results (concentrations, depositions and exceedance days) per calculation, custom point and receptor.
 * Use 'calculation_id', 'receptor_id', 'substance_id' and 'result_type' in the where-clause.
 */
CREATE OR REPLACE VIEW jobs.calculation_point_sector_results_view AS
SELECT
	calculation_id,
	calculation_point_id,
	label,
	substance_id,
	result_type,
	result_set_type_key AS sector_id,
	result,
	calculation_points.geometry

	FROM jobs.calculation_point_results
		INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
		INNER JOIN jobs.calculations USING (calculation_id)
		INNER JOIN jobs.calculation_points USING (calculation_point_set_id, calculation_point_id)

	WHERE result_set_type = 'sector'
;


/*
 * calculation_sub_point_results_view
 * ----------------------------------
 * View returning all emission results (concentrations, depositions and exceedance days) per calculation and sub point.
 * Use 'calculation_id' or 'calculation_result_set_id', 'substance_id' and 'result_type' in the where-clause.
 */
CREATE OR REPLACE VIEW jobs.calculation_sub_point_results_view AS
SELECT
	calculation_id,
	calculation_result_set_id,
	calculation_sub_point_id,
	receptor_id,
	level,
	substance_id,
	result_type,
	result,
	calculation_sub_points.geometry

	FROM jobs.calculation_sub_point_results
		INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
		INNER JOIN jobs.calculations USING (calculation_id)
		INNER JOIN jobs.calculation_sub_points USING (calculation_sub_point_set_id, calculation_sub_point_id, receptor_id)

	WHERE result_set_type = 'total'
;


/*
 * calculation_sub_point_sector_results_view
 * -----------------------------------------
 * View returning all emission results (concentrations, depositions and exceedance days) per calculation, sub point and receptor.
 * Use 'calculation_id', 'substance_id' and 'result_type' in the where-clause.
 */
CREATE OR REPLACE VIEW jobs.calculation_sub_point_sector_results_view AS
SELECT
	calculation_id,
	calculation_sub_point_id,
	receptor_id,
	level,
	substance_id,
	result_type,
	result_set_type_key AS sector_id,
	result,
	calculation_sub_points.geometry

	FROM jobs.calculation_sub_point_results
		INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
		INNER JOIN jobs.calculations USING (calculation_id)
		INNER JOIN jobs.calculation_sub_points USING (calculation_sub_point_set_id, calculation_sub_point_id, receptor_id)

	WHERE result_set_type = 'sector'
;

