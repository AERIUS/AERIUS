/*
 * calculation_summed_deposition_results_view
 * ------------------------------------------
 * View returning the summed NOx and NH3 (= substance_id 1711) project contribution per calculation and receptor.
 *
 * Use 'calculation-id' in the where-clause.
 */
CREATE OR REPLACE VIEW jobs.calculation_summed_deposition_results_view AS
SELECT
	calculation_id,
	receptor_id,
	result AS deposition

	FROM jobs.calculation_results_view

	WHERE
		substance_id = 1711
		AND result_type = 'deposition'
;


/*
 * calculation_deposition_results_view
 * -----------------------------------
 * View returning the project contribution (calculation_deposition) per calculation, receptor and substance.
 * substance_id is converted to a calculation_substance_type. Also returns the combined substance 'noxnh3' (substance_id 1711)
 *
 * Use 'calculation_id' and optionally 'calculation_substance' in the where-clause.
 */
CREATE OR REPLACE VIEW jobs.calculation_deposition_results_view AS
SELECT calculation_id, receptor_id, 'nox'::calculation_substance_type AS calculation_substance, result AS calculation_deposition
	FROM jobs.calculation_results_view
	WHERE
		substance_id = 11
		AND result_type = 'deposition'
UNION ALL
SELECT calculation_id, receptor_id, 'nh3'::calculation_substance_type AS calculation_substance, result AS calculation_deposition
	FROM jobs.calculation_results_view
	WHERE
		substance_id = 17
		AND result_type = 'deposition'
UNION ALL
SELECT calculation_id, receptor_id, 'noxnh3'::calculation_substance_type AS calculation_substance, deposition AS calculation_deposition
	FROM jobs.calculation_summed_deposition_results_view
;
