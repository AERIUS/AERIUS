/*
 * ae_threshold_adjusted_result
 * ----------------------------
 * Helper function that adjusts results based on the deposition threshold for nitrogen (NH3+NOx) deposition.
 * Other results will be returned as-is.
 */
CREATE OR REPLACE FUNCTION jobs.ae_threshold_adjusted_result(result real, emission_result_type emission_result_type, substance_id smallint)
	RETURNS REAL AS
$BODY$
	SELECT CASE
		WHEN emission_result_type = 'deposition'::emission_result_type AND substance_id = 1711::smallint
		THEN ae_abs_threshold(result, ae_constant('PRONOUNCEMENT_THRESHOLD_VALUE')::posreal)
		ELSE result
	END;
$BODY$
LANGUAGE SQL IMMUTABLE;


/*
 * ae_calculation_results
 * ----------------------
 * Function returning the results per receptor and result set for a calculations.
 *
 * Intended for use in 'left join' constructions.
 */
CREATE OR REPLACE FUNCTION jobs.ae_calculation_results(v_calculation_id integer)
	RETURNS TABLE(calculation_result_set_id integer, emission_result_type emission_result_type, substance_id smallint, receptor_id integer, result real) AS
$BODY$
SELECT
	calculation_result_set_id,
	result_type AS emission_result_type,
	substance_id,
	receptor_id,
	result

	FROM jobs.calculation_results_view

	WHERE calculation_id = v_calculation_id
;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_calculation_point_results
 * ----------------------------
 * Function returning the results per custom point and result set for a calculation.
 *
 * Intended for use in 'left join' constructions.
 */
CREATE OR REPLACE FUNCTION jobs.ae_calculation_point_results(v_calculation_id integer)
	RETURNS TABLE(calculation_result_set_id integer, emission_result_type emission_result_type, substance_id smallint, calculation_point_id integer, geometry geometry, result real) AS
$BODY$
SELECT
	calculation_result_set_id,
	result_type AS emission_result_type,
	substance_id,
	calculation_point_id,
	geometry,
	result

	FROM jobs.calculation_point_results_view

	WHERE calculation_id = v_calculation_id
;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_calculation_sub_point_results
 * --------------------------------
 * Function returning the results per sub point and result set for a calculation.
 *
 * Intended for use in 'left join' constructions.
 */
CREATE OR REPLACE FUNCTION jobs.ae_calculation_sub_point_results(v_calculation_id integer)
	RETURNS TABLE(calculation_result_set_id integer, emission_result_type emission_result_type, substance_id smallint, calculation_sub_point_id integer, receptor_id integer, result real) AS
$BODY$
SELECT
	calculation_result_set_id,
	result_type AS emission_result_type,
	substance_id,
	calculation_sub_point_id,
	receptor_id,
	result

	FROM jobs.calculation_sub_point_results_view

	WHERE calculation_id = v_calculation_id
;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_calculations_max_results
 * ---------------------------
 * Function returning the maximum result per receptor and result_type/substance_id for multiple calculations.
 */
CREATE OR REPLACE FUNCTION jobs.ae_calculations_max_results(v_calculation_ids integer[])
	RETURNS TABLE(emission_result_type emission_result_type, substance_id smallint, receptor_id integer, result real) AS
$BODY$
SELECT
	result_type AS emission_result_type,
	substance_id,
	receptor_id,
	MAX(result) AS result

	FROM jobs.calculation_results_view

	WHERE calculation_id = ANY(v_calculation_ids)

	GROUP BY result_type, substance_id, receptor_id
;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_calculation_point_max_results
 * --------------------------------
 * Function returning the maximum result per custom point and result_type/substance_id for multiple calculations.
 */
CREATE OR REPLACE FUNCTION jobs.ae_calculation_point_max_results(v_calculation_ids integer[])
	RETURNS TABLE(emission_result_type emission_result_type, substance_id smallint, calculation_point_id integer, geometry geometry, result real) AS
$BODY$
SELECT
	result_type AS emission_result_type,
	substance_id,
	calculation_point_id,
	geometry,
	MAX(result) AS result

	FROM jobs.calculation_point_results_view

	WHERE calculation_id = ANY(v_calculation_ids)

	GROUP BY result_type, substance_id, calculation_point_id, geometry
;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_calculation_sub_point_max_results
 * ------------------------------------
 * Function returning the maximum result per sub point and result_type/substance_id for multiple calculations.
 */
CREATE OR REPLACE FUNCTION jobs.ae_calculation_sub_point_max_results(v_calculation_ids integer[])
	RETURNS TABLE(emission_result_type emission_result_type, substance_id smallint, calculation_sub_point_id integer, receptor_id integer, result real) AS
$BODY$
SELECT
	result_type AS emission_result_type,
	substance_id,
	calculation_sub_point_id,
	receptor_id,
	MAX(result) AS result

	FROM jobs.calculation_sub_point_results_view

	WHERE calculation_id = ANY(v_calculation_ids)

	GROUP BY result_type, substance_id, calculation_sub_point_id, receptor_id
;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_calculations_sum_results
 * ---------------------------
 * Function returning the summed results per receptor and result_type/substance_id for multiple calculations.
 */
CREATE OR REPLACE FUNCTION jobs.ae_calculations_sum_results(v_calculation_ids integer[])
	RETURNS TABLE(emission_result_type emission_result_type, substance_id smallint, receptor_id integer, result real) AS
$BODY$
SELECT
	result_type AS emission_result_type,
	substance_id,
	receptor_id,
	SUM(result::double precision)::real AS result

	FROM jobs.calculation_results_view

	WHERE calculation_id = ANY(v_calculation_ids)

	GROUP BY result_type, substance_id, receptor_id
;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_calculation_point_sum_results
 * --------------------------------
 * Function returning the summed results per custom point and result_type/substance_id for multiple calculations.
 */
CREATE OR REPLACE FUNCTION jobs.ae_calculation_point_sum_results(v_calculation_ids integer[])
	RETURNS TABLE(emission_result_type emission_result_type, substance_id smallint, calculation_point_id integer, result real) AS
$BODY$
SELECT
	result_type AS emission_result_type,
	substance_id,
	calculation_point_id,
	SUM(result::double precision)::real AS result

	FROM jobs.calculation_point_results_view

	WHERE calculation_id = ANY(v_calculation_ids)

	GROUP BY result_type, substance_id, calculation_point_id
;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_calculation_sub_point_sum_results
 * ------------------------------------
 * Function returning the summed results per sub point and result_type/substance_id for multiple calculations.
 */
CREATE OR REPLACE FUNCTION jobs.ae_calculation_sub_point_sum_results(v_calculation_ids integer[])
	RETURNS TABLE(emission_result_type emission_result_type, substance_id smallint, calculation_sub_point_id integer, receptor_id integer, result real) AS
$BODY$
SELECT
	result_type AS emission_result_type,
	substance_id,
	calculation_sub_point_id,
	receptor_id,
	SUM(result::double precision)::real AS result

	FROM jobs.calculation_sub_point_results_view

	WHERE calculation_id = ANY(v_calculation_ids)

	GROUP BY result_type, substance_id, calculation_sub_point_id, receptor_id
;
$BODY$
LANGUAGE SQL STABLE;

/*
 * ae_scenario_calculation_point_situation_results
 * -----------------------------------------------
 * Function returning results for a situation calculation for custom points.
 * Basically the same as normal results, only the nitrogen deposition results below threshold are returned as NULL.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_calculation_point_situation_results(v_calculation_id integer)
	RETURNS TABLE(calculation_result_set_id integer, emission_result_type emission_result_type, substance_id smallint, calculation_point_id integer, geometry geometry, result real) AS
$BODY$
SELECT
	calculation_result_set_id,
	result_type AS emission_result_type,
	substance_id,
	calculation_point_id,
	geometry,
	jobs.ae_threshold_adjusted_result(result, result_type, substance_id) AS result

	FROM jobs.calculation_point_results_view

	WHERE calculation_id = v_calculation_id
;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_project_calculation
 * -------------------------------
 * Function returning results for a project calculation.
 *
 * For a project calculation, the results of the proposed situation are used as the basis.
 * When present, the results of the reference situation and/or off site reduction situation are subtracted.
 * The hexagonset of the proposed situation is leading.
 *
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_project_calculation(v_proposed_calculation_id integer, v_reference_calculation_id integer, v_off_site_reduction_calculation_id integer)
	RETURNS TABLE(calculation_result_set_id integer, emission_result_type emission_result_type, substance_id smallint, receptor_id integer, result real) AS
$BODY$
	SELECT
		proposed_calculation_results.calculation_result_set_id,
		emission_result_type,
		substance_id,
		receptor_id,
		proposed_calculation_results.result
			- COALESCE(reference_calculation_results.result, 0)
			- COALESCE(off_site_reduction_calculation_results.result, 0)
			AS result

	FROM jobs.ae_calculation_results(v_proposed_calculation_id) AS proposed_calculation_results
		LEFT JOIN jobs.ae_calculation_results(v_reference_calculation_id) AS reference_calculation_results USING (receptor_id, emission_result_type, substance_id)
		LEFT JOIN jobs.ae_calculation_results(v_off_site_reduction_calculation_id) AS off_site_reduction_calculation_results USING (receptor_id, emission_result_type, substance_id)
;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_calculation_point_project_results
 * ---------------------------------------------
 * Function returning results for a project calculation for custom points.
 *
 * For a project calculation, the results of the proposed situation are used as the basis.
 * When present, the results of the reference situation and/or off site reduction situation are subtracted.
 * The pointset of the proposed situation is leading.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_calculation_point_project_results(v_proposed_calculation_id integer, v_reference_calculation_id integer, v_off_site_reduction_calculation_id integer)
	RETURNS TABLE(emission_result_type emission_result_type, substance_id smallint, calculation_point_id integer, geometry geometry, result real) AS

$BODY$
	SELECT
		emission_result_type,
		substance_id,
		calculation_point_id,
		proposed_calculation_results.geometry,
		jobs.ae_threshold_adjusted_result(proposed_calculation_results.result
			- COALESCE(reference_calculation_results.result, 0)
			- COALESCE(off_site_reduction_calculation_results.result, 0),
			emission_result_type, substance_id)
			AS result

	FROM jobs.ae_calculation_point_results(v_proposed_calculation_id) AS proposed_calculation_results
		LEFT JOIN jobs.ae_calculation_point_results(v_reference_calculation_id) AS reference_calculation_results USING (calculation_point_id, emission_result_type, substance_id)
		LEFT JOIN jobs.ae_calculation_point_results(v_off_site_reduction_calculation_id) AS off_site_reduction_calculation_results USING (calculation_point_id, emission_result_type, substance_id)
;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_calculation_sub_point_project_results
 * -------------------------------------------------
 * Function returning results for a project calculation for sub points.
 *
 * For a project calculation, the results of the proposed situation are used as the basis.
 * When present, the results of the reference situation and/or off site reduction situation are subtracted.
 * The pointset of the proposed situation is leading.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_calculation_sub_point_project_results(v_proposed_calculation_id integer, v_reference_calculation_id integer, v_off_site_reduction_calculation_id integer)
	RETURNS TABLE(calculation_result_set_id integer, emission_result_type emission_result_type, substance_id smallint, calculation_sub_point_id integer, receptor_id integer, result real) AS

$BODY$
	SELECT
		proposed_calculation_results.calculation_result_set_id,
		emission_result_type,
		substance_id,
		calculation_sub_point_id,
		receptor_id,
		proposed_calculation_results.result
			- COALESCE(reference_calculation_results.result, 0)
			- COALESCE(off_site_reduction_calculation_results.result, 0)
			AS result

	FROM jobs.ae_calculation_sub_point_results(v_proposed_calculation_id) AS proposed_calculation_results
		LEFT JOIN jobs.ae_calculation_sub_point_results(v_reference_calculation_id) AS reference_calculation_results USING (calculation_sub_point_id, receptor_id, emission_result_type, substance_id)
		LEFT JOIN jobs.ae_calculation_sub_point_results(v_off_site_reduction_calculation_id) AS off_site_reduction_calculation_results USING (calculation_sub_point_id, receptor_id, emission_result_type, substance_id)
;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_temporary_calculation
 * ---------------------------------
 * Function returning results for temporary effect calculation.
 *
 * For a temporary effect calculation, the maximum of the results of all supplied temporary calculation is used as a basis.
 * When present, the results of the reference situation and/or off site reduction situation are subtracted.
 * When these should not be subtracted or are not present, 0 can be supplied as the ID instead.
 *
 * The hexagon set of the union of all temporary situations is leading.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_temporary_calculation(v_temporary_calculation_ids integer[], v_reference_calculation_id integer, v_off_site_reduction_calculation_id integer)
	RETURNS TABLE(calculation_id integer, emission_result_type emission_result_type, substance_id smallint, receptor_id integer, result real) AS

$BODY$
	SELECT
		v_temporary_calculation_ids[1],
		emission_result_type,
		substance_id,
		receptor_id,
		temporary_calculation_results.result
			- COALESCE(reference_calculation_results.result, 0)
			- COALESCE(off_site_reduction_calculation_results.result, 0)
			AS result

	FROM jobs.ae_calculations_max_results(v_temporary_calculation_ids) AS temporary_calculation_results
		LEFT JOIN jobs.ae_calculation_results(v_reference_calculation_id) AS reference_calculation_results USING (receptor_id, emission_result_type, substance_id)
		LEFT JOIN jobs.ae_calculation_results(v_off_site_reduction_calculation_id) AS off_site_reduction_calculation_results USING (receptor_id, emission_result_type, substance_id)
;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_calculation_point_temporary_results
 * -----------------------------------------------
 * Function returning results for temporary effect calculation for custom points.
 *
 * For a temporary effect calculation, the maximum of the results of all supplied temporary calculation is used as a basis.
 * When present, the results of the reference situation and/or off site reduction situation are subtracted.
 * When these should not be subtracted or are not present, 0 can be supplied as the ID instead.
 *
 * The point set of the union of all temporary situations is leading.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_calculation_point_temporary_results(v_temporary_calculation_ids integer[], v_reference_calculation_id integer, v_off_site_reduction_calculation_id integer)
	RETURNS TABLE(emission_result_type emission_result_type, substance_id smallint, calculation_point_id integer, geometry geometry, result real) AS

$BODY$
	SELECT
		emission_result_type,
		substance_id,
		calculation_point_id,
		temporary_calculation_results.geometry,
		jobs.ae_threshold_adjusted_result(temporary_calculation_results.result
			- COALESCE(reference_calculation_results.result, 0)
			- COALESCE(off_site_reduction_calculation_results.result, 0),
			emission_result_type, substance_id)
			AS result

	FROM jobs.ae_calculation_point_max_results(v_temporary_calculation_ids) AS temporary_calculation_results
		LEFT JOIN jobs.ae_calculation_point_results(v_reference_calculation_id) AS reference_calculation_results USING (calculation_point_id, emission_result_type, substance_id)
		LEFT JOIN jobs.ae_calculation_point_results(v_off_site_reduction_calculation_id) AS off_site_reduction_calculation_results USING (calculation_point_id, emission_result_type, substance_id)
;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_calculation_sub_point_temporary_results
 * ---------------------------------------------------
 * Function returning results for temporary effect calculation for sub points.
 *
 * For a temporary effect calculation, the maximum of the results of all supplied temporary calculation is used as a basis.
 * When present, the results of the reference situation and/or off site reduction situation are subtracted.
 * When these should not be subtracted or are not present, 0 can be supplied as the ID instead.
 *
 * The point set of the union of all temporary situations is leading.
 *
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_calculation_sub_point_temporary_results(v_temporary_calculation_ids integer[], v_reference_calculation_id integer, v_off_site_reduction_calculation_id integer)
	RETURNS TABLE(calculation_id integer, emission_result_type emission_result_type, substance_id smallint, calculation_sub_point_id integer, receptor_id integer, result real) AS

$BODY$
	SELECT
		v_temporary_calculation_ids[1],
		emission_result_type,
		substance_id,
		calculation_sub_point_id,
		receptor_id,
		temporary_calculation_results.result
			- COALESCE(reference_calculation_results.result, 0)
			- COALESCE(off_site_reduction_calculation_results.result, 0)
			AS result

	FROM jobs.ae_calculation_sub_point_max_results(v_temporary_calculation_ids) AS temporary_calculation_results
		LEFT JOIN jobs.ae_calculation_sub_point_results(v_reference_calculation_id) AS reference_calculation_results USING (calculation_sub_point_id, receptor_id, emission_result_type, substance_id)
		LEFT JOIN jobs.ae_calculation_sub_point_results(v_off_site_reduction_calculation_id) AS off_site_reduction_calculation_results USING (calculation_sub_point_id, receptor_id, emission_result_type, substance_id)
;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_in_combination_calculation
 * --------------------------------------
 * Function returning results for a combination calculation.
 *
 * For a combination calculation, the results of the proposed situation are used as a basis.
 * When present, the results of the reference situation and/or off site reduction situation are subtracted.
 * Additionally, the results of all in-combination-proposed situations are added, and the results of all in-combination-reference situations is subtracted.
 *
 * The hexagonset for the proposed situation is leading.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_in_combination_calculation(v_proposed_calculation_id integer, v_reference_calculation_id integer, v_off_site_reduction_calculation_id integer, v_in_combination_proposed_calculation_ids integer[], v_in_combination_reference_calculation_ids integer[])
	RETURNS TABLE(calculation_result_set_id integer, receptor_id integer, result real) AS
$BODY$
	SELECT
		project_results.calculation_result_set_id,
		receptor_id,
		project_results.result
			+ COALESCE(in_combination_proposed_results.result, 0)
			- COALESCE(in_combination_reference_results.result, 0)
			AS result

	FROM jobs.ae_scenario_project_calculation(v_proposed_calculation_id, v_reference_calculation_id, v_off_site_reduction_calculation_id) AS project_results
		LEFT JOIN jobs.ae_calculations_sum_results(v_in_combination_proposed_calculation_ids) AS in_combination_proposed_results USING (receptor_id, emission_result_type, substance_id)
		LEFT JOIN jobs.ae_calculations_sum_results(v_in_combination_reference_calculation_ids) AS in_combination_reference_results USING (receptor_id, emission_result_type, substance_id)
;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_calculation_point_in_combination_results
 * ----------------------------------------------------
 * Function returning results for a combination calculation for custom points, based on calculation IDs.
 *
 * For a combination calculation, the results of the proposed situation are used as a basis.
 * When present, the results of the reference situation and/or off site reduction situation are subtracted.
 * Additionally, the results of all in-combination-proposed situations are added, and the results of all in-combination-reference situations is subtracted.
 *
 * The point set for the proposed situation is leading.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_calculation_point_in_combination_results(v_proposed_calculation_id integer, v_reference_calculation_id integer, v_off_site_reduction_calculation_id integer, v_in_combination_proposed_calculation_ids integer[], v_in_combination_reference_calculation_ids integer[])
	RETURNS TABLE(emission_result_type emission_result_type, substance_id smallint, calculation_point_id integer, geometry geometry, result real) AS

$BODY$
	SELECT
		emission_result_type,
		substance_id,
		calculation_point_id,
		geometry,
		project_results.result
			+ COALESCE(in_combination_proposed_results.result, 0)
			- COALESCE(in_combination_reference_results.result, 0)
			AS result

	FROM jobs.ae_scenario_calculation_point_project_results(v_proposed_calculation_id, v_reference_calculation_id, v_off_site_reduction_calculation_id) AS project_results
		LEFT JOIN jobs.ae_calculation_point_sum_results(v_in_combination_proposed_calculation_ids) AS in_combination_proposed_results USING (calculation_point_id, emission_result_type, substance_id)
		LEFT JOIN jobs.ae_calculation_point_sum_results(v_in_combination_reference_calculation_ids) AS in_combination_reference_results USING (calculation_point_id, emission_result_type, substance_id)
;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_calculation_sub_point_in_combination_results
 * --------------------------------------------------------
 * Function returning results for a combination calculation for sub points.
 *
 * For a combination calculation, the results of the proposed situation are used as a basis.
 * When present, the results of the reference situation and/or off site reduction situation are subtracted.
 * Additionally, the results of all in-combination-proposed situations are added, and the results of all in-combination-reference situations is subtracted.
 *
 * The point set for the proposed situation is leading.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_calculation_sub_point_in_combination_results(v_proposed_calculation_id integer, v_reference_calculation_id integer, v_off_site_reduction_calculation_id integer, v_in_combination_proposed_calculation_ids integer[], v_in_combination_reference_calculation_ids integer[])
	RETURNS TABLE(calculation_result_set_id integer, calculation_sub_point_id integer, receptor_id integer, result real) AS

$BODY$
	SELECT
		project_results.calculation_result_set_id,
		calculation_sub_point_id,
		receptor_id,
		project_results.result
			+ COALESCE(in_combination_proposed_results.result, 0)
			- COALESCE(in_combination_reference_results.result, 0)
			AS result

	FROM jobs.ae_scenario_calculation_sub_point_project_results(v_proposed_calculation_id, v_reference_calculation_id, v_off_site_reduction_calculation_id) AS project_results
		LEFT JOIN jobs.ae_calculation_sub_point_sum_results(v_in_combination_proposed_calculation_ids) AS in_combination_proposed_results USING (calculation_sub_point_id, receptor_id, emission_result_type, substance_id)
		LEFT JOIN jobs.ae_calculation_sub_point_sum_results(v_in_combination_reference_calculation_ids) AS in_combination_reference_results USING (calculation_sub_point_id, receptor_id, emission_result_type, substance_id)
;
$BODY$
LANGUAGE SQL STABLE;
