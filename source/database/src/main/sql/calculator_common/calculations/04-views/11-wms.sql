/*
 * wms_calculation_results_view
 * ----------------------------
 * WMS view for calculation_results_view, returning calculated results per result type and substance for receptors.
 *
 * Use 'calculation_id' in the where-clause.
 */
CREATE OR REPLACE VIEW jobs.wms_calculation_results_view AS
SELECT
	calculation_id,
	receptor_id,
	result_type AS emission_result_type,
	substance_id,
	result,
	geometry

	FROM jobs.calculation_results_view
		INNER JOIN receptors USING (receptor_id)
;


/*
 * wms_calculation_sub_point_results_view
 * --------------------------------------
 * WMS view for calculation_sub_point_results_view, returning calculated results per result type and substance for sub points.
 *
 * Use 'calculation_id' in the where-clause.
 */
CREATE OR REPLACE VIEW jobs.wms_calculation_sub_point_results_view AS
SELECT
	calculation_id,
	calculation_sub_point_id,
	receptor_id,
	result_type AS emission_result_type,
	substance_id,
	result,
	geometry

	FROM jobs.calculation_sub_point_results_view
;


/*
 * wms_calculation_substance_deposition_results_view
 * -------------------------------------------------
 * WMS view for calculation_deposition_results_view, returning calculated depositions per substance for receptors as hexagons.
 *
 * Use 'calculation_id' and 'zoom_level' in the where-clause.
 */
CREATE OR REPLACE VIEW jobs.wms_calculation_substance_deposition_results_view AS
SELECT
	calculation_id,
	receptor_id,
	calculation_substance,
	calculation_deposition AS deposition,
	zoom_level,
	geometry

	FROM jobs.calculation_deposition_results_view
		INNER JOIN hexagons USING (receptor_id)
	WHERE calculation_deposition > ae_constant('PRONOUNCEMENT_THRESHOLD_VALUE')::real
;
