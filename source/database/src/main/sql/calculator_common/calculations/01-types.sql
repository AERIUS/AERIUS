/*
 * calculation_state_type
 * ----------------------
 * Enum specifying the status of a calculation.
 */
CREATE TYPE calculation_state_type AS ENUM
	('initialized', 'running', 'cancelled', 'completed');

/*
 * calculation_substance_type
 * --------------------------
 * Enum specifying the substances that are calculated.
 * Geeft aan welke stoffen doorberekend moeten worden.
 */
CREATE TYPE calculation_substance_type AS ENUM
	('pm10', 'pm25', 'no2', 'nox', 'nh3', 'noxnh3');

/*
 * calculation_result_set_type
 * ---------------------------
 * Enum specifying the type of result set.
 */
CREATE TYPE calculation_result_set_type AS ENUM
	('total', 'sector');
