/*
 * calculation_point_sets
 * ----------------------
 * Table containing the calculation point sets.
 * When a user starts a calculation in calculator, a user-defined set of calculation points can be used.
 * Multiple calculations can use the same set of points.
 */
CREATE TABLE jobs.calculation_point_sets (
	calculation_point_set_id serial NOT NULL,

	CONSTRAINT calculation_point_sets_pkey PRIMARY KEY (calculation_point_set_id)
);


/*
 * calculation_points
 * ------------------
 * Table containing the points that a user has supplied for calculation.
 * A calculation point is indentifid by the combination of calculation point set id and a point ID. The point ID is unique within the context of a set.
 * 'nearest_receptor_id' is the id of the nearest receptor in the normal hexagon grid.
 * This can be used for quickly retrieving terrain properties (roughness and such).
 */
CREATE TABLE jobs.calculation_points (
	calculation_point_set_id integer NOT NULL,
	calculation_point_id integer NOT NULL,
	label text,
	nearest_receptor_id integer NOT NULL,
	geometry geometry(Point),

	CONSTRAINT calculation_points_pkey PRIMARY KEY (calculation_point_set_id, calculation_point_id),
	CONSTRAINT calculation_points_fkey_calculation_point_sets FOREIGN KEY (calculation_point_set_id) REFERENCES jobs.calculation_point_sets ON DELETE CASCADE
);

CREATE INDEX idx_calculation_points_gist ON jobs.calculation_points USING GIST (geometry);


/*
 * calculation_sub_point_sets
 * --------------------------
 * Table containing calculation sub point sets.
 * A calculation can use sub calculation points.
 * Multiple calculations can use the same set of sub points.
 * That set is called the calculation sub point set.
 */
CREATE TABLE jobs.calculation_sub_point_sets (
	calculation_sub_point_set_id serial NOT NULL,

	CONSTRAINT calculation_sub_point_sets_pkey PRIMARY KEY (calculation_sub_point_set_id)
);


/*
 * calculation_sub_points
 * ----------------------
 * Table containing the sub calculation points that are (being) calculated.
 * A sub calculation point is identified by the combination of calculation sub point set ID and sub point ID + receptor ID. 
 * The sub point ID is unique within the context of a receptor ID, and the combination of receptor ID + sub point ID is unique within the system.
 * 'receptor_id' is the ID of the receptor from the normal hexagon grid to which the sub calculation point belongs.
 * The receptor ID can be used to aggregate sub point results to a receptor, or to determine things like background values.
 */
CREATE TABLE jobs.calculation_sub_points (
	calculation_sub_point_set_id integer NOT NULL,
	calculation_sub_point_id integer NOT NULL,
	receptor_id integer NOT NULL,
	level integer NOT NULL,
	geometry geometry(Point) NOT NULL,

	CONSTRAINT calculation_sub_points_pkey PRIMARY KEY (calculation_sub_point_set_id, calculation_sub_point_id, receptor_id),
	CONSTRAINT calculation_sub_points_fkey_calculation_sub_point_sets FOREIGN KEY (calculation_sub_point_set_id) REFERENCES jobs.calculation_sub_point_sets ON DELETE CASCADE
);

CREATE INDEX idx_calculation_sub_points_gist ON jobs.calculation_sub_points USING GIST (geometry);


/*
 * calculations
 * ------------
 * Table containing calculations.
 * When a user starts a calculation in Calculator, multiple calculations can be done by the system.
 * The results of these calculations for one situation are persisted under 1 calculation, which is referenced in this tabl.
 * The calculation has its own ID, and the status of the calculation is also tracked.
 *
 * @column calculation_point_set_id Optional id to the set of points to calculate.
 * @column calculation_sub_point_set_id Optional id to the set of sub-points to calculate.
 * @column year The emission data year used for the calculation.
 * @column state State of the calculation.
 * @column number_of_sources Total number of source units that need to be calculated for a calculation.
 * @column number_of_substances Total number of substances that need to be calculated for a calculation.
 */
CREATE TABLE jobs.calculations (
	calculation_id serial NOT NULL,
	calculation_point_set_id integer,
	calculation_sub_point_set_id integer,
	year year_type NOT NULL,
	state calculation_state_type NOT NULL DEFAULT 'initialized'::calculation_state_type,
	number_of_sources integer NOT NULL DEFAULT 0,
	number_of_substances integer NOT NULL DEFAULT 0,

	CONSTRAINT calculations_pkey PRIMARY KEY (calculation_id),
	CONSTRAINT calculations_fkey_calculation_point_sets FOREIGN KEY (calculation_point_set_id) REFERENCES jobs.calculation_point_sets,
	CONSTRAINT calculations_fkey_calculation_sub_point_sets FOREIGN KEY (calculation_sub_point_set_id) REFERENCES jobs.calculation_sub_point_sets
);


/*
 * calculation_result_sets
 * -----------------------
 * Table containing the result sets for a calculation.
 * Per calculation there will be 1 or multiple results per receptor. These results are tracked in a result set per substance/result type.
 * Each result set has its own ID.
 */
CREATE TABLE jobs.calculation_result_sets (
	calculation_result_set_id serial NOT NULL,
	calculation_id integer NOT NULL,
	result_set_type calculation_result_set_type NOT NULL,
	result_set_type_key integer NOT NULL,
	result_type emission_result_type NOT NULL,
	substance_id smallint NOT NULL,

	CONSTRAINT calculation_result_sets_pkey PRIMARY KEY (calculation_result_set_id),
	CONSTRAINT calculation_result_sets_fkey_calculation FOREIGN KEY (calculation_id) REFERENCES jobs.calculations ON DELETE CASCADE,
	CONSTRAINT calculation_result_sets_fkey_substances FOREIGN KEY (substance_id) REFERENCES substances,
	CONSTRAINT calculation_result_sets_unique_combination UNIQUE (calculation_id, result_set_type, result_set_type_key, result_type, substance_id),
	CONSTRAINT calculation_result_sets_type_key CHECK (
		(result_set_type = 'total' AND result_set_type_key = 0)
		OR (result_set_type = 'sector' AND result_set_type_key != 0))
);

CREATE INDEX idx_calculation_result_sets_by_calculation_id
		ON jobs.calculation_result_sets (calculation_id, result_set_type, substance_id, result_type);
CREATE INDEX idx_calculation_result_sets_with_result_set_type_key
		ON jobs.calculation_result_sets (calculation_id, result_set_type, result_set_type_key, substance_id, result_type);


/*
 * calculation_results
 * -------------------
 * Table containing results of a calculation.
 * The results of a calculation are persisted per receptor.
 * Information about which substance and which result type (form) the results are for can be found in the related calculation_result_sets table.
 *
 * As results are also calculated for locations where there is no nature site (and as such don't have a record in the receptors table),
 * there is no foreign key to the receptors table.
 */
CREATE TABLE jobs.calculation_results (
	calculation_result_set_id integer NOT NULL,
	receptor_id integer NOT NULL,
	result posreal NOT NULL,

	CONSTRAINT calculation_results_pkey PRIMARY KEY (calculation_result_set_id, receptor_id),
	CONSTRAINT calculation_results_fkey_calculation_result_sets FOREIGN KEY (calculation_result_set_id) REFERENCES jobs.calculation_result_sets ON DELETE CASCADE
);

CREATE INDEX idx_calculation_results_by_calculation_set_id
		ON jobs.calculation_results (calculation_result_set_id);


/*
 * calculation_point_results
 * -------------------------
 * Table containing the results for user defined points (custom points).
 * The results of a calculation are persisted per custom point.
 * Information about which substance and which result type (form) the results are for can be found in the related calculation_result_sets table.
 */
CREATE TABLE jobs.calculation_point_results (
	calculation_result_set_id integer NOT NULL,
	calculation_point_id integer NOT NULL,
	result posreal NOT NULL,

	CONSTRAINT calculation_point_results_pkey PRIMARY KEY (calculation_result_set_id, calculation_point_id),
	CONSTRAINT calculation_point_results_fkey_calculation_result_set FOREIGN KEY (calculation_result_set_id) REFERENCES jobs.calculation_result_sets ON DELETE CASCADE
);

CREATE INDEX idx_calculation_point_results_by_calculation_set_id
		ON jobs.calculation_point_results (calculation_result_set_id);


/*
 * calculation_sub_point_results
 * -----------------------------
 * Table containing the results of a calculation for a sub point.
 * The results of a calculation are persisted per sub point.
 * Information about which substance and which result type (form) the results are for can be found in the related calculation_result_sets table.
 */
CREATE TABLE jobs.calculation_sub_point_results (
	calculation_result_set_id integer NOT NULL,
	calculation_sub_point_id integer NOT NULL,
	receptor_id integer NOT NULL,
	result posreal NOT NULL,

	CONSTRAINT calculation_sub_point_results_pkey PRIMARY KEY (calculation_result_set_id, calculation_sub_point_id, receptor_id),
	CONSTRAINT calculation_sub_point_results_fkey_calculation_result_set FOREIGN KEY (calculation_result_set_id) REFERENCES jobs.calculation_result_sets ON DELETE CASCADE
);

CREATE INDEX idx_calculation_sub_point_results_by_calculation_set_id
		ON jobs.calculation_sub_point_results (calculation_result_set_id);


/*
 * calculation_overlapping_receptors
 * ---------------------------------
 * Table containing the overlapping receptors.
 * A calculation can use a maximum calculation distance from each source to receptor.
 * When that is the case, this table contains the receptors for which all sources are within the calculation distance.
 */
CREATE TABLE jobs.calculation_overlapping_receptors (
	calculation_id integer NOT NULL,
	receptor_id integer NOT NULL,

	CONSTRAINT calculation_overlapping_receptors_pkey PRIMARY KEY (calculation_id, receptor_id),
	CONSTRAINT calculation_overlapping_receptors_fkey_calculation FOREIGN KEY (calculation_id) REFERENCES jobs.calculations ON DELETE CASCADE
);
