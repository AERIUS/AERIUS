/*
 * depositions_view
 * ----------------
 * Opendata view for AERIUS refined deposition map.
 *
 * Returns the refined nitrogen deposition map per background year for all zoom level 1 hexagons that intersect with a Natura2000 area.
 *
 * Also returns all (non-refined) background depositions at zoom level 4, nationwide.
 */
CREATE OR REPLACE VIEW opendata.depositions_view AS
SELECT
	background_year AS year,
	receptor_id,
	zoom_level,
	result AS total_deposition,
	geometry

	FROM receptor_background_results
		INNER JOIN hexagons USING (receptor_id)

	WHERE
		zoom_level = 1
		AND emission_result_type = 'deposition'
		AND substance_id = 1711
UNION ALL
SELECT
	year,
	background_cell_id as "receptor_id",
	4 as "zoom_level",
	deposition AS total_deposition,
	geometry

	FROM background_cell_depositions
		INNER JOIN background_years ON (background_year = year)
		INNER JOIN background_cells USING (background_cell_id)
;


/*
 * hexagons_view
 * -------------
 * Opendata view for the AERIUS hexagon grid.
 *
 * Returns all hexagons that intersect with a Natura2000 area, as well as a few characteristics.
 * Hexagons are returned for all zoom levels, but only at zoom level 1 the characteristics will be returned.
 *
 * @column relevant Boolean indicating if the hexagon is considered nitrogen-relevant.
 * @column exceeding Boolean indicating if the critical deposition value on the hexagon is (nearly) exceeding.
 * @column above_cl Boolean indicating if the critical deposition value on the hexagon is exceeded.
 * @column extra_assessment Boolean indicating if the hexagon is part of set of hexagons that require extra assessment, due to change in the habitat geometries.
 * @column critical_deposition The (lowest) critical deposition value (kritische depositie waarde, KDW) on that hexagon.
 */
CREATE OR REPLACE VIEW opendata.hexagons_view AS
SELECT
	receptor_id,
	zoom_level,
	CASE WHEN zoom_level = 1 THEN COALESCE(bool_or(hexagon_type = 'relevant_hexagons'), false) ELSE NULL END AS relevant,
	CASE WHEN zoom_level = 1 THEN COALESCE(bool_or(hexagon_type = 'exceeding_hexagons'), false) ELSE NULL END AS exceeding,
	CASE WHEN zoom_level = 1 THEN COALESCE(bool_or(hexagon_type = 'above_cl_hexagons'), false) ELSE NULL END AS above_cl,
	CASE WHEN zoom_level = 1 THEN COALESCE(bool_or(hexagon_type = 'extra_assessment_hexagons'), false) ELSE NULL END AS extra_assessment,
	CASE WHEN zoom_level = 1 THEN critical_deposition ELSE NULL END AS critical_deposition,
	hexagons.geometry

	FROM receptors
		LEFT JOIN critical_depositions_view USING (receptor_id)
		LEFT JOIN hexagon_type_receptors USING (receptor_id)
		INNER JOIN hexagons USING (receptor_id)

	GROUP BY receptor_id, zoom_level, critical_deposition, hexagons.geometry

	ORDER BY zoom_level, receptor_id
;


/*
 * hexagons_to_relevant_habitats_view
 * ----------------------------------
 * Opendata view for linking hexagons and relevant habitats.
 *
 * Aside from the link, the natura2000 area, the critical deposition value and the coverage of the relevant habitat is also returned.
 * Also returns the surface of the intersection between hexagon and relevant habitat.
 *
 * Only returns data for zoom level 1.
 */
CREATE OR REPLACE VIEW opendata.hexagons_to_relevant_habitats_view AS
SELECT
	receptor_id,
	zoom_level,
	assessment_area_id AS natura2000_area_id,
	assessment_areas.name AS natura2000_area_name,
	habitat_type_id,
	habitat_types.name AS habitat_type_name,
	habitat_types.description AS habitat_type_description,
	critical_deposition,
	surface,
	receptor_habitat_coverage AS coverage,
	hexagons.geometry

	FROM receptors_to_relevant_habitats_view
		INNER JOIN assessment_areas USING (assessment_area_id)
		INNER JOIN habitat_types USING (habitat_type_id)
		INNER JOIN habitat_type_critical_depositions_view USING (habitat_type_id)
		INNER JOIN hexagons USING (receptor_id)

	WHERE zoom_level = 1

	ORDER BY receptor_id, zoom_level, natura2000_area_id, habitat_type_id
;


/*
 * extra_assessment_hexagons_to_habitats_view
 * ------------------------------------------
 * Opendata view for linking extra assessment hexagons and habitat types.
 *
 * Aside from the link, the natura2000 area and the critical deposition value of the habitat type is also returned.
 *
 * Only returns data for zoom level 1.
 */
CREATE OR REPLACE VIEW opendata.extra_assessment_hexagons_to_habitats_view AS
SELECT
	receptor_id,
	zoom_level,
	assessment_area_id AS natura2000_area_id,
	assessment_areas.name AS natura2000_area_name,
	habitat_type_id,
	habitat_types.name AS habitat_type_name,
	habitat_types.description AS habitat_type_description,
	critical_deposition,
	hexagons.geometry

	FROM extra_assessment_receptors
		INNER JOIN assessment_areas USING (assessment_area_id)
		INNER JOIN habitat_types USING (habitat_type_id)
		INNER JOIN habitat_type_critical_depositions_view USING (habitat_type_id)
		INNER JOIN hexagons USING (receptor_id)

	WHERE zoom_level = 1

	ORDER BY receptor_id, zoom_level, natura2000_area_id, habitat_type_id
;


/*
 * relevant_habitats_view
 * ----------------------
 * Opendata view for AERIUS relevant habitats.
 *
 * These are the relevant parts of the combined habitat areas within a natura2000 area.
 * The critical deposition value (kritische depositie waarde, KDW) and the coverage is also returned.
 */
CREATE OR REPLACE VIEW opendata.relevant_habitats_view AS
SELECT
	assessment_area_id AS natura2000_area_id,
	assessment_areas.name AS natura2000_area_name,
	habitat_type_id,
	habitat_types.name AS habitat_type_name,
	habitat_types.description AS habitat_type_description,
	critical_deposition,
	habitat_coverage AS coverage,
	relevant_habitats.geometry

	FROM relevant_habitats
		INNER JOIN assessment_areas USING (assessment_area_id)
		INNER JOIN habitat_types USING (habitat_type_id)
		INNER JOIN habitat_type_critical_depositions_view USING (habitat_type_id)

	ORDER BY natura2000_area_id, habitat_type_id
;


/*
 * terrain_properties_view
 * -----------------------
 * Opendata view for AERIUS terrain properties.
 *
 * The terrain properties of the relevant receptors with the hexagon geometry.
 *
 * Only returns data for zoom level 1.
 */
CREATE OR REPLACE VIEW opendata.terrain_properties_view AS
SELECT
	receptor_id,
	average_roughness,
	dominant_land_use,
	array_to_string(land_uses, ',') AS land_uses,
	geometry

	FROM terrain_properties
		INNER JOIN included_receptors USING (receptor_id)
		INNER JOIN hexagons USING (receptor_id, zoom_level)

	WHERE zoom_level = 1

	ORDER BY receptor_id
;


/*
 * waterway_categories_view
 * ------------------------
 * Opendata view for AERIUS waterways.
 *
 * Returns the waterways including geometry and their category information.
 */
CREATE OR REPLACE VIEW opendata.waterway_categories_view AS
SELECT
	shipping_inland_waterway_id,
	code,
	name,
	description,
	flowing,
	geometry

	FROM shipping_inland_waterways
		INNER JOIN shipping_inland_waterway_categories USING (shipping_inland_waterway_category_id)

	ORDER BY shipping_inland_waterway_id
;

