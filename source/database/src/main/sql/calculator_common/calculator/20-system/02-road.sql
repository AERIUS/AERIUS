/*
 * system.hide_road_type_categories
 * --------------------------------
 * Table containing the road type categories that should NOT be shown in the UI.
 */
CREATE TABLE system.hide_road_type_categories
(
	road_type_category_id integer NOT NULL,

	CONSTRAINT hide_road_type_categories_pkey PRIMARY KEY (road_type_category_id),
	CONSTRAINT hide_road_type_categories_fkey FOREIGN KEY (road_type_category_id) REFERENCES road_type_categories
);
