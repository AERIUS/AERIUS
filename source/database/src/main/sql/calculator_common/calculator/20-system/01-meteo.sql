/*
 * meteo
 * -----
 * System table containing the available meteo data.
 *
 * @column code Unique code, returning in the used calculation options. Should be mappable for the calculation model.
 * @column description A user-friendly description, for example used in UI.
 * @column start_year The start year for the meteo, inclusive.
 * @column end_year The end year for the meteo, inclusive.
 *
 * In case of a single year meteo file, the start_year and end_year should be the same.
 */
CREATE TABLE system.meteo (
	code text NOT NULL,
	description text NOT NULL,
	start_year int NOT NULL,
	end_year int NOT NULL,

	CONSTRAINT meteo_pkey PRIMARY KEY (code)
);
