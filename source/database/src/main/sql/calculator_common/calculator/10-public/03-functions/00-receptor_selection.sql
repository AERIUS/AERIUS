/*
 * receptor_hexagon_type_view
 * --------------------------
 * View returning the receptors for each hexagon_type.
 */
CREATE OR REPLACE VIEW receptor_hexagon_type_view AS
SELECT
	receptor_id,
	hexagon_type
	FROM hexagon_type_receptors
;


/*
 * ae_overlapping_receptors
 * ------------------------
 * Function returning all completely overlapping receptors in the context of the supplied calculations.
 */
CREATE OR REPLACE FUNCTION jobs.ae_overlapping_receptors(calculation_ids integer[])
	RETURNS TABLE(receptor_id integer) AS
$BODY$
SELECT receptor_id
	FROM jobs.calculation_overlapping_receptors
	WHERE calculation_id = ANY (array_remove(array_remove(calculation_ids, NULL), 0))
	GROUP BY receptor_id
	HAVING count(*) = (array_length(array_remove(array_remove(calculation_ids, NULL), 0), 1))
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_calculation_receptors
 * ------------------------
 * Function returning all receptors in the context of the supplied calculations.
 */
CREATE OR REPLACE FUNCTION jobs.ae_calculation_receptors(calculation_ids integer[])
	RETURNS TABLE(receptor_id integer) AS
$BODY$
SELECT DISTINCT
	receptor_id

	FROM jobs.calculation_results
		INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
	WHERE calculation_id = ANY (array_remove(array_remove(calculation_ids, NULL), 0))
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_receptors_with_hexagon_type
 * ------------------------------
 * Function returning all receptors for each hexagon type in the context of the supplied calculations.
 */
CREATE OR REPLACE FUNCTION jobs.ae_receptors_with_hexagon_type(calculation_ids integer[])
	RETURNS TABLE(receptor_id integer, hexagon_type hexagon_type) AS
$BODY$
WITH receptors AS (
	SELECT receptor_id FROM jobs.ae_calculation_receptors(calculation_ids)
)
SELECT receptor_id,
	hexagon_type AS receptor_hexagon_type
	FROM receptors
		INNER JOIN hexagon_type_receptors USING (receptor_id)
;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_receptors_with_overlapping_hexagon_type
 * ------------------------------------------
 * Function returning all receptors for each overlapping hexagon type in the context of the supplied calculations.
 */
CREATE OR REPLACE FUNCTION jobs.ae_receptors_with_overlapping_hexagon_type(calculation_ids integer[])
	RETURNS TABLE(receptor_id integer, overlapping_hexagon_type overlapping_hexagon_type) AS
$BODY$
WITH receptors AS (
	SELECT receptor_id FROM jobs.ae_calculation_receptors(calculation_ids)
), overlapping_receptors AS (
	SELECT receptor_id FROM jobs.ae_overlapping_receptors(calculation_ids)
)
SELECT
	receptor_id,
	'all_hexagons'::overlapping_hexagon_type AS overlapping_hexagon_type
	FROM receptors
UNION ALL
SELECT receptor_id,
	'overlapping_hexagons_only'::overlapping_hexagon_type AS overlapping_hexagon_type
	FROM overlapping_receptors
UNION ALL
SELECT receptors.receptor_id,
	'non_overlapping_hexagons_only'::overlapping_hexagon_type AS overlapping_hexagon_type
	FROM receptors
		LEFT JOIN overlapping_receptors USING (receptor_id)
	WHERE overlapping_receptors.receptor_id IS NULL;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_calculation_receptors
 * ---------------------------------
 * Function returning all receptors for each hexagon type and overlapping hexagon type combination in the context of the supplied calculations.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_calculation_receptors(calculation_ids integer[])
	RETURNS TABLE(receptor_id integer, hexagon_type hexagon_type, overlapping_hexagon_type overlapping_hexagon_type) AS
$BODY$
SELECT
	receptor_id,
	hexagon_type,
	overlapping_hexagon_type

	FROM jobs.ae_receptors_with_hexagon_type(calculation_ids)
		INNER JOIN jobs.ae_receptors_with_overlapping_hexagon_type(calculation_ids) USING (receptor_id)
$BODY$
LANGUAGE SQL STABLE;
