/*
 * ae_job_summary_situations
 * -------------------------
 * Function to determine the summary for single situations in a job.
 */
CREATE OR REPLACE FUNCTION jobs.ae_job_summary_situations(v_job_id integer)
	RETURNS TABLE (assessment_area_id integer, situation_reference text, emission_result_type emission_result_type, substance_id smallint, max_contribution real) AS
$BODY$
	SELECT
		assessment_area_id,
		situation_reference,
		result_type AS emission_result_type,
		substance_id,
		max(value) AS max_contribution

		FROM jobs.scenario_calculation_assessment_area_statistics
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN jobs.job_calculations USING (calculation_id, job_id)

		WHERE scenario_result_type = 'situation_result'
			AND result_statistic_type = 'max_contribution'
			AND ((result_type = 'deposition' AND substance_id = 1711) OR result_type = 'concentration')
			AND job_id = v_job_id
		GROUP BY assessment_area_id, situation_reference, result_type, substance_id;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_job_summary_projects
 * -----------------------
 * Function to determine the summary for project contribution calculation in a job.
 */
CREATE OR REPLACE FUNCTION jobs.ae_job_summary_projects(v_job_id integer)
	RETURNS TABLE (assessment_area_id integer, situation_reference text, emission_result_type emission_result_type, substance_id smallint, result_statistic_type result_statistic_type, max_value real) AS
$BODY$
	SELECT
		assessment_area_id,
		situation_reference,
		result_type AS emission_result_type,
		substance_id,
		result_statistic_type,
		max(value) AS max_value

		FROM jobs.scenario_calculation_assessment_area_statistics
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN jobs.job_calculations USING (calculation_id, job_id)
		WHERE scenario_result_type = 'project_calculation'
			AND ((result_type = 'deposition' AND substance_id = 1711) OR result_type = 'concentration')
			AND result_statistic_type = ANY (ARRAY['max_increase', 'max_total', 'max_percentage_critical_level', 'max_total_percentage_critical_level']::result_statistic_type[])
			AND job_id = v_job_id
		GROUP BY assessment_area_id, situation_reference, result_type, result_statistic_type, substance_id;
$BODY$
LANGUAGE SQL STABLE;
