/*
 * calculation_permit_areas
 * ------------------------
 * Table containing the different permit areas that can be selected for a calculation.
 */
CREATE TABLE calculation_permit_areas (
	calculation_permit_area_id integer NOT NULL,
	code text NOT NULL UNIQUE,
	name text NOT NULL UNIQUE,
	description text,

	CONSTRAINT calculation_permit_areas_pkey PRIMARY KEY (calculation_permit_area_id)
);


/*
 * calculation_project_categories
 * ------------------------------
 * Table containing the different project categories that can be selected for a calculation.
 */
CREATE TABLE calculation_project_categories (
	calculation_project_category_id integer NOT NULL,
	code text NOT NULL UNIQUE,
	name text NOT NULL UNIQUE,
	description text,

	CONSTRAINT calculation_project_categories_pkey PRIMARY KEY (calculation_project_category_id)
);


/*
 * calculation_default_distances
 * -----------------------------
 * Table containing the default calculation distances for permit area and project category combinations.
 * 
 * This also specifies which permit area and project category combinations are valid.
 */
CREATE TABLE calculation_default_distances (
	calculation_permit_area_id integer NOT NULL,
	calculation_project_category_id integer NOT NULL,
	default_calculation_distance integer NOT NULL,

	CONSTRAINT calculation_default_distances_pkey PRIMARY KEY (calculation_permit_area_id, calculation_project_category_id)
);
