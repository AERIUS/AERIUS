/*
 * scenario_calculation_assessment_area_statistics_view
 * ----------------------------------------------------
 * View returning aggregated statistics of a single calculation per result type, hexagon type, overlapping hexagon type and assessment area.
 *
 * Use at least 'calculation_id', 'emission_result_type' and 'substance_id' in the where clause
 */
CREATE OR REPLACE VIEW jobs.scenario_calculation_assessment_area_statistics_view AS
SELECT
	calculation_id,
	calculation_result_sets.result_type AS emission_result_type,
	substance_id,
	scenario_result_type,
	hexagon_type,
	overlapping_hexagon_type,
	assessment_area_id,
	result_statistic_type,
	value

	FROM jobs.scenario_calculation_assessment_area_statistics
		INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
;


/*
 * scenario_calculation_assessment_area_statistic_markers_view
 * -----------------------------------------------------------
 * View returning the markers (or receptors) where the statistics are located, per result type, hexagon type, overlapping hexagon type and assessment area.
 *
 * Use at least 'calculation_id', 'emission_result_type' and 'substance_id' in the where clause.
 */
CREATE OR REPLACE VIEW jobs.scenario_calculation_assessment_area_statistic_markers_view AS
SELECT
	calculation_id,
	calculation_result_sets.result_type AS emission_result_type,
	substance_id,
	scenario_result_type,
	hexagon_type,
	overlapping_hexagon_type,
	assessment_area_id,
	result_statistic_type,
	receptor_id

	FROM jobs.scenario_calculation_assessment_area_statistic_markers
		INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
;


/*
 * scenario_calculation_critical_deposition_area_statistics_view
 * -------------------------------------------------------------
 * View returning aggregated statistics of a single calculation per result type, hexagon type, overlapping hexagon type, assessment area and habitat type.
 *
 * Use at least 'calculation_id', 'emission_result_type' and 'substance_id' in the where clause
 */
CREATE OR REPLACE VIEW jobs.scenario_calculation_critical_deposition_area_statistics_view AS
SELECT
	calculation_id,
	calculation_result_sets.result_type AS emission_result_type,
	substance_id,
	scenario_result_type,
	hexagon_type,
	overlapping_hexagon_type,
	assessment_area_id,
	critical_deposition_area_id,
	result_statistic_type,
	value

	FROM jobs.scenario_calculation_critical_deposition_area_statistics
		INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
;


/*
 * scenario_calculation_assessment_area_chart_statistics_view
 * ----------------------------------------------------------
 * View returning the aggregated statistics of a single calculation for charts, per result type, hexagon type, overlapping hexagon type and assessment area.
 *
 * Use at least 'calculation_id', 'emission_result_type' and 'substance_id' in the where clause.
 */
CREATE OR REPLACE VIEW jobs.scenario_calculation_assessment_area_chart_statistics_view AS
SELECT
	calculation_id,
	calculation_result_sets.result_type AS emission_result_type,
	substance_id,
	emission_result_chart_type,
	scenario_result_type,
	hexagon_type,
	overlapping_hexagon_type,
	assessment_area_id,
	color_range_type,
	lower_bound,
	cartographic_surface

	FROM jobs.scenario_calculation_assessment_area_chart_statistics
		INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
;
