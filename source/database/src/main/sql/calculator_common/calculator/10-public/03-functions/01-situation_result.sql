/*
 * ae_calculation_statistics_base
 * ------------------------------
 * Base function to determine statistics for a single calculation.
 */
CREATE OR REPLACE FUNCTION jobs.ae_calculation_statistics_base(v_calculation_id integer)
	RETURNS TABLE(calculation_result_set_id integer, receptor_id integer, result real, total_result real) AS
$BODY$
	SELECT
		calculation_result_set_id,
		calculation_results.receptor_id,
		calculation_results.result,
		calculation_results.result + COALESCE(background.result, 0) AS total_result

		FROM jobs.calculation_results_view AS calculation_results
			INNER JOIN jobs.calculations USING (calculation_id)
			LEFT JOIN receptor_background_results_view AS background ON (
					background.receptor_id = calculation_results.receptor_id
						AND background.emission_result_type = calculation_results.result_type
						AND background.substance_id = calculation_results.substance_id
						AND background.year = jobs.calculations.year)

		WHERE calculation_id = v_calculation_id
			-- If it's deposition take the threshold into account, otherwise don't.
			-- Only do deposition, concentrations will be on sub level.
			AND calculation_results.result_type = 'deposition'
			AND calculation_results.result > ae_constant('PRONOUNCEMENT_THRESHOLD_VALUE')::posreal;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_calculation_areas_statistics_base
 * ------------------------------------
 * Base function to determine statistics per assessment area/habitat for a single calculation.
 */
CREATE OR REPLACE FUNCTION jobs.ae_calculation_areas_statistics_base(v_calculation_id integer)
	RETURNS TABLE(calculation_result_set_id integer, hexagon_type hexagon_type, receptor_id integer, assessment_area_id integer, critical_deposition_area_id integer, cartographic_surface real, result real, total_result real) AS
$BODY$
	SELECT
		calculation_result_set_id,
		hexagon_type,
		receptor_id,
		assessment_area_id,
		critical_deposition_area_id,
		cartographic_surface,
		result,
		total_result

		FROM jobs.ae_calculation_statistics_base(v_calculation_id)
			INNER JOIN receptors_to_relevant_habitats USING (receptor_id)
			INNER JOIN jobs.ae_receptors_with_hexagon_type(ARRAY[v_calculation_id]) USING (receptor_id)
		WHERE hexagon_type != 'extra_assessment_hexagons'
	UNION ALL
	SELECT
		calculation_result_set_id,
		hexagon_type,
		receptor_id,
		assessment_area_id,
		habitat_type_id AS critical_deposition_area_id,
		1 AS cartographic_surface,
		result,
		total_result

		FROM jobs.ae_calculation_statistics_base(v_calculation_id)
			INNER JOIN receptors_to_assessment_areas USING (receptor_id)
			INNER JOIN jobs.ae_receptors_with_hexagon_type(ARRAY[v_calculation_id]) USING (receptor_id)
			INNER JOIN extra_assessment_receptors USING (receptor_id, assessment_area_id)
		WHERE hexagon_type = 'extra_assessment_hexagons';
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_calculation_sub_statistics_base
 * ----------------------------------
 * Base function to determine statistics for a single calculation for sub points.
 */
CREATE OR REPLACE FUNCTION jobs.ae_calculation_sub_statistics_base(v_calculation_id integer)
	RETURNS TABLE(calculation_result_set_id integer, calculation_sub_point_id integer, receptor_id integer, assessment_area_id integer, critical_deposition_area_id integer, result real, total_result real) AS
$BODY$
	SELECT
		calculation_result_set_id,
		calculation_results.calculation_sub_point_id,
		calculation_results.receptor_id,
		assessment_area_id,
		critical_deposition_area_id,
		calculation_results.result,
		calculation_results.result + COALESCE(background.result, 0) AS total_result

		FROM jobs.calculation_sub_point_results_view AS calculation_results
			INNER JOIN receptors_to_relevant_habitats USING (receptor_id)
			INNER JOIN jobs.calculations USING (calculation_id)
			LEFT JOIN receptor_background_results_view AS background ON (
					background.receptor_id = calculation_results.receptor_id
						AND background.emission_result_type = calculation_results.result_type
						AND background.substance_id = calculation_results.substance_id
						AND background.year = jobs.calculations.year)

		WHERE calculation_id = v_calculation_id
			-- Only do sub statistics for non-depositions
			AND calculation_results.result_type != 'deposition';
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_calculation_statistics
 * -------------------------
 * Function returning the general statistics for a single calculation.
 */
CREATE OR REPLACE FUNCTION jobs.ae_calculation_statistics(v_calculation_id integer)
	RETURNS TABLE(emission_result_type emission_result_type, substance_id smallint, hexagon_type hexagon_type, result_statistic_type result_statistic_type, value real) AS
$BODY$
	-- As sum_contribution is one of the statistics, need to make sure results are not counted double due to multiple habitats for 1 receptor.
	SELECT
		calculation_result_sets.result_type AS emission_result_type,
		calculation_result_sets.substance_id,
		hexagon_type,
		UNNEST(array ['count_receptors'::result_statistic_type, 'max_contribution'::result_statistic_type, 'max_total'::result_statistic_type, 'sum_contribution'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [count(DISTINCT receptor_id)::real, max(result), max(total_result), sum(result)]) AS value

		FROM jobs.ae_calculation_statistics_base(v_calculation_id)
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN receptor_hexagon_type_view USING (receptor_id)

		GROUP BY emission_result_type, substance_id, hexagon_type
	UNION ALL
	SELECT
		calculation_result_sets.result_type AS emission_result_type,
		calculation_result_sets.substance_id,
		hexagon_type,
		UNNEST(array ['sum_cartographic_surface'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [sum(cartographic_surface::numeric)::real]) AS value

		FROM jobs.ae_calculation_statistics_base(v_calculation_id)
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN receptors_to_relevant_habitats USING (receptor_id)
			INNER JOIN receptor_hexagon_type_view USING (receptor_id)

		GROUP BY emission_result_type, substance_id, hexagon_type
	UNION ALL
	SELECT
		jobs.calculation_result_sets.result_type AS emission_result_type,
		jobs.calculation_result_sets.substance_id,
		hexagon_type,
		UNNEST(array ['count_calculation_points'::result_statistic_type, 'max_contribution'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [count(DISTINCT (calculation_sub_point_id, receptor_id))::real, max(result), max(total_result)]) AS value

		FROM jobs.ae_calculation_sub_statistics_base(v_calculation_id)
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN receptor_hexagon_type_view USING (receptor_id)

		GROUP BY emission_result_type, substance_id, hexagon_type;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_fill_calculation_statistics
 * ------------------------------
 * Function to determine all statistics for a single calculation and to persist them in the appropriate tables.
 */
CREATE OR REPLACE FUNCTION jobs.ae_fill_calculation_statistics(v_job_id integer, v_calculation_id integer)
	RETURNS void AS
$BODY$
	-- Assessment area
	-- As sum_contribution is one of the statistics, need to make sure results are not counted double due to multiple habitats for 1 receptor.
	WITH area_results AS (
	SELECT DISTINCT
		calculation_result_set_id,
		hexagon_type,
		receptor_id,
		assessment_area_id,
		result,
		total_result

		FROM jobs.ae_calculation_areas_statistics_base(v_calculation_id)
	)
	INSERT INTO jobs.scenario_calculation_assessment_area_statistics (job_id, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		'situation_result'::scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		UNNEST(array ['count_receptors'::result_statistic_type, 'max_contribution'::result_statistic_type, 'max_total'::result_statistic_type, 'sum_contribution'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [count(DISTINCT receptor_id)::real, max(result), max(total_result), sum(result)]) AS value

		FROM area_results

		GROUP BY calculation_result_set_id, assessment_area_id, hexagon_type;

	INSERT INTO jobs.scenario_calculation_assessment_area_statistics (job_id, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		'situation_result'::scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		UNNEST(array ['sum_cartographic_surface'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [sum(cartographic_surface::numeric)::real]) AS value

		FROM jobs.ae_calculation_areas_statistics_base(v_calculation_id)

		GROUP BY calculation_result_set_id, assessment_area_id, hexagon_type;


	-- Statistic markers
	WITH area_results AS (
	SELECT DISTINCT
		calculation_result_set_id,
		hexagon_type,
		receptor_id,
		assessment_area_id,
		result,
		total_result

		FROM jobs.ae_calculation_areas_statistics_base(v_calculation_id)
	)
	INSERT INTO jobs.scenario_calculation_assessment_area_statistic_markers (job_id, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, result_statistic_type, receptor_id)
	SELECT DISTINCT ON (calculation_result_set_id, assessment_area_id, hexagon_type, result_statistic_type)
		v_job_id,
		'situation_result'::scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		UNNEST(array ['max_contribution'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [
			first_value(receptor_id) OVER (PARTITION BY calculation_result_set_id, assessment_area_id, hexagon_type ORDER BY result DESC, receptor_id),
			first_value(receptor_id) OVER (PARTITION BY calculation_result_set_id, assessment_area_id, hexagon_type ORDER BY total_result DESC, receptor_id)
		]) AS receptor_id

		FROM area_results;


	-- Chart statistics
	INSERT INTO jobs.scenario_calculation_assessment_area_chart_statistics (job_id, emission_result_chart_type, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, color_range_type, lower_bound, cartographic_surface)
	SELECT
		v_job_id,
		'emission_result'::emission_result_chart_type,
		scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		color_range_type,
		(UNNEST(ae_color_range(color_range_type, result::numeric, cartographic_surface::numeric))).lower_value::real AS lower_bound,
		(UNNEST(ae_color_range(color_range_type, result::numeric, cartographic_surface::numeric))).total::real AS cartographic_surface

		FROM jobs.ae_calculation_areas_statistics_base(v_calculation_id)
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN scenario_result_types_color_ranges ON (calculation_result_sets.result_type = scenario_result_types_color_ranges.emission_result_type AND calculation_result_sets.substance_id = scenario_result_types_color_ranges.substance_id)

		WHERE scenario_result_type = 'situation_result'::scenario_result_type
			AND hexagon_type != 'extra_assessment_hexagons'

		GROUP BY scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, color_range_type;

	-- Special case for extra_assessment_hexagons, where we want to display number of unique receptors
	WITH area_results AS (
	SELECT DISTINCT
		calculation_result_set_id,
		hexagon_type,
		receptor_id,
		assessment_area_id,
		result

		FROM jobs.ae_calculation_areas_statistics_base(v_calculation_id)
		WHERE hexagon_type = 'extra_assessment_hexagons'
	)
	INSERT INTO jobs.scenario_calculation_assessment_area_chart_statistics (job_id, emission_result_chart_type, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, color_range_type, lower_bound, cartographic_surface)
	SELECT
		v_job_id,
		'emission_result'::emission_result_chart_type,
		scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		color_range_type,
		(UNNEST(ae_color_range(color_range_type, result::numeric, 1::numeric))).lower_value::real AS lower_bound,
		(UNNEST(ae_color_range(color_range_type, result::numeric, 1::numeric))).total::real AS cartographic_surface

		FROM area_results
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN scenario_result_types_color_ranges ON (calculation_result_sets.result_type = scenario_result_types_color_ranges.emission_result_type AND calculation_result_sets.substance_id = scenario_result_types_color_ranges.substance_id)

		WHERE scenario_result_type = 'situation_result'::scenario_result_type

		GROUP BY scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, color_range_type;


	-- Assessment area and critical deposition area
	INSERT INTO jobs.scenario_calculation_critical_deposition_area_statistics (job_id, scenario_result_type, calculation_result_set_id, assessment_area_id, critical_deposition_area_id, hexagon_type, overlapping_hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		'situation_result'::scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		critical_deposition_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		UNNEST(array ['count_receptors'::result_statistic_type, 'sum_cartographic_surface'::result_statistic_type, 'max_contribution'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [count(DISTINCT receptor_id)::real, sum(cartographic_surface::numeric)::real, max(result), max(total_result)]) AS value

		FROM jobs.ae_calculation_areas_statistics_base(v_calculation_id)

		GROUP BY calculation_result_set_id, assessment_area_id, critical_deposition_area_id, hexagon_type;


	-- Sub points
	-- Assessment area
	INSERT INTO jobs.scenario_calculation_assessment_area_statistics (job_id, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		'situation_result'::scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		UNNEST(array ['count_calculation_points'::result_statistic_type, 'max_contribution'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [count(DISTINCT (calculation_sub_point_id, receptor_id))::real, max(result), max(total_result)]) AS value

		FROM jobs.ae_calculation_sub_statistics_base(v_calculation_id)
			INNER JOIN jobs.ae_receptors_with_hexagon_type(ARRAY[v_calculation_id]) USING (receptor_id)

		GROUP BY calculation_result_set_id, assessment_area_id, hexagon_type;


	-- Statistic markers
	INSERT INTO jobs.scenario_calculation_assessment_area_statistic_markers (job_id, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, result_statistic_type, receptor_id)
	SELECT DISTINCT ON (calculation_result_set_id, assessment_area_id, hexagon_type, result_statistic_type)
		v_job_id,
		'situation_result'::scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		UNNEST(array ['max_contribution'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [
			first_value(receptor_id) OVER (PARTITION BY calculation_result_set_id, assessment_area_id, hexagon_type ORDER BY result DESC, receptor_id),
			first_value(receptor_id) OVER (PARTITION BY calculation_result_set_id, assessment_area_id, hexagon_type ORDER BY total_result DESC, receptor_id)
		]) AS receptor_id

		FROM jobs.ae_calculation_sub_statistics_base(v_calculation_id)
			INNER JOIN jobs.ae_receptors_with_hexagon_type(ARRAY[v_calculation_id]) USING (receptor_id);


	-- Chart statistics
	-- Ensure only unique point/area combinations are used (otherwise critical_deposition_areas will cause duplicates)
	WITH sub_point_results AS (
		SELECT DISTINCT
			calculation_result_set_id,
			calculation_sub_point_id,
			receptor_id,
			assessment_area_id,
			result

			FROM jobs.ae_calculation_sub_statistics_base(v_calculation_id)
	)
	INSERT INTO jobs.scenario_calculation_assessment_area_chart_statistics (job_id, emission_result_chart_type, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, color_range_type, lower_bound, cartographic_surface)
	SELECT
		v_job_id,
		'emission_result'::emission_result_chart_type,
		scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		color_range_type,
		(UNNEST(ae_color_range(color_range_type, result::numeric, 1))).lower_value::real AS lower_bound,
		(UNNEST(ae_color_range(color_range_type, result::numeric, 1))).total::real AS cartographic_surface

		FROM sub_point_results
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN jobs.ae_receptors_with_hexagon_type(ARRAY[v_calculation_id]) USING (receptor_id)
			INNER JOIN scenario_result_types_color_ranges ON (calculation_result_sets.result_type = scenario_result_types_color_ranges.emission_result_type AND calculation_result_sets.substance_id = scenario_result_types_color_ranges.substance_id)

		WHERE scenario_result_type = 'situation_result'::scenario_result_type

		GROUP BY scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, color_range_type;


	-- Assessment area and critical deposition area
	INSERT INTO jobs.scenario_calculation_critical_deposition_area_statistics (job_id, scenario_result_type, calculation_result_set_id, assessment_area_id, critical_deposition_area_id, hexagon_type, overlapping_hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		'situation_result'::scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		critical_deposition_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		UNNEST(array ['count_calculation_points'::result_statistic_type, 'max_contribution'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [count(DISTINCT (calculation_sub_point_id, receptor_id))::real, max(result), max(total_result)]) AS value

		FROM jobs.ae_calculation_sub_statistics_base(v_calculation_id)
			INNER JOIN jobs.ae_receptors_with_hexagon_type(ARRAY[v_calculation_id]) USING (receptor_id)

		GROUP BY calculation_result_set_id, assessment_area_id, critical_deposition_area_id, hexagon_type;
$BODY$
LANGUAGE SQL VOLATILE;


/*
 * ae_scenario_calculation_receptor_statistics
 * -------------------------------------------
 * Function returning the receptors with the maximum values per statistic for a single calculation.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_calculation_receptor_statistics(v_result_type scenario_result_type, v_calculation_id integer)
	RETURNS TABLE(emission_result_type emission_result_type, substance_id smallint, scenario_result_type scenario_result_type, hexagon_type hexagon_type, overlapping_hexagon_type overlapping_hexagon_type, result_statistic_type result_statistic_type, assessment_area_id integer, receptor_id integer) AS
$BODY$

	SELECT
		calculation_result_sets.result_type AS emission_result_type,
		substance_id,
		scenario_result_type,
		hexagon_type,
		overlapping_hexagon_type,
		result_statistic_type,
		(ae_max_with_key(assessment_area_id::numeric, value::numeric)).key::integer as assessment_area_id,
		(ae_max_with_key(receptor_id::numeric, value::numeric)).key::integer as receptor_id

		FROM jobs.scenario_calculation_assessment_area_statistic_markers
			INNER JOIN jobs.scenario_calculation_assessment_area_statistics USING (calculation_result_set_id, scenario_result_type, hexagon_type, overlapping_hexagon_type, assessment_area_id, result_statistic_type)
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)

		WHERE calculation_id = v_calculation_id
			AND scenario_result_type = v_result_type

		GROUP BY emission_result_type, substance_id, scenario_result_type, calculation_result_set_id, hexagon_type, overlapping_hexagon_type, result_statistic_type
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_receptor_statistics
 * -------------------------------
 * Function returning the receptors with the maximum values per statistic for a scenario job.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_receptor_statistics(v_result_type scenario_result_type, v_job_id integer)
	RETURNS TABLE(emission_result_type emission_result_type, substance_id smallint, scenario_result_type scenario_result_type, hexagon_type hexagon_type, result_statistic_type result_statistic_type, assessment_area_id integer, receptor_id integer) AS
$BODY$

	SELECT
		emission_result_type,
		substance_id,
		scenario_result_type,
		hexagon_type,
		result_statistic_type,
		(ae_max_with_key(assessment_area_id::numeric, value::numeric)).key::integer as assessment_area_id,
		(ae_max_with_key(receptor_id::numeric, value::numeric)).key::integer as receptor_id

		FROM jobs.scenario_assessment_area_statistic_markers
			INNER JOIN jobs.scenario_assessment_area_statistics USING (job_id, emission_result_type, substance_id, scenario_result_type, hexagon_type, assessment_area_id, result_statistic_type)

		WHERE job_id = v_job_id
			AND scenario_result_type = v_result_type

		GROUP BY emission_result_type, substance_id, scenario_result_type, hexagon_type, result_statistic_type
$BODY$
LANGUAGE SQL STABLE;
