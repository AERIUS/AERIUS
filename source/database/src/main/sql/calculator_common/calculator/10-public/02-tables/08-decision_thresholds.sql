/*
 * development_pressure_classes
 * ----------------------------
 * Table containing development pressure classes.
 * The nearby_projects_range contains the range of number of other projects within the 'zone of influence'.
 * Over all rows, this range should be without gaps from 0 to infinity.
 */
CREATE TABLE development_pressure_classes (
	development_pressure_class_id integer NOT NULL,
	code text NOT NULL UNIQUE,
	description text NOT NULL,
	nearby_projects_range int4range NOT NULL,

	CONSTRAINT development_pressure_classes_pkey PRIMARY KEY (development_pressure_class_id),
	CONSTRAINT development_pressure_classes_range_excl EXCLUDE USING gist (nearby_projects_range WITH &&)
);


/*
 * receptor_decision_making_thresholds
 * -----------------------------------
 * Table containing decision making thresholds per receptor and pollutant.
 */
CREATE TABLE receptor_decision_making_thresholds (
	receptor_id integer NOT NULL,
	emission_result_type emission_result_type NOT NULL,
	substance_id smallint NOT NULL,
	threshold real NOT NULL,

	CONSTRAINT receptor_decision_making_thresholds_pkey PRIMARY KEY (receptor_id, emission_result_type, substance_id),
	CONSTRAINT receptor_decision_making_thresholds_fkey_substances FOREIGN KEY (substance_id) REFERENCES substances
);


/*
 * receptor_site_relevant_thresholds
 * ---------------------------------
 * Table containing site relevant thresholds per development pressure class, receptor and pollutant.
 */
CREATE TABLE receptor_site_relevant_thresholds (
	development_pressure_class_id integer NOT NULL,
	receptor_id integer NOT NULL,
	assessment_area_id integer NOT NULL,
	emission_result_type emission_result_type NOT NULL,
	substance_id smallint NOT NULL,
	threshold real NOT NULL,

	CONSTRAINT receptor_site_relevant_thresholds_pkey PRIMARY KEY (development_pressure_class_id, receptor_id, assessment_area_id, emission_result_type, substance_id),
	CONSTRAINT receptor_site_relevant_thresholds_fkey_pressure_class FOREIGN KEY (development_pressure_class_id) REFERENCES development_pressure_classes,
	CONSTRAINT receptor_site_relevant_thresholds_fkey_substances FOREIGN KEY (substance_id) REFERENCES substances
);


/*
 * jobs_development_pressure_class
 * -------------------------------
 * Table containing the development pressure class for a job.
 */
CREATE TABLE jobs.job_development_pressure_class (
	job_id integer NOT NULL,
	development_pressure_class_id integer NOT NULL,

	CONSTRAINT job_development_pressure_class_pkey PRIMARY KEY (job_id),
	CONSTRAINT job_development_pressure_class_fk_jobs FOREIGN KEY (job_id) REFERENCES jobs.jobs ON DELETE CASCADE,
	CONSTRAINT job_development_pressure_class_fk_classes FOREIGN KEY (development_pressure_class_id) REFERENCES development_pressure_classes
);
