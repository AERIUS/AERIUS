/*
 * ae_fill_temporary_effect_results
 * --------------------------------
 * Function to persist results per receptor for a temporary effect calculation.
 */
CREATE OR REPLACE FUNCTION jobs.ae_fill_temporary_effect_results(v_job_id integer, v_temporary_calculation_ids integer[], v_reference_calculation_id integer, v_off_site_reduction_calculation_id integer)
	RETURNS void AS
$BODY$
	INSERT INTO jobs.temporary_effect_calculation_results (job_id, emission_result_type, substance_id, calculation_id, receptor_id, result, total_result)
	SELECT
		v_job_id,
		emission_result_type,
		substance_id,
		calculation_id,
		receptor_id,
		calculation_results.result,
		calculation_results.result + COALESCE(background.result, 0) AS total_result

		FROM jobs.ae_scenario_temporary_calculation(v_temporary_calculation_ids, v_reference_calculation_id, v_off_site_reduction_calculation_id) AS calculation_results
			INNER JOIN jobs.calculations USING (calculation_id)
			LEFT JOIN receptor_background_results_view AS background USING (receptor_id, emission_result_type, substance_id, year);

	INSERT INTO jobs.temporary_effect_calculation_sub_results (job_id, emission_result_type, substance_id, calculation_id, calculation_sub_point_id, receptor_id, result, total_result)
	SELECT
		v_job_id,
		emission_result_type,
		substance_id,
		calculation_id,
		calculation_sub_point_id,
		receptor_id,
		calculation_results.result,
		calculation_results.result + COALESCE(background.result, 0) AS total_result

		FROM jobs.ae_scenario_calculation_sub_point_temporary_results(v_temporary_calculation_ids, v_reference_calculation_id, v_off_site_reduction_calculation_id) AS calculation_results
			INNER JOIN jobs.calculations USING (calculation_id)
			LEFT JOIN receptor_background_results_view AS background USING (receptor_id, emission_result_type, substance_id, year);

	INSERT INTO jobs.temporary_effect_receptors (job_id, receptor_id, hexagon_type, overlapping_hexagon_type)
	SELECT
		v_job_id,
		receptor_id,
		hexagon_type,
		overlapping_hexagon_type

		FROM jobs.ae_scenario_calculation_receptors(array_cat(v_temporary_calculation_ids, ARRAY [v_reference_calculation_id]));
$BODY$
LANGUAGE SQL VOLATILE;


/*
 * ae_scenario_job_temporary_effect_deposition
 * -------------------------------------------
 * Function to determine results for a temporary effect calculation based on supplied job key.
 * Only returns the total nitrogen deposition results, NOx and NH3 combined, filtered by deposition threshold value.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_job_temporary_effect_deposition(v_job_key text, v_hexagon_type hexagon_type, v_overlapping_hexagon_type overlapping_hexagon_type)
	RETURNS TABLE(receptor_id integer, deposition real) AS
$BODY$
	SELECT
		receptor_id,
		result AS deposition

	FROM jobs.jobs
		INNER JOIN jobs.temporary_effect_calculation_results USING (job_id)
		INNER JOIN jobs.temporary_effect_receptors USING (job_id, receptor_id)

	WHERE jobs.key = v_job_key
		AND hexagon_type = v_hexagon_type
		AND overlapping_hexagon_type = v_overlapping_hexagon_type
		AND emission_result_type = 'deposition'
		AND substance_id = 1711
		AND ABS(result) > ae_constant('PRONOUNCEMENT_THRESHOLD_VALUE')::posreal;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_job_temporary_effect_results
 * ----------------------------------------
 * Function to determine results for a temporary effect calculation, based on supplied job key.
 * Returns all emission results, and does not filter on things like nitrogen deposition threshold.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_job_temporary_effect_results(v_job_key text, v_hexagon_type hexagon_type, v_overlapping_hexagon_type overlapping_hexagon_type)
	RETURNS TABLE(receptor_id integer, emission_result_type emission_result_type, substance_id smallint, result real) AS
$BODY$
	SELECT
		receptor_id,
		emission_result_type,
		substance_id,
		result

	FROM jobs.jobs
		INNER JOIN jobs.temporary_effect_calculation_results USING (job_id)
		INNER JOIN jobs.temporary_effect_receptors USING (job_id, receptor_id)

	WHERE jobs.key = v_job_key
		AND hexagon_type = v_hexagon_type
		AND overlapping_hexagon_type = v_overlapping_hexagon_type;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_job_temporary_effect_sub_results
 * --------------------------------------------
 * Function to determine results for a temporary effect calculation, based on supplied job key, for sub points.
 * Returns all emission results for sub points, and does not filter on things like nitrogen deposition threshold.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_job_temporary_effect_sub_results(v_job_key text, v_hexagon_type hexagon_type)
	RETURNS TABLE(calculation_sub_point_id integer, receptor_id integer, emission_result_type emission_result_type, substance_id smallint, result real, geometry geometry(Point)) AS
$BODY$
	SELECT
		calculation_sub_point_id,
		receptor_id,
		emission_result_type,
		substance_id,
		result,
		geometry

	FROM jobs.jobs
		INNER JOIN jobs.temporary_effect_calculation_sub_results USING (job_id)
		INNER JOIN jobs.calculations USING (calculation_id)
		INNER JOIN jobs.calculation_sub_points USING (calculation_sub_point_set_id, calculation_sub_point_id, receptor_id)
		INNER JOIN receptor_hexagon_type_view USING (receptor_id)

	WHERE jobs.key = v_job_key
		AND hexagon_type = v_hexagon_type;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_temporary_effect_statistics_base
 * --------------------------------------------
 * Base function to determine statistics for a temporary effect calculation.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_temporary_effect_statistics_base(v_job_id integer)
	RETURNS TABLE(job_id integer, emission_result_type emission_result_type, substance_id smallint, receptor_id integer, result real, total_result real) AS
$BODY$
	SELECT
		job_id,
		emission_result_type,
		substance_id,
		receptor_id,
		calculation_results.result,
		calculation_results.total_result

	FROM jobs.temporary_effect_calculation_results AS calculation_results

	WHERE job_id = v_job_id
		-- If it's deposition take the threshold into account, otherwise don't.
		-- Only do deposition, concentrations will be on sub level.
		AND emission_result_type = 'deposition'
		AND ABS(calculation_results.result) > ae_constant('PRONOUNCEMENT_THRESHOLD_VALUE')::posreal;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_temporary_effect_areas_statistics_base
 * --------------------------------------------------
 * Base function to determine statistics for a temporary effect calculation for assessment areas/habitats.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_temporary_effect_areas_statistics_base(v_job_id integer)
	RETURNS TABLE(job_id integer, emission_result_type emission_result_type, substance_id smallint, hexagon_type hexagon_type, overlapping_hexagon_type overlapping_hexagon_type, receptor_id integer, assessment_area_id integer, critical_deposition_area_id integer, cartographic_surface real, result real, total_result real) AS
$BODY$
	SELECT
		job_id,
		emission_result_type,
		substance_id,
		hexagon_type,
		overlapping_hexagon_type,
		receptor_id,
		assessment_area_id,
		critical_deposition_area_id,
		cartographic_surface,
		calculation_results.result,
		calculation_results.total_result

		FROM jobs.ae_scenario_temporary_effect_statistics_base(v_job_id) AS calculation_results
			INNER JOIN receptors_to_relevant_habitats USING (receptor_id)
			INNER JOIN jobs.temporary_effect_receptors USING (job_id, receptor_id)
		WHERE hexagon_type != 'extra_assessment_hexagons'
	UNION ALL
	SELECT
		job_id,
		emission_result_type,
		substance_id,
		hexagon_type,
		overlapping_hexagon_type,
		receptor_id,
		assessment_area_id,
		habitat_type_id AS critical_deposition_area_id,
		1 AS cartographic_surface,
		calculation_results.result,
		calculation_results.total_result

		FROM jobs.ae_scenario_temporary_effect_statistics_base(v_job_id) AS calculation_results
			INNER JOIN receptors_to_assessment_areas USING (receptor_id)
			INNER JOIN jobs.temporary_effect_receptors USING (job_id, receptor_id)
			INNER JOIN extra_assessment_receptors USING (receptor_id, assessment_area_id)
		WHERE hexagon_type = 'extra_assessment_hexagons';
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_temporary_effect_sub_statistics_base
 * ------------------------------------------------
 * Base function to determine statistics for a temporary effect calculation for sub points.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_temporary_effect_sub_statistics_base(v_job_id integer)
	RETURNS TABLE(job_id integer, emission_result_type emission_result_type, substance_id smallint, calculation_sub_point_id integer, receptor_id integer, assessment_area_id integer, critical_deposition_area_id integer, result real, total_result real) AS
$BODY$
	SELECT
		job_id,
		emission_result_type,
		substance_id,
		calculation_sub_point_id,
		receptor_id,
		assessment_area_id,
		critical_deposition_area_id,
		calculation_results.result,
		calculation_results.total_result

	FROM jobs.temporary_effect_calculation_sub_results AS calculation_results
		INNER JOIN receptors_to_relevant_habitats USING (receptor_id)

	WHERE job_id = v_job_id
		-- Only do sub statistics for non-depositions
		AND emission_result_type != 'deposition';
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_temporary_effect_statistics
 * ---------------------------------------
 * Function returning the general statistics for a temporary effect calculation.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_temporary_effect_statistics(v_job_id integer)
	RETURNS TABLE(emission_result_type emission_result_type, substance_id smallint, hexagon_type hexagon_type, overlapping_hexagon_type overlapping_hexagon_type, result_statistic_type result_statistic_type, value real) AS
$BODY$
	SELECT
		emission_result_type,
		substance_id,
		hexagon_type,
		overlapping_hexagon_type,
		UNNEST(array ['count_receptors'::result_statistic_type, 'max_temp_increase'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [count(DISTINCT receptor_id)::real, max(result), max(total_result)]) AS value

		FROM jobs.ae_scenario_temporary_effect_statistics_base(v_job_id)
			INNER JOIN jobs.temporary_effect_receptors USING (job_id, receptor_id)

		GROUP BY emission_result_type, substance_id, hexagon_type, overlapping_hexagon_type
	UNION ALL
	SELECT
		emission_result_type,
		substance_id,
		hexagon_type,
		overlapping_hexagon_type,
		UNNEST(array ['sum_cartographic_surface'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [sum(cartographic_surface::numeric)::real]) AS value

		FROM jobs.ae_scenario_temporary_effect_areas_statistics_base(v_job_id)

		GROUP BY emission_result_type, substance_id, hexagon_type, overlapping_hexagon_type
	UNION ALL
	SELECT
		emission_result_type,
		substance_id,
		hexagon_type,
		overlapping_hexagon_type,
		UNNEST(array ['count_calculation_points'::result_statistic_type, 'max_temp_increase'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [count(DISTINCT (calculation_sub_point_id, receptor_id))::real, max(result), max(total_result)]) AS value

		FROM jobs.ae_scenario_temporary_effect_sub_statistics_base(v_job_id)
			INNER JOIN jobs.temporary_effect_receptors USING (job_id, receptor_id)

		GROUP BY emission_result_type, substance_id, hexagon_type, overlapping_hexagon_type
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_temporary_effect_fill_calculation_statistics
 * --------------------------------------------------------
 * Function to determine all statistics for a temporary effect calculation and to persist them in the appropriate tables.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_temporary_effect_fill_calculation_statistics(v_job_id integer, v_temporary_calculation_ids integer[], v_reference_calculation_id integer, v_off_site_reduction_calculation_id integer)
	RETURNS void AS
$BODY$
	-- Assessment area
	INSERT INTO jobs.scenario_assessment_area_statistics (job_id, emission_result_type, substance_id, scenario_result_type, assessment_area_id, hexagon_type, overlapping_hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		emission_result_type,
		substance_id,
		'max_temporary_effect'::scenario_result_type,
		assessment_area_id,
		hexagon_type,
		overlapping_hexagon_type,
		UNNEST(array ['count_receptors'::result_statistic_type, 'sum_cartographic_surface'::result_statistic_type, 'max_temp_increase'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [count(DISTINCT receptor_id)::real, sum(cartographic_surface::numeric)::real, max(result), max(total_result)]) AS value

		FROM jobs.ae_scenario_temporary_effect_areas_statistics_base(v_job_id)
		GROUP BY emission_result_type, substance_id, assessment_area_id, hexagon_type, overlapping_hexagon_type;


	-- Assessment area and critical deposition area
	INSERT INTO jobs.scenario_critical_deposition_area_statistics (job_id, emission_result_type, substance_id, scenario_result_type, assessment_area_id, critical_deposition_area_id, hexagon_type, overlapping_hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		emission_result_type,
		substance_id,
		'max_temporary_effect'::scenario_result_type,
		assessment_area_id,
		critical_deposition_area_id,
		hexagon_type,
		overlapping_hexagon_type,
		UNNEST(array ['count_receptors'::result_statistic_type, 'sum_cartographic_surface'::result_statistic_type, 'max_temp_increase'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [count(DISTINCT receptor_id)::real, sum(cartographic_surface::numeric)::real, max(result), max(total_result)]) AS value

		FROM jobs.ae_scenario_temporary_effect_areas_statistics_base(v_job_id)
		GROUP BY emission_result_type, substance_id, assessment_area_id, critical_deposition_area_id, hexagon_type, overlapping_hexagon_type;


	-- Markers
	-- markers for the 'non_overlapping_hexagons_only' set don't need to be calculated
	INSERT INTO jobs.scenario_assessment_area_statistic_markers (job_id, emission_result_type, substance_id, scenario_result_type, assessment_area_id, hexagon_type, overlapping_hexagon_type, result_statistic_type, receptor_id)
	SELECT DISTINCT ON (emission_result_type, substance_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, result_statistic_type)
		v_job_id,
		emission_result_type,
		substance_id,
		'max_temporary_effect'::scenario_result_type,
		assessment_area_id,
		hexagon_type,
		overlapping_hexagon_type,
		UNNEST(array ['max_temp_increase'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [
			first_value(receptor_id) OVER (PARTITION BY emission_result_type, substance_id, hexagon_type, overlapping_hexagon_type, assessment_area_id ORDER BY result DESC, receptor_id),
			first_value(receptor_id) OVER (PARTITION BY emission_result_type, substance_id, hexagon_type, overlapping_hexagon_type, assessment_area_id ORDER BY total_result DESC, receptor_id)
		]) AS receptor_id

		FROM jobs.ae_scenario_temporary_effect_areas_statistics_base(v_job_id)
		WHERE overlapping_hexagon_type != 'non_overlapping_hexagons_only';


	-- Charts
	-- charts for the 'non_overlapping_hexagons_only' set don't need to be calculated
	INSERT INTO jobs.scenario_assessment_area_chart_statistics (job_id, emission_result_chart_type, emission_result_type, substance_id, scenario_result_type, assessment_area_id, hexagon_type, overlapping_hexagon_type, color_range_type, lower_bound, cartographic_surface)
	SELECT
		v_job_id,
		'emission_result'::emission_result_chart_type,
		emission_result_type,
		substance_id,
		scenario_result_type,
		assessment_area_id,
		hexagon_type,
		overlapping_hexagon_type,
		color_range_type,
		(UNNEST(ae_color_range(color_range_type, result::numeric, cartographic_surface::numeric))).lower_value::real AS lower_bound,
		(UNNEST(ae_color_range(color_range_type, result::numeric, cartographic_surface::numeric))).total::real AS cartographic_surface

		FROM jobs.ae_scenario_temporary_effect_areas_statistics_base(v_job_id)
			INNER JOIN scenario_result_types_color_ranges USING (emission_result_type, substance_id)

		WHERE scenario_result_type = 'max_temporary_effect'::scenario_result_type
			AND overlapping_hexagon_type != 'non_overlapping_hexagons_only'
			AND hexagon_type != 'extra_assessment_hexagons'

		GROUP BY emission_result_type, substance_id, scenario_result_type, assessment_area_id, color_range_type, hexagon_type, overlapping_hexagon_type;

	-- Special case for extra_assessment_hexagons, where we want to display number of unique receptors
	WITH area_results AS (
	SELECT DISTINCT
		emission_result_type,
		substance_id,
		hexagon_type,
		overlapping_hexagon_type,
		receptor_id,
		assessment_area_id,
		result

		FROM jobs.ae_scenario_temporary_effect_areas_statistics_base(v_job_id)
		WHERE hexagon_type = 'extra_assessment_hexagons'
			AND overlapping_hexagon_type != 'non_overlapping_hexagons_only'
	)
	INSERT INTO jobs.scenario_assessment_area_chart_statistics (job_id, emission_result_chart_type, emission_result_type, substance_id, scenario_result_type, assessment_area_id, hexagon_type, overlapping_hexagon_type, color_range_type, lower_bound, cartographic_surface)
	SELECT
		v_job_id,
		'emission_result'::emission_result_chart_type,
		emission_result_type,
		substance_id,
		scenario_result_type,
		assessment_area_id,
		hexagon_type,
		overlapping_hexagon_type,
		color_range_type,
		(UNNEST(ae_color_range(color_range_type, result::numeric, 1::numeric))).lower_value::real AS lower_bound,
		(UNNEST(ae_color_range(color_range_type, result::numeric, 1::numeric))).total::real AS cartographic_surface

		FROM area_results
			INNER JOIN scenario_result_types_color_ranges USING (emission_result_type, substance_id)

		WHERE scenario_result_type = 'max_temporary_effect'::scenario_result_type

		GROUP BY emission_result_type, substance_id, scenario_result_type, assessment_area_id, color_range_type, hexagon_type, overlapping_hexagon_type;


	-- Sub points
	-- Assessment area
	INSERT INTO jobs.scenario_assessment_area_statistics (job_id, emission_result_type, substance_id, scenario_result_type, assessment_area_id, hexagon_type, overlapping_hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		emission_result_type,
		substance_id,
		'max_temporary_effect'::scenario_result_type,
		assessment_area_id,
		hexagon_type,
		overlapping_hexagon_type,
		UNNEST(array ['count_calculation_points'::result_statistic_type, 'max_temp_increase'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [count(DISTINCT (calculation_sub_point_id, receptor_id))::real, max(result), max(total_result)]) AS value

		FROM jobs.ae_scenario_temporary_effect_sub_statistics_base(v_job_id)
			INNER JOIN jobs.temporary_effect_receptors using (job_id, receptor_id)
			GROUP BY emission_result_type, substance_id, assessment_area_id, hexagon_type, overlapping_hexagon_type;


	-- Assessment area and critical deposition area
	INSERT INTO jobs.scenario_critical_deposition_area_statistics (job_id, emission_result_type, substance_id, scenario_result_type, assessment_area_id, critical_deposition_area_id, hexagon_type, overlapping_hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		emission_result_type,
		substance_id,
		'max_temporary_effect'::scenario_result_type,
		assessment_area_id,
		critical_deposition_area_id,
		hexagon_type,
		overlapping_hexagon_type,
		UNNEST(array ['count_calculation_points'::result_statistic_type, 'max_temp_increase'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [count(DISTINCT (calculation_sub_point_id, receptor_id))::real, max(result), max(total_result)]) AS value

		FROM jobs.ae_scenario_temporary_effect_sub_statistics_base(v_job_id)
			INNER JOIN jobs.temporary_effect_receptors USING (job_id, receptor_id)
		GROUP BY emission_result_type, substance_id, assessment_area_id, critical_deposition_area_id, hexagon_type, overlapping_hexagon_type;

	-- Markers
	-- markers for the 'non_overlapping_hexagons_only' set don't need to be calculated
	WITH filtered_receptors AS (
		SELECT * FROM jobs.temporary_effect_receptors
			WHERE job_id = v_job_id
				AND overlapping_hexagon_type != 'non_overlapping_hexagons_only'
	)
	INSERT INTO jobs.scenario_assessment_area_statistic_markers (job_id, emission_result_type, substance_id, scenario_result_type, assessment_area_id, hexagon_type, overlapping_hexagon_type, result_statistic_type, receptor_id)
	SELECT DISTINCT ON (emission_result_type, substance_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, result_statistic_type)
		v_job_id,
		emission_result_type,
		substance_id,
		'max_temporary_effect'::scenario_result_type,
		assessment_area_id,
		hexagon_type,
		overlapping_hexagon_type,
		UNNEST(array ['max_temp_increase'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [
			first_value(receptor_id) OVER (PARTITION BY emission_result_type, substance_id, hexagon_type, overlapping_hexagon_type, assessment_area_id ORDER BY result DESC, receptor_id),
			first_value(receptor_id) OVER (PARTITION BY emission_result_type, substance_id, hexagon_type, overlapping_hexagon_type, assessment_area_id ORDER BY total_result DESC, receptor_id)
		]) AS receptor_id

		FROM jobs.ae_scenario_temporary_effect_sub_statistics_base(v_job_id)
			INNER JOIN filtered_receptors USING (job_id, receptor_id);

	-- Charts
	-- charts for the 'non_overlapping_hexagons_only' set don't need to be calculated
	WITH filtered_receptors AS (
		SELECT * FROM jobs.temporary_effect_receptors
			WHERE job_id = v_job_id
				AND overlapping_hexagon_type != 'non_overlapping_hexagons_only'
	-- Ensure only unique point/area combinations are used (otherwise critical_deposition_areas will cause duplicates)
	), sub_point_results AS (
		SELECT DISTINCT
			job_id,
			emission_result_type,
			substance_id,
			calculation_sub_point_id,
			receptor_id,
			assessment_area_id,
			result

			FROM jobs.ae_scenario_temporary_effect_sub_statistics_base(v_job_id)
	)
	INSERT INTO jobs.scenario_assessment_area_chart_statistics (job_id, emission_result_chart_type, emission_result_type, substance_id, scenario_result_type, assessment_area_id, hexagon_type, overlapping_hexagon_type, color_range_type, lower_bound, cartographic_surface)
	SELECT
		v_job_id,
		'emission_result'::emission_result_chart_type,
		emission_result_type,
		substance_id,
		scenario_result_type,
		assessment_area_id,
		hexagon_type,
		overlapping_hexagon_type,
		color_range_type,
		(UNNEST(ae_color_range(color_range_type, result::numeric, 1))).lower_value::real AS lower_bound,
		(UNNEST(ae_color_range(color_range_type, result::numeric, 1))).total::real AS cartographic_surface

		FROM sub_point_results
			INNER JOIN filtered_receptors USING (job_id, receptor_id)
			INNER JOIN scenario_result_types_color_ranges USING (emission_result_type, substance_id)

		WHERE scenario_result_type = 'max_temporary_effect'::scenario_result_type

		GROUP BY emission_result_type, substance_id, scenario_result_type, assessment_area_id, color_range_type, hexagon_type, overlapping_hexagon_type;

$BODY$
LANGUAGE SQL VOLATILE;


/*
 * scenario_temporary_effect_calculation_edge_effect_table
 * -------------------------------------------------------
 * Function to determine the non-overlapping hexagons and their nitrogen deposition results for a temporary effect calculation.
 */
CREATE OR REPLACE FUNCTION jobs.scenario_temporary_effect_calculation_edge_effect_table(v_job_id integer, v_hexagon_type hexagon_type)
	RETURNS TABLE (assessment_area_id integer, receptor_id integer, result real, reference_result real, off_site_reduction_result real, proposed_result real) AS
$BODY$
	WITH correct_job AS (
		SELECT * FROM jobs.jobs WHERE job_id = v_job_id
	), job_calcs AS (
		SELECT
			jobs.ae_scenario_get_calculation_id_of_situation_type(key, 'reference') AS reference_calc_id,
			jobs.ae_scenario_get_calculation_id_of_situation_type(key, 'off_site_reduction') AS off_site_reduction_calculation_id,
			jobs.ae_scenario_get_calculation_ids_of_situation_type(key, 'temporary') AS temporary_calc_ids
			FROM correct_job
	)
	SELECT
		assessment_area_id,
		receptor_id,
		temporary_effect_calculation_results.result,
		reference_results.result,
		off_site_reduction_results.result,
		max_temporary_results.result

	FROM correct_job
		INNER JOIN jobs.temporary_effect_calculation_results USING (job_id)
		INNER JOIN jobs.temporary_effect_receptors USING (job_id, receptor_id)
		INNER JOIN receptors_to_assessment_areas USING (receptor_id)
		LEFT JOIN jobs.ae_calculation_results((SELECT reference_calc_id FROM job_calcs)) AS reference_results USING (receptor_id, substance_id, emission_result_type)
		LEFT JOIN jobs.ae_calculation_results((SELECT off_site_reduction_calculation_id FROM job_calcs)) AS off_site_reduction_results USING (receptor_id, substance_id, emission_result_type)
		LEFT JOIN jobs.ae_calculations_max_results((SELECT temporary_calc_ids FROM job_calcs)) AS max_temporary_results USING (receptor_id, substance_id, emission_result_type)

	WHERE substance_id = 1711
		AND emission_result_type = 'deposition'
		AND hexagon_type = v_hexagon_type
		AND overlapping_hexagon_type ='non_overlapping_hexagons_only'
		AND ABS(temporary_effect_calculation_results.result) > ae_constant('PRONOUNCEMENT_THRESHOLD_VALUE')::posreal;
$BODY$
LANGUAGE SQL STABLE;
