/**
 * ae_copy_job
 * -----------
 * Function to copy a job and the results to another job.
 * It reuses the calculation results, but the statistics are linked to the job_id
 * therefore the statistics are copied.
 *
 * @param v_from_job_key the job_key of the job to copy the data from
 * @param v_to_job_key the job key of the job to copy the data to
 */
CREATE OR REPLACE FUNCTION jobs.ae_copy_job(v_from_job_key text, v_to_job_key text)
	RETURNS void AS
$BODY$
DECLARE
	v_from_job_id integer;
	v_to_job_id integer;
BEGIN
	SELECT job_id INTO v_from_job_id FROM jobs.jobs WHERE key = v_from_job_key;
 	SELECT job_id INTO v_to_job_id FROM jobs.jobs WHERE key = v_to_job_key;

	INSERT INTO jobs.job_calculations(job_id, calculation_id, situation_reference, situation_type, situation_name, situation_version)
		SELECT v_to_job_id, calculation_id, situation_reference, situation_type, situation_name, situation_version
		FROM jobs.job_calculations
		WHERE job_id = v_from_job_id;

	INSERT INTO jobs.scenario_calculation_assessment_area_statistics (calculation_result_set_id, scenario_result_type, hexagon_type,
		overlapping_hexagon_type, assessment_area_id, result_statistic_type, job_id, value)
		SELECT calculation_result_set_id, scenario_result_type, hexagon_type, overlapping_hexagon_type, assessment_area_id, result_statistic_type, v_to_job_id, value
		FROM jobs.scenario_calculation_assessment_area_statistics
		WHERE job_id = v_from_job_id;

	INSERT INTO jobs.scenario_calculation_assessment_area_statistic_markers (calculation_result_set_id, scenario_result_type, hexagon_type, overlapping_hexagon_type, assessment_area_id, result_statistic_type, job_id, receptor_id)
		SELECT calculation_result_set_id, scenario_result_type, hexagon_type, overlapping_hexagon_type, assessment_area_id, result_statistic_type, v_to_job_id, receptor_id
		FROM jobs.scenario_calculation_assessment_area_statistic_markers
		WHERE job_id = v_from_job_id;

	INSERT INTO jobs.scenario_calculation_critical_deposition_area_statistics (calculation_result_set_id, scenario_result_type, hexagon_type, overlapping_hexagon_type, assessment_area_id, critical_deposition_area_id, result_statistic_type, job_id, value)
		SELECT calculation_result_set_id, scenario_result_type, hexagon_type, overlapping_hexagon_type, assessment_area_id, critical_deposition_area_id, result_statistic_type, v_to_job_id, value
		FROM jobs.scenario_calculation_critical_deposition_area_statistics
		WHERE job_id = v_from_job_id;

	INSERT INTO jobs.scenario_calculation_assessment_area_chart_statistics (calculation_result_set_id, emission_result_chart_type, scenario_result_type, hexagon_type, overlapping_hexagon_type, assessment_area_id, color_range_type, lower_bound, job_id, cartographic_surface)
		SELECT calculation_result_set_id, emission_result_chart_type, scenario_result_type, hexagon_type, overlapping_hexagon_type, assessment_area_id, color_range_type, lower_bound, v_to_job_id, cartographic_surface
		FROM jobs.scenario_calculation_assessment_area_chart_statistics
		WHERE job_id = v_from_job_id;

	INSERT INTO jobs.scenario_assessment_area_statistics (job_id, emission_result_type, substance_id, scenario_result_type, hexagon_type, overlapping_hexagon_type, assessment_area_id, result_statistic_type, value)
		SELECT v_to_job_id, emission_result_type, substance_id, scenario_result_type, hexagon_type, overlapping_hexagon_type, assessment_area_id, result_statistic_type, value
		FROM jobs.scenario_assessment_area_statistics
		WHERE job_id = v_from_job_id;

	INSERT INTO jobs.scenario_assessment_area_statistic_markers (job_id, emission_result_type, substance_id, scenario_result_type, hexagon_type, overlapping_hexagon_type, assessment_area_id, result_statistic_type, receptor_id)
		SELECT v_to_job_id, emission_result_type, substance_id, scenario_result_type, hexagon_type, overlapping_hexagon_type, assessment_area_id, result_statistic_type, receptor_id
		FROM jobs.scenario_assessment_area_statistic_markers
		WHERE job_id = v_from_job_id;

	INSERT INTO jobs.scenario_critical_deposition_area_statistics (job_id, emission_result_type, substance_id, scenario_result_type, hexagon_type, overlapping_hexagon_type, assessment_area_id, critical_deposition_area_id, result_statistic_type, value)
		SELECT v_to_job_id, emission_result_type, substance_id, scenario_result_type, hexagon_type, overlapping_hexagon_type, assessment_area_id, critical_deposition_area_id, result_statistic_type, value
		FROM jobs.scenario_critical_deposition_area_statistics
		WHERE job_id = v_from_job_id;

	INSERT INTO jobs.scenario_assessment_area_chart_statistics (job_id, emission_result_chart_type, emission_result_type, substance_id, scenario_result_type, hexagon_type, overlapping_hexagon_type, assessment_area_id, color_range_type, lower_bound, cartographic_surface)
		SELECT v_to_job_id, emission_result_chart_type, emission_result_type, substance_id, scenario_result_type, hexagon_type, overlapping_hexagon_type, assessment_area_id, color_range_type, lower_bound, cartographic_surface
		FROM jobs.scenario_assessment_area_chart_statistics
		WHERE job_id = v_from_job_id;

END
$BODY$
LANGUAGE plpgsql VOLATILE;

