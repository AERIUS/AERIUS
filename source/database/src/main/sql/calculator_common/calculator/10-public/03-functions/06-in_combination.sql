/*
 * ae_fill_in_combination_results
 * ------------------------------
 * Function to persist results per receptor for an in-combination calculation.
 */
CREATE OR REPLACE FUNCTION jobs.ae_fill_in_combination_results(v_job_id integer, v_proposed_calculation_id integer)
	RETURNS void AS
$BODY$
	WITH correct_job AS (
		SELECT * FROM jobs.jobs WHERE job_id = v_job_id
	), job_calcs AS (
		SELECT
			jobs.ae_scenario_get_calculation_id_of_situation_type(key, 'reference') AS reference_calc_id,
			jobs.ae_scenario_get_calculation_id_of_situation_type(key, 'off_site_reduction') AS off_site_reduction_calculation_id,
			jobs.ae_scenario_get_calculation_ids_of_situation_type(key, 'combination_proposed') AS combination_proposed_calc_ids,
			jobs.ae_scenario_get_calculation_ids_of_situation_type(key, 'combination_reference') AS combination_reference_calc_ids

			FROM correct_job
	)
	INSERT INTO jobs.in_combination_calculation_results (calculation_result_set_id, receptor_id, result, total_result)
	SELECT
		calculation_result_set_id,
		calculation_results.receptor_id,
		calculation_results.result + COALESCE(archive_contribution.result, 0) AS result,
		calculation_results.result + COALESCE(archive_contribution.result, 0) + COALESCE(background.result, 0) AS total_result

		FROM jobs.ae_scenario_in_combination_calculation(v_proposed_calculation_id,
				(SELECT reference_calc_id FROM job_calcs),
				(SELECT off_site_reduction_calculation_id FROM job_calcs),
				(SELECT combination_proposed_calc_ids FROM job_calcs),
				(SELECT combination_reference_calc_ids FROM job_calcs)) AS calculation_results
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN jobs.calculations USING (calculation_id)
			LEFT JOIN jobs.archive_contribution_results AS archive_contribution USING (calculation_result_set_id, receptor_id)
			LEFT JOIN receptor_background_results_view AS background ON (
				background.receptor_id = calculation_results.receptor_id
					AND background.emission_result_type = calculation_result_sets.result_type
					AND background.substance_id = calculation_result_sets.substance_id
					AND background.year = jobs.calculations.year);

	WITH correct_job AS (
		SELECT * FROM jobs.jobs WHERE job_id = v_job_id
	), job_calcs AS (
		SELECT
			jobs.ae_scenario_get_calculation_id_of_situation_type(key, 'reference') AS reference_calc_id,
			jobs.ae_scenario_get_calculation_id_of_situation_type(key, 'off_site_reduction') AS off_site_reduction_calculation_id,
			jobs.ae_scenario_get_calculation_ids_of_situation_type(key, 'combination_proposed') AS combination_proposed_calc_ids,
			jobs.ae_scenario_get_calculation_ids_of_situation_type(key, 'combination_reference') AS combination_reference_calc_ids

			FROM correct_job
	)
	INSERT INTO jobs.in_combination_calculation_sub_results (calculation_result_set_id, calculation_sub_point_id, receptor_id, result, total_result)
	SELECT
		calculation_result_set_id,
		calculation_results.calculation_sub_point_id,
		calculation_results.receptor_id,
		calculation_results.result + COALESCE(archive_contribution.result, 0) AS result,
		calculation_results.result + COALESCE(archive_contribution.result, 0) + COALESCE(background.result, 0) AS total_result

		FROM jobs.ae_scenario_calculation_sub_point_in_combination_results(v_proposed_calculation_id,
				(SELECT reference_calc_id FROM job_calcs),
				(SELECT off_site_reduction_calculation_id FROM job_calcs),
				(SELECT combination_proposed_calc_ids FROM job_calcs),
				(SELECT combination_reference_calc_ids FROM job_calcs)) AS calculation_results
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN jobs.calculations USING (calculation_id)
			LEFT JOIN jobs.archive_contribution_sub_results AS archive_contribution USING (calculation_result_set_id, calculation_sub_point_id, receptor_id)
			LEFT JOIN receptor_background_results_view AS background ON (
				background.receptor_id = calculation_results.receptor_id
					AND background.emission_result_type = calculation_result_sets.result_type
					AND background.substance_id = calculation_result_sets.substance_id
					AND background.year = jobs.calculations.year);
$BODY$
LANGUAGE SQL VOLATILE;


/*
 * ae_scenario_job_in_combination_deposition
 * -----------------------------------------
 * Function to determine results for an in-combination calculation based on the supplied job key and proposed situation reference.
 * Only returns the total nitrogen deposition results, NOx and NH3 combined, filtered by deposition threshold value.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_job_in_combination_deposition(v_job_key text, v_proposed_situation_reference text, v_hexagon_type hexagon_type)
	RETURNS TABLE(receptor_id integer, deposition real) AS
$BODY$
	SELECT
		receptor_id,
		result AS deposition

		FROM jobs.in_combination_calculation_results
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN receptor_hexagon_type_view USING (receptor_id)

		WHERE result_type = 'deposition'
			AND substance_id = 1711
			AND calculation_id = (SELECT jobs.ae_scenario_get_calculation_id_of_situation_reference(v_job_key, v_proposed_situation_reference))
			AND hexagon_type = v_hexagon_type
			AND ABS(result) > ae_constant('PRONOUNCEMENT_THRESHOLD_VALUE')::posreal;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_job_in_combination_results
 * --------------------------------------
 * Function to determine results for an in-combination calculation, based on supplied job key and proposed situation reference.
 * Returns all emission results, and does not filter on things like nitrogen deposition threshold.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_job_in_combination_results(v_job_key text, v_proposed_situation_reference text, v_hexagon_type hexagon_type)
	RETURNS TABLE(receptor_id integer, emission_result_type emission_result_type, substance_id smallint, result real) AS
$BODY$
	SELECT
		receptor_id,
		result_type AS emission_result_type,
		substance_id,
		result

		FROM jobs.in_combination_calculation_results
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN receptor_hexagon_type_view USING (receptor_id)

		WHERE calculation_id = (SELECT jobs.ae_scenario_get_calculation_id_of_situation_reference(v_job_key, v_proposed_situation_reference))
			AND hexagon_type = v_hexagon_type;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_job_in_combination_sub_results
 * ------------------------------------------
 * Function to determine results for an in-combination calculation, based on supplied job key and proposed situation reference, for sub points.
 * Returns all emission results for sub points, and does not filter on things like nitrogen deposition threshold.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_job_in_combination_sub_results(v_job_key text, v_proposed_situation_reference text, v_hexagon_type hexagon_type)
	RETURNS TABLE(calculation_sub_point_id integer, receptor_id integer, emission_result_type emission_result_type, substance_id smallint, result real, geometry geometry(Point)) AS
$BODY$
	SELECT
		calculation_sub_point_id,
		receptor_id,
		result_type AS emission_result_type,
		substance_id,
		result,
		geometry

		FROM jobs.in_combination_calculation_sub_results
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN jobs.calculations USING (calculation_id)
			INNER JOIN jobs.calculation_sub_points USING (calculation_sub_point_set_id, calculation_sub_point_id, receptor_id)
			INNER JOIN receptor_hexagon_type_view USING (receptor_id)

		WHERE calculation_id = (SELECT jobs.ae_scenario_get_calculation_id_of_situation_reference(v_job_key, v_proposed_situation_reference))
			AND hexagon_type = v_hexagon_type;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_job_in_combination_percentage_cl
 * --------------------------------------------
 * Function to determine the percentages of critical levels for an in-combination calculation, based on supplied job key and proposed situation reference.
 * Only returns results for locations where there is a critical level for the emission result type/substance combination.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_job_in_combination_percentage_cl(v_job_key text, v_proposed_situation_reference text)
	RETURNS TABLE(receptor_id integer, emission_result_type emission_result_type, substance_id smallint, result real, percentage_cl real, total_percentage_cl real) AS
$BODY$
	SELECT
		calculation_results.receptor_id,
		calculation_result_sets.result_type AS emission_result_type,
		calculation_result_sets.substance_id,
		calculation_results.result,
		calculation_results.result / critical_level * 100::real AS percentage_cl,
		calculation_results.total_result / critical_level * 100::real AS total_percentage_cl

		FROM jobs.in_combination_calculation_results AS calculation_results
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN critical_levels USING (receptor_id, result_type, substance_id)

		WHERE critical_level > 0
			AND calculation_id = (SELECT jobs.ae_scenario_get_calculation_id_of_situation_reference(v_job_key, v_proposed_situation_reference));
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_job_in_combination_sub_percentage_cl
 * ------------------------------------------------
 * Function to determine the percentages of critical levels for an in-combination calculation, based on supplied job key and proposed situation reference, for sub points.
 * Critical level is based on the associated receptor, not the actual sub point area.
 * Only returns results for locations where there is a critical level for the emission result type/substance combination.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_job_in_combination_sub_percentage_cl(v_job_key text, v_proposed_situation_reference text, v_hexagon_type hexagon_type)
	RETURNS TABLE(calculation_sub_point_id integer, receptor_id integer, emission_result_type emission_result_type, substance_id smallint, result real, percentage_cl real, total_percentage_cl real, geometry geometry(Point)) AS
$BODY$
	SELECT
		calculation_results.calculation_sub_point_id,
		calculation_results.receptor_id,
		calculation_result_sets.result_type AS emission_result_type,
		calculation_result_sets.substance_id,
		calculation_results.result,
		calculation_results.result / critical_level * 100::real AS percentage_cl,
		calculation_results.total_result / critical_level * 100::real AS total_percentage_cl,
		calculation_sub_points.geometry

		FROM jobs.in_combination_calculation_sub_results AS calculation_results
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN jobs.calculations USING (calculation_id)
			INNER JOIN jobs.calculation_sub_points USING (calculation_sub_point_set_id, calculation_sub_point_id, receptor_id)
			INNER JOIN critical_levels USING (receptor_id, result_type, substance_id)
			INNER JOIN receptor_hexagon_type_view USING (receptor_id)

		WHERE critical_level > 0
			AND calculation_id = (SELECT jobs.ae_scenario_get_calculation_id_of_situation_reference(v_job_key, v_proposed_situation_reference))
			AND hexagon_type = v_hexagon_type;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_in_combination_statistics_base
 * ------------------------------------------
 * Base function to determine statistics for an in-combination calculation.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_in_combination_statistics_base(v_proposed_calculation_id integer)
	RETURNS TABLE(calculation_result_set_id integer, receptor_id integer, result real, total_result real) AS
$BODY$
	SELECT
		calculation_result_set_id,
		calculation_results.receptor_id,
		calculation_results.result,
		calculation_results.total_result

		FROM jobs.in_combination_calculation_results AS calculation_results
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
	
		WHERE calculation_id = v_proposed_calculation_id
			-- If it's deposition take the threshold into account, otherwise don't.
			-- Only do deposition, concentrations will be on sub level.
			AND calculation_result_sets.result_type = 'deposition'
			AND ABS(calculation_results.result) > ae_constant('PRONOUNCEMENT_THRESHOLD_VALUE')::posreal;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_in_combination_areas_statistics_base
 * ------------------------------------------------
 * Base function to determine statistics for an in-combination calculation for assessment areas/habitats.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_in_combination_areas_statistics_base(v_proposed_calculation_id integer)
	RETURNS TABLE(calculation_result_set_id integer, hexagon_type hexagon_type, receptor_id integer, assessment_area_id integer, critical_deposition_area_id integer, cartographic_surface real, result real, total_result real, percentage_cl real, total_percentage_cl real) AS
$BODY$
	SELECT
		calculation_result_set_id,
		hexagon_type,
		calculation_results.receptor_id,
		assessment_area_id,
		critical_deposition_area_id,
		cartographic_surface,
		calculation_results.result,
		calculation_results.total_result,
		calculation_results.result / NULLIF(critical_level, 0) * 100::real AS percentage_cl,
		calculation_results.total_result / NULLIF(critical_level, 0) * 100::real AS total_percentage_cl

		FROM jobs.ae_scenario_in_combination_statistics_base(v_proposed_calculation_id) AS calculation_results
			INNER JOIN receptors_to_relevant_habitats USING (receptor_id)
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN jobs.ae_receptors_with_hexagon_type(ARRAY[v_proposed_calculation_id]) USING (receptor_id)
			LEFT JOIN critical_deposition_area_critical_levels_view USING (critical_deposition_area_id, result_type, substance_id)
		WHERE hexagon_type != 'extra_assessment_hexagons'
	UNION ALL
	SELECT
		calculation_result_set_id,
		hexagon_type,
		calculation_results.receptor_id,
		assessment_area_id,
		habitat_type_id AS critical_deposition_area_id,
		1 AS cartographic_surface,
		calculation_results.result,
		calculation_results.total_result,
		calculation_results.result / NULLIF(critical_level, 0) * 100::real AS percentage_cl,
		calculation_results.total_result / NULLIF(critical_level, 0) * 100::real AS total_percentage_cl

		FROM jobs.ae_scenario_in_combination_statistics_base(v_proposed_calculation_id) AS calculation_results
			INNER JOIN receptors_to_assessment_areas USING (receptor_id)
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN jobs.ae_receptors_with_hexagon_type(ARRAY[v_proposed_calculation_id]) USING (receptor_id)
			INNER JOIN extra_assessment_receptors USING (receptor_id, assessment_area_id)
			LEFT JOIN critical_deposition_area_critical_levels_view ON 
				(critical_deposition_area_critical_levels_view.critical_deposition_area_id = extra_assessment_receptors.habitat_type_id
					AND critical_deposition_area_critical_levels_view.result_type = calculation_result_sets.result_type
					AND critical_deposition_area_critical_levels_view.substance_id = calculation_result_sets.substance_id)
		WHERE hexagon_type = 'extra_assessment_hexagons'
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_in_combination_sub_statistics_base
 * ----------------------------------------------
 * Base function to determine statistics for an in-combination calculation for sub points.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_in_combination_sub_statistics_base(v_proposed_calculation_id integer)
	RETURNS TABLE(calculation_result_set_id integer, calculation_sub_point_id integer, receptor_id integer, assessment_area_id integer, critical_deposition_area_id integer, result real, total_result real, percentage_cl real, total_percentage_cl real) AS
$BODY$
	SELECT
		calculation_result_set_id,
		calculation_results.calculation_sub_point_id,
		calculation_results.receptor_id,
		assessment_area_id,
		critical_deposition_area_id,
		calculation_results.result,
		calculation_results.total_result,
		calculation_results.result / NULLIF(critical_level, 0) * 100::real AS percentage_cl,
		calculation_results.total_result / NULLIF(critical_level, 0) * 100::real AS total_percentage_cl

	FROM jobs.in_combination_calculation_sub_results AS calculation_results
		INNER JOIN receptors_to_relevant_habitats USING (receptor_id)
		INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
		LEFT JOIN critical_deposition_area_critical_levels_view USING (critical_deposition_area_id, result_type, substance_id)

	WHERE calculation_id = v_proposed_calculation_id
		-- Only do sub statistics for non-depositions
		AND calculation_result_sets.result_type != 'deposition';
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_in_combination_statistics
 * -------------------------------------
 * Function returning the general statistics for an in-combination calculation.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_in_combination_statistics(v_proposed_calculation_id integer, v_hexagon_type hexagon_type)
	RETURNS TABLE(emission_result_type emission_result_type, substance_id smallint, result_statistic_type result_statistic_type, value real) AS
$BODY$
	SELECT
		calculation_result_sets.result_type AS emission_result_type,
		calculation_result_sets.substance_id,
		UNNEST(array ['count_receptors'::result_statistic_type, 'count_receptors_increase'::result_statistic_type, 'count_receptors_decrease'::result_statistic_type, 'max_increase'::result_statistic_type, 'max_decrease'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [count(DISTINCT receptor_id)::real, coalesce(count(DISTINCT receptor_id) filter ( where result > 0 ), 0)::real, coalesce(count(DISTINCT receptor_id) filter ( where result < 0 ), 0)::real, max(result) filter ( where result > 0 ), -min(result) filter ( where result < 0 ), max(total_result)]) AS value

		FROM jobs.ae_scenario_in_combination_statistics_base(v_proposed_calculation_id)
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN receptor_hexagon_type_view USING (receptor_id)

		WHERE hexagon_type = v_hexagon_type

		GROUP BY emission_result_type, substance_id
	UNION ALL
	SELECT
		calculation_result_sets.result_type AS emission_result_type,
		calculation_result_sets.substance_id,
		UNNEST(array ['sum_cartographic_surface'::result_statistic_type, 'sum_cartographic_surface_increase'::result_statistic_type, 'sum_cartographic_surface_decrease'::result_statistic_type, 'max_percentage_critical_level'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [sum(cartographic_surface::numeric)::real, coalesce(sum(cartographic_surface::numeric) filter ( where result > 0 ), 0)::real, coalesce(sum(cartographic_surface::numeric) filter ( where result < 0 ), 0)::real, max(percentage_cl)]) AS value

		FROM jobs.ae_scenario_in_combination_areas_statistics_base(v_proposed_calculation_id)
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)

		WHERE hexagon_type = v_hexagon_type

		GROUP BY emission_result_type, substance_id
	UNION ALL
	SELECT
		calculation_result_sets.result_type AS emission_result_type,
		calculation_result_sets.substance_id,
		UNNEST(array ['count_calculation_points'::result_statistic_type, 'count_calculation_points_increase'::result_statistic_type, 'count_calculation_points_decrease'::result_statistic_type, 'max_increase'::result_statistic_type, 'max_decrease'::result_statistic_type, 'max_total'::result_statistic_type, 'max_percentage_critical_level'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [count(DISTINCT (calculation_sub_point_id, receptor_id))::real, coalesce(count(DISTINCT (calculation_sub_point_id, receptor_id)) filter ( where result > 0 ), 0)::real, coalesce(count(DISTINCT (calculation_sub_point_id, receptor_id)) filter ( where result < 0 ), 0)::real, max(result) filter ( where result > 0 ), -min(result) filter ( where result < 0 ), max(total_result), max(percentage_cl)]) AS value

		FROM jobs.ae_scenario_in_combination_sub_statistics_base(v_proposed_calculation_id)
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN receptor_hexagon_type_view USING (receptor_id)

		WHERE hexagon_type = v_hexagon_type

		GROUP BY emission_result_type, substance_id;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_in_combination_fill_calculation_statistics
 * ------------------------------------------------------
 * Function to determine all statistics for an in-combination calculation and to persist them in the appropriate tables.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_in_combination_fill_calculation_statistics(v_job_id integer, v_proposed_calculation_id integer)
	RETURNS void AS
$BODY$
	-- Assessment area
	INSERT INTO jobs.scenario_calculation_assessment_area_statistics (job_id, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		'in_combination'::scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		UNNEST(array ['count_receptors'::result_statistic_type, 'count_receptors_increase'::result_statistic_type, 'count_receptors_decrease'::result_statistic_type, 'sum_cartographic_surface'::result_statistic_type, 'sum_cartographic_surface_increase'::result_statistic_type, 'sum_cartographic_surface_decrease'::result_statistic_type, 'max_increase'::result_statistic_type, 'max_decrease'::result_statistic_type, 'max_total'::result_statistic_type, 'max_percentage_critical_level'::result_statistic_type, 'max_total_percentage_critical_level'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [count(DISTINCT receptor_id)::real, coalesce(count(DISTINCT receptor_id) filter ( where result > 0 ), 0)::real, coalesce(count(DISTINCT receptor_id) filter ( where result < 0 ), 0)::real, sum(cartographic_surface::numeric)::real, coalesce(sum(cartographic_surface::numeric) filter ( where result > 0 ), 0)::real, coalesce(sum(cartographic_surface::numeric) filter ( where result < 0 ), 0)::real, max(result) filter ( where result > 0 ), -min(result) filter ( where result < 0 ), max(total_result), max(percentage_cl), max(total_percentage_cl)]) AS value

		FROM jobs.ae_scenario_in_combination_areas_statistics_base(v_proposed_calculation_id)
		GROUP BY calculation_result_set_id, assessment_area_id, hexagon_type;


	-- Assessment area and critical deposition area
	INSERT INTO jobs.scenario_calculation_critical_deposition_area_statistics (job_id, scenario_result_type, calculation_result_set_id, assessment_area_id, critical_deposition_area_id, hexagon_type, overlapping_hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		'in_combination'::scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		critical_deposition_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		UNNEST(array ['count_receptors'::result_statistic_type, 'count_receptors_increase'::result_statistic_type, 'count_receptors_decrease'::result_statistic_type, 'sum_cartographic_surface'::result_statistic_type, 'sum_cartographic_surface_increase'::result_statistic_type, 'sum_cartographic_surface_decrease'::result_statistic_type, 'max_increase'::result_statistic_type, 'max_decrease'::result_statistic_type, 'max_background'::result_statistic_type, 'max_total'::result_statistic_type, 'max_percentage_critical_level'::result_statistic_type, 'max_total_percentage_critical_level'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [count(DISTINCT receptor_id)::real, coalesce(count(DISTINCT receptor_id) filter ( where result > 0 ), 0)::real, coalesce(count(DISTINCT receptor_id) filter ( where result < 0 ), 0)::real, sum(cartographic_surface::numeric)::real, coalesce(sum(cartographic_surface::numeric) filter ( where result > 0 ), 0)::real, coalesce(sum(cartographic_surface::numeric) filter ( where result < 0 ), 0)::real, max(result) filter ( where result > 0 ), -min(result) filter ( where result < 0 ), max(total_result - result), max(total_result), max(percentage_cl), max(total_percentage_cl)]) AS value

		FROM jobs.ae_scenario_in_combination_areas_statistics_base(v_proposed_calculation_id)
		GROUP BY calculation_result_set_id, assessment_area_id, critical_deposition_area_id, hexagon_type;


	-- Markers
	INSERT INTO jobs.scenario_calculation_assessment_area_statistic_markers (job_id, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, result_statistic_type, receptor_id)
	SELECT DISTINCT ON (calculation_result_set_id, assessment_area_id, hexagon_type, result_statistic_type)
		v_job_id,
		'in_combination'::scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		UNNEST(array ['max_increase'::result_statistic_type, 'max_decrease'::result_statistic_type, 'max_total'::result_statistic_type, 'max_percentage_critical_level'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [
			first_value(CASE WHEN result > 0 THEN receptor_id END) OVER (PARTITION BY calculation_result_set_id, hexagon_type, assessment_area_id ORDER BY result DESC, receptor_id),
			first_value(CASE WHEN result < 0 THEN receptor_id END) OVER (PARTITION BY calculation_result_set_id, hexagon_type, assessment_area_id ORDER BY result, receptor_id),
			first_value(receptor_id) OVER (PARTITION BY calculation_result_set_id, hexagon_type, assessment_area_id ORDER BY total_result DESC, receptor_id),
			first_value(CASE WHEN percentage_cl IS NOT NULL THEN receptor_id END) OVER (PARTITION BY calculation_result_set_id, hexagon_type, assessment_area_id ORDER BY percentage_cl DESC, receptor_id)
			]) AS receptor_id

		FROM jobs.ae_scenario_in_combination_areas_statistics_base(v_proposed_calculation_id);


	-- Charts: emission results
	INSERT INTO jobs.scenario_calculation_assessment_area_chart_statistics (job_id, emission_result_chart_type, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, color_range_type, lower_bound, cartographic_surface)
	SELECT
		v_job_id,
		'emission_result'::emission_result_chart_type,
		scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		color_range_type,
		(UNNEST(ae_color_range(color_range_type, result::numeric, cartographic_surface::numeric))).lower_value::real AS lower_bound,
		(UNNEST(ae_color_range(color_range_type, result::numeric, cartographic_surface::numeric))).total::real AS cartographic_surface

		FROM jobs.ae_scenario_in_combination_areas_statistics_base(v_proposed_calculation_id)
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN scenario_result_types_color_ranges ON (calculation_result_sets.result_type = scenario_result_types_color_ranges.emission_result_type AND calculation_result_sets.substance_id = scenario_result_types_color_ranges.substance_id)

		WHERE scenario_result_type = 'in_combination'::scenario_result_type
			AND hexagon_type != 'extra_assessment_hexagons'

		GROUP BY scenario_result_type, calculation_result_set_id, assessment_area_id, color_range_type, hexagon_type;

	-- Special case for extra_assessment_hexagons, where we want to display number of unique receptors
	WITH area_results AS (
	SELECT DISTINCT
		calculation_result_set_id,
		hexagon_type,
		receptor_id,
		assessment_area_id,
		result

		FROM jobs.ae_scenario_in_combination_areas_statistics_base(v_proposed_calculation_id)
		WHERE hexagon_type = 'extra_assessment_hexagons'
	)
	INSERT INTO jobs.scenario_calculation_assessment_area_chart_statistics (job_id, emission_result_chart_type, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, color_range_type, lower_bound, cartographic_surface)
	SELECT
		v_job_id,
		'emission_result'::emission_result_chart_type,
		scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		color_range_type,
		(UNNEST(ae_color_range(color_range_type, result::numeric, 1::numeric))).lower_value::real AS lower_bound,
		(UNNEST(ae_color_range(color_range_type, result::numeric, 1::numeric))).total::real AS cartographic_surface

		FROM area_results
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN scenario_result_types_color_ranges ON (calculation_result_sets.result_type = scenario_result_types_color_ranges.emission_result_type AND calculation_result_sets.substance_id = scenario_result_types_color_ranges.substance_id)

		WHERE scenario_result_type = 'in_combination'::scenario_result_type

		GROUP BY scenario_result_type, calculation_result_set_id, assessment_area_id, color_range_type, hexagon_type;


	-- Charts: percentage CL
	INSERT INTO jobs.scenario_calculation_assessment_area_chart_statistics (job_id, emission_result_chart_type, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, color_range_type, lower_bound, cartographic_surface)
	SELECT
		v_job_id,
		'percentage_cl'::emission_result_chart_type,
		'in_combination'::scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		'in_combination_percentage_critical_load'::color_range_type,
		(UNNEST(ae_color_range('in_combination_percentage_critical_load'::color_range_type, percentage_cl::numeric, cartographic_surface::numeric))).lower_value::real AS lower_bound,
		(UNNEST(ae_color_range('in_combination_percentage_critical_load'::color_range_type, percentage_cl::numeric, cartographic_surface::numeric))).total::real AS cartographic_surface

	FROM jobs.ae_scenario_in_combination_areas_statistics_base(v_proposed_calculation_id)
		INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)

	WHERE percentage_cl IS NOT NULL
		AND hexagon_type != 'extra_assessment_hexagons'

	GROUP BY scenario_result_type, calculation_result_set_id, assessment_area_id, color_range_type, hexagon_type;
	
	-- Special case for extra_assessment_hexagons, where we want to display number of unique receptors
	WITH area_results AS (
	SELECT DISTINCT
		calculation_result_set_id,
		hexagon_type,
		receptor_id,
		assessment_area_id,
		percentage_cl

		FROM jobs.ae_scenario_in_combination_areas_statistics_base(v_proposed_calculation_id)
		WHERE hexagon_type = 'extra_assessment_hexagons'
	)
	INSERT INTO jobs.scenario_calculation_assessment_area_chart_statistics (job_id, emission_result_chart_type, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, color_range_type, lower_bound, cartographic_surface)
	SELECT
		v_job_id,
		'percentage_cl'::emission_result_chart_type,
		'in_combination'::scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		'in_combination_percentage_critical_load'::color_range_type,
		(UNNEST(ae_color_range('in_combination_percentage_critical_load'::color_range_type, percentage_cl::numeric, 1::numeric))).lower_value::real AS lower_bound,
		(UNNEST(ae_color_range('in_combination_percentage_critical_load'::color_range_type, percentage_cl::numeric, 1::numeric))).total::real AS cartographic_surface

	FROM area_results
		INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)

	WHERE percentage_cl IS NOT NULL

	GROUP BY scenario_result_type, calculation_result_set_id, assessment_area_id, color_range_type, hexagon_type;


	-- Sub points
	-- Assessment area
	INSERT INTO jobs.scenario_calculation_assessment_area_statistics (job_id, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		'in_combination'::scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		UNNEST(array ['count_calculation_points'::result_statistic_type, 'count_calculation_points_increase'::result_statistic_type, 'count_calculation_points_decrease'::result_statistic_type, 'max_increase'::result_statistic_type, 'max_decrease'::result_statistic_type, 'max_total'::result_statistic_type, 'max_percentage_critical_level'::result_statistic_type, 'max_total_percentage_critical_level'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [count(DISTINCT (calculation_sub_point_id, receptor_id))::real, coalesce(count(DISTINCT (calculation_sub_point_id, receptor_id)) filter ( where result > 0 ), 0)::real, coalesce(count(DISTINCT (calculation_sub_point_id, receptor_id)) filter ( where result < 0 ), 0)::real, max(result) filter ( where result > 0 ), -min(result) filter ( where result < 0 ), max(total_result), max(percentage_cl), max(total_percentage_cl)]) AS value

	FROM jobs.ae_scenario_in_combination_sub_statistics_base(v_proposed_calculation_id)
		INNER JOIN jobs.ae_receptors_with_hexagon_type(ARRAY[v_proposed_calculation_id]) USING (receptor_id)
	GROUP BY calculation_result_set_id, assessment_area_id, hexagon_type;

	-- Assessment area and critical deposition area
	INSERT INTO jobs.scenario_calculation_critical_deposition_area_statistics (job_id, scenario_result_type, calculation_result_set_id, assessment_area_id, critical_deposition_area_id, hexagon_type, overlapping_hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		'in_combination'::scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		critical_deposition_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		UNNEST(array ['count_calculation_points'::result_statistic_type, 'count_calculation_points_increase'::result_statistic_type, 'count_calculation_points_decrease'::result_statistic_type, 'max_increase'::result_statistic_type, 'max_decrease'::result_statistic_type, 'max_background'::result_statistic_type, 'max_total'::result_statistic_type, 'max_percentage_critical_level'::result_statistic_type, 'max_total_percentage_critical_level'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [count(DISTINCT (calculation_sub_point_id, receptor_id))::real, coalesce(count(DISTINCT (calculation_sub_point_id, receptor_id)) filter ( where result > 0 ), 0)::real, coalesce(count(DISTINCT (calculation_sub_point_id, receptor_id)) filter ( where result < 0 ), 0)::real, max(result) filter ( where result > 0 ), -min(result) filter ( where result < 0 ), max(total_result - result), max(total_result), max(percentage_cl), max(total_percentage_cl)]) AS value

	FROM jobs.ae_scenario_in_combination_sub_statistics_base(v_proposed_calculation_id)
		INNER JOIN jobs.ae_receptors_with_hexagon_type(ARRAY[v_proposed_calculation_id]) USING (receptor_id)
	GROUP BY calculation_result_set_id, assessment_area_id, critical_deposition_area_id, hexagon_type;

	-- Markers
	INSERT INTO jobs.scenario_calculation_assessment_area_statistic_markers (job_id, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, result_statistic_type, receptor_id)
	SELECT DISTINCT ON (calculation_result_set_id, assessment_area_id, hexagon_type, result_statistic_type)
		v_job_id,
		'in_combination'::scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		UNNEST(array ['max_increase'::result_statistic_type, 'max_decrease'::result_statistic_type, 'max_total'::result_statistic_type, 'max_percentage_critical_level'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [
			first_value(CASE WHEN result > 0 THEN receptor_id END) OVER (PARTITION BY calculation_result_set_id, hexagon_type, assessment_area_id ORDER BY result DESC, receptor_id),
			first_value(CASE WHEN result < 0 THEN receptor_id END) OVER (PARTITION BY calculation_result_set_id, hexagon_type, assessment_area_id ORDER BY result, receptor_id),
			first_value(receptor_id) OVER (PARTITION BY calculation_result_set_id, hexagon_type, assessment_area_id ORDER BY total_result DESC, receptor_id),
			first_value(CASE WHEN percentage_cl IS NOT NULL THEN receptor_id END) OVER (PARTITION BY calculation_result_set_id, hexagon_type, assessment_area_id ORDER BY percentage_cl DESC, receptor_id)
			]) AS receptor_id

		FROM jobs.ae_scenario_in_combination_sub_statistics_base(v_proposed_calculation_id)
			INNER JOIN jobs.ae_receptors_with_hexagon_type(ARRAY[v_proposed_calculation_id]) USING (receptor_id);

	-- Charts: emission results
	WITH sub_point_results AS (
		SELECT DISTINCT
			calculation_result_set_id,
			calculation_sub_point_id,
			receptor_id,
			assessment_area_id,
			result

			FROM jobs.ae_scenario_in_combination_sub_statistics_base(v_proposed_calculation_id)
	)
	INSERT INTO jobs.scenario_calculation_assessment_area_chart_statistics (job_id, emission_result_chart_type, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, color_range_type, lower_bound, cartographic_surface)
	SELECT
		v_job_id,
		'emission_result'::emission_result_chart_type,
		scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		color_range_type,
		(UNNEST(ae_color_range(color_range_type, result::numeric, 1))).lower_value::real AS lower_bound,
		(UNNEST(ae_color_range(color_range_type, result::numeric, 1))).total::real AS cartographic_surface

	FROM sub_point_results
		INNER JOIN jobs.ae_receptors_with_hexagon_type(ARRAY[v_proposed_calculation_id]) USING (receptor_id)
		INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
		INNER JOIN scenario_result_types_color_ranges ON (calculation_result_sets.result_type = scenario_result_types_color_ranges.emission_result_type AND calculation_result_sets.substance_id = scenario_result_types_color_ranges.substance_id)

	WHERE scenario_result_type = 'in_combination'::scenario_result_type

	GROUP BY scenario_result_type, calculation_result_set_id, assessment_area_id, color_range_type, hexagon_type;

	-- Charts: percentage CL
	WITH sub_point_results AS (
		SELECT
			calculation_result_set_id,
			calculation_sub_point_id,
			receptor_id,
			assessment_area_id,
			max(percentage_cl) AS percentage_cl

			FROM jobs.ae_scenario_in_combination_sub_statistics_base(v_proposed_calculation_id)

			GROUP BY calculation_result_set_id, calculation_sub_point_id, receptor_id, assessment_area_id
	)
	INSERT INTO jobs.scenario_calculation_assessment_area_chart_statistics (job_id, emission_result_chart_type, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, color_range_type, lower_bound, cartographic_surface)
	SELECT
		v_job_id,
		'percentage_cl'::emission_result_chart_type,
		'in_combination'::scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		'in_combination_percentage_critical_load'::color_range_type,
		(UNNEST(ae_color_range('in_combination_percentage_critical_load'::color_range_type, percentage_cl::numeric, 1))).lower_value::real AS lower_bound,
		(UNNEST(ae_color_range('in_combination_percentage_critical_load'::color_range_type, percentage_cl::numeric, 1))).total::real AS cartographic_surface

	FROM sub_point_results
		INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
		INNER JOIN jobs.ae_receptors_with_hexagon_type(ARRAY[v_proposed_calculation_id]) USING (receptor_id)

	WHERE percentage_cl IS NOT NULL

	GROUP BY scenario_result_type, calculation_result_set_id, assessment_area_id, color_range_type, hexagon_type;
$BODY$
LANGUAGE SQL VOLATILE;


/*
 * ae_scenario_calculation_point_in_combination_results
 * ----------------------------------------------------
 * Function returning results for an in-combination calculation for custom points, based on job ID and proposed calculation ID.
 *
 * For an in-combination calculation, the results of the proposed situation are used as a basis.
 * When present, the results of the reference situation and/or off site reduction situation are subtracted.
 * Additionally, the results of all in-combination-proposed situations are added, and the results of all in-combination-reference situations is subtracted.
 *
 * The point set for the proposed situation is leading.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_calculation_point_in_combination_results(v_job_id integer, v_proposed_calculation_id integer)
	RETURNS TABLE(emission_result_type emission_result_type, substance_id smallint, calculation_point_id integer, geometry geometry, result real) AS
$BODY$
	WITH correct_job AS (
		SELECT * FROM jobs.jobs WHERE job_id = v_job_id
	), job_calcs AS (
		SELECT
			jobs.ae_scenario_get_calculation_id_of_situation_type(key, 'reference') AS reference_calc_id,
			jobs.ae_scenario_get_calculation_id_of_situation_type(key, 'off_site_reduction') AS off_site_reduction_calculation_id,
			jobs.ae_scenario_get_calculation_ids_of_situation_type(key, 'combination_proposed') AS combination_proposed_calc_ids,
			jobs.ae_scenario_get_calculation_ids_of_situation_type(key, 'combination_reference') AS combination_reference_calc_ids

			FROM correct_job
	)
	SELECT * FROM jobs.ae_scenario_calculation_point_in_combination_results(v_proposed_calculation_id,
				(SELECT reference_calc_id FROM job_calcs),
				(SELECT off_site_reduction_calculation_id FROM job_calcs),
				(SELECT combination_proposed_calc_ids FROM job_calcs),
				(SELECT combination_reference_calc_ids FROM job_calcs));
$BODY$
LANGUAGE SQL STABLE;
