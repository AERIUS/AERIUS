/*
 * ae_analyze_calculation_related
 * ------------------------------
 * Function that can be used to analyze the tables that are filled during a calculation.
 *
 * This analysis is required to ensure the query planner has realistic values, mostly during the first calculation(s).
 *
 * The function uses 2 parts to determine when to analyse:
 * 1. Number of calculations.
 *   When this is below a certain threshold, the analysis will be executed.
 * 2. The system.constants value of 'NEXT_ANALYSE_MOMENT'.
 *   When this is absent, or if the moment is before the moment of calling this function, the value will be adjusted and the analysis will be executed.
 * This is introduced to prevent the case where an analysis that takes a long time will block parallel calculations that also want to execute analysis.
 */
CREATE OR REPLACE FUNCTION ae_analyze_calculation_related()
	RETURNS void AS
$BODY$
DECLARE
	analyzables regclass[] := ARRAY['jobs.calculations'::regclass, 'jobs.calculation_result_sets'::regclass, 'jobs.calculation_results'::regclass,
		'jobs.calculation_overlapping_receptors'::regclass,
		'jobs.calculation_point_sets'::regclass, 'jobs.calculation_points'::regclass, 'jobs.calculation_point_results'::regclass,
		'jobs.calculation_sub_point_sets'::regclass, 'jobs.calculation_sub_points'::regclass, 'jobs.calculation_points'::regclass,
		'jobs.project_calculation_calculation_results'::regclass, 'jobs.project_calculation_calculation_sub_results'::regclass, 'jobs.project_calculation_receptors'::regclass,
		'jobs.in_combination_calculation_results'::regclass, 'jobs.in_combination_calculation_sub_results'::regclass,
		'jobs.archive_contribution_results'::regclass, 'jobs.archive_contribution_sub_results'::regclass,
		'jobs.temporary_effect_calculation_results'::regclass, 'jobs.temporary_effect_calculation_sub_results'::regclass, 'jobs.temporary_effect_receptors'::regclass,
		'jobs.scenario_assessment_area_statistics'::regclass, 'jobs.scenario_assessment_area_chart_statistics'::regclass,
		'jobs.scenario_assessment_area_statistic_markers'::regclass, 'jobs.scenario_critical_deposition_area_statistics'::regclass,
		'jobs.scenario_calculation_assessment_area_statistics'::regclass, 'jobs.scenario_calculation_assessment_area_chart_statistics'::regclass,
		'jobs.scenario_calculation_assessment_area_statistic_markers'::regclass, 'jobs.scenario_calculation_critical_deposition_area_statistics'::regclass,
		'jobs.jobs'::regclass, 'jobs.job_calculations'::regclass];
	analyze_table regclass;
	next_analyse_moment timestamp;
	nr_calculations integer;
BEGIN
	next_analyse_moment := value::timestamp FROM system.constants WHERE key = 'NEXT_ANALYSE_MOMENT';
	nr_calculations := COUNT(*) FROM jobs.calculations;
	IF nr_calculations < 50 OR next_analyse_moment IS NULL OR next_analyse_moment < now() THEN
		INSERT INTO system.constants (key, value)
			VALUES('NEXT_ANALYSE_MOMENT', now() + '1 day')
			ON CONFLICT (key)
			DO UPDATE SET value = now() + '1 day';
		FOREACH analyze_table IN ARRAY analyzables
		LOOP
			EXECUTE 'ANALYZE ' || analyze_table;
		END LOOP;
	END IF;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;
