/*
 * scenario_result_types_color_ranges
 * ----------------------------------
 * Table containing the color range type to use for different scenario result types and emission result type/substance combinations.
 */
CREATE TABLE scenario_result_types_color_ranges (
  scenario_result_type scenario_result_type NOT NULL,
	emission_result_type emission_result_type NOT NULL,
	substance_id smallint NOT NULL,
	color_range_type color_range_type NOT NULL,

	CONSTRAINT result_types_color_ranges_pkey PRIMARY KEY (scenario_result_type, emission_result_type, substance_id),
	CONSTRAINT result_types_color_ranges_fkey_substances FOREIGN KEY (substance_id) REFERENCES substances
);
