/*
 * job_scenario_results_view
 * -------------------------
 * View returning scenario results (that is, combined results of different situations/calculations within a job).
 *
 * Use at least 'job_id' or 'key' in the where clause.
 */
CREATE OR REPLACE VIEW jobs.job_scenario_results_view AS
SELECT
	job_id,
	key,
	'project_calculation' AS scenario_result_type,
	receptor_id,
	result_type,
	substance_id,
	result,
	total_result

	FROM jobs.jobs
		INNER JOIN jobs.job_calculations USING (job_id)
		INNER JOIN jobs.calculation_result_sets USING (calculation_id)
		INNER JOIN jobs.project_calculation_calculation_results USING (calculation_result_set_id)
UNION ALL
SELECT
	job_id,
	key,
	'max_temporary_effect' AS scenario_result_type,
	receptor_id,
	emission_result_type AS result_type,
	substance_id,
	result,
	total_result

	FROM jobs.jobs
		INNER JOIN jobs.temporary_effect_calculation_results USING (job_id)
UNION ALL
SELECT
	job_id,
	key,
	'in_combination' AS scenario_result_type,
	receptor_id,
	result_type,
	substance_id,
	result,
	total_result

	FROM jobs.jobs
		INNER JOIN jobs.job_calculations USING (job_id)
		INNER JOIN jobs.calculation_result_sets USING (calculation_id)
		INNER JOIN jobs.in_combination_calculation_results USING (calculation_result_set_id)
UNION ALL
SELECT
	job_id,
	key,
	'archive_contribution' AS scenario_result_type,
	receptor_id,
	result_type,
	substance_id,
	result,
	total_result

	FROM jobs.jobs
		INNER JOIN jobs.job_calculations USING (job_id)
		INNER JOIN jobs.calculation_result_sets USING (calculation_id)
		INNER JOIN jobs.archive_contribution_results USING (calculation_result_set_id)
;
