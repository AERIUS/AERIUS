-- For results for individual calculations, the calculation_results_view can be used
-- For results on combinations of calculations, this view can also be used,
-- however for performance reasons it can be better to persist these results per receptor.
-- Tables in this file contain that preprocessed data.

/*
 * project_calculation_calculation_results
 * ---------------------------------------
 * Table containing results for a project contribution calculation per receptor.
 */
CREATE TABLE jobs.project_calculation_calculation_results (
	calculation_result_set_id integer NOT NULL,
	receptor_id integer NOT NULL,
	result real NOT NULL,
	total_result real NOT NULL,

	CONSTRAINT project_calculation_calculation_results_pkey PRIMARY KEY (calculation_result_set_id, receptor_id),
	CONSTRAINT project_calculation_calculation_results_fkey_calculation_result_sets FOREIGN KEY (calculation_result_set_id) REFERENCES jobs.calculation_result_sets ON DELETE CASCADE
);


/*
 * project_calculation_calculation_sub_results
 * -------------------------------------------
 * Table containing results for a project contribution calculation per sub point.
 */
CREATE TABLE jobs.project_calculation_calculation_sub_results (
	calculation_result_set_id integer NOT NULL,
	calculation_sub_point_id integer NOT NULL,
	receptor_id integer NOT NULL,
	result real NOT NULL,
	total_result real NOT NULL,

	CONSTRAINT project_calculation_calculation_sub_results_pkey PRIMARY KEY (calculation_result_set_id, calculation_sub_point_id, receptor_id),
	CONSTRAINT project_calculation_calculation_sub_results_fkey_calculation_result_sets FOREIGN KEY (calculation_result_set_id) REFERENCES jobs.calculation_result_sets ON DELETE CASCADE
);


/*
 * project_calculation_receptors
 * -----------------------------
 * Table containing the receptors for a project contribution calculation per hexagon set.
 */
CREATE TABLE jobs.project_calculation_receptors (
	calculation_id integer NOT NULL,
	receptor_id integer NOT NULL,
	hexagon_type hexagon_type NOT NULL,
	overlapping_hexagon_type overlapping_hexagon_type NOT NULL,

	CONSTRAINT project_calculation_receptors_pkey PRIMARY KEY (calculation_id, receptor_id, hexagon_type, overlapping_hexagon_type),
	CONSTRAINT project_calculation_receptors_fkey_calculations FOREIGN KEY (calculation_id) REFERENCES jobs.calculations ON DELETE CASCADE
);


/*
 * temporary_effect_calculation_results
 * ------------------------------------
 * Table containing results of a temporary effect calculation per receptor.
 */
CREATE TABLE jobs.temporary_effect_calculation_results (
	job_id integer NOT NULL,
	emission_result_type emission_result_type NOT NULL,
	substance_id smallint NOT NULL,
	calculation_id integer NOT NULL,
	receptor_id integer NOT NULL,
	result real NOT NULL,
	total_result real NOT NULL,

	CONSTRAINT temporary_effect_calculation_results_pkey PRIMARY KEY (job_id, emission_result_type, substance_id, calculation_id, receptor_id),
	CONSTRAINT temporary_effect_calculation_results_fkey_jobs FOREIGN KEY (job_id) REFERENCES jobs.jobs ON DELETE CASCADE,
	CONSTRAINT temporary_effect_calculation_results_fkey_calculations FOREIGN KEY (calculation_id) REFERENCES jobs.calculations ON DELETE CASCADE
);


/*
 * temporary_effect_calculation_sub_results
 * ----------------------------------------
 * Table containing results of a temporary effect calculation per sub point.
 */
CREATE TABLE jobs.temporary_effect_calculation_sub_results (
	job_id integer NOT NULL,
	emission_result_type emission_result_type NOT NULL,
	substance_id smallint NOT NULL,
	calculation_id integer NOT NULL,
	calculation_sub_point_id integer NOT NULL,
	receptor_id integer NOT NULL,
	result real NOT NULL,
	total_result real NOT NULL,

	CONSTRAINT temporary_effect_calculation_sub_results_pkey PRIMARY KEY (job_id, emission_result_type, substance_id, calculation_id, calculation_sub_point_id, receptor_id),
	CONSTRAINT temporary_effect_calculation_sub_results_fkey_jobs FOREIGN KEY (job_id) REFERENCES jobs.jobs ON DELETE CASCADE,
	CONSTRAINT temporary_effect_calculation_sub_results_fkey_calculations FOREIGN KEY (calculation_id) REFERENCES jobs.calculations ON DELETE CASCADE
);


/*
 * temporary_effect_receptors
 * --------------------------
 * Table containing the receptors for a temporary effect calculation per hexagon set.
 */
CREATE TABLE jobs.temporary_effect_receptors (
	job_id integer NOT NULL,
	receptor_id integer NOT NULL,
	hexagon_type hexagon_type NOT NULL,
	overlapping_hexagon_type overlapping_hexagon_type NOT NULL,

	CONSTRAINT temporary_effect_receptors_pkey PRIMARY KEY (job_id, receptor_id, hexagon_type, overlapping_hexagon_type),
	CONSTRAINT temporary_effect_receptors_fkey_jobs FOREIGN KEY (job_id) REFERENCES jobs.jobs ON DELETE CASCADE
);


/*
 * in_combination_calculation_results
 * ----------------------------------
 * Table containing results of a in-combination calculation per receptor.
 */
CREATE TABLE jobs.in_combination_calculation_results (
	calculation_result_set_id integer NOT NULL,
	receptor_id integer NOT NULL,
	result real NOT NULL,
	total_result real NOT NULL,

	CONSTRAINT in_combination_calculation_results_pkey PRIMARY KEY (calculation_result_set_id, receptor_id),
	CONSTRAINT in_combination_calculation_results_fkey_calculation_result_sets FOREIGN KEY (calculation_result_set_id) REFERENCES jobs.calculation_result_sets ON DELETE CASCADE
);


/*
 * in_combination_calculation_sub_results
 * --------------------------------------
 * Table containing results of a in-combination calculation per sub point.
 */
CREATE TABLE jobs.in_combination_calculation_sub_results (
	calculation_result_set_id integer NOT NULL,
	calculation_sub_point_id integer NOT NULL,
	receptor_id integer NOT NULL,
	result real NOT NULL,
	total_result real NOT NULL,

	CONSTRAINT in_combination_calculation_sub_results_pkey PRIMARY KEY (calculation_result_set_id, calculation_sub_point_id, receptor_id),
	CONSTRAINT in_combination_calculation_sub_results_fkey_calculation_result_sets FOREIGN KEY (calculation_result_set_id) REFERENCES jobs.calculation_result_sets ON DELETE CASCADE
);
