/*
 * procurement_thresholds
 * ----------------------
 * Table containing the procurement (aankoopregeling) thresholds.
 * Depending on the type of threshold a key can be used.
 * For example in the case of a threshold per assessment area this can contain the assessment_area_id.
 */
CREATE TABLE procurement_thresholds (
	procurement_policy procurement_policy_type NOT NULL,
	threshold_type procurement_threshold_type NOT NULL,
	key integer NOT NULL,
	threshold real NOT NULL,

	CONSTRAINT procurement_policy_thresholds_pkey PRIMARY KEY (procurement_policy, threshold_type, key)
);
