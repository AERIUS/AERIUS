/*
 * overlapping_hexagon_type
 * ------------------------
 * Enum specifying the overlapping hexagon types, used for statistics of a calculation.
 */
CREATE TYPE overlapping_hexagon_type AS ENUM
	('overlapping_hexagons_only', 'non_overlapping_hexagons_only', 'all_hexagons');

/*
 * result_statistic_type
 * ---------------------
 * Enum specifying the statistic types, used for statistics of a calculation.
 */
CREATE TYPE result_statistic_type AS ENUM
	('sum_cartographic_surface', 'count_calculation_points', 'max_background', 'max_contribution', 'max_total', 'sum_cartographic_surface_increase', 'sum_cartographic_surface_decrease', 'count_calculation_points_increase', 'count_calculation_points_decrease', 'max_increase', 'max_decrease', 'max_temp_increase', 'max_percentage_critical_level', 'max_total_percentage_critical_level', 'development_space_demand_check', 'sum_contribution', 'count_receptors', 'count_receptors_increase', 'count_receptors_decrease');

/*
 * scenario_result_type
 * --------------------
 * Enum specifying the scenario result types.
 */
CREATE TYPE scenario_result_type AS ENUM
	('situation_result', 'project_calculation', 'max_temporary_effect', 'in_combination', 'archive_contribution');

/*
 * emission_result_chart_type
 * --------------------------
 * Enum specifying the result chart types.
 */
CREATE TYPE emission_result_chart_type AS ENUM
	('emission_result', 'percentage_cl');

/*
 * procurement_policy_type
 * -----------------------
 * Enum specifying the procurement policies supported.
 */
CREATE TYPE procurement_policy_type AS ENUM
	('wnb_lbv', 'wnb_lbv_plus');

/*
 * procurement_threshold_type
 * --------------------------
 * Enum specifying the procurement threshold types.
 */
CREATE TYPE procurement_threshold_type AS ENUM
	('sum_deposition_national', 'sum_deposition_assessment_area');
