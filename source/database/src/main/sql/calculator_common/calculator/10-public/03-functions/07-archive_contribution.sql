/*
 * ae_insert_archive_contribution_receptor
 * ---------------------------------------
 * Function to the archive contribution for a receptor for a calculation result set.
 * The result is also combined with the background value to fill the total_result column in the jobs.archive_contribution_results table.
 */
CREATE OR REPLACE FUNCTION jobs.ae_insert_archive_contribution_receptor(v_calculation_result_set_id integer, v_receptor_id integer, v_result double precision)
	RETURNS void AS
$BODY$
	WITH new_result AS (
	SELECT 
		v_calculation_result_set_id AS calculation_result_set_id,
		v_receptor_id AS receptor_id,
		v_result AS result
		WHERE NOT EXISTS (SELECT calculation_result_set_id FROM jobs.archive_contribution_results
			WHERE calculation_result_set_id = v_calculation_result_set_id AND receptor_id = v_receptor_id LIMIT 1)
	)
	INSERT INTO jobs.archive_contribution_results (calculation_result_set_id, receptor_id, result, total_result)
	SELECT 
		new_result.calculation_result_set_id,
		new_result.receptor_id,
		new_result.result,
		new_result.result + COALESCE(background.result, 0) AS total_result
	
		FROM new_result
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN jobs.calculations USING (calculation_id)
			LEFT JOIN receptor_background_results_view AS background ON (
					background.receptor_id = new_result.receptor_id
						AND background.emission_result_type = calculation_result_sets.result_type
						AND background.substance_id = calculation_result_sets.substance_id
						AND background.year = calculations.year);
$BODY$
LANGUAGE SQL VOLATILE;


/*
 * ae_insert_archive_contribution_sub_point
 * ----------------------------------------
 * Function to the archive contribution for a receptor for a calculation result set.
 * The result is also combined with the background value to fill the total_result column in the jobs.archive_contribution_results table.
 */
CREATE OR REPLACE FUNCTION jobs.ae_insert_archive_contribution_sub_point(v_calculation_result_set_id integer, v_calculation_sub_point_id integer, v_receptor_id integer, v_result double precision)
	RETURNS void AS
$BODY$
	WITH new_result AS (
	SELECT 
		v_calculation_result_set_id AS calculation_result_set_id,
		v_calculation_sub_point_id AS calculation_sub_point_id,
		v_receptor_id AS receptor_id,
		v_result AS result
		WHERE NOT EXISTS (SELECT calculation_result_set_id FROM jobs.archive_contribution_sub_results
			WHERE calculation_result_set_id = v_calculation_result_set_id AND calculation_sub_point_id = v_calculation_sub_point_id AND receptor_id = v_receptor_id LIMIT 1)
	)
	INSERT INTO jobs.archive_contribution_sub_results (calculation_result_set_id, calculation_sub_point_id, receptor_id, result, total_result)
	SELECT 
		new_result.calculation_result_set_id,
		new_result.calculation_sub_point_id,
		new_result.receptor_id,
		new_result.result,
		new_result.result + COALESCE(background.result, 0) AS total_result
	
		FROM new_result
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN jobs.calculations USING (calculation_id)
			LEFT JOIN receptor_background_results_view AS background ON (
					background.receptor_id = new_result.receptor_id
						AND background.emission_result_type = calculation_result_sets.result_type
						AND background.substance_id = calculation_result_sets.substance_id
						AND background.year = calculations.year);
$BODY$
LANGUAGE SQL VOLATILE;


/*
 * ae_scenario_job_archive_contribution_deposition
 * -----------------------------------------------
 * Function to determine results for archive contribution based on the supplied job key and proposed situation reference.
 * Only returns the total nitrogen deposition results, NOx and NH3 combined, filtered by deposition threshold value.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_job_archive_contribution_deposition(v_job_key text, v_proposed_situation_reference text, v_hexagon_type hexagon_type)
	RETURNS TABLE(receptor_id integer, deposition real) AS
$BODY$
	SELECT
		receptor_id,
		result AS deposition

		FROM jobs.archive_contribution_results
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN receptor_hexagon_type_view USING (receptor_id)

		WHERE result_type = 'deposition'
			AND substance_id = 1711
			AND calculation_id = (SELECT jobs.ae_scenario_get_calculation_id_of_situation_reference(v_job_key, v_proposed_situation_reference))
			AND hexagon_type = v_hexagon_type
			AND ABS(result) > ae_constant('PRONOUNCEMENT_THRESHOLD_VALUE')::posreal;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_job_archive_contribution_results
 * --------------------------------------------
 * Function to determine results for archive contribution, based on supplied job key and proposed situation reference.
 * Returns all emission results, and does not filter on things like nitrogen deposition threshold.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_job_archive_contribution_results(v_job_key text, v_proposed_situation_reference text, v_hexagon_type hexagon_type)
	RETURNS TABLE(receptor_id integer, emission_result_type emission_result_type, substance_id smallint, result real) AS
$BODY$
	SELECT
		receptor_id,
		result_type AS emission_result_type,
		substance_id,
		result

		FROM jobs.archive_contribution_results
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN receptor_hexagon_type_view USING (receptor_id)

		WHERE calculation_id = (SELECT jobs.ae_scenario_get_calculation_id_of_situation_reference(v_job_key, v_proposed_situation_reference))
			AND hexagon_type = v_hexagon_type;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_job_archive_contribution_sub_results
 * ------------------------------------------------
 * Function to determine results for archive contribution, based on supplied job key and proposed situation reference, for sub points.
 * Returns all emission results for sub points, and does not filter on things like nitrogen deposition threshold.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_job_archive_contribution_sub_results(v_job_key text, v_proposed_situation_reference text, v_hexagon_type hexagon_type)
	RETURNS TABLE(calculation_sub_point_id integer, receptor_id integer, emission_result_type emission_result_type, substance_id smallint, result real, geometry geometry(Point)) AS
$BODY$
	SELECT
		calculation_sub_point_id,
		receptor_id,
		result_type AS emission_result_type,
		substance_id,
		result,
		geometry

		FROM jobs.archive_contribution_sub_results
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN jobs.calculations USING (calculation_id)
			INNER JOIN jobs.calculation_sub_points USING (calculation_sub_point_set_id, calculation_sub_point_id, receptor_id)
			INNER JOIN receptor_hexagon_type_view USING (receptor_id)

		WHERE calculation_id = (SELECT jobs.ae_scenario_get_calculation_id_of_situation_reference(v_job_key, v_proposed_situation_reference))
			AND hexagon_type = v_hexagon_type;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_job_archive_contribution_percentage_cl
 * --------------------------------------------------
 * Function to determine the percentages of critical levels for archive contribution, based on supplied job key and proposed situation reference.
 * Only returns results for locations where there is a critical level for the emission result type/substance combination.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_job_archive_contribution_percentage_cl(v_job_key text, v_proposed_situation_reference text)
	RETURNS TABLE(receptor_id integer, emission_result_type emission_result_type, substance_id smallint, result real, percentage_cl real, total_percentage_cl real) AS
$BODY$
	SELECT
		calculation_results.receptor_id,
		calculation_result_sets.result_type AS emission_result_type,
		calculation_result_sets.substance_id,
		calculation_results.result,
		calculation_results.result / critical_level * 100::real AS percentage_cl,
		calculation_results.total_result / critical_level * 100::real AS total_percentage_cl

		FROM jobs.archive_contribution_results AS calculation_results
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN critical_levels USING (receptor_id, result_type, substance_id)

		WHERE critical_level > 0
			AND calculation_id = (SELECT jobs.ae_scenario_get_calculation_id_of_situation_reference(v_job_key, v_proposed_situation_reference));
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_job_archive_contribution_sub_percentage_cl
 * ------------------------------------------------------
 * Function to determine the percentages of critical levels for archive contribution, based on supplied job key and proposed situation reference, for sub points.
 * Critical level is based on the associated receptor, not the actual sub point area.
 * Only returns results for locations where there is a critical level for the emission result type/substance combination.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_job_archive_contribution_sub_percentage_cl(v_job_key text, v_proposed_situation_reference text, v_hexagon_type hexagon_type)
	RETURNS TABLE(calculation_sub_point_id integer, receptor_id integer, emission_result_type emission_result_type, substance_id smallint, result real, percentage_cl real, total_percentage_cl real, geometry geometry(Point)) AS
$BODY$
	SELECT
		calculation_results.calculation_sub_point_id,
		calculation_results.receptor_id,
		calculation_result_sets.result_type AS emission_result_type,
		calculation_result_sets.substance_id,
		calculation_results.result,
		calculation_results.result / critical_level * 100::real AS percentage_cl,
		calculation_results.total_result / critical_level * 100::real AS total_percentage_cl,
		calculation_sub_points.geometry

		FROM jobs.archive_contribution_sub_results AS calculation_results
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN jobs.calculations USING (calculation_id)
			INNER JOIN jobs.calculation_sub_points USING (calculation_sub_point_set_id, calculation_sub_point_id, receptor_id)
			INNER JOIN critical_levels USING (receptor_id, result_type, substance_id)
			INNER JOIN receptor_hexagon_type_view USING (receptor_id)

		WHERE critical_level > 0
			AND calculation_id = (SELECT jobs.ae_scenario_get_calculation_id_of_situation_reference(v_job_key, v_proposed_situation_reference))
			AND hexagon_type = v_hexagon_type;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_archive_contribution_statistics_base
 * ------------------------------------------------
 * Base function to determine statistics for archive contributions.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_archive_contribution_statistics_base(v_proposed_calculation_id integer)
	RETURNS TABLE(calculation_result_set_id integer, receptor_id integer, result real, total_result real) AS
$BODY$
	SELECT
		calculation_result_set_id,
		calculation_results.receptor_id,
		calculation_results.result,
		calculation_results.total_result

		FROM jobs.archive_contribution_results AS calculation_results
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)

		WHERE calculation_id = v_proposed_calculation_id
			-- If it's deposition take the threshold into account, otherwise don't.
			-- Only do deposition, concentrations will be on sub level.
			AND calculation_result_sets.result_type = 'deposition'
			AND ABS(calculation_results.result) > ae_constant('PRONOUNCEMENT_THRESHOLD_VALUE')::posreal;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_archive_contribution_areas_statistics_base
 * ------------------------------------------------------
 * Base function to determine statistics for archive contributions for assessment areas/habitats.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_archive_contribution_areas_statistics_base(v_proposed_calculation_id integer)
	RETURNS TABLE(calculation_result_set_id integer, hexagon_type hexagon_type, receptor_id integer, assessment_area_id integer, critical_deposition_area_id integer, cartographic_surface real, result real, total_result real, percentage_cl real, total_percentage_cl real) AS
$BODY$
	SELECT
		calculation_result_set_id,
		hexagon_type,
		calculation_results.receptor_id,
		assessment_area_id,
		critical_deposition_area_id,
		cartographic_surface,
		calculation_results.result,
		calculation_results.total_result,
		calculation_results.result / NULLIF(critical_level, 0) * 100::real AS percentage_cl,
		calculation_results.total_result / NULLIF(critical_level, 0) * 100::real AS total_percentage_cl

		FROM jobs.ae_scenario_archive_contribution_statistics_base(v_proposed_calculation_id) AS calculation_results
			INNER JOIN receptors_to_relevant_habitats USING (receptor_id)
			INNER JOIN jobs.ae_receptors_with_hexagon_type(ARRAY[v_proposed_calculation_id]) USING (receptor_id)
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			LEFT JOIN critical_deposition_area_critical_levels_view USING (critical_deposition_area_id, result_type, substance_id)
		WHERE hexagon_type != 'extra_assessment_hexagons'
	UNION ALL
	SELECT
		calculation_result_set_id,
		hexagon_type,
		calculation_results.receptor_id,
		assessment_area_id,
		habitat_type_id AS critical_deposition_area_id,
		1 AS cartographic_surface,
		calculation_results.result,
		calculation_results.total_result,
		calculation_results.result / NULLIF(critical_level, 0) * 100::real AS percentage_cl,
		calculation_results.total_result / NULLIF(critical_level, 0) * 100::real AS total_percentage_cl

		FROM jobs.ae_scenario_archive_contribution_statistics_base(v_proposed_calculation_id) AS calculation_results
			INNER JOIN receptors_to_assessment_areas USING (receptor_id)
			INNER JOIN jobs.ae_receptors_with_hexagon_type(ARRAY[v_proposed_calculation_id]) USING (receptor_id)
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN extra_assessment_receptors USING (receptor_id, assessment_area_id)
			LEFT JOIN critical_deposition_area_critical_levels_view ON 
				(critical_deposition_area_critical_levels_view.critical_deposition_area_id = extra_assessment_receptors.habitat_type_id
					AND critical_deposition_area_critical_levels_view.result_type = calculation_result_sets.result_type
					AND critical_deposition_area_critical_levels_view.substance_id = calculation_result_sets.substance_id)
		WHERE hexagon_type = 'extra_assessment_hexagons';
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_archive_contribution_sub_statistics_base
 * ----------------------------------------------------
 * Base function to determine statistics for archive contributions for sub points.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_archive_contribution_sub_statistics_base(v_proposed_calculation_id integer)
	RETURNS TABLE(calculation_result_set_id integer, calculation_sub_point_id integer, receptor_id integer, assessment_area_id integer, critical_deposition_area_id integer, result real, total_result real, percentage_cl real, total_percentage_cl real) AS
$BODY$
	SELECT
		calculation_result_set_id,
		calculation_results.calculation_sub_point_id,
		calculation_results.receptor_id,
		assessment_area_id,
		critical_deposition_area_id,
		calculation_results.result,
		calculation_results.total_result,
		calculation_results.result / NULLIF(critical_level, 0) * 100::real AS percentage_cl,
		calculation_results.total_result / NULLIF(critical_level, 0) * 100::real AS total_percentage_cl

		FROM jobs.archive_contribution_sub_results AS calculation_results
			INNER JOIN receptors_to_relevant_habitats USING (receptor_id)
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			LEFT JOIN critical_deposition_area_critical_levels_view USING (critical_deposition_area_id, result_type, substance_id)
	
		WHERE calculation_id = v_proposed_calculation_id
			-- Only do sub statistics for non-depositions
			AND calculation_result_sets.result_type != 'deposition';
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_archive_contribution_statistics
 * -------------------------------------------
 * Function returning the general statistics for archive contributions.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_archive_contribution_statistics(v_proposed_calculation_id integer, v_hexagon_type hexagon_type)
	RETURNS TABLE(emission_result_type emission_result_type, substance_id smallint, result_statistic_type result_statistic_type, value real) AS
$BODY$
	SELECT
		calculation_result_sets.result_type AS emission_result_type,
		calculation_result_sets.substance_id,
		UNNEST(array ['count_receptors'::result_statistic_type, 'count_receptors_increase'::result_statistic_type, 'count_receptors_decrease'::result_statistic_type, 'max_increase'::result_statistic_type, 'max_decrease'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [count(DISTINCT receptor_id)::real, coalesce(count(DISTINCT receptor_id) filter ( where result > 0 ), 0)::real, coalesce(count(DISTINCT receptor_id) filter ( where result < 0 ), 0)::real, max(result) filter ( where result > 0 ), -min(result) filter ( where result < 0 ), max(total_result)]) AS value

		FROM jobs.ae_scenario_archive_contribution_statistics_base(v_proposed_calculation_id)
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN receptor_hexagon_type_view USING (receptor_id)

		WHERE hexagon_type = v_hexagon_type

		GROUP BY emission_result_type, substance_id
	UNION ALL
	SELECT
		calculation_result_sets.result_type AS emission_result_type,
		calculation_result_sets.substance_id,
		UNNEST(array ['sum_cartographic_surface'::result_statistic_type, 'sum_cartographic_surface_increase'::result_statistic_type, 'sum_cartographic_surface_decrease'::result_statistic_type, 'max_percentage_critical_level'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [sum(cartographic_surface::numeric)::real, coalesce(sum(cartographic_surface::numeric) filter ( where result > 0 ), 0)::real, coalesce(sum(cartographic_surface::numeric) filter ( where result < 0 ), 0)::real, max(percentage_cl)]) AS value

		FROM jobs.ae_scenario_archive_contribution_areas_statistics_base(v_proposed_calculation_id)
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)

		WHERE hexagon_type = v_hexagon_type

		GROUP BY emission_result_type, substance_id
	UNION ALL
	SELECT
		calculation_result_sets.result_type AS emission_result_type,
		calculation_result_sets.substance_id,
		UNNEST(array ['count_calculation_points'::result_statistic_type, 'count_calculation_points_increase'::result_statistic_type, 'count_calculation_points_decrease'::result_statistic_type, 'max_increase'::result_statistic_type, 'max_decrease'::result_statistic_type, 'max_total'::result_statistic_type, 'max_percentage_critical_level'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [count(DISTINCT (calculation_sub_point_id, receptor_id))::real, coalesce(count(DISTINCT (calculation_sub_point_id, receptor_id)) filter ( where result > 0 ), 0)::real, coalesce(count(DISTINCT (calculation_sub_point_id, receptor_id)) filter ( where result < 0 ), 0)::real, max(result) filter ( where result > 0 ), -min(result) filter ( where result < 0 ), max(total_result), max(percentage_cl)]) AS value

		FROM jobs.ae_scenario_archive_contribution_sub_statistics_base(v_proposed_calculation_id)
			INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
			INNER JOIN receptor_hexagon_type_view USING (receptor_id)

		WHERE hexagon_type = v_hexagon_type

		GROUP BY emission_result_type, substance_id;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_scenario_archive_contribution_fill_calculation_statistics
 * ------------------------------------------------------------
 * Function to determine all statistics for archive contributions and to persist them in the appropriate tables.
 */
CREATE OR REPLACE FUNCTION jobs.ae_scenario_archive_contribution_fill_calculation_statistics(v_job_id integer, v_proposed_calculation_id integer)
	RETURNS void AS
$BODY$
	-- Assessment area
	INSERT INTO jobs.scenario_calculation_assessment_area_statistics (job_id, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		'archive_contribution'::scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		UNNEST(array ['count_receptors'::result_statistic_type, 'count_receptors_increase'::result_statistic_type, 'count_receptors_decrease'::result_statistic_type, 'sum_cartographic_surface'::result_statistic_type, 'sum_cartographic_surface_increase'::result_statistic_type, 'sum_cartographic_surface_decrease'::result_statistic_type, 'max_increase'::result_statistic_type, 'max_decrease'::result_statistic_type, 'max_total'::result_statistic_type, 'max_percentage_critical_level'::result_statistic_type, 'max_total_percentage_critical_level'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [count(DISTINCT receptor_id)::real, coalesce(count(DISTINCT receptor_id) filter ( where result > 0 ), 0)::real, coalesce(count(DISTINCT receptor_id) filter ( where result < 0 ), 0)::real, sum(cartographic_surface::numeric)::real, coalesce(sum(cartographic_surface::numeric) filter ( where result > 0 ), 0)::real, coalesce(sum(cartographic_surface::numeric) filter ( where result < 0 ), 0)::real, max(result) filter ( where result > 0 ), -min(result) filter ( where result < 0 ), max(total_result), max(percentage_cl), max(total_percentage_cl)]) AS value

		FROM jobs.ae_scenario_archive_contribution_areas_statistics_base(v_proposed_calculation_id)
		GROUP BY calculation_result_set_id, assessment_area_id, hexagon_type;


	-- Assessment area and critical deposition area
	INSERT INTO jobs.scenario_calculation_critical_deposition_area_statistics (job_id, scenario_result_type, calculation_result_set_id, assessment_area_id, critical_deposition_area_id, hexagon_type, overlapping_hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		'archive_contribution'::scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		critical_deposition_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		UNNEST(array ['count_receptors'::result_statistic_type, 'count_receptors_increase'::result_statistic_type, 'count_receptors_decrease'::result_statistic_type, 'sum_cartographic_surface'::result_statistic_type, 'sum_cartographic_surface_increase'::result_statistic_type, 'sum_cartographic_surface_decrease'::result_statistic_type, 'max_increase'::result_statistic_type, 'max_decrease'::result_statistic_type, 'max_total'::result_statistic_type, 'max_percentage_critical_level'::result_statistic_type, 'max_total_percentage_critical_level'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [count(DISTINCT receptor_id)::real, coalesce(count(DISTINCT receptor_id) filter ( where result > 0 ), 0)::real, coalesce(count(DISTINCT receptor_id) filter ( where result < 0 ), 0)::real, sum(cartographic_surface::numeric)::real, coalesce(sum(cartographic_surface::numeric) filter ( where result > 0 ), 0)::real, coalesce(sum(cartographic_surface::numeric) filter ( where result < 0 ), 0)::real, max(result) filter ( where result > 0 ), -min(result) filter ( where result < 0 ), max(total_result), max(percentage_cl), max(total_percentage_cl)]) AS value

		FROM jobs.ae_scenario_archive_contribution_areas_statistics_base(v_proposed_calculation_id)
		GROUP BY calculation_result_set_id, assessment_area_id, critical_deposition_area_id, hexagon_type;


	-- Markers
	INSERT INTO jobs.scenario_calculation_assessment_area_statistic_markers (job_id, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, result_statistic_type, receptor_id)
	SELECT DISTINCT ON (calculation_result_set_id, assessment_area_id, hexagon_type, result_statistic_type)
		v_job_id,
		'archive_contribution'::scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		UNNEST(array ['max_increase'::result_statistic_type, 'max_decrease'::result_statistic_type, 'max_total'::result_statistic_type, 'max_percentage_critical_level'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [
			first_value(CASE WHEN result > 0 THEN receptor_id END) OVER (PARTITION BY calculation_result_set_id, hexagon_type, assessment_area_id ORDER BY result DESC, receptor_id),
			first_value(CASE WHEN result < 0 THEN receptor_id END) OVER (PARTITION BY calculation_result_set_id, hexagon_type, assessment_area_id ORDER BY result, receptor_id),
			first_value(receptor_id) OVER (PARTITION BY calculation_result_set_id, hexagon_type, assessment_area_id ORDER BY total_result DESC, receptor_id),
			first_value(CASE WHEN percentage_cl IS NOT NULL THEN receptor_id END) OVER (PARTITION BY calculation_result_set_id, hexagon_type, assessment_area_id ORDER BY percentage_cl DESC, receptor_id)
			]) AS receptor_id

		FROM jobs.ae_scenario_archive_contribution_areas_statistics_base(v_proposed_calculation_id);


	-- Charts: emission results
	INSERT INTO jobs.scenario_calculation_assessment_area_chart_statistics (job_id, emission_result_chart_type, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, color_range_type, lower_bound, cartographic_surface)
	SELECT
		v_job_id,
		'emission_result'::emission_result_chart_type,
		scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		color_range_type,
		(UNNEST(ae_color_range(color_range_type, result::numeric, cartographic_surface::numeric))).lower_value::real AS lower_bound,
		(UNNEST(ae_color_range(color_range_type, result::numeric, cartographic_surface::numeric))).total::real AS cartographic_surface

	FROM jobs.ae_scenario_archive_contribution_areas_statistics_base(v_proposed_calculation_id)
		INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
		INNER JOIN scenario_result_types_color_ranges ON (calculation_result_sets.result_type = scenario_result_types_color_ranges.emission_result_type AND calculation_result_sets.substance_id = scenario_result_types_color_ranges.substance_id)

	WHERE scenario_result_type = 'archive_contribution'::scenario_result_type
		AND hexagon_type != 'extra_assessment_hexagons'

	GROUP BY scenario_result_type, calculation_result_set_id, assessment_area_id, color_range_type, hexagon_type;
	
	-- Special case for extra_assessment_hexagons, where we want to display number of unique receptors
	WITH area_results AS (
	SELECT DISTINCT
		calculation_result_set_id,
		hexagon_type,
		receptor_id,
		assessment_area_id,
		result

		FROM jobs.ae_scenario_archive_contribution_areas_statistics_base(v_proposed_calculation_id)
		WHERE hexagon_type = 'extra_assessment_hexagons'
	)
	INSERT INTO jobs.scenario_calculation_assessment_area_chart_statistics (job_id, emission_result_chart_type, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, color_range_type, lower_bound, cartographic_surface)
	SELECT
		v_job_id,
		'emission_result'::emission_result_chart_type,
		scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		color_range_type,
		(UNNEST(ae_color_range(color_range_type, result::numeric, 1::numeric))).lower_value::real AS lower_bound,
		(UNNEST(ae_color_range(color_range_type, result::numeric, 1::numeric))).total::real AS cartographic_surface

	FROM area_results
		INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
		INNER JOIN scenario_result_types_color_ranges ON (calculation_result_sets.result_type = scenario_result_types_color_ranges.emission_result_type AND calculation_result_sets.substance_id = scenario_result_types_color_ranges.substance_id)

	WHERE scenario_result_type = 'archive_contribution'::scenario_result_type

	GROUP BY scenario_result_type, calculation_result_set_id, assessment_area_id, color_range_type, hexagon_type;


	-- Charts: percentage CL
	INSERT INTO jobs.scenario_calculation_assessment_area_chart_statistics (job_id, emission_result_chart_type, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, color_range_type, lower_bound, cartographic_surface)
	SELECT
		v_job_id,
		'percentage_cl'::emission_result_chart_type,
		'archive_contribution'::scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		'archive_contribution_percentage_critical_load'::color_range_type,
		(UNNEST(ae_color_range('archive_contribution_percentage_critical_load'::color_range_type, percentage_cl::numeric, cartographic_surface::numeric))).lower_value::real AS lower_bound,
		(UNNEST(ae_color_range('archive_contribution_percentage_critical_load'::color_range_type, percentage_cl::numeric, cartographic_surface::numeric))).total::real AS cartographic_surface

	FROM jobs.ae_scenario_archive_contribution_areas_statistics_base(v_proposed_calculation_id)
		INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)

	WHERE percentage_cl IS NOT NULL
		AND hexagon_type != 'extra_assessment_hexagons'

	GROUP BY scenario_result_type, calculation_result_set_id, assessment_area_id, color_range_type, hexagon_type;

	-- Special case for extra_assessment_hexagons, where we want to display number of unique receptors
	WITH area_results AS (
	SELECT DISTINCT
		calculation_result_set_id,
		hexagon_type,
		receptor_id,
		assessment_area_id,
		percentage_cl

		FROM jobs.ae_scenario_archive_contribution_areas_statistics_base(v_proposed_calculation_id)
		WHERE hexagon_type = 'extra_assessment_hexagons'
	)
	INSERT INTO jobs.scenario_calculation_assessment_area_chart_statistics (job_id, emission_result_chart_type, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, color_range_type, lower_bound, cartographic_surface)
	SELECT
		v_job_id,
		'percentage_cl'::emission_result_chart_type,
		'archive_contribution'::scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		'archive_contribution_percentage_critical_load'::color_range_type,
		(UNNEST(ae_color_range('archive_contribution_percentage_critical_load'::color_range_type, percentage_cl::numeric, 1::numeric))).lower_value::real AS lower_bound,
		(UNNEST(ae_color_range('archive_contribution_percentage_critical_load'::color_range_type, percentage_cl::numeric, 1::numeric))).total::real AS cartographic_surface

	FROM area_results
		INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)

	WHERE percentage_cl IS NOT NULL

	GROUP BY scenario_result_type, calculation_result_set_id, assessment_area_id, color_range_type, hexagon_type;


	-- Sub points
	-- Assessment area
	INSERT INTO jobs.scenario_calculation_assessment_area_statistics (job_id, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		'archive_contribution'::scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		UNNEST(array ['count_calculation_points'::result_statistic_type, 'count_calculation_points_increase'::result_statistic_type, 'count_calculation_points_decrease'::result_statistic_type, 'max_increase'::result_statistic_type, 'max_decrease'::result_statistic_type, 'max_total'::result_statistic_type, 'max_percentage_critical_level'::result_statistic_type, 'max_total_percentage_critical_level'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [count(DISTINCT (calculation_sub_point_id, receptor_id))::real, coalesce(count(DISTINCT (calculation_sub_point_id, receptor_id)) filter ( where result > 0 ), 0)::real, coalesce(count(DISTINCT (calculation_sub_point_id, receptor_id)) filter ( where result < 0 ), 0)::real, max(result) filter ( where result > 0 ), -min(result) filter ( where result < 0 ), max(total_result), max(percentage_cl), max(total_percentage_cl)]) AS value

	FROM jobs.ae_scenario_archive_contribution_sub_statistics_base(v_proposed_calculation_id)
		INNER JOIN jobs.ae_receptors_with_hexagon_type(ARRAY[v_proposed_calculation_id]) USING (receptor_id)
	GROUP BY calculation_result_set_id, assessment_area_id, hexagon_type;

	-- Assessment area and critical deposition area
	INSERT INTO jobs.scenario_calculation_critical_deposition_area_statistics (job_id, scenario_result_type, calculation_result_set_id, assessment_area_id, critical_deposition_area_id, hexagon_type, overlapping_hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		'archive_contribution'::scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		critical_deposition_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		UNNEST(array ['count_calculation_points'::result_statistic_type, 'count_calculation_points_increase'::result_statistic_type, 'count_calculation_points_decrease'::result_statistic_type, 'max_increase'::result_statistic_type, 'max_decrease'::result_statistic_type, 'max_total'::result_statistic_type, 'max_percentage_critical_level'::result_statistic_type, 'max_total_percentage_critical_level'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [count(DISTINCT (calculation_sub_point_id, receptor_id))::real, coalesce(count(DISTINCT (calculation_sub_point_id, receptor_id)) filter ( where result > 0 ), 0)::real, coalesce(count(DISTINCT (calculation_sub_point_id, receptor_id)) filter ( where result < 0 ), 0)::real, max(result) filter ( where result > 0 ), -min(result) filter ( where result < 0 ), max(total_result), max(percentage_cl), max(total_percentage_cl)]) AS value

	FROM jobs.ae_scenario_archive_contribution_sub_statistics_base(v_proposed_calculation_id)
		INNER JOIN jobs.ae_receptors_with_hexagon_type(ARRAY[v_proposed_calculation_id]) USING (receptor_id)
	GROUP BY calculation_result_set_id, assessment_area_id, critical_deposition_area_id, hexagon_type;

	-- Markers
	INSERT INTO jobs.scenario_calculation_assessment_area_statistic_markers (job_id, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, result_statistic_type, receptor_id)
	SELECT DISTINCT ON (calculation_result_set_id, assessment_area_id, hexagon_type, result_statistic_type)
		v_job_id,
		'archive_contribution'::scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		UNNEST(array ['max_increase'::result_statistic_type, 'max_decrease'::result_statistic_type, 'max_total'::result_statistic_type, 'max_percentage_critical_level'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [
			first_value(CASE WHEN result > 0 THEN receptor_id END) OVER (PARTITION BY calculation_result_set_id, hexagon_type, assessment_area_id ORDER BY result DESC, receptor_id),
			first_value(CASE WHEN result < 0 THEN receptor_id END) OVER (PARTITION BY calculation_result_set_id, hexagon_type, assessment_area_id ORDER BY result, receptor_id),
			first_value(receptor_id) OVER (PARTITION BY calculation_result_set_id, hexagon_type, assessment_area_id ORDER BY total_result DESC, receptor_id),
			first_value(CASE WHEN percentage_cl IS NOT NULL THEN receptor_id END) OVER (PARTITION BY calculation_result_set_id, hexagon_type, assessment_area_id ORDER BY percentage_cl DESC, receptor_id)
			]) AS receptor_id

		FROM jobs.ae_scenario_archive_contribution_sub_statistics_base(v_proposed_calculation_id)
			INNER JOIN jobs.ae_receptors_with_hexagon_type(ARRAY[v_proposed_calculation_id]) USING (receptor_id);

	-- Charts: emission results
	WITH sub_point_results AS (
		SELECT DISTINCT
			calculation_result_set_id,
			calculation_sub_point_id,
			receptor_id,
			assessment_area_id,
			result

			FROM jobs.ae_scenario_archive_contribution_sub_statistics_base(v_proposed_calculation_id)
	)
	INSERT INTO jobs.scenario_calculation_assessment_area_chart_statistics (job_id, emission_result_chart_type, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, color_range_type, lower_bound, cartographic_surface)
	SELECT
		v_job_id,
		'emission_result'::emission_result_chart_type,
		scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		color_range_type,
		(UNNEST(ae_color_range(color_range_type, result::numeric, 1))).lower_value::real AS lower_bound,
		(UNNEST(ae_color_range(color_range_type, result::numeric, 1))).total::real AS cartographic_surface

	FROM sub_point_results
		INNER JOIN jobs.ae_receptors_with_hexagon_type(ARRAY[v_proposed_calculation_id]) USING (receptor_id)
		INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
		INNER JOIN scenario_result_types_color_ranges ON (calculation_result_sets.result_type = scenario_result_types_color_ranges.emission_result_type AND calculation_result_sets.substance_id = scenario_result_types_color_ranges.substance_id)

	WHERE scenario_result_type = 'archive_contribution'::scenario_result_type

	GROUP BY scenario_result_type, calculation_result_set_id, assessment_area_id, color_range_type, hexagon_type;

	-- Charts: percentage CL
	WITH sub_point_results AS (
		SELECT
			calculation_result_set_id,
			calculation_sub_point_id,
			receptor_id,
			assessment_area_id,
			max(percentage_cl) AS percentage_cl

			FROM jobs.ae_scenario_archive_contribution_sub_statistics_base(v_proposed_calculation_id)

			GROUP BY calculation_result_set_id, calculation_sub_point_id, receptor_id, assessment_area_id
	)
	INSERT INTO jobs.scenario_calculation_assessment_area_chart_statistics (job_id, emission_result_chart_type, scenario_result_type, calculation_result_set_id, assessment_area_id, hexagon_type, overlapping_hexagon_type, color_range_type, lower_bound, cartographic_surface)
	SELECT
		v_job_id,
		'percentage_cl'::emission_result_chart_type,
		'archive_contribution'::scenario_result_type,
		calculation_result_set_id,
		assessment_area_id,
		hexagon_type,
		'all_hexagons'::overlapping_hexagon_type,
		'archive_contribution_percentage_critical_load'::color_range_type,
		(UNNEST(ae_color_range('archive_contribution_percentage_critical_load'::color_range_type, percentage_cl::numeric, 1))).lower_value::real AS lower_bound,
		(UNNEST(ae_color_range('archive_contribution_percentage_critical_load'::color_range_type, percentage_cl::numeric, 1))).total::real AS cartographic_surface

	FROM sub_point_results
		INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
		INNER JOIN jobs.ae_receptors_with_hexagon_type(ARRAY[v_proposed_calculation_id]) USING (receptor_id)

	WHERE percentage_cl IS NOT NULL

	GROUP BY scenario_result_type, calculation_result_set_id, assessment_area_id, color_range_type, hexagon_type;
$BODY$
LANGUAGE SQL VOLATILE;
