/*
 * archive_contribution_results_view
 * ---------------------------------
 * View returning all archive contribution results per calculation and receptor.
 */
CREATE OR REPLACE VIEW jobs.archive_contribution_results_view AS
SELECT
	calculation_id,
	calculation_result_set_id,
	receptor_id,
	substance_id,
	result_type,
	result

	FROM jobs.archive_contribution_results
		INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)

	WHERE result_set_type = 'total'
;


/*
 * archive_contribution_sub_results_view
 * -------------------------------------
 * View returning all archive contributions (concentrations, depositions and exceedance days) per calculation and sub point.
 */
CREATE OR REPLACE VIEW jobs.archive_contribution_sub_results_view AS
SELECT
	calculation_id,
	calculation_result_set_id,
	calculation_sub_point_id,
	receptor_id,
	level,
	substance_id,
	result_type,
	result,
	calculation_sub_points.geometry

	FROM jobs.archive_contribution_sub_results
		INNER JOIN jobs.calculation_result_sets USING (calculation_result_set_id)
		INNER JOIN jobs.calculations USING (calculation_id)
		INNER JOIN jobs.calculation_sub_points USING (calculation_sub_point_set_id, calculation_sub_point_id, receptor_id)

	WHERE result_set_type = 'total'
;
