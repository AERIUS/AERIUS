/*
 * calculation_default_distances_view
 * ----------------------------------
 * View returning default distances per permit area and project category.
 */
CREATE OR REPLACE VIEW calculation_default_distances_view AS
SELECT
	calculation_permit_area_id,
	calculation_permit_areas.code AS permit_area_code,
	calculation_project_category_id,
	calculation_project_categories.code AS project_category_code,
	default_calculation_distance

	FROM calculation_default_distances
		INNER JOIN calculation_permit_areas USING (calculation_permit_area_id)
		INNER JOIN calculation_project_categories USING (calculation_project_category_id)
;
