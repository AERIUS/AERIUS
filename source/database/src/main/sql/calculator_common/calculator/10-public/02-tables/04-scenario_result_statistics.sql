/*
 * scenario_assessment_area_statistics
 * -----------------------------------
 * Table containing the aggregated statistics of a scenario job per scenario result type, hexagon type and assessment area.
 */
CREATE TABLE jobs.scenario_assessment_area_statistics (
	job_id integer NOT NULL,
	emission_result_type emission_result_type NOT NULL,
	substance_id smallint NOT NULL,
	scenario_result_type scenario_result_type NOT NULL,
	hexagon_type hexagon_type NOT NULL,
	overlapping_hexagon_type overlapping_hexagon_type NOT NULL,
	assessment_area_id integer NOT NULL,
	result_statistic_type result_statistic_type NOT NULL,
	value real,

	CONSTRAINT scenario_assessment_area_statistics_pkey PRIMARY KEY (job_id, emission_result_type, substance_id, scenario_result_type, hexagon_type, overlapping_hexagon_type, assessment_area_id, result_statistic_type),
	CONSTRAINT scenario_assessment_area_statistics_fk_jobs FOREIGN KEY (job_id) REFERENCES jobs.jobs ON DELETE CASCADE
);


/*
 * scenario_assessment_area_statistic_markers
 * ------------------------------------------
 * Table containing the marker locations (receptor_id) of specific statistics within a scenario job per scenario result type, hexagon type and assessment area.
 */
CREATE TABLE jobs.scenario_assessment_area_statistic_markers (
	job_id integer NOT NULL,
	emission_result_type emission_result_type NOT NULL,
	substance_id smallint NOT NULL,
	scenario_result_type scenario_result_type NOT NULL,
	hexagon_type hexagon_type NOT NULL,
	overlapping_hexagon_type overlapping_hexagon_type NOT NULL,
	assessment_area_id integer NOT NULL,
	result_statistic_type result_statistic_type NOT NULL,
	receptor_id integer,

	CONSTRAINT scenario_assessment_area_statistic_markers_pkey PRIMARY KEY (job_id, emission_result_type, substance_id, scenario_result_type, hexagon_type, overlapping_hexagon_type, assessment_area_id, result_statistic_type),
	CONSTRAINT scenario_assessment_area_statistic_markers_f_jobs FOREIGN KEY (job_id) REFERENCES jobs.jobs ON DELETE CASCADE
);


/*
 * scenario_critical_deposition_area_statistics
 * --------------------------------------------
 * Table containing the aggregated statistics of a scenario job per scenario result type, hexagon type, assessment area and habitat type.
 */
CREATE TABLE jobs.scenario_critical_deposition_area_statistics (
	job_id integer NOT NULL,
	emission_result_type emission_result_type NOT NULL,
	substance_id smallint NOT NULL,
	scenario_result_type scenario_result_type NOT NULL,
	hexagon_type hexagon_type NOT NULL,
	overlapping_hexagon_type overlapping_hexagon_type NOT NULL,
	assessment_area_id integer NOT NULL,
	critical_deposition_area_id integer NOT NULL,
	result_statistic_type result_statistic_type NOT NULL,
	value real,

	CONSTRAINT scenario_critical_deposition_area_statistics_pkey PRIMARY KEY (job_id, emission_result_type, substance_id, scenario_result_type, hexagon_type, overlapping_hexagon_type, assessment_area_id, critical_deposition_area_id, result_statistic_type),
	CONSTRAINT scenario_critical_deposition_area_statistics_f_jobs FOREIGN KEY (job_id) REFERENCES jobs.jobs ON DELETE CASCADE
);


/*
 * scenario_assessment_area_chart_statistics
 * -----------------------------------------
 * Table containing the aggregated statistics of a scenario job for charts per scenario result type, hexagon type and assessment area.
 */
CREATE TABLE jobs.scenario_assessment_area_chart_statistics (
	job_id integer NOT NULL,
	emission_result_chart_type emission_result_chart_type NOT NULL,
	emission_result_type emission_result_type NOT NULL,
	substance_id smallint NOT NULL,
	scenario_result_type scenario_result_type NOT NULL,
	hexagon_type hexagon_type NOT NULL,
	overlapping_hexagon_type overlapping_hexagon_type NOT NULL,
	assessment_area_id integer NOT NULL,
	color_range_type color_range_type NOT NULL,
	lower_bound real NOT NULL,
	cartographic_surface real NOT NULL,

	CONSTRAINT scenario_assessment_area_chart_statistics_pkey PRIMARY KEY (job_id, emission_result_chart_type, emission_result_type, substance_id, scenario_result_type, hexagon_type, overlapping_hexagon_type, assessment_area_id, color_range_type, lower_bound),
	CONSTRAINT scenario_assessment_area_chart_statistics_f_jobs FOREIGN KEY (job_id) REFERENCES jobs.jobs ON DELETE CASCADE
);
