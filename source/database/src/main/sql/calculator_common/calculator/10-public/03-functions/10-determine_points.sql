/*
 * ae_relevant_habitat_points_of_interest_by_source_collection
 * -----------------------------------------------------------
 * Function returning per assessment area and habitat type combination what the nearest point (the points of interest are,
 * given the supplied sources (sourceCollection), the supplied radius (withinRadius) and the selected authorities (selectedAuths).
 */
CREATE OR REPLACE FUNCTION ae_relevant_habitat_points_of_interest_by_source_collection(sourceCollection geometry, withinRadius integer, selectedAuths authority_type[])
	RETURNS TABLE(assessment_area_id integer, habitat_type_id integer, assessment_area_name text, habitat_name text, distance double precision, x_coord integer, y_coord integer) AS
$BODY$
	SELECT
		assessment_area_id,
		habitat_type_id,
		assessment_areas.name AS assessment_area_name,
		habitat_types.name AS habitat_name,
		ST_Distance(relevant_habitats.geometry, ST_ConvexHull(sourceCollection)) AS distance,
		ROUND(ST_X(ST_ClosestPoint(relevant_habitats.geometry, ST_ConvexHull(sourceCollection)))) AS x_coord,
		ROUND(ST_Y(ST_ClosestPoint(relevant_habitats.geometry, ST_ConvexHull(sourceCollection)))) AS y_coord

		FROM relevant_habitats
			INNER JOIN habitat_types USING (habitat_type_id)
			INNER JOIN assessment_areas USING (assessment_area_id)
			INNER JOIN authorities USING (authority_id)

		WHERE ST_Intersects(relevant_habitats.geometry, ST_Buffer(ST_ConvexHull(sourceCollection), withinRadius))
			AND authorities.type = ANY(selectedAuths)
$BODY$
LANGUAGE sql STABLE;


/*
 * ae_assessment_area_points_of_interest_by_source_collection
 * ----------------------------------------------------------
 * Function returning per (natura2000) assessment area the nearest point (the points of interest),
 * given the supplied sources (sourceCollection), the supplied radius (withinRadius) and the selected authorities (selectedAuths).
 */
CREATE OR REPLACE FUNCTION ae_assessment_area_points_of_interest_by_source_collection(sourceCollection geometry, withinRadius integer, selectedAuths authority_type[])
	RETURNS TABLE(assessment_area_id integer, assessment_area_name text, distance double precision, x_coord integer, y_coord integer) AS
$BODY$
	SELECT
		assessment_areas.assessment_area_id,
		assessment_areas.name AS assessment_area_name,
		ST_Distance(assessment_areas.geometry, ST_ConvexHull(sourceCollection)) AS distance,
		ROUND(ST_X(ST_ClosestPoint(assessment_areas.geometry, ST_ConvexHull(sourceCollection)))) AS x_coord,
		ROUND(ST_Y(ST_ClosestPoint(assessment_areas.geometry, ST_ConvexHull(sourceCollection)))) AS y_coord

		FROM assessment_areas
			INNER JOIN authorities USING (authority_id)
		WHERE
			ST_Intersects(geometry, ST_Buffer(ST_ConvexHull(sourceCollection), withinRadius))
			AND assessment_areas.type = 'natura2000_area'
			AND authorities.type = ANY(selectedAuths)
$BODY$
LANGUAGE sql STABLE;
