/*
 * archive_contribution_results
 * ----------------------------
 * Table containing archive contribution results per receptor.
 */
CREATE TABLE jobs.archive_contribution_results (
	calculation_result_set_id integer NOT NULL,
	receptor_id integer NOT NULL,
	result real NOT NULL,
	total_result real NOT NULL,

	CONSTRAINT archive_contribution_results_pkey PRIMARY KEY (calculation_result_set_id, receptor_id),
	CONSTRAINT archive_contribution_results_fkey_calculation_result_sets FOREIGN KEY (calculation_result_set_id) REFERENCES jobs.calculation_result_sets ON DELETE CASCADE
);


/*
 * archive_contribution_sub_results
 * --------------------------------
 * Table containing archive contribution results per sub point.
 */
CREATE TABLE jobs.archive_contribution_sub_results (
	calculation_result_set_id integer NOT NULL,
	calculation_sub_point_id integer NOT NULL,
	receptor_id integer NOT NULL,
	result real NOT NULL,
	total_result real NOT NULL,

	CONSTRAINT archive_contribution_sub_results_pkey PRIMARY KEY (calculation_result_set_id, calculation_sub_point_id, receptor_id),
	CONSTRAINT archive_contribution_sub_results_fkey_calculation_result_sets FOREIGN KEY (calculation_result_set_id) REFERENCES jobs.calculation_result_sets ON DELETE CASCADE
);


/*
 * archive_metadata
 * ----------------
 * Table containing metadata about archive usage for a job.
 */
CREATE TABLE jobs.archive_metadata (
	calculation_id integer NOT NULL,
	metadata json,

	CONSTRAINT archive_metadata_pkey PRIMARY KEY (calculation_id),
	CONSTRAINT archive_metadata_fkey_calculations FOREIGN KEY (calculation_id) REFERENCES jobs.calculations ON DELETE CASCADE
);
