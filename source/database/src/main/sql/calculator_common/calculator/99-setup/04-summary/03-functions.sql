/*
 * ae_output_summary_table
 * -----------------------
 * Function generating files with statistics for all Natura 2000 areas.
 * Should be used to compare database releases.
 *
 * This function generates a file in the supplied location.
 * it is a generic table with N2000 information. The export is CSV.
 * The location to export to should contain the string {title}, this will be replaced per table for which information is exported by the name of the table.
 * The string {datesuffix} will be replaced by the current date in YYYYMMDD format.
 * @param filespec Path and filename as described.
 */
CREATE OR REPLACE FUNCTION setup.ae_output_summary_table(filespec text)
	RETURNS void AS
$BODY$
DECLARE
	filename text;
	query text;
BEGIN
	RAISE NOTICE 'Creating summary output.';

	-- There's a number of views that we'll store in temporary tables for the duration of this function. This is
	-- to improve performance when we are creating the tables later on.
	-- Additionally we'll do a temporary mapping between natura2000 areas and receptors. Also for performance.

	RAISE NOTICE 'Mapping N2000 areas to receptors in temporary table...';

	CREATE TEMPORARY TABLE temp_natura2000_areas_to_receptors (
		natura2000_area_id integer NOT NULL,
		receptor_id integer NOT NULL,

		CONSTRAINT temp_natura2000_areas_to_receptors_pkey PRIMARY KEY (natura2000_area_id, receptor_id)
	) ON COMMIT DROP;
	CREATE INDEX idx_temp_natura2000_areas_to_receptors_receptor_id ON temp_natura2000_areas_to_receptors (receptor_id);

	INSERT INTO temp_natura2000_areas_to_receptors(natura2000_area_id, receptor_id)
	SELECT
		natura2000_area_id,
		receptor_id

		FROM natura2000_areas
			INNER JOIN receptors ON ST_Intersects(receptors.geometry, natura2000_areas.geometry);

	filename := replace(filespec, '{datesuffix}', to_char(current_timestamp, 'YYYYMMDD'));
	filename := '''' || filename || '''';

	-- Main table
	-- Natura 2000 number, name, geometry surface, number of receptors touching, number of habitats touching.

	RAISE NOTICE 'Creating main table...';

	query := $QUERY$
	SELECT
		natura2000_area_id,
		name,
		ROUND(ST_Area(geometry)) AS surface,
		num_receptors,
		num_habitats

		FROM natura2000_areas
			INNER JOIN
				(SELECT
					natura2000_area_id,
					COUNT(receptor_id) AS num_receptors

					FROM temp_natura2000_areas_to_receptors

					GROUP BY natura2000_area_id
				) AS receptor_count USING (natura2000_area_id)
			INNER JOIN
				(SELECT
					natura2000_area_id,
					COUNT(habitat_type_id) AS num_habitats
					/*, SUM(ST_Area(habitat_areas.geometry))*/

					FROM natura2000_areas
						INNER JOIN habitat_areas ON ST_Intersects(habitat_areas.geometry, natura2000_areas.geometry)
						INNER JOIN habitat_types USING (habitat_type_id)

					GROUP BY natura2000_area_id
				) AS habitat_count USING (natura2000_area_id)

		ORDER BY natura2000_area_id
	$QUERY$;

	EXECUTE 'COPY (' || query || ') TO ' || replace(filename, '{title}', 'main_summary_table') || ' CSV HEADER';
END;
$BODY$
LANGUAGE plpgsql VOLATILE;
