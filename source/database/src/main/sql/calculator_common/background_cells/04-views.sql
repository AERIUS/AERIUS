/*
 * background_cell_results_view
 * ----------------------------
 * View returning background results (concentrations, depositions, etc).
 * Use 'year', 'substance_id', 'emission_result_type' and ST_Intersects(.., geometry) in the where-clause.
 * Example:
 *     WHERE year = 2018 AND substance_id = 10 AND emission_result_type = 'concentration'
 *     AND ST_Intersects(ST_SetSRID(ST_Point(218928, 486793), ae_get_srid()), geometry)
 */
CREATE OR REPLACE VIEW background_cell_results_view AS
SELECT
	background_cell_id,
	year,
	substance_id,
	result_type AS emission_result_type,
	result,
	geometry

	FROM background_cells
		INNER JOIN background_cell_results USING (background_cell_id)
;
