/*
 * farm_animal_categories
 * ----------------------
 * Translation table for farm animal categories.
 */
CREATE TABLE i18n.farm_animal_categories (
	farm_animal_category_id integer NOT NULL,
	language_code i18n.language_code_type NOT NULL,
	description text,

	CONSTRAINT farm_animal_categories_pkey PRIMARY KEY (farm_animal_category_id, language_code),
	CONSTRAINT farm_animal_categories_fkey FOREIGN KEY (farm_animal_category_id) REFERENCES farm_animal_categories
);


/*
 * farm_animal_housing_categories
 * ------------------------------
 * Translation table for farm animal housing categories.
 */
CREATE TABLE i18n.farm_animal_housing_categories (
	farm_animal_housing_category_id integer NOT NULL,
	language_code i18n.language_code_type NOT NULL,
	description text,

	CONSTRAINT farm_animal_housing_categories_pkey PRIMARY KEY (farm_animal_housing_category_id, language_code),
	CONSTRAINT farm_animal_housing_categories_fkey FOREIGN KEY (farm_animal_housing_category_id) REFERENCES farm_animal_housing_categories
);


/*
 * farm_additional_housing_systems
 * -------------------------------
 * Translation table for farm animal additional housing systems.
 */
CREATE TABLE i18n.farm_additional_housing_systems (
	farm_additional_housing_system_id integer NOT NULL,
	language_code i18n.language_code_type NOT NULL,
	description text,

	CONSTRAINT farm_additional_housing_systems_pkey PRIMARY KEY (farm_additional_housing_system_id, language_code),
	CONSTRAINT farm_additional_housing_systems_fkey FOREIGN KEY (farm_additional_housing_system_id) REFERENCES farm_additional_housing_systems
);


/*
 * farm_source_categories
 * ----------------------
 * Translation table for farm source categories.
 */
CREATE TABLE i18n.farm_source_categories (
	farm_source_category_id integer NOT NULL,
	language_code i18n.language_code_type NOT NULL,
	description text,

	CONSTRAINT farm_source_categories_pkey PRIMARY KEY (farm_source_category_id, language_code),
	CONSTRAINT farm_source_categories_fkey FOREIGN KEY (farm_source_category_id) REFERENCES farm_source_categories
);
