/*
 * shipping_inland_laden_state
 * ---------------------------
 * Enum indicating if an inland shipping ship is laden or not.
 */
CREATE TYPE shipping_inland_laden_state AS ENUM
	('laden', 'unladen');
