/*
 * farm_animal_categories
 * ----------------------
 * Table containing the farm animal categories.
 * 
 * For NL: these are the categories as present in the omgevingswet list.
 */
CREATE TABLE farm_animal_categories (
	farm_animal_category_id integer NOT NULL,
	code text NOT NULL UNIQUE,
	farm_animal_type farm_animal_type NOT NULL,
	name text NOT NULL UNIQUE,
	description text,

	CONSTRAINT farm_animal_categories_pkey PRIMARY KEY (farm_animal_category_id)
);


/*
 * farm_animal_housing_categories
 * ------------------------------
 * Table containing farm animal housing systems (huisvestingssystemen).
 * A housing system is always linked to a single farm animal category.
 * 
 * For NL: this is in essence the Omgevingsregeling huisvestingssystemen list, see https://wetten.overheid.nl/jci1.3:c:BWBR0045528&bijlage=V&z=2024-01-01&g=2024-01-01
 */
CREATE TABLE farm_animal_housing_categories (
	farm_animal_housing_category_id integer NOT NULL,
	farm_animal_category_id integer NOT NULL,
	code text NOT NULL UNIQUE,
	name text NOT NULL UNIQUE,
	description text,
	farm_emission_factor_type farm_emission_factor_type NOT NULL,

	CONSTRAINT farm_animal_housing_categories_pkey PRIMARY KEY (farm_animal_housing_category_id),
	CONSTRAINT farm_animal_housing_categories_fkey_animal_categories FOREIGN KEY (farm_animal_category_id) REFERENCES farm_animal_categories
);


/*
 * farm_animal_basic_housing
 * -------------------------
 * Table containing what the basic housing systems are for non-basic housing systems.
 * 
 * This can be used to determine the emission reduction obtained by using a non-basic housing system with respect to the basic housing system.
 * This in turn is required for a NL calculation rule for additional air scrubber systems.
 * 
 * If not specified for a housing category, the assumption is made that the housing category itself is basic.
 * This effectively disables the calculation rule (for UK for instance).
 */
CREATE TABLE farm_animal_basic_housing (
	farm_animal_housing_category_id integer NOT NULL,
	basic_housing_category_id integer NOT NULL,

	CONSTRAINT farm_animal_basic_housing_pkey PRIMARY KEY (farm_animal_housing_category_id),
	CONSTRAINT farm_animal_basic_housing_fkey_housing_categories FOREIGN KEY (farm_animal_housing_category_id) REFERENCES farm_animal_housing_categories,
	CONSTRAINT farm_animal_basic_housing_fkey_basic_housing FOREIGN KEY (basic_housing_category_id) REFERENCES farm_animal_housing_categories
);


/*
 * farm_housing_emission_factors
 * -----------------------------
 * Table containing the emission factors for animal housing.
 * 
 * Unit of the emission factor depends on the farm_emission_factor_type of the associated housing system.
 * Generally this is kg/animal/year, sometimes kg/animal/day.
 */
CREATE TABLE farm_housing_emission_factors (
	farm_animal_housing_category_id integer NOT NULL,
	substance_id smallint NOT NULL,
	emission_factor posreal NOT NULL,

	CONSTRAINT farm_housing_emission_factors_pkey PRIMARY KEY (farm_animal_housing_category_id),
	CONSTRAINT farm_housing_emission_factors_fkey_housing_categories FOREIGN KEY (farm_animal_housing_category_id) REFERENCES farm_animal_housing_categories,
	CONSTRAINT farm_housing_emission_factors_fkey_substances FOREIGN KEY (substance_id) REFERENCES substances
);


/*
 * farm_additional_housing_systems
 * -------------------------------
 * Table containing additional farm animal housing systems (aanvullende technieken).
 * These are not directly related to housing systems, but there are reducton factors per animal category instead.
 * 
 * The air scrubber boolean is an indication required for a calculation rule specific for using multiple air scrubbers.
 * 
 * For NL: this is in essence the Omgevingsregeling aanvullende technieken list, see https://wetten.overheid.nl/jci1.3:c:BWBR0045528&bijlage=VI&z=2024-01-01&g=2024-01-01
 */
CREATE TABLE farm_additional_housing_systems (
	farm_additional_housing_system_id integer NOT NULL,
	code text NOT NULL UNIQUE,
	name text NOT NULL UNIQUE,
	description text,
	air_scrubber boolean NOT NULL,

	CONSTRAINT farm_additional_housing_systems_pkey PRIMARY KEY (farm_additional_housing_system_id)
);


/*
 * farm_additional_housing_factors
 * -------------------------------
 * Table containing the reduction factors for additional animal housing systems.
 * 
 * Reduction factors can be different per animal category, hence the reference to farm_animal_categories.
 * This link can be used to determine which additional system can be used for which housing system (which is also linked to farm_animal_categories).
 */
CREATE TABLE farm_additional_housing_factors (
	farm_additional_housing_system_id integer NOT NULL,
	farm_animal_housing_category_id integer NOT NULL,
	substance_id smallint NOT NULL,
	reduction_factor fraction NOT NULL,

	CONSTRAINT farm_additional_housing_factors_pkey PRIMARY KEY (farm_additional_housing_system_id, farm_animal_housing_category_id, substance_id),
	CONSTRAINT farm_additional_housing_factors_fkey_additional_categories FOREIGN KEY (farm_additional_housing_system_id) REFERENCES farm_additional_housing_systems,
	CONSTRAINT farm_additional_housing_factors_fkey_housing_categories FOREIGN KEY (farm_animal_housing_category_id) REFERENCES farm_animal_housing_categories,
	CONSTRAINT farm_additional_housing_factors_fkey_substances FOREIGN KEY (substance_id) REFERENCES substances
);


/*
 * farm_housing_categories_additional_systems
 * ------------------------------------------
 * Table containing the allowed additional housing systems per housing category.
 * 
 * If not specified for a housing category, the additional housing system should not be used for that housing category.
 */
CREATE TABLE farm_housing_categories_additional_systems (
	farm_animal_housing_category_id integer NOT NULL,
	farm_additional_housing_system_id integer NOT NULL,

	CONSTRAINT farm_housing_categories_additional_systems_pkey PRIMARY KEY (farm_animal_housing_category_id, farm_additional_housing_system_id),
	CONSTRAINT farm_housing_categories_additional_systems_fkey_housing_categories FOREIGN KEY (farm_animal_housing_category_id) REFERENCES farm_animal_housing_categories,
	CONSTRAINT farm_housing_categories_additional_systems_fkey_additional_systems FOREIGN KEY (farm_additional_housing_system_id) REFERENCES farm_additional_housing_systems
);


/*
 * farm_source_categories
 * ----------------------
 * Table containing the farm source categories.
 * A farm source category is a combination of a sector, animal type and emission factor type.
 * The goal is to differentiate in the unit of emissions of different types of farm emission sources.
 */
CREATE TABLE farm_source_categories (
	farm_source_category_id integer NOT NULL,
	sector_id integer NOT NULL,
	code text NOT NULL UNIQUE,
	farm_animal_category_id integer NOT NULL,
	name text NOT NULL,
	description text,
	farm_emission_factor_type farm_emission_factor_type NOT NULL,

	CONSTRAINT farm_source_categories_pkey PRIMARY KEY (farm_source_category_id),
	CONSTRAINT farm_source_categories_fkey_sectors FOREIGN KEY (sector_id) REFERENCES sectors,
	CONSTRAINT farm_source_categories_fkey_farm_animal_categories FOREIGN KEY (farm_animal_category_id) REFERENCES farm_animal_categories
);

/*
 * farm_source_emission_factors
 * ----------------------------
 * Table containing the emission factors for farm source categories.
 */
CREATE TABLE farm_source_emission_factors (
	farm_source_category_id integer NOT NULL,
	substance_id integer NOT NULL,
	emission_factor real NOT NULL,

	CONSTRAINT farm_source_emission_factors_pkey PRIMARY KEY (farm_source_category_id, substance_id),
	CONSTRAINT farm_source_emission_factors_fkey_farm_source_categories FOREIGN KEY (farm_source_category_id) REFERENCES farm_source_categories,
	CONSTRAINT farm_source_emission_factors_fkey_substances FOREIGN KEY (substance_id) REFERENCES substances
);
