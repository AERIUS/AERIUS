/*
 * wms_met_sites_nwp_view
 * ----------------------
 * WMS view returning NWP site locations as a circle with a radius of 1km.
 */
CREATE OR REPLACE VIEW wms_met_sites_nwp_view AS
WITH nwp_sites AS (
SELECT DISTINCT met_sites.* 
	FROM met_sites
		INNER JOIN met_site_datasets USING (met_site_id)
	WHERE dataset_type = 'nwp_3km2'
)
SELECT 
	met_site_id, 
	name, 
	ST_Buffer(geometry, 1000) as geometry
	
	FROM nwp_sites
;
