/*
 * met_sites
 * ---------
 * Table containing met sites (weather stations).
 */
CREATE TABLE met_sites (
	met_site_id integer NOT NULL,
	name text NOT NULL,
	geometry geometry(Point) NOT NULL,

	CONSTRAINT met_sites_pkey PRIMARY KEY (met_site_id)
);


/*
 * met_site_datasets
 * -----------------
 * Table containing datasets available for met sites, as well as associated (ADMS) properties for those files.
 */
CREATE TABLE met_site_datasets (
	met_site_id integer NOT NULL,
	dataset_type met_dataset_type NOT NULL,
	year text NOT NULL,
	roughness double precision NOT NULL,
	albedo double precision NOT NULL,
	priestly_taylor double precision NOT NULL,
	min_monin_obukhov_length double precision NOT NULL,
	wind_in_sectors boolean NOT NULL,
	remarks text,

	CONSTRAINT met_site_datasets_pkey PRIMARY KEY (met_site_id, dataset_type, year),
	CONSTRAINT met_site_datasets_fkey_met_sites FOREIGN KEY (met_site_id) REFERENCES met_sites
);
