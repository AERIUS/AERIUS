/*
 * met_dataset_type
 * ----------------
 * Enum specifying the types of datasets that Met files can be.
 */
CREATE TYPE met_dataset_type AS ENUM
  ('obs_raw_gt_90pct', 'obs_i_gt_75pct', 'obs_winds_gt_90pct', 'nwp_3km2');
