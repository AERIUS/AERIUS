-- Overrule the generation of the geometry_of_interests
-- Normal function is far to slow.....
CREATE OR REPLACE FUNCTION setup.ae_assessment_area_geometry_of_interest(v_assessment_area_id integer, v_land_geometry geometry)
	RETURNS geometry AS
$BODY$
DECLARE
BEGIN
	RETURN ST_Buffer((SELECT geometry FROM assessment_areas WHERE assessment_area_id = v_assessment_area_id), ae_constant('GEOMETRY_OF_INTEREST_BUFFER')::integer);
END;
$BODY$
LANGUAGE plpgsql STABLE;


-- Override setup.build_relevant_habitat_areas_view to ignore directive and process every area, as UK doesn't differentiate between habitat and species.
-- Also ignore design status, as that is not something used in UK, so no point in taken it into account
-- Supplied data contains false for both habitat_directive and bird_directive, but we still want to generate stuff
CREATE OR REPLACE VIEW setup.build_relevant_habitat_areas_view AS
WITH natura2000_directive_area_properties AS (
	SELECT
		natura2000_directive_area_id,
		natura2000_area_id AS assessment_area_id,
		geometry

		FROM natura2000_directive_areas
)
SELECT * FROM
	(SELECT
		assessment_area_id,
		habitat_area_id,
		habitat_type_id,
		coverage,
		ST_CollectionExtract(ST_Multi(ST_Union(ST_Intersection(natura2000_directive_area_geometry, habitat_area_geometry))), 3) AS geometry

		FROM
			-- Nitrogen-sensitive designated habitat within an area
			(SELECT
				assessment_area_id,
				habitat_area_id,
				habitat_type_id,
				natura2000_directive_area_id,
				coverage,
				natura2000_directive_area_properties.geometry AS natura2000_directive_area_geometry,
				habitat_areas.geometry AS habitat_area_geometry

				FROM habitat_areas
					INNER JOIN habitat_types USING (habitat_type_id)
					INNER JOIN habitat_type_sensitivity_view USING (habitat_type_id)
					INNER JOIN natura2000_directive_area_properties USING (assessment_area_id)
					INNER JOIN habitat_type_relations USING (habitat_type_id)
					LEFT JOIN habitat_properties USING (goal_habitat_type_id, assessment_area_id)
					LEFT JOIN designated_habitats_view USING (habitat_type_id, assessment_area_id)

				WHERE
					sensitive IS TRUE
					AND designated_habitats_view.habitat_type_id IS NOT NULL

		) AS relevant_habitats

		GROUP BY assessment_area_id, habitat_area_id, habitat_type_id, coverage
	) AS relevant_habitat_areas

	WHERE NOT ST_IsEmpty(geometry)
;
