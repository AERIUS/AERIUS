-- load
{import_common 'general/'}
{import_common 'sectors/data_24.sql'}

-- build
{import_common 'areas_hexagons_and_receptors/data/general_base_24.sql'}
{import_common 'areas_hexagons_and_receptors/build.sql'}

-- store
{import_common 'areas_hexagons_and_receptors/store_nature.sql'}
