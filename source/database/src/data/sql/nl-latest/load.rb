add_build_constants

cluster_tables

if has_build_flag :generate_nature then
  $logger.writeln "Load data, generate derived tables for nature and export generated data to export-folder..."
  run_sql "generate_nature.sql"

else

  run_sql "load_all.sql"

  synchronize_serials

  $do_run_unit_tests = true unless has_build_flag :no_unittest
  $do_validate_contents = true if has_build_flag :validate
  $do_create_summary = true if has_build_flag :summary

end
