{import_common 'general/'}

{import_common 'sectors/'}

{import_common 'areas_hexagons_and_receptors/load.sql'}

{import_common 'met_sites/load.sql'}

{import_common 'calculator/'}

{import_common 'receptor_backgrounds/'}

{import_common 'background_cells/'}

{import_common 'emission_factors/'}

{import_common 'time_varying_profiles/'}

{import_common 'calculation_categories/'}

{import_common 'decision_thresholds/'}

{import_common 'system/'}

--i18n folder within uk_latest uses some import_common i18n itself (targetted at common) at the moment. For now loading those in load.rb
--import_common 'i18n/'--
