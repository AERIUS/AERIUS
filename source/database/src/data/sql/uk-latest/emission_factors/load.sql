--TODO: fill based on data supplied by UK partners

/* Road data */
BEGIN; SELECT setup.ae_load_table('road_area_categories', '{data_folder}/UK/public/road_area_categories_20220419.txt', true); COMMIT;
BEGIN; SELECT setup.ae_load_table('road_type_categories', '{data_folder}/UK/public/road_type_categories_20220419.txt', true); COMMIT;
BEGIN; SELECT setup.ae_load_table('road_vehicle_categories', '{data_folder}/UK/public/road_vehicle_categories_20220419.txt', true); COMMIT;
BEGIN; SELECT setup.ae_load_table('road_speed_profiles', '{data_folder}/UK/public/road_speed_profiles_20220419.txt', true); COMMIT;
BEGIN; SELECT setup.ae_load_table('road_areas_to_road_types', '{data_folder}/UK/public/road_areas_to_road_types_20220419.txt', true); COMMIT;
BEGIN; SELECT setup.ae_load_table('road_types_to_speed_profiles', '{data_folder}/UK/public/road_types_to_speed_profiles_20220419.txt', true); COMMIT;
BEGIN; SELECT setup.ae_load_table('road_categories', '{data_folder}/UK/public/road_categories_20220419.txt', true); COMMIT;
BEGIN; SELECT setup.ae_load_table('road_category_emission_factors', '{data_folder}/UK/public/road_category_emission_factors_20220419.txt', true); COMMIT;

--mobile sources on road (EURO) not part of MVP
--BEGIN; SELECT setup.ae_load_table('mobile_source_on_road_categories', '{data_folder}/public/mobile_source_on_road_categories_20210618.txt'); COMMIT;
--BEGIN; SELECT setup.ae_load_table('mobile_source_on_road_category_emission_factors', '{data_folder}/public/mobile_source_on_road_category_emission_factors_TEMP2035_20210630.txt'); COMMIT;

/* Farm animal category and -lodging data */
--TODO: remove old farm lodging stuff.
--Data supplied so far for UK
BEGIN; SELECT setup.ae_load_table('farm_animal_categories_deprecated', '{data_folder}/UK/public/farm_animal_categories_20221031.txt', TRUE); COMMIT;

--Not available/supplied (unknown for UK)
--BEGIN; SELECT setup.ae_load_table('farm_lodging_system_definitions', '{data_folder}/UK/public/farm_lodging_system_definitions_20161107.txt'); COMMIT;
--BEGIN; SELECT setup.ae_load_table('farm_lodging_types', '{data_folder}/UK/public/farm_lodging_types_20221031.txt', TRUE); COMMIT;
--BEGIN; SELECT setup.ae_load_table('farm_lodging_type_emission_factors', '{data_folder}/UK/public/farm_lodging_type_emission_factor_20221031.txt', TRUE); COMMIT;

--New animal housing approach
BEGIN; SELECT setup.ae_load_table('farm_animal_categories', '{data_folder}/UK/public/farm_animal_categories_20240515.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_animal_housing_categories', '{data_folder}/UK/public/farm_animal_housing_categories_20240515.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_housing_emission_factors', '{data_folder}/UK/public/farm_housing_emission_factors_20240515.txt'); COMMIT;

--Not available/supplied (unknown for UK)
--BEGIN; SELECT setup.ae_load_table('farm_lodging_types_other_lodging_type', '{data_folder}/UK/public/farm_lodging_types_other_lodging_type_20161107.txt'); COMMIT;
--BEGIN; SELECT setup.ae_load_table('farm_lodging_types_to_lodging_system_definitions', '{data_folder}/UK/public/farm_lodging_types_to_lodging_system_definitions_20161107.txt'); COMMIT;

--Not available/supplied (unknown for UK)
--BEGIN; SELECT setup.ae_load_table('farm_additional_lodging_systems', '{data_folder}/UK/public/farm_additional_lodging_systems_20161107.txt'); COMMIT;
--BEGIN; SELECT setup.ae_load_table('farm_additional_lodging_system_emission_factors', '{data_folder}/UK/public/farm_additional_lodging_system_emission_factors_20161107.txt'); COMMIT;
--BEGIN; SELECT setup.ae_load_table('farm_additional_lodging_systems_to_lodging_system_definitions', '{data_folder}/UK/public/farm_additional_lodging_systems_to_lodging_system_definitions_20161107.txt'); COMMIT;
--BEGIN; SELECT setup.ae_load_table('farm_lodging_types_to_additional_lodging_systems', '{data_folder}/UK/public/farm_lodging_types_to_additional_lodging_systems_20161107.txt'); COMMIT;

--While previously used, with SCAIL data we no longer have reductive systems.
--BEGIN; SELECT setup.ae_load_table('farm_reductive_lodging_systems', '{data_folder}/UK/public/farm_reductive_lodging_systems_20220315.txt', TRUE); COMMIT;
--BEGIN; SELECT setup.ae_load_table('farm_reductive_lodging_system_reduction_factors', '{data_folder}/UK/public/farm_reductive_lodging_system_reduction_factors_20220315.txt', TRUE); COMMIT;
--BEGIN; SELECT setup.ae_load_table('farm_lodging_types_to_reductive_lodging_systems', '{data_folder}/UK/public/farm_lodging_types_to_reductive_lodging_systems_20220315.txt', TRUE); COMMIT;

--Not available/supplied (unknown for UK)
--BEGIN; SELECT setup.ae_load_table('farm_reductive_lodging_systems_to_lodging_system_definitions', '{data_folder}/UK/public/farm_reductive_lodging_systems_to_lodging_system_definitions_20161107.txt'); COMMIT;

--Not available/supplied (unknown for UK)
--BEGIN; SELECT setup.ae_load_table('farm_lodging_fodder_measures', '{data_folder}/UK/public/farm_lodging_fodder_measures_20161107.txt'); COMMIT;
--BEGIN; SELECT setup.ae_load_table('farm_lodging_fodder_measure_reduction_factors', '{data_folder}/UK/public/farm_lodging_fodder_measure_reduction_factors_20161107.txt'); COMMIT;
--BEGIN; SELECT setup.ae_load_table('farm_lodging_fodder_measures_animal_category', '{data_folder}/UK/public/farm_lodging_fodder_measures_animal_category_20161107.txt'); COMMIT;

/* Farmland categories */
BEGIN; SELECT setup.ae_load_table('farmland_categories', '{data_folder}/UK/public/farmland_categories_20221019.txt'); COMMIT;

/* Farm source categories */
BEGIN; SELECT setup.ae_load_table('farm_source_categories', '{data_folder}/UK/public/farm_source_categories_20221031.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_source_emission_factors', '{data_folder}/UK/public/farm_source_emission_factors_20221031.txt', TRUE); COMMIT;

--Other categories: mobile_source_off_road, shipping_maritime and shipping_inland
--Not using these (yet). Shipping requires something 
