BEGIN; SELECT setup.ae_load_table('background_years', '{data_folder}/UK/public/background_years_20230220.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('target_background_years', '{data_folder}/UK/public/target_background_years_20230220.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('receptor_background_results', '{data_folder}/UK/public/receptor_background_results_20230222.txt', TRUE); COMMIT;
