-- Permit areas for calculation settings
INSERT INTO calculation_permit_areas
	(calculation_permit_area_id, code, name)
VALUES
	(1, 'ENGLAND', 'England'),
	(2, 'WALES', 'Wales'),
	(3, 'SCOTLAND', 'Scotland'),
	(4, 'NORTHERN_IRELAND', 'Northern Ireland');

-- Project categories for calculation settings
INSERT INTO calculation_project_categories
	(calculation_project_category_id, code, name)
VALUES
	(1, 'AGRICULTURE', 'Agriculture'),
	(2, 'COMBUSTION_PLANT', 'Combustion plant'),
	(3, 'ROADS', 'Roads'),
	(4, 'OTHER', 'Other');

-- Default distances for project categories, for now no distinction between permit areas.
INSERT INTO calculation_default_distances
	(calculation_permit_area_id, calculation_project_category_id, default_calculation_distance)
SELECT 
	calculation_permit_area_id,
	calculation_project_category_id,
	7500 AS default_distance

	FROM calculation_permit_areas
	CROSS JOIN calculation_project_categories
	WHERE calculation_project_categories.code IN ('AGRICULTURE');

INSERT INTO calculation_default_distances
	(calculation_permit_area_id, calculation_project_category_id, default_calculation_distance)
SELECT 
	calculation_permit_area_id,
	calculation_project_category_id,
	15000 AS default_distance

	FROM calculation_permit_areas
	CROSS JOIN calculation_project_categories
	WHERE calculation_project_categories.code IN ('COMBUSTION_PLANT', 'OTHER');

INSERT INTO calculation_default_distances
	(calculation_permit_area_id, calculation_project_category_id, default_calculation_distance)
SELECT 
	calculation_permit_area_id,
	calculation_project_category_id,
	200 AS default_distance

	FROM calculation_permit_areas
	CROSS JOIN calculation_project_categories
	WHERE calculation_project_categories.code IN ('ROADS');
