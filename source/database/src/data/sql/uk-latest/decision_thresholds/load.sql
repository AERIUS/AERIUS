-- Initial pressure classes.
INSERT INTO development_pressure_classes (development_pressure_class_id, code, description, nearby_projects_range)
VALUES
	(1, 'VERY_LOW', 'Very Low', '[0,1]'),
	(2, 'LOW', 'Low', '[2,5]'),
	(3, 'MEDIUM', 'Medium', '[6,10]'),
	(4, 'HIGH', 'High', '[11,)');

-- Files containing thresholds for Northern Ireland
BEGIN; SELECT setup.ae_load_table('receptor_decision_making_thresholds', '{data_folder}/UK/public/receptor_decision_making_thresholds_20241104.txt', true); COMMIT;
BEGIN; SELECT setup.ae_load_table('receptor_site_relevant_thresholds', '{data_folder}/UK/public/receptor_site_relevant_thresholds_20241104.txt', true); COMMIT;

-- To have DMT and (more importantly) archive in-combination working, inserting thresholds for applicable receptors for other countries
WITH receptors_without_threshold AS (
SELECT DISTINCT receptor_id

	FROM natura2000_areas
		INNER JOIN authorities USING (authority_id)
		INNER JOIN receptors_to_critical_deposition_areas USING (assessment_area_id)
		INNER JOIN receptors USING (receptor_id)
		LEFT JOIN receptor_decision_making_thresholds USING (receptor_id)

	WHERE authorities.code NOT ilike 'N%'
		AND receptor_decision_making_thresholds.receptor_id IS NULL
)
INSERT INTO receptor_decision_making_thresholds (receptor_id, emission_result_type, substance_id, threshold)
SELECT
	receptor_id,
	unnest(array['concentration', 'concentration', 'deposition']::emission_result_type[]) AS emission_result_type,
	unnest(array[11, 17, 1711]) AS substance_id,
	0 AS threshold

	FROM receptors_without_threshold;


WITH receptors_without_threshold AS (
SELECT DISTINCT receptor_id, assessment_area_id

	FROM natura2000_areas
		INNER JOIN authorities USING (authority_id)
		INNER JOIN receptors_to_critical_deposition_areas USING (assessment_area_id)
		INNER JOIN receptors USING (receptor_id)
		LEFT JOIN receptor_site_relevant_thresholds USING (receptor_id, assessment_area_id)

	WHERE authorities.code NOT ilike 'N%'
		AND receptor_site_relevant_thresholds.receptor_id IS NULL
)
INSERT INTO receptor_site_relevant_thresholds (development_pressure_class_id, receptor_id, assessment_area_id, emission_result_type, substance_id, threshold)
SELECT
	development_pressure_class_id,
	receptor_id,
	assessment_area_id,
	unnest(array['concentration', 'concentration', 'deposition']::emission_result_type[]) AS emission_result_type,
	unnest(array[11, 17, 1711]) AS substance_id,
	0 AS threshold

	FROM receptors_without_threshold
	CROSS JOIN development_pressure_classes;
