BEGIN; SELECT setup.ae_load_table('standard_time_varying_profiles', '{data_folder}/UK/public/standard_diurnal_variation_profiles_20240212.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('standard_time_varying_profile_values', '{data_folder}/UK/public/standard_diurnal_variation_profile_values_20240212.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('sector_default_time_varying_profiles', '{data_folder}/UK/public/sector_default_diurnal_variation_profiles_20240212.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('sector_year_default_time_varying_profiles', '{data_folder}/UK/public/sector_year_default_diurnal_variation_profiles_20240212.txt', TRUE); COMMIT;
