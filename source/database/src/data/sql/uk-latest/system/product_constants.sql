/*
 * Product constants, which can be different between products, but the same across versions.
 */

INSERT INTO system.constants (key, value) VALUES ('DEFAULT_LOCALE', 'en');

-- Release (valid values: PRODUCTION, CONCEPT, DEPRECATED)
INSERT INTO system.constants (key, value) VALUES ('RELEASE', 'CONCEPT');

-- Calculation config for workers
INSERT INTO system.constants (key, value) VALUES ('CHUNKER_ENGINE_UNITS_WORKER_MAX', '100000');
INSERT INTO system.constants (key, value) VALUES ('CHUNKER_RECEPTORS_WORKER_MIN', '1000');
INSERT INTO system.constants (key, value) VALUES ('CHUNKER_RECEPTORS_WORKER_MAX', '1500');

-- Calculation config for Quick Run
INSERT INTO system.constants (key, value) VALUES ('CHUNKER_ENGINE_UNITS_QUICKRUN_MAX', '500000');
INSERT INTO system.constants (key, value) VALUES ('CHUNKER_RECEPTORS_QUICKRUN_MIN', '1000');
INSERT INTO system.constants (key, value) VALUES ('CHUNKER_RECEPTORS_QUICKRUN_MAX', '3000');

-- Calculation config for the UI
INSERT INTO system.constants (key, value) VALUES ('CHUNKER_ENGINE_UNITS_UI_MAX', '20000');
INSERT INTO system.constants (key, value) VALUES ('CHUNKER_RECEPTORS_UI_MIN', '1000');
INSERT INTO system.constants (key, value) VALUES ('CHUNKER_RECEPTORS_UI_MAX', '1500');
INSERT INTO system.constants (key, value) VALUES ('MAX_ENGINE_SOURCES_UI', '1000000');

-- Emission result display settings
INSERT INTO system.constants (key, value) VALUES ('EMISSION_RESULT_DISPLAY_CONVERSION_FACTOR', 1.0);
INSERT INTO system.constants (key, value) VALUES ('EMISSION_RESULT_DISPLAY_UNIT', 'KILOGRAM_UNITS'); -- Option of: MOLAR_UNITS, KILOGRAM_UNITS
INSERT INTO system.constants (key, value) VALUES ('EMISSION_RESULT_DISPLAY_ROUNDING_LENGTH', 3);
INSERT INTO system.constants (key, value) VALUES ('EMISSION_RESULT_DISPLAY_PRECISE_ROUNDING_LENGTH', 4);
INSERT INTO system.constants (key, value) VALUES ('EMISSION_RESULT_DISPLAY_DEPOSITION_SUM_ROUNDING_LENGTH', 0);

-- External URLs
INSERT INTO system.constants (key, value) VALUES ('RELEASE_DETAILS_URL', 'https://aerius.uk/');
INSERT INTO system.constants (key, value) VALUES ('REGISTER_BASE_URL', 'https://register.aerius.uk');
INSERT INTO system.constants (key, value) VALUES ('MANUAL_BASE_URL', 'https://docs.aerius.uk');

-- Calculator min/max years (used for selection-range for user).
INSERT INTO system.constants (key, value) VALUES ('MIN_YEAR', '2018');
INSERT INTO system.constants (key, value) VALUES ('MAX_YEAR', '2030');

-- Google Tag Manager constants
INSERT INTO system.constants (key, value) VALUES ('GOOGLE_TAG_MANAGER_ENABLE', 'true');
INSERT INTO system.constants (key, value) VALUES ('GOOGLE_TAG_MANAGER_CONTAINER_ID', 'GTM-MH7MKRX');

-- Driving side constants
INSERT INTO system.constants (key, value) VALUES ('DRIVING_SIDE', 'LEFT');

-- Characteristics type constants
INSERT INTO system.constants (key, value) VALUES ('CHARACTERISTICS_TYPE', 'ADMS');

-- Default netting factor
INSERT INTO system.constants (key, value) VALUES ('DEFAULT_NETTING_FACTOR', '0.3');

-- Default calculation point placement radius
INSERT INTO system.constants (key, value) VALUES ('DEFAULT_CALCULATION_POINTS_PLACEMENT_RADIUS', '15000');

-- Collect feedback constants
-- if COLLECT_FEEDBACK is set to false, and a real url is set on FEEDBACK url,
-- clicking on the link in the banner will than open the page, instead of opening the JIRA form.
-- This only works for the UK version, since COLLECT_FEEDBACK_URL is only used in the UK.
INSERT INTO system.constants (key, value) VALUES ('COLLECT_FEEDBACK', 'true');
INSERT INTO system.constants (key, value) VALUES ('COLLECT_COLLECTOR_ID', 'e5e3516c');
INSERT INTO system.constants (key, value) VALUES ('COLLECT_FEEDBACK_URL', 'https://aerius.atlassian.net/rest/collectors/1.0/template/form/');

-- Easter
INSERT INTO system.constants (key, value) VALUES ('EASTER_MODULE_ENABLE', 'true');
