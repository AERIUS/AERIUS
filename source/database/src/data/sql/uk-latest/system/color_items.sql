INSERT INTO system.color_items
	(color_item_type, item_value, color, line_width, sort_order)
VALUES
	('road_type', 'Mot', 'ec1c24', 8, 1),
	('road_type', 'Rur', '009245', 6, 2),
	('road_type', 'Urb', '0073b7', 4, 3),
	('road_type', 'Lon2', '431c53', 4, 4),
	('road_type', 'Lon1', '732c7C', 4, 5),
	('road_type', 'Lon3', '937ca0', 4, 6),
	('road_type', 'Lon4', 'cf7aac', 6, 7);

INSERT INTO system.color_items
	(color_item_type, item_value, color, line_width, sort_order)
VALUES
	('road_barrier_type', 'NOISE_BARRIER', '0071bc', 4, 1),
	('road_barrier_type', 'BRICK_WALL', 'be1719', 4, 2),
	('road_barrier_type', 'STREET_CANYON_TERRACED_HOUSES', 'cf7aac', 4, 3),
	('road_barrier_type', 'STREET_CANYON_SEMIDETACHED_HOUSES', '937ca0', 4, 4),
	('road_barrier_type', 'STREET_CANYON_DETACHED_HOUSES', '732c7c', 4, 5),
	('road_barrier_type', 'TREE_BARRIER_DENSE', '009245',4, 6),
	('road_barrier_type', 'TREE_BARRIER_OPEN', 'd9e021', 4, 7),
	('road_barrier_type', 'OTHER', '999999', 4, 8);

INSERT INTO system.color_items
	(color_item_type, item_value, color, line_width, sort_order)
VALUES
	('natura2000_directive', 'SAC', 'f4e798', 0, 1),
	('natura2000_directive', 'SPA', 'bbddea', 0, 2),
	('natura2000_directive', 'SSSI_ASSI', 'cfe2a1', 0, 3),
	('natura2000_directive', 'unassigned', 'd6b9d2', 0, 4);
