INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('contribution_deposition', 0, 'fffdb3'),
	('contribution_deposition', 0.25, 'fde76a'),
	('contribution_deposition', 0.5, 'feb66e'),
	('contribution_deposition', 1, 'a5cc46'),
	('contribution_deposition', 1.5, '23a870'),
	('contribution_deposition', 2, '5a7a32'),
	('contribution_deposition', 3, '0093bd'),
	('contribution_deposition', 4, '0d75b5'),
	('contribution_deposition', 5, '6a70b1'),
	('contribution_deposition', 10, '304594'),
	('contribution_deposition', 15, '5e2c8f'),
	('contribution_deposition', 20, '3f2a84'),
	('contribution_deposition', 25, '2a1612');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('difference_deposition', '-infinity'::real, '507122'),
	('difference_deposition', -25, '507122'),
	('difference_deposition', -16, '738d4e'),
	('difference_deposition', -8, '96aa7a'),
	('difference_deposition', -4, 'b9c6a7'),
	('difference_deposition', -2, 'dce3d3'),
	('difference_deposition', 0, 'd8d3e5'),
	('difference_deposition', 2, 'b1a9cb'),
	('difference_deposition', 4, '8b7db0'),
	('difference_deposition', 8, '645296'),
	('difference_deposition', 16, '3d277c'),
	('difference_deposition', 25, '3d277c');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('in_combination_deposition', '-infinity'::real, '507122'),
	('in_combination_deposition', -30, '507122'),
	('in_combination_deposition', -20, '738d4e'),
	('in_combination_deposition', -15, '96aa7a'),
	('in_combination_deposition', -10, 'b9c6a7'),
	('in_combination_deposition', -5, 'dce3d3'),
	('in_combination_deposition', 0, 'd8d3e5'),
	('in_combination_deposition', 5, 'b1a9cb'),
	('in_combination_deposition', 10, '8b7db0'),
	('in_combination_deposition', 15, '645296'),
	('in_combination_deposition', 20, '3d277c'),
	('in_combination_deposition', 30, '3d277c');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('project_calculation_percentage_critical_load', '-infinity'::real, '7c9459'),
	('project_calculation_percentage_critical_load', 0, 'b0bf9c'),
	('project_calculation_percentage_critical_load', 0.1, 'dce3d3'),
	('project_calculation_percentage_critical_load', 1, 'fcc5c0'),
	('project_calculation_percentage_critical_load', 4, 'f768a1'),
	('project_calculation_percentage_critical_load', 10, 'dd3497'),
	('project_calculation_percentage_critical_load', 20, 'ae017e');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('in_combination_percentage_critical_load', '-infinity'::real, '7c9459'),
	('in_combination_percentage_critical_load', 0, 'b0bf9c'),
	('in_combination_percentage_critical_load', 0.1, 'dce3d3'),
	('in_combination_percentage_critical_load', 1, 'fbc1f2'),
	('in_combination_percentage_critical_load', 4, 'c668f6'),
	('in_combination_percentage_critical_load', 10, '8434dc'),
	('in_combination_percentage_critical_load', 20, '3902ad');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('archive_contribution_percentage_critical_load', '-infinity'::real, '7c9459'),
	('archive_contribution_percentage_critical_load', 0, 'b0bf9c'),
	('archive_contribution_percentage_critical_load', 0.1, 'dce3d3'),
	('archive_contribution_percentage_critical_load', 1, 'fbc1f2'),
	('archive_contribution_percentage_critical_load', 4, 'c668f6'),
	('archive_contribution_percentage_critical_load', 10, '8434dc'),
	('archive_contribution_percentage_critical_load', 20, '3902ad');

INSERT INTO system.color_ranges
  (color_range_type, lower_value, color)
VALUES
  ('max_total_as_percentage_critical_load', 80, 'afb8b9'),
  ('max_total_as_percentage_critical_load', 100, '4d4d4d');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('contribution_concentration_nox', 0, 'fffdb3'),
	('contribution_concentration_nox', 0.2, 'fde76a'),
	('contribution_concentration_nox', 0.4, 'feb66e'),
	('contribution_concentration_nox', 0.6, 'a5cc46'),
	('contribution_concentration_nox', 0.8, '23a870'),
	('contribution_concentration_nox', 1, '5a7a32'),
	('contribution_concentration_nox', 1.5, '0093bd'),
	('contribution_concentration_nox', 2, '0d75b5'),
	('contribution_concentration_nox', 4, '6a70b1'),
	('contribution_concentration_nox', 6, '304594'),
	('contribution_concentration_nox', 8, '5e2c8f'),
	('contribution_concentration_nox', 10, '3f2a84'),
	('contribution_concentration_nox', 15, '2a1612');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('difference_concentration_nox', '-infinity'::real, '507122'),
	('difference_concentration_nox', -15, '507122'),
	('difference_concentration_nox', -10, '738d4e'),
	('difference_concentration_nox', -6, '96aa7a'),
	('difference_concentration_nox', -4, 'b9c6a7'),
	('difference_concentration_nox', -2, 'dce3d3'),
	('difference_concentration_nox', 0, 'd8d3e5'),
	('difference_concentration_nox', 2, 'b1a9cb'),
	('difference_concentration_nox', 4, '8b7db0'),
	('difference_concentration_nox', 6, '645296'),
	('difference_concentration_nox', 10, '3d277c'),
	('difference_concentration_nox', 15, '3d277c');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('in_combination_concentration_nox', '-infinity'::real, '507122'),
	('in_combination_concentration_nox', -30, '507122'),
	('in_combination_concentration_nox', -20, '738d4e'),
	('in_combination_concentration_nox', -15, '96aa7a'),
	('in_combination_concentration_nox', -10, 'b9c6a7'),
	('in_combination_concentration_nox', -5, 'dce3d3'),
	('in_combination_concentration_nox', 0, 'd8d3e5'),
	('in_combination_concentration_nox', 5, 'b1a9cb'),
	('in_combination_concentration_nox', 10, '8b7db0'),
	('in_combination_concentration_nox', 15, '645296'),
	('in_combination_concentration_nox', 20, '3d277c'),
	('in_combination_concentration_nox', 30, '3d277c');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('contribution_concentration_nh3', 0, 'fffdb3'),
	('contribution_concentration_nh3', 0.1, 'fde76a'),
	('contribution_concentration_nh3', 0.2, 'feb66e'),
	('contribution_concentration_nh3', 0.4, 'a5cc46'),
	('contribution_concentration_nh3', 0.6, '23a870'),
	('contribution_concentration_nh3', 0.8, '5a7a32'),
	('contribution_concentration_nh3', 1, '0093bd'),
	('contribution_concentration_nh3', 1.5, '0d75b5'),
	('contribution_concentration_nh3', 2, '6a70b1'),
	('contribution_concentration_nh3', 4, '304594'),
	('contribution_concentration_nh3', 6, '5e2c8f'),
	('contribution_concentration_nh3', 8, '3f2a84'),
	('contribution_concentration_nh3', 10, '2a1612');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('difference_concentration_nh3', '-infinity'::real, '507122'),
	('difference_concentration_nh3', -10, '507122'),
	('difference_concentration_nh3', -8, '738d4e'),
	('difference_concentration_nh3', -6, '96aa7a'),
	('difference_concentration_nh3', -4, 'b9c6a7'),
	('difference_concentration_nh3', -2, 'dce3d3'),
	('difference_concentration_nh3', 0, 'd8d3e5'),
	('difference_concentration_nh3', 2, 'b1a9cb'),
	('difference_concentration_nh3', 4, '8b7db0'),
	('difference_concentration_nh3', 6, '645296'),
	('difference_concentration_nh3', 8, '3d277c'),
	('difference_concentration_nh3', 10, '3d277c');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('in_combination_concentration_nh3', '-infinity'::real, '507122'),
	('in_combination_concentration_nh3', -20, '507122'),
	('in_combination_concentration_nh3', -16, '738d4e'),
	('in_combination_concentration_nh3', -12, '96aa7a'),
	('in_combination_concentration_nh3', -8, 'b9c6a7'),
	('in_combination_concentration_nh3', -4, 'dce3d3'),
	('in_combination_concentration_nh3', 0, 'd8d3e5'),
	('in_combination_concentration_nh3', 4, 'b1a9cb'),
	('in_combination_concentration_nh3', 8, '8b7db0'),
	('in_combination_concentration_nh3', 12, '645296'),
	('in_combination_concentration_nh3', 16, '3d277c'),
	('in_combination_concentration_nh3', 20, '3d277c');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('background_deposition', 0, 'FFFFD4'),
	('background_deposition', 10, 'FEE391'),
	('background_deposition', 14, 'FEC44F'),
	('background_deposition', 18, 'FE9929'),
	('background_deposition', 22, 'EC7014'),
	('background_deposition', 28, 'CC4C02'),
	('background_deposition', 32, '8C2D04');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('background_concentration_nox', 0, 'fffdb3'),
	('background_concentration_nox', 21, 'fde76a'),
	('background_concentration_nox', 24, 'feb66e'),
	('background_concentration_nox', 27, 'a5cc46'),
	('background_concentration_nox', 30, '23a870'),
	('background_concentration_nox', 33, '5a7a32'),
	('background_concentration_nox', 36, '0c75b5'),
	('background_concentration_nox', 39, '304594');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('background_concentration_nh3', 0, 'fffdb3'),
	('background_concentration_nh3', 2.4, 'fde76a'),
	('background_concentration_nh3', 2.6, 'feb66e'),
	('background_concentration_nh3', 2.8, 'a5cc46'),
	('background_concentration_nh3', 3, '23a870'),
	('background_concentration_nh3', 3.2, '5a7a32'),
	('background_concentration_nh3', 3.4, '0c75b5'),
	('background_concentration_nh3', 3.6, '304594');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('road_max_speed', 0, '707070'),
	('road_max_speed', 5, '50ACD1'),
	('road_max_speed', 50, 'D9E021'),
	('road_max_speed', 95, 'F7931E'),
	('road_max_speed', 140, 'EC1C24');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('road_barrier_height', 0, 'eace00'),
	('road_barrier_height', 3, 'f7931e'),
	('road_barrier_height', 6, 'ec1c24');

INSERT INTO system.color_ranges
(color_range_type, lower_value, color)
VALUES
	('road_barrier_porosity', 0, '7a0177'),
	('road_barrier_porosity', 5, 'ae017e'),
	('road_barrier_porosity', 20, 'dd3497'),
	('road_barrier_porosity', 40, 'f768a1'),
	('road_barrier_porosity', 50, 'fa9fb5'),
	('road_barrier_porosity', 80, 'fcc5c0'),
	('road_barrier_porosity', 95, 'feebe2');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('road_emission_nox', 0, 'cde0ea'),
	('road_emission_nox', 0.1, '9bc2d5'),
	('road_emission_nox', 0.2, '6aa3c0'),
	('road_emission_nox', 0.3, '3885ab'),
	('road_emission_nox', 0.4, '066696');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('road_emission_nh3', 0, 'e1d2d0'),
	('road_emission_nh3', 0.01, 'c3a6a1'),
	('road_emission_nh3', 0.02, 'a47972'),
	('road_emission_nh3', 0.03, '864d43'),
	('road_emission_nh3', 0.04, '682014');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('road_traffic_volume', 0, '095da7'),
	('road_traffic_volume', 40000, '095da7'),
	('road_traffic_volume', 80000, '095da7'),
	('road_traffic_volume', 120000, '095da7'),
	('road_traffic_volume', 160000, '095da7');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('road_traffic_volume_car', 0, '095da7'),
	('road_traffic_volume_car', 30000, '095da7'),
	('road_traffic_volume_car', 60000, '095da7'),
	('road_traffic_volume_car', 90000, '095da7'),
	('road_traffic_volume_car', 120000, '095da7');
INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('road_traffic_volume_tax', 0, '095da7'),
	('road_traffic_volume_tax', 250, '095da7'),
	('road_traffic_volume_tax', 500, '095da7'),
	('road_traffic_volume_tax', 750, '095da7'),
	('road_traffic_volume_tax', 1000, '095da7');
INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('road_traffic_volume_lgv', 0, '095da7'),
	('road_traffic_volume_lgv', 5000, '095da7'),
	('road_traffic_volume_lgv', 10000, '095da7'),
	('road_traffic_volume_lgv', 15000, '095da7'),
	('road_traffic_volume_lgv', 20000, '095da7');
INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('road_traffic_volume_hgv', 0, '095da7'),
	('road_traffic_volume_hgv', 3000, '095da7'),
	('road_traffic_volume_hgv', 6000, '095da7'),
	('road_traffic_volume_hgv', 9000, '095da7'),
	('road_traffic_volume_hgv', 12000, '095da7');
INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('road_traffic_volume_bus', 0, '095da7'),
	('road_traffic_volume_bus', 500, '095da7'),
	('road_traffic_volume_bus', 1000, '095da7'),
	('road_traffic_volume_bus', 1500, '095da7'),
	('road_traffic_volume_bus', 2000, '095da7');
INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('road_traffic_volume_mot', 0, '095da7'),
	('road_traffic_volume_mot', 250, '095da7'),
	('road_traffic_volume_mot', 500, '095da7'),
	('road_traffic_volume_mot', 750, '095da7'),
	('road_traffic_volume_mot', 1000, '095da7');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('habitat_areas_sensitivity_level_deposition', 3, 'E4BDFF'),
	('habitat_areas_sensitivity_level_deposition', 5, 'DE87FF'),
	('habitat_areas_sensitivity_level_deposition', 8, 'A455FF'),
	('habitat_areas_sensitivity_level_deposition', 10, '681DFF'),
	('habitat_areas_sensitivity_level_deposition', 15, '1001CA'),
	('habitat_areas_sensitivity_level_deposition', 20, '080098');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('habitat_areas_sensitivity_level_concentration_nox', 30, '066797');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('habitat_areas_sensitivity_level_concentration_nh3', 1, '96645C'),
	('habitat_areas_sensitivity_level_concentration_nh3', 3, '682014');
