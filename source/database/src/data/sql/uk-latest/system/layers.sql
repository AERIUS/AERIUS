--layer legends
INSERT INTO system.layer_legends (layer_legend_id, legend_type, description) VALUES
	(1, 'circle', 'Nature areas'),
	(3, 'circle', 'Nitrogen sensitivity habitat types');

--layer legend items
INSERT INTO system.layer_legend_color_items (layer_legend_id, sort_order, name, color) VALUES
	(1, 1, 'Habitat directive', 'F4E798'),
	(1, 2, 'Bird directive', 'BBDDEA'),
	(1, 3, 'Bird directive, Habitat directive', 'CFE2A1'),
	(1, 4, 'Unassigned', 'D6B9D2'),

	(3, 1, 'Highly sensitive', '7B3294'),
	(3, 2, 'Sensitive', 'C2A5CF'),
	(3, 3, 'Less/not sensitive', 'EBF5C8');

-- Layer bundles
INSERT INTO system.layer_bundles(bundle_id, bundle_name)
	VALUES (1, 'Background layer');

INSERT INTO system.wmts_layer_properties (layer_properties_id, title, opacity, min_scale, max_scale, url, image_type, service_version, tile_matrix_set, projection, attribution, bundle_id) VALUES
	(5, 'OSNI - Discoverer 50K', 1.0, 18800, null, 'https://maps.aerius.uk/OSNI_Discoverer_50K/MapServer/WMTS', 'image/jpgpng', '1.0.0', 'default028mm', 'EPSG:29902', 'Land & Property Services &copy; Crown Copyright Licence No. NIMA S&LA577.2', 1);
INSERT INTO system.wmts_layer_properties (layer_properties_id, title, opacity, min_scale, max_scale, url, image_type, service_version, tile_matrix_set, projection, attribution, bundle_id) VALUES
	(6, 'OSNI - Fusion', 1.0, null, null, 'https://maps.aerius.uk/OSNIFusionBasemap/MapServer/WMTS', 'image/png', '1.0.0', 'default028mm', 'EPSG:29902', 'Land & Property Services &copy; Crown Copyright Licence No. NIMA S&LA577.2', 1);

--wms layers (for now ID's 10+)
--default WMS layer properties
INSERT INTO system.wms_layer_properties (layer_properties_id, opacity, layer_legend_id, min_scale, max_scale, url)
	VALUES (10, 1.0, null, 12800000, null, '[AERIUS_GEOSERVER_HOST]');
--wms_nature_areas_view
INSERT INTO system.wms_layer_properties (layer_properties_id, title, opacity, layer_legend_id, min_scale, max_scale, url)
	VALUES (11, 'Special areas of conservation', 0.8, 1, 12800000, null, '[AERIUS_GEOSERVER_HOST]');
--wms_habitat_areas_sensitivity_view
INSERT INTO system.wms_layer_properties (layer_properties_id, title, opacity, layer_legend_id, min_scale, max_scale, url)
	VALUES (13, 'Nitrogen sensitive habitat types', 0.8, 3, 12800000, null, '[AERIUS_GEOSERVER_HOST]');
--wms_habitat_types
INSERT INTO system.wms_layer_properties (layer_properties_id, title, opacity, layer_legend_id, min_scale, max_scale, url, dynamic_type)
	VALUES (14, 'Habitat types', 1.0, null, 12800000, null, '[AERIUS_GEOSERVER_HOST]', 'habitat_type');
-- wms_province_areas_view (not product specific)
INSERT INTO system.wms_layer_properties (layer_properties_id, opacity, layer_legend_id, min_scale, max_scale, url)
	VALUES (28, 0.8, null, null, null, '[AERIUS_GEOSERVER_HOST]');

-- fall-back for when Bing-maps not available.
--INSERT INTO system.wms_layer_properties (layer_properties_id, title, attribution, opacity, layer_legend_id, min_scale, max_scale, url, tile_size)
--  VALUES (36, 'Base Layer', '&copy Crown copyright and database rights 2013 Ordnance Survey [100017955]', 0.8, null, null, null, 'https://test.aerius.nl/ukbars/map?', 250);

INSERT INTO system.bing_layer_properties (layer_properties_id, title, attribution, opacity, layer_legend_id, min_scale, max_scale, api_key, bundle_id)
	VALUES (36, 'Base Layer', '&copy Microsoft Bing', 1, null, null, null, 'BING_API_KEY', 1);
INSERT INTO system.bing_layer_properties (layer_properties_id, title, attribution, opacity, layer_legend_id, min_scale, max_scale, api_key, bundle_id)
	VALUES (37, 'Base Layer (Aerial)', '&copy Microsoft Bing', 1, null, null, null, 'BING_API_KEY', 1);
INSERT INTO system.bing_layer_properties (layer_properties_id, title, attribution, opacity, layer_legend_id, min_scale, max_scale, api_key, bundle_id)
  VALUES (38, 'Base Layer (Ordnance Survey)', '&copy Microsoft Bing', 0.8, null, null, null, 'BING_API_KEY', 1);

--default layers background layers.
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (5, 36, 'bing', 'RoadOnDemand');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (6, 37, 'bing', 'AerialWithLabelsOnDemand');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
  VALUES (7, 38, 'bing', 'OrdnanceSurvey');

INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (9, 5, 'wmts', 'RasterBasemaps_OSNI_Discoverer_50K');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (10, 6, 'wmts', 'VectorBasemaps_OSNIFusionBasemap');

-- i18n
INSERT INTO i18n.layers (layer_id, language_code, description) VALUES
	(5, 'nl', 'Achtergrondkaart'),
	(5, 'en', 'Base Layer'),
	(6, 'nl', 'Achtergrondkaart (Luchtfoto)'),
	(6, 'en', 'Base Layer (Aerial)'),
	(7, 'nl', 'Achtergrondkaart (Ordnance Survey)'),
	(7, 'en', 'Base Layer (Ordnance Survey)'),
	(9, 'nl', 'Achtergrondkaart (OSNI Discoverer)'),
	(9, 'en', 'Base Layer (OSNI Discoverer)'),
	(10, 'nl', 'Achtergrondkaart (OSNI Fusion)'),
	(10, 'en', 'Base Layer (OSNI Fusion)'),
	(11, 'nl', 'Natuurgebieden'),
	(11, 'en', 'Ecological sites'),
	(13, 'nl', 'Stikstofgevoelige habitattypen'),
	(13, 'en', 'Nitrogen-sensitive habitat and species'),
	(14, 'nl', 'Habitattypen'),
	(14, 'en', 'Habitat types');

--wms layers from 11+
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (14, 14, 'wms', 'calculator:wms_habitat_types');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (17, 10, 'wms', 'calculator:wms_habitats_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (18, 10, 'wms', 'calculator:wms_relevant_habitat_info_for_receptor_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (21, 28, 'wms', 'calculator:wms_province_areas_view');

-- theme layers
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
	VALUES (5, 'nca', 0, true, true); -- Road
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
	VALUES (6, 'nca', 1, true, false); -- Aerial
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (7, 'nca', 2, true, false); -- Ordnance
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
	VALUES (9, 'nca', 3, true, false); -- OSNI Discoverer
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
	VALUES (10, 'nca', 4, true, false); -- OSNI Fusion
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
	VALUES (14, 'nca', 5, false, false); --habittattypen
