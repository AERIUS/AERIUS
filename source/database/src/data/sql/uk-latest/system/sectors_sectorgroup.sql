/*
 * Insert sectors_sectorgroup.
 */

INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (2100, 'energy');
INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (1900, 'energy');
INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (1910, 'energy');
INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (1920, 'energy');
INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (4110, 'agriculture');
INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (4120, 'agriculture');
INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (4130, 'farmland');
INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (4140, 'farmland');
INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (4150, 'agriculture');
INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (4160, 'farmland');
INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (4200, 'farmland');
INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (4320, 'agriculture');
INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (4400, 'farmland');
INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (4600, 'agriculture');
--INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (8640, 'live_and_work');
--INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (8200, 'live_and_work');
--INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (8210, 'live_and_work');
INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (1050, 'industry');
INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (1100, 'industry');
INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (1300, 'industry');
INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (1400, 'industry');
INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (1500, 'industry');
INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (1700, 'industry');
INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (1800, 'industry');
--INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (3210, 'mobile_equipment');
--INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (3220, 'mobile_equipment');
--INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (3530, 'mobile_equipment');
--INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (3710, 'rail_transportation');
--INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (3720, 'rail_transportation');
--INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (3610, 'aviation');
--INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (3620, 'aviation');
--INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (3630, 'aviation');
--INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (3640, 'aviation');
INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (3100, 'road_transportation');
--INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (7510, 'shipping');
--INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (7520, 'shipping');
--INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (7530, 'shipping');
--INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (7610, 'shipping');
--INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (7620, 'shipping');
INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) VALUES (9999, 'other');
