/*
 * Version-specific constants, which can be different between both products and versions.
 */

-- Model versions constants
INSERT INTO system.constants (key, value) VALUES ('MODEL_VERSION_ADMS', '5.0.3.0-9166');

-- IMAER export version
INSERT INTO system.constants (key, value) VALUES ('IMAER_VERSION', 'V6_0');

-- Import results: allowed versions (comma separated, * as wildcard is allowed)
-- The current version is allowed by default
INSERT INTO system.constants (key, value) VALUES ('IMPORT_RESULTS_ALLOWED_VERSIONS', '*');

-- Manual version (in-context help)
-- Don't use versioning for UK
INSERT INTO system.constants (key, value) VALUES ('MANUAL_VERSION', 'latest');
