INSERT INTO scenario_result_types_color_ranges 
	(scenario_result_type, emission_result_type, substance_id, color_range_type)
VALUES 
	('situation_result', 'deposition', 1711, 'contribution_deposition'),
	('project_calculation', 'deposition', 1711, 'difference_deposition'),
	('max_temporary_effect', 'deposition', 1711, 'difference_deposition'),
	('in_combination', 'deposition', 1711, 'in_combination_deposition'),
	('archive_contribution', 'deposition', 1711, 'in_combination_deposition'),

	('situation_result', 'concentration', 11, 'contribution_concentration_nox'),
	('project_calculation', 'concentration', 11, 'difference_concentration_nox'),
	('max_temporary_effect', 'concentration', 11, 'difference_concentration_nox'),
	('in_combination', 'concentration', 11, 'in_combination_concentration_nox'),
	('archive_contribution', 'concentration', 11, 'in_combination_concentration_nox'),

	('situation_result', 'concentration', 17, 'contribution_concentration_nh3'),
	('project_calculation', 'concentration', 17, 'difference_concentration_nh3'),
	('max_temporary_effect', 'concentration', 17, 'difference_concentration_nh3'),
	('in_combination', 'concentration', 17, 'in_combination_concentration_nh3'),
	('archive_contribution', 'concentration', 17, 'in_combination_concentration_nh3');
