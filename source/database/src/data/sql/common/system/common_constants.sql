/*
 * Common constants, which are the same across all products and versions.
 */

INSERT INTO system.constants (key, value) VALUES ('CONNECT_INTERNAL_URL', 'http://localhost:8081/api');
INSERT INTO system.constants (key, value) VALUES ('GEOSERVER_INTERNAL_URL', 'http://localhost:8082/geoserver-calculator/wms');
INSERT INTO system.constants (key, value) VALUES ('SEARCH_ENDPOINT_URL', '/api/search/');

-- email constants
INSERT INTO system.constants (key, value) VALUES ('NOREPLY_EMAIL', 'noreply@aerius.nl');

-- System info message default empty 
INSERT INTO system.constants (key, value) VALUES ('SYSTEM_INFO_PASSKEY', '');
INSERT INTO system.constants (key, value) VALUES ('SYSTEM_INFO_POLLING_TIME', 120000);

-- Calculator limits
INSERT INTO system.constants (key, value) VALUES ('CALCULATOR_LIMITS_MAX_SOURCES', '20000');
INSERT INTO system.constants (key, value) VALUES ('CALCULATOR_LIMITS_MAX_LINE_LENGTH', '100000');
INSERT INTO system.constants (key, value) VALUES ('CALCULATOR_LIMITS_MAX_POLYGON_SURFACE', '5000');
INSERT INTO system.constants (key, value) VALUES ('CALCULATOR_LARGE_CALCULATIONS_SPLIT', '100');

-- Connect constants
INSERT INTO system.constants (key, value) VALUES ('CONNECT_GENERATING_APIKEY_ENABLED', 'true');
INSERT INTO system.constants (key, value) VALUES ('CONNECT_MAX_CONCURRENT_JOBS_FOR_NEW_USER', '6');
INSERT INTO system.constants (key, value) VALUES ('CONNECT_DEFAULT_PERIOD_JOB_RATE_LIMIT', '100');
