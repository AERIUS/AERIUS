/**
 * The number of hexagons in a horizontal row.
 */
INSERT INTO system.constants (key, value) VALUES ('HEXAGON_ROWS', ae_determine_number_of_hexagon_rows(1));
