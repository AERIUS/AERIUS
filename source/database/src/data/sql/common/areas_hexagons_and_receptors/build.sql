-- Expect base data (usually general_base_xx.sql file) to be imported first.
{import_common 'areas_hexagons_and_receptors/build/habitats.sql'}
{import_common 'areas_hexagons_and_receptors/build/receptors_and_hexagons.sql'}
{import_common 'areas_hexagons_and_receptors/build/relevant_habitats.sql'}
{import_common 'areas_hexagons_and_receptors/build/receptors_to_assessment_areas.sql'}
{import_common 'areas_hexagons_and_receptors/build/receptors_to_critical_deposition_areas.sql'}
{import_common 'areas_hexagons_and_receptors/build/receptors_to_relevant_habitats.sql'}
