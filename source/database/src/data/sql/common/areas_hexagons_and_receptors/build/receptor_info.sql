SELECT ae_raise_notice('Build: included_receptors @ ' || timeofday());
BEGIN;
	INSERT INTO included_receptors(receptor_id)
		SELECT receptor_id
		FROM setup.build_included_receptors_view;
COMMIT;

SELECT ae_raise_notice('Build: critical_levels @ ' || timeofday());
BEGIN;
	INSERT INTO critical_levels(receptor_id, substance_id, result_type, critical_level)
		SELECT receptor_id, substance_id, result_type, critical_level
		FROM setup.build_critical_levels;
COMMIT;

SELECT ae_raise_notice('Build: hexagon_type_receptors @ ' || timeofday());
BEGIN;
	INSERT INTO hexagon_type_receptors(hexagon_type, receptor_id)
		SELECT hexagon_type, receptor_id
		FROM setup.build_hexagon_type_receptors_view;
COMMIT;
