SELECT ae_raise_notice('Build: habitats @ ' || timeofday());
BEGIN;
	INSERT INTO habitats(assessment_area_id, habitat_type_id, habitat_coverage, geometry)
		SELECT assessment_area_id, habitat_type_id, habitat_coverage, geometry
		FROM setup.build_habitats_view;
COMMIT;
