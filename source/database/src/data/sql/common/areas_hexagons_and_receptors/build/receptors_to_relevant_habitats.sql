SELECT ae_raise_notice('Build: receptors_to_relevant_habitats @ ' || timeofday());
BEGIN;
	INSERT INTO receptors_to_relevant_habitats (assessment_area_id, critical_deposition_area_id, receptor_id, cartographic_surface)
	SELECT 
		assessment_area_id, 
		critical_deposition_area_id, 
		receptor_id,
		surface * receptor_habitat_coverage AS cartographic_surface
		
		FROM receptors_to_critical_deposition_areas
		WHERE type = 'relevant_habitat';
COMMIT;
