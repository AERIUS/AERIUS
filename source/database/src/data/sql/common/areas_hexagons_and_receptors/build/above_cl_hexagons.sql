SELECT ae_raise_notice('Build: above_cl_hexagons for hexagon_type_receptors @ ' || timeofday());
BEGIN;
	INSERT INTO hexagon_type_receptors(hexagon_type, receptor_id)
	SELECT 
		'above_cl_hexagons'::hexagon_type,
		receptor_id

		FROM critical_levels
			INNER JOIN receptor_background_results_view USING (receptor_id, substance_id) 

    WHERE year = (SELECT background_year FROM background_years LIMIT 1)
    	AND result > critical_level 
    	AND substance_id = 1711;
COMMIT;
