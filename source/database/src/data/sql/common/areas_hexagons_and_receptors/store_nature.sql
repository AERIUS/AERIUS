SELECT setup.ae_store_query(
	'receptors_to_assessment_areas',
	$$ SELECT * FROM receptors_to_assessment_areas ORDER BY receptor_id, assessment_area_id $$,
	'{data_folder}/export/{tablename}_{datesuffix}.txt',
	FALSE
);


SELECT setup.ae_store_query(
	'relevant_habitat_areas',
	$$ SELECT * FROM relevant_habitat_areas ORDER BY assessment_area_id, habitat_area_id, habitat_type_id $$,
	'{data_folder}/export/{tablename}_{datesuffix}.txt',
	FALSE
);


SELECT setup.ae_store_query(
	'habitats',
	$$ SELECT * FROM habitats ORDER BY assessment_area_id, habitat_type_id $$,
	'{data_folder}/export/{tablename}_{datesuffix}.txt',
	FALSE
);


SELECT setup.ae_store_query(
	'relevant_habitats',
	$$ SELECT * FROM relevant_habitats ORDER BY assessment_area_id, habitat_type_id $$,
	'{data_folder}/export/{tablename}_{datesuffix}.txt',
	FALSE
);


SELECT setup.ae_store_query(
	'receptors_to_critical_deposition_areas',
	$$ SELECT * FROM receptors_to_critical_deposition_areas ORDER BY assessment_area_id, type, critical_deposition_area_id, receptor_id $$,
	'{data_folder}/export/{tablename}_{datesuffix}.txt',
	FALSE
);


SELECT setup.ae_store_query(
	'setup.geometry_of_interests',
	$$ SELECT * FROM setup.geometry_of_interests ORDER BY assessment_area_id $$,
	'{data_folder}/export/{tablename}_{datesuffix}.txt',
	FALSE
);


SELECT setup.ae_store_query(
	'receptors',
	$$ SELECT DISTINCT receptors.* 
			FROM receptors
				INNER JOIN 
					(SELECT
						DISTINCT receptor_id
						FROM setup.geometry_of_interests
							INNER JOIN assessment_areas USING (assessment_area_id)
							INNER JOIN authorities USING (authority_id)
							INNER JOIN receptors ON ST_Within(receptors.geometry, geometry_of_interests.geometry)
						WHERE authorities.country_id = 1
					) AS nl_receptors USING (receptor_id)


			ORDER BY receptor_id 
	$$,
	'{data_folder}/export/{tablename}_{datesuffix}.txt',
	FALSE
);


SELECT setup.ae_store_query(
	'hexagons',
	$$ SELECT DISTINCT hexagons.* 
			FROM hexagons 
				INNER JOIN 
					(SELECT
						DISTINCT receptor_id
						FROM setup.geometry_of_interests
							INNER JOIN assessment_areas USING (assessment_area_id)
							INNER JOIN authorities USING (authority_id)
							INNER JOIN receptors ON ST_Within(receptors.geometry, geometry_of_interests.geometry)
						WHERE authorities.country_id = 1
					) AS nl_receptors USING (receptor_id)

			ORDER BY receptor_id, zoom_level 
	$$,
	'{data_folder}/export/{tablename}_{datesuffix}.txt',
	FALSE
);
