{import_common 'general/'}

{import_common 'sectors/data_24.sql'}

{import_common 'background_cells/data_24.sql'}

{import_common 'receptor_backgrounds/data_24.sql'}

{import_common 'background_cells/load.sql'}

{import_common 'areas_hexagons_and_receptors/load.sql'}

{import_common 'calculator/'}

{import_common 'shipping/data_24.sql'}

{import_common 'emission_factors/load.sql'}

{import_common 'procurement_thresholds/'}

{import_common 'system/'}

{import_common 'i18n/'}
{import_common 'i18n_sectors/sectors_24.sql'}
