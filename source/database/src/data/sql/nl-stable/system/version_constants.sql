/*
 * Version-specific constants, which can be different between both products and versions.
 */

-- Calculator min/max years (used for selection-range for user).
INSERT INTO system.constants (key, value) VALUES ('MIN_YEAR', '2024');
INSERT INTO system.constants (key, value) VALUES ('MAX_YEAR', '2040');

-- Model versions constants
INSERT INTO system.constants (key, value) VALUES ('MODEL_VERSION_OPS', 'STABLE');
INSERT INTO system.constants (key, value) VALUES ('MODEL_VERSION_SRM_OWN2000', 'STABLE');
INSERT INTO system.constants (key, value) VALUES ('MODEL_VERSION_SRM_RBL', 'RBL_STABLE');

-- IMAER export version
INSERT INTO system.constants (key, value) VALUES ('IMAER_VERSION', 'V6_0');

-- Import results: allowed versions (comma separated, * as wildcard is allowed)
-- The current version is allowed by default
INSERT INTO system.constants (key, value) VALUES ('IMPORT_RESULTS_ALLOWED_VERSIONS', '2024_*,2024.*');

-- Manual version (in-context help)
INSERT INTO system.constants (key, value) VALUES ('MANUAL_VERSION', '2024');

-- Manual language (in-context help)
-- Make the client determine the language based on the browser settings
INSERT INTO system.constants (key, value) VALUES ('MANUAL_LANGUAGE', ':language');
