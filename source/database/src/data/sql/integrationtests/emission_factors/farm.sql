/* Farm animal category and animal housing data */
INSERT INTO farm_animal_categories (farm_animal_category_id, code, farm_animal_type, name, description)
VALUES
	(1, 'HA1', 'cow', 'HA1', 'Melk- en kalfkoeien van 2 jaar en ouder (inclusief kalveren jonger dan 14 dagen)'),
	(2, 'HA2', 'cow', 'HA2', 'Vrouwelijk jongvee jonger dan 2 jaar, fokstieren jonger dan 2 jaar'),
	(16, 'HE1', 'chicken', 'HE1', 'Opfokhennen en -hanen van legkippen jonger dan 18 weken, kooihuisvesting');

INSERT INTO farm_animal_housing_categories (farm_animal_housing_category_id, farm_animal_category_id, code, name, description, farm_emission_factor_type)
VALUES
	(1, 1, 'HA1.1', 'HA1.1', 'Grupstal met drijfmest', 'per_animal_per_year'),
	(40, 1, 'HA1.100', 'HA1.100', 'Overige huisvestingssystemen', 'per_animal_per_year'),
	(41, 2, 'HA2.100', 'HA2.100', 'Overige huisvestingssystemen', 'per_animal_per_year'),
	(127, 16, 'HE1.1.2.1', 'HE1.1.2.1', 'Beluchting 0,2 m3/uur per dierplaats', 'per_animal_per_year');

INSERT INTO farm_animal_basic_housing (farm_animal_housing_category_id, basic_housing_category_id)
VALUES
	(1, 40);

INSERT INTO farm_housing_emission_factors (farm_animal_housing_category_id, substance_id, emission_factor)
VALUES
	(1, 17, 5.7),
	(40, 17, 13),
	(41, 17, 4.4),
	(127, 17, 0.02);

INSERT INTO farm_additional_housing_systems (farm_additional_housing_system_id, code, name, description, air_scrubber)
VALUES
	(19, 'AR1.1', 'AR1.1', 'Beweiden', false),
	(24, 'LW1.1', 'LW1.1', 'Biologisch luchtwassysteem', true);

INSERT INTO farm_additional_housing_factors (farm_additional_housing_system_id, farm_animal_housing_category_id, substance_id, reduction_factor)
VALUES
	(19, 1, 17, 0),
	(19, 40, 17, 0),
	(24, 127, 17, 0.7);

INSERT INTO farm_housing_categories_additional_systems (farm_animal_housing_category_id, farm_additional_housing_system_id)
VALUES
	(1, 19),
	(40, 19),
	(127, 24);

/* Farmland categories */
INSERT INTO farmland_categories (farmland_category_id, sector_id, code, name, description)
VALUES
	(1, 4130, 'PASTURE', 'Beweiding', 'Beweiding'),
	(2, 4140, 'MANURE', 'Mestaanwending: dierlijke mest', 'Mestaanwending (dierlijke mest)');
