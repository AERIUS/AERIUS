/* Off road mobile source data */
INSERT INTO mobile_source_off_road_categories (mobile_source_off_road_category_id, code, name, description, sort_order)
VALUES
	(1, 'SI56DSN', 'Stage-I, <= 2001, <= 56 kW, diesel, SCR: nee', 'Stage-I, <= 2001, <= 56 kW, diesel, SCR: nee', 1),
	(2, 'SI5675DSN', 'Stage-I, <= 2001, 56-75 kW, diesel, SCR: nee', 'Stage-I, <= 2001, 56-75 kW, diesel, SCR: nee', 2),
	(16, 'SIIIB75560DSJ', 'Stage-IIIB, 2011-2013, 75-560 kW, diesel, SCR: ja', 'Stage-IIIB, 2011-2013, 75-560 kW, diesel, SCR: ja', 16);

INSERT INTO mobile_source_off_road_category_emission_factors (mobile_source_off_road_category_id, substance_id, emission_factor_per_liter_fuel, emission_factor_per_operating_hour, emission_factor_per_liter_adblue)
VALUES
	(1, 11, 0.03, 0.005, 0),
	(1, 17, 7.5e-06, 0, 0),
	(2, 11, 0.03, 0.005, 0),
	(2, 17, 7.5e-06, 0, 0),
	(16, 11, 0.025, 0.005, -0.46),
	(16, 17, 0.00024, 0, 0);

INSERT INTO mobile_source_off_road_category_adblue_properties (mobile_source_off_road_category_id, max_adblue_fuel_ratio)
VALUES
	(16, 0.04);
