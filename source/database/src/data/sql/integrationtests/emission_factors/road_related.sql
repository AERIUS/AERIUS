-- Contains road, mobile source on road and cold start data.

/* Road data */
INSERT INTO road_area_categories (road_area_category_id, code, name)
VALUES
	(1, 'NL', 'nationaal');

INSERT INTO road_type_categories (road_type_category_id, code, name)
VALUES
	(1, 'FREEWAY', 'snelweg'),
	(2, 'NON_URBAN_ROAD_NATIONAL', 'buitenweg nationale weg'),
	(4, 'URBAN_ROAD_NORMAL', 'normaal stadsverkeer');

INSERT INTO road_vehicle_categories (road_vehicle_category_id, code, name)
VALUES
	(1, 'LIGHT_TRAFFIC', 'Licht verkeer'),
	(2, 'NORMAL_FREIGHT', 'Middelzwaar vrachtverkeer'),
	(3, 'HEAVY_FREIGHT', 'Zwaar vrachtverkeer'),
	(4, 'AUTO_BUS', 'Busverkeer');

INSERT INTO road_speed_profiles (road_speed_profile_id, speed_limit_enforcement, maximum_speed, gradient)
VALUES
	(1, 'irrelevant', null, null),
	(2, 'not_strict', 80, null),
	(3, 'not_strict', 100, null),
	(4, 'not_strict', 120, null),
	(5, 'not_strict', 130, null),
	(6, 'strict', 80, null),
	(7, 'strict', 100, null);

INSERT INTO road_areas_to_road_types (road_area_category_id, road_type_category_id)
VALUES
	(1, 1),
	(1, 2),
	(1, 4);

INSERT INTO road_types_to_speed_profiles (road_type_category_id, road_speed_profile_id)
VALUES
	(1, 2),
	(1, 3),
	(1, 4),
	(1, 5),
	(1, 6),
	(1, 7),
	(2, 2),
	(4, 1);

INSERT INTO road_categories (road_category_id, road_area_category_id, road_type_category_id, road_vehicle_category_id, road_speed_profile_id)
VALUES
	(1, 1, 1, 1, 2),
	(2, 1, 1, 2, 2),
	(3, 1, 1, 3, 2),
	(4, 1, 1, 4, 2),
	(25, 1, 2, 1, 2),
	(26, 1, 2, 2, 2),
	(27, 1, 2, 3, 2),
	(28, 1, 2, 4, 2),
	(33, 1, 4, 1, 1),
	(34, 1, 4, 2, 1),
	(35, 1, 4, 3, 1),
	(36, 1, 4, 4, 1);

INSERT INTO road_category_emission_factors (road_category_id, year, substance_id, emission_factor, stagnated_emission_factor)
SELECT 1, generate_series(2024, 2040), 11, 0.2037, 0.3912
UNION
SELECT 1, generate_series(2024, 2040), 17, 0.0277, 0.0308
UNION
SELECT 2, generate_series(2024, 2040), 11, 1.6371, 5.8604
UNION
SELECT 2, generate_series(2024, 2040), 17, 0.0572, 0.0572
UNION
SELECT 3, generate_series(2024, 2040), 11, 1.7974, 6.7085
UNION
SELECT 3, generate_series(2024, 2040), 17, 0.0729, 0.0729
UNION
SELECT 4, generate_series(2024, 2040), 11, 1.6371, 5.8604
UNION
SELECT 4, generate_series(2024, 2040), 17, 0.0572, 0.0572
UNION
SELECT 25, generate_series(2024, 2040), 11, 0.2016, 0.3913
UNION
SELECT 25, generate_series(2024, 2040), 17, 0.018, 0.0131
UNION
SELECT 26, generate_series(2024, 2040), 11, 2.0666, 5.4447
UNION
SELECT 26, generate_series(2024, 2040), 17, 0.0628, 0.0511
UNION
SELECT 27, generate_series(2024, 2040), 11, 3.2732, 6.1514
UNION
SELECT 27, generate_series(2024, 2040), 17, 0.0931, 0.0731
UNION
SELECT 28, generate_series(2024, 2040), 11, 1.0115, 2.1942
UNION
SELECT 28, generate_series(2024, 2040), 17, 0.0053, 0.0053
UNION
SELECT 33, generate_series(2024, 2040), 11, 0.3064, 0.4775
UNION
SELECT 33, generate_series(2024, 2040), 17, 0.0125, 0.0144
UNION
SELECT 34, generate_series(2024, 2040), 11, 3.5795, 6.2537
UNION
SELECT 34, generate_series(2024, 2040), 17, 0.0567, 0.0567
UNION
SELECT 35, generate_series(2024, 2040), 11, 4.823, 7.2952
UNION
SELECT 35, generate_series(2024, 2040), 17, 0.0829, 0.092
UNION
SELECT 36, generate_series(2024, 2040), 11, 1.6436, 2.7448
UNION
SELECT 36, generate_series(2024, 2040), 17, 0.0053, 0.0053;

/* On road mobile source data */
INSERT INTO mobile_source_on_road_categories (mobile_source_on_road_category_id, code, name, description)
VALUES
	(1, 'LBALEUR3', 'Bestelauto - LPG - Euro-3', 'Bestelauto - LPG - Euro-3'),
	(2, 'LBALEUR4', 'Bestelauto - LPG - Euro-4', 'Bestelauto - LPG - Euro-4');

INSERT INTO mobile_source_on_road_category_emission_factors (mobile_source_on_road_category_id, road_type_category_id, year, substance_id, emission_factor)
SELECT 1, 1, generate_series(2024, 2040), 11, 0.19394
UNION
SELECT 1, 1, generate_series(2024, 2040), 17, 0.065016
UNION
SELECT 1, 2, generate_series(2024, 2040), 11, 0.29846
UNION
SELECT 1, 2, generate_series(2024, 2040), 17, 0.029541
UNION
SELECT 1, 4, generate_series(2024, 2040), 11, 0.35959
UNION
SELECT 1, 4, generate_series(2024, 2040), 17, 0.058468
UNION
SELECT 2, 1, generate_series(2024, 2040), 11, 0.035881
UNION
SELECT 2, 1, generate_series(2024, 2040), 17, 0.06492
UNION
SELECT 2, 2, generate_series(2024, 2040), 11, 0.14704
UNION
SELECT 2, 2, generate_series(2024, 2040), 17, 0.029497
UNION
SELECT 2, 4, generate_series(2024, 2040), 11, 0.16868
UNION
SELECT 2, 4, generate_series(2024, 2040), 17, 0.037734;

/* Cold start data */
INSERT INTO cold_start_standard_emission_factors (road_vehicle_category_id, year, substance_id, emission_factor)
SELECT 1, generate_series(2024, 2040), 11, 0.2849
UNION
SELECT 1, generate_series(2024, 2040), 17, 0.0593
UNION
SELECT 2, generate_series(2024, 2040), 11, 20.4967
UNION
SELECT 2, generate_series(2024, 2040), 17, 0.1786;

INSERT INTO cold_start_specific_emission_factors (mobile_source_on_road_category_id, year, substance_id, emission_factor)
SELECT 1, generate_series(2024, 2040), 11, 1.15
UNION
SELECT 1, generate_series(2024, 2040), 17, 0.29234
UNION
SELECT 2, generate_series(2024, 2040), 11, 0.51
UNION
SELECT 2, generate_series(2024, 2040), 17, 0.18867;

INSERT INTO cold_start_source_characteristics (cold_start_source_characteristics_id, heat_content, height, spread, emission_diurnal_variation_id, particle_size_distribution)
VALUES
	(1, 0, 0.3, 0.1, 31, 0),
	(2, 0, 0.3, 0.1, 32, 0),
	(3, 0, 0.3, 0.1, 33, 0);

INSERT INTO cold_start_standard_characteristics (road_vehicle_category_id, cold_start_source_characteristics_id)
VALUES
	(1, 1),
	(2, 2),
	(3, 2),
	(4, 3);

INSERT INTO cold_start_specific_characteristics (mobile_source_on_road_category_id, cold_start_source_characteristics_id)
VALUES
	(1, 1),
	(2, 1);
