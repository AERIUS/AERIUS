/* Maritime shipping data */
INSERT INTO shipping_maritime_categories (shipping_maritime_category_id, code, name, description)
VALUES
	(1, 'OO100', 'Olietankers, overige tankers GT: 100-1599', 'Olietankers, overige tankers GT: 100-1599'),
	(2, 'OO1600', 'Olietankers, overige tankers GT: 1600-2999', 'Olietankers, overige tankers GT: 1600-2999'),
	(5, 'OO10000', 'Olietankers, overige tankers GT: 10000-29999', 'Olietankers, overige tankers GT: 10000-29999');

INSERT INTO shipping_maritime_category_maneuver_properties (shipping_maritime_category_id, maneuver_factor, maneuver_length)
VALUES
	(1, 1, 1000),
	(2, 1, 1000),
	(5, 1.8, 2200);

INSERT INTO shipping_maritime_maneuver_areas (shipping_maritime_maneuver_area_id, maneuver_factor, geometry)
VALUES
	(1, 1.5, '010300002040710000010000000B0000000000000060D7F8400000000028691E4100000000A0F6F8400000000028691E4100000000E015F9400000000028691E4100000000E015F9400000000058611E4100000000A0F6F8400000000058611E410000000060D7F8400000000058611E410000000020B8F8400000000058611E4100000000E098F8400000000058611E4100000000E098F8400000000028691E410000000020B8F8400000000028691E410000000060D7F8400000000028691E41');

INSERT INTO shipping_maritime_category_emission_factors (shipping_maritime_category_id, year, substance_id, movement_type, emission_factor)
SELECT 1, generate_series(2024, 2040), 11, 'dock'::shipping_movement_type, 0.45514837
UNION
SELECT 1, generate_series(2024, 2040), 11, 'inland'::shipping_movement_type, 0.39370972
UNION
SELECT 1, generate_series(2024, 2040), 11, 'maritime'::shipping_movement_type, 0.40276244
UNION
SELECT 2, generate_series(2024, 2040), 11, 'dock'::shipping_movement_type, 0.9127268
UNION
SELECT 2, generate_series(2024, 2040), 11, 'inland'::shipping_movement_type, 0.6738046
UNION
SELECT 2, generate_series(2024, 2040), 11, 'maritime'::shipping_movement_type, 0.64895743
UNION
SELECT 5, generate_series(2024, 2040), 11, 'dock'::shipping_movement_type, 7.2854033
UNION
SELECT 5, generate_series(2024, 2040), 11, 'inland'::shipping_movement_type, 2.5867498
UNION
SELECT 5, generate_series(2024, 2040), 11, 'maritime'::shipping_movement_type, 2.6643746;

INSERT INTO shipping_maritime_category_source_characteristics (shipping_maritime_category_id, year, movement_type, heat_content, height, spread, emission_diurnal_variation_id, particle_size_distribution)
SELECT 1, generate_series(2024, 2040), 'dock'::shipping_movement_type, 0.049422987, 3, 1.5, 0, 3831
UNION
SELECT 1, generate_series(2024, 2040), 'inland'::shipping_movement_type, 0.441092, 13, 6.5, 0, 3831
UNION
SELECT 1, generate_series(2024, 2040), 'maritime'::shipping_movement_type, 0.34973913, 13, 6.5, 0, 3831
UNION
SELECT 2, generate_series(2024, 2040), 'dock'::shipping_movement_type, 0.10583825, 7, 3.5, 0, 3831
UNION
SELECT 2, generate_series(2024, 2040), 'inland'::shipping_movement_type, 0.6707177, 16, 8, 0, 3831
UNION
SELECT 2, generate_series(2024, 2040), 'maritime'::shipping_movement_type, 0.62175405, 16, 8, 0, 3831
UNION
SELECT 5, generate_series(2024, 2040), 'dock'::shipping_movement_type, 0.9161085, 22, 11, 0, 3831
UNION
SELECT 5, generate_series(2024, 2040), 'inland'::shipping_movement_type, 1.8222765, 30, 15, 0, 3831
UNION
SELECT 5, generate_series(2024, 2040), 'maritime'::shipping_movement_type, 2.1057897, 30, 15, 0, 3831;

INSERT INTO shipping_maritime_mooring_maneuver_factors (tonnage_category_id, tonnage_lower_threshold, maneuver_factor, maneuver_length)
VALUES
	(1, 0, 1, 1000),
	(5, 10000, 1.8, 2200),
	(7, 60000, 1.8, 4600),
	(8, 100000, 1.8, 7700);
