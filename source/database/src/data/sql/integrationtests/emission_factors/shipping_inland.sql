/* Inland shipping data */
INSERT INTO shipping_inland_categories (shipping_inland_category_id, code, name, description)
VALUES
	(1, 'BI', 'BI', 'Duwstel BI (Europa I)'),
	(2, 'BII-1', 'BII-1', 'Duwstel - BII-1 (Europa II)'),
	(7, 'BII-6L', 'BII-6L', 'Duwstel - BII-6l (6-baksduwstel lang)');

INSERT INTO shipping_inland_category_source_characteristics (shipping_inland_category_id, shipping_inland_waterway_category_id, ship_direction, laden_state, heat_content, height, spread, emission_diurnal_variation_id, particle_size_distribution)
VALUES
	(1, 3, 'irrelevant'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, 0.044, 2.7, 1.35, 1, 3861),
	(1, 3, 'irrelevant'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, 0.02, 5.3, 2.65, 1, 3861),
	(1, 5, 'irrelevant'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, 0.07, 2.7, 1.35, 1, 3861),
	(1, 5, 'irrelevant'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, 0.046, 5.3, 2.65, 1, 3861),
	(1, 11, 'upstream'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, 0.613, 2.7, 1.35, 1, 3861),
	(1, 11, 'upstream'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, 0.314, 5.3, 2.65, 1, 3861),
	(1, 11, 'downstream'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, 0.121, 2.7, 1.35, 1, 3861),
	(1, 11, 'downstream'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, 0.07, 5.3, 2.65, 1, 3861),
	(1, 13, 'upstream'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, 0.615, 2.7, 1.35, 1, 3861),
	(1, 13, 'upstream'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, 0.588, 5.3, 2.65, 1, 3861),
	(1, 13, 'downstream'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, 0.051, 2.7, 1.35, 1, 3861),
	(1, 13, 'downstream'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, 0.111, 5.3, 2.65, 1, 3861),
	(2, 3, 'irrelevant'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, 0.089, 2.7, 1.35, 1, 3861),
	(2, 3, 'irrelevant'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, 0.699, 5.3, 2.65, 1, 3861),
	(2, 5, 'irrelevant'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, 0.546, 2.7, 1.35, 1, 3861),
	(2, 5, 'irrelevant'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, 0.203, 5.3, 2.65, 1, 3861),
	(2, 11, 'upstream'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, 0.944, 2.7, 1.35, 1, 3861),
	(2, 11, 'upstream'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, 0.658, 5.3, 2.65, 1, 3861),
	(2, 11, 'downstream'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, 0.266, 2.7, 1.35, 1, 3861),
	(2, 11, 'downstream'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, 0.137, 5.3, 2.65, 1, 3861),
	(2, 13, 'upstream'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, 0.846, 2.7, 1.35, 1, 3861),
	(2, 13, 'upstream'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, 0.556, 5.3, 2.65, 1, 3861),
	(2, 13, 'downstream'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, 0.498, 2.7, 1.35, 1, 3861),
	(2, 13, 'downstream'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, 0.155, 5.3, 2.65, 1, 3861),
	(7, 13, 'upstream'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, 1.735, 2.7, 1.35, 1, 3861),
	(7, 13, 'upstream'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, 2.625, 5.5, 2.75, 1, 3861),
	(7, 13, 'downstream'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, 0.234, 2.7, 1.35, 1, 3861),
	(7, 13, 'downstream'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, 0.493, 5.5, 2.75, 1, 3861);

INSERT INTO shipping_inland_category_source_characteristics_docked(shipping_inland_category_id, laden_state, heat_content, height, spread, emission_diurnal_variation_id, particle_size_distribution)
VALUES
	(1, 'laden'::shipping_inland_laden_state, 0.01, 2.7, 1.35, 1, 3861),
	(1, 'unladen'::shipping_inland_laden_state, 0.01, 5.3, 2.65, 1, 3861),
	(2, 'laden'::shipping_inland_laden_state, 0.02, 2.7, 1.35, 1, 3861),
	(2, 'unladen'::shipping_inland_laden_state, 0.02, 5.3, 2.65, 1, 3861),
	(7, 'laden'::shipping_inland_laden_state, 0.02, 2.7, 1.35, 1, 3861),
	(7, 'unladen'::shipping_inland_laden_state, 0.02, 5.5, 2.75, 1, 3861);

INSERT INTO shipping_inland_category_emission_factors (shipping_inland_category_id, shipping_inland_waterway_category_id, ship_direction, laden_state, year, substance_id, emission_factor)
SELECT 1, 3, 'irrelevant'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 197.188
UNION
SELECT 1, 3, 'irrelevant'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 69.665
UNION
SELECT 1, 5, 'irrelevant'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 177.902
UNION
SELECT 1, 5, 'irrelevant'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 113.026
UNION
SELECT 1, 11, 'upstream'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 701.896
UNION
SELECT 1, 11, 'upstream'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 313.211
UNION
SELECT 1, 11, 'downstream'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 141.75
UNION
SELECT 1, 11, 'downstream'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 75.847
UNION
SELECT 1, 13, 'upstream'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 703.476
UNION
SELECT 1, 13, 'upstream'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 456.769
UNION
SELECT 1, 13, 'downstream'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 91.941
UNION
SELECT 1, 13, 'downstream'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 89.355
UNION
SELECT 2, 3, 'irrelevant'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 310.862
UNION
SELECT 2, 3, 'irrelevant'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 605.833
UNION
SELECT 2, 5, 'irrelevant'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 644.569
UNION
SELECT 2, 5, 'irrelevant'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 216.673
UNION
SELECT 2, 11, 'upstream'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 1225.065
UNION
SELECT 2, 11, 'upstream'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 560.854
UNION
SELECT 2, 11, 'downstream'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 268.15
UNION
SELECT 2, 11, 'downstream'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 135.925
UNION
SELECT 2, 13, 'upstream'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 1004.911
UNION
SELECT 2, 13, 'upstream'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 519.208
UNION
SELECT 2, 13, 'downstream'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 357.668
UNION
SELECT 2, 13, 'downstream'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 126.152
UNION
SELECT 7, 13, 'upstream'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 2323.368
UNION
SELECT 7, 13, 'upstream'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 2663.455
UNION
SELECT 7, 13, 'downstream'::shipping_inland_ship_direction_type, 'laden'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 401.246
UNION
SELECT 7, 13, 'downstream'::shipping_inland_ship_direction_type, 'unladen'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 442.838;

INSERT INTO shipping_inland_category_emission_factors_docked (shipping_inland_category_id, laden_state, year, substance_id, emission_factor)
SELECT 1, 'laden'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 95
UNION
SELECT 1, 'unladen'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 95
UNION
SELECT 2, 'laden'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 119.9
UNION
SELECT 2, 'unladen'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 119.9
UNION
SELECT 7, 'laden'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 119.9
UNION
SELECT 7, 'unladen'::shipping_inland_laden_state, generate_series(2024, 2040), 11, 119.9;
