BEGIN;
	INSERT INTO i18n.farm_animal_categories
	SELECT farm_animal_category_id, unnest(ARRAY['nl', 'en']::i18n.language_code_type[]), description FROM farm_animal_categories;
COMMIT;

BEGIN;
	INSERT INTO i18n.farm_animal_housing_categories
	SELECT farm_animal_housing_category_id, unnest(ARRAY['nl', 'en']::i18n.language_code_type[]), description FROM farm_animal_housing_categories;
COMMIT;

BEGIN;
	INSERT INTO i18n.farm_additional_housing_systems
	SELECT farm_additional_housing_system_id, unnest(ARRAY['nl', 'en']::i18n.language_code_type[]), description FROM farm_additional_housing_systems;
COMMIT;

BEGIN;
	INSERT INTO i18n.farmland_categories
	SELECT farmland_category_id, unnest(ARRAY['nl', 'en']::i18n.language_code_type[]), name, description FROM farmland_categories;
COMMIT;

BEGIN;
	INSERT INTO i18n.mobile_source_off_road_categories
	SELECT mobile_source_off_road_category_id, unnest(ARRAY['nl', 'en']::i18n.language_code_type[]), name, description FROM mobile_source_off_road_categories;
COMMIT;

BEGIN;
	INSERT INTO i18n.road_area_categories
	SELECT road_area_category_id, unnest(ARRAY['nl', 'en']::i18n.language_code_type[]), name, name FROM road_area_categories;
COMMIT;

BEGIN;
	INSERT INTO i18n.road_type_categories
	SELECT road_type_category_id, unnest(ARRAY['nl', 'en']::i18n.language_code_type[]), name, name FROM road_type_categories;
COMMIT;

BEGIN;
	INSERT INTO i18n.road_vehicle_categories
	SELECT road_vehicle_category_id, unnest(ARRAY['nl', 'en']::i18n.language_code_type[]), name, name FROM road_vehicle_categories;
COMMIT;

BEGIN;
	INSERT INTO i18n.mobile_source_on_road_categories (mobile_source_on_road_category_id, language_code, name, description)
	SELECT mobile_source_on_road_category_id, unnest(ARRAY['nl', 'en']::i18n.language_code_type[]), name, description FROM mobile_source_on_road_categories;
COMMIT;

BEGIN;
	INSERT INTO i18n.shipping_maritime_categories
	SELECT shipping_maritime_category_id, unnest(ARRAY['nl', 'en']::i18n.language_code_type[]), name, description FROM shipping_maritime_categories;
COMMIT;

BEGIN;
	INSERT INTO i18n.shipping_inland_categories
	SELECT shipping_inland_category_id, unnest(ARRAY['nl', 'en']::i18n.language_code_type[]), name, description FROM shipping_inland_categories;
COMMIT;

BEGIN;
	INSERT INTO i18n.shipping_inland_waterway_categories
	SELECT shipping_inland_waterway_category_id, unnest(ARRAY['nl', 'en']::i18n.language_code_type[]), name, description FROM shipping_inland_waterway_categories;
COMMIT;
