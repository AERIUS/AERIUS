{import_common 'areas_hexagons_and_receptors/data/general_base.sql'}


-- normally preprocessed, but small enough now that it should be fast enough
-- insert any required receptors outside of the areas beforehand to generate rest of data appropriately
INSERT INTO receptors (receptor_id, geometry)
SELECT 4324599, ae_determine_coordinates_from_receptor_id(4324599);

{import_common 'areas_hexagons_and_receptors/build.sql'}

-- additional loads

INSERT INTO extra_assessment_receptors (receptor_id, assessment_area_id, habitat_type_id)
VALUES
	(4256096, 65, 144),
	(4256097, 65, 144),
	(4257626, 65, 144),
	(4260686, 65, 144),
	(4260686, 65, 174),
	(4260687, 65, 144),
	(4262215, 65, 144),
	(4262215, 65, 174),
	(4263742, 65, 144),
	(4265273, 65, 144),
	(4271381, 65, 176),
	(4272911, 65, 176),
	(4274439, 65, 176),
	(4275968, 65, 176),
	(4275969, 65, 176),
	(4275970, 65, 174),
	(4277497, 65, 176),
	(4279026, 65, 176),
	(4279027, 65, 176),
	(4286670, 65, 144),
	(4288199, 65, 144),
	(4291257, 65, 144),
	(4291257, 65, 174),
	(4294316, 65, 174);

-- terrain_properties ignored, only used in WFS for opendata.

INSERT INTO system.habitat_type_colors (habitat_type_id, fill_color, stroke_color)
SELECT habitat_type_id, '6706FF', '4E4E4E' FROM habitat_types;

INSERT INTO non_exceeding_receptors (receptor_id)
VALUES
	(4256097),
	(4269854),
	(4271382),
	(4272912),
	(4286669),
	(4286670);
