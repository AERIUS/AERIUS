INSERT INTO system.color_ranges 
	(color_range_type, lower_value, color)
VALUES
	('contribution_deposition', 0, 'fffdb3'),
	('contribution_deposition', 1.07, 'fde76a'),
	('contribution_deposition', 2.86, 'feb66e'),
	('contribution_deposition', 5, 'a5cc46'),
	('contribution_deposition', 7.14, '23a870'),
	('contribution_deposition', 10, '5a7a32'),
	('contribution_deposition', 15, '0093bd'),
	('contribution_deposition', 20, '0d75b5'),
	('contribution_deposition', 25, '6a70b1'),
	('contribution_deposition', 35.71, '304594'),
	('contribution_deposition', 71.43, '5e2c8f'),
	('contribution_deposition', 107.14, '3f2a84'),
	('contribution_deposition', 142.86, '2a1612');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('difference_deposition', '-infinity'::real, '507122'),
	('difference_deposition', -20, '507122'),
	('difference_deposition', -10, '738d4e'),
	('difference_deposition', -5, '96aa7a'),
	('difference_deposition', -2.86, 'b9c6a7'),
	('difference_deposition', -1.07, 'dce3d3'),
	('difference_deposition', 0, 'd8d3e5'),
	('difference_deposition', 1.07, 'b1a9cb'),
	('difference_deposition', 2.86, '8b7db0'),
	('difference_deposition', 5, '645296'),
	('difference_deposition', 10, '3d277c'),
	('difference_deposition', 20, '3d277c');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('in_combination_deposition', '-infinity'::real, '507122'),
	('in_combination_deposition', -20, '507122'),
	('in_combination_deposition', -10, '738d4e'),
	('in_combination_deposition', -5, '96aa7a'),
	('in_combination_deposition', -2.86, 'b9c6a7'),
	('in_combination_deposition', -1.07, 'dce3d3'),
	('in_combination_deposition', 0, 'd8d3e5'),
	('in_combination_deposition', 1.07, 'b1a9cb'),
	('in_combination_deposition', 2.86, '8b7db0'),
	('in_combination_deposition', 5, '645296'),
	('in_combination_deposition', 10, '3d277c'),
	('in_combination_deposition', 20, '3d277c');
