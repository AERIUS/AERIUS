INSERT INTO system.color_items
	(color_item_type, item_value, color, line_width, sort_order)
VALUES
	('road_type', 'FREEWAY', 'ec1c24', 8, 1),
	('road_type', 'URBAN_ROAD', '732c7c', 4, 4),

	('natura2000_directive', 'HR', 'f4e798', 0, 1),
	('natura2000_directive', 'VR', 'bbddea', 0, 2),
	('natura2000_directive', 'VR+HR', 'cfe2a1', 0, 3),
	('natura2000_directive', 'onbekend', 'd6b9d2', 0, 4);
