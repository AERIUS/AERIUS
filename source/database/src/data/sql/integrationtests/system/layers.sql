--layer legends
INSERT INTO system.layer_legends (layer_legend_id,legend_type,description) VALUES
	(1, 'circle',	'Natuurgebieden'),
	(3, 'circle',	'Stikstofgevoeligheid habitattypes');

--layer legend items
INSERT INTO system.layer_legend_color_items (layer_legend_id, sort_order, name, color) VALUES
	(1, 1, 'Habitatrichtlijn', 'F4E798'),
	(1, 2, 'Vogelrichtlijn', 'BBDDEA'),
	(1, 3, 'Vogelrichtlijn, Habitatrichtlijn', 'CFE2A1'),
	(1, 4, 'Niet bepaald', 'D6B9D2'),

	(3, 1, 'high_sensitivity', '7B3294'),
	(3, 2, 'normal_sensitivity', 'C2A5CF'),
	(3, 3, 'low_sensitivity', 'EBF5C8');

-- Layer bundles
INSERT INTO system.layer_bundles(bundle_id, bundle_name) VALUES
	(1, 'background'),
	(2, 'nitrogen_sensitivity'),
	(3, 'habitat_type');

--brtachtergrondkaarten
--luchtfoto https://www.pdok.nl/introductie/-/article/luchtfoto-pdok
INSERT INTO system.wmts_layer_properties (layer_properties_id, title, opacity, min_scale, max_scale, url, image_type, service_version, tile_matrix_set, projection, attribution, bundle_id) VALUES
	(5, 'Achtergrondkaart (Kleur)', 0.8, null, null, 'https://service.pdok.nl/brt/achtergrondkaart/wmts/v2_0', 'png8', '1.0.0', 'EPSG:28992', 'EPSG:28992', '&copy; OSM &amp; Kadaster', 1),
	(6, 'Luchtfoto (PDOK)', 1.0, null, null, 'https://service.pdok.nl/hwh/luchtfotorgb/wmts/v1_0/', 'jpeg', '1.0.0', 'EPSG:28992', 'EPSG:28992', NULL, 1);

--wms layers (for now ID's 10+)
INSERT INTO system.wms_layer_properties (layer_properties_id, title, opacity, layer_legend_id, min_scale, max_scale, url, dynamic_type, begin_year, bundle_id) VALUES
	(13, 'Stikstofgevoeligheid: Relevante habitats', 0.8, 3, 188000, null, '[AERIUS_GEOSERVER_HOST]', NULL, NULL, 2), --wms_habitat_areas_sensitivity_view
	(26, 'BAG', 0.4, null, 24000, null, 'https://service.pdok.nl/kadaster/bu/wms/v1_0?', NULL, NULL, NULL); -- BAG (not product specific)

--default layers
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) VALUES
	(4, 26, 'wms', 'BU.Building'),
	(5, 5, 'wmts', 'standaard'),
	(6, 6, 'wmts', 'Actueel_orthoHR');

-- i18n
INSERT INTO i18n.layers (layer_id, language_code, description) VALUES
	(4, 'nl', 'BAG'),
	(4, 'en', 'BAG'),
	(5, 'nl', 'Achtergrondkaart (Kleur)'),
	(5, 'en', 'Base Layer (Colour)'),
	(6, 'nl', 'Luchtfoto (PDOK)'),
	(6, 'en', 'Aerial (PDOK)'),
	(13, 'nl', 'Stikstofgevoeligheid: Relevante habitats'),
	(13, 'en', 'Nitrogen sensitivity: Relevant habitats');


--wms layers from 11+
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (13, 13, 'wms', 'calculator:wms_habitat_areas_sensitivity_view');

-- theme layers
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (5, 'own2000', 2, true, false); -- brtachtergrondkaart standaard
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (6, 'own2000', 5, true, false); -- luchtfoto
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (4, 'own2000', 6, false, false); -- bag kaart
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (13, 'own2000', 9, false, false); -- stikstofgevoeligheid
