-- Values picked due to OPSSourceConversionVerificationTest
-- sectors
WITH sector_links AS (
SELECT 'SECTOR' As type, unnest(ARRAY[3111, 3112, 3113]) AS old_value, 3100 AS new_value, false AS issue_warning
UNION
SELECT 'SECTOR' As type, 3230 AS old_value, 3220 AS new_value, false AS issue_warning
), gml_versions AS (
SELECT unnest(ARRAY['V1_0', 'V1_1', 'V2_0', 'V2_1', 'V2_2', 'V3_0', 'V3_1', 'V4_0']) AS gml_version
)
INSERT INTO system.gml_conversions (gml_version, type, old_value, new_value, issue_warning)
SELECT gml_version, type, old_value, new_value, false
	FROM sector_links
	CROSS JOIN gml_versions;

-- off road, mapping don't match actual values but works with what we got in data
WITH off_road_links AS (
SELECT
	'OFF_ROAD_MOBILE_SOURCE' As type, 
	unnest(ARRAY['P1991', 'S1C', 'S2E', 'S3AH', 'S3AI', 'S3AK', 'S4R1']) AS old_value, 
	'SI56DSN' AS new_value, true AS issue_warning
), gml_versions AS (
SELECT unnest(ARRAY['V1_0', 'V1_1', 'V2_0', 'V2_1', 'V2_2', 'V3_0', 'V3_1']) AS gml_version
)
INSERT INTO system.gml_conversions (gml_version, type, old_value, new_value, issue_warning)
SELECT gml_version, type, old_value, new_value, false
	FROM off_road_links
	CROSS JOIN gml_versions;

INSERT INTO system.gml_mobile_source_off_road_conversions (old_code, new_fuel_consumption)
VALUES 
	('P1991', 19.54),
	('S1C', 3.295),
	('S2E', 19.54),
	('S3AH', 19.54),
	('S3AI', 19.54),
	('S3AK', 3.295),
	('S4R1', 19.54);

-- farm, mapping don't match actual values but works with what we got in data
INSERT INTO system.gml_farm_lodging_conversions (old_code, animal_type_code, animal_housing_code, additional_system_code)
VALUES
	('A1.2', 'HA1', 'HA1.1', null),
	('D1.1.2', 'HA2', 'HA2.100', null),
	('E2.12.1', 'HE1', 'HE1.1.2.1', null);
