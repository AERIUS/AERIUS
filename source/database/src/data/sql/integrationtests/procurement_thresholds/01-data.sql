INSERT INTO procurement_thresholds (procurement_policy, threshold_type, key, threshold)
VALUES
	('wnb_lbv_plus', 'sum_deposition_national', 0, 2500),
	('wnb_lbv', 'sum_deposition_assessment_area', 57, 5300),
	('wnb_lbv', 'sum_deposition_assessment_area', 65, 7);
