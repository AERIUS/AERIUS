-- Subset of normal NL sector list.
-- Try to keep sector IDs in line with NL, but don't keep complete list.
INSERT INTO sectors (sector_id, description)
VALUES
	(1100, 'Voedings- en genotmiddelen'),
	(1800, 'Industrie Overig'),
	(3100, 'Rijdend verkeer'),
	(3150, 'Koude start: parkeergarage'),
	(3160, 'Koude start: overig'),
	(3210, 'Landbouw mobiele werktuigen'),
	(3220, 'Bouw, Industrie en Delfstoffenwinning'),
	(4110, 'Dierhuisvesting'),
	(4130, 'Beweiding'),
	(4140, 'Mestaanwending'),
	(7510, 'Zeescheepvaart: Aanlegplaats'),
	(7520, 'Zeescheepvaart: Binnengaats route'),
	(7530, 'Zeescheepvaart: Zeeroute'),
	(7610, 'Binnenvaart: Aanlegplaats'),
	(7620, 'Binnenvaart: Vaarroute'),
	(9999, 'Overig');

INSERT INTO emission_diurnal_variations (emission_diurnal_variation_id, code, name, description)
VALUES
	(0, 'CONTINUOUS', 'Continue emissie', 'Continue emissie in de tijd'),
	(1, 'INDUSTRIAL_ACTIVITY', 'Standaard profiel industrie', 'Volgens de (gemiddelde) industriële activiteit gedurende een werkdag'),
	(4, 'ANIMAL_HOUSING', 'Dierverblijven', 'Bijzondere waarde voor de verdamping emissies van NH3 en NOx uit dierverblijven'),
	(5, 'FERTILISER', 'Meststoffen', 'Bijzondere waarde voor de verdamping emissies van NH3 en NOx uit de toepassing van dierlijke mest en kunstmest'),
	(31, 'LIGHT_DUTY_VEHICLES', 'Licht verkeer', 'Volgens de (gemiddelde) verkeersintensiteit van lichte voertuigen'),
	(32, 'HEAVY_DUTY_VEHICLES', 'Zwaar verkeer', 'Volgens de (gemiddelde) verkeersintensiteit van zware voertuigen'),
	(33, 'BUSES', 'Bussen', 'Volgens de (gemiddelde) verkeersintensiteit van (openbaar vervoer) bussen');

INSERT INTO sector_default_source_characteristics (sector_id, heat_content, height, spread, emission_diurnal_variation_id, particle_size_distribution)
VALUES
	(1100, 0.34, 15, 7.5, 1, 0),
	(1800, 0.28, 22, 11, 1, 0),
	(3100, 0, 2.5, 2.5, 31, 0),
	(3150, 0, 0.3, 0.1, 31, 0),
	(3160, 0, 0.3, 0.1, 31, 0),
	(3210, 0.02, 2.5, 1.3, 1, 0),
	(3220, 0.035, 2.5, 1.3, 1, 0),
	(4110, 0, 5, 2.5, 4, 0),
	(4130, 0, 0.5, 0.3, 5, 0),
	(4140, 0, 0.5, 0.3, 5, 0),
	(7510, 0.15, 21.9, 10.9, 0, 0),
	(7520, 0.513, 13.5, 6.7, 0, 0),
	(7530, 0.676, 18.4, 9.2, 0, 0),
	(7610, 0.33, 3, 1.5, 1, 0),
	(7620, 0.33, 3, 1.5, 1, 0),
	(9999, 0, 0, 0, 0, 0);
