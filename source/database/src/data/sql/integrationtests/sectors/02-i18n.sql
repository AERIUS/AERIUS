-- i18n voor de module sectors
INSERT INTO i18n.sectors (sector_id, language_code, description)
SELECT sector_id, unnest(array['nl','en']::i18n.language_code_type[]), description FROM sectors;

INSERT INTO i18n.emission_diurnal_variations (emission_diurnal_variation_id, language_code, name, description)
SELECT emission_diurnal_variation_id, unnest(array['nl','en']::i18n.language_code_type[]), name, description FROM emission_diurnal_variations;
