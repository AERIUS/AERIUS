INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) 
VALUES 
	(1100, 'industry'),
	(1800, 'industry'),
	(3100, 'road_transportation'),
	(3150, 'road_transportation'),
	(3160, 'road_transportation'),
	(3210, 'mobile_equipment'),
	(3220, 'mobile_equipment'),
	(4110, 'agriculture'),
	(4130, 'farmland'),
	(4140, 'farmland'),
	(7510, 'shipping'),
	(7520, 'shipping'),
	(7530, 'shipping'),
	(7610, 'shipping'),
	(7620, 'shipping'),
	(9999, 'other');

INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) 
VALUES 
	(1100, 'GENERIC', 'OPS'),
	(1800, 'GENERIC', 'OPS'),
	(3100, 'SRM2_ROAD', 'ASRM2'),
	(3150, 'COLD_START_PARKING_GARAGE', 'OPS'),
	(3160, 'COLD_START_OTHER', 'OPS'),
	(3210, 'OFFROAD_MOBILE', 'OPS'),
	(3220, 'OFFROAD_MOBILE', 'OPS'),
	(4110, 'FARM_ANIMAL_HOUSING', 'OPS'),
	(4130, 'FARMLAND', 'OPS'),
	(4140, 'FARMLAND', 'OPS'),
	(7510, 'SHIPPING_MARITIME_DOCKED', 'OPS'),
	(7520, 'SHIPPING_MARITIME_INLAND', 'OPS'),
	(7530, 'SHIPPING_MARITIME_MARITIME', 'OPS'),
	(7610, 'SHIPPING_INLAND_DOCKED', 'OPS'),
	(7620, 'SHIPPING_INLAND', 'OPS'),
	(9999, 'GENERIC', 'OPS');
