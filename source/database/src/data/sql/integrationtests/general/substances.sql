INSERT INTO substances (substance_id, name, description)
VALUES
	(7, 'no2', 'Stikstofdioxide'),
	(10, 'pm10', 'Fijnstof'),
	(11, 'nox', 'Stikstofoxide'),
	(17, 'nh3', 'Ammoniak'),
	(1711, 'nox+nh3', 'Stikstofoxide + Ammoniak');
