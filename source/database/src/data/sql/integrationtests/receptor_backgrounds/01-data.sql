INSERT INTO background_years (background_year)
VALUES (2022);

INSERT INTO target_background_years (year, emission_result_type, substance_id, background_year)
SELECT generate_series(2024, 2040), 'deposition', 1711, 2022;

INSERT INTO receptor_background_results (background_year, emission_result_type, substance_id, receptor_id, result)
SELECT 2022, 'deposition', 1711, receptor_id, receptor_id * 0.001 FROM receptors;
