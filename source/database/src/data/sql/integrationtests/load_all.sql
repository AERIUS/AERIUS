{import_common 'general/constants.sql'}

{import_common 'general/substances.sql'}

{import_common 'sectors/'}

{import_common 'areas_hexagons_and_receptors/load.sql'}

{import_common 'background_cells/'}

{import_common 'receptor_backgrounds/'}

{import_common 'calculator/'}

{import_common 'shipping/'}

{import_common 'emission_factors/'}

{import_common 'procurement_thresholds/'}

{import_common 'system/'}

{import_common 'i18n/'}
