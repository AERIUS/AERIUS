INSERT INTO background_cells (background_cell_id, geometry)
VALUES (4282081, 'SRID=28992;POLYGON((167886.77779349062 447239.7904552958,167638.61649773008 447669.6184280252,167142.29390620894 447669.6184280252,166894.1326104484 447239.7904552958,167142.29390620894 446809.9624825664,167638.61649773008 446809.9624825664,167886.77779349062 447239.7904552958))');

INSERT INTO background_cell_depositions (background_cell_id, year, deposition)
SELECT 4282081, generate_series(2024, 2040), 3059.9001;

BEGIN;
	INSERT INTO background_cell_results
		SELECT background_cell_id, year, substance_id, result_type, result FROM setup.build_background_cell_results_view;
COMMIT;

BEGIN;
	INSERT INTO receptors_to_background_cells
		SELECT receptor_id, background_cell_id FROM setup.build_receptors_to_background_cells_view;
COMMIT;
