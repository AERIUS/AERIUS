BEGIN;
	INSERT INTO background_cell_results
		SELECT background_cell_id, year, substance_id, result_type, result FROM setup.build_background_cell_results_view;
COMMIT;

BEGIN;
	INSERT INTO receptors_to_background_cells
		SELECT receptor_id, background_cell_id FROM setup.build_receptors_to_background_cells_view;
COMMIT;
