BEGIN; SELECT setup.ae_load_table('sectors', '{data_folder}/public/sectors_20240614.txt', TRUE); COMMIT;

BEGIN; SELECT setup.ae_load_table('emission_diurnal_variations', '{data_folder}/public/emission_diurnal_variations_20170119.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('sector_default_source_characteristics', '{data_folder}/public/sector_default_source_characteristics_20240716.txt', TRUE); COMMIT;
