-- i18n voor de module sectors
BEGIN; SELECT setup.ae_load_table('i18n.sectors', '{data_folder}/i18n/i18n.sectors_20240620.txt', true); COMMIT;

BEGIN; SELECT setup.ae_load_table('i18n.emission_diurnal_variations', '{data_folder}/i18n/i18n.emission_diurnal_variations_20170119_nl.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('i18n.emission_diurnal_variations', '{data_folder}/i18n/i18n.emission_diurnal_variations_20170119_en.txt'); COMMIT;
