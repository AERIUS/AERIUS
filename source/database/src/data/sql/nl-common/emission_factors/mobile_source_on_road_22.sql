/* On road mobile source data */
BEGIN; SELECT setup.ae_load_table('mobile_source_on_road_categories', '{data_folder}/public/mobile_source_on_road_categories_20210618.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('mobile_source_on_road_category_emission_factors', '{data_folder}/public/mobile_source_on_road_category_emission_factors_20220719.txt', true); COMMIT;
