/* Farm animal category and animal housing data */
BEGIN; SELECT setup.ae_load_table('farm_animal_categories', '{data_folder}/public/farm_animal_categories_20240807.txt', TRUE); COMMIT;

BEGIN; SELECT setup.ae_load_table('farm_animal_housing_categories', '{data_folder}/public/farm_animal_housing_categories_20240807.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_animal_basic_housing', '{data_folder}/public/farm_animal_basic_housing_20240807.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_housing_emission_factors', '{data_folder}/public/farm_housing_emission_factors_20240807.txt', TRUE); COMMIT;

BEGIN; SELECT setup.ae_load_table('farm_additional_housing_systems', '{data_folder}/public/farm_additional_housing_systems_20240813.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_additional_housing_factors', '{data_folder}/public/farm_additional_housing_factors_20241004.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_housing_categories_additional_systems', '{data_folder}/public/farm_housing_categories_additional_systems_20241004.txt', TRUE); COMMIT;

/* Farmland categories */
BEGIN; SELECT setup.ae_load_table('farmland_categories', '{data_folder}/temp/temp_farmland_categories_20200527.txt'); COMMIT;
