/* On road mobile source data */
BEGIN; SELECT setup.ae_load_table('mobile_source_on_road_categories', '{data_folder}/public/mobile_source_on_road_categories_20240607.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('mobile_source_on_road_category_emission_factors', '{data_folder}/public/mobile_source_on_road_category_emission_factors_20240607.txt', TRUE); COMMIT;
