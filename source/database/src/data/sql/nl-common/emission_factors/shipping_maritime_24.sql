/* Maritime shipping data */
BEGIN; SELECT setup.ae_load_table('shipping_maritime_categories', '{data_folder}/public/shipping_maritime_categories_20140331.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('shipping_maritime_category_maneuver_properties', '{data_folder}/temp/temp_shipping_maritime_category_maneuver_properties_20140402.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('shipping_maritime_maneuver_areas', '{data_folder}/public/shipping_maritime_maneuver_areas_20240606.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('shipping_maritime_category_emission_factors', '{data_folder}/public/shipping_maritime_category_emission_factors_20230704.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('shipping_maritime_category_source_characteristics', '{data_folder}/public/shipping_maritime_category_source_characteristics_20240322.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('shipping_maritime_mooring_maneuver_factors', '{data_folder}/public/shipping_maritime_mooring_maneuver_factors_20210604.txt'); COMMIT;
