/* Maritime shipping data */
BEGIN; SELECT setup.ae_load_table('shipping_maritime_categories', '{data_folder}/public/shipping_maritime_categories_20140331.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('shipping_maritime_category_maneuver_properties', '{data_folder}/temp/temp_shipping_maritime_category_maneuver_properties_20140402.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('shipping_maritime_maneuver_areas', '{data_folder}/temp/temp_shipping_maritime_maneuver_areas_20140422.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('shipping_maritime_category_emission_factors', '{data_folder}/public/shipping_maritime_category_emission_factors_TEMP2035_20210616.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('shipping_maritime_category_source_characteristics', '{data_folder}/public/shipping_maritime_category_source_characteristics_TEMP2035_20210616.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('shipping_maritime_mooring_maneuver_factors', '{data_folder}/public/shipping_maritime_mooring_maneuver_factors_20210604.txt'); COMMIT;
