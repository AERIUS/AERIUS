/* Road data */
BEGIN; SELECT setup.ae_load_table('road_area_categories', '{data_folder}/public/road_area_categories_20220203.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('road_type_categories', '{data_folder}/public/road_type_categories_20220203.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('road_vehicle_categories', '{data_folder}/public/road_vehicle_categories_20220203.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('road_speed_profiles', '{data_folder}/public/road_speed_profiles_20220223.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('road_areas_to_road_types', '{data_folder}/public/road_areas_to_road_types_20220203.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('road_types_to_speed_profiles', '{data_folder}/public/road_types_to_speed_profiles_20220203.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('road_categories', '{data_folder}/public/road_categories_20220203.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('road_category_emission_factors', '{data_folder}/public/road_category_emission_factors_20220203.txt', TRUE); COMMIT;
