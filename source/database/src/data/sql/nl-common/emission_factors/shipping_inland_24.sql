/* Inland shipping data */
BEGIN; SELECT setup.ae_load_table('shipping_inland_categories', '{data_folder}/public/shipping_inland_categories_20230717.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('shipping_inland_category_source_characteristics', '{data_folder}/public/shipping_inland_category_source_characteristics_20240322.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('shipping_inland_category_source_characteristics_docked', '{data_folder}/public/shipping_inland_category_source_characteristics_docked_20240322.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('shipping_inland_category_emission_factors', '{data_folder}/public/shipping_inland_category_emission_factors_20230707.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('shipping_inland_category_emission_factors_docked', '{data_folder}/public/shipping_inland_category_emission_factors_docked_20230707.txt', TRUE); COMMIT;
