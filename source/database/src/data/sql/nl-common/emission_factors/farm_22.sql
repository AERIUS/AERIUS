/* Farm animal category and -lodging data */
BEGIN; SELECT setup.ae_load_table('farm_animal_categories_deprecated', '{data_folder}/public/farm_animal_categories_20210607.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_lodging_system_definitions', '{data_folder}/public/farm_lodging_system_definitions_20211026.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('farm_lodging_types', '{data_folder}/temp/temp_farm_lodging_types_20220913.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_lodging_type_emission_factors', '{data_folder}/public/farm_lodging_type_emission_factors_20221101.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_lodging_types_other_lodging_type', '{data_folder}/public/farm_lodging_types_other_lodging_type_20211026.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_lodging_types_to_lodging_system_definitions', '{data_folder}/public/farm_lodging_types_to_lodging_system_definitions_20221101.txt',TRUE); COMMIT;

BEGIN; SELECT setup.ae_load_table('farm_additional_lodging_systems', '{data_folder}/public/farm_additional_lodging_systems_20210607.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_additional_lodging_system_emission_factors', '{data_folder}/public/farm_additional_lodging_system_emission_factors_20210607.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_additional_lodging_systems_to_lodging_system_definitions', '{data_folder}/public/farm_additional_lodging_systems_to_lodging_system_definitions_20210607.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_lodging_types_to_additional_lodging_systems', '{data_folder}/public/farm_lodging_types_to_additional_lodging_systems_20200914.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('farm_reductive_lodging_systems', '{data_folder}/public/farm_reductive_lodging_systems_20211026.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_reductive_lodging_system_reduction_factors', '{data_folder}/public/farm_reductive_lodging_system_reduction_factors_20211026.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_reductive_lodging_systems_to_lodging_system_definitions', '{data_folder}/public/farm_reductive_lodging_systems_to_lodging_system_definitions_20221101.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_lodging_types_to_reductive_lodging_systems', '{data_folder}/public/farm_lodging_types_to_reductive_lodging_systems_20210607.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('farm_lodging_fodder_measures', '{data_folder}/public/farm_lodging_fodder_measures_20210607.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_lodging_fodder_measure_reduction_factors', '{data_folder}/public/farm_lodging_fodder_measure_reduction_factors_20210607.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_lodging_fodder_measures_animal_category', '{data_folder}/public/farm_lodging_fodder_measures_animal_category_20210628.txt'); COMMIT;

/* Farmland categories */
BEGIN; SELECT setup.ae_load_table('farmland_categories', '{data_folder}/temp/temp_farmland_categories_20200527.txt'); COMMIT;
