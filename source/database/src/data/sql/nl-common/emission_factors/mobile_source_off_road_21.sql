/* Off road mobile source data */
BEGIN; SELECT setup.ae_load_table('mobile_source_off_road_categories', '{data_folder}/public/mobile_source_off_road_categories_20210923.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('mobile_source_off_road_category_emission_factors', '{data_folder}/public/mobile_source_off_road_category_emission_factors_20210922.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('mobile_source_off_road_category_adblue_properties', '{data_folder}/public/mobile_source_off_road_category_adblue_properties_20210929.txt', TRUE); COMMIT;
