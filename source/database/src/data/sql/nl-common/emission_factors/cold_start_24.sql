/* Cold start data */
BEGIN; SELECT setup.ae_load_table('cold_start_standard_emission_factors', '{data_folder}/public/cold_start_standard_emission_factors_20240320.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('cold_start_specific_emission_factors', '{data_folder}/public/cold_start_specific_emission_factors_20240607.txt', TRUE); COMMIT;

BEGIN; SELECT setup.ae_load_table('cold_start_source_characteristics', '{data_folder}/public/cold_start_source_characteristics_20240715.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('cold_start_standard_characteristics', '{data_folder}/public/cold_start_standard_characteristics_20240704.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('cold_start_specific_characteristics', '{data_folder}/public/cold_start_specific_characteristics_20240705.txt', TRUE); COMMIT;
