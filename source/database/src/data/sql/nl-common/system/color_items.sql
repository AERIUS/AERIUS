INSERT INTO system.color_items
	(color_item_type, item_value, color, line_width, sort_order)
VALUES
	('road_type', 'FREEWAY', 'ec1c24', 8, 1),
	('road_type', 'NON_URBAN_ROAD_NATIONAL', 'd86c36', 6, 2),
	-- Uncommented types that are currently hidden.
	--('road_type', 'NON_URBAN_ROAD_GENERAL', 'f4b06c', 6, 3),
	('road_type', 'URBAN_ROAD_NORMAL', '732c7c', 4, 4),
	('road_type', 'URBAN_ROAD_STAGNATING', '431c53', 4, 5),
	('road_type', 'URBAN_ROAD_FREE_FLOW', '937ca0', 4, 6),
	
	('road_elevation_type', 'NORMAL', '23babb', 6, 1),
	('road_elevation_type', 'NORMAL_DYKE', 'fa7f07', 6, 2),
	('road_elevation_type', 'STEEP_DYKE', 'f24406', 6, 3),
	('road_elevation_type', 'VIADUCT', '0897b4', 6, 4),
	('road_elevation_type', 'TUNNEL', 'bda524', 6, 5),

	('natura2000_directive', 'HR', 'f4e798', 0, 1),
	('natura2000_directive', 'VR', 'bbddea', 0, 2),
	('natura2000_directive', 'VR+HR', 'cfe2a1', 0, 3),
	('natura2000_directive', 'onbekend', 'd6b9d2', 0, 4);

INSERT INTO system.color_items
	(color_item_type, item_value, color, line_width, sort_order)
VALUES
	('road_barrier_type', 'SCREEN', '0071bc', 4, 1),
	('road_barrier_type', 'WALL', 'be1719', 4, 2);
