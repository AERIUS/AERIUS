/*
 * Hide the following categories: 
 * - Buitenweg (met stagnatie-effect)
 */
INSERT INTO system.hide_road_type_categories (road_type_category_id)
SELECT road_type_category_id
	FROM road_type_categories
	WHERE code IN ('NON_URBAN_ROAD_GENERAL');
