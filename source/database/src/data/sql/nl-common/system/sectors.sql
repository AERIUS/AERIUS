/*
 * Insert sector_calculation_properties.
 */

INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (2100, 'GENERIC', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (4110, 'FARM_ANIMAL_HOUSING', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (4120, 'GENERIC', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (4130, 'FARMLAND', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (4140, 'FARMLAND', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (4150, 'FARMLAND', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (4200, 'FARMLAND', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (4320, 'GENERIC', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (4400, 'FARMLAND', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (4600, 'GENERIC', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (8640, 'GENERIC', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (8200, 'GENERIC', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (8210, 'GENERIC', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (1050, 'GENERIC', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (1100, 'GENERIC', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (1300, 'GENERIC', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (1400, 'GENERIC', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (1500, 'GENERIC', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (1700, 'GENERIC', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (1800, 'GENERIC', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (3210, 'OFFROAD_MOBILE', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (3220, 'OFFROAD_MOBILE', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (3530, 'OFFROAD_MOBILE', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (3710, 'GENERIC', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (3720, 'GENERIC', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (3610, 'GENERIC', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (3620, 'GENERIC', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (3630, 'GENERIC', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (3640, 'GENERIC', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (3100, 'SRM2_ROAD', 'ASRM2');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (7510, 'SHIPPING_MARITIME_DOCKED', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (7520, 'SHIPPING_MARITIME_INLAND', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (7530, 'SHIPPING_MARITIME_MARITIME', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (7610, 'SHIPPING_INLAND_DOCKED', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (7620, 'SHIPPING_INLAND', 'OPS');
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine) VALUES (9999, 'GENERIC', 'OPS');
-- Avoid trying to add sectors that are not available (stable and check don't know these sectors, but latest does. This works for both)
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine)
	SELECT sector_id, 'COLD_START_PARKING_GARAGE', 'OPS' FROM sectors WHERE sector_id = 3150;
INSERT INTO system.sector_calculation_properties (sector_id, emission_calculation_method, calculation_engine)
	SELECT sector_id, 'COLD_START_OTHER', 'OPS' FROM sectors WHERE sector_id = 3160;
