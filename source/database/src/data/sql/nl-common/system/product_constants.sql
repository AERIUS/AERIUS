/*
 * Product constants, which can be different between products, but the same across versions.
 */

INSERT INTO system.constants (key, value) VALUES ('DEFAULT_LOCALE', 'nl');

-- Release (valid values: PRODUCTION, CONCEPT, DEPRECATED)
INSERT INTO system.constants (key, value) VALUES ('RELEASE', 'CONCEPT');

-- Calculation config for workers
INSERT INTO system.constants (key, value) VALUES ('CHUNKER_ENGINE_UNITS_WORKER_MAX', '100000');
INSERT INTO system.constants (key, value) VALUES ('CHUNKER_RECEPTORS_WORKER_MIN', '5');
INSERT INTO system.constants (key, value) VALUES ('CHUNKER_RECEPTORS_WORKER_MAX', '850000'); -- higher will crash ops

-- Calculation config for the UI
INSERT INTO system.constants (key, value) VALUES ('CHUNKER_ENGINE_UNITS_UI_MAX', '20000');
INSERT INTO system.constants (key, value) VALUES ('CHUNKER_RECEPTORS_UI_MIN', '5');
INSERT INTO system.constants (key, value) VALUES ('CHUNKER_RECEPTORS_UI_MAX', '2000');
INSERT INTO system.constants (key, value) VALUES ('MAX_ENGINE_SOURCES_UI', '1000000');

-- Emission result display settings
INSERT INTO system.constants (key, value) VALUES ('EMISSION_RESULT_DISPLAY_CONVERSION_FACTOR', 1);
INSERT INTO system.constants (key, value) VALUES ('EMISSION_RESULT_DISPLAY_UNIT', 'MOLAR_UNITS'); -- Option of: MOLAR_UNITS, KILOGRAM_UNITS
INSERT INTO system.constants (key, value) VALUES ('EMISSION_RESULT_DISPLAY_ROUNDING_LENGTH', 2);
INSERT INTO system.constants (key, value) VALUES ('EMISSION_RESULT_DISPLAY_PRECISE_ROUNDING_LENGTH', 2);
INSERT INTO system.constants (key, value) VALUES ('EMISSION_RESULT_DISPLAY_DEPOSITION_SUM_ROUNDING_LENGTH', 0);

-- Help URLs
INSERT INTO system.constants (key, value) VALUES ('MANUAL_URL', 'https://link.aerius.nl/documenten/calculator/handboek_2024.pdf');
INSERT INTO system.constants (key, value) VALUES ('RELEASE_DETAILS_URL', 'https://link.aerius.nl/website');
INSERT INTO system.constants (key, value) VALUES ('MANUAL_BASE_URL_CALCULATOR', 'https://docs.aerius.nl');
INSERT INTO system.constants (key, value) VALUES ('MANUAL_BASE_URL_LBV_POLICY', 'https://docs.aerius.nl');
INSERT INTO system.constants (key, value) VALUES ('NEXT_STEPS_URL', 'https://www.rvo.nl/onderwerpen/minder-stikstofuitstoot-agrarisch-ondernemer');
INSERT INTO system.constants (key, value) VALUES ('NEXT_STEPS_SECONDARY_URL', 'https://www.onslevendlandschap.nl/aanpak-piekbelasting');

-- Google Tag Manager constants - we don't have any for NL at this time
INSERT INTO system.constants (key, value) VALUES ('GOOGLE_TAG_MANAGER_ENABLE', 'false');
INSERT INTO system.constants (key, value) VALUES ('GOOGLE_TAG_MANAGER_CONTAINER_ID', '');

-- Driving side constants
INSERT INTO system.constants (key, value) VALUES ('DRIVING_SIDE', 'RIGHT');

-- Characteristics type constants
INSERT INTO system.constants (key, value) VALUES ('CHARACTERISTICS_TYPE', 'OPS');

-- Default netting factor
INSERT INTO system.constants (key, value) VALUES ('DEFAULT_NETTING_FACTOR', '0.3');

-- Default calculation point placement radius
INSERT INTO system.constants (key, value) VALUES ('DEFAULT_CALCULATION_POINTS_PLACEMENT_RADIUS', '25000');

-- Collect feedback constants
INSERT INTO system.constants (key, value) VALUES ('COLLECT_FEEDBACK', 'true');
INSERT INTO system.constants (key, value) VALUES ('COLLECT_COLLECTOR_ID', '7130abc1');

-- Easter
INSERT INTO system.constants (key, value) VALUES ('EASTER_MODULE_ENABLE', 'true');
