--layer legends
INSERT INTO system.layer_legends (layer_legend_id,legend_type,description) VALUES
	(1, 'circle',	'Natuurgebieden'),
	(3, 'circle',	'Stikstofgevoeligheid habitattypes');

--layer legend items
INSERT INTO system.layer_legend_color_items (layer_legend_id, sort_order, name, color) VALUES
	(1, 1, 'Habitatrichtlijn', 'F4E798'),
	(1, 2, 'Vogelrichtlijn', 'BBDDEA'),
	(1, 3, 'Vogelrichtlijn, Habitatrichtlijn', 'CFE2A1'),
	(1, 4, 'Niet bepaald', 'D6B9D2'),

	(3, 1, 'high_sensitivity', '7B3294'),
	(3, 2, 'normal_sensitivity', 'C2A5CF'),
	(3, 3, 'low_sensitivity', 'EBF5C8');

-- Layer bundles
INSERT INTO system.layer_bundles(bundle_id, bundle_name) VALUES
	(1, 'background'),
	(2, 'nitrogen_sensitivity'),
	(3, 'habitat_type');

--brtachtergrondkaarten
--luchtfoto https://www.pdok.nl/introductie/-/article/luchtfoto-pdok
INSERT INTO system.wmts_layer_properties (layer_properties_id, title, opacity, min_scale, max_scale, url, image_type, service_version, tile_matrix_set, projection, attribution, bundle_id) VALUES
	(5, 'Achtergrondkaart (Kleur)', 0.8, null, null, 'https://service.pdok.nl/brt/achtergrondkaart/wmts/v2_0', 'png8', '1.0.0', 'EPSG:28992', 'EPSG:28992', '&copy; OSM &amp; Kadaster', 1),
	(6, 'Luchtfoto (PDOK)', 1.0, null, null, 'https://service.pdok.nl/hwh/luchtfotorgb/wmts/v1_0/', 'jpeg', '1.0.0', 'EPSG:28992', 'EPSG:28992', NULL, 1),
	(7, 'Achtergrondkaart (Pastel)', 0.8, null, null, 'https://service.pdok.nl/brt/achtergrondkaart/wmts/v2_0', 'png8', '1.0.0', 'EPSG:28992', 'EPSG:28992', '&copy; OSM &amp; Kadaster', 1),
	(8, 'Achtergrondkaart', 0.8, null, null, 'https://service.pdok.nl/brt/achtergrondkaart/wmts/v2_0', 'png8', '1.0.0', 'EPSG:28992', 'EPSG:28992', '&copy; OSM &amp; Kadaster', 1),
	(9, 'Achtergrondkaart (Water)', 0.8, null, null, 'https://service.pdok.nl/brt/achtergrondkaart/wmts/v2_0', 'png8', '1.0.0', 'EPSG:28992', 'EPSG:28992', '&copy; OSM &amp; Kadaster', 1);

--wms layers (for now ID's 10+)
INSERT INTO system.wms_layer_properties (layer_properties_id, title, opacity, layer_legend_id, min_scale, max_scale, url, dynamic_type, begin_year, bundle_id) VALUES
	(10, NULL, 1.0, null, 188000, null, '[AERIUS_GEOSERVER_HOST]', NULL, NULL, NULL), --default WMS layer properties
	(11, 'Natuurgebieden', 0.8, 1, 1504000, null, '[AERIUS_GEOSERVER_HOST]', NULL, NULL, NULL), --wms_nature_areas_view
	(13, 'Stikstofgevoeligheid: Relevante habitats', 0.8, 3, 188000, null, '[AERIUS_GEOSERVER_HOST]', NULL, NULL, 2), --wms_habitat_areas_sensitivity_view
	(14, 'Habitats: Relevante habitattypen', 1.0, null, 1504000, null, '[AERIUS_GEOSERVER_HOST]', 'habitat_type', NULL, 3), --wms_habitat_types
	(15, 'Zeescheepvaart netwerk', 1.0, null, 1504000, null, 'https://geo.rijkswaterstaat.nl/services/ogc/gdr/verkeersscheidingsstelsel_nz/wms?', NULL, NULL, NULL), --nzvss_beg,nzvss_sym,nzvss_sep (zeescheepvaart)
	(16, 'Scheepvaart netwerk', 1.0, null, 1504000, null, '[AERIUS_GEOSERVER_HOST]', NULL, NULL, NULL), --wms_shipping_maritime_network_view
	(26, 'BAG', 0.4, null, 24000, null, 'https://service.pdok.nl/kadaster/bu/wms/v1_0?', NULL, NULL, NULL), -- BAG (not product specific)
	(28, 'Provinciegrenzen', 0.8, null, null, null, '[AERIUS_GEOSERVER_HOST]', NULL, NULL, NULL), -- wms_province_areas_view (not product specific)
	(29, 'Binnenvaart netwerk', 1.0, null, 1504000, null, '[AERIUS_GEOSERVER_HOST]', NULL, NULL, NULL), -- wms_inland_shipping_routes (calculator/scenario)
	(30, 'Stikstofgevoeligheid: Habitats met hersteldoelen', 0.8, 3, 188000, null, '[AERIUS_GEOSERVER_HOST]', NULL, NULL, 2), --wms_extra_assessment_hexagons_sensitivity_view
	(31, 'Habitattypes: Habitats met hersteldoelen', 0.8, null, 188000, null, '[AERIUS_GEOSERVER_HOST]', 'habitat_type', NULL, 3); --wms_extra_assessment_hexagons_view

--default layers
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) VALUES
	(4, 26, 'wms', 'BU.Building'),
	(5, 5, 'wmts', 'standaard'),
	(6, 6, 'wmts', 'Actueel_orthoHR'),
	(7, 7, 'wmts', 'pastel'),
	(8, 8, 'wmts', 'grijs'),
	(9, 9, 'wmts', 'water');

-- i18n
INSERT INTO i18n.layers (layer_id, language_code, description) VALUES
	(4, 'nl', 'BAG'),
	(4, 'en', 'BAG'),
	(5, 'nl', 'Achtergrondkaart (Kleur)'),
	(5, 'en', 'Base Layer (Colour)'),
	(6, 'nl', 'Luchtfoto (PDOK)'),
	(6, 'en', 'Aerial (PDOK)'),
	(7, 'nl', 'Achtergrondkaart (Pastel)'),
	(7, 'en', 'Base Layer (Pastel)'),
	(8, 'nl', 'Achtergrondkaart'),
	(8, 'en', 'Base Layer'),
	(9, 'nl', 'Achtergrondkaart (Water)'),
	(9, 'en', 'Base Layer (Water)'),
	(11, 'nl', 'Natuurgebieden'),
	(11, 'en', 'Ecological sites'),
	(13, 'nl', 'Stikstofgevoeligheid: Relevante habitats'),
	(13, 'en', 'Nitrogen sensitivity: Relevant habitats'),
	(14, 'nl', 'Habitattypen: Relevante habitattypen'),
	(14, 'en', 'Habitat types: Relevant habitats'),
	(15, 'nl', 'Zeescheepvaart netwerk'),
	(15, 'en', 'Maritime shipping network'),
	(16, 'nl', 'Scheepvaart netwerk'),
	(16, 'en', 'Shipping network'),
	(21, 'nl', 'Provinciegrenzen'),
	(21, 'en', 'Provincial boundaries'),
	(22, 'nl', 'Binnenvaart netwerk'),
	(22, 'en', 'Inland shipping network'),
	(30, 'nl', 'Stikstofgevoeligheid: Habitats met hersteldoelen'),
	(30, 'en', 'Nitrogen sensitivity: Habitats with recovery goals'),
	(31, 'nl', 'Habitattypen: Habitats met hersteldoelen'),
	(31, 'en', 'Habitat types: Habitats with recovery goals');


--wms layers from 11+
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (13, 13, 'wms', 'calculator:wms_habitat_areas_sensitivity_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (14, 14, 'wms', 'calculator:wms_habitat_types');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (15, 15, 'wms', 'verkeersscheidingsstelsel_nz:begrenzing,verkeersscheidingsstelsel_nz:symbolen,verkeersscheidingsstelsel_nz:separatiezones');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (16, 16, 'wms', 'calculator:wms_shipping_maritime_network_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (18, 10, 'wms', 'calculator:wms_relevant_habitat_info_for_receptor_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (21, 28, 'wms', 'calculator:wms_province_areas_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (22, 29, 'wms', 'calculator:wms_inland_shipping_routes');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (30, 30, 'wms', 'calculator:wms_extra_assessment_hexagons_sensitivity_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (31, 31, 'wms', 'calculator:wms_extra_assessment_hexagons_view');

-- theme layers
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (5, 'own2000', 2, true, false); -- brtachtergrondkaart standaard
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (7, 'own2000', 1, true, false); -- brtachtergrondkaart pastel
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (8, 'own2000', 0, true, true); -- brtachtergrondkaart grijs
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (9, 'own2000', 3, true, false); -- brtachtergrondkaart water
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (21, 'own2000', 4, true, true); -- provinciegrenzen
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (6, 'own2000', 5, true, false); -- luchtfoto
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (4, 'own2000', 6, false, false); -- bag kaart
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (13, 'own2000', 9, false, false); -- stikstofgevoeligheid
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (14, 'own2000', 10, false, false); --habittattypen
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (15, 'own2000', 11, false, false); -- zeescheepvaart netwerk
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (16, 'own2000', 12, false, false); -- scheepvaart netwerk
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (22, 'own2000', 13, false, false); -- binnenvaart netwerk
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (30, 'own2000', 14, false, false); -- herstel hexagonen
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (31, 'own2000', 15, false, false); -- herstel hexagonen
