INSERT INTO system.meteo (code, description, start_year, end_year)
		SELECT year::text AS code, year::text AS description, year AS start_year, year AS end_year from generate_series(1981, 2023) AS year;

INSERT INTO system.meteo (code, description, start_year, end_year)
	VALUES ('1995-2004', '1995 - 2004', 1995, 2004);

INSERT INTO system.meteo (code, description, start_year, end_year)
	VALUES ('2005-2014', '2005 - 2014', 2005, 2014);

INSERT INTO system.meteo (code, description, start_year, end_year)
	VALUES ('2014-2023', '2014 - 2023', 2014, 2023);
