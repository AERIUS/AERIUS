BEGIN; SELECT setup.ae_load_table('system.gml_conversions', '{data_folder}/system/system.gml_conversions_20150204.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('system.gml_conversions', '{data_folder}/system/system.gml_conversions_farm_lodging_20150204.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('system.gml_conversions', '{data_folder}/system/system.gml_conversions_on_road_mobile_source_20210618.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('system.gml_conversions', '{data_folder}/system/gml_conversions_off_road_mobile_source_20210924.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('system.gml_conversions', '{data_folder}/system/system.gml_conversions_sector_20220223.txt', TRUE); COMMIT;

BEGIN; SELECT setup.ae_load_table('system.gml_mobile_source_off_road_conversions', '{data_folder}/system/gml_mobile_source_off_road_conversions_20211125.txt', TRUE); COMMIT;

BEGIN; SELECT setup.ae_load_table('system.gml_plan_conversions', '{data_folder}/system/system.gml_plan_conversions_20240323.txt', TRUE); COMMIT;

BEGIN; SELECT setup.ae_load_table('system.gml_farm_lodging_conversions', '{data_folder}/system/system.gml_farm_lodging_conversions_20241003.txt', TRUE); COMMIT;
