INSERT INTO system.color_ranges 
	(color_range_type, lower_value, color)
VALUES
	('contribution_deposition', 0, 'fffdb3'),
	('contribution_deposition', 1.07, 'fde76a'),
	('contribution_deposition', 2.86, 'feb66e'),
	('contribution_deposition', 5, 'a5cc46'),
	('contribution_deposition', 7.14, '23a870'),
	('contribution_deposition', 10, '5a7a32'),
	('contribution_deposition', 15, '0093bd'),
	('contribution_deposition', 20, '0d75b5'),
	('contribution_deposition', 25, '6a70b1'),
	('contribution_deposition', 35.71, '304594'),
	('contribution_deposition', 71.43, '5e2c8f'),
	('contribution_deposition', 107.14, '3f2a84'),
	('contribution_deposition', 142.86, '2a1612');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('difference_deposition', '-infinity'::real, '507122'),
	('difference_deposition', -20, '507122'),
	('difference_deposition', -10, '738d4e'),
	('difference_deposition', -5, '96aa7a'),
	('difference_deposition', -2.86, 'b9c6a7'),
	('difference_deposition', -1.07, 'dce3d3'),
	('difference_deposition', 0, 'd8d3e5'),
	('difference_deposition', 1.07, 'b1a9cb'),
	('difference_deposition', 2.86, '8b7db0'),
	('difference_deposition', 5, '645296'),
	('difference_deposition', 10, '3d277c'),
	('difference_deposition', 20, '3d277c');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('in_combination_deposition', '-infinity'::real, '507122'),
	('in_combination_deposition', -20, '507122'),
	('in_combination_deposition', -10, '738d4e'),
	('in_combination_deposition', -5, '96aa7a'),
	('in_combination_deposition', -2.86, 'b9c6a7'),
	('in_combination_deposition', -1.07, 'dce3d3'),
	('in_combination_deposition', 0, 'd8d3e5'),
	('in_combination_deposition', 1.07, 'b1a9cb'),
	('in_combination_deposition', 2.86, '8b7db0'),
	('in_combination_deposition', 5, '645296'),
	('in_combination_deposition', 10, '3d277c'),
	('in_combination_deposition', 20, '3d277c');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('background_deposition', 0, 'FFFFD4'),
	('background_deposition', 700, 'FEE391'),
	('background_deposition', 980, 'FEC44F'),
	('background_deposition', 1260, 'FE9929'),
	('background_deposition', 1540, 'EC7014'),
	('background_deposition', 1960, 'CC4C02'),
	('background_deposition', 2240, '8C2D04');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('road_max_speed', 80, 'd9e021'),
	('road_max_speed', 100, 'ffe603'),
	('road_max_speed', 120, 'f7931e'),
	('road_max_speed', 130, 'ec1c24');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('road_barrier_height', 0, 'eace00'),
	('road_barrier_height', 3, 'f7931e'),
	('road_barrier_height', 6, 'ec1c24');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('road_traffic_volume', 0, '095da7'),
	('road_traffic_volume', 10000, '095da7'),
	('road_traffic_volume', 20000, '095da7'),
	('road_traffic_volume', 30000, '095da7'),
	('road_traffic_volume', 50000, '095da7');
	
INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('road_traffic_volume_light_traffic', 0, '095da7'),
	('road_traffic_volume_light_traffic', 10000, '095da7'),
	('road_traffic_volume_light_traffic', 20000, '095da7'),
	('road_traffic_volume_light_traffic', 30000, '095da7'),
	('road_traffic_volume_light_traffic', 50000, '095da7');
	
INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('road_traffic_volume_normal_traffic', 0, '095da7'),
	('road_traffic_volume_normal_traffic', 10000, '095da7'),
	('road_traffic_volume_normal_traffic', 20000, '095da7'),
	('road_traffic_volume_normal_traffic', 30000, '095da7'),
	('road_traffic_volume_normal_traffic', 50000, '095da7');
	
INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('road_traffic_volume_heavy_freight', 0, '095da7'),
	('road_traffic_volume_heavy_freight', 10000, '095da7'),
	('road_traffic_volume_heavy_freight', 20000, '095da7'),
	('road_traffic_volume_heavy_freight', 30000, '095da7'),
	('road_traffic_volume_heavy_freight', 50000, '095da7');
	
INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('road_traffic_volume_auto_bus', 0, '095da7'),
	('road_traffic_volume_auto_bus', 10000, '095da7'),
	('road_traffic_volume_auto_bus', 20000, '095da7'),
	('road_traffic_volume_auto_bus', 30000, '095da7'),
	('road_traffic_volume_auto_bus', 50000, '095da7');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('road_elevation_height', '-infinity'::real, '0897b4'),
	('road_elevation_height', 0, 'ffec5c'),
	('road_elevation_height', 0.00001, 'fdc27f'),
	('road_elevation_height', 3, 'fa7f07'),
	('road_elevation_height', 6, 'f24406');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('road_tunnelfactor', '-infinity'::real, 'fa7f07'),
	('road_tunnelfactor', 1, 'bda524'),
	('road_tunnelfactor', 1.00001, '732c7c');
