BEGIN; SELECT setup.ae_load_table('setup.province_land_borders', '{data_folder}/setup/setup.province_land_borders_20131129.txt'); COMMIT;


BEGIN; SELECT setup.ae_load_table('countries', '{data_folder}/temp/temp_countries_20150721.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('authorities', '{data_folder}/public/authorities_20191105.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('natura2000_areas', '{data_folder}/public/natura2000_areas_20201113.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('natura2000_directives', '{data_folder}/public/natura2000_directives_20220328.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('natura2000_directive_areas', '{data_folder}/public/natura2000_directive_areas_20220328.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('natura2000_area_properties', '{data_folder}/public/natura2000_area_properties_20200714.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('habitat_types', '{data_folder}/public/habitat_types_20200730.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('habitat_type_critical_levels', '{data_folder}/public/habitat_type_critical_levels_20200730.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('habitat_areas', '{data_folder}/public/habitat_areas_20210903.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('habitat_properties', '{data_folder}/public/habitat_properties_20200727.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('habitat_type_relations', '{data_folder}/public/habitat_type_relations_20200730.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('species', '{data_folder}/public/species_20200526.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('species_properties', '{data_folder}/public/species_properties_20200727.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('species_to_habitats', '{data_folder}/public/species_to_habitats_20200714.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('designated_species', '{data_folder}/public/designated_species_20200727.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('province_areas', '{data_folder}/public/province_areas_20210527.txt'); COMMIT;
