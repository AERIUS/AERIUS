BEGIN; SELECT setup.ae_load_table('setup.province_land_borders', '{data_folder}/setup/setup.province_land_borders_20131129.txt'); COMMIT;


BEGIN; SELECT setup.ae_load_table('countries', '{data_folder}/temp/temp_countries_20150721.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('authorities', '{data_folder}/public/authorities_20221115.txt', TRUE); COMMIT;

BEGIN; SELECT setup.ae_load_table('natura2000_areas', '{data_folder}/public/natura2000_areas_20230503.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('natura2000_directives', '{data_folder}/public/natura2000_directives_20220328.txt', true); COMMIT;
BEGIN; SELECT setup.ae_load_table('natura2000_directive_areas', '{data_folder}/public/natura2000_directive_areas_20230503.txt', true); COMMIT;
BEGIN; SELECT setup.ae_load_table('natura2000_area_properties', '{data_folder}/public/natura2000_area_properties_20230404.txt', true); COMMIT;

BEGIN; SELECT setup.ae_load_table('habitat_types', '{data_folder}/public/habitat_types_20230619.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('habitat_type_critical_levels', '{data_folder}/public/habitat_type_critical_levels_20230619.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('habitat_areas', '{data_folder}/public/habitat_areas_20230704.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('habitat_properties', '{data_folder}/public/habitat_properties_20221216.txt', true); COMMIT;
BEGIN; SELECT setup.ae_load_table('habitat_type_relations', '{data_folder}/public/habitat_type_relations_20230619.txt', TRUE); COMMIT;

BEGIN; SELECT setup.ae_load_table('species', '{data_folder}/public/species_20221216.txt', true); COMMIT;
BEGIN; SELECT setup.ae_load_table('species_properties', '{data_folder}/public/species_properties_20221216.txt', true); COMMIT;
BEGIN; SELECT setup.ae_load_table('species_to_habitats', '{data_folder}/public/species_to_habitats_20230516.txt', TRUE); COMMIT;

BEGIN; SELECT setup.ae_load_table('designated_species', '{data_folder}/public/designated_species_20221216.txt', true); COMMIT;

BEGIN; SELECT setup.ae_load_table('province_areas', '{data_folder}/public/province_areas_20220614.txt', true); COMMIT;
