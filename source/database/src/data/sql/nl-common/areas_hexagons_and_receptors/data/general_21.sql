{import_common 'areas_hexagons_and_receptors/data/general_base_21.sql'}


-- preprocessed

BEGIN; SELECT setup.ae_load_table('setup.geometry_of_interests', '{data_folder}/setup/setup.geometry_of_interests_20210924.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('receptors', '{data_folder}/public/receptors_20210924.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('hexagons', '{data_folder}/public/hexagons_20210924.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('receptors_to_assessment_areas', '{data_folder}/public/receptors_to_assessment_areas_20210924.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('relevant_habitat_areas', '{data_folder}/public/relevant_habitat_areas_20210924.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('habitats', '{data_folder}/public/habitats_20210924.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('relevant_habitats', '{data_folder}/public/relevant_habitats_20210924.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('receptors_to_critical_deposition_areas', '{data_folder}/public/receptors_to_critical_deposition_areas_20211015.txt'); COMMIT;


-- additional loads

BEGIN; SELECT setup.ae_load_table('terrain_properties', '{data_folder}/temp/temp_terrain_properties_20160404.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('system.habitat_type_colors', '{data_folder}/system/system.habitat_type_colors_20170421.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('non_exceeding_receptors', '{data_folder}/public/non_exceeding_receptors_20210927.txt'); COMMIT;
