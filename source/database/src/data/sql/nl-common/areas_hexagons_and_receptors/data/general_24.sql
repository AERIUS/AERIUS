{import_common 'areas_hexagons_and_receptors/data/general_base_24.sql'}


-- preprocessed

BEGIN; SELECT setup.ae_load_table('setup.geometry_of_interests', '{data_folder}/setup/setup.geometry_of_interests_20240625.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('receptors', '{data_folder}/public/receptors_20240625.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('hexagons', '{data_folder}/public/hexagons_20240625.txt'); COMMIT;

-- TODO: Feature branch remark, decide if this should be in this location.
-- This data is for new extra assessment receptors, is supplied by Michiel Schram, and might not be definitive.
-- This should be checked before merging to main.
BEGIN; SELECT setup.ae_load_table('extra_assessment_receptors', '{data_folder}/public/extra_assessment_receptors_20240815.txt', TRUE); COMMIT;


BEGIN; SELECT setup.ae_load_table('receptors_to_assessment_areas', '{data_folder}/public/receptors_to_assessment_areas_20240625.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('relevant_habitat_areas', '{data_folder}/public/relevant_habitat_areas_20240625.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('habitats', '{data_folder}/public/habitats_20240625.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('relevant_habitats', '{data_folder}/public/relevant_habitats_20240625.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('receptors_to_critical_deposition_areas', '{data_folder}/public/receptors_to_critical_deposition_areas_20240625.txt'); COMMIT;


-- additional loads

-- When updating terrain properties, be sure to update the file used by the worker as well. This file only has effect on OpenData.
BEGIN; SELECT setup.ae_load_table('terrain_properties', '{data_folder}/public/terrain_properties_20220128.txt'); COMMIT;


BEGIN; SELECT setup.ae_load_table('system.habitat_type_colors', '{data_folder}/system/system.habitat_type_colors_20230626.txt'); COMMIT;


BEGIN; SELECT setup.ae_load_table('non_exceeding_receptors', '{data_folder}/public/non_exceeding_receptors_20240614.txt', TRUE); COMMIT;
