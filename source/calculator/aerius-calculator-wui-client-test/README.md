# Testing the web user interface with Cypress

Running the tests of the web user interface with Cypress can be done in 2 different ways.
First run with Maven, and second with Node.js.
This page describes how to start the tests.
The tests require some environment variables, and some are optional.
The following environment variables are available:

| Environment Variables       | Description                                                        | Required |
| --------------------------- | ------------------------------------------------------------------ | -------- |
| `BASE_URL_UI`               | URL to the AERIUS instance you want to test                        | Required |
| `API_KEY`                   | Connect API Key                                                    | Optional |
| `USERNAME`                  | Username if basic authentication is enabled                        | Optional |
| `PASSWORD`                  | Password if basic authentication is enabled                        | Optional |
| `SYSTEM_INFO_UUID`          | UUID for testing the System Information page                       | Optional |
| `TEST_PROFILE`              | Run tests for product [`UK`/`NL`/`LBV`] defaults to NL             | Optional |
| `SKIP_COOKIES_CHECK`        | If set cookie check in Cypress test is skipped                     | Optional |
| `MAX_TEST_DURATION_MINUTES` | Max time the maven test is expected to run. Default 30 minutes     | Optional |

In the `scripts/dev` map in the root of this project there is a script `cypress.sh` for easy starting Cypress tests.

## Using Maven

### Runs the unit tests in Docker container.

```bash
mvn test -DCYPRESS_BASE_URL=<base_url> [-DCYPRESS_API_KEY=<api_key>] [-DCYPRESS_USERNAME=<username>] [-DCYPRESS_PASSWORD=<password>] [-DCYPRESS_MAX_TEST_DURATION_MINUTES=<max_duration>] [-DCYPRESS_TEST_PROFILE=<test_profile>]
```
For the maven run all environment variables mentioned in the table above need to be prefixed with `CYPRESS_`.

## Using Node.js

### Cypress

Execute the commands from the `src/test/e2e` directory.

#### The project is set up as a Node.js project. Install modules and library based on the data from package.json

```bash
npm install
```

#### Run Cypress

```bash
npx cypress run --config baseUrl=[base_url] --env API_KEY=[api_key],USERNAME=[username],PASSWORD=[password],TEST_PROFILE=[test_profile]
```

#### Run Cypress interactive mode

```bash
npx cypress open --config baseUrl=[base_url] --env API_KEY=[api_key],USERNAME=[username],PASSWORD=[password],TEST_PROFILE=[test_profile]
```

## BrowserStack Cloud testing platform

Browserstack is used to run the UK tests in Cloud testing platform.

| Browserstack Parameter                 | Description                                              | Required |
| -------------------------------------- | -------------------------------------------------------- | -------- |
| `-- username [browser_stack_username]` | Username for browserstack credentials                    | Required |
| `--key [browserstack_key]`             | Key for browserstack credentials                         | Required |


#### Run Cypress on BROWSERSTACK

```bash
browserstack-cypress run --sync --username [browser_stack_username] --key [browserstack_key]
 --spec-timeout 120 --headless --config baseUrl=[base_url]--env USERNAME=[username]
,PASSWORD=[password],TEST_PROFILE=UK,BROWSERSTACK=true
```

#### Show reports

```bash
node ./cypress/reports/cucumber-html-report.js
```

### Developing the Cypress code

For documentation on the code style in the Cypress tests, including how to automatically format code using eslint and prettier, read [Code style](CodeStyle.md).
