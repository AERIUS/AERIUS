/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cypress.test;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import javax.validation.constraints.NotNull;

import org.junit.jupiter.api.DynamicContainer;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.testcontainers.images.PullPolicy;

import io.github.wimdeblauwe.testcontainers.cypress.CypressContainer;
import io.github.wimdeblauwe.testcontainers.cypress.CypressTest;
import io.github.wimdeblauwe.testcontainers.cypress.CypressTestResults;
import io.github.wimdeblauwe.testcontainers.cypress.CypressTestSuite;

/**
 * User interface Cypress Tests.
 */
class ClientCypressTest {

  private static final String CYPRESS_BASE_URL = "CYPRESS_BASE_URL";
  private static final String API_KEY = "CYPRESS_API_KEY";
  private static final String USERNAME = "CYPRESS_USERNAME";
  private static final String PASSWORD = "CYPRESS_PASSWORD";
  private static final String TEST_PROFILE = "CYPRESS_TEST_PROFILE";
  private static final String SYSTEM_INFO_UUID = "CYPRESS_SYSTEM_INFO_UUID";
  private static final String MAX_TEST_DURATION = "CYPRESS_MAX_TEST_DURATION_MINUTES";
  private static final String SKIP_COOKIES_CHECK = "SKIP_COOKIES_CHECK";
  private static final String TAGS = "CYPRESS_TAGS";
  private static final String NODE_ENV = "NODE_ENV";
  private static final Long DEFAULT_MAX_TESTDURATION = 30L;

  @TestFactory
  List<DynamicContainer> runCypressTests() throws InterruptedException, IOException, TimeoutException {
    try (final CypressContainer container = new CypressContainer()) {
      container.withBaseUrl(System.getProperty(CYPRESS_BASE_URL))
          .withImagePullPolicy(PullPolicy.alwaysPull())
          .withEnv(API_KEY, System.getProperty(API_KEY))
          .withEnv(USERNAME, System.getProperty(USERNAME))
          .withEnv(PASSWORD, System.getProperty(PASSWORD))
          .withEnv(TEST_PROFILE, System.getProperty(TEST_PROFILE))
          .withEnv(SKIP_COOKIES_CHECK, System.getProperty(SKIP_COOKIES_CHECK))
          .withEnv(TAGS, System.getProperty(TAGS))
          .withEnv(SYSTEM_INFO_UUID, System.getProperty(SYSTEM_INFO_UUID))
          .withEnv(NODE_ENV, "production")
          .withMaximumTotalTestDuration(maxTestDuration());
      container.start();
      final CypressTestResults testResults = container.getTestResults();
      return convertToJUnitDynamicTests(testResults);
    }
  }

  private Duration maxTestDuration() {
    final String duration = System.getProperty(MAX_TEST_DURATION);
    return Duration.ofMinutes(duration == null ? DEFAULT_MAX_TESTDURATION : Long.valueOf(duration));
  }

  @NotNull
  private List<DynamicContainer> convertToJUnitDynamicTests(final CypressTestResults testResults) {
    final List<DynamicContainer> dynamicContainers = new ArrayList<>();
    final List<CypressTestSuite> suites = testResults.getSuites();
    for (final CypressTestSuite suite : suites) {
      createContainerFromSuite(dynamicContainers, suite);
    }
    return dynamicContainers;
  }

  private void createContainerFromSuite(final List<DynamicContainer> dynamicContainers, final CypressTestSuite suite) {
    final List<DynamicTest> dynamicTests = new ArrayList<>();
    for (final CypressTest test : suite.getTests()) {
      dynamicTests.add(DynamicTest.dynamicTest(test.getDescription(),
          () -> assertTrue(test.isSuccess(), "DynamicTest failed: " + test.getDescription())));
    }
    dynamicContainers.add(DynamicContainer.dynamicContainer(suite.getTitle(), dynamicTests));
  }
}
