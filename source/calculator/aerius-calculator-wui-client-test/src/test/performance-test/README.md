# JMeter

JMeter is a tool to test functional behavior and measure performance

## Download and installation
JMeter can be downloaded and installed from https://jmeter.apache.org/
There is also a lot of documentation about JMeter on this website, 
To get started you can use the following tutorial: https://jmeter.apache.org/usermanual/get-started.html

After installing, you have to put the csv file 'test_variables' in the folder where you run the test or change folder at the CSV Data Set Config.
The url to the server and the (calculation) path is set in this file. 
You have to put this into the file after downloading. 
Also, you can copy the server name in JMeter if you find that easier.
In some cases you need a cookie to do the performance tests. 
The cookie can be found in the response from the website after you logged in.
You can paste the cookie in this file if using a cookie is necessary. 
Again, if you find it more convenient to paste it in the tests of JMeter itself, you can also do that. 

## Running JMeter
After this, you can run JMeter from the 'GUI mode' or the 'non GUI mode' (command line).
Opening JMeter in the 'GUI mode' can be done by clicking on the icon.
Opening JMeter in the 'non GUI mode' can be done with the following command (Windows):

```bash
jmeter -n -t my_test.jmx
```

## JMeter tests
There are tests for the NL environment and the UK environment. 
In both cases JSON files are sent to the backend either sequential or parallel.
You can paste the JSON files in JMeter or put them in the folder and use a command in the GUI of JMeter to load the JSON file.
The road network is so big that it is not possible to paste it in the GUI of JMeter. 
It needs to be loaded from the folder.

JMeter tests (NL):

After installing, you have to put the csv file test_variables in the folder where you run the test or change folder at the CSV Data Set Config.
The url to the server and the (calculation) path is set in this file. You have to put this into the file after downloading. Also, you can copy the servername in JMeter if you find that easier.
In some cases you need a cookie to do the performance tests. The cookie can be found in the response from the website after you logged in.
You can paste the cookie in this file if using a cookie is necessary. Again, if you find it more convenient to paste it in the tests of JMeter itself, you can also do that. 

## Running JMeter
After this, you can run JMeter from the 'GUI mode' or the 'non GUI mode' (command line).
Opening JMeter in the 'GUI mode' can be done by clicking on the icon.
Opening JMeter in the 'non GUI mode' can be done with the following command (Windows):

```bash
jmeter -n -t my_test.jmx
```

## JMeter tests
There are tests for the NL environment and the UK environment. 
In both cases JSON files are sent to the backend either sequential or parallel.
You can paste the JSON files in JMeter or put them in the folder and use a command in the GUI of JMeter to load the JSON file.
The road network is so big that it is not possible to paste it in the GUI of JMeter. 
It needs to be loaded from the folder.

JMeter tests (NL):

There are three different kind of tests:
1) load tests
2) performance test (sequential)
3) interval test

### Load test
These scripts fire a certain amount of requests at the backend of the application.
These scripts simulate a request fired from the front end. 
This request consists of a calculation that is started after a click on the calculate button.
These tests are also the most straightforward scripts from this script set and follow the way JMeter is typically used. 

The other two types of tests also include scripts added by the Q&A team in order to test things that are not typically tested by JMeter and that are not standard options in the JMeter framework.
An example of this is measuring the total calculation time of a calculation. 

### Performance test
These scripts validate the performance of the system. 
Requests to start calculations are started sequentially (after each other) in a loop.
Afterwards the calculation time is logged and an average calculation time can be calculated.

### Interval test
For some cases it works well to use the previous two types of tests, but there are situations in which you want to know the calculation time of the requests on a relaxed and a busy system. 
Because of that an interval test has been created. 
The idea is that you start for example a small test (e.g. agriculture) at an interval and a bigger calculation (industry) at another or the same interval. 
By in- or decreasing the intervals you simulate the performance of when a lot of people would use the system (a lot of requests in a certain amount of time) or when the system is hardly used (few requests during a given amount of time). 

### JMeter test (UK):
There are several test files available to test the UK environment. 
The files need to be placed in a folder. 
This folder is set in the testVariablesUK file. 
After checking out the pull request, the files in the folder “jsonFiles” will be used for the performance tests. 
These files will be run sequentially.

These scripts fire a certain amount of requests at the backend of the application.
A request from the front end is simulated during these scripts. This request consists of a calculation that is started after a click on the calculate button.
These tests are also the most straightforward scripts from this script set and follow the way JMeter is typically used. 

The other two types of tests also include scripts added by the Q&A team in order to test things that are not typically tested by JMeter and that are not standard options in the JMeter framework.
An example of this is measuring the total calculation time of a calculation. 

### Performance tests
These scripts validate the performance of the system. 
Requests to start calculations are started sequentially (after each other) in a loop.
Afterwards the calculation time is logged and an average calculation time can be calculated.

### Interval tests
For some cases it works well to use the previous two types of tests, but there are situations in which you want to know the calculation time of the requests on a system that's under normal and heavy load. 
Because of that an interval test has been created. 
The idea is that you start for example a small test (e.g. agriculture) at an interval and a bigger calculation (industry) at another or the same interval. 
By in- or decreasing the intervals you simulate the performance of when a lot of people would use the system (a lot of requests in a certain amount of time) or when the system is hardly used (few requests during a given amount of time). 

### JMeter tests (UK):
There are several test files available to test the UK environment. 
These files need to be placed in a folder. 
This folder is set in the `testVariablesUK` file. 
After checking out the pull request, the files in the folder “jsonFiles” will be used for the performance tests. 
These files will be run sequentially.
