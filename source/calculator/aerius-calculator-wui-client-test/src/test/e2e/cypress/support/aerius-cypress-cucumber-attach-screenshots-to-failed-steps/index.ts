/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

afterEach(() => {
  const screenshotsFolder = Cypress.config("screenshotsFolder");
  if (window.cucumberJson?.generate) {
    const testState = window.testState;
    const stepResult = testState.runTests[testState.currentScenario.name][testState.currentStep];
    if (stepResult?.status === "failed") {
      const scenarioName = testState.currentScenario.name.endsWith(".")
        ? testState.currentScenario.name.substring(0, testState.currentScenario.name.length - 1)
        : testState.currentScenario.name;

      const screenshotFileName = testState.feature.name + " -- " + scenarioName + " (failed).png";
      const screenshotFileNamePath = screenshotsFolder + "/" + Cypress.spec.relativeToCommonRoot + "/" + screenshotFileName;
      cy.wait(2_000);
      cy.task('readFileMaybe', screenshotFileNamePath).then((base64OrNull) => { 
        // try not to fail this test if file does not exist
        if (base64OrNull != null) {
          if (base64OrNull) {
            stepResult.attachment = {
              data: base64OrNull,
              media: { type: "image/png" },
              index: testState.currentStep,
              testCase: testState.formatTestCase(testState.currentScenario)
            };
          }
        } else {
          cy.log("Problem attaching screenshot " + screenshotFileNamePath);
        }
      });
    }
  }
});
