const decompress = require("decompress");

const unzip = ({ outputPath, inputFilePath }) => decompress(inputFilePath, outputPath);

module.exports = {
  unzip
};
