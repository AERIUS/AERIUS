/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";

import { CommonGenericUIPage } from "../../common/CommonGenericUIPage";
import { GenericUIPage } from "./GenericUIPage";

Given("I select the name input field", () => {
  GenericUIPage.selectSourceNameInputField();
});

Given("I select the building name input field", () => {
  GenericUIPage.selectBuildingNameInputField();
});

Given("I expect the privacy warning to exist", () => {
  CommonGenericUIPage.assertPrivacyWarningExists("UK");
});

Given("I validate that demo mode message is show", () => {
  GenericUIPage.validateDemoModeMessage();
});

Given("I press the cookie preferences", () => {
  GenericUIPage.toggleCookiePreferenceWindow();
});

Given("I validate the cookie preferences are {string}", (status) => {
  GenericUIPage.validateCookiePreferenceWindow(status);
});

Given("I validate the value of cookie preference {string} is {string}", (objectName, value) => {
  GenericUIPage.validateCookiePreferenceTitles(objectName, value);
});

Given("I press the cookie notices", () => {
  GenericUIPage.pressCookieNotice();
});

Given("I validate the value of the cookie notice {string} is {string}", (objectName, value) => {
  GenericUIPage.validateCookieNoticeTitles(objectName, value);
});

Given("I close the cookie notices", () => {
  GenericUIPage.closeCookieNotice();
});

Given("I close the cookie preferences", () => {
  GenericUIPage.closeCookiePreferences();
});

Given("I validate the cookie preferences should be {string}", (status) => {
  GenericUIPage.validateCookiePreferencesStatus(status);
});

Given("I select the cookie preferences status {string}", (status) => {
  GenericUIPage.changeCookiePreferencesStatus(status);
});

// Language
Given("I validate the language menu does not exist", () => {
  GenericUIPage.validateLanguageMenuDoesNotExist();
});

// Preferences
Given("the selected WKT conversion method should be {string}", (conversionMethod: string) => {
  GenericUIPage.assertSelectedWKTConversionMethod(conversionMethod);
});

Given("I set the WKT conversion method to {string}", (conversionMethod: string) => {
  GenericUIPage.setWKTConversionMethod(conversionMethod);
});
