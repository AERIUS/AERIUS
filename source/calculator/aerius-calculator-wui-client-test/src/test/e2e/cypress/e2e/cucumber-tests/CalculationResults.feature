#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Calculation results

    Scenario: Create new Industry source start calculation and validate result screens (proposed)
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Industrie' and sector 'Afvalverwerking'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose heat content type 'Niet geforceerd', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '10' and NH3 '20000'
        And I save the data
        And I navigate to the 'Calculation jobs' tab
        Then I intercept the status of the calculation
        And I intercept the response from '/api/v8/ui/calculation/*/*/PROJECT_CALCULATION?summaryHexagonType=EXCEEDING_HEXAGONS&summaryOverlappingHexagonType=ALL_HEXAGONS*' with 'projectCalculationExceedingHexagons.json'
            | statisticType                  | ExpectedStatistics |
            | maxDecrease                    | 7.7                |
            | maxIncrease                    | 0.067              |
            | sumCartographicSurfaceIncrease | 77777777.7         |
            | sumCartographicSurfaceDecrease | 777.7              |
            | maxPercentageCriticalLevel     | 0.7777777777       |
            | sumCartographicSurface         | 7.9e7              |
            | maxTotal                       | 7777.99            |
        And I intercept the response from '/api/v8/ui/calculation/*/*/PROJECT_CALCULATION?summaryHexagonType=RELEVANT_HEXAGONS*' with 'projectCalculationRelevantHexagons.json'
            | statisticType                  | ExpectedStatistics |
            | maxDecrease                    | 6.6                |
            | maxIncrease                    | 0.057              |
            | sumCartographicSurfaceIncrease | 66666666.6         |
            | sumCartographicSurfaceDecrease | 666.6              |
            | maxPercentageCriticalLevel     | 0.66666666666      |
            | sumCartographicSurface         | 6.9e7              |
            | maxTotal                       | 6666.99            |
        And I intercept the response from '/api/v8/ui/calculation/*/*/PROJECT_CALCULATION?summaryHexagonType=EXTRA_ASSESSMENT_HEXAGONS*' with 'projectCalculationExtraHexagons.json'
            | statisticType                  | ExpectedStatistics |
            | countReceptors                 | 21                 |
            | sumCartographicSurfaceDecrease | 0.0                |
            | maxDecrease                    | 6.6                |
            | maxIncrease                    | 0.105837           |
            | maxTotal                       | 5555.99            |
            | sumCartographicSurface         | 24.0               |
            | maxPercentageCriticalLevel     | 0.016275799        |
            | countReceptorsIncrease         | 11.0               |
            | countReceptorsDecrease         | 2                  |
            | sumCartographicSurfaceIncrease | 24.0               |

        And I intercept the response from '/api/v8/ui/calculation/*/*/SITUATION_RESULT?*' with 'situationResultExceedingHexagons.json'
            | statisticType          | ExpectedStatistics |
            | sumCartographicSurface | 9.9e7              |
            | maxContribution        | 9.99               |
            | maxTotal               | 9999.99            |
        And I intercept the response from '/api/v8/ui/calculation/*/*/SITUATION_RESULT?summaryHexagonType=RELEVANT_HEXAGONS*' with 'situationResultRelevantHexagons.json'
            | statisticType          | ExpectedStatistics |
            | sumCartographicSurface | 8.9e7              |
            | maxContribution        | 8.99               |
            | maxTotal               | 8888.99            |
        And I intercept the response from '/api/v8/ui/calculation/*/*/SITUATION_RESULT?summaryHexagonType=EXTRA_ASSESSMENT_HEXAGONS*' with 'situationResultExtraHexagons.json'
            | statisticType          | ExpectedStatistics |
            | countReceptors         | 7                  |
            | maxContribution        | 7.99               |
            | maxTotal               | 7777.99            |

        When I create a new calculation job
        And I save the calculation job
        And I start the calculation
        Then I validate that the situation 'Situatie 1 - Beoogd' has results combinations
            | resultSelectId     | resultSelectLabel           |
            | resultsResultType  | Situatieresultaat           |
            | resultsHexagonType | OwN2000-registratieset      |
            | resultsHexagonType | Relevante hexagonen         |
            | resultsHexagonType | Hexagonen met hersteldoelen |
            | resultsResultType  | Projectberekening           |
            | resultsHexagonType | OwN2000-registratieset      |
            | resultsHexagonType | Relevante hexagonen         |
            | resultsHexagonType | Hexagonen met hersteldoelen |
        Then I validate that for situation 'Situatie 1 - Beoogd' the resultsType 'Situatieresultaat' has '3' options
        Then I validate that for situation 'Situatie 1 - Beoogd' the resultsType 'Projectberekening' has '3' options

        Then I select situation with label 'Situatie 1 - Beoogd' and validate the 'Projectberekening' result screen with mock results for the result 'OwN2000-registratieset'
            | elementId                      | expectedResult |
            | sumCartographicSurface         | 7.900,00       |
            | maxTotal                       | 7.777,99       |
            | sumCartographicSurfaceIncrease | 7.777,78       |
            | maxIncrease                    | 0,07           |
            | sumCartographicSurfaceDecrease | 0,08           |
            | maxDecrease                    | 7,70           |
        Then I select situation with label 'Situatie 1 - Beoogd' and validate the 'Projectberekening' result screen with mock results for the result 'Relevante hexagonen'
            | elementId                      | expectedResult |
            | sumCartographicSurface         | 6.900,00       |
            | maxTotal                       | 6.666,99       |
            | sumCartographicSurfaceIncrease | 6.666,67       |
            | maxIncrease                    | 0,06           |
            | sumCartographicSurfaceDecrease | 0,07           |
            | maxDecrease                    | 6,60           |
        And I check if the deposition tab has 7 results
        And I check if the markers tab has 7 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the habitat tab has 7 results
        Then I select situation with label 'Situatie 1 - Beoogd' and validate the 'Projectberekening' result screen with mock results for the result 'Hexagonen met hersteldoelen'
            | elementId                      | expectedResult |
            | countReceptors                 | 21             |
            | maxTotal                       | 5.555,99       |
            | countReceptorsIncrease         | 11.0           |
            | countReceptorsDecrease         | 2              |
            | maxIncrease                    | 0,11           |
            | maxDecrease                    | 6,60           |
        And I check if the deposition tab has 1 results
        And I check if the markers tab has 1 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the extra assessment habitat tab has 1 results

        Then I select situation with label 'Situatie 1 - Beoogd' and validate the 'Situatieresultaat' result screen with mock results for the result 'Relevante hexagonen'
            | elementId              | expectedResult |
            | sumCartographicSurface | 8.900,00       |
            | maxTotal               | 8.888,99       |
            | maxContribution        | 8,99           |
        And I check if the deposition tab has 7 results
        And I check if the markers tab has 7 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the habitat tab has 7 results
        Then I select situation with label 'Situatie 1 - Beoogd' and validate the 'Situatieresultaat' result screen with mock results for the result 'OwN2000-registratieset'
            | elementId              | expectedResult |
            | sumCartographicSurface | 9.900,00       |
            | maxTotal               | 9.999,99       |
            | maxContribution        | 9,99           |
        And I check if the deposition tab has 4 results
        And I check if the markers tab has 4 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the habitat tab has 4 results
        Then I select situation with label 'Situatie 1 - Beoogd' and validate the 'Situatieresultaat' result screen with mock results for the result 'Hexagonen met hersteldoelen'
            | elementId              | expectedResult |
            | countReceptors         | 7              |
            | maxTotal               | 7.777,99       |
            | maxContribution        | 7,99           |
        And I check if the deposition tab has 1 results
        And I check if the markers tab has 1 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the extra assessment habitat tab has 1 results

    Scenario: Create new Industry source start calculation with temporary situation and validate result screens
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Industrie' and sector 'Afvalverwerking'
        And I fill location with coordinates 'POINT(126256.32 411244.86)'
        And I choose heat content type 'Niet geforceerd', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '10' and NH3 '20000'
        And I save the data
        And I select scenarioType 'Referentie'
        And I save the scenario updates
        And I duplicate the situation
        And I select scenarioType 'Tijdelijk'
        And I save the scenario updates
        And I select the source 'Bron 1'
        And I choose to edit the source
        And I fill the emission fields with NOx '20' and NH3 '40000'
        And I save the data
        And I navigate to the 'Calculation jobs' tab
        Then I intercept the status of the calculation
        And I intercept the response from '/api/v8/ui/calculation/*/*/PROJECT_CALCULATION?summaryHexagonType=EXCEEDING_HEXAGONS&summaryOverlappingHexagonType=ALL_HEXAGONS*' with 'projectCalculationExceedingHexagons.json'
            | statisticType                  | ExpectedStatistics |
            | maxDecrease                    | 7.7                |
            | maxIncrease                    | 0.067              |
            | sumCartographicSurfaceIncrease | 77777777.7         |
            | sumCartographicSurfaceDecrease | 777.7              |
            | maxPercentageCriticalLevel     | 0.7777777777       |
            | sumCartographicSurface         | 7.9e7              |
            | maxTotal                       | 7777.99            |
        And I intercept the response from '/api/v8/ui/calculation/*/*/PROJECT_CALCULATION?summaryHexagonType=RELEVANT_HEXAGONS*' with 'projectCalculationRelevantHexagons.json'
            | statisticType                  | ExpectedStatistics |
            | maxDecrease                    | 6.6                |
            | maxIncrease                    | 0.057              |
            | sumCartographicSurfaceIncrease | 66666666.6         |
            | sumCartographicSurfaceDecrease | 666.6              |
            | maxPercentageCriticalLevel     | 0.66666666666      |
            | sumCartographicSurface         | 6.9e7              |
            | maxTotal                       | 6666.99            |

        And I intercept the response from '/api/v8/ui/calculation/*/*/SITUATION_RESULT?summaryHexagonType=EXCEEDING_HEXAGONS*' with 'situationResultExceedingHexagons.json'
            | statisticType          | ExpectedStatistics |
            | sumCartographicSurface | 9.9e7              |
            | maxContribution        | 9.99               |
            | maxTotal               | 9999.99            |
        And I intercept the response from '/api/v8/ui/calculation/*/*/SITUATION_RESULT?summaryHexagonType=RELEVANT_HEXAGONS*' with 'situationResultRelevantHexagons.json'
            | statisticType          | ExpectedStatistics |
            | sumCartographicSurface | 8.9e7              |
            | maxContribution        | 8.99               |
            | maxTotal               | 8888.99            |
        And I intercept the response from '/api/v8/ui/calculation/*/*/SITUATION_RESULT?summaryHexagonType=EXTRA_ASSESSMENT_HEXAGONS*' with 'situationResultExtraHexagons.json'
            | statisticType          | ExpectedStatistics |
            | countReceptors         | 7                  |
            | maxContribution        | 7.99               |
            | maxTotal               | 7777.99            |

        And I intercept the response from 'api/v8/ui/calculation/*/*/MAX_TEMPORARY_EFFECT?summaryHexagonType=EXCEEDING_HEXAGONS&summaryOverlappingHexagonType=ALL_HEXAGONS*' with 'projectCalculationMaxTemporaryContribution.json'
            | statisticType          | ExpectedStatistics |
            | sumCartographicSurface | 66666.50           |
            | maxTempContribution    | 6666.50            |
            | maxTempIncrease        | 6666.50            |
            | maxTotal               | 666.50             |
        And I intercept the response from '/api/v8/ui/calculation/*/*/MAX_TEMPORARY_EFFECT?summaryHexagonType=RELEVANT_HEXAGONS*' with 'projectCalculationMaxTemporaryEffect.json'
            | statisticType          | ExpectedStatistics |
            | sumCartographicSurface | 55555.50           |
            | maxTempContribution    | 5555.50            |
            | maxTempIncrease        | 5555.50            |
            | maxTotal               | 555.50             |
        And I intercept the response from '/api/v8/ui/calculation/*/*/MAX_TEMPORARY_EFFECT?summaryHexagonType=EXTRA_ASSESSMENT_HEXAGONS*' with 'projectCalculationMaxTemporaryEffectExtraAssessment.json'
            | statisticType          | ExpectedStatistics |
            | countReceptors         | 44                 |
            | maxTempContribution    | 4444.50            |
            | maxTempIncrease        | 4444.50            |
            | maxTotal               | 444.50             |

        When I create a new calculation job
        Then the following error is shown: 'Er ontbreken verplichte situaties: Beoogd'
        Then the following error is shown: 'Er zijn geen situaties geselecteerd om door te rekenen'
        When I select calculation jobType 'Maximaal tijdelijk effect'
        #uncheck default temporary
        And I check the checkbox for the temporary situation 'Situatie 1 (1) - Tijdelijk'
        Then the following error is shown: 'Er ontbreken verplichte situaties: Tijdelijk'
        Then the following error is shown: 'Er zijn geen situaties geselecteerd om door te rekenen'
        And I select the situation 'Situatie 1' as 'referenceSituation'
        And I check the checkbox for the temporary situation 'Situatie 1 (1) - Tijdelijk'

        And I save the calculation job
        And I start the calculation
        Then I validate that the situation 'Situatie 1 - Referentie' has results combinations
            | resultSelectId     | resultSelectLabel           |
            | resultsResultType  | Situatieresultaat           |
            | resultsHexagonType | OwN2000-registratieset      |
            | resultsHexagonType | Relevante hexagonen         |
            | resultsHexagonType | Hexagonen met hersteldoelen |
        Then I validate that for situation 'Situatie 1 - Referentie' the resultsType 'Situatieresultaat' has '3' options

        Then I select situation with label 'Situatie 1 - Referentie' and validate the 'Situatieresultaat' result screen with mock results for the result 'Relevante hexagonen'
            | elementId              | expectedResult |
            | sumCartographicSurface | 8.900,00       |
            | maxTotal               | 8.888,99       |
            | maxContribution        | 8,99           |
        And I check if the deposition tab has 7 results
        And I check if the markers tab has 7 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the habitat tab has 7 results
        Then I select situation with label 'Situatie 1 - Referentie' and validate the 'Situatieresultaat' result screen with mock results for the result 'OwN2000-registratieset'
            | elementId              | expectedResult |
            | sumCartographicSurface | 9.900,00       |
            | maxTotal               | 9.999,99       |
            | maxContribution        | 9,99           |
        And I check if the deposition tab has 4 results
        And I check if the markers tab has 4 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the habitat tab has 4 results
        Then I select situation with label 'Situatie 1 - Referentie' and validate the 'Situatieresultaat' result screen with mock results for the result 'Hexagonen met hersteldoelen'
            | elementId              | expectedResult |
            | countReceptors         | 7              |
            | maxTotal               | 7.777,99       |
            | maxContribution        | 7,99           |
        And I check if the deposition tab has 1 results
        And I check if the markers tab has 1 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the extra assessment habitat tab has 1 results

        Then I validate that the situation 'Situatie 1 (1) - Tijdelijk' has results combinations
            | resultSelectId     | resultSelectLabel                                 |
            | resultsResultType  | Max. tijdelijke effect                            |
            | resultsHexagonType | OwN2000-registratieset                            |
            | resultsHexagonType | OwN2000-registratieset(zonder randhexagonen)      |
            | resultsHexagonType | OwN2000-registratieset(alleen randhexagonen)      |
            | resultsHexagonType | Relevante hexagonen                               |
            | resultsHexagonType | Relevante hexagonen(zonder randhexagonen)         |
            | resultsHexagonType | Relevante hexagonen(alleen randhexagonen)         |
            | resultsHexagonType | Hexagonen met hersteldoelen                       |
            | resultsHexagonType | Hexagonen met hersteldoelen(zonder randhexagonen) |
            | resultsHexagonType | Hexagonen met hersteldoelen(alleen randhexagonen) |
            | resultsResultType  | Situatieresultaat                                 |
            | resultsHexagonType | Relevante hexagonen                               |
            | resultsHexagonType | OwN2000-registratieset                            |
            | resultsHexagonType | Hexagonen met hersteldoelen                       |

        Then I validate that for situation 'Situatie 1 (1) - Tijdelijk' the resultsType 'Max. tijdelijke effect' has '9' options
        Then I validate that for situation 'Situatie 1 (1) - Tijdelijk' the resultsType 'Situatieresultaat' has '3' options

        Then I select situation with label 'Situatie 1 (1) - Tijdelijk' and validate the 'Max. tijdelijke effect' result screen with mock results for the result 'Relevante hexagonen'
            | elementId              | expectedResult |
            | sumCartographicSurface | 5,56           |
            | maxTotal               | 555,50         |
            | maxTempIncrease        | 5.555,50       |
        And I check if the deposition tab has 4 results
        And I check if the markers tab has 4 results and the text 'Maximale tijdelijke toename (mol N/ha/jr)'
        Then I select situation with label 'Situatie 1 (1) - Tijdelijk' and validate the 'Max. tijdelijke effect' result screen with mock results for the result 'OwN2000-registratieset'
            | elementId              | expectedResult |
            | sumCartographicSurface | 6,67           |
            | maxTotal               | 666,50         |
            | maxTempIncrease        | 6.666,50       |
        And I check if the deposition tab has 4 results
        And I check if the markers tab has 4 results and the text 'Maximale tijdelijke toename (mol N/ha/jr)'
        Then I select situation with label 'Situatie 1 (1) - Tijdelijk' and validate the 'Max. tijdelijke effect' result screen with mock results for the result 'Hexagonen met hersteldoelen'
            | elementId              | expectedResult |
            | countReceptors         | 44             |
            | maxTotal               | 444,50         |
            | maxTempIncrease        | 4.444,50       |
        And I check if the deposition tab has 2 results
        And I check if the markers tab has 2 results and the text 'Maximale tijdelijke toename (mol N/ha/jr)'
        And I check if the extra assessment habitat tab has 2 results

    @Smoke
    Scenario: Create new Industry source start calculation max temporary effect with reference, temporary and saldering situation and validate result screens
        Given I login with correct username and password
        And I create a new source from the start page situation label 'PROPOSED'
        And I name the source 'Bron 1' and select sector group 'Industrie' and sector 'Afvalverwerking'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose heat content type 'Niet geforceerd', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '20' and NH3 '20000'
        And I save the data
        And I duplicate the situation
        And I add situation label 'TEMPORARY'
        And I select scenarioType 'TEMPORARY'
        And I save the scenario updates
        And I select the source 'Bron 1'
        And I choose to edit the source
        And I fill the emission fields with NOx '10' and NH3 '500'
        And I save the data
        And I duplicate the situation
        And I add situation label 'REFERENCE'
        And I select scenarioType 'REFERENCE'
        And I save the scenario updates
        And I select the source 'Bron 1'
        And I choose to edit the source
        And I fill the emission fields with NOx '22' and NH3 '22000'
        And I save the data
        And I duplicate the situation
        And I add situation label 'OFF_SITE_REDUCTION'
        And I select scenarioType 'OFF_SITE_REDUCTION'
        And I save the scenario updates
        And I select the source 'Bron 1'
        And I choose to edit the source
        And I fill the emission fields with NOx '5' and NH3 '12000'
        And I save the data
        And I navigate to the 'Calculation jobs' tab
        Then I intercept the status of the calculation
        And I intercept the response from '/api/v8/ui/calculation/*/*/SITUATION_RESULT?*' with 'situationResultExceedingHexagons.json'
            | statisticType          | ExpectedStatistics |
            | sumCartographicSurface | 9.9e7              |
            | maxContribution        | 9.99               |
            | maxTotal               | 9999.99            |
        And I intercept the response from '/api/v8/ui/calculation/*/*/SITUATION_RESULT?summaryHexagonType=RELEVANT_HEXAGONS**' with 'situationResultRelevantHexagons.json'
            | statisticType          | ExpectedStatistics |
            | sumCartographicSurface | 8.9e7              |
            | maxContribution        | 8.99               |
            | maxTotal               | 8888.99            |
        And I intercept the response from '/api/v8/ui/calculation/*/*/SITUATION_RESULT?summaryHexagonType=EXTRA_ASSESSMENT_HEXAGONS*' with 'situationResultExtraHexagons.json'
            | statisticType          | ExpectedStatistics |
            | countReceptors         | 7                  |
            | maxContribution        | 7.99               |
            | maxTotal               | 7777.99            |

        And I intercept the response from '/api/v8/ui/calculation/*/*/PROJECT_CALCULATION?*' with 'projectCalculationExceedingHexagons.json'
            | statisticType                  | ExpectedStatistics |
            | maxDecrease                    | 7.7                |
            | maxIncrease                    | 0.067              |
            | sumCartographicSurfaceIncrease | 77777777.7         |
            | sumCartographicSurfaceDecrease | 777.7              |
            | maxPercentageCriticalLevel     | 0.7777777777       |
            | sumCartographicSurface         | 7.9e7              |
            | maxTotal                       | 7777.99            |
        And I intercept the response from '/api/v8/ui/calculation/*/*/PROJECT_CALCULATION?summaryHexagonType=RELEVANT_HEXAGONS*' with 'projectCalculationRelevantHexagons.json'
            | statisticType                  | ExpectedStatistics |
            | maxDecrease                    | 6.6                |
            | maxIncrease                    | 0.057              |
            | sumCartographicSurfaceIncrease | 66666666.6         |
            | sumCartographicSurfaceDecrease | 666.6              |
            | maxPercentageCriticalLevel     | 0.66666666666      |
            | sumCartographicSurface         | 6.9e7              |
            | maxTotal                       | 6666.99            |

        And I intercept the response from '/api/v8/ui/calculation/*/*/MAX_TEMPORARY_EFFECT?summaryHexagonType=EXCEEDING_HEXAGON*' with 'projectCalculationMaxTemporaryContribution.json'
            | statisticType          | ExpectedStatistics |
            | sumCartographicSurface | 66666.50           |
            | maxTempContribution    | 6666.50            |
            | maxTempIncrease        | 6666.50            |
            | maxTotal               | 666.50             |
        And I intercept the response from '/api/v8/ui/calculation/*/*/MAX_TEMPORARY_EFFECT?summaryHexagonType=RELEVANT_HEXAGONS*' with 'projectCalculationMaxTemporaryEffect.json'
            | statisticType          | ExpectedStatistics |
            | sumCartographicSurface | 55555.50           |
            | maxTempContribution    | 5555.50            |
            | maxTempIncrease        | 5555.50            |
            | maxTotal               | 555.50             |
        And I intercept the response from '/api/v8/ui/calculation/*/*/MAX_TEMPORARY_EFFECT?summaryHexagonType=EXTRA_ASSESSMENT_HEXAGONS*' with 'projectCalculationMaxTemporaryEffectExtraAssessment.json'
            | statisticType          | ExpectedStatistics |
            | countReceptors         | 44                 |
            | maxTempContribution    | 4444.50            |
            | maxTempIncrease        | 4444.50            |
            | maxTotal               | 444.50             |

        When I create a new calculation job

        When I select calculation jobType 'Maximaal tijdelijk effect'
        #uncheck default temporary
        And I check the checkbox for the temporary situation 'TEMPORARY - Tijdelijk'
        Then the following error is shown: 'Er ontbreken verplichte situaties: Tijdelijk'
        Then the following error is shown: 'Er zijn geen situaties geselecteerd om door te rekenen'
        And I select the situation 'REFERENCE' as 'referenceSituation'
        And I select the situation 'OFF_SITE_REDUCTION' as 'off_site_reductionSituation'
        And I check the checkbox for the temporary situation 'TEMPORARY - Tijdelijk'
        And I save the calculation job
        And I start the calculation

        Then I validate that the situation 'TEMPORARY - Tijdelijk' has results combinations
            | resultSelectId     | resultSelectLabel                                 |
            | resultsResultType  | Max. tijdelijke effect                            |
            | resultsHexagonType | OwN2000-registratieset                            |
            | resultsHexagonType | OwN2000-registratieset(zonder randhexagonen)      |
            | resultsHexagonType | OwN2000-registratieset(alleen randhexagonen)      |
            | resultsHexagonType | Relevante hexagonen                               |
            | resultsHexagonType | Relevante hexagonen(zonder randhexagonen)         |
            | resultsHexagonType | Relevante hexagonen(alleen randhexagonen)         |
            | resultsHexagonType | Hexagonen met hersteldoelen                       |
            | resultsHexagonType | Hexagonen met hersteldoelen(zonder randhexagonen) |
            | resultsHexagonType | Hexagonen met hersteldoelen(alleen randhexagonen) |
            | resultsResultType  | Situatieresultaat                                 |
            | resultsHexagonType | Relevante hexagonen                               |
            | resultsHexagonType | OwN2000-registratieset                            |
            | resultsHexagonType | Hexagonen met hersteldoelen                       |
        Then I validate that for situation 'TEMPORARY - Tijdelijk' the resultsType 'Max. tijdelijke effect' has '9' options
        Then I validate that for situation 'TEMPORARY - Tijdelijk' the resultsType 'Situatieresultaat' has '3' options

        Then I validate that the situation 'REFERENCE - Referentie' has results combinations
            | resultSelectId     | resultSelectLabel           |
            | resultsResultType  | Situatieresultaat           |
            | resultsHexagonType | Relevante hexagonen         |
            | resultsHexagonType | OwN2000-registratieset      |
            | resultsHexagonType | Hexagonen met hersteldoelen |
        Then I validate that for situation 'REFERENCE - Referentie' the resultsType 'Situatieresultaat' has '3' options

        Then I validate that the situation 'OFF_SITE_REDUCTION - Saldering' has results combinations
            | resultSelectId     | resultSelectLabel           |
            | resultsResultType  | Situatieresultaat           |
            | resultsHexagonType | Relevante hexagonen         |
            | resultsHexagonType | OwN2000-registratieset      |
            | resultsHexagonType | Hexagonen met hersteldoelen |
        Then I validate that for situation 'OFF_SITE_REDUCTION - Saldering' the resultsType 'Situatieresultaat' has '3' options

        Then I select situation with label 'TEMPORARY - Tijdelijk' and validate the 'Max. tijdelijke effect' result screen with mock results for the result 'OwN2000-registratieset'
            | elementId              | expectedResult |
            | sumCartographicSurface | 6,67           |
            | maxTotal               | 666,50         |
            | maxTempIncrease        | 6.666,50       |
        Then I select situation with label 'TEMPORARY - Tijdelijk' and validate the 'Max. tijdelijke effect' result screen with mock results for the result 'Relevante hexagonen'
            | elementId              | expectedResult |
            | sumCartographicSurface | 5,56           |
            | maxTotal               | 555,50         |
            | maxTempIncrease        | 5.555,50       |
        And I check if the deposition tab has 4 results
        And I check if the markers tab has 4 results and the text 'Maximale tijdelijke toename (mol N/ha/jr)'
        And I check if the habitat tab has 4 results
        Then I select situation with label 'TEMPORARY - Tijdelijk' and validate the 'Max. tijdelijke effect' result screen with mock results for the result 'Hexagonen met hersteldoelen'
            | elementId              | expectedResult |
            | countReceptors         | 44             |
            | maxTotal               | 444,50         |
            | maxTempIncrease        | 4.444,50       |
        And I check if the deposition tab has 2 results
        And I check if the markers tab has 2 results and the text 'Maximale tijdelijke toename (mol N/ha/jr)'
        And I check if the extra assessment habitat tab has 2 results

        Then I select situation with label 'REFERENCE - Referentie' and validate the 'Situatieresultaat' result screen with mock results for the result 'Relevante hexagonen'
            | elementId              | expectedResult |
            | sumCartographicSurface | 8.900,00       |
            | maxTotal               | 8.888,99       |
            | maxContribution        | 8,99           |
        And I check if the deposition tab has 7 results
        And I check if the markers tab has 7 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the habitat tab has 7 results
        Then I select situation with label 'REFERENCE - Referentie' and validate the 'Situatieresultaat' result screen with mock results for the result 'Hexagonen met hersteldoelen'
            | elementId              | expectedResult |
            | countReceptors         | 7              |
            | maxTotal               | 7.777,99       |
            | maxContribution        | 7,99           |
        And I check if the deposition tab has 1 results
        And I check if the markers tab has 1 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the extra assessment habitat tab has 1 results

        Then I select situation with label 'OFF_SITE_REDUCTION - Saldering' and validate the 'Situatieresultaat' result screen with mock results for the result 'OwN2000-registratieset'
            | elementId              | expectedResult |
            | sumCartographicSurface | 9.900,00       |
            | maxTotal               | 9.999,99       |
            | maxContribution        | 9,99           |
        And I check if the deposition tab has 4 results
        And I check if the markers tab has 4 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the habitat tab has 4 results        
        Then I select situation with label 'OFF_SITE_REDUCTION - Saldering' and validate the 'Situatieresultaat' result screen with mock results for the result 'Hexagonen met hersteldoelen'
            | elementId              | expectedResult |
            | countReceptors         | 7              |
            | maxTotal               | 7.777,99       |
            | maxContribution        | 7,99           |
        And I check if the deposition tab has 1 results
        And I check if the markers tab has 1 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the extra assessment habitat tab has 1 results

    Scenario: Create new Industry source start calculation process contribution with reference and reference situation and validate result screens Ede calculation
        Given I login with correct username and password
        And I create a new source from the start page situation label 'PROPOSED'
        And I name the source 'Bron 1' and select sector group 'Industrie' and sector 'Afvalverwerking'
        And I fill location with coordinates 'POINT(266945.32 454878.89)'
        And I choose heat content type 'Niet geforceerd', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '5000' and NH3 '0'
        And I save the data
        And I duplicate the situation
        And I add situation label 'REFERENCE'
        And I select scenarioType 'REFERENCE'
        And I save the scenario updates
        And I select the source 'Bron 1'
        And I choose to edit the source
        And I open collapsible panel 'Locatie'
        And I empty the field 'emissionSourceGeometryInput'
        And I fill location with coordinates 'POINT(259110.72 450068.17)'
        And I save the data
        And I navigate to the 'Calculation jobs' tab
        Then I intercept the status of the calculation
        And I intercept the response from 'api/v8/ui/calculation/*/*/PROJECT_CALCULATION?summaryHexagonType=RELEVANT_HEXAGONS&summaryOverlappingHexagonType=OVERLAPPING_HEXAGONS_ONLY*' with 'edgeHexagonProjectCalculationRelevantOverlapping.json'
            | statisticType                  | ExpectedStatistics |
            | maxDecrease                    | 0.005372           |
            | maxIncrease                    | 0                  |
            | sumCartographicSurfaceIncrease | 0                  |
            | sumCartographicSurfaceDecrease | 63096              |
            | maxPercentageCriticalLevel     | 5000               |
            | sumCartographicSurface         | 63096.316          |
            | maxTotal                       | 2160.5752          |
        And I intercept the response from 'api/v8/ui/calculation/*/*/PROJECT_CALCULATION?summaryHexagonType=RELEVANT_HEXAGONS&summaryOverlappingHexagonType=NON_OVERLAPPING_HEXAGONS_ONLY*' with 'edgeHexagonProjectCalculationRelevantNonOverlapping.json'
            | statisticType                  | ExpectedStatistics |
            | maxDecrease                    | 0                  |
            | maxIncrease                    | 0.01               |
            | sumCartographicSurfaceIncrease | 272089.12          |
            | sumCartographicSurfaceDecrease | 0                  |
            | maxPercentageCriticalLevel     | 0                  |
            | sumCartographicSurface         | 272089.12          |
            | maxTotal                       | 2171.0688          |
        And I intercept the response from 'api/v8/ui/calculation/*/*/PROJECT_CALCULATION?summaryHexagonType=EXCEEDING_HEXAGONS&summaryOverlappingHexagonType=OVERLAPPING_HEXAGONS_ONLY*' with 'edgeHexagonProjectCalculationExceedingOverlapping.json'
            | statisticType                  | ExpectedStatistics |
            | maxDecrease                    | 0.005372           |
            | maxIncrease                    | 0                  |
            | sumCartographicSurfaceIncrease | 0                  |
            | sumCartographicSurfaceDecrease | 63096              |
            | maxPercentageCriticalLevel     | 5000               |
            | sumCartographicSurface         | 63096.316          |
            | maxTotal                       | 2160.5752          |
        And I intercept the response from 'api/v8/ui/calculation/*/*/PROJECT_CALCULATION?summaryHexagonType=EXCEEDING_HEXAGONS&summaryOverlappingHexagonType=NON_OVERLAPPING_HEXAGONS_ONLY*' with 'edgeHexagonProjectCalculationExceedingNonOverlapping.json'
            | statisticType                  | ExpectedStatistics |
            | maxDecrease                    | 0                  |
            | maxIncrease                    | 0.006991           |
            | sumCartographicSurfaceIncrease | 130505.055         |
            | sumCartographicSurfaceDecrease | 0                  |
            | maxPercentageCriticalLevel     | 0                  |
            | sumCartographicSurface         | 130505.055         |
            | maxTotal                       | 2171.0688          |
        When I create a new calculation job
        And I select the situation 'REFERENCE' as 'referenceSituation'
        And I save the calculation job
        And I start the calculation
        Then I validate that the situation 'PROPOSED - Beoogd' has results combinations
            | resultSelectId     | resultSelectLabel                                 |
            | resultsResultType  | Situatieresultaat                                 |
            | resultsHexagonType | OwN2000-registratieset                            |
            | resultsHexagonType | Relevante hexagonen                               |
            | resultsResultType  | Projectberekening                                 |
            | resultsHexagonType | OwN2000-registratieset                            |
            | resultsHexagonType | OwN2000-registratieset(zonder randhexagonen)      |
            | resultsHexagonType | OwN2000-registratieset(alleen randhexagonen)      |
            | resultsHexagonType | Relevante hexagonen                               |
            | resultsHexagonType | Relevante hexagonen(zonder randhexagonen)         |
            | resultsHexagonType | Relevante hexagonen(alleen randhexagonen)         |
            | resultsHexagonType | Hexagonen met hersteldoelen                       |
            | resultsHexagonType | Hexagonen met hersteldoelen(zonder randhexagonen) |
            | resultsHexagonType | Hexagonen met hersteldoelen(alleen randhexagonen) |
        Then I validate that for situation 'PROPOSED - Beoogd' the resultsType 'Situatieresultaat' has '3' options
        Then I validate that for situation 'PROPOSED - Beoogd' the resultsType 'Projectberekening' has '9' options
        
        Then I validate that the situation 'REFERENCE - Referentie' has results combinations
            | resultSelectId     | resultSelectLabel           |
            | resultsResultType  | Situatieresultaat           |
            | resultsHexagonType | Relevante hexagonen         |
            | resultsHexagonType | OwN2000-registratieset      |
            | resultsHexagonType | Hexagonen met hersteldoelen |
        Then I validate that for situation 'REFERENCE - Referentie' the resultsType 'Situatieresultaat' has '3' options

        Then I select situation with label 'PROPOSED - Beoogd' and validate the 'Projectberekening' result screen with mock results for the result 'OwN2000-registratieset(zonder randhexagonen)'
            | elementId                      | expectedResult |
            | sumCartographicSurface         | 6,31           |
            | maxTotal                       | 2.160,58       |
            | sumCartographicSurfaceIncrease | 0,00           |
            | maxIncrease                    | 0,00           |
            | sumCartographicSurfaceDecrease | 6,31           |
            | maxDecrease                    | 0,01           |
        And I check the natural areas for the result for 'willinks-weust' and amount 2
        Then I select situation with label 'PROPOSED - Beoogd' and validate the 'Projectberekening' result screen with mock results for the result 'OwN2000-registratieset(alleen randhexagonen)'
            | elementId                      | expectedResult |
            | sumCartographicSurface         | 13,05          |
            | maxTotal                       | 2.171,07       |
            | sumCartographicSurfaceIncrease | 13,05          |
            | maxIncrease                    | 0,01           |
            | sumCartographicSurfaceDecrease | 0,00           |
            | maxDecrease                    | 0,00           |
        And I check the natural areas for the result for 'landgoederen-oldenzaal' and amount 2
        And I check if 'landgoederen-oldenzaal' has 29 hexagons with the following characteristics
            | elementId                         | expectedResult |
            | edgeEffectReceptorId              | 51480145       |
            | edgeEffectReceptorResult          | 0,01           |
            | edgeEffectReceptorReferenceResult | 0,00           |
            | edgeEffectReceptorProposedResult  | 0,01           |
        Then I select situation with label 'PROPOSED - Beoogd' and validate the 'Projectberekening' result screen with mock results for the result 'Relevante hexagonen(zonder randhexagonen)'
            | elementId                      | expectedResult |
            | sumCartographicSurface         | 6,31           |
            | maxTotal                       | 2.160,58       |
            | sumCartographicSurfaceIncrease | 0,00           |
            | maxIncrease                    | 0,00           |
            | sumCartographicSurfaceDecrease | 6,31           |
            | maxDecrease                    | 0,01           |
        And I check the natural areas for the result for 'willinks-weust' and amount 2
        Then I select situation with label 'PROPOSED - Beoogd' and validate the 'Projectberekening' result screen with mock results for the result 'Relevante hexagonen(alleen randhexagonen)'
            | elementId                      | expectedResult |
            | sumCartographicSurface         | 27,21          |
            | maxTotal                       | 2.171,07       |
            | sumCartographicSurfaceIncrease | 27,21          |
            | maxIncrease                    | 0,01           |
            | sumCartographicSurfaceDecrease | 0,00           |
            | maxDecrease                    | 0,00           |
        And I check the natural areas for the result for 'landgoederen-oldenzaal' and amount 2
        And I check if 'landgoederen-oldenzaal' has 31 hexagons with the following characteristics
            | elementId                         | expectedResult |
            | edgeEffectReceptorId              | 51480145       |
            | edgeEffectReceptorResult          | 0,01           |
            | edgeEffectReceptorReferenceResult | 0,00           |
            | edgeEffectReceptorProposedResult  | 0,01           |

    Scenario: Validate the text of the omitted areas in the result screen
        Given I login with correct username and password
        And I create a new source from the start page situation label 'PROPOSED'
        And I name the source 'Bron 1' and select sector group 'Industrie' and sector 'Afvalverwerking'
        And I fill location with coordinates 'POINT(252866.81 461991.67)'
        And I choose heat content type 'Niet geforceerd', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '5000' and NH3 '0'
        And I save the data
        And I duplicate the situation
        And I add situation label 'REFERENCE'
        And I select scenarioType 'REFERENCE'
        And I save the scenario updates
        And I navigate to the 'Calculation jobs' tab
        And I intercept the status of the calculation
        And I intercept the response from '/api/v8/ui/calculation/*/*/PROJECT_CALCULATION?summaryHexagonType=EXCEEDING_HEXAGONS&summaryOverlappingHexagonType=ALL_HEXAGONS*' with 'projectCalculationOmittedResult.json'
        And I create a new calculation job
        And I save the calculation job
        And I start the calculation
        And I navigate to the 'Results' tab
        And I wait for the results spinner to resolve
        Then the following warning about Ommited areas is shown: 'Onderstaand is een overzicht opgenomen van alle Natura 2000-gebieden (binnen de maximale rekenafstand van 25 km) waar in de "Beoogde situatie" een bijdrage groter dan 0,00 mol/ha/jaar is berekend, maar waar in de "Projectberekening" (=verschilberekening) geen toe- of afname is berekend. Het effect vanuit de "Projectberekening" op deze gebieden is daarmee 0,00 mol/ha/jaar.'

    Scenario: Validate the text of the omitted areas with the English language selected
        Given I login with correct username and password and with the English language selected
        And I create a new source from the start page situation label 'PROPOSED'
        And I name the source 'Bron 1' and select sector group 'industry' and sector 'Waste processing'
        And I fill location with coordinates 'POINT(252866.81 461991.67)'
        And I choose heat content type 'Natural flow', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '5000' and NH3 '0'
        And I save the data
        And I duplicate the situation
        And I add situation label 'REFERENCE'
        And I select scenarioType 'REFERENCE'
        And I save the scenario updates
        And I navigate to the 'Calculation jobs' tab
        And I intercept the status of the calculation
        And I intercept the response from '/api/v8/ui/calculation/*/*/PROJECT_CALCULATION?summaryHexagonType=EXCEEDING_HEXAGONS&summaryOverlappingHexagonType=ALL_HEXAGONS*' with 'projectCalculationOmittedResult.json'
        And I create a new calculation job
        And I save the calculation job
        And I start the calculation
        And I navigate to the 'Results' tab
        And I wait for the results spinner to resolve
        Then the following warning about Ommited areas is shown: 'Below is an overview of all Natura 2000 areas (within the maximum calculation distance of 25 km) where a contribution greater than 0.00 mol/ha/year has been calculated in the "Project situation", but where no increase or decrease has been calculated in the "Process calculation" (=difference calculation). The effect from the "Project calculation" on these areas is therefore 0.00 mol/ha/year.'
