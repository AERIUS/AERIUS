/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class InfoPanelPage {  

  static validateInfoPanelNatureAreas(dataTable: any) {
    dataTable.hashes().forEach((natureArea: any) => {
      cy.get('[aria-labelledby="' + natureArea.nature_area.toLowerCase().replaceAll(" ", "-") + '-title"]')
        .within(() => {
          cy.get('[id="infoPanelContractor"]')
            .within(() => {
              cy.get('[id="infoRowValue"]').contains(natureArea.contractor);
            });
          cy.get('[id="infoPanelSurface"]')
            .within(() => {
              cy.get('[id="infoRowValue"]').contains(natureArea.surface);
            });
          cy.get('[id="infoPanelProtection"]')
            .within(() => {
              cy.get('[id="infoRowValue"]').contains(natureArea.protection);
            });
          cy.get('[id="infoPanelStatus"]')
            .within(() => {
              cy.get('[id="infoRowValue"]').contains(natureArea.status);
            });
          cy.get('[id="infoPanelNatura2000AreaId"]')
            .within(() => {
              cy.get('[id="infoRowValue"]').contains(natureArea.areaId);
            });
          cy.get('.select-view-row').should("have.length", natureArea.habitatCount);
        });
    });
  }
  
  static validateInfoPanelNatureArea(natureAreaName: string, dataTable: any) {
    cy.get('[id="collapsibleNaturaInfo"]').contains(natureAreaName).click()
    dataTable.hashes().forEach((natureArea: any) => {
      cy.get('[id="collapsibleNaturaInfo"]').contains(natureAreaName)
      .parent()
      .parent()
      .within(() => {
        cy.get('.select-view-row').contains(natureArea.habitatcode).click().trigger("mousedown", { which: 1});
        cy.get('[id="habitatInfoPanelSurfaceRelevantMapped"] > [id="infoRowValue"]').should("contain.text", natureArea.relevantMapped);
        cy.get('[id="habitatInfoPanelSurfaceRelevantCartographic"] > [id="infoRowValue"]').should("contain.text", natureArea.relevantCartographic);
        cy.get('[id="habitatInfoPanelSurfaceExtentGoal"] > [id="infoRowValue"]').should("contain.text", natureArea.surfaceExtentGoal);
        cy.get('[id="habitatInfoPanelAreaInfoQualityGoal"] > [id="infoRowValue"]').should("contain.text", natureArea.areaInfoQualityGoal);
        cy.get('[id="habitatInfoPanelAreaInfoKDW"] > [id="infoRowValue"]').should("contain.text", natureArea.AreainfoKDW);
      });
    });
  }

  static validateInfoPanelHabitats(dataTable: any) {
    cy.get('[aria-controls="collapsibleHabitatType-content"]').click();
    if (dataTable == undefined) {
      cy.get('[id="collapsibleHabitatType-content"]')
        .within(() => {
          cy.get('.noResults').contains("Geen habitattypes");
        });
    } else {
      dataTable.hashes().forEach((habitat: any) => {
        for(var propName in habitat) {
          let containText: string = habitat[propName];
          cy.get('[id="collapsibleHabitatType-content"]').contains(containText);
        }
      });
    }
    cy.get('[aria-controls="collapsibleHabitatType-content"]').click();
  }
}
