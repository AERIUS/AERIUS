/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";
import { AgricultureSourcePage }  from "./AgricultureSourcePage";
import { CommonAgricultureSourcePage } from "../../common/CommonAgricultureSourcePage";

Given("I add a animal housing system with housing code {string}, number of animals {string} and number of days {string}", (housingCode, numberOfAnimals, numberOfDays) => {
  AgricultureSourcePage.addAnimalHousingSystemWithHousingCodeNumberOfAnimalsNumberOfDays(housingCode, numberOfAnimals, numberOfDays);
});

Given(
  "I validate the subsource with housing code {string} with number of animals {string} and emission {string}",
  (housingCode, numberOfAnimals, emission) => {
    CommonAgricultureSourcePage.validateSubsourceWithDescriptionNumberOfAnimalsEmission(housingCode, numberOfAnimals, emission);
  }
);

Given("I add a animal housing system with housing code {string} and number of animals {string}", (housingCode, numberOfAnimals) => {
  AgricultureSourcePage.addAnimalHousingSystemWithHousingCodeNumberOfAnimals(housingCode, numberOfAnimals);
});

Given(
  "the animal housing system with housing code {string}, number of animals {string}, factor {string}, days {string} and emission {string} is correctly created",
  (housingCode, numberOfAnimals, factor, days, emission) => {
    AgricultureSourcePage.animalHousingSystemWithHousingCodeDaysCreated(housingCode, numberOfAnimals, factor, days, emission);
  }
);

Given("the animal housing system with housing code {string} is copied", (animalHousingSystemWithHousingCode) => {
  AgricultureSourcePage.animalHousingSystemIsCopied(animalHousingSystemWithHousingCode);
});

Given(
  "I fill the farmland category with {string}, option {string}, amount {string} and number of days with value {string}",
  (category, option, amount, numberOfDays) => {
    AgricultureSourcePage.fillInFieldsFarmlandOptionAmountNumberOfDaysSource(category, option, amount, numberOfDays);
  }
);

Given(
  "the farmland category with {string}, option {string}, amount {string} and number of days with value {string} is correctly saved",
  (category, option, amount, numberOfDays) => {
    AgricultureSourcePage.fillInFieldsFarmlandOptionAmountNumberOfDaysSourceCorrectrlySaved(category, option, amount, numberOfDays);
  }
);

Given("I fill the Manure storage category with {string}, Area with value {string}", (category, area) => {
  AgricultureSourcePage.fillInFieldsManureStorageSource(category, area);
});

Given("farmland category Manure storage with Description {string} and area {string} is correctly created", (category, area) => {
  AgricultureSourcePage.fillInFieldsManureStorageSourceCorrectlySaved(category, area);
});

Given(
  "ADMS source characteristics with source height {string} and source diameter {string} is correctly saved",
  (sourceHeight, sourceDiameter) => {
    AgricultureSourcePage.assertADMSSourceHeightAndDiameter(sourceHeight, sourceDiameter);
  }
);

Given("I edit the animal housing system to housing code {string} and number of animals {string}", (housingCode, numberOfAnimals) => {
  AgricultureSourcePage.editAnimalHousingSystemWithHousingCodeAndNumberOfAnimals(housingCode, numberOfAnimals);
});
