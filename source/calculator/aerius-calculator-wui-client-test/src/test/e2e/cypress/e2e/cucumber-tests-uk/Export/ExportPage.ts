/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class ExportPage {
  static assertAdditionalInfoPanelHidden() {
    cy.get('[id="includingExtraInfoToggle"]').should("not.exist");
    cy.get('[id="additional-info-line"]').should("not.exist");
    cy.get('[id="scenarioMetaDataCorporation"]').should("not.exist");
    cy.get('[id="scenarioMetaDataProjectName"]').should("not.exist");
    cy.get('[id="scenarioMetaDataStreetAddress"]').should("not.exist");
    cy.get('[id="scenarioMetaDataPostcode"]').should("not.exist");
    cy.get('[id="scenarioMetaDataCity"]').should("not.exist");
    cy.get('[id="scenarioMetaDataDescription"]').should("not.exist");
    cy.get('[id="agreeDataInclusion"]').should("not.exist");
  }
}
