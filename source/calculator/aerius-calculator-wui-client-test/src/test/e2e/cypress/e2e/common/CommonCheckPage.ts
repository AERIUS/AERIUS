/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

/*
* Legacy implementation for lodging system with RAV and BWL for product LBV / check
* Product LBV / check does not support new Housing emission for Omgevingswet
*/

import { CommonAgricultureSourcePage } from "./CommonAgricultureSourcePage";

export class CommonCheckPage {
  
  static addLodgingSystemWithRavCodeNumberOfAnimalsBwlCode(ravCode: string, numberOfAnimals: string, bwlCode: string) {
    CommonAgricultureSourcePage.openSubsourcesPanel();
    CommonAgricultureSourcePage.createNewSubSource();

    this.selectRavCodePanel();
    this.fillInRavCode(ravCode);

    this.fillInNumberOfAnimals(numberOfAnimals);
    this.fillInBwlCode(bwlCode);
  }

  static selectRavCodePanel() {
    cy.get('[id="tab-farmLodgingType-STANDARD"]').click({ force: true });
  }

   static fillInRavCode(ravCode: string) {
    cy.get('[id="lodgingCodesItem"]').type(ravCode);
    cy.get('[id="' + ravCode.replace(" ", "-") + '"]')
      .should("contain", ravCode)
      .click();
  }

  static fillInNumberOfAnimals(numberOfAnimals: string) {
    cy.get('[id="numberOfAnimals"]').clear().type(numberOfAnimals);
  }

  static fillInBwlCode(bwlCode: string) {
    cy.get('[id="bwlCodeListBox"]').select(bwlCode);
  }

  static validateSubsourceWithDescriptionNumberOfAnimalsEmission(description: string, numberOfAnimals: string, emission: string) {
    cy.get('[id="subSourceListDescription"]').contains(description).parent().get('[id="subSourceListEmission"]').should("have.text", emission);
    cy.get('[id="subSourceListDescription"]').contains(description).parent().get('[id="subSourceListAmount"]').should("have.text", numberOfAnimals);
  }
}
