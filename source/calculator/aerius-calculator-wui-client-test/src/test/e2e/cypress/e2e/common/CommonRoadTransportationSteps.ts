/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";
import { CommonRoadTransportationPage } from "./CommonRoadTransportationPage";

Given("the title for {string} road subsource {string} is {string}", (type, subsourceIndex, title) => {
  CommonRoadTransportationPage.assertRoadSubsourceTitleIs(type, subsourceIndex, title);
});

Given("I select tunnel factor {int}", (tunnelFactor: number) => {
  CommonRoadTransportationPage.selectTunnelFactor(tunnelFactor);
});

Given("I choose road elevation type {string}", (roadElevationType: string) => {
  CommonRoadTransportationPage.chooseRoadTransportationElevationType(roadElevationType);
});

Given("I validate that the minimum elevation height is {int}, and the max is {int}, and decimals are not allowed", (minElevationHeight: number, maxElevationHeight: number) => {
  CommonRoadTransportationPage.validateElevationHeightLimits(minElevationHeight, maxElevationHeight);
});

Given("I choose road elevation type {string} and elevation height {string}", (roadElevationType: string, elevationHeight: string) => {
  CommonRoadTransportationPage.chooseRoadTransportationElevation(roadElevationType, elevationHeight);
});

Given("I validate that I can't set an elevation height", () => {
  CommonRoadTransportationPage.validateElevationHeightCantBeSet();
});

Given(
  "I choose as road side barrier type from A to B {string}, barrier height {string} and barrier distance {string}",
  (barrierType, barrierHeight, barrierDistance) => {
    CommonRoadTransportationPage.chooseRoadTransportationBarrierFromAToB(barrierType, barrierHeight, barrierDistance);
  }
);

Given(
  "I choose as road side barrier type from B to A {string}, barrier height {string} and barrier distance {string}",
  (barrierType: string, barrierHeight: string, barrierDistance: string) => {
    CommonRoadTransportationPage.chooseRoadTransportationBarrierFromBToA(barrierType, barrierHeight, barrierDistance);
  }
);

Given("I click to add a emission source of type {string}", (emissionSourceType: string) => {
  CommonRoadTransportationPage.addEmissionSourceOfType(emissionSourceType);
});

Given("I open the panel for the vehicle information", () => {
  CommonRoadTransportationPage.openEmissionSourcePanel();
});

Given("I fill emission {string} per vehicle {string}", (emission: string, fraction: string) => {
  CommonRoadTransportationPage.fillEmissionAndFraction(emission, fraction);
});

Given(
  "source characteristics with road type {string}, tunnel factor {string}, elevation type {string} and elevation height {string} is correctly saved",
  (roadType: string, tunnelFactor: string, elevationType: string, elevationHeight: string) => {
    CommonRoadTransportationPage.sourceCharacteristicsCorrectlySaved(roadType, tunnelFactor, elevationType, elevationHeight);
  }
);

Given(
  "road side barrier type from A to B {string}, barrier height {string}, barrier height warning {string}, barrier distance {string} and barrier distance warning {string} is correctly saved",
  (barrierType: string, barrierHeight: string, barrierHeightWarning: string, barrierDistance: string, barrierDistanceWarning: string) => {
    CommonRoadTransportationPage.barrierAToBHeightDistanceWarningCorrectlySaved(barrierType, barrierHeight, barrierHeightWarning, barrierDistance, barrierDistanceWarning);
  }
);

Given(
  "road side barrier type from B to A {string}, barrier height {string}, barrier height warning {string}, barrier distance {string} and barrier distance warning {string} is correctly saved",
  (barrierType: string, barrierHeight: string, barrierHeightWarning: string, barrierDistance: string, barrierDistanceWarning: string) => {
    CommonRoadTransportationPage.barrierBToAHeightDistanceWarningCorrectlySaved(barrierType, barrierHeight, barrierHeightWarning, barrierDistance, barrierDistanceWarning);
  }
);

Given(
  "road side barrier type from A to B {string}, barrier height {string} and barrier distance {string} is correctly saved",
  (barrierType: string, barrierHeight: string, barrierDistance: string) => {
    CommonRoadTransportationPage.barrierAToBCorrectlySaved(barrierType, barrierHeight, barrierDistance);
  }
);

Given(
  "road side barrier type from A to B {string}, barrier height {string}, barrier height warning {string} and barrier distance {string} is correctly saved",
  (barrierType: string, barrierHeight: string, barrierHeightWarning: string, barrierDistance: string) => {
    CommonRoadTransportationPage.barrierAToBHeightWarningCorrectlySaved(barrierType, barrierHeight, barrierHeightWarning, barrierDistance);
  }
);

Given(
  "road side barrier type from B to A {string}, barrier height {string} and barrier distance {string} is correctly saved",
  (barrierType: string, barrierHeight: string, barrierDistance: string) => {
    CommonRoadTransportationPage.barrierBToACorrectlySaved(barrierType, barrierHeight, barrierDistance);
  }
);

Given(
  "road side barrier type from B to A {string}, barrier height {string}, barrier height warning {string} and barrier distance {string} is correctly saved",
  (barrierType: string, barrierHeight: string, barrierHeightWarning: string, barrierDistance: string) => {
    CommonRoadTransportationPage.barrierBToAHeightWarningCorrectlySaved(barrierType, barrierHeight, barrierHeightWarning, barrierDistance);
  }
);

Given("I change the {string} to {string}", (contentType: string, ventilationType: string) => {
  CommonRoadTransportationPage.changeSourceCharacteristic(contentType, ventilationType)
});

Given("I select time unit {string} for cold starts", (timeUnit: string) => {
  CommonRoadTransportationPage.selectTimeUnitForColdStarts(timeUnit)
});

Given("I fill in the following number of vehicles", (dataTable: any) => {
  CommonRoadTransportationPage.fillInNumberOfVehicles(dataTable)
});

Given("the following number of vehicles is correctly saved", (dataTable: any) => {
  CommonRoadTransportationPage.assertNumberOfVehicles(dataTable)
});

Given("the time unit {string} is correctly saved", (timeUnit: string) => {
  CommonRoadTransportationPage.assertTimeUnit(timeUnit)
});

Given("the following source characteristics are correctly saved", (dataTable: any) => {
  CommonRoadTransportationPage.assertSourceCharacteristics(dataTable)
});

Given("I choose Euro class {string} from custom", (euroClass: string) => {
  CommonRoadTransportationPage.chooseEuroClassFromCustom(euroClass);
});

Given("the chosen custom Euro class should be {string}", (euroClass: string) => {
  CommonRoadTransportationPage.validateChosenCustomEuroClass(euroClass);
});

Given("I choose Euro class {string} from specific", (euroClass: string) => {
  CommonRoadTransportationPage.chooseEuroClassFromSpecific(euroClass);
});

Given("the chosen specific Euro class should be {string}", (euroClass: string) => {
  CommonRoadTransportationPage.validateChosenSpecificEuroClass(euroClass);
});

Given("Euro class {string} is correctly saved", (euroClass: string) => {
  CommonRoadTransportationPage.euroClassIsCorrectlySaved(euroClass);
});

Given("emission {string} per vehicle is updated with {string}", (emission: string, fraction: string) => {
  CommonRoadTransportationPage.assertEmissionsUpdatedAfterSelection(emission, fraction);
});
