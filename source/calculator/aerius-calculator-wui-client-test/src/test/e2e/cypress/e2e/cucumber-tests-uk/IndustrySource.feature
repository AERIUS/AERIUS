#
# Crown copyright
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Industry source

    Scenario: Create new Industry source starting at the start page
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Industry' and sector 'Waste processing'
        And I fill location with coordinates 'POINT(473383.26 415592.23)'
        And I choose source height '25' and source diameter '10'
        Then Building influence should 'exist'
        And I chooce source Specify temperature or density 'Temperature'
        Then Temperature should 'exist'
        And I chooce source Specify temperature or density 'Density'
        #Then Density should 'exist'
        And I chooce source Specify temperature or density 'Ambient'
        And I choose source efluxtype 'Velocity'
        Then VerticalVelocity should 'exist'
        And I choose source efluxtype 'Volume'
        #Then VolumetricFlowRate should 'exist'
        And I fill the emission fields with NOx '10' and NH3 '20000'
        And I save the data
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Industry' and sector 'Waste processing' is correctly saved
        And location with coordinates 'X:473383.26 Y:415592.23' is correctly saved
        And the total 'generic' emission 'NOx' is '10.0 kg/y'
        And the total 'generic' emission 'NH3' is '20.0 tonne/y'

# Sector 'House and Office' not supported currently
#    Scenario: Create new House and Office source starting at the start page
#        Given I login with correct username and password
#        And I create a new source from the start page
#        And I name the source 'Bron 1' and select sector group 'House and Office' and sector 'Recreation'
#        And I fill location with coordinates 'POINT(473383.26 415592.23)'
#        And I choose source height '25' and source diameter '10'
#        And I fill the emission fields with NOx '10' and NH3 '20000'
#        And I save the data
#        Then I select the source 'Bron 1'
#        Then the source 'Bron 1' with sector group 'House and Office' and sector 'Recreation' is correctly saved
#        And location with coordinates 'X:473383.26 Y:415592.23' is correctly saved
#        And the total 'generic' emission 'NOx' is '10.0 kg/y'
#        And the total 'generic' emission 'NH3' is '20.0 tonne/y'

# Sector 'Rail transport' not supported currently
#    Scenario: Create new Rail transport source starting at the start page
#        Given I login with correct username and password
#        And I create a new source from the start page
#        And I name the source 'Bron 1' and select sector group 'Rail transport' and sector 'Railway'
#        And I fill location with coordinates 'POINT(473383.26 415592.23)'
#        And I choose source height '25' and source diameter '10'
#        And I fill the emission fields with NOx '10' and NH3 '20000'
#        And I save the data
#        Then I select the source 'Bron 1'
#        Then the source 'Bron 1' with sector group 'Rail transport' and sector 'Railway' is correctly saved
#        And location with coordinates 'X:473383.26 Y:415592.23' is correctly saved
#        And the total 'generic' emission 'NOx' is '10.0 kg/y'
#        And the total 'generic' emission 'NH3' is '20.0 tonne/y'

# Sector 'Aviation' not supported currently
#    Scenario: Create new Aviation source starting at the start page
#        Given I login with correct username and password
#        And I create a new source from the start page
#        And I name the source 'Bron 1' and select sector group 'Aviation' and sector 'Taxiing'
#        And I fill location with coordinates 'POINT(473383.26 415592.23)'
#        And I choose source height '25' and source diameter '10'
#        And I fill the emission fields with NOx '10' and NH3 '20000'
#        And I save the data
#        Then I select the source 'Bron 1'
#        Then the source 'Bron 1' with sector group 'Aviation' and sector 'Taxiing' is correctly saved
#        And location with coordinates 'X:473383.26 Y:415592.23' is correctly saved
#        And the total 'generic' emission 'NOx' is '10.0 kg/y'
#        And the total 'generic' emission 'NH3' is '20.0 tonne/y'
