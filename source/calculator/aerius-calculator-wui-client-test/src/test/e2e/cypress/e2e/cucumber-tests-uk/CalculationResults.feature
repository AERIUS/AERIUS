#
# Crown copyright
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Calculation results

    Scenario: Create new Energy source, start calculation and validate result screens
        Given I create a simple uk emission source from the start page
        When I navigate to the 'Calculation jobs' tab
        Then I validate the calculation job message 'There are no calculation jobs yet. Create a new calculation job by pressing the button below.'
        And I create a new calculation job
        And I select country 'England'
        And I select project category 'Combustion plant'
        And I select development pressure sources
            | source   |
            | Source 1 |
        And I select the uk meteorological site location selection method 'NWP'
        And I select the uk meteorological site location selection method 'Search'
        And I select the first meteorological site location
        And I select the meteorological year '2021'
        And I save the calculation job
        Then I start an UK calculation mock the finish and validate the result screen

    Scenario: Create new Industry source start calculation with reference, temporary and saldering situation and validate result screens
        Given I login with correct username and password
        And I create a new source from the start page situation label 'PROPOSED'
        And I name the source 'Bron 1' and select sector group 'Energy' and sector 'Other'
        And I fill location with coordinates 'POINT(340816.79 729703.62)'
        And I choose source height '25' and source diameter '0.5'
        And I fill the emission fields with NOx '8' and NH3 '40'
        And I save the data
        And I duplicate the situation
        And I add situation label 'TEMPORARY'
        And I select scenarioType 'TEMPORARY'
        And I save the scenario updates
        And I select the source 'Bron 1'
        And I choose to edit the source
        And I fill the emission fields with NOx '10' and NH3 '50'
        And I save the data
        And I duplicate the situation
        And I add situation label 'REFERENCE'
        And I select scenarioType 'REFERENCE'
        And I save the scenario updates
        And I select the source 'Bron 1'
        And I choose to edit the source
        And I fill the emission fields with NOx '22' and NH3 '220'
        And I save the data
        And I duplicate the situation
        And I add situation label 'OFF_SITE_REDUCTION'
        And I select scenarioType 'OFF_SITE_REDUCTION'
        And I save the scenario updates
        And I select the source 'Bron 1'
        And I choose to edit the source
        And I fill the emission fields with NOx '5' and NH3 '120'
        And I save the data
        And I duplicate the situation
        And I add situation label 'COMBINATION_PROPOSED'
        And I select scenarioType 'COMBINATION_PROPOSED'
        And I save the scenario updates
        And I select the source 'Bron 1'
        And I choose to edit the source
        And I fill the emission fields with NOx '4' and NH3 '110'
        And I save the data
        And I duplicate the situation
        And I add situation label 'COMBINATION_REFERENCE'
        And I select scenarioType 'COMBINATION_REFERENCE'
        And I save the scenario updates
        And I select the source 'Bron 1'
        And I choose to edit the source
        And I fill the emission fields with NOx '3' and NH3 '90'
        And I save the data
        When I navigate to the 'Calculation jobs' tab
        Then I validate the calculation job message 'There are no calculation jobs yet. Create a new calculation job by pressing the button below.'
        And I create a new calculation job        
        When I select calculation jobType 'Maximum temporary effect'
        And I select country 'Wales'        
        And I select project category 'Combustion plant'
        And I select the situation 'REFERENCE' as 'referenceSituation'
        And I select the situation 'OFF_SITE_REDUCTION' as 'off_site_reductionSituation'
        And I select included calculation scenario 'TEMPORARY'
        And I select the uk meteorological site location selection method 'NWP'
        And I select the uk meteorological site location selection method 'Search'
        And I select the first meteorological site location
        And I select the meteorological year '2021'
        And I save the calculation job

        Then I start an UK calculation mock the finish and validate the result screen
        Then I validate that the situation 'REFERENCE - Reference' has results combinations
            | resultSelectId     | resultSelectLabel                          |
            | resultsResultType  | Scenario contribution                      |
            | resultsHexagonType | Fixed assessment grid                      |
        Then I validate that the situation 'OFF_SITE_REDUCTION - Off-site reduction' has results combinations
            | resultSelectId     | resultSelectLabel                          |
            | resultsResultType  | Scenario contribution                      |
            | resultsHexagonType | Fixed assessment grid                      |
        Then I validate that the situation 'TEMPORARY - Temporary' has results combinations
            | resultSelectId     | resultSelectLabel                          |
            | resultsResultType  | Scenario contribution                      |
            | resultsHexagonType | Fixed assessment grid                      |
            | resultsResultType  | Maximum temporary effect                   |
            | resultsHexagonType | Fixed assessment grid                      |

    @Smoke
    Scenario: Create new Industry source start calculation with reference, saldering, in combination project and in combination reference situation and validate result screens
        Given I login with correct username and password
        And I create a new source from the start page situation label 'PROPOSED'
        And I name the source 'Bron 1' and select sector group 'Energy' and sector 'Other'
        And I fill location with coordinates 'POINT(340816.79 729703.62)'
        And I choose source height '25' and source diameter '0.5'
        And I fill the emission fields with NOx '8' and NH3 '40'
        And I save the data
        And I duplicate the situation
        And I add situation label 'TEMPORARY'
        And I select scenarioType 'TEMPORARY'
        And I save the scenario updates
        And I select the source 'Bron 1'
        And I choose to edit the source
        And I fill the emission fields with NOx '10' and NH3 '50'
        And I save the data
        And I duplicate the situation
        And I add situation label 'REFERENCE'
        And I select scenarioType 'REFERENCE'
        And I save the scenario updates
        And I select the source 'Bron 1'
        And I choose to edit the source
        And I fill the emission fields with NOx '22' and NH3 '220'
        And I save the data
        And I duplicate the situation
        And I add situation label 'OFF_SITE_REDUCTION'
        And I select scenarioType 'OFF_SITE_REDUCTION'
        And I save the scenario updates
        And I select the source 'Bron 1'
        And I choose to edit the source
        And I fill the emission fields with NOx '5' and NH3 '120'
        And I save the data
        And I duplicate the situation
        And I add situation label 'COMBINATION_PROPOSED'
        And I select scenarioType 'COMBINATION_PROPOSED'
        And I save the scenario updates
        And I select the source 'Bron 1'
        And I choose to edit the source
        And I fill the emission fields with NOx '4' and NH3 '110'
        And I save the data
        And I duplicate the situation
        And I add situation label 'COMBINATION_REFERENCE'
        And I select scenarioType 'COMBINATION_REFERENCE'
        And I save the scenario updates
        And I select the source 'Bron 1'
        And I choose to edit the source
        And I fill the emission fields with NOx '3' and NH3 '90'
        And I save the data
        When I navigate to the 'Calculation jobs' tab
        Then I validate the calculation job message 'There are no calculation jobs yet. Create a new calculation job by pressing the button below.'
        And I create a new calculation job
        When I select calculation jobType 'In-combination process contribution'
        And I select country 'Scotland'        
        And I select project category 'Combustion plant'
        And I select the situation 'REFERENCE' as 'referenceSituation'
        And I select the situation 'OFF_SITE_REDUCTION' as 'off_site_reductionSituation'
        And I select In-Combination include project from archive
        And I select In-Combination reference scenario 'COMBINATION_REFERENCE'
        And I select In-Combination project scenario 'COMBINATION_PROPOSED'
        And I select development pressure sources
            | source |
            | Bron 1 |
        And I select the uk meteorological site location selection method 'NWP'
        And I select the uk meteorological site location selection method 'Search'
        And I select the first meteorological site location
        And I select the meteorological year '2021'
        And I save the calculation job
        Then I start an UK calculation mock the finish and validate the result screen
        Then I validate that the situation 'REFERENCE - Reference' has results combinations
            | resultSelectId     | resultSelectLabel                          |
            | resultsResultType  | Scenario contribution                      |
            | resultsHexagonType | Fixed assessment grid                      |
        Then I validate that the situation 'PROPOSED - Project' has results combinations
            | resultSelectId     | resultSelectLabel                          |
            | resultsResultType  | Scenario contribution                      |
            | resultsHexagonType | Fixed assessment grid                      |
            | resultsResultType  | In combination                             |
            | resultsHexagonType | Fixed assessment grid                      |
            | resultsResultType  | Archive contribution                       |
            | resultsHexagonType | Fixed assessment grid                      |
        Then I validate that the situation 'OFF_SITE_REDUCTION - Off-site reduction' has results combinations
            | resultSelectId     | resultSelectLabel                          |
            | resultsResultType  | Scenario contribution                      |
            | resultsHexagonType | Fixed assessment grid                      |
        Then I validate that the situation 'COMBINATION_REFERENCE - In-combination reference' has results combinations
            | resultSelectId     | resultSelectLabel                          |
            | resultsResultType  | Scenario contribution                      |
            | resultsHexagonType | Fixed assessment grid                      |
        Then I validate that the situation 'COMBINATION_PROPOSED - In-combination project' has results combinations
            | resultSelectId     | resultSelectLabel                          |
            | resultsResultType  | Scenario contribution                      |
            | resultsHexagonType | Fixed assessment grid                      |

    Scenario: Create new Other source start In-combination process contribution include projects from archive and validate Archive contribution result screens
        Given I login with correct username and password
        And I create a new source from the start page situation label 'Scenario 1'
        And I name the source 'Source 1' and select sector group 'Other'
        And I fill location with coordinates 'POINT(421169.55 1071372.82)'
        And I choose source height '25' and source diameter '10'
        And I fill the emission fields with NOx '1000' and NH3 '0'
        And I save the data

        And I intercept the response from 'api/v8/ui/calculation/*/summary' with 'uk_in_combination_archive_summary.json'
        And I intercept the response from 'api/v8/ui/calculation/*/*/ARCHIVE_CONTRIBUTION?summaryHexagonType=EXCEEDING_HEXAGONS*' with 'uk_in_combination_archive_contribution_exceeding_hexagons.json'
        And I intercept the response from 'api/v8/ui/calculation/*/*/ARCHIVE_CONTRIBUTION?summaryHexagonType=RELEVANT_HEXAGONS*' with 'uk_in_combination_archive_archive_contribution_relevant_hexagons.json'

        And I navigate to the 'Calculation jobs' tab
        When I create a new calculation job
        When I select calculation jobType 'In-combination process contribution'
        And I select the uk calculation method "Quick run"
        And I select zone of influence "500"
        And I select the situation 'Scenario 1' as 'proposedSituation'
        And I select In-Combination include project from archive
        And I select country 'Northern Ireland'        
        And I select project category 'Combustion plant'
        And I select development pressure sources
            | source   |
            | Source 1 |
        And I select the uk meteorological site location selection method 'NWP'
        And I select the uk meteorological site location selection method 'Search'
        And I select the first meteorological site location
        And I select the meteorological year '2021'
        And I save the calculation job
        Then I start an UK calculation mock the finish and validate the result screen

        And I navigate to the 'Results' tab
        Then I select calculation results tab 'Results'
        Then I validate that the situation 'Scenario 1 - Project' has results combinations
            | resultSelectId     | resultSelectLabel                          |
            | resultsResultType  | Scenario contribution                      |
            | resultsHexagonType | Fixed assessment grid                      |
            | resultsResultType  | Process contribution                       |
            | resultsHexagonType | Fixed assessment grid                      |
            | resultsResultType  | In combination                             |
            | resultsHexagonType | Fixed assessment grid                      |
            | resultsResultType  | Archive contribution                       |
            | resultsHexagonType | Fixed assessment grid                      |

        Then I select calculation results tab 'Results'
        Then I select situation with label 'Scenario 1 - Project' and validate the 'Archive contribution' with Pollutant 'Deposition NOₓ + NH₃' and results for 'Fixed assessment grid'
            | elementId                      | expectedResult |
            | sumCartographicSurface         | 290.58         |
            | maxPercentageCriticalLevel     | 1 %            |
            | sumCartographicSurfaceIncrease | 290.58         |
            | maxIncrease                    | 0.118          |
            | sumCartographicSurfaceDecrease | 0.00           |
            | maxDecrease                    | 0.000          |
        And I check if the critical levels tab has 2 results
        And I check if the deposition tab has 2 results
        And I check if the markers tab has 2 results and the text 'Highest PEC (kg N/ha/y)'
        And I check if the habitat tab has 2 results

        Then I select calculation results tab 'Summary'
        And I check if the summary tab has 2 scenario contributions results
        And I check if the summary tab has 6 process contributions results

        Then I select calculation results tab 'Scenarios'
        Then I select calculation results tab 'Scenarios' and validate
            | elementId                            | expectedResult                       |
            | .situationsContainer > h2            | User Scenarios                       |
            | [id=situationName]                   | Scenario 1                           |
            | [id=situationType]                   | Project                              |
            | [id=situationVersion]                | 2023.1-SNAPSHOT_20231128_3e1cd250e8  |
            | .archiveProjectsContainer > h2       | In-combination Archive projects      |
            | [id=archiveProjectId]                | 1                                    |
            | [id=archiveProjectName]              | space already in use                 |
            | [id=archiveProjectType]              | type                                 |
            | [id=archiveProjectPermitReference]   | permit reference                     |
            | [id=archiveProjectPlanningReference] | plan reference                       |
            | [id=archiveProjectNetEmissionsNOx    | 2.1 kg/y                             |
            | [id=archiveProjectNetEmissionsNH3    | 0.1 kg/y                             |
            | [id=archiveProjectVersion]           | 2023.1-SNAPSHOT_20231115_6b22f099ea  |
        And I validate that Archive project results are retrieved on 'Results retrieved on 28 November 2023'

Scenario: Validate the text of the omitted areas in the result screen
        Given I login with correct username and password
        And I create a new source from the start page situation label 'PROPOSED'
        And I name the source 'Source 1' and select sector group 'Other'
        And I name the source 'Bron 1' and select sector group 'Energy' and sector 'Other'
        And I fill location with coordinates 'POINT(369192.93 397360.63)'
        And I choose source height '0.5' and source diameter '1'
        And I fill the emission fields with NOx '10' and NH3 '0'
        And I save the data
        And I duplicate the situation
        And I add situation label 'REFERENCE'
        And I select scenarioType 'REFERENCE'
        And I save the scenario updates
        And I intercept the response from '/api/v8/ui/calculation/*/*/PROJECT_CALCULATION?summaryHexagonType=RELEVANT_HEXAGONS&summaryOverlappingHexagonType=ALL_HEXAGONS*' with 'uk_project_calculation_omitted_result.json'
        And I navigate to the 'Calculation jobs' tab
        And I create a new calculation job
        And I select calculation jobType 'Process contribution'
        And I select the uk calculation method "Quick run"
        And I select zone of influence "50"
        And I select the situation 'REFERENCE' as 'referenceSituation'
        And I select country 'England'        
        And I select project category 'Combustion plant'
        And I select development pressure sources
            | source |
            | Bron 1 |
        And I select the uk meteorological site location selection method 'NWP'
        And I select the uk meteorological site location selection method 'Search'
        And I select the first meteorological site location
        And I select the meteorological year '2021'
        And I save the calculation job
        And I start an UK calculation mock the finish and validate the result screen
        And I navigate to the 'Results' tab
        And I wait for the results spinner to resolve
        Then the following warning about Ommited areas is shown: 'Below is a list of all nature conservation areas (within the specified calculation distance) where a "Scenario contribution" greater than zero has been calculated for the "Project" scenario, but no increase or decrease has been calculated for the "Process contribution". The effect of the "Project" scenario in relation to the "Reference" scenario is therefore zero.'

   Scenario: Notice when results are below decision making thresholds
        Given I login with correct username and password
        And I select file 'UKAPAS_Calculationjob1_below_DMT.zip' to be imported
        And I navigate to the 'Results' tab
        When I select calculation results tab 'Decision Framework'
        And I wait for the results spinner to resolve        
        Then the following warning is visible: 'The calculation results in a Process Contribution where no further assessment of aerial emissions of nitrogen is required.'

   Scenario: Notice when results are above decision making thresholds
        Given I login with correct username and password
        And I select file 'UKAPAS_Calculationjob1_above_DMT.zip' to be imported
        And I navigate to the 'Results' tab
        When I select calculation results tab 'Decision Framework'
        And I wait for the results spinner to resolve        
        Then the following warning is visible: 'The calculation results in a Process Contribution which requires further assessment. Please check the relevant thresholds per site and pollutant.'        
        And the following text about the site-relevant threshold class is visible: 'Development pressure class: Very Low'
