/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class CommonAgricultureSourcePage {
  static openSubsourcesPanel() {
    cy.get('[id="collapsibleSubSources"] > .line > .title').click({
      force: true
    });
  }

  static createNewSubSource() {
    cy.get('[id="sourceListButtonNewSource"] > .icon').click({ force: true });
  }

  static selectHousingCodePanel() {
    cy.get('[id="tab-farmAnimalHousingType-STANDARD"]').click({ force: true });
  }

  static selectOwnSpecificationPanel() {
    cy.get('[id="tab-farmAnimalHousingType-CUSTOM"]').click({ force: true });
  }

  static fillInHousingCode(housingCode: string) {
    cy.get('[id="farmlandStandardCategoryCodeItem"]').clear({ force: true }).type(housingCode);
    cy.get('[id="' + housingCode.replace(" ", "-") + '"]')
      .should("contain", housingCode)
      .click();
  }

  static fillInNumberOfAnimals(numberOfAnimals: string) {
    cy.get('[id="farmAnimalHousingStandardNumberOfAnimals"]').clear({ force: true }).type(numberOfAnimals);
  }

  static fillInNumberOfAnimalsForAdditionalTechnique(numberOfAnimals: string) {
    cy.get('[id="additionalSystem"]').parent().find('[id="farmAnimalHousingStandardNumberOfAnimals"]').clear({ force: true }).type(numberOfAnimals);
  }

  static assertSubsourceIsEmpty() {
    this.validateAnimalHousingSubsourceStandardRowHasValues("", "0", "-", "", "0,0 kg/j");
  }

  static validateSubsourceToHaveValues(dataTable: any) {
    let additionalIndex = 0;
    dataTable.hashes().forEach((animalHousing: any) => {
      switch (animalHousing.typeOfRule) {
        case "Standard":
          this.validateAnimalHousingSubsourceStandardRowHasValues(animalHousing.housingCode, animalHousing.numberOfAnimals, animalHousing.factor, animalHousing.reduction, animalHousing.emission, animalHousing.strikeThrough);
          break;
        case "Additional":
          this.validateAnimalHousingSubsourceAdditionalRowHasValues(animalHousing.housingCode, animalHousing.numberOfAnimals, animalHousing.factor, animalHousing.reduction, animalHousing.emission, animalHousing.strikeThrough, additionalIndex);
          additionalIndex++;
          break;
        default:
          throw new Error("Unknown Animal housing rule type: " + animalHousing.typeOfRule);
      }
    });
  }

  static validateAnimalHousingSubsourceStandardRowHasValues(housingCode: string, numberOfAnimals: string, factor: string, reduction: string, emission: string, strikeThrough = "", index = 0) {
    cy.get('[id="farmAnimalHousingDetailStandardRow-0"]').eq(index).find('[id="farmAnimalHousingDetailStandardCode"]').should("have.text", housingCode);
    cy.get('[id="farmAnimalHousingDetailStandardRow-0"]').eq(index).find('[id="farmAnimalHousingDetailStandardNumberOfAnimals"]').should("have.text", numberOfAnimals);
    cy.get('[id="farmAnimalHousingDetailStandardRow-0"]').eq(index).find('[id="farmAnimalHousingDetailStandardEmissionFactor"] > div').should("have.text", factor);
    if (reduction === "") {
      cy.get('[id="farmAnimalHousingDetailStandardRow-0"]').eq(index).find('[id="farmAnimalHousingDetailStandardReduction"]').should("not.exist");
    } else {
      cy.get('[id="farmAnimalHousingDetailStandardRow-0"]').eq(index).find('[id="farmAnimalHousingDetailStandardReduction"]').should("have.text", reduction);
    }
    cy.get('[id="farmAnimalHousingDetailStandardRow-0"]').eq(index).find('[id="farmAnimalHousingDetailStandardEmission"] > div').should("have.text", emission);

    if (strikeThrough === "true") {
      cy.get('[id="farmAnimalHousingDetailStandardRow-0"]').eq(index).find('[id="farmAnimalHousingDetailStandardEmission"]').should('have.css', 'text-decoration').and("match", /line-through/);
    } else if (strikeThrough === "false") {
      cy.get('[id="farmAnimalHousingDetailStandardRow-0"]').eq(index).find('[id="farmAnimalHousingDetailStandardEmission"]').should('have.css', 'text-decoration').and("not.match", /line-through/);
    }
  }

  static validateAnimalHousingSubsourceAdditionalRowHasValues(housingCode: string, numberOfAnimals: string, factor: string, reduction: string, emission: string, strikeThrough = null, index = 0) {
    cy.get('[id^="farmAdditionalHousingSystemRow-"]').eq(index).find('[id="farmAdditionalHousingSystemDetailName"]').should("have.text", housingCode);
    cy.get('[id^="farmAdditionalHousingSystemRow-"]').eq(index).find('[id="farmAdditionalHousingSystemDetailNumberOfAnimals"]').should("have.text", numberOfAnimals);
    cy.get('[id^="farmAdditionalHousingSystemRow-"]').eq(index).find('[id="farmAdditionalHousingSystemDetailFactor"]').should("have.text", factor);
    cy.get('[id^="farmAdditionalHousingSystemRow-"]').eq(index).find('[id="farmAdditionalHousingSystemDetailReduction"]').should("have.text", reduction);
    cy.get('[id^="farmAdditionalHousingSystemRow-"]').eq(index).find('[id="farmAdditionalHousingSystemDetailEmission"]').should("have.text", emission);

    if (strikeThrough === "true") {
      cy.get('[id^="farmAdditionalHousingSystemRow-"]').eq(index).find('[id="farmAdditionalHousingSystemDetailEmission"]').should('have.css', 'text-decoration').and("match", /line-through/);
    } else if (strikeThrough === "false") {
      cy.get('[id^="farmAdditionalHousingSystemRow-"]').eq(index).find('[id="farmAdditionalHousingSystemDetailEmission"]').should('have.css', 'text-decoration').and("not.match", /line-through/);
    }
  }

  static createEmptyAnimalHousingSubsource() {
    this.openSubsourcesPanel();
    this.createNewSubSource();
  }

  static addSystemWithHousingCodeNumberOfAnimals(housingCode: string, numberOfAnimals: string) {
    this.openSubsourcesPanel();
    this.createNewSubSource();
    this.selectHousingCodePanel();

    this.fillInHousingCode(housingCode);
    this.fillInNumberOfAnimals(numberOfAnimals);
  }

  static validateAdditionalHousingIsAvailable(dataTable: any) {
    let rowCount: number = dataTable.hashes().length;
    // Open the dropdown list
    cy.get('[id="farmAnimalHousingAdditionalCodesItem"]').type(" ");
    cy.get('[id="farmAnimalHousingAdditionalCodesItem"]').parent().within( () => {
      cy.get('li').should("have.length", rowCount)
    });
    dataTable.hashes().forEach((additionalHouding: any, index: number) => {
      cy.get('[id="farmAnimalHousingAdditionalCodesItem"]').parent().within( () => {
        cy.get('li').eq(index).should("have.text", additionalHouding.rule);
      });
    });
  };

  static selectAnimalHousingSystemWithHousingCode(housingCode: string) {
    cy.get('[id="subSourceListDescription"]').contains(housingCode).click();
  }

  static housingRowCount(count: number) {
    // Validate that we only have {count} rows
    cy.get('[id="farmAnimalHousingDetailStandardRows"] >').should("have.length", count);
  }

  static deleteAnimalHousingSystem() {
    cy.get('[id="sourceListButtonDeleteSource"] > .icon').click();
  }

  static assertHousingCodeIsNotVisible() {
    // Validate farm animal housing table cells do not exist
    cy.get('[id="farmAnimalHousingDetailStandardRows"]').should("not.exist");
    cy.get('[id="farmAnimalHousingDetailStandardCode"]').should("not.exist");
    cy.get('[id="farmAnimalHousingDetailStandardNumberOfAnimals"]').should("not.exist");
    cy.get('[id="farmAnimalHousingDetailStandardEmissionFactor"]').should("not.exist");
    cy.get('[id="farmAnimalHousingDetailStandardReduction"]').should("not.exist");
    cy.get('[id="farmAnimalHousingDetailStandardEmission"]').should("not.exist");
    cy.get('[id="farmAnimalHousingDetailDescription"]').should("not.exist");
  }

  static copyAnimalHousingSystem() {
    cy.get('[id="sourceListButtonCopySource"]').click();
  }

  static fillInFieldsAnimalHousingSystemCustomSpec(dataTable: any) {
    dataTable.hashes().forEach((farmLodge: any) => {
      cy.get('[id="sourceListButtonNewSource"] > .icon').click();
      this.chooseAnimalHousingSystemOfType("Custom specification");
      cy.get('[id="emissionSourceFarmAnimalHousingCustomDescription"]').clear({ force: true }).type(farmLodge.animalType);
      cy.get('[id="animalType"]').select(farmLodge.animalType, { force: true });
      cy.get('[id="emissionFactorInput"]').type(farmLodge.factor, { force: true });
      cy.get('[id="numberOfAnimalsInput"]').type(farmLodge.numberOfAnimals, { force: true });
    });
  }

  static animalHousingCustomSpecDaysCreated(dataTable: any) {
    dataTable.hashes().forEach((farmLodge: any) => {
      if (farmLodge.days === "") {
        cy.get('[id="farmAnimalHousingDetailCustomNumberOfDays"]').should("not.exist");
      } else {
        cy.get('[id="farmAnimalHousingDetailCustomNumberOfDays"]').contains(farmLodge.days).should("have.text", farmLodge.days);
      }
      this.animalHousingValidateCreatedCustomSpec(farmLodge.description, farmLodge.numberOfAnimals, farmLodge.factor, farmLodge.emission);
    });
  }

  static animalHousingCustomDetailReduction(reduction: string) {
    if (reduction === "") {
      cy.get('[id="farmAnimalHousingCustomDetailReduction"]').should("not.exist");
    } else {
      cy.get('[id="farmAnimalHousingCustomDetailReduction"]').should("have.text", reduction);
    }
  }

  static animalHousingCustomSpecCreated(dataTable: any) {
    dataTable.hashes().forEach((farmLodge: any) => {
      this.animalHousingCustomDetailReduction(farmLodge.reduction);
      this.animalHousingValidateCreatedCustomSpec(farmLodge.description, farmLodge.numberOfAnimals, farmLodge.factor, farmLodge.emission);
    });
  }


  static animalHousingValidateCustomSpec(description: string, numberOfAnimals: string, factor: string, reduction: string, emission: string) {
    this.animalHousingCustomDetailReduction(reduction);
    this.animalHousingValidateCreatedCustomSpec(description, numberOfAnimals, factor,  emission);
  }

  static animalHousingValidateCreatedCustomSpec(description: string, numberOfAnimals: string, factor: string, emission: string) {
    cy.get('[id="farmAnimalHousingCustomDetailDescription"]').contains(description).should("have.text", description);
    cy.get('[id="farmAnimalHousingCustomDetailNumberOfAnimals"]').contains(numberOfAnimals).should("have.text", numberOfAnimals);
    cy.get('[id="farmAnimalHousingCustomDetailEmissionFactor"]').contains(factor).should("have.text", factor);
    cy.get('[id="farmAnimalHousingCustomDetailEmission"]').contains(emission).should("have.text", emission);
  }

  static animalHousingSystemContainsFollowingIconAndData(dataTable: any) {
    let index = 0;
    dataTable.hashes().forEach((animalHousingSystem: any) => {
      cy.get('[id="subSourceListAmount"]').eq(index).contains(animalHousingSystem.amount).should("have.text", animalHousingSystem.amount);
      cy.get('[id="subSourceListDescription"]').eq(index).contains(animalHousingSystem.description).should("have.text", animalHousingSystem.description);
      cy.get('[id="subSourceListEmission"]').eq(index).contains(animalHousingSystem.emission).should("have.text", animalHousingSystem.emission);
      if (animalHousingSystem.icon === "") {
        cy.get('[id="subSourceListIcon"]').eq(index).should("not.contain");
      } else {
          cy.get('[id="subSourceListIcon"]').eq(index).find("." + animalHousingSystem.icon);
        }
      index++;
    });
  }

  static chooseAnimalHousingSystemOfType(typeAnimalHousingSystem: string) {
    switch (typeAnimalHousingSystem) {
      case "Custom specification":
      case "Eigen specificatie":
        cy.get('[id="tab-farmAnimalHousingType-CUSTOM"]').click();
        break;
      case "Default emissions":
        cy.get('[id="tab-farmAnimalHousingType-STANDARD"]').click();
        break;
      default:
        throw new Error("Unknown Animal Housing type: " + typeAnimalHousingSystem);
    }
  }

  static fillInFieldsSpecificAnimalHousingSystemCustomSpec(description: string, animalType: string, factor: string, numberOfAnimals: string) {
    cy.get('[id="emissionSourceFarmAnimalHousingCustomDescription"]').clear({ force: true }).type("{selectAll}").type(description);
    cy.get('[id="animalType"]').select(animalType, { force: true });
    cy.get('[id="emissionFactorInput"]').type("{selectAll}", { force: true }).type(factor);
    cy.get('[id="numberOfAnimalsInput"]').type("{selectAll}", { force: true }).type(numberOfAnimals);
  }

  static specificAnimalHousingSystemCustomSpec(description: string, numberOfAnimals: string, factor: string, reduction: string, emission: string) {
    cy.get('[id="farmAnimalHousingCustomDetailDescription"]').should("have.text", description);
    cy.get('[id="farmAnimalHousingCustomDetailNumberOfAnimals"]').should("have.text", numberOfAnimals);
    cy.get('[id="farmAnimalHousingCustomDetailEmissionFactor"]').should("have.text", factor);
    this.animalHousingCustomDetailReduction(reduction);
    cy.get('[id="farmAnimalHousingCustomDetailEmission"]').should("have.text", emission);
  }

  static validateSpecificAnimalHousingSystemCustomSpecDays(description: string, numberOfAnimals: string, factor: string, days: string, emission: string) {
    cy.get('[id="farmAnimalHousingCustomDetailDescription"]').should("have.text", description);
    cy.get('[id="farmAnimalHousingCustomDetailNumberOfAnimals"]').should("have.text", numberOfAnimals);
    cy.get('[id="farmAnimalHousingCustomDetailEmissionFactor"]').should("have.text", factor);
    if (days === "") {
      cy.get('[id="farmAnimalHousingDetailStandardNumberOfDays"]').should("not.exist");
    } else {
      cy.get('[id="farmAnimalHousingDetailStandardNumberOfDays"]').should("have.text", days);
    }
    cy.get('[id="farmAnimalHousingCustomDetailEmission"]').should("have.text", emission);
  }

  static validateSubsourceWithDescriptionNumberOfAnimalsEmission(description: string, numberOfAnimals: string, emission: string) {
    cy.get('[id="subSourceListDescription"]').contains(description).parent().get('[id="subSourceListEmission"]').should("have.text", emission);
    cy.get('[id="subSourceListDescription"]').contains(description).parent().get('[id="subSourceListAmount"]').should("have.text", numberOfAnimals);
  }

  static addAdditionalRule(typeOfAdditionalRule: string) {
    cy.get('[id="farmAnimalHousingAdditionalCodesItem"]').select(typeOfAdditionalRule);
  }

  static findEmissionReducingTechnique(housingCode: string) {
    cy.get('[id="reductiveSystemItem"]').type(housingCode);
  }

  static findFodderMeasure(pasCode: string) {
    cy.get('[id="fodderMeasureItem"]').type(pasCode);
  }

  static addAdditionalRuleWithCode(codeAdditionalRule: string) {
    cy.get('[id="farmAnimalHousingAdditionalCodesItem"]').type(codeAdditionalRule);
    cy.get('[id="' + codeAdditionalRule.replaceAll(" ", "-") + '"]')
    .should("contain", codeAdditionalRule)
    .click();
  }

  static errorMessageCombinationNotAllowed() {
    cy.get('[id="additionalIncompatibleError-0"]').should(
      "have.text",
      "Let op: de combinatie van deze systemen kan niet worden doorgerekend. "
    );
  }

  static deleteAdditionalRule() {
    cy.get('[id="delete"]').click({ force: true });
  }

  static addAnimalHousingSystem() {
    cy.get('[id="collapsibleSubSources"] > .line > .title').click();
    cy.get('[id="sourceListButtonNewSource"] > .icon').click();
  }

  static fillInFieldsFarmlandSource(category: string, NOX: string, NH3: string) {
    cy.get('[id="emissionSourceFarmlandCategory"]').select(category, {
      force: true
    });
    cy.get('[id="FarmlandemissionInput_NOX"]').clear({ force: true }).type(NOX);
    cy.get('[id="FarmlandemissionInput_NH3"]').clear({ force: true }).type(NH3);
  }

  static farmlandSourceCorrectlySaved(category: string, NOX: string, NH3: string) {
    const categoryId = this.getFarmlandCategoryId(category);

    cy.get('[id^="farmlandDetailActivity-' + categoryId + '-"]').contains(category);
    cy.get('[id^="farmlandDetailActivity-' + categoryId + '-"]').parent().find('[id="farmlandDetailEmissionNOX"]').should("have.text", NOX);
    cy.get('[id^="farmlandDetailActivity-' + categoryId + '-"]').parent().find('[id="farmlandDetailEmissionNH3"]').should("have.text", NH3);
  }

  static getFarmlandCategoryId(category: string) {
    switch (category) {
      case "Beweiding":
        return "PASTURE";
      case "Mestaanwending (dierlijke mest)":
        return "MANURE";
      case "Mestaanwending (kunstmest)":
        return "FERTILIZER";
      case "Organische processen":
        return "ORGANIC_PROCESSES";
      default:
        throw new Error("Unknown farmland category: " + category);
    }
  }

  static editAddTechniqueTo(additionalTechnique: string) {
    cy.get('[id="farmlandStandardCategoryCodeItem"]').clear().type(additionalTechnique);   
  }

  static fillInExplanation(explanation: string) {
    cy.get('[id^="farmAnimalHousingStandardAddExplanation-"]').clear().type(explanation);   
  }
}

