/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";

import { CommonExportPage } from "./CommonExportPage";

Given("I select {string} as export type", (exportType) => {
  CommonExportPage.selectExportType(exportType);
});

Given("I choose to export through the menu", () => {
  CommonExportPage.chooseExportThroughMenu();
});

Given("I see the option to export OPS input files", () => {
  CommonExportPage.assertExportOpsIsVisible();
});

Given("I choose to add extra information for {string}", (selectedExportOption) => {
  CommonExportPage.chooseToAddExtraInformation(selectedExportOption);
});

Given("I select the calculation job {string}", (calculationJob) => {
  CommonExportPage.selectCalculationJob(calculationJob);
});

Given("I select the calculation type {string}", (calculationType) => {
  CommonExportPage.selectCalculationType(calculationType);
});

Given("I select the calculation job {string} calculation type {string}", (calculationJob, calculationType) => {
  CommonExportPage.selectCalculationJob(calculationJob.substring(0, 128) + " " + calculationType);
});

Given("I fill in the following email address {string}", (emailAddress) => {
  CommonExportPage.fillInEmailAdress(emailAddress);
});

Given("I fill in the additional info form", (dataTable) => {
  CommonExportPage.fillInAdditionalInfoForm(dataTable);
});

Given("I select the acknowledge selection", () => {
  CommonExportPage.selectAcknowledgePDFExport();
});

Given("I click on the button to export", () => {
  CommonExportPage.clickOnExportButtonAndWaitForExportToStart();
});

Given("I click on the button to export and don't catch the call", () => {
  CommonExportPage.clickOnExportButton();
});

Given("I click on the button to export, expecting a warning", () => {
  CommonExportPage.clickOnExportButton();
});

Given("the button {string} is visible", (buttonType) => {
  CommonExportPage.assertButton(buttonType);
});

Given("I expect to see the {string}", (warningType) => {
  CommonExportPage.assertWarningIsShown(warningType);
});

Given("I accept the data inclusion warning", () => {
  CommonExportPage.acceptDataInclusionWarning();
});

Given("I select create new calculation job", () => {
  CommonExportPage.clickOnNewCalculationJobButton();
});

Given("I select additional info", () => {
  CommonExportPage.clickAdditionalInfo();
});

Given("I select additional info optional", () => {
  CommonExportPage.clickAdditionalInfoOptional();
});

Given("I wait till task is completed", () => {
  CommonExportPage.checkTaskStatusCompleteAndDownload();
});

Given("I can extract downloaded results and validate it contains {int} file", (fileCount) => {
  CommonExportPage.extractDownloadedResults(fileCount);
});

Given("I can extract downloaded results and validate it contains {int} files", (fileCount) => {
  CommonExportPage.extractDownloadedResults(fileCount);
});

Given("I select last downloaded results to be imported", () => {
  CommonExportPage.loadDownloadedResults();
});

Given("I validate results contain in combination results", (files) => {
  CommonExportPage.validateInCombinationResults(files);
});

Given("I cancel the export", () => {
  CommonExportPage.cancelExport();
});

Given("I validate if the following situations are checked", (dataTable) => {
  CommonExportPage.validateSituations(dataTable);
});

Given("I select the appendices", () => {
  CommonExportPage.clickAppendices();
});

Given("the appendices for edgehexagons is available", () => {
  CommonExportPage.validateIfAppendiceCheckBoxEdgeEffectExists(true);
});

Given("the appendices for edgehexagons is unavailable", () => {
  CommonExportPage.validateIfAppendiceCheckBoxEdgeEffectExists(false);
});

Given("the appendices for edgehexagons is checked", () => {
  CommonExportPage.validateIfAppendicePanelIsChecked(true);
});

Given("the appendices for edgehexagons is unchecked", () => {
  CommonExportPage.validateIfAppendicePanelIsChecked(false);
});

Given("I check the box for the edgehexagon appendice", () => {
  CommonExportPage.checkEdgeHexagonAppendice(true);
});

Given("I uncheck the box for the edgehexagon appendice", () => {
  CommonExportPage.checkEdgeHexagonAppendice(false);
});

Given("I validate that the tab {string} is selected", (tab) => {
  CommonExportPage.validateTabCalculationMenu(tab);
});

Given("I validate that calculation job {string} is selected", (calculationJob) => {
  CommonExportPage.validateCalculationJob(calculationJob);
});
