#
# Crown copyright
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Export
    Test the export panel and how exporting files is handled.

    Scenario: Assert additional info panel and its content is hidden
        Given I login with correct username and password
        When I select file 'UKAPAS_everything_import_file.gml' to be imported
        And I navigate to the 'Export' tab
        When I select 'Input files' as export type
        Then I don't see the additional info panel and its contents
        When I select 'Calculation job' as export type
        Then I don't see the additional info panel and its contents
        When I select 'Report' as export type
        Then I don't see the additional info panel and its contents

    @Smoke
    Scenario: Import file with all different ADMS sources and export GML
        Given I login with correct username and password
        When I select file 'UKAPAS_everything_import_file.gml' to be imported
        And I navigate to the 'Export' tab
        Then I select 'Input files' as export type
        And I fill in the following email address 'test@aerius.nl'
        And I click on the button to export
        Then the following notification is displayed 'Export has started successfully. You will receive a download link by email when your files are available'
        Then I cancel the export
        Then the following notification is displayed "Your export '1 scenarios' has been cancelled."

    @Smoke
    Scenario: Import file with all different ADMS sources and export calculation GML
        Given I login with correct username and password
        When I select file 'UKAPAS_everything_import_file.gml' to be imported

        # Navigate to the calculation jobs tab first to generate a calculation job
        And I navigate to the 'Calculation jobs' tab
        Then I create a new calculation job
        And I select country 'England'        
        And I select project category 'Agriculture'
        And I select development pressure sources
            | source   |
            | Source 1 |
            | Source 3 |
        And I select the uk meteorological site location selection method 'NWP'
        And I select the uk meteorological site location selection method 'Search'
        And I select the first meteorological site location
        And I select the meteorological year '2021'
        And I save the calculation job
        # accept the warnings
        And I save the calculation job

        And I navigate to the 'Export' tab
        Then I select 'Calculation job' as export type
        And I select the calculation job 'Calculation job 1 (Process contribution)'
        And I fill in the following email address 'test@aerius.nl'
        And I click on the button to export
        Then the following notification is displayed 'Export has started successfully. You will receive a download link by email when your files are available'
        Then I cancel the export
        Then the following notification is displayed "Your export 'Calculation job 1' has been cancelled."

    Scenario: Export PDF
        Given I login with correct username and password
        When I select file 'UKAPAS_everything_import_file.gml' to be imported
        
        # Navigate to the calculation jobs tab first to generate a calculation job
        And I navigate to the 'Calculation jobs' tab
        Then I create a new calculation job
        And I select the uk calculation method 'Formal assessment'
        And I select country 'Wales'        
        And I select project category 'Agriculture'
        And I select development pressure sources
            | source   |
            | Source 1 |
            | Source 3 |
        And I select the uk meteorological site location selection method 'NWP'
        And I select the uk meteorological site location selection method 'Search'
        And I select the first meteorological site location
        And I select the meteorological year '2021'
        And I save the calculation job
        # accept the warnings
        And I save the calculation job

        And I navigate to the 'Export' tab
        Then I select 'Report' as export type
        And I select the calculation type 'Process contribution'
        And I select the calculation job 'Calculation job 1 (Process contribution)'
        And I fill in the following email address 'test@aerius.nl'
        And I click on the button to export
        Then the following notification is displayed 'Export has started successfully. You will receive a download link by email when your files are available'
        Then I cancel the export
        Then the following notification is displayed "Your export 'report of Calculation job 1' has been cancelled."

    Scenario: Import file with assessment points export calculation GML and download file
        Given I login with correct username and password
        When I select file 'UKAPAS_assessment_points_file.gml' to be imported
        And I navigate to the 'Calculation jobs' tab
        And I edit the selected calculation job
        And I select country 'England'
        And I select project category 'Agriculture'
        And I select development pressure sources
            | source   |
            | Source 1 |
        And I select the uk meteorological site location selection method 'NWP'
        And I select the uk meteorological site location selection method 'Search'
        And I select the first meteorological site location
        And I select the meteorological year '2021'
        And I save the calculation job

        And I navigate to the 'Export' tab
        Then I select 'Calculation job' as export type
        And I select the calculation job 'Calculation job 1 - UKAPAS_assessment_points_file (Process contribution)'
        And I fill in the following email address 'test@aerius.nl'
        And I click on the button to export
        Then the following notification is displayed 'Export has started successfully. You will receive a download link by email when your files are available'
        And I wait till task is completed
        And I can extract downloaded results and validate it contains 1 file

    @Smoke
    Scenario: Import file with in combination process contribution results export calculation GML and download file
        Given I login with correct username and password
        When I select file 'UKAPAS_in_combination_results.gml' to be imported
        Then the situation name is 'Scenario 1'
        And the situation type is 'Project'
        And the calculation year is '2025'

        And I navigate to the 'Calculation jobs' tab
        And I edit the selected calculation job
        And I select country 'Northern Ireland'
        And I select project category 'Combustion plant'
        And I select In-Combination include project from archive
        And I select development pressure sources
            | source   |
            | Source 1 |
        And I select the uk meteorological site location selection method 'NWP'
        And I select the uk meteorological site location selection method 'Search'
        And I select the first meteorological site location
        And I select the meteorological year '2022'
        And I save the calculation job

        And I navigate to the 'Export' tab
        And I select 'Calculation job' as export type
        And I select the calculation job 'Calculation job 1 - UKAPAS_in_combination_results' calculation type '(In-combination process contribution)'
        And I fill in the following email address 'test@aerius.nl'
        When I click on the button to export
        Then the following notification is displayed 'Export has started successfully. You will receive a download link by email when your files are available'
        And I wait till task is completed
        And I can extract downloaded results and validate it contains 2 files
        Then I validate results contain in combination results
        | fileName       | type     |
        | Scenario1.gml  | scenario |
        | ARCHIVE.gml    | archive  |

    # will be fixed with https://aerius.atlassian.net/browse/AER-3386
    Scenario: Import file with incorrect building height versus emission height and export calculation job
        Given I login with correct username and password
        When I select file 'UKAPAS_buildingheight_emissionheight_error.gml' to be imported

        # Navigate to the calculation jobs tab first to generate a calculation job
        And I navigate to the 'Calculation jobs' tab
        Then I create a new calculation job
        And I select country 'Scotland'        
        And I select project category 'Agriculture'
        And I select development pressure sources
            | source   |
            | Source 1 |
        And I select the first meteorological site location
        And I select the meteorological year '2021'
        And I save the calculation job
        And I navigate to the 'Export' tab
        Then I select 'Calculation job' as export type        
        And I fill in the following email address 'test@aerius.nl'
        And I click on the button to export
        Then the following notification is displayed 'Export has started successfully. You will receive a download link by email when your files are available'
        Then the following notification is displayed 'An error occurred during the export error message : "The ADMS program returned an unknown error while calculating: ERROR : Source Source 1 is inside building Building.1". Your export has been aborted.'
    