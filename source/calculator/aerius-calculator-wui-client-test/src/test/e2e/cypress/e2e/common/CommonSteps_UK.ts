/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";
import { CommonPage } from "./CommonPage";
import { CommonPage_UK } from "./CommonPage_UK";
import { RoadTransportationSourceRoadTrafficPage } from "../cucumber-tests/RoadTransportationSourceRoadTraffic/RoadTransportationSourceRoadTrafficPage";
import { ADMSSourceCharacteristicsPage } from "./ADMSSourceCharacteristicsPage";
import { CommonAgricultureSourcePage } from "./CommonAgricultureSourcePage";

Given("I login with correct username and password with ADMS enabled", () => {
  CommonPage_UK.visitAndLoginWithAdms();
});

Given("I create a simple uk emission source from the start page", () => {
  CommonPage.visitAndLogin();
  CommonPage.createNewSourceFromTheStartPage();
  CommonPage.createNewSourceFromTheEmissionSourcePage();
  CommonPage.selectSectorGroupAndSector("Source 1", "Energy", "Large combustion plant");
  CommonPage.fillLocationWithCoordinates("POINT(322447.2 594884.3)");
  ADMSSourceCharacteristicsPage.chooseSourceCharacteristics("25", "0.5");
  CommonPage.fillEmissionFields("5", "0");
  CommonPage.saveSource();
});

Given("I open panel {string}", (panelName) => {
  CommonPage_UK.openPanel(panelName);
});

Given("I choose area {string}", (area) => {
  CommonPage_UK.chooseArea(area);
});

Given("I choose road type {string} direction {string}", (roadType, direction) => {
  RoadTransportationSourceRoadTrafficPage.chooseRoadTypeDirection(roadType, direction);
});

Given("I add an emission source of type {string}", (newSourceOfType) => {
  CommonPage_UK.addNewSourceOfType(newSourceOfType);
});

Given("I fill road width with value {string}, road elevation with {string} and gradient with {string}", (width, elevation, gradient) => {
  CommonPage_UK.fillRoadWidthElevationGradient(width, elevation, gradient);
});

Given("I fill in maximum speed {string}", (maxSpeed) => {
  CommonPage_UK.fillInMaxSpeed(maxSpeed);
});

Given("I select time unit {string}", (timeUnit) => {
  CommonPage_UK.selectTimeUnit(timeUnit);
});

Given("I fill in number {string} for vehicle {string}", (numberOfVehicle, vehicleType) => {
  CommonPage_UK.fillInNumberForVehicle(numberOfVehicle, vehicleType);
});

Given("area with {string} is correctly saved", (savedArea) => {
  CommonPage_UK.areaCorrectlySaved(savedArea);
});

Given("road type {string} and traffic direction {string} are correctly saved", (roadType, direction) => {
  CommonPage_UK.roadTypeDirectionCorrectlySaved(roadType, direction);
});

Given("road width {string}, elevation {string} and gradient {string} are correctly saved", (width, elevation, gradient) => {
  CommonPage_UK.roadWidthElevationGradientCorrectlySaved(width, elevation, gradient);
});

Given("barrier type left {string} and right {string} are correctly saved", (barrierTypeLeft, barrierTypeRight) => {
  CommonPage_UK.barrierTypeLeftRight(barrierTypeLeft, barrierTypeRight);
});

Given("number of vehicles {string} for vehicletype {string} is correctly saved", (numberOfVehicle, typeOfVehicle) => {
  CommonPage_UK.numberOfVehicleForType(numberOfVehicle, typeOfVehicle);
});

Given("maximum speed {string} is correctly saved", (speed) => {
  CommonPage_UK.speedCorrectlySaved(speed);
});

Given("I fill description with {string}", (description) => {
  RoadTransportationSourceRoadTrafficPage.fillDescription(description);
});

Given("I choose time unit {string} with number of vehicles {string}", (timeUnit, numberOfVehicles) => {
  RoadTransportationSourceRoadTrafficPage.chooseTimeUnitAndNumberOfVehicles(timeUnit, numberOfVehicles);
});

Given("I fill emission {string} with value {string}", (emission, value) => {
  CommonPage_UK.fillEmissionWithValue(emission, value);
});

Given("I fill road width with value {string}", (value) => {
  CommonPage_UK.fillRoadWidthWithValue(value);
});

Given("description {string} is correctly saved", (description) => {
  RoadTransportationSourceRoadTrafficPage.descriptionCorrectlySaved(description);
});

Given("time unit {string} with number of vehicles {string} is correctly saved", (timeUnit, numberOfVehicles) => {
  RoadTransportationSourceRoadTrafficPage.timeUnitAndNumberOfVehiclesIsCorrectlySaved(timeUnit, numberOfVehicles);
});

Given("emission {string} with value {string} is correctly saved", (emission, value) => {
  CommonPage_UK.emissionWithValueCorrectlySaved(emission, value);
});

Given("I choose to edit the source", () => {
  CommonPage.editSource();
});

Given("I copy the source {string}", (sourceName) => {
  CommonPage.copySource(sourceName);
});

Given("I delete source {string}", (sourceName) => {
  CommonPage.deleteSelectedSource(sourceName);
});

Given("the next source does not exist {string}", (sourceName) => {
  CommonPage.sourcesDoesNotExists(sourceName);
});

Given("the Emission Sources is empty", () => {
  CommonPage.sourcesIsEmpty();
});

Given("I select {string} barrier type {string}", (leftOrRight, barrierType) => {
  CommonPage_UK.selectLeftOrRightBarrierType(leftOrRight, barrierType);
});

Given("I fill {string} width with {string}", (leftOrRight, width) => {
  CommonPage_UK.fillWidthWith(leftOrRight, width);
});

Given("I fill {string} maximum height with {string}", (leftOrRight, maxHeight) => {
  CommonPage_UK.fillMaxHeightWith(leftOrRight, maxHeight);
});

Given("I fill {string} average height with {string}", (leftOrRight, averageHeight) => {
  CommonPage_UK.fillAverageHeightWith(leftOrRight, averageHeight);
});

Given("I fill {string} minimum height with {string}", (leftOrRight, minimumHeight) => {
  CommonPage_UK.fillMinimumHeightWith(leftOrRight, minimumHeight);
});

Given("I fill {string} porosity with {string}", (leftOrRight, porosity) => {
  CommonPage_UK.fillPorosityWith(leftOrRight, porosity);
});

Given("I fill coverage with {string}", (coverage) => {
  CommonPage_UK.fillCoverageWith(coverage);
});

Given("{string} barrier type {string} is correctly saved", (leftOrRight, barrierType) => {
  CommonPage_UK.barrierTypeIsCorrectlySaved(leftOrRight, barrierType);
});

Given("{string} width {string} is correctly saved", (leftOrRight, width) => {
  CommonPage_UK.widthIsCorrectlySaved(leftOrRight, width);
});

Given("{string} maximum height {string} is correctly saved", (leftOrRight, maxHeight) => {
  CommonPage_UK.maxHeightIsCorrectlySaved(leftOrRight, maxHeight);
});

Given("{string} average height {string} is correctly saved", (leftOrRight, averageHeight) => {
  CommonPage_UK.averageHeightIsCorrectlySaved(leftOrRight, averageHeight);
});

Given("{string} minimum height {string} is correctly saved", (leftOrRight, minimumHeight) => {
  CommonPage_UK.minimumHeightIsCorrectlySaved(leftOrRight, minimumHeight);
});

Given("{string} porosity {string} is correctly saved", (leftOrRight, porosity) => {
  CommonPage_UK.porosityIsCorrectlySaved(leftOrRight, porosity);
});

Given("coverage {string} is correctly saved", (coverage) => {
  CommonPage_UK.coverageIsCorrectlySaved(coverage);
});

Given("tunnel {string} is correctly saved", (tunnelFactor) => {
  CommonPage_UK.tunnelIsCorrectlySaved(tunnelFactor);
});

Given("the following {string} warning is visible {string}", (warningType, warningText) => {
  CommonPage_UK.followingWarningIsVisible(warningType, warningText);
});

Given("the following {string} error is visible {string}", (errorType, errorText) => {
  CommonPage_UK.followingErrorIsVisible(errorType, errorText, true);
});

Given("the following {string} error {string} is not visible", (errorType, errorText) => {
  CommonPage_UK.followingErrorIsVisible(errorType, errorText, false);
});

Given("the {string} warning is not visible", (warningType) => {
  CommonPage_UK.followingWarningIsNotVisible(warningType);
});

Given("I choose time-varying variation {string}", (timeVaryingProfileType) => {
  CommonPage_UK.chooseTimeVaryingProfile(timeVaryingProfileType);
});

Given("I select time-varying profile {string}", (timeVaryingProfile) => {
  CommonPage_UK.selectTimeVaryingProfile(timeVaryingProfile);
});

Given("I select custom time-varying profile {string}", (customTimeVaryingProfile) => {
  CommonPage_UK.selectCustomTimeVaryingProfile(customTimeVaryingProfile);
});

Given("time-varying profile with name {string} is correctly saved", (timeVaryingProfileName) => {
  CommonPage_UK.timeVaryingProfileCorrectlySaved(timeVaryingProfileName);
});

Given("I choose to cancel {string}", (debugId) => {
  CommonPage.chooseToCancel(debugId);
});

Given("I select predefined profile {string}", (selectedProfile) => {
  CommonPage_UK.selectPredefinedProfile(selectedProfile);
});

Given("The specified listbox should or should not contain the following values", (dataTable) => {
  CommonPage_UK.checkListboxValues(dataTable);
});

Given("The {string} listbox contains {string} items", (specificListbox, optionAmount) => {
  CommonPage_UK.checkListboxAmount(specificListbox, optionAmount);
});

Given(
  "the custom specification with description {string}, number of animals {string}, factor {string}, days {string} and emission {string} is correctly created",
  (description, numberOfAnimals, factor, days, emission) => {
    CommonAgricultureSourcePage.validateSpecificAnimalHousingSystemCustomSpecDays(description, numberOfAnimals, factor, days, emission);
  }
);

Given("I select the uk calculation method {string}", (method) => {
  CommonPage_UK.selectCalculationMethod(method);
});

Given("I select the uk calculation method {string} and validate hint message is {string}", (method, isHintVisible) => {
  CommonPage_UK.selectCalculationMethod(method);
  CommonPage_UK.calculationMethodHintVisible(isHintVisible);
});

Given("I select for {string} profile the option {string}", (profileType, profileOption) => {
  CommonPage_UK.selectOptionForTimeVaryingProfile(profileType, profileOption);
});

Given("the details of the profile are correctly saved", (dataTable) => {
  CommonPage_UK.timeVaryingProfileIsCorrectlySaved(dataTable);
});

Given("I name the scenario {string}", (scenarioName) => {
  CommonPage_UK.nameScenario(scenarioName);
});

Given("I select country {string}", (country) => {
  CommonPage_UK.selectCountry(country);
});

Given("I select the uk meteorological site location selection method {string}", (locationMethod) => {
  CommonPage_UK.selectMetLocationSelectionMethod(locationMethod);
});

Given("I mark export ADMS input files", () => {
  CommonPage_UK.checkMarkExportADMS();
});

Given("I select meteorological data type {string}", (metDataType) => {
  CommonPage_UK.selectMetDataType(metDataType);
});

Given("I edit the {string} to be {string}", (field, value) => {
  ADMSSourceCharacteristicsPage.fillInCharacteristic(field, value);
});

Given("I select project category {string}", (projectCategory) => {
  CommonPage_UK.selectCalculationProjectCategory(projectCategory);
});

Given("I select development pressure sources", (dataTable) => {
  CommonPage_UK.selectDevelopmentPressure(dataTable);
});
Given("I select zone of influence {string}", (calculationDistance) => {
  CommonPage_UK.setZoneOfInfluence(calculationDistance);
});

Given("I select min. Monin-Obukhov length {string}", (minMoninObukhovLength) => {
  CommonPage_UK.selectMinMoninObukhovLength(minMoninObukhovLength);
});

Given("I select surface albedo {string}", (surfaceAlbedo) => {
  CommonPage_UK.selectSurfaceAlbedo(surfaceAlbedo);
});

Given("I select Priestley-Taylor parameter {string}", (priestleyTaylorParameter) => {
  CommonPage_UK.selectPriestleyTaylorParameter(priestleyTaylorParameter);
});

Given("the Development pressure search panel is {string}", (visibilityDPSpanel) => {
  CommonPage_UK.visibilityDPSpanel(visibilityDPSpanel);
});

// Preferences
Given("I switch the WKT conversion to {string}", (wktConversion: string) => {
  CommonPage_UK.switchWktConversion(wktConversion);
});

Given("I expect the selected sources for development pressure search to be {string}", (selectedSources) => {
  CommonPage_UK.assertSelectedSources(selectedSources);
});

Given("I expect the following Meteorological settings", (dataTable) => {
  CommonPage_UK.assertMetSettings(dataTable);
});

Given("I expect the following Advanced settings", (dataTable) => {
  CommonPage_UK.assertAdvancedSettings(dataTable);
});

Given("I expect the following Road emissions options", (dataTable) => {
  CommonPage_UK.assertRoadEmissionsOptions(dataTable);
});
