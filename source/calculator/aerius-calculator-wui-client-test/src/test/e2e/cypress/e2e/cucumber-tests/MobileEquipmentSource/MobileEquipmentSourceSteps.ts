/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given, Then } from "cypress-cucumber-preprocessor/steps";
import { MobileEquipmentSourcePage } from "./MobileEquipmentSourcePage";
import { CommonPage } from "../../common/CommonPage";

Given(
  "I fill the mobile equipment description {string} with stage class {string}, fuel usage {string}, operation hours per year {string} and adblue usage {string}",
  (description, stageClass, fuelUsage, operationHours, adBlue) => {
    MobileEquipmentSourcePage.inputDescriptionStageClassFuelUsageOperationHoursAdBlue(
      description,
      stageClass,
      fuelUsage,
      operationHours,
      adBlue
    );
  }
);

Given(
  "I edit the mobile equipment description {string} with stage class {string}, fuel usage {string}, operation hours per year {string} and adblue usage {string}",
  (description, stageClass, fuelUsage, operationHours, adBlue) => {
    MobileEquipmentSourcePage.editInputDescriptionStageClassFuelUsageOperationHoursAdBlue(
      description,
      stageClass,
      fuelUsage,
      operationHours,
      adBlue
    );
  }
);

Then(
  "the following mobile equipment source is correctly saved", (dataTable) => {
    MobileEquipmentSourcePage.descriptionStageClassFuelUsageOperationHoursAdBlueCreated(dataTable);
  }
);

Then("blue ratio warning is visible", () => {
  MobileEquipmentSourcePage.blueRatioWarningisVisible();
});

Then("blue ratio warning is not visible", () => {
  MobileEquipmentSourcePage.blueRatioWarningisNotVisible();
});

Given("I choose to add the new stage class {string}", (stageClass) => {
  MobileEquipmentSourcePage.chooseToAddNewStageClass(stageClass);
});

Given("field {string} is visible", (fieldName) => {
  MobileEquipmentSourcePage.fieldVisible(fieldName);
});

Given("field {string} is not visible", (fieldName) => {
  MobileEquipmentSourcePage.fieldNotVisible(fieldName);
});

Given("I select stage class {string}", (stageClass) => {
  MobileEquipmentSourcePage.selectStageClass(stageClass);
});

Given("I choose to edit the source", () => {
  CommonPage.editSource();
});
