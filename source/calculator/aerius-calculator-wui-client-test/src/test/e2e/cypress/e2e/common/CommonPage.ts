/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { CommonSettings } from "./CommonSettings";

export class CommonPage {
  static assertActualResult: string = "";

  static visitAndLogin(url: any = Cypress.config("baseUrl")) {
    cy.clearAllCookies();
    cy.viewport(1920, 1080);
    const username = Cypress.env("USERNAME");
    const password = Cypress.env("PASSWORD");
    const acceptAllCookies = Cypress.env("ACCEPT_ALL_COOKIES");
    const skipCookiesCheck = Cypress.env("SKIP_COOKIES_CHECK");

    if (username && password) {
      // test site for redirect
      cy.request({
        url: url,
        followRedirect: false,
        failOnStatusCode: false
      }).then((response: any) => {
        cy.log(response.status);
        if (response.status == 302) {
          cy.log("got a 302 redirect to coginito with redirect url");
          const redirectUrl = response.redirectedToUrl;

          if (skipCookiesCheck) {
            /* block the call to cookie controle the call resets the AWS cognito cookie */
            cy.intercept("https://apikeys.civiccomputing.com/**", (request: any) => {
              request.destroy();
            });
            cy.intercept("https://apikeys.civiccomputing.com/**", { log: false});
          }
          /* block the call to google-analytics */
          cy.intercept("https://region1.google-analytics.com/**", (request: any) => {
            request.destroy();
          });
          cy.intercept("https://region1.google-analytics.com/**", { log: false});
          
          cy.intercept("/application/v2/service/context").as("getContext");

          cy.visit(redirectUrl);
          cy.get("body").then((body) => {
            if (body.find("input[name=username]:visible").length > 0) {
              cy.get("input[name=username]:visible").type(username);
              cy.get("input[name=password]:visible").type(password);
              cy.get("input[name=signInSubmitButton]:visible").click();
            } else {
              cy.log("cognito user credentials still in cache");
            }
          });
          cy.wait("@getContext");
        } else {
          cy.intercept("/application/v2/service/context").as("getContext");
          cy.visit(url, {
            auth: {
              username: username,
              password: password
            }
          });
          cy.wait("@getContext");
        }
      });
    } else {
      cy.intercept("/application/v2/service/context").as("getContext");
      cy.visit(url);
      cy.wait("@getContext");
    }
    if (acceptAllCookies == "true") {
      cy.get('[id="ccc-notify-accept"]', { timeout: CommonSettings.getElementLongTimeOut }).if().click();
    }
  }

  static changeViewport(width: number, height: number) {
    cy.viewport(width, height);
  }

  static visitAndLoginWithOps() {
    this.visitAndLogin("&ops=true");
  }

  static visitAndLoginWithlocaleEn() {
    this.visitAndLogin(Cypress.config("baseUrl")?.replace("locale=nl", "locale=en"));
  }

  static visitAndLoginWithProduct(productCode: string) {
    var url;
    if (productCode == "CHECK") {
      url = Cypress.env("CHECK_URL_UI");
    } else {
      throw new Error("Unknown Product specified: " + productCode);
    }
    this.visitAndLogin(url);
  }

  static navigateTo(buttonName: string, confirm = true) {
    if (!confirm) {
      cy.on("window:confirm", () => false);
    }

    switch (buttonName) {
      case "Home":
        cy.get('[id="NAV_ICON-MENU-HOME"]').click();
        break;
      case "Input":
        cy.get('a[href$="/inputs/"]').click();
        break;
      case "Assessment points":
        cy.get('a[href*="/calculation-points/"]').click();
        break;
      case "Calculation jobs":
        cy.get('[id="NAV_ICON-MENU-CALCULATE"]').click();
        break;
      case "Results":
        cy.get('[id="NAV_ICON-MENU-RESULTS"]').click();
        break;
      case "Export":
        cy.get('a[href*="/export/"]').click();
        break;
      case "Language":
        cy.get('[id="NAV_ICON-MENU-LANGUAGE"]').click();
        break;
      case "Preferences":
        cy.get('[id="NAV_ICON-MENU-SETTINGS"]').click();
        break;
      case "Next steps":
        cy.get('a[href*="/next-steps/"]').click();
        break;
      default:
        throw new Error("Unknown navigation button: " + buttonName);
    }
  }

  static openCollapsiblePanel(panelName: string) {
    switch (panelName) {
      case "Locatie":
      case "Location":
        cy.get('[id="collapsibleLocation"]').click();
        break;
      case "Bronkenmerken":
        // Find by name
        cy.contains(panelName)
          .parent()
          .parent()
          .then(($object: any) => {
            const classList = Array.from($object[0].classList);
            // Only open if it is not open
            if (!classList.includes("open")) {
              cy.contains(panelName).click();
            }
          });
        break;
      case "Source characteristics":
        cy.get('[id="collapsibleADMSCharacteristics"]').click();
        break;
      case "Emissies":
      case "Emissions":
        cy.get('[id="collapsibleSubSources"]').click();
        break;
      default:
        throw new Error("Unknown panel: " + panelName);
    }
  }

  static createNewSourceFromTheStartPage() {
    cy.get('[id="calculationLaunchButtonNewSituation"]').click({ force: true });
  }

  static createNewSourceFromTheEmissionSourcePage() {
    cy.get('[id="calculationLaunchButtonNewsource"]').click({ force: true });
  }

  static createNewSourceFromTheSourcePage() {
    cy.get('[id="sourceListButtonNewSource"]').click();
  }

  static createNewSourceFromTheScenarioInputPage() {
    cy.get('[id="calculationLaunchButtonNewsource"]').click({ force: true });
  }

  static nameTheSource(sourceName: string) {
    cy.get('[id="label"]').clear().type(sourceName);
  }

  static validateEmissionSourcesPanelIsClosed() {
    cy.get('[id="collapsible-panel-EMISSION_SOURCES"]').should("not.have.class", "open");
  }

  static validateLocationPanelIsOpen() {
    cy.get('[id="collapsibleLocation"]').should("have.class", "open");
  }

  static validateSourceCharacteristicsPanelIsNotVisible() {
    cy.get('[id="collapsibleOPSCharacteristics-title"]').should("not.visible");
  }

  static validateBuildingsPanelIsClosed() {
    cy.get('[id="collapsible-panel-BUILDING"]').should("not.have.class", "open");
  }

  static openEmissionSources() {
    cy.get('[id="collapsible-panel-EMISSION_SOURCES"]').click();
    cy.get('[id="collapsible-panel-EMISSION_SOURCES"]').should("have.class", "open");
  }

  // Fill in fields for source with only a sector group:'Energy'/'Other'
  static fillInFieldsForSectorGroup(sectorGroup: string) {
    // Sector
    cy.get('[id="label"]').clear().type("Bron 1");
    // Specific sector group
    cy.get('[id="sourceListDetailSectorGroup"]').select(sectorGroup);
    // Location
    cy.get('[id="emissionSourceGeometryInput"]').type("POINT(172203.2 599120.32)");
    // Source characteristics
    cy.get('[id="collapsibleOPSCharacteristics"]').click();
    cy.get('[id="heatContentType"]').select("Niet geforceerd");
    cy.get('[id="emissionHeight"]').type("15");
    cy.get('[id="emissionHeatContent"]').type("20");
    // Emissions
    cy.get('[id="collapsibleSubSources"]').click();
    cy.get('[id="emissionRow_NOX"]').type("200");
    cy.get('[id="emissionRow_NH3"]').type("300");
  }

  static selectSectorGroup(sourceName: string, sectorGroup: string) {
    // Name of source
    cy.get('[id="label"]').clear().type(sourceName);
    // Specific sector group
    cy.get('[id="sourceListDetailSectorGroup"]').select(sectorGroup);
  }

  static selectSectorGroupAndSector(sourceName: string, sectorGroup: string, sector: string) {
    // Name of source
    cy.get('[id="label"]').clear({ force: true }).type(sourceName);
    // Specific sector group
    cy.get('[id="sourceListDetailSectorGroup"]').select(sectorGroup);
    // Specific sector
    cy.get('[id="sourceListDetailSector"]').select(sector);
  }

  // Validate fields for source with name and  sector group
  static sourceNameWithSectorGroupCorrectlySaved(sourceName: string, sectorGroup: string) {
    // Validate name of source
    cy.get('[id="sourceListDetailTitle"]').should("have.text", sourceName);
    // Validate specific sector group
    cy.get('[id="sourceListDetailSectorGroup"]').should("have.text", sectorGroup);
  }

  // Validate fields for source with name, sector group and a sector
  static sourceNameWithSectorGroupAndSectorCorrectlySaved(sourceName: string, sectorGroup: string, sector: string) {
    // Validate name of source
    cy.get('[id="sourceListDetailTitle"]').should("have.text", sourceName);
    // Validate specific sector group
    cy.get('[id="sourceListDetailSectorGroup"]').should("have.text", sectorGroup);
    // Validate specific sector
    cy.get('[id="sourceListDetailSector"]').should("have.text", sector);
  }

  static selectSector(sector: string) {
    cy.get('[id="sourceListDetailSector"]').select(sector);
  }

  static fillLocationWithCoordinates(locationCoordinates: string) {
    cy.get('[id="emissionSourceGeometryInput"]').clear().type(locationCoordinates);
  }

  static changeLocationToCoordinates(locationCoordinates: string) {
    cy.get('[id="collapsibleLocation"]').click();
    cy.get('[id="emissionSourceGeometryInput"]').clear().type(locationCoordinates);
  }

  static locationWithCoordinatesCorrectlySaved(locationCoordinates: string) {
    cy.get('[id="sourceListDetailLocation"]').should("have.text", locationCoordinates);
  }

  static lengthOfLocationCorrectlySaved(lengthOfLocation: string) {
    cy.get('[id="sourceListDetailLocationStatistic"]').should("contain", lengthOfLocation);
  }

  static valueInLocationPanel(valueLocationPanel: string) {
    cy.get('[id="emissionSourceGeometryStatistic"]').should("contain", valueLocationPanel);
  }

  static surfaceOfLocationCorrectlySaved(surfaceOfLocation: string) {
    cy.get('[id="sourceListDetailLocationStatistic"]').should("contain", surfaceOfLocation);
  }

  static chooseSourceCharacteristicsAnimalHousingEmission(
    establishmentDate: string,
    heatContentType: string,
    emissionHeight: string,
    heatContent: string
  ) {
    // Source characteristics
    cy.get('[id="collapsibleOPSCharacteristics"]').click();
    cy.get('[id="established"]').type(establishmentDate);
    cy.get('[id="heatContentType"]').select(heatContentType);
    cy.get('[id="emissionHeight"]').type("{selectAll}").type(emissionHeight);
    cy.get('[id="emissionHeatContent"]').type("{selectAll}").type(heatContent);
  }

  // TODO: Create ADMS version
  static chooseSourceCharacteristics(heatContentType: string, emissionHeight: string, heatContent: string) {
    // Source characteristics
    cy.get('[id="collapsibleOPSCharacteristics"]').click();
    cy.get('[id="heatContentType"]').select(heatContentType);
    cy.get('[id="emissionHeight"]').type("{selectAll}").type(emissionHeight);
    cy.get('[id="emissionHeatContent"]').type("{selectAll}").type(heatContent);
  }

  static chooseSourceCharacteristicsWithBuilding(emissionHeight: string, heatContent: string) {
    // Source characteristics
    cy.get('[id="emissionHeight"]').type("{selectAll}").type(emissionHeight);
    cy.get('[id="emissionHeatContent"]').type("{selectAll}").type(heatContent);
  }

  // UK Specific
  static chooseefluxTypeUK(effluxType: string) {
    cy.get('[id="effluxType"]').scrollIntoView();
    cy.get('[id="effluxType"]').contains(effluxType).click({ force: true });
  }

  // UK Specific
  static chooseSpecifyTemperatureDensityUK(temperatureType: string) {
    cy.get('[id="buoyancyType"]').scrollIntoView();
    cy.get('[id="buoyancyType"]').contains(temperatureType).click({ force: true });
  }

  // UK Specific
  static existIdUK(id: string, state: string) {
    cy.get('[id="' + id + '"]').scrollIntoView();
    cy.get('[id="' + id + '"]').should(state);
  }

  static chooseSourceCharacteristicsWithDiurnalVariationType(
    heatContentType: string,
    emissionHeight: string,
    heatContent: string,
    diurnalVariationType: string
  ) {
    // Source characteristics
    cy.get('[id="collapsibleOPSCharacteristics"]').click();
    cy.get('[id="heatContentType"]').select(heatContentType);
    cy.get('[id="emissionHeight"]').type("{selectAll}").type(emissionHeight);
    cy.get('[id="emissionHeatContent"]').type("{selectAll}").type(heatContent);
    cy.get('[id="diurnalVariationType"]').type("{selectAll}").select(diurnalVariationType);
  }

  static sourceCharacteristicsCorrectlySaved(
    heatContentType: string,
    emissionHeight: string,
    heatContent: string,
    diurnalVariationType: string
  ) {
    cy.get('[id="sourcelistDetailVentilation"]').should("have.text", heatContentType);
    cy.get('[id="sourcelistDetailBuilding"]').should("have.text", "Geen");
    this.assertMinMaxLabel("sourcelistDetailEmissionHeight", emissionHeight + " m");
    cy.get('[id="sourcelistDetailHeatContent"]').should("include.text", heatContent + " MW");
    cy.get('[id="sourcelistDetailDiurnalVariation"]').should("have.text", diurnalVariationType);

    this.assertInfluencingBuildingIs("Geen");
  }

  static chooseSourceCharacteristicsWithOutflow(
    heatContentType: string,
    emissionHeight: string,
    emissionTemperature: string,
    outflowDiameter: string,
    outflowDirectionType: string,
    outflowVelocity: string
  ) {
    // Source characteristics
    cy.get('[id="collapsibleOPSCharacteristics"]').click();
    cy.get('[id="heatContentType"]').select(heatContentType);
    cy.get('[id="emissionHeight"]').type("{selectAll}").type(emissionHeight);
    cy.get('[id="emissionTemperature"]').type("{selectAll}").type(emissionTemperature);
    cy.get('[id="outflowDiameter"]').type("{selectAll}").type(outflowDiameter);
    cy.get('[id="outflowDirectionType"]').type("{selectAll}").select(outflowDirectionType);
    cy.get('[id="outflowVelocity"]').type("{selectAll}").type(outflowVelocity);
  }

  static sourceCharacteristicsOutflowWithBuildingCorrectlySaved(
    heatContentType: string,
    buildingInfluence: string,
    emissionHeight: string,
    emissionTemperature: string,
    outflowDiameter: string,
    outflowDirectionType: string,
    outflowVelocity: string,
    diurnalVariation: string
  ) {
    cy.get('[id="sourcelistDetailVentilation"]').should("have.text", heatContentType);
    cy.get('[id="sourcelistDetailBuilding"]').should("have.text", buildingInfluence);
    this.assertMinMaxLabel("sourcelistDetailEmissionHeight", emissionHeight);
    this.assertMinMaxLabel("sourcelistDetailOutflowDiameter", outflowDiameter + " m");
    cy.get('[id="sourcelistDetailOutflowDirection"]').should("have.text", outflowDirectionType);
    this.assertMinMaxLabel("sourcelistDetailOutflowVelocity", outflowVelocity + " m/s");
    cy.get('[id="sourcelistDetailDiurnalVariation"]').should("have.text", diurnalVariation);

    if (buildingInfluence == "Geen") {
      cy.get('[id="sourcelistDetailEmissionTemperature"]').should("have.text", emissionTemperature);
    } else {
      this.assertMinMaxLabel("sourcelistDetailEmissionTemperature", emissionTemperature);
    }
  }

  static assertMinMaxLabel(dataId: string, dimension: string) {
    if (dimension.includes("(")) {
      const originalDimension = dimension.split("(")[0].trim();
      const correctedDimension = dimension.split("(")[1].replace("(", "").replace(")", "").trim();

      cy.get('[id="' + dataId + '"] [id="noSingleColumnMinMaxLabel"]').should("have.text", originalDimension);
      cy.get('[id="' + dataId + '"] [id="noSingleColumnMinMaxLabelCorrected"]').should("have.text", correctedDimension);
    } else {
      cy.get('[id="' + dataId + '"] > [id="minMaxLabel"]').should("have.text", dimension);
    }
  }

  static spreadNotVisible() {
    cy.get('[id="sourceSpread"]').should("not.exist");
  }

  static spreadIsVisible() {
    cy.get('[id="sourceSpread"]').should("exist");
  }

  static fillInSpread(inputSpread: string) {
    cy.get('[id="sourceSpread"]').clear().type(inputSpread);
  }

  static spreadCorrectlySaved(valueSpread: string) {
    cy.get('[id="sourcelistDetailSpread"]').should("have.value", valueSpread);
  }

  // UK Specific
  static sourceCharacteristicsCorrectlySavedUK(sectorGroup: string, emissionNOx: string, emissionNH3: string) {
    cy.get('[id="sourceListDetailSectorGroup"]').should("have.text", sectorGroup);
    cy.get('[id="genericDetailEmission"]').eq(0).should("have.text", emissionNOx);
    cy.get('[id="genericDetailEmission"]').eq(1).should("have.text", emissionNH3);
  }

  static fillEmissionFields(emissionNOx: string, emissionNH3: string) {
    cy.get('[id="collapsibleSubSources"]').click();
    cy.get('[id="emissionRow_NOX"]').type("{selectAll}").type(emissionNOx);
    cy.get('[id="emissionRow_NH3"]').type("{selectAll}").type(emissionNH3);
  }

  // Fill in fields for source with a sector group and a sector
  static fillInFieldsForSectorGroupAndSector(sectorGroup: string, sector: string) {
    // //Sector
    cy.get('[id="label"]').clear().type("Bron 1");
    // Specific sector group
    cy.get('[id="sourceListDetailSectorGroup"]').select(sectorGroup);
    // Specific sector for agriculture
    cy.get('[id="sourceListDetailSector"]').select(sector);
    // Location
    cy.get('[id="emissionSourceGeometryInput"]').type("POINT(172203.2 599120.32)");
    // Source characteristics
    cy.get('[id="collapsibleOPSCharacteristics"]').click();
    cy.get('[id="heatContentType"]').select("Niet geforceerd");
    cy.get('[id="emissionHeight"]').type("15");
    cy.get('[id="emissionHeatContent"]').type("20");
  }

  // Validate fields for source with a sector group and a sector
  static sourceWithSectorGroupAndSectorCreated(sectorGroup: string, sector: string, sourceName: string) {
    // Validate Sector
    cy.get('[id="sourceListDetailTitle"]').should("have.text", sourceName);
    // Validate specific sector group
    cy.get('[id="sourceListDetailSectorGroup"]').should("have.text", sectorGroup);
    // Validate specific sector
    cy.get('[id="sourceListDetailSector"]').should("have.text", sector);
    // Validate Location
    cy.get('[id="sourceListDetailLocation"]').should("have.text", "X:172203,2 Y:599120,32");
    // Validate Source characteristics
    cy.get('[id="sourcelistDetailVentilation"]').should("have.text", "Niet geforceerd");
    cy.get('[id="sourcelistDetailEmissionHeight"]').should("have.text", "15,0 m");
    cy.get('[id="sourcelistDetailHeatContent"]').should("have.text", "20,000 MW");
    cy.get('[id="sourcelistDetailDiurnalVariation"]').should("have.text", "Continue Emissie");
    /*
     * Validate total emissions
     * cy.get('[id="sourcelistTotalNOX"]').should('have.text','x') //TODO AER3-913
     * cy.get('[id="sourcelistTotalNH3"]').should('have.text','y') //TODO AER3-913
     */

    this.assertInfluencingBuildingIs("Geen");
  }

  // Save the data for source
  static saveSource() {
    cy.get('[id="buttonSave"]').click();
  }

  static saveSourceValidateRefresh() {
    cy.intercept("/api/v8/ui/source/refreshEmissions").as("refreshEmissions");
    this.saveSource();
    cy.wait("@refreshEmissions", { responseTimeout: CommonSettings.validationTimeOut });
    //wait till update spinner is hidden
    cy.get(".loading", { timeout: CommonSettings.validationTimeOut }).should("not.have.css", "opacity", "1");
  }

  static startCalculation() {
    cy.get('[id="calculateCalculationJobButton"]').click();
  }

  // Verify save is disabled
  static saveSourceDisabled() {
    // Click the button a bunch of times.
    for (let i = 0; i < 3; i++) {
      this.saveSource();
    }

    // If button is still visible, assume save didn't complete and is thus disabled.
    cy.get("[id=buttonSave]").should("be.visible");
  }

  // Save the data for source and acknowlegde the warning and wait for refresh
  static saveSourceValidateWarningAndValidateRefresh() {
    // Click the save button
    cy.get('[id="buttonSave"]').click();
    // Click the acknowledge button for the warning
    this.saveSourceValidateRefresh();
    this.openEmissionSources();
  }

  // Open Emissionsources again
  static unfoldSources() {
    cy.get('[id="collapsible-panel-EMISSION_SOURCES-title"]').click();
  }

  static scrollToBottomSources() {
    cy.get(".sources > div").find("> div").last().scrollIntoView();
  }
  static openBuildingsPanel() {
    cy.get('[id="collapsible-panel-BUILDING-title"]').click();
  }

  static selectSource(sourceName: string) {
    cy.get(".sources").contains(sourceName).click();
    switch (sourceName) {
      case "Road transport network":
      case "Verkeersnetwerk":
        break;
      default:
        cy.get('[id="sourceListDetailButtonClose"]').should("exist");
        cy.get('[id="sourceListDetailTitle"]').should("contain", sourceName);
    }
  }

  static selectSituation(situationName: string) {
    cy.get('[id="situationSelector"]').select(situationName);
  }

  static editSource() {
    cy.get('[id="sourceListButtonEditSource"]').click();
  }

  static clickToOpenTab(tabName: string) {
    cy.get("[id=" + tabName + "]").click();
  }

  static validateTabTitle(tabName: string, tabTitle: string) {
    cy.get("[id=" + tabName + "] > .tab-text").should("contain", tabTitle);
  }

  static emptyField(fieldName: string) {
    cy.get("[id=" + fieldName + "]").clear();
  }

  static copySource(sourceName: string) {
    cy.get('[id="sourceListButtonCopySource"]').click();
  }

  static deleteSelectedSource(sourceName: string) {
    cy.get(".sources").contains(sourceName).click();
    cy.get('[id="sourceListButtonDeleteSource"]').click();
  }

  // The next source(s) exist(s)
  static sourcesExists(sourceName1: string, sourceName2: string) {
    cy.get(".sources").contains(sourceName1);
    cy.get(".sources").contains(sourceName2);
  }

  // The next source(s) does not exist(s)
  static sourcesDoesNotExists(sourceName: string) {
    cy.get(".sources").should("not.contain", sourceName);
  }

  // The sources list is empty and button new source is visible
  static sourcesIsEmpty() {
    cy.get('[id="calculationLaunchButtonNewsource"]').should("exist");
  }

  // Click on button to import a source from start page
  static clickImportSourceFromStartPage() {
    cy.get('[id="fileInput"]').click();
  }

  // Click on button to import
  static chooseToImport(forceSave = false) {
    this.saveImportFile(forceSave);
  }

  // Go to the start page
  static clickStartMenuButton() {
    cy.get('[id="NAV_ICON-MENU-HOME"]').click();
  }

  // Open file dialog to import
  static openFileDialogToImport() {
    cy.get('[id="buttonOpenFileDialog"]').click();
  }

  static selectCalculationPointFileForImport(fileName: string) {
    // wait for validation for sources
    this.selectFileForImport(fileName);
    // wait for validation for calculationpoints
    cy.intercept("/api/v8/ui/import/*/validationresult").as("validationResult");
    cy.wait("@validationResult");
  }

  // Select file to import and test validationStatus
  static selectFileForImport(fileName: string) {
    this.selectFileForImportDoNotImport(fileName, false);
    this.waitForUploadToFinish();
    cy.get("#buttonSaveimport").scrollIntoView();
    cy.get("#buttonSaveimport").click();
    cy.get("@validationResult").its("response.statusCode").should("eq", 200);
    // wait for the pop-up message to appear so the buttons have time to become active
    cy.get(".message", { timeout: CommonSettings.responseTimeOut });
  }

  // Select file to import and test validationStatus
  static selectFileForImportDoNotImport(fileName: string, isAdvancedEnabled: boolean, openExpandablePanel = true) {
    cy.intercept("/api/v8/ui/import/*/validationresult").as("validationResult");
    const updatedFileName = this.getUpdatedFileName(fileName);
    const fileInput: any = cy.get('[id="fileInput"]');
    fileInput.selectFile(updatedFileName, { force: true });

    this.checkAndWaitForValidationResponse();

    if (openExpandablePanel) {
      if (isAdvancedEnabled) {
        // wait for listContainer to fill
        cy.get(".listContainer > .fileContainer").should((items) => {
          expect(items.length).to.be.greaterThan(1);
        });
        cy.get(".listContainer > .fileContainer").then((elm) => {
          for (var i = 1; i < elm.length; i++) {
            this.checkAndWaitForValidationResponse();
          }
          cy.get('[title="' + this.getFileNameFromPath(updatedFileName).pop() + '"]').click({ multiple: true });
        });
      } else {
        cy.get('[title="' + this.getFileNameFromPath(updatedFileName).pop() + '"]').click({ multiple: true });
      }
    }
  }

  static checkAndWaitForValidationResponse() {
    const checkAndWaitForValidationResponse = (count: number) => {
      if (count >= 0) {
        cy.wait("@validationResult").then((request: any) => {
          if (count == 0) {
            console.log(request);
            console.log(request.response ? request.response.body.validationStatus : "No response is received");
          }
          if (request.response.body.validationStatus != "PENDING") {
            // exit the loop
            cy.log("validation is not 'pending' anymore");
          } else {
            checkAndWaitForValidationResponse(count--);
          }
        });
      } else {
        throw new Error("Validation response requests exceeds limit");
      }
    };
    checkAndWaitForValidationResponse(10);
  }

  static getFileNameFromPath(fileName: string) {
    return fileName.split(/[\\/]/);
  }

  // Update file name to add .zip when running in BrowserStack environment
  static getUpdatedFileName(fileName: string) {
    let browserStackUpdateFileName = new String(Cypress.env("BROWSERSTACK")).valueOf();
    cy.log("what is value of browserStackUpdateFileName:" + browserStackUpdateFileName);
    if (browserStackUpdateFileName == "true" && fileName.substring(fileName.length - 3) != "zip") {
      let outputFile = ".\\temp\\" + fileName + ".zip";
      cy.task("zip", {
        inputFile: ".\\cypress\\fixtures\\" + fileName,
        outputFile: ".\\cypress\\fixtures\\" + outputFile
      });
      cy.log("updating file name :" + outputFile);
      return "./cypress/fixtures/" + outputFile;
    } else {
      return "./cypress/fixtures/" + fileName;
    }
  }

  static saveImportFile(forceSave = true) {
    cy.get('[id="buttonSaveimport"]').click({ force: forceSave });

    this.waitForImportToFinish();
  }

  static waitForImportToFinish() {
    cy.get(".importing").should("not.exist");
  }

  // Drag and drop file to import
  static dragAndDropFileForImport(fileName: string) {
    cy.get('[id="fileInput"]').selectFile("./cypress/fixtures/" + fileName, {
      action: "drag-drop"
    });
    this.waitForUploadToFinish();
    this.saveImportFile();
  }

  // Wait until the loader and the spinner are gone
  static waitForUploadToFinish() {
    cy.get(".loader", { timeout: CommonSettings.importLoadingTimeOut }).should("not.exist");
    cy.get(".importing", { timeout: CommonSettings.importLoadingTimeOut }).should("not.exist");
  }

  static validateImportErrorWarningCount(count: number, warningType: string) {
    if (warningType == "errors") {
      cy.get('[id="tab-ERRORS"]').click();
      if (count == 0) {
        cy.get(".errors").should("not.exist");
      } else {
        cy.get(".errors").should("exist");
        cy.get(".errors").children().should("have.length", count);
      }
    } else if (warningType == "warnings") {
      cy.get('[id="tab-WARNINGS"]').click();
      if (count == 0) {
        cy.get(".warning").should("not.exist");
      } else {
        cy.get(".warning").should("exist");
        cy.get(".warning").children().should("have.length", count);
      }
    }
  }

  static validateWarningsMessages(dataTable: any) {
    cy.get('[id="tab-WARNINGS"]').click();
    dataTable.hashes().forEach((element: any) => {
      cy.get(".warning").eq(element.row).contains(element.warning);
    });
  }

  static validateErrorMessages(dataTable: any) {
    cy.get('[id="tab-ERRORS"]').click();
    dataTable.hashes().forEach((element: any) => {
      cy.get(".error").eq(element.row).contains(element.error);
    });
  }

  static selectImportActionForImport(importAction: string) {
    cy.get('[id="importActions"]').select(importAction);
  }

  static selectSituationTypeImport(situationType: string) {
    cy.get('[id="situationSelection"]').select(situationType);
  }

  static duplicateSituation() {
    cy.get('[id="duplicateButton"]').click();
  }

  // Select file to import and choose import action and situation type
  static selectFileForImportAndSelectImportActionAndSituationType(fileName: string, importAction: string, situationType: string) {
    this.selectFileForImportDoNotImport(fileName, false);
    this.selectImportActionForImport(importAction);
    this.selectSituationTypeImport(situationType);
    this.waitForUploadToFinish();
  }

  // Select file to import and choose import action
  static selectFileForImportAndSelectImportAction(fileName: string, importAction: string) {
    this.selectFileForImportDoNotImport(fileName, false);
    this.selectImportActionForImport(importAction);
    this.waitForUploadToFinish();
  }

  // Select file to import and choose situation type
  static selectFileForImportAndSelectSituationType(fileName: string, situationType: string) {
    this.selectFileForImportDoNotImport(fileName, false);
    this.selectSituationTypeImport(situationType);
    this.waitForUploadToFinish();
  }

  // Select import action for specific file
  static selectImportActionForFile(importAction: string, fileName: string) {
    cy.get(".name").contains(fileName).parent().parent().find('[id="importActions"]').select(importAction);
  }

  // Validate noticication text
  static followingNotificationIsDisplayed(notificationText: string) {
    // use a large timeout as way to wait if timeconsuming task is completed
    cy.contains(".notification", notificationText, { timeout: CommonSettings.notificationTimeOut });
  }

  static followingSystemProfileNotificationIsDisplayed(notificationText: string) {
    cy.get(".notice").should("have.text", notificationText);
  }

  static clearNotifications() {
    cy.get(".buttonContainer").within(() => {
      cy.get(".removeAll").click();
    });
  }
  static followingTitleIsDisplayed(title: string) {
    cy.get('[title = "' + title + '"]', { timeout: CommonSettings.notificationTimeOut });
  }

  static followingSubStringIsDisplayed(notificationText: string, textShouldBeDisplayed: boolean) {
    if (textShouldBeDisplayed) {
      cy.get(".historyContainer").should("include.text", notificationText, { timeout: CommonSettings.notificationTimeOut });
    } else {
      cy.get(".historyContainer").should("not.include.text", notificationText, { timeout: CommonSettings.notificationTimeOut });
    }
  }

  static assertNotificationCenterIsOpen() {
    cy.get(".notificationPanel").should("exist").should("not.have.css", "display", "none");
  }

  static assertNotificationCenterIsClosed() {
    cy.get(".notificationPanel", { timeout: CommonSettings.notificationCenterTimeOut }).should("have.css", "display", "none");
  }

  static assertNoNotificationIsDisplayed() {
    cy.get(".notificationButton > .counter").should("not.exist");
  }

  static assertNumberOfNotificationsIsDisplayed(expectedAmountOfNotifications: number) {
    cy.get(".notificationButton > .counter").should("have.text", expectedAmountOfNotifications);
  }

  static assertNotificationIsDisplayedWithDateTime(notificationText: string) {
    this.assertNotificationCenterIsOpen();
    cy.get(".notificationPanel .message > .text").contains(notificationText).should("exist");
    this.checkNotificationCenterDateTime(notificationText);
  }

  static checkNotificationCenterDateTime(notificationText: string) {
    // To match a date time string in the format the notification center is using, the following regex pattern is used:
    //
    // ^\d{1,2}: Matches the day at the beginning of the string, allowing for one or two digits.
    // \s: Matches a space.
    // (?:januari|februari|maart|april|mei|juni|juli|augustus|september|oktober|november|december): Matches the month by name.
    // \s\d{4}: Matches a space followed by a four-digit year.
    // ,\s: Matches a comma followed by a space.
    // \d{2}:\d{2}: Matches the time in HH:MM format.
    // \suur$: Matches the literal string " uur" at the end of the string.

    cy.get(".notificationPanel .message > .text")
      .contains(notificationText)
      .parent()
      .find(".datetime > span")
      .then((element) => {
        expect(element.text()).to.match(
          /^\d{1,2}\s(?:januari|februari|maart|april|mei|juni|juli|augustus|september|oktober|november|december)\s\d{4},\s\d{2}:\d{2}\suur$/
        );
      });
  }

  static openNotificationCenter() {
    cy.get(".notificationButton").click();
  }

  static deleteNotification(notificationText: string) {
    cy.get(".notificationPanel .message > .text").contains(notificationText).parent().find('[id="removeButton"]').click();
  }

  static validateBuildingMessage(warningMessage: string) {
    if (warningMessage == "") {
      cy.get(".warningContainer").should("not.exist");
    } else {
      cy.get(".warningContainer").contains(warningMessage);
    }
  }

  static checkDocumentionSpecification(dataTable: any) {
    dataTable.hashes().forEach((element: any) => {
      cy.get(".html-output-container .html-content-target").within(() => {
        cy.get("a[target='_blank']").eq(element.row).contains(element.description);
        cy.get("a[target='_blank']").eq(element.row).should("have.attr", "href").and("include", element.url);
        cy.get("p").contains(element.additionalText);
      });
      // validate if link exists
      cy.request({
        url: element.url,
        failOnStatusCode: false
      }).then((response) => {
        expect(response.status).to.eq(200, "Validate url exists : " + element.url);
      });
    });
  }

  static wait(wait: number) {
    cy.wait(wait);
  }

  static openSubSource() {
    cy.get('[id="collapsibleSubSources"]').click();
  }

  static subsourceErrorTextExists(errorText: string) {
    cy.get(".noticeContainer:first > p").should("have.text", errorText);
  }

  static saveSourceErrorTextExists(errorText: string) {
    cy.get(".errorContainer:last > div").should("have.text", errorText);
  }

  static followingWarningIsDisplayed(warningText: string) {
    cy.on("window:alert", (txt) => {
      expect(txt).to.contains(warningText);
    });
  }

  static followingWarningIsVisible(warningText: string) {
    cy.get(".noticeContainer").contains(warningText);
  }

  static textOmittedAreasIsVisible(warningText: string) {
    cy.get('[id="resultsSummaryOmittedAreaTitle"]').contains(warningText, { timeout: CommonSettings.resultsLoadingTimeOut });
  }

  static textSiteRelevantThresholdIsVisible(text: string) {
    cy.get(".classification").contains(text);
  }

  static warningIsNotVisible() {
    cy.get(".noticeContainer").should("not.exist");
  }

  static followingErrorIsVisible(errorText: string) {
    cy.get(".errorContainer").contains(errorText);
  }

  static chooseNotShowPrivacyWarning() {
    cy.get('[id="closePrivacyWarning"]').click();
  }

  static enterSituationName(situationName: string) {
    cy.get('[id="sourceListSituationName"]').clear();
    if (situationName.length > 0) {
      cy.get('[id="sourceListSituationName"]').type(situationName);
    }
  }

  static validateSituationNameError(errorMessage: string) {
    cy.get('[id="sourceListSituationName"]').parent().parent().parent().within(() => {
      cy.get('[id="inputErrorMessage"]').should("have.text", errorMessage);
    });
  }

  static validateSituationName(situationName: string) {
    cy.get('[id="sourceListSituationName"]').should("have.value", situationName);
  }

  static validateSituationType(situationType: string) {
    cy.get('[id="sourceListSituationType"]').find("option:selected").should("have.text", situationType);
  }

  static validateCalculationYear(calculationYear: string) {
    cy.get('[id="sourceListSituationYear"]').find("option:selected").should("have.text", calculationYear);
  }

  static validateNettingFactor(nettingFactor: string) {
    cy.get('[id="sourceListSituationNettingFactor"]').should("have.value", nettingFactor);
  }

  static selectViewMode(viewMode: string) {
    cy.get("#collapsible-panel-" + viewMode + " > .line").click();
  }

  static validateSource(sourceCount: number) {
    cy.log("validate source count " + sourceCount);
    if (sourceCount == 0) {
      cy.get(".sources").should("not.exist");
    } else {
      cy.get(".sources").find(".row").should("have.length", sourceCount);
    }
  }

  static assertNumberOfCalculationJobs(jobCount: number) {
    cy.log("validating calculation job count " + jobCount);
    if (jobCount == 0) {
      cy.get(".jobs").should("not.exist");
    } else {
      cy.get(".jobs").should("exist");
    }
    cy.get(".jobs .transitionGroup > .row-container").should("have.length", jobCount);
  }

  static validateRoadSource(roadSourceCount: number) {
    cy.log("validate road source count " + roadSourceCount);
    if (roadSourceCount == 0) {
      cy.get(".sources").find(".expand").should("not.exist");
    } else {
      cy.get(".sources").find(".expand").click();
      cy.get(".road-network-sources").find(".row").should("have.length", roadSourceCount);
    }
  }

  static deleteCurrentCalculationSetting() {
    cy.get('[id="deleteButton"]').click();
  }

  static deleteCurrentScenario() {
    cy.get('[id="deleteButton"]').click();
  }

  static openCollapsibleCalculationSetting(job: string) {
    cy.get('[id="collapsibleCalculationSettings-' + job.replaceAll(" ", "-") + '"]').click();
  }

  static selectSituationForCalculation(situation: string, situationId: string) {
    cy.get(".collapsible-item.open")
      .find('[id="' + situationId + '"]')
      .select(situation);
  }

  static navigateBack() {
    cy.go("back");
  }

  static cancelCalculation() {
    cy.get(".cancelButton").click();
  }

  static closeSourceDetail() {
    cy.get('[id="sourceListDetailButtonClose"]').click();
  }

  static startCalculationAndValidateRequest(dataTable: any, buttonId: string) {
    Cypress.on("uncaught:exception", (err) => {
      /*
       * Returning false here prevents Cypress from
       * failing the test in case a unrelated error occurs
       */
      return false;
    });
    cy.intercept("/api/v8/ui/calculation").as("postCalculation");
    cy.get('[id="' + buttonId + '"]').click();
    cy.wait("@postCalculation")
      .its("request")
      .then((request) => {
        dataTable.hashes().forEach((dataRow: any) => {
          for (const key in dataRow) {
            if (!!dataRow[key]) {
              switch (key) {
                case "situationNr":
                case "featureNr":
                  break;
                case "label":
                case "emissionHeight":
                case "heatContent":
                case "coordinateType":
                case "coordinates":
                case "NO2":
                case "PM10":
                case "NOX":
                case "PM25":
                case "NH3":
                case "EC":
                case "buildingId":
                case "gmlId":
                case "buildingLabel":
                case "buildingHeight":
                case "buildingCoordinates":
                  // validate actual value
                  assert.equal(this.getActualValue(request, dataRow.situationNr, dataRow.featureNr, key), dataRow[key]);
                  break;
                default:
                  // validate based on translation of the key
                  assert.equal(
                    this.getActualValue(request, dataRow.situationNr, dataRow.featureNr, key),
                    this.getExpectedValue(dataRow[key])
                  );
                  break;
              }
            }
          }
        });
      });
  }

  static getActualValue(request: any, situationNr: string, featureNr: string, key: string) {
    switch (key) {
      case "type":
        return request.body.scenario.situations[situationNr][key];
      case "label":
        return request.body.scenario.situations[situationNr].emissionSources.features[featureNr].properties[key];
      case "buildingId":
      case "emissionHeight":
      case "heatContent":
      case "heatContentType":
      case "diurnalVariation":
        return request.body.scenario.situations[situationNr].emissionSources.features[featureNr].properties.characteristics[key];
      case "NO2":
      case "PM10":
      case "NOX":
      case "PM25":
      case "NH3":
      case "EC":
        return request.body.scenario.situations[situationNr].emissionSources.features[featureNr].properties.emissions[key];
      case "coordinateType":
        return request.body.scenario.situations[situationNr].emissionSources.features[featureNr].geometry.type;
      case "coordinates":
        return "[" + request.body.scenario.situations[situationNr].emissionSources.features[featureNr].geometry.coordinates + "]";
      case "gmlId":
        return request.body.scenario.situations[situationNr].buildings.features[featureNr].properties.gmlId;
      case "buildingLabel":
        return request.body.scenario.situations[situationNr].buildings.features[featureNr].properties.label;
      case "buildingHeight":
        return request.body.scenario.situations[situationNr].buildings.features[featureNr].properties.height;
      case "buildingCoordinates":
        request.body.scenario.situations[situationNr].buildings.features[featureNr].geometry.coordinates;
        return "[" + request.body.scenario.situations[situationNr].buildings.features[featureNr].geometry.coordinates + "]";
      default:
        throw new Error("Unknown key: " + key);
    }
  }

  static getExpectedValue(key: string) {
    switch (key) {
      case "Beoogd":
        return "PROPOSED";
      case "Referentie":
        return "REFERENCE";
      case "Saldering":
        return "OFF_SITE_REDUCTION";
      case "Tijdelijk":
        return "TEMPORARY";
      case "Niet geforceerd":
        return "NOT_FORCED";
      case "Geforceerd":
        return "FORCED";
      case "Continue Emissie":
        return "CONTINUOUS";
      case "Standaard Profiel Industrie":
        return "INDUSTRIAL_ACTIVITY";
      case "Verwarming van Ruimten":
        return "SPACE_HEATING";
      case "Transport":
        return "TRAFFIC";
      case "Dierverblijven":
        return "ANIMAL_HOUSING";
      case "Meststoffen":
        return "FERTILISER";
      case "Verwarming van Ruimten (Zonder Seizoenscorrectie)":
        return "SPACE_HEATING_WITHOUT_SEASONAL_CORRECTION";
      case "Licht Verkeer":
        return "LIGHT_DUTY_VEHICLES";
      case "Zwaar Verkeer":
        return "HEAVY_DUTY_VEHICLES";
      case "Bussen":
        return "BUSES";
      case "buildingCoordinates":
        return "coordinates";
      case "buildingLabel":
        return "label";
      case "buildingHeight":
        return "height";
      default:
        //  return key;
        throw new Error("Unknown key: " + key);
    }
  }

  static chooseToCancel(debugId: string) {
    cy.get('[id="buttonCancel' + debugId + '"]').click();
  }

  static importFileOpenDetails(importFileName: string) {
    cy.get('[title="' + this.getFileNameFromPath(importFileName).pop() + '"]').click();
  }

  static sourceValidationErrorsVisible(visibilityStatus: string) {
    switch (visibilityStatus) {
      case "be visible":
        cy.get(".errorContainer").should("be.visible");
        break;
      case "not exist":
        cy.get(".errorContainer").should("not.be.exist");
        break;
      default:
        throw new Error("Unknown visibilityStatus: " + visibilityStatus);
    }
  }

  static defaultDrawingType(drawingType: string) {
    switch (drawingType) {
      case "Puntbron":
      case "Point source":
        cy.get('[id="sourceLocationButtonAddPoint"]').should("have.class", "active");
        cy.get('[id="sourceLocationButtonAddLine"]').should("not.have.class", "active");
        cy.get('[id="sourceLocationButtonAddPolygon"]').should("not.have.class", "active");
        break;
      case "Lijnbron":
      case "Line source":
        cy.get('[id="sourceLocationButtonAddPoint"]').should("not.have.class", "active");
        cy.get('[id="sourceLocationButtonAddLine"]').should("have.class", "active");
        cy.get('[id="sourceLocationButtonAddPolygon"]').should("not.have.class", "active");
        break;
      case "Vlakbron":
      case "Area source":
        cy.get('[id="sourceLocationButtonAddPoint"]').should("not.have.class", "active");
        cy.get('[id="sourceLocationButtonAddLine"]').should("not.have.class", "active");
        cy.get('[id="sourceLocationButtonAddPolygon"]').should("have.class", "active");
        break;
      default:
        throw new Error("Unknown drawingType: " + drawingType);
    }
  }

  static selectDrawingType(drawingType: string) {
    switch (drawingType) {
      case "Puntbron":
      case "Point source":
        cy.get('[id="sourceLocationButtonAddPoint"]').click();
        break;
      case "Lijnbron":
      case "Line source":
        cy.get('[id="sourceLocationButtonAddLine"]').click();
        break;
      case "Vlakbron":
      case "Area source":
        cy.get('[id="sourceLocationButtonAddPolygon"]').click();
        break;
      default:
        throw new Error("Unknown drawingType: " + drawingType);
    }
  }

  static valuesDroplistCalculationYear(dataTable: any) {
    dataTable.hashes().forEach((element: any) => {
      cy.get('[id="sourceListSituationYear"]').contains(element.calculationYear);
    });
  }

  static setCalculationYear(calculationYear: string) {
    calculationYear = CommonSettings.updateCalculationYear(calculationYear);
    cy.get('[id="sourceListSituationYear"]').select(calculationYear);
  }

  static selectScenarioType(scenarioType: string) {
    cy.get('[id="sourceListSituationType"]').select(scenarioType);
  }

  static fillInNettingFactor(factor: string) {
    cy.get('[id="sourceListSituationNettingFactor"]').clear().type(factor);
  }

  static saveSituationUpdate() {
    cy.get('[id="sourceListSituationApply"]').click();
  }

  static enableAdvancedImportmodus() {
    cy.get(".icon-menu-settings").click();
    cy.get('[id="advancedImport"]').check({ force: true });
    cy.get(".icon-close").first().click();
  }

  static assertBuildingListHasNumberOfBuildings(numberOfBuildings: number) {
    if (numberOfBuildings == 0) {
      cy.get(".emptyBuildings").should("exist");
    } else {
      cy.get(".buildings > div").find("> div").should("have.length", numberOfBuildings);
    }
  }

  static selectBuildingWithName(name: string) {
    cy.get(".buildings .labelColumn").contains(name).click({ force: true });
  }

  static clickOnSpecificButton(buttonName: string) {
    switch (buttonName) {
      case "error":
        cy.get('[id="fileErrorIconButton"]').click();
        break;
      case "warning":
        cy.get('[id="fileWarningIconButton"]').click();
        break;
      case "Alle verwijderen":
      case "Delete all":
        cy.get('[id="removeAll-import"]').click();
        break;
      case "delete source":
        cy.get('[id="sourceListButtonDeleteSource"]').click();
        break;
      case "Edit time varying profile":
        cy.get('[id="time-varying-sourceListButtonEditSource"]').click();
        break;
      case "Copy time varying profile":
        cy.get('[id="time-varying-sourceListButtonCopySource"]').click();
        break;
      case "Delete time varying profile":
        cy.get('[id="time-varying-sourceListButtonDeleteSource"]').click();
        break;
      default:
        throw new Error("Unknown buttonName: " + buttonName);
    }
  }

  static checkTextIsVisible(textType: string, visibleText: string) {
    switch (textType) {
      case "warning":
        cy.get(".warning > p").contains(visibleText);
        break;
      case "error":
        cy.get(".error > p").contains(visibleText);
        break;
    }
  }

  static addNewFarmlandSource() {
    cy.get('[id="collapsibleSubSources"]').click();
    cy.get('[id="sourceListButtonNewSource"]').click();
  }

  static assertSituationType(fileName: string, situationType: string) {
    cy.get(".name").contains(fileName).parent().parent().find(".situationLabel").should("have.text", situationType);
  }

  static assertEnabledDisabledSituationType(fileName: string, isInputEnabled: string) {
    switch (isInputEnabled) {
      case "enabled":
        cy.get(".name").contains(fileName).parent().parent().find('[id="situationSelection"]').should("exist");
        break;
      case "disabled":
        cy.get(".name").contains(fileName).parent().parent().find('[id="situationSelection"]').should("not.exist");
        break;
      default:
        throw new Error("Unknown input status of field");
    }
  }

  static assertPageLocationIsIntroduction() {
    cy.get(".html-output-container .html-content-target > h2").contains("Introductie");
  }

  static assertButtonIsDisabled(buttonName: string) {
    switch (buttonName) {
      case "Alle verwijderen":
      case "Remove all":
        cy.get('[id="removeAll-import"]').should("be.disabled");
        break;
      case "Importeer":
      case "Import":
        cy.get('[id="buttonSaveimport"]').should("be.disabled");
        break;
      case "Edit time varying profile":
        cy.get('[id="time-varying-sourceListButtonEditSource"]').should("be.disabled");
        break;
      case "Copy time varying profile":
        cy.get('[id="time-varying-sourceListButtonCopySource"]').should("be.disabled");
        break;
      case "Delete time varying profile":
        cy.get('[id="time-varying-sourceListButtonDeleteSource"]').should("be.disabled");
        break;
      default:
        throw new Error("Unknown button name: " + buttonName);
    }
  }

  static assertButtonIsEnabled(buttonName: string) {
    switch (buttonName) {
      case "Alle verwijderen":
      case "Remove all":
        cy.get('[id="removeAll-import"]').should("be.enabled");
        break;
      case "Importeer":
      case "Import":
        cy.get('[id="buttonSaveimport"]').should("be.enabled");
        break;
      case "Bladeren":
      case "Browse":
        cy.get('[id="buttonOpenFileDialog"]').should("be.enabled");
        break;
      case "Edit time varying profile":
        cy.get('[id="time-varying-sourceListButtonEditSource"]').should("be.enabled");
        break;
      case "Copy time varying profile":
        cy.get('[id="time-varying-sourceListButtonCopySource"]').should("be.enabled");
        break;
      case "Delete time varying profile":
        cy.get('[id="time-varying-sourceListButtonDeleteSource"]').should("be.enabled");
        break;
      default:
        throw new Error("Unknown button name: " + buttonName);
    }
  }

  static assertImportFileIsNotVisible(fileName: string) {
    cy.get('[id="fileInput"]').find(fileName).should("not.exist");
  }

  static assertAddingToSituationIsImpossible(fileName: string) {
    cy.get(".name").contains(fileName).parent().parent().find('[label="Toevoegen aan bestaande situatie"]').should("not.exist");
  }

  static assertAddingToSituationIsPossible(fileName: string) {
    cy.get(".name").contains(fileName).parent().parent().find('[label="Toevoegen aan bestaande situatie"]').should("exist");
  }

  static uncheckBoxForImport(uncheckedElement: string) {
    switch (uncheckedElement) {
      case "emissiebron":
      case "emission source":
        cy.get(".section").contains("emissiebron").click();
        break;
      case "gebouw":
      case "building":
        cy.get(".section").contains("gebouw").click();
        break;
      case "rekentaak":
      case "calculation job":
        cy.get(".section").contains("rekentaak").click();
        break;
      case "time-varying profiles":
        cy.get(".section").contains("time-varying profiles").click();
        break;
      default:
        throw new Error("Unknown element: " + uncheckedElement);
    }
  }

  static validateImportOptionsAvailability(dataTable: any) {
    dataTable.hashes().forEach((importOption: any) => {
      if (importOption.available == "true") {
        cy.get(".section").contains(importOption.element);
      } else {
        cy.get(".section").contains(importOption.element).should("not.exist");
      }
    });
  }

  static assertPageTitleIs(expectedTitle: string) {
    cy.get("h1").should("have.text", expectedTitle);
  }

  static assertTotalEmission(emissionSourceType: string, emissionType: string, emission: string, inverted = false) {
    let testIDBase = "";

    switch (emissionSourceType) {
      case "generic":
        testIDBase = "genericDetailTotalEmission-";
        break;
      case "road":
        testIDBase = "srm2DetailRoadStandardEmissionTotalValue-";
        break;
      case "farm lodging":
        testIDBase = "farmLodgingTotalEmission-";
        break;
      case "animal housing":
        testIDBase = "farmAnimalHousingTotalEmission-";
        break;
      case "farmland":
        testIDBase = "farmlandTotalEmission-";
        break;
      case "maritime shipping":
        testIDBase = "maritimeShippingDetailTotalEmission-";
        break;
      case "inland shipping":
        testIDBase = "inlandShippingDetailTotalEmission-";
        break;
      case "mobile equipment":
        testIDBase = "offroadDetailTotalEmission-";
        break;
      case "scenario":
        testIDBase = "sourceListTotal";
        break;
      case "cold start":
        testIDBase = "coldStartDetailRoadStandardEmissionTotalValue-";
        break;
      default:
        throw new Error("unknown emissionSourceType: " + emissionSourceType);
    }
    cy.get("[id=" + testIDBase + emissionType.toUpperCase() + "]").should("exist");

    if (inverted) {
      cy.get("[id=" + testIDBase + emissionType.toUpperCase() + "]", { timeout: CommonSettings.emissionTextValidationTimeOut }).should(
        "not.have.text",
        emission
      );
    } else {
      cy.get("[id=" + testIDBase + emissionType.toUpperCase() + "]", { timeout: CommonSettings.emissionTextValidationTimeOut }).should(
        "have.text",
        emission
      );
    }
  }

  static assertEmission(emissionSourceType: string, rowIndex: string, emissionNOx: string, emissionNH3: string) {
    let testIDBase = "";

    switch (emissionSourceType) {
      case "standard inland shipping":
        testIDBase = "inlandStandardShippingEmission-";
        break;
      default:
        throw new Error("unknown emissionSourceType: " + emissionSourceType);
    }

    cy.get("[id=" + testIDBase + rowIndex + "-NOX]").should("have.text", emissionNOx);
    cy.get("[id=" + testIDBase + rowIndex + "-NH3]").should("have.text", emissionNH3);
  }

  static animalHousingSystemWithHousingCodeCreated(
    housingCode: string,
    numberOfAnimals: string,
    factor: string,
    reduction: string,
    emission: string
  ) {
    if (reduction === "") {
      cy.get('[id="farmAnimalHousingDetailStandardReduction"]').should("not.exist");
    } else {
      cy.get('[id="farmAnimalHousingDetailStandardReduction"]').should("have.text", reduction);
    }
    this.lodgingSystemWithHousingCodeCommonCreated(housingCode, numberOfAnimals, factor, emission);
  }

  static lodgingSystemWithHousingCodeDaysCreated(
    housingCode: string,
    numberOfAnimals: string,
    factor: string,
    days: string,
    emission: string
  ) {
    cy.get('[id="farmAnimalHousingDetailStandardNumberOfDays"]').should("have.text", days);
    this.lodgingSystemWithHousingCodeCommonCreated(housingCode, numberOfAnimals, factor, emission);
  }

  static lodgingSystemWithHousingCodeCommonCreated(housingCode: string, numberOfAnimals: string, factor: string, emission: string) {
    cy.get('[id="farmAnimalHousingDetailStandardCode"]').should("have.text", housingCode);
    cy.get('[id="farmAnimalHousingDetailStandardNumberOfAnimals"]').should("have.text", numberOfAnimals);
    cy.get('[id="farmAnimalHousingDetailStandardEmissionFactor"] > div').should("have.text", factor);
    cy.get('[id="farmAnimalHousingDetailStandardEmission"] > div').should("have.text", emission);
    cy.get('[id="farmAnimalHousingDetailDescription"]').contains(housingCode);
  }

  static saveCalculationJob() {
    cy.get('[id="buttonSaveCalculationJobBottomButtons"]').click();
  }

  static createNewCalculationJob() {
    cy.get("#buttonAddJob,#calculation-job-sourceListButtonNewSource").click();
  }

  static pressExportButton() {
    cy.get("#exportCalculationJobButton").click();
  }

  static editCalculationJob() {
    cy.get("#calculation-job-sourceListButtonEditSource").click();
  }

  static copyCalculationJob() {
    cy.get("#calculation-job-sourceListButtonCopySource").click();
  }

  static deleteSelectedCalculationJob() {
    cy.get("#calculation-job-sourceListButtonDeleteSource").click();
  }

  static deleteAllCalculationJobs() {
    cy.get("#calculationJobsListButtonDeleteAll").click();
  }

  static switchToCalculationJob(calculationJob: number) {
    cy.get('[id="calculationJob-' + calculationJob + '"]').click();
  }

  static selectCalculationJobType(jobType: string) {
    cy.get(".collapsible-item.open").find('[id="calculationJobType"]').select(jobType);
    cy.get(".collapsible-item.open").find('[id="calculationJobType"]').contains(jobType);
  }

  static selectCalculationJob(calculationJob: string) {
    cy.get('[title="' + calculationJob + '"]').click();
  }

  static selectCalculationMethod(method: string) {
    cy.get(".collapsible-item.open").find('[id="scenarioResultType"]').select(method);
    cy.get(".collapsible-item.open").find('[id="scenarioResultType"]').contains(method);
  }

  static checkCheckboxForTemporarySituation(tempSituationName: string) {
    cy.get(".collapsible-item.open")
      .contains(".label", tempSituationName)
      .parent()
      .parent()
      .find('[id^="otherSituation-TEMPORARY-"]')
      .parent()
      .find(".checkmark")
      .click();
  }

  static selectScenario(scenarioType: string, nameScenario: string) {
    switch (scenarioType) {
      case "Reference scenario":
        cy.get(".collapsible-item.open").find('[id="referenceSituation"]').select(nameScenario);
        break;
      case "Project scenario":
        cy.get(".collapsible-item.open").find('[id="proposedSituation"]').select(nameScenario);
        break;
      case "Off-site reduction scenario":
        cy.get(".collapsible-item.open").find('[id="off_site_reductionSituation"]').select(nameScenario);
        break;
      case "Temporary scenario":
        cy.get('[id^="otherSituation-TEMPORARY"]').first().check({ force: true });
        break;
      case "In-combination project":
        cy.get('[id^="situation-COMBINATION_PROPOSED"]').first().check({ force: true });
        break;
      case "In-combination reference":
        cy.get('[id^="situation-COMBINATION_REFERENCE"]').first().check({ force: true });
        break;
      case "Scenario":
        cy.get('[id="situation"]').select(nameScenario);
        break;
      default:
        throw new Error("Unknown scenario type: " + scenarioType);
    }
  }

  static addCalculationJobName(jobName: string) {
    cy.get(".collapsible-item.open").find('[id="label"]').clear().type(jobName);
  }

  static assertFieldVisibleAndEnabled(dataTable: any) {
    dataTable.hashes().forEach((calculationOption: any) => {
      switch (calculationOption.option) {
        case "Area":
          cy.get('[id="permitAreaSelection"]').should("exist").should("be.enabled");
          break;
        case "Zone of influence":
          cy.get('[id="zoneOfInfluenceInput"]').should("exist").should("be.enabled");
          break;
        case "Min. Monin-Obukhov length":
          cy.get('[id="minMoninObukhovLength"]').should("exist").should("be.enabled");
          break;
        case "Surface albedo":
          cy.get('[id="surfaceAlbedo"]').should("exist").should("be.enabled");
          break;
        case "Priestly-Taylor parameter":
          cy.get('[id="priestleyTaylorParameter"]').should("exist").should("be.enabled");
          break;
        case "Plume depletion (NH3)":
          cy.get('[id="plumeDepletionNH3"]').should("exist").should("be.enabled");
          break;
        case "Plume depletion (NOx)":
          cy.get('[id="plumeDepletionNOX"]').should("exist").should("be.enabled");
          break;
        case "Complex terrain":
          cy.get('[id="complexTerrain"]').should("exist").should("be.enabled");
          break;
        case "Automatic receptor points":
          cy.get('[id="roadLocalFractionNO2Receptors"]').should("exist").should("be.enabled");
          break;
        case "Custom assessment points":
          cy.get('[id="roadLocalFractionNO2Points"]').should("exist").should("be.enabled");
          break;
        case "Include projects from archive":
          cy.get('[id="includeArchiveProjects"]').should("exist").should("be.enabled");
          break;
        default:
          throw new Error("Unknown calculation option: " + calculationOption);
      }
    });
  }

  static assertFieldNotVisible(dataTable: any) {
    dataTable.hashes().forEach((calculationOption: any) => {
      switch (calculationOption.option) {
        case "Area":
          cy.get('[id="permitAreaSelection"]').should("not.exist");
          break;
        case "Zone of influence":
          cy.get('[id="zoneOfInfluenceInput"]').should("not.exist");
          break;
        case "Min. Monin-Obukhov length":
          cy.get('[id="minMoninObukhovLength"]').should("not.exist");
          break;
        case "Surface albedo":
          cy.get('[id="surfaceAlbedo"]').should("not.exist");
          break;
        case "Priestly-Taylor parameter":
          cy.get('[id="priestleyTaylorParameter"]').should("not.exist");
          break;
        case "Plume depletion (NH3)":
          cy.get('[id="plumeDepletionNH3"]').should("not.exist");
          break;
        case "Plume depletion (NOx)":
          cy.get('[id="plumeDepletionNOX"]').should("not.exist");
          break;
        case "Complex terrain":
          cy.get('[id="complexTerrain"]').should("not.exist");
          break;
        case "Automatic receptor points":
          cy.get('[id="roadLocalFractionNO2Receptors"]').should("not.exist");
          break;
        case "Custom assessment points":
          cy.get('[id="roadLocalFractionNO2Points"]').should("not.exist");
          break;
        case "Project scenario":
          cy.get('[id="Project scenario"]').should("not.exist");
          break;
        case "Temporary scenario":
          cy.get('[id="TEMPORARY - Temporary"]').should("not.exist");
          break;
        case "Include projects from archive":
          cy.get('[id="includeArchiveProjects"]').should("not.exist");
          break;
        default:
          throw new Error("Unknown calculation option: " + calculationOption.option);
      }
    });
  }

  static assertNoErrorIsShown() {
    cy.get(".collapsible-item.open").find(".noticeContainer.error").should("not.exist");
  }

  static assertProductStyling(productProfile: string) {
    switch (productProfile) {
      case "CHECK":
        cy.get('[id="NAV_ICON-MENU-HOME"]').click();
        cy.get('[id="NAV_ICON-MENU-HOME"]').should("have.css", "background-color", "rgb(209, 99, 38)");
        cy.get('[id="calculationLaunchButtonNewSituation"]').should("have.css", "color", "rgb(209, 99, 38)");
        break;
      case "CALCULATOR":
        cy.get('[id="NAV_ICON-MENU-HOME"]').click();
        cy.get('[id="NAV_ICON-MENU-HOME"]').should("have.css", "background-color", "rgb(21, 121, 160)");
        cy.get('[id="calculationLaunchButtonNewSituation"]').should("have.css", "color", "rgb(21, 121, 160)");
        break;
      default:
        throw new Error("Unknown product profile : " + productProfile);
    }
  }

  static assertInfluencingBuildingIs(influencingBuilding: string) {
    cy.get('[id="sourcelistDetailBuilding"]').should("have.text", influencingBuilding);

    if (influencingBuilding == "Geen") {
      cy.get('[id="buildingDetailTitle"]').should("not.exist");
    } else {
      cy.get('[id="buildingDetailTitle"]').should("have.text", influencingBuilding);
    }
  }

  static assertNoBuildingInfluence() {
    cy.get('[id="sourcelistDetailBuilding"]').should("not.exist");
    cy.get('[id="buildingDetailTitle"]').should("not.exist");
  }

  static selectSubSource(subSourceName: string) {    
    cy.get('[id="subSourceListDescription"]').contains(subSourceName).click({ force: true });
  }

  static toggleHelpWindow() {
    cy.get(".icon-menu-contextual-help").parent().click();
  }

  static validateHelpWindowStatus(status: string) {
    switch (status) {
      case "visible":
        cy.get(".aer-manual");
        cy.get(".manualContainer");
        cy.get(".headerBar");
        cy.get(".manualContentContainer");
        break;
      case "hidden":
        cy.get(".aer-manual").should("not.exist");
        break;
      default:
        throw new Error("Unknown help window status: " + status);
    }
  }

  static pressHelpWindowClose() {
    cy.get(".exit").first().click();
  }

  static validateJobMessage(message: string) {
    cy.get(".aer-left .empty").contains(message);
  }

  static validateMenuItems(dataTable: any) {
    dataTable.hashes().forEach((menuItem: any) => {
      if (menuItem.status == "disabled") {
        cy.get('[id="' + menuItem.menuId + '"]').should("have.class", menuItem.status);
      } else {
        cy.get('[id="' + menuItem.menuId + '"]').should("not.have.class", menuItem.status);
      }
    });
  }

  static assertIfFieldIsEmpty(fieldName: string) {
    cy.get('[id="' + fieldName + '"]').should("be.empty");
  }

  static assertErrorIsShown(errorText: string) {
    cy.get(".noticeContainer.error").should("contain.text", errorText);
  }

  static assertWarningIsShown(warningText: string) {
    cy.get(".noticeContainer.warning").should("contain.text", warningText);
  }

  static assertNoticeIsShown(noticeText: string) {
    cy.get(".noticeContainer.notice").should("contain.text", noticeText);
  }

  static assertCalculationJobIsNamed(nameCalculationJob: string) {
    cy.get(".labelColumn").should("contain.text", nameCalculationJob);
  }

  static assertCalculationJobNotToExist(nameCalculationJob: string) {
    cy.get(".labelColumn").should("not.have.text", nameCalculationJob);
  }

  static assertCalculationJobType(calculationJobType: string) {
    cy.get("#calculation-job-detail-job-type").should("contain.text", calculationJobType);
  }

  static assertCalculationMethod(calculationMethod: string) {
    cy.get("#calculation-job-detail-method").should("contain.text", calculationMethod);
  }

  static assertScenariosOfCalulationJob(scenarioType: string, nameScenario : string) {
    switch (scenarioType) {
      case "PROJECT":
        cy.get('[id="calculation-job-detail-proposed-situation"]').should("have.text", nameScenario);
        return;
      case "REFERENCE":
        cy.get('[id="calculation-job-detail-reference-situation"]').should("have.text", nameScenario);
        return;
      case "OFF_SITE_REDUCTION":
        cy.get('[id="calculation-job-detail-off_site_reduction-situation"]').should("have.text", nameScenario);
        return;
      case "TEMPORARY":
        cy.get('[id="calculation-job-detail-temporary-situation"]').should("have.text", nameScenario);
        return;  
      default:
        throw new Error("Unknown scenarioType: " + scenarioType);
    }
  }

  static selectSourceValidateDefaults(dataTable: any) {
    dataTable.hashes().forEach((source: any) => {
      cy.get('[id="sourceListDetailSectorGroup"]').select(source.sectorGroup);
      if (source.sector != "") {
        cy.get('[id="sourceListDetailSector"]').select(source.sector);
      }
      CommonPage.openCollapsiblePanel("Bronkenmerken");

      cy.get('[id="diurnalVariationType"]').contains(source.diurnalVariationType);
      cy.get('[id="emissionHeight"]').should("have.value", source.emissionHeight);
      cy.get('[id="emissionHeatContent"]').should("have.value", source.emissionHeatContent);
    });
  }

  static validateDefaultSourceCharacteristic(dataTable: any) {
    CommonPage.openCollapsiblePanel("Bronkenmerken");
    dataTable.hashes().forEach((source: any) => {
      switch (source.sourceCharacteristicId) {
        case "buildingInfluence":
          if (source.expectedValue != "CHECKED") {
            cy.get('[id="' + source.sourceCharacteristicId + '"]').should("not.have.class", "icon-checked");
          } else {
            cy.get('[id="' + source.sourceCharacteristicId + '"]').should("have.class", "icon-checked");
          }
          break;
        case "heatContentType":
        case "emissionHeight":
        case "sourceSpread":
        case "emissionHeatContent":
        case "emissionTemperature":
        case "outflowDiameter":
        case "outflowDirectionType":
        case "outflowVelocity":
          cy.get('[id="' + source.sourceCharacteristicId + '"]').should("have.value", source.expectedValue);
          break;
        case "diurnalVariationType":
          cy.get('[id="' + source.sourceCharacteristicId + '"]').contains(source.expectedValue);
          break;
        default:
          throw new Error("Unknown source characteristic: " + source.sourceCharacteristicId);
      }
    });
  }

  static validateCharacteristicsDontExist(dataTable: any) {
    dataTable.hashes().forEach((source: any) => {
      switch (source.sourceCharacteristicId) {
        case "emissionTemperature":
        case "outflowDiameter":
        case "outflowDirectionType":
        case "outflowVelocity":
        case "sourceSpread":
        case "emissionHeatContent":
          cy.get('[id="' + source.sourceCharacteristicId + '"]').should("not.exist");
          break;
        default:
          throw new Error("Unknown source characteristic: " + source.sourceCharacteristicId);
      }
    });
  }

  static assertTimeVaryingProfilesListHasNumberOfprofiles(numberOfProfiles: number) {
    if (numberOfProfiles == 0) {
      cy.get(".emptyProfiles").should("exist");
    } else {
      cy.get(".profiles > div").find("> div").should("have.length", numberOfProfiles);
    }
  }

  static waitForResultsSpinnerToResolve() {
    cy.get(".lds-spinner", { timeout: CommonSettings.resultsLoadingTimeOut }).should("not.exist");
  }

  static closePanel() {
    cy.get(".icon-close").first().click();
  }

  // Preferences
  static assertPreferencesCheckboxSetting(setting: string, value: boolean) {
    switch (setting) {
      case "Advanced importing":
        cy.get('[id="advancedImport"]')
          .parent()
          .find(".checkmark")
          .should(value ? "have.class" : "not.have.class", "icon-checked");
        break;
      case "Automatically turning layers on or off":
        cy.get('[id="layerAutoSwitch"]')
          .parent()
          .find(".checkmark")
          .should(value ? "have.class" : "not.have.class", "icon-checked");
        break;
      case "Automatically switching manual pages":
        cy.get('[id="manualAutoSwitch"]')
          .parent()
          .find(".checkmark")
          .should(value ? "have.class" : "not.have.class", "icon-checked");
        return;
      default:
        throw new Error("Unknown preferences checkbox setting: " + setting);
    }
  }

  static changePreferencesCheckboxSetting(setting: string, value: boolean) {
    switch (setting) {
      case "Advanced importing":
        cy.get('[id="advancedImport"]').click({ force: true });
        break;
      case "Automatically turning layers on or off":
        cy.get('[id="layerAutoSwitch"]').click({ force: true });
        break;
      case "Automatically switching manual pages":
        cy.get('[id="manualAutoSwitch"]').click({ force: true });
        return;
      default:
        throw new Error("Unknown preferences checkbox setting: " + setting);
    }

    this.assertPreferencesCheckboxSetting(setting, value);
  }
}
