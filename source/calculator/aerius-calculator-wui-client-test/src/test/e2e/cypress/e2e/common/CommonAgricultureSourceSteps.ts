/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";

import { CommonAgricultureSourcePage } from "./CommonAgricultureSourcePage";

Given("I create an empty animal housing subsource", () => {
  CommonAgricultureSourcePage.createEmptyAnimalHousingSubsource();
});

Given("I validate that the subsource is empty", () => {
  CommonAgricultureSourcePage.assertSubsourceIsEmpty();
});

Given("I fill in the housing code as {string}", (housingCode) => {
  CommonAgricultureSourcePage.fillInHousingCode(housingCode);
});

Given("I fill in the amount of animals as {string}", (numberOfAnimals) => {
  CommonAgricultureSourcePage.fillInNumberOfAnimals(numberOfAnimals);
});

Given("I validate the animal housing subsource to have the following values", (dataTable) => {
  CommonAgricultureSourcePage.validateSubsourceToHaveValues(dataTable)
});

Given(
  "I add a system with housing code {string} and number of animals {string}",
  (housingCode, numberOfAnimals) => {
    CommonAgricultureSourcePage.addSystemWithHousingCodeNumberOfAnimals(housingCode, numberOfAnimals);
  }
);

Given("I validate that only the following additional rules is available", (dataTable) => {
  CommonAgricultureSourcePage.validateAdditionalHousingIsAvailable(dataTable);
});

Given("I select animal housing system with housing code {string}", (housingCode) => {
  CommonAgricultureSourcePage.selectAnimalHousingSystemWithHousingCode(housingCode);
});

Given("the housing code row count has {string} rows", (count) => {
  CommonAgricultureSourcePage.housingRowCount(count);
});

Given("I delete the animal housing system", () => {
  CommonAgricultureSourcePage.deleteAnimalHousingSystem();
});

Given("the housing code is not visible", () => {
  CommonAgricultureSourcePage.assertHousingCodeIsNotVisible();
});

Given("I copy the animal housing system", () => {
  CommonAgricultureSourcePage.copyAnimalHousingSystem();
});

Given("I add a animal housing system and fill in the custom specification fields with the following data", (dataTable) => {
  CommonAgricultureSourcePage.fillInFieldsAnimalHousingSystemCustomSpec(dataTable);
});

Given("the following animal housing systems with custom specification are created", (dataTable) => {
  CommonAgricultureSourcePage.animalHousingCustomSpecCreated(dataTable);
});

Given("the following animal housing systems with custom specification with days are created", (dataTable) => {
  CommonAgricultureSourcePage.animalHousingCustomSpecDaysCreated(dataTable);
});

Given("the animal housing systems contains the following icons and data in the list", (dataTable) => {
  CommonAgricultureSourcePage.animalHousingSystemContainsFollowingIconAndData(dataTable);
});

Given("I choose animal housing system of type {string}", (typeAnimalHousingSystem) => {
  CommonAgricultureSourcePage.chooseAnimalHousingSystemOfType(typeAnimalHousingSystem);
});

Given(
  "I fill in the fields with description {string}, animal type {string}, factor {string} and number of animals {string}",
  (description, animalType, factor, numberOfAnimals) => {
    CommonAgricultureSourcePage.fillInFieldsSpecificAnimalHousingSystemCustomSpec(description, animalType, factor, numberOfAnimals);
  }
);

Given(
  "the custom specification with description {string}, number of animals {string}, factor {string}, reduction {string} and emission {string} is correctly created",
  (description, numberOfAnimals, factor, reduction, emission) => {
    CommonAgricultureSourcePage.specificAnimalHousingSystemCustomSpec(description, numberOfAnimals, factor, reduction, emission);
  }
);

Given(
  "the animal housing system with custom descripton {string}, number of animals {string}, factor {string}, reduction {string} and emission {string} is correctly created",
  (description, numberOfAnimals, factor, reduction, emission) => {
    CommonAgricultureSourcePage.animalHousingValidateCustomSpec(description, numberOfAnimals, factor, reduction, emission);
  }
);

Given(
  "I validate the subsource with custom specification {string} with number of animals {string} and emission {string}",
  (description, numberOfAnimals, emission) => {
    CommonAgricultureSourcePage.validateSubsourceWithDescriptionNumberOfAnimalsEmission(description, numberOfAnimals, emission);
  }
);

Given("I add an empty additional rule of type {string}", (typeOfAdditionalRule) => {
  CommonAgricultureSourcePage.addAdditionalRule(typeOfAdditionalRule);
});

Given("I add an additional rule with code {string}", (codeAdditionalRule) => {
  CommonAgricultureSourcePage.addAdditionalRuleWithCode(codeAdditionalRule);
});

Given("a animal housing error is visible that the combination is not allowed", () => {
  CommonAgricultureSourcePage.errorMessageCombinationNotAllowed();
});

Given("I delete the additional rule", () => {
  CommonAgricultureSourcePage.deleteAdditionalRule();
});

Given("I add a animal housing system", () => {
  CommonAgricultureSourcePage.addAnimalHousingSystem();
});

Given("I fill the farmland category with {string}, NOX with value {string} and NH3 with value {string}", (category, NOX, NH3) => {
  CommonAgricultureSourcePage.fillInFieldsFarmlandSource(category, NOX, NH3);
});

Given("farmland category {string} with NOX {string} and NH3 {string} is correctly created", (category, NOX, NH3) => {
  CommonAgricultureSourcePage.farmlandSourceCorrectlySaved(category, NOX, NH3);
});

Given("I edit the additional technique to: {string}", (additionalTechnique) => {
  CommonAgricultureSourcePage.editAddTechniqueTo(additionalTechnique);
});

Given("I fill in the explanation with the following description {string}", (explanation) => {
  CommonAgricultureSourcePage.fillInExplanation(explanation);
});
