/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";

import { CommonInApplicationHelpPage } from "./CommonInApplicationHelpPage";

// Press buttons
Given("I press the {string} button", (buttonName) => {
  CommonInApplicationHelpPage.pressButton(buttonName);
});

Given("I open the settings panel", () => {
  CommonInApplicationHelpPage.openSettingsPanel();
});

// Check visibility of elements
Given("the in-application help should show", () => {
  CommonInApplicationHelpPage.assertManualIsShown();
});

Given("the in-application help should not show", () => {
  CommonInApplicationHelpPage.assertManualIsNotShown();
});

Given("a {string} button should be visible", (buttonName: string) => {
  CommonInApplicationHelpPage.assertButtonIsVisible(buttonName);
});

Given("the title {string} should show", (title: string) => {
  CommonInApplicationHelpPage.assertTitleIsShown(title);
});

// Other
Given("the in-application help should have a scrollbar and slider", () => {
  CommonInApplicationHelpPage.assertManualHasScrollbar();
  CommonInApplicationHelpPage.assertManualHasSlider();
});
