/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";

import { AgricultureSourcePage }  from "./AgricultureSourcePage";

Given("I change the number of animals of the additional rule to {string}", (amountOfFarmAnimals) => {
  AgricultureSourcePage.changeAmountOfAnimalsInAdditionalRuleTo(amountOfFarmAnimals);
});

Given("I delete the additional rule", () => {
  AgricultureSourcePage.deleteAdditionalRule();
});

Given("no additional rule should exist in the detail screen", () => {
  AgricultureSourcePage.assertNoAdditionalRuleExists();
});

Given("I fill in reduction of emission with {string}%", (reductionPercentage) => {
  AgricultureSourcePage.fillInReductionPercentage(reductionPercentage);
});

Given("I fill in the explanation with {string}%", (explanationText) => {
  AgricultureSourcePage.fillInExplanation(explanationText);
});

Given("I validate that animal housing constraining footnote is {string}", (visibleState) => {
  AgricultureSourcePage.validateAnimalHousingConstrainingFootnote(visibleState);
});
