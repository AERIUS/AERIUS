/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class AssessmentPointsPage {
  static assertConvertNoticeShown() {
    cy.get('[id="inputErrorMessage"]').should("be.visible");
  }

  static selectConvertedWkt() {
    cy.get('[class="notice"]').find('a[href="#"]').click();
  }

  static assertNewPointScreenShown() {
    cy.get('[id="location-content"]').should("exist");
    cy.get(".confirmContainer").should("exist");
  }

  static selectAssessmentPointCategory(category: string) {
    cy.get('[id="sourceListCategory"]').select(category);
  }

  static enterAssessmentPointRoadHeight(roadHeight: string) {
    cy.get('[id="pointRoadHeight"]').clear().type(roadHeight);
  }

  static enterAssessmentPointPrimaryFraction(primaryFraction: string) {
    cy.get('[id="pointRoadFractionNO2"]').clear().type(primaryFraction);
  }
}
