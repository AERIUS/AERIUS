/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";

import { CalculationJobsPage } from "./CalculationJobsPage";
import { CommonPage } from "../../common/CommonPage";

Given("I expect the following data in the scenario overview", (dataTable) => {
  CalculationJobsPage.assertDataShownInScenarioOverview(dataTable);
});

Given("I expect the following scenarios to be selected", (dataTable) => {
  CalculationJobsPage.assertScenariosAreSelected(dataTable);
});

Given("I expect no scenarios to be selected", () => {
  CalculationJobsPage.assertNoScenariosSelected();
});

Given("I select the situation {string}", (situationName) => {
  CalculationJobsPage.selectSituation("", situationName);
});

Given("I expect the selected {string} situation to be {string}", (situationType, situationName) => {
  CalculationJobsPage.assertSelectedScenarioIs(situationType, situationName);
});

Given("I expect the duplicate calculation job warning to show", () => {
  CalculationJobsPage.assertDuplicateCalculationJobWarningIsShown(true);
});

Given("I expect the duplicate calculation job warning to not show", () => {
  CalculationJobsPage.assertDuplicateCalculationJobWarningIsShown(false);
});

Given("I expect the calculation job {string} to not exist", (calculationJob) => {
  CalculationJobsPage.assertCalculationDoesNotExist(calculationJob);
});

Given("I change the calculation job name to {string}", (name) => {
  CalculationJobsPage.changeCalculationJobNameTo(name);
});

Given("I expect a calculation job with title {string} to exist", (title) => {
  CalculationJobsPage.assertCalculationJobWithTitleExists(title);
});

Given("the calculation wasn't saved", () => {
  CalculationJobsPage.assertCalculationNotSaved();
});

Given("I see a warning that the calculation differs from the current selection", () => {
  let expectedWarningText =
    "De resultaten voor deze berekening zijn verlopen doordat de invoer is aangepast sinds de laatste doorrekening; de resultaten blijven inzichtelijk maar gelden niet langer voor deze rekentaak.";
  CommonPage.assertWarningIsShown(expectedWarningText);
});

Given("the selected base calculation job is {string}", (expectedBaseCalculationJob) => {
  CalculationJobsPage.assertSelectedBaseCalculationJobIs(expectedBaseCalculationJob);
});

Given("I select the base calculation job {string}", (calculationJob) => {
  CalculationJobsPage.setBaseCalculationJob(calculationJob);
});

Given("the selected {string} situation is {string}", (situationType, situationName) => {
  CalculationJobsPage.assertSelectedSituationIs(situationType, situationName);
});

Given("the following situations are selected", (dataTable) => {
  CalculationJobsPage.assertSituationsAreSelected(dataTable);
});

Given("the error container is shown", () => {
  CalculationJobsPage.assertErrorContainerIsShown();
});

Given("situation {string} should not exist", (situationType) => {
  CalculationJobsPage.assertSituationDoesNotExist(situationType);
});
