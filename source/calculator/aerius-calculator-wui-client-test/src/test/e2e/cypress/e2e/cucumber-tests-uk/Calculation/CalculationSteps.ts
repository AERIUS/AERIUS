/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";

import { CalculationPage } from "./CalculationPage";

Given("I select the uk calculation option {string}", (option) => {
  CalculationPage.setCalculationOptionsTo(option);
});

Given("I expect the uk calculation options", (dataTable) => {
  CalculationPage.assertElements(dataTable);
});

Given("I expect the uk calculation option element {string} to be {string}", (element, state) => {
  CalculationPage.assertElement(element, state);
});

Given("I select fNO2 option I expect element roadLocalFractionNO2 to have a specific state", (dataTable) => {
  CalculationPage.selectFNO2Option(dataTable);
});
