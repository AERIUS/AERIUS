#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Mobile equipment source

    Scenario: Create new mobile equipment source - Agriculture
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Mobiele werktuigen' and sector 'Landbouw'
        And I fill location with coordinates 'POLYGON((108207.43 521706.71,108207.87 521660.31,108248.14 521663.37,108243.33 521711.53,108207.43 521706.71))'
        And I fill the mobile equipment description 'Mobile equipment' with stage class 'Stage-V, >= 2019 , >= 560  kW, diesel, SCR: ja', fuel usage '10000', operation hours per year '250' and adblue usage '401'
        Then blue ratio warning is visible
        When I save the data validate the warning and wait for refresh
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Mobiele werktuigen' and sector 'Landbouw' is correctly saved
        And location with coordinates 'X:108226,76 Y:521685,92' is correctly saved
        And the following mobile equipment source is correctly saved    
        | description        | stageClass                                     | fuelUsage  | operationHours | adBlue  | emissionNOx  | emissionNH3 |
        | Mobile equipment   | Stage-V, >= 2019 , >= 560  kW, diesel, SCR: ja | 10.000 l/j | 250 u/j        | 401 l/j | 67,3 kg/j    | 2,4 kg/j    |
        And the total 'mobile equipment' emission 'NOx' is '67,3 kg/j'
        And the total 'mobile equipment' emission 'NH3' is '2,4 kg/j'

    Scenario: Create new mobile equipment source - Construction, Industry and Mining
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Mobiele werktuigen' and sector 'Bouw, Industrie en Delfstoffenwinning'
        And I fill location with coordinates 'POLYGON((108207.43 521706.71,108207.87 521660.31,108248.14 521663.37,108243.33 521711.53,108207.43 521706.71))'
        And I fill the mobile equipment description 'Mobile equipment' with stage class 'alle werktuigen op benzine, 2takt', fuel usage '101', operation hours per year '' and adblue usage ''
        Then blue ratio warning is not visible
        When I save the data and wait for refresh
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Mobiele werktuigen' and sector 'Bouw, Industrie en Delfstoffenwinning' is correctly saved
        And location with coordinates 'X:108226,76 Y:521685,92' is correctly saved
        And the following mobile equipment source is correctly saved    
        | description        | stageClass                                     | fuelUsage  | operationHours | adBlue | emissionNOx | emissionNH3 |
        | Mobile equipment   | alle werktuigen op benzine, 2takt              | 101 l/j    | 0 u/j          | 0 l/j  | 0,4 kg/j    | 0,0 kg/j    |
        And the total 'mobile equipment' emission 'NOx' is '0,4 kg/j'
        And the total 'mobile equipment' emission 'NH3' is '0,0 kg/j'

    Scenario: Create new mobile equipment source - Consumer mobile equipment
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Mobiele werktuigen' and sector 'Consumenten mobiele werktuigen'
        And I fill location with coordinates 'POLYGON((108207.43 521706.71,108207.87 521660.31,108248.14 521663.37,108243.33 521711.53,108207.43 521706.71))'
        And I fill the mobile equipment description 'Mobile equipment' with stage class 'Stage-II, 2002-2005, >= 560  kW, diesel, SCR: nee', fuel usage '10000', operation hours per year '250' and adblue usage ''
        Then blue ratio warning is not visible
        When I save the data and wait for refresh
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Mobiele werktuigen' and sector 'Consumenten mobiele werktuigen' is correctly saved
        And location with coordinates 'X:108226,76 Y:521685,92' is correctly saved
        And the following mobile equipment source is correctly saved    
        | description        | stageClass                                        | fuelUsage  | operationHours | adBlue | emissionNOx | emissionNH3 |
        | Mobile equipment   | Stage-II, 2002-2005, >= 560  kW, diesel, SCR: nee | 10.000 l/j | 250 u/j        | 0 l/j  | 301,3 kg/j  | 75,0 g/j    |
        And the total 'mobile equipment' emission 'NOx' is '301,3 kg/j'
        And the total 'mobile equipment' emission 'NH3' is '75,0 g/j'

    Scenario: Create new mobile equipment source - Consumer mobile equipment, heavy duty sources
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Mobiele werktuigen' and sector 'Consumenten mobiele werktuigen'
        And I fill location with coordinates 'POLYGON((108207.43 521706.71,108207.87 521660.31,108248.14 521663.37,108243.33 521711.53,108207.43 521706.71))'
        And I fill the mobile equipment description 'Mobile equipment 1' with stage class 'Zware utiliteitsvoertuigen (meer dan 6L cilinderinhoud) op diesel', fuel usage '', operation hours per year '2500' and adblue usage ''
        When I save the data and wait for refresh
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Mobiele werktuigen' and sector 'Consumenten mobiele werktuigen' is correctly saved
        And location with coordinates 'X:108226,76 Y:521685,92' is correctly saved
        And the following mobile equipment source is correctly saved        
        | description        | stageClass                                                        | fuelUsage  | operationHours | adBlue | emissionNOx  | emissionNH3 |
        | Mobile equipment 1 | Zware utiliteitsvoertuigen (meer dan 6L cilinderinhoud) op diesel | 0 l/j      | 2.500 u/j      | 0 l/j  | 500,0 kg/j   | 3,7 kg/j    |
        And the total 'mobile equipment' emission 'NOx' is '500,0 kg/j'
        And the total 'mobile equipment' emission 'NH3' is '3,7 kg/j'
        # add multiple subsources, total of emissions will increase
        When I choose to edit the source
        And I fill the mobile equipment description 'Mobile equipment 2' with stage class 'Zware utiliteitsvoertuigen (meer dan 6L cilinderinhoud) op diesel', fuel usage '', operation hours per year '6000' and adblue usage ''
        When I save the data and wait for refresh
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Mobiele werktuigen' and sector 'Consumenten mobiele werktuigen' is correctly saved
        And location with coordinates 'X:108226,76 Y:521685,92' is correctly saved
        And the following mobile equipment source is correctly saved
        | description        | stageClass                                                        | fuelUsage  | operationHours | adBlue | emissionNOx  | emissionNH3 |
        | Mobile equipment 1 | Zware utiliteitsvoertuigen (meer dan 6L cilinderinhoud) op diesel | 0 l/j      | 2.500 u/j      | 0 l/j  | 500,0 kg/j   | 3,7 kg/j    |
        | Mobile equipment 2 | Zware utiliteitsvoertuigen (meer dan 6L cilinderinhoud) op diesel | 0 l/j      | 6.000 u/j      | 0 l/j  | 1.200,0 kg/j | 8,8 kg/j    |
        # total emissions
        And the total 'mobile equipment' emission 'NOx' is '1.700,0 kg/j'
        And the total 'mobile equipment' emission 'NH3' is '12,5 kg/j'

    @Smoke
    Scenario: Variable fields depending on stage class
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Mobiele werktuigen' and sector 'Landbouw'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        When I choose to add the new stage class 'alle werktuigen op LPG'
        Then field 'Brandstofverbruik' is visible
        Then field 'Draaiuren' is not visible
        Then field 'AdBlue verbruik' is not visible
        When I choose to add the new stage class 'Middelzware utiliteitsvoertuigen (tot 6L cilinderinhoud) op diesel'
        Then field 'Brandstofverbruik' is not visible
        Then field 'Draaiuren' is visible
        Then field 'AdBlue verbruik' is not visible
        When I choose to add the new stage class 'Stage-II, 2002-2005, >= 560  kW, diesel, SCR: nee'
        Then field 'Brandstofverbruik' is visible
        Then field 'Draaiuren' is visible
        Then field 'AdBlue verbruik' is not visible
        When I choose to add the new stage class 'Stage-IIIB, 2011-2013, 75-560 kW, diesel, SCR: ja'
        Then field 'Brandstofverbruik' is visible
        Then field 'Draaiuren' is visible
        Then field 'AdBlue verbruik' is visible

    Scenario: Edit variable fields depending on stage class
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Mobiele werktuigen' and sector 'Landbouw'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'

        # fuel usage, operation hours, adBlue visible
        When I fill the mobile equipment description 'Mobile equipment' with stage class 'Stage-IIIB, 2011-2013, 75-560 kW, diesel, SCR: ja', fuel usage '10000', operation hours per year '250' and adblue usage '12'
        And I save the data and wait for refresh
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Mobiele werktuigen' and sector 'Landbouw' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And the following mobile equipment source is correctly saved    
        | description        | stageClass                                        | fuelUsage  | operationHours | adBlue | emissionNOx | emissionNH3 |
        | Mobile equipment   | Stage-IIIB, 2011-2013, 75-560 kW, diesel, SCR: ja | 10.000 l/j | 250 u/j        | 12 l/j | 245,7 kg/j  | 2,4 kg/j    |        
        And the total 'mobile equipment' emission 'NOx' is '245,7 kg/j'
        And the total 'mobile equipment' emission 'NH3' is '2,4 kg/j'

        # fuel usage, operation hours visible, adBlue not visible
        When I choose to edit the source
        And I select stage class 'Mobile equipment'
        And I edit the mobile equipment description 'Mobile equipment' with stage class 'Stage-II, 2002-2005, >= 560  kW, diesel, SCR: nee', fuel usage '20000', operation hours per year '350' and adblue usage ''
        And I save the data and wait for refresh
        Then the source 'Bron 1' with sector group 'Mobiele werktuigen' and sector 'Landbouw' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And the following mobile equipment source is correctly saved    
        | description        | stageClass                                        | fuelUsage  | operationHours | adBlue | emissionNOx | emissionNH3 |
        | Mobile equipment   | Stage-II, 2002-2005, >= 560  kW, diesel, SCR: nee | 20.000 l/j | 350 u/j        | 0 l/j  | 601,8 kg/j  | 0,2 kg/j    |
        And the total 'mobile equipment' emission 'NOx' is '601,8 kg/j'
        And the total 'mobile equipment' emission 'NH3' is '0,2 kg/j'

        # operation hours visible, fuel usage and adBlue not visible
        And I close source detail screen
        When I select the source 'Bron 1'
        And I choose to edit the source
        When I select stage class 'Mobile equipment'
        And I edit the mobile equipment description 'Mobile equipment' with stage class 'Middelzware utiliteitsvoertuigen (tot 6L cilinderinhoud) op diesel', fuel usage '', operation hours per year '450' and adblue usage ''
        And I save the data and wait for refresh
        Then the source 'Bron 1' with sector group 'Mobiele werktuigen' and sector 'Landbouw' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved

        And the following mobile equipment source is correctly saved    
        | description        | stageClass                                                         | fuelUsage | operationHours | adBlue | emissionNOx | emissionNH3 |
        | Mobile equipment   | Middelzware utiliteitsvoertuigen (tot 6L cilinderinhoud) op diesel | 0 l/j     | 450 u/j        | 0 l/j  | 54,0 kg/j   | 0,4 kg/j    |        
        And the total 'mobile equipment' emission 'NOx' is '54,0 kg/j'
        And the total 'mobile equipment' emission 'NH3' is '0,4 kg/j'

        # fuel usage visible, operation hours and adBlue not visible
        And I close source detail screen
        When I select the source 'Bron 1'
        And I choose to edit the source
        When I select stage class 'Mobile equipment'
        And I edit the mobile equipment description 'Mobile equipment' with stage class 'alle werktuigen op LPG', fuel usage '30000', operation hours per year '' and adblue usage ''
        And I save the data and wait for refresh
        Then the source 'Bron 1' with sector group 'Mobiele werktuigen' and sector 'Landbouw' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And the following mobile equipment source is correctly saved    
        | description        | stageClass             | fuelUsage  | operationHours | adBlue | emissionNOx | emissionNH3 |
        | Mobile equipment   | alle werktuigen op LPG | 30.000 l/j | 0 u/j          | 0 l/j  | 120,0 kg/j  | 0,2 kg/j    |
        And the total 'mobile equipment' emission 'NOx' is '120,0 kg/j'
        And the total 'mobile equipment' emission 'NH3' is '0,2 kg/j'
