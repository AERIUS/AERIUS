/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class CalculationJobsPage {
  static assertCalculationDistanceValueIsSet() {
    cy.get('[id="zoneOfInfluenceInput"]').should("not.have.value", "");
  }

  static assertMinMoninObukhovLengthValueIsSet() {
    cy.get('[id="minMoninObukhovLength"]').should("not.have.value", "");
  }

  static assertSurfaceAlbedoValueIsSet() {
    cy.get('[id="surfaceAlbedo"]').should("not.have.value", "");
  }

  static assertPriestlyTaylorParameterValueIsSet() {
    cy.get('[id="priestleyTaylorParameter"]').should("not.have.value", "");
  }

  static assertZoneOfInfluenceErrorIsShown(shouldBeShown: boolean) {
    if (shouldBeShown) {
      cy.get('[id="zoneOfInfluenceInput_error"]').should("exist");
    } else {
      cy.get('[id="zoneOfInfluenceInput_error"]').should("not.exist");
    }
  }

  static assertMinMoninObukhovLengthErrorIsShown(shouldBeShown: boolean) {
    if (shouldBeShown) {
      cy.get('[id="minMoninObukhovLength_error"]').should("exist");
    } else {
      cy.get('[id="minMoninObukhovLength_error"]').should("not.exist");
    }
  }

  static assertSurfaceAlbedoErrorIsShown(shouldBeShown: boolean) {
    if (shouldBeShown) {
      cy.get('[id="surfaceAlbedo_error"]').should("exist");
    } else {
      cy.get('[id="surfaceAlbedo_error"]').should("not.exist");
    }
  }

  static assertPriestleyTaylorParameterErrorIsShown(shouldBeShown: boolean) {
    if (shouldBeShown) {
      cy.get('[id="priestleyTaylorParameter_error"]').should("exist");
    } else {
      cy.get('[id="priestleyTaylorParameter_error"]').should("not.exist");
    }
  }
}
