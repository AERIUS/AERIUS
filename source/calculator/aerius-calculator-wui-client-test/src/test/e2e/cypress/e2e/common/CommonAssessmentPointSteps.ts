/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";

import { CommonAssessmentPointsPage }  from "./CommonAssessmentPointsPage";

Given("I create the first assessment point", () => {
  CommonAssessmentPointsPage.createFirstNewPoint();
});

Given("I create a new assessment point", () => {
  CommonAssessmentPointsPage.createNewPoint();
});

Given("I enter the name {string}", (name) => {
  CommonAssessmentPointsPage.enterName(name);
});

Given("I enter the coordinates {string}", (coordinates) => {
  CommonAssessmentPointsPage.enterCoordinates(coordinates);
});

Given("the entered coordinates should be {string}", (coordinates) => {
  CommonAssessmentPointsPage.assertCoordinatesAre(coordinates);
});

Given("I save the assessment point", () => {
  CommonAssessmentPointsPage.savePoint();
});

Given("the overview list has {int} assessment points", (numberOfAssessmentPoints: number) => {
  CommonAssessmentPointsPage.assertNumberOfAssessmentPoints(numberOfAssessmentPoints);
});

Given("the assessment point {string} exists", (name) => {
  CommonAssessmentPointsPage.assertPointExists(name);
});

Given("the following assessment points exists in the following order", (dataTable) => {
  CommonAssessmentPointsPage.assertAssessmentPoints(dataTable);
});

Given("the assessment point {string} does not exist", (name) => {
  CommonAssessmentPointsPage.assertPointDoesNotExists(name);
});

Given("I select the assessment point {string}", (name) => {
  CommonAssessmentPointsPage.selectPoint(name);
});

Given("I select the following assessment points using multiselect", (dataTable) => {
  CommonAssessmentPointsPage.toggleMultiplePoints(dataTable);
});

Given("I deselect the following assessment points using multiselect", (dataTable) => {
  CommonAssessmentPointsPage.toggleMultiplePoints(dataTable);
});

Given("I check if the following assessment points are selected", (dataTable) => {
  CommonAssessmentPointsPage.assertCalculationPointsAreSelected(dataTable, true);
});

Given("I check if the following assessment points are not selected", (dataTable) => {
  CommonAssessmentPointsPage.assertCalculationPointsAreSelected(dataTable, false);
});

Given("I update this assessment point", () => {
  CommonAssessmentPointsPage.updatePoint();
});

Given("I delete all assessment points", () => {
  CommonAssessmentPointsPage.deleteAllPoints();
});

Given("I delete this assessment point", () => {
  CommonAssessmentPointsPage.deleteSelectedPoint();
});

Given("an error should be shown", () => {
  CommonAssessmentPointsPage.assertErrorExists();
});

Given("I enter an empty name", () => {
  CommonAssessmentPointsPage.enterEmptyName();
});

Given("calculation point {string} is correctly saved", (nameCalculationPoint) => {
  CommonAssessmentPointsPage.calculationPointCorrectlySaved(nameCalculationPoint);
});

// Characteristics
Given("I open assessment point characteristics", () => {
  CommonAssessmentPointsPage.openAssessmentPointCharacteristics();
});

Given(
  "assessment point category {string}, road height {string} and custom primary fraction {string} are correctly saved",
  (category, roadHeight, primaryFraction) => {
    CommonAssessmentPointsPage.assertAssessmentPointCategoryRoadHeightCustomPrimaryFraction(category, roadHeight, primaryFraction);
  }
);

// Habitats
Given("I open assessment point habitats", () => {
  CommonAssessmentPointsPage.openAssessmentPointHabitats();
});


