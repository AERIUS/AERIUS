/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";

import { CommonCalculationResultsPage } from "./CommonCalculationResultsPage";
import { CommonPage } from "./CommonPage";

Given("I duplicate the situation", () => {
  CommonPage.duplicateSituation();
});

Given("I intercept the status of the calculation", () => {
  CommonCalculationResultsPage.interceptTheStatusOfTheCalculation();
});

Given("I select temporary results", () => {
  CommonCalculationResultsPage.selectTemporaryResults();
});

Given("I validate that the situation {string} has results combinations", (situationName, dataTable) => {
  CommonCalculationResultsPage.navigateResultScreenCombinatations(situationName, dataTable);
});

Given("I validate that for situation {string} the resultsType {string} has {string} options", (situationName, resultType, rowCount) => {
  CommonCalculationResultsPage.validateResultScreenCombinationsCount(situationName, resultType, rowCount);
});

Given("I intercept the response from {string} with {string}", (urlPath, jsonFile, dataTable) => {
  CommonCalculationResultsPage.interceptResult(urlPath, jsonFile, dataTable);
});

Given("I validate procurement policy threshold for the results {string}", (productProfile, dataTable) => {
  CommonCalculationResultsPage.validateProcurementPolicyThreshold(productProfile, dataTable);
});

Given(
  "I select situation with label {string} and validate the {string} result screen with mock results for the result {string}",
  (situationName, calculationType, resultType, dataTable) => {
    CommonCalculationResultsPage.validateResultScreen(situationName, calculationType, resultType, dataTable);
  }
);

Given(
  "I select situation with label {string} and validate the {string} result screen with results for the result {string}",
  (situationName, calculationType, resultType, dataTable) => {
    CommonCalculationResultsPage.validateResultScreen(situationName, calculationType, resultType, dataTable);
  }
);

Given("I check if the summary tab has {int} scenario contributions results", (count) => {
  CommonCalculationResultsPage.validateSummaryScenarioContributionCount(count);
});

Given("I check if the summary tab has {int} process contributions results", (count) => {
  CommonCalculationResultsPage.validateSummaryProcessContributionsCount(count);
});

Given("I validate process contributions result table values", (dataTable) => {
  CommonCalculationResultsPage.validateSummaryProcessContributionTableValues(dataTable);
});

Given("I validate markers table values", (dataTable) => {
  CommonCalculationResultsPage.validateMarkersProcessContributionTableValues(dataTable);
});

Given("I validate the habitat and species table values", (dataTable) => {
  CommonCalculationResultsPage.validateHabitatAndSpeciesProcessContributionTableValues(dataTable);
});

Given("I select calculation results tab {string}", (tabName) => {
  CommonCalculationResultsPage.validateCalculationResultsTab(tabName);
});

Given("I validate that Archive project results are retrieved on {string}", (retrievedDateTime) => {
  CommonCalculationResultsPage.validateArchiveProjectResultRetrieveDateTime(retrievedDateTime);
});

Given("I validate that Archive project link contains project ids", (dataTable) => {
  CommonCalculationResultsPage.validateArchiveHyperlinkContents(dataTable);
});

Given("I select calculation results tab {string} and validate", (tabName, dataTable) => {
  CommonCalculationResultsPage.validateCalculationResultsTabWithContent(tabName, dataTable);
});

Given("I select calculation results tab {string} and validate with element index", (tabName, dataTable) => {
  CommonCalculationResultsPage.validateCalculationResultsTabWithContent(tabName, dataTable, true);
});

Given(
  "I select situation with label {string} and validate the {string} with Pollutant {string} and results for {string}",
  (situationName, calculationType, pollutant, resultType, dataTable) => {
    CommonCalculationResultsPage.validateSituationNameCalculationTypePollutantResultTypeScreen(
      situationName,
      calculationType,
      pollutant,
      resultType,
      dataTable
    );
  }
);

Given("I validate the calculation point results", (dataTable) => {
  CommonCalculationResultsPage.validateCalculationPointsResult(dataTable);
});

Given("I check if the critical levels tab has {int} results", (numberOfResults) => {
  CommonCalculationResultsPage.validateTab("tab-PERCENTAGE_CRITICAL_LEVELS", numberOfResults);
});

Given("I select site {string} and validate message {string}", (siteName, expectedtext) => {
  CommonCalculationResultsPage.validateSiteExpectedText(siteName, expectedtext);
});

Given("I select site {string} and validate no message exist", (siteName) => {
  CommonCalculationResultsPage.validateSiteExpectedTextNoText(siteName);
});

Given("I check if the critical levels tab has no results and the text {string}", (expectedtext) => {
  CommonCalculationResultsPage.validateCriticalLevelsTab("tab-PERCENTAGE_CRITICAL_LEVELS", expectedtext);
});

Given("I check if the deposition tab has {int} results", (numberOfResults) => {
  CommonCalculationResultsPage.validateTab("tab-DEPOSITION_DISTRIBUTION", numberOfResults);
});

Given("I check if the markers tab has {int} results and the text {string}", (numberOfResults, expectedtext) => {
  CommonCalculationResultsPage.validateMarkersTab(numberOfResults, expectedtext);
});

Given("I check if the habitat tab has {int} results", (numberOfResults) => {
  CommonCalculationResultsPage.validateTab("tab-HABITAT_TYPES", numberOfResults);
});

Given("I check if the extra assessment habitat tab has {int} results", (numberOfResults) => {
  CommonCalculationResultsPage.validateTab("tab-EXTRA_ASSESSMENT_HABITAT_TYPES", numberOfResults);
});

Given("I check if omitted areas contains {int} results", (numberOfResults) => {
  CommonCalculationResultsPage.validateOmittedAreas(numberOfResults);
});

Given("I check the natural areas for the result for {string} and amount {int}", (area, numberOfAreas) => {
  CommonCalculationResultsPage.validateTab(area + "-title", numberOfAreas);
});

Given("I check if {string} has {int} hexagons with the following characteristics", (area, numberOfAreas, dataTable) => {
  CommonCalculationResultsPage.validateHexagons(area, numberOfAreas, dataTable);
});

Given("I check if the calculation points tab has {int} results", (numberOfResults) => {
  CommonCalculationResultsPage.validateCalculationPoints(numberOfResults);
});

// UK
Given("I select the meteorological site location {string}", (location) => {
  CommonCalculationResultsPage.selectMeteorologicalSiteLocation(location);
});

Given("I search meteorological site location {string}", (location) => {
  CommonCalculationResultsPage.searchMeteorologicalSiteLocation(location);
});

Given("the met site location shows {string} options", (optionsCount) => {
  CommonCalculationResultsPage.validateMeteorologicalSiteLocationCount(optionsCount);
});

Given("I select the first meteorological site location", () => {
  CommonCalculationResultsPage.selectFirstMeteorologicalSiteLocation();
});

Given("I select the meteorological year {string}", (year) => {
  CommonCalculationResultsPage.selectMeteorologicalYear(year);
});

Given("the meteorological year is not selectable", () => {
  CommonCalculationResultsPage.validateMeteorologicalYearNotSelectable();
});

Given("I start an UK calculation mock the finish and validate the result screen", () => {
  CommonCalculationResultsPage.startUKCalculationMockFinishAndValidateResultScreen();
});

Given("I start an UK calculation and validate the result screen", () => {
  CommonCalculationResultsPage.startUKCalculationAndValidateResultScreen();
});

Given("I select included calculation scenario {string}", (scenario) => {
  CommonCalculationResultsPage.selectCalulationScenario(scenario);
});

Given("I select In-Combination include project from archive", () => {
  CommonCalculationResultsPage.selectInCombinationIncludeArchive();
});

Given("I select In-Combination reference scenario {string}", (scenario) => {
  CommonCalculationResultsPage.selectInCombinationScenario(scenario);
});

Given("I select In-Combination project scenario {string}", (scenario) => {
  CommonCalculationResultsPage.selectInCombinationScenario(scenario);
});

Given("I delete the opened calculation job", () => {
  CommonCalculationResultsPage.deleteCurrentCalculationJob();
});

Given("I validate nature area chart output for selected nature area with pollutant {string}", (pollutant, dataTable) => {
  CommonCalculationResultsPage.selectNatureAreaValidateMap(dataTable, pollutant);
});

Given("I open map layer and validate total percentage cl", (dataTable) => {
  CommonCalculationResultsPage.selectNatureAreaValidateTotalPercentageCLMap(dataTable);
});

Given("I open map layer and validate percentage cl", (dataTable) => {
  CommonCalculationResultsPage.selectNatureAreaValidatePercentageCLMap(dataTable);
});

Given("I validate archive project sites on the map", () => {
  CommonCalculationResultsPage.validateArchiveProjectSitesOnMap();
});

Given("I validate decision-making thresholds info message {string}", (message) => {
  CommonCalculationResultsPage.validateDecisionMakingThresholdsinfoMessage(message);
});

Given("I validate site-relevant thresholds info message {string}", (message) => {
  CommonCalculationResultsPage.validateSiteRelevantThresholdsinfoMessage(message);
});

Given("I validate decision-making thresholds values", (dataTable) => {
  CommonCalculationResultsPage.validateDecisionMakingThresholds(dataTable);
});

Given("I validate site-relevant thresholds values for site {string} site id {string}", (siteName, siteId, dataTable) => {
  CommonCalculationResultsPage.validateSiteRelevantThresholds(siteName, siteId, dataTable);
});
