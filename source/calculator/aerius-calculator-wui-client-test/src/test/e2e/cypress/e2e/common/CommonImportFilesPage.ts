/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class CommonImportFilesPage {
  static assertDownloadWarningsAndErrorsButtonIsNotShown() {
    cy.get(".downloadLog").should("not.exist");
  }

  static assertDownloadWarningsAndErrorsButtonIsShown() {
    cy.get(".downloadLog").should("exist");
  }

  static assertAdvancedImportFileDetailContains(scenarioName: string, fileName: string, expectedString: string) {
    cy.get(".fileContainer .situationName")
      .filter((index, element) => {
        return Cypress.$(element).text() === scenarioName && Cypress.$(element).parent().find(".name").text() === fileName;
      })
      .parent()
      .parent()
      .parent()
      .find(".statsContainer")
      .should("contain.text", expectedString);
  }

  static toggleAdvancedImportFileDetailLine(scenarioName: string, fileName: string, lineTitle: string) {
    cy.get(".fileContainer .situationName")
      .filter((index, element) => {
        return Cypress.$(element).text() === scenarioName;
      })
      .parent()
      .parent()
      .parent()
      .find(".statsContainer")
      .contains(lineTitle)
      .click();
  }

  static validateAdvancedImportFileDetailLine(dataTable: any) {
    dataTable.hashes().forEach((advancedLine: any) => {
      let validateStyle = '';
      let validateClass = '';
      switch (advancedLine.checkBoxStatus) {
        case "checked":
          validateStyle = 'icon-checked';
          validateClass = 'have.class';
          break;
        case "unchecked":
          validateStyle = "icon-checked";
          validateClass = 'not.have.class';
          break;
        case "disabled":
          validateStyle = "icon-disabled-unchecked";
          validateClass = 'have.class';
          break;
        default:
          throw new Error("unknown checkbox status: " + advancedLine.status);
      }

      cy.get(".fileContainer .situationName")
      .filter((index, element) => {
        return Cypress.$(element).text() === advancedLine.scenario;
      })
      .parent()
      .parent()
      .parent()
      .find(".statsContainer")
      .contains(advancedLine.lineTitle)
      .within(() => {
        cy.get('.checkmark').should(validateClass, validateStyle);
      });
    });
  }

  static assertAdvancedImportFileDetailHeaderIs(scenarioName: string, fileName: string, expectedString: string) {
    cy.get(".fileContainer .situationName")
      .filter((index, element) => {
        return Cypress.$(element).text() === scenarioName && Cypress.$(element).parent().find(".name").text() === fileName;
      })
      .parent()
      .parent()
      .parent()
      .find(".statsContainer h3")
      .should("have.text", expectedString);
  }
  
  static expandAdvancedLineFor(scenarioName: string, fileName: string) {
    cy.get(".fileContainer .situationName")
      .filter((index, element) => {
        return Cypress.$(element).text() === scenarioName;
      })
      .parent()
      .find(".name")
      .contains(fileName)
      .click();
  }

  static assertNumberOfScenarioToBeImported(expectedNrOfScenarios: string) {
    cy.get(".listContainer").find("> div").should("have.length", expectedNrOfScenarios);
  }
}
