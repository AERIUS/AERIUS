#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: AERIUS Check, genericUI
    Test generic ui behavour for product Check.

    @Smoke
    Scenario: Check correct styling for product.
        Given I login with correct username and password
        Then the current styling is conform product 'CHECK'

    @Smoke
    Scenario: Check link to help page
        Given I login with correct username and password
        Then I toggle the help window
        And I validate help info is 'visible'
        Then I toggle the help window
        And I validate help info is 'hidden'
        Then I toggle the help window
        And I validate help info is 'visible'
        Then I close help window with close button
        And I validate help info is 'hidden'

    @Smoke
    Scenario: Check status of menu items
        Given I login with correct username and password
        Then I validate the status of the menu items
        | menuId                             | status   |
        | NAV_ICON-MENU-INPUT                | disabled |
        | NAV_ICON-MENU-ASSESSMENT-POINTS    | disabled |
        | NAV_ICON-MENU-CALCULATE            | disabled |
        | NAV_ICON-MENU-RESULTS              | disabled |
        | NAV_ICON-MENU-EXPORT               | disabled |
        | NAV_ICON-MENU-CONTEXTUAL-HELP      | enabled  |
        | NAV_ICON-MENU-SETTINGS             | enabled  |
        | NAV_ICON-MENU-HOTKEYS              | enabled  |
        | NAV_ICON-MENU-MANUAL               | enabled  |
        | NAV_ICON-MENU-LANGUAGE             | enabled  |
        And I create a new source from the start page
        Then I validate the status of the menu items
        | menuId                             | status   |
        | NAV_ICON-MENU-INPUT                | enabled  |
        | NAV_ICON-MENU-ASSESSMENT-POINTS    | disabled |
        | NAV_ICON-MENU-CALCULATE            | enabled  |
        | NAV_ICON-MENU-RESULTS              | disabled |
        | NAV_ICON-MENU-EXPORT               | enabled  |
        | NAV_ICON-MENU-CONTEXTUAL-HELP      | enabled  |
        | NAV_ICON-MENU-SETTINGS             | enabled  |
        | NAV_ICON-MENU-HOTKEYS              | enabled  |
        | NAV_ICON-MENU-MANUAL               | enabled  |
        | NAV_ICON-MENU-LANGUAGE             | enabled  |
