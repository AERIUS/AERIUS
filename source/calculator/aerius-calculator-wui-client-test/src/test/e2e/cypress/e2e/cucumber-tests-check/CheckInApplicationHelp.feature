#
# Crown copyright
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: AERIUS Check, In-application help
    Test everything to do with in-application help

    Scenario: Test opening/closing in-application help
        Given I login with correct username and password
        Then the in-application help should not show

        When I press the 'manual' button
        Then the in-application help should show

        When I press the 'manual' button
        Then the in-application help should not show

        When I press the 'manual' button
        And I press the 'close' button
        Then the in-application help should not show

    Scenario: Test general usage of in-application help
        # Open application with in-application help and verify the availability of buttons
        Given I login with correct username and password
        When I press the 'manual' button
        Then a 'Inhoudsopgave' button should be visible
        And a 'close' button should be visible
        And the in-application help should have a scrollbar and slider

        # Test table of contents
        When I press the 'Inhoudsopgave' button
        Then the title 'Inhoudsopgave' should show
    
    Scenario: Check default pages for every page in the application
        Given I login with correct username and password

        # Start
        When I press the 'manual' button
        Then the title 'Start' should show

        # Invoer
        When I create an empty scenario from the start page
        Then the title 'Invoer' should show

        # Rekentaken
        When I navigate to the 'Calculation jobs' tab
        Then the title 'Rekentaken' should show

        # Exporteren 
        When I navigate to the 'Export' tab
        Then the title 'Exporteren' should show

        # Vervolgstappen
        When I navigate to the 'Next steps' tab
        Then the title 'Vervolgstappen' should show

        # Home (2)
        When I navigate to the 'Home' tab
        Then the title 'Start' should show

        # Resultaten (close the manual button for a bit to save screen estate)
        When I press the 'manual' button
        And I select file 'Check_small_source.gml' to be imported
        And I navigate to the 'Calculation jobs' tab
        And I start the calculation

        Then I navigate to the 'Results' tab
        When I press the 'manual' button
        Then the title 'Resultaten' should show

        # Settings (shouldn't change manual page)
        When I open the settings panel
        Then the title 'Resultaten' should show
