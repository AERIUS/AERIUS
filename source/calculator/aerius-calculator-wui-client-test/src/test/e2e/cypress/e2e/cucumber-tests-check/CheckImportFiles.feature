#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: AERIUS Check, Import files
    Test how importing files is handled for product Check.

    # Fails due to AER-3022
    @Smoke
    Scenario: Import GML file with default situation type 'Beoogd'
        Given I login with correct username and password
        And I select file 'AERIUS_import_file.gml' to be imported
        Then the situation name is 'Situatie 1'
        And the situation type is 'Beoogd'
        And the calculation year is '2021'
        When I open the source list
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Stalemissies' is correctly saved
        And location with coordinates 'X:57213,62 Y:427320,74' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '25,000' and diurnal variation type 'Dierverblijven' is correctly saved
        And the lodging system with RAV code 'E1.5.1', number of animals '1000', factor '0,02', reduction '-' and emission '20,0 kg/j' is correctly created
        When I select the source 'Bron 2'
        Then the source 'Bron 2' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:57245,55 Y:427432,47' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '150,0 kg/j'
        And the total 'generic' emission 'NH3' is '180,0 kg/j'
        When I select view mode 'BUILDING'
        And I select the building 'Gebouw 1'
        Then the building with name 'Gebouw 1', length '137,0 m  (105,0 m)', width '102,7 m  (87,2 m)', height '23,0 m  (20,0 m)' and orientation '89°' is saved and selected
