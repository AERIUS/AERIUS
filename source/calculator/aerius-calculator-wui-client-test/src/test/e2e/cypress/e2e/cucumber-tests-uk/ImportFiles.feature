#
# Crown copyright
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Import files
    Test how importing files is handled.

    Scenario: Validate 50 buildings is the max for importing
        Given I login with correct username and password
        And I enable the advanced importmodus

        # Try to import GML with 50 buildings
        When I select file 'UKAPAS_50_buildings.gml' to be imported, but I don't upload it yet
        Then the button to download warnings and errors should not be shown
        And the detail info of the line with scenario 'Situatie 1' of import file 'UKAPAS_50_buildings.gml' should contain '50 buildings'
        And button 'Importeer' is enabled

        # Try to import GML with 51 buildings
        When I select file 'UKAPAS_51_buildings.gml' to be imported, but I don't upload it yet
        Then the button to download warnings and errors should be shown
        When I click on the 'error' button
        Then the following 'error' text is visible: 'There are too many buildings in the scenario, the maximum allowed is 50.'
        And button 'Importeer' is disabled

    @Smoke
    Scenario: Import a GML with an instance of each source type, buildings and a time-varying profile and check whether it imports correctly
        Given I login with correct username and password
        When I select file 'UKAPAS_everything_import_file.gml' to be imported
        And I open emission source list

        # General details for every source
        Then I select the following sources and check their ADMS details
            | name      | sectorGroup | sector                  | location                | locationDetail | sourceType | elevationAngle | horizontalAngle | verticalDimension | sourceHeight | sourceDiameter | sourceWidth | buoyancy    | temperature | density    | effluxType | velocity  | volumeticFlowRate | specificHeatCapacity | timeVaryingProfileName_THREE_DAY | timeVaryingProfileType_THREE_DAY | timeVaryingProfileName_MONTHLY | timeVaryingProfileType_MONTHLY |
            | Source 1  | Energy      | Large combustion plant  | X:436054.69 Y:435483.37 |                | Jet        | 31.00 °        | 1.00 °          |                   | 12.00 m      | 13.00 m        |             | Temperature | 15.00 °C    |            | Velocity   | 21.00 m/s |                   | 15.000 J/K/kg        | Continuous                       | Predefined profile               | Sector default (Continuous)    | Predefined profile             |
            | Source 2  | Energy      | Specified generator     | X:436044.34 Y:435506.32 | 22.45 m        | Line       |                |                 |                   | 12.00 m      |                | 0.00 m      |             |             | 1.00 kg/m³ | Volume     |           | 12.00 m³/s        | 1,012.000 J/K/kg     | Sector default (Continuous)      | Predefined profile               | Sector default (Continuous)    | Predefined profile             |
            | Source 3  | Energy      | Other                   | X:436069.67 Y:435506.32 | 0.00 ha        | Volume     |                |                 | 1.00 m            | 12.00 m      |                |             |             |             |            |            |           |                   |                      | Sector default (Continuous)      | Predefined profile               | Sector default (Continuous)    | Predefined profile             |
            | Source 4  | Agriculture | Animal housing          | X:436049.43 Y:435496.34 | 53.81 m        | Line       |                |                 |                   | 3.00 m       |                | 1.00 m      |             |             |            | Velocity   | 15.00 m/s |                   | 1,012.000 J/K/kg     | Custom diurnal profile           | Custom profile                   | Sector default (Continuous)    | Predefined profile             |
            | Source 5  | Agriculture | Manure storage          | X:436067.67 Y:435513.8  | 0.03 ha        | Area       |                |                 |                   | 54.00 m      |                |             |             |             | 2.00 kg/m³ | Volume     |           | 52.00 m³/s        | 1,012.000 J/K/kg     | Continuous                       | Predefined profile               | Sector default (Continuous)    | Predefined profile             | 
            | Source 7  | Agriculture | Farmland                | X:436021.18 Y:435546.96 |                | Point      |                |                 |                   | 12.00 m      | 3.00 m         |             | Temperature | 15.00 °C    |            | Velocity   | 15.00 m/s |                   | 100.000 J/K/kg       | Sector default (Continuous)      | Predefined profile               | Sector default (Continuous)    | Predefined profile             |
            | Source 8  | Agriculture | Greenhouse horticulture | X:436115.72 Y:435492.23 | 0.11 ha        | Area       |                |                 |                   | 12.00 m      |                |             |             |             |            | Velocity   | 15.00 m/s |                   | 1,012.000 J/K/kg     | Custom diurnal profile (1)       | Custom profile                   | Sector default (Continuous)    | Predefined profile             |
            | Source 9  | Agriculture | Fireplaces, other       | X:436069.89 Y:435517.61 |                | Point      |                |                 |                   | 2.00 m       | 1.00 m         |             | Temperature | 15.00 °C    |            | Velocity   | 15.00 m/s |                   | 1,012.000 J/K/kg     | Sector default (Continuous)      | Predefined profile               | Sector default (Continuous)    | Predefined profile             |
            | Source 10 | Industry    | Waste processing        | X:436096.68 Y:435528.25 |                | Point      |                |                 |                   | 0.00 m       | 12.00 m        |             | Temperature | 15.00 °C    |            | Velocity   | 15.00 m/s |                   | 1,012.000 J/K/kg     | Continuous                       | Predefined profile               | Sector default (Continuous)    | Predefined profile             |
            | Source 11 | Industry    | Food and beverages      | X:436030.16 Y:435510.79 | 43.29 m        | Line       |                |                 |                   | 1.00 m       |                | 0.00 m      | Temperature | 15.00 °C    |            | Velocity   | 15.00 m/s |                   | 1,012.000 J/K/kg     | Custom diurnal profile           | Custom profile                   | Sector default (Continuous)    | Predefined profile             |
            | Source 12 | Industry    | Chemical industry       | X:436062.37 Y:435516.81 | 16.15 m        | Line       |                |                 |                   | 12.00 m      |                | 0.00 m      |             |             | 1.23 kg/m³ | Velocity   | 15.00 m/s |                   | 1,012.000 J/K/kg     | Custom diurnal profile (1)       | Custom profile                   | Sector default (Continuous)    | Predefined profile             |
            | Source 13 | Industry    | Building materials      | X:436057.15 Y:435524.24 |                | Jet        | 12.00 °        | 2.00 °          |                   | 3.00 m       | 5.00 m         |             | Temperature | 15.00 °C    |            | Velocity   | 15.00 m/s |                   | 1,012.000 J/K/kg     | Sector default (Continuous)      | Predefined profile               | Sector default (Continuous)    | Predefined profile             |
            | Source 14 | Industry    | Basic metal             | X:436044.56 Y:435529.65 | 0.01 ha        | Area       |                |                 |                   | 2.00 m       |                |             |             |             |            | Velocity   | 15.00 m/s |                   | 1,012.000 J/K/kg     | Sector default (Continuous)      | Predefined profile               | Sector default (Continuous)    | Predefined profile             |
            | Source 15 | Industry    | Metalworking industry   | X:436029.46 Y:435556.64 | 21.74 m        | Line       |                |                 |                   | 5.00 m       |                | 0.00 m      |             |             | 2.00 kg/m³ | Velocity   | 15.00 m/s |                   | 1,012.000 J/K/kg     | Sector default (Continuous)      | Predefined profile               | Sector default (Continuous)    | Predefined profile             |
            | Source 16 | Industry    | Other                   | X:435989.38 Y:435535.77 | 0.32 ha        | Area       |                |                 |                   | 2.00 m       |                |             |             | 3.00 °C     |            | Velocity   | 15.00 m/s |                   | 1,012.000 J/K/kg     | Continuous                       | Predefined profile               | Sector default (Continuous)    | Predefined profile             |
            | Source 17 | Other       |                         | X:436054.76 Y:435518.12 | 1.31 ha        | Area       |                |                 |                   | 23.00 m      |                |             |             |             | 0.01 kg/m³ | Volume     |           | 2.00 m³/s         | 1,012.000 J/K/kg     | Continuous                       | Predefined profile               | Sector default (Continuous)    | Predefined profile             |

        # Road source subsources
        Then I open the road transport network panel so the underlying sources can be selected
        And I check the following traffic sources and check their details
            | name     | sectorGroup    | sector | location                | locationDetail  | roadArea | roadType           | roadTrafficDirection | roadWidth | roadElevation | roadGradient | barrierCoverage | linkSpeed | cars | blackCabs | motorcycles | lgv | hgv | busAndCoach | trafficDescription | numberOfVehicles | noxEmissionPerVehicle | nh3EmissionPerVehicle | timeVaryingProfileName_THREE_DAY | timeVaryingProfileType_THREE_DAY | timeVaryingProfileName_MONTHLY | timeVaryingProfileType_MONTHLY |
            | Source 6 | Traffic        |        | X:436085.15 Y:435516.01 | 42.02 m         | Wales    | Urban (not London) | Two way              | 2 m       | 1 m           | 2 %          | 51 %            | 78 km/h   | 1.0  | 2.0       | 3.0         | 4.0 | 5.0 | 6.0         | 123                | 3 /24 hours      | 23.0 g/km/s           | 1.0 g/km/s            | Sector default (UK road 2022)    | Predefined profile               | Sector default (Continuous)    | Predefined profile             |

        # Housing source subsources
        Then I check for the following housing subsource details for source 'Source 4'
            | custom | housingCode | nrOfAnimals | emissionFactor    | days      | emission      | description                                             |
            | 0      | A1.1        | 12          | 0.071 kg/animal/d | 3         | 2.6 kg/y      | A1.1 Slurry based cubicle housing (Cattle - Dairy cows) |
            | 1      | Custom      | 2000        | 5 kg/animal/y     |           | 10,000.0 kg/y |                                                         |

        # Farmland source subsources
        And I check for farmland subsource details for source 'Source 7'
            | subsource                       | animalType | number | factor     | days | emission     | noxEmission | nh3Emission      |
            | Farmland grazing                | Beef cows  | 12000  | 0.009 kg/y | 340  | 36.7 tonne/y |             |                  |
            | Manure application (fertilizer) |            |        |            |      |              | 31.0 kg/y   | 24.0 kg/y        |
            | Outdoor yards                   | Dairy cows | 56     | 0.01 kg/y  | 83   | 46.5 g/y     |             |                  |
            | Organic processes               |            |        |            |      |              | 1.0 g/y     | 12,000.0 tonne/y |

        # Building influence
        Then I check for building info for the following sources
            | source    | isCircular | length   | width    | height  | orientation |
            | Source 7  | 1          |          | 6.230 m  | 5.000 m |             |
            | Source 10 | 0          | 18.623 m | 11.149 m | 2.000 m | 152°        |

        # Time-varying profiles
        When I select view mode 'TIME_VARYING_PROFILES'
        Then I check the custom time-varying profiles for values
            | profileId                | profileTitle       | profileName                | profileType    | weekdays | saturdays | sundays | total    | averageWeekdays| averageSaturdays| averageSundays | average |
            | profile-DiurnalProfile.1 | Diurnal profile    | Custom diurnal profile     | Custom profile | 122.5000 | 24.7000   | 21.0000 | 168.2000 | 1.0208         | 1.0292          | 0.8750         | 1.0012  |
            | profile-DiurnalProfile.2 | Diurnal profile    | Custom diurnal profile (1) | Custom profile | 120.0000 | 24.0000   | 24.0000 | 168.0000 | 1.0000         | 1.0000          | 1.0000         | 1.0000  |

        # TODO: Check total emissions for every emissionsource with subsources
        # TODO: Check traffic movements type (f.e. p/month, p/24 hours)

        When I navigate to the 'Assessment points' tab
        Then calculation point 'Rekenpuntje' is correctly saved

        When I select the assessment point 'Rekenpuntje'
        And I update this assessment point
        Then the entered coordinates should be 'POINT(208988.1 676882.88)'

        When I open assessment point characteristics
        Then assessment point category 'ECOLOGY', road height '1.6' and custom primary fraction '1' are correctly saved

        When I open assessment point habitats
        Then the habitat list exists and has 1 habitats

        When I select habitat 'Mijn habitat'
        Then the selected habitat has the following data
        | name         | CLNOx | CLNOxEnabled | CLNH3 | CLNH3Enabled | CL | CLEnabled |
        | Mijn habitat | 31    | true         | 4     | true         | 26 | true      |
       
    Scenario: Import a ADMS APL or UPL file support Irish Grid coordinates be converted to British National Grid
        Given I login with correct username and password
        When I select file './adms/epsg29903.upl' to be imported
        And I open emission source list

        # General details for every source
        Then I select the following sources and check their ADMS details
            | name   | sectorGroup | sector | location                | locationDetail | sourceType | elevationAngle | horizontalAngle | verticalDimension| sourceHeight | sourceDiameter | sourceWidth | buoyancy    | temperature | density | effluxType | velocity  | volumeticFlowRate | specificHeatCapacity | volumeticFlowRate | timeVaryingProfileName_THREE_DAY | timeVaryingProfileType_THREE_DAY | timeVaryingProfileName_MONTHLY | timeVaryingProfileType_MONTHLY |
            | Point1 | Industry    | Other  | X:123425.25 Y:542469.96 |                | Point      |                |                 |                  | 35.00 m      | 1.00 m         |             | Temperature | 40.00 °C    |         | Velocity   | 15.00 m/s | 1,012.000 J/K/kg  |                      |                   | Sector default (Continuous)      | Predefined profile               | Sector default (Continuous)    | Predefined profile             |

        # Building influence
        And I select view mode 'BUILDING'
        Then the buildinglist has 1 buildings
        And I select the building with name 'Cuboid'

        Then I check for building info for the following sources
            | source | isCircular | length   | width    | height   | orientation |
            | Point1 | 0          | 34.000 m | 34.000 m | 34.000 m | 0°          |
        And I edit the building
        Then I validate the building shape with WKT string 'POLYGON((123442.25 542486.96,123408.25 542486.96,123408.25 542452.96,123442.25 542452.96,123442.25 542486.96))'

    Scenario: Import UKAPAS file and validate the advanced settings when changing calculation job line and results line to import
        Given I login with correct username and password
        And I enable the advanced importmodus
        When I select file 'UKAPAS_in_combination_results_with_archive_results.zip' to be imported, but I don't upload it, or open the collapsible panel yet
        Then there should be 2 scenarios ready to imported

        When I expand the line with scenario 'Scenario 1' for file 'UKAPAS_in_combination_results_with_archive_results.zip'
        And the detail info of the line with scenario 'Scenario 1' of import file 'UKAPAS_in_combination_results_with_archive_results.zip' should contain '1 emission source'
        And the detail info of the line with scenario 'Scenario 1' of import file 'UKAPAS_in_combination_results_with_archive_results.zip' should contain '1 calculation job (based on file)'
        And the detail info of the line with scenario 'Scenario 1' of import file 'UKAPAS_in_combination_results_with_archive_results.zip' should contain '237 calculation results'

        When I expand line with no scenario for file 'UKAPAS_in_combination_results_with_archive_results.zip'
        And the detail info of the line with no scenario of import file 'UKAPAS_in_combination_results_with_archive_results.zip' should contain '1 calculation job (based on file)'
        And the detail info of the line with no scenario of import file 'UKAPAS_in_combination_results_with_archive_results.zip' should contain '94 calculation results'
        Then I validate the selected import lines
            | scenario   | lineTitle                         | checkBoxStatus |
            | Scenario 1 | 1 emission source                 | checked        |
            | Scenario 1 | 1 calculation job (based on file) | checked        |
            | Scenario 1 | 237 calculation results           | checked        |
            |            | 1 calculation job (based on file) | checked        |
            |            | 94 calculation results            | checked        |

        When I toggle the line with scenario 'Scenario 1' of import file 'UKAPAS_in_combination_results_with_archive_results.zip' line '1 emission source'
        Then I validate the selected import lines
            | scenario   | lineTitle                         | checkBoxStatus |
            | Scenario 1 | 1 emission source                 | unchecked      |
            | Scenario 1 | 1 calculation job (based on file) | checked        |
            | Scenario 1 | 237 calculation results           | checked        |
            |            | 1 calculation job (based on file) | checked        |
            |            | 94 calculation results            | checked        |

        When I toggle the line with scenario 'Scenario 1' of import file 'UKAPAS_in_combination_results_with_archive_results.zip' line '1 calculation job (based on file)'
        Then I validate the selected import lines
            | scenario   | lineTitle                         | checkBoxStatus |
            | Scenario 1 | 1 emission source                 | unchecked      |
            | Scenario 1 | 1 calculation job (based on file) | unchecked      |
            | Scenario 1 | 237 calculation results           | disabled       |
            |            | 1 calculation job (based on file) | unchecked      |
            |            | 94 calculation results            | disabled       |

        When I toggle the line with scenario 'Scenario 1' of import file 'UKAPAS_in_combination_results_with_archive_results.zip' line '1 emission source'
        And I toggle the line with scenario 'Scenario 1' of import file 'UKAPAS_in_combination_results_with_archive_results.zip' line '1 calculation job (based on file)'        
        Then I validate the selected import lines
            | scenario   | lineTitle                         | checkBoxStatus |
            | Scenario 1 | 1 emission source                 | checked        |
            | Scenario 1 | 1 calculation job (based on file) | checked        |
            | Scenario 1 | 237 calculation results           | unchecked      |
            |            | 1 calculation job (based on file) | checked        |
            |            | 94 calculation results            | unchecked      |

        And I toggle the line with scenario 'Scenario 1' of import file 'UKAPAS_in_combination_results_with_archive_results.zip' line '237 calculation results'
        Then I validate the selected import lines
            | scenario   | lineTitle                         | checkBoxStatus |
            | Scenario 1 | 1 emission source                 | checked        |
            | Scenario 1 | 1 calculation job (based on file) | checked        |
            | Scenario 1 | 237 calculation results           | checked        |
            |            | 1 calculation job (based on file) | checked        |
            |            | 94 calculation results            | checked        |
