#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Road transportation - Cold start

    Scenario: Check default values of source characteristics for Cold start - parking garage
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: parkeergarage'
        # source with geometry: POLYGON
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        Then I validate the default transport characteristics
            | sourceCharacteristicId | expectedValue |
            | buildingInfluence      | NOT_CHECKED   |
            | heatContentType        | NOT_FORCED    |
            | emissionHeight         | 0.3           |
            | sourceSpread           | 0.1           |
            | emissionHeatContent    | 0             |
            | diurnalVariationType   | Licht Verkeer |
        And the following characteristics should not exist
            | sourceCharacteristicId |
            | emissionTemperature    |
            | outflowDiameter        |
            | outflowDirectionType   |
            | outflowVelocity        |

        When I change the 'heatContentType' to 'FORCED'
        Then I validate the default transport characteristics
            | sourceCharacteristicId | expectedValue |
            | buildingInfluence      | NOT_CHECKED   |
            | heatContentType        | FORCED        |
            | emissionHeight         | 0.3           |
            | emissionTemperature    | 11.85         |
            | outflowDiameter        | 0.1           |
            | outflowDirectionType   | VERTICAL      |
            | outflowVelocity        | 0             |
            | sourceSpread           | 0.1           |
            | diurnalVariationType   | Licht Verkeer |
        And the following characteristics should not exist
            | sourceCharacteristicId |
            | emissionHeatContent    |

        # source with geometry: LINESTRING
        When I open collapsible panel 'Location'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I open collapsible panel 'Bronkenmerken' 
        And I change the 'heatContentType' to 'NOT_FORCED'
        Then I validate the default transport characteristics
            | sourceCharacteristicId | expectedValue |
            | buildingInfluence      | NOT_CHECKED   |
            | heatContentType        | NOT_FORCED    |
            | emissionHeight         | 0.3           |
            | emissionHeatContent    | 0             |
            | diurnalVariationType   | Licht Verkeer |
        And the following characteristics should not exist
            | sourceCharacteristicId |
            | emissionTemperature    |
            | outflowDiameter        |
            | outflowDirectionType   |
            | outflowVelocity        |
            | sourceSpread           |
        When I change the 'heatContentType' to 'FORCED'
        Then I validate the default transport characteristics
            | sourceCharacteristicId | expectedValue |
            | buildingInfluence      | NOT_CHECKED   |
            | heatContentType        | FORCED        |
            | emissionHeight         | 0.3           |
            | emissionTemperature    | 11.85         |
            | outflowDiameter        | 0.1           |
            | outflowDirectionType   | VERTICAL      |
            | outflowVelocity        | 0             |
            | diurnalVariationType   | Licht Verkeer |
        And the following characteristics should not exist
            | sourceCharacteristicId |
            | sourceSpread           |
            | emissionHeatContent    |

        # source with geometry: POINT
        When I open collapsible panel 'Location'   
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I open collapsible panel 'Bronkenmerken' 
        And I change the 'heatContentType' to 'NOT_FORCED'
        Then I validate the default transport characteristics
            | sourceCharacteristicId | expectedValue |
            | buildingInfluence      | NOT_CHECKED   |
            | heatContentType        | NOT_FORCED    |
            | emissionHeight         | 0.3           |
            | emissionHeatContent    | 0             |
            | diurnalVariationType   | Licht Verkeer |
        And the following characteristics should not exist
            | sourceCharacteristicId |
            | emissionTemperature    |
            | outflowDiameter        |
            | outflowDirectionType   |
            | outflowVelocity        |
            | sourceSpread           |
        When I change the 'heatContentType' to 'FORCED'
        Then I validate the default transport characteristics
            | sourceCharacteristicId | expectedValue |
            | buildingInfluence      | NOT_CHECKED   |
            | heatContentType        | FORCED        |
            | emissionHeight         | 0.3           |
            | emissionTemperature    | 11.85         |
            | outflowDiameter        | 0.1           |
            | outflowDirectionType   | VERTICAL      |
            | outflowVelocity        | 0             |
            | diurnalVariationType   | Licht Verkeer |
        And the following characteristics should not exist
            | sourceCharacteristicId |
            | sourceSpread           |
            | emissionHeatContent    |

    Scenario: Check default values of source characteristics for Cold start - other
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: overig'

        # source with geometry: POLYGON
        When I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        Then the source characteristics panel does not exist

        # source with geometry: LINESTRING
        When I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        Then the source characteristics panel does not exist

        # source with geometry: POINT
        When I fill location with coordinates 'POINT(172203.2 599120.32)'
        Then the source characteristics panel does not exist       

    @Smoke
    Scenario: Add source Cold start: parking garage - Emission factor toolkit with vehicles per hour
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: parkeergarage'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        And I click to add a emission source of type 'Koude start: Voorgeschreven factoren'
        When I select time unit '/uur' for cold starts
        And I fill in the following number of vehicles
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | 100              |
            | Middelzware vrachtvoertuigen  | 200              |
            | Zware vrachtvoertuigen        | 300              |
            | Bussen                        | 400              |
        And I save the data
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Verkeer' and sector 'Koude start: parkeergarage' is correctly saved
        And location with coordinates 'X:142250,05 Y:462711,05' is correctly saved
        And surface of location with value '162,44 ha' is correctly saved
        Then the following source characteristics are correctly saved
            | sourceCharacteristic | expectedValue    |
            | Ventilatie           | Niet geforceerd  |
            | Gebouwinvloed        | Geen             |
            | Uittreedhoogte       | 0,3 m            |
            | Warmteinhoud         | 0,000 MW         |
            | Spreiding            | 0 m              |
            | Temporele variatie   | Licht Verkeer    |
        And the time unit 'Koude starts /uur' is correctly saved
        And the following number of vehicles is correctly saved
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | 100,0            |
            | Middelzware vrachtvoertuigen  | 200,0            |
            | Zware vrachtvoertuigen        | 300,0            |
            | Bussen                        | 400,0            |
        And the total 'cold start' emission 'NOx' is '133,4 ton/j'
        And the total 'cold start' emission 'NH3' is '1.226,0 kg/j'

   Scenario: Add source Cold start: parking garage - Emission factor toolkit with vehicles per 24 hour
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: parkeergarage'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I open the subsource dropdown
        When I click to add a emission source of type 'Koude start: Voorgeschreven factoren'
        And I select time unit '/etmaal' for cold starts
        And I fill in the following number of vehicles
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | 1231             |
            | Middelzware vrachtvoertuigen  | 3341             |
            | Zware vrachtvoertuigen        | 229.5            |
            | Bussen                        | 234              |
        And I save the data
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Verkeer' and sector 'Koude start: parkeergarage' is correctly saved
        And the title for 'standard' road subsource '0' is 'Voorgeschreven factoren'
        And the time unit 'Koude starts /etmaal' is correctly saved
        And the following number of vehicles is correctly saved
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | 1.231,0          |
            | Middelzware vrachtvoertuigen  | 3.341,0          |
            | Zware vrachtvoertuigen        | 229,5            |
            | Bussen                        | 234,0            |
        And the total 'cold start' emission 'NOx' is '25,9 ton/j'
        And the total 'cold start' emission 'NH3' is '301,0 kg/j'

    Scenario: Add source Cold start: parking garage - Emission factor toolkit with vehicles per month
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: parkeergarage'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I open the subsource dropdown
        When I click to add a emission source of type 'Koude start: Voorgeschreven factoren'
        And I select time unit '/maand' for cold starts
        And I fill in the following number of vehicles
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | 1231             |
            | Middelzware vrachtvoertuigen  | 3341             |
            | Zware vrachtvoertuigen        | 229.5            |
            | Bussen                        | 234              |
        And I save the data
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Verkeer' and sector 'Koude start: parkeergarage' is correctly saved        
        And the title for 'standard' road subsource '0' is 'Voorgeschreven factoren'
        And the time unit 'Koude starts /maand' is correctly saved
        And the following number of vehicles is correctly saved
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | 1.231,0          |
            | Middelzware vrachtvoertuigen  | 3.341,0          |
            | Zware vrachtvoertuigen        | 229,5            |
            | Bussen                        | 234,0            |
        And the total 'cold start' emission 'NOx' is '852,3 kg/j'
        And the total 'cold start' emission 'NH3' is '9,9 kg/j'

    Scenario: Add source Cold start: parking garage - Emission factor toolkit with vehicles per year
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: parkeergarage'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        When I click to add a emission source of type 'Koude start: Voorgeschreven factoren'
        And I select time unit '/jaar' for cold starts
        And I fill in the following number of vehicles
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | 1231             |
            | Middelzware vrachtvoertuigen  | 3341             |
            | Zware vrachtvoertuigen        | 229.5            |
            | Bussen                        | 234              |
        And I save the data
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Verkeer' and sector 'Koude start: parkeergarage' is correctly saved        
        And the title for 'standard' road subsource '0' is 'Voorgeschreven factoren'
        And the time unit 'Koude starts /jaar' is correctly saved
        And the following number of vehicles is correctly saved
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | 1.231,0          |
            | Middelzware vrachtvoertuigen  | 3.341,0          |
            | Zware vrachtvoertuigen        | 229,5            |
            | Bussen                        | 234,0            |
        And the total 'cold start' emission 'NOx' is '71,0 kg/j'
        And the total 'cold start' emission 'NH3' is '0,8 kg/j'

    @Smoke
    Scenario: Add source Cold start: parking garage - Custom specification with Euro class with vehicles per hour
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: parkeergarage'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        When I click to add a emission source of type 'Koude start: Eigen specificatie'
        And I choose Euro class 'Trekker - benzine - zwaar - pre Euro wetgeving (1992)' from custom
        And I choose time unit '/uur' with number of vehicles '100'
        And emission 'NOX' per vehicle is updated with '10.1464' 
        And emission 'NH3' per vehicle is updated with '0.003'
        And I save the data
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Verkeer' and sector 'Koude start: parkeergarage' is correctly saved      
        And the title for 'specific' road subsource '0' is 'Eigen specificatie'
        And Euro class 'Trekker - benzine - zwaar - pre Euro wetgeving (1992)' is correctly saved
        And time unit '/uur' with number of cold starts '100' for Euro class is correctly saved
        And the total 'cold start' emission 'NOx' is '8.888,2 kg/j'
        And the total 'cold start' emission 'NH3' is '2,6 kg/j'

    Scenario: Add source Cold start: parking garage - Custom specification with Euro class with vehicles per 24 hour
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: parkeergarage'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        When I click to add a emission source of type 'Koude start: Eigen specificatie'
        And I choose Euro class 'Trekker - diesel - licht - Euro-4 - N1 klasse 3' from custom
        And I choose time unit '/etmaal' with number of vehicles '1000'
        And emission 'NOX' per vehicle is updated with '0.14' 
        And emission 'NH3' per vehicle is updated with '0.002'
        And I save the data
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Verkeer' and sector 'Koude start: parkeergarage' is correctly saved     
        And the title for 'specific' road subsource '0' is 'Eigen specificatie'
        And Euro class 'Trekker - diesel - licht - Euro-4 - N1 klasse 3' is correctly saved
        And time unit '/etmaal' with number of cold starts '1.000' for Euro class is correctly saved
        And the total 'cold start' emission 'NOx' is '51,1 kg/j'
        And the total 'cold start' emission 'NH3' is '0,7 kg/j'

    Scenario: Add source Cold start: parking garage - Custom specification with Euro class with vehicles per month
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: parkeergarage'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        When I click to add a emission source of type 'Koude start: Eigen specificatie'
        And I choose Euro class 'Vrachtauto - diesel - middelzwaar - Euro-2 - zwaar' from custom
        And I choose time unit '/maand' with number of vehicles '1500'
        And emission 'NOX' per vehicle is updated with '16.854904' 
        And emission 'NH3' per vehicle is updated with '0.003'
        And I save the data
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Verkeer' and sector 'Koude start: parkeergarage' is correctly saved     
        And the title for 'specific' road subsource '0' is 'Eigen specificatie'
        And Euro class 'Vrachtauto - diesel - middelzwaar - Euro-2 - zwaar' is correctly saved
        And time unit '/maand' with number of cold starts '1.500' for Euro class is correctly saved
        And the total 'cold start' emission 'NOx' is '303,4 kg/j'
        And the total 'cold start' emission 'NH3' is '54,0 g/j'

    Scenario: Add source Cold start: parking garage - Custom specification with Euro class with vehicles per year
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: parkeergarage'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        When I click to add a emission source of type 'Koude start: Eigen specificatie'
        And I choose Euro class 'Personenauto - elektrisch' from custom
        And I choose time unit '/jaar' with number of vehicles '2500'
        And emission 'NOX' per vehicle is updated with '0' 
        And emission 'NH3' per vehicle is updated with '0'
        And I save the data
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Verkeer' and sector 'Koude start: parkeergarage' is correctly saved      
        And the title for 'specific' road subsource '0' is 'Eigen specificatie'
        And Euro class 'Personenauto - elektrisch' is correctly saved
        And time unit '/jaar' with number of cold starts '2.500' for Euro class is correctly saved
        And the total 'cold start' emission 'NOx' is '0,0 kg/j'
        And the total 'cold start' emission 'NH3' is '0,0 kg/j'

    Scenario: Add source Cold start: parking garage - Custom specification with Euro class 'Other'
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: parkeergarage'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        When I click to add a emission source of type 'Koude start: Eigen specificatie'
        # Euro class 'Anders' is by default selected
        And I fill description with 'Eigen specificatie koude start - parkeergarage'
        And I choose time unit '/maand' with number of vehicles '1000'
        And I fill emission 'NOX' per vehicle '0.2101'
        And I fill emission 'NH3' per vehicle '0.31011'
        And I save the data
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Verkeer' and sector 'Koude start: parkeergarage' is correctly saved     
        And the title for 'custom' road subsource '0' is 'Eigen specificatie'
        And description 'Eigen specificatie koude start - parkeergarage' is correctly saved        
        And time unit '/maand' with number of cold starts '1.000' for Euro class Other is correctly saved
        And the total 'cold start' emission 'NOx' is '2,5 kg/j'
        And the total 'cold start' emission 'NH3' is '3,7 kg/j'

    Scenario: Copy and delete source Cold start: parking garage - Emission factor toolkit
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: parkeergarage'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        And I click to add a emission source of type 'Koude start: Voorgeschreven factoren'
        When I select time unit '/uur' for cold starts
        And I fill in the following number of vehicles
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | 100              |
            | Middelzware vrachtvoertuigen  | 200              |
            | Zware vrachtvoertuigen        | 300              |
            | Bussen                        | 400              |
        And I save the data
        And I select the source 'Bron 1'
        When I copy the source 'Bron 1'
        Then the source 'Bron 1 (1)' with sector group 'Verkeer' and sector 'Koude start: parkeergarage' is correctly saved
        And location with coordinates 'X:142250,05 Y:462711,05' is correctly saved
        And surface of location with value '162,44 ha' is correctly saved
        Then the following source characteristics are correctly saved
            | sourceCharacteristic | expectedValue    |
            | Ventilatie           | Niet geforceerd  |
            | Gebouwinvloed        | Geen             |
            | Uittreedhoogte       | 0,3 m            |
            | Warmteinhoud         | 0,000 MW         |
            | Spreiding            | 0 m              |
            | Temporele variatie   | Licht Verkeer    |
        And the time unit 'Koude starts /uur' is correctly saved
        And the following number of vehicles is correctly saved
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | 100,0            |
            | Middelzware vrachtvoertuigen  | 200,0            |
            | Zware vrachtvoertuigen        | 300,0            |
            | Bussen                        | 400,0            |
        And the total 'cold start' emission 'NOx' is '133,4 ton/j'
        And the total 'cold start' emission 'NH3' is '1.226,0 kg/j' 

        # delete source
        When I click on the 'delete source' button
        Then the next source does not exist 'Bron 1 (1)'
  
    Scenario: Copy and delete source Cold start: parking garage - Custom specification with Euro class
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: parkeergarage'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        When I click to add a emission source of type 'Koude start: Eigen specificatie'
        And I choose Euro class 'Trekker - benzine - zwaar - pre Euro wetgeving (1992)' from custom
        And I choose time unit '/uur' with number of vehicles '100'
        And emission 'NOX' per vehicle is updated with '10.1464' 
        And emission 'NH3' per vehicle is updated with '0.003'
        And I save the data
        When I select the source 'Bron 1'
        And I copy the source 'Bron 1'
        Then the source 'Bron 1 (1)' with sector group 'Verkeer' and sector 'Koude start: parkeergarage' is correctly saved      
        And the title for 'specific' road subsource '0' is 'Eigen specificatie'
        And Euro class 'Trekker - benzine - zwaar - pre Euro wetgeving (1992)' is correctly saved
        And time unit '/uur' with number of cold starts '100' for Euro class is correctly saved
        And the total 'cold start' emission 'NOx' is '8.888,2 kg/j'
        And the total 'cold start' emission 'NH3' is '2,6 kg/j'

        # delete source
        When I click on the 'delete source' button
        Then the next source does not exist 'Bron 1 (1)'
  
    Scenario: Copy and delete source Cold start: parking garage - Custom specification with Euro class 'Other'
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: parkeergarage'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        When I click to add a emission source of type 'Koude start: Eigen specificatie'
        # Euro class 'Anders' is by default selected
        And I fill description with 'Eigen specificatie koude start - parkeergarage'
        And I choose time unit '/maand' with number of vehicles '1000'
        And I fill emission 'NOX' per vehicle '0.2101'
        And I fill emission 'NH3' per vehicle '0.31011'
        And I save the data
        When I select the source 'Bron 1'
        And I copy the source 'Bron 1'
        Then the source 'Bron 1 (1)' with sector group 'Verkeer' and sector 'Koude start: parkeergarage' is correctly saved     
        And the title for 'custom' road subsource '0' is 'Eigen specificatie'
        And description 'Eigen specificatie koude start - parkeergarage' is correctly saved        
        And time unit '/maand' with number of cold starts '1.000' for Euro class Other is correctly saved
        And the total 'cold start' emission 'NOx' is '2,5 kg/j'
        And the total 'cold start' emission 'NH3' is '3,7 kg/j'

        # delete source
        When I click on the 'delete source' button
        Then the next source does not exist 'Bron 1 (1)'

    Scenario: Validate NO2 emission input not available for Cold start: parking garage - Custom specification
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: parkeergarage'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        When I click to add a emission source of type 'Koude start: Eigen specificatie'
        # Euro class 'Anders' is by default selected
        And I fill description with 'Eigen specificatie koude start - parkeergarage'
        And I choose time unit '/maand' with number of vehicles '1000'
        And I fill emission 'NOX' per vehicle '0.2101'
        And I fill emission 'NH3' per vehicle '0.31011'
        And emission NO2 input field should not exist
        And I save the data
        When I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Verkeer' and sector 'Koude start: parkeergarage' is correctly saved
        And the title for 'custom' road subsource '0' is 'Eigen specificatie'
        And description 'Eigen specificatie koude start - parkeergarage' is correctly saved
        And time unit '/maand' with number of cold starts '1.000' for Euro class Other is correctly saved
        And the total 'cold start' emission 'NOx' is '2,5 kg/j'
        And the total 'cold start' emission 'NH3' is '3,7 kg/j'
        And the detail emission contains 2 substance

    @Smoke
    Scenario: Add source Cold start: other - Emission factor toolkit with vehicles per hour
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: overig'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        And I click to add a emission source of type 'Koude start: Voorgeschreven factoren'
        When I select time unit '/uur' for cold starts
        And I fill in the following number of vehicles
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | 100              |
            | Middelzware vrachtvoertuigen  | 200              |
            | Zware vrachtvoertuigen        | 300              |
            | Bussen                        | 400              |
        And I save the data
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Verkeer' and sector 'Koude start: overig' is correctly saved
        And location with coordinates 'X:142250,05 Y:462711,05' is correctly saved
        And surface of location with value '162,44 ha' is correctly saved
        And the time unit 'Koude starts /uur' is correctly saved
        And the following number of vehicles is correctly saved
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | 100,0            |
            | Middelzware vrachtvoertuigen  | 200,0            |
            | Zware vrachtvoertuigen        | 300,0            |
            | Bussen                        | 400,0            |
        And the total 'cold start' emission 'NOx' is '133,4 ton/j'
        And the total 'cold start' emission 'NH3' is '1.226,0 kg/j'

    Scenario: Add source Cold start: other - Emission factor toolkit with vehicles per 24 hour
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: overig'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I open the subsource dropdown
        When I click to add a emission source of type 'Koude start: Voorgeschreven factoren'
        And I select time unit '/etmaal' for cold starts
        And I fill in the following number of vehicles
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | 1231             |
            | Middelzware vrachtvoertuigen  | 3341             |
            | Zware vrachtvoertuigen        | 229.5            |
            | Bussen                        | 234              |
        And I save the data
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Verkeer' and sector 'Koude start: overig' is correctly saved
        And the title for 'standard' road subsource '0' is 'Voorgeschreven factoren'
        And the time unit 'Koude starts /etmaal' is correctly saved
        And the following number of vehicles is correctly saved
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | 1.231,0          |
            | Middelzware vrachtvoertuigen  | 3.341,0          |
            | Zware vrachtvoertuigen        | 229,5            |
            | Bussen                        | 234,0            |
        And the total 'cold start' emission 'NOx' is '25,9 ton/j'
        And the total 'cold start' emission 'NH3' is '301,0 kg/j'

    Scenario: Add source Cold start: other - Emission factor toolkit with vehicles per month
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: overig'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I open the subsource dropdown
        When I click to add a emission source of type 'Koude start: Voorgeschreven factoren'
        And I select time unit '/maand' for cold starts
        And I fill in the following number of vehicles
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | 1231             |
            | Middelzware vrachtvoertuigen  | 3341             |
            | Zware vrachtvoertuigen        | 229.5            |
            | Bussen                        | 234              |
        And I save the data
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Verkeer' and sector 'Koude start: overig' is correctly saved                
        And the title for 'standard' road subsource '0' is 'Voorgeschreven factoren'
        And the time unit 'Koude starts /maand' is correctly saved
        And the following number of vehicles is correctly saved
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | 1.231,0          |
            | Middelzware vrachtvoertuigen  | 3.341,0          |
            | Zware vrachtvoertuigen        | 229,5            |
            | Bussen                        | 234,0            |
        And the total 'cold start' emission 'NOx' is '852,3 kg/j'
        And the total 'cold start' emission 'NH3' is '9,9 kg/j'

    Scenario: Add source Cold start: other - Emission factor toolkit with vehicles per year
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: overig'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        When I click to add a emission source of type 'Koude start: Voorgeschreven factoren'
        And I select time unit '/jaar' for cold starts
        And I fill in the following number of vehicles
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | 1231             |
            | Middelzware vrachtvoertuigen  | 3341             |
            | Zware vrachtvoertuigen        | 229.5            |
            | Bussen                        | 234              |
        And I save the data
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Verkeer' and sector 'Koude start: overig' is correctly saved              
        And the title for 'standard' road subsource '0' is 'Voorgeschreven factoren'
        And the time unit 'Koude starts /jaar' is correctly saved
        And the following number of vehicles is correctly saved
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | 1.231,0          |
            | Middelzware vrachtvoertuigen  | 3.341,0          |
            | Zware vrachtvoertuigen        | 229,5            |
            | Bussen                        | 234,0            |
        And the total 'cold start' emission 'NOx' is '71,0 kg/j'
        And the total 'cold start' emission 'NH3' is '0,8 kg/j'

    @Smoke
    Scenario: Add source Cold start: other - Custom specification with Euro class with vehicles per hour
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: overig'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        When I click to add a emission source of type 'Koude start: Eigen specificatie'
        And I choose Euro class 'Trekker - benzine - zwaar - pre Euro wetgeving (1992)' from custom
        And I choose time unit '/uur' with number of vehicles '100'
        And emission 'NOX' per vehicle is updated with '10.1464' 
        And emission 'NH3' per vehicle is updated with '0.003'
        And I save the data
        When I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Verkeer' and sector 'Koude start: overig' is correctly saved     
        And the title for 'specific' road subsource '0' is 'Eigen specificatie'
        And Euro class 'Trekker - benzine - zwaar - pre Euro wetgeving (1992)' is correctly saved
        And time unit '/uur' with number of cold starts '100' for Euro class is correctly saved
        And the total 'cold start' emission 'NOx' is '8.888,2 kg/j'
        And the total 'cold start' emission 'NH3' is '2,6 kg/j'

    Scenario: Add source Cold start: other - Custom specification with Euro class with vehicles per 24 hour
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: overig'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        When I click to add a emission source of type 'Koude start: Eigen specificatie'
        And I choose Euro class 'Trekker - diesel - licht - Euro-4 - N1 klasse 3' from custom
        And I choose time unit '/etmaal' with number of vehicles '1000'
        And emission 'NOX' per vehicle is updated with '0.14' 
        And emission 'NH3' per vehicle is updated with '0.002'
        And I save the data
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Verkeer' and sector 'Koude start: overig' is correctly saved         
        And the title for 'specific' road subsource '0' is 'Eigen specificatie'
        And Euro class 'Trekker - diesel - licht - Euro-4 - N1 klasse 3' is correctly saved
        And time unit '/etmaal' with number of cold starts '1.000' for Euro class is correctly saved
        And the total 'cold start' emission 'NOx' is '51,1 kg/j'
        And the total 'cold start' emission 'NH3' is '0,7 kg/j'

    Scenario: Add source Cold start: other - Custom specification with Euro class with vehicles per month
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: overig'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        When I click to add a emission source of type 'Koude start: Eigen specificatie'
        And I choose Euro class 'Vrachtauto - diesel - middelzwaar - Euro-2 - zwaar' from custom
        And I choose time unit '/maand' with number of vehicles '1500'
        And emission 'NOX' per vehicle is updated with '16.854904' 
        And emission 'NH3' per vehicle is updated with '0.003'
        And I save the data
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Verkeer' and sector 'Koude start: overig' is correctly saved               
        And the title for 'specific' road subsource '0' is 'Eigen specificatie'
        And Euro class 'Vrachtauto - diesel - middelzwaar - Euro-2 - zwaar' is correctly saved
        And time unit '/maand' with number of cold starts '1.500' for Euro class is correctly saved
        And the total 'cold start' emission 'NOx' is '303,4 kg/j'
        And the total 'cold start' emission 'NH3' is '54,0 g/j'

    Scenario: Add source Cold start: other - Custom specification with Euro class with vehicles per year
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: overig'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        When I click to add a emission source of type 'Koude start: Eigen specificatie'
        And I choose Euro class 'Personenauto - diesel - Euro-3 - halfopen filter' from custom
        And I choose time unit '/jaar' with number of vehicles '2500'
        And emission 'NOX' per vehicle is updated with '0.34' 
        And emission 'NH3' per vehicle is updated with '0.003'
        And I save the data
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Verkeer' and sector 'Koude start: overig' is correctly saved        
        And the title for 'specific' road subsource '0' is 'Eigen specificatie'
        And Euro class 'Personenauto - diesel - Euro-3 - halfopen filter' is correctly saved
        And time unit '/jaar' with number of cold starts '2.500' for Euro class is correctly saved
        And the total 'cold start' emission 'NOx' is '0,9 kg/j'
        And the total 'cold start' emission 'NH3' is '7,5 g/j'

    @Smoke
    Scenario: Source Cold start - selecting a different euro class will lead to different emission per vehicle
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: overig'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        When I click to add a emission source of type 'Koude start: Eigen specificatie'
        And I choose Euro class 'Personenauto - diesel - Euro-3 - halfopen filter' from custom
        And I choose time unit '/jaar' with number of vehicles '2500'
        And emission 'NOX' per vehicle is updated with '0.34' 
        And emission 'NH3' per vehicle is updated with '0.003'
        And I save the data
        And I select the source 'Bron 1'
        And I choose to edit the source
        And I select the subsource 'Euro klasse LPADEUR3HOF'
        When I choose Euro class 'Vrachtauto - diesel - middelzwaar - Euro-2 - zwaar' from specific
        Then emission 'NOX' per vehicle is updated with '16.854904' 
        And emission 'NH3' per vehicle is updated with '0.003'
        And I save the data
        Then the source 'Bron 1' with sector group 'Verkeer' and sector 'Koude start: overig' is correctly saved               
        And the title for 'specific' road subsource '0' is 'Eigen specificatie'
        And Euro class 'Vrachtauto - diesel - middelzwaar - Euro-2 - zwaar' is correctly saved
        And time unit '/jaar' with number of cold starts '2.500' for Euro class is correctly saved
        And the total 'cold start' emission 'NOx' is '42,1 kg/j'
        And the total 'cold start' emission 'NH3' is '7,5 g/j'
        
    Scenario: Add source Cold start: other - Custom specification with Euro class 'Other' - Light vehicles /hour
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: overig'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        When I click to add a emission source of type 'Koude start: Eigen specificatie'
        # Euro class 'Anders' is by default selected
        And I fill description with 'Eigen specificatie koude start - parkeergarage'
        And I select vehicle type 'Licht verkeer'
        And I choose time unit '/uur' with number of vehicles '500'
        And I fill emission 'NOX' per vehicle '0.2311'
        And I fill emission 'NH3' per vehicle '0.4453'
        And I save the data
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Verkeer' and sector 'Koude start: overig' is correctly saved     
        And the title for 'custom' road subsource '0' is 'Eigen specificatie'
        And description 'Eigen specificatie koude start - parkeergarage' is correctly saved        
        And time unit '/uur' with number of cold starts '500' for Euro class Other is correctly saved
        And vehicle type 'Licht verkeer' is correctly saved
        And the total 'cold start' emission 'NOx' is '1.012,2 kg/j'
        And the total 'cold start' emission 'NH3' is '1.950,4 kg/j'

    Scenario: Add source Cold start: other - Custom specification with Euro class 'Other' - Medium freight vehicles /24 hour
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: overig'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        When I click to add a emission source of type 'Koude start: Eigen specificatie'
        # Euro class 'Anders' is by default selected
        And I fill description with 'Eigen specificatie koude start - parkeergarage'
        And I select vehicle type 'Middelzwaar vrachtverkeer'
        And I choose time unit '/etmaal' with number of vehicles '1500'
        And I fill emission 'NOX' per vehicle '0.14'
        And I fill emission 'NH3' per vehicle '0.002'
        And I save the data
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Verkeer' and sector 'Koude start: overig' is correctly saved     
        And the title for 'custom' road subsource '0' is 'Eigen specificatie'
        And description 'Eigen specificatie koude start - parkeergarage' is correctly saved        
        And time unit '/etmaal' with number of cold starts '1.500' for Euro class Other is correctly saved
        And vehicle type 'Middelzwaar vrachtverkeer' is correctly saved
        And the total 'cold start' emission 'NOx' is '76,7 kg/j'
        And the total 'cold start' emission 'NH3' is '1,1 kg/j'

    Scenario: Add source Cold start: other - Custom specification with Euro class 'Other' - Heavy freight vehicles /month
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: overig'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        When I click to add a emission source of type 'Koude start: Eigen specificatie'
        # Euro class 'Anders' is by default selected
        And I fill description with 'Eigen specificatie koude start - parkeergarage'
        And I select vehicle type 'Zwaar vrachtverkeer'
        And I choose time unit '/maand' with number of vehicles '3000'
        And I fill emission 'NOX' per vehicle '0.2101'
        And I fill emission 'NH3' per vehicle '0.31011'
        And I save the data
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Verkeer' and sector 'Koude start: overig' is correctly saved     
        And the title for 'custom' road subsource '0' is 'Eigen specificatie'
        And description 'Eigen specificatie koude start - parkeergarage' is correctly saved        
        And time unit '/maand' with number of cold starts '3.000' for Euro class Other is correctly saved
        And vehicle type 'Zwaar vrachtverkeer' is correctly saved
        And the total 'cold start' emission 'NOx' is '7,6 kg/j'
        And the total 'cold start' emission 'NH3' is '11,2 kg/j'

    Scenario: Add source Cold start: other - Custom specification with Euro class 'Other' - Buses /year
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: overig'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        When I click to add a emission source of type 'Koude start: Eigen specificatie'
        # Euro class 'Anders' is by default selected
        And I fill description with 'Eigen specificatie koude start - parkeergarage'
        And I select vehicle type 'Busverkeer'
        And I choose time unit '/jaar' with number of vehicles '50000'
        And I fill emission 'NOX' per vehicle '5,02432'
        And I fill emission 'NH3' per vehicle '0,4'
        And I save the data
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Verkeer' and sector 'Koude start: overig' is correctly saved     
        And the title for 'custom' road subsource '0' is 'Eigen specificatie'
        And description 'Eigen specificatie koude start - parkeergarage' is correctly saved        
        And time unit '/jaar' with number of cold starts '50.000' for Euro class Other is correctly saved
        And vehicle type 'Busverkeer' is correctly saved
        And the total 'cold start' emission 'NOx' is '25.121,6 ton/j'
        And the total 'cold start' emission 'NH3' is '200,0 kg/j'

    Scenario: Copy and delete source Cold start: other - Emission factor toolkit
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: overig'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        And I click to add a emission source of type 'Koude start: Voorgeschreven factoren'
        When I select time unit '/uur' for cold starts
        And I fill in the following number of vehicles
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | 100              |
            | Middelzware vrachtvoertuigen  | 200              |
            | Zware vrachtvoertuigen        | 300              |
            | Bussen                        | 400              |
        And I save the data
        When I select the source 'Bron 1'
        And I copy the source 'Born 1'
        Then the source 'Bron 1 (1)' with sector group 'Verkeer' and sector 'Koude start: overig' is correctly saved
        And location with coordinates 'X:142250,05 Y:462711,05' is correctly saved
        And surface of location with value '162,44 ha' is correctly saved
        And the time unit 'Koude starts /uur' is correctly saved
        And the following number of vehicles is correctly saved
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | 100,0            |
            | Middelzware vrachtvoertuigen  | 200,0            |
            | Zware vrachtvoertuigen        | 300,0            |
            | Bussen                        | 400,0            |
        And the total 'cold start' emission 'NOx' is '133,4 ton/j'
        And the total 'cold start' emission 'NH3' is '1.226,0 kg/j'
        # delete source
        When I click on the 'delete source' button
        Then the next source does not exist 'Bron 1 (1)'
  
    Scenario: Copy and delete source Cold start: other - Custom specification with Euro class
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: overig'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        When I click to add a emission source of type 'Koude start: Eigen specificatie'
        And I choose Euro class 'Trekker - benzine - zwaar - pre Euro wetgeving (1992)' from custom
        And I choose time unit '/uur' with number of vehicles '100'
        And emission 'NOX' per vehicle is updated with '10.1464' 
        And emission 'NH3' per vehicle is updated with '0.003'
        And I save the data
        When I select the source 'Bron 1'
        And I copy the source 'Bron 1 (1)'
        Then the source 'Bron 1 (1)' with sector group 'Verkeer' and sector 'Koude start: overig' is correctly saved     
        And the title for 'specific' road subsource '0' is 'Eigen specificatie'
        And Euro class 'Trekker - benzine - zwaar - pre Euro wetgeving (1992)' is correctly saved
        And time unit '/uur' with number of cold starts '100' for Euro class is correctly saved
        And the total 'cold start' emission 'NOx' is '8.888,2 kg/j'
        And the total 'cold start' emission 'NH3' is '2,6 kg/j'
        # delete source
        When I click on the 'delete source' button
        Then the next source does not exist 'Bron 1 (1)'
  
    Scenario: Copy and delete source Cold start: other - Custom specification with Euro class 'Other'
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: overig'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        When I click to add a emission source of type 'Koude start: Eigen specificatie'
        # Euro class 'Anders' is by default selected
        And I fill description with 'Eigen specificatie koude start - parkeergarage'
        And I select vehicle type 'Licht verkeer'
        And I choose time unit '/uur' with number of vehicles '500'
        And I fill emission 'NOX' per vehicle '0.2311'
        And I fill emission 'NH3' per vehicle '0.4453'
        And I save the data
        When I select the source 'Bron 1'
        And I copy the source 'Bron 1'
        Then the source 'Bron 1 (1)' with sector group 'Verkeer' and sector 'Koude start: overig' is correctly saved     
        And the title for 'custom' road subsource '0' is 'Eigen specificatie'
        And description 'Eigen specificatie koude start - parkeergarage' is correctly saved        
        And time unit '/uur' with number of cold starts '500' for Euro class Other is correctly saved
        And vehicle type 'Licht verkeer' is correctly saved
        And the total 'cold start' emission 'NOx' is '1.012,2 kg/j'
        And the total 'cold start' emission 'NH3' is '1.950,4 kg/j'
        # delete source
        When I click on the 'delete source' button
        Then the next source does not exist 'Bron 1 (1)'

    Scenario: Validation on input source Cold start: parking garage - Emission factor toolkit
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: parkeergarage'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        And I click to add a emission source of type 'Koude start: Voorgeschreven factoren'
        When I select time unit '/uur' for cold starts
        And I fill in the following number of vehicles
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | -1               |
            | Middelzware vrachtvoertuigen  | -9999            |
            | Zware vrachtvoertuigen        | -2               |
            | Bussen                        | -1234            |
        And I save the data
        Then the following 'error' text is visible: "(-1) is geen geldige invoer"
        And the following 'error' text is visible: "(-9999) is geen geldige invoer"
        And the following 'error' text is visible: "(-2) is geen geldige invoer"
        And the following 'error' text is visible: "(-1234) is geen geldige invoer"

    Scenario: Validation on input source Cold start: parking garage - Custom specification
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: parkeergarage'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        When I click to add a emission source of type 'Koude start: Eigen specificatie'
        And I fill description with 'Eigen specificatie koude start - parkeergarage'
        When I choose time unit '/uur' with number of vehicles '-9999' 
        And I fill emission 'NOX' per vehicle '-0.22'
        And I fill emission 'NH3' per vehicle '-0.4453'
        And I save the data
        Then the following 'error' text is visible: "(-9999) is geen geldige invoer"
        And the following 'error' text is visible: "(-0.22) is geen geldige invoer"
        And the following 'error' text is visible: "(-0.4453) is geen geldige invoer"

    Scenario: Validation on input source Cold start: other - Emission factor toolkit
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: overig'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        And I click to add a emission source of type 'Koude start: Voorgeschreven factoren'
        When I select time unit '/jaar' for cold starts
        And I fill in the following number of vehicles
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | -1231            |
            | Middelzware vrachtvoertuigen  | -3341            |
            | Zware vrachtvoertuigen        | -229.5           |
            | Bussen                        | -234             |
        And I save the data
        Then the following 'error' text is visible: "(-1231) is geen geldige invoer"
        And the following 'error' text is visible: "(-3341) is geen geldige invoer"
        And the following 'error' text is visible: "(-229.5) is geen geldige invoer"
        And the following 'error' text is visible: "(-234) is geen geldige invoer"

    Scenario: Validation on input source Cold start: other - Custom specification
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Koude start: overig'
        And I fill location with coordinates 'POLYGON((141792.06 463152.32,141186.39 461975.6,142709.21 462252.48,143297.57 463446.5,141792.06 463152.32))'
        And I open the subsource dropdown
        And I click to add a emission source of type 'Koude start: Eigen specificatie'
        And I fill description with 'Eigen specificatie koude start - parkeergarage'
        And I select vehicle type 'Licht verkeer'
        When I choose time unit '/uur' with number of vehicles '-500'
        And I fill emission 'NOX' per vehicle '-0.2311'
        And I fill emission 'NH3' per vehicle '-0.4453'
        And I save the data
        Then the following 'error' text is visible: "(-500) is geen geldige invoer"
        And the following 'error' text is visible: "(-0.2311) is geen geldige invoer"
        And the following 'error' text is visible: "(-0.4453) is geen geldige invoer"

    Scenario: Edit source Cold start: parking garage to Cold start: other with building influence
        Given I login with correct username and password
        And I select file 'AERIUS_gml_cold_start.zip' to be imported
        And I open the source list
        And I select the source 'Bron 1'
        And the source 'Bron 1' with sector group 'Verkeer' and sector 'Koude start: parkeergarage' is correctly saved
        And the title for 'standard' road subsource '0' is 'Voorgeschreven factoren'
        And the time unit 'Koude starts /etmaal' is correctly saved
        And the following number of vehicles is correctly saved
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | 111,0            |
            | Middelzware vrachtvoertuigen  | 222,0            |
            | Zware vrachtvoertuigen        | 33,0             |
            | Bussen                        | 44,0             |
        And the building with name 'Gebouw 1', length '67,7 m', width '60,9 m', height '4,0 m' and orientation '80°' is saved and selected for the emission source
        And the building the source is influenced by is 'Gebouw 1'
        # Edit source Cold start: parking garage to Cold start: other
        When I choose to edit the source
        # And I open collapsible panel 'Bronkenmerken'
        And I select sector 'Koude start: overig'
        And I click to add a emission source of type 'Koude start: Voorgeschreven factoren'
        And I select time unit '/maand' for cold starts
        And I fill in the following number of vehicles
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | 1231             |
            | Middelzware vrachtvoertuigen  | 3341             |
            | Zware vrachtvoertuigen        | 229.5            |
            | Bussen                        | 234              |
        When I save the data
        Then the source 'Bron 1' with sector group 'Verkeer' and sector 'Koude start: overig' is correctly saved
        And the title for 'standard' road subsource '0' is 'Voorgeschreven factoren'
        And the time unit 'Koude starts /maand' is correctly saved
        And the following number of vehicles is correctly saved
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | 1.231,0          |
            | Middelzware vrachtvoertuigen  | 3.341,0          |
            | Zware vrachtvoertuigen        | 229,5            |
            | Bussen                        | 234,0            |
        And there is no building influence

    Scenario: Edit source Cold start: other  to Cold start: parking garage with building influence
        Given I login with correct username and password
        And I select file 'AERIUS_gml_cold_start.zip' to be imported
        And I open the source list
        And I select the source 'Bron 2'
        And the source 'Bron 2' with sector group 'Verkeer' and sector 'Koude start: overig' is correctly saved
        And the title for 'standard' road subsource '0' is 'Voorgeschreven factoren'
        And the time unit 'Koude starts /etmaal' is correctly saved
        And the following number of vehicles is correctly saved
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | 222,0            |
            | Middelzware vrachtvoertuigen  | 3,0              |
            | Zware vrachtvoertuigen        | 33,0             |
            | Bussen                        | 33,0             |
        And there is no building influence
        # Edit source Cold start: other to Cold start: parking garage
        When I choose to edit the source
        And I select sector 'Koude start: parkeergarage'
        And I click to add a emission source of type 'Koude start: Voorgeschreven factoren'
        And I select time unit '/maand' for cold starts
        And I fill in the following number of vehicles
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | 1231             |
            | Middelzware vrachtvoertuigen  | 3341             |
            | Zware vrachtvoertuigen        | 229.5            |
            | Bussen                        | 234              |
        And I open collapsible panel 'Bronkenmerken'
        And I toggle building influenced
        And I select the building 'Gebouw 1' from the primary building list
        And I save the data
        Then the source 'Bron 2' with sector group 'Verkeer' and sector 'Koude start: parkeergarage' is correctly saved
        And the title for 'standard' road subsource '0' is 'Voorgeschreven factoren'
        And the time unit 'Koude starts /maand' is correctly saved
        And the following number of vehicles is correctly saved
            | typeOfVehicle                 | numberOfVehicles |
            | Lichte voertuigen             | 1.231,0          |
            | Middelzware vrachtvoertuigen  | 3.341,0          |
            | Zware vrachtvoertuigen        | 229,5            |
            | Bussen                        | 234,0            |
        And the building with name 'Gebouw 1', length '67,7 m', width '60,9 m', height '4,0 m' and orientation '80°' is saved and selected for the emission source
        And the building the source is influenced by is 'Gebouw 1'
