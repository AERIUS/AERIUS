/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";
import { ADMSSourceCharacteristicsPage } from "./ADMSSourceCharacteristicsPage";

Given("I expect the source type dropdown to have the following options", (dataTable) => {
  ADMSSourceCharacteristicsPage.assertSourceTypeDropdownContains(dataTable);
});

Given("I expect the source type dropdown to not exist", () => {
  ADMSSourceCharacteristicsPage.assertSourceTypeDropdownDoesNotExist();
});

Given("I select the source type {string}", (sourceType) => {
  ADMSSourceCharacteristicsPage.selectSourceType(sourceType);
});

Given("I expect to see the following source characteristics options", (dataTable) => {
  ADMSSourceCharacteristicsPage.checkSourceCharacteristicsAvailability(dataTable);
});

Given("the available buoyancy types are", (dataTable) => {
  ADMSSourceCharacteristicsPage.assertAvailableBuoyancyTypesAre(dataTable);
});

Given("I select the buoyancy type {string}", (buoyancyType) => {
  ADMSSourceCharacteristicsPage.selectBuoyancyType(buoyancyType);
});

Given("there should be no buoyancy type dropdown", () => {
  ADMSSourceCharacteristicsPage.assertBuoyancyTypeDropdownDoesntExist();
});


Given("the available efflux types should be", (dataTable) => {
  ADMSSourceCharacteristicsPage.assertAvailableEffluxTypesAre(dataTable);
});

Given("I select the efflux type {string}", (effluxType) => {
  ADMSSourceCharacteristicsPage.selectEffluxType(effluxType);
});

Given("there should be no efflux type dropdown", () => {
  ADMSSourceCharacteristicsPage.assertEffluxTypeDropdownDoesntExist();
});

Given("I expect to see the following source characteristics in the detail panel", (dataTable) => {
  ADMSSourceCharacteristicsPage.checkShownSourceCharacteristicsOnDetailPanel(dataTable);
})

Given("I choose source height {string} and source diameter {string}", (sourceHeight, sourceDiameter) => {
  ADMSSourceCharacteristicsPage.chooseSourceCharacteristics(sourceHeight, sourceDiameter);
});

Given("I choose source height {string}", (sourceHeight) => {
  ADMSSourceCharacteristicsPage.chooseSourceHeight(sourceHeight);
});

Given("I choose buoyancy {string} {string}", (buoyancyType, buoyancyValue) => {
  ADMSSourceCharacteristicsPage.chooseBuoyancy(buoyancyType, buoyancyValue);
});

Given("I choose efflux {string} {string}", (buoyancyType, buoyancyValue) => {
  ADMSSourceCharacteristicsPage.chooseEfflux(buoyancyType, buoyancyValue);
});

Given("I choose specific heat capacity {string}", (specificHeatCapacity) => {
  ADMSSourceCharacteristicsPage.chooseSpecificHeatCapacity(specificHeatCapacity);
});

Given("source characteristics with emission height {string} is correctly saved", (emissionHeight) => {
  ADMSSourceCharacteristicsPage.sourceWithEmissionHeight(emissionHeight);
});

Given("source characteristics with source diameter {string} is correctly saved", (sourceDiameter) => {
  ADMSSourceCharacteristicsPage.sourceWithDiameter(sourceDiameter);
});

Given("source characteristics with buoyancy {string} {string} is correctly saved", (buoyancyType, buoyancyValue) => {
  ADMSSourceCharacteristicsPage.sourceWithBuoyancy(buoyancyType, buoyancyValue);
});

Given("source characteristics with efflux {string} {string} is correctly saved", (effluxType, effluxValue) => {
  ADMSSourceCharacteristicsPage.sourceWithEfflux(effluxType, effluxValue);
});

Given("efflux type {string} shouldn't exist", (effluxType) => {
  ADMSSourceCharacteristicsPage.assertEffluxTypeIsInvisible(effluxType);
});

Given("source characteristics with specific heat capacity {string} is correctly saved", (heatCapacity) => {
  ADMSSourceCharacteristicsPage.sourceWithSpecificHeatCapacity(heatCapacity);
});
