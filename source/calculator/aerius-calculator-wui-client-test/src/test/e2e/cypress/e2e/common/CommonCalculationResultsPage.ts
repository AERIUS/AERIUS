/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { CommonPage } from "./CommonPage";
import { CommonPage_UK } from "./CommonPage_UK";
import { CommonExportPage } from "./CommonExportPage";
import { CommonMaplayersPage } from "./CommonMaplayersPage";
import { CommonSettings } from "./CommonSettings";
import { VisualValidationId } from "./VisualValiationId";

export class CommonCalculationResultsPage {
  static count: number = 0;

  static uncaughtException() {
    Cypress.on("uncaught:exception", () => {
      /*
       * Returning false here prevents Cypress from
       * failing the test in case a unrelated error occurs
       */
      return false;
    });
  }

  static selectTemporaryResults() {
    cy.get('[type="checkbox"]').check({ force: true });
  }

  static interceptTheStatusOfTheCalculation() {
    this.uncaughtException();
    this.interceptStartCalculationStatus();
  }

  static interceptStartCalculationStatus() {
    cy.intercept("GET", "/api/v8/ui/calculation/*", (request: any) => {
      request.reply((response: any) => {
        // Force a complete
        response.body.jobProgress.state = "COMPLETED";
        response.body.jobProgress.numberOfPointsCalculated = 9999;
        return response;
      });
    }).as("startCalculation");
  }

  static interceptResult(urlPath: string, jsonFile: string, dataTable: any) {
    let countReceptorsExpectedResult = "";
    let sumCartographicSurfaceExpectedResult = "";
    let maxContributionExpectedResult = "";
    let sumContributionExpectedResult = "";
    let maxTotalExpectedResult = "";
    let maxDecreaseExpectedResult = "";
    let maxIncreaseExpectedResult = "";
    let sumCartographicSurfaceIncreaseExpectedResult = "";
    let sumCartographicSurfaceDecreaseExpectedResult = "";
    let maxPercentageCriticalLevelExpectedResult = "";
    let maxTempContributionExpectedResult = "";
    let maxTempIncreaseExpectedResult = "";
    let procurementPolicyThresholdPercentageExpectedResult = "";
    let countReceptorsIncreaseExpectedResult = "";
    let countReceptorsDecreaseExpectedResult = "";

    if (dataTable != null) {
      dataTable.hashes().forEach((statistic: any) => {
        let statisticTypeResult = statistic.statisticType;
        switch (statisticTypeResult) {
          case "countReceptors":
            countReceptorsExpectedResult = statistic.ExpectedStatistics;
            return;
          case "countReceptorsIncrease":
            countReceptorsIncreaseExpectedResult = statistic.ExpectedStatistics;
            return;
          case "countReceptorsDecrease":
            countReceptorsDecreaseExpectedResult = statistic.ExpectedStatistics;
            return;      
          case "sumCartographicSurface":
            sumCartographicSurfaceExpectedResult = statistic.ExpectedStatistics;
            return;
          case "sumContribution":
            sumContributionExpectedResult = statistic.ExpectedStatistics;
            return;
          case "maxContribution":
              maxContributionExpectedResult = statistic.ExpectedStatistics;
              return;
          case "maxTotal":
            maxTotalExpectedResult = statistic.ExpectedStatistics;
            return;
          case "maxDecrease":
            maxDecreaseExpectedResult = statistic.ExpectedStatistics;
            return;
          case "maxIncrease":
            maxIncreaseExpectedResult = statistic.ExpectedStatistics;
            return;
          case "sumCartographicSurfaceIncrease":
            sumCartographicSurfaceIncreaseExpectedResult = statistic.ExpectedStatistics;
            return;
          case "sumCartographicSurfaceDecrease":
            sumCartographicSurfaceDecreaseExpectedResult = statistic.ExpectedStatistics;
            return;
          case "maxPercentageCriticalLevel":
            maxPercentageCriticalLevelExpectedResult = statistic.ExpectedStatistics;
            return;
          case "maxTempContribution":
            maxContributionExpectedResult = statistic.ExpectedStatistics;
            return;
          case "maxTempIncrease":
            maxTempIncreaseExpectedResult = statistic.ExpectedStatistics;
            return;
          case "procurementPolicyThresholdPercentage":
            procurementPolicyThresholdPercentageExpectedResult = statistic.ExpectedStatistics;
            return;
          default:
            throw new Error("Unknown calculation statisticType: " + statisticTypeResult);
        }
      });
    }
    cy.readFile("cypress/fixtures/results/" + jsonFile).then((responseJson) => {
      cy.intercept("GET", urlPath, (request) => {
        request.reply((response: any) => {
          Object.assign(response.body, responseJson);
          if (sumCartographicSurfaceExpectedResult) {
            response.body.statistics.sumCartographicSurface = sumCartographicSurfaceExpectedResult
              ? sumCartographicSurfaceExpectedResult
              : null;
          }
          if (countReceptorsIncreaseExpectedResult) {
            response.body.statistics.countReceptorsIncrease = countReceptorsIncreaseExpectedResult;
          }
          if (countReceptorsDecreaseExpectedResult) {
            response.body.statistics.countReceptorsDecrease = countReceptorsDecreaseExpectedResult;
          }
          if (countReceptorsExpectedResult) {
            response.body.statistics.countReceptors = countReceptorsExpectedResult;
          }
          if (sumContributionExpectedResult) {
            response.body.statistics.sumContribution = sumContributionExpectedResult;
          }
          if (maxContributionExpectedResult) {
            response.body.statistics.maxContribution = maxContributionExpectedResult;
          }
          if (maxTotalExpectedResult) {
            response.body.statistics.maxTotal = maxTotalExpectedResult;
          }
          if (maxDecreaseExpectedResult) {
            response.body.statistics.maxDecrease = maxDecreaseExpectedResult;
          }
          if (maxIncreaseExpectedResult) {
            response.body.statistics.maxIncrease = maxIncreaseExpectedResult;
          }
          if (sumCartographicSurfaceIncreaseExpectedResult) {
            response.body.statistics.sumCartographicSurfaceIncrease = sumCartographicSurfaceIncreaseExpectedResult;
          }
          if (sumCartographicSurfaceDecreaseExpectedResult) {
            response.body.statistics.sumCartographicSurfaceDecrease = sumCartographicSurfaceDecreaseExpectedResult;
          }
          if (maxPercentageCriticalLevelExpectedResult) {
            response.body.statistics.maxPercentageCriticalLevel = maxPercentageCriticalLevelExpectedResult;
          }
          if (maxTempContributionExpectedResult) {
            response.body.statistics.maxTempContribution = maxTempContributionExpectedResult;
          }
          if (maxTempIncreaseExpectedResult) {
            response.body.statistics.maxTempIncrease = maxTempIncreaseExpectedResult;
          }
          if (procurementPolicyThresholdPercentageExpectedResult) {
            response.body.statistics.procurementPolicyThresholdPercentage = procurementPolicyThresholdPercentageExpectedResult;
          }
          return response;
        });
      }).as(jsonFile);
    });
  }

  static validateSummaryScenarioContributionCount(numberOfSites: string) {
    cy.contains("Scenario contributions (SC)").parent().find("tbody").children().should("have.length", numberOfSites);
  }

  static validateSummaryProcessContributionsCount(numberOfSites: string) {
    cy.contains("Process contributions (PC)").parent().parent().find("tbody").children().should("have.length", numberOfSites);
  }

  static validateSummaryProcessContributionTableValues(dataTable: any) {
    cy.get("table").last().scrollIntoView().within( () => {
      dataTable.hashes().forEach((rowValue: any, row: number) => {
        for(var idName in rowValue) {
          cy.get('[id="' + idName + '-' + row + '"]').contains(rowValue[idName]);
        }
      });
    });
  }

  static validateMarkersProcessContributionTableValues(dataTable: any) {
    cy.get("table").scrollIntoView().within( () => {
      dataTable.hashes().forEach((rowValue: any, row: number) => {      
        for(var idName in rowValue) {
          if(idName.indexOf("resulttype") > -1) {
            cy.get('[id="' + "area_statistics-" + row + "-" + idName + '"]').contains(rowValue[idName]);
          } else {
            cy.get('[id="' + idName + '-' + row + '"]').contains(rowValue[idName]);
          }
        }
      });
    });
  }

  static validateHabitatAndSpeciesProcessContributionTableValues(dataTable: any) {
    dataTable.hashes().forEach((rowValue: any, row: number) => {
      for(var idName in rowValue) {
        if(idName.indexOf("area-title") > -1) {
          cy.get('[id="' + this.getSiteNameId(rowValue[idName]) + "-line" + '"]').scrollIntoView().click();
        } else {
          cy.get("table").scrollIntoView().within( () => {
            cy.get('[id="' + idName + '-' + row + '"]').contains(rowValue[idName]);
          });
        }
      }
    });
  }

  static validateResultScreen(situationName: string, calculationType: string, resultType: string, dataTable: any) {
    cy.get('[id="resultsSituation"]').select(situationName);
    cy.get('[id="resultsResultType"]')
      .select(calculationType)
      .then(() => {
        cy.get("[id=resultsHexagonType]")
          .select(resultType)
          .then(() => {
            cy.get(".statistics-container", { timeout: CommonSettings.resultsLoadingTimeOut });
            cy.get(".tab-header-container", { timeout: CommonSettings.resultsLoadingTimeOut });
            this.validateResultByElementIds(dataTable);
          });
      });
  }

  static validateSituationNameCalculationTypePollutantResultTypeScreen(situationName: string, calculationType: string, pollutant: string, resultType: string, dataTable: any) {
    cy.get('[id="resultsSituation"]').select(situationName);
    cy.get('[id="resultsResultType"]')
      .select(calculationType)
      .then(() => {
        cy.get('[id="resultsEmission"]')
          .select(pollutant)
          .then(() => {
            cy.get("[id=resultsHexagonType]")
            .select(resultType)
            .then(() => {
              this.validateResultByElementIds(dataTable);
            });
          });
      });
  };

  static validateResultByElementIds(dataTable: any) {
    if (dataTable != undefined) {
      dataTable.hashes().forEach((element: any) => {
        cy.get("[id=" + element.elementId + "]", { timeout: CommonSettings.getElementLongTimeOut }).should("have.text", element.expectedResult); 
      });
    };
  }

  static validateCalculationResultsTab(tabName: string) {
    switch (tabName) {
      case "Scenarios":
        cy.get("[id=results-tab-CALCULATED_INFO]").click().should("have.text", tabName);
        return;
      case "Summary":
        cy.get("[id=results-tab-SUMMARY]").click().should("have.text", tabName);
        return;
      case "Decision Framework":
          cy.get("[id=results-tab-DECISION_FRAMEWORK]").click().should("have.text", tabName);
          return;
      case "Results":
        cy.get("[id=results-tab-SCENARIO_RESULTS]").click().should("have.text", tabName);
        return;
      default:
        throw new Error("Unknown calculation resultsscreen tab: " + tabName);
    }
  }

  static validateArchiveProjectResultRetrieveDateTime(retrievedDateTime: string) {
    cy.get(".archiveProjectsContainer").contains(retrievedDateTime);
  }

  static validateCalculationResultsTabWithContent(tabName: string, dataTable: any, useIndex = false) {
    switch (tabName) {
      case "Scenarios":
        cy.get("[id=results-tab-CALCULATED_INFO]").click().should("have.text", tabName);
        dataTable.hashes().forEach((element: any) => {
          if (useIndex && element.elementIndex > 0) {
            cy.get(element.elementId, { timeout: CommonSettings.getElementLongTimeOut }).eq(element.elementIndex-1).should("include.text", element.expectedResult);
          } else {
            cy.get(element.elementId, { timeout: CommonSettings.getElementLongTimeOut }).should("include.text", element.expectedResult);
          }
        });
        return;
      default:
        throw new Error("Unknown calculation resultsscreen tab: " + tabName);
    }
  }

  static validateArchiveHyperlinkContents(dataTable: any) {
    cy.get(".archiveProjectsContainer").within(() => {
      cy.get("a")
      .invoke('attr', 'href')
      .then((href: any) => {      
        expect(href).to.contains("https://register-dev.aerius.uk/archive?projectIds=");
        dataTable.hashes().forEach((element: any) => {
          expect(href).to.contains(element.projectId);
        });
      });
    });
  };

  static validateCalculationPointsResult(dataTable: any) {
    cy.get(".table > .body")
      .within( () => {
        dataTable.hashes().forEach((element: any) => {
          cy.contains(".cell-id", new RegExp('^' + element.id + '$')).parent().contains(".cell-name", element.name);
          cy.contains(".cell-id", new RegExp('^' + element.id + '$')).parent().contains(".cell-value", element.expectedResult);
        });
      });
  }

  static validateProcurementPolicyThreshold(productProfile: string, dataTable: any) {
    switch (productProfile) {
      case "Lbv":
        dataTable.hashes().forEach((assessmentArea: any) => {
          cy.get("[id=" + assessmentArea.assessmentAreaId + "]");
          cy.get("[id=procurement-policy-area-" + assessmentArea.assessmentAreaId + "-name]").should("contain.text", assessmentArea.assessmentAreaName);
          cy.get("[id=procurement-policy-area-" + assessmentArea.assessmentAreaId + "-threshold]").should("contain.text", assessmentArea.procurementPolicyThresholdValue);
          cy.get("[id=procurement-policy-area-" + assessmentArea.assessmentAreaId + "-value]").should("contain.text", assessmentArea.sumContribution);

          if (assessmentArea.procurementPolicyThresholdCheck == "true") {
            cy.get("[id=procurement-policy-area-" + assessmentArea.assessmentAreaId + "-check]").find("i").should("have.class", "icon-checked");
          } else {
            cy.get("[id=procurement-policy-area-" + assessmentArea.assessmentAreaId + "-check]").find("i").should("have.class", "icon-close");
          }
          cy.get("[id=" + assessmentArea.assessmentAreaId + "]")
            .find("threshold-indicator-component")
            .invoke('attr', 'deposition')
            .as('depositionValue');
          cy.get('@depositionValue').then((depositionValue) => {
            expect(depositionValue).to.contain(assessmentArea.procurementPolicyThresholdPercentage);
          });
        });
        return;
      case "Lbv-plus":
        // Lbv-plus should always have 1 row
        cy.get("[id=procurement-policy-row-container").children().should("have.length", 1);

        dataTable.hashes().forEach((element: any) => {
          cy.get("[id=procurement-policy-overall-checked");
          cy.get("[id=procurement-policy-overall-threshold]").should("contain.text", element.procurementPolicyThresholdValue);
          cy.get("[id=procurement-policy-overall-value]").should("contain.text", element.sumContribution);

          if (element.procurementPolicyThresholdCheck == "true") {
            cy.get("[id=procurement-policy-overall-checked").find("i").should("have.class", "icon-checked");
          } else {
            cy.get("[id=procurement-policy-overall-checked").find("i").should("have.class", "icon-close");
          }
          cy.get("[id=procurement-policy-overall-checked")
            .parent()
            .find("threshold-indicator-component")
            .invoke('attr', 'deposition')
            .as("overallDepostionValue");
          cy.get('@overallDepostionValue').then((overallDepostionValue) => {
              expect(overallDepostionValue).to.contain(element.procurementPolicyThresholdPercentage);
            });
        });
        return;
      default:
          throw new Error("Unknown product profile for procurement policy threshold: " + productProfile);
    }
  }


  // Count natural area entries
  static validateTab(tabType: string, numberOfItems: string) {
    cy.get("[id=" + tabType + "]").click();
    cy.get(".collapsible-container > div").children().should("have.length", numberOfItems);
  }

  static validateCriticalLevelsTab(tabType: string, expectedtext: string) {
    cy.get("[id=" + tabType + "]").click();
    cy.get(".tabPanel > .info").should("have.text", expectedtext);
  }

  // Count markers
  static validateMarkersTab(numberOfMarkers: string, expectedtext: string) {
    cy.get("[id=tab-MARKERS]").click();
    cy.get(".tabPanel > .table > .row").should("have.length", numberOfMarkers);
    cy.get(".tabPanel > .table > .header > .header-item:first > span").should("have.text", expectedtext);
  }

  static validateOmittedAreas(numberOfResults: string) {
    cy.get("[id=omittedAreasList]").should("have.length", numberOfResults);
  }

  // Validate hexagons
  static validateHexagons(area: string, numberOfhexagons: number, dataTable: any) {
    cy.get("[id=" + area + "-content] > table > tbody")
      .children()
      .should("have.length", numberOfhexagons);
    dataTable.hashes().forEach((element: any) => {
      cy.get("[id=" + element.elementId + "]").should("contain.text", element.expectedResult);
    });
  }

  static validateCalculationPoints(numberOfResults: number) {
    cy.get(".table > .body")
    .children()
    .should("have.length", numberOfResults);
  }

  static navigateResultScreenCombinatations(situationName: string, dataTable: any) {
    CommonPage.navigateTo("Results");
    cy.get('[id="resultsSituation"]');
    cy.get('[id="resultsSituation"]').select(situationName);
    dataTable.hashes().forEach((element: any) => {
      cy.log(element.resultSelectId);
      cy.log(element.resultSelectLabel);
      cy.get('[id="' + element.resultSelectId + '"]').select(element.resultSelectLabel);
    });
  }

  static validateResultScreenCombinationsCount(situationName: string,  resultType: string, rowCount: string) {
    CommonPage.navigateTo("Results");
    cy.get('[id="resultsSituation"]').select(situationName);
    cy.get('[id="resultsResultType"]').select(resultType);
    cy.get('[id="resultsHexagonType"]').find('option').should('have.length', rowCount);
  }

  static validateSiteExpectedText(siteName: string, expectedtext: string) {
    cy.get('[id="' + this.getSiteNameId(siteName) + "-line" + '"]').scrollIntoView().click();
    cy.get('[id="' + this.getSiteNameId(siteName) + '"]').contains(expectedtext);
  }

  static validateSiteExpectedTextNoText(siteName: string) {
    cy.get('[id="' + this.getSiteNameId(siteName) + "-line" + '"]').scrollIntoView().click();
    cy.get('[id="' + this.getSiteNameId(siteName) + '"]').within( () => {
      cy.get(".info").should('not.exist');
    });
  }

  static getSiteNameId(siteName: string) : string {
    return siteName.toLowerCase().replaceAll(' ', '-');
  }

  static selectMeteorologicalSiteLocation(location: string) {
    CommonPage_UK.openCalculationJobPanel("Meteorological settings");
    cy.get('[id="siteLocation"]').select(location, {force:true});
  }

  static searchMeteorologicalSiteLocation(location: string) {
    CommonPage_UK.openCalculationJobPanel("Meteorological settings");
    cy.get('[aria-labelledby="tab-metSiteSelection-SEARCH"]')
    .within(() => {
      cy.get('input').type(location);
  });
  }

  static validateMeteorologicalSiteLocationCount(optionsCount: string) {
    CommonPage_UK.openCalculationJobPanel("Meteorological settings");
    cy.get('select[id="siteLocation"]').within(() => { 
      cy.get('option').should('have.length', optionsCount);
     })
  }

  static selectFirstMeteorologicalSiteLocation() {
    CommonPage_UK.openCalculationJobPanel("Meteorological settings");
    cy.get('[id="siteLocation"]').select(1);
  }

  static selectMeteorologicalYear(year: string) {
    CommonPage_UK.openCalculationJobPanel("Meteorological settings");
    cy.get('[id="siteYears"]').select(year);
  }

  static validateMeteorologicalYearNotSelectable() {
    CommonPage_UK.openCalculationJobPanel("Meteorological settings");
    cy.get('[id="siteYears"]').should('have.length', 1);
    cy.get('[id="siteYears"]').parent().get('[id="inputHintMessage"]').should('have.text', 'Select a meteorological site location to choose a specific meteorological year.');
  }

  static startUKCalculationAndValidateResultScreen() {

    Cypress.on("uncaught:exception", () => {
      /*
       * Returning false here prevents Cypress from
       * failing the test in case a unrelated error occurs
       */
      return false;
    });

    cy.intercept("/api/v8/ui/calculation/*").as("getCalculation"); // Intercept the url that is triggered after clicking the calculate button
    cy.get(".startButton").click();
    cy.wait("@getCalculation").then((response: any) => {
      if (response.response.body != "") {
        // store jobId in global variable
        cy.wrap(response.response.body.jobProgress.key).as('jobKey'); 
        CommonExportPage.checkTaskStatus("COMPLETED");
      }
    });    
   
    cy.log("let start validating");
  }

  static startUKCalculationMockFinishAndValidateResultScreen() {
    Cypress.on("uncaught:exception", () => {
      /*
       * Returning false here prevents Cypress from
       * failing the test in case a unrelated error occurs
       */
      return false;
    });

    cy.readFile("cypress/fixtures/results/uk_mock_calculation_jobprogress_completed.json" ).then((responseJson) => {
      cy.intercept("GET", "/api/v8/ui/calculation/*", (request) => {
        request.reply((response: any) => {
          Object.assign(response.body, responseJson);
          return response;
        });
      }).as("uk_mock_calculation_jobprogress_completed");
    });
    cy.get(".startButton").click();
    cy.get('[href="nca/results/"]').click();

    // Select calculation job
    cy.get('[id="selectedCalculation"]').select("Calculation job 1");

    // Check header name
    cy.get('[id="results-tab-SCENARIO_RESULTS"]').should("have.text", "Results");
  }

  static selectCalulationScenario(situationType: string) {
    switch (situationType) {
      case "TEMPORARY":
      case "COMBINATION_PROPOSED":
      case "COMBINATION_REFERENCE":
        cy.get('[id^="otherSituation-' + situationType + '"]').check({ force: true });
        return;
      default:
        throw new Error("Unknown calculation situationType: " + situationType);
    }
  }

  static selectInCombinationIncludeArchive() {
    cy.get('[id="includeArchiveProjects"]').check({force: true});
  }

  static selectInCombinationScenario(scenario: string) {
    cy.get("label").contains(scenario).parent().parent().get('[type="checkbox"]').check({force: true});
  }

  static deleteCurrentCalculationJob() {
    cy.get("#calculation-job-sourceListButtonDeleteSource").click();
  }

  static selectNatureAreaValidateMap(dataTable: any, polutantCategory: string) {
    let interceptLayer: any;
    if (polutantCategory == "Deposition") {
      interceptLayer = /.*deposition.*/;
    } else if (polutantCategory == "Concentration") {
      interceptLayer = /.*sub_results.*/;
    } else {
      throw new Error("Unknown polutant category: " + polutantCategory);
    }
    dataTable.hashes().forEach((natureArea: any) => {
      const interceptAlias = `wmsResult_${this.count++}`;
      CommonCalculationResultsPage.zoomToNatureAreaAndCompareMap(
        interceptLayer,
        interceptAlias,
        natureArea.name,
        parseInt(natureArea.pathsCount),
        natureArea.bbox,
      );
    });
  }

  static zoomToNatureAreaAndCompareMap(interceptLayer: RegExp, interceptAlias: string, natureAreaName: any, pathsCount: number, bbox: any) {
    this.zoomToNatureAreaWithLayerDisabledThenIntercept(
      interceptLayer,
      interceptAlias,
      "nl.overheid.aerius.wui.application.geo.layers.results.CalculationResultLayerGroup-line",
      natureAreaName
    );
    this.validateResultOnTheMapInJson(`@${interceptAlias}`, natureAreaName, pathsCount, bbox);
  }

  /*
   * When zooming to another area, there is a small animation which makes it harder to capture the correct request.
   * To avoid that, this function first disables the layer, then zooms to the nature site, waits a bit, starts the intercept and then enables the layer again.
   * Now the request that is captured should reliably happen with the same view port.
   *
   * This function relies on not being zoomed to the correct location yet.
   * If it has, no new network requests will happen and the intercept will do nothing.
   */
  static zoomToNatureAreaWithLayerDisabledThenIntercept(
    interceptLayer: RegExp,
    interceptAlias: string,
    disableLayerId: string,
    natureAreaName: any
  ) {
    cy.intercept("GET", interceptLayer, { times: 1 }).as(interceptAlias);

    // Assuming map layer panel is closed, clicking to open.
    CommonMaplayersPage.selectMapbutton("LayerPanel");
    // Assuming layer is enabled, clicking to disable
    cy.get(`[id="${disableLayerId}"]`).within(() => {
      cy.get(".toggle").click();
    });
    // Wait for results to finish loading
    cy.get(".results-loading", { timeout: CommonSettings.resultsLoadingTimeOut }).should("not.exist");
    // Zooming to the area
    this.selectNatureArea(natureAreaName);
    // Wait a bit to give zoom animation some time
    cy.wait(1_000);
    // Click layer again to enable, causing the correct network request
    cy.get(`[id="${disableLayerId}"]`).within(() => {
      cy.get(".toggle").click();
    });
    // Clicking map layer panel again to close.
    CommonMaplayersPage.selectMapbutton("LayerPanel");
  }

  static selectNatureAreaValidateTotalPercentageCLMap(dataTable: any) {
    cy.intercept("GET", /.*wms_project_calculation_total_percentage_cl_hexagon.*/, { times: dataTable.length }).as(
      "wmsResultTotalPercentageCLMapLayers"
    );
    CommonMaplayersPage.selectMapbutton("LayerPanel");
    dataTable.hashes().forEach((natureArea: any) => {
      this.selectNatureArea(natureArea.name);
      cy.get('[id="TotalPercentageCriticalLevelResultLayer-line"]').within(() => {
        cy.get(".toggle").click();
      });
      this.validateResultOnTheMapInJson("@wmsResultTotalPercentageCLMapLayers", natureArea.name, parseInt(natureArea.pathsCount), natureArea.bbox);
      cy.get('[id="TotalPercentageCriticalLevelResultLayer-line"]').within(() => {
        cy.get(".toggle").click();
      });
    });
    // close the map layer
    CommonMaplayersPage.selectMapbutton("LayerPanel");
  }

  static selectNatureAreaValidatePercentageCLMap(dataTable: any) {
    cy.intercept("GET", /.*wms_project_calculation_percentage_cl_hexagon.*/, {times: dataTable.length}).as("wmsResultPercentageCLMapLayers");
    CommonMaplayersPage.selectMapbutton("LayerPanel");
    dataTable.hashes().forEach((natureArea: any) => {
      this.selectNatureArea(natureArea.name);
      cy.get('[id="PercentageCriticalLevelResultLayer-line"]').within( () => {
        cy.get(".toggle").click();
      });
      this.validateResultOnTheMapInJson("@wmsResultPercentageCLMapLayers", natureArea.name, parseInt(natureArea.pathsCount), natureArea.bbox);
      cy.get('[id="PercentageCriticalLevelResultLayer-line"]').within( () => {
        cy.get(".toggle").click();
      });
    });
    // close the map layer
    CommonMaplayersPage.selectMapbutton("LayerPanel");
  }

  static selectNatureArea(natureAreaName: string) {
    const id = natureAreaName.toLowerCase().replaceAll(" ", "-") + "-line";
    cy.get('[id="' + id + '"]').scrollIntoView();
    cy.get('[id="' + id + '"] > button').click({force: true});
  }

  static validateResultOnTheMapInJson(waitAlias: any, natureAreaName: string, pathCounts: number, bbox: any) {
    cy.wait(waitAlias, { timeout: CommonSettings.resultsLoadingTimeOut }).then((interception: any) => {
      let requestUrl = interception.request.url.replace("png","svg");
      if (bbox != undefined) {
        // If specified replace the BBOX.
        requestUrl = requestUrl.substring(0, requestUrl.indexOf("BBOX")) + bbox;
      }
      cy.request(requestUrl).as('svgRequest');
    });
    cy.get("@svgRequest", { timeout: CommonSettings.resultsLoadingTimeOut }).then((interception: any) => {
      let outputSvgJson: any = this.svg2json(interception.body);
      cy.log(natureAreaName + "Expect paths : " + pathCounts + " actualy : " + outputSvgJson.length);
      expect(outputSvgJson.length).to.equal(pathCounts, natureAreaName);
    });
  }

  static svg2json(xmlString: any) {
    let svgPaths: any = [];
    let xmlDoc: any = Cypress.$.parseXML(xmlString);
    let childNodes: any = xmlDoc.documentElement.childNodes;
    childNodes.forEach((childNode: any) => {
      if (childNode.nodeName == "g") {
        let svgMembers = childNode.childNodes;
        svgMembers.forEach((svgMember: any) => {
          if (svgMember.nodeName == "g") {
            let groupSvgMembers = svgMember.childNodes;
            groupSvgMembers.forEach((groupSvgMember: any) => {
              if (groupSvgMember.nodeName == "path") {
                svgPaths.push({
                  d: groupSvgMember.getAttribute("d"),
                });
              }
            });
          }
        });
      }
    });
    return svgPaths;
  };

  static validateArchiveProjectSitesOnMap() {
    cy.get(".ol-map").compareSnapshot(VisualValidationId.ARCHIVE_PROJECT_SITE_MAP);
  }

  static validateDecisionMakingThresholdsinfoMessage(message: string) {
    cy.get('[id="inputNoticeMessage"]').first().should("contain", message);
  }

  static validateSiteRelevantThresholdsinfoMessage(message: string) {
    cy.get('[id="inputNoticeMessage"]').last().should("contain", message);
  }

  static validateDecisionMakingThresholds(dataTable: any) {
    dataTable.hashes().forEach((threshold: any, index: number) => {
      this.validateThresholdRow("dmt", threshold, index);
    });
  }

  static validateSiteRelevantThresholds(siteName: string, siteId: string, dataTable: any) {
    const id = siteName.replaceAll(" ", "-").toLowerCase();
    cy.get('[id="' + id + '-line"]').click();
    cy.get('[id="' + id + '-content"]').within( () => {
      dataTable.hashes().forEach((threshold: any, index: number) => {
        this.validateThresholdRow(siteId, threshold, index);
      });
    });
  }

  static validateThresholdRow(kind: string, threshold: any, index: number) {
    let id = "";
    if (index == 0) {
      id = kind + "-EmissionResultKey-[Substance-[NOx+NH3:1711],-DEPOSITION]";
    } else if (index == 1) {
      id = kind + "-EmissionResultKey-[Substance-[NOx:11],-CONCENTRATION]";
    } else {
      id = kind + "-EmissionResultKey-[Substance-[NH3:17],-CONCENTRATION]";
    };
    let className = "";
    if (threshold.relevantThreshold == "true") {
      className = "icon-checked";
    } else if (threshold.relevantThreshold == "false") {
      className = "icon-close";
    }
    cy.get('[id="' + id + '"]').within( () => {
      if (className != "") {
        cy.get('[id="site-relevant-threshold-checked"] > i').should('have.class', className);
        cy.get("threshold-indicator-component").should('have.attr', 'percentage', threshold.compairThresholdRaw );
        cy.get("threshold-indicator-component").shadow().find(".score").should("contain", threshold.compairThreshold);
      }
      cy.get('[id="site-relevant-threshold-' + kind + '-pollutant-'+ index + '"]').should("contain", threshold.pollutant);
      cy.get('[id="site-relevant-threshold-' + kind + '-contribution-'+ index + '"]').should("contain", threshold.contribution);
      cy.get('[id="site-relevant-threshold-' + kind + '-threshold-'+ index + '"]').should("contain", threshold.threshold);
    });
  }
}
