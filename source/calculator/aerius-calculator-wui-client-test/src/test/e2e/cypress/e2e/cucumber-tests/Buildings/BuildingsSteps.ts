/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";
import { BuildingsPage } from "./BuildingsPage";

Given(
  "the building with name {string}, length {string}, width {string}, height {string} and orientation {string} is saved and selected for the emission source",
  (name, length, width, height, orientation) => {
    BuildingsPage.assertPolygonalBuildingSavedAndSelectedForEmissionSource(name, length, width, height, orientation);
  }
);

Given("I delete all buildings", () => {
  BuildingsPage.deleteAllBuildings();
});

Given("I select the building {string} as the primary building", (name) => {
  BuildingsPage.selectBuildingAsPrimaryBuilding(name);
});

Given("no building is selected", () => {
  BuildingsPage.assertNoBuildingSelected();
});

Given("I open the bronkenmerken panel", () => {
  BuildingsPage.openBronkenmerkenPanel();
});
