/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class ADMSSourceCharacteristicsPage {
  static assertSourceTypeDropdownContains(dataTable: any) {
    dataTable.hashes().forEach((sourceType: any) => {
      cy.get('[id="sourceType"] option').should("contain.text", sourceType.option);
    });
  }

  static assertSourceTypeDropdownDoesNotExist() {
    cy.get('[id="sourceType"]').should("not.exist");
  }

  static selectSourceType(sourceType: string) {
    cy.get('[id="sourceType"]').select(sourceType);
  }

  static checkSourceCharacteristicsAvailability(dataTable: any) {
    dataTable.hashes().forEach((characteristic: any) => {
      if (characteristic.shouldBeVisible == "false") {
        cy.get("[id=" + characteristic.testId + "]").should("not.exist");
      } else {
        switch (characteristic.type) {
          case "checkbox":
            this.checkCheckboxSourceCharacteristicIsAvailable(characteristic);
            break;
          case "text":
            this.checkTextSourceCharacteristicIsAvailable(characteristic);
            break;
          case "dropdown":
            this.checkDropdownSourceCharacteristicIsAvailable(characteristic);
            break;
          default:
            throw new Error("unknown characteristic type: " + characteristic.type);
        }
      }
    });
  }

  static checkCheckboxSourceCharacteristicIsAvailable(characteristic: any) {
    if (characteristic.expectedValue == "true") {
      cy.get("[id=" + characteristic.testId + "]").should("be.checked");
    } else {
      cy.get("[id=" + characteristic.testId + "]").should("not.be.checked");
    }
  }

  static checkTextSourceCharacteristicIsAvailable(characteristic: any) {
    cy.get("input[id=" + characteristic.testId + "]").should("have.value", characteristic.expectedValue);
  }

  static checkDropdownSourceCharacteristicIsAvailable(characteristic: any) {
    cy.get("select[id=" + characteristic.testId + "]").should("have.value", characteristic.expectedValue.toUpperCase());
  }

  static assertAvailableBuoyancyTypesAre(dataTable: any) {
    dataTable.hashes().forEach((buoyancyType: any) => {
      cy.get('[id="buoyancyType"] option').should("contain.text", buoyancyType.name);
    });
  }

  static selectBuoyancyType(buoyancyType: string) {
    cy.get('[id="buoyancyType"]').select(buoyancyType);
  }

  static assertBuoyancyTypeDropdownDoesntExist() {
    cy.get('[id="buoyancyType"]').should("not.exist");
  }

  static assertAvailableEffluxTypesAre(dataTable: any) {
    dataTable.hashes().forEach((effluxType: any) => {
      cy.get('[id="effluxType"] option').should("contain.text", effluxType.name);
    });
  }

  static selectEffluxType(effluxType: string) {
    cy.get('[id="effluxType"]').select(effluxType);
  }

  static assertEffluxTypeDropdownDoesntExist() {
    cy.get('[id="effluxType"]').should("not.exist");
  }

  static fillInCharacteristic(field: string, value: string) {
    switch (field) {
      case "Source diameter":
        cy.get('[id="sourceDiameter"]').clear().type(value);
        break;
      default:
        throw new Error("unknown characteristic input field: " + field);
    }
  }

  static checkShownSourceCharacteristicsOnDetailPanel(dataTable: any) {
    dataTable.hashes().forEach((characteristic: any) => {
      if (characteristic.shouldBeVisible == "false") {
        cy.get("[id=" + characteristic.testId + "]").should("not.exist");
      } else {
        cy.get("[id=" + characteristic.testId + "]").should("have.text", characteristic.expectedValue);
      }
    });
  }

  static chooseSourceCharacteristics(sourceHeight: string, sourceDiameter: string) {
    cy.get('[id="collapsibleADMSCharacteristics-title"]').click();
    cy.get('[id="sourceHeight"]').type("{selectAll}").type(sourceHeight);
    cy.get('[id="sourceDiameter"]').type("{selectAll}").type(sourceDiameter);
  }

  static chooseSourceHeight(sourceHeight: string) {
    cy.get('[id="sourceHeight"]').type("{selectAll}").type(sourceHeight);;
  }

  static sourceWithEmissionHeight(emissionHeight: string) {
    cy.get("[id=sourceADMSHeight]").should("have.text", emissionHeight);
  }

  static sourceWithDiameter(sourceDiameter: string) {
    cy.get("[id=sourceADMSDiameter]").should("have.text", sourceDiameter);
  }

  static buoyancyType: any = {
    TEMPERATURE: {
      label: "Temperature",
      inputField: "sourceTemperature",
      detailsField: "sourceADMSTemperature"
    },
    DENSITY: {
      label: "Density",
      inputField: "sourceDensity",
      detailsField: "sourceADMSDensity"
    },
    AMBIENT: {
      label: "Ambient",
      inputField: "",
      detailsField: ""
    }
  };

  static chooseBuoyancy(buoyancyType: string, buoyancyValue: string) {
    cy.get("[id=buoyancyType]").select(buoyancyType);
    // In case AMBIENT is chosen, then no field will appear for a value so check if inputField is specified before filling value.
    if (this.buoyancyType[buoyancyType]?.inputField) {
      cy.get("[id=" + this.buoyancyType[buoyancyType].inputField + "]")
        .type("{selectAll}")
        .type(buoyancyValue);
    }
  }

  static sourceWithBuoyancy(buoyancyType: string, buoyancyValue: string) {
    cy.get("[id=sourceADMSBuoyancyTypeDetailLabel]").should("have.text", this.buoyancyType[buoyancyType].label);
    // In case AMBIENT is chosen, then no details will appear. So check if detailsField is specified before checking value.
    if (this.buoyancyType[buoyancyType].detailsField) {
      cy.get("[id=" + this.buoyancyType[buoyancyType].detailsField + "]").should("have.text", buoyancyValue);
    }
  }

  static effluxType: any = {
    VELOCITY: {
      label: "Velocity",
      inputField: "sourceVerticalVelocity",
      detailsField: "sourceADMSVerticalVelocity"
    },
    VOLUME: {
      label: "Volume",
      inputField: "sourceVolumetricFlowRate",
      detailsField: "sourceADMSVolumetricFlowRate"
    }
  };

  static chooseEfflux(effluxType: string, effluxValue: string) {
    cy.get("[id=effluxType]").select(effluxType);
    cy.get("[id=" + this.effluxType[effluxType].inputField + "]")
      .type("{selectAll}")
      .type(effluxValue);
  }

  static sourceWithEfflux(effluxType: string, effluxValue: string) {
    cy.get("[id=sourceADMSEffluxTypeLabels]").should("have.text", this.effluxType[effluxType].label);
    cy.get("[id=" + this.effluxType[effluxType].detailsField + "]").should("have.text", effluxValue);
  }

  static assertEffluxTypeIsInvisible(effluxType: string) {
    cy.get("[id=" + this.effluxType[effluxType].detailsField + "]").should("not.exist");
  }

  static chooseSpecificHeatCapacity(specificHeatCapacity: string) {
    cy.get("[id=sourceSpecificHeatCapacity]").type("{selectAll}").type(specificHeatCapacity);
  }

  static sourceWithSpecificHeatCapacity(specificHeatCapacity: string) {
    cy.get("[id=sourceADMSSpecificHeatCapacity]").should("have.text", specificHeatCapacity);
  }
}
