/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { CommonSettings } from "./CommonSettings";
import { CommonPage } from "./CommonPage";

export class CommonExportPage {
  static selectExportType(exportType: string) {
    switch (exportType) {
      case "Invoerbestanden":
      case "Input files":
        cy.get('[id="tab-exportType-SOURCES"]').click();
        break;
      case "Rekentaak":
      case "Calculation job":
        cy.get('[id="tab-exportType-CALCULATION"]').click();
        break;
      case "Rapportage":
      case "Report":
        cy.get('[id="tab-exportType-REPORT"]').click();
        break;
      default:
        throw new Error("Unknown export type: " + exportType);
    }
  }

  static chooseExportThroughMenu() {
    cy.get('[id="NAV_ICON-MENU-EXPORT"]').click();
  }

  static assertExportOpsIsVisible() {
    cy.get(".additionalOptionsContainer").should("be.visible");
  }

  static chooseToAddExtraInformation(selectedExportOption: string) {
    cy.get("#additional-info-line").then((el) => {
      if (!el.parent().hasClass("open")) {
        el.click();
      }
    });
    switch (selectedExportOption) {
      case "GML":
        cy.get('[id="includingExtraInfoToggle"]').should("have.class", "open");
        break;
      case "PDF":
        cy.get('[id="additional-info-content"]').should("be.visible");
        break;
      default:
        throw new Error("Unknown export additional information option: " + selectedExportOption);
    }
  }

  static selectCalculationJob(calculationJob: string) {
    cy.get('[id="selectedJob"]').select(calculationJob);
  }

  static selectCalculationType(calculationType: string) {
    cy.get('[id="exportOptions"]').select(calculationType);
  }

  static fillInEmailAdress(emailAddress: string) {
    cy.get('[id="exportEmail"]').type(emailAddress);
  }

  static clickOnExportButton() {
    cy.get('[id="buttonSaveExport"]').click();
  }

  static clickOnExportButtonAndWaitForExportToStart() {
    cy.intercept("/api/v8/ui/calculation/*").as("getCalculation"); // Intercept the url that is triggered after clicking the export button
    cy.get('[id="buttonSaveExport"]').click();
    cy.wait("@getCalculation").then((response: any) => {
      if (response.response.body != "") {
        // store jobId in global variable
        cy.wrap(response.response.body.jobProgress.key).as("jobKey");
      }
    });
  }

  static assertDataInclusionWarningIsShown() {
    cy.get('[id="agreeDataInclusion_warning"]').should("exist").should("be.visible");
  }

  static assertWarningIsShown(warningType: string) {
    cy.get('[id="' + warningType + '"]')
      .should("exist")
      .should("be.visible");
  }

  static acceptDataInclusionWarning() {
    cy.get('[id="agreeDataInclusion"]').parent().find("span.checkmark").click();
  }

  static clickOnNewCalculationJobButton() {
    cy.get(".jobButton").click();
  }

  static clickAdditionalInfo() {
    cy.get('[id="additional-info-title"]').click();
  }

  static validateSituations(dataTable: any) {
    dataTable.hashes().forEach((situation: any) => {
      if (situation.checked == "true") {
        cy.get(".situationSelection")
          .get(".inputContainer >")
          .contains(situation.name)
          .parent()
          .parent()
          .within(() => {
            cy.get('[class="checkmark icon-checked"]');
          });
      } else {
        cy.get(".situationSelection")
          .get(".inputContainer >")
          .contains(situation.name)
          .parent()
          .parent()
          .within(() => {
            cy.get('[class="checkmark"]').should("not.have.class", "checkmark icon-checked");
          });
      }
    });
  }

  static clickAppendices() {
    cy.get('[id="panel-appendices-title"]').click();
  }

  static validateIfAppendiceCheckBoxEdgeEffectExists(checkBoxAvailable: boolean) {
    if (checkBoxAvailable == true) {
      cy.get('[id="includeEdgeEffectReport"]');
    } else {
      cy.get('[id="includeEdgeEffectReport"]').should("not.exist");
    }
  }

  static validateIfAppendicePanelIsChecked(panelChecked: boolean) {
    if (panelChecked == true) {
      cy.get('[id="includeEdgeEffectReport"]')
        .parent()
        .within(() => {
          cy.get('[class="checkmark icon-checked"]');
        });
    } else {
      cy.get('[id="includeEdgeEffectReport"]')
        .parent()
        .within(() => {
          cy.get('[class="checkmark"]').should("not.have.class", "checkmark icon-checked");
        });
    }
  }

  static checkEdgeHexagonAppendice(checkEdgeHexagon: boolean) {
    if (checkEdgeHexagon == true) {
      cy.get('[id="includeEdgeEffectReport"]').check({ force: true });
    } else {
      cy.get('[id="includeEdgeEffectReport"]').uncheck({ force: true });
    }
  }

  static clickAdditionalInfoOptional() {
    cy.get('[id="includingExtraInfoToggle-title"]').click();
  }

  static selectAcknowledgePDFExport() {
    cy.get('[id="agreeDataInclusion"]').check({ force: true });
  }

  static fillInAdditionalInfoForm(dataTable: any) {
    dataTable.hashes().forEach((additionalInfo: any) => {
      cy.get('[id="scenarioMetaData' + additionalInfo.inputField + '"]').type(additionalInfo.input);
    });
  }

  static assertButton(buttonType: string) {
    cy.get('[id="' + buttonType + '"]').should("exist");
  }

  static cancelExport() {
    cy.get('[id="deleteButton"]').click();
    cy.on("window:confirm", () => true);
    cy.get('[id="deleteButton"]').should("not.exist");
  }

  static checkTaskStatusCompleteAndDownload() {
    this.checkTaskStatus("COMPLETED");
    cy.get("@jobKey").then((jobKey) => {
      cy.request({
        method: "GET",
        url: this.getCleanBaseUrl() + Cypress.env("url_ui_calculation") + jobKey
      }).then((response) => {
        console.log(response.body.jobProgress);
        const body = response.body.jobProgress;
        if (body["state"] == "COMPLETED") {
          const filePath = body["resultUrl"];
          cy.downloadFile(filePath, "cypress/fixtures/download/" + jobKey + "/", "result.zip");
        }
      });
      cy.readFile("cypress/fixtures/download/" + jobKey + "/" + "result.zip");
      cy.log("download done");
    });
  }

  static checkTaskStatus(expectedStatus: string) {
    cy.get("@jobKey").then((jobKey) => {
      const checkAndReload = (count: number) => {
        cy.request({
          method: "GET",
          url: this.getCleanBaseUrl() + Cypress.env("url_ui_calculation") + jobKey
        }).then((response) => {
          cy.log(response.body);
          console.log(response.body);
          let body = response.body.jobProgress;
          expect(body)
            .to.have.nested.property("state")
            .not.equal("ERROR", "Job had an exception before complete (" + body["errorMessage"] + ")");
          if (body["state"] == expectedStatus) {
            cy.log("Job has the expected status");
          } else if (count <= 30) {
            cy.log("Count: " + count + ", number of points calculated " + body["numberOfPointsCalculated"]);
            cy.wait(10_000);
            checkAndReload(count++);
          } else {
            throw new Error("Validation task status exceeds time limit");
          }
        });
      };
      checkAndReload(0);
    });
  }

  static getCleanBaseUrl() {
    const url: any = Cypress.config("baseUrl");
    const position = url.indexOf("?");
    return position > -1 ? url.substring(0, position) : url;
  }

  static extractDownloadedResults(fileCount: number) {
    cy.get("@jobKey").then((jobKey) => {
      cy.task("unzipping", {
        outputPath: "cypress/fixtures/download/" + jobKey + "/unzip/",
        inputFilePath: "cypress/fixtures/download/" + jobKey + "/" + "result.zip",
        timeout: CommonSettings.fileioTimeOut
      });
      cy.task("readdir", { path: "cypress/fixtures/download/" + jobKey + "/unzip/" }).then((files: any) => {
        expect(files.length).to.be.equal(fileCount, "Validate total count of files");
      });
    });
  }

  static loadDownloadedResults() {
    cy.get("@jobKey").then((jobKey) => {
      let inputFilePath = "download/" + jobKey + "/" + "result.zip";
      CommonPage.selectFileForImport(inputFilePath);
    });
  }

  static validateInCombinationResults(expectedFiles: any) {
    cy.get("@jobKey").then((jobKey) => {
      var count = 0;
      cy.task("readdir", { path: "cypress/fixtures/download/" + jobKey + "/unzip/" }).then((files: any) => {
        files.forEach((fileName: any) => {
          expectedFiles.hashes().forEach((expectedFile: any, index: number) => {
            if (fileName.indexOf(expectedFile.fileName) > -1) {
              count++;
              cy.readFile("cypress/fixtures/download/" + jobKey + "/unzip/" + files[0]).then((responseGml) => {
                if (expectedFile.type === "scenario") {
                  expect(responseGml).to.contain("IN_COMBINATION_PROCESS_CONTRIBUTION");
                } else if (expectedFile.type === "archive") {
                  expect(responseGml).to.contain("imaer:ArchiveMetadata");
                  expect(responseGml).to.contain("imaer:retrievalDateTime");
                } else {
                  throw new Error("Unknown archive file type " + expectedFile.type);
                }
              });
            }
          });
        });
        expect(count).to.equal(files.length, "Files should match processed files.");
      });
    });
  }

  static validateTabCalculationMenu(tab: string) {
    switch (tab) {
      case "Invoerbestanden":
        cy.get('[id="tab-exportType-SOURCES"]').should("have.attr", "aria-selected", "true");
        break;
      case "Rekentaak":
        cy.get('[id="tab-exportType-CALCULATION"]').should("have.attr", "aria-selected", "true");
        break;
      case "Rapportage":
        cy.get('[id="tab-exportType-REPORT"]').should("have.attr", "aria-selected", "true");
        break;
      default:
        throw new Error("unknown tab: " + tab);
    }
  }

  static validateCalculationJob(calculationJob: string) {
    cy.get('[id="selectedJob"]').find(":selected").contains(calculationJob);
  }
}
