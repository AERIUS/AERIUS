/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";

import { CommonGenericUIPage } from "./CommonGenericUIPage";

Given("I select the scenario name", () => {
  CommonGenericUIPage.selectScenarioName();
});

Given("I start a search with {string} and validate {string} categories and the following detail results are shown", (queryString, resultsCount, dataTable) => {
  CommonGenericUIPage.assertSearchQueryResults(queryString, resultsCount, dataTable);
});

Given("I select search result {string} and validate we have no Discoverer errors", (searchName) => {
  CommonGenericUIPage.assertSelectSearchQueryResultsNoStatusCodeErrors(searchName);
});

Given("I select search result {string}", (value) => {
  CommonGenericUIPage.clickOnSearchResult(value);
});

// Preferences
Given("the preferences panel is closed", () => {
  CommonGenericUIPage.assertPreferencesPanelIsClosed();
});

// Sources
Given("there should be {int} sources", (numberOfSources) => {
  CommonGenericUIPage.assertNumberOfSources(numberOfSources);
});

