/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";
import { CommonPage_UK } from "../../common/CommonPage_UK";
import { RoadTransportationSourceRoadTrafficPage } from "./RoadTransportationSourceRoadTrafficPage";

Given("I choose road type {string} direction {string}", (roadType, direction) => {
  RoadTransportationSourceRoadTrafficPage.chooseRoadTypeDirection(roadType, direction);
});

Given("I choose maximum speed {string}", (maxSpeed) => {
  RoadTransportationSourceRoadTrafficPage.chooseMaxSpeed(maxSpeed);
});

Given("I choose time unit {string}", (timeUnit) => {
  RoadTransportationSourceRoadTrafficPage.chooseTimeUnit(timeUnit);
});

Given(
  "I choose for traffic type {string} the number of vehicles {string} and stagnation percentage {string}",
  (trafficType, numberOfVehicles, stagnationPercentage) => {
    RoadTransportationSourceRoadTrafficPage.chooseTrafficTypeNumberVehiclesAndStagnation(trafficType, numberOfVehicles, stagnationPercentage);
  }
);

Given("direction {string} is correctly saved", (direction) => {
  RoadTransportationSourceRoadTrafficPage.directionCorrectlySaved(direction);
});

Given("maximum speed {string} is correctly saved", (maxSpeed) => {
  RoadTransportationSourceRoadTrafficPage.maxSpeedCorrectlySaved(maxSpeed);
});

Given(
  "traffic type {string} with number of vehicles {string} and stagnation percentage {string} is correctly saved",
  (trafficType, numberOfVehicles, stagnationPercentage) => {
    RoadTransportationSourceRoadTrafficPage.trafficTypeNumberVehiclesAndStagPercentageCorrectlySaved(
      trafficType,
      numberOfVehicles,
      stagnationPercentage
    );
  }
);

Given(
  "traffic type {string} with number of vehicles {string} and stagnation percentage {string} is available for edit",
  (trafficType, numberOfVehicles, stagnationPercentage) => {
    RoadTransportationSourceRoadTrafficPage.trafficTypeNumberVehiclesAndStagPercentageInputFieldsHaveValues(
      trafficType,
      numberOfVehicles,
      stagnationPercentage
    );
  }
);

Given("I fill description with {string}", (description) => {
  RoadTransportationSourceRoadTrafficPage.fillDescription(description);
});

Given("I choose time unit {string} with number of vehicles {string}", (timeUnit, numberOfVehicles) => {
  RoadTransportationSourceRoadTrafficPage.chooseTimeUnitAndNumberOfVehicles(timeUnit, numberOfVehicles);
});

Given("I fill emission {string} per vehicle {string} for Euro class", (emission, fraction) => {
  RoadTransportationSourceRoadTrafficPage.fillEmissionAndFractionForEuroClass(emission, fraction);
});

Given("description {string} is correctly saved", (description) => {
  RoadTransportationSourceRoadTrafficPage.descriptionCorrectlySaved(description);
});

Given("time unit {string} with number of vehicles {string} is correctly saved", (timeUnit, numberOfVehicles) => {
  RoadTransportationSourceRoadTrafficPage.timeUnitAndNumberOfVehiclesIsCorrectlySaved(timeUnit, numberOfVehicles);
});

Given("time unit {string} with number of vehicles {string} is correctly saved", (timeUnit, numberOfVehicles) => {
  RoadTransportationSourceRoadTrafficPage.timeUnitAndNumberOfVehiclesForEuroClassIsCorrectlySaved(timeUnit, numberOfVehicles);
});

Given("time unit {string} with number of vehicles {string} for Euro class is correctly saved", (timeUnit, numberOfVehicles) => {
  RoadTransportationSourceRoadTrafficPage.timeUnitAndNumberOfVehiclesForEuroClassIsCorrectlySaved(timeUnit, numberOfVehicles);
});

Given("emission {string} per vehicle {string} is correctly saved", (emission, fraction) => {
  RoadTransportationSourceRoadTrafficPage.emissionAndFractionIsCorrectlySaved(emission, fraction);
});

Given("the header of the amount of {string} vehicles on row {string} contains {string}", (typeOfVehicles, row, timeUnit) => {
  RoadTransportationSourceRoadTrafficPage.assertAmountOfCustomVehiclesContainsTimeUnit(typeOfVehicles, row, timeUnit);
});

// UK
Given("I select for {string} profile the option {string}", (profileType, profileOption) => {
  CommonPage_UK.selectOptionForTimeVaryingProfile(profileType, profileOption);
});

Given("the details of the profile are correctly saved", (dataTable) => {
  CommonPage_UK.timeVaryingProfileIsCorrectlySaved(dataTable);
});
