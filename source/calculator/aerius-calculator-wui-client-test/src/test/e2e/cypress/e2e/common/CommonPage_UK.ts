/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { CommonPage } from "./CommonPage";

export class CommonPage_UK {
  static visitAndLoginWithAdms() {
    CommonPage.visitAndLogin(Cypress.config("baseUrl") + "&adms");
  }

  static openPanel(panelName: string) {
    switch (panelName) {
      case "Location":
        cy.get('[id="location-title"]').click();
        break;
      case "Source characteristics":
        cy.get('[id="collapsibleADMSCharacteristics-line"]').click();
        break;
      case "Emissions":
        cy.get('[id="collapsibleSubSources-line"]').click();
        break;
      case "Traffic direction, speed, traffic and emission":
        cy.get('[id="collapsibleSubSources"]').click();
        break;
      case "Time-varying profiles":
        cy.get('[id="collapsibleTimeVaryingProfiles-line"]').click();
        break;
      default:
        throw new Error("Unknown panel: " + panelName);
    }
  }

  static chooseArea(area: string) {
    cy.get('[id="roadAreaCategory"]').select(area, { force: true });
  }

  static addNewSourceOfType(newSourceOfType: string) {
    cy.get('[id="sourceListButtonNewSource"]').click();
    switch (newSourceOfType) {
      case "Custom specification":
        cy.get('[id="tab-vehicleType-CUSTOM"]').click();
        break;
      case "Emission factor toolkit":
        cy.get('[id="tab-vehicleType-STANDARD"]').click();
        break;
      default:
        throw new Error("Unknown source type: " + newSourceOfType);
    }
  }

  static selectDevelopmentPressure(dataTable: any) {
    this.openCalculationJobPanel("Development pressure search");
    // Ensure road network element is open if it's there
    cy.get(".sources").then((x: JQuery<HTMLElement>) => {
      x.children().find(".icon-traffic-network").trigger("click");
    });
    let selectSources = dataTable.hashes().map((x: any) => x.source);
    // Ensure all the ones that should be selected are actually present
    selectSources.forEach((x: string) => cy.get(".sources").contains(x));
    cy.get(".sources .labelColumn").each((labelElement: JQuery<HTMLElement>) => {
      // If it should be selected, but isn't yet, click it.
      if (selectSources.includes(labelElement.text()) && !labelElement.parent().hasClass("selected")) {
        labelElement.trigger("click");
      }
      // If it shouldn't be selected, but is selected, click it.
      if (!selectSources.includes(labelElement.text()) && labelElement.parent().hasClass("selected")) {
        labelElement.trigger("click");
      }
    });
  }

  static selectMinMoninObukhovLength(minMoninObukhovLength: string) {
    this.openCalculationJobPanel("Advanced settings");
    cy.get('[id="minMoninObukhovLength"]').clear();

    if (minMoninObukhovLength !== "") {
      cy.get('[id="minMoninObukhovLength"]').type(minMoninObukhovLength);
    }
  }

  static selectSurfaceAlbedo(surfaceAlbedo: string) {
    this.openCalculationJobPanel("Advanced settings");
    cy.get('[id="surfaceAlbedo"]').clear();

    if (surfaceAlbedo !== "") {
      cy.get('[id="surfaceAlbedo"]').type(surfaceAlbedo);
    }
  }

  static selectPriestleyTaylorParameter(priestleyTaylorParameter: string) {
    this.openCalculationJobPanel("Advanced settings");
    cy.get('[id="priestleyTaylorParameter"]').clear();

    if (priestleyTaylorParameter !== "") {
      cy.get('[id="priestleyTaylorParameter"]').type(priestleyTaylorParameter);
    }
  }

  static visibilityDPSpanel(visibilityDPSpanel: string) {
    switch (visibilityDPSpanel) {
      case "visible":
        cy.get('[id="collapsible-panel-DevelopmentPressureSearch-title"]').should ("be.visible");
        break;  
      case "not visible":
        cy.get('[id="collapsible-panel-DevelopmentPressureSearch-title"]').should ("not.exist");
        break;
      default:
        throw new Error("Unknown visibility: " + visibilityDPSpanel);  
    }
  }

  static setZoneOfInfluence(zoneOfInfuence: string) {
    this.openCalculationJobPanel("Calculation job settings");
    cy.get('[id="zoneOfInfluenceInput"]').clear();

    if (zoneOfInfuence !== "") {
      cy.get('[id="zoneOfInfluenceInput"]').type(zoneOfInfuence);
    }
  }

  static selectCalculationProjectCategory(projectCategory: string) {
    cy.get('[id="projectCategory"]').select(projectCategory);
  }

  static fillRoadWidthElevationGradient(width: string, elevation: string, gradient: string) {
    cy.get("[id=roadWidth").clear().type(width);
    cy.get("[id=roadElevation").clear().type(elevation);
    cy.get("[id=roadGradient").clear().type(gradient);
  }

  static fillInMaxSpeed(maxSpeed: string) {
    cy.get('[id="emissionSourceRoadMaxSpeed"]').clear().type(maxSpeed);
  }

  static selectTimeUnit(timeUnit: string) {
    cy.get('[id="timeUnit"]').select(timeUnit);
  }

  static fillInNumberForVehicle(numberOfVehicle: string, vehicleType: string) {
    switch (vehicleType) {
      case "Car":
        cy.get('[id="numberOfVehiclesRow_Car"]').clear().type(numberOfVehicle);
        break;
      case "Taxi (black cab)":
        cy.get('[id="numberOfVehiclesRow_Tax"]').clear().type(numberOfVehicle);
        break;
      case "Motorcycle":
        cy.get('[id="numberOfVehiclesRow_Mot"]').clear().type(numberOfVehicle);
        break;
      case "LGV":
        cy.get('[id="numberOfVehiclesRow_LGV"]').clear().type(numberOfVehicle);
        break;
      case "HGV":
        cy.get('[id="numberOfVehiclesRow_HGV"]').clear().type(numberOfVehicle);
        break;
      case "Bus and Coach":
        cy.get('[id="numberOfVehiclesRow_Bus"]').clear().type(numberOfVehicle);
        break;
      default:
        throw new Error("Unknown vehicle type: " + vehicleType);
    }
  }

  static areaCorrectlySaved(savedArea: string) {
    cy.get('[id="roadArea"]').should("have.text", savedArea);
  }

  static roadTypeDirectionCorrectlySaved(roadType: string, direction: string) {
    cy.get('[id="roadType"]').should("have.text", roadType);
    cy.get('[id="roadTrafficDirection"]').should("have.text", direction);
  }

  static roadWidthElevationGradientCorrectlySaved(width: string, elevation: string, gradient: string) {
    cy.get('[id="roadWidth"]').should("have.text", width);
    cy.get('[id="roadElevation"]').should("have.text", elevation);
    cy.get('[id="roadGradient"]').should("have.text", gradient);
  }

  static barrierTypeLeftRight(barrierTypeLeft: string, barrierTypeRight: string) {
    cy.get('[id="admsDetailBarrierTypeLeft"]').should("have.text", barrierTypeLeft);
    cy.get('[id="admsDetailBarrierTypeRight"]').should("have.text", barrierTypeRight);
  }

  static speedCorrectlySaved(speed: string) {
    cy.get('[id="detailRoadStandardVehicleMaxSpeed-0"]').should("have.text", speed);
  }

  static numberOfVehicleForType(numberOfVehicle: string, typeOfVehicle: string) {
    switch (typeOfVehicle) {
      case "Car":
        cy.get('[id="detailRoadStandardVehicleTypeDescription-0-0"] > [id="values"] > div:first').should("have.text", numberOfVehicle);
        break;
      case "Taxi (black cab)":
        cy.get('[id="detailRoadStandardVehicleTypeDescription-0-1"] > [id="values"] > div:first').should("have.text", numberOfVehicle);
        break;
      case "Motorcycle":
        cy.get('[id="detailRoadStandardVehicleTypeDescription-0-2"] > [id="values"] > div:first').should("have.text", numberOfVehicle);
        break;
      case "LGV":
        cy.get('[id="detailRoadStandardVehicleTypeDescription-0-3"] > [id="values"] > div:first').should("have.text", numberOfVehicle);
        break;
      case "HGV":
        cy.get('[id="detailRoadStandardVehicleTypeDescription-0-4"] > [id="values"] > div:first').should("have.text", numberOfVehicle);
        break;
      case "Bus and Coach":
        cy.get('[id="detailRoadStandardVehicleTypeDescription-0-5"] > [id="values"] > div:first').should("have.text", numberOfVehicle);
        break;
      default:
        throw new Error("Unknown vehicle type: " + typeOfVehicle);
    }
  }

  static fillEmissionWithValue(emission: string, value: string) {
    switch (emission) {
      case "NOx":
        cy.get('[id="emissionFactorInput_NOX"]').clear().type(value);
        break;
      case "NH3":
        cy.get('[id="emissionFactorInput_NH3"]').clear().type(value);
        break;
      default:
        throw new Error("Unknown emission type: " + emission);
    }
  }

  static fillRoadWidthWithValue(value: string) {
    cy.get('[id="roadWidth"]').clear().type(value);
  }

  static emissionWithValueCorrectlySaved(emission: string, value: string) {
    switch (emission) {
      case "NOx":
        cy.get('[id="detailRoadCustomVehicleSubstance-0-NOX"]').should("have.text", value);
        break;
      case "NH3":
        cy.get('[id="detailRoadCustomVehicleSubstance-0-NH3"]').should("have.text", value);
        break;
      default:
        throw new Error("Unknown emission type: " + emission);
    }
  }

  static selectLeftOrRightBarrierType(leftOrRight: string, barrierType: string) {
    switch (leftOrRight) {
      case "left":
        cy.get('[id="roadSideBarrierLeftType"]').select(barrierType);
        break;
      case "right":
        cy.get('[id="roadSideBarrierRightType"]').select(barrierType);
        break;
      default:
        throw new Error("Unknown barrier side: " + leftOrRight);
    }
  }

  static fillWidthWith(leftOrRight: string, width: string) {
    switch (leftOrRight) {
      case "left":
        cy.get('[id="barrierLeftWidth"]').clear().type(width);
        break;
      case "right":
        cy.get('[id="barrierRightWidth"]').clear().type(width);
        break;
      default:
        throw new Error("Unknown barrier side wide: " + leftOrRight);
    }
  }

  static fillMaxHeightWith(leftOrRight: string, maxHeight: string) {
    switch (leftOrRight) {
      case "left":
        cy.get('[id="barrierLeftMaximumHeight"]').clear().type(maxHeight);
        break;
      case "right":
        cy.get('[id="barrierRightMaximumHeight"]').clear().type(maxHeight);
        break;
      default:
        throw new Error("Unknown barrier side height: " + leftOrRight);
    }
  }

  static fillAverageHeightWith(leftOrRight: string, averageHeight: string) {
    switch (leftOrRight) {
      case "left":
        cy.get('[id="barrierLeftAverageHeight"]').clear().type(averageHeight);
        break;
      case "right":
        cy.get('[id="barrierRightAverageHeight"]').clear().type(averageHeight);
        break;
      default:
        throw new Error("Unknown barrier side height: " + leftOrRight);
    }
  }

  static fillMinimumHeightWith(leftOrRight: string, minHeight: string) {
    switch (leftOrRight) {
      case "left":
        cy.get('[id="barrierLeftMinimumHeight"]').clear().type(minHeight);
        break;
      case "right":
        cy.get('[id="barrierRightMinimumHeight"]').clear().type(minHeight);
        break;
      default:
        throw new Error("Unknown barrier side minimum height: " + leftOrRight);
    }
  }

  static fillPorosityWith(leftOrRight: string, porosity: string) {
    switch (leftOrRight) {
      case "left":
        cy.get('[id="barrierLeftPorosity"]').clear().type(porosity);
        break;
      case "right":
        cy.get('[id="barrierRightPorosity"]').clear().type(porosity);
        break;
      default:
        throw new Error("Unknown barrier side porosity: " + leftOrRight);
    }
  }

  static fillCoverageWith(coverage: string) {
    cy.get('[id="roadCoverage"]').clear().type(coverage);
  }

  static barrierTypeIsCorrectlySaved(leftOrRight: string, barrierType: string) {
    switch (leftOrRight) {
      case "left":
        cy.get("#admsDetailBarrierTypeLeft").should("have.text", barrierType);
        break;
      case "right":
        cy.get("#admsDetailBarrierTypeRight").should("have.text", barrierType);
        break;
      default:
        throw new Error("Unknown barrier side type: " + leftOrRight);
    }
  }

  static widthIsCorrectlySaved(leftOrRight: string, width: string) {
    switch (leftOrRight) {
      case "left":
        cy.get("#admsDetailBarrierWidthLeft").should("have.text", width);
        break;
      case "right":
        cy.get("#admsDetailBarrierWidthRight").should("have.text", width);
        break;
      default:
        throw new Error("Unknown barrier side wide: " + leftOrRight);
    }
  }

  static maxHeightIsCorrectlySaved(leftOrRight: string, maxHeight: string) {
    switch (leftOrRight) {
      case "left":
        cy.get("#admsDetailBarrierMaxHeightLeft").should("have.text", maxHeight);
        break;
      case "right":
        cy.get("#admsDetailBarrierMaxHeightRight").should("have.text", maxHeight);
        break;
      default:
        throw new Error("Unknown barrier side height: " + leftOrRight);
    }
  }

  static averageHeightIsCorrectlySaved(leftOrRight: string, averageHeight: string) {
    switch (leftOrRight) {
      case "left":
        cy.get("#admsDetailBarrierAvgHeightLeft").should("have.text", averageHeight);
        break;
      case "right":
        cy.get("#admsDetailBarrierAvgHeightRight").should("have.text", averageHeight);
        break;
      default:
        throw new Error("Unknown barrier side average height: " + leftOrRight);
    }
  }

  static minimumHeightIsCorrectlySaved(leftOrRight: string, minHeight: string) {
    switch (leftOrRight) {
      case "left":
        cy.get("#admsDetailBarrierMinHeightLeft").should("have.text", minHeight);
        break;
      case "right":
        cy.get("#admsDetailBarrierMinHeightRight").should("have.text", minHeight);
        break;
      default:
        throw new Error("Unknown barrier side minimum height: " + leftOrRight);
    }
  }

  static porosityIsCorrectlySaved(leftOrRight: string, porosity: string) {
    switch (leftOrRight) {
      case "left":
        cy.get("#admsDetailBarrierPorosityLeft").should("have.text", porosity);
        break;
      case "right":
        cy.get("#admsDetailBarrierPorosityRight").should("have.text", porosity);
        break;
      default:
        throw new Error("Unknown barrier side porosity: " + leftOrRight);
    }
  }

  static followingWarningIsVisible(warningType: string, warningText: string) {
    switch (warningType) {
      case "road width":
        cy.get('[id="roadWidth_error"]').should("have.text", warningText);
        break;
      case "road elevation":
        cy.get('[id="roadElevation_error"]').should("have.text", warningText);
        break;
      case "road gradient":
        cy.get('[id="roadGradient_error"]').should("have.text", warningText);
        break;
      case "barrier width left":
        cy.get('[id="barrierLeftWidthError"]').should("have.text", warningText);
        break;
      case "barrier width right":
        cy.get('[id="barrierRightWidthError"]').should("have.text", warningText);
        break;
      case "road area":
        cy.get('[id="roadAreaCategory_error"]').should("have.text", warningText);
        break;
      case "road type":
        cy.get('[id="roadTypeCategory_error"]').should("have.text", warningText);
        break;
      case "assessmentpoint road height":
        cy.get('[id="pointRoadHeight_error"]').should("have.text", warningText);
        break;
      case "asessmentpoint custom primary fraction":
        cy.get('[id="pointRoadFractionNO2_error"]').should("have.text", warningText);
        break;
      case "source height":
        cy.get('[id="sourceHeight_warning"]').should("have.text", warningText);
        break;
      case "save notification":
        cy.get('[id="modifyCancelSaveContainer"]');
        break;
      case "input error message":
        cy.get('[id="inputErrorMessage"]');
        break;
      default:
        throw new Error("Unknown warning type: " + warningType);
    }
  }

  static followingErrorIsVisible(errorType: string, errorText: string, visible: boolean) {
    switch (errorType) {
      case "source height":
        if (visible) {
          cy.get('[id="inputErrorMessage"]').should("have.text", errorText);
        } else {
          cy.get('[id="inputErrorMessage"]').should("not.exist", errorText);
        }
        break;
      case "input":
        cy.get('[id="inputErrorMessage"]').should("have.text", errorText);
        break;
      case "Width left":
      case "Width right":
      case "Maximum height left":
      case "Maximum height right":
      case "Average height left":
      case "Average height right":
      case "Minimum height left":
      case "Minimum height right":
      case "Porosity left":
      case "Porosity right":
      case "Coverage":
        if (visible) {
          cy.get('[id="inputErrorMessage"]').contains(errorText);
        } else {
          cy.get('[id="inputErrorMessage"]').should("not.exist", errorText);
        }
        break;
      default:
        throw new Error("Unknown error type: " + errorType);
    }
  }

  static followingWarningIsNotVisible(warningType: string) {
    switch (warningType) {
      case "road width":
        cy.get('[id="roadWidth_error"]').should("not.exist");
        break;
      case "road elevation":
        cy.get('[id="roadElevation_error"]').should("not.exist");
        break;
      case "road gradient":
        cy.get('[id="roadGradient_error"]').should("not.exist");
        break;
      case "barrier width left":
        cy.get('[id="barrierLeftWidthError"]').should("not.exist");
        break;
      case "barrier width right":
        cy.get('[id="barrierRightWidthError"]').should("not.exist");
        break;
      case "road area":
        cy.get('[id="roadAreaCategory_error"]').should("not.exist");
        break;
      case "road type":
        cy.get('[id="roadTypeCategory_error"]').should("not.exist");
        break;
      default:
        throw new Error("Unknown warning type: " + warningType);
    }
  }

  static chooseTimeVaryingProfile(timeVaryingProfileType: string) {
    switch (timeVaryingProfileType) {
      case "Custom":
        cy.get('[id="tab-timeVaryingProfile-CUSTOM"]').click();
        break;
      case "Predefined":
        cy.get('[id="tab-timeVaryingProfile-STANDARD"]').click();
        break;
      default:
        throw new Error("Unknown time-varying profile: " + timeVaryingProfileType);
    }
  }

  static coverageIsCorrectlySaved(coverage: string) {
    cy.get('[id="barrierCoverage"]').should("have.text", coverage);
  }

  static tunnelIsCorrectlySaved(tunnelFactor: string) {
    cy.get('[id="tunnelFactor"]').should("have.text", tunnelFactor);
  }

  static selectTimeVaryingProfile(timeVaryingProfile: string) {
    cy.get('[id="standardTimeVaryingProfile"]').select(timeVaryingProfile);
  }

  static selectCustomTimeVaryingProfile(customTimeVaryingProfile: string) {
    cy.get(".profiles").contains(customTimeVaryingProfile).click();
  }

  static timeVaryingProfileCorrectlySaved(timeVaryingProfileName: string) {
    cy.get('[id="timeVaryingProfileName-THREE_DAY"]').should("have.text", timeVaryingProfileName);
  }

  static selectPredefinedProfile(selectedProfile: string) {
    switch (selectedProfile) {
      case "UK road 2018":
        cy.get('[id="profile-system-profile-UK_ROAD_2018"]').click();
        break;
      case "UK road 2019":
        cy.get('[id="profile-system-profile-UK_ROAD_2019"]').click();
        break;
      case "UK road 2020":
        cy.get('[id="profile-system-profile-UK_ROAD_2020"]').click();
        break;
      case "UK road 2021":
        cy.get('[id="profile-system-profile-UK_ROAD_2021"]').click();
        break;
      case "UK road 2022":
        cy.get('[id="profile-system-profile-UK_ROAD_2022"]').click();
        break;
      default:
        throw new Error("Unknown road profile: " + selectedProfile);
    }
  }

  static selectBuildingsPanel() {
    cy.get('[id="collapsible-panel-BUILDING-line"]').click();
  }

  static checkSpecificText(specificText: string) {
    cy.get(".paragraph").should("contain", specificText);
  }

  static checkUrlLocation(specificURL: string) {
    cy.get(".introductionContainer a[target='_blank']").should("have.attr", "href").and("include", specificURL);
  }

  static checkListboxValues(dataTable: any) {
    dataTable.hashes().forEach((element: any) => {
      switch (element.listboxName) {
        case "Efflux type":
          cy.get('[id="effluxType"]').should(element.checkType, element.listboxValue);
          break;
        case "Sector group":
          cy.get('[id="sourceListDetailSectorGroup"]').should(element.checkType, element.listboxValue);
          break;
        default:
          throw new Error("Unknown listbox: " + element.specificListbox);
      }
    });
  }

  static checkListboxAmount(specificListbox: string, optionAmount: string) {
    switch (specificListbox) {
      case "Sector group":
        cy.get('[id="sourceListDetailSectorGroup"]').find("option").not(":disabled").should("have.length", optionAmount);
        break;
      case "Efflux type":
        cy.get('[id="effluxType"]').find("option").not(":disabled").should("have.length", optionAmount);
        break;
      default:
        throw new Error("Unknown listbox: " + specificListbox);
    }
  }

  static openCalculationJobPanel(jobPanelName: string) {
    switch (jobPanelName) {
      case "Calculation job settings":
        this.openPanelWhenClosed('[id="collapsible-panel-CalculationJobSettings"]');
        break;
      case "Development pressure search":
        this.openPanelWhenClosed('[id="collapsible-panel-DevelopmentPressureSearch"]');
        break;
      case "Meteorological settings":
        this.openPanelWhenClosed('[id="collapsible-panel-MeteorologicalSettings"]');
        break;
      case "Advanced settings":
        this.openPanelWhenClosed('[id="collapsible-panel-AdvancedSettings"]');
        break;
      default:
        throw new Error("Unknown calculation job panel: " + jobPanelName);
    }
  }

  static openPanelWhenClosed(element: string) {
    cy.get(element)
      .should("have.attr", "class")
      .then((classList) => {
        const classes: any = classList.split(" ");
        if (!classes.includes("open")) {
          cy.get(element).click();
        }
      });
  }

  static selectCalculationMethod(calculationMethod: string) {
    this.openCalculationJobPanel("Calculation job settings");
    cy.get('[id="scenarioResultType"]').select(calculationMethod);
  }

  static calculationMethodHintVisible(isHintVisible: string) {
    cy.get('[id="scenarioResultType"]')
      .parent()
      .parent()
      .parent()
      .parent()
      .within(() => {
        if (isHintVisible == "visible") {
          cy.get('[id="inputHintMessage"]').should("be.visible");
        } else {
          cy.get('[id="inputHintMessage"]').should("not.exist");
        }
      });
  }

  static selectOptionForTimeVaryingProfile(profileType: string, profileOption: string) {
    switch (profileType) {
      case "diurnal":
        cy.get('[id="timeVaryingProfileSelection-THREE_DAY"]').select(profileOption);
        break;
      case "monthly":
        cy.get('[id="timeVaryingProfileSelection-MONTHLY"]').select(profileOption);
        break;
      default:
        throw new Error("Unknown profile type: " + profileType + " or unknown option: " + profileOption);
    }
  }

  static timeVaryingProfileIsCorrectlySaved(dataTable: any) {
    dataTable.hashes().forEach((dataRow: any) => {
      switch (dataRow.profileTitle) {
        case "Diurnal profile":
          cy.get('[id="timeVaryingProfileTitle-THREE_DAY"]').should("have.text", dataRow.profileTitle);
          cy.get('[id="timeVaryingProfileName-THREE_DAY"]').should("have.text", dataRow.profileName);
          cy.get('[id="timeVaryingProfileType-THREE_DAY"]').should("have.text", dataRow.profileType);
          break;
        case "Monthly profile":
          cy.get('[id="timeVaryingProfileTitle-MONTHLY"]').should("have.text", dataRow.profileTitle);
          cy.get('[id="timeVaryingProfileName-MONTHLY"]').should("have.text", dataRow.profileName);
          cy.get('[id="timeVaryingProfileType-MONTHLY"]').should("have.text", dataRow.profileType);
          break;
        default:
          throw new Error("Unknown profile type: " + dataRow.profileType);
      }
    });
  }

  static nameScenario(scenarioName: string) {
    cy.get('[id="sourceListSituationName"]').clear().type(scenarioName);
  }

  static selectCountry(country: string) {
    cy.get('[id="permitAreaSelection"]').select(country);
  }

  static selectMetLocationSelectionMethod(locationMethod: string) {
    this.openCalculationJobPanel("Meteorological settings");
    switch (locationMethod) {
      case "NWP":
        cy.get('[id="tab-datasetTypeSelection-NWP"]').click();
        break;
      case "Observational":
        cy.get('[id="tab-datasetTypeSelection-OBS"]').click();
        break;
      case "Search":
        cy.get('[id="tab-metSiteSelection-SEARCH"]').click();
        break;
      case "Suggested":
        cy.get('[id="tab-metSiteSelection-SUGGEST"]').click();
        break;
      default:
        throw new Error("Unknown Met location method: " + locationMethod);
    }
  }

  static checkMarkExportADMS() {
    cy.get('[id="ADMSExport"]').check({ force: true });
  }

  static selectMetDataType(metDataType: string) {
    cy.get('[id="metDatasetType"]').select(metDataType);
  }

  // Preferences
  static switchWktConversion(wktConversion: string) {
    cy.get('[id="projectionSelect"]').select(wktConversion);
  }

  static assertSelectedSources(selectedSources: string) {
    cy.get('[id="calculation-job-detail-selected-sources"]').should("have.text", selectedSources);
  }

  static assertMetSettings(dataTable: any) {
    dataTable.hashes().forEach((metSetting: any) => {
      cy.get('[id="calculation-job-detail-met-site-type"]').should("have.text", metSetting.type);
      cy.get('[id="calculation-job-detail-met-site-data-type"]').should("have.text", metSetting.dataType);
      cy.get('[id="calculation-job-detail-met-site-location"]').should("have.text", metSetting.location);
      cy.get('[id="calculation-job-detail-met-years"]').should("have.text", metSetting.year);
    });
  }

  static assertAdvancedSettings(dataTable: any) {
    dataTable.hashes().forEach((advancedSetting: any) => {
      cy.get('[id="calculation-job-detail-min-monin-obukhov-length"]').should("have.text", advancedSetting.min_MO_length);
      cy.get('[id="calculation-job-detail-surface-albedo"]').should("have.text", advancedSetting.surface_albedo);
      cy.get('[id="calculation-job-detail-priestley-taylor-parameter"]').should("have.text", advancedSetting.PT_param);
      cy.get('[id="calculation-job-detail-plume-depletion-nh3"]').should("have.text", advancedSetting.plume_NH3);
      cy.get('[id="calculation-job-detail-plume-depletion-nox"]').should("have.text", advancedSetting.plume_NOx);
      cy.get('[id="calculation-job-detail-complex-terrain"]').should("have.text", advancedSetting.complex_terrain);
    });
  }

  static assertRoadEmissionsOptions(dataTable: any) {
    dataTable.hashes().forEach((roadEmissionsOption: any) => {
      cy.get('[id="calculation-job-detail-automatic-receptor-points"]').should("have.text", roadEmissionsOption.autom_receptor_points);
      cy.get('[id="calculation-job-detail-custom-receptor-points"]').should("have.text", roadEmissionsOption.custom_receptor_points);
    });
  }
}
