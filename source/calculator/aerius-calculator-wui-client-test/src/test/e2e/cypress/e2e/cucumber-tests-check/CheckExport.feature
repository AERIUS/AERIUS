#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: AERIUS Check, Export
    Test how exporting files is handled for product Check.

    @Smoke
    Scenario: Export farm source GML
        Given I login with correct username and password
        And I create a new source from the start page
        When I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Stalemissies'
        And I fill location with coordinates 'POINT(185032.22 482922.49)'
        And I add a lodging system with RAV code 'E 1.5.1', number of animals '100' and BWL code 'BB 93.06.008'
        And I validate the subsource with RAV code 'E1.5.1' with number of animals '100' and emission '2,0 kg/j'
        And I save the data
        And I select scenarioType 'Beoogd'
        And I save the scenario updates
        When I choose to export through the menu
        And I select 'Invoerbestanden' as export type
        And I fill in the following email address 'test@aerius.nl'
        And I click on the button to export
        Then the following notification is displayed 'Uw export is succesvol gestart. U ontvangt een e-mail met downloadlink van de bestanden wanneer deze klaar is.'

    @Smoke
    Scenario: Export farm source PDF
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose heat content type 'Niet geforceerd', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data
        When I choose to export through the menu
        And I select 'Rapportage' as export type        
        And I select create new calculation job
        When I create a new calculation job
        When I press the export button
        And I select additional info
        And I fill in the additional info form
            | inputField    | input                 |
            | Corporation   | Corporation X         |
            | ProjectName   | Project X             |
            | StreetAddress | Test street 123       |
            | Postcode      | postal code '1234 AB' |
            | City          | Testcity              |
            | Description   | xxx                   |
        And I select the acknowledge selection
        And I fill in the following email address 'test@aerius.nl'
        And I click on the button to export
        Then the following notification is displayed 'Uw export is succesvol gestart. U ontvangt een e-mail met downloadlink van de bestanden wanneer deze klaar is.'
