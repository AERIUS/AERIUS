#
# Crown copyright
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: ADMS Characteristics
    Test everything to do with ADMS characteristics

    # General
    @Smoke
    Scenario: Confirm available source types for each location type
        Given I login with correct username and password
        When I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Energy' and sector 'Large combustion plant'
        And I fill location with coordinates 'POINT(322447.2 594884.3)'
        When I open collapsible panel 'Location'
        And I fill location with coordinates 'LINESTRING(200380.4 493929.51,192361.53 488279.85,211861.97 487186.37)'
        When I open collapsible panel 'Source characteristics'
        Then I expect the source type dropdown to not exist
        When I open collapsible panel 'Location'
        And I fill location with coordinates 'POLYGON((173407.83 567557.35,166117.94 572842.51,166117.94 572842.51,171403.11 577580.94,173407.83 567557.35))'
        When I open collapsible panel 'Source characteristics'
        Then I expect the source type dropdown to have the following options
        | option |
        | Area   |
        | Volume |
    
    # Input
    @Smoke
    Scenario: Confirm available source characteristics and their default values for each source type - Sourcetypes Area and Volume - New (with switching)
        Given I login with correct username and password
        When I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Energy' and sector 'Large combustion plant'
        And I fill location with coordinates 'POLYGON((173407.83 567557.35,166117.94 572842.51,166117.94 572842.51,171403.11 577580.94,173407.83 567557.35))'
        When I open collapsible panel 'Source characteristics'
        
        # SourceType "Area"
        And I select the source type 'Area'
        Then I expect to see the following source characteristics options
        | testId                     | expectedValue | type     | shouldBeVisible |
        | buildingInfluence          |               |          | false           |
        | elevationAngle             |               |          | false           |
        | horizontalAngle            |               |          | false           |
        | sourceVerticalDimension    |               |          | false           |
        | sourceHeight               | 0.5           | text     | true            |
        | sourceDiameter             |               |          | false           |
        | buoyancyType               | Temperature   | dropdown | true            |
        | sourceTemperature          | 15            | text     | true            |
        | sourceDensity              |               |          | false           |
        | effluxType                 | Velocity      | dropdown | true            |
        | sourceVerticalVelocity     | 15            | text     | true            |
        | sourceVolumetricFlowRate   |               |          | false           |
        | sourceSpecificHeatCapacity | 1012          | text     | true            |
        And the available buoyancy types are
        | name        |
        | Ambient     |
        | Density     |
        | Temperature |
        And the available efflux types should be
        | name     |
        | Velocity |
        | Volume   |

        When I select the buoyancy type 'Ambient'
        Then I expect to see the following source characteristics options
        | testId            | expectedValue | type     | shouldBeVisible |
        | sourceTemperature |               |          | false           |
        | sourceDensity     |               |          | false           |

        When I select the buoyancy type 'Density'
        Then I expect to see the following source characteristics options
        | testId            | expectedValue | type     | shouldBeVisible |
        | sourceTemperature |               |          | false           |
        | sourceDensity     | 1.225         | text     | true            |


        When I select the efflux type 'Volume'
        Then I expect to see the following source characteristics options
        | testId                     | expectedValue | type     | shouldBeVisible |
        | sourceVerticalVelocity     |               |          | false           |
        | sourceVolumetricFlowRate   | 11.781        | text     | true            |

        # These lines to reset the buoyancy and efflux type are required until AER-1444 is fixed
        Given I select the buoyancy type 'Ambient'
        And I select the efflux type 'Velocity'

        # SourceType "Volume"
        When I select the source type 'Volume'
        Then I expect to see the following source characteristics options
        | testId                     | expectedValue | type     | shouldBeVisible |
        | buildingInfluence          |               |          | false           |
        | elevationAngle             |               |          | false           |
        | horizontalAngle            |               |          | false           |
        | sourceVerticalDimension    | 1             | text     | true            |
        | sourceHeight               | 0.5           | text     | true            |
        | sourceDiameter             |               |          | false           |
        | buoyancyType               |               |          | false           |
        | sourceTemperature          |               |          | false           |
        | sourceDensity              |               |          | false           |
        | effluxType                 |               |          | false           |
        | sourceVerticalVelocity     |               |          | false           |
        | sourceVolumetricFlowRate   |               |          | false           |
        | sourceSpecificHeatCapacity |               |          | false           |
        And there should be no buoyancy type dropdown
        And there should be no efflux type dropdown


    # Detail and edit
    @Smoke
    Scenario: Confirm available source characteristics and their default values - Detail and edit
        Given I login with correct username and password
        When I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Energy' and sector 'Large combustion plant'
        And I fill location with coordinates 'POINT(322447.2 594884.3)'
        When I open collapsible panel 'Source characteristics'
        And I edit the 'Source diameter' to be '1'
        Then I save the data

        When I select the source 'Source 1'
        Then I expect to see the following source characteristics in the detail panel
        | testId                            | expectedValue    | shouldBeVisible |
        | sourceADMSSourceTypeLabels        | Point            | true            |
        | sourceADMSBuildingInfluence       | None             | true            |
        | sourceADMSElevationAngle          |                  | false           |
        | sourceADMSHorizontalAngle         |                  | false           |
        | sourceADMSVerticalDimension       |                  | false           |
        | sourceADMSHeight                  | 0.50 m           | true            |
        | sourceADMSDiameter                | 1.00 m           | true            |
        | sourceADMSBuoyancyTypeDetailLabel | Temperature      | true            |
        | sourceADMSTemperature             | 15.00 °C         | true            |
        | sourceADMSEffluxTypeLabels        | Velocity         | true            |
        | sourceADMSVerticalVelocity        | 15.00 m/s        | true            |
        | sourceADMSSpecificHeatCapacity    | 1,012.000 J/K/kg | true            |

        When I choose to edit the source
        And I open collapsible panel 'Source characteristics'
        Then I expect to see the following source characteristics options
        | testId                     | expectedValue | type     | shouldBeVisible |
        | buildingInfluence          | false         | checkbox | true            |
        | elevationAngle             |               |          | false           |
        | horizontalAngle            |               |          | false           |
        | sourceVerticalDimension    |               |          | false           |
        | sourceHeight               | 0.5           | text     | true            |
        | sourceDiameter             | 1             | text     | true            |
        | buoyancyType               | Temperature   | dropdown | true            |
        | sourceTemperature          | 15            | text     | true            |
        | sourceDensity              |               |          | false           |
        | effluxType                 | Velocity      | dropdown | true            |
        | sourceVerticalVelocity     | 15            | text     | true            |
        | sourceVolumetricFlowRate   |               |          | false           |
        | sourceSpecificHeatCapacity | 1012          | text     | true            |

 
    Scenario: Confirm available source characteristics and their default values for each source type - SourceType Area - Detail and edit
        Given I login with correct username and password
        When I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Energy' and sector 'Large combustion plant'
        And I fill location with coordinates 'POLYGON((173407.83 567557.35,166117.94 572842.51,166117.94 572842.51,171403.11 577580.94,173407.83 567557.35))'
        When I open collapsible panel 'Source characteristics'
        And I select the source type 'Area'
        Then I save the data

        When I select the source 'Source 1'
        Then I expect to see the following source characteristics in the detail panel
        | testId                            | expectedValue    | shouldBeVisible |
        | sourceADMSSourceTypeLabels        | Area             | true            |
        | sourceADMSBuildingInfluence       | None             | true            |
        | sourceADMSElevationAngle          |                  | false           |
        | sourceADMSHorizontalAngle         |                  | false           |
        | sourceADMSVerticalDimension       |                  | false           |
        | sourceADMSHeight                  | 0.50 m           | true            |
        | sourceADMSDiameter                |                  | false           |
        | sourceADMSBuoyancyTypeDetailLabel | Temperature      | true            |
        | sourceADMSTemperature             | 15.00 °C         | true            |
        | sourceADMSEffluxTypeLabels        | Velocity         | true            |
        | sourceADMSVerticalVelocity        | 15.00 m/s        | true            |
        | sourceADMSSpecificHeatCapacity    | 1,012.000 J/K/kg | true            |

        When I choose to edit the source
        And I open collapsible panel 'Source characteristics'
        Then I expect to see the following source characteristics options
        | testId                     | expectedValue | type     | shouldBeVisible |
        | buildingInfluence          |               |          | false           |
        | elevationAngle             |               |          | false           |
        | horizontalAngle            |               |          | false           |
        | sourceVerticalDimension    |               |          | false           |
        | sourceHeight               | 0.5           | text     | true            |
        | sourceDiameter             |               |          | false           |
        | buoyancyType               | Temperature   | dropdown | true            |
        | sourceTemperature          | 15            | text     | true            |
        | sourceDensity              |               |          | false           |
        | effluxType                 | Velocity      | dropdown | true            |
        | sourceVerticalVelocity     | 15            | text     | true            |
        | sourceVolumetricFlowRate   |               |          | false           |
        | sourceSpecificHeatCapacity | 1012          | text     | true            |

    Scenario: Confirm available source characteristics and their default values for each source type - SourceType Volume - Detail and edit
        Given I login with correct username and password
        When I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Energy' and sector 'Large combustion plant'
        And I fill location with coordinates 'POLYGON((173407.83 567557.35,166117.94 572842.51,166117.94 572842.51,171403.11 577580.94,173407.83 567557.35))'
        When I open collapsible panel 'Source characteristics'
        And I select the source type 'Volume'
        Then I save the data

        When I select the source 'Source 1'
        Then I expect to see the following source characteristics in the detail panel
        | testId                            | expectedValue    | shouldBeVisible |
        | sourceADMSSourceTypeLabels        | Volume           | true            |
        | sourceADMSBuildingInfluence       | None             | true            |
        | sourceADMSElevationAngle          |                  | false           |
        | sourceADMSHorizontalAngle         |                  | false           |
        | sourceADMSVerticalDimension       | 1.00 m           | true            |
        | sourceADMSHeight                  | 0.50 m           | true            |
        | sourceADMSDiameter                |                  | false           |
        | sourceADMSBuoyancyTypeDetailLabel |                  | false           |
        | sourceADMSTemperature             |                  | false           |
        | sourceADMSEffluxTypeLabels        |                  | false           |
        | sourceADMSVerticalVelocity        |                  | false           |
        | sourceADMSSpecificHeatCapacity    |                  | false           |

        When I choose to edit the source
        And I open collapsible panel 'Source characteristics'
        Then I expect to see the following source characteristics options
        | testId                     | expectedValue | type     | shouldBeVisible |
        | buildingInfluence          |               |          | false           |
        | elevationAngle             |               |          | false           |
        | horizontalAngle            |               |          | false           |
        | sourceVerticalDimension    | 1             | text     | true            |
        | sourceHeight               | 0.5           | text     | true            |
        | sourceDiameter             |               |          | false           |
        | buoyancyType               |               |          | false           |
        | sourceTemperature          |               |          | false           |
        | sourceDensity              |               |          | false           |
        | effluxType                 |               |          | false           |
        | sourceVerticalVelocity     |               |          | false           |
        | sourceVolumetricFlowRate   |               |          | false           |
        | sourceSpecificHeatCapacity |               |          | false           |
    
    #TODO: Add tests for AER-1444 and AER-1471 once they are fixed.
