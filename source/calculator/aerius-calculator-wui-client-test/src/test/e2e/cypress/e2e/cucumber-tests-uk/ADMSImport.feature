#
# Crown copyright
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#
Feature: ADMS Import
  Test importing ADMS upl files and exporting to validate characteristics

  Scenario: Import a ADMS upl file and export and validate characteristics scenario 1
    Given I login with correct username and password with ADMS enabled
    When I select file 'adms/Run1/Run1.upl' to be imported
    And I navigate to the 'Assessment points' tab
    And I create the first assessment point
    And I enter the name 'My Custom Assessment point'
    And I enter the coordinates 'POINT(400878.4 399835.42)'
    And I save the assessment point
    
    And I navigate to the 'Calculation jobs' tab
    Then I create a new calculation job
    And I select country 'England'
    And I select project category 'Agriculture'
    And I select development pressure sources
        | source  |
        | Point1  |
        | Line1   |
        | Area1   |
        | Volume1 |
    And I select the uk calculation method 'Custom assessment points'
    And I select the uk meteorological site location selection method 'NWP'
    And I select the uk meteorological site location selection method 'Search'
    And I select the first meteorological site location
    And I select the meteorological year '2021'
    And I save the calculation job
    
    And I navigate to the 'Export' tab
    Then I select 'Calculation job' as export type
    And I mark export ADMS input files
    And I fill in the following email address 'test@aerius.nl'
    And I click on the button to export
    Then the following notification is displayed 'Export has started successfully. You will receive a download link by email when your files are available'
    And I wait till task is completed
    And I can extract downloaded results and validate it contains 1 file

    Then I validate ADMS source 'Run1/adms-export.upl' with ADMS export and skip characteristics
      | field                     |
      | SupAddInputPath           |
      | MetDataFileWellFormedPath |
      | HilRoughPath              |
      | GrdPtsPointsFilePath      |
      | MetLatitude               |
      | MetWindInSectors          |
      | MetSubsetYearStart        |

  Scenario: Import a ADMS upl file and export and validate characteristics scenario 2
    Given I login with correct username and password with ADMS enabled
    When I select file 'adms/Run2/Run2.upl' to be imported
    And I navigate to the 'Assessment points' tab
    And I create the first assessment point
    And I enter the name 'My Custom Assessment point'
    And I enter the coordinates 'POINT(400878.4 399835.42)'
    And I save the assessment point

    And I navigate to the 'Calculation jobs' tab
    Then I create a new calculation job
    And I select country 'Wales'
    And I select project category 'Agriculture'
    And I select development pressure sources
        | source  |
        | Point1  |
        | Line1   |
        | Area1   |
    And I select the uk calculation method 'Custom assessment points'
    And I select the uk meteorological site location selection method 'NWP'
    And I select the uk meteorological site location selection method 'Search'
    And I select the first meteorological site location
    And I select the meteorological year '2021'
    And I save the calculation job

    And I navigate to the 'Export' tab
    Then I select 'Calculation job' as export type
    And I mark export ADMS input files
    And I fill in the following email address 'test@aerius.nl'
    And I click on the button to export
    Then the following notification is displayed 'Export has started successfully. You will receive a download link by email when your files are available'
    And I wait till task is completed
    And I can extract downloaded results and validate it contains 1 file

    Then I validate ADMS source 'Run2/adms-export.upl' with ADMS export and skip characteristics
      | field                     |
      | SupAddInputPath           |
      | MetDataFileWellFormedPath |
      | HilRoughPath              |
      | GrdPtsPointsFilePath      |
      | MetLatitude               |
      | MetWindInSectors          |
      | MetSubsetYearStart        |
      | MetSubsetYearEnd          |

  Scenario: Import a ADMS upl file and export and validate characteristics scenario 3
    Given I login with correct username and password with ADMS enabled
    When I select file 'adms/Run3/Run3.upl' to be imported
    And I navigate to the 'Assessment points' tab
    And I create the first assessment point
    And I enter the name 'My Custom Assessment point'
    And I enter the coordinates 'POINT(400878.4 399835.42)'
    And I save the assessment point

    And I navigate to the 'Calculation jobs' tab
    Then I create a new calculation job
    And I select country 'Scotland'        
    And I select project category 'Agriculture'
    And I select development pressure sources
        | source  |
        | Point1  |
        | Line1   |
        | Area1   |
    And I select the uk calculation method 'Custom assessment points'
    And I select the uk meteorological site location selection method 'NWP'
    And I select the uk meteorological site location selection method 'Search'
    And I select the first meteorological site location
    And I select the meteorological year '2021'
    And I save the calculation job

    And I navigate to the 'Export' tab
    Then I select 'Calculation job' as export type
    And I mark export ADMS input files
    And I fill in the following email address 'test@aerius.nl'
    And I click on the button to export
    Then the following notification is displayed 'Export has started successfully. You will receive a download link by email when your files are available'
    And I wait till task is completed
    And I can extract downloaded results and validate it contains 1 file

    Then I validate ADMS source 'Run3/adms-export.upl' with ADMS export and skip characteristics
      | field                     |
      | SupAddInputPath           |
      | MetDataFileWellFormedPath |
      | HilRoughPath              |
      | GrdPtsPointsFilePath      |
      | MetLatitude               |
      | MetWindInSectors          |
      | MetSubsetYearStart        |
      | MetSubsetYearEnd          |

  Scenario: Import a ADMS upl file and export and validate characteristics scenario 4
    Given I login with correct username and password with ADMS enabled
    When I select file 'adms/Run4/Run4.upl' to be imported
    And I navigate to the 'Assessment points' tab
    And I create the first assessment point
    And I enter the name 'My Custom Assessment point'
    And I enter the coordinates 'POINT(400878.4 399835.42)'
    And I save the assessment point

    And I navigate to the 'Calculation jobs' tab
    Then I create a new calculation job
    And I select country 'Northern Ireland'
    And I select project category 'Agriculture'
    And I select development pressure sources
        | source  |
        | Point1  |
        | Line1   |
        | Area1   |
    And I select the uk calculation method 'Custom assessment points'
    And I select the uk meteorological site location selection method 'NWP'
    And I select the uk meteorological site location selection method 'Search'
    And I select the first meteorological site location
    And I select the meteorological year '2021'
    And I save the calculation job

    And I navigate to the 'Export' tab
    Then I select 'Calculation job' as export type
    And I mark export ADMS input files
    And I fill in the following email address 'test@aerius.nl'
    And I click on the button to export
    Then the following notification is displayed 'Export has started successfully. You will receive a download link by email when your files are available'
    And I wait till task is completed
    And I can extract downloaded results and validate it contains 1 file

    Then I validate ADMS source 'Run4/adms-export.upl' with ADMS export and skip characteristics
      | field                     |
      | SupAddInputPath           |
      | MetDataFileWellFormedPath |
      | HilRoughPath              |
      | GrdPtsPointsFilePath      |
      | MetLatitude               |
      | MetWindInSectors          |
      | MetSubsetYearStart        |
      | MetSubsetYearEnd          |

  Scenario: Import a ADMS upl file and export and validate characteristics scenario 5
    Given I login with correct username and password with ADMS enabled
    When I select file 'adms/Run5/Run5.upl' to be imported
    And I navigate to the 'Assessment points' tab
    And I create the first assessment point
    And I enter the name 'My Custom Assessment point'
    And I enter the coordinates 'POINT(400878.4 399835.42)'
    And I save the assessment point

    And I navigate to the 'Calculation jobs' tab
    Then I create a new calculation job
    And I select country 'England'
    And I select project category 'Agriculture'
    And I select development pressure sources
        | source  |
        | Point1  |
        | Line1   |
        | Area1   |
    And I select the uk calculation method 'Custom assessment points'
    And I select the uk meteorological site location selection method 'NWP'
    And I select the uk meteorological site location selection method 'Search'
    And I select the first meteorological site location
    And I select the meteorological year '2021'
    And I save the calculation job

    And I navigate to the 'Export' tab
    Then I select 'Calculation job' as export type
    And I mark export ADMS input files
    And I fill in the following email address 'test@aerius.nl'
    And I click on the button to export
    Then the following notification is displayed 'Export has started successfully. You will receive a download link by email when your files are available'
    And I wait till task is completed
    And I can extract downloaded results and validate it contains 1 file

    Then I validate ADMS source 'Run5/adms-export.upl' with ADMS export and skip characteristics
      | field                     |
      | SupAddInputPath           |
      | MetDataFileWellFormedPath |
      | HilRoughPath              |
      | GrdPtsPointsFilePath      |
      | MetLatitude               |
      | MetWindInSectors          |
      | MetSubsetYearStart        |
      | MetSubsetYearEnd          |

  Scenario: Import a ADMS upl file and export and validate characteristics scenario 6
    Given I login with correct username and password with ADMS enabled
    When I select file 'adms/Run6/Run6.upl' to be imported
    And I navigate to the 'Assessment points' tab
    And I create the first assessment point
    And I enter the name 'My Custom Assessment point'
    And I enter the coordinates 'POINT(400878.4 399835.42)'
    And I save the assessment point

    And I navigate to the 'Calculation jobs' tab
    Then I create a new calculation job
    And I select country 'England'
    And I select project category 'Agriculture'
    And I select development pressure sources
        | source  |
        | Point1  |
        | Line1   |
        | Area1   |
    And I select the uk calculation method 'Custom assessment points'
    And I select the uk meteorological site location selection method 'NWP'
    And I select the uk meteorological site location selection method 'Search'
    And I select the first meteorological site location
    And I select the meteorological year '2021'
    And I save the calculation job

    And I navigate to the 'Export' tab
    Then I select 'Calculation job' as export type
    And I mark export ADMS input files
    And I fill in the following email address 'test@aerius.nl'
    And I click on the button to export
    Then the following notification is displayed 'Export has started successfully. You will receive a download link by email when your files are available'
    And I wait till task is completed
    And I can extract downloaded results and validate it contains 1 file

    Then I validate ADMS source 'Run6/adms-export.upl' with ADMS export and skip characteristics
      | field                     |
      | SupAddInputPath           |
      | MetDataFileWellFormedPath |
      | HilRoughPath              |
      | GrdPtsPointsFilePath      |
      | MetLatitude               |
      | MetWindInSectors          |
      | MetSubsetYearStart        |
      | MetSubsetYearEnd          |

  Scenario: Import a ADMS upl file and export and validate characteristics scenario 7
    Given I login with correct username and password with ADMS enabled
    When I select file 'adms/Run7/Run7.upl' to be imported

    # remove road and recreate it.    
    And I open emission source list
    And I select the source 'Road transport network'
    And I select the source 'Road1'
    When I delete source 'Road1'
    
    When I create a new source from the scenario input page
    And I name the source 'Road1' and select sector group 'Traffic'
    And I fill location with coordinates 'LINESTRING(399900 396200,400100 396200)'
    Then I open panel "Source characteristics"
    And I fill road width with value '10'
    And I open panel 'Traffic direction, speed, traffic and emission'
    And I choose area 'London'
    And I choose road type 'London - Inner' direction 'From A to B'
    And I add an emission source of type 'Custom specification'
    And I fill description with 'Description 123X'
    And I choose time unit '/hour' with number of vehicles '9999'
    And I fill emission 'NOx' with value '12345'
    And I fill emission 'NH3' with value '23456'
    And I save the data

    And I navigate to the 'Assessment points' tab
    And I create the first assessment point
    And I enter the name 'My Custom Assessment point'
    And I enter the coordinates 'POINT(400878.4 399835.42)'
    And I save the assessment point

    And I navigate to the 'Calculation jobs' tab
    Then I create a new calculation job
    And I select country 'England'    
    And I select project category 'Agriculture'
    And I select development pressure sources
        | source |
        | Road1  |
    And I select the uk calculation method 'Custom assessment points'
    And I select the uk meteorological site location selection method 'NWP'
    And I select the uk meteorological site location selection method 'Search'
    And I select the first meteorological site location
    And I select the meteorological year '2021'
    And I save the calculation job

    And I navigate to the 'Export' tab
    Then I select 'Calculation job' as export type
    And I mark export ADMS input files
    And I fill in the following email address 'test@aerius.nl'
    And I click on the button to export
    Then the following notification is displayed 'Export has started successfully. You will receive a download link by email when your files are available'
    And I wait till task is completed
    And I can extract downloaded results and validate it contains 2 file
    
    Then I validate ADMS source 'Run7/adms-export-nox.upl' with ADMS export pollutant 'NOx' and skip characteristics
      | field                     |
      | SupAddInputPath           |
      | MetDataFileWellFormedPath |
      | HilRoughPath              |
      | GrdPtsPointsFilePath      |
      | MetLatitude               |
      | MetWindInSectors          |
      | MetSubsetYearStart        |
      | MetSubsetYearEnd          |
      | SupTimeVaryingFACPath     |

    Then I validate ADMS source 'Run7/adms-export-nh3.upl' with ADMS export pollutant 'NH3' and skip characteristics
      | field                     |
      | SupAddInputPath           |
      | MetDataFileWellFormedPath |
      | HilRoughPath              |
      | GrdPtsPointsFilePath      |
      | MetLatitude               |
      | MetWindInSectors          |
      | MetSubsetYearStart        |
      | MetSubsetYearEnd          |
      | SupTimeVaryingFACPath     |

  Scenario: Import a ADMS upl file and export and validate characteristics scenario 8
    Given I login with correct username and password with ADMS enabled
    When I select file 'adms/Run8/Run8.upl' to be imported

    # remove road and recreate it.    
    And I open emission source list
    And I select the source 'Road transport network'
    And I select the source 'Road1'
    When I delete source 'Road1'
    
    When I create a new source from the scenario input page
    And I name the source 'Road1' and select sector group 'Traffic'
    And I fill location with coordinates 'LINESTRING(399900 396200,400100 396200)'
    Then I open panel "Source characteristics"
    And I fill road width with value '10'
    And I open panel 'Traffic direction, speed, traffic and emission'
    And I choose area 'London'
    And I choose road type 'London - Inner' direction 'From A to B'
    And I add an emission source of type 'Custom specification'
    And I fill description with 'Description 123X'
    And I choose time unit '/hour' with number of vehicles '9999'
    And I fill emission 'NOx' with value '12345'
    And I fill emission 'NH3' with value '23456'
    And I save the data

    And I navigate to the 'Assessment points' tab
    And I create the first assessment point
    And I enter the name 'My Custom Assessment point'
    And I enter the coordinates 'POINT(400878.4 399835.42)'
    And I save the assessment point

    And I navigate to the 'Calculation jobs' tab
    Then I create a new calculation job
    And I select country 'England'    
    And I select project category 'Agriculture'
    And I select development pressure sources
        | source  |
        | Road1   |
    And I select the uk calculation method 'Custom assessment points'
    And I select the uk meteorological site location selection method 'NWP'
    And I select the uk meteorological site location selection method 'Search'
    And I select the first meteorological site location
    And I select the meteorological year '2021'
    And I save the calculation job

    And I navigate to the 'Export' tab
    Then I select 'Calculation job' as export type
    And I mark export ADMS input files
    And I fill in the following email address 'test@aerius.nl'
    And I click on the button to export
    Then the following notification is displayed 'Export has started successfully. You will receive a download link by email when your files are available'
    And I wait till task is completed
    And I can extract downloaded results and validate it contains 2 file

    Then I validate ADMS source 'Run8/adms-export-nox.upl' with ADMS export pollutant 'NOx' and skip characteristics
      | field                     |
      | SupAddInputPath           |
      | MetDataFileWellFormedPath |
      | HilRoughPath              |
      | GrdPtsPointsFilePath      |
      | MetLatitude               |
      | MetWindInSectors          |
      | MetSubsetYearStart        |
      | MetSubsetYearEnd          |
      | SupTimeVaryingFACPath     |
    Then I validate ADMS source 'Run8/adms-export-nh3.upl' with ADMS export pollutant 'NH3' and skip characteristics
      | field                     |
      | SupAddInputPath           |
      | MetDataFileWellFormedPath |
      | HilRoughPath              |
      | GrdPtsPointsFilePath      |
      | MetLatitude               |
      | MetWindInSectors          |
      | MetSubsetYearStart        |
      | MetSubsetYearEnd          |
      | SupTimeVaryingFACPath     |

  Scenario: Import a ADMS upl file and export and validate characteristics scenario 9
    Given I login with correct username and password with ADMS enabled
    When I select file 'adms/Run9/Run9.upl' to be imported
    
    # remove road and recreate it.    
    And I open emission source list
    And I select the source 'Road transport network'
    And I select the source 'Road1'
    When I delete source 'Road1'
    
    When I create a new source from the scenario input page
    And I name the source 'Road1' and select sector group 'Traffic'
    And I fill location with coordinates 'LINESTRING(399900 396200,400100 396200)'
    Then I open panel "Source characteristics"
    And I fill road width with value '10'
    And I open panel 'Traffic direction, speed, traffic and emission'
    And I choose area 'London'
    And I choose road type 'London - Inner' direction 'From A to B'
    And I add an emission source of type 'Custom specification'
    And I fill description with 'Description 123X'
    And I choose time unit '/hour' with number of vehicles '9999'
    And I fill emission 'NOx' with value '12345'
    And I fill emission 'NH3' with value '23456'
    And I save the data
    
    And I navigate to the 'Assessment points' tab
    And I create the first assessment point
    And I enter the name 'My Custom Assessment point'
    And I enter the coordinates 'POINT(400878.4 399835.42)'
    And I save the assessment point

    And I navigate to the 'Calculation jobs' tab
    Then I create a new calculation job
    And I select country 'England'    
    And I select project category 'Agriculture'
    And I select development pressure sources
        | source |
        | Road1  |
    And I select the uk calculation method 'Custom assessment points'
    And I select the uk meteorological site location selection method 'NWP'
    And I select the uk meteorological site location selection method 'Search'
    And I select the first meteorological site location
    And I select the meteorological year '2021'
    And I save the calculation job

    And I navigate to the 'Export' tab
    Then I select 'Calculation job' as export type
    And I mark export ADMS input files
    And I fill in the following email address 'test@aerius.nl'
    And I click on the button to export
    Then the following notification is displayed 'Export has started successfully. You will receive a download link by email when your files are available'
    And I wait till task is completed
    And I can extract downloaded results and validate it contains 2 file

    Then I validate ADMS source 'Run9/adms-export-nox.upl' with ADMS export pollutant 'NOx' and skip characteristics
      | field                     |
      | SupAddInputPath           |
      | MetDataFileWellFormedPath |
      | HilRoughPath              |
      | GrdPtsPointsFilePath      |
      | MetLatitude               |
      | MetWindInSectors          |
      | MetSubsetYearStart        |
      | MetSubsetYearEnd          |
      | SupTimeVaryingFACPath     |
    Then I validate ADMS source 'Run9/adms-export-nh3.upl' with ADMS export pollutant 'NH3' and skip characteristics
      | field                     |
      | SupAddInputPath           |
      | MetDataFileWellFormedPath |
      | HilRoughPath              |
      | GrdPtsPointsFilePath      |
      | MetLatitude               |
      | MetWindInSectors          |
      | MetSubsetYearStart        |
      | MetSubsetYearEnd          |
      | SupTimeVaryingFACPath     |

  Scenario: Import a ADMS upl file and export and validate characteristics scenario 10
    Given I login with correct username and password with ADMS enabled
    When I select file 'adms/Run10/Run10.upl' to be imported
    
    # remove road and recreate it.    
    And I open emission source list
    And I select the source 'Road transport network'
    And I select the source 'Road1'
    When I delete source 'Road1'
    
    When I create a new source from the scenario input page
    And I name the source 'Road1' and select sector group 'Traffic'
    And I fill location with coordinates 'LINESTRING(399900 396200,400100 396200)'
    Then I open panel "Source characteristics"
    And I fill road width with value '10'
    And I open panel 'Traffic direction, speed, traffic and emission'
    And I choose area 'London'
    And I choose road type 'London - Inner' direction 'From A to B'
    And I add an emission source of type 'Custom specification'
    And I fill description with 'Description 123X'
    And I choose time unit '/hour' with number of vehicles '9999'
    And I fill emission 'NOx' with value '12345'
    And I fill emission 'NH3' with value '23456'
    And I save the data

    And I navigate to the 'Assessment points' tab
    And I create the first assessment point
    And I enter the name 'My Custom Assessment point'
    And I enter the coordinates 'POINT(400878.4 399835.42)'
    And I save the assessment point

    And I navigate to the 'Calculation jobs' tab
    Then I create a new calculation job
    And I select country 'England'    
    And I select project category 'Agriculture'
    And I select development pressure sources
        | source |
        | Road1  |
    And I select the uk calculation method 'Custom assessment points'
    And I select the uk meteorological site location selection method 'NWP'
    And I select the uk meteorological site location selection method 'Search'
    And I select the first meteorological site location
    And I select the meteorological year '2021'
    And I save the calculation job

    And I navigate to the 'Export' tab
    Then I select 'Calculation job' as export type
    And I mark export ADMS input files
    And I fill in the following email address 'test@aerius.nl'
    And I click on the button to export
    Then the following notification is displayed 'Export has started successfully. You will receive a download link by email when your files are available'
    And I wait till task is completed
    And I can extract downloaded results and validate it contains 2 file

    Then I validate ADMS source 'Run10/adms-export-nox.upl' with ADMS export pollutant 'NOx' and skip characteristics
      | field                     |
      | SupAddInputPath           |
      | MetDataFileWellFormedPath |
      | HilRoughPath              |
      | GrdPtsPointsFilePath      |
      | MetLatitude               |
      | MetWindInSectors          |
      | MetSubsetYearStart        |
      | MetSubsetYearEnd          |
      | SupTimeVaryingFACPath     |
    Then I validate ADMS source 'Run10/adms-export-nh3.upl' with ADMS export pollutant 'NH3' and skip characteristics
      | field                     |
      | SupAddInputPath           |
      | MetDataFileWellFormedPath |
      | HilRoughPath              |
      | GrdPtsPointsFilePath      |
      | MetLatitude               |
      | MetWindInSectors          |
      | MetSubsetYearStart        |
      | MetSubsetYearEnd          |
      | SupTimeVaryingFACPath     |

  Scenario: Import a ADMS upl file and export and validate characteristics scenario 11 NH3 AllSource
    Given I login with correct username and password with ADMS enabled
    When I select file 'adms/Run11/NH3_AllSources/Run11_NH3_AllSources.upl' to be imported

    # remove road and recreate it.
    And I open emission source list
    And I select the source 'Road transport network'
    And I select the source 'Road1'
    When I delete source 'Road1'
    
    When I create a new source from the source page
    And I name the source 'Road1' and select sector group 'Traffic'
    And I fill location with coordinates 'LINESTRING(399900 396200,400100 396200)'
    Then I open panel "Source characteristics"
    And I fill road width with value '10'
    And I open panel 'Traffic direction, speed, traffic and emission'
    And I choose area 'London'
    And I choose road type 'London - Inner' direction 'From A to B'
    And I add an emission source of type 'Custom specification'
    And I fill description with 'Description 123X'
    And I choose time unit '/hour' with number of vehicles '9999'
    And I fill emission 'NOx' with value '12345'
    And I fill emission 'NH3' with value '23456'
    And I save the data

    And I navigate to the 'Assessment points' tab
    And I create the first assessment point
    And I enter the name 'My Custom Assessment point'
    And I enter the coordinates 'POINT(400878.4 399835.42)'
    And I save the assessment point

    And I navigate to the 'Calculation jobs' tab
    Then I create a new calculation job
    And I select country 'England'    
    And I select project category 'Agriculture'
    And I select development pressure sources
        | source |
        | Road1  |
    And I select the uk calculation method 'Custom assessment points'
    And I select the uk meteorological site location selection method 'NWP'
    And I select the uk meteorological site location selection method 'Search'
    And I select the first meteorological site location
    And I select the meteorological year '2021'
    And I save the calculation job

    And I navigate to the 'Export' tab
    Then I select 'Calculation job' as export type
    And I mark export ADMS input files
    And I fill in the following email address 'test@aerius.nl'
    And I click on the button to export
    Then the following notification is displayed 'Export has started successfully. You will receive a download link by email when your files are available'
    And I wait till task is completed
    And I can extract downloaded results and validate it contains 2 file

    Then I validate ADMS source 'Run11/NH3_AllSources/adms-export-nox.upl' with ADMS export pollutant 'NOx' and skip characteristics
      | field                     |
      | SupAddInputPath           |
      | MetDataFileWellFormedPath |
      | HilRoughPath              |
      | GrdPtsPointsFilePath      |
      | MetLatitude               |
      | MetWindInSectors          |
      | MetSubsetYearStart        |
      | MetSubsetYearEnd          |
      | SupTimeVaryingFACPath     |
    Then I validate ADMS source 'Run11/NH3_AllSources/adms-export-nh3.upl' with ADMS export pollutant 'NH3' and skip characteristics
      | field                     |
      | SupAddInputPath           |
      | MetDataFileWellFormedPath |
      | HilRoughPath              |
      | GrdPtsPointsFilePath      |
      | MetLatitude               |
      | MetWindInSectors          |
      | MetSubsetYearStart        |
      | MetSubsetYearEnd          |
      | SupTimeVaryingFACPath     |

  Scenario: Import a ADMS upl file and export and validate characteristics scenario 11 NOx_NonRoads
    Given I login with correct username and password with ADMS enabled
    When I select file 'adms/Run11/NOx_NonRoads/Run11_NOx_NonRoads.upl' to be imported
    And I navigate to the 'Assessment points' tab
    And I create the first assessment point
    And I enter the name 'My Custom Assessment point'
    And I enter the coordinates 'POINT(400878.4 399835.42)'
    And I save the assessment point
    And I navigate to the 'Calculation jobs' tab
    Then I create a new calculation job
    And I select country 'England'    
    And I select project category 'Agriculture'
    And I select development pressure sources
        | source |
        | Point1 |
    And I select the uk calculation method 'Custom assessment points'
    And I select the uk meteorological site location selection method 'NWP'
    And I select the uk meteorological site location selection method 'Search'
    And I select the first meteorological site location
    And I select the meteorological year '2021'
    And I save the calculation job

    And I navigate to the 'Export' tab
    Then I select 'Calculation job' as export type    
    And I mark export ADMS input files
    And I fill in the following email address 'test@aerius.nl'
    And I click on the button to export
    Then the following notification is displayed 'Export has started successfully. You will receive a download link by email when your files are available'
    And I wait till task is completed
    And I can extract downloaded results and validate it contains 1 file

    Then I validate ADMS source 'Run11/NOx_NonRoads/adms-export.upl' with ADMS export and skip characteristics
      | field                     |
      | SupAddInputPath           |
      | MetDataFileWellFormedPath |
      | HilRoughPath              |
      | GrdPtsPointsFilePath      |
      | MetLatitude               |
      | MetWindInSectors          |
      | MetSubsetYearStart        |
      | MetSubsetYearEnd          |

  Scenario: Import a ADMS upl file and export and validate characteristics scenario 11 NOx_Roads
    Given I login with correct username and password with ADMS enabled
    When I select file 'adms/Run11/NOx_Roads/Run11_NOx_Roads.upl' to be imported
    
    # remove road and recreate it.
    And I open emission source list
    And I select the source 'Road transport network'
    And I select the source 'Road1'
    When I delete source 'Road1'
    
    When I create a new source from the scenario input page
    And I name the source 'Road1' and select sector group 'Traffic'
    And I fill location with coordinates 'LINESTRING(399900 396200,400100 396200)'
    Then I open panel "Source characteristics"
    And I fill road width with value '10'
    And I open panel 'Traffic direction, speed, traffic and emission'
    And I choose area 'London'
    And I choose road type 'London - Inner' direction 'From A to B'
    And I add an emission source of type 'Custom specification'
    And I fill description with 'Description 123X'
    And I choose time unit '/hour' with number of vehicles '9999'
    And I fill emission 'NOx' with value '12345'
    And I fill emission 'NH3' with value '23456'
    And I save the data

    And I navigate to the 'Assessment points' tab
    And I create the first assessment point
    And I enter the name 'My Custom Assessment point'
    And I enter the coordinates 'POINT(400878.4 399835.42)'
    And I save the assessment point
    
    And I navigate to the 'Calculation jobs' tab
    Then I create a new calculation job
    And I select country 'England'    
    And I select project category 'Agriculture'
    And I select development pressure sources
        | source |
        | Road1  |
    And I select the uk calculation method 'Custom assessment points'
    And I select the uk meteorological site location selection method 'NWP'
    And I select the uk meteorological site location selection method 'Search'
    And I select the first meteorological site location
    And I select the meteorological year '2021'
    And I save the calculation job

    And I navigate to the 'Export' tab
    Then I select 'Calculation job' as export type
    And I mark export ADMS input files
    And I fill in the following email address 'test@aerius.nl'
    And I click on the button to export
    Then the following notification is displayed 'Export has started successfully. You will receive a download link by email when your files are available'
    And I wait till task is completed
    And I can extract downloaded results and validate it contains 2 file

    Then I validate ADMS source 'Run11/NOx_Roads/adms-export-nox.upl' with ADMS export pollutant 'NOx' and skip characteristics
      | field                     |
      | SupAddInputPath           |
      | MetDataFileWellFormedPath |
      | HilRoughPath              |
      | GrdPtsPointsFilePath      |
      | MetLatitude               |
      | MetWindInSectors          |
      | MetSubsetYearStart        |
      | MetSubsetYearEnd          |
      | SupTimeVaryingFACPath     |
    Then I validate ADMS source 'Run11/NOx_Roads/adms-export-nh3.upl' with ADMS export pollutant 'NH3' and skip characteristics
      | field                     |
      | SupAddInputPath           |
      | MetDataFileWellFormedPath |
      | HilRoughPath              |
      | GrdPtsPointsFilePath      |
      | MetLatitude               |
      | MetWindInSectors          |
      | MetSubsetYearStart        |
      | MetSubsetYearEnd          |
      | SupTimeVaryingFACPath     |

  Scenario: Import a ADMS upl file and export and validate characteristics scenario 12
    Given I login with correct username and password with ADMS enabled
    When I select file 'adms/Run12/Run12.upl' to be imported
    And I navigate to the 'Assessment points' tab
    And I create the first assessment point
    And I enter the name 'My Custom Assessment point'
    And I enter the coordinates 'POINT(400878.4 399835.42)'
    And I save the assessment point
    
    And I navigate to the 'Calculation jobs' tab
    Then I create a new calculation job
    And I select country 'England'    
    And I select project category 'Agriculture'
    And I select development pressure sources
        | source |
        | Point1 |
    And I select the uk calculation method 'Custom assessment points'
    And I select the uk meteorological site location selection method 'NWP'
    And I select the uk meteorological site location selection method 'Search'
    And I select the first meteorological site location
    And I select the meteorological year '2021'
    And I save the calculation job

    And I navigate to the 'Export' tab
    Then I select 'Calculation job' as export type
    And I mark export ADMS input files
    And I fill in the following email address 'test@aerius.nl'
    And I click on the button to export
    Then the following notification is displayed 'Export has started successfully. You will receive a download link by email when your files are available'
    And I wait till task is completed
    And I can extract downloaded results and validate it contains 1 file

    Then I validate ADMS source 'Run12/adms-export.upl' with ADMS export and skip characteristics
      | field                     |
      | SupAddInputPath           |
      | MetDataFileWellFormedPath |
      | HilRoughPath              |
      | GrdPtsPointsFilePath      |
      | MetLatitude               |
      | MetWindInSectors          |
      | MetSubsetYearStart        |
      | MetSubsetYearEnd          |
