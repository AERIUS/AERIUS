#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: InfoPanel
    InfoPanel popup tests based on selected location
    
    @Smoke
    Scenario: Use the search to find a hexagon 4277497 select it and validate infopanel content
        Given I login with correct username and password
        And I create a new source from the start page
        Then I start a search with '4277497' and validate '1' categories and the following detail results are shown
            | result                                                 |
            | Receptor result 1:Receptor 4277497 - x:168041 y:447078 |
        And I select search result 'Receptor result 1:Receptor 4277497 - x:168041 y:447078'
        Then I click on the center of the map and open infoMarker

        And I validate receptor id '4277497' and location 'x:168042 y:447079'
        And I validate infomarker values
            | infoPanelReceptorId | infoPanelCoordinate | infoPanelRelevantHexagon | infoPanelExceedingHexagon | infoPanelAboveCriticalLevel | infoPanelExtraAssessment |
            | 4277497             | x:168042 y:447079   | Nee                      | Nee                       | Nee                         | Ja                       |

        And I validate background substance 'NOₓ + NH₃' value '1.358,24 mol/ha/j'
        And I validate info panel nature areas
            | nature_area | contractor        | surface | protection       | status                            | areaId | habitatCount |
            | Binnenveld  | Provincie Utrecht | 111     | Habitatrichtlijn | Natura 2000-besluit 30 april 2014 | 65     | 2            |
        And I validate info panel habitas

    Scenario: Use the search to find a hexagon 6288293 select it and validate infopanel content
        Given I login with correct username and password
        And I create a new source from the start page
        Then I start a search with '6288293' and validate '1' categories and the following detail results are shown
            | result                                                 |
            | Receptor result 1:Receptor 6288293 - x:197914 y:517731 |
        And I select search result 'Receptor result 1:Receptor 6288293 - x:197914 y:517731'
        Then I click on the center of the map and open infoMarker

        And I validate receptor id '6288293' and location 'x:197914 y:517732'
        And I validate infomarker values
            | infoPanelReceptorId | infoPanelCoordinate | infoPanelRelevantHexagon | infoPanelExceedingHexagon | infoPanelAboveCriticalLevel | infoPanelExtraAssessment |
            | 6288293             | x:197914 y:517732   | Ja                       | Nee                       | Nee                         | Nee                      |
        And I validate background substance 'NOₓ + NH₃' value '1.164,08 mol/ha/j'
        And I validate info panel nature areas
            | nature_area | contractor                                  | surface  | protection                       | status                                                     | areaId | habitatCount |
            | De Wieden   | Provincie Overijssel                        | 9018     | Habitatrichtlijn, Vogelrichtlijn | Natura 2000-besluit 06 jan2014, wijzigingsbesluit jan 2015 | 35     | 23           |
            | Zwarte Meer | Ministerie van Infrastructuur en Waterstaat | 2162     | Habitatrichtlijn, Vogelrichtlijn | Natura 2000-besluit 2010                                   | 74     | 1            |
        And I validate info panel nature area 'De Wieden'
            | habitatcode                                                                      | relevantMapped | relevantCartographic | surfaceExtentGoal | areaInfoQualityGoal | AreainfoKDW    |
            | H3140lv - Kranswierwateren, in laagveengebieden                                  | 9,0 ha         | 7,9 ha               | Verbetering       | Verbetering         | 2.143 mol/ha/j |
            | ZGH3140lv - Kranswierwateren, in laagveengebieden                                | 56,6 ha        | 56,6 ha              | Verbetering       | Verbetering         | 2.143 mol/ha/j |
            | H3150baz - Meren met krabbenscheer en fonteinkruiden, buiten afgesloten zeearmen | 155,3 ha       | 134,5 ha             | Verbetering       | Verbetering         | 2.143 mol/ha/j |
        And I validate info panel habitas
            | habitatDescription                                                  | kdwTitle   | kdw   | surfaceTitle | surface |
            | H6510B - Glanshaver- en vossenstaarthooilanden (grote vossenstaart) | (mol/ha/j) | 1.571 | (ha)         | 0,5     |

    Scenario: Use the search to find a hexagon 6173084 outside nature area select it and validate infopanel content
        Given I login with correct username and password
        And I create a new source from the start page
        Then I start a search with '6173084' and validate '1' categories and the following detail results are shown
            | result                                                 |
            | Receptor result 1:Receptor 6173084 - x:98618 y:513701 |
        And I select search result 'Receptor result 1:Receptor 6173084 - x:98618 y:513701'
        Then I click on the center of the map and open infoMarker

        And I validate receptor id '6173084' and location 'x:98619 y:513702'
        And I validate infomarker values
            | infoPanelReceptorId | infoPanelCoordinate | infoPanelRelevantHexagon | infoPanelExceedingHexagon | infoPanelAboveCriticalLevel | infoPanelExtraAssessment |
            | 6173084             | x:98619 y:513702    | Nee                       | Nee                       | Nee                         | Nee                     |
        And I validate background substance 'NOₓ + NH₃' value '-'
        And I validate info panel habitas

    Scenario: Use the search to find a hexagon 7650171 with calculation results select it and validate infopanel content
        Given I login with correct username and password
        And I select file 'AERIUS_project_calculation_with_results.zip' to be imported
        Then the following notification is displayed 'Eén bestand is succesvol geïmporteerd.'
        Then the situation name is 'Situatie 1'
        And the situation type is 'Beoogd'
        And the calculation year is '2024'

        And I navigate to the 'Results' tab
        Then the following notification is displayed 'Rekentaak 1 - AERIUS_project_calculation_with_results is gestart.'
        Then the following notification is displayed 'Rekentaak 1 - AERIUS_project_calculation_with_results is voltooid.'

        And I navigate to the 'Input' tab
        Then I start a search with '7650171' and validate '1' categories and the following detail results are shown
            | result                                                 |
            | Receptor result 1:Receptor 7650171 - x:112205 y:565603 |
        And I select search result 'Receptor result 1:Receptor 7650171 - x:112205 y:565603'
        Then I click on the center of the map and open infoMarker

        And I validate receptor id '7650171' and location 'x:112206 y:565604'
        And I validate background substance 'NOₓ + NH₃' value '826,44 mol/ha/j'
        And I validate info marker results for calculation task 'Resultaten Rekentaak 1 - AERIUS_project_calculation_with_results'
            | element                                              | title                                    | infoRowLabel1       | infoRowValue1   |
            | infoPanelResultScenarioType-PROJECT_CALCULATION      | Projectberekening                        | Depositie NOₓ + NH₃ | 0,01 mol/ha/j   |
            | infoPanelResultScenarioTypeTotal-PROJECT_CALCULATION | Totaal (Projectberekening + achtergrond) | Depositie NOₓ + NH₃ | 826,45 mol/ha/j |
            | infoPanelResultScenario-0                            | Situatie 1 - Beoogd                      | Depositie NOₓ + NH₃ | 0,12 mol/ha/j   |
            | infoPanelResultScenario-1                            | Situatie 8 - Referentie                  | Depositie NOₓ + NH₃ | 0,05 mol/ha/j   |
            | infoPanelResultScenario-2                            | Situatie 6 - Saldering                   | Depositie NOₓ + NH₃ | 0,06 mol/ha/j   |

    Scenario: Validate infopanel content - results are depending on calculation job type - Max temporary effect
        Given I login with correct username and password
        And I select file 'AERIUS_rekentaak_max_tijdelijk_effect.zip' to be imported
        And the following notification is displayed 'Eén bestand is succesvol geïmporteerd.'
        And I navigate to the 'Results' tab
        And the following notification is displayed 'Rekentaak 1 - AERIUS_rekentaak_max_tijdelijk_effect is gestart.'
        And the following notification is displayed 'Rekentaak 1 - AERIUS_rekentaak_max_tijdelijk_effect is voltooid.'
        And I navigate to the 'Input' tab
        And I start a search with '7601247' and validate '1' categories and the following detail results are shown
            | result                                                 |
            | Receptor result 1:Receptor 7601247 - x:112950 y:563884 |
        And I select search result 'Receptor result 1:Receptor 7601247 - x:112950 y:563884'

        When I click on the center of the map and open infoMarker
        Then I validate receptor id '7601247' and location 'x:112950 y:563884'
        And I validate background substance 'NOₓ + NH₃' value '1.218,13 mol/ha/j'
        And I validate info marker results for calculation task 'Resultaten Rekentaak 1 - AERIUS_rekentaak_max_tijdelijk_effect'
            | element                                               | title                                         | infoRowLabel1       | infoRowValue1     |
            | infoPanelResultScenarioType-MAX_TEMPORARY_EFFECT      | Max. tijdelijke effect                        | Depositie NOₓ + NH₃ | 0,04 mol/ha/j     |
            | infoPanelResultScenarioTypeTotal-MAX_TEMPORARY_EFFECT | Totaal (Max. tijdelijke effect + achtergrond) | Depositie NOₓ + NH₃ | 1.218,17 mol/ha/j |
            | infoPanelResultScenario-0                             | Situatie 1 - Tijdelijk                        | Depositie NOₓ + NH₃ | 0,04 mol/ha/j     |

    Scenario: Validate infopanel content - results are depending on calculation job type - Single scenario
        Given I login with correct username and password
        And I select file 'AERIUS_rekentaak_enkele_situatie.zip' to be imported
        And the following notification is displayed 'Eén bestand is succesvol geïmporteerd.'
        And I navigate to the 'Results' tab
        And the following notification is displayed 'Rekentaak 1 - AERIUS_rekentaak_enkele_situatie is gestart.'
        And the following notification is displayed 'Rekentaak 1 - AERIUS_rekentaak_enkele_situatie is voltooid.'
        And I navigate to the 'Input' tab
        And I start a search with '6430009' and validate '1' categories and the following detail results are shown
            | result                                                 |
            | Receptor result 1:Receptor 6430009 - x:108483 y:522728 |
        And I select search result 'Receptor result 1:Receptor 6430009 - x:108483 y:522728'

        When I click on the center of the map and open infoMarker
        Then I validate receptor id '6430009' and location 'x:108483 y:522728'
        And I validate background substance 'NOₓ + NH₃' value '1.557,15 mol/ha/j'
        And I validate info marker results for calculation task 'Resultaten Rekentaak 1 - AERIUS_rekentaak_enkele_situatie'
            | element                                               | title                                         | infoRowLabel1       | infoRowValue1     |
            | infoPanelResultScenario-0                             | Situatie 1 - Tijdelijk                        | Depositie NOₓ + NH₃ | 0,05 mol/ha/j     |            
