#
# Crown copyright
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: GenericUI
    Generic UI tests

    Scenario: Check personal data warning shown
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select the scenario name
        Then I expect the privacy warning to exist

        When I create a new source from the scenario input page
        And I select the name input field
        Then I expect the privacy warning to exist

        When I choose to cancel
        And I open the buildings panel
        And I create a new building from the input page
        And I select the building name input field
        Then I expect the privacy warning to exist

        When I navigate to the 'Assessment points' tab
        And I create the first assessment point
        And I select the name input field
        Then I expect the privacy warning to exist

    Scenario: Validate that when in demo mode message is show
        Given I login with correct username and password
        Then I validate that demo mode message is show

    Scenario: Check status of menu items
        Given I login with correct username and password
        Then I validate the language menu does not exist
        Then I validate the status of the menu items
        | menuId                             | status   |
        | NAV_ICON-MENU-INPUT                | disabled |
        | NAV_ICON-MENU-ASSESSMENT-POINTS    | disabled |
        | NAV_ICON-MENU-CALCULATE            | disabled |
        | NAV_ICON-MENU-RESULTS              | disabled |
        | NAV_ICON-MENU-EXPORT               | disabled |
        | NAV_ICON-MENU-NEXT                 | disabled |
        | NAV_ICON-MENU-MANUAL1              | enabled  |
        | NAV_ICON-MENU-SETTINGS             | enabled  |
        | NAV_ICON-MENU-HOTKEYS              | enabled  |
        | NAV_ICON-MENU-ABOUT                | enabled  |

        When I create a new source from the start page
        Then I validate the status of the menu items
        | menuId                             | status   |
        | NAV_ICON-MENU-INPUT                | enabled  |
        | NAV_ICON-MENU-ASSESSMENT-POINTS    | enabled  |
        | NAV_ICON-MENU-CALCULATE            | enabled  |
        | NAV_ICON-MENU-RESULTS              | disabled |
        | NAV_ICON-MENU-EXPORT               | enabled  |
        | NAV_ICON-MENU-NEXT                 | enabled  |
        | NAV_ICON-MENU-MANUAL1              | enabled  |
        | NAV_ICON-MENU-SETTINGS             | enabled  |
        | NAV_ICON-MENU-HOTKEYS              | enabled  |
        | NAV_ICON-MENU-ABOUT                | enabled  |

    @Smoke
    Scenario: Check cookie preferences and notices
        Given I login with correct username and password
        When I press the cookie preferences
        Then I validate the cookie preferences are 'visible'
        And I validate the value of cookie preference 'title' is 'The Air Pollution Assessment tool uses cookies'
        And I validate the value of cookie preference 'necessary-title' is 'Essential Cookies'
        And I validate the value of cookie preference 'optional-cookie-header' is 'Analytics Cookies'

        When I press the cookie notices
        Then I validate the cookie preferences are 'not visible'
        And I validate the value of the cookie notice 'cookie-policy' is 'Cookie policy ​'

        When I close the cookie notices
        And I validate the cookie preferences are 'visible'
        Then I select the cookie preferences status 'on'
        And I validate the cookie preferences should be 'on'

        When I close the cookie preferences
        Then I validate the cookie preferences are 'not visible'

        When I press the cookie preferences
        Then I validate the cookie preferences are 'visible'
        And I validate the cookie preferences should be 'on'
        Then I select the cookie preferences status 'off'
        And I validate the cookie preferences should be 'off'

        When I close the cookie preferences
        Then I validate the cookie preferences are 'not visible'

        When I press the cookie preferences
        Then I validate the cookie preferences should be 'off'
        Then I close the cookie preferences

    Scenario: Check preferences panel
        Given I login with correct username and password
        When I navigate to the 'Preferences' tab

        Then the selected WKT conversion method should be 'No conversion'
        And the setting for 'Advanced importing' should be 'off'
        And the setting for 'Automatically turning layers on or off' should be 'on'
        And the setting for 'Automatically switching manual pages' should be 'on'

        # Change settings and test whether they are saved
        When I set the WKT conversion method to 'EPSG:29903'
        When I change the setting for 'Advanced importing' to 'on'
        And I change the setting for 'Automatically turning layers on or off' to 'off'
        And I change the setting for 'Automatically switching manual pages' to 'off'
        And I close the panel
        Then the preferences panel is closed

        When I navigate to the 'Preferences' tab
        Then the selected WKT conversion method should be 'EPSG:29903 Irish grid'
        Then the setting for 'Advanced importing' should be 'on'
        And the setting for 'Automatically turning layers on or off' should be 'off'
        And the setting for 'Automatically switching manual pages' should be 'off'

        # TODO's:
        # Check linked manual
        # Check with and without WKT conversion

    # 15-3-2024 it seems that search results can vary should review this test
    Scenario: Search functionality
        Given I login with correct username and password
        And I create an empty scenario from the start page
        Given I start a search with '8693343' and validate '1' categories and the following detail results are shown
            | result                                                 |
            | Receptor result 1:Receptor 8693343 - x:141918 y:527315 |
        Given I start a search with 'x:141918 y:527315' and validate '2' categories and the following detail results are shown
            | result                                                 |
            | Receptor result 1:Receptor 8693343 - x:141918 y:527315 |
            | Coordinate result 1:x:141918 y:527315                  |
        Given I start a search with 'blackpool' and validate '2' categories and the following detail results are shown
            | result                                                     |
            | Town result 1:Blackpool, Blackpool                         |
            | Miscellaneous result 1:Blackpool Pleasure Beach, Blackpool |
            | Miscellaneous result 2:Blackpool North, Blackpool          |
            | Miscellaneous result 3:Blackpool Tower                     |
            | Miscellaneous result 4:Blackpool Zoo                       |
        Given I start a search with 'Londonderry' and validate '3' categories and the following detail results are shown
            | result                                                       |
            | Town result 1:Londonderry, Derry City And Strabane           |
            | Town result 2:Londonderry, Northallerton, North Yorkshire    |
            | Municipality result 1:West Midlands                          |
            | Municipality result 2:Derry                                  | 
            | Miscellaneous result 1:Londonderry Park, Ards And North Down |
            #| Street result 1:Londonderry                                 |

    Scenario: Search functionality no OSNI Discoverer errors
        Given I login with correct username and password
        And I create an empty scenario from the start page
        Given I start a search with '8693343' and validate '1' categories and the following detail results are shown
            | result                                                 |
            | Receptor result 1:Receptor 8693343 - x:141918 y:527315 |        
        Then I select search result 'Receptor result 1:Receptor 8693343 - x:141918 y:527315' and validate we have no Discoverer errors

    Scenario: System info message
        Given I login with correct username and password
        Then I validate no system info message is shown
        Given I open aerius system info update
        And I enter system info message 'Short system message !'
        Given I login with correct username and password
        Then I validate system info message 'Short system message !'
        Given I open aerius system info update
        And I delete info message
        Given I login with correct username and password
        Then I validate no system info message is shown
