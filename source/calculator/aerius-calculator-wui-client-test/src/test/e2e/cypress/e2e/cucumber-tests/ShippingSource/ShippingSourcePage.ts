/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class ShippingSourcePage {
  static editMaritimeDescriptionCategoryShipPerTimeUnitTimeUnitAverageResidenceTimeShorePowerFactor(
    shipDescription: string,
    shipCategory: string,
    shipsPerTimeUnit: string,
    timeUnit: string,
    averageResidenceTime: string,
    shorePowerFactor: string
  ) {
    cy.get('[id="collapsibleSubSources"] > .line > .title').click({
      force: true
    });
    cy.get('[id="sourceListButtonNewSource"] > .icon').click({ force: true });

    ShippingSourcePage.maritimeDescriptionCategory(shipDescription, shipCategory);
    ShippingSourcePage.visitsTimeUnit(shipsPerTimeUnit, timeUnit);
    ShippingSourcePage.averageResidenceTimeShorePower(averageResidenceTime, shorePowerFactor);
  }

  static editMaritimeDescriptionCategoryShipPerTimeUnitTimeUnit(shipDescription: string, shipCategory: string, shipsPerTimeUnit: string, timeUnit: string) {
    cy.get('[id="collapsibleSubSources"] > .line > .title').click({
      force: true
    });
    cy.get('[id="sourceListButtonNewSource"] > .icon').click({ force: true });

    cy.get('[id="emissionSourceMaritimeStandardDescription"]').clear({ force: true }).type(shipDescription);
    cy.get('[id="emissionSourceMaritimeShippingCategory"]').select(shipCategory);
    cy.get('[id="maritimeMovementInput"]').type("{selectAll}").type(shipsPerTimeUnit);
    cy.get('[id="timeUnit"]').select(timeUnit);
  }

  static maritimeDescriptionCategory(shipDescription: string, shipCategory: string) {
    cy.get('[id="mooringMaritimeShippingStandardDescription"]').clear().type(shipDescription);
    cy.get('[id="mooringMaritimeStandardShippingType"]').select(shipCategory);
  }

  static visitsTimeUnit(shipsPerTimeUnit: string, timeUnit: string) {
    cy.get('[id="mooringMaritimeShippingStandardAmountOfVisits"]').clear().type(shipsPerTimeUnit);
    cy.get('[id="timeUnit"]').select(timeUnit);
  }

  static averageResidenceTimeShorePower(averageResidenceTime: string, shorePowerFactor: string) {
    cy.get('[id="averageResidenceTimeInput"]').clear().type(averageResidenceTime);
    cy.get('[id="mooringMaritimeShippingStandardShorePowerFactor"]').clear().type(shorePowerFactor);
  }

  static maritimeDescriptionCategoryShipPerTimeUnitTimeUnitAverageResidenceTimeShorePowerFactorCreated(
    shippingType: string,
    rowNumber: string,
    description: string,
    shipCategory: string,
    shipPerTimeUnit: string,
    timeUnit: string,
    averageResidenceTime: string,
    shorePowerFactor: string,
    emissionNOx: string,
    emissionNH3: string
  ) {
    ShippingSourcePage.maritimeDescriptionCategoryShipPerTimeUnitTimeUnitCreated(
      shippingType,
      rowNumber,
      description,
      shipCategory,
      shipPerTimeUnit,
      timeUnit,
      emissionNOx,
      emissionNH3
    );
    cy.get("[id=maritime" + shippingType + "ShippingAverageResidenceTime-" + rowNumber + "]").should("have.text", averageResidenceTime);
    cy.get("[id=maritime" + shippingType + "ShippingShorePowerFactor-" + rowNumber + "]").should("have.text", shorePowerFactor);
  }

  static maritimeDescriptionCategoryShipPerTimeUnitTimeUnitCreated(
    shippingType: string, 
    rowNumber: string, 
    description: string, 
    shipCategory: string, 
    shipPerTimeUnit: string, 
    timeUnit: string, 
    emissionNOx: string, 
    emissionNH3: string
   ) {
    cy.get("[id=maritime" + shippingType + "ShippingCode-" + rowNumber + "]").should('have.text', shipCategory);
    cy.get("[id=maritime" + shippingType + "ShippingDescription-" + rowNumber + "]").should("have.text", description);
    cy.get("[id=maritime" + shippingType + "ShippingMovements-" + rowNumber + "]").should("have.text", shipPerTimeUnit);
    cy.get("[id=maritime" + shippingType + "ShippingTimeUnit-" + rowNumber + "]").should("have.text", timeUnit);

    cy.get("[id=maritime" + shippingType + "ShippingEmission-" + rowNumber + "-NOX]").should("have.text", emissionNOx);
    cy.get("[id=maritime" + shippingType + "ShippingEmission-" + rowNumber + "-NH3]").should("have.text", emissionNH3);
  }

  static maritimeDescriptionCategoryShipPerTimeUnitTimeUnitInputFieldsHaveValues(
    description: string,
    shipCategory: string,
    shipPerTimeUnit: string,
    timeUnit: string,
    averageResidenceTime: string,
    shorePowerPercentage: string
  ) {
    cy.get("[id=mooringMaritimeShippingStandardDescription]").should("have.value", description);
    cy.get("[id=mooringMaritimeStandardShippingType]").should("have.value", shipCategory);
    cy.get("[id=mooringMaritimeShippingStandardAmountOfVisits]").should("have.value", shipPerTimeUnit);
    cy.get("[id=timeUnit]").should("have.value", timeUnit);
    cy.get("[id=averageResidenceTimeInput]").should("have.value", averageResidenceTime);
    cy.get("[id=mooringMaritimeShippingStandardShorePowerFactor]").should("have.value", shorePowerPercentage);
  }

  static inlandDescriptionCategoryShipPerTimeUnitTimeUnitSorePowerPercentageLadenPercentageInputFieldsHaveValues(
    description: string,
    shipCategory: string,
    shipPerTimeUnit: string,
    timeUnit: string,
    averageResidenceTime: string,
    shorePowerPercentage: string,
    ladenPercentage: string
  ) {
    cy.get("[id=emissionSourceInlandStandardDescription]").should("have.value", description);
    cy.get("[id=shipCategory]").should("have.value", shipCategory);
    cy.get("[id=inlandVisitsInput]").should("have.value", shipPerTimeUnit);
    cy.get("[id=timeUnit]").should("have.value", timeUnit);
    cy.get("[id=averageResidenceTimeInput]").should("have.value", averageResidenceTime);
    cy.get("[id=shippingShorePowerFactor]").should("have.value", shorePowerPercentage);
    cy.get("[id=shippingPercentageLaden]").should("have.value", ladenPercentage);
  }

  static editInlandDescriptionCategoryShipPerTimeUnitTimeUnitAverageResidenceTimeShorePowerFactorPercentageLaden(
    shipDescription: string,
    shipCategory: string,
    shipsPerTimeUnit: string,
    timeUnit: string,
    averageResidenceTime: string,
    shorePowerFactor: string,
    percentageLaden: string
  ) {
    cy.get('[id="collapsibleSubSources"] > .line > .title').click({
      force: true
    });
    cy.get('[id="sourceListButtonNewSource"] > .icon').click({ force: true });

    ShippingSourcePage.inlandDescriptionCategory(shipDescription, shipCategory);
    cy.get('[id="inlandVisitsInput"]').type("{selectAll}").type(shipsPerTimeUnit);
    cy.get('[id="timeUnit"]').select(timeUnit);
    cy.get('[id="averageResidenceTimeInput"]').clear().type(averageResidenceTime);
    cy.get('[id="shippingShorePowerFactor"]').clear().type(shorePowerFactor);
    cy.get('[id="shippingPercentageLaden"]').clear().type(percentageLaden);
  }

  static editInlandDescriptionCategory(shipDescription: string, shipCategory: string) {
    cy.get('[id="collapsibleSubSources"] > .line > .title').click({
      force: true
    });
    cy.get('[id="sourceListButtonNewSource"] > .icon').click({ force: true });

    ShippingSourcePage.inlandDescriptionCategory(shipDescription, shipCategory);
  }

  static inlandDescriptionCategory(shipDescription: string, shipCategory: string) {
    cy.get('[id="emissionSourceInlandStandardDescription"]').clear().type(shipDescription);
    cy.get('[id="shipCategory"]').select(shipCategory);
  }

  static editMovementTypeShipMovementsTimeUnitPercentageLaden(movementType: string, shipMovements: string, timeUnit: string, percentageLaden: string) {
    if (movementType == "A to B") {
      cy.get('[id="shipMovements_A_TO_B"]').type("{selectAll}").type(shipMovements);
      cy.get('[id="timeUnit"]').first().select(timeUnit);
      cy.get('[id="shipPercentageLaden_A_TO_B"]').type("{selectAll}").type(percentageLaden);
    } else if (movementType == "B to A") {
      cy.get('[id="shipMovements_B_TO_A"]').type("{selectAll}").type(shipMovements);
      cy.get('[id="timeUnit"]').last().select(timeUnit, 1);
      cy.get('[id="shipPercentageLaden_B_TO_A"]').clear().type("{selectAll}").type(percentageLaden);
    } else {
      cy.contains("movementType does not exist").should("not.exist");
    }
  }

  static editWaterwayDirection(waterway: string, direction: string) {
    cy.get('[id="collapsibleSubSources"] > .line > .title').click({
      force: true
    });
    cy.get('[id="waterwayCategory"]').select(waterway);
    if (direction == "Stroomopwaarts") {
      cy.get('[id="emissionSourceShipDirectionToggleDirectionUpstream"]').click();
    } else if (direction == "Stroomafwaarts") {
      cy.get('[id="emissionSourceShipDirectionToggleDirectionDownstream"]').click();
    }
    cy.get('[id="collapsibleSubSources"] > .line > .title').click({
      force: true
    });
  }

  static editWaterway(waterway: string) {
    cy.get('[id="waterwayCategory"]').select(waterway);
  }

  static assertWaterwayDirectionIsSelected(waterwayDirection: string) {
    switch (waterwayDirection) {
      case "Stroomopwaarts":
        cy.get('[id="emissionSourceShipDirectionToggleDirectionUpstream"]').should("have.class", "active");
        cy.get('[id="emissionSourceShipDirectionToggleDirectionDownstream"]').should("not.have.class", "active");
        break;
      case "Stroomafwaarts":
        cy.get('[id="emissionSourceShipDirectionToggleDirectionUpstream"]').should("not.have.class", "active");
        cy.get('[id="emissionSourceShipDirectionToggleDirectionDownstream"]').should("have.class", "active");
        break;
      default:
        throw new Error("Unknown waterway direction: " + waterwayDirection);
    }
  }

  static openShippingSubsourcesCollapsible() {
    cy.get('[id="collapsibleSubSources"] > .line > .title').click({
      force: true
    });
  }

  static assertWaterwayDirectionOptionIsVisible(shouldBeVisible: string) {
    if (shouldBeVisible) {
      cy.get('[id="emissionSourceShipDirectionToggleDirectionUpstream"]').should("exist").should("be.visible");
      cy.get('[id="emissionSourceShipDirectionToggleDirectionDownstream"]').should("exist").should("be.visible");
    } else {
      cy.get('[id="emissionSourceShipDirectionToggleDirectionUpstream"]').should("not.exist");
      cy.get('[id="emissionSourceShipDirectionToggleDirectionDownstream"]').should("not.exist");
    }
  }

  static inlandDescriptionCategoryShipPerTimeUnitTimeUnitAverageResidenceTimeShorePowerFactorPercentageLadenCreated(
    shippingType: string,
    rowNumber: string,
    description: string,
    shipCategory: string,
    shipPerTimeUnit: string,
    timeUnit: string,
    averageResidenceTime: string,
    shorePowerFactor: string,
    percentageLaden: string,
    emissionNOx: string,
    emissionNH3: string
  ) {
    cy.get("[id=inland" + shippingType + "ShippingCode-" + rowNumber + "]").should('have.text', shipCategory);
    cy.get("[id=inland" + shippingType + "ShippingDescription-" + rowNumber + "]").should("have.text", description);
    cy.get("[id=inland" + shippingType + "ShippingMovements-" + rowNumber + "]").should("have.text", shipPerTimeUnit);
    cy.get("[id=inland" + shippingType + "ShippingTimeUnit-" + rowNumber + "]").should("have.text", timeUnit);
    cy.get("[id=inland" + shippingType + "ShippingAverageResidenceTime-" + rowNumber + "]").should("have.text", averageResidenceTime);
    cy.get("[id=inland" + shippingType + "ShippingShorePowerFactor-" + rowNumber + "]").should("have.text", shorePowerFactor);
    cy.get("[id=inland" + shippingType + "ShippingPercentageLaden-" + rowNumber + "]").should("have.text", percentageLaden);

    cy.get("[id=inland" + shippingType + "ShippingEmission-" + rowNumber + "-NOX]").should("have.text", emissionNOx);
    cy.get("[id=inland" + shippingType + "ShippingEmission-" + rowNumber + "-NH3]").should("have.text", emissionNH3);
  }

  static inlandDescriptionCategoryCreated(shippingType: string, rowNumber: string, shipCategory: string) {
    cy.get("[id=inland" + shippingType + "ShippingCode-" + rowNumber + "]").should('have.text', shipCategory);
  }

  static movementTypeDescriptionShipMovementsTimeUnitPercentageLadenCreated(
    rowNumber: string,
    movementType: string,
    description: string,
    shipMovements: string,
    timeUnit: string,
    percentageLaden: string
  ) {
    cy.get("[id=inlandStandardShippingDescription-" + rowNumber + "]").should("have.text", description);

    if (movementType == "A to B") {
      cy.get("[id=inlandStandardShippingMovementsAtoB-" + rowNumber + "]").should("have.text", shipMovements);
      cy.get("[id=inlandStandardShippingTimeUnitAtoB-" + rowNumber + "]").should("have.text", timeUnit);
      cy.get("[id=inlandStandardShippingPercentageLadenAtoB-" + rowNumber + "]").should("have.text", percentageLaden);
    } else if ("B to A") {
      cy.get("[id=inlandStandardShippingMovementsBtoA-" + rowNumber + "]").should("have.text", shipMovements);
      cy.get("[id=inlandStandardShippingTimeUnitBtoA-" + rowNumber + "]").should("have.text", timeUnit);
      cy.get("[id=inlandStandardShippingPercentageLadenBtoA-" + rowNumber + "]").should("have.text", percentageLaden);
    }
  }

  static waterwayDirectionCreated(waterway: string, direction: string) {
    cy.get('[id="inlandShippingWaterwayCode"]').should("have.text", waterway);
    cy.get('[id="inlandShippingWaterwayDirectionType"]').should("have.text", direction);
  }

  static fillDockA(dockA: string) {
    cy.get('[id="emissionSourceMooringACategory"]').select(dockA, { force: true });
  }

  static fillDockB(dockB: string) {
    cy.get('[id="emissionSourceMooringBCategory"]').select(dockB, { force: true });
  }

  static dockACorrectlySaved(dockA: string) {
    cy.get('[id="maritimeShippingMooringAId"]').should("have.text", dockA);
  }

  static dockBCorrectlySaved(dockB: string) {
    cy.get('[id="maritimeShippingMooringBId"]').should("have.text", dockB);
  }

  static fieldNotVisible(notVisibleField: string) {
    switch (notVisibleField) {
      case "dock A":
        cy.get('[id="maritimeShippingMooringAId"]').should("not.exist");
        break;
      case "dock B":
        cy.get('[id="maritimeShippingMooringBId"]').should("not.exist");
        break;
      default:
        throw new Error("Unknown dock type: " + notVisibleField);
    }
  }

}
