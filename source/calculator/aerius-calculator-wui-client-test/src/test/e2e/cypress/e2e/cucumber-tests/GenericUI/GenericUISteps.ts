/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";
import { CommonGenericUIPage } from "../../common/CommonGenericUIPage";
import { CommonAssessmentPointsPage } from "../../common/CommonAssessmentPointsPage";
import { GenericUIPage } from "./GenericUIPage";

Given("I save the current session id", () => {
  GenericUIPage.saveSessionId();
});

// Language
Given("I switch language to {string}", (language: string) => {
  GenericUIPage.switchLanguage(language);
});

Given("I switch language to {string}, not expecting a reload", (language: string) => {
  GenericUIPage.switchLanguage(language, false);
});

Given("the current language should be {string}", (language: string) => {
  GenericUIPage.currentLanguage(language);
});

Given("the language screen should be closed", () => {
  GenericUIPage.assertLanguageScreenIsClosed();
});

// Other
Given("I expect the privacy warning to exist", () => {
  CommonGenericUIPage.assertPrivacyWarningExists("NL");
});
