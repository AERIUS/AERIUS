/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";

import { CommonMaplayersPage } from "./CommonMaplayersPage";

Given("the {string} maplayer should be visible", (maplayer) => {
  CommonMaplayersPage.assertMaplayerVisible(maplayer);
});

Given("the {string} maplayer should not exist", (maplayer) => {
  CommonMaplayersPage.assertMaplayerDoesNotExist(maplayer);
});

Given("I select the mapbutton {string}", (mapbutton) => {
  CommonMaplayersPage.selectMapbutton(mapbutton);
});

Given("I open maplayer {string}", (maplayer) => {
  CommonMaplayersPage.openMaplayer(maplayer);
});

Given("I expect the map layer {string} to have an empty legenda", (mapLayer) => {
  CommonMaplayersPage.validateMapLayerNoLegenda(mapLayer);
});

Given("I expect the map layer {string} to have the following values", (mapLayer, dataTable) => {
  CommonMaplayersPage.validateMapLayer(mapLayer, dataTable);
});

Given("I expect the map layer {string} to have title {string} with the following values", (mapLayer, title, dataTable) => {
  CommonMaplayersPage.validateMapLayer(mapLayer, dataTable, { title: title });
});

Given("I expect the map layer {string} for data type {string} to have title {string} and the following values", (mapLayer, dataType, title, dataTable) => {
  CommonMaplayersPage.validateMapLayer(mapLayer, dataTable, { title: title, dataType: dataType });
});

Given("I expect the map layer {string} for data type {string} to have the following values", (mapLayer, dataType, dataTable) => {
  CommonMaplayersPage.validateMapLayer(mapLayer, dataTable, { dataType: dataType });
});

Given("I expect the map layer {string} for data type {string} and {string} to have the following values", (mapLayer, dataType, subDataType, dataTable) => {
  CommonMaplayersPage.validateMapLayer(mapLayer, dataTable, { dataType: dataType, subDataType: subDataType });
});

Given("I expect the map layer {string} for data type {string} and {string} to have title {string} and the following values", (mapLayer, dataType, subDataType, title, dataTable) => {
  CommonMaplayersPage.validateMapLayer(mapLayer, dataTable, { dataType: dataType, subDataType: subDataType, title: title });
});

Given("I validate the map layers", (dataTable) => {
  CommonMaplayersPage.validateMapLayers(dataTable);
});

Given("I validate the habitats type layers", (dataTable) => {
  CommonMaplayersPage.validateHabitatsTypeLayers(dataTable);
});

Given("I validate the habitats areas sensitivity layers", (dataTable) => {
  CommonMaplayersPage.validateHabitatsAreasSensitivityLayers(dataTable);
});

Given("I validate the base layers", (dataTable) => {
  CommonMaplayersPage.validateBaseLayers(dataTable);
});

Given("I validate the critical level subselections contains filter", (dataTable) => {
  CommonMaplayersPage.validateCriticalLevelSubMaps(dataTable);
});

Given("the following maplayers should be visible", (dataTable) => {
  CommonMaplayersPage.assertMaplayersVisible(dataTable);
});
