/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";

import { CommonSystemInfoUpdatePage } from "./CommonSystemInfoUpdatePage";

Given("I open aerius system info update", () => {
  CommonSystemInfoUpdatePage.visitSystemInfoUpdate();
  CommonSystemInfoUpdatePage.openSystemInfoUpdate();
});

Given("I validate no system info message is shown", () => {
  CommonSystemInfoUpdatePage.validateNoInfoMessage();
});

Given("I enter system info message {string}", (message) => {
  CommonSystemInfoUpdatePage.addSystemInfoMessage(message);
});

Given("I validate system info message {string}", (message) => {
  CommonSystemInfoUpdatePage.validateSystemInfoMessage(message);
});

Given("I delete info message", () => {
  CommonSystemInfoUpdatePage.removeSystemInfoMessage();
});
