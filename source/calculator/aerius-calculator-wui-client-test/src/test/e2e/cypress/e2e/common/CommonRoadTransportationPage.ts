/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { CommonPage } from "./CommonPage";

export class CommonRoadTransportationPage {
  static assertRoadSubsourceTitleIs(type: string, subsourceIndex: string, title: string) {
    switch (type) {
      case "standard":
        cy.get('[id="detailRoadStandardVehicleTitle-' + subsourceIndex + '"]').should("have.text", title);
        break;
      case "custom":
        cy.get('[id="detailRoadCustomVehicleTitle-' + subsourceIndex + '"]').should("have.text", title);
        break;
      case "specific":
        cy.get('[id="detailRoadSpecificVehicleTitle-' + subsourceIndex + '"]').should("have.text", title);
        break;
      default:
        throw new Error("unknown road subsource type: " + type);
    }
  }

  static selectTunnelFactor(tunnelFactor: number) {
    cy.get('[id="InputSRM2RoadTunnelFactorCheckbox"]').click();
    cy.get('[id="tunnelFactor"]').clear().type(tunnelFactor.toString());
  }

  static chooseRoadTransportationElevationType(roadElevationType: string) {
    cy.get('[id="roadElevationType"]').select(roadElevationType);
  }

  static validateElevationHeightLimits(minElevationHeight: number, maxElevationHeight: number) {
    const tooLowElevationHeight1 = minElevationHeight - 1;
    const tooHighElevationHeight1 = maxElevationHeight + 1;

    const decimal = maxElevationHeight - (maxElevationHeight - minElevationHeight) / 2 + 0.1;

    // No value
    cy.get('[id="elevationHeight_warning"]').should("not.exist");

    // Too low
    cy.get('[id="elevationHeight"]').clear().type(tooLowElevationHeight1.toString());
    cy.get('[id="elevationHeight_warning"]').should("exist");
    
    // Lowest
    cy.get('[id="elevationHeight"]').clear().type(minElevationHeight.toString());
    cy.get('[id="elevationHeight_warning"]').should("not.exist");

    // Clear
    cy.get('[id="elevationHeight"]').clear();
    cy.get('[id="elevationHeight_warning"]').should("not.exist");

    // Too high
    cy.get('[id="elevationHeight"]').clear().type(tooHighElevationHeight1.toString());
    cy.get('[id="elevationHeight_warning"]').should("exist");

    // Highest
    cy.get('[id="elevationHeight"]').clear().type(minElevationHeight.toString());
    cy.get('[id="elevationHeight_warning"]').should("not.exist");

    // Validate decimal values are not allowed
    cy.get('[id="elevationHeight"]').clear().type(decimal.toString());
    cy.get('[id="elevationHeight_warning"]').should("not.exist");
    cy.get('[id="elevationHeight_error"]').should("exist");
  }
  
  static chooseRoadTransportationElevation(roadElevationType: string, elevationHeight: string) {
    cy.get('[id="roadElevationType"]').select(roadElevationType);
    cy.get('[id="elevationHeight"]').clear().type(elevationHeight);
  }

  static validateElevationHeightCantBeSet() {
    cy.get('[id="elevationHeight"]').should("be.disabled");
  }

  static chooseRoadTransportationBarrierFromAToB(barrierType: string, barrierHeight: string, barrierDistance: string) {
    cy.get('[id="roadSideBarrierLeftType"]').select(barrierType);
    cy.get('[id="barrierLeftHeight"]').clear().type(barrierHeight);
    cy.get('[id="barrierLeftDistance"]').clear().type(barrierDistance);
  }

  static chooseRoadTransportationBarrierFromBToA(barrierType: string, barrierHeight: string, barrierDistance: string) {
    cy.get('[id="roadSideBarrierRightType"]').select(barrierType);
    cy.get('[id="barrierRightHeight"]').clear().type(barrierHeight);
    cy.get('[id="barrierRightDistance"]').clear().type(barrierDistance);
  }
  
  static openEmissionSourcePanel() {
    cy.get('[id="collapsibleSubSources"]').click();
  }

  static addEmissionSourceOfType(emissionSourceType: string) {
    cy.get('[id="sourceListButtonNewSource"]').click();
    switch (emissionSourceType) {
      case "Voorgeschreven factoren":
        cy.get('[id="tab-vehicleType-STANDARD"]').click({ force: true });
        break;
      case "Eigen specificatie":
        cy.get('[id="tab-vehicleType-CUSTOM"]').click({ force: true });
        break;
        case "Koude start: Voorgeschreven factoren":
          cy.get('[id="tab-vehicleType-STANDARD_CS"]').click({ force: true });
        break;
        case "Koude start: Eigen specificatie":
          cy.get('[id="tab-vehicleType-CUSTOM"]').click({ force: true });
        break;
      default:
        throw new Error("Unknown emission source type: " + emissionSourceType);
    }
  }

  static changeSourceCharacteristic(contentType: string, ventilationType: string) {
    switch (contentType) {
      case "heatContentType":
        cy.get('[id="heatContentType"]').select(ventilationType);
        break;
      default:
        throw new Error("Unknown emission source type: " + contentType);
    }
  }

  static fillEmissionAndFraction(emission: string, fraction: string) {
    switch (emission) {
      case "NO2":
        cy.get('[id="emissionFactorInput_NO2"]').clear().type(fraction);
        break;
      case "NOX":
        cy.get('[id="emissionFactorInput_NOX"]').clear().type(fraction);
        break;
      case "NH3":
        cy.get('[id="emissionFactorInput_NH3"]').clear().type(fraction);
        break;
      default:
        throw new Error("Unknown emission: " + emission);
    }
  }

  static sourceCharacteristicsCorrectlySaved(roadType: string, tunnelFactor: string, elevationType: string, elevationHeight: string) {
    cy.get('[id="srm2DetailRoadType"]').should("have.text", roadType);
    cy.get('[id="srm2DetailTunnelFactor"]').should("have.text", tunnelFactor);
    cy.get('[id="srm2DetailElevation"]').should("have.text", elevationType);
    CommonPage.assertMinMaxLabel("srm2DetailElevationHeightCorrected", elevationHeight);
  }

  static barrierAToBCorrectlySaved(barrierType: string, barrierHeight: string, barrierDistance: string) {
    cy.get('[id="srm2DetailBarrierTypeLeft"]').should("have.text", barrierType);
    cy.get('[id="srm2DetailBarrierHeightLeft"] > div:first').should("have.text", barrierHeight);
    cy.get('[id="srm2DetailBarrierDistanceLeft"] > div:first').should("have.text", barrierDistance);
  }

  static barrierBToACorrectlySaved(barrierType: string, barrierHeight: string, barrierDistance: string) {
    cy.get('[id="srm2DetailBarrierTypeRight"]').should("have.text", barrierType);
    cy.get('[id="srm2DetailBarrierHeightRight"] > div:first').should("have.text", barrierHeight);
    cy.get('[id="srm2DetailBarrierDistanceRight"] > div:first').should("have.text", barrierDistance);
  }

  static barrierAToBHeightWarningCorrectlySaved(barrierType: string, barrierHeight: string, barrierHeightWarning: string, barrierDistance: string) {
    this.barrierAToBCorrectlySaved(barrierType, barrierHeight, barrierDistance);
    cy.get('[id="srm2DetailBarrierHeightLeft"] > div:last > span').should("have.text", barrierHeightWarning);
  }

  static barrierAToBHeightDistanceWarningCorrectlySaved(
    barrierType: string, 
    barrierHeight: string, 
    barrierHeightWarning: string, 
    barrierDistance: string, 
    barrierDistanceWarning: string) {
    this.barrierAToBHeightWarningCorrectlySaved(barrierType, barrierHeight, barrierHeightWarning, barrierDistance);
    cy.get('[id="srm2DetailBarrierDistanceLeft"] > div:last > span').should("have.text", barrierDistanceWarning);
  }

  static barrierBToAHeightWarningCorrectlySaved(barrierType: string, barrierHeight: string, barrierHeightWarning: string, barrierDistance: string) {
    this.barrierBToACorrectlySaved(barrierType, barrierHeight, barrierDistance);
    cy.get('[id="srm2DetailBarrierHeightRight"] > div:last > span').should("have.text", barrierHeightWarning);
  }

  static barrierBToAHeightDistanceWarningCorrectlySaved(
    barrierType: string, 
    barrierHeight: string, 
    barrierHeightWarning: string, 
    barrierDistance: string, 
    barrierDistanceWarning: string) {
    this.barrierBToAHeightWarningCorrectlySaved(barrierType, barrierHeight, barrierHeightWarning, barrierDistance);
    cy.get('[id="srm2DetailBarrierDistanceRight"] > div:last > span').should("have.text", barrierDistanceWarning);
  }

  static selectTimeUnitForColdStarts(timeUnit: string) {
    cy.get('[id="timeUnit"]').select(timeUnit, { force: true });
  }

  static fillInNumberOfVehicles(dataTable: any) {
    dataTable.hashes().forEach((dataRow: any) => {
      switch (dataRow.typeOfVehicle) {
        case "Lichte voertuigen":
          cy.get('[id="numberOfVehiclesRow_LIGHT_TRAFFIC"]').clear({ force: true }).type(dataRow.numberOfVehicles);
          break;
        case "Middelzware vrachtvoertuigen":
          cy.get('[id="numberOfVehiclesRow_NORMAL_FREIGHT"]').clear({ force: true }).type(dataRow.numberOfVehicles);
          break;
        case "Zware vrachtvoertuigen":
          cy.get('[id="numberOfVehiclesRow_HEAVY_FREIGHT"]').clear({ force: true }).type(dataRow.numberOfVehicles);
          break;
        case "Bussen":
          cy.get('[id="numberOfVehiclesRow_AUTO_BUS"]').clear({ force: true }).type(dataRow.numberOfVehicles);
          break;
        default:
          throw new Error("Unknown type of vehicle: " + dataRow.typeOfVehicle);        
      }
    })
  }

  static assertNumberOfVehicles(dataTable: any) {
    dataTable.hashes().forEach((dataRow: any) => {
      switch (dataRow.typeOfVehicle) {
        case "Lichte voertuigen":
          cy.get('[id="detailColdStartStandardVehicleTypeDescription-0-0"]').should("have.text", dataRow.numberOfVehicles);
          break;
        case "Middelzware vrachtvoertuigen":
          cy.get('[id="detailColdStartStandardVehicleTypeDescription-0-1"]').should("have.text", dataRow.numberOfVehicles);
          break;
        case "Zware vrachtvoertuigen":
          cy.get('[id="detailColdStartStandardVehicleTypeDescription-0-2"]').should("have.text", dataRow.numberOfVehicles);
          break;
        case "Bussen":
          cy.get('[id="detailColdStartStandardVehicleTypeDescription-0-3"]').should("have.text", dataRow.numberOfVehicles);
          break;
        default:
          throw new Error("Unknown type of vehicle: " + dataRow.typeOfVehicle);                
      }
    })
  }

  static assertTimeUnit(timeUnit: string) {
    cy.get('[id="detailColdStartStandardVehicleTraffic-0"]').should("have.text", timeUnit);
  }

  static assertSourceCharacteristics(dataTable: any) {
    dataTable.hashes().forEach((dataRow: any) => {
      switch (dataRow.sourceCharacteristic) {
        case "Ventilatie":
          cy.get('[id="sourcelistDetailVentilation"]').should("have.text", dataRow.expectedValue);
          break;
        case "Gebouwinvloed":
          cy.get('[id="sourcelistDetailBuilding"]').should("have.text", dataRow.expectedValue);
          break;
        case "Uittreedhoogte":
          cy.get('[id="sourcelistDetailEmissionHeight"]').contains(dataRow.expectedValue);
          break;
        case "Temperatuur Emissie":
          cy.get('[id="sourcelistDetailEmissionTemperature"]').should("have.text", dataRow.expectedValue);
          break;
        case "Uittreeddiameter":
          cy.get('[id="sourcelistDetailOutflowDiameter"]').should("have.text", dataRow.expectedValue);
          break;
        case "Uittreedrichting":
          cy.get('[id="sourcelistDetailOutflowDirection"]').should("have.text", dataRow.expectedValue);
          break;
        case "Uittreedsnelheid":
          cy.get('[id="sourcelistDetailOutflowVelocity"]').should("have.text", dataRow.expectedValue);
          break;
        case "Warmteinhoud":
          cy.get('[id="sourcelistDetailHeatContent"]').contains(dataRow.expectedValue);
          break;
        case "Spreiding":
          cy.get('[id="sourcelistDetailSpread"]').should("have.text", dataRow.expectedValue);
          break;
        case "Temporele variatie":
          cy.get('[id="sourcelistDetailDiurnalVariation"]').should("have.text", dataRow.expectedValue);
          break; 
        default:
          throw new Error("Unknown source characteristic: " + dataRow.sourceCharacteristic);
      }
     });
  }

  static chooseEuroClassFromCustom(euroClass: string) {
    cy.get('[id="euroclass"]').select(euroClass);
    cy.wait(500); // Form is switched wait a moment
  }

  static validateChosenCustomEuroClass(euroClass: string) {
    cy.get('[id="euroclass"] > option:selected').should("contain.text", euroClass);
  }

  static chooseEuroClassFromSpecific(euroClass: string) {
    cy.get('[id="onRoadMobileSourceCategories"]').select(euroClass, { force: true });
    cy.wait(500); // Form is switched wait a moment
  }

  static validateChosenSpecificEuroClass(euroClass: string) {
    cy.get('[id="onRoadMobileSourceCategories"] > option:selected').should("contain.text", euroClass);
  }

  static euroClassIsCorrectlySaved(euroClass: string) {
    cy.get('[id="detailRoadSpecificVehicleCode-0"]').should("have.text", euroClass);
  }

  static assertEmissionsUpdatedAfterSelection(emission: string, fraction: string) {
    switch (emission) {
      case "NOX":
        cy.get('[id="emissionFactorInput_NOX"]').should("have.value", fraction);
        break;
      case "NO2":
        cy.get('[id="emissionFactorInput_NO2"]').should("have.value", fraction);
        break;
      case "NH3":
        cy.get('[id="emissionFactorInput_NH3"]').should("have.value", fraction);
        break;
      default:
        throw new Error("Unknown emission: " + emission);
    }
  }
}
