/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";

import { ExportPage } from "./ExportPage";

Given("I select exporting of OPS input files", () => {
  ExportPage.selectExportOPSInputFiles();
});

Given("I select included situation {string}", (situation) => {
  ExportPage.selectSituation(situation, true);
});
  
Given("I deselect included situation {string}", (situation) => {
  ExportPage.selectSituation(situation, false);
});

Given("I cancel the calculation export {string}", (calculationJob) => {
  ExportPage.cancelExport(calculationJob);
});

Given("calculation export has {string} rows", (count) => {
  ExportPage.validateCalculationResultsCount(count);
});
