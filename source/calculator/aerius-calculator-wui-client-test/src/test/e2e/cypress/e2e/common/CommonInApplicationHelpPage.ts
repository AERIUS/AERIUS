/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class CommonInApplicationHelpPage {
  // Press buttons
  static pressButton(buttonName: string) {
    switch (buttonName) {
      case "manual":
        cy.get('[id="NAV_ICON-MENU-MANUAL1"]').click();
        break;
      case "close":
        cy.get(".headerBar > .exit").click();
        break;
      case "Inhoudsopgave":
        cy.get(".headerBar > button").contains("Inhoudsopgave").click();
        this.assertTitleIsShown("Inhoudsopgave") // Await visibility of table of contents before continuing
        break;
      case "Table of contents":
        cy.get(".headerBar > button").contains("Table of contents").click({force: true});
        this.assertTitleIsShown("Table of contents") // Await visibility of table of contents before continuing
        break;
      case "Home":
      case "Input":
      case "Assessment points":
      case "Calculation jobs":
      case "Results":
      case "Export":
      case "Next steps":
      case "Start":
      case "Invoer":
      case "Rekentaken":
      case "Resultaten":
      case "Exporteren":
        cy.get(".contentTarget a").contains(buttonName).click();
        break;
      default:
        throw new Error("Unknown button: " + buttonName);
    }
  }

  static openSettingsPanel() {
    cy.get(".icon-menu-settings").click();
  }

  // Check visibility of elements
  static assertButtonIsVisible(buttonName: string) {
    switch (buttonName) {
      case "close":
        cy.get(".headerBar > .exit").should("be.visible");
        break;
      case "Inhoudsopgave":
        cy.get(".headerBar > button").contains("Inhoudsopgave").should("be.visible");
        break;
      case "Table of contents":
        cy.get(".headerBar > button").contains("Table of contents").should("be.visible");
        break;
      default:
        throw new Error("Unknown button: " + buttonName);
    }
  }

  static assertTitleIsShown(title: string) {
    const expectedId = title.toLowerCase().replaceAll(" ", "-");
    cy.get('h1[id="' + expectedId + '"]').should("be.visible");
  }

  static assertManualIsShown() {
    cy.get(".aer-manual").should("exist");
  }

  static assertManualIsNotShown() {
    cy.get(".aer-manual").should("not.exist");
  }

  // Other
  static assertManualHasScrollbar() {
    cy.get(".aer-manual").should("have.css", "overflow", "auto");
  }

  static assertManualHasSlider() {
    cy.get(".aer-manual").find(".width-slider").should("be.visible");
  }

}
