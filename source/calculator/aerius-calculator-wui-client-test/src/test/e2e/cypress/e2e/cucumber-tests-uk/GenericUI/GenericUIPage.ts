/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class GenericUIPage {
  static selectSourceNameInputField() {
    cy.get('[id="label"]').click();
  }

  static selectBuildingNameInputField() {
    cy.get('[id="buildingNameInput"]').click();
  }

  static validateDemoModeMessage() {
    const url: any = Cypress.config("baseUrl");
    if (url.toUpperCase().includes("DEMO")) {
      cy.get(".notification > p").should("have.text", "The application is in demo mode. Spatially varying surface roughness, complex terrain, and plume depletion are all disabled. Calculation results will not meet the requirements for applying for permits.");
    } else {
      cy.get(".notification > p").should('not.exist');
    }
  }

  static toggleCookiePreferenceWindow() {
    cy.get('[id="ccc-icon"]').click();
  }

  static validateCookiePreferenceWindow(status: string) {
    if (status == "visible") {
      cy.get('[id="ccc-content"]').should("exist");
    } else {
      cy.get('[id="ccc-content"]').should("not.exist");
    }
  }

  static validateCookiePreferenceTitles(objectName: string, value: string) {
    switch (objectName) {
      case "title":
        this.idShouldHaveText("ccc-title", value);
        break;
      case "necessary-title":
        this.idShouldHaveText("ccc-necessary-title", value);
        break;
      case "optional-cookie-header":
        cy.get(".optional-cookie-header").should("have.text", value);
        break;
      default:
        throw new Error("Unknown cookie preference name: " + objectName);
    }
  }

  static idShouldHaveText(id: string, value: string) {
    cy.get('[id="'+ id + '"]').should("have.text", value);
  }

  static pressCookieNotice() {
    cy.get('[title="cookie notice"]').click();
  }

  static validateCookieNoticeTitles(objectName: string, value: string) {
    this.idShouldHaveText(objectName, value);
  }

  static closeCookieNotice() {
    cy.get('[id="panel-close"]').last().click();
  }

  static closeCookiePreferences() {
    cy.get('[id="ccc-close"]').click();
  }

  static validateCookiePreferencesStatus(status: string) {
    if (status == "on") {
      cy.get(".checkbox-toggle-input").should("be.checked");
    } else {
      cy.get(".checkbox-toggle-input").should("not.be.checked");
    }
  }
  static changeCookiePreferencesStatus(status: string) {
    if (status == "on") {
      cy.get(".checkbox-toggle-input").check({force: true});
    } else {
      cy.get(".checkbox-toggle-input").uncheck({force: true});
    }
  }

  // Language
  static validateLanguageMenuDoesNotExist() {
    cy.get('[id="NAV_ICON-MENU-LANGUAGE"]').should("not.exist");
  }

  // Preferences
  static assertSelectedWKTConversionMethod(conversionMethod: string) {
    cy.get('[id="projectionSelect"]').find("option:selected").should("have.text", conversionMethod);
  }

  static setWKTConversionMethod(conversionMethod: string) {
    cy.get('[id="projectionSelect"]').select(conversionMethod);
  }
}
