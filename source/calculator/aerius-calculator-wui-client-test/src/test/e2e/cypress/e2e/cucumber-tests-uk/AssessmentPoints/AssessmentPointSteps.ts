/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";

import { AssessmentPointsPage } from "./AssessmentPointsPage";
import { AssessmentPointsHabitatsPage } from "./AssessmentPointsHabitatsPage";

Given("the convert notice should be visible", () => {
  AssessmentPointsPage.assertConvertNoticeShown();
});

Given("I select the converted WKT", () => {
  AssessmentPointsPage.selectConvertedWkt();
});

Given("I should still be in the new assessment point screen", () => {
  AssessmentPointsPage.assertNewPointScreenShown();
});

Given("I select assessment point category {string}", (category) => {
  AssessmentPointsPage.selectAssessmentPointCategory(category);
});

Given("I enter assessment point road height {string}", (roadHeight) => {
  AssessmentPointsPage.enterAssessmentPointRoadHeight(roadHeight);
});

Given("I enter assessment point custom primary fraction {string}", (primaryFraction) => {
  AssessmentPointsPage.enterAssessmentPointPrimaryFraction(primaryFraction);
});

// Habitats
Given("I assert that the habitats list does exist", () => {
  AssessmentPointsHabitatsPage.assertHabitatsWhetherListExists(true);
});

Given("I assert that the habitats list does not exist", () => {
  AssessmentPointsHabitatsPage.assertHabitatsWhetherListExists(false);
});

Given("the empty habitats text and new habitat button should be visible", () => {
  AssessmentPointsHabitatsPage.assertEmptyHabitatsTextAndButtonShown();
});

Given("I add the first habitat", () => {
  AssessmentPointsHabitatsPage.addFirstHabitat();
});

Given("I add a habitat", () => {
  AssessmentPointsHabitatsPage.addHabitat();
});

Given("I duplicate the selected habitat", () => {
  AssessmentPointsHabitatsPage.duplicateHabitat();
});

Given("I delete the selected habitat", () => {
  AssessmentPointsHabitatsPage.deleteHabitat();
});

Given("the habitat list exists and has {int} habitats", (nrOfHabitats: number) => {
  AssessmentPointsHabitatsPage.assertHabitatsListExistsAndHasHabitats(nrOfHabitats);
});

Given("the selected habitat has the following data", (dataTable: any) => {
  AssessmentPointsHabitatsPage.assertHabitatData(dataTable.hashes()[0]);
});

Given("I rename the habitat to {string}", (newName: string) => {
  AssessmentPointsHabitatsPage.renameHabitat(newName);
});

Given("I disable the CLNOx value", () => {
  AssessmentPointsHabitatsPage.toggleCLNOx();
});

Given("I enable the CLNOx value", () => {
  AssessmentPointsHabitatsPage.toggleCLNOx();
});

Given("I disable the CLNH3 value", () => {
  AssessmentPointsHabitatsPage.toggleCLNH3();
});

Given("I enable the CLNH3 value", () => {
  AssessmentPointsHabitatsPage.toggleCLNH3();
});

Given("I disable the CL value", () => {
  AssessmentPointsHabitatsPage.toggleCL();
});

Given("I enable the CL value", () => {
  AssessmentPointsHabitatsPage.toggleCL();
});

Given("the following {string} error is visible {string}", (input: string, errorText: string) => {
  AssessmentPointsHabitatsPage.assertErrorVisible(input, errorText);
});

Given("the {string} error should not exist", (input: string) => {
  AssessmentPointsHabitatsPage.assertErrorDoesNotExist(input);
});

Given("the assigment point error container should exist", () => {
  AssessmentPointsHabitatsPage.assertAssignmentPointErrorContainerExists(true);
});

Given("the assigment point error container should not exist", () => {
  AssessmentPointsHabitatsPage.assertAssignmentPointErrorContainerExists(false);
});

Given("I change the CLNOx value to {string}", (newValue: string) => {
  AssessmentPointsHabitatsPage.changeCLNOxValue(newValue);
});

Given("I change the CLNH3 value to {string}", (newValue: string) => {
  AssessmentPointsHabitatsPage.changeCLNH3Value(newValue);
});

Given("I change the CL value to {string}", (newValue: string) => {
  AssessmentPointsHabitatsPage.changeCLValue(newValue);
});
