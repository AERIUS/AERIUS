#
# Crown copyright
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Generic emission source

    Scenario: Initiate creation of new source and cancel creation
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Other'
        And I save the data
        Then The validation errors for the source should 'be visible'
        When I cancel the new source creation
        And I create a new source from the scenario input page
        Then The validation errors for the source should 'not exist'

    Scenario: Create an emission source with a WKT input converted to the 'EPSG:29903 Irish grid'
        Given I login with correct username and password
        When I navigate to the 'Preferences' tab
        And I switch the WKT conversion to 'EPSG:29903 Irish grid'
        And I close the panel
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Other'
        And I fill location with coordinates 'POINT(0 0)'
        Then the WKT conversion notice pops up
        And I open panel 'Source characteristics'
        And I edit the 'Source diameter' to be '1'
        And I save the data

        When I select the source 'Source 1'
        Then location with coordinates 'X:-218400.71 Y:184024.93' is correctly saved

    Scenario: Default drawing type LINE - Traffic
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Traffic'
        Then the default drawing type is 'Line source'
        When I select drawing type 'Point source'
        Then the following warning is visible: 'This emission source must be of another geometry type.'
        When I select drawing type 'Line source'
        Then the warning is not visible
        When I select drawing type 'Area source'
        Then the following warning is visible: 'This emission source must be of another geometry type.'

    Scenario: Default drawing type POINT - sources other then Traffic
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Other'
        Then the default drawing type is 'Point source'
        And the warning is not visible
        When I select drawing type 'Line source'
        Then the warning is not visible
        When I select drawing type 'Area source'
        Then the warning is not visible

    Scenario: Error message invalid geometry
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Other'
        And I fill location with coordinates 'point(107905.57 517935.55'
        Then the following 'input' error is visible 'Geometry is not valid. An example of the required format is: POINT(384793.69 397848.42). Coordinates have a maximum of 2 decimal places.'
        When I select drawing type 'Line source'
        And I fill location with coordinates 'LINESTRING(141744.69 458722.55'
        Then the following 'input' error is visible 'Geometry is not valid. An example of the required format is: LINESTRING(384650.5 398682.03,384397.82 398380.49). Coordinates have a maximum of 2 decimal places.'
        When I select drawing type 'Area source'
        And I fill location with coordinates 'POLYGON((142444.56 460216.93'
        Then  the following 'input' error is visible 'Geometry is not valid. An example of the required format is: POLYGON((384798.6 397912.75,384756.38 397820.67,384902.75 397760.36,384947.78 397859.68,384798.6 397912.75)). Coordinates have a maximum of 2 decimal places.'
        When I save the data
        Then the savesource error is given 'Fill in the highlighted fields to save your work' 

    Scenario: Check available Sector groups
        Given I login with correct username and password
        And I create a new source from the start page
        Then The specified listbox should or should not contain the following values
            | listboxName  | checkType | listboxValue   |
            | Sector group | contain   | Energy         |
            | Sector group | contain   | Agriculture    |
            | Sector group | contain   | Traffic        |
            | Sector group | contain   | Industry       |
            | Sector group | contain   | Other          |
        And The 'Sector group' listbox contains '5' items

    Scenario: Validate warnings for the height of a VOLUME soure
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Other'
        And I select drawing type 'Area source'
        And I fill location with coordinates 'POLYGON((384798.6 397912.75,384756.38 397820.67,384902.75 397760.36,384947.78 397859.68,384798.6 397912.75))'
        And I open collapsible panel 'Source characteristics'
        And I select the source type 'Volume'

        # Boundary testing for the height error between 0,0005 and 15,000 and half the height
        And I choose source height '0.0009'
        Then the following 'source height' error is visible 'Source height must have a value of at least half the vertical dimension value'
        And I choose source height '0.001'
        Then the following 'source height' error is visible 'Source height must have a value of at least half the vertical dimension value'
        When I choose source height '15001'
        Then the following 'source height' error is visible 'Source height must have a value between 0.0005 and 15,000 (inclusive)'
        And I choose source height '15000'
        Then the following 'source height' warning is visible 'Check the source height. For a volume source that is sitting on the ground, the source height should be equal to half the vertical dimension. '      

        # Boundary testing for values from half the height and above
        When I choose source height '0.49'
        Then the following 'source height' error is visible 'Source height must have a value of at least half the vertical dimension value'  
        When I choose source height '0.5'
        Then the following 'source height' error 'Source height must have a value of at least half the vertical dimension value' is not visible 
        When I choose source height '0.51'
        Then the following 'source height' warning is visible 'Check the source height. For a volume source that is sitting on the ground, the source height should be equal to half the vertical dimension. '
        When I save the data 
        Then the following 'save notification' warning is visible 'Check the fields above and confirm you are satisfied with the values before saving'
        When I save the data 
        And I select the source 'Source 1'
        Then the source 'Source 1' with sector group 'Other' is correctly saved

        # Validate warning for the height when importing a GML file
        Given I login with correct username and password
        Then I select file "UKAPAS_volume_height_warning.gml" to be imported, but I don't upload it yet
        And I expect 2 import 'warnings' to be shown
        And I validate the following warnings
            | warning                                                                                                    | row |
            | The GML file is valid but is not the latest IMAER version, provided version 'V5_1' expected version 'V6_0' | 0   |
            | The height of a volume source 'ES.1' should be half the vertical dimension if it's sitting on the ground.  | 1   |
