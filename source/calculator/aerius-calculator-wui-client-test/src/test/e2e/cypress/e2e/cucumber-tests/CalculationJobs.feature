#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Calculation jobs
    Test everything to do with calculation jobs.

    Scenario: Import a file with no emissions
        Given I login with correct username and password
        When I select file 'emissionSourceNoEmissions.gml' to be imported
        And I navigate to the 'Calculation jobs' tab
        Then the calculation jobs list has 0 calculation jobs
        Then I expect the following data in the scenario overview
            | name       | situationType | year | nettingFactor | emissionSources | emissionNOx | emissionNH3 |
            | Situatie 1 | Beoogd        | 2024 |               | 1               | 0,0 kg/j    | 0,0 kg/j    |

        When I create a new calculation job
        Then I expect the following scenarios to be selected
            | scenarioName |
            | Situatie 1   |
        And I expect the selected 'PROPOSED' situation to be 'Situatie 1'
        And the following error is shown: 'Er zijn situaties in deze rekentaak zonder emissie'

   Scenario: Create new agriculture source - validate situation without emission 
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Dierhuisvesting'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I add a system with housing code 'HA1.1' and number of animals '0'
        When I save the data
        
        And I navigate to the 'Calculation jobs' tab
        Then I expect the following data in the scenario overview
            | name       | situationType | year | nettingFactor | emissionSources | emissionNOx | emissionNH3 |
            | Situatie 1 | Beoogd        | 2025 |               | 1               | 0,0 kg/j    | 0,0 kg/j    |
        And I create a new calculation job
        And I expect the selected 'PROPOSED' situation to be 'Situatie 1'
        And the following error is shown: 'Er zijn situaties in deze rekentaak zonder emissie'

    Scenario: Import a file with multiple sources, of which one has no emissions
        Given I login with correct username and password
        When I select file 'emission2Sources1WithoutEmissions.gml' to be imported
        And I navigate to the 'Calculation jobs' tab
        Then the calculation jobs list has 0 calculation jobs
        Then I expect the following data in the scenario overview
            | name       | situationType | year | nettingFactor | emissionSources | emissionNOx | emissionNH3 |
            | Situatie 1 | Beoogd        | 2024 |               | 2               | 5,0 kg/j    | 0,0 kg/j    |

        When I create a new calculation job
        Then I expect the following scenarios to be selected
            | scenarioName |
            | Situatie 1   |
        And I expect the selected 'PROPOSED' situation to be 'Situatie 1'
        And no error should show

    Scenario: Import multiple proposed files and switch between them
        Given I login with correct username and password
        And I select file 'scenario/scenario_composting_proposed.gml' to be imported, but I don't upload it yet
        And I select file 'scenario/scenario_greenhouse_proposed.gml' to be imported
        And I navigate to the 'Calculation jobs' tab
        And I create a new calculation job

        When I select the situation 'beoogde situatie' as 'proposedSituation'
        Then I expect the following scenarios to be selected
            | scenarioName        |
            | beoogde situatie    |
        And no error should show

        When I select the situation 'Beoogd gebruik' as 'proposedSituation'
        Then I expect the following scenarios to be selected
            | scenarioName        |
            | Beoogd gebruik      |
        And no error should show

        When I select the situation 'Geen' as 'proposedSituation'
        Then I expect no scenarios to be selected
        And the following error is shown: 'Er zijn geen situaties geselecteerd om door te rekenen '

    @Smoke
    Scenario: Test with multiple calculation jobs
        Given I login with correct username and password
        And I select file 'scenario/scenario_composting_proposed.gml' to be imported, but I don't upload it yet
        And I select file 'scenario/scenario_greenhouse_proposed.gml' to be imported
        And I navigate to the 'Calculation jobs' tab
        When I create a new calculation job

        When I select the situation 'beoogde situatie' as 'proposedSituation'        
        Then I expect the following scenarios to be selected
            | scenarioName        |
            | beoogde situatie    |
        And no error should show

        When I save the calculation job
        Then the calculation jobs list has 1 calculation jobs

        When I create a new calculation job
        And I select the situation 'beoogde situatie' as 'proposedSituation'
        Then I expect the following scenarios to be selected
            | scenarioName     |
            | beoogde situatie |
        And no error should show
        And I expect the duplicate calculation job warning to show

        And I select the situation 'Beoogd gebruik' as 'proposedSituation'
        Then I expect the following scenarios to be selected
            | scenarioName   |
            | Beoogd gebruik |
        And no error should show
        And I expect the duplicate calculation job warning to not show

        When I save the calculation job
        Then the calculation jobs list has 2 calculation jobs

        When I switch calculation job to calculation job '1'
        Then I expect the following scenarios to be selected
            | scenarioName     |
            | beoogde situatie |
        And no error should show
        And I expect the duplicate calculation job warning to not show

        When I delete the opened calculation job
        Then I expect the calculation job '2' to not exist
        And the calculation jobs list has 1 calculation jobs
        When I switch calculation job to calculation job '1'
        And I delete the opened calculation job
        Then the calculation jobs list has 0 calculation jobs

    @Smoke
    Scenario: Import multiple files, each having emissions
        Given I login with correct username and password
        And I select file 'scenario/scenario_agriculture_reference.gml' to be imported, but I don't upload it yet
        And I select file 'scenario/scenario_agriculture_proposed.gml' to be imported, but I don't upload it yet
        And I select file 'scenario/scenario_agriculture_netting.gml' to be imported
        And I navigate to the 'Calculation jobs' tab
        And I create a new calculation job
        And I select the situation 'Referentie 1997' as 'referenceSituation'
        And I select the situation 'Saldering' as 'off_site_reductionSituation'
        Then I expect the following data in the scenario overview
            | name            | situationType | year | nettingFactor | emissionSources | emissionNOx | emissionNH3  |
            | Referentie 1997 | Referentie    | 2024 |               | 5               | 332,7 kg/j  | 661,3 kg/j   |
            | Gewenst         | Beoogd        | 2024 |               | 6               | 278,0 kg/j  | 1.497,8 kg/j |
            | Saldering       | Saldering     | 2024 | 0,00          | 2               | 0,0 kg/j    | 1.100,2 kg/j |
        And I expect the following scenarios to be selected
            | scenarioName    |
            | Referentie 1997 |
            | Gewenst         |
            | Saldering       |

        And I expect the selected 'REFERENCE' situation to be 'Referentie 1997'
        And I expect the selected 'PROPOSED' situation to be 'Gewenst'
        And I expect the selected 'OFF_SITE_REDUCTION' situation to be 'Saldering'
        And no error should show

        And I select the situation 'Geen' as 'referenceSituation'
        Then I expect the following scenarios to be selected
            | scenarioName |
            | Gewenst      |
            | Saldering    |
        And no error should show

        And I select the situation 'Geen' as 'proposedSituation'
        And I select the situation 'Geen' as 'off_site_reductionSituation'
        Then I expect no scenarios to be selected
        And the following error is shown: 'Er zijn geen situaties geselecteerd om door te rekenen '

    Scenario: Check whether a calculation job has more than the maximum of 6 situations
        Given I login with correct username and password
        And I enable the advanced importmodus

        # Import file with 1 reference, 1 project and 6 temporary situations
        When I select a file with multiple situations 'AERIUS_gml_many_situations.zip' to be imported in advanced mode
        And I choose to import the files
        And I navigate to the 'Calculation jobs' tab
        And I create a new calculation job
        Then no error should show

        When I select calculation jobType 'Maximaal tijdelijk effect'
        Then the following error is shown: 'Er ontbreken verplichte situaties: Tijdelijk'
        Then the following error is shown: 'Er zijn geen situaties geselecteerd om door te rekenen'
        And I select the situation 'Referentie' as 'referenceSituation'
        Then the following error is shown: 'Er ontbreken verplichte situaties: Tijdelijk'
        And I select the situation 'Saldering' as 'off_site_reductionSituation'
        Then the following error is shown: 'Er ontbreken verplichte situaties: Tijdelijk'
        And I check the checkbox for the temporary situation 'Tijdelijk 1'
        Then no error should show
        And I check the checkbox for the temporary situation 'Tijdelijk 2'
        Then no error should show
        And I check the checkbox for the temporary situation 'Tijdelijk 3'
        Then no error should show
        And I check the checkbox for the temporary situation 'Tijdelijk 4'
        Then no error should show

        When I check the checkbox for the temporary situation 'Tijdelijk 5'
        Then the following error is shown: 'Meer dan het maximum aantal situaties om door te rekenen (6) zijn geselecteerd.'

        And I select the situation 'Geen' as 'referenceSituation'
        Then no error should show
        And I select the situation 'Geen' as 'off_site_reductionSituation'
        Then no error should show
        And I check the checkbox for the temporary situation 'Tijdelijk 5'
        And I check the checkbox for the temporary situation 'Tijdelijk 6'
        Then no error should show

    Scenario: Change the name of a calculation task
        Given I login with correct username and password
        Then I select file './scenario/scenario_agriculture_proposed.gml' to be imported
        And I navigate to the 'Calculation jobs' tab
        And I create a new calculation job

        When I change the calculation job name to 'Test berekening'
        And I select the situation 'Gewenst' as 'proposedSituation'
        And I save the calculation job
        Then I expect a calculation job with title 'Test berekening' to exist

        When I create a new calculation job
        And I change the calculation job name to 'Berekening 2'
        And I save the calculation job
        Then I expect a calculation job with title 'Test berekening' to exist
        And I expect a calculation job with title 'Berekening 2' to exist

    Scenario: Copy calculation task and delete all existing calculation jobs
        Given I login with correct username and password
        Then I select file './AERIUS_all_situation_types.zip' to be imported
        And I navigate to the 'Calculation jobs' tab
        And I create a new calculation job
        And I select the situation 'Situatie 1' as 'proposedSituation'
        And I select the situation 'Situatie 4' as 'referenceSituation'
        And I select the situation 'Situatie 2' as 'off_site_reductionSituation'
        And I save the calculation job
        And  I expect a calculation job with title 'Rekentaak 1' to exist
        When I copy the selected calculation job
        Then I expect a calculation job with title 'Kopie van Rekentaak 1' to exist
        And I expect that the calculation job is of type 'Projectberekening'
        And I expect that the calculation method is 'OwN2000-methode'
        And I expect the 'PROJECT' scenario to be 'Situatie 1'
        And I expect the 'REFERENCE' scenario to be 'Situatie 4'
        And I expect the 'OFF_SITE_REDUCTION' scenario to be 'Situatie 2'

        # delete all calculation jobs
        When I delete all the calculation jobs
        Then I expect the calculation job 'Rekentaak 1' to not exist
        And I expect the calculation job 'Kopie van Rekentaak 1' to not exist

    @Smoke
    Scenario: Check link to export from calculation jobs
        Given I login with correct username and password
        And I enable the advanced importmodus
        When I select a file with multiple situations 'AERIUS_gml_many_situations.zip' to be imported in advanced mode
        And I choose to import the files
        And I navigate to the 'Calculation jobs' tab
        Then I validate the calculation job message 'Er zijn nog geen rekentaken. Maak een nieuwe rekentaak aan door op onderstaande knop te drukken.'
        When I create a new calculation job

        When I select calculation jobType 'Maximaal tijdelijk effect'
        Then the following error is shown: 'Er ontbreken verplichte situaties: Tijdelijk'
        Then the following error is shown: 'Er zijn geen situaties geselecteerd om door te rekenen'
        And I select the situation 'Referentie' as 'referenceSituation'
        Then the following error is shown: 'Er ontbreken verplichte situaties: Tijdelijk'
        And I select the situation 'Saldering' as 'off_site_reductionSituation'
        Then the following error is shown: 'Er ontbreken verplichte situaties: Tijdelijk'
        And I check the checkbox for the temporary situation 'Tijdelijk 1 - Tijdelijk'
        Then no error should show
        # Test default values
        When I save the calculation job
        And I press the export button
        Then I expect the page title to be 'Exporteren'
        And the selected base calculation job is 'Rekentaak 1 (Maximaal tijdelijk effect)'
        Then the following situations are selected
        | name        | shouldBeSelected |
        | Beoogd      | false            |
        | Referentie  | true             |
        | Saldering   | true             |
        | Tijdelijk 1 | true             |
        | Tijdelijk 2 | false            |
        | Tijdelijk 3 | false            |
        | Tijdelijk 4 | false            |
        | Tijdelijk 5 | false            |
        | Tijdelijk 6 | false            |

        # Adjust values
        When I navigate to the 'Calculation jobs' tab
        When I create a new calculation job
        When I select calculation jobType 'Maximaal tijdelijk effect'
        And I select the situation 'Geen' as 'referenceSituation'
        And I select the situation 'Saldering' as 'off_site_reductionSituation'
        And I check the checkbox for the temporary situation 'Tijdelijk 1'
        And I check the checkbox for the temporary situation 'Tijdelijk 3'
        And I check the checkbox for the temporary situation 'Tijdelijk 4'
        And I check the checkbox for the temporary situation 'Tijdelijk 5'
        And I check the checkbox for the temporary situation 'Tijdelijk 6'
        
        # Check adjusted values
        When I save the calculation job
        And I press the export button
        Then the selected base calculation job is 'Rekentaak 2 (Maximaal tijdelijk effect)'
        Then the following situations are selected
        | name        | shouldBeSelected |
        | Beoogd      | false            |
        | Referentie  | false            |
        | Saldering   | true             |
        | Tijdelijk 1 | true             |
        | Tijdelijk 2 | false            |
        | Tijdelijk 3 | true             |
        | Tijdelijk 4 | true             |
        | Tijdelijk 5 | true             |
        | Tijdelijk 6 | true             |

        # Check whether default values in other calculation job have changed (they shouldn't've)
        When I select the base calculation job 'Rekentaak 1 (Maximaal tijdelijk effect)'
        Then the selected base calculation job is 'Rekentaak 1 (Maximaal tijdelijk effect)'
        Then the following situations are selected
        | name        | shouldBeSelected |
        | Beoogd      | false            |
        | Referentie  | true             |
        | Saldering   | true             |
        | Tijdelijk 1 | true             |
        | Tijdelijk 2 | false            |
        | Tijdelijk 3 | false            |
        | Tijdelijk 4 | false            |
        | Tijdelijk 5 | false            |
        | Tijdelijk 6 | false            |

    Scenario: Start a calculation with only custom calculation points without having any
        Given I login with correct username and password
        Then I select file './scenario/scenario_agriculture_reference.gml' to be imported
        And I navigate to the 'Calculation jobs' tab
        And I create a new calculation job
        When I select the calculation method 'Alleen eigen rekenpunten'        
        Then the following error is shown: 'Er zijn geen rekenpunten om door te rekenen'
        Then the following error is shown: 'Er ontbreken verplichte situaties: Beoogd'
        Then the following error is shown: 'Er zijn geen situaties geselecteerd om door te rekenen'     
        When I save the calculation job
        Then the calculation wasn't saved
        And the error container is shown

    @Smoke
    Scenario: Calculate a calculcation job and then change it afterwards
        Given I login with correct username and password
        Then I select file './scenario/scenario_agriculture_proposed.gml' to be imported
        And I navigate to the 'Calculation jobs' tab
        Then I intercept the status of the calculation
        And I intercept the response from '/api/v8/ui/calculation/*/*/PROJECT_CALCULATION?summaryHexagonType=EXCEEDING_HEXAGONS&summaryOverlappingHexagonType=ALL_HEXAGONS*' with 'projectCalculationExceedingHexagons.json'
            | statisticType                  | ExpectedStatistics |
            | maxDecrease                    | 7.7                |
            | maxIncrease                    | 0.067              |
            | sumCartographicSurfaceIncrease | 77777777.7         |
            | sumCartographicSurfaceDecrease | 777.7              |
            | maxPercentageCriticalLevel     | 0.7777777777       |
            | sumCartographicSurface         | 7.9e7              |
            | maxTotal                       | 7777.99            |
        And I intercept the response from '/api/v8/ui/calculation/*/*/PROJECT_CALCULATION?summaryHexagonType=RELEVANT_HEXAGONS*' with 'projectCalculationRelevantHexagons.json'
            | statisticType                  | ExpectedStatistics |
            | maxDecrease                    | 6.6                |
            | maxIncrease                    | 0.057              |
            | sumCartographicSurfaceIncrease | 66666666.6         |
            | sumCartographicSurfaceDecrease | 666.6              |
            | maxPercentageCriticalLevel     | 0.66666666666      |
            | sumCartographicSurface         | 6.9e7              |
            | maxTotal                       | 6666.99            |
        And I intercept the response from '/api/v8/ui/calculation/*/*/SITUATION_RESULT?*' with 'situationResultExceedingHexagons.json'
            | statisticType          | ExpectedStatistics |
            | sumCartographicSurface | 9.9e7              |
            | maxContribution        | 9.99               |
            | maxTotal               | 9999.99            |
        And I intercept the response from '/api/v8/ui/calculation/*/*/SITUATION_RESULT?summaryHexagonType=RELEVANT_HEXAGONS*' with 'situationResultExceedingHexagons.json'
            | statisticType          | ExpectedStatistics |
            | sumCartographicSurface | 8.9e7              |
            | maxContribution        | 8.99               |
            | maxTotal               | 8888.99            |
        
        When I create a new calculation job
        And I save the calculation job
        And I start the calculation
        When I navigate to the 'Calculation jobs' tab
        And I edit the selected calculation job
        And I select the calculation method 'Alleen eigen rekenpunten'
        Then I see a warning that the calculation differs from the current selection

    Scenario: Import calculation and validate single scenario calculation job
        Given I login with correct username and password
        And I enable the advanced importmodus
        When I select a file with multiple situations 'AERIUS_gml_many_situations.zip' to be imported in advanced mode
        And I choose to import the files
        And I navigate to the 'Calculation jobs' tab
        Then I validate the calculation job message 'Er zijn nog geen rekentaken. Maak een nieuwe rekentaak aan door op onderstaande knop te drukken.'
        And I create a new calculation job
        Then no error should show

        When I select calculation jobType 'Enkele situatie'
        And I select the situation 'Geen'
        And the following error is shown: 'Er zijn geen situaties geselecteerd om door te rekenen '
        And I select the situation 'Referentie'
        Then no error should show
        And I select the situation 'Beoogd'
        Then no error should show
        And I select the situation 'Saldering'
        Then no error should show
        And I select the situation 'Tijdelijk 1'
        Then no error should show
        And situation 'TEMPORARY-CHECKBOX' should not exist

    Scenario: Import calculation and validate process contribution calculation job
        Given I login with correct username and password
        And I enable the advanced importmodus
        When I select a file with multiple situations 'AERIUS_gml_many_situations.zip' to be imported in advanced mode
        And I choose to import the files
        And I navigate to the 'Calculation jobs' tab
        And I create a new calculation job
        Then no error should show

        When I select calculation jobType 'Projectberekening'
        And I select the situation 'Geen' as 'off_site_reductionSituation'
        Then no error should show
        And I select the situation 'Geen' as 'referenceSituation'
        Then no error should show
        And I select the situation 'Geen' as 'proposedSituation'
        And the following error is shown: 'Er ontbreken verplichte situaties: Beoogd'
        And I select the situation 'Saldering' as 'off_site_reductionSituation'
        And the following error is shown: 'Er ontbreken verplichte situaties: Beoogd'
        And I select the situation 'Referentie' as 'referenceSituation'
         And the following error is shown: 'Er ontbreken verplichte situaties: Beoogd'
        And I select the situation 'Beoogd' as 'proposedSituation'
        Then no error should show
        And situation 'TEMPORARY-CHECKBOX' should not exist
        And situation 'TEMPORARY-LISTBOX' should not exist

    Scenario: Import calculation and validate max temporary effect calculation job
        Given I login with correct username and password
        And I enable the advanced importmodus
        When I select a file with multiple situations 'AERIUS_gml_many_situations.zip' to be imported in advanced mode
        And I choose to import the files
        And I navigate to the 'Calculation jobs' tab
        And I create a new calculation job
        Then no error should show

        When I select calculation jobType 'Maximaal tijdelijk effect'
        And the following error is shown: 'Er ontbreken verplichte situaties: Tijdelijk'
        And I check the checkbox for the temporary situation 'Tijdelijk 1'
        Then no error should show
        And I select the situation 'Geen' as 'off_site_reductionSituation'
        Then no error should show
        And I select the situation 'Geen' as 'referenceSituation'
        Then no error should show
        And situation 'PROPOSED' should not exist
        And situation 'TEMPORARY-LISTBOX' should not exist
