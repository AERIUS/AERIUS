/*CommonPage
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { CommonPage } from './CommonPage';

export class CommonBuildingPage {
  static createNewBuildingFromTheInputPage() {
    cy.get('[id="calculationLaunchButtonNewBuilding"]').click({ force: true });
  }

  static createNewBuildingFromBuildingList() {
    cy.get('[id="building-sourceListButtonNewSource"]').click({ force: true });
  }

  static nameBuilding(name: string) {
    cy.get('[id="buildingNameInput"]').clear().type(name);
  }

  static selectBuilding(buildingName: string) {
    cy.get(".buildings").contains(buildingName).click();
  }

  static editBuilding() {
    cy.get('[id="building-sourceListButtonEditSource"]').click();
  }

  static selectBuildingFromPrimaryBuildingList(buildingName: string) {
    cy.get('[id="buildingSelection"]').select(buildingName);
  }

  static selectBuildingShape(shape: string) {
    switch (shape) {
      case "Circle":
        cy.get('[id="circleButton"]').click();
        break;
      case "Polygon":
        cy.get('[id="polygonButton"]').click();
        break;
      default:
        throw new Error("unknown building shape: " + shape);
    }
  }

  static buildingSourceGeometryInput(buildingGeometryInput: string) {
    cy.get('[id="buildingGeometryInput"]').clear().type(buildingGeometryInput);
  }

  static validateBuildingSourceGeometryInput(expectedValue: string) {
    cy.get('[id="buildingGeometryInput"]')
      .invoke('val')
      .should('equal', expectedValue);
  }

  static enterHeight(height: string) {
    cy.get('[id="buildingHeightInput"]').type("{selectAll}").type(height);
  }

  static enterDiameter(buildingDiameter: string) {
    cy.get('[id="buildingDiameter"]').type("{selectAll}").type(buildingDiameter);
  }

  // Save the data for building
  static saveBuilding() {
    cy.get('[id="buttonSaveBuilding"]').click();
  }

  static assertPolygonalBuildingSavedAndSelected(name: string, length: string, width: string, height: string, orientation: string) {
    // Assert building is in building list
    cy.get('[id^="building-"][id$="-label"]').contains(name).should("be.visible");

    // Assert detail screen building is given building
    cy.get('[id="buildingDetailTitle"]').should("have.text", name);
    CommonPage.assertMinMaxLabel("buildingLength", length);
    CommonPage.assertMinMaxLabel("buildingWidth", width);
    CommonPage.assertMinMaxLabel("buildingHeight", height);
    cy.get('[id="buildingOrientation"]').should("have.text", orientation);
  }

  static assertCircularBuildingSavedAndSelected(name: string, diameter: string, height: string) {
    // Assert building is in building list
    cy.get('[id^="building-"][id$="-label"]').contains(name).should("be.visible");

    // Assert detail screen building is given building
    cy.get('[id="buildingDetailTitle"]').should("have.text", name);
    cy.get('[id="buildingDiameter"] > [id="label"]').should("have.text", diameter);
    cy.get('[id="buildingHeight"] > [id="minMaxLabel"]').should("have.text", height);
  }

  
  static toggleBuildingInfluenced() {
    cy.get("#buildingInfluence").click({ force: true });
  }

  static addNewBuilding() {
    cy.get('[id="addNewButton"').click();
  }
  
  static duplicateBuilding() {
    cy.get('[id="building-sourceListButtonCopySource"]').click();
  }

  static deleteSelectedBuilding() {
    cy.get('[id="building-sourceListButtonDeleteSource"]').click();
  }
}
