/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class AgricultureSourcePage {
  static addAnimalHousingSystemWithHousingCodeNumberOfAnimalsNumberOfDays(housingCode: string, numberOfAnimals: string, numberOfDays: string) {
    this.addAnimalHousingSystemWithHousingCodeNumberOfAnimals(housingCode, numberOfAnimals);
    cy.get('[id="farmAnimalHousingStandardNumberOfDays"]').clear().type(numberOfDays);
  }

  static addAnimalHousingSystemWithHousingCodeNumberOfAnimals(housingCode: string, numberOfAnimals: string) {
    cy.get('[id="collapsibleSubSources"] > .line > .title').click({
      force: true
    });
    cy.get('[id="sourceListButtonNewSource"] > .icon').click({ force: true });
    cy.get('[id="tab-farmAnimalHousingType-STANDARD"]').click({ force: true });
    this.editAnimalHousingSystemWithHousingCodeAndNumberOfAnimals(housingCode, numberOfAnimals);
  }

  static animalHousingSystemWithHousingCodeDaysCreated(housingCode: string, numberOfAnimals: string, factor: string, days:string, emission:string) {
    // Validate lodging system
    cy.get('[id="farmAnimalHousingDetailStandardCode"]').should("have.text", housingCode);
    cy.get('[id="farmAnimalHousingDetailStandardNumberOfAnimals"]').should("have.text", numberOfAnimals);
    cy.get('[id="farmAnimalHousingDetailStandardEmissionFactor"]').should("include.text", factor);
    if (days === "") {
      cy.get('[id="farmAnimalHousingDetailStandardNumberOfDays"]').should("not.exist");
    } else {
      cy.get('[id="farmAnimalHousingDetailStandardNumberOfDays"]').should("have.text", days);
    }
    cy.get('[id="farmAnimalHousingDetailStandardEmission"]').should("include.text", emission);
    cy.get('[id="farmAnimalHousingDetailDescription"]').should("include.text", housingCode);
  }

  static animalHousingSystemIsCopied(lodgingSystemWithHousingCode: string) {
    cy.get(
      '[id="farmAnimalHousingSubSource-0"] [id="farmAnimalHousingDetailStandardRow-0"] > [id="farmAnimalHousingDetailStandardCode"]'
    ).contains(lodgingSystemWithHousingCode);
    cy.get(
      '[id="farmAnimalHousingSubSource-1"] [id="farmAnimalHousingDetailStandardRow-0"] > [id="farmAnimalHousingDetailStandardCode"]'
    ).contains(lodgingSystemWithHousingCode);
  }

  static fillInFieldsFarmlandOptionAmountNumberOfDaysSource(category: string, option: any, amount: string, numberOfDays: string) {
    cy.get('[id="emissionSourceFarmlandCategory"]').select(category);
    cy.get('[aria-labelledby="emissionSourceFarmlandStandardCategory"]').type(option);
    cy.get('[id="' + option.replaceAll(" ", "-") + '"]').click();
    cy.get('[id="numberOfAnimalsInput"]').type(amount);
    cy.get('[id="numberOfDaysInput"]').type(numberOfDays);
  }

  static fillInFieldsFarmlandOptionAmountNumberOfDaysSourceCorrectrlySaved(category: string, option: string, amount: string, numberOfDays: string) {
    cy.get('[id="farmlandDetailActivity-PASTURE-0"]').contains(category);
    cy.get('[id="farmlandStandardDetailDescription0"]').contains(option);
    cy.get('[id="farmlandStandardDetailNumberOfAnimals0"]').contains(amount);
    cy.get('[id="farmlandStandardDetailNumberOfDays0"]').type(numberOfDays);
  }

  static fillInFieldsManureStorageSource(category: any, area: string) {
    cy.get('[id="collapsibleSubSources-line"]').click();
    cy.get('[id="sourceListButtonNewSource"]').click();
    cy.get('[aria-labelledby="emissionSourceManureStorageStandardCategory"]').type(category);
    cy.get('[id="' + category.replaceAll(" ", "-") + '"]').click();
    cy.get('[id="metersSquaredInput"]').clear().type(area, { force: true });
  }

  static fillInFieldsManureStorageSourceCorrectlySaved(category: string, area: string) {
    cy.get('[id="manureStorageStandardDetailDescription0"]').contains(category);
    cy.get('[id="manureStorageStandardDetailMetersSquared0"]').should("have.text", area);
  }

  static assertADMSSourceHeightAndDiameter(sourceHeight: string, sourceDiameter: string) {
    cy.get('[id="sourceADMSHeight"]').should("have.text", sourceHeight);
    cy.get('[id="sourceADMSDiameter"]').should("have.text", sourceDiameter);
  }

  static editAnimalHousingSystemWithHousingCodeAndNumberOfAnimals(housingCode: string, numberOfAnimals: string) {
    cy.get('[id="farmlandStandardCategoryCodeItem"]').clear({ force: true }).type(housingCode);
    cy.get('[id="' + housingCode.replace(" ", "-") + '"]')
      .should("contain", housingCode)
      .click();
    cy.get('[id="farmAnimalHousingStandardNumberOfAnimals"]').clear({ force: true }).type(numberOfAnimals);
  }
}
