/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";

import { RoadTransportationSourceColdStartPage } from "./RoadTransportationSourceColdStartPage";
import { BuildingsPage } from "../Buildings/BuildingsPage";

  Given("time unit {string} with number of cold starts {string} for Euro class is correctly saved", (timeUnit, numberOfColdStarts) => {
    RoadTransportationSourceColdStartPage.timeUnitAndNumberOfColdStartsForEuroClassIsCorrectlySaved(timeUnit, numberOfColdStarts);
  });
    
  Given("time unit {string} with number of cold starts {string} for Euro class Other is correctly saved", (timeUnit, numberOfColdStarts) => {
    RoadTransportationSourceColdStartPage.timeUnitAndNumberOfColdStartsForEuroClassOtherIsCorrectlySaved(timeUnit, numberOfColdStarts);
  });

  Given("I select vehicle type {string}", (vehicleType) => {
    RoadTransportationSourceColdStartPage.selectVehicleTypeForCustomSpecification(vehicleType);
  });

  Given("vehicle type {string} is correctly saved", (vehicleType) => {
    RoadTransportationSourceColdStartPage.assertVehicleTypeCorrectlySaved(vehicleType);
  });

  Given(
    "the building with name {string}, length {string}, width {string}, height {string} and orientation {string} is saved and selected for the emission source",
    (name, length, width, height, orientation) => {
      BuildingsPage.assertPolygonalBuildingSavedAndSelectedForEmissionSource(name, length, width, height, orientation);
    }
  );

  Given("emission NO2 input field should not exist", () => {
    RoadTransportationSourceColdStartPage.emissionInputNO2ShouldNotEist();
  });

  Given("the detail emission contains {int} substance", (substanceCount) => {
    RoadTransportationSourceColdStartPage.detailEmissionSubtanceCount(substanceCount);
  });
