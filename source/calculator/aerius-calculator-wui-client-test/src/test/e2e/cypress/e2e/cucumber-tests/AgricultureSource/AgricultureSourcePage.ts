/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class AgricultureSourcePage {
  static changeAmountOfAnimalsInAdditionalRuleTo(amountOfFarmAnimals: string) {
    cy.get('[id="farmAnimalHousingStandardNumberOfAnimals"]').clear({ force: true }).type(amountOfFarmAnimals);
  }

  static deleteAdditionalRule() {
    cy.get('[id="additionalDelete-0"]').click({ force: true });
  }

  static assertNoAdditionalRuleExists() {
    cy.get('[id="farmAnimalHousingDetailStandardRows"]').find('[id^="farmAdditionalHousingSystemRow-"]').should("not.exist");
  }

  static fillInReductionPercentage(reductionPercentage: string) {
    cy.get('[id^="farmAnimalHousingStandardAddReduction-"]').clear().type(reductionPercentage);
  }

  static fillInExplanation(explanationText: string) {
    cy.get('[id^="farmAnimalHousingStandardAddExplanation-"]').type(explanationText);
  }

  static validateAnimalHousingConstrainingFootnote(visibleState: string) {
    switch (visibleState) {
      case "visible":
        cy.get('[id="farmAnimalHousingConstrainedFootNote"]')
          .should("exist")
          .should("contain", "Volgens de Omgevingsregeling artikel 4.6,");
        break;
      case "not visible":
        cy.get('[id="farmAnimalHousingConstrainedFootNote"]').should("not.exist");
        break;
      default:
        throw new Error("unknown visible type: " + visibleState);
    }
  }
}
