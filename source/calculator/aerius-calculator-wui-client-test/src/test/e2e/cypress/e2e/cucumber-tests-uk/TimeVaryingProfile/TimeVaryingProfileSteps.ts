/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";
import { TimeVaryingProfilePage } from "./TimeVaryingProfilePage";

Given("I test the time-varying profile settings for the following sectors and sector groups", (dataTable) => {
  TimeVaryingProfilePage.testTimeVaryingProfileSettingsOfSectorGroup(dataTable);
});

Given("I see a list of the following predefined time-varying profiles", (dataTable) => {
  TimeVaryingProfilePage.checkListOfPredefinedTimeVaryingProfilesExists(dataTable);
});

Given("I see a list of the following custom time-varying profiles", (dataTable) => {
  TimeVaryingProfilePage.checkListOfCustomTimeVaryingProfilesExists(dataTable);
});

Given("I select time-varying profile {string}", (timeVaryingProfileName) => {
  TimeVaryingProfilePage.selectTimeVaryingProfileFromList(timeVaryingProfileName);
});

Given(
  "I see a detail panel containing detailed information about time-varying profile {string} with description {string} and graph with {int} lines",
  (name, description, linesInGraph) => {
    TimeVaryingProfilePage.checkDetailPanelForTimeVaryingProfile(name, description, linesInGraph);
  }
);

Given("I expect the selected profile to be {string}", (expectedTimeVaryingProfile) => {
  TimeVaryingProfilePage.assertSelectedTimeVaryingProfile(expectedTimeVaryingProfile);
});

Given("I expect the graph to have {int} lines", (expectedAmountOfLines) => {
  TimeVaryingProfilePage.assertTimeVaryingProfileGraphHasAmountOfLines(expectedAmountOfLines);
});

Given("I expect the {string} graph to have {int} lines", (profileType, linesInGraph) => {
  TimeVaryingProfilePage.assertCustomTimeVaryingProfileGraphHasAmountOfLines(profileType, linesInGraph);
});

Given("I check the details of the custom diurnal profile", (dataTable) => {
  TimeVaryingProfilePage.checkDetailsDiurnalProfile(dataTable);
});

Given("I check the details of the custom monthly profile", (dataTable) => {
  TimeVaryingProfilePage.checkDetailsMonthlyProfile(dataTable);
});

Given("I see a launch button for adding a new custom {string} profile", (profileType) => {
  TimeVaryingProfilePage.checkLaunchButtonCustomProfile(profileType);
});

Given("I select custom profile {string}", (selectedProfile) => {
  TimeVaryingProfilePage.selectCustomProfile(selectedProfile);
});

Given("I choose to add a custom {string} profile when there are no custom profiles", (profileType) => {
  TimeVaryingProfilePage.addCustomProfileWhenNoCustomProfiles(profileType);
});

Given("I choose to add a custom {string} profile", (profileType) => {
  TimeVaryingProfilePage.addCustomProfile(profileType);
});

Given("I choose to edit the custom profile", () => {
  TimeVaryingProfilePage.editCustomProfile();
});

Given("I choose to copy the custom profile", () => {
  TimeVaryingProfilePage.copyCustomProfile();
});

Given("I choose to delete the custom profile", () => {
  TimeVaryingProfilePage.deleteCustomProfile();
});

Given("I choose to delete all the custom profiles", () => {
  TimeVaryingProfilePage.deleteAllCustomProfiles();
});

Given("I click on the button to {string}", (buttonName) => {
  TimeVaryingProfilePage.clickOnButton(buttonName);
});

Given("I click on the {string} button for profile type {string}", (buttonName, profileType) => {
  TimeVaryingProfilePage.clickOnButtonForProfileType(buttonName, profileType);
});

Given("I change the name of the profile to {string}", (profileName) => {
  TimeVaryingProfilePage.editProfileName(profileName);
});

Given("I edit the column {string} for time of the day {string} to value {string}", (columnName, timeOfDay, inputValue) => {
  TimeVaryingProfilePage.editValuesForTimeOfDayForColumn(columnName, timeOfDay, inputValue);
});

Given("I edit the column Monthly profile for month {string} to value {string}", (monthInput, inputValue) => {
  TimeVaryingProfilePage.editValuesForMonthlyProfile(monthInput, inputValue);
});

Given("I select input mode {string} to add a new time varying profile", (inputMode) => {
  TimeVaryingProfilePage.selectInputMode(inputMode);
});

Given("I paste the following text in the text area for the {string} profile:", (profileType, input) => {
  TimeVaryingProfilePage.pasteRawInput(profileType, input);
});

Given("button {string} for profile type {string} is disabled", (buttonName, profileType) => {
  TimeVaryingProfilePage.assertButtonIsDisabledForProfileType(buttonName, profileType);
});

Given("button {string} for profile type {string} is enabled", (buttonName, profileType) => {
  TimeVaryingProfilePage.assertButtonIsEnabledForProfileType(buttonName, profileType);
});

Given("the following error for profile type {string} is visible: {string}", (profileType, errorText) => {
  TimeVaryingProfilePage.assertErrorIsVisibleForProfileType(profileType, errorText);
});

Given("the following notice for profile type {string} is visible: {string}", (profileType, noticeText) => {
  TimeVaryingProfilePage.assertNoticeIsVisibleForProfileType(profileType, noticeText);
});

Given("the following error about the required amount is visible: {string}", (errorTextAmount) => {
  TimeVaryingProfilePage.assertErrorRequiredAmountIsVisible(errorTextAmount);
});

Given("the following notice about the required amount is visible: {string}", (noticeTextAmount) => {
  TimeVaryingProfilePage.assertNoticeRequiredAmountIsVisible(noticeTextAmount);
});

Given("the user has to confirm and the following notice is visible: {string}", (noticeTextAmount) => {
  TimeVaryingProfilePage.confirmAndAssertNoticeIsVisible(noticeTextAmount);
});

Given("I expect the exported text in the text area for the {string} profile:", (profileType, exportedText) => {
  TimeVaryingProfilePage.assertExportedTextInTextArea(profileType, exportedText);
});

Given("I expect a graph line for {string}", (graphLineType) => {
  TimeVaryingProfilePage.assertGraphLineForType(graphLineType);
});
