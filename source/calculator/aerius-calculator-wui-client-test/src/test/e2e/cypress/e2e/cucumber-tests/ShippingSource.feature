#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: ShippingSource
    Test adding, editing and deleting shipping sources, and validate the filled in values in between.

    Scenario: Create new shipping source - maritime - dock
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Aanlegplaats'
        And I fill location with coordinates 'LINESTRING(99727.25 497872.33,100995.49 498063.89,101847.59 498090.31,102455.28 497997.83,103426.28 497905.36)'
        And I fill the maritime shipping description 'Ship heading for docking', type 'Passagiersschepen GT: 100-1599', visits '240', time unit '/jaar', average residence time '12' hours and shorepower usage '12' percent
        And I fill the maritime shipping description 'Another ship heading for docking', type 'Container, GDC (stukgoed), RoRo GT: 100-1599', visits '1', time unit '/etmaal', average residence time '5' hours and shorepower usage '6.3' percent
        And I save the data and wait for refresh
        And I set the calculation year to '2030'
        And I save the scenario updates        
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Scheepvaart' and sector 'Zeescheepvaart: Aanlegplaats' is correctly saved
        And location with coordinates 'X:101575,19 Y:498081,86' is correctly saved
        Then maritime 'Docked' shipping row '0' with description 'Ship heading for docking', type 'Passagiersschepen GT: 100-1599', visits '240', time unit '/jaar', average residence time '12 u', shorepower usage '12,0 %', emission NOx '329,4 kg/j' and emission NH3 '0,0 kg/j' is correctly created
        And maritime 'Docked' shipping row '1' with description 'Another ship heading for docking', type 'Container, GDC (stukgoed), RoRo GT: 100-1599', visits '1', time unit '/etmaal', average residence time '5 u', shorepower usage '6,3 %', emission NOx '490,3 kg/j' and emission NH3 '0,0 kg/j' is correctly created
        And the total 'maritime shipping' emission 'NOx' is '819,7 kg/j'
        And the total 'maritime shipping' emission 'NH3' is '0,0 kg/j'    
        And I choose to edit the source 
        Then I select the subsource 'Another ship heading for docking'
        Then maritime docked with description 'Another ship heading for docking', type 'CG100', visits '1', time unit 'DAY', average residence time '5' and shorepower usage '6.3' is available for edit
        Then I select the subsource 'Ship heading for docking'
        And maritime docked with description 'Ship heading for docking', type 'PS100', visits '240', time unit 'YEAR', average residence time '12' and shorepower usage '12' is available for edit

    Scenario: Create new shipping source - maritime - inland route
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Binnengaats route'
        And I fill location with coordinates 'LINESTRING(99727.25 497872.33,100995.49 498063.89,101847.59 498090.31,102455.28 497997.83,103426.28 497905.36)'
        And I fill the maritime shipping description 'Ship inland', type 'Passagiersschepen GT: 100-1599', visits '242' and time unit '/jaar'
        And I fill the maritime shipping description 'Another ship inland', type 'Container, GDC (stukgoed), RoRo GT: 100-1599', visits '2' and time unit '/etmaal'
        And I save the data and wait for refresh
        And I set the calculation year to '2030'
        And I save the scenario updates        
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Scheepvaart' and sector 'Zeescheepvaart: Binnengaats route' is correctly saved
        And location with coordinates 'X:101575,19 Y:498081,86' is correctly saved
        Then maritime 'Standard' shipping row '0' with description 'Ship inland', type 'Passagiersschepen GT: 100-1599', visits '242', time unit '/jaar', emission NOX '383,4 kg/j' and emission NH3 '0,0 kg/j' is correctly saved
        And maritime 'Standard' shipping row '1' with description 'Another ship inland', type 'Container, GDC (stukgoed), RoRo GT: 100-1599', visits '2', time unit '/etmaal', emission NOX '1.054,8 kg/j' and emission NH3 '0,0 kg/j' is correctly saved
        And the total 'maritime shipping' emission 'NOx' is '1.438,1 kg/j'
        And the total 'maritime shipping' emission 'NH3' is '0,0 kg/j'

    Scenario: Create new shipping source - maritime - maritime route
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Zeeroute'
        And I fill location with coordinates 'LINESTRING(99727.25 497872.33,100995.49 498063.89,101847.59 498090.31,102455.28 497997.83,103426.28 497905.36)'
        And I fill the maritime shipping description 'Ship from sea', type 'Passagiersschepen GT: 100-1599', visits '244' and time unit '/jaar'
        And I fill the maritime shipping description 'Arrr, another ship from sea', type 'Container, GDC (stukgoed), RoRo GT: 100-1599', visits '4' and time unit '/uur'
        And I save the data and wait for refresh
        And I set the calculation year to '2030'
        And I save the scenario updates        
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Scheepvaart' and sector 'Zeescheepvaart: Zeeroute' is correctly saved
        And location with coordinates 'X:101575,19 Y:498081,86' is correctly saved
        Then maritime 'Standard' shipping row '0' with description 'Ship from sea', type 'Passagiersschepen GT: 100-1599', visits '244', time unit '/jaar', emission NOX '198,3 kg/j' and emission NH3 '0,0 kg/j' is correctly saved
        And maritime 'Standard' shipping row '1' with description 'Arrr, another ship from sea', type 'Container, GDC (stukgoed), RoRo GT: 100-1599', visits '4', time unit '/uur', emission NOX '47,0 ton/j' and emission NH3 '0,0 kg/j' is correctly saved
        And the total 'maritime shipping' emission 'NOx' is '47,2 ton/j'
        And the total 'maritime shipping' emission 'NH3' is '0,0 kg/j'

    Scenario: Create new shipping source - inland - dock
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Scheepvaart' and sector 'Binnenvaart: Aanlegplaats'
        And I fill location with coordinates 'LINESTRING(99727.25 497872.33,100995.49 498063.89,101847.59 498090.31,102455.28 497997.83,103426.28 497905.36)'
        And I fill the inland shipping description 'Ship heading for docking', type 'Motorvrachtschip - M1  (Spits)', visits '241', time unit '/jaar', average residence time '12' hour, shorepower usage '12.9' and percent laden '14' percent
        And I fill the inland shipping description 'Another ship heading for docking', type 'Duwstel – BI (Europa I)', visits '1', time unit '/maand', average residence time '623' hour, shorepower usage '31.1' and percent laden '1' percent
        And I save the data and wait for refresh
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Scheepvaart' and sector 'Binnenvaart: Aanlegplaats' is correctly saved
        And location with coordinates 'X:101575,19 Y:498081,86' is correctly saved
        Then inland 'Docked' shipping row '0' with description 'Ship heading for docking', type 'Motorvrachtschip - M1  (Spits)', visits '241', time unit '/jaar', average residence time '12 u', shorepower usage '12,9 %', percent laden '14,0 %', emission NOX '239,3 kg/j' and emission NH3 '0,0 kg/j' is correctly saved
        And inland 'Docked' shipping row '1' with description 'Another ship heading for docking', type 'Duwstel – BI (Europa I)', visits '1', time unit '/maand', average residence time '623 u', shorepower usage '31,1 %', percent laden '1,0 %', emission NOX '489,3 kg/j' and emission NH3 '0,0 kg/j' is correctly saved
        And the total 'inland shipping' emission 'NOx' is '728,6 kg/j'
        And the total 'inland shipping' emission 'NH3' is '0,0 kg/j'
        And I choose to edit the source        
        Then I select the subsource 'Ship heading for docking'
        And inland docked with description 'Ship heading for docking', type 'M1', visits '241', time unit 'YEAR', average residence time '12', shorepower usage '12.9' and percent laden '14' is available for edit
        Then I select the subsource 'Another ship heading for docking'
        Then inland docked with description 'Another ship heading for docking', type 'BI', visits '1', time unit 'MONTH', average residence time '623', shorepower usage '31.1' and percent laden '1' is available for edit

    Scenario: Create new shipping source - inland route
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Scheepvaart' and sector 'Binnenvaart: Vaarroute'
        And I fill location with coordinates 'LINESTRING(99727.25 497872.33,100995.49 498063.89,101847.59 498090.31,102455.28 497997.83,103426.28 497905.36)'
        Then the following notification is displayed "Er kon geen eenduidig vaarwegtype worden vastgesteld voor de bron. De route die het beste lijkt te passen is 'CEMT_VIb'. De volgende andere typen zijn gevonden: CEMT_Va. Kies de juiste route als de voorgestelde niet klopt."
        Then I click on remove all notifications
        And I fill the inland shipping description 'Ship heading for docking', type 'Motorvrachtschip - M1  (Spits)'
        And I fill the movement 'A to B' with ships movements '12', time unit '/jaar' and percentage laden '55'
        And I fill the movement 'B to A' with ships movements '12', time unit '/jaar' and percentage laden '11'
        And I save the data and wait for refresh
        And I set the calculation year to '2030'
        And I save the scenario updates
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Scheepvaart' and sector 'Binnenvaart: Vaarroute' is correctly saved
        Then waterway 'CEMT_VIb' with direction 'Irrelevant' is correctly saved
        And location with coordinates 'X:101575,19 Y:498081,86' is correctly saved
        Then inland 'Standard' shipping row '0' with type 'Motorvrachtschip - M1  (Spits)' is correctly saved
        Then movement row '0' with direction 'A to B', description 'Ship heading for docking', ships movements '12', time unit '/jaar' and percentage laden '55,0 %' is correctly saved
        And movement row '0' with direction 'B to A', description 'Ship heading for docking', ships movements '12', time unit '/jaar' and percentage laden '11,0 %' is correctly saved
        And the 'standard inland shipping' row '0' has an NOx emission of '5,0 kg/j' and an NH3 emission of '0,0 kg/j'
        And the total 'inland shipping' emission 'NOx' is '5,0 kg/j'
        And the total 'inland shipping' emission 'NH3' is '0,0 kg/j'

    Scenario: Test waterway options for inland shipping route
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Scheepvaart' and sector 'Binnenvaart: Vaarroute'
        And I fill location with coordinates 'LINESTRING(99727.25 497872.33,100995.49 498063.89,101847.59 498090.31,102455.28 497997.83,103426.28 497905.36)'
        And the following notification is displayed "Er kon geen eenduidig vaarwegtype worden vastgesteld voor de bron. De route die het beste lijkt te passen is 'CEMT_VIb'. De volgende andere typen zijn gevonden: CEMT_Va. Kies de juiste route als de voorgestelde niet klopt."
        When I open the shipping subsource panel
        Then the waterway direction option should not be visible
        When I select waterway 'CEMT_0'
        Then the waterway direction option should not be visible
        When I select waterway 'IJssel'
        Then the waterway direction option should be visible
        When I select waterway 'CEMT_VIb'
        Then the waterway direction option should not be visible

    Scenario: Linking maritime inland route to dock A
        Given I login with correct username and password
        # new source dock 1
        And I create a new source from the start page
        And I name the source 'Aanlegplaats 1' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Aanlegplaats'
        And I fill location with coordinates 'POINT(99719.42 497860.02)'
        And I fill the maritime shipping description 'Ship heading for docking 1', type 'Passagiersschepen GT: 100-1599', visits '240', time unit '/jaar', average residence time '12' hours and shorepower usage '12.0 %' percent
        And I save the data and wait for refresh
        And I set the calculation year to '2030'
        And I save the scenario updates
        # new source dock 2
        And I create a new source from the source page
        And I name the source 'Aanlegplaats 2' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Aanlegplaats'
        And I fill location with coordinates 'POINT(103444.54 497912.19)'
        And I fill the maritime shipping description 'Ship heading for docking 2', type 'Passagiersschepen GT: 100-1599', visits '240', time unit '/jaar', average residence time '12' hours and shorepower usage '12' percent
        And I save the data and wait for refresh
        # new source inland route
        And I create a new source from the source page
        And I name the source 'Binnengaats route' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Binnengaats route'
        And I fill location with coordinates 'LINESTRING(99727.25 497872.33,100995.49 498063.89,101847.59 498090.31,102455.28 497997.83,103426.28 497905.36)'
        # linking dock A
        And I fill dock A with 'Aanlegplaats 1'
        And I fill the maritime shipping description 'Ship inland', type 'Passagiersschepen GT: 100-1599', visits '242' and time unit '/jaar'
        And I save the data and wait for refresh
        Then I select the source 'Binnengaats route'
        Then the source 'Binnengaats route' with sector group 'Scheepvaart' and sector 'Zeescheepvaart: Binnengaats route' is correctly saved
        And location with coordinates 'X:101575,19 Y:498081,86' is correctly saved
        And length of location with value '3.725,21 m' is correctly saved
        Then dock A with value 'Aanlegplaats 1' is correctly saved
        And maritime 'Standard' shipping row '0' with description 'Ship inland', type 'Passagiersschepen GT: 100-1599', visits '242', time unit '/jaar', emission NOX '383,4 kg/j' and emission NH3 '0,0 kg/j' is correctly saved
        And the total 'maritime shipping' emission 'NOx' is '383,4 kg/j'
        And the total 'maritime shipping' emission 'NH3' is '0,0 kg/j'
        And the total 'scenario' emission 'NOx' is '1.042,2 kg/j'
        And the total 'scenario' emission 'NH3' is '0,0 kg/j'

    Scenario: Linking maritime inland route to dock B
        Given I login with correct username and password
        # new source dock 1
        And I create a new source from the start page
        And I name the source 'Aanlegplaats 1' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Aanlegplaats'
        And I fill location with coordinates 'POINT(99719.42 497860.02)'
        And I fill the maritime shipping description 'Ship heading for docking 1', type 'Passagiersschepen GT: 100-1599', visits '240', time unit '/jaar', average residence time '12' hours and shorepower usage '12.0 %' percent
        And I save the data and wait for refresh
        And I set the calculation year to '2030'
        And I save the scenario updates
        # new source dock 2
        And I create a new source from the source page
        And I name the source 'Aanlegplaats 2' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Aanlegplaats'
        And I fill location with coordinates 'POINT(103444.54 497912.19)'
        And I fill the maritime shipping description 'Ship heading for docking 2', type 'Passagiersschepen GT: 100-1599', visits '240', time unit '/jaar', average residence time '12' hours and shorepower usage '12.0 %' percent
        And I save the data and wait for refresh
        # new source inland route
        And I create a new source from the source page
        And I name the source 'Binnengaats route' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Binnengaats route'
        And I fill location with coordinates 'LINESTRING(99727.25 497872.33,100995.49 498063.89,101847.59 498090.31,102455.28 497997.83,103426.28 497905.36)'
        # linking dock B
        And I fill dock B with 'Aanlegplaats 2'
        And I fill the maritime shipping description 'Ship inland', type 'Passagiersschepen GT: 100-1599', visits '242' and time unit '/jaar'
        And I save the data and wait for refresh
        Then I select the source 'Binnengaats route'
        Then the source 'Binnengaats route' with sector group 'Scheepvaart' and sector 'Zeescheepvaart: Binnengaats route' is correctly saved
        And location with coordinates 'X:101575,19 Y:498081,86' is correctly saved
        And length of location with value '3.725,21 m' is correctly saved
        Then dock B with value 'Aanlegplaats 2' is correctly saved
        And maritime 'Standard' shipping row '0' with description 'Ship inland', type 'Passagiersschepen GT: 100-1599', visits '242', time unit '/jaar', emission NOX '383,4 kg/j' and emission NH3 '0,0 kg/j' is correctly saved
        And the total 'maritime shipping' emission 'NOx' is '383,4 kg/j'
        And the total 'maritime shipping' emission 'NH3' is '0,0 kg/j'
        And the total 'scenario' emission 'NOx' is '1.042,2 kg/j'
        And the total 'scenario' emission 'NH3' is '0,0 kg/j'

    Scenario: Linking maritime inland route to dock A and dock B
        Given I login with correct username and password
        # new source dock 1
        And I create a new source from the start page
        And I name the source 'Aanlegplaats 1' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Aanlegplaats'
        And I fill location with coordinates 'POINT(99719.42 497860.02)'
        And I fill the maritime shipping description 'Ship heading for docking 1', type 'Passagiersschepen GT: 100-1599', visits '240', time unit '/jaar', average residence time '12' hours and shorepower usage '12.0 %' percent
        And I save the data and wait for refresh
        And I set the calculation year to '2030'
        And I save the scenario updates
        # new source dock 2
        And I create a new source from the source page
        And I name the source 'Aanlegplaats 2' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Aanlegplaats'
        And I fill location with coordinates 'POINT(103444.54 497912.19)'
        And I fill the maritime shipping description 'Ship heading for docking 2', type 'Passagiersschepen GT: 100-1599', visits '240', time unit '/jaar', average residence time '12' hours and shorepower usage '12.0 %' percent
        And I save the data and wait for refresh
        # new source inland route
        And I create a new source from the source page
        And I name the source 'Binnengaats route' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Binnengaats route'
        And I fill location with coordinates 'LINESTRING(99727.25 497872.33,100995.49 498063.89,101847.59 498090.31,102455.28 497997.83,103426.28 497905.36)'
        # linking dock A and B
        And I fill dock A with 'Aanlegplaats 1'
        And I fill dock B with 'Aanlegplaats 2'
        And I fill the maritime shipping description 'Ship inland', type 'Passagiersschepen GT: 100-1599', visits '242' and time unit '/jaar'
        And I save the data and wait for refresh
        Then I select the source 'Binnengaats route'
        Then the source 'Binnengaats route' with sector group 'Scheepvaart' and sector 'Zeescheepvaart: Binnengaats route' is correctly saved
        And location with coordinates 'X:101575,19 Y:498081,86' is correctly saved
        And length of location with value '3.725,21 m' is correctly saved
        Then dock A with value 'Aanlegplaats 1' is correctly saved
        Then dock B with value 'Aanlegplaats 2' is correctly saved
        And maritime 'Standard' shipping row '0' with description 'Ship inland', type 'Passagiersschepen GT: 100-1599', visits '242', time unit '/jaar', emission NOX '383,4 kg/j' and emission NH3 '0,0 kg/j' is correctly saved
        And the total 'maritime shipping' emission 'NOx' is '383,4 kg/j'
        And the total 'maritime shipping' emission 'NH3' is '0,0 kg/j'
        And the total 'scenario' emission 'NOx' is '1.042,2 kg/j'
        And the total 'scenario' emission 'NH3' is '0,0 kg/j'

    Scenario: Delete a linked dock will delete the link between dock and route
        Given I login with correct username and password
        # new source dock 1
        And I create a new source from the start page
        And I name the source 'Aanlegplaats 1' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Aanlegplaats'
        And I fill location with coordinates 'POINT(99719.42 497860.02)'
        And I fill the maritime shipping description 'Ship heading for docking 1', type 'Passagiersschepen GT: 100-1599', visits '240', time unit '/jaar', average residence time '12' hours and shorepower usage '12.0 %' percent
        And I save the data and wait for refresh
        And I set the calculation year to '2030'
        And I save the scenario updates
        # new source dock 2
        And I create a new source from the source page
        And I name the source 'Aanlegplaats 2' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Aanlegplaats'
        And I fill location with coordinates 'POINT(103444.54 497912.19)'
        And I fill the maritime shipping description 'Ship heading for docking 2', type 'Passagiersschepen GT: 100-1599', visits '240', time unit '/jaar', average residence time '12' hours and shorepower usage '12.0 %' percent
        And I save the data and wait for refresh
        # new source inland route
        And I create a new source from the source page
        And I name the source 'Binnengaats route' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Binnengaats route'
        And I fill location with coordinates 'LINESTRING(99727.25 497872.33,100995.49 498063.89,101847.59 498090.31,102455.28 497997.83,103426.28 497905.36)'
        # linking dock A and B        
        And I fill dock A with 'Aanlegplaats 1'
        And I fill dock B with 'Aanlegplaats 2'
        And I fill the maritime shipping description 'Ship inland', type 'Passagiersschepen GT: 100-1599', visits '242' and time unit '/jaar'
        And I save the data and wait for refresh
        When I delete source 'Aanlegplaats 1'
        # Warning is displayed
        Then the following warning is displayed: 'Deze aanlegplaats is gekoppeld aan de volgende vaarroute(s): Binnengaats route. Door deze aanlegplaats te verwijderen, verdwijnt de koppeling.'
        When I select the source 'Binnengaats route'
        Then the source 'Binnengaats route' with sector group 'Scheepvaart' and sector 'Zeescheepvaart: Binnengaats route' is correctly saved
        And location with coordinates 'X:101575,19 Y:498081,86' is correctly saved
        And length of location with value '3.725,21 m' is correctly saved
        Then field 'dock A' is not visible
        Then dock B with value 'Aanlegplaats 2' is correctly saved
        And maritime 'Standard' shipping row '0' with description 'Ship inland', type 'Passagiersschepen GT: 100-1599', visits '242', time unit '/jaar', emission NOX '383,4 kg/j' and emission NH3 '0,0 kg/j' is correctly saved
        And the total 'maritime shipping' emission 'NOx' is '383,4 kg/j'
        And the total 'maritime shipping' emission 'NH3' is '0,0 kg/j'
        And the total 'scenario' emission 'NOx' is '712,8 kg/j'
        And the total 'scenario' emission 'NH3' is '0,0 kg/j'

    @Smoke
    Scenario: Edit link between route and dock
        Given I login with correct username and password
        # new source dock 1
        And I create a new source from the start page
        And I name the source 'Aanlegplaats 1' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Aanlegplaats'
        And I fill location with coordinates 'POINT(99719.42 497860.02)'
        And I fill the maritime shipping description 'Ship heading for docking 1', type 'Passagiersschepen GT: 100-1599', visits '240', time unit '/jaar', average residence time '12' hours and shorepower usage '12.0 %' percent
        And I save the data and wait for refresh
        And I set the calculation year to '2030'
        And I save the scenario updates
        And the total 'scenario' emission 'NOx' is '329,4 kg/j'
        And the total 'scenario' emission 'NH3' is '0,0 kg/j'

        # new source dock 2
        And I create a new source from the source page
        And I name the source 'Aanlegplaats 2' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Aanlegplaats'
        And I fill location with coordinates 'POINT(103444.54 497912.19)'
        And I fill the maritime shipping description 'Ship heading for docking 2', type 'Passagiersschepen GT: 100-1599', visits '240', time unit '/jaar', average residence time '12' hours and shorepower usage '12.0 %' percent
        And I save the data and wait for refresh
        And the total 'scenario' emission 'NOx' is '658,8 kg/j'
        And the total 'scenario' emission 'NH3' is '0,0 kg/j'

        # new source dock 3
        And I create a new source from the source page
        And I name the source 'Aanlegplaats 3' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Aanlegplaats'
        And I fill location with coordinates 'POINT(103209.13 498378.92)'
        And I fill the maritime shipping description 'Ship heading for docking 3', type 'Passagiersschepen GT: 100-1599', visits '240', time unit '/jaar', average residence time '12' hours and shorepower usage '12.0 %' percent
        And I save the data and wait for refresh
        And the total 'scenario' emission 'NOx' is '988,2 kg/j'
        And the total 'scenario' emission 'NH3' is '0,0 kg/j'

        # new source inland route
        And I create a new source from the source page
        And I name the source 'Binnengaats route' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Binnengaats route'
        And I fill location with coordinates 'LINESTRING(99727.25 497872.33,100995.49 498063.89,101847.59 498090.31,102455.28 497997.83,103426.28 497905.36)'
        # linking dock A and B
        And I fill dock A with 'Aanlegplaats 1'
        And I fill dock B with 'Aanlegplaats 2'
        And I fill the maritime shipping description 'Ship inland', type 'Passagiersschepen GT: 100-1599', visits '242' and time unit '/jaar'
        And I save the data and wait for refresh
        And the total 'scenario' emission 'NOx' is '1.371,6 kg/j'
        And the total 'scenario' emission 'NH3' is '0,0 kg/j'

        # linking dock A to another dock
        When I select the source 'Binnengaats route'
        And I choose to edit the source
        And I fill dock A with 'Aanlegplaats 3'
        And I fill dock B with 'Geen'
        And I save the data and wait for refresh
        And the total 'scenario' emission 'NOx' is '1.371,6 kg/j'
        And the total 'scenario' emission 'NH3' is '0,0 kg/j'

        Then the source 'Binnengaats route' with sector group 'Scheepvaart' and sector 'Zeescheepvaart: Binnengaats route' is correctly saved
        And location with coordinates 'X:101575,19 Y:498081,86' is correctly saved
        And length of location with value '3.725,21 m' is correctly saved
        Then dock A with value 'Aanlegplaats 3' is correctly saved
        Then field 'dock B' is not visible
        And maritime 'Standard' shipping row '0' with description 'Ship inland', type 'Passagiersschepen GT: 100-1599', visits '242', time unit '/jaar', emission NOX '383,4 kg/j' and emission NH3 '0,0 kg/j' is correctly saved
        And the total 'maritime shipping' emission 'NOx' is '383,4 kg/j'
        And the total 'maritime shipping' emission 'NH3' is '0,0 kg/j'
