#
# Crown Copyright
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Map layers

    Scenario: Total %CL and %CL maplayers should not exist
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select the mapbutton 'LayerPanel'
        Then the 'Total percentage critical load' maplayer should not exist
        Then the 'Percentage critical level' maplayer should not exist

    Scenario: Validate maps
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select the mapbutton 'LayerPanel'
        Then I validate the map layers
            | id                                                                                                                        | title                                               |
            | nl.overheid.aerius.wui.application.geo.layers.results.CalculationMarkersLayer                                             | Markers                                  |
            | nl.overheid.aerius.wui.application.geo.layers.results.CalculationResultLayerGroup                                         | Rekenresultaten                          |
            | nl.overheid.aerius.wui.application.geo.layers.calculationpoint.CalculationPointLayerGroup                                 | Invoer - rekenpunten                     |
            | nl.overheid.aerius.wui.application.geo.layers.source.RoadSourceGeometryLayer                                              | Verkeersnetwerk                          |
            | nl.overheid.aerius.wui.application.geo.layers.job.JobInputLayerGroup                                                      | Invoer - rekentaak                       |
            | nl.overheid.aerius.wui.application.geo.layers.source.SourceLayerGroup                                                     | Invoer - situatie                        |
            | nl.overheid.aerius.wui.application.geo.layers.BackgroundResultLayer                                                       | Achtergrond                              |
            | calculator:wms_inland_shipping_routes                                                                                     | Binnenvaart netwerk                      |
            | calculator:wms_shipping_maritime_network_view                                                                             | Scheepvaart netwerk                      |
            | calculator:wms_habitat_types                                                                                              | Habitattypen: Relevante habitattypen     |
            | calculator:wms_habitat_areas_sensitivity_view                                                                             | Stikstofgevoeligheid: Relevante habitats |
            | verkeersscheidingsstelsel_nz:begrenzing,verkeersscheidingsstelsel_nz:symbolen,verkeersscheidingsstelsel_nz:separatiezones | Zeescheepvaart netwerk                   |
            | BU.Building                                                                                                               | BAG                                      |
            | nl.overheid.aerius.wui.application.geo.layers.NatureAreasLayer                                                            | Natuurgebieden                           |
            | nl.overheid.aerius.wui.application.geo.layers.zone.CalculationBoundaryLayer-title                                         | Geografische rekengrondslag              |
            | calculator:wms_province_areas_view                                                                                        | Provinciegrenzen                         |
            | grijs                                                                                                                     | Achtergrondkaart                         |
        Then I validate the habitats type layers
            | id                                                                           | title                                    |
            | calculator:wms_habitat_types                                                 | Habitattypen: Relevante habitattypen     |
            | calculator:wms_extra_assessment_hexagons_view                                | Habitattypen: Habitats met hersteldoelen |
            | nl.overheid.aerius.wui.application.geo.layers.habitats.HabitatTypeLayerGroup | Habitattypen: Alle                       |
        Then I validate the habitats areas sensitivity layers 
            | id                                                                                   | title                                            |
            | calculator:wms_habitat_areas_sensitivity_view                                        | Stikstofgevoeligheid: Relevante habitats         |
            | calculator:wms_extra_assessment_hexagons_sensitivity_view                            | Stikstofgevoeligheid: Habitats met hersteldoelen |
            | nl.overheid.aerius.wui.application.geo.layers.habitats.NitrogenSensitivityLayerGroup | Stikstofgevoeligheid: Alle                       |
        Then I validate the base layers
            | id              | title                     | copyright        |
            | grijs           | Achtergrondkaart          | © OSM & Kadaster |
            | pastel          | Achtergrondkaart (Pastel) | © OSM & Kadaster |
            | standaard       | Achtergrondkaart (Kleur)  | © OSM & Kadaster |
            | water           | Achtergrondkaart (Water)  | © OSM & Kadaster |
            | Actueel_orthoHR | Luchtfoto (PDOK)          |                  |

    Scenario: Test default map layers for each menu item that has a map
        Given I login with correct username and password

        # Import file with results, so we can test the layers for the result panel as well
        When I select file 'AERIUS_calculation_points_results.gml' to be imported
        And I select the mapbutton 'LayerPanel'
        Then the following maplayers should be visible
            | layer                                       | shouldBeEnabled |
            | Markers                                     | no              |
            | Rekenresultaten                             | no              |
            | Invoer - rekenpunten                        | no              |
            | Verkeersnetwerk                             | yes             |
            | Invoer - rekentaak                          | no              |
            | Invoer - situatie                           | yes             |
            | Achtergrond                                 | no              |
            | Binnenvaart netwerk                         | no              |
            | Scheepvaart netwerk                         | no              |
            | Zeescheepvaart netwerk                      | no              |
            | Habitattypen: Relevante habitattypen        | no              |
            | Stikstofgevoeligheid: Relevante habitats    | no              |
            | BAG                                         | no              |
            | Natuurgebieden                              | yes             |
            | Geografische rekengrondslag                 | yes             |
            | Provinciegrenzen                            | yes             |

        When I navigate to the 'Assessment points' tab
        Then the following maplayers should be visible
            | layer                                       | shouldBeEnabled |
            | Markers                                     | no              |
            | Rekenresultaten                             | no              |
            | Invoer - rekenpunten                        | yes             |
            | Verkeersnetwerk                             | no              |
            | Invoer - rekentaak                          | no              |
            | Invoer - situatie                           | no              |
            | Achtergrond                                 | no              |
            | Binnenvaart netwerk                         | no              |
            | Scheepvaart netwerk                         | no              |
            | Zeescheepvaart netwerk                      | no              |
            | Habitattypen: Relevante habitattypen        | no              |
            | Stikstofgevoeligheid: Relevante habitats    | no              |
            | BAG                                         | no              |
            | Natuurgebieden                              | yes             |
            | Geografische rekengrondslag                 | yes             |
            | Provinciegrenzen                            | yes             |

        When I navigate to the 'Results' tab
        Then the following maplayers should be visible
            | layer                                       | shouldBeEnabled |
            | Markers                                     | no              |
            | Rekenresultaten                             | yes             |
            | Invoer - rekenpunten                        | no              |
            | Verkeersnetwerk                             | no              |
            | Invoer - rekentaak                          | yes             |
            | Invoer - situatie                           | no              |
            | Achtergrond                                 | no              |
            | Binnenvaart netwerk                         | no              |
            | Scheepvaart netwerk                         | no              |
            | Zeescheepvaart netwerk                      | no              |
            | Habitattypen: Relevante habitattypen        | no              |
            | Stikstofgevoeligheid: Relevante habitats    | no              |
            | BAG                                         | no              |
            | Natuurgebieden                              | yes             |
            | Geografische rekengrondslag                 | yes             |
            | Provinciegrenzen                            | yes             |

        # Validate turning off the 'Automatically turn layers on or off' preference stops the maplayers from updating
        When I navigate to the 'Preferences' tab
        And I change the setting for 'Automatically turning layers on or off' to 'off'
        And I navigate to the 'Input' tab
        Then the following maplayers should be visible
            | layer                                       | shouldBeEnabled |
            | Markers                                     | no              |
            | Rekenresultaten                             | yes             |
            | Invoer - rekenpunten                        | no              |
            | Verkeersnetwerk                             | no              |
            | Invoer - rekentaak                          | yes             |
            | Invoer - situatie                           | no              |
            | Achtergrond                                 | no              |
            | Binnenvaart netwerk                         | no              |
            | Scheepvaart netwerk                         | no              |
            | Zeescheepvaart netwerk                      | no              |
            | Habitattypen: Relevante habitattypen        | no              |
            | Stikstofgevoeligheid: Relevante habitats    | no              |
            | BAG                                         | no              |
            | Natuurgebieden                              | yes             |
            | Geografische rekengrondslag                 | yes             |
            | Provinciegrenzen                            | yes             |

    Scenario: Check map layer legend values
        Given I login with correct username and password

        # Import file with results so the `Rekenresultaten` maplayer has a legend
        When I select file 'AERIUS_calculation_points_results.gml' to be imported
        And I select the mapbutton 'LayerPanel'

        # Default rekenresultaten legend
        When I open maplayer 'Rekenresultaten'
        Then I expect the map layer 'Rekenresultaten' to have an empty legenda
        
        # Difference rekenresultaten legend
        When I navigate to the 'Results' tab
        And I wait for the results spinner to resolve
        Then I expect the map layer 'Rekenresultaten' to have title 'Depositie (mol/ha/jr)' with the following values
            | value           |
            | < -20           |
            | -20 tot -10     |
            | -10 tot -5      |
            | -5 tot -2.86    |
            | -2.86 tot -1.07 |
            | -1.07 tot 0     |
            | 0 tot 1.07      |
            | 1.07 tot 2.86   |
            | 2.86 tot 5      |
            | 5 tot 10        |
            | 10 tot 20       |
            | ≥ 20            |

        # Verkeersnetwerk legends
        When I open maplayer 'Verkeersnetwerk'
        Then I expect the map layer 'Verkeersnetwerk' for data type 'Wegtypering' to have the following values
            | value                              |
            | Snelweg                            |
            | Buitenweg                          |
            | Binnen bebouwde kom (normaal)      |
            | Binnen bebouwde kom (stagnerend)   |
            | Binnen bebouwde kom (doorstromend) |
            | Onbekend                           |

        And I expect the map layer 'Verkeersnetwerk' for data type 'Maximumsnelheid' to have the following values
            | value              |
            | 80 km/uur          |
            | 100 km/uur         |
            | 120 km/uur         |
            | 130 km/uur         |
            | Onbekend           |
            | Strikte handhaving |

        And I expect the map layer 'Verkeersnetwerk' for data type 'Verkeersintensiteit' and 'Totaal aantal voertuigbewegingen' to have title 'Aantal voertuigbewegingen/etmaal' and the following values
            | value           |
            | 0 tot 10000     |
            | 10000 tot 20000 |
            | 20000 tot 30000 |
            | 30000 tot 50000 |
            | ≥ 50000         |
        And I expect the map layer 'Verkeersnetwerk' for data type 'Verkeersintensiteit' and 'Licht verkeer' to have title 'Aantal voertuigbewegingen/etmaal' and the following values
            | value           |
            | 0 tot 10000     |
            | 10000 tot 20000 |
            | 20000 tot 30000 |
            | 30000 tot 50000 |
            | ≥ 50000         |
        And I expect the map layer 'Verkeersnetwerk' for data type 'Verkeersintensiteit' and 'Middelzwaar vrachtverkeer' to have title 'Aantal voertuigbewegingen/etmaal' and the following values
            | value           |
            | 0 tot 10000     |
            | 10000 tot 20000 |
            | 20000 tot 30000 |
            | 30000 tot 50000 |
            | ≥ 50000         |
        And I expect the map layer 'Verkeersnetwerk' for data type 'Verkeersintensiteit' and 'Zwaar vrachtverkeer' to have title 'Aantal voertuigbewegingen/etmaal' and the following values
            | value           |
            | 0 tot 10000     |
            | 10000 tot 20000 |
            | 20000 tot 30000 |
            | 30000 tot 50000 |
            | ≥ 50000         |
        And I expect the map layer 'Verkeersnetwerk' for data type 'Verkeersintensiteit' and 'Busverkeer' to have title 'Aantal voertuigbewegingen/etmaal' and the following values
            | value           |
            | 0 tot 10000     |
            | 10000 tot 20000 |
            | 20000 tot 30000 |
            | 30000 tot 50000 |
            | ≥ 50000         |

        And I expect the map layer 'Verkeersnetwerk' for data type 'Congestie' and 'Licht verkeer' to have the following values
            | value      |
            | 0%         |
            | < 10%      |
            | 10 tot 25% |
            | 25 tot 50% |
            | ≥ 50%      |
        And I expect the map layer 'Verkeersnetwerk' for data type 'Congestie' and 'Middelzwaar vrachtverkeer' to have the following values
            | value      |
            | 0%         |
            | < 10%      |
            | 10 tot 25% |
            | 25 tot 50% |
            | ≥ 50%      |
        And I expect the map layer 'Verkeersnetwerk' for data type 'Congestie' and 'Zwaar vrachtverkeer' to have the following values
            | value      |
            | 0%         |
            | < 10%      |
            | 10 tot 25% |
            | 25 tot 50% |
            | ≥ 50%      |
        And I expect the map layer 'Verkeersnetwerk' for data type 'Congestie' and 'Busverkeer' to have the following values
            | value      |
            | 0%         |
            | < 10%      |
            | 10 tot 25% |
            | 25 tot 50% |
            | ≥ 50%      |

        And I expect the map layer 'Verkeersnetwerk' for data type 'Afschermende constructies' and 'Type' to have the following values
            | value           |
            | Scherm          |
            | Wal             |
            | Verkeersnetwerk |
        And I expect the map layer 'Verkeersnetwerk' for data type 'Afschermende constructies' and 'Hoogte' to have title 'm' and the following values
            | value           |
            | 0 tot 3         |
            | 3 tot 6         |
            | ≥ 6             |
            | Verkeersnetwerk |

        And I expect the map layer 'Verkeersnetwerk' for data type 'Type hoogteligging' to have the following values
            | value        |
            | Normaal      |
            | Normale dijk |
            | Steile dijk  |
            | Viaduct      |
            | Tunnelbak    |
            | Onbekend     |

        And I expect the map layer 'Verkeersnetwerk' for data type 'Weghoogte t.o.v. maaiveld' to have the following values
            | value                           |
            | Verdiepte ligging (< 0 m)       |
            | Maaiveld (0 m)                  |
            | Verhoogde ligging (> 0 tot 3 m) |
            | Verhoogde ligging (3 tot 6 m)   |
            | Verhoogde ligging (≥ 6 m)       |

        And I expect the map layer 'Verkeersnetwerk' for data type 'Tunnelfactor' to have the following values
            | value            |
            | Tunnelfactor < 1 |
            | Tunnelfactor = 1 |
            | Tunnelfactor > 1 |

        # Achtergrond legend
        When I open maplayer 'Achtergrond'
        Then I expect the map layer 'Achtergrond' to have title 'Depositie (mol/ha/jr)' with the following values
            | value                 |
            | 0,00 tot 700,00       |
            | 700,00 tot 980,00     |
            | 980,00 tot 1.260,00   |
            | 1.260,00 tot 1.540,00 |
            | 1.540,00 tot 1.960,00 |
            | 1.960,00 tot 2.240,00 |
            | ≥ 2.240,00            |

        # Stikstofgevoeligheid relevante habitattypen' legend
        When I open maplayer 'Stikstofgevoeligheid: Relevante habitats'
        Then I expect the map layer 'Stikstofgevoeligheid: Relevante habitats' to have the following values
            | value                |
            | Zeer gevoelig        |
            | Gevoelig             |
            | Minder/niet gevoelig |

        # Natuurgebieden legend
        When I open maplayer 'Natuurgebieden'
        Then I expect the map layer 'Natuurgebieden' to have the following values
            | value                            |
            | Habitatrichtlijn                 |
            | Vogelrichtlijn                   |
            | Vogelrichtlijn, Habitatrichtlijn |
            | Niet bepaald                     |
