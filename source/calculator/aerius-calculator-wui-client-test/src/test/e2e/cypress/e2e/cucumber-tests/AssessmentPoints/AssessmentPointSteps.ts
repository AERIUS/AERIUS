/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";

import { AssessmentPointsPage } from "./AssessmentPointsPage";

Given("I should still be in the new assessment point screen", () => {
  AssessmentPointsPage.assertNewPointScreenShown();
});

Given("I create a new calculation point", () => {
  AssessmentPointsPage.pressCreateCalculationPointButton();
});

Given("I open the panel for generating calculation points", () => {
  AssessmentPointsPage.openGenerateCalculationPointsPanel();
});

Given("I enable the generation of calculation points inside of the Netherlands", () => {
  AssessmentPointsPage.enableCalculationPointsInsideNL();
});

Given("I disable the generation of calculation points for assessment areas abroad", () => {
  AssessmentPointsPage.disableCalculationPointsAbroad();
});

Given("I set the radius to {string} meters", (radiusInMeters) => {
  AssessmentPointsPage.setRadiusTo(radiusInMeters);
});

Given("I expect to see a radius error", () => {
  AssessmentPointsPage.assertRadiusErrorIsShown();
});

Given("I expect to see no radius error", () => {
  AssessmentPointsPage.assertNoRadiusErrorIsShown();
});

Given("I cancel the creation the generation of calculation points", () => {
  AssessmentPointsPage.cancelGenerationOfCalculationPoints();
});

Given("I determine the generated calculation points", () => {
  AssessmentPointsPage.determineGeneratedCalculationPoints();
});

Given("I save the generated calculation points", () => {
  AssessmentPointsPage.saveGeneratedCalculationPoints();
});

Given("no calculation points should've been generated", () => {
  AssessmentPointsPage.assertNoCalculationPointsHaveBeenGenerated();
});

Given("calculation points should've been generated", () => {
  AssessmentPointsPage.assertCalculationPointsHaveBeenGenerated();
});

Given("I select the option to automatically generate points again", () => {
  AssessmentPointsPage.generatePointsAgain();
});

Given("I select the scenario {string} as scenario to generate assessment points for", (scenarioName) => {
  AssessmentPointsPage.selectScenario(scenarioName);
});
