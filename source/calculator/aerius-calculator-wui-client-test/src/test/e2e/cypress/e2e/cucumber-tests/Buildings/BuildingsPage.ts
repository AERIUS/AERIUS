/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class BuildingsPage {
  static assertPolygonalBuildingSavedAndSelectedForEmissionSource(name: string, length: string, width: string, height: string, orientation: string) {
    // Assert detail screen building is given building
    cy.get('[id="buildingDetailTitle"]').should("have.text", name);
    cy.get('[id="buildingLength"] > [id="minMaxLabel"]').should("have.text", length);
    cy.get('[id="buildingWidth"] > [id="minMaxLabel"]').should("have.text", width);
    cy.get('[id="buildingHeight"] > [id="minMaxLabel"]').should("have.text", height);
    cy.get('[id="buildingOrientation"]').should("have.text", orientation);
  }

  static deleteAllBuildings() {
    cy.get('[id="buildingListButtonDeleteAllBuildings"]').click();
  }

  static selectBuildingAsPrimaryBuilding(name: string) {
    cy.get('[id="buildingSelection"]').select(name);
  }

  static assertNoBuildingSelected() {
    cy.get(".buildings .row.selected").should("have.length", 0);

    cy.get('[id="building-sourceListButtonEditSource"]').should("have.attr", "disabled", "disabled");
    cy.get('[id="building-sourceListButtonDeleteSource"]').should("have.attr", "disabled", "disabled");
    cy.get('[id="building-sourceListButtonCopySource"]').should("have.attr", "disabled", "disabled");
  }

  static openBronkenmerkenPanel() {
    cy.get('[id="collapsibleOPSCharacteristics"]').click();
  }
}
