/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class RoadTransportationSourceColdStartPage {
  static timeUnitAndNumberOfColdStartsForEuroClassIsCorrectlySaved(timeUnit: string, numberOfColdStarts: string) {
    cy.get('[id="detailRoadSpecificVehicleAmount-0"]').should("have.text", numberOfColdStarts + " " + timeUnit);
  }

  static timeUnitAndNumberOfColdStartsForEuroClassOtherIsCorrectlySaved(timeUnit: string, numberOfColdStarts: string) {
    cy.get('[id="detailRoadCustomVehicleAmount-0"]').should("have.text", numberOfColdStarts + " " + timeUnit);
  }

  static selectVehicleTypeForCustomSpecification(vehicleType: string) {
    cy.get('[id="emissionSourceVehicleType"]').select(vehicleType);
  }

  static assertVehicleTypeCorrectlySaved(vehicleType: string) {
    cy.get('[id="label"]').should("have.text", vehicleType);
  }

  static emissionInputNO2ShouldNotEist() {
    cy.get('[id="emissionFactorInput_NO2"]').should("not.exist");
  }

  static detailEmissionSubtanceCount(substanceCount: number) {
    cy.get(".detail-emission-subcontainer").should("have.length", substanceCount);
  }

}
