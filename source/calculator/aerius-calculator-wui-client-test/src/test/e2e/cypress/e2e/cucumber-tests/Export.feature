#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Export
    Test how exporting files is handled.

    Scenario: Check if OPS option is added
        Given I login with correct username and password with OPS enabled
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data
        When I navigate to the 'Calculation jobs' tab
        And I create a new calculation job
        And I save the calculation job
        Then I navigate to the 'Export' tab
        When I select 'Rekentaak' as export type
        And I select the calculation job 'Rekentaak 1 (Projectberekening)'
        Then I see the option to export OPS input files

    Scenario: Export Sources GML
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose heat content type 'Niet geforceerd', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data
        When I choose to export through the menu
        And I select 'Invoerbestanden' as export type
        Then I validate if the following situations are checked
        | name                      | checked |
        | Situatie 1 - Beoogd       | true    |
        When I navigate to the 'Input' tab
        And I duplicate the situation
        When I choose to export through the menu
        And I validate if the following situations are checked
        | name                      | checked |
        | Situatie 1 - Beoogd       | true    |
        | Situatie 1 (1) - Beoogd   | false   |
        Then I select included situation 'Situatie 1 (1) - Beoogd'
        And I validate if the following situations are checked
        | name                      | checked |
        | Situatie 1 - Beoogd       | true    |
        | Situatie 1 (1) - Beoogd   | true    |
        Then I deselect included situation 'Situatie 1 (1) - Beoogd'
        And I validate if the following situations are checked
        | name                      | checked |
        | Situatie 1 - Beoogd       | true    |
        | Situatie 1 (1) - Beoogd   | false   |
        And I check if the field 'exportEmail' is empty    
        And I click on the button to export, expecting a warning
        And I expect to see the 'exportEmail_error'   
        And I fill in the following email address 'test@aerius.nl'
        And I click on the button to export
        Then the following notification is displayed 'Uw export is succesvol gestart. U ontvangt een e-mail met downloadlink van de bestanden wanneer deze klaar is.'
        Then a notification is displayed containing the text "Export '1 situaties'"
        And the button 'deleteButton' is visible
        And a button title is available containing the text 'Export annuleren en verwijderen'
        # TODO: test Id is missing for the counter button, please add it and add a test for it
        And a tooltip is available containing the text 'De rekentaak staat in de wachtrij; het getal geeft aan hoeveel rekentaken voor u in de wachtrij staan'
        Then I cancel the export
        Then the following notification is displayed "Uw export '1 situaties' is geannuleerd."
        And a notification containing the text "Export '1 situaties'" is not displayed            

    Scenario: Export Calculation GML
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose heat content type 'Niet geforceerd', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data
        When I choose to export through the menu
        And I select 'Rekentaak' as export type
        And I select create new calculation job
        When I create a new calculation job
        And I save the calculation job
        And I press the export button
        Then I select 'Rekentaak' as export type
        And I select additional info
        And I fill in the additional info form
        | inputField    | input                 |
        | Corporation   | Corporation X         |
        | ProjectName   | Project X             |
        | StreetAddress | Test street 123       |
        | Postcode      | postal code '1234 AB' |
        | City          | Testcity              |
        | Description   | xxx                   |
        And I check if the field 'exportEmail' is empty 
        And I fill in the following email address 'test@aerius.nl'
        And I click on the button to export, expecting a warning
        Then I expect to see the 'agreeDataInclusion_warning'  

        # Ignore the warning and export anyways without accepting the inclusion
        When I click on the button to export
        Then the following notification is displayed 'Uw export is succesvol gestart. U ontvangt een e-mail met downloadlink van de bestanden wanneer deze klaar is.'
        Then a notification is displayed containing the text "Export 'Rekentaak 1'"
        And the button 'deleteButton' is visible
        And a button title is available containing the text 'Export annuleren en verwijderen'        
        And a tooltip is available containing the text 'De rekentaak staat in de wachtrij; het getal geeft aan hoeveel rekentaken voor u in de wachtrij staan'
        Then I cancel the export
        Then the following notification is displayed "Uw export 'Rekentaak 1' is geannuleerd."
        And a notification containing the text "Export 'Rekentaak 1'" is not displayed    

    @Smoke
    Scenario: Export Calculation GML with OPS enabled
        Given I login with correct username and password with OPS enabled
        And I create a new source from the start page

        # Create source
        Then I name the source 'Bron 1' and select sector group 'Energie'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose heat content type 'Niet geforceerd', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data

        # Create calculation job
        When I navigate to the 'Calculation jobs' tab
        Then I create a new calculation job
        And I save the calculation job

        # Export
        When I navigate to the 'Export' tab
        Then I select 'Rekentaak' as export type
        And I select exporting of OPS input files
        And I select additional info
        And I fill in the additional info form
        | inputField    | input                 |
        | Corporation   | Corporation X         |
        | ProjectName   | Project X             |
        | StreetAddress | Test street 123       |
        | Postcode      | postal code '1234 AB' |
        | City          | Testcity              |
        | Description   | xxx                   |
        And I check if the field 'exportEmail' is empty 
        And I fill in the following email address 'test@aerius.nl'
        And I click on the button to export, expecting a warning
        Then I expect to see the 'agreeDataInclusion_warning'  

        # Accept the inclusion warning and export
        When I accept the data inclusion warning
        And I click on the button to export
        Then the following notification is displayed 'Uw export is succesvol gestart. U ontvangt een e-mail met downloadlink van de bestanden wanneer deze klaar is.'
        Then a notification is displayed containing the text "Export '1 situaties'"
        And the button 'deleteButton' is visible
        And a button title is available containing the text 'Export annuleren en verwijderen'
        And a tooltip is available containing the text 'De rekentaak staat in de wachtrij; het getal geeft aan hoeveel rekentaken voor u in de wachtrij staan'
        Then I cancel the export
        Then the following notification is displayed "Uw export '1 situaties' is geannuleerd."
        And a notification containing the text "Export '1 situaties'" is not displayed   

    Scenario: Export PDF
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose heat content type 'Niet geforceerd', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data
        When I choose to export through the menu
        And I select 'Rapportage' as export type        
        And I select create new calculation job
        When I create a new calculation job
        And I save the calculation job
        When I press the export button
        And I select additional info
        And I fill in the additional info form
        | inputField    | input                 |
        | Corporation   | Corporation X         |
        | ProjectName   | Project X             |
        | StreetAddress | Test street 123       |
        | Postcode      | postal code '1234 AB' |
        | City          | Testcity              |
        | Description   | xxx                   |
        And I check if the field 'exportEmail' is empty 
        And I select the acknowledge selection
        And I fill in the following email address 'test@aerius.nl'
        And I click on the button to export
        Then the following notification is displayed 'Uw export is succesvol gestart. U ontvangt een e-mail met downloadlink van de bestanden wanneer deze klaar is.'
        Then a notification is displayed containing the text "Export 'rapportage Rekentaak 1'"
        And the button 'deleteButton' is visible
        And a button title is available containing the text 'Export annuleren en verwijderen'
        And a tooltip is available containing the text 'De rekentaak staat in de wachtrij; het getal geeft aan hoeveel rekentaken voor u in de wachtrij staan'
        Then I cancel the export
        Then the following notification is displayed "Uw export 'rapportage Rekentaak 1' is geannuleerd."
        And a notification containing the text "Export 'rapportage Rekentaak 1'" is not displayed 

    @Smoke
    Scenario: Import situations with all different situation types and export all these situations
        Given I login with correct username and password
        And I enable the advanced importmodus
        When I select a file with multiple situations 'AERIUS_gml_many_situations.zip' to be imported in advanced mode
        And I choose to import the files
        When I choose to export through the menu
        And I fill in the following email address 'test@aerius.nl'
        And I click on the button to export
        Then the following notification is displayed 'Uw export is succesvol gestart. U ontvangt een e-mail met downloadlink van de bestanden wanneer deze klaar is.'
        Then a notification is displayed containing the text "Export '9 situaties'"
        And the button 'deleteButton' is visible
        And a button title is available containing the text 'Export annuleren en verwijderen'
        And a tooltip is available containing the text 'De rekentaak staat in de wachtrij; het getal geeft aan hoeveel rekentaken voor u in de wachtrij staan'           
        Then I cancel the export
        Then the following notification is displayed "Uw export '9 situaties' is geannuleerd."
        And a notification containing the text "Export '9 situaties'" is not displayed 
   
    Scenario: Start multiple exports and cancel the exports
        Given I login with correct username and password
        And I enable the advanced importmodus
        When I select a file with multiple situations 'AERIUS_gml_many_situations.zip' to be imported in advanced mode
        And I choose to import the files
        When I navigate to the 'Calculation jobs' tab
        Then I create a new calculation job
        And I save the calculation job
        When I press the export button
        And I select 'Invoerbestanden' as export type
        And I select included situation 'Beoogd'
        And I select included situation 'Referentie'
        And I select additional info optional
        And I fill in the additional info form
        | inputField    | input                 |
        | Corporation   | Corporation X         |
        | ProjectName   | Project X             |
        | StreetAddress | Test street 123       |
        | Postcode      | postal code '1234 AB' |
        | City          | Testcity              |
        | Description   | xxx                   |
        And I select the acknowledge selection
        And I check if the field 'exportEmail' is empty 
        And I fill in the following email address 'test@aerius.nl'
        And I click on the button to export

        Then the following notification is displayed 'Uw export is succesvol gestart. U ontvangt een e-mail met downloadlink van de bestanden wanneer deze klaar is.'
        Then a notification is displayed containing the text "Export '2 situaties'"
        # And the button 'deleteButton' is visible
        And calculation export has '1' rows
        And a tooltip is available containing the text 'De rekentaak staat in de wachtrij; het getal geeft aan hoeveel rekentaken voor u in de wachtrij staan'
        And a button title is available containing the text 'Export annuleren en verwijderen'
        When I cancel the calculation export "Export '2 situaties'"
        Then the following notification is displayed "Uw export '2 situaties' is geannuleerd."
        And a notification containing the text "Export '2 situaties'" is not displayed 
        Then calculation export has '0' rows
        When I select 'Rekentaak' as export type
        And I click on the button to export

        # And the button 'deleteButton' is visible
        Then calculation export has '1' rows
        And a tooltip is available containing the text 'De rekentaak staat in de wachtrij; het getal geeft aan hoeveel rekentaken voor u in de wachtrij staan'
        And a button title is available containing the text 'Export annuleren en verwijderen'
        When I cancel the calculation export "Export 'Rekentaak 1'"
        Then the following notification is displayed "Uw export 'Rekentaak 1' is geannuleerd."
        And a notification containing the text "Export 'Rekentaak 1'" is not displayed         
        Then calculation export has '0' rows
        When I select 'Rapportage' as export type
        And I click on the button to export

        # And the button 'deleteButton' is visible
        Then calculation export has '1' rows
        And a tooltip is available containing the text 'De rekentaak staat in de wachtrij; het getal geeft aan hoeveel rekentaken voor u in de wachtrij staan'
        And a button title is available containing the text 'Export annuleren en verwijderen'
        When I cancel the calculation export "Export 'rapportage Rekentaak 1'"
        Then the following notification is displayed "Uw export 'rapportage Rekentaak 1' is geannuleerd."
        And a notification containing the text "Export 'rapportage Rekentaak 1'" is not displayed          
        Then calculation export has '0' rows

        When I select 'Invoerbestanden' as export type
        And I click on the button to export

        When I select 'Rekentaak' as export type
        And I click on the button to export

        When I select 'Rapportage' as export type
        And I click on the button to export

        Then calculation export has '3' rows
        And I cancel the calculation export "Export '2 situaties'"
        And the following notification is displayed "Uw export '2 situaties' is geannuleerd."
        And a notification containing the text "Export '2 situaties'" is not displayed
        And calculation export has '2' rows   
        When I cancel the calculation export "Export 'Rekentaak 1'"
        Then the following notification is displayed "Uw export 'Rekentaak 1' is geannuleerd."
        And a notification containing the text "Export 'Rekentaak 1'" is not displayed
        And calculation export has '1' rows 
        When I cancel the calculation export "Export 'rapportage Rekentaak 1'"
        Then the following notification is displayed "Uw export 'Rekentaak 1' is geannuleerd."
        And a notification containing the text "Export 'Rekentaak 1'" is not displayed
        And calculation export has '0' rows

    Scenario: Validate the checkbox for the edgehexagons appendice
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose heat content type 'Niet geforceerd', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data
        And I duplicate the situation
        And I add situation label 'Situatie 2'
        And I select scenarioType 'REFERENCE'
        And I save the scenario updates
        And I navigate to the 'Calculation jobs' tab
        Then I create a new calculation job
        And I select the situation 'Situatie 2' as 'referenceSituation'
        And I save the calculation job
        When I press the export button
        And I select the appendices
        Then the appendices for edgehexagons is available
        And the appendices for edgehexagons is unchecked
        And I check the box for the edgehexagon appendice

        # validate that it automatically unchecks when you move away
        And the appendices for edgehexagons is checked
        And I navigate to the 'Calculation jobs' tab
        When I press the export button
        And I select the appendices
        And the appendices for edgehexagons is unchecked

        # validate that the panel disappears when the reference scenario is unchecked
        And I navigate to the 'Calculation jobs' tab
        And I edit the selected calculation job
        And I select the situation 'Geen' as 'referenceSituation'
        And I save the calculation job
        When I press the export button
        And I select the appendices
        Then the appendices for edgehexagons is unavailable     

    Scenario: Validate the flow from the calculationjob menu to the export menu 
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose heat content type 'Niet geforceerd', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data
        And I duplicate the situation
        And I add situation label 'Situatie 2'
        And I select scenarioType 'REFERENCE'
        And I save the scenario updates
        And I navigate to the 'Calculation jobs' tab
        Then I create a new calculation job
        And I save the calculation job
        And I create a new calculation job
        And I select the situation 'Situatie 2' as 'referenceSituation'
        And I save the calculation job
        When I press the export button
        Then I validate that the tab 'Rapportage' is selected
        And I validate that calculation job 'Rekentaak 2' is selected
        Then I navigate to the 'Calculation jobs' tab
        And I switch calculation job to calculation job '1'
        And I edit the selected calculation job
        And I select calculation jobType 'Enkele situatie'
        And I save the calculation job
        Then I press the export button
        Then I validate that the tab 'Rekentaak' is selected
        And I validate that calculation job 'Rekentaak 1' is selected

    @Smoke
    Scenario: Import file with in combination process contribution results export OPS settings and download file
        Given I login with correct username and password with OPS enabled
        When I select file 'AERIUS_everything_import_file.gml' to be imported
        Then the situation name is 'Situatie 1'
        And the situation type is 'Beoogd'
        And the calculation year is '2024'

        # Navigate to the calculation jobs tab first to generate a calculation job
        And I navigate to the 'Calculation jobs' tab
        Then I create a new calculation job
        And I save the calculation job
        And I navigate to the 'Export' tab
        Then I select 'Calculation job' as export type
        And I select the calculation job 'Rekentaak 1 (Projectberekening)'
        And I select exporting of OPS input files
        And I select additional info
        And I accept the data inclusion warning
        And I fill in the following email address 'test@aerius.nl'        

        And I click on the button to export
        Then the following notification is displayed 'Uw export is succesvol gestart. U ontvangt een e-mail met downloadlink van de bestanden wanneer deze klaar is.'
        And I wait till task is completed
        And I can extract downloaded results and validate it contains 39 files

    Scenario: Import file with in situation and reference export GML with edge hexagon report and validate downloaded files
        Given I login with correct username and password
        When I select file 'AERIUS_import_situation_and_reference.zip' to be imported
        And I navigate to the 'Calculation jobs' tab
        And I create a new calculation job
        And I select the situation 'Beoogd' as 'proposedSituation'
        And I select the situation 'Referentie' as 'referenceSituation'
        And I save the calculation job
        And I navigate to the 'Export' tab
        Then I select 'Report' as export type
        And I select the appendices
        And I check the box for the edgehexagon appendice
        And I select additional info
        And I accept the data inclusion warning
        And I fill in the following email address 'test@aerius.nl'

        And I click on the button to export
        Then the following notification is displayed 'Uw export is succesvol gestart. U ontvangt een e-mail met downloadlink van de bestanden wanneer deze klaar is.'
        And I wait till task is completed
        And I can extract downloaded results and validate it contains 3 files

    Scenario: Import PDF with situation and custom calculation points export as report and reimport the downloaded report and validate
        Given I login with correct username and password
        When I select file 'AERIUS_calculation_points_results.pdf' to be imported
        And I navigate to the 'Export' tab
        Then I select 'Report' as export type
        And I select additional info
        And I accept the data inclusion warning
        And I fill in the following email address 'test@aerius.nl'
        And I click on the button to export
        Then the following notification is displayed 'Uw export is succesvol gestart. U ontvangt een e-mail met downloadlink van de bestanden wanneer deze klaar is.'
        And I wait till task is completed
        And I can extract downloaded results and validate it contains 2 files
        When I reload the calculator application
        And I select last downloaded results to be imported

        Then the following notification is displayed 'Rekentaak 1 - result is gestart.'
        And the following notification is displayed 'Rekentaak 1 - result is voltooid.'

        And I navigate to the 'Assessment points' tab
        Then the overview list has 69 assessment points
        And I navigate to the 'Results' tab
        Then I select situation with label 'Situatie 1 - Beoogd' and validate the 'Projectberekening' result screen with results for the result 'OwN2000-registratieset'
        | elementId                      | expectedResult |
        | sumCartographicSurface         |         448,79 |
        | maxTotal                       |       2.252,08 |
        | sumCartographicSurfaceIncrease |         448,79 |
        | maxIncrease                    |           0,99 |
        | sumCartographicSurfaceDecrease |           0,00 |
        | maxDecrease                    | -              |
        
        When I open the notification center
        And I click on remove all notifications

        # Start calculation with custom points and validate results
        When I navigate to the 'Calculation jobs' tab
        And I edit the selected calculation job
        And I select the calculation method 'Alleen eigen rekenpunten'

        And I save the calculation job
        And I start the calculation
        And I navigate to the 'Results' tab

        Then the following notification is displayed 'Rekentaak 1 - result is gestart.'
        And the following notification is displayed 'Rekentaak 1 - result is voltooid.'

        Then I select situation with label 'Situatie 1 - Beoogd' and validate the 'Projectberekening' result screen with results for the result 'Eigen rekenpunten'
        | elementId                      | expectedResult |
        | countCalculationPoints         |             69 |
        | countCalculationPointsIncrease |             41 |
        | countCalculationPointsDecrease |              0 |
        | maxIncrease                    |           1,06 |
        | maxDecrease                    |           0,00 |
        Then I validate the calculation point results
        | id | name                                                   | expectedResult |
        |  4 | Botshol H91D0 (<1 km)                                  |           1,04 |
        |  3 | Botshol H7140B (<1 km)                                 |           1,06 |
        | 69 | Rekenpunt x                                            |           0,96 |
        |  5 | Botshol H7210 (<1 km)                                  |           0,36 |
        |  2 | Botshol H6510A (<1 km)                                 |           0,52 |
        |  1 | Botshol (<1 km)                                        |           0,68 |
        | 42 | Naardermeer H7140A (15 km)                             |           0,02 |
        | 32 | Markermeer & IJmeer (12 km)                            |           0,02 |
        | 37 | Naardermeer H91D0 (13 km)                              |           0,02 |
        | 11 | Oostelijke Vechtplassen H3150baz (9 km)                |           0,01 |
        | 15 | Oostelijke Vechtplassen H7140A (11 km)                 |           0,02 |
        | 38 | Naardermeer H7140B (14 km)                             |           0,02 |
        | 41 | Naardermeer H4010B (14 km)                             |           0,02 |
        | 44 | Naardermeer ZGH3150baz (16 km)                         |           0,01 |
        | 18 | Oostelijke Vechtplassen ZGH7140B (12 km)               |           0,01 |
        |  9 | Oostelijke Vechtplassen ZGH3140 (9 km)                 |           0,01 |
        | 13 | Oostelijke Vechtplassen H7140B (11 km)                 |           0,01 |
        | 33 | Naardermeer (13 km)                                    |           0,01 |
        | 10 | Oostelijke Vechtplassen H91D0 (9 km)                   |           0,01 |
        |  6 | Oostelijke Vechtplassen (9 km)                         |           0,01 |
        | 39 | Naardermeer H3140lv (14 km)                            |           0,01 |
        | 34 | Naardermeer Lg05 (13 km)                               |           0,01 |
        | 35 | Naardermeer H3150baz (13 km)                           |           0,01 |
        | 36 | Naardermeer ZGH7140B (13 km)                           |           0,01 |
        |  8 | Oostelijke Vechtplassen Lg05 (9 km)                    |           0,01 |
        |  7 | Oostelijke Vechtplassen ZGH3150 (9 km)                 |           0,01 |
        | 16 | Oostelijke Vechtplassen H6410 (11 km)                  |           0,01 |
        | 40 | Naardermeer H9999:94 (14 km)                           |           0,01 |
        | 45 | Eemmeer & Gooimeer Zuidoever (18 km)                   |           0,01 |
        | 12 | Oostelijke Vechtplassen H3140lv (10 km)                |           0,01 |
        | 19 | Oostelijke Vechtplassen H4010B (12 km)                 |           0,01 |
        | 14 | Oostelijke Vechtplassen H7210 (11 km)                  |           0,01 |
        | 17 | Oostelijke Vechtplassen ZGH91D0 (12 km)                |           0,01 |
        | 43 | Naardermeer H3130 & Naardermeer H6410 (16 km)          |           0,01 |
        | 24 | Nieuwkoopse Plassen & De Haeck H3150baz (11 km)        |           0,01 |
        | 46 | Ilperveld, Varkensland, Oostzanerveld & Twiske (19 km) |           0,01 |
        | 26 | Nieuwkoopse Plassen & De Haeck H91D0 (12 km)           |           0,01 |
        | 21 | Nieuwkoopse Plassen & De Haeck (10 km)                 |           0,01 |
        | 20 | Oostelijke Vechtplassen H9999:95 (15 km)               |           0,01 |
        | 25 | Nieuwkoopse Plassen & De Haeck H3140lv (11 km)         |              - |
        | 22 | Nieuwkoopse Plassen & De Haeck H7140B (11 km)          |              - |
