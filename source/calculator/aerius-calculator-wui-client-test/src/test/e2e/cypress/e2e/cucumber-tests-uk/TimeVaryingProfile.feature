#
# Crown Copyright
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Time-varying Profile

    Scenario: I would like to view the predefined time-varying profiles for road sources
        Given I login with correct username and password
        When I create an empty scenario from the start page
        And I select view mode 'TIME_VARYING_PROFILES'
        Then I see a list of the following predefined time-varying profiles
          | name                          |
          | Sector default (UK road 2018) |
          | Sector default (UK road 2019) |
          | Sector default (UK road 2020) |
          | Sector default (UK road 2021) |
          | Sector default (UK road 2022) |

        When I select time-varying profile 'UK road 2018'
        Then I check the details of the custom diurnal profile
          | profileTitle       | profileName                   | profileType        | weekdays | saturdays | sundays | total    | averageWeekdays| averageSaturdays| averageSundays | average |
          | Diurnal profile    | Sector default (UK road 2018) | Predefined profile | 128.1219 | 21.1761   | 18.7020 | 168.0000 | 1.0677         | 0.8823          | 0.7793         | 1.0000  |
        And I expect the 'diurnal' graph to have 3 lines

        When I select time-varying profile 'UK road 2019'
        Then I check the details of the custom diurnal profile
          | profileTitle       | profileName                   | profileType        | weekdays | saturdays | sundays | total    | averageWeekdays| averageSaturdays| averageSundays | average |
          | Diurnal profile    | Sector default (UK road 2019) | Predefined profile | 127.8919 | 21.2796   | 18.8285 | 168.0000 | 1.0658         | 0.8867          | 0.7845         | 1.0000  |
        And I expect the 'diurnal' graph to have 3 lines
       
        When I select time-varying profile 'UK road 2020'
        Then I check the details of the custom diurnal profile
          | profileTitle       | profileName                   | profileType        | weekdays | saturdays | sundays | total    | averageWeekdays| averageSaturdays| averageSundays | average |
          | Diurnal profile    | Sector default (UK road 2020) | Predefined profile | 128.9152 | 21.0720   | 18.0127 | 168.0000 | 1.0743         | 0.8780          | 0.7505         | 1.0000  |
        And I expect the 'diurnal' graph to have 3 lines

        When I select time-varying profile 'UK road 2021'
        Then I check the details of the custom diurnal profile
          | profileTitle       | profileName                   | profileType        | weekdays | saturdays | sundays | total    | averageWeekdays| averageSaturdays| averageSundays | average |
          | Diurnal profile    | Sector default (UK road 2021) | Predefined profile | 127.8264 | 21.4958   | 18.6778 | 168.0000 | 1.0652         | 0.8957          | 0.7782         | 1.0000  |
        And I expect the 'diurnal' graph to have 3 lines

        When I select time-varying profile 'UK road 2022'
        Then I check the details of the custom diurnal profile
          | profileTitle       | profileName                   | profileType        | weekdays | saturdays | sundays | total    | averageWeekdays| averageSaturdays| averageSundays | average |
          | Diurnal profile    | Sector default (UK road 2022) | Predefined profile | 126.9450 | 21.7862   | 19.2688 | 168.0000 | 1.0579         | 0.9078          | 0.8029         | 1.0000  |
        And I expect the 'diurnal' graph to have 3 lines
    
    @Smoke
    Scenario: Import time-varying profiles
        Given I login with correct username and password
        And I enable the advanced importmodus
        When I select file 'UKAPAS_time_varying_profiles.zip' to be imported, but I don't upload it yet
        And I validate import element availability
          | element               | available |
          | emission source       | true      |
          | time-varying profiles | true      |
          | calculation job       | true      |
          | calculation results   | true      |
        And I choose to import the files
        When I select view mode 'EMISSION_SOURCES'
        Then the emission source list has 1 sources
        When I select view mode 'BUILDING'
        Then the buildinglist has 0 buildings
        When I select view mode 'TIME_VARYING_PROFILES'
        Then the time-varying profileslist has 2 profiles
        When I navigate to the 'Calculation jobs' tab
        Then the calculation jobs list has 1 calculation jobs
        Then I expect the following calculation job to exist 'Calculation job 1 - UKAPAS_time_varying_profiles'

    Scenario: Import time-varying profiles then exclude time-varying profiles
        Given I login with correct username and password
        And I enable the advanced importmodus
        When I select file 'UKAPAS_time_varying_profiles.zip' to be imported, but I don't upload it yet
        And I validate import element availability
          | element               | available |
          | emission source       | true      |
          | time-varying profiles | true      |
          | calculation job       | true      |
          | calculation results   | true      |
        And I uncheck the box for importing element 'time-varying profiles'
        And I choose to import the files
        When I select view mode 'EMISSION_SOURCES'
        Then the emission source list has 1 sources
        When I select view mode 'BUILDING'
        Then the buildinglist has 0 buildings
        When I select view mode 'TIME_VARYING_PROFILES'
        Then the time-varying profileslist has 0 profiles
        When I navigate to the 'Calculation jobs' tab
        Then the calculation jobs list has 1 calculation jobs
        Then I expect the following calculation job to exist 'Calculation job 1 - UKAPAS_time_varying_profiles'

    @Smoke
    Scenario: Add and copy custom diurnal profile
        # Add custom diurnal profile when there are no other time-varying profiles
        Given I login with correct username and password
        And I create an empty scenario from the start page
        When I select view mode 'TIME_VARYING_PROFILES'
        # check for launch buttons when profile list is empty
        Then I see a launch button for adding a new custom 'diurnal' profile
        And I see a launch button for adding a new custom 'monthly' profile
        When I choose to add a custom 'diurnal' profile when there are no custom profiles
        And I edit the column 'Weekday avg.' for time of the day '00-01' to value '2.000'
        And I edit the column 'Weekday avg.' for time of the day '07-08' to value '0.000'
        And I edit the column 'Saturday' for time of the day '12-13' to value '2.000'
        And I edit the column 'Saturday' for time of the day '23-24' to value '0.000'
        And I edit the column 'Sunday' for time of the day '14-15' to value '2.000'
        And I edit the column 'Sunday' for time of the day '20-21' to value '0.000'
        And I click on the button to 'save'
        Then I check the details of the custom diurnal profile
          | profileTitle       | profileName           | profileType    | weekdays | saturdays | sundays | total    | averageWeekdays| averageSaturdays| averageSundays | average |
          | Diurnal profile    | Diurnal profile 1     | Custom profile | 120.0000 | 24.0000   | 24.0000 | 168.0000 | 1.0000         | 1.0000          | 1.0000         | 1.0000  |
        # 3 lines on top of each other because of default values
        And I expect the 'diurnal' graph to have 3 lines
        And I expect a graph line for 'weekday'
        And I expect a graph line for 'saturday'
        And I expect a graph line for 'sunday'

        # Copy custom diurnal profile
        When I choose to copy the custom profile
        Then I see a list of the following custom time-varying profiles
          | profileName           |
          | Diurnal profile 1     |
          | Diurnal profile 1 (1) |
        And I check the details of the custom diurnal profile
          | profileTitle        | profileName           | profileType    | weekdays | saturdays | sundays | total    | averageWeekdays| averageSaturdays| averageSundays | average |
          | Diurnal profile     | Diurnal profile 1 (1) | Custom profile | 120.0000 | 24.0000   | 24.0000 | 168.0000 | 1.0000         | 1.0000          | 1.0000         | 1.0000  |
        And I expect the 'diurnal' graph to have 3 lines
        And I expect a graph line for 'weekday'
        And I expect a graph line for 'saturday'
        And I expect a graph line for 'sunday'

        # Add custom diurnal profile when there's already a list; added this test because there are two different buttons to add
        When I choose to add a custom 'diurnal' profile
        And I click on the button to 'save'
        Then I see a list of the following custom time-varying profiles
          | profileName           |
          | Diurnal profile 1     |
          | Diurnal profile 1 (1) |
          | Diurnal profile 3     |       
        And I check the details of the custom diurnal profile
          | profileTitle       | profileName           | profileType    | weekdays | saturdays | sundays | total    | averageWeekdays| averageSaturdays| averageSundays | average |
          | Diurnal profile    | Diurnal profile 3     | Custom profile | 120.0000 | 24.0000   | 24.0000 | 168.0000 | 1.0000         | 1.0000          | 1.0000         | 1.0000  |
        And I expect the 'diurnal' graph to have 3 lines
        And I expect a graph line for 'weekday'
        And I expect a graph line for 'saturday'
        And I expect a graph line for 'sunday'

    @Smoke
    Scenario: Add and copy custom monthly profile
         # Add monthly diurnal profile when there are no other time-varying profiles
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'TIME_VARYING_PROFILES'
        When I choose to add a custom 'monthly' profile when there are no custom profiles
        And I click on the button to 'save'
        Then I check the details of the custom monthly profile
          | profileTitle    | profileName           | profileType    | total   | average |
          | Monthly profile | Monthly profile 1     | Custom profile | 12.0000 | 1.0000  |
        And I expect the 'monthly' graph to have 1 lines
        And I expect a graph line for 'monthly'

        # Copy custom monthly profile
        When I choose to copy the custom profile
        Then I see a list of the following custom time-varying profiles
          | profileName           |
          | Monthly profile 1     |
          | Monthly profile 1 (1) |
        And I check the details of the custom monthly profile
          | profileTitle    | profileName           | profileType    | total   | average |
          | Monthly profile | Monthly profile 1 (1) | Custom profile | 12.0000 | 1.0000  |
        And I expect the 'monthly' graph to have 1 lines
        And I expect a graph line for 'monthly'

        # Add custom monthly profile when there's already a list; added this test because there are two different buttons to add
        When I choose to add a custom 'monthly' profile
        And I click on the button to 'save'
        Then I see a list of the following custom time-varying profiles
          | profileName           |
          | Monthly profile 1     |
          | Monthly profile 1 (1) |
          | Monthly profile 3     |        
        And I check the details of the custom monthly profile
          | profileTitle    | profileName           | profileType    | total   | average  |
          | Monthly profile | Monthly profile 3     | Custom profile | 12.0000 | 1.0000   |
        And I expect the 'monthly' graph to have 1 lines
        And I expect a graph line for 'monthly'

    Scenario: Edit name of custom diurnal profile
        Given I login with correct username and password
        And I create an empty scenario from the start page
        When I select view mode 'TIME_VARYING_PROFILES'
        And I choose to add a custom 'diurnal' profile when there are no custom profiles
        And I change the name of the profile to 'Diurnal profile 1 - edited'
        And I edit the column 'Weekday avg.' for time of the day '00-01' to value '2.000'
        And I edit the column 'Weekday avg.' for time of the day '07-08' to value '0.000'
        And I edit the column 'Saturday' for time of the day '12-13' to value '2.000'
        And I edit the column 'Saturday' for time of the day '23-24' to value '0.000'
        And I edit the column 'Sunday' for time of the day '14-15' to value '2.000'
        And I edit the column 'Sunday' for time of the day '20-21' to value '0.000'
        And I click on the button to 'save'
        Then I check the details of the custom diurnal profile
          | profileTitle       | profileName                | profileType    | weekdays | saturdays | sundays | total    | averageWeekdays| averageSaturdays| averageSundays | average |
          | Diurnal profile    | Diurnal profile 1 - edited | Custom profile | 120.0000 | 24.0000   | 24.0000 | 168.0000 | 1.0000         | 1.0000          | 1.0000         | 1.0000  |
        And I expect the 'diurnal' graph to have 3 lines
        And I expect a graph line for 'weekday'
        And I expect a graph line for 'saturday'
        And I expect a graph line for 'sunday'

        When I choose to edit the custom profile
        And I change the name of the profile to 'Diurnal profile 1 - edited again'
        And I click on the button to 'save'
        Then I check the details of the custom diurnal profile
          | profileTitle       | profileName                      | profileType    | weekdays | saturdays | sundays | total    | averageWeekdays| averageSaturdays| averageSundays | average |
          | Diurnal profile    | Diurnal profile 1 - edited again | Custom profile | 120.0000 | 24.0000   | 24.0000 | 168.0000 | 1.0000         | 1.0000          | 1.0000         | 1.0000  |
        And I expect the 'diurnal' graph to have 3 lines
        And I expect a graph line for 'weekday'
        And I expect a graph line for 'saturday'
        And I expect a graph line for 'sunday'

    Scenario: Edit name of custom monthly profile
        Given I login with correct username and password
        And I create an empty scenario from the start page
        When I select view mode 'TIME_VARYING_PROFILES'
        When I choose to add a custom 'monthly' profile when there are no custom profiles
        And I change the name of the profile to 'Monthly profile 1 - edited'
        And I click on the button to 'save'
        Then I check the details of the custom monthly profile        
          | profileTitle    | profileName                | profileType    | total   | average  |
          | Monthly profile | Monthly profile 1 - edited | Custom profile | 12.0000 | 1.0000   |
        And I expect the 'monthly' graph to have 1 lines
        And I expect a graph line for 'monthly'

        When I choose to edit the custom profile
        And I change the name of the profile to 'Monthly profile 1 - edited again'
        And I click on the button to 'save'
        Then I check the details of the custom monthly profile
          | profileTitle    | profileName                      | profileType    | total   | average  |
          | Monthly profile | Monthly profile 1 - edited again | Custom profile | 12.0000 | 1.0000   |     
        And I expect the 'monthly' graph to have 1 lines
        And I expect a graph line for 'monthly'   

    Scenario: Edit hourly input of custom diurnal profile
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'TIME_VARYING_PROFILES'
        And I choose to add a custom 'diurnal' profile when there are no custom profiles
        When I edit the column 'Weekday avg.' for time of the day '00-01' to value '2.000'
        And I edit the column 'Weekday avg.' for time of the day '07-08' to value '0.000'
        And I edit the column 'Weekday avg.' for time of the day '17-18' to value '2.000'
        And I edit the column 'Weekday avg.' for time of the day '23-24' to value '0.000'

        And I edit the column 'Saturday' for time of the day '00-01' to value '0.5000'
        And I edit the column 'Saturday' for time of the day '07-08' to value '0.5000'
        And I edit the column 'Saturday' for time of the day '10-11' to value '0.5000'
        And I edit the column 'Saturday' for time of the day '14-15' to value '0.5000'
        And I edit the column 'Saturday' for time of the day '16-17' to value '2.000'
        And I edit the column 'Saturday' for time of the day '23-24' to value '2.000'

        And I edit the column 'Sunday' for time of the day '00-01' to value '1.500'
        And I edit the column 'Sunday' for time of the day '11-12' to value '0.000'
        And I edit the column 'Sunday' for time of the day '21-22' to value '2.000'
        And I edit the column 'Sunday' for time of the day '23-24' to value '0.500'

        And I click on the button to 'save'
        Then I check the details of the custom diurnal profile
          | profileTitle       | profileName       | profileType    | weekdays | saturdays | sundays | total    | averageWeekdays| averageSaturdays| averageSundays | average |
          | Diurnal profile    | Diurnal profile 1 | Custom profile | 120.0000 | 24.0000   | 24.0000 | 168.0000 | 1.0000         | 1.0000          | 1.0000         | 1.0000  |
        And I expect the 'diurnal' graph to have 3 lines
        And I expect a graph line for 'weekday'
        And I expect a graph line for 'saturday'
        And I expect a graph line for 'sunday'

    Scenario: Edit monthly input of custom monthly profile
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'TIME_VARYING_PROFILES'
        And I choose to add a custom 'monthly' profile when there are no custom profiles
        When I edit the column Monthly profile for month 'January' to value '0.5000'
        And I edit the column Monthly profile for month 'March' to value '0.5000'
        And I edit the column Monthly profile for month 'September' to value '2.000'
        And I edit the column Monthly profile for month 'November' to value '0.5000'
        And I edit the column Monthly profile for month 'December' to value '1.5000'
        And I click on the button to 'save'
        Then I check the details of the custom monthly profile        
          | profileTitle    | profileName       | profileType    | total   | average  |
          | Monthly profile | Monthly profile 1 | Custom profile | 12.0000 | 1.0000   |
        And I expect the 'monthly' graph to have 1 lines
        And I expect a graph line for 'monthly'

   Scenario: Delete custom diurnal and monthly profile 
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'TIME_VARYING_PROFILES'
        And I choose to add a custom 'diurnal' profile when there are no custom profiles
        And I click on the button to 'save'
        And I see a list of the following custom time-varying profiles
          | profileName           |
          | Diurnal profile 1     |
        And I choose to add a custom 'monthly' profile
        And I click on the button to 'save'
        And I see a list of the following custom time-varying profiles
          | profileName           |
          | Diurnal profile 1     |
          | Monthly profile 2     |

        # delete diurnal profile
        When I select custom profile 'Diurnal profile 1'
        And I choose to delete the custom profile
        Then I see a list of the following custom time-varying profiles
          | profileName           |
          | Monthly profile 2     |
        
        # delete monthly profile
        When I select custom profile 'Monthly profile 2'
        And I choose to delete the custom profile
        Then I see a list of the following custom time-varying profiles
          | profileName           |

    Scenario: Delete all custom time-varying profiles
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'TIME_VARYING_PROFILES'
        And I choose to add a custom 'diurnal' profile when there are no custom profiles
        And I click on the button to 'save'
        And I choose to copy the custom profile
        And I choose to copy the custom profile
        And I choose to copy the custom profile
        And I choose to add a custom 'monthly' profile
        And I click on the button to 'save'
        And I choose to copy the custom profile
        And I choose to copy the custom profile
        And I choose to copy the custom profile
        And I see a list of the following custom time-varying profiles
          | profileName           |
          | Diurnal profile 1     |
          | Diurnal profile 1 (1) |
          | Diurnal profile 1 (2) |
          | Diurnal profile 1 (3) |
          | Monthly profile 5     |
          | Monthly profile 5 (1) |
          | Monthly profile 5 (2) |
          | Monthly profile 5 (3) |
        When I choose to delete all the custom profiles
        Then I see a list of the following custom time-varying profiles
          | profileName           |

    Scenario: Cancel adding a custom diurnal profile when profile list is empty
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'TIME_VARYING_PROFILES'
        When I choose to add a custom 'diurnal' profile when there are no custom profiles
        And I click on the button to 'cancel'
        Then I see a list of the following custom time-varying profiles
          | profileName           |

    Scenario: Cancel adding a custom diurnal profile when profile list is not empty
        Given I login with correct username and password
        And I create an empty scenario from the start page
        When I select view mode 'TIME_VARYING_PROFILES'
        When I choose to add a custom 'diurnal' profile when there are no custom profiles
        And I click on the button to 'save'
        Then I see a list of the following custom time-varying profiles
          | profileName           |
          | Diurnal profile 1     |
        When I choose to add a custom 'diurnal' profile
        And I click on the button to 'cancel'
        Then I see a list of the following custom time-varying profiles
          | profileName           |
          | Diurnal profile 1     |

    Scenario: Cancel adding a custom monthly profile when profile list is empty
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'TIME_VARYING_PROFILES'
        When I choose to add a custom 'monthly' profile when there are no custom profiles
        And I click on the button to 'cancel'
        Then I see a list of the following custom time-varying profiles
          | profileName           |

    Scenario: Cancel adding a custom monthly profile when profile list is not empty
        Given I login with correct username and password
        And I create an empty scenario from the start page
        When I select view mode 'TIME_VARYING_PROFILES'
        When I choose to add a custom 'monthly' profile when there are no custom profiles
        And I click on the button to 'save'
        Then I see a list of the following custom time-varying profiles
          | profileName           |
          | Monthly profile 1     |
        When I choose to add a custom 'monthly' profile
        And I click on the button to 'cancel'
        Then I see a list of the following custom time-varying profiles
          | profileName           |
          | Monthly profile 1     |
 
    Scenario: Adding a custom diurnal/monthly profile to a source - profile 'Continuous'
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Energy' and sector 'Other'
        And I fill location with coordinates 'POINT(171561.58 621179.02)'
        And I choose source height '25' and source diameter '10'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I open panel 'Emissions'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I open panel 'Time-varying profiles'
        And I select for 'diurnal' profile the option 'Continuous'
        And I select for 'monthly' profile the option 'Continuous'
        And I save the data
        When I select the source 'Source 1'
        Then the source 'Source 1' with sector group 'Energy' and sector 'Other' is correctly saved
        And location with coordinates 'X:171561.58 Y:621179.02' is correctly saved
        And the total 'generic' emission 'NOx' is '10.0 kg/y'
        And the total 'generic' emission 'NH3' is '20.0 kg/y'
        And the details of the profile are correctly saved
          | profileTitle       | profileName  | profileType        |
          | Diurnal profile    | Continuous   | Predefined profile |
          | Monthly profile    | Continuous   | Predefined profile |
        And I expect the 'diurnal' graph to have 3 lines
        And I expect a graph line for 'weekday'
        And I expect a graph line for 'saturday'
        And I expect a graph line for 'sunday'
        And I expect the 'monthly' graph to have 1 lines
        And I expect a graph line for 'monthly'

    Scenario: Adding a custom diurnal/monthly profile to a source - profile 'Sector default'
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Energy' and sector 'Other'
        And I fill location with coordinates 'POINT(171561.58 621179.02)'
        And I choose source height '25' and source diameter '10'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I open panel 'Emissions'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I open panel 'Time-varying profiles'
        And I select for 'diurnal' profile the option 'Sector default'
        And I select for 'monthly' profile the option 'Sector default'
        And I save the data
        When I select the source 'Source 1'
        Then the source 'Source 1' with sector group 'Energy' and sector 'Other' is correctly saved
        And location with coordinates 'X:171561.58 Y:621179.02' is correctly saved
        And the total 'generic' emission 'NOx' is '10.0 kg/y'
        And the total 'generic' emission 'NH3' is '20.0 kg/y'
        And the details of the profile are correctly saved
          | profileTitle       | profileName                 | profileType        |
          | Diurnal profile    | Sector default (Continuous) | Predefined profile |
          | Monthly profile    | Sector default (Continuous) | Predefined profile |
        And I expect the 'diurnal' graph to have 3 lines
        And I expect a graph line for 'weekday'
        And I expect a graph line for 'saturday'
        And I expect a graph line for 'sunday'
        And I expect the 'monthly' graph to have 1 lines
        And I expect a graph line for 'monthly'

    Scenario: Adding a new custom diurnal profile while adding a source
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Energy' and sector 'Other'
        And I fill location with coordinates 'POINT(171561.58 621179.02)'
        And I choose source height '25' and source diameter '10'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I open panel 'Emissions'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I open panel 'Time-varying profiles'
        And I select for 'diurnal' profile the option 'Add new...'
        And I change the name of the profile to 'Diurnal profile added to source'
        And I save the data
        When I save the data
        And I select the source 'Source 1'
        Then the source 'Source 1' with sector group 'Energy' and sector 'Other' is correctly saved
        And location with coordinates 'X:171561.58 Y:621179.02' is correctly saved
        And the total 'generic' emission 'NOx' is '10.0 kg/y'
        And the total 'generic' emission 'NH3' is '20.0 kg/y'
        Then the details of the profile are correctly saved
          | profileTitle       | profileName                     | profileType        |
          | Diurnal profile    | Diurnal profile added to source | Custom profile     |
          | Monthly profile    | Sector default (Continuous)     | Predefined profile |
        And I expect the 'diurnal' graph to have 3 lines
        And I expect a graph line for 'weekday'
        And I expect a graph line for 'saturday'
        And I expect a graph line for 'sunday'
        And I expect the 'monthly' graph to have 1 lines
        And I expect a graph line for 'monthly'

   Scenario: Adding a new custom monthly profile while adding a source
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Energy' and sector 'Other'
        And I fill location with coordinates 'POINT(171561.58 621179.02)'
        And I choose source height '25' and source diameter '10'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I open panel 'Emissions'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I open panel 'Time-varying profiles'
        And I select for 'monthly' profile the option 'Add new...'
        And I change the name of the profile to 'Monthly profile added to source'
        And I save the data
        When I save the data
        And I select the source 'Source 1'
        Then the source 'Source 1' with sector group 'Energy' and sector 'Other' is correctly saved
        And location with coordinates 'X:171561.58 Y:621179.02' is correctly saved
        And the total 'generic' emission 'NOx' is '10.0 kg/y'
        And the total 'generic' emission 'NH3' is '20.0 kg/y'
        Then the details of the profile are correctly saved
          | profileTitle       | profileName                     | profileType        |
          | Diurnal profile    | Sector default (Continuous)     | Predefined profile |
          | Monthly profile    | Monthly profile added to source | Custom profile     |
        And I expect the 'diurnal' graph to have 3 lines
        And I expect a graph line for 'weekday'
        And I expect a graph line for 'saturday'
        And I expect a graph line for 'sunday'
        And I expect the 'monthly' graph to have 1 lines
        And I expect a graph line for 'monthly'

    Scenario: Warning when deleting profile while sources are linked to that profile 
        Given I login with correct username and password
        # Linking diurnal profile to source
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Energy' and sector 'Other'
        And I fill location with coordinates 'POINT(171561.58 621179.02)'
        And I choose source height '25' and source diameter '10'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I open panel 'Emissions'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I open panel 'Time-varying profiles'
        And I select for 'diurnal' profile the option 'Add new...'
        And I change the name of the profile to 'Diurnal profile added to source'
        And I save the data
        And I save the data
        # Linking monthly profile to source
        And I create a new source from the source page
        And I name the source 'Source 2' and select sector group 'Industry' and sector 'Waste processing'
        And I fill location with coordinates 'POINT(322447.2 594884.3)'
        And I choose source height '25' and source diameter '10'
        And I open panel 'Time-varying profiles'
        And I select for 'monthly' profile the option 'Add new...'
        And I change the name of the profile to 'Monthly profile added to source'
        And I save the data
        And I save the data
        # Linking diurnal and monthly profile to source
        And I create a new source from the source page
        And I name the source 'Source 3' and select sector group 'Energy' and sector 'Large combustion plant'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose source height '25' and source diameter '10'
        And I open panel 'Time-varying profiles'
        And I select for 'diurnal' profile the option 'Diurnal profile added to source'   
        And I select for 'monthly' profile the option 'Monthly profile added to source'
        And I save the data
        When I select the source 'Source 1'
        Then the details of the profile are correctly saved
          | profileTitle       | profileName                     | profileType        |
          | Diurnal profile    | Diurnal profile added to source | Custom profile     |
          | Monthly profile    | Sector default (Continuous)     | Predefined profile |
        When I select the source 'Source 2'
        Then the details of the profile are correctly saved
          | profileTitle       | profileName                     | profileType        |
          | Diurnal profile    | Sector default (Continuous)     | Predefined profile |
          | Monthly profile    | Monthly profile added to source | Custom profile     |
        When I select the source 'Source 3'
        Then the details of the profile are correctly saved
          | profileTitle       | profileName                     | profileType        |
          | Diurnal profile    | Diurnal profile added to source | Custom profile     |
          | Monthly profile    | Monthly profile added to source | Custom profile     |

        # Delete diurnal profile will show warning
        When I select view mode 'TIME_VARYING_PROFILES'
        And I select custom profile 'Diurnal profile added to source'
        And I choose to delete the custom profile
        Then the following warning is displayed: 'This time-varying profile (TimeVaryingProfile.1 - Diurnal profile added to source) is linked to 2 sources. By removing this profile, those links will be severed.'
        When I select view mode 'EMISSION_SOURCES'
        And I select the source 'Source 1'
        Then the details of the profile are correctly saved
          | profileTitle       | profileName                     | profileType        |
          | Diurnal profile    | Sector default (Continuous)     | Predefined profile |
          | Monthly profile    | Sector default (Continuous)    | Predefined profile |
        When I select the source 'Source 2'
        Then the details of the profile are correctly saved
          | profileTitle       | profileName                     | profileType        |
          | Diurnal profile    | Sector default (Continuous)     | Predefined profile |
          | Monthly profile    | Monthly profile added to source | Custom profile     |
        When I select the source 'Source 3'
        Then the details of the profile are correctly saved
          | profileTitle       | profileName                     | profileType        |
          | Diurnal profile    | Sector default (Continuous)     | Predefined profile |
          | Monthly profile    | Monthly profile added to source | Custom profile     |

        # Delete montly profile will show warning
        When I select view mode 'TIME_VARYING_PROFILES'
        And I select custom profile 'Monthly profile added to source'        
        And I choose to delete the custom profile
        Then the following warning is displayed: 'This time-varying profile (TimeVaryingProfile.2 - Monthly profile added to source) is linked to 2 sources. By removing this profile, those links will be severed.'
        When I select view mode 'EMISSION_SOURCES'
        And I select the source 'Source 1'
          | profileTitle       | profileName                     | profileType        |
          | Diurnal profile    | Sector default (Continuous)     | Predefined profile |
          | Monthly profile    | Sector default (Continuous)     | Predefined profile |
        When I select the source 'Source 2'
        Then the details of the profile are correctly saved
          | profileTitle       | profileName                     | profileType        |
          | Diurnal profile    | Sector default (Continuous)     | Predefined profile |
          | Monthly profile    | Sector default (Continuous)     | Predefined profile |
        When I select the source 'Source 3'
        Then the details of the profile are correctly saved
          | profileTitle       | profileName                     | profileType        |
          | Diurnal profile    | Sector default (Continuous)     | Predefined profile |
          | Monthly profile    | Sector default (Continuous)     | Predefined profile |

    Scenario: Add custom diurnal profile by raw input
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'TIME_VARYING_PROFILES'
        And I choose to add a custom 'diurnal' profile when there are no custom profiles
        And I select input mode 'Raw input' to add a new time varying profile
        When I paste the following text in the text area for the 'diurnal' profile:
          """
          hour,weekdays,saturdays,sundays
          0,1.0000,1.5000,0.5000
          1,1.0000,1.5000,0.5000
          2,1.0000,1.0000,1.0000
          3,1.0000,1.0000,1.0000
          4,2.0000,1.0000,1.0000
          5,1.0000,0.5000,1.0000
          6,1.0000,1.0000,1.0000
          7,1.0000,1.0000,1.0000
          8,0.5000,1.0000,1.0000
          9,0.5000,1.0000,1.0000
          10,1.0000,1.0000,1.0000
          11,1.0000,0.5000,1.0000
          12,1.0000,1.0000,1.0000
          13,1.0000,2.0000,1.0000
          14,1.0000,1.0000,0.5000
          15,1.0000,1.0000,1.0000
          16,1.0000,1.0000,1.0000
          17,1.0000,1.0000,1.0000
          18,1.0000,1.0000,0.5000
          19,1.0000,1.0000,2.0000
          20,1.0000,1.0000,1.0000
          21,1.0000,1.0000,1.0000
          22,1.0000,1.0000,1.0000
          23,1.0000,1.0000,1.0000
          """
        And I click on the 'Import text input to profile' button for profile type 'diurnal'
        And I click on the button to 'save'
        Then I check the details of the custom diurnal profile
          | profileTitle       | profileName       | profileType    | weekdays | saturdays | sundays | total    | averageWeekdays| averageSaturdays| averageSundays | average |
          | Diurnal profile    | Diurnal profile 1 | Custom profile | 120.0000 | 25.0000   | 23.0000 | 168.0000 | 1.0000         | 1.0417          | 0.9583         | 1.0000  |

    Scenario: Add custom monthly profile by raw input
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'TIME_VARYING_PROFILES'
        And I choose to add a custom 'monthly' profile when there are no custom profiles
        And I select input mode 'Raw input' to add a new time varying profile
        When I paste the following text in the text area for the 'monthly' profile:
          """
          month,factor
          1,2.0000
          2,1.0000
          3,1.0000
          4,0.0000
          5,0.5000
          6,1.0000
          7,1.0000
          8,0.5000
          9,1.0000
          10,1.0000
          11,1.0000
          12,2.0000
          """
        And I click on the 'Import text input to profile' button for profile type 'monthly'
        And I click on the button to 'save'
        Then I check the details of the custom monthly profile
          | profileTitle    | profileName           | profileType    | total   | average  |
          | Monthly profile | Monthly profile 1     | Custom profile | 12.0000 | 1.0000   |

    Scenario: Error handling when adding custom diurnal profile by raw input
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'TIME_VARYING_PROFILES'
        And I choose to add a custom 'diurnal' profile when there are no custom profiles
        And I select input mode 'Raw input' to add a new time varying profile
        Then button 'Import text input to profile' for profile type 'diurnal' is disabled
        And button 'Export current profile to text area' for profile type 'diurnal' is enabled        
        When I paste the following text in the text area for the 'diurnal' profile:
          """
          hour,weekdays,saturdays,sundays
          """
        Then the following error for profile type 'diurnal' is visible: 'The number of rows should be either 24 or 25.'
        And button 'Import text input to profile' for profile type 'diurnal' is disabled

        When I paste the following text in the text area for the 'diurnal' profile:
          """
          hour,weekdays,saturdays,sundays,x
          0,1.0000,1.5000,0.5000
          1,1.0000,1.5000,0.5000
          2,1.0000,1.0000,1.0000
          3,1.0000,1.0000,1.0000
          4,2.0000,1.0000,1.0000
          5,1.0000,0.5000,1.0000
          6,1.0000,1.0000,1.0000
          7,1.0000,1.0000,1.0000
          8,0.5000,1.0000,1.0000
          9,0.5000,1.0000,1.0000
          10,1.0000,1.0000,1.0000
          11,1.0000,0.5000,1.0000
          12,1.0000,1.0000,1.0000
          13,1.0000,2.0000,1.0000
          14,1.0000,1.0000,0.5000
          15,1.0000,1.0000,1.0000
          16,1.0000,1.0000,1.0000
          17,1.0000,1.0000,1.0000
          18,1.0000,1.0000,0.5000
          19,1.0000,1.0000,2.0000
          20,1.0000,1.0000,1.0000
          21,1.0000,1.0000,1.0000
          22,1.0000,1.0000,1.0000
          23,1.0000,1.0000,1.0000
          """
        Then the following error for profile type 'diurnal' is visible: 'The number of columns should be either 3 or 4.'
        And button 'Import text input to profile' for profile type 'diurnal' is disabled
        
        When I paste the following text in the text area for the 'diurnal' profile:
          """
        hour,weekdays,saturdays,sundays
          0,1.0000,1.5000,0.5000
          1,1.0000,1.5000,0.5000
          2,1.0000,1.0000,1.0000
          3,1.0000,1.0000,1.0000
          4,2.0000,1.0000,1.0000
          5,1.0000,0.5000,1.0000
          6,1.0000,1.0000,1.0000
          7,1.0000,1.0000,1.0000
          8,0.5000,1.0000,1.0000
          9,0.5000,1.0000,1.0000
          10,1.0000,1.0000,1.0000
          11,1.0000,0.5000,1.0000
          12,1.0000,1.0000,1.0000
          13,1.0000,2.0000,1.0000
          14,1.0000,1.0000,0.5000
          15,1.0000,1.0000,1.0000
          16,1.0000,1.0000,1.0000
          17,1.0000,1.0000,1.0000
         18,1.0000,1.0000,0.5000
          19,1.0000,1.0000,2.0000
          20,1.0000,1.0000,1.0000
          21,1.0000,1.0000,1.0000
          22,1.0000,1.0000,1.0000
          23,1.0000,1.0000,1.0000
          """
        Then the following notice for profile type 'diurnal' is visible: 'Input is well-formed and can be imported.'
        And button 'Import text input to profile' for profile type 'diurnal' is enabled

        When I click on the 'Import text input to profile' button for profile type 'diurnal'
        Then I check the details of the custom diurnal profile
          | profileTitle       | profileName       | profileType    | weekdays | saturdays | sundays | total    | averageWeekdays| averageSaturdays| averageSundays | average |
          | Diurnal profile    | Diurnal profile 1 | Custom profile | 120.0000 | 25.0000   | 23.0000 | 168.0000 | 1.0000         | 1.0417          | 0.9583         | 1.0000  |
        And I expect the 'diurnal' graph to have 3 lines
        And I expect a graph line for 'weekday'
        And I expect a graph line for 'saturday'
        And I expect a graph line for 'sunday'
        
        When I click on the button to 'save'
        Then I check the details of the custom diurnal profile
          | profileTitle       | profileName       | profileType    | weekdays | saturdays | sundays | total    | averageWeekdays| averageSaturdays| averageSundays | average |
          | Diurnal profile    | Diurnal profile 1 | Custom profile | 120.0000 | 25.0000   | 23.0000 | 168.0000 | 1.0000         | 1.0417          | 0.9583         | 1.0000  |
        And I expect the 'diurnal' graph to have 3 lines
        And I expect a graph line for 'weekday'
        And I expect a graph line for 'saturday'
        And I expect a graph line for 'sunday'

    Scenario: Error handling when adding custom monthly profile by raw input
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'TIME_VARYING_PROFILES'
        And I choose to add a custom 'monthly' profile when there are no custom profiles
        And I select input mode 'Raw input' to add a new time varying profile
        Then button 'Import text input to profile' for profile type 'monthly' is disabled
        And button 'Export current profile to text area' for profile type 'monthly' is enabled
        When I paste the following text in the text area for the 'monthly' profile:
          """
          month,factor
          """
        Then the following error for profile type 'monthly' is visible: 'The number of rows should be either 12 or 13.'
        And button 'Import text input to profile' for profile type 'monthly' is disabled

        When I paste the following text in the text area for the 'monthly' profile:
          """
          month,factor,x
          1,2.0000
          2,1.0000
          3,1.0000
          4,0.0000
          5,0.5000
          6,1.0000
          7,1.0000
          8,0.5000
          9,1.0000
          10,1.0000
          11,1.0000
          12,2.0000
          """
        Then the following error for profile type 'monthly' is visible: 'The number of columns should be either 1 or 2.'
        And button 'Import text input to profile' for profile type 'monthly' is disabled
        
        When I paste the following text in the text area for the 'monthly' profile:
          """
          month,factor
          1,2.0000
          2,1.0000
          3,1.0000
          4,0.0000
          5,0.5000
          6,1.0000
          7,1.0000
          8,0.5000
          9,1.0000
          10,1.0000
          11,1.0000
          12,2.0000
          """
        Then the following notice for profile type 'monthly' is visible: 'Input is well-formed and can be imported.'
        And button 'Import text input to profile' for profile type 'monthly' is enabled

        When I click on the 'Import text input to profile' button for profile type 'monthly'
        Then I check the details of the custom monthly profile
          | profileTitle    | profileName           | profileType    | total   | average  |
          | Monthly profile | Monthly profile 1     | Custom profile | 12.0000 | 1.0000   |
        And I expect the 'monthly' graph to have 1 lines
        And I expect a graph line for 'monthly'
        
        When I click on the button to 'save'
        Then I check the details of the custom monthly profile
          | profileTitle    | profileName           | profileType    | total   | average  |
          | Monthly profile | Monthly profile 1     | Custom profile | 12.0000 | 1.0000   |
        And I expect the 'monthly' graph to have 1 lines
        And I expect a graph line for 'monthly'   

    Scenario: Custom diurnal profile by raw input - acceptable threshold - boundaries above threshold (+0.5)
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'TIME_VARYING_PROFILES'
        And I choose to add a custom 'diurnal' profile when there are no custom profiles
        And I select input mode 'Raw input' to add a new time varying profile
        
        # total= 168.5001 - error
        When I paste the following text in the text area for the 'diurnal' profile:
          """
          hour,weekdays,saturdays,sundays
          0,1.0000,1.0000,1.5001
          1,1.0000,1.0000,1.0000
          2,1.0000,1.0000,1.0000
          3,1.0000,1.0000,1.0000
          4,1.0000,1.0000,1.0000
          5,1.0000,1.0000,1.0000
          6,1.0000,1.0000,1.0000
          7,1.0000,1.0000,1.0000
          8,1.0000,1.0000,1.0000
          9,1.0000,1.0000,1.0000
          10,1.0000,1.0000,1.0000
          11,1.0000,1.0000,1.0000
          12,1.0000,1.0000,1.0000
          13,1.0000,1.0000,1.0000
          14,1.0000,1.0000,1.0000
          15,1.0000,1.0000,1.0000
          16,1.0000,1.0000,1.0000
          17,1.0000,1.0000,1.0000
          18,1.0000,1.0000,1.0000
          19,1.0000,1.0000,1.0000
          20,1.0000,1.0000,1.0000
          21,1.0000,1.0000,1.0000
          22,1.0000,1.0000,1.0000
          23,1.0000,1.0000,1.0000
          """
        And I click on the 'Import text input to profile' button for profile type 'diurnal'
        Then the following error about the required amount is visible: 'The computed total does not match the required amount: 168.5001 / 168'

        # total= 168.5000 - warning
        When I paste the following text in the text area for the 'diurnal' profile:
          """
          hour,weekdays,saturdays,sundays
          0,1.0000,1.0000,1.5000
          1,1.0000,1.0000,1.0000
          2,1.0000,1.0000,1.0000
          3,1.0000,1.0000,1.0000
          4,1.0000,1.0000,1.0000
          5,1.0000,1.0000,1.0000
          6,1.0000,1.0000,1.0000
          7,1.0000,1.0000,1.0000
          8,1.0000,1.0000,1.0000
          9,1.0000,1.0000,1.0000
          10,1.0000,1.0000,1.0000
          11,1.0000,1.0000,1.0000
          12,1.0000,1.0000,1.0000
          13,1.0000,1.0000,1.0000
          14,1.0000,1.0000,1.0000
          15,1.0000,1.0000,1.0000
          16,1.0000,1.0000,1.0000
          17,1.0000,1.0000,1.0000
          18,1.0000,1.0000,1.0000
          19,1.0000,1.0000,1.0000
          20,1.0000,1.0000,1.0000
          21,1.0000,1.0000,1.0000
          22,1.0000,1.0000,1.0000
          23,1.0000,1.0000,1.0000
          """
        And I click on the 'Import text input to profile' button for profile type 'diurnal'
        Then the following notice about the required amount is visible: 'The computed total is within the correct margins, however does not exactly match the ideal amount: 168.5000 / 168'

        When I click on the button to 'save'
        Then the user has to confirm and the following notice is visible: 'Check the fields above and confirm you are satisfied with the values before saving'
        And I click on the button to 'save'
        Then I check the details of the custom diurnal profile
          | profileTitle       | profileName       | profileType    | weekdays | saturdays | sundays | total    | averageWeekdays| averageSaturdays| averageSundays | average |
          | Diurnal profile    | Diurnal profile 1 | Custom profile | 120.0000 | 24.0000   | 24.5000 | 168.5000 | 1.0000         | 1.0000          | 1.0208         | 1.0030  |
        And I expect the 'diurnal' graph to have 3 lines

    Scenario: Custom diurnal profile by raw input - acceptable threshold - boundaries below threshold (-0.5)
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'TIME_VARYING_PROFILES'
        And I choose to add a custom 'diurnal' profile when there are no custom profiles
        And I select input mode 'Raw input' to add a new time varying profile
        
        # total= 167.4999 - error
        When I paste the following text in the text area for the 'diurnal' profile:
          """
          hour,weekdays,saturdays,sundays
          0,1.0000,1.0000,0.4999
          1,1.0000,1.0000,1.0000
          2,1.0000,1.0000,1.0000
          3,1.0000,1.0000,1.0000
          4,1.0000,1.0000,1.0000
          5,1.0000,1.0000,1.0000
          6,1.0000,1.0000,1.0000
          7,1.0000,1.0000,1.0000
          8,1.0000,1.0000,1.0000
          9,1.0000,1.0000,1.0000
          10,1.0000,1.0000,1.0000
          11,1.0000,1.0000,1.0000
          12,1.0000,1.0000,1.0000
          13,1.0000,1.0000,1.0000
          14,1.0000,1.0000,1.0000
          15,1.0000,1.0000,1.0000
          16,1.0000,1.0000,1.0000
          17,1.0000,1.0000,1.0000
          18,1.0000,1.0000,1.0000
          19,1.0000,1.0000,1.0000
          20,1.0000,1.0000,1.0000
          21,1.0000,1.0000,1.0000
          22,1.0000,1.0000,1.0000
          23,1.0000,1.0000,1.0000
          """
        And I click on the 'Import text input to profile' button for profile type 'diurnal'
        Then the following error about the required amount is visible: 'The computed total does not match the required amount: 167.4999 / 168'

        # total= 167.5000 - warning
        When I paste the following text in the text area for the 'diurnal' profile:
          """
          hour,weekdays,saturdays,sundays
          0,1.0000,1.0000,0.5000
          1,1.0000,1.0000,1.0000
          2,1.0000,1.0000,1.0000
          3,1.0000,1.0000,1.0000
          4,1.0000,1.0000,1.0000
          5,1.0000,1.0000,1.0000
          6,1.0000,1.0000,1.0000
          7,1.0000,1.0000,1.0000
          8,1.0000,1.0000,1.0000
          9,1.0000,1.0000,1.0000
          10,1.0000,1.0000,1.0000
          11,1.0000,1.0000,1.0000
          12,1.0000,1.0000,1.0000
          13,1.0000,1.0000,1.0000
          14,1.0000,1.0000,1.0000
          15,1.0000,1.0000,1.0000
          16,1.0000,1.0000,1.0000
          17,1.0000,1.0000,1.0000
          18,1.0000,1.0000,1.0000
          19,1.0000,1.0000,1.0000
          20,1.0000,1.0000,1.0000
          21,1.0000,1.0000,1.0000
          22,1.0000,1.0000,1.0000
          23,1.0000,1.0000,1.0000
          """
        And I click on the 'Import text input to profile' button for profile type 'diurnal'
        Then the following notice about the required amount is visible: 'The computed total is within the correct margins, however does not exactly match the ideal amount: 167.5000 / 168'

        When I click on the button to 'save'
        Then the user has to confirm and the following notice is visible: 'Check the fields above and confirm you are satisfied with the values before saving'
        And I click on the button to 'save'
        Then I check the details of the custom diurnal profile
          | profileTitle       | profileName       | profileType    | weekdays | saturdays | sundays | total    | averageWeekdays| averageSaturdays| averageSundays | average |
          | Diurnal profile    | Diurnal profile 1 | Custom profile | 120.0000 | 24.0000   | 23.5000 | 167.5000 | 1.0000         | 1.0000          | 0.9792         | 0.9970  |
        And I expect the 'diurnal' graph to have 3 lines
        And I expect a graph line for 'weekday'
        And I expect a graph line for 'saturday'
        And I expect a graph line for 'sunday'
    
    Scenario: Custom diurnal profile by table - acceptable threshold - boundaries above threshold (+0.5)
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'TIME_VARYING_PROFILES'
        And I choose to add a custom 'diurnal' profile when there are no custom profiles
        And I select input mode 'Table' to add a new time varying profile
        
        # total= 168.5001 - error
        When I edit the column 'Sunday' for time of the day '00-01' to value '1.5001'
        Then the following error about the required amount is visible: 'The computed total does not match the required amount: 168.5001 / 168'

        # total= 168.5000 - warning
        When I edit the column 'Sunday' for time of the day '00-01' to value '1.5000'
        Then the following notice about the required amount is visible: 'The computed total is within the correct margins, however does not exactly match the ideal amount: 168.5000 / 168'

        When I click on the button to 'save'
        Then the user has to confirm and the following notice is visible: 'Check the fields above and confirm you are satisfied with the values before saving'
        And I click on the button to 'confirm'
        Then I check the details of the custom diurnal profile
          | profileTitle       | profileName       | profileType    | weekdays | saturdays | sundays | total    | averageWeekdays| averageSaturdays| averageSundays | average |
          | Diurnal profile    | Diurnal profile 1 | Custom profile | 120.0000 | 24.0000   | 24.5000 | 168.5000 | 1.0000         | 1.0000          | 1.0208         | 1.0030  |
        And I expect the 'diurnal' graph to have 3 lines
        And I expect a graph line for 'weekday'
        And I expect a graph line for 'saturday'
        And I expect a graph line for 'sunday'

    Scenario: Custom diurnal profile by table - acceptable threshold - boundaries below threshold (-0.5)
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'TIME_VARYING_PROFILES'
        And I choose to add a custom 'diurnal' profile when there are no custom profiles
        And I select input mode 'Table' to add a new time varying profile
        
        # total= 167.4999 - error
        When I edit the column 'Sunday' for time of the day '00-01' to value '0.4999'
         Then the following error about the required amount is visible: 'The computed total does not match the required amount: 167.4999 / 168'

        # total= 167.5000 - warning
        When I edit the column 'Sunday' for time of the day '00-01' to value '0.5000'
        Then the following notice about the required amount is visible: 'The computed total is within the correct margins, however does not exactly match the ideal amount: 167.5000 / 168'

        When I click on the button to 'save'
        Then the user has to confirm and the following notice is visible: 'Check the fields above and confirm you are satisfied with the values before saving'
        And I click on the button to 'confirm'
        Then I check the details of the custom diurnal profile
          | profileTitle       | profileName       | profileType    | weekdays | saturdays | sundays | total    | averageWeekdays| averageSaturdays| averageSundays | average |
          | Diurnal profile    | Diurnal profile 1 | Custom profile | 120.0000 | 24.0000   | 23.5000 | 167.5000 | 1.0000         | 1.0000          | 0.9792         | 0.9970  |
        And I expect the 'diurnal' graph to have 3 lines
        And I expect a graph line for 'weekday'
        And I expect a graph line for 'saturday'
        And I expect a graph line for 'sunday'

    Scenario: Custom monthly profile by raw input - acceptable threshold - boundaries above threshold (+0.1)
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'TIME_VARYING_PROFILES'
        And I choose to add a custom 'monthly' profile when there are no custom profiles
        And I select input mode 'Raw input' to add a new time varying profile
        
        # total= 12.1001 - error
        When I paste the following text in the text area for the 'monthly' profile:
          """
          month,factor
          1,2.1001
          2,1.0000
          3,1.0000
          4,0.0000
          5,0.5000
          6,1.0000
          7,1.0000
          8,0.5000
          9,1.0000
          10,1.0000
          11,1.0000
          12,2.0000
          """
        And I click on the 'Import text input to profile' button for profile type 'monthly'
        Then the following error about the required amount is visible: 'The computed total does not match the required amount: 12.1001 / 12'

        # total= 12.1000 - warning
        When I paste the following text in the text area for the 'monthly' profile:
          """
          month,factor
          1,2.1000
          2,1.0000
          3,1.0000
          4,0.0000
          5,0.5000
          6,1.0000
          7,1.0000
          8,0.5000
          9,1.0000
          10,1.0000
          11,1.0000
          12,2.0000
          """
        And I click on the 'Import text input to profile' button for profile type 'monthly'
        Then the following notice about the required amount is visible: 'The computed total is within the correct margins, however does not exactly match the ideal amount: 12.1000 / 12'

        When I click on the button to 'save'
        Then the user has to confirm and the following notice is visible: 'Check the fields above and confirm you are satisfied with the values before saving'
        And I click on the button to 'confirm'
        Then I check the details of the custom monthly profile
          | profileTitle    | profileName           | profileType    | total   | average  |
          | Monthly profile | Monthly profile 1     | Custom profile | 12.1000 | 1.0083   |
        And I expect the 'monthly' graph to have 1 lines
        And I expect a graph line for 'monthly'
        
    Scenario: Custom monthly profile by raw input - acceptable threshold - boundaries below threshold (-0.1)
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'TIME_VARYING_PROFILES'
        And I choose to add a custom 'monthly' profile when there are no custom profiles
        And I select input mode 'Raw input' to add a new time varying profile
        
        # total= 11.8999 - error
        When I paste the following text in the text area for the 'monthly' profile:
          """
          month,factor
          1,1.8999
          2,1.0000
          3,1.0000
          4,0.0000
          5,0.5000
          6,1.0000
          7,1.0000
          8,0.5000
          9,1.0000
          10,1.0000
          11,1.0000
          12,2.0000
          """
        And I click on the 'Import text input to profile' button for profile type 'monthly'
        Then the following error about the required amount is visible: 'The computed total does not match the required amount: 11.8999 / 12'

        # total= 11.9000 - warning
        When I paste the following text in the text area for the 'monthly' profile:
          """
          month,factor
          1,1.9000
          2,1.0000
          3,1.0000
          4,0.0000
          5,0.5000
          6,1.0000
          7,1.0000
          8,0.5000
          9,1.0000
          10,1.0000
          11,1.0000
          12,2.0000
          """
        And I click on the 'Import text input to profile' button for profile type 'monthly'
        Then the following notice about the required amount is visible: 'The computed total is within the correct margins, however does not exactly match the ideal amount: 11.9000 / 12'

        When I click on the button to 'save'
        Then the user has to confirm and the following notice is visible: 'Check the fields above and confirm you are satisfied with the values before saving'
        And I click on the button to 'confirm'
        Then I check the details of the custom monthly profile
          | profileTitle    | profileName           | profileType    | total   | average  |
          | Monthly profile | Monthly profile 1     | Custom profile | 11.9000 | 0.9917   |
        And I expect the 'monthly' graph to have 1 lines
        And I expect a graph line for 'monthly'

    Scenario: Custom monthly profile by table - acceptable threshold - boundaries above threshold (+0.1)
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'TIME_VARYING_PROFILES'
        And I choose to add a custom 'monthly' profile when there are no custom profiles
        And I select input mode 'Table' to add a new time varying profile
        
        # total= 12.1001 - error
        When I edit the column Monthly profile for month 'April' to value '1.1001'
        And I click on the button to 'save'
        Then the following error about the required amount is visible: 'The computed total does not match the required amount: 12.1001 / 12'

        # total= 12.1000 - warning
        When I edit the column Monthly profile for month 'April' to value '1.1000'
        Then the following notice about the required amount is visible: 'The computed total is within the correct margins, however does not exactly match the ideal amount: 12.1000 / 12'

        When I click on the button to 'save'
        Then the user has to confirm and the following notice is visible: 'Check the fields above and confirm you are satisfied with the values before saving'
        And I click on the button to 'confirm'
        Then I check the details of the custom monthly profile
          | profileTitle    | profileName           | profileType    | total   | average  |
          | Monthly profile | Monthly profile 1     | Custom profile | 12.1000 | 1.0083   |
        And I expect the 'monthly' graph to have 1 lines
        And I expect a graph line for 'monthly'
        
    Scenario: Custom monthly profile by table - acceptable threshold - boundaries below threshold (-0.1)
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'TIME_VARYING_PROFILES'
        And I choose to add a custom 'monthly' profile when there are no custom profiles
        And I select input mode 'Table' to add a new time varying profile
        
        # total= 11.8999 - error
        When I edit the column Monthly profile for month 'August' to value '0.8999'
        And I click on the button to 'save'
        Then the following error about the required amount is visible: 'The computed total does not match the required amount: 11.8999 / 12'

        # total= 11.9000 - warning
        When I edit the column Monthly profile for month 'August' to value '0.9000'     
        Then the following notice about the required amount is visible: 'The computed total is within the correct margins, however does not exactly match the ideal amount: 11.9000 / 12'

        When I click on the button to 'save'
        Then the user has to confirm and the following notice is visible: 'Check the fields above and confirm you are satisfied with the values before saving'
        And I click on the button to 'confirm'
        Then I check the details of the custom monthly profile
          | profileTitle    | profileName           | profileType    | total   | average  |
          | Monthly profile | Monthly profile 1     | Custom profile | 11.9000 | 0.9917   |
        And I expect the 'monthly' graph to have 1 lines
        And I expect a graph line for 'monthly'

    Scenario: Custom diurnal profile by raw input - export current profile
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'TIME_VARYING_PROFILES'
        And I choose to add a custom 'diurnal' profile when there are no custom profiles
        When I edit the column 'Weekday avg.' for time of the day '00-01' to value '0.000'
        And I edit the column 'Weekday avg.' for time of the day '03-04' to value '2.000'
        And I edit the column 'Saturday' for time of the day '07-08' to value '1.5000'
        And I edit the column 'Saturday' for time of the day '13-14' to value '0.5000'
        And I edit the column 'Sunday' for time of the day '20-21' to value '0.5000'
        And I edit the column 'Sunday' for time of the day '23-00' to value '1.5000'
        When I select input mode 'Raw input' to add a new time varying profile
        And I click on the 'Export current profile to text area' button for profile type 'diurnal'
        Then I expect the exported text in the text area for the 'diurnal' profile:
          """
          hour,weekdays,saturdays,sundays
          0,0.0000,1.0000,1.0000
          1,1.0000,1.0000,1.0000
          2,1.0000,1.0000,1.0000
          3,2.0000,1.0000,1.0000
          4,1.0000,1.0000,1.0000
          5,1.0000,1.0000,1.0000
          6,1.0000,1.0000,1.0000
          7,1.0000,1.5000,1.0000
          8,1.0000,1.0000,1.0000
          9,1.0000,1.0000,1.0000
          10,1.0000,1.0000,1.0000
          11,1.0000,1.0000,1.0000
          12,1.0000,1.0000,1.0000
          13,1.0000,0.5000,1.0000
          14,1.0000,1.0000,1.0000
          15,1.0000,1.0000,1.0000
          16,1.0000,1.0000,1.0000
          17,1.0000,1.0000,1.0000
          18,1.0000,1.0000,1.0000
          19,1.0000,1.0000,1.0000
          20,1.0000,1.0000,0.5000
          21,1.0000,1.0000,1.0000
          22,1.0000,1.0000,1.0000
          23,1.0000,1.0000,1.5000

          """

    Scenario: Custom monthly profile by raw input - export current profile
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'TIME_VARYING_PROFILES'
        And I choose to add a custom 'monthly' profile when there are no custom profiles
        And I edit the column Monthly profile for month 'January' to value '0.5000'
        And I edit the column Monthly profile for month 'May' to value '0.5000'
        And I edit the column Monthly profile for month 'December' to value '2.0000'
        When I select input mode 'Raw input' to add a new time varying profile
        And I click on the 'Export current profile to text area' button for profile type 'monthly'
        Then I expect the exported text in the text area for the 'monthly' profile:
          """
          month,factor
          1,0.5000
          2,1.0000
          3,1.0000
          4,1.0000
          5,0.5000
          6,1.0000
          7,1.0000
          8,1.0000
          9,1.0000
          10,1.0000
          11,1.0000
          12,2.0000

          """
 
    Scenario: button enabled/disabled when TVP (predefined or custom) is selected
        Given I login with correct username and password
        When I create an empty scenario from the start page
        And I select view mode 'TIME_VARYING_PROFILES'
        Then button 'Edit time varying profile' is disabled
        And button 'Copy time varying profile' is disabled
        And button 'Delete time varying profile' is disabled

        # Selection of predefined profile
        When I select time-varying profile 'UK road 2020'
        Then button 'Edit time varying profile' is enabled
        And button 'Copy time varying profile' is enabled
        And button 'Delete time varying profile' is disabled
        When I click on the 'Edit time varying profile' button
        And the following system profile notification is displayed 'Note: This is a system profile which cannot be modified.'
        And I choose to cancel

        # Selection of custom profiles
        And I choose to add a custom 'diurnal' profile when there are no custom profiles
        And I click on the button to 'save'
        And I choose to add a custom 'monthly' profile
        And I click on the button to 'save'
        When I select custom time-varying profile 'Diurnal profile 1'
        Then button 'Edit time varying profile' is enabled
        And button 'Copy time varying profile' is enabled
        And button 'Delete time varying profile' is enabled
        When I select custom time-varying profile 'Monthly profile 2'
        Then button 'Edit time varying profile' is enabled
        And button 'Copy time varying profile' is enabled
        And button 'Delete time varying profile' is enabled
