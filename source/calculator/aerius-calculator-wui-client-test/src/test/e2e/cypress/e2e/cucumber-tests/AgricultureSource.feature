#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Agriculture source

    @Smoke
    Scenario: Create new agriculture source without subsource validate warning - Animal housing
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Dierhuisvesting'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose date establishment of animal shelter '2021-02-01', heat content type 'Niet geforceerd', emission height '115' and heat content '99'
        And I save the data
        Then the subsource error is given 'Voeg ten minste één subbron toe.'
        Then the savesource error is given 'Er zijn foutmeldingen, los deze eerst op.'

    Scenario: Create new agriculture source - Manure storage
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Mestopslag'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose heat content type 'Niet geforceerd', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Mestopslag' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '20,000' and diurnal variation type 'Dierverblijven' is correctly saved
        And the total 'generic' emission 'NOx' is '10,0 kg/j'
        And the total 'generic' emission 'NH3' is '20,0 kg/j'

    @Smoke
    Scenario: Create new agriculture source - Farmland
        Given I login with correct username and password
        When I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Landbouwgrond'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose heat content type 'Niet geforceerd', emission height '0' and heat content '0'
        Then I add a new farmland source
        And I fill the farmland category with 'Beweiding', NOX with value '9999' and NH3 with value '10001'
        Then I add a new farmland source
        And I fill the farmland category with 'Mestaanwending (dierlijke mest)', NOX with value '9998' and NH3 with value '11002'
        Then I add a new farmland source
        And I fill the farmland category with 'Mestaanwending (kunstmest)', NOX with value '9997' and NH3 with value '12003'
        Then I add a new farmland source
        And I fill the farmland category with 'Organische processen', NOX with value '9996' and NH3 with value '13004'
        And I save the data and wait for refresh
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Landbouwgrond' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '0,0', heat content '0,000' and diurnal variation type 'Meststoffen' is correctly saved
        And farmland category 'Beweiding' with NOX '9.999,0 kg/j' and NH3 '10,0 ton/j' is correctly created
        And farmland category 'Mestaanwending (dierlijke mest)' with NOX '9.998,0 kg/j' and NH3 '11,0 ton/j' is correctly created
        And farmland category 'Mestaanwending (kunstmest)' with NOX '9.997,0 kg/j' and NH3 '12,0 ton/j' is correctly created
        And farmland category 'Organische processen' with NOX '9.996,0 kg/j' and NH3 '13,0 ton/j' is correctly created
        And the total 'farmland' emission 'NOx' is '40,0 ton/j'
        And the total 'farmland' emission 'NH3' is '46,0 ton/j'

    Scenario: Create new agriculture source - Greenhouse horticulture
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Glastuinbouw'
        And I fill location with coordinates 'LINESTRING(200380.4 493929.51,192361.53 488279.85,211861.97 487186.37)'
        And I choose heat content type 'Niet geforceerd', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '0.01' and NH3 '0.0055'
        And I save the data
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Glastuinbouw' is correctly saved
        And location with coordinates 'X:197214,83 Y:488007,7' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '20,000' and diurnal variation type 'Verwarming van Ruimten (Zonder Seizoenscorrectie)' is correctly saved
        And the total 'generic' emission 'NOx' is '10,0 g/j'
        And the total 'generic' emission 'NH3' is '5,5 g/j'

    Scenario: Create new agriculture source - Fireplaces, other
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Vuurhaarden, overig'
        And I fill location with coordinates 'POLYGON((173407.83 567557.35,166117.94 572842.51,166117.94 572842.51,171403.11 577580.94,173407.83 567557.35))'
        And I choose heat content type 'Niet geforceerd', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '99999' and NH3 '88888'
        And I save the data
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Vuurhaarden, overig' is correctly saved
        And location with coordinates 'X:169450,23 Y:572569,15' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '20,000' and diurnal variation type 'Verwarming van Ruimten (Zonder Seizoenscorrectie)' is correctly saved
        And the total 'generic' emission 'NOx' is '100,0 ton/j'
        And the total 'generic' emission 'NH3' is '88,9 ton/j'

    @Smoke
    Scenario: Create new agriculture source - Animal housing: Validate workings of overview table
        Given I login with correct username and password
        And I create a new source from the start page
        When I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Dierhuisvesting'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I create an empty animal housing subsource

        # Validate subsource is initialized empty
        Then I validate that the subsource is empty

        # Validate "nr of animals" * "emission factor" = "emission NH3"
        When I fill in the housing code as 'HA3.1'
        And I fill in the amount of animals as '100'
        Then I validate the animal housing subsource to have the following values
        | typeOfRule | housingCode | numberOfAnimals | factor | reduction | emission     | strikeThrough |
        | Standard   | HA3.1       | 100             | 2,5    |           | 250,0 kg/j   | false         |

        # Add additional techniques and validate
        When I add an additional rule with code 'LW1.1'
        Then I validate the animal housing subsource to have the following values
        | typeOfRule | housingCode | numberOfAnimals | factor | reduction | emission     | strikeThrough |
        | Standard   | HA3.1       | 100             | 2,5    | -         | 250,0 kg/j   | true          |
        | Additional | LW1.1       | -               | -      | 70 %      | 75,0 kg/j    | false         |

    Scenario: Create new agriculture source - Animal housing
        Given I login with correct username and password
        And I create a new source from the start page
        When I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Dierhuisvesting'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I add a system with housing code 'HA1.39' and number of animals '250'
        Then I validate the animal housing subsource to have the following values
        | typeOfRule | housingCode | numberOfAnimals | factor | reduction | emission     | strikeThrough |
        | Standard   | HA1.39      | 250             | 6,2    |           | 1.550,0 kg/j | false         |
        And I save the data and wait for refresh
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        Then I validate the animal housing subsource to have the following values
        | typeOfRule | housingCode | numberOfAnimals | factor | reduction | emission     | strikeThrough |
        | Standard   | HA1.39      | 250             | 6,2    |           | 1.550,0 kg/j | false         |
        And the total 'animal housing' emission 'NOx' is '0,0 kg/j'
        And the total 'animal housing' emission 'NH3' is '1.550,0 kg/j'

    Scenario: Create new agriculture source - Animal housing with additional rule
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Dierhuisvesting'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        When I add a system with housing code 'HD1.11' and number of animals '2500'
        Then I validate the animal housing subsource to have the following values
        | typeOfRule | housingCode | numberOfAnimals | factor | reduction | emission     | strikeThrough |
        | Standard   | HD1.11      | 2500            | 0,21   |           | 525,0 kg/j   | false         |
        When I add an additional rule with code 'AV100.1'
        Then I validate the animal housing subsource to have the following values
        | typeOfRule | housingCode | numberOfAnimals | factor | reduction | emission     | strikeThrough |
        | Standard   | HD1.11      | 2500            | 0,21   | -         | 525,0 kg/j   | true          |
        | Additional | AV100.1     | -               | -      | 29 %      | 372,8 kg/j   | false         |
        And I save the data and wait for refresh
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And I validate the animal housing subsource to have the following values
        | typeOfRule | housingCode | numberOfAnimals | factor | reduction | emission     | strikeThrough |
        | Standard   | HD1.11      | 2500            | 0,21   | -         | 525,0 kg/j   | true          |
        | Additional | AV100.1     | -               | -      | 29 %      | 372,8 kg/j   | false         |
        And the total 'animal housing' emission 'NOx' is '0,0 kg/j'
        And the total 'animal housing' emission 'NH3' is '372,8 kg/j'

        # Edit the source, change the number of animals for the additional rule, and validate the results
        When I choose to edit the source
        And I select the subsource 'HD1.11'
        And I change the number of animals of the additional rule to '5000'
        Then I validate the animal housing subsource to have the following values 
        | typeOfRule | housingCode | numberOfAnimals | factor | reduction | emission     | strikeThrough |
        | Standard   | HD1.11      | 5000            | 0,21   | -         | 1.050,0 kg/j | true          |
        | Additional | AV100.1     | -               | -      | 29 %      | 745,5 kg/j   | false         |
        And I save the data and wait for refresh
        Then I close source detail screen
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And I validate the animal housing subsource to have the following values
        | typeOfRule | housingCode | numberOfAnimals | factor | reduction | emission     | strikeThrough |
        | Standard   | HD1.11      | 5000            | 0,21   | -         | 1.050,0 kg/j | true          |
        | Additional | AV100.1     | -               | -      | 29 %      | 745,5 kg/j   | false         |
        And the total 'animal housing' emission 'NOx' is '0,0 kg/j'
        And the total 'animal housing' emission 'NH3' is '745,5 kg/j'

        # Delete additional rule and validate the results
        And I choose to edit the source
        And I select the subsource 'HD1.11'
        Then I delete the additional rule
        And I save the data and wait for refresh
        Then no additional rule should exist in the detail screen
        Then the total 'animal housing' emission 'NOx' is '0,0 kg/j'
        And the total 'animal housing' emission 'NH3' is '1.050,0 kg/j'

    Scenario: Create new agriculture source - Animal housing with multiple additional rules
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Dierhuisvesting'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        When I add a system with housing code 'HD1.11' and number of animals '2500'
        Then I validate the animal housing subsource to have the following values
        | typeOfRule | housingCode | numberOfAnimals | factor | reduction | emission     | strikeThrough |
        | Standard   | HD1.11      | 2500            | 0,21   |           | 525,0 kg/j   | false         |
        When I add an additional rule with code 'AV100.1'
        Then I validate the animal housing subsource to have the following values
        | typeOfRule | housingCode | numberOfAnimals | factor | reduction | emission     | strikeThrough |
        | Standard   | HD1.11      | 2500            | 0,21   | -         | 525,0 kg/j   | true          |
        | Additional | AV100.1     | -               | -      | 29 %      | 372,8 kg/j   | false         |
        When I add an additional rule with code 'LW4.6'
        Then I validate the animal housing subsource to have the following values
        | typeOfRule | housingCode | numberOfAnimals | factor | reduction | emission     | strikeThrough |
        | Standard   | HD1.11      | 2500            | 0,21   | -         | 525,0 kg/j   | true          |
        | Additional | AV100.1     | -               | -      | 29 %      | 372,8 kg/j   | true          |
        | Additional | LW4.6       | -               | -      | 85 %      | 55,9 kg/j    | false         |
        And I save the data and wait for refresh
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And I validate the animal housing subsource to have the following values
        | typeOfRule | housingCode | numberOfAnimals | factor | reduction | emission     | strikeThrough |
        | Standard   | HD1.11      | 2500            | 0,21   | -         | 525,0 kg/j   | true          |
        | Additional | AV100.1     | -               | -      | 29 %      | 372,8 kg/j   | true          |
        | Additional | LW4.6       | -               | -      | 85 %      | 55,9 kg/j    | false         |
        And the total 'animal housing' emission 'NOx' is '0,0 kg/j'
        And the total 'animal housing' emission 'NH3' is '55,9 kg/j'

    Scenario: Cancel adding an animal housing system
        Given I login with correct username and password
        And I create a new source from the start page
        When I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Dierhuisvesting'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I add a system with housing code 'HD1.11' and number of animals '2500'
        And I add an additional rule with code 'AV100.1'
        And I save the data and wait for refresh
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And I validate the animal housing subsource to have the following values
        | typeOfRule | housingCode | numberOfAnimals | factor | reduction | emission     | strikeThrough |
        | Standard   | HD1.11      | 2500            | 0,21   | -         | 525,0 kg/j   | true          |
        | Additional | AV100.1     | -               | -      | 29 %      | 372,8 kg/j   | false         |
        When I create a new source from the source page
        And I name the source 'Bron 2' and select sector group 'Landbouw' and sector 'Dierhuisvesting'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I add a system with housing code 'HA1.39' and number of animals '50'
        When I choose to cancel  
        Then the next source does not exist 'Bron 2'

    Scenario: Copy existing agriculture source - Animal housing with additional rule
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Dierhuisvesting'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I add a system with housing code 'HD1.11' and number of animals '5000'
        And I add an additional rule with code 'AV100.1'
        And I save the data and wait for refresh
        And I select the source 'Bron 1'
        And the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And I validate the animal housing subsource to have the following values
        | typeOfRule | housingCode | numberOfAnimals | factor | reduction | emission     | strikeThrough |
        | Standard   | HD1.11      | 5000            | 0,21   | -         | 1.050,0 kg/j | true          |
        | Additional | AV100.1     | -               | -      | 29 %      | 745,5 kg/j   | false         |
        And the total 'animal housing' emission 'NOx' is '0,0 kg/j'
        And the total 'animal housing' emission 'NH3' is '745,5 kg/j'

        # Copy source
        When I copy the source 'Bron 1'
        Then the next sources exists 'Bron 1', 'Bron 1 (1)'
        And I select the source 'Bron 1'
        And I validate the animal housing subsource to have the following values
        | typeOfRule | housingCode | numberOfAnimals | factor | reduction | emission     | strikeThrough |
        | Standard   | HD1.11      | 5000            | 0,21   | -         | 1.050,0 kg/j | true          |
        | Additional | AV100.1     | -               | -      | 29 %      | 745,5 kg/j   | false         |
        And I select the source 'Bron 1 (1)'
        And I validate the animal housing subsource to have the following values
        | typeOfRule | housingCode | numberOfAnimals | factor | reduction | emission     | strikeThrough |
        | Standard   | HD1.11      | 5000            | 0,21   | -         | 1.050,0 kg/j | true          |
        | Additional | AV100.1     | -               | -      | 29 %      | 745,5 kg/j   | false         |
        And the total 'animal housing' emission 'NOx' is '0,0 kg/j'
        And the total 'animal housing' emission 'NH3' is '745,5 kg/j'

    Scenario: Delete existing agriculture source - Animal housing with additional rule
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Dierhuisvesting'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I add a system with housing code 'HA1.11' and number of animals '250'
        And I add an additional rule with code 'AR1.1'
        And I save the data and wait for refresh
        When I select the source 'Bron 1'
        And I copy the source 'Bron 1'
        And I copy the source 'Bron 1 (1)'
        Given I delete source 'Bron 1 (1)'
        Then the next sources exists 'Bron 1', 'Bron 1 (2)'
        And the next source does not exist 'Bron 1 (1)'

    Scenario: Create new agriculture source - Animal housing with additional technique Custom: Air scrubber
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Dierhuisvesting'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I add a system with housing code 'HD1.11' and number of animals '2500'
        When I add an additional rule with code 'Eigen specificatie: Luchtwasser'
        And I fill in reduction of emission with '38'%
        And I fill in the explanation with the following description 'xxx'
        Then I validate the animal housing subsource to have the following values
        | typeOfRule | housingCode | numberOfAnimals | factor | reduction | emission   | strikeThrough |
        | Standard   | HD1.11      | 2500            | 0,21   | -         | 525,0 kg/j | true          |
        | Additional | Eigen       | -               | -      | 38 %      | 325,5 kg/j | false         |
        And I save the data and wait for refresh
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And I validate the animal housing subsource to have the following values
        | typeOfRule | housingCode | numberOfAnimals | factor | reduction | emission   | strikeThrough |
        | Standard   | HD1.11      | 2500            | 0,21   | -         | 525,0 kg/j | true          |
        | Additional | Eigen       | -               | -      | 38 %      | 325,5 kg/j | false         |
        And the total 'animal housing' emission 'NOx' is '0,0 kg/j'
        And the total 'animal housing' emission 'NH3' is '325,5 kg/j'

    Scenario: Create new agriculture source - Animal housing with additional technique Custom: Other
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Dierhuisvesting'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I add a system with housing code 'HD1.11' and number of animals '25000'
        When I add an additional rule with code 'Eigen specificatie: Overig'
        And I fill in reduction of emission with '32'%
        And I fill in the explanation with the following description 'xxx'   
        Then I validate the animal housing subsource to have the following values
        | typeOfRule | housingCode | numberOfAnimals | factor | reduction | emission     | strikeThrough |
        | Standard   | HD1.11      | 25000           | 0,21   | -         | 5.250,0 kg/j | true          |
        | Additional | Eigen       | -               | -      | 32 %      | 3.570,0 kg/j | false         |
        And I save the data and wait for refresh
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And I validate the animal housing subsource to have the following values
        | typeOfRule | housingCode | numberOfAnimals | factor | reduction | emission     | strikeThrough |
        | Standard   | HD1.11      | 25000           | 0,21   | -         | 5.250,0 kg/j | true          |
        | Additional | Eigen       | -               | -      | 32 %      | 3.570,0 kg/j | false         |
        And the total 'animal housing' emission 'NH3' is '3.570,0 kg/j'

    Scenario: Error message if combination of housing code and additional rule is not allowed - Additional technique
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Dierhuisvesting'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        When I add a system with housing code 'HD1.11' and number of animals '2500'
        And I add an additional rule with code 'AV100.1'
        And I fill in the housing code as 'HA3.1'
        Then a animal housing error is visible that the combination is not allowed
        When I save the data
        Then the following error is visible: 'Er zijn foutmeldingen, los deze eerst op'
        When I delete the additional rule
        And I add an additional rule with code 'LW4.6'        
        And I save the data and wait for refresh
        And I select the source 'Bron 1'
        And the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And I validate the animal housing subsource to have the following values
        | typeOfRule | housingCode | numberOfAnimals | factor | reduction | emission     | strikeThrough |
        | Standard   | HA3.1       | 2500            | 2,5    | -         | 6.250,0 kg/j | true          |
        | Additional | LW4.6       | -               | -      | 85 %      | 937,5 kg/j   | false         |
        And the total 'animal housing' emission 'NOx' is '0,0 kg/j'
        And the total 'animal housing' emission 'NH3' is '937,5 kg/j'

    Scenario: Create new agriculture source - Validate available additional rules 
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Dierhuisvesting'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I add a system with housing code 'HA1.1' and number of animals '25000'
        Then I validate that only the following additional rules is available
        | rule                                            |
        | Eigen specificatie: Luchtwasser                 |
        | Eigen specificatie: Overig                      |
        | AR1.1 Beweiden                                  |

    Scenario: Create new agriculture source - Validate 0 number of animals 
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Dierhuisvesting'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I add a system with housing code 'HA1.1' and number of animals '0'
        When I save the data
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        Then I validate the animal housing subsource to have the following values
        | typeOfRule | housingCode | numberOfAnimals | factor | reduction | emission     | strikeThrough |
        | Standard   | HA1.1       | 0               | 5,7    |           | 0,0 kg/j     | false         |

    Scenario: Create new agriculture source - Validate available additional rules 
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Dierhuisvesting'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I add a system with housing code 'HE1.1.3' and number of animals '2500'
        When I add an additional rule with code 'Eigen specificatie: Luchtwasser'
        Then I validate that animal housing constraining footnote is 'visible'
        And I delete the additional rule
        Then I validate that animal housing constraining footnote is 'not visible'

    @Smoke
    Scenario: Create new agriculture source - Animal housing emissions with custom specification
        Given I login with correct username and password
        And I create a new source from the start page
        And I fill in the fields for sector group 'Landbouw' with sector 'Dierhuisvesting'
        And I open the subsource dropdown
        When I add a animal housing system and fill in the custom specification fields with the following data
        | animalType    | factor | numberOfAnimals |
        | Rundvee       | 5      | 100             |
        | Schapen       | 10     | 200             |
        | Geiten        | 15     | 300             |
        | Varkens       | 20     | 400             |
        | Kippen        | 25     | 500             |
        | Kalkoenen     | 30     | 600             |
        | Paarden       | 35     | 600             |
        | Eenden        | 40     | 700             |
        | Pelsdieren    | 45     | 800             |
        | Konijnen      | 50     | 900             |
        | Parelhoenders | 55     | 1000            |
        | Struisvogels  | 60     | 2000            |
        | Overige       | 65     | 3000            |
        And I save the data and wait for refresh
        When I select the source 'Bron 1'
        Then the following animal housing systems with custom specification are created
        | description   | numberOfAnimals | factor | reduction | emission     |
        | Rundvee       | 100             | 5      |           | 500,0 kg/j   |
        | Schapen       | 200             | 10     |           | 2.000,0 kg/j |
        | Geiten        | 300             | 15     |           | 4.500,0 kg/j |
        | Varkens       | 400             | 20     |           | 8.000,0 kg/j |
        | Kippen        | 500             | 25     |           | 12,5 ton/j   |
        | Kalkoenen     | 600             | 30     |           | 18,0 ton/j   |
        | Paarden       | 600             | 35     |           | 21,0 ton/j   |
        | Eenden        | 700             | 40     |           | 28,0 ton/j   |
        | Pelsdieren    | 800             | 45     |           | 36,0 ton/j   |
        | Konijnen      | 900             | 50     |           | 45,0 ton/j   |
        | Parelhoenders | 1000            | 55     |           | 55,0 ton/j   |
        | Struisvogels  | 2000            | 60     |           | 120,0 ton/j  |
        | Overige       | 3000            | 65     |           | 195,0 ton/j  |
        And the total 'animal housing' emission 'NOx' is '0,0 kg/j'
        And the total 'animal housing' emission 'NH3' is '545,5 ton/j'
        When I choose to edit the source
        Then the animal housing systems contains the following icons and data in the list
        | icon                    | amount | description   | emission     |
        | icon-animal-cow         | 100    | Rundvee       | 500,0 kg/j   |
        | icon-animal-sheep       | 200    | Schapen       | 2.000,0 kg/j |
        | icon-animal-goat        | 300    | Geiten        | 4.500,0 kg/j |
        | icon-animal-pig         | 400    | Varkens       | 8.000,0 kg/j |
        | icon-animal-chicken     | 500    | Kippen        | 12,5 ton/j   |
        | icon-animal-turkey      | 600    | Kalkoenen     | 18,0 ton/j   |
        | icon-animal-horse       | 600    | Paarden       | 21,0 ton/j   |
        | icon-animal-duck        | 700    | Eenden        | 28,0 ton/j   |
        | icon-animal-mink        | 800    | Pelsdieren    | 36,0 ton/j   |
        | icon-animal-rabbit      | 900    | Konijnen      | 45,0 ton/j   |
        | icon-animal-guinea-fowl | 1000   | Parelhoenders | 55,0 ton/j   |
        | icon-animal-ostrich     | 2000   | Struisvogels  | 120,0 ton/j  |
        |                         | 3000   | Overige       | 195,0 ton/j  |

    Scenario: Edit agriculture source - Animal housing emissions with custom specifications
        Given I login with correct username and password
        And I create a new source from the start page
        And I fill in the fields for sector group 'Landbouw' with sector 'Dierhuisvesting'
        And I add a animal housing system
        And I choose animal housing system of type 'Eigen specificatie'
        And I fill in the fields with description 'Stalsysteem met kippen', animal type 'Kippen', factor '5' and number of animals '15'
        And I validate the subsource with custom specification 'Stalsysteem met kippen' with number of animals '15' and emission '75,0 kg/j'
        And I save the data and wait for refresh
        When I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        Then the custom specification with description 'Stalsysteem met kippen', number of animals '15', factor '5', reduction '' and emission '75,0 kg/j' is correctly created
        When I choose to edit the source
        And I select animal housing system with housing code 'Stalsysteem met kippen'
        And I fill in the fields with description 'Stalsysteem met konijnen', animal type 'Konijnen', factor '5' and number of animals '175'
        And I validate the subsource with custom specification 'Stalsysteem met konijnen' with number of animals '175' and emission '875,0 kg/j'
        And I save the data and wait for refresh
        Then the custom specification with description 'Stalsysteem met konijnen', number of animals '175', factor '5', reduction '' and emission '875,0 kg/j' is correctly created
        And the total 'animal housing' emission 'NOx' is '0,0 kg/j'
        And the total 'animal housing' emission 'NH3' is '875,0 kg/j'

    Scenario: Import and copy existing agriculture source - Animal housing emissions
        Given I login with correct username and password
        And I open the file dialog to import
        And I select file 'AERIUS_gml_landbouw_energie_bron.gml' to be imported
        And I open the source list
        When I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:136319,74 Y:577666,32' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '25,000' and diurnal variation type 'Dierverblijven' is correctly saved
        When I select the source 'Bron 2'
        Then the source 'Bron 2' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:52884,22 Y:430148,88' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '1.500,0 kg/j'
        And the total 'generic' emission 'NH3' is '1.800,0 kg/j'
        When I copy the source 'Bron 2'
        And I select the source 'Bron 2 (1)'
        Then the source 'Bron 2 (1)' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:52884,22 Y:430148,88' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '1.500,0 kg/j'
        And the total 'generic' emission 'NH3' is '1.800,0 kg/j'

    Scenario: Import agriculture source - validate emission animal housing emissions details
        Given I login with correct username and password
        And I open the file dialog to import
        And I select file 'AERIUS_gml_landbouw_energie_bron.gml' to be imported
        And I open the source list
        When I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:136319,74 Y:577666,32' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '25,000' and diurnal variation type 'Dierverblijven' is correctly saved
        And the animal housing system with housing code 'HE1.1.2.1', number of animals '1000', factor '0,02', reduction '' and emission '20,0 kg/j' is correctly created
        And the animal housing system with custom descripton 'Sierkippen', number of animals '50', factor '2', reduction '' and emission '100,0 kg/j' is correctly created
        And the total 'animal housing' emission 'NOx' is '0,0 kg/j'
        And the total 'animal housing' emission 'NH3' is '120,0 kg/j'

    Scenario: Import agriculture source - validate emission animal housing emissions details
        Given I login with correct username and password
        And I open the file dialog to import
        And I select file "AERIUS_old_CalculationAnimalHousingSource.gml" to be imported, but I don't upload it yet
        And I expect 5 import 'warnings' to be shown
        And I validate the following warnings
            | warning                                                                                                                                                                                                                                                                                                                                                                                                          | row |
            | Het GML-bestand is geldig maar bevat niet de meest recente IMAER versie, gevonden versie 'V5_1' verwacht 'V6_0'                                                                                                                                                                                                                                                                                                  | 0   |
            | De bron 'Bron 1' ('ES.1') bevat een code 'H1.1', 'Overig' die niet converteerbaar is van de RAV-systematiek naar de nieuwe Omgevingswet dierhuisvesting. In plaats daarvan wordt de bron geïmporteerd als eigen specificatie - dierhuisvesting. Let op: de emissiefactor moet handmatig worden toegevoegd. Additionele technieken zijn niet automatisch geconverteerd en dienen handmatig te worden toegevoegd.  | 1   |
            | De bron 'Bron 1' ('ES.1') bevat een code 'A1.1', 'BB93.06.009' die is geconverteerd van de RAV-systematiek naar de Omgevingswet dierhuisvesting. Additionele technieken zijn niet automatisch geconverteerd en dienen handmatig te worden toegevoegd.                                                                                                                                                            | 2   |
            | De bron 'Bron 1' ('ES.1') bevat een code 'A4.1', 'BWL2013.08' die is geconverteerd van de RAV-systematiek naar de Omgevingswet dierhuisvesting.                                                                                                                                                                                                                                                                  | 3   |
            | Een subbron van bron 'Bron 1' heeft geen emissie.                                                                                                                                                                                                                                                                                                                                                                | 4   |
    