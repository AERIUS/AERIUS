#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: AERIUS Check, Calculation results
    Test calculation result pages product Check.

    @Smoke
    Scenario: Create new Farm source start calculation and validate result screens (reference)
        Given I login with correct username and password
        And I create a new source from the start page
        When I name the source 'Farmer stopping' and select sector group 'Landbouw' and sector 'Stalemissies'
        And I fill location with coordinates 'POINT(185032.22 482922.49)'
        And I add a lodging system with RAV code 'A 1.1', number of animals '500' and BWL code 'BB 93.06.009'
        And I validate the subsource with RAV code 'A1.1' with number of animals '500' and emission '2.850,0 kg/j'
        And I save the data
        And I navigate to the 'Calculation jobs' tab                
        When I create a new calculation job
        And I save the calculation job
        And I start the calculation
        And I navigate to the 'Results' tab

        Then the following notification is displayed 'Rekentaak 1 is gestart.'
        Then the following notification is displayed 'Rekentaak 1 is voltooid.'
        
        Then I validate that the situation 'Situatie 1 - Referentie' has results combinations
            | resultSelectId     | resultSelectLabel       |
            | resultsResultType  | Situatieresultaat       |
            | resultsHexagonType | OwN2000-registratieset  |
            | resultsHexagonType | Relevante hexagonen     |
            | resultsResultType  | Depositievracht         |
            | resultsHexagonType | Lbv                     |
            | resultsHexagonType | Lbv-plus                |

        When I select situation with label 'Situatie 1 - Referentie' and validate the 'Depositievracht' result screen with mock results for the result 'Lbv'
            | elementId              | expectedResult | label                          |
            | countReceptors         | 57518          | Berekende hexagonen            |
            | sumCartographicSurface | 51.570,93      | Berekend (ha gekarteerd)       |
            | sumContribution        | 37.983         | Depositievracht (mol N/jr)     |
            | maxContribution        | 106,80         | Hoogste bijdrage (mol N/ha/jr) |
        Then I validate procurement policy threshold for the results 'Lbv'
            | assessmentAreaId | assessmentAreaName | procurementPolicyThresholdPercentage | procurementPolicyThresholdValue | procurementPolicyThresholdCheck | sumContribution |
            | 57               | Veluwe             | 716.1                                | 5.300                           | true                            | 37.954          |
            | 38               | Rijntakken         | 54.9044                              | 53                              | false                           | 29              |

        When I select situation with label 'Situatie 1 - Referentie' and validate the 'Depositievracht' result screen with mock results for the result 'Lbv-plus'
            | elementId              | expectedResult | label                          |
            | countReceptors         | 54601          | Berekende hexagonen            |
            | sumCartographicSurface | 49.149,65      | Berekend (ha gekarteerd)       |
            | sumContribution        | 36.700         | Depositievracht (mol N/jr)     |
            | maxContribution        | 106,80         | Hoogste bijdrage (mol N/ha/jr) |
        Then I validate procurement policy threshold for the results 'Lbv-plus'
            | procurementPolicyThresholdPercentage | procurementPolicyThresholdValue | procurementPolicyThresholdCheck | sumContribution |
            | 1467.99064                           |  2.500                          | true                            | 36.700          |

        Then I select situation with label 'Situatie 1 - Referentie' and validate the 'Situatieresultaat' result screen with mock results for the result 'Relevante hexagonen'
            | elementId              | expectedResult | label                                  |
            | sumCartographicSurface | 54.009,03      | Berekend (ha gekarteerd)               |
            | maxTotal               | 7.201,70       | Hoogste totale depositie (mol N/ha/jr) |
            | maxContribution        | 117,40         | Hoogste bijdrage (mol N/ha/jr)         |
        And I check if the deposition tab has 2 results
        And I check if the markers tab has 2 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the habitat tab has 2 results

        Then I select situation with label 'Situatie 1 - Referentie' and validate the 'Situatieresultaat' result screen with mock results for the result 'OwN2000-registratieset'
            | elementId              | expectedResult | label                                  |
            | sumCartographicSurface | 51.570,93      | Berekend (ha gekarteerd)               |
            | maxTotal               | 7.201,70       | Hoogste totale depositie (mol N/ha/jr) |
            | maxContribution        | 106,80         | Hoogste bijdrage (mol N/ha/jr)         |
        And I check if the deposition tab has 2 results
        And I check if the markers tab has 2 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the habitat tab has 2 results

    Scenario: Create new Farm source start calculation and validate the boundary value (100%)
        Given I login with correct username and password
        And I create a new source from the start page
        When I name the source 'Farmer stopping' and select sector group 'Landbouw' and sector 'Stalemissies'
        And I fill location with coordinates 'POINT(185032.22 482922.49)'
        And I add a lodging system with RAV code 'A 1.1', number of animals '500' and BWL code 'BB 93.06.009'
        And I validate the subsource with RAV code 'A1.1' with number of animals '500' and emission '2.850,0 kg/j'
        And I save the data
        And I navigate to the 'Calculation jobs' tab
        When I create a new calculation job
        And I save the calculation job
        And I start the calculation
        And I navigate to the 'Results' tab

        Then the following notification is displayed 'Rekentaak 1 is gestart.'
        Then the following notification is displayed 'Rekentaak 1 is voltooid.'

        Then I validate that the situation 'Situatie 1 - Referentie' has results combinations
            | resultSelectId     | resultSelectLabel |
            | resultsHexagonType | Lbv-plus          |
        And I validate procurement policy threshold for the results 'Lbv-plus'
            | procurementPolicyThresholdPercentage | procurementPolicyThresholdValue | procurementPolicyThresholdCheck | sumContribution |
            | 1467.9                               | 2.500                           | true                            | 36.700          |
