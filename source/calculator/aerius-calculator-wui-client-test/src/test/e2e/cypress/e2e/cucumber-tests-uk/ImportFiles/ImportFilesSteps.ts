/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";
import ImportFilesPage from "./ImportFilesPage";
import { AssessmentPointsHabitatsPage } from "../AssessmentPoints/AssessmentPointsHabitatsPage";
import { CommonAssessmentPointsPage } from "../../common/CommonAssessmentPointsPage";

Given("I expect import errors to be shown", () => {
  ImportFilesPage.assertImportErrorsShown();
});

Given("I expect to be in the introduction screen", () => {
  ImportFilesPage.assertIntroductionScreenIsShown();
});

Given("I select the following sources and check their ADMS details", (dataTable: any) => {
  ImportFilesPage.checkSourceADMSDetails(dataTable);
});

Given("I open the road transport network panel so the underlying sources can be selected", () => {
  ImportFilesPage.openRoadTransportNetworkPanel();
});

Given("I check the following traffic sources and check their details", (dataTable: any) => {
  ImportFilesPage.checkSourceDetailsForTrafficSource(dataTable);
});

Given("I check for the following housing subsource details for source {string}", (sourceName: string, dataTable: any) => {
  ImportFilesPage.checkHousingEmissions(sourceName, dataTable);
});

Given("I check for farmland subsource details for source {string}", (sourceName: string, dataTable: any) => {
  ImportFilesPage.checkFarmlandSubsourceDetails(sourceName, dataTable);
});

Given("I check for building info for the following sources", (dataTable: any) => {
  ImportFilesPage.checkBuildingsInfluencingSources(dataTable);
});

Given("I check the custom time-varying profiles for values", (dataTable: any) => {
  ImportFilesPage.checkTimeVaryingProfiles(dataTable);
});

Given("the habitat list exists and has {int} habitats", (nrOfHabitats: number) => {
  AssessmentPointsHabitatsPage.assertHabitatsListExistsAndHasHabitats(nrOfHabitats);
});

Given("I select habitat {string}", (habitatName: string) => {
  CommonAssessmentPointsPage.selectHabitat(habitatName);
});

Given("the selected habitat has the following data", (dataTable: any) => {
  AssessmentPointsHabitatsPage.assertHabitatData(dataTable.hashes()[0]);
});
