/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { CommonSettings } from "../../common/CommonSettings";

export class GenericUIPage {
  
  static saveSessionId() {
    cy.wrap(Math.floor(Date.now() / 1_000).toString(36)).as("id");
  }

  // Language
  static switchLanguage(language: string, expectReload = true) {
    if (expectReload) {
      // Change language and await reload completion
      cy.get("@id").then((uniqueId) => {
        
        cy.intercept("/application/v2/service/context").as("getContext_" + uniqueId);
        cy.contains(".row", language).click();
        cy.wait("@getContext_" + uniqueId, { timeout: CommonSettings.responseTimeOut });
      });
    } else {
      // Just change language
      cy.contains(".row", language).click();
    }
  }

  static currentLanguage(language: string) {
    cy.contains(".selected", language).should("have.text", language);
  }

  static assertLanguageScreenIsClosed() {
    cy.get('[id="language-header"]').should("not.exist");
  }
}
