#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Generic emission source

    @Smoke
    Scenario: Edit buttons should be disabled when no source is selected
        Given I create a simple emission source
        Then the edit buttons are disabled
        Then I select the source 'Bron 1'
        Then the edit buttons are enabled

    Scenario: Create and delete an emission source
        Given I create a simple emission source
        Then I select the source 'Bron 1'
        And I delete the selected sources
        Then there should be no sources
    
    Scenario: Check error on empty scenario name
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I add situation label ''
        Then I validate situation label error message 'Dit veld is verplicht'

    @Smoke
    Scenario: Create multiple sources and then delete a specific emission source
        Given I create a simple emission source
        And I create another simple emission source named 'Bron 2'
        And I create another simple emission source named 'Bron 3'
        Then I select the source 'Bron 2'
        And I delete the selected sources
        Then there should be 2 sources
        Then the source 'Bron 1' should exist
        Then the source 'Bron 3' should exist

    Scenario: Create multiple sources and then delete some of them
        Given I create a simple emission source
        And I create another simple emission source named 'Bron 2'
        And I create another simple emission source named 'Bron 3'
        Then I select the source 'Bron 2'
        And I multiselect the source 'Bron 3'
        And I delete the selected sources
        Then there should be 1 sources
        Then the source 'Bron 1' should exist

    Scenario: Create and then delete multiple sources
        Given I create a simple emission source
        And I create another simple emission source named 'Bron 2'
        And I create another simple emission source named 'Bron 3'
        And I delete all sources
        Then there should be no sources

    Scenario: Open and close emission source detail view
        Given I create a simple emission source
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Energie' is correctly saved
        Given I click the detail view close button
        Then the detail view should not be visible

    Scenario: Create and duplicate an emission source
        Given I create a simple emission source
        And I create another simple emission source named 'Bron 2'
        And I create another simple emission source named 'Bron 3'
        Then I select the source 'Bron 2'
        And I duplicate the selected sources
        Then there should be 4 sources
        Then the source 'Bron 2 (1)' with sector group 'Energie' is correctly saved
        And I duplicate the selected sources
        Then there should be 5 sources
        Then the source 'Bron 2 (2)' with sector group 'Energie' is correctly saved
        Then I select the source 'Bron 2'
        And I duplicate the selected sources
        Then there should be 6 sources
        Then the source 'Bron 2 (3)' with sector group 'Energie' is correctly saved

    Scenario: Create and duplicate multiple emission sources
        Given I create a simple emission source
        And I create another simple emission source named 'Bron 2'
        And I create another simple emission source named 'Bron 3'
        Then I select the source 'Bron 2'
        And I multiselect the source 'Bron 3'
        And I duplicate the selected sources
        Then there should be 5 sources
        Then the source 'Bron 2 (1)' with sector group 'Energie' is correctly saved
        Then the source 'Bron 2 (1)' is selected in the overview
        Then the source 'Bron 3 (1)' is selected in the overview
        And I duplicate the selected sources
        Then there should be 7 sources
        Then the source 'Bron 2 (2)' is selected in the overview
        Then the source 'Bron 3 (2)' is selected in the overview
        And I multiselect the source 'Bron 2'
        And I duplicate the selected sources
        Then there should be 10 sources
        Then the source 'Bron 2 (3)' is selected in the overview
        Then the source 'Bron 2 (4)' is selected in the overview
        Then the source 'Bron 3 (3)' is selected in the overview

    @Smoke
    Scenario: Check detail screen functionality for multiselect, with duplication and deletion
        Given I create a simple emission source
        And I create another simple emission source named 'Bron 2'
        And I create another simple emission source named 'Bron 3'
        Then I select the source 'Bron 2'
        And I multiselect the source 'Bron 3'
        Then the source 'Bron 2' with sector group 'Energie' is correctly saved
        Given I click the detail view close button
        Then the detail view should not be visible
        Then no source should be selected in the overview
        Then I select the source 'Bron 2'
        And I multiselect the source 'Bron 3'
        And I multiselect the source 'Bron 2'
        Then the source 'Bron 3' with sector group 'Energie' is correctly saved
        Given I click the detail view close button
        Then I select the source 'Bron 3'
        And I duplicate the selected sources
        Then the source 'Bron 3 (1)' with sector group 'Energie' is correctly saved
        And I delete the selected sources
        Then the detail view should not be visible
        Then I select the source 'Bron 1'
        And I multiselect the source 'Bron 2'
        And I duplicate the selected sources
        Then the source 'Bron 1 (1)' with sector group 'Energie' is correctly saved
        And I delete the selected sources
        Then the detail view should not be visible
        Then no source should be selected in the overview

    Scenario: Create emission source and then edit its name
        Given I create a simple emission source
        And I select the source 'Bron 1'
        And I choose to edit the source 'Bron 1'
        When I edit the name of the source to 'Bron gewijzigd'
        And I open emission source list
        Then the source 'Bron gewijzigd' with sector group 'Energie' is correctly saved

    Scenario: Create emission source and then edit its sector group
        Given I create a simple emission source
        And I select the source 'Bron 1'
        And I choose to edit the source 'Bron 1'
        When I edit the sector group of the source to 'Anders...'
        And I open emission source list
        Then the source 'Bron 1' with sector group 'Anders...' is correctly saved

    Scenario: Create emission source and then edit its sector
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Industrie' and sector 'Afvalverwerking'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I save the data
        And I select the source 'Bron 1'
        And I choose to edit the source 'Bron 1'
        When I edit the sector of the source to 'Basismetaal'
        And I open emission source list
        Then the source 'Bron 1' with sector group 'Industrie' and sector 'Basismetaal' is correctly saved

    Scenario: Create emission source and then edit its location
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Industrie' and sector 'Afvalverwerking'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I save the data
        When I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Industrie' and sector 'Afvalverwerking' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And I choose to edit the source 'Bron 1'
        And I change location to coordinates 'LINESTRING(200380.4 493929.51,192361.53 488279.85,211861.97 487186.37)'
        When I save the data
        Then the source 'Bron 1' with sector group 'Industrie' and sector 'Afvalverwerking' is correctly saved
        And location with coordinates 'X:197214,83 Y:488007,7' is correctly saved

    @Smoke
    Scenario: Create emission sourcen and then edit its emissions
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Industrie' and sector 'Afvalverwerking'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I fill the emission fields with NOx '9999' and NH3 '0'
        And I save the data
        When I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Industrie' and sector 'Afvalverwerking' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And the total 'generic' emission 'NOx' is '9.999,0 kg/j'
        And the total 'generic' emission 'NH3' is '0,0 kg/j'
        And I choose to edit the source 'Bron 1'
        And I fill the emission fields with NOx '0' and NH3 '9999'
        When I save the data
        Then the source 'Bron 1' with sector group 'Industrie' and sector 'Afvalverwerking' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And the total 'generic' emission 'NOx' is '0,0 kg/j'
        And the total 'generic' emission 'NH3' is '9.999,0 kg/j'

    @Smoke
    Scenario: Edit source characteristics of emission source
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Industrie' and sector 'Afvalverwerking'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I fill the emission fields with NOx '9999' and NH3 '0'
        And I save the data
        When I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Industrie' and sector 'Afvalverwerking' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '6,0', heat content '0,100' and diurnal variation type 'Continue Emissie' is correctly saved
        And the total 'generic' emission 'NOx' is '9.999,0 kg/j'
        And the total 'generic' emission 'NH3' is '0,0 kg/j'
        And I choose to edit the source 'Bron 1'
        And I choose heat content type 'Geforceerd', emission height '15', emission temperature '12', outflow diameter '0.3', outflow direction type 'Horizontaal' and outflow velocity '1.0'
        When I save the data
        Then the source 'Bron 1' with sector group 'Industrie' and sector 'Afvalverwerking' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And source characteristics with heat content type 'Geforceerd', building influence 'Geen', emission height '15,0 m', emission temperature '12,00 °C', outflow diameter '0,3', outflow direction type 'Horizontaal', outflow velocity '1,0' and diurnal variation type 'Continue Emissie' is correctly saved
        And the total 'generic' emission 'NOx' is '9.999,0 kg/j'
        And the total 'generic' emission 'NH3' is '0,0 kg/j'

    Scenario: Edit subsource
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Landbouwgrond'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I add a new subsource
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Landbouwgrond' is correctly saved
        When I choose to edit the source 'Bron 1'
        And I edit the fields of the subsource
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Landbouwgrond' is correctly saved
        And the subsource is correctly saved
        Then the total 'farmland' emission 'NOx' is '0,0 kg/j'
        And the total 'farmland' emission 'NH3' is '200,0 kg/j'

    Scenario: Copy subsource and delete subsource
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Landbouwgrond'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I add a new subsource
        When I select the source 'Bron 1'
        And I choose to edit the source 'Bron 1'
        And I duplicate the selected subsource
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Landbouwgrond' is correctly saved
        And the subsource and the duplicated subsource are correctly saved
        And the total 'farmland' emission 'NOx' is '20,0 ton/j'
        And the total 'farmland' emission 'NH3' is '2.000,0 kg/j'
        And I close source detail screen
        When I select the source 'Bron 1'
        And I choose to edit the source 'Bron 1'
        And I delete the selected subsource 'Beweiding'
        Then there should be 1 subsources of subsource 'Beweiding'
        And I save the data
        And the total 'farmland' emission 'NOx' is '9.999,0 kg/j'
        And the total 'farmland' emission 'NH3' is '1.000,0 kg/j'

    Scenario: Default drawing type LINE - Traffic
        Given I login with correct username and password
        And I create a new source from the start page
        #And I name the source 'Bron 1' and select sector group 'Rijdend verkeer'
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Rijdend verkeer'
        Then the default drawing type is 'Lijnbron'
        When I select drawing type 'Puntbron'
        Then the following warning is visible: 'Deze bron moet van een ander geometrietype zijn.'
        When I select drawing type 'Lijnbron'
        Then the warning is not visible
        When I select drawing type 'Vlakbron'
        Then the following warning is visible: 'Deze bron moet van een ander geometrietype zijn.'

    Scenario: Default drawing type LINE - Maritime shipping - Dock
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Aanlegplaats'
        Then the default drawing type is 'Puntbron'
        And the warning is not visible
        When I select drawing type 'Lijnbron'
        Then the warning is not visible
        When I select drawing type 'Vlakbron'
        Then the warning is not visible

    Scenario: Default drawing type LINE - Maritime shipping - inland route
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Binnengaats route'
        Then the default drawing type is 'Lijnbron'
        When I select drawing type 'Puntbron'
        Then the following warning is visible: 'Deze bron moet van een ander geometrietype zijn.'
        When I select drawing type 'Lijnbron'
        Then the warning is not visible
        When I select drawing type 'Vlakbron'
        Then the following warning is visible: 'Deze bron moet van een ander geometrietype zijn.'

    Scenario: Default drawing type LINE - Maritime shipping - maritime route
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Zeeroute'
        Then the default drawing type is 'Lijnbron'
        When I select drawing type 'Puntbron'
        Then the following warning is visible: 'Deze bron moet van een ander geometrietype zijn.'
        When I select drawing type 'Lijnbron'
        Then the warning is not visible
        When I select drawing type 'Vlakbron'
        Then the following warning is visible: 'Deze bron moet van een ander geometrietype zijn.'

    Scenario: Default drawing type LINE - Inland shipping - Dock
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Scheepvaart' and sector 'Binnenvaart: Aanlegplaats'
        Then the default drawing type is 'Puntbron'
        And the warning is not visible
        When I select drawing type 'Lijnbron'
        Then the warning is not visible
        When I select drawing type 'Vlakbron'
        Then the warning is not visible

    Scenario: Default drawing type LINE - Inland shipping - Route
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Scheepvaart' and sector 'Binnenvaart: Vaarroute'
        Then the default drawing type is 'Lijnbron'
        When I select drawing type 'Puntbron'
        Then the following warning is visible: 'Deze bron moet van een ander geometrietype zijn.'
        When I select drawing type 'Lijnbron'
        Then the warning is not visible
        When I select drawing type 'Vlakbron'
        Then the following warning is visible: 'Deze bron moet van een ander geometrietype zijn.'

    Scenario: Default drawing type POINT - sources other then traffic and shipping
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        Then the default drawing type is 'Puntbron'
        And the warning is not visible
        When I select drawing type 'Lijnbron'
        Then the warning is not visible
        When I select drawing type 'Vlakbron'
        Then the warning is not visible

    @Smoke
    Scenario: Visibility privacy warning
        Given I login with correct username and password
        And I create a new source from the start page
        # personal data warning when adding a source
        When I name the source 'Bron 1'
        Then the following warning is visible: "Pas op met het invoeren van persoonlijke gegevens in dit veld"
        When I fill in the fields for sector group 'Energie'
        And I save the data
        # personal data warning when editing the source
        When I select the source 'Bron 1'        
        When I choose to edit the source 'Bron 1'
        And I name the source 'Bron 2'
        Then the following warning is visible: "Pas op met het invoeren van persoonlijke gegevens in dit veld"
        # personal data warning not visible after clicking on button do not shown again 
        When I choose not to show the privacy warning again
        And I save the data
        And I choose to edit the source 'Bron 2'
        And I name the source 'Bron 3'
        Then the warning is not visible

    Scenario: Location panel default open
        Given I login with correct username and password
        When I create a new source from the start page
        Then the location panel should be open
        
    Scenario: Length of line source is visible
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        When I fill location with coordinates 'LINESTRING(109361.79 535106.82,109544.26 535490,109927.43 535736.32)'
        Then the value 'Lengte: 879,92 m' is visible in the location panel
        And I save the data
        When I select the source 'Bron 1'
        Then location with coordinates 'X:109557,34 Y:535498,41' is correctly saved
        And length of location with value '879,92 m' is correctly saved 

    Scenario: Surface of polygon source is visible
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        When I fill location with coordinates 'POLYGON((104710.69 478135.91,104641.56 478066.79,104726.05 477997.66,104795.17 478074.47,104710.69 478135.91))'
        Then the value 'Oppervlakte: 1,07 ha' is visible in the location panel
        And I save the data
        When I select the source 'Bron 1'
        Then location with coordinates 'X:104714,91 Y:478066,79' is correctly saved
        And surface of location with value '1,07 ha' is correctly saved 

    @Smoke
    Scenario: Spread is only visible for polygon sources
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        # POINT source - spread not visible
        When I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I open collapsible panel 'Bronkenmerken'
        Then the spread for the source is not visible
        # LINESTRING source - spread not visible
        When I open collapsible panel 'Locatie'
        And I fill location with coordinates 'LINESTRING(109361.79 535106.82,109544.26 535490,109927.43 535736.32)'
        When I open collapsible panel 'Bronkenmerken'
        Then the spread for the source is not visible
        # POLYGON source - spread is visible
        When I open collapsible panel 'Locatie'
        And I fill location with coordinates 'POLYGON((104710.69 478135.91,104641.56 478066.79,104726.05 477997.66,104795.17 478074.47,104710.69 478135.91))'
        When I open collapsible panel 'Bronkenmerken'
        Then the spread for the source is visible
        # Bug AER-1729
        # When I fill in '25,9' as spread
        # And I save the data
        # When I select the source 'Bron 1'
        # Then the spread is correctly saved with value '25,9 m'

    Scenario: Warning intersection of area source
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        When I fill location with coordinates 'POLYGON((107905.57 517935.55,108132.19 518029.97,108207.73 517803.35,107905.57 517652.27,108207.73 517652.27,107905.57 517935.55)))'
        Then the following warning is visible: "De geometrie mag zichzelf niet kruisen" 
        And I save the data
        Then the following error is visible: "Er zijn foutmeldingen, los deze eerst op." 

    Scenario: Error message invalid geometry
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        And I fill location with coordinates 'point(107905.57 517935.55'
        Then the following warning is visible: 'Deze geometrie is niet geldig. Een voorbeeld van het benodigde formaat is: POINT(142492.58 460229.98). Coördinaten hebben maximaal 2 decimalen.'
        When I select drawing type 'Lijnbron'
        And I fill location with coordinates 'LINESTRING(141744.69 458722.55'
        Then the following warning is visible: 'Deze geometrie is niet geldig. Een voorbeeld van het benodigde formaat is: LINESTRING(141744.69 458722.55,141455.23 458861.56). Coördinaten hebben maximaal 2 decimalen.'
        When I select drawing type 'Vlakbron'
        And I fill location with coordinates 'POLYGON((142444.56 460216.93'
        Then the following warning is visible: 'Deze geometrie is niet geldig. Een voorbeeld van het benodigde formaat is: POLYGON((142444.56 460216.93,142449.5 460206.07,142541.33 460244.57,142535.4 460256.09,142444.56 460216.93)). Coördinaten hebben maximaal 2 decimalen.'
        When I save the data
        Then the following error is visible: 'Er zijn foutmeldingen, los deze eerst op.' 
       
    Scenario: Warning - value outside of range - temperature
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I open collapsible panel 'Bronkenmerken'
        And I choose heat content type 'Geforceerd', emission height '40', emission temperature '12', outflow diameter '0.3', outflow direction type 'Horizontaal' and outflow velocity '1.0'
        And I save the data
        When I select the source 'Bron 1'
        Then source characteristics with heat content type 'Geforceerd', building influence 'Geen', emission height '40,0 m', emission temperature '12,00 °C', outflow diameter '0,3', outflow direction type 'Horizontaal', outflow velocity '1,0' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved

    Scenario: Warning - value outside of range - height
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I open collapsible panel 'Bronkenmerken'
        And I toggle building influenced
        And I add a new building
        And I name the building 'Gebouw met invloed'
        And I fill the building shape with WKT string 'POLYGON((172166.77 599127.49,172167.57 599067.4,172269.05 599078.74,172261.69 599161.92,172166.77 599127.49))'
        And I fill the building height with '5'
        And I save the building
        And I choose heat content type 'Geforceerd', emission height '40', emission temperature '12', outflow diameter '0.3', outflow direction type 'Horizontaal' and outflow velocity '1.0'
        And I save the data
        And I save the data
        When I select the source 'Bron 1'
        And source characteristics with heat content type 'Geforceerd', building influence 'Gebouw met invloed', emission height '40,0 m  (20,0 m)', emission temperature '12,00 °C  (11,85 °C)', outflow diameter '0,3', outflow direction type 'Horizontaal', outflow velocity '1,0' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved

    @Smoke
    Scenario: Warning - value outside of range - barrier height and distance
        Given I login with correct username and password
        And I create a new source from the start page
        #And I name the source 'Bron 1' and select sector group 'Rijdend verkeer'
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Rijdend verkeer'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I open collapsible panel 'Bronkenmerken'
        And I choose road elevation type 'Tunnelbak' and elevation height '20'
        And I choose as road side barrier type from A to B 'Scherm', barrier height '10' and barrier distance '51'
        And I choose as road side barrier type from B to A 'Wal', barrier height '20' and barrier distance '99'
        And I choose road type 'Buitenweg' direction 'Van A naar B'
        And I click to add a emission source of type 'Eigen specificatie'
        And I fill description with 'Eigen weg specificatie'
        And I choose time unit '/etmaal' with number of vehicles '100'
        And I fill emission 'NO2' per vehicle '0.710'
        And I fill emission 'NOX' per vehicle '0.8101'
        And I fill emission 'NH3' per vehicle '0.91011'
        When I save the data validate the warning and wait for refresh
        And I select the source 'Verkeersnetwerk'
        And I select the source 'Bron 1'
        Then source characteristics with road type 'Buitenweg', tunnel factor '1', elevation type 'Tunnelbak' and elevation height '20 m  (12 m)' is correctly saved
        And road side barrier type from A to B 'Scherm', barrier height '10,0 m', barrier height warning '6,0 m', barrier distance '51,0 m' and barrier distance warning '-' is correctly saved
        And road side barrier type from B to A 'Wal', barrier height '20,0 m', barrier height warning '6,0 m', barrier distance '99,0 m' and barrier distance warning '-' is correctly saved

    Scenario: Validate geometry in GUI and when exported to a JSON
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'

        # testing a geometry without decimals 
        And I fill location with coordinates 'POINT(172203 599120)'
        And I choose heat content type 'Niet geforceerd', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:172203 Y:599120' is correctly saved
        When I navigate to the 'Calculation jobs' tab
        And I create a new calculation job
        And I save the calculation job 
        When I choose to export through the menu
        And I fill in the following email address 'test@aerius.nl'
        Then I start an export and validate the request
          |situationNr | featureNr | coordinates     |
          | 0          | 0         | [172203,599120] |
        And I cancel the export

        # testing a geometry with two decimals 
        When I navigate to the 'Input' tab
        And I select the source 'Bron 1'
        And I choose to edit the source 'Bron 1'
        And I open collapsible panel 'Locatie'
        And I fill location with coordinates 'POINT(172203.24 599120.34)'
        And I choose heat content type 'Niet geforceerd', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data
        Then the source 'Bron 1' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:172203,24 Y:599120,34' is correctly saved
        When I navigate to the 'Calculation jobs' tab
        And I create a new calculation job
        And I save the calculation job 
        When I choose to export through the menu
        Then I start an export and validate the request
          |situationNr | featureNr | coordinates           |
          | 0          | 0         | [172203.24,599120.34] |
        And I cancel the export

        # The following tests are turned off because item AER-2361 has been reverted:
        # # Boundary testing: geometry should be rounded upwards (UI)
        # When When I navigate to the 'Input' tab
        # And I choose to edit the source 'Bron 1'
        # And I open collapsible panel 'Locatie'
        # And I fill location with coordinates 'POINT(172203.245 599120.345)'
        # And I save the data
        # Then the source 'Bron 1' with sector group 'Energie' is correctly saved
        # And location with coordinates 'X:172203,25 Y:599120,35' is correctly saved
        # When I choose to export through the menu
        # Then I start an export and validate the request
        #   |situationNr | featureNr | coordinates            |
        #   | 0          | 0         | [17220.245,599120.345] |
        # And I cancel the export        

        # # Boundary testing: geometry should be rounded downwards (UI)
        # When When I navigate to the 'Home' tab
        # And I choose to edit the source 'Bron 1'
        # And I open collapsible panel 'Locatie'        
        # And I fill location with coordinates 'POINT(172203.244 599120.344))'
        # And I save the data
        # Then the source 'Bron 1' with sector group 'Energie' is correctly saved
        # And location with coordinates 'X:172203,24 Y:599120,34' is correctly saved
        # When I choose to export through the menu
        # And I select 'Invoerbestanden' as export type
        # Then I start an export and validate the request
        #   |situationNr | featureNr | coordinates             |
        #   | 0          | 0         | [172203.244,599120.344] |
        # And I cancel the export        

    Scenario: Validate geometry when importing and exporting a GML file
        Given I login with correct username and password
        When I select file 'AERIUS_validate_import_geometry.gml' to be imported
        And I open the source list
        And I select the source 'Bron 1'
        Then location with coordinates 'X:57213,62 Y:427320,74' is correctly saved
        When I choose to export through the menu
        And I select 'Invoerbestanden' as export type
        And I fill in the following email address 'test@aerius.nl'
        Then I start an export and validate the request
          |situationNr | featureNr | coordinates                           |
          | 0          | 0         | [57213.62458245031,427320.7372087822] |
        And I cancel the export   
