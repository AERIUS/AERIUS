/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { CommonSettings } from "./CommonSettings";

export class CommonMaplayersPage {
  static CHECKMARK_PATH: string = "M20.42,25.57l-7-6.44a2,2,0,1,1,2.71-2.95l3.5,3.21L26,9.36a2,2,0,0,1,3.38,2.15Z";
  static DISABLED_LINE_PATH: string = "M27.55,19H13.31a2,2,0,0,1,0-4H27.55a2,2,0,0,1,0,4Z";

  static assertMaplayerVisible(maplayer: string) {
    switch (maplayer) {
      case "Total as percentage of critical level / critical load":
        cy.get(".layerContainer").find('[id="TotalPercentageCriticalLevelResultLayer-line"]').should("be.visible");
        break;
      case "Percentage critical level / critical load":
        cy.get(".layerContainer").find('[id="PercentageCriticalLevelResultLayer-line"]').should("be.visible");
        break;
      default:
        throw new Error("Unknown maplayer: " + maplayer);
    }
  }

  static assertMaplayerDoesNotExist(maplayer: string) {
    switch (maplayer) {
      case "Total percentage critical load":
        cy.get(".layerContainer").find('[id="TotalPercentageCriticalLevelResultLayer-title"]').should("not.exist");
        break;
      case "Percentage critical level":
        cy.get(".layerContainer").find('[id="PercentageCriticalLevelResultLayer-title"]').should("not.exist");
        break;
      default:
        throw new Error("Unknown maplayer: " + maplayer);
    }
  }

  static selectMapbutton(mapbutton: string) {
    let selectableIcon = "";
    switch (mapbutton) {
      case "LayerPanel":
        selectableIcon = "icon-menu-maplayers";
        break;
      default:
        throw new Error("Unknown mapbutton: " + mapbutton);
    }
    cy.get("." + selectableIcon).click();
  }

  static openMaplayer(maplayer: string) {
   if (maplayer === "Stikstofgevoeligheid: Relevante habitats") {
      cy.get('[id="calculator:wms_habitat_areas_sensitivity_view"]').click({ force: true });
    } else {
      cy.get(".layerContainer > div").contains(maplayer).click({force: true});
    }
  }

  static validateMapLayerNoLegenda(mapLayer: string) {
    cy.get('[title="' + mapLayer + '"]').then((content) => {
      cy.wrap(content).should("not.have.class", ".legend");
    });
  }

  static validateMapLayer(mapLayer: string, dataTable: any, options: { title?: string | null, dataType?: string | null, subDataType?: string | null } = {}) {
    if (mapLayer === "Stikstofgevoeligheid: Relevante habitats") {
      cy.get('[id="calculator:wms_habitat_areas_sensitivity_view"]').then((content) => {
        this.validateMapLayerLegenda(content, dataTable, options);
      });
    } else {
      cy.get('[title="' + mapLayer + '"]').then((content) => {
        this.validateMapLayerLegenda(content, dataTable, options);
      });
    }
  }

  static validateMapLayerLegenda(content: any, dataTable: any, options: { title?: string | null, dataType?: string | null, subDataType?: string | null } = {}) {
    if (options.dataType != null) {
      cy.wrap(content).find(".options").eq(0).select(options.dataType);
    }

    if (options.subDataType != null) {
      cy.wrap(content).find(".options").eq(1).select(options.subDataType);
    }

    if (options.title != null) {
      cy.wrap(content).find(".unit").should("have.text", options.title);
    }

    cy.wrap(content).find(".legend .colorLabelContainer .row").should("have.length", dataTable.hashes().length);
    cy.wrap(content).find(".legend .colorLabelContainer .row .label").each((label, index) => {
      cy.wrap(label).should("have.text", dataTable.hashes()[index].value);
    });
  }

  static validateMapLayers(dataTable: any) {
    cy.get(".layerContainer>div").should("have.length", dataTable.hashes().length);
    dataTable.hashes().forEach((layer: any) => {
      cy.log(layer.id);
      cy.log(layer.title);
      cy.get('[id="' + layer.id + '"]').within( () => {
        cy.contains(layer.title);
      });
    });
  }

  static validateHabitatsTypeLayers(dataTable: any) {
      this.validateSubselectionLayers("calculator:wms_habitat_types-line", dataTable);
  }

  static validateHabitatsAreasSensitivityLayers(dataTable: any) {
    this.validateSubselectionLayers("calculator:wms_habitat_areas_sensitivity_view-line", dataTable);
  }

  static validateSubselectionLayers(objectId: string, dataTable: any) {
    cy.get('[id="' + objectId + '"] > select').within( () => {
      cy.get("option").should("have.length", dataTable.hashes().length);
      dataTable.hashes().forEach((layer: any, index: number) => {
        cy.get("option").eq(index).should('have.value', layer.id);
        cy.get("option").eq(index).should('include.text', layer.title);
      });
    });
  }

  static validateBaseLayers(dataTable: any) {
    cy.get("select").last().within( () => {
      cy.get("option").should("have.length", dataTable.hashes().length);
    });
    dataTable.hashes().forEach((layer: any) => {
      cy.get("select").last().select(layer.title).should('have.value', layer.id);
      if (layer.copyright.length > 0) {
        cy.get('.ol-attribution').within( () =>{
          cy.get("li").should("include.text", layer.copyright);
        });
      }
    });
  }

  static validateCriticalLevelSubMaps(dataTable: any) {
    cy.get('[id="TotalPercentageCriticalLevelResultLayer-line"]').click().within( () => {
      cy.get(".toggle").click();
    });
    cy.get('[id="TotalPercentageCriticalLevelResultLayer-content"]').within( () => {
      cy.get(".row").should("have.length", dataTable.hashes().length);
    });
    dataTable.hashes().forEach((layer: any, count: number) => {
      cy.get('[id="TotalPercentageCriticalLevelResultLayer-content"]').contains(layer.title);

      // trigger the chart layer and validate it contains a CQL_FILTER
      cy.intercept("GET", /.*wms__total_percentage_cl_hexagon.*/, {times: 1}).as("wmsResultTotalPercentageCLMapLayers" + count);
      cy.get('[for="filterLegend' + count + '"').parent().within( () =>{
        cy.get(".toggle").click();
      });
      cy.wait("@wmsResultTotalPercentageCLMapLayers" + count, { timeout: CommonSettings.resultsLoadingTimeOut }).then((interception: any) => {
        expect(decodeURI(interception.request.url)).to.contain(layer.cqlFilter);
      });
    });
  }

  static assertMaplayersVisible(dataTable: any) {
    dataTable.hashes().forEach((mapLayer: any) => {
      if (mapLayer.layer === "Habitattypen: Relevante habitattypen") {        
        cy.get('[id="calculator:wms_habitat_types-line"]').parent().find(".toggle").then((element) => {
          this.validateMapLayerStatus(mapLayer.shouldBeEnabled, element);
        });
      } else if (mapLayer.layer === "Stikstofgevoeligheid: Relevante habitats") {
        cy.get('[id="calculator:wms_habitat_areas_sensitivity_view"]').parent().find(".toggle").then((element) => {
          this.validateMapLayerStatus(mapLayer.shouldBeEnabled, element);
        });
      } else {
        cy.get(".layerContainer").contains(mapLayer.layer).parent().find(".toggle").then((element) => {
          this.validateMapLayerStatus(mapLayer.shouldBeEnabled, element);        
        });
      }
    });
  }
  static validateMapLayerStatus(shouldBeEnabled: string, element: any) {
    if (shouldBeEnabled === "yes") {
      cy.wrap(element).find('path[d="' + this.CHECKMARK_PATH + '"]').parent().parent().should("not.have.css", "display", "none"); // Checkmark
      cy.wrap(element).find('path[d="' + this.DISABLED_LINE_PATH + '"]').parent().parent().should("have.css", "display", "none"); // Disabled line
    } else if (shouldBeEnabled === "no") {
      cy.wrap(element).find('path[d="' + this.CHECKMARK_PATH + '"]').parent().parent().should("have.css", "display", "none");
      cy.wrap(element).find('path[d="' + this.DISABLED_LINE_PATH + '"]').parent().parent().should("not.have.css", "display", "none");
    } else {
      throw new Error("Unknown expected maplayer state: " + shouldBeEnabled);
    }
  }
}
