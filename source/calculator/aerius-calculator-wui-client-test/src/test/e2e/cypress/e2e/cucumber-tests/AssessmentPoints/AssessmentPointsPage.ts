/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { CommonSettings } from "../../common/CommonSettings";

export class AssessmentPointsPage {

  static assertNewPointScreenShown() {
    cy.get('[id="locatie-content"]').should("exist");
    cy.get(".confirmContainer").should("exist");
  }

  static pressCreateCalculationPointButton() {
    cy.get('[id="calculationPointsListNewPoint"]').click();
  }

  static openGenerateCalculationPointsPanel() {
    // Open the generation panel
    cy.get('[id="calculationPointsListAutomaticPlacement"]').click();
  }

  static enableCalculationPointsInsideNL() {
    cy.get('[id="checkPointsInland"]').click({ force: true });
  }

  static disableCalculationPointsAbroad() {
    cy.get('[id="checkPointsAbroad"]').click({ force: true });
  }

  static setRadiusTo(radiusInMeters: string) {
    cy.get('[id="radiusPointsInland"]').clear().type(radiusInMeters);
  }

  static assertRadiusErrorIsShown() {
    cy.get(".pointTypes > .noticeContainer").should("exist"); // TODO: This element should have a testID
  }

  static assertNoRadiusErrorIsShown() {
    cy.get(".pointTypes > .noticeContainer").should("not.exist"); // TODO: This element should have a testID
  }

  static cancelGenerationOfCalculationPoints() {
    cy.get(".btnCancel").click(); // TODO: This element should have a testID
  }

  static determineGeneratedCalculationPoints() {
    cy.get(".btnDetermine").click(); // TODO: This element should have a testID
  }

  static saveGeneratedCalculationPoints() {
    cy.get(".btnAccept").click(); // TODO: This element should have a testID
  }

  static assertNoCalculationPointsHaveBeenGenerated() {
    cy.get(".btnAccept").should("not.exist"); // TODO: This element should have a testID
  }

  static assertCalculationPointsHaveBeenGenerated() {
    cy.get(".btnAccept", { timeout: CommonSettings.getElementLongTimeOut }).should("exist"); // TODO: This element should have a testID
  }

  static generatePointsAgain() {
    cy.get('[id="sourceListButtonAutomaticPlacement"]').click();
  }

  static selectScenario(scenarioName: string) {
    cy.get('[name="calculationPointsPlacementSituation"]').select(scenarioName); // TODO: This element should have a testID
  }

  static selectMenuCalculationPoint() {
    cy.get('[id="NAV_ICON-MENU-ASSESSMENT-POINTS"]').click();
  }
}
