/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class AssessmentPointsHabitatsPage {
  static assertHabitatsWhetherListExists(shouldExist: boolean) {
    if (shouldExist) {
      cy.get('[class="habitatsContainer"]').should("exist");
    } else {
      cy.get('[class="habitatsContainer"]').should("not.exist");
    }
  }

  static assertEmptyHabitatsTextAndButtonShown() {
    cy.get('[id="emptyHabitats"]').should("exist");
    cy.get('[class="habitats"]').should("not.exist");
    cy.get('[id="assessmentPointNewHabitatButtonEmpty"]').should("exist");
  }
  
  static addFirstHabitat() {
    cy.get('[id="assessmentPointNewHabitatButtonEmpty"]').click();
  }

  static addHabitat() {
    cy.get('[id="calculation-point-habitat-sourceListButtonNewSource"]').click();
  }

  static duplicateHabitat() {
    cy.get('[id="calculation-point-habitat-sourceListButtonCopySource"]').click();
  }

  static deleteHabitat() {
    cy.get('[id="calculation-point-habitat-sourceListButtonDeleteSource"]').click();
  }

  static assertHabitatsListExistsAndHasHabitats(amountOfHabitats: number) {
    cy.get('[id="emptyHabitats"]').should("not.exist");
    cy.get('[class="habitats"]').should("exist");

    cy.get('[class="habitats"] > [class="transitionGroup"]').find('[id^="CalculationPointHabitatRow-"]').should("have.length", amountOfHabitats);
  }

  static assertHabitatData(dataRow: any) {
    cy.get('[id="calculationPointHabitatNameInput"]').should("have.value", dataRow.name);

    if (dataRow.CLNOxEnabled === "true") {
      cy.get('[id="calculationPointHabitatNOxInput"]').should("have.value", dataRow.CLNOx);
      cy.get('[id="calculationPointHabitatNOxCheckbox"]').find('[class="checkmark icon-checked"]').should("exist");
    } else {
      cy.get('[id="calculationPointHabitatNOxInput"]').should("have.attr", "disabled", "disabled");
      cy.get('[id="calculationPointHabitatNOxCheckbox"]').find('[class="checkmark icon-checked"]').should("not.exist");
    }

    if (dataRow.CLNH3Enabled === "true") {
      cy.get('[id="calculationPointHabitatCLNH3Input"]').should("have.value", dataRow.CLNH3);
      cy.get('[id="calculationPointHabitatNH3Checkbox"]').find('[class="checkmark icon-checked"]').should("exist");
    } else {
      cy.get('[id="calculationPointHabitatCLNH3Input"]').should("have.attr", "disabled", "disabled");
      cy.get('[id="calculationPointHabitatNH3Checkbox"]').find('[class="checkmark icon-checked"]').should("not.exist");
    }

    if (dataRow.CLEnabled === "true") {
      cy.get('[id="calculationPointHabitatCLInput"]').should("have.value", dataRow.CL);
      cy.get('[id="calculationPointHabitatCLCheckbox"]').find('[class="checkmark icon-checked"]').should("exist");
    } else {
      cy.get('[id="calculationPointHabitatCLInput"]').should("have.attr", "disabled", "disabled");
      cy.get('[id="calculationPointHabitatCLCheckbox"]').find('[class="checkmark icon-checked"]').should("not.exist");
    }
  }

  static renameHabitat(newName: string) {
    cy.get('[id="calculationPointHabitatNameInput"]').clear().type(newName);
  }

  static toggleCLNOx() {
    cy.get('[id="calculationPointHabitatNOxCheckbox"]').click();
  }

  static toggleCLNH3() {
    cy.get('[id="calculationPointHabitatNH3Checkbox"]').click();
  }

  static toggleCL() {
    cy.get('[id="calculationPointHabitatCLCheckbox"]').click();
  }

  static assertErrorVisible(input: string, errorText: string) {
    if (input === "CLNOx") {
      cy.get('[id="criticalLevelNOx_error"] > p').should("exist").should("contain", errorText);
    } else if (input === "CLNH3") {
      cy.get('[id="criticalLevelNH3_error"] > p').should("exist").should("contain", errorText);
    } else if (input === "CL") {
      cy.get('[id="criticalLoad_error"] > p').should("exist").should("contain", errorText);
    }
  }

  static assertErrorDoesNotExist(input: string) {
    if (input === "CLNOx") {
      cy.get('[id="criticalLevelNOx_error"]').should("not.exist");
    } else if (input === "CLNH3") {
      cy.get('[id="criticalLevelNH3_error"]').should("not.exist");
    } else if (input === "CL") {
      cy.get('[id="criticalLoad_error"]').should("not.exist");
    }
  }

  static assertAssignmentPointErrorContainerExists(shouldExist: boolean) {
    if (shouldExist) {
      cy.get('[id="calculationPointErrorContainer"]').should("exist");
    } else {
      cy.get('[id="calculationPointErrorContainer"]').should("not.exist");
    }
  }

  static changeCLNOxValue(newValue: string) {
    cy.get('[id="calculationPointHabitatNOxInput"]').clear().type(newValue);
  }

  static changeCLNH3Value(newValue: string) {
    cy.get('[id="calculationPointHabitatCLNH3Input"]').clear().type(newValue);
  }

  static changeCLValue(newValue: string) {
    cy.get('[id="calculationPointHabitatCLInput"]').clear().type(newValue);
  }
}
