/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";

import { GenericEmissionSourcePage } from "./GenericEmissionSourcePage";
import { CommonPage }  from "../../common/CommonPage";
import { CommonAgricultureSourcePage } from "../../common/CommonAgricultureSourcePage";

Given("I create a simple emission source", () => {
  CommonPage.visitAndLogin();
  CommonPage.createNewSourceFromTheStartPage();
  CommonPage.createNewSourceFromTheEmissionSourcePage();
  CommonPage.selectSectorGroup("Bron 1", "Energie");
  CommonPage.fillLocationWithCoordinates("POINT(184557.31 465122.31)");
  CommonPage.saveSource();
});

Given("I create another simple emission source named {string}", (sourceName) => {
  GenericEmissionSourcePage.createNewSourceFromOverviewPage();
  CommonPage.selectSectorGroup(sourceName, "Energie");
  CommonPage.fillLocationWithCoordinates("POINT(184557.31 465122.31)");
  CommonPage.saveSource();
});

Given("the edit buttons are disabled", () => {
  GenericEmissionSourcePage.assertEditButtonsDisabled();
});

Given("the edit buttons are enabled", () => {
  GenericEmissionSourcePage.assertEditButtonsEnabled();
});

Given("I click the detail view close button", () => {
  GenericEmissionSourcePage.closeDetailView();
});

Given("the detail view should not be visible", () => {
  GenericEmissionSourcePage.assertDetailViewClosed();
});

Given("I delete all sources", () => {
  GenericEmissionSourcePage.deleteAllSources();
});

Given("I delete the selected sources", () => {
  GenericEmissionSourcePage.deleteSelectedSources();
});

Given("I delete the selected subsource {string}", (selectedSubsource) => {
  GenericEmissionSourcePage.deleteSelectedSubSource(selectedSubsource);
});

Given("I multiselect the source {string}", (sourceName) => {
  GenericEmissionSourcePage.multiSelectSource(sourceName);
});

Given("the source {string} should exist", (sourceName) => {
  GenericEmissionSourcePage.assertSourceExists(sourceName);
});

Given("there should be no sources", () => {
  GenericEmissionSourcePage.assertNoSources();
});

Given("there should be {int} subsources of subsource {string}", (numberOfSubsources, nameSubsource) => {
  GenericEmissionSourcePage.assertNumberOfSubsources(numberOfSubsources, nameSubsource);
});

Given("I duplicate the selected sources", () => {
  GenericEmissionSourcePage.duplicateSources();
});

Given("the source {string} is selected in the overview", (sourceName) => {
  GenericEmissionSourcePage.assertSourceSelectedInOverview(sourceName);
});

Given("no source should be selected in the overview", () => {
  GenericEmissionSourcePage.assertNoSourceSelectedInOverview();
});

Given("I choose to edit the source {string}", (sourceName) => {
  GenericEmissionSourcePage.chooseToEditSource(sourceName);
});

Given("I edit the name of the source to {string}", (editedSourceName) => {
  GenericEmissionSourcePage.editSourceNameTo(editedSourceName);
});

Given("I edit the sector group of the source to {string}", (editedSectorGroup) => {
  GenericEmissionSourcePage.editSectorGroupTo(editedSectorGroup);
});

Given("I edit the sector of the source to {string}", (editedSector) => {
  GenericEmissionSourcePage.editSectorTo(editedSector);
});

Given("I add a new subsource", () => {
  CommonPage.addNewFarmlandSource();
  CommonAgricultureSourcePage.fillInFieldsFarmlandSource("Beweiding", "9999", "1000");
  CommonPage.saveSource();
});

Given("I edit the fields of the subsource", () => {
  GenericEmissionSourcePage.selectSubsource();
  CommonAgricultureSourcePage.fillInFieldsFarmlandSource("Organische processen", "0", "200");
  CommonPage.saveSource();
});

Given("the subsource is correctly saved", () => {
  GenericEmissionSourcePage.assertSubsourceExists("Organische processen", "0,0", "200");
});

Given("I duplicate the selected subsource", () => {
  GenericEmissionSourcePage.selectSubsource();
  GenericEmissionSourcePage.duplicateSubsource();
  CommonPage.saveSource();
});

Given("the subsource and the duplicated subsource are correctly saved", () => {
  GenericEmissionSourcePage.assertSubsourceAndDuplicatedExists("Beweiding", "9.999,0", "1.000,0");
});
