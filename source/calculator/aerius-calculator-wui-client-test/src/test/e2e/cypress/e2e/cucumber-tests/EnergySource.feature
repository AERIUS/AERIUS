#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Energy source

    Scenario: Create new energy source starting at the start page
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose heat content type 'Niet geforceerd', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '20,000' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '10,0 kg/j'
        And the total 'generic' emission 'NH3' is '20,0 kg/j'

