/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class GenericEmissionSourcePage {
  static createNewSourceFromOverviewPage() {
    cy.get('[id="collapsible-panel-EMISSION_SOURCES"]').click();
    cy.get('[id="sourceListButtonNewSource"]').click();
  }

  static assertEditButtonsDisabled() {
    // Asserts no source is selected
    cy.get(".sources .row.selected").should("have.length", 0);

    cy.get('[id="sourceListButtonEditSource"]').should("have.attr", "disabled", "disabled");
    cy.get('[id="sourceListButtonDeleteSource"]').should("have.attr", "disabled", "disabled");
    cy.get('[id="sourceListButtonCopySource"]').should("have.attr", "disabled", "disabled");
  }

  static assertEditButtonsEnabled() {
    // Asserts at least 1 source is selected
    cy.get(".sources .row.selected").should("have.length.greaterThan", 0);

    cy.get('[id="sourceListButtonEditSource"]').should("not.have.attr", "disabled", "disabled");
    cy.get('[id="sourceListButtonDeleteSource"]').should("not.have.attr", "disabled", "disabled");
    cy.get('[id="sourceListButtonCopySource"]').should("not.have.attr", "disabled", "disabled");
  }

  static closeDetailView() {
    cy.get('[id="sourceListDetailButtonClose"]').click({force:true});
  }

  static assertDetailViewClosed() {
    cy.get(".sourceContainer").should("not.exist");
  }

  static deleteAllSources() {
    cy.get('[id="sourceListButtonDeleteAllSources"]').click();
  }

  static deleteSelectedSources() {
    cy.get('[id="sourceListButtonDeleteSource"]').click({ force: true });
  }

  static deleteSelectedSubSource(selectedSubsource: string) {
    cy.get('[id="subSourceListDescription"]').contains(selectedSubsource).click();
    cy.get('[id="sourceListButtonDeleteSource"]').click();
  }

  static multiSelectSource(sourceName: string) {
    cy.get(".sources").contains(sourceName).click({ ctrlKey: true, force: true });
  }

  static assertSourceExists(sourceName: string) {
    cy.get(".sources").contains(sourceName).should("exist");
  }

  static assertNoSources() {
    cy.get(".emptySources").should("exist");
  }

  static assertNumberOfSubsources(numberOfSubsources: string, nameSubsource: string) {
    cy.get(".rowContainer > div").find("> div").contains(nameSubsource).should("have.length", numberOfSubsources);
  }

  static duplicateSources() {
    cy.get('[id="sourceListButtonCopySource"]').click();
  }

  static assertSourceSelectedInOverview(sourceName: string) {
    cy.get(".sources .row.selected .labelColumn").should("contain.text", sourceName);
  }

  static assertNoSourceSelectedInOverview() {
    cy.get(".sources .row.selected").should("have.length", 0);
  }

  static chooseToEditSource() {
    cy.get('[id="sourceListButtonEditSource"]').click();
  }

  static editSourceNameTo(editedSourceName: string) {
    cy.get('[id="label"]').clear().type(editedSourceName);
    cy.get('[id="buttonSave"]').click();
  }

  static editSectorGroupTo(editSectorGroupTo: string) {
    cy.get('[id="sourceListDetailSectorGroup"]').select(editSectorGroupTo);
    cy.get('[id="buttonSave"]').click();
  }

  static editSectorTo(editSectorTo: string) {
    cy.get('[id="sourceListDetailSector"]').select(editSectorTo);
    cy.get('[id="buttonSave"]').click();
  }

  static selectSubsource() {
    cy.get('[id="sourcelistRow0"]').click();
  }

  static assertSubsourceExists(category: string, NOX: string, NH3: string) {
    cy.get(".margin").contains(category).should("exist");
    cy.get(".margin").contains(NOX).should("exist");
    cy.get(".margin").contains(NH3).should("exist");
  }

  static duplicateSubsource() {
    cy.get('[id="sourceListButtonCopySource"]').click();
  }

  static assertTotalAmountNOX(totalAmountNOX: string) {
    cy.get('[id="sourceListTotalNOX"]').contains(totalAmountNOX);
  }

  static assertTotalAmountNH3(totalAmountNH3: string) {
    cy.get('[id="sourceListTotalNH3"]').contains(totalAmountNH3);
  }

  static assertSubsourceAndDuplicatedExists(category: string, NOX: string, NH3: string) {
    cy.get('[id="farmlandDetailActivity-PASTURE-0"]').contains(category).should("exist");
    cy.get('[id="farmlandDetailActivity-PASTURE-0"]').parent().find('[id="farmlandDetailEmissionNOX"]').contains(NOX).should("exist");
    cy.get('[id="farmlandDetailActivity-PASTURE-0"]').parent().find('[id="farmlandDetailEmissionNH3"]').contains(NH3).should("exist");
    cy.get('[id="farmlandDetailActivity-PASTURE-1"]').contains(category).should("exist");
    cy.get('[id="farmlandDetailActivity-PASTURE-1"]').parent().find('[id="farmlandDetailEmissionNOX"]').contains(NOX).should("exist");
    cy.get('[id="farmlandDetailActivity-PASTURE-1"]').parent().find('[id="farmlandDetailEmissionNH3"]').contains(NH3).should("exist");
  }
}
