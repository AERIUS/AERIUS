/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given} from "cypress-cucumber-preprocessor/steps";

import { CommonBuildingPage } from "./CommonBuildingPage";

Given("I create a new building from the input page", () => {
  CommonBuildingPage.createNewBuildingFromTheInputPage();
});

Given("I create a new building from the building list", () => {
  CommonBuildingPage.createNewBuildingFromBuildingList();
});

Given("I name the building {string}", (name) => {
  CommonBuildingPage.nameBuilding(name);
});

Given("I select building shape {string}", (shape) => {
  CommonBuildingPage.selectBuildingShape(shape);
});

Given("I fill the building shape with WKT string {string}", (buildingGeometryInput) => {
  CommonBuildingPage.buildingSourceGeometryInput(buildingGeometryInput);
});

Given("I validate the building shape with WKT string {string}", (validateBuildingGeometryInput) => {
 CommonBuildingPage. validateBuildingSourceGeometryInput(validateBuildingGeometryInput);
});

Given("I fill the building height with {string}", (height) => {
  CommonBuildingPage.enterHeight(height);
});

Given("I fill the building diameter with {string}", (diameter) => {
  CommonBuildingPage.enterDiameter(diameter);
});

Given("I select the building {string}", (buildingName) => {
  CommonBuildingPage.selectBuilding(buildingName);
});

Given("I edit the building", () => {
  CommonBuildingPage.editBuilding();
});

Given("I select the building {string} from the primary building list", (buildingName) => {
  CommonBuildingPage.selectBuildingFromPrimaryBuildingList(buildingName);
});

Given("I save the building", () => {
  CommonBuildingPage.saveBuilding();
});

Given(
  "the building with name {string}, length {string}, width {string}, height {string} and orientation {string} is saved and selected",
  (name, length, width, height, orientation) => {
    CommonBuildingPage.assertPolygonalBuildingSavedAndSelected(name, length, width, height, orientation);
  }
);

Given("the circular building with name {string}, diameter {string} and height {string} is saved and selected", (name, diameter, height) => {
  CommonBuildingPage.assertCircularBuildingSavedAndSelected(name, diameter, height);
});

Given("I toggle building influenced", () => {
  CommonBuildingPage.toggleBuildingInfluenced();
});

Given("I add a new building", () => {
  CommonBuildingPage.addNewBuilding();
});

Given("I duplicate the building", () => {
  CommonBuildingPage.duplicateBuilding();
});

Given("I delete the building", () => {
  CommonBuildingPage.deleteSelectedBuilding();
});
