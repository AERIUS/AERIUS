/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";
import { CommonPage } from "./CommonPage";
import { CalculationJobsPage } from "./../cucumber-tests/CalculationJobs/CalculationJobsPage";
import { CommonSettings } from "./CommonSettings";

Given("I login with correct username and password", () => {
  CommonPage.visitAndLogin();
});

Given("I login with correct username and password with OPS enabled", () => {
  CommonPage.visitAndLoginWithOps();
});

Given("I login with correct username and password and with the English language selected", () => {
  CommonPage.visitAndLoginWithlocaleEn();
});

Given("I login with correct username and password for product {string}", (productCode) => {
  CommonPage.visitAndLoginWithProduct(productCode);
});

Given("I change the viewport to width {int} height {int}", (width, height) => {
  CommonPage.changeViewport(width, height);
});

Given("I navigate to the {string} tab", (tab) => {
  CommonPage.navigateTo(tab);
});

Given("I click to navigate to the {string} tab, but press cancel on the confirm pop-up", (tab) => {
  CommonPage.navigateTo(tab, false);
});

Given("I select the situation {string} as {string}", (situation, situationId) => {
  CommonPage.selectSituationForCalculation(situation, situationId);
});

Given("I delete the current scenario", () => {
  CommonPage.deleteCurrentScenario();
});

Given("I click on the start page button", () => {
  // Click on the start page button
  CommonPage.clickStartMenuButton();
});

Given("I create a new source from the start page", () => {
  // On the launch page we start with new
  CommonPage.createNewSourceFromTheStartPage();

  CommonPage.selectViewMode("EMISSION_SOURCES");

  // On the source page we have to start with new
  CommonPage.createNewSourceFromTheEmissionSourcePage();
});

Given("I create an empty scenario from the start page", () => {
  CommonPage.createNewSourceFromTheStartPage();
});

Given("I create a new source from the start page situation label {string}", (situation) => {
  CommonPage.createNewSourceFromTheStartPage();
  CommonPage.enterSituationName(situation);
  CommonPage.saveSituationUpdate();
  CommonPage.createNewSourceFromTheEmissionSourcePage();
});

Given("I create a new source from the source page", () => {
  CommonPage.createNewSourceFromTheSourcePage();
});

Given("I create a new source from the scenario input page", () => {
  CommonPage.createNewSourceFromTheScenarioInputPage();
});

Given("I name the source {string}", (sourceName) => {
  CommonPage.nameTheSource(sourceName);
});

Given("I fill in the fields for sector group {string}", (sectorGroup) => {
  CommonPage.fillInFieldsForSectorGroup(sectorGroup);
});

Given("I name the source {string} and select sector group {string}", (sourceName, sectorGroup) => {
  CommonPage.selectSectorGroup(sourceName, sectorGroup);
});

Given("I name the source {string} and select sector group {string} and sector {string}", (sourceName, sectorGroup, sector) => {
  CommonPage.selectSectorGroupAndSector(sourceName, sectorGroup, sector);
});

Given("the source {string} with sector group {string} is correctly saved", (sourceName, sectorGroup) => {
  CommonPage.sourceNameWithSectorGroupCorrectlySaved(sourceName, sectorGroup);
});

Given("the source {string} with sector group {string} and sector {string} is correctly saved", (sourceName, sectorGroup, sector) => {
  CommonPage.sourceNameWithSectorGroupAndSectorCorrectlySaved(sourceName, sectorGroup, sector);
});

Given("I select sector {string}", (sector) => {
  CommonPage.selectSector(sector);
});

Given("I fill location with coordinates {string}", (locationCoordinates) => {
  CommonPage.fillLocationWithCoordinates(locationCoordinates);
});

Given("I change location to coordinates {string}", (locationCoordinates) => {
  CommonPage.changeLocationToCoordinates(locationCoordinates);
});

Given("location with coordinates {string} is correctly saved", (locationCoordinates) => {
  CommonPage.locationWithCoordinatesCorrectlySaved(locationCoordinates);
});

Given("the value {string} is visible in the location panel", (valueLocationPanel) => {
  CommonPage.valueInLocationPanel(valueLocationPanel);
});

Given("length of location with value {string} is correctly saved", (lengthOfLocation) => {
  CommonPage.lengthOfLocationCorrectlySaved(lengthOfLocation);
});

Given("surface of location with value {string} is correctly saved", (surfaceOfLocation) => {
  CommonPage.surfaceOfLocationCorrectlySaved(surfaceOfLocation);
});

Given(
  "I choose date establishment of animal shelter {string}, heat content type {string}, emission height {string} and heat content {string}",
  (establishmentDate, heatContentType, emissionHeight, heatContent) => {
    CommonPage.chooseSourceCharacteristicsAnimalHousingEmission(establishmentDate, heatContentType, emissionHeight, heatContent);
  }
);

Given(
  "I choose heat content type {string}, emission height {string} and heat content {string}",
  (heatContentType, emissionHeight, heatContent) => {
    CommonPage.chooseSourceCharacteristics(heatContentType, emissionHeight, heatContent);
  }
);

Given("I choose emission height {string} and heat content {string}", (emissionHeight, heatContent) => {
  CommonPage.chooseSourceCharacteristicsWithBuilding(emissionHeight, heatContent);
});

Given(
  "I choose heat content type {string}, emission height {string}, heat content {string} and diurnal variation type {string}",
  (heatContentType, emissionHeight, heatContent, diurnalVariationType) => {
    CommonPage.chooseSourceCharacteristicsWithDiurnalVariationType(heatContentType, emissionHeight, heatContent, diurnalVariationType);
  }
);

Given(
  "source characteristics with heat content type {string}, emission height {string}, heat content {string} and diurnal variation type {string} is correctly saved",
  (heatContentType, emissionHeight, heatContent, diurnalVariationType) => {
    +CommonPage.sourceCharacteristicsCorrectlySaved(heatContentType, emissionHeight, heatContent, diurnalVariationType);
  }
);

Given(
  "I choose heat content type {string}, emission height {string}, emission temperature {string}, outflow diameter {string}, outflow direction type {string} and outflow velocity {string}",
  (heatContentType, emissionHeight, emissionTemperature, outflowDiameter, outflowDirectionType, outflowVelocity) => {
    CommonPage.chooseSourceCharacteristicsWithOutflow(
      heatContentType,
      emissionHeight,
      emissionTemperature,
      outflowDiameter,
      outflowDirectionType,
      outflowVelocity
    );
  }
);

Given(
  "source characteristics with heat content type {string}, building influence {string}, emission height {string}, emission temperature {string}, outflow diameter {string}, outflow direction type {string}, outflow velocity {string} and diurnal variation type {string} is correctly saved",
  (
    heatContentType,
    buildingInfluence,
    emissionHeight,
    emissionTemperature,
    outflowDiameter,
    outflowDirectionType,
    outflowVelocity,
    diurnalVariation
  ) => {
    CommonPage.sourceCharacteristicsOutflowWithBuildingCorrectlySaved(
      heatContentType,
      buildingInfluence,
      emissionHeight,
      emissionTemperature,
      outflowDiameter,
      outflowDirectionType,
      outflowVelocity,
      diurnalVariation
    );
  }
);

Given("the spread for the source is not visible", () => {
  CommonPage.spreadNotVisible();
});

Given("the spread for the source is visible", () => {
  CommonPage.spreadIsVisible();
});

Given("I fill in {string} as spread", (inputSpread: string) => {
  CommonPage.fillInSpread(inputSpread);
});

Given("the spread is correctly saved with value {string}", (valueSpread) => {
  CommonPage.spreadCorrectlySaved(valueSpread);
});

// UK Specific
Given("I choose source efluxtype {string}", (efluxtype) => {
  CommonPage.chooseefluxTypeUK(efluxtype);
});

// UK Specific
Given("I chooce source Specify temperature or density {string}", (temperatureType) => {
  CommonPage.chooseSpecifyTemperatureDensityUK(temperatureType);
});

// UK Specific
Given("Building influence should {string}", (state) => {
  CommonPage.existIdUK("buildingInfluence", state);
});

// UK Specific
Given("Custom time-varying profile should {string}", (state) => {
  CommonPage.existIdUK("timeVaryingProfileAttached", state);
});

// UK Specific
Given("Elevation angle should {string}", (state) => {
  CommonPage.existIdUK("elevationAngle", state);
});

// UK Specific
Given("Horizontal angle should {string}", (state) => {
  CommonPage.existIdUK("horizontalAngle", state);
});

// UK Specific
Given("Temperature should {string}", (state) => {
  CommonPage.existIdUK("sourceTemperature", state);
});

// UK Specific
Given("Density should {string}", (state) => {
  CommonPage.existIdUK("sourceDensity", state);
});

// UK Specific
Given("VerticalVelocity should {string}", (state) => {
  CommonPage.existIdUK("sourceVerticalVelocity", state);
});

// UK Specific
Given("VolumetricFlowRate should {string}", (state) => {
  CommonPage.existIdUK("sourceVolumetricFlowRate", state);
});

// UK Specific
Given("MassFlux should {string}", (state) => {
  CommonPage.existIdUK("sourceMassFlux", state);
});

// UK Specific
Given(
  "source characteristics with sector group {string}, emission NOx {string} and NH3 {string} is correctly saved",
  (sectorGroup, emissionNOx, emissionNH3) => {
    CommonPage.sourceCharacteristicsCorrectlySavedUK(sectorGroup, emissionNOx, emissionNH3);
  }
);

Given("I fill the emission fields with NOx {string} and NH3 {string}", (emissionNOx, emissionNH3) => {
  CommonPage.fillEmissionFields(emissionNOx, emissionNH3);
});

Given("I fill in the fields for sector group {string} with sector {string}", (sectorGroup, sector) => {
  CommonPage.fillInFieldsForSectorGroupAndSector(sectorGroup, sector);
});

Given(
  "the source with sector group {string} and sector {string} is created and visible for source {string}",
  (sectorGroup, sector, sourceName) => {
    CommonPage.sourceWithSectorGroupAndSectorCreated(sectorGroup, sector, sourceName);
  }
);

Given("I save the data", () => {
  CommonPage.saveSource();
});

Given("I save the data and wait for refresh", () => {
  CommonPage.saveSourceValidateRefresh();
});

Given("I save the data validate the warning and wait for refresh", () => {
  CommonPage.saveSourceValidateWarningAndValidateRefresh();
});

Given("the savesource button is disabled.", () => {
  CommonPage.saveSourceDisabled();
});

Given("I select situation {string}", (situationName) => {
  CommonPage.selectSituation(situationName);
});

Given("I select the source {string}", (sourceName) => {
  CommonPage.selectSource(sourceName);
});

Given("I open the source list", () => {
  CommonPage.unfoldSources();
});

Given("I open the source list and scroll to bottom", () => {
  CommonPage.unfoldSources();
  CommonPage.scrollToBottomSources();
});

Given("I open the buildings panel", () => {
  CommonPage.openBuildingsPanel();
});

Given("I choose to edit the source", () => {
  CommonPage.editSource();
});

Given("I choose to cancel", () => {
  CommonPage.chooseToCancel("");
});

Given("I click at the tab {string}", (tabName) => {
  CommonPage.clickToOpenTab("tab-" + tabName);
});

Given("I click at the tab {string} which should have the title {string}", (tabName, tabTitle) => {
  CommonPage.clickToOpenTab("tab-" + tabName);
  CommonPage.validateTabTitle("tab-" + tabName, tabTitle);
});

Given("I open collapsible panel {string}", (panelName) => {
  CommonPage.openCollapsiblePanel(panelName);
});

Given("I empty the field {string}", (fieldName) => {
  CommonPage.emptyField(fieldName);
});

Given("I check if the field {string} is empty", (fieldName) => {
  CommonPage.assertIfFieldIsEmpty(fieldName);
});

Given("I copy the source {string}", (sourceName) => {
  CommonPage.copySource(sourceName);
});

Given("I delete source {string}", (sourceName) => {
  CommonPage.deleteSelectedSource(sourceName);
});

Given("the next sources exists {string}, {string}", (sourceName1, sourceName2) => {
  CommonPage.sourcesExists(sourceName1, sourceName2);
});

Given("the next source does not exist {string}", (sourceName) => {
  CommonPage.sourcesDoesNotExists(sourceName);
});

Given("I choose to import a source starting from the start page", () => {
  CommonPage.clickImportSourceFromStartPage();
});

Given("I choose to import the files", () => {
  CommonPage.chooseToImport();
});

Given("I open the file dialog to import", () => {
  CommonPage.openFileDialogToImport();
});

Given("I select file {string} to be imported, but I don't upload it yet", (fileName) => {
  CommonPage.selectFileForImportDoNotImport(fileName, false);
});

Given("I select file {string} to be imported, but I don't upload it, or open the collapsible panel yet", (fileName) => {
  CommonPage.selectFileForImportDoNotImport(fileName, false, false);
});

Given("I select file {string} to be imported", (fileName) => {
  CommonPage.selectFileForImport(fileName);
});

Given("I select a file with multiple situations {string} to be imported in advanced mode", (fileName) => {
  CommonPage.selectFileForImportDoNotImport(fileName, true);
});

Given(
  "I select a file with multiple situations {string} to be imported in advanced mode, but don't open the collapsible panel yet",
  (fileName) => {
    CommonPage.selectFileForImportDoNotImport(fileName, true, false);
  }
);

Given("I select calculation point file {string} to be imported", (fileName) => {
  CommonPage.selectCalculationPointFileForImport(fileName);
});

Given("I drag and drop file {string} to be imported", (fileName) => {
  CommonPage.dragAndDropFileForImport(fileName);
});

Given("I select file {string} to be imported and select situation type {string}, but I don't upload it yet", (fileName, situationType) => {
  CommonPage.selectFileForImportAndSelectSituationType(fileName, situationType);
});

Given(
  "I select file {string} to be imported, select import action {string} and situation type {string}, but I don't upload it yet",
  (fileName, importAction, situationType) => {
    CommonPage.selectFileForImportAndSelectImportActionAndSituationType(fileName, importAction, situationType);
  }
);

Given("I select file {string} to be imported and select import action {string}, but I don't upload it yet", (fileName, importAction) => {
  CommonPage.selectFileForImportAndSelectImportAction(fileName, importAction);
});

Given("I select import action {string} for file {string}", (importAction, fileName) => {
  CommonPage.selectImportActionForFile(importAction, fileName);
});

Given("the following notification is displayed {string}", (notificationText: string) => {
  CommonPage.followingNotificationIsDisplayed(notificationText);
});

Given("the following calculation job notification is displayed {string} status {string}", (notificationText: string, jobStatus: string) => {
  CommonPage.followingNotificationIsDisplayed(notificationText.substring(0, 128) + " " + jobStatus);
});

Given("the following system profile notification is displayed {string}", (notificationText: string) => {
  CommonPage.followingSystemProfileNotificationIsDisplayed(notificationText);
});

Given("I click on remove all notifications", () => {
  CommonPage.clearNotifications();
});

Given("a tooltip is available containing the text {string}", (notificationText: string) => {
  CommonPage.followingSubStringIsDisplayed(notificationText, true);
});

Given("a button title is available containing the text {string}", (notificationText: string) => {
  CommonPage.followingTitleIsDisplayed(notificationText);
});

Given("a notification is displayed containing the text {string}", (notificationText: string) => {
  CommonPage.followingSubStringIsDisplayed(notificationText, true);
});

Given("a notification containing the text {string} is not displayed", (notificationText: string) => {
  CommonPage.followingSubStringIsDisplayed(notificationText, false);
});

Given("the notification center should be open", () => {
  CommonPage.assertNotificationCenterIsOpen();
});

Given("the notification center should be closed", () => {
  CommonPage.assertNotificationCenterIsClosed();
});

Given("no notification should be displayed", () => {
  CommonPage.assertNoNotificationIsDisplayed();
});

Given("{int} notification should be displayed", (expectedAmountOfNotifications: number) => {
  CommonPage.assertNumberOfNotificationsIsDisplayed(expectedAmountOfNotifications);
});

Given("{int} notifications should be displayed", (expectedAmountOfNotifications: number) => {
  CommonPage.assertNumberOfNotificationsIsDisplayed(expectedAmountOfNotifications);
});

Given("I should see the notification {string} with a date and time", (notificationText: string) => {
  CommonPage.assertNotificationIsDisplayedWithDateTime(notificationText);
});

Given("I delete the notification {string}", (notificationText: string) => {
  CommonPage.deleteNotification(notificationText);
});

Given("I open the notification center", () => {
  CommonPage.openNotificationCenter();
});

Given("I expect {int} import {string} to be shown", (count: number, warningType: string) => {
  CommonPage.validateImportErrorWarningCount(count, warningType);
});

Given("I validate the following warnings", (dataTable: any) => {
  CommonPage.validateWarningsMessages(dataTable);
});

Given("I validate the following errors", (dataTable: any) => {
  CommonPage.validateErrorMessages(dataTable);
});

Given("Import the file", () => {
  CommonPage.chooseToImport(true);
});

Given("I wait {string}", (wait: string) => {
  CommonPage.wait(Number(wait));
});

Given("I open the subsource dropdown", () => {
  CommonPage.openSubSource();
});

Given("the subsource error is given {string}", (errorText: string) => {
  CommonPage.subsourceErrorTextExists(errorText);
});

Given("the savesource error is given {string}", (errorText: string) => {
  CommonPage.saveSourceErrorTextExists(errorText);
});

Given("the following warning is displayed: {string}", (warningText) => {
  CommonPage.followingWarningIsDisplayed(warningText);
});

Given("the following warning is visible: {string}", (warningText) => {
  CommonPage.followingWarningIsVisible(warningText);
});

Given("the warning is not visible", () => {
  CommonPage.warningIsNotVisible();
});

Given("the following error is visible: {string}", (errorText) => {
  CommonPage.followingErrorIsVisible(errorText);
});

Given("I choose not to show the privacy warning again", () => {
  CommonPage.chooseNotShowPrivacyWarning();
});

Given("I add situation label {string}", (situationName) => {
  CommonPage.enterSituationName(situationName);
});

Given("I validate situation label error message {string}", (errorMessage) => {
  CommonPage.validateSituationNameError(errorMessage);
});


Given("the situation name is {string}", (situationName) => {
  CommonPage.validateSituationName(situationName);
});

Given("the situation type is {string}", (situationType) => {
  CommonPage.validateSituationType(situationType);
});

Given("the calculation year is {string}", (calculationYear) => {
  calculationYear = CommonSettings.updateCalculationYear(calculationYear);
  CommonPage.validateCalculationYear(calculationYear);
});

Given("the netting factor is {string}", (nettingFactor) => {
  CommonPage.validateNettingFactor(nettingFactor);
});

Given("I set the calculation year to {string}", (calculationYear) => {
  calculationYear = CommonSettings.updateCalculationYear(calculationYear);
  CommonPage.setCalculationYear(calculationYear);
});

Given("I fill in netting factor {string}", (factor) => {
  CommonPage.fillInNettingFactor(factor);
});

Given("I select view mode {string}", (viewMode) => {
  CommonPage.selectViewMode(viewMode);
});

Given("the emission source panel should be closed", () => {
  CommonPage.validateEmissionSourcesPanelIsClosed();
});

Given("the location panel should be open", () => {
  CommonPage.validateLocationPanelIsOpen();
});

Given("the source characteristics panel does not exist", () => {
  CommonPage.validateSourceCharacteristicsPanelIsNotVisible();
});

Given("the buildings panel should be closed", () => {
  CommonPage.validateBuildingsPanelIsClosed();
});

Given("I open emission source list", () => {
  CommonPage.openEmissionSources();
});

Given("I set the calculation year to {string}", (calculationYear) => {
  calculationYear = CommonSettings.updateCalculationYear(calculationYear);
  CommonPage.setCalculationYear(calculationYear);
});

Given("I fill in netting factor {string}", (factor) => {
  CommonPage.fillInNettingFactor(factor);
});

Given("the buildings panel should be closed", () => {
  CommonPage.validateBuildingsPanelIsClosed();
});

Given("the emission source list has {int} sources", (sourceCount) => {
  CommonPage.validateSource(sourceCount);
});

Given("the calculation jobs list has {int} calculation jobs", (jobCount) => {
  CommonPage.assertNumberOfCalculationJobs(jobCount);
});

Given("the roadnetwork has {int} sources", (roadSourceCount) => {
  CommonPage.validateRoadSource(roadSourceCount);
});

Given("I navigate back in my browser", () => {
  CommonPage.navigateBack();
});

Given("I delete the calculation settings for the current job", () => {
  CommonPage.deleteCurrentCalculationSetting();
});

Given("I open the calculation settings for {string}", (job) => {
  CommonPage.openCollapsibleCalculationSetting(job);
});

Given("I start calculation and validate the request", (dataTable) => {
  CommonPage.startCalculationAndValidateRequest(dataTable, "calculateCalculationJobButton");
});

Given("I start an export and validate the request", (dataTable) => {
  CommonPage.startCalculationAndValidateRequest(dataTable, "buttonSaveExport");
});

Given("I start the calculation", () => {
  CommonPage.startCalculation();
});

Given("I cancel the calculation", () => {
  CommonPage.cancelCalculation();
});

Given("I close source detail screen", () => {
  CommonPage.closeSourceDetail();
});

Given("The validation errors for the source should {string}", (visibilityStatus) => {
  CommonPage.sourceValidationErrorsVisible(visibilityStatus);
});

Given("I cancel the new source creation", () => {
  CommonPage.chooseToCancel("");
});

Given("the default drawing type is {string}", (defaultDrawingType) => {
  CommonPage.defaultDrawingType(defaultDrawingType);
});

Given("I select drawing type {string}", (selectDrawingType) => {
  CommonPage.selectDrawingType(selectDrawingType);
});

Given("the droplist calculation year contains the following years:", (dataTable) => {
  CommonPage.valuesDroplistCalculationYear(dataTable);
});

Given("I set the calculation year to {string}", (calculationYear) => {
  calculationYear = CommonSettings.updateCalculationYear(calculationYear);
  CommonPage.setCalculationYear(calculationYear);
});

Given("I select scenarioType {string}", (scenarioType) => {
  CommonPage.selectScenarioType(scenarioType);
});

Given("I fill in netting factor {string}", (factor) => {
  CommonPage.fillInNettingFactor(factor);
});

Given("I save the scenario updates", () => {
  CommonPage.saveSituationUpdate();
});

Given("I enable the advanced importmodus", () => {
  CommonPage.enableAdvancedImportmodus();
});

Given("I validate building warning message {string}", (warningMessage) => {
  CommonPage.validateBuildingMessage(warningMessage);
});

Given("I validate documentation links", (dataTable) => {
  CommonPage.checkDocumentionSpecification(dataTable);
});

Given("the buildinglist has {int} buildings", (numberOfBuildings) => {
  CommonPage.assertBuildingListHasNumberOfBuildings(numberOfBuildings);
});

Given("I select the building with name {string}", (name) => {
  CommonPage.selectBuildingWithName(name);
});

Given("I click on the {string} button", (buttonName) => {
  CommonPage.clickOnSpecificButton(buttonName);
});

Given("the following {string} text is visible: {string}", (textType, visibleText) => {
  CommonPage.checkTextIsVisible(textType, visibleText);
});

Given("I add a new farmland source", () => {
  CommonPage.addNewFarmlandSource();
});

Given("situation type for file {string} is {string}", (fileName, situationType) => {
  CommonPage.assertSituationType(fileName, situationType);
});

Given("input field situation type for file {string} is {string}", (fileName, isInputEnabled) => {
  CommonPage.assertEnabledDisabledSituationType(fileName, isInputEnabled);
});

Given("I am located at the introduction page", () => {
  CommonPage.assertPageLocationIsIntroduction();
});

Given("button {string} is disabled", (buttonName) => {
  CommonPage.assertButtonIsDisabled(buttonName);
});

Given("button {string} is enabled", (buttonName) => {
  CommonPage.assertButtonIsEnabled(buttonName);
});

Given("file {string} is not visible in the import screen", (fileName) => {
  CommonPage.assertImportFileIsNotVisible(fileName);
});

Given("it is not possible for file {string} to be added to a situation that does not exist", (fileName) => {
  CommonPage.assertAddingToSituationIsImpossible(fileName);
});

Given("it is possible for file {string} to be added to a situation that does exist", (fileName) => {
  CommonPage.assertAddingToSituationIsPossible(fileName);
});

Given("I uncheck the box for importing element {string}", (uncheckedElement) => {
  CommonPage.uncheckBoxForImport(uncheckedElement);
});

Given("I validate import element availability", (dataTable) => {
  CommonPage.validateImportOptionsAvailability(dataTable);
});

Given("I save the calculation job", () => {
  CommonPage.saveCalculationJob();
});

Given("I create a new calculation job", () => {
  CommonPage.createNewCalculationJob();
});

Given("I edit the selected calculation job", () => {
  CommonPage.editCalculationJob();
});

Given("I copy the selected calculation job", () => {
  CommonPage.copyCalculationJob();
});

Given("I delete the selected calculation job", () => {
  CommonPage.deleteSelectedCalculationJob();
});

Given("I delete all the calculation jobs", () => {
  CommonPage.deleteAllCalculationJobs();
});

Given("I switch calculation job to calculation job {string}", (calculationJob) => {
  CommonPage.switchToCalculationJob(calculationJob);
});

Given("I expect the following calculation job to exist {string}", (nameCalculationJob) => {
  CommonPage.assertCalculationJobIsNamed(nameCalculationJob.substring(0, 128));
});

Given("I expect the following calculation job not to exist {string}", (nameCalculationJob) => {
  CommonPage.assertCalculationJobNotToExist(nameCalculationJob);
});

Given("I expect that the calculation job is of type {string}", (calculationJobType) => {
  CommonPage.assertCalculationJobType(calculationJobType);
});

Given("I expect that the calculation method is {string}", (calculationMethod) => {
  CommonPage.assertCalculationMethod(calculationMethod);
});

Given("I expect the {string} scenario to be {string}", (scenarioType, nameScenario) => {
  CommonPage.assertScenariosOfCalulationJob(scenarioType, nameScenario);
});

Given("I expect the page title to be {string}", (expectedTitle) => {
  CommonPage.assertPageTitleIs(expectedTitle);
});

Given("the total {string} emission {string} is {string}", (emissionSourceType, emissionType, emission) => {
  CommonPage.assertTotalEmission(emissionSourceType, emissionType, emission);
});

Given("the total {string} emission {string} is not {string}", (emissionSourceType, emissionType, emission) => {
  CommonPage.assertTotalEmission(emissionSourceType, emissionType, emission, true);
});

Given(
  "the {string} row {string} has an NOx emission of {string} and an NH3 emission of {string}",
  (emissionSourceType, rowIndex, emissionNOx, emissionNH3) => {
    CommonPage.assertEmission(emissionSourceType, rowIndex, emissionNOx, emissionNH3);
  }
);

Given(
  "the animal housing system with housing code {string}, number of animals {string}, factor {string}, reduction {string} and emission {string} is correctly created",
  (housingCode, numberOfAnimals, factor, reduction, emission) => {
    CommonPage.animalHousingSystemWithHousingCodeCreated(housingCode, numberOfAnimals, factor, reduction, emission);
  }
);

Given("I press the export button", () => {
  CommonPage.pressExportButton();
});

Given("I select calculation job {string}", (calculationJobName) => {
  CommonPage.selectCalculationJob(calculationJobName);
});

Given("I select calculation jobType {string}", (jobType) => {
  CommonPage.selectCalculationJobType(jobType);
});

Given("I select calculation method {string}", (method) => {
  CommonPage.selectCalculationMethod(method);
});

Given("the following fields are visible and editable", (dataTable) => {
  CommonPage.assertFieldVisibleAndEnabled(dataTable);
});

Given("the following fields are not visible", (dataTable) => {
  CommonPage.assertFieldNotVisible(dataTable);
});

Given("I check the checkbox for the temporary situation {string}", (tempSituationName) => {
  CommonPage.checkCheckboxForTemporarySituation(tempSituationName);
});

Given("I can select {string}: {string}", (scenarioType, nameScenario) => {
  CommonPage.selectScenario(scenarioType, nameScenario);
});

Given("no error should show", () => {
  CommonPage.assertNoErrorIsShown();
});

Given("the building the source is influenced by is {string}", (influencingBuilding) => {
  CommonPage.assertInfluencingBuildingIs(influencingBuilding);
});

Given("there is no building influence", () => {
  CommonPage.assertNoBuildingInfluence();
});

Given("the current styling is conform product {string}", (productProfile) => {
  CommonPage.assertProductStyling(productProfile);
});

Given("I select the subsource {string}", (subSourceName) => {
  CommonPage.selectSubSource(subSourceName);
});

Given("I validate the calculation job message {string}", (message) => {
  CommonPage.validateJobMessage(message);
});

Given("I toggle the help window", () => {
  CommonPage.toggleHelpWindow();
});

Given("I validate help info is {string}", (status) => {
  CommonPage.validateHelpWindowStatus(status);
});

Given("I close help window with close button", () => {
  CommonPage.pressHelpWindowClose();
});

Given("I validate the status of the menu items", (dataTable) => {
  CommonPage.validateMenuItems(dataTable);
});

Given("the following error is shown: {string}", (errorText) => {
  CommonPage.assertErrorIsShown(errorText);
});

Given("the following warning is shown: {string}", (warningText) => {
  CommonPage.assertWarningIsShown(warningText);
});

Given("the following notice is shown: {string}", (noticeText) => {
  CommonPage.assertNoticeIsShown(noticeText);
});

Given("the following warning about Ommited areas is shown: {string}", (noticeText) => {
  CommonPage.textOmittedAreasIsVisible(noticeText);
});

Given("the following text about the site-relevant threshold class is visible: {string}", (text) => {
  CommonPage.textSiteRelevantThresholdIsVisible(text);
});

Given("I select a source and validate default characteristics", (dataTable) => {
  CommonPage.selectSourceValidateDefaults(dataTable);
});

Given("I validate the default transport characteristics", (dataTable) => {
  CommonPage.validateDefaultSourceCharacteristic(dataTable);
});

Given("the following characteristics should not exist", (dataTable) => {
  CommonPage.validateCharacteristicsDontExist(dataTable);
});

Given("the time-varying profileslist has {int} profiles", (numberOfProfiles) => {
  CommonPage.assertTimeVaryingProfilesListHasNumberOfprofiles(numberOfProfiles);
});

Given("I wait for the results spinner to resolve", () => {
  CommonPage.waitForResultsSpinnerToResolve();
});

Given("I close the panel", () => {
  CommonPage.closePanel();
});

// Preferences
Given("the setting for {string} should be {string}", (setting: string, value: string) => {
  let booleanValue: boolean = value === "on" ? true : false;
  CommonPage.assertPreferencesCheckboxSetting(setting, booleanValue);
});

Given("I change the setting for {string} to {string}", (setting: string, value: string) => {
  let booleanValue: boolean = value === "on" ? true : false;
  CommonPage.changePreferencesCheckboxSetting(setting, booleanValue);
});

Given("I reload the calculator application", () => {
  cy.reload();
});

Given("I select the calculation method {string}", (calculationPointsOption) => {
  CalculationJobsPage.selectCalculationPointsOption(calculationPointsOption);
});
