/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class CommonSettings {
    static emissionTextValidationTimeOut: number = 10_000;
    static getElementLongTimeOut: number = 800_000;
    static fileioTimeOut: number = 15_000;
    static importLoadingTimeOut: number = 15_000;
    static notificationTimeOut: number = 400_000;
    static notificationCenterTimeOut: number = 10_000;
    static responseTimeOut: number = 15_000;
    static resultsLoadingTimeOut: number = 20_000;
    static validationTimeOut: number = 90_000;

    static minCalculationYear: string = "2024";
    static maxCalculationYear: string = "2040";

    static updateCalculationYear(calculationYear: any): any {
        if (calculationYear === "minCalculationYear") {
            return this.minCalculationYear;
        } else if (calculationYear === "maxCalculationYear") {
            return this.maxCalculationYear;
        } else {
            return calculationYear;
        }
      }
}
