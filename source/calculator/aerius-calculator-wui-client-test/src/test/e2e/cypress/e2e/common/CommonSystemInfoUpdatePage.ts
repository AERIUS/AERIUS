/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class CommonSystemInfoUpdatePage {
 
  static visitSystemInfoUpdate() {
    const username = Cypress.env("USERNAME");
    const password = Cypress.env("PASSWORD");
    const currentBaseUrl = Cypress.config().baseUrl;
    let url: any = currentBaseUrl ? "https://" + currentBaseUrl.replace(/(http(s)?:\/\/)|(\/.*){1}/g, "") + "/system-info-update" : "";

    if (username && password) {
      cy.log(url);
      cy.visit(url, {
        auth: {
          username: username,
          password: password
        }
      });
    } else {
      cy.visit(url);
    }
  }

  static validateNoInfoMessage() {
    cy.get('[id="validateNoInfoMessage"]').should("not.exist");
    cy.get('[id="systemInfoMessage"]').should("not.exist");
  }

  static openSystemInfoUpdate() {
    cy.get('[id="gwt-debug-message"]').should("exist");
    cy.get('[id="gwt-debug-locale"]').should("exist");
    cy.get('[id="gwt-debug-delete-all"]').should("exist");
    cy.get('[id="gwt-debug-submit"]').should("exist");
  }

  static addSystemInfoMessage(message: string) {
    cy.get('[id="gwt-debug-message"]').type(message);
    cy.get('[id="gwt-debug-uuid"]').type(Cypress.env("SYSTEM_INFO_UUID"));
    cy.get('[id="gwt-debug-submit"]').click();
  }

  static validateSystemInfoMessage(message: string) {
    cy.get('[id="systemInfoCenter"]').parent().should("have.class", "aer-aux").and("be.visible");
    cy.get('[id="systemInfoMessage"]').should("have.text", message).and("be.visible");
  }

  static removeSystemInfoMessage() {
    cy.get('[id="gwt-debug-delete-all"]').check({ force: true });
    cy.get('[id="gwt-debug-uuid"]').type(Cypress.env("SYSTEM_INFO_UUID"));
    cy.get('[id="gwt-debug-submit"]').click();
  }
}