/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class RoadTransportationSourceRoadTrafficPage {

  static chooseRoadTypeDirection(roadType: string, direction: string) {
    cy.get('[id="collapsibleSubSources"]').click();
    cy.get('[id="roadTypeCategory"]').select(roadType);
    switch (direction) {
      case "Beide richtingen":
      case "Two way":
        cy.get('[id="tab-trafficDirection-BOTH"]').click();
        break;
      case "Van A naar B":
      case "From A to B":
        cy.get('[id="tab-trafficDirection-A_TO_B"]').click();
        break;
      case "Van B naar A":
      case "From B to A":
        cy.get('[id="tab-trafficDirection-B_TO_A"]').click();
        break;
      default:
        throw new Error("Unknown road type direction: " + direction);
    }
  }

  static chooseMaxSpeed(maxSpeed: string) {
    cy.get('[id="emissionSourceRoadMaxSpeed"]').select(maxSpeed);
  }

  static chooseTimeUnit(timeUnit: string) {
    cy.get('[id="timeUnit"]').select(timeUnit);
  }

  static chooseTrafficTypeNumberVehiclesAndStagnation(trafficType: string, numberOfVehicles: string, stagnationPercentage: string) {
    switch (trafficType) {
      case "Licht verkeer":
        cy.get('[id="numberOfVehiclesRow_LIGHT_TRAFFIC"]').clear({ force: true }).type(numberOfVehicles);
        cy.get('[id="EmissionSourceRoadStagnationRow_LIGHT_TRAFFIC"]').clear({ force: true }).type(stagnationPercentage);
        break;
      case "Middelzwaar vrachtverk.":
        cy.get('[id="numberOfVehiclesRow_NORMAL_FREIGHT"]').clear({ force: true }).type(numberOfVehicles);
        cy.get('[id="EmissionSourceRoadStagnationRow_NORMAL_FREIGHT"]').clear({ force: true }).type(stagnationPercentage);
        break;
      case "Zwaar vrachtverkeer":
        cy.get('[id="numberOfVehiclesRow_HEAVY_FREIGHT"]').clear({ force: true }).type(numberOfVehicles);
        cy.get('[id="EmissionSourceRoadStagnationRow_HEAVY_FREIGHT"]').clear({ force: true }).type(stagnationPercentage);
        break;
      case "Busverkeer":
        cy.get('[id="numberOfVehiclesRow_AUTO_BUS"]').clear({ force: true }).type(numberOfVehicles);
        cy.get('[id="EmissionSourceRoadStagnationRow_AUTO_BUS"]').clear({ force: true }).type(stagnationPercentage);
        break;
      default:
        throw new Error("Unknown Vehicle type: " + trafficType);
    }
  }

  static directionCorrectlySaved(direction: string) {
    cy.get('[id="srm2DetailDrivingDirection"]').should("have.text", direction);
  }

  static maxSpeedCorrectlySaved(maxSpeed: string) {
    cy.get('[id="detailRoadStandardVehicleMaxSpeed-0"]').should("have.text", maxSpeed);
  }

  static trafficTypeNumberVehiclesAndStagPercentageCorrectlySaved(trafficType: string, numberOfVehicles: string, stagnationPercentage: string) {
    switch (trafficType) {
      case "Licht verkeer":
        cy.get('[id="detailRoadStandardVehicleTypeDescription-0-0"] > .row > div:first').should("have.text", numberOfVehicles);
        cy.get('[id="detailRoadStandardVehicleTypeDescription-0-0"] > .row > div:last').should("have.text", stagnationPercentage);
        break;
      case "Middelzwaar vrachtverk.":
        cy.get('[id="detailRoadStandardVehicleTypeDescription-0-1"] > .row > div:first').should("have.text", numberOfVehicles);
        cy.get('[id="detailRoadStandardVehicleTypeDescription-0-1"] > .row > div:last').should("have.text", stagnationPercentage);
        break;
      case "Zwaar vrachtverkeer":
        cy.get('[id="detailRoadStandardVehicleTypeDescription-0-2"] > .row > div:first').should("have.text", numberOfVehicles);
        cy.get('[id="detailRoadStandardVehicleTypeDescription-0-2"] > .row > div:last').should("have.text", stagnationPercentage);
        break;
      case "Busverkeer":
        cy.get('[id="detailRoadStandardVehicleTypeDescription-0-3"] > .row > div:first').should("have.text", numberOfVehicles);
        cy.get('[id="detailRoadStandardVehicleTypeDescription-0-3"] > .row > div:last').should("have.text", stagnationPercentage);
        break;
      default:
        throw new Error("Unknown Vehicle stagnation type: " + trafficType);
    }
  }

  static trafficTypeNumberVehiclesAndStagPercentageInputFieldsHaveValues(trafficType: string, numberOfVehicles: string, stagnationPercentage: string) {
    switch (trafficType) {
      case "Licht verkeer":
        cy.get('[id="numberOfVehiclesRow_LIGHT_TRAFFIC"]').should("have.value", numberOfVehicles);
        cy.get('[id="EmissionSourceRoadStagnationRow_LIGHT_TRAFFIC"]').should("have.value", stagnationPercentage);
        break;
      case "Middelzwaar vrachtverk.":
        cy.get('[id="numberOfVehiclesRow_NORMAL_FREIGHT"]').should("have.value", numberOfVehicles);
        cy.get('[id="EmissionSourceRoadStagnationRow_NORMAL_FREIGHT"]').should("have.value", stagnationPercentage);
        break;
      case "Zwaar vrachtverkeer":
        cy.get('[id="numberOfVehiclesRow_HEAVY_FREIGHT"]').should("have.value", numberOfVehicles);
        cy.get('[id="EmissionSourceRoadStagnationRow_HEAVY_FREIGHT"]').should("have.value", stagnationPercentage);
        break;
      case "Busverkeer":
        cy.get('[id="numberOfVehiclesRow_AUTO_BUS"]').should("have.value", numberOfVehicles);
        cy.get('[id="EmissionSourceRoadStagnationRow_AUTO_BUS"]').should("have.value", stagnationPercentage);
        break;
      default:
        throw new Error("Unknown Vehicle stagnation type: " + trafficType);
    }
  }

  static fillDescription(description: string) {
    cy.get('[id="emissionSourceRoadCustomDescription"]').clear().type(description);
  }

  static chooseTimeUnitAndNumberOfVehicles(timeUnit: string, numberOfVehicles: string) {
    cy.get('[id="timeUnit"]').select(timeUnit);
    cy.get('[id="numberOfVehiclesInput"]').clear().type(numberOfVehicles);
  }

  /*
   * This function does not start with a clear, only text input
   * when we execute the clear the form / DOM is switchted so the
   * input wil change
   */
  static fillEmissionAndFractionForEuroClass(emission: string, fraction: string) {
    switch (emission) {
      case "NO2":
        cy.get('[id="emissionFactorInput_NO2"]').type(fraction);
        break;
      case "NOX":
        cy.get('[id="emissionFactorInput_NOX"]').type(fraction);
        break;
      case "NH3":
        cy.get('[id="emissionFactorInput_NH3"]').type(fraction);
        break;
      default:
        throw new Error("Unknown emission for euroclass: " + emission);
    }
  }

  static emissionAndFractionIsCorrectlySaved(emission: string, fraction: string) {
    switch (emission) {
      case "NO2":
        cy.get('[id="detailRoadCustomVehicleSubstance-0-NO2"]').should("have.text", fraction);
        break;
      case "NOX":
        cy.get('[id="detailRoadCustomVehicleSubstance-0-NOX"]').should("have.text", fraction);
        break;
      case "NH3":
        cy.get('[id="detailRoadCustomVehicleSubstance-0-NH3"]').should("have.text", fraction);
        break;
      default:
        throw new Error("Unknown emission correctly saved: " + emission);
    }
  }

  static descriptionCorrectlySaved(description: string) {
    cy.get('[id="detailRoadCustomVehicleDescription-0"]').should("have.text", description);
  }

  static timeUnitAndNumberOfVehiclesIsCorrectlySaved(timeUnit: string, numberOfVehicles: string) {
    cy.get('[id="detailRoadCustomVehicleAmount-0"]').should("have.text", numberOfVehicles + " " + timeUnit);
  }

  static timeUnitAndNumberOfVehiclesForEuroClassIsCorrectlySaved(timeUnit: string, numberOfVehicles: string) {
    cy.get('[id="detailRoadSpecificVehicleAmount-0"]').should("have.text", numberOfVehicles + " " + timeUnit);
  }

  static assertAmountOfCustomVehiclesContainsTimeUnit(typeOfVehicles: string, row: string, timeUnit: string) {
    if (typeOfVehicles == "standard") {
      cy.get('[id="detailRoadStandardVehicleTraffic-' + row + '"]').should("contain", timeUnit);
    } else if (typeOfVehicles == "custom") {
      cy.get('[id="detailRoadCustomVehicleAmount-' + row + '"]').should("contain", timeUnit);
    } else {
      throw new Error("Unknown type of vehicle: " + typeOfVehicles);
    }
  }
  
}
