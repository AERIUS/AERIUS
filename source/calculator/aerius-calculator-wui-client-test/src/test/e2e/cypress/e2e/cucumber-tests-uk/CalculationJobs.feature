#
# Crown copyright
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Calculation jobs
    Test everything to do with calculation jobs.

    Scenario: Validate whether calculation can be started with/without certain selections
        Given I login with correct username and password
        When I select file 'UKAPAS_agriculture_calc_points.zip' to be imported
        And I navigate to the 'Calculation jobs' tab
        
        # Default values should be set and errors should be hidden by default
        When I create a new calculation job
        #Then I expect the calculation distance value to be set
        #And I expect the calculation distance error to be hidden
        And I expect the min. Monin-Obukhov length value to be set
        And I expect the min. Monin-Obukhov length error to be hidden
        And I expect the surface albedo value to be set
        And I expect the surface albedo error to be hidden
        And I expect the Priestley-Taylor parameter value to be set
        And I expect the Priestley-Taylor parameter error to be hidden
        
        # Test empty values
        When I select zone of influence ""
        And I select min. Monin-Obukhov length ""
        And I select surface albedo ""
        And I select Priestley-Taylor parameter ""

        Then I expect the zone of influence error to be shown
        And I expect the min. Monin-Obukhov length error to be shown
        And I expect the surface albedo error to be shown
        And I expect the Priestley-Taylor parameter error to be shown

        # Test min values
        When I select zone of influence "0"
        And I select min. Monin-Obukhov length "1"
        And I select surface albedo "0"
        And I select Priestley-Taylor parameter "0"

        Then I expect the calculation distance error to be hidden
        And I expect the min. Monin-Obukhov length error to be hidden
        And I expect the surface albedo error to be hidden
        And I expect the Priestley-Taylor parameter error to be hidden
        
        # Test too low values
        When I select zone of influence "-0.0000001"
        And I select min. Monin-Obukhov length "0.99999999"
        And I select surface albedo "-0.0000001"
        And I select Priestley-Taylor parameter "-0.0000001"

        Then I expect the zone of influence error to be shown
        And I expect the min. Monin-Obukhov length error to be shown
        And I expect the surface albedo error to be shown
        And I expect the Priestley-Taylor parameter error to be shown

        # Test max values
        When I select zone of influence "999999999999999999999"
        And I select min. Monin-Obukhov length "200"
        And I select surface albedo "1"
        And I select Priestley-Taylor parameter "3"

        #Then I expect the calculation distance error to be hidden
        And I expect the min. Monin-Obukhov length error to be hidden
        And I expect the surface albedo error to be hidden
        And I expect the Priestley-Taylor parameter error to be hidden

        # Test too high values
        And I select min. Monin-Obukhov length "200.00001"
        And I select surface albedo "1.0000001"
        And I select Priestley-Taylor parameter "3.0000001"

        And I expect the min. Monin-Obukhov length error to be shown
        And I expect the surface albedo error to be shown
        And I expect the Priestley-Taylor parameter error to be shown

    @Smoke
    Scenario: Import calculation and validate max temporary effect calculation job
        Given I login with correct username and password
        And I create a new source from the start page situation label 'PROPOSED'
        And I name the source 'Bron 1' and select sector group 'Energy' and sector 'Other'
        And I fill location with coordinates 'POINT(340816.79 729703.62)'
        And I choose source height '25' and source diameter '0.5'
        And I fill the emission fields with NOx '8' and NH3 '40'
        And I save the data
        And I duplicate the situation
        And I add situation label 'TEMPORARY'
        And I select scenarioType 'TEMPORARY'
        And I save the scenario updates
        And I select the source 'Bron 1'
        And I choose to edit the source
        And I fill the emission fields with NOx '10' and NH3 '50'
        And I save the data
        And I duplicate the situation
        And I add situation label 'REFERENCE'
        And I select scenarioType 'REFERENCE'
        And I save the scenario updates
        And I select the source 'Bron 1'
        And I choose to edit the source
        And I fill the emission fields with NOx '22' and NH3 '220'
        And I save the data
        And I duplicate the situation
        And I add situation label 'OFF_SITE_REDUCTION'
        And I select scenarioType 'OFF_SITE_REDUCTION'
        And I save the scenario updates
        And I select the source 'Bron 1'
        And I choose to edit the source
        And I fill the emission fields with NOx '5' and NH3 '120'
        And I save the data
        And I duplicate the situation
        And I add situation label 'COMBINATION_PROPOSED'
        And I select scenarioType 'COMBINATION_PROPOSED'
        And I save the scenario updates
        And I select the source 'Bron 1'
        And I choose to edit the source
        And I fill the emission fields with NOx '4' and NH3 '110'
        And I save the data
        And I duplicate the situation
        And I add situation label 'COMBINATION_REFERENCE'
        And I select scenarioType 'COMBINATION_REFERENCE'
        And I save the scenario updates
        And I select the source 'Bron 1'
        And I choose to edit the source
        And I fill the emission fields with NOx '3' and NH3 '90'
        And I save the data
        When I navigate to the 'Calculation jobs' tab
        Then I validate the calculation job message 'There are no calculation jobs yet. Create a new calculation job by pressing the button below.'
        And I create a new calculation job

        # Allowed scenarios for calculation job type: 'Process contribution'
        When I select calculation jobType 'Process contribution'
        Then I can select 'Reference scenario': 'REFERENCE'
        And I can select 'Project scenario': 'PROPOSED'
        And I can select 'Off-site reduction scenario': 'OFF_SITE_REDUCTION'
        Then the following fields are not visible
        | option                        |
        | Temporary scenario            |
        | Include projects from archive |

        # Allowed scenarios for calculation job type: 'Maximum temporary effect'
        When I select calculation jobType 'Maximum temporary effect'
        Then I can select 'Temporary scenario': 'TEMPORARY'
        And I can select 'Reference scenario': 'REFERENCE'        
        And I can select 'Off-site reduction scenario': 'OFF_SITE_REDUCTION'
        Then the following fields are not visible
        | option                        |
        | Project scenario              |
        | Include projects from archive |

        # Allowed scenarios for calculation job type: 'In-combination process contribution'
        When I select calculation jobType 'In-combination process contribution'
        Then I can select 'Project scenario': 'PROPOSED' 
        And I can select 'Reference scenario': 'REFERENCE'        
        And I can select 'Off-site reduction scenario': 'OFF_SITE_REDUCTION'
        And I can select 'In-combination project': 'COMBINATION_PROPOSED'
        And I can select 'In-combination reference': 'COMBINATION_REFERENCE'                 
        Then the following fields are not visible
        | option                        |
        | Temporary scenario            |

        # Allowed scenarios for calculation job type: 'Single scenario'
        When I select calculation jobType 'Single scenario'
        Then I can select 'Scenario': '-'
        And I can select 'Scenario': 'PROPOSED'
        And I can select 'Scenario': 'TEMPORARY'
        And I can select 'Scenario': 'REFERENCE'
        And I can select 'Scenario': 'OFF_SITE_REDUCTION'
        And I can select 'Scenario': 'COMBINATION_PROPOSED'
        And I can select 'Scenario': 'COMBINATION_REFERENCE'                         

    Scenario: Visible fields for calculation method: 'Quick run'
        Given I login with correct username and password
        When I select file 'UKAPAS_agriculture_calc_points.zip' to be imported
        When I navigate to the 'Calculation jobs' tab

        Then I create a new calculation job
        And I select calculation jobType 'Process contribution'
        And I select calculation method 'Quick run'
        Then the following fields are visible and editable
        | option                        |
        | Zone of influence             |
        | Min. Monin-Obukhov length     |
        | Surface albedo                |
        | Priestly-Taylor parameter     |        
        | Plume depletion (NH3)         |
        | Plume depletion (NOx)         |
        | Complex terrain               |
        | Automatic receptor points     |
        | Custom assessment points      |
        | Area                          |
        And the following fields are not visible
        | option                        |        
        | Include projects from archive |
    
    Scenario: Visible fields for calculation method: 'Formal assessment'
        Given I login with correct username and password
        When I select file 'UKAPAS_agriculture_calc_points.zip' to be imported
        When I navigate to the 'Calculation jobs' tab
        Then I create a new calculation job
        And I select calculation jobType 'Process contribution'
        And I select calculation method 'Formal assessment'
        Then the following fields are visible and editable
        | option                        |
        | Area                          |
        | Min. Monin-Obukhov length     |
        | Surface albedo                |
        | Priestly-Taylor parameter     |         
        | Plume depletion (NH3)         |
        | Plume depletion (NOx)         |
        | Complex terrain               |
        | Automatic receptor points     |
        | Custom assessment points      |
        And the following fields are not visible
        | option                        |
        | Zone of influence             |
        | Include projects from archive |

    Scenario: Visible fields for calculation method: 'Custom assessment'
        Given I login with correct username and password
        When I select file 'UKAPAS_agriculture_calc_points.zip' to be imported
        When I navigate to the 'Calculation jobs' tab
        Then I create a new calculation job
        And I select calculation jobType 'Process contribution'
        And I select calculation method 'Custom assessment'
        Then the following fields are visible and editable
        | option                        |
        | Zone of influence             |
        | Min. Monin-Obukhov length     |
        | Surface albedo                |
        | Priestly-Taylor parameter     |
        | Plume depletion (NH3)         |
        | Plume depletion (NOx)         |
        | Complex terrain               |
        | Automatic receptor points     |
        | Custom assessment points      |
        | Area                          |
        And the following fields are not visible
        | option                        |        
        | Include projects from archive |

    Scenario: Visible fields for calculation method: 'Custom assessment points'
        Given I login with correct username and password
        When I select file 'UKAPAS_agriculture_calc_points.zip' to be imported
        When I navigate to the 'Calculation jobs' tab
        Then I create a new calculation job
        And I select calculation jobType 'Process contribution'
        And I select calculation method 'Custom assessment points'
        Then the following fields are visible and editable
        | option                        |
        | Min. Monin-Obukhov length     |
        | Surface albedo                |
        | Priestly-Taylor parameter     |
        | Plume depletion (NH3)         |
        | Plume depletion (NOx)         |
        | Complex terrain               |
        | Automatic receptor points     |
        | Custom assessment points      |
        | Area                          |
        And the following fields are not visible
        | option                        |        
        | Zone of influence             |
        | Include projects from archive |

  Scenario: Visible checkbox 'Include projects from archive' for calculation method: 'In-combination process'
        Given I login with correct username and password
        When I select file 'UKAPAS_agriculture_calc_points.zip' to be imported
        When I navigate to the 'Calculation jobs' tab
        Then I create a new calculation job
        Then the following fields are not visible
        | option                        |
        | Include projects from archive |
        When I select calculation jobType 'In-combination process contribution'
        Then the following fields are visible and editable
        | option                        |
        | Include projects from archive |

  Scenario: Validate MET site location method
        Given I login with correct username and password
        When I select file 'UKAPAS_agriculture_calc_points.zip' to be imported
        When I navigate to the 'Calculation jobs' tab

        Then I create a new calculation job
        And I select the uk meteorological site location selection method 'Observational'
        And I select meteorological data type 'Raw >90% Random'
        And I select the uk meteorological site location selection method 'Suggested'
        Then the met site location shows '7' options
        And I select the uk meteorological site location selection method 'Search'
        Then the met site location shows '149' options

        And I select meteorological data type 'Interpolated >75% Random'
        And I select the uk meteorological site location selection method 'Suggested'
        Then the met site location shows '5' options
        And I select the uk meteorological site location selection method 'Search'
        Then the met site location shows '5' options

        And I select meteorological data type 'Wind sectors >90% Random'
        And I select the uk meteorological site location selection method 'Suggested'
        Then the met site location shows '7' options
        And I select the uk meteorological site location selection method 'Search'
        Then the met site location shows '75' options
        
        And I select the uk meteorological site location selection method 'NWP'
        And I select the uk meteorological site location selection method 'Suggested'
        Then the met site location shows '7' options
        And the meteorological year is not selectable
        And I select the meteorological site location '1.1 km - 002 Antrim Lurigethan'
        #And I select the meteorological year '2021'

        And I select the uk meteorological site location selection method 'Search'
        Then the met site location shows '4612' options
        Then I search meteorological site location '100_200'
        Then the met site location shows '2' options
        And I select the meteorological site location '(BETA3) 100_200'
        And I select the first meteorological site location
        #And I select the meteorological year '2022'

    Scenario: Availability of Development pressure search panel is depending on selected job type
        Given I login with correct username and password
        And I select file 'UKAPAS_agriculture_calc_points.zip' to be imported
        And I navigate to the 'Calculation jobs' tab
        And I create a new calculation job

        When I select calculation jobType 'Process contribution'
        Then the Development pressure search panel is 'visible'

        When I select calculation jobType 'Maximum temporary effect'
        Then the Development pressure search panel is 'not visible'

        When I select calculation jobType 'In-combination process contribution'
        Then the Development pressure search panel is 'visible'

        When I select calculation jobType 'Single scenario'
        Then the Development pressure search panel is 'not visible'

    Scenario: add, edit, copy and delete calculation job
        Given I login with correct username and password
        And I select file 'UKAPAS_all_scenario_types.zip' to be imported
        And the following notification is displayed 'Single file imported successfully.'
        And I navigate to the 'Calculation jobs' tab

        # add a calculation job
        When I create a new calculation job
        And I select calculation jobType 'Process contribution'
        And I can select 'Project scenario': 'Scenario 1'
        And I can select 'Reference scenario': 'Scenario 2'
        And I can select 'Off-site reduction scenario': 'Scenario 3'
        And I select country 'England'
        And I select project category 'Other'
        And I select the first meteorological site location
        And I select the meteorological year '2022'
        And I save the calculation job

        # copy
        And I copy the selected calculation job
        Then I expect the following calculation job to exist 'Copy of Calculation job 1'
        And I expect that the calculation job is of type 'Process contribution'
        And I expect that the calculation method is 'Quick run'
        And I expect the 'REFERENCE' scenario to be 'Scenario 2'
        And I expect the 'PROJECT' scenario to be 'Scenario 1'
        And I expect the 'OFF_SITE_REDUCTION' scenario to be 'Scenario 3'
        And I expect the selected sources for development pressure search to be '2/2'
        And I expect the following Meteorological settings
            | type          | dataType        | location          | year |
            | Observational | Raw >90% Random | (BETA3) WITTERING | 2022 |
        And I expect the following Advanced settings
            | min_MO_length | surface_albedo | PT_param | plume_NH3 | plume_NOx | complex_terrain |
            | 30.00 m       | 0.23           | 1.00     | No        | No        | No              |
        And I expect the following Road emissions options
            | autom_receptor_points     | custom_receptor_points    | 
            | Default based on location | Default based on location |

        # edit calculation job
        When I edit the selected calculation job
        And I select calculation method 'Formal assessment'
        And I select country 'Scotland'
        And I select project category 'Agriculture'
        And I can select 'Reference scenario': 'Scenario 2'
        And I can select 'Off-site reduction scenario': 'Scenario 3'
        And I select development pressure sources
            | source   |
            | Source 1 |
        And I select the meteorological year '2021'
        And I save the calculation job
        Then I expect that the calculation job is of type 'Process contribution'
        And I expect that the calculation method is 'Formal assessment'
        And I expect the 'REFERENCE' scenario to be 'Scenario 2'
        And I expect the 'PROJECT' scenario to be 'Scenario 1'
        And I expect the 'OFF_SITE_REDUCTION' scenario to be 'Scenario 3'
        And I expect the selected sources for development pressure search to be '1/2'
        And I expect the following Meteorological settings
            | type          | dataType        | location          | year |
            | Observational | Raw >90% Random | (BETA3) WITTERING | 2021 |
        And I expect the following Advanced settings
            | min_MO_length | surface_albedo | PT_param | plume_NH3 | plume_NOx | complex_terrain |
            | 30.00 m       | 0.23           | 1.00     | No        | No        | No              |
        And I expect the following Road emissions options
            | autom_receptor_points     | custom_receptor_points    | 
            | Default based on location | Default based on location |

        # copy
        When I copy the selected calculation job
        Then I expect the following calculation job to exist 'Copy of Calculation job 1'
        And I expect that the calculation job is of type 'Process contribution'
        And I expect that the calculation method is 'Formal assessment'
        And I expect the 'REFERENCE' scenario to be 'Scenario 2'
        And I expect the 'PROJECT' scenario to be 'Scenario 1'
        And I expect the 'OFF_SITE_REDUCTION' scenario to be 'Scenario 3'
        And I expect the selected sources for development pressure search to be '1/2'
        And I expect the following Meteorological settings
            | type          | dataType        | location          | year |
            | Observational | Raw >90% Random | (BETA3) WITTERING | 2021 |
        And I expect the following Advanced settings
            | min_MO_length | surface_albedo | PT_param | plume_NH3 | plume_NOx | complex_terrain |
            | 30.00 m       | 0.23           | 1.00     | No        | No        | No              |
        And I expect the following Road emissions options
            | autom_receptor_points     | custom_receptor_points    | 
            | Default based on location | Default based on location |

        # delete
        When I delete the selected calculation job
        Then I expect the following calculation job to exist 'Calculation job 1'
        And I expect the following calculation job not to exist 'Copy of Calculation job 1'
