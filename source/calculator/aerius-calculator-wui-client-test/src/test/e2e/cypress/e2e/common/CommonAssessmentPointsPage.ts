/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class CommonAssessmentPointsPage {
  static createFirstNewPoint() {
    cy.get('[id="calculationPointsListNewPoint"]').click();
  }

  static createNewPoint() {
    cy.get('[id="sourceListButtonNewSource"]').click();
  }

  static enterCoordinates(coordinates: string) {
    cy.get('[id="calculationPointGeometryInput"]').clear().type(coordinates);
  }

  static assertCoordinatesAre(coordinates: string) {
    cy.get('input[id="calculationPointGeometryInput"]').should("have.value", coordinates);
  }

  static savePoint() {
    cy.get('[id="buttonSaveAssessmentPoint"]').click();
  }

  static assertNumberOfAssessmentPoints(numberOfAssessmentPoints: number) {
    if (numberOfAssessmentPoints == 0) {
      cy.get('[id="emptyCalculationPoints"]').should("exist");
    } else {
      cy.get('[id^="calculationPoint-"][id$="-row"]').should("have.length", numberOfAssessmentPoints);
    }
  }

  static enterName(name: string) {
    cy.get('[id="label"]').clear({force: true}).type(name);
  }

  static assertPointExists(name: string) {
    cy.get('[id^="calculationPoint-"][id$="-label"]').contains(name);
  }

  static assertAssessmentPoints(dataTable: any) {
    dataTable.hashes().forEach((assessmentPoint: any) => {
      cy.get('[id="' + assessmentPoint.rowId + '"]').contains(assessmentPoint.name);
    });
  }

  static assertPointDoesNotExists(name: string) {
    cy.get('[id^="calculationPoint-"][id$="-label"]').contains(name).should("not.exist");
  }

  static selectPoint(name: string) {
    cy.get('[id^="calculationPoint-"][id$="-label"]').contains(name).click();
  }

  static toggleMultiplePoints(dataTable: any) {
    dataTable.hashes().forEach((assesmentPoint: any) => {
      cy.get('[id="' + assesmentPoint.assessmentId + '-row"]').click({ ctrlKey: true });
    });
  }

  static assertCalculationPointsAreSelected(dataTable: any, isSelected: boolean) {
    dataTable.hashes().forEach((assessmentPoint: any) => {
      if (isSelected) {
        cy.get('[id="' + assessmentPoint.assessmentId + '-row"]').should("have.class", "selected");
      } else {
        cy.get('[id="' + assessmentPoint.assessmentId + '-row"]').should("not.have.class", "selected");
      }
    });
  }

  static updatePoint() {
    cy.get('[id="sourceListButtonEditSource"]').click();
  }

  static deleteAllPoints() {
    cy.get('[id="calculationPointsListButtonDeleteAll"]').click();
  }

  static deleteSelectedPoint() {
    cy.get('[id="sourceListButtonDeleteSource"]').click();
  }

  static assertErrorExists() {
    cy.get(".error").should("exist");
  }

  static enterEmptyName() {
    cy.get('[id="label"]').clear();
  }

  static calculationPointCorrectlySaved(nameCalculationPoint: string) {
    cy.get('[id="calculationPoints"]').contains(nameCalculationPoint);
  }

  // Statistics
  static openAssessmentPointCharacteristics() {
    cy.get('[id="point-characteristics-(height-and-fno₂)-title"]').click();
  }
  
  static assertAssessmentPointCategoryRoadHeightCustomPrimaryFraction(category: string, roadHeight: string, customPrimaryFraction: string) {
    cy.get('[id="sourceListCategory"]').find("option:selected").should("have.value", category);
    cy.get('[id="pointRoadHeight"]').should("have.value", roadHeight);
    cy.get('[id="pointRoadFractionNO2"]').should("have.value", customPrimaryFraction);
  }

  // Habitats
  static openAssessmentPointHabitats() {
    cy.get('[id="assign-habitat-or-species"]').click();
  }

  static selectHabitat(habitatName: string) {
    cy.get('[id^="CalculationPointHabitatRow-"]').contains(habitatName).click();
  }
}
