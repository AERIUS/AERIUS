#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Import calculation results

    Scenario: Import GML file with project calculation results and validate the results on then map
        Given I login with correct username and password
        And I select file 'UKAPAS_calculation_with_results.gml' to be imported
        Then the following notification is displayed 'Single file imported successfully.'
        Then the situation name is 'Scenario'
        And the situation type is 'Project'
        And the calculation year is '2022'

        And I navigate to the 'Results' tab

        Then the following calculation job notification is displayed 'Calculation job 1 - UKAPAS_calculation_with_results' status 'started'
        Then the following calculation job notification is displayed 'Calculation job 1 - UKAPAS_calculation_with_results' status 'finished'

        Then I select situation with label 'Scenario - Project' and validate the 'Process contribution' with Pollutant 'Deposition NOₓ + NH₃' and results for 'Fixed assessment grid'
        And I check if the deposition tab has 11 results
        Then I validate nature area chart output for selected nature area with pollutant 'Deposition'
            | name                                   | pathsCount |
            | Madbanks and Ledsham Banks (SSSI_ASSI) | 180        |

        Then I select situation with label 'Scenario - Project' and validate the 'Process contribution' with Pollutant 'Concentration NH₃' and results for 'Fixed assessment grid'
        And I check if the deposition tab has 11 results
        Then I validate nature area chart output for selected nature area with pollutant 'Concentration'
            | name                                   | pathsCount |
            | Linton Common (SSSI_ASSI)              | 6          |

        Then I select situation with label 'Scenario - Project' and validate the 'Process contribution' with Pollutant 'Concentration NOₓ' and results for 'Fixed assessment grid'
        And I check if the critical levels tab has 11 results
        And I check if the deposition tab has 11 results
        Then I validate nature area chart output for selected nature area with pollutant 'Concentration'
            | name                                   | pathsCount |
            | Stutton Ings (SSSI_ASSI)               | 14         |
    
        Then I select situation with label 'Scenario - Project' and validate the 'Process contribution' with Pollutant 'Deposition NOₓ + NH₃' and results for 'Fixed assessment grid'
        And I open map layer and validate total percentage cl
            | name                                   | pathsCount |
            | Townclose Hills (SSSI_ASSI)            | 58         |
        And I open map layer and validate percentage cl
            | name                                   | pathsCount |
            | Fairburn and Newton Ings (SSSI_ASSI)   | 168        |

    Scenario: Import GML file with assessment points calculation results and validate the results
        Given I login with correct username and password
        And I select file 'UKAPAS_calculation_with_assessment_points_results.gml' to be imported
        Then the following notification is displayed 'Single file imported successfully.'
        Then the situation name is 'Scenario 1'
        And the situation type is 'Project'
        And the calculation year is '2023'

        Then the following calculation job notification is displayed 'Calculation job 1 - UKAPAS_calculation_with_assessment_points_results' status 'started'
        Then the following calculation job notification is displayed 'Calculation job 1 - UKAPAS_calculation_with_assessment_points_results' status 'finished'

        And I navigate to the 'Results' tab
        Then I select calculation results tab 'Summary'
        Then I select calculation results tab 'Scenarios'
        Then I select calculation results tab 'Results'

        Then I validate that the situation 'Scenario 1 - Project' has results combinations
            | resultSelectId     | resultSelectLabel                         |
            | resultsResultType  | Scenario contribution                     |
            | resultsHexagonType | Custom assessment points                  |
            | resultsResultType  | Process contribution                      |
            | resultsHexagonType | Custom assessment points                  |
        
        Then I select situation with label 'Scenario 1 - Project' and validate the 'Process contribution' result screen with results for the result 'Custom assessment points'
            | elementId                      | expectedResult |
            | countCalculationPoints         | 3              |
            | countCalculationPointsIncrease | 3              |
            | countCalculationPointsDecrease | 0              |
            | maxIncrease                    | 0.055          |
            | maxDecrease                    | 0.000          |
        Then I validate the calculation point results
            | id | name                    | expectedResult |
            | 1  | Assessment point 1      | 0.055          |
            | 3  | Assessment point 3      | 0.010          |
            | 2  | Assessment point 2      | 0.010          |
        And I check if the calculation points tab has 3 results

        Then I select situation with label 'Scenario 1 - Project' and validate the 'Scenario contribution' result screen with results for the result 'Custom assessment points'
            | elementId                      | expectedResult |
            | countCalculationPoints         | 3              |
            | maxContribution                | 0.055          |
        Then I validate the calculation point results
            | id | name                    | expectedResult |
            | 1  | Assessment point 1      | 0.055          |
            | 3  | Assessment point 3      | 0.010          |
            | 2  | Assessment point 2      | 0.010          |
        And I check if the calculation points tab has 3 results

    Scenario: Import GML file with project calculation results and validate the results
        Given I login with correct username and password
        And I select file 'UKAPAS_calculation_with_results.gml' to be imported
        Then the following notification is displayed 'Single file imported successfully.'
        Then the situation name is 'Scenario'
        And the situation type is 'Project'
        And the calculation year is '2022'

        Then the following calculation job notification is displayed 'Calculation job 1 - UKAPAS_calculation_with_results' status 'started'
        Then the following calculation job notification is displayed 'Calculation job 1 - UKAPAS_calculation_with_results' status 'finished'
        
        And I navigate to the 'Results' tab
        Then I select calculation results tab 'Summary'
        Then I select calculation results tab 'Scenarios'
        Then I select calculation results tab 'Results'

        Then I validate that the situation 'Scenario - Project' has results combinations
            | resultSelectId     | resultSelectLabel      |
            | resultsResultType  | Scenario contribution  |
            | resultsHexagonType | Fixed assessment grid  |
            | resultsResultType  | Process contribution   |
            | resultsHexagonType | Fixed assessment grid  |

        Then I select situation with label 'Scenario - Project' and validate the 'Process contribution' result screen with results for the result 'Fixed assessment grid'
            | elementId                      | expectedResult |
            | sumCartographicSurface         | 2,588.76       |
            | maxPercentageCriticalLevel     | 1,512 %        |
            | sumCartographicSurfaceIncrease | 2,588.76       |
            | maxIncrease                    | 151.239        |
            | sumCartographicSurfaceDecrease | 0.00           |
            | maxDecrease                    | -              |
        And I check if the critical levels tab has 11 results
        And I check if the deposition tab has 11 results
        And I check if the markers tab has 11 results and the text 'Highest PEC (kg N/ha/y)'
        And I check if the habitat tab has 11 results
        Then I select situation with label 'Scenario - Project' and validate the 'Scenario contribution' result screen with results for the result 'Fixed assessment grid'
            | elementId                      | expectedResult |
            | sumCartographicSurface         | 2,588.76       |
            | maxTotal                       | 164.922        |
            | maxContribution                | 151.239        |
        And I check if the critical levels tab has no results and the text 'This information is supported for the Process Contribution and Combined Contribution result types'
        And I check if the deposition tab has 11 results
        And I check if the markers tab has 11 results and the text 'Highest PEC (kg N/ha/y)'
        And I check if the habitat tab has 11 results

    @Smoke
    Scenario: Import GML file with in combination project calculation results and validate the results no archive results
        Given I login with correct username and password
        And I select file 'UKAPAS_in_combination_results_without_archive_results.zip' to be imported
        Then the following notification is displayed 'Single file imported successfully.'
        Then the situation name is 'Scenario 1'
        And the situation type is 'Project'
        And the calculation year is '2025'

        Then the following calculation job notification is displayed 'Calculation job 1 - UKAPAS_in_combination_results_without_archive_results' status 'started'
        Then the following calculation job notification is displayed 'Calculation job 1 - UKAPAS_in_combination_results_without_archive_results' status 'finished'

        And I navigate to the 'Results' tab

        Then I select calculation results tab 'Summary'
        And I check if the summary tab has 1 scenario contributions results
        And I check if the summary tab has 4 process contributions results

        Then I select calculation results tab 'Scenarios' and validate
            | elementId                     | expectedResult                                                                         |
            | .situationsContainer > h2     | User Scenarios                                                                         |
            | .archiveProjectsContainer >h2 | In-combination Archive projects                                                        |
            | .noProjectsContainer          | No relevant projects were found in the archive and there are no results for this view. |
        And I validate that Archive project results are retrieved on 'Results retrieved on 7 February 2024'

        Then I select calculation results tab 'Results'
        Then I select situation with label 'Scenario 1 - Project' and validate the 'Process contribution' with Pollutant 'Deposition NOₓ + NH₃' and results for 'Fixed assessment grid'
            | elementId                      | expectedResult |
            | sumCartographicSurface         | 628.96         |
            | maxPercentageCriticalLevel     | 1 %            |
            | sumCartographicSurfaceIncrease | 628.96         |
            | maxIncrease                    | 0.090          |
            | sumCartographicSurfaceDecrease | 0.00           |
            | maxDecrease                    | -              |
        And I check if the critical levels tab has 1 results
        And I check if the deposition tab has 1 results
        And I check if the markers tab has 1 results and the text 'Highest PEC (kg N/ha/y)'
        And I check if the habitat tab has 1 results
        Then I select situation with label 'Scenario 1 - Project' and validate the 'Process contribution' with Pollutant 'Concentration NOₓ' and results for 'Fixed assessment grid'
            | elementId                      | expectedResult |
            | countCalculationPoints         | 204            |
            | maxPercentageCriticalLevel     | 0 %            |
            | countCalculationPointsIncrease | 204            |
            | maxIncrease                    | 0.029          |
            | countCalculationPointsDecrease | 0              |
            | maxDecrease                    | -              |
        And I check if the critical levels tab has 1 results
        And I check if the deposition tab has 1 results
        And I check if the markers tab has 1 results and the text 'Highest PEC (µg/m³)'
        And I check if the habitat tab has 1 results
        Then I select situation with label 'Scenario 1 - Project' and validate the 'Process contribution' with Pollutant 'Concentration NH₃' and results for 'Fixed assessment grid'
            | elementId                      | expectedResult |
            | countCalculationPoints         | 204            |
            | maxPercentageCriticalLevel     | -              |
            | countCalculationPointsIncrease | 204            |
            | maxIncrease                    | 0.016          |
            | countCalculationPointsDecrease | 0              |
            | maxDecrease                    | -              |
        And I check if the critical levels tab has 1 results
        And I check if the deposition tab has 1 results
        And I check if the markers tab has 1 results and the text 'Highest PEC (µg/m³)'
        And I check if the habitat tab has 1 results

    Scenario: Import GML file with in combination project calculation results and validate the archive results
        Given I login with correct username and password
        And I select file 'UKAPAS_in_combination_results_with_archive_results.zip' to be imported
        Then the following notification is displayed 'Single file imported successfully.'
        Then the situation name is 'Scenario 1'
        And the situation type is 'Project'
        And the calculation year is '2025'

        Then the following calculation job notification is displayed 'Calculation job 1 - UKAPAS_in_combination_results_with_archive_results' status 'started'
        Then the following calculation job notification is displayed 'Calculation job 1 - UKAPAS_in_combination_results_with_archive_results' status 'finished'

        And I navigate to the 'Results' tab

        Then I select calculation results tab 'Summary'
        And I check if the summary tab has 1 scenario contributions results
        And I check if the summary tab has 4 process contributions results

        Then I select calculation results tab 'Scenarios' and validate with element index
            | elementId                       | elementIndex | expectedResult                                   |
            | .situationsContainer > h2       | 0            | User Scenarios                                   |
            | .archiveProjectsContainer >h2   | 0            | In-combination Archive projects                  |
            | [id=archiveProjectId]           | 1            | 1                                                |
            | [id=archiveProjectName]         | 1            | Road Scheme - A625 Grindleford                   |
            | [id=archiveProjectId]           | 2            | 2                                                |
            | [id=archiveProjectName]         | 2            | Biomass Boiler - Barbrook Cottage                |
            | .archiveProjectsContainer > div | 2            | Regulators can view the below projects using the |
        And I validate that Archive project results are retrieved on 'Results retrieved on 30 January 2024'
        And I validate that Archive project link contains project ids
            | projectId                            | 
            | 83045bc4-20aa-4f31-926c-d51266d3a69b | 
            | 625769ca-ad33-4625-85d6-5c42e44c9633 |

   Scenario: Import GML file validate the critical load results
        Given I login with correct username and password
        And I select file 'UKAPAS_Scenario_Critical_Load.gml' to be imported
        Then the following notification is displayed 'Single file imported successfully.'
        Then the situation name is 'Scenario 1'
        And the situation type is 'Project'
        And the calculation year is '2024'

        Then the following calculation job notification is displayed 'Calculation job 1 - UKAPAS_Scenario_Critical_Load' status 'started'
        Then the following calculation job notification is displayed 'Calculation job 1 - UKAPAS_Scenario_Critical_Load' status 'finished'

        And I navigate to the 'Results' tab

        Then I select calculation results tab 'Summary'
        And I check if the summary tab has 1 scenario contributions results
        And I check if the summary tab has 4 process contributions results
        And I validate process contributions result table values
            | sitename   | situationname | pollutant            | max_increase     | max_percentage_CL | max_total | max_total_percentage_CL |
            | Big Waters | Scenario 1    | Concentration NO₂    | 2.007 µg/m³      | -                 | 8.941     | -                       |
            | Big Waters | Scenario 1    | Concentration NOₓ    | 2.866 µg/m³      | 10 %              | 11.756    | 39 %                    |
            | Big Waters | Scenario 1    | Concentration NH₃    | 0.284 µg/m³      | -                 | 1.981     | -                       |
            | Big Waters | Scenario 1    | Deposition NOₓ + NH₃ | 0.489 kg N/ha/y  | -                 | 10.017    | -                       |

        Then I select calculation results tab 'Results'
        Then I select situation with label 'Scenario 1 - Project' and validate the 'Process contribution' with Pollutant 'Deposition NOₓ + NH₃' and results for 'Fixed assessment grid'
            | elementId                      | expectedResult |
            | sumCartographicSurface         | 18.47          |
            | maxPercentageCriticalLevel     | -              |
            | sumCartographicSurfaceIncrease | 18.47          |
            | maxIncrease                    | 0.489          |
            | sumCartographicSurfaceDecrease | 0.00           |
            | maxDecrease                    | -              |
        And I check if the critical levels tab has 1 results
        And I select site 'Big Waters (SSSI_ASSI)' and validate message 'This feature is not sensitive to nitrogen deposition and therefore has no assigned critical load.'
        And I check if the deposition tab has 1 results
        And I check if the markers tab has 1 results and the text 'Highest PEC (kg N/ha/y)'
        And I validate markers table values
            | area-title             | area-surface | resulttype-0 | resulttype-1 | resulttype-2 | resulttype-3 |
            | Big Waters (SSSI_ASSI) | 18.47        | 10.017       | -            | 0.489        | -            |
        And I check if the habitat tab has 1 results
        And I validate the habitat and species table values
            | area-title             | habitatcode                        | habitatname                  | surfaceOrPointsValue | minimum_CL | depositionValue |
            | Big Waters (SSSI_ASSI) | Phalaris Arundinacea Tall-Herb Fen | BH00:NVC149:EU000:NCL101:NSH | 18.47                | -          | -               |

        Then I select situation with label 'Scenario 1 - Project' and validate the 'Process contribution' with Pollutant 'Concentration NOₓ' and results for 'Fixed assessment grid'
            | elementId                      | expectedResult |
            | countCalculationPoints         | 719            |
            | maxPercentageCriticalLevel     | 10 %           |
            | countCalculationPointsIncrease | 719            |
            | maxIncrease                    | 2.866          |
            | countCalculationPointsDecrease | 0              |
            | maxDecrease                    | -              |
        And I check if the critical levels tab has 1 results
        And I select site 'Big Waters (SSSI_ASSI)' and validate no message exist
        And I check if the deposition tab has 1 results
        And I check if the markers tab has 1 results and the text 'Highest PEC (µg/m³)'
        And I validate markers table values
            | area-title             | area-surface | resulttype-0 | resulttype-1 | resulttype-2 | resulttype-3 |
            | Big Waters (SSSI_ASSI) | 719          | 11.756       | 10 %         | 2.866        | -            |
        And I check if the habitat tab has 1 results
        And I validate the habitat and species table values
            | area-title             | habitatcode                        | habitatname                  | surfaceOrPointsValue | minimum_CL | depositionValue |
            | Big Waters (SSSI_ASSI) | Phalaris Arundinacea Tall-Herb Fen | BH00:NVC149:EU000:NCL101:NSH | 719                  | 30.000     | 10 %            |

        Then I select situation with label 'Scenario 1 - Project' and validate the 'Process contribution' with Pollutant 'Concentration NH₃' and results for 'Fixed assessment grid'
            | elementId                      | expectedResult |
            | countCalculationPoints         | 719            |
            | maxPercentageCriticalLevel     | -              |
            | countCalculationPointsIncrease | 719            |
            | maxIncrease                    | 0.284          |
            | countCalculationPointsDecrease | 0              |
            | maxDecrease                    | -              |
        And I check if the critical levels tab has 1 results
        And I select site 'Big Waters (SSSI_ASSI)' and validate message 'This feature is sensitive to nitrogen deposition but has no assigned critical load. Please contact the relevant Statutory Nature Conservation Body for site-specific advice.'
        And I check if the deposition tab has 1 results
        And I check if the markers tab has 1 results and the text 'Highest PEC (µg/m³)'
        And I validate markers table values
            | area-title             | area-surface | resulttype-0 | resulttype-1 | resulttype-2 | resulttype-3 |
            | Big Waters (SSSI_ASSI) | 719          | 1.981        | -            | 0.284        | -            |
        And I check if the habitat tab has 1 results
        And I validate the habitat and species table values
            | area-title             | habitatcode                        | habitatname                  | surfaceOrPointsValue | minimum_CL | depositionValue |
            | Big Waters (SSSI_ASSI) | Phalaris Arundinacea Tall-Herb Fen | BH00:NVC149:EU000:NCL101:NSH | 719                  | -          | -               |
   @Visual 
   Scenario: Import GML file with in combination project calculation results and validate the in combination archive sites on the map
        Given I login with correct username and password
        And I select file 'UKAPAS_in_combination_results_with_archive_map_results.zip' to be imported
        Then the following notification is displayed 'Single file imported successfully.'
        Then the situation name is 'Scenario 1'
        And the situation type is 'Project'
        And the calculation year is '2024'
        And I navigate to the 'Results' tab
        Then the following calculation job notification is displayed 'Calculation job 1 - UKAPAS_in_combination_results_with_archive_map_results' status 'started'       
        Then the following calculation job notification is displayed 'Calculation job 1 - UKAPAS_in_combination_results_with_archive_map_results' status 'finished'
        And I validate archive project sites on the map

   Scenario: Import GML file and validate the decision framework results
        Given I login with correct username and password
        And I select file 'UKAPAS_DMT_North_Ireland_with_results.zip' to be imported
        Then the following notification is displayed 'Single file imported successfully.'
        Then the situation name is 'Scenario 1'
        And the situation type is 'Project'
        And the calculation year is '2025'

        Then the following calculation job notification is displayed 'Calculation job 1 - UKAPAS_DMT_North_Ireland_with_results' status 'started'
        Then the following calculation job notification is displayed 'Calculation job 1 - UKAPAS_DMT_North_Ireland_with_results' status 'finished'

        And I navigate to the 'Results' tab

        Then I select calculation results tab 'Summary'
        And I check if the summary tab has 3 scenario contributions results
        And I check if the summary tab has 12 process contributions results

        Then I select calculation results tab 'Decision Framework'
        And I validate decision-making thresholds info message 'The calculation results in a Process Contribution which requires further assessment. Please check the relevant thresholds per site and pollutant.'
        And I validate decision-making thresholds values
        | relevantThreshold | pollutant                       | compairThresholdRaw | compairThreshold | contribution | threshold |
        | false             | Nitrogen deposition (kg N/ha/y) | 511.623             | 512              | 0.047        | 0.0092    |
        | true              | NOₓ concentration (µg/m³)       | 5.81153             | 6                | 0.00081      | 0.014     |
        | false             | NH₃ concentration (µg/m³)       | 367.049             | 367              | 0.0029       | 0.00079   |

        And I validate site-relevant thresholds info message 'The calculation results in a Process Contribution where no further assessment of aerial emissions of nitrogen is required.'
        And I validate site-relevant thresholds values for site 'River Roe and Tributaries (SSSI_ASSI)' site id '7777'
        | relevantThreshold | pollutant                       | compairThresholdRaw | compairThreshold | contribution | threshold |
        | true              | Nitrogen deposition (kg N/ha/y) | 54.18300000000001   | 54               | 0.047        | 0.087     |
        | true              | NOₓ concentration (µg/m³)       | 0.935188            | 1                | 0.00081      | 0.087     |
        | N/A               | NH₃ concentration (µg/m³)       | N/A                 | N/A              | N/A          | N/A       |

        And I validate site-relevant thresholds values for site 'River Roe and Tributaries (SAC)' site id '646'
        | relevantThreshold | pollutant                       | compairThresholdRaw | compairThreshold | contribution | threshold |
        | true              | Nitrogen deposition (kg N/ha/y) | 54.18300000000001   | 54               | 0.047        | 0.087     |
        | true              | NOₓ concentration (µg/m³)       | 0.935188            | 1                | 0.00081      | 0.087     |
        | N/A               | NH₃ concentration (µg/m³)       | N/A                 | N/A              | N/A          | N/A       |

        And I validate site-relevant thresholds values for site 'Carn/Glenshane Pass (SAC)' site id '619'
        | relevantThreshold | pollutant                       | compairThresholdRaw | compairThreshold | contribution | threshold |
        | true              | Nitrogen deposition (kg N/ha/y) | 17.4117             | 17               | 0.015        | 0.089     |
        | true              | NOₓ concentration (µg/m³)       | 0.302998            | 0                | 0.00026      | 0.087     |
        | true              | NH₃ concentration (µg/m³)       | 38.6625             | 39               | 0.0029       | 0.0075    |


        
