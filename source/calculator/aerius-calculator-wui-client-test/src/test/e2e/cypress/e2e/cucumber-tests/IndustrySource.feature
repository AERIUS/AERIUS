#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Industry source

    Scenario: Create new Industry source starting at the start page
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Industrie' and sector 'Afvalverwerking'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose heat content type 'Niet geforceerd', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '10' and NH3 '20000'
        And I save the data

        And I create a new source from the source page
        And I name the source 'Bron 2' and select sector group 'Industrie' and sector 'Afvalverwerking'
        And I fill location with coordinates 'POINT(172303.2 599120.32)'
        And I fill the emission fields with NOx '10' and NH3 '20000'
        And I save the data

        And I create a new source from the source page
        And I name the source 'Bron 3' and select sector group 'Industrie' and sector 'Voedings- en genotmiddelen'
        And I fill location with coordinates 'POINT(172403.2 599120.32)'
        And I fill the emission fields with NOx '10' and NH3 '20000'
        And I save the data

        And I create a new source from the source page
        And I name the source 'Bron 4' and select sector group 'Industrie' and sector 'Chemische industrie'
        And I fill location with coordinates 'POINT(172503.2 599120.32)'
        And I fill the emission fields with NOx '10' and NH3 '20000'
        And I save the data

        And I create a new source from the source page
        And I name the source 'Bron 5' and select sector group 'Industrie' and sector 'Bouwmaterialen'
        And I fill location with coordinates 'POINT(172603.2 599120.32)'
        And I fill the emission fields with NOx '10' and NH3 '20000'
        And I save the data

        And I create a new source from the source page
        And I name the source 'Bron 6' and select sector group 'Industrie' and sector 'Basismetaal'
        And I fill location with coordinates 'POINT(172703.2 599120.32)'
        And I fill the emission fields with NOx '10' and NH3 '20000'
        And I save the data

        And I create a new source from the source page
        And I name the source 'Bron 7' and select sector group 'Industrie' and sector 'Metaalbewerkingsindustrie'
        And I fill location with coordinates 'POINT(172803.2 599120.32)'
        And I fill the emission fields with NOx '10' and NH3 '20000'
        And I save the data

        And I create a new source from the source page
        And I name the source 'Bron 8' and select sector group 'Industrie' and sector 'Overig'
        And I fill location with coordinates 'POINT(172903.2 599120.32)'
        And I fill the emission fields with NOx '10' and NH3 '20000'
        And I save the data

        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Industrie' and sector 'Afvalverwerking' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '20,000' and diurnal variation type 'Continue Emissie' is correctly saved
        And the total 'generic' emission 'NOx' is '10,0 kg/j'
        And the total 'generic' emission 'NH3' is '20,0 ton/j'

        Then I select the source 'Bron 2'
        Then the source 'Bron 2' with sector group 'Industrie' and sector 'Afvalverwerking' is correctly saved
        And location with coordinates 'X:172303,2 Y:599120,32' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '6,0', heat content '0,100' and diurnal variation type 'Continue Emissie' is correctly saved
        And the total 'generic' emission 'NOx' is '10,0 kg/j'
        And the total 'generic' emission 'NH3' is '20,0 ton/j'

        Then I select the source 'Bron 3'
        Then the source 'Bron 3' with sector group 'Industrie' and sector 'Voedings- en genotmiddelen' is correctly saved
        And location with coordinates 'X:172403,2 Y:599120,32' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '0,340' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '10,0 kg/j'
        And the total 'generic' emission 'NH3' is '20,0 ton/j'

        Then I select the source 'Bron 4'
        Then the source 'Bron 4' with sector group 'Industrie' and sector 'Chemische industrie' is correctly saved
        And location with coordinates 'X:172503,2 Y:599120,32' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '12,0', heat content '0,130' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '10,0 kg/j'
        And the total 'generic' emission 'NH3' is '20,0 ton/j'

        Then I select the source 'Bron 5'
        Then the source 'Bron 5' with sector group 'Industrie' and sector 'Bouwmaterialen' is correctly saved
        And location with coordinates 'X:172603,2 Y:599120,32' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '17,0', heat content '0,440' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '10,0 kg/j'
        And the total 'generic' emission 'NH3' is '20,0 ton/j'

        Then I select the source 'Bron 6'
        Then the source 'Bron 6' with sector group 'Industrie' and sector 'Basismetaal' is correctly saved
        And location with coordinates 'X:172703,2 Y:599120,32' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '13,0', heat content '0,050' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '10,0 kg/j'
        And the total 'generic' emission 'NH3' is '20,0 ton/j'

        Then I select the source 'Bron 7'
        Then the source 'Bron 7' with sector group 'Industrie' and sector 'Metaalbewerkingsindustrie' is correctly saved
        And location with coordinates 'X:172803,2 Y:599120,32' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '10,0', heat content '0,000' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '10,0 kg/j'
        And the total 'generic' emission 'NH3' is '20,0 ton/j'

        Then I select the source 'Bron 8'
        Then the source 'Bron 8' with sector group 'Industrie' and sector 'Overig' is correctly saved
        And location with coordinates 'X:172903,2 Y:599120,32' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '22,0', heat content '0,280' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '10,0 kg/j'
        And the total 'generic' emission 'NH3' is '20,0 ton/j'

        Then I navigate to the 'Calculation jobs' tab
        Then I validate the calculation job message 'Er zijn nog geen rekentaken. Maak een nieuwe rekentaak aan door op onderstaande knop te drukken.'
        And I create a new calculation job
        And I save the calculation job
        Then I start calculation and validate the request
        |situationNr |type       |featureNr |label                   |heatContentType |emissionHeight |heatContent |diurnalVariation             |coordinateType |coordinates          |
        |0           |Beoogd     |0         |Bron 1                  |Niet geforceerd |15             |20          |Continue Emissie             |Point          |[172203.2,599120.32] |
        |0           |Beoogd     |1         |Bron 2                  |Niet geforceerd |6              |0.1         |Continue Emissie             |Point          |[172303.2,599120.32] |
        |0           |Beoogd     |2         |Bron 3                  |Niet geforceerd |15             |0.34        |Standaard Profiel Industrie  |Point          |[172403.2,599120.32] |
        |0           |Beoogd     |3         |Bron 4                  |Niet geforceerd |12             |0.13        |Standaard Profiel Industrie  |Point          |[172503.2,599120.32] |
        |0           |Beoogd     |4         |Bron 5                  |Niet geforceerd |17             |0.44        |Standaard Profiel Industrie  |Point          |[172603.2,599120.32] |
        |0           |Beoogd     |5         |Bron 6                  |Niet geforceerd |13             |0.05        |Standaard Profiel Industrie  |Point          |[172703.2,599120.32] |
        |0           |Beoogd     |6         |Bron 7                  |Niet geforceerd |10             |0.0         |Standaard Profiel Industrie  |Point          |[172803.2,599120.32] |
        |0           |Beoogd     |7         |Bron 8                  |Niet geforceerd |22             |0.28        |Standaard Profiel Industrie  |Point          |[172903.2,599120.32] |
        Then I cancel the calculation

    @Smoke
    Scenario: Create new House and Office source starting at the start page
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Wonen en Werken' and sector 'Recreatie'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose heat content type 'Niet geforceerd', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '10' and NH3 '20000'
        And I save the data
        And I create a new source from the source page
        And I name the source 'Bron 2 recreatie default' and select sector group 'Wonen en Werken' and sector 'Recreatie'
        And I fill location with coordinates 'POINT(172303.2 599120.32)'
        And I fill the emission fields with NOx '10' and NH3 '20000'
        And I save the data
        And I create a new source from the source page
        And I name the source 'Bron 3 kantoren en winkels default' and select sector group 'Wonen en Werken' and sector 'Kantoren en winkels'
        And I fill location with coordinates 'POINT(172403.2 599120.32)'
        And I fill the emission fields with NOx '10' and NH3 '20000'
        And I save the data

        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Wonen en Werken' and sector 'Recreatie' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '20,000' and diurnal variation type 'Continue Emissie' is correctly saved
        And the total 'generic' emission 'NOx' is '10,0 kg/j'
        And the total 'generic' emission 'NH3' is '20,0 ton/j'

        Then I select the source 'Bron 2 recreatie default'
        Then the source 'Bron 2 recreatie default' with sector group 'Wonen en Werken' and sector 'Recreatie' is correctly saved
        And location with coordinates 'X:172303,2 Y:599120,32' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '1,0', heat content '0,002' and diurnal variation type 'Continue Emissie' is correctly saved
        And the total 'generic' emission 'NOx' is '10,0 kg/j'
        And the total 'generic' emission 'NH3' is '20,0 ton/j'

        Then I select the source 'Bron 3 kantoren en winkels default'
        Then the source 'Bron 3 kantoren en winkels default' with sector group 'Wonen en Werken' and sector 'Kantoren en winkels' is correctly saved
        And location with coordinates 'X:172403,2 Y:599120,32' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '11,0', heat content '0,014' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '10,0 kg/j'
        And the total 'generic' emission 'NH3' is '20,0 ton/j'

        Then I navigate to the 'Calculation jobs' tab
        Then I validate the calculation job message 'Er zijn nog geen rekentaken. Maak een nieuwe rekentaak aan door op onderstaande knop te drukken.'
        And I create a new calculation job
        And I save the calculation job
        Then I start calculation and validate the request
        |situationNr |type       |featureNr |label                              |heatContentType |emissionHeight |heatContent |diurnalVariation             |coordinateType |coordinates          |
        |0           |Beoogd     |0         |Bron 1                             |Niet geforceerd |15             |20          |Continue Emissie             |Point          |[172203.2,599120.32] |
        |0           |Beoogd     |1         |Bron 2 recreatie default           |Niet geforceerd |1              |0.002       |Continue Emissie             |Point          |[172303.2,599120.32] |
        |0           |Beoogd     |2         |Bron 3 kantoren en winkels default |Niet geforceerd |11             |0.014       |Standaard Profiel Industrie  |Point          |[172403.2,599120.32] |
        Then I cancel the calculation

    Scenario: Create new Rail transport source starting at the start page
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Railverkeer' and sector 'Spoorweg'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose heat content type 'Niet geforceerd', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '10' and NH3 '20000'
        And I save the data

        And I create a new source from the source page
        And I name the source 'Bron 2 spoorweg default' and select sector group 'Railverkeer' and sector 'Spoorweg'
        And I fill location with coordinates 'POINT(172303.2 599120.32)'
        And I fill the emission fields with NOx '10' and NH3 '20000'
        And I save the data

        And I create a new source from the source page
        And I name the source 'Bron 3 emplacement default' and select sector group 'Railverkeer' and sector 'Emplacement'
        And I fill location with coordinates 'POINT(172403.2 599120.32)'
        And I fill the emission fields with NOx '10' and NH3 '20000'
        And I save the data

        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Railverkeer' and sector 'Spoorweg' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '20,000' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '10,0 kg/j'
        And the total 'generic' emission 'NH3' is '20,0 ton/j'

        Then I select the source 'Bron 2 spoorweg default'
        Then the source 'Bron 2 spoorweg default' with sector group 'Railverkeer' and sector 'Spoorweg' is correctly saved
        And location with coordinates 'X:172303,2 Y:599120,32' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '5,0', heat content '0,000' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '10,0 kg/j'
        And the total 'generic' emission 'NH3' is '20,0 ton/j'

        Then I select the source 'Bron 3 emplacement default'
        Then the source 'Bron 3 emplacement default' with sector group 'Railverkeer' and sector 'Emplacement' is correctly saved
        And location with coordinates 'X:172403,2 Y:599120,32' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '3,5', heat content '0,200' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '10,0 kg/j'
        And the total 'generic' emission 'NH3' is '20,0 ton/j'

        Then I navigate to the 'Calculation jobs' tab
        Then I validate the calculation job message 'Er zijn nog geen rekentaken. Maak een nieuwe rekentaak aan door op onderstaande knop te drukken.'
        And I create a new calculation job
        And I save the calculation job
        Then I start calculation and validate the request
        |situationNr |type       |featureNr |label                      |heatContentType |emissionHeight |heatContent |diurnalVariation             |coordinateType |coordinates          |
        |0           |Beoogd     |0         |Bron 1                     |Niet geforceerd |15             |20          |Standaard Profiel Industrie  |Point          |[172203.2,599120.32] |
        |0           |Beoogd     |1         |Bron 2 spoorweg default    |Niet geforceerd |5              |0           |Standaard Profiel Industrie  |Point          |[172303.2,599120.32] |
        |0           |Beoogd     |2         |Bron 3 emplacement default |Niet geforceerd |3.5            |0.2         |Standaard Profiel Industrie  |Point          |[172403.2,599120.32] |
        Then I cancel the calculation

    Scenario: Create new Aviation source starting at the start page
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Luchtverkeer' and sector 'Landen'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose heat content type 'Niet geforceerd', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '10' and NH3 '20000'
        And I save the data

        And I create a new source from the source page
        And I name the source 'Bron 2' and select sector group 'Luchtverkeer' and sector 'Stijgen'
        And I fill location with coordinates 'POINT(172303.2 599120.32)'
        And I fill the emission fields with NOx '10' and NH3 '20000'
        And I save the data

        And I create a new source from the source page
        And I name the source 'Bron 3' and select sector group 'Luchtverkeer' and sector 'Landen'
        And I fill location with coordinates 'POINT(172403.2 599120.32)'
        And I fill the emission fields with NOx '10' and NH3 '20000'
        And I save the data

        And I create a new source from the source page
        And I name the source 'Bron 4' and select sector group 'Luchtverkeer' and sector 'Taxiën'
        And I fill location with coordinates 'POINT(172503.2 599120.32)'
        And I fill the emission fields with NOx '10' and NH3 '20000'
        And I save the data

        And I create a new source from the source page
        And I name the source 'Bron 5' and select sector group 'Luchtverkeer' and sector 'Bronnen luchthaventerrein'
        And I fill location with coordinates 'POINT(172603.2 599120.32)'
        And I fill the emission fields with NOx '10' and NH3 '20000'
        And I save the data

        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Luchtverkeer' and sector 'Landen' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '20,000' and diurnal variation type 'Continue Emissie' is correctly saved
        And the total 'generic' emission 'NOx' is '10,0 kg/j'
        And the total 'generic' emission 'NH3' is '20,0 ton/j'

        Then I select the source 'Bron 2'
        Then the source 'Bron 2' with sector group 'Luchtverkeer' and sector 'Stijgen' is correctly saved
        And location with coordinates 'X:172303,2 Y:599120,32' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '450,0', heat content '0,000' and diurnal variation type 'Continue Emissie' is correctly saved
        And the total 'generic' emission 'NOx' is '10,0 kg/j'
        And the total 'generic' emission 'NH3' is '20,0 ton/j'

        Then I select the source 'Bron 3'
        Then the source 'Bron 3' with sector group 'Luchtverkeer' and sector 'Landen' is correctly saved
        And location with coordinates 'X:172403,2 Y:599120,32' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '450,0', heat content '0,000' and diurnal variation type 'Continue Emissie' is correctly saved
        And the total 'generic' emission 'NOx' is '10,0 kg/j'
        And the total 'generic' emission 'NH3' is '20,0 ton/j'

        Then I select the source 'Bron 4'
        Then the source 'Bron 4' with sector group 'Luchtverkeer' and sector 'Taxiën' is correctly saved
        And location with coordinates 'X:172503,2 Y:599120,32' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '6,0', heat content '0,040' and diurnal variation type 'Continue Emissie' is correctly saved
        And the total 'generic' emission 'NOx' is '10,0 kg/j'
        And the total 'generic' emission 'NH3' is '20,0 ton/j'

        Then I select the source 'Bron 5'
        Then the source 'Bron 5' with sector group 'Luchtverkeer' and sector 'Bronnen luchthaventerrein' is correctly saved
        And location with coordinates 'X:172603,2 Y:599120,32' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '6,0', heat content '0,040' and diurnal variation type 'Continue Emissie' is correctly saved
        And the total 'generic' emission 'NOx' is '10,0 kg/j'
        And the total 'generic' emission 'NH3' is '20,0 ton/j'

        Then I navigate to the 'Calculation jobs' tab
        Then I validate the calculation job message 'Er zijn nog geen rekentaken. Maak een nieuwe rekentaak aan door op onderstaande knop te drukken.'
        And I create a new calculation job
        And I save the calculation job
        Then I start calculation and validate the request
        |situationNr |type       |featureNr |label                   |heatContentType |emissionHeight |heatContent |diurnalVariation             |coordinateType |coordinates          |
        |0           |Beoogd     |0         |Bron 1                  |Niet geforceerd |15             |20          |Continue Emissie             |Point          |[172203.2,599120.32] |
        |0           |Beoogd     |1         |Bron 2                  |Niet geforceerd |450            |0           |Continue Emissie             |Point          |[172303.2,599120.32] |
        |0           |Beoogd     |2         |Bron 3                  |Niet geforceerd |450            |0           |Continue Emissie             |Point          |[172403.2,599120.32] |
        |0           |Beoogd     |3         |Bron 4                  |Niet geforceerd |6              |0.040       |Continue Emissie             |Point          |[172503.2,599120.32] |
        |0           |Beoogd     |4         |Bron 5                  |Niet geforceerd |6              |0.040       |Continue Emissie             |Point          |[172603.2,599120.32] |
        Then I cancel the calculation
