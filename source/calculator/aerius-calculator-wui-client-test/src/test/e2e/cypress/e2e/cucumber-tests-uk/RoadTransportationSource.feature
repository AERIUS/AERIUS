#
# Crown copyright
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Road transportation source

    Scenario: Create new road transport source - EFT - England (not London), Urban (not London)
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Traffic'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        Then I open panel "Source characteristics"
        And I fill road width with value '2'
        And I open panel 'Traffic direction, speed, traffic and emission'
        And I choose area 'England (not London)'
        And I choose road type 'Urban (not London)' direction 'Two way'
        And I add an emission source of type 'Emission factor toolkit'
        And I fill in maximum speed '140'
        And I select time unit '/24 hours'
        And I fill in number '100.1' for vehicle 'Car'
        And I fill in number '100.2' for vehicle 'Taxi (black cab)'
        And I fill in number '100.3' for vehicle 'Motorcycle'
        And I fill in number '100.4' for vehicle 'LGV'
        And I fill in number '100.5' for vehicle 'HGV'
        And I fill in number '100.6' for vehicle 'Bus and Coach'
        When I save the data
        And I select the source 'Road transport network'
        And I select the source 'Source 1'
        Then the source 'Source 1' with sector group 'Traffic' is correctly saved
        And location with coordinates 'X:146296.7 Y:592777.17' is correctly saved
        And length of location with value '26,115.45 m' is correctly saved
        And area with 'England (not London)' is correctly saved
        And road type 'Urban (not London)' and traffic direction 'Two way' are correctly saved
        And road width '2 m', elevation '0 m' and gradient '0 %' are correctly saved
        And barrier type left 'None' and right 'None' are correctly saved
        And the title for 'standard' road subsource '0' is 'Emission factor toolkit'
        And maximum speed '140 km/h' is correctly saved
        And number of vehicles '100.1' for vehicletype 'Car' is correctly saved
        And number of vehicles '100.2' for vehicletype 'Taxi (black cab)' is correctly saved
        And number of vehicles '100.3' for vehicletype 'Motorcycle' is correctly saved
        And number of vehicles '100.4' for vehicletype 'LGV' is correctly saved
        And number of vehicles '100.5' for vehicletype 'HGV' is correctly saved
        And number of vehicles '100.6' for vehicletype 'Bus and Coach' is correctly saved

    Scenario: Create new road transport source - EFT - Northern Ireland, Motorway (not London)
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Traffic'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I open panel 'Source characteristics'
        And I fill road width with value '2', road elevation with '1' and gradient with '-50'
        And I open panel 'Traffic direction, speed, traffic and emission'
        And I choose area 'Northern Ireland'
        And I choose road type 'Motorway (not London)' direction 'From A to B'
        And I add an emission source of type 'Emission factor toolkit'
        And I fill in maximum speed '120'
        And I select time unit '/hour'
        And I fill in number '100' for vehicle 'Car'
        When I save the data
        And I select the source 'Road transport network'
        And I select the source 'Source 1'
        Then the source 'Source 1' with sector group 'Traffic' is correctly saved
        And location with coordinates 'X:146296.7 Y:592777.17' is correctly saved
        And length of location with value '26,115.45 m' is correctly saved
        And area with 'Northern Ireland' is correctly saved
        And road type 'Motorway (not London)' and traffic direction 'From A to B' are correctly saved
        And road width '2 m', elevation '1 m' and gradient '-50 %' are correctly saved
        And barrier type left 'None' and right 'None' are correctly saved
        And the title for 'standard' road subsource '0' is 'Emission factor toolkit'
        And maximum speed '120 km/h' is correctly saved
        And number of vehicles '100.0' for vehicletype 'Car' is correctly saved
        And number of vehicles '0.0' for vehicletype 'Taxi (black cab)' is correctly saved
        And number of vehicles '0.0' for vehicletype 'Motorcycle' is correctly saved
        And number of vehicles '0.0' for vehicletype 'LGV' is correctly saved
        And number of vehicles '0.0' for vehicletype 'HGV' is correctly saved
        And number of vehicles '0.0' for vehicletype 'Bus and Coach' is correctly saved

    Scenario: Create new road transport source - EFT - Wales, Rural (not London)
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Traffic'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I open panel 'Source characteristics'
        And I fill road width with value '100', road elevation with '15000' and gradient with '50'
        And I open panel 'Traffic direction, speed, traffic and emission'
        And I choose area 'Wales'
        And I choose road type 'Rural (not London)' direction 'From B to A'
        And I add an emission source of type 'Emission factor toolkit'
        And I fill in maximum speed '120'
        And I select time unit '/month'
        And I fill in number '100' for vehicle 'Car'
        And I fill in number '200' for vehicle 'Taxi (black cab)'
        And I fill in number '300' for vehicle 'Motorcycle'
        And I fill in number '400' for vehicle 'LGV'
        And I fill in number '500' for vehicle 'HGV'
        And I fill in number '600' for vehicle 'Bus and Coach'
        When I save the data
        And I select the source 'Road transport network'
        And I select the source 'Source 1'
        Then the source 'Source 1' with sector group 'Traffic' is correctly saved
        And location with coordinates 'X:146296.7 Y:592777.17' is correctly saved
        And length of location with value '26,115.45 m' is correctly saved
        And area with 'Wales' is correctly saved
        And road type 'Rural (not London)' and traffic direction 'From B to A' are correctly saved
        And road width '100 m', elevation '15,000 m' and gradient '50 %' are correctly saved
        And barrier type left 'None' and right 'None' are correctly saved
        And the title for 'standard' road subsource '0' is 'Emission factor toolkit'
        And maximum speed '120 km/h' is correctly saved
        And number of vehicles '100.0' for vehicletype 'Car' is correctly saved
        And number of vehicles '200.0' for vehicletype 'Taxi (black cab)' is correctly saved
        And number of vehicles '300.0' for vehicletype 'Motorcycle' is correctly saved
        And number of vehicles '400.0' for vehicletype 'LGV' is correctly saved
        And number of vehicles '500.0' for vehicletype 'HGV' is correctly saved
        And number of vehicles '600.0' for vehicletype 'Bus and Coach' is correctly saved

    Scenario: Create new road transport source - Custom specification - time unit - per 24 hours
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Traffic'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I open panel 'Source characteristics'
        And I fill road width with value '100', road elevation with '15000' and gradient with '50'
        And I open panel 'Traffic direction, speed, traffic and emission'
        And I choose area 'Wales'
        And I choose road type 'Rural (not London)' direction 'From B to A'
        And I add an emission source of type 'Custom specification'
        And I fill description with 'Description 123X'
        And I choose time unit '/24 hours' with number of vehicles '9999'
        And I fill emission 'NOx' with value '12345'
        And I fill emission 'NH3' with value '23456'
        When I save the data
        And I select the source 'Road transport network'
        And I select the source 'Source 1'
        Then the title for 'custom' road subsource '0' is 'Custom specification'
        And description 'Description 123X' is correctly saved
        And time unit '/24 hours' with number of vehicles '9,999' is correctly saved
        And emission 'NOx' with value '12,345.0 g/km/s' is correctly saved
        And emission 'NH3' with value '23,456.0 g/km/s' is correctly saved

    Scenario: Create new road transport source - Custom specification - time unit - per hour
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Traffic'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I open panel 'Source characteristics'
        And I fill road width with value '100', road elevation with '15000' and gradient with '50'
        And I open panel 'Traffic direction, speed, traffic and emission'
        And I choose area 'Wales'
        And I choose road type 'Rural (not London)' direction 'From B to A'
        And I add an emission source of type 'Custom specification'
        And I fill description with 'Description 123X'
        And I choose time unit '/hour' with number of vehicles '0'
        And I fill emission 'NOx' with value '12345'
        And I fill emission 'NH3' with value '23456'
        When I save the data
        And I select the source 'Road transport network'
        And I select the source 'Source 1'
        Then the title for 'custom' road subsource '0' is 'Custom specification'
        And description 'Description 123X' is correctly saved
        And time unit '/hour' with number of vehicles '0' is correctly saved
        And emission 'NOx' with value '12,345.0 g/km/s' is correctly saved
        And emission 'NH3' with value '23,456.0 g/km/s' is correctly saved

    Scenario: Create new road transport source - Custom specification - time unit - per month
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Traffic'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I open panel 'Source characteristics'
        And I fill road width with value '100', road elevation with '15000' and gradient with '50'
        And I open panel 'Traffic direction, speed, traffic and emission'
        And I choose area 'Wales'
        And I choose road type 'Rural (not London)' direction 'From B to A'
        And I add an emission source of type 'Custom specification'
        And I fill description with 'Description 123X'
        And I choose time unit '/month' with number of vehicles '9999'
        And I fill emission 'NOx' with value '12345'
        And I fill emission 'NH3' with value '23456'
        When I save the data
        And I select the source 'Road transport network'
        And I select the source 'Source 1'
        Then the title for 'custom' road subsource '0' is 'Custom specification'
        And description 'Description 123X' is correctly saved
        And time unit '/month' with number of vehicles '9,999' is correctly saved
        And emission 'NOx' with value '12,345.0 g/km/s' is correctly saved
        And emission 'NH3' with value '23,456.0 g/km/s' is correctly saved

    Scenario: Create new road transport source - Custom specification - time unit - per year
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Traffic'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I open panel 'Source characteristics'
        And I fill road width with value '100', road elevation with '15000' and gradient with '50'
        And I open panel 'Traffic direction, speed, traffic and emission'
        And I choose area 'Wales'
        And I choose road type 'Rural (not London)' direction 'From B to A'
        And I add an emission source of type 'Custom specification'
        And I fill description with 'Description 123X'
        And I choose time unit '/year' with number of vehicles '9999'
        And I fill emission 'NOx' with value '12345'
        And I fill emission 'NH3' with value '23456'
        When I save the data
        And I select the source 'Road transport network'
        And I select the source 'Source 1'
        Then the title for 'custom' road subsource '0' is 'Custom specification'
        And description 'Description 123X' is correctly saved
        And time unit '/year' with number of vehicles '9,999' is correctly saved
        And emission 'NOx' with value '12,345.0 g/km/s' is correctly saved
        And emission 'NH3' with value '23,456.0 g/km/s' is correctly saved

    Scenario: Edit road transport source for custom specification
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Traffic'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        Then I open panel "Source characteristics"
        And I fill road width with value '2'
        And I open panel 'Traffic direction, speed, traffic and emission'
        And I choose area 'Scotland'
        And I choose road type 'Urban (not London)' direction 'From A to B'
        And I add an emission source of type 'Custom specification'
        And I fill description with 'Description 123X'
        And I choose time unit '/hour' with number of vehicles '9999'
        And I fill emission 'NOx' with value '12345'
        And I fill emission 'NH3' with value '23456'
        And I save the data
        When I select the source 'Road transport network'
        And I select the source 'Source 1'
        And I choose to edit the source
        And I open panel 'Traffic direction, speed, traffic and emission'
        And I choose area 'England (not London)'
        And I choose road type 'Motorway (not London)' direction 'Two way'
        And I save the data
        And area with 'England (not London)' is correctly saved
        And road type 'Motorway (not London)' and traffic direction 'Two way' are correctly saved

    Scenario: Edit road transport source for emission factor toolkit and validate subsource roadspeed AER-1488
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Traffic'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        Then I open panel "Source characteristics"
        And I fill road width with value '100', road elevation with '15000' and gradient with '50'
        And I open panel 'Traffic direction, speed, traffic and emission'
        And I choose area 'Scotland'
        And I choose road type 'Urban (not London)' direction 'From A to B'
        And I add an emission source of type 'Emission factor toolkit'
        And I fill in maximum speed '120'
        And I select time unit '/month'
        And I fill in number '100' for vehicle 'Car'
        And I fill in number '200' for vehicle 'Taxi (black cab)'
        And I fill in number '300' for vehicle 'Motorcycle'
        And I fill in number '400' for vehicle 'LGV'
        And I fill in number '500' for vehicle 'HGV'
        And I fill in number '600' for vehicle 'Bus and Coach'
        When I save the data
        And I select the source 'Road transport network'
        And I select the source 'Source 1'
        Then the source 'Source 1' with sector group 'Traffic' is correctly saved
        And location with coordinates 'X:146296.7 Y:592777.17' is correctly saved
        And length of location with value '26,115.45 m' is correctly saved
        And area with 'Scotland' is correctly saved
        And road type 'Urban (not London)' and traffic direction 'From A to B' are correctly saved
        And road width '100 m', elevation '15,000 m' and gradient '50 %' are correctly saved
        And barrier type left 'None' and right 'None' are correctly saved
        And the title for 'standard' road subsource '0' is 'Emission factor toolkit'
        And maximum speed '120 km/h' is correctly saved
        And number of vehicles '100.0' for vehicletype 'Car' is correctly saved
        And number of vehicles '200.0' for vehicletype 'Taxi (black cab)' is correctly saved
        And number of vehicles '300.0' for vehicletype 'Motorcycle' is correctly saved
        And number of vehicles '400.0' for vehicletype 'LGV' is correctly saved
        And number of vehicles '500.0' for vehicletype 'HGV' is correctly saved
        And number of vehicles '600.0' for vehicletype 'Bus and Coach' is correctly saved
        And I choose to edit the source
        And I open panel 'Traffic direction, speed, traffic and emission'
        And I choose area 'England (not London)'
        And I choose road type 'Motorway (not London)' direction 'Two way'
        And I save the data
        Then the source 'Source 1' with sector group 'Traffic' is correctly saved
        And location with coordinates 'X:146296.7 Y:592777.17' is correctly saved
        And length of location with value '26,115.45 m' is correctly saved
        And area with 'England (not London)' is correctly saved
        And road type 'Motorway (not London)' and traffic direction 'Two way' are correctly saved
        And road width '100 m', elevation '15,000 m' and gradient '50 %' are correctly saved
        And barrier type left 'None' and right 'None' are correctly saved
        And the title for 'standard' road subsource '0' is 'Emission factor toolkit'
        And maximum speed '120 km/h' is correctly saved
        And number of vehicles '100.0' for vehicletype 'Car' is correctly saved
        And number of vehicles '200.0' for vehicletype 'Taxi (black cab)' is correctly saved
        And number of vehicles '300.0' for vehicletype 'Motorcycle' is correctly saved
        And number of vehicles '400.0' for vehicletype 'LGV' is correctly saved
        And number of vehicles '500.0' for vehicletype 'HGV' is correctly saved
        And number of vehicles '600.0' for vehicletype 'Bus and Coach' is correctly saved

    Scenario: Copy road transport source
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Traffic'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        Then I open panel "Source characteristics"
        And I fill road width with value '2'
        And I open panel 'Traffic direction, speed, traffic and emission'
        And I choose area 'London'
        And I choose road type 'London - Inner' direction 'From A to B'
        And I add an emission source of type 'Custom specification'
        And I fill description with 'Description 123X'
        And I choose time unit '/hour' with number of vehicles '9999'
        And I fill emission 'NOx' with value '12345'
        And I fill emission 'NH3' with value '23456'
        And I save the data
        When I select the source 'Road transport network'
        And I select the source 'Source 1'
        And I copy the source 'Source 1'
        Then the source 'Source 1 (1)' with sector group 'Traffic' is correctly saved
        And location with coordinates 'X:146296.7 Y:592777.17' is correctly saved
        And length of location with value '26,115.45 m' is correctly saved
        And area with 'London' is correctly saved
        And road type 'London - Inner' and traffic direction 'From A to B' are correctly saved
        And road width '2 m', elevation '0 m' and gradient '0 %' are correctly saved
        And barrier type left 'None' and right 'None' are correctly saved
        Then the title for 'custom' road subsource '0' is 'Custom specification'
        And description 'Description 123X' is correctly saved
        And time unit '/hour' with number of vehicles '9,999' is correctly saved
        And emission 'NOx' with value '12,345.0 g/km/s' is correctly saved
        And emission 'NH3' with value '23,456.0 g/km/s' is correctly saved

    Scenario: Delete road transport source
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Traffic'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        Then I open panel "Source characteristics"
        And I fill road width with value '2'
        And I open panel 'Traffic direction, speed, traffic and emission'
        And I choose area 'London'
        And I choose road type 'London - Inner' direction 'From A to B'
        And I add an emission source of type 'Custom specification'
        And I fill description with 'Description 123X'
        And I choose time unit '/hour' with number of vehicles '9999'
        And I fill emission 'NOx' with value '12345'
        And I fill emission 'NH3' with value '23456'
        And I save the data
        And I select the source 'Road transport network'
        And I select the source 'Source 1'
        When I delete source 'Source 1'
        Then the Emission Sources is empty

    # TODO AER-826 Scenario: Road transport source with Tunnel
    #     Given I login with correct username and password
    #     And I create a new source from the start page
    #     And I name the source 'Source 1' and select sector group 'Traffic'
    #     And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
    #     And I open panel 'Source characteristics'
    #     And I check checkmark 'Tunnel'
    #     Then the next field does not exist 'Road barrier'
    #     Then the next field does not exist 'Barrier type'
    #     And I open panel 'Traffic direction, speed, traffic and emission'
    #     And I choose area 'London'
    #     And I choose road type 'London - Inner' direction 'From A to B'
    #     And I add an emission source of type 'Custom specification'
    #     And I fill description with 'Description 123X'
    #     And I choose time unit '/hour' with number of vehicles '9999'
    #     And I fill emission 'NOx' with value '12345'
    #     And I fill emission 'NH3' with value '23456'
    #     And I save the data
    #     And I select the source 'Road transport network'
    #     And I select the source 'Source 1'
    #     And tunnel '1' is correctly saved

    @Smoke
    Scenario: Road transport source with left and right barrier type
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Traffic'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I open panel 'Source characteristics'
        And I fill road width with value '2'
        And I select 'left' barrier type 'Noise barrier'
        And I fill 'left' width with '100'
        And I fill 'left' maximum height with '200'
        And I fill 'left' average height with '500'
        And I fill 'left' minimum height with '400'
        And I fill 'left' porosity with '0'
        And I select 'right' barrier type 'Brick wall'
        And I fill 'right' width with '111'
        And I fill 'right' maximum height with '222'
        And I fill 'right' average height with '555'
        And I fill 'right' minimum height with '444'
        And I fill 'right' porosity with '99'
        And I fill coverage with '100'
        And I open panel 'Traffic direction, speed, traffic and emission'
        And I choose area 'London'
        And I choose road type 'London - Inner' direction 'From A to B'
        And I add an emission source of type 'Custom specification'
        And I fill description with 'Description 123X'
        And I choose time unit '/hour' with number of vehicles '9999'
        And I fill emission 'NOx' with value '12345'
        And I fill emission 'NH3' with value '23456'
        And I save the data
        And I select the source 'Road transport network'
        And I select the source 'Source 1'
        Then 'left' barrier type 'Noise barrier' is correctly saved
        And 'left' width '100 m' is correctly saved
        And 'left' maximum height '200 m' is correctly saved
        And 'left' average height '500 m' is correctly saved
        And 'left' minimum height '400 m' is correctly saved
        And 'left' porosity '0 %' is correctly saved
        Then 'right' barrier type 'Brick wall' is correctly saved
        And 'right' width '111 m' is correctly saved
        And 'right' maximum height '222 m' is correctly saved
        And 'right' average height '555 m' is correctly saved
        And 'right' minimum height '444 m' is correctly saved
        And 'right' porosity '99 %' is correctly saved
        And coverage '100 %' is correctly saved

    Scenario: Road transport source validations
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Traffic'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        # validations Source characteristics
        And I open panel 'Source characteristics'
        And I fill road width with value ' ', road elevation with ' ' and gradient with ' '
        Then the following 'road width' warning is visible 'Road width must have a value between 2 and 100 (inclusive) '
        And the following 'road elevation' warning is visible 'Elevation rel. to ground level must have a value between 0 and 15,000 (inclusive) '
        And the following 'road gradient' warning is visible 'Gradient must have a value between -50 and 50 (inclusive) '
        When I fill road width with value '2', road elevation with ' ' and gradient with ' '
        Then the 'road width' warning is not visible
        And the following 'road elevation' warning is visible 'Elevation rel. to ground level must have a value between 0 and 15,000 (inclusive) '
        And the following 'road gradient' warning is visible 'Gradient must have a value between -50 and 50 (inclusive) '
        When I fill road width with value '100', road elevation with '15,000' and gradient with ' '
        Then the 'road width' warning is not visible
        And the 'road elevation' warning is not visible
        And the following 'road gradient' warning is visible 'Gradient must have a value between -50 and 50 (inclusive) '
        When I fill road width with value '100', road elevation with '15,000' and gradient with '-50 '
        Then the 'road width' warning is not visible
        And the 'road elevation' warning is not visible
        And the 'road gradient' warning is not visible
        When I save the data
        # validations Traffic direction, speed and emission
        Then the following 'road area' warning is visible 'Area must be selected '
        When I choose area 'Wales'
        Then the 'road area' warning is not visible
        And the 'road type' warning is not visible
        And I choose road type 'Rural (not London)' direction 'From B to A'
        And I add an emission source of type 'Custom specification'
        And I fill description with 'Description 123X'
        And I choose time unit '/year' with number of vehicles '9999'
        When I save the data
        And I select the source 'Road transport network'
        And I select the source 'Source 1'
        Then area with 'Wales' is correctly saved
        And road type 'Rural (not London)' and traffic direction 'From B to A' are correctly saved
        And road width '100 m', elevation '15,000 m' and gradient '-50 %' are correctly saved
        And barrier type left 'None' and right 'None' are correctly saved
        Then the title for 'custom' road subsource '0' is 'Custom specification'
        And description 'Description 123X' is correctly saved
        And time unit '/year' with number of vehicles '9,999' is correctly saved
        And emission 'NOx' with value '0.0 g/km/s' is correctly saved
        And emission 'NH3' with value '0.0 g/km/s' is correctly saved

    Scenario: Road transport source with time-varying profile - Sector default
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Traffic'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I open panel 'Source characteristics'
        And I fill road width with value '2'
        And I choose area 'London'
        And I choose road type 'London - Inner' direction 'From A to B'
        And I add an emission source of type 'Custom specification'
        And I fill description with 'Description 123X'
        And I choose time unit '/hour' with number of vehicles '9999'
        And I fill emission 'NOx' with value '12345'
        And I fill emission 'NH3' with value '23456'
        And I open panel 'Time-varying profiles'
        And I select for 'diurnal' profile the option 'Sector default'
        And I select for 'monthly' profile the option 'Sector default'
        And I save the data
        And I select the source 'Road transport network'
        And I select the source 'Source 1'
        And the details of the profile are correctly saved
        | profileTitle       | profileName                   | profileType        |
        | Diurnal profile    | Sector default (UK road 2022) | Predefined profile |
        | Monthly profile    | Sector default (Continuous)   | Predefined profile |

    Scenario: Road transport source with time-varying profile - Continuous
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Traffic'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I open panel 'Source characteristics'
        And I fill road width with value '2'
        And I choose area 'London'
        And I choose road type 'London - Inner' direction 'From A to B'
        And I add an emission source of type 'Custom specification'
        And I fill description with 'Description 123X'
        And I choose time unit '/hour' with number of vehicles '9999'
        And I fill emission 'NOx' with value '12345'
        And I fill emission 'NH3' with value '23456'
        And I open panel 'Time-varying profiles'
        And I select for 'diurnal' profile the option 'Continuous'
        And I select for 'monthly' profile the option 'Continuous'        
        And I save the data
        And I select the source 'Road transport network'
        And I select the source 'Source 1'
        | profileTitle    | profileName   | profileType        |
        | Diurnal profile | Continuous    | Predefined profile |
        | Monthly profile | Continuous    | Predefined profile |

    Scenario: Road transport source with time-varying profile - Custom
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Traffic'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I open panel 'Source characteristics'
        And I fill road width with value '2'
        And I choose area 'London'
        And I choose road type 'London - Inner' direction 'From A to B'
        And I add an emission source of type 'Custom specification'
        And I fill description with 'Description 123X'
        And I choose time unit '/hour' with number of vehicles '9999'
        And I fill emission 'NOx' with value '12345'
        And I fill emission 'NH3' with value '23456'
        And I open panel 'Time-varying profiles'
        And I select for 'diurnal' profile the option 'Add new...'
        And I save the data
        And I select for 'monthly' profile the option 'Add new...'
        And I save the data
        And I save the data
        And I select the source 'Road transport network'
        And I select the source 'Source 1'
        And I select the source 'Source 1'
        | profileTitle    | profileName       | profileType     |
        | Diurnal profile | Diurnal profile 1 |                 |
        | Monthly profile | Monthly profile 1 |                 |

    # TODO AER-830 Scenario: Road transport source with time-varying profile - Custom predefined profile
    #     Given I login with correct username and password
    #     And I create a new source from the start page
    #     And I name the source 'Source 1' and select sector group 'Traffic'
    #     And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
    #     And I open panel 'Source characteristics'
    #     And I choose time-varying variation 'Custom'
    #     And I choose to add a new time-varying profile
    #     And I choose to cancel
    #     And I select predefined profile 'UK road 2020'
    #     And I open panel 'Traffic direction, speed, traffic and emission'
    #     And I choose area 'London'
    #     And I choose road type 'London - Inner' direction 'From A to B'
    #     And I add an emission source of type 'Custom specification'
    #     And I fill description with 'Description 123X'
    #     And I choose time unit '/hour' with number of vehicles '9999'
    #     And I fill emission 'NOx' with value '12345'
    #     And I fill emission 'NH3' with value '23456'
    #     And I save the data
    #     And I select the source 'Road transport network'
    #     And I select the source 'Source 1'
    #     Then time-varying profile with name 'UK road 2020' is correctly saved

    @Smoke
    Scenario: Apply a predefined time-varying profile to a road source
        Given I login with correct username and password
        # Update scenario data
        When I create an empty scenario from the start page
        And I name the scenario 'Time-varying test'
        And I select scenarioType 'Project'
        And I set the calculation year to '2020'
        And I save the scenario updates
        And I select view mode 'EMISSION_SOURCES'
        # Create source
        And I create a new source from the scenario input page
        And I name the source 'Road 1 - predefined time-varying profile' and select sector group 'Traffic'
        And I fill location with coordinates 'LINESTRING(368886.32 399387.96,369374.67 399748.22,370375.39 400452.72,371672.31 401045.14)'
        # Fill Traffic specific info
        Then I open the subsource dropdown
        And I choose area 'England (not London)'
        And I choose road type 'Urban (not London)' direction 'Two way'
        # Add source characteristics
        Then I open panel "Source characteristics"
        And I fill road width with value '2'
        # Make subsource
        When I open panel "Traffic direction, speed, traffic and emission"
        Then I add an emission source of type 'Custom specification'
        And I fill description with 'Custom emissions rate'
        And I choose time unit '/24 hours' with number of vehicles '30000'
        And I fill emission 'NOx' with value '1'
        And I fill emission 'NH3' with value '0.8'
        Then I save the data
        # Check detail screen
        And I select the source 'Road transport network'
        And I select the source 'Road 1 - predefined time-varying profile'
        Then the details of the profile are correctly saved
        | profileTitle    | profileName                   | profileType        |
        | Diurnal profile | Sector default (UK road 2020) | Predefined profile |
        | Monthly profile | Sector default (Continuous)   | Predefined profile |

    Scenario: Time-varying profile should follow selected year of project except for years after 2022
        Given I login with correct username and password
        When I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Traffic'
        And I fill location with coordinates 'LINESTRING(291815.52 621769.04,291584.68 621711.33)'
        And I open panel 'Source characteristics'
        And I fill road width with value '2'        
        And I open panel 'Traffic direction, speed, traffic and emission'
        And I choose area 'London'
        And I choose road type 'London - Inner' direction 'From A to B'
        And I add an emission source of type 'Custom specification'
        And I fill description with 'Description 123X'
        And I choose time unit '/hour' with number of vehicles '100'
        And I fill emission 'NOx' with value '10'
        And I fill emission 'NH3' with value '20'
        And I save the data

        # Selected year: 2018
        When I set the calculation year to '2018'
        And I save the scenario updates
        And I select the source 'Road transport network'
        When I select the source 'Source 1'
        Then time-varying profile with name 'Sector default (UK road 2018)' is correctly saved

        # Selected year: 2019
        When I set the calculation year to '2019'
        And I save the scenario updates
        When I select the source 'Source 1'
        Then time-varying profile with name 'Sector default (UK road 2019)' is correctly saved

        # Selected year: 2020
        When I set the calculation year to '2020'
        And I save the scenario updates
        When I select the source 'Source 1'
        Then time-varying profile with name 'Sector default (UK road 2020)' is correctly saved

        # Selected year: 2021
        When I set the calculation year to '2021'
        And I save the scenario updates
        When I select the source 'Source 1'
        Then time-varying profile with name 'Sector default (UK road 2021)' is correctly saved

        # Selected year: 2022
        When I set the calculation year to '2022'
        And I save the scenario updates
        When I select the source 'Source 1'
        Then time-varying profile with name 'Sector default (UK road 2022)' is correctly saved

        # Selected year: 2022+
        When I set the calculation year to '2023'
        And I save the scenario updates
        When I select the source 'Source 1'
        Then time-varying profile with name 'Sector default (UK road 2022)' is correctly saved

    Scenario: Validation minimum barrier width
        Given I login with correct username and password
        When I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Traffic'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I open panel 'Source characteristics'
        And I fill road width with value '100', road elevation with '0' and gradient with '0'
        And I select 'left' barrier type 'Brick wall'
        And I fill 'left' width with '49.99'
        Then the following 'barrier width left' warning is visible 'Barrier Left width (49.99) on each side of the canyon with a canyon wall should be greater than half the road carriageway width (100). '
        When I select 'right' barrier type 'Other'
        And I fill 'right' width with '49.99'
        Then the following 'barrier width right' warning is visible 'Barrier Right width (49.99) on each side of the canyon with a canyon wall should be greater than half the road carriageway width (100). '
        When I fill 'left' width with '50'
        Then the 'barrier width left' warning is not visible
        And I fill 'right' width with '50'
        Then the 'barrier width right' warning is not visible

    Scenario: Validation on input of required fields after selecting barrier type
        Given I login with correct username and password
        When I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Traffic'
        And I fill location with coordinates 'LINESTRING(284133.68 594020.1,285317.32 594566.4)'
        And I open panel 'Source characteristics'
        When I select 'left' barrier type 'Brick wall'
        And I select 'right' barrier type 'Semidetached properties'
        # Width
        When I fill 'left' width with '-1'
        And I fill 'right' width with '-1'
        Then the following 'Width left' error is visible 'Width left must have a value greater than or equal to 0'
        And the following 'Width right' error is visible 'Width right must have a value greater than or equal to 0'
        When I fill 'left' width with '0'
        And I fill 'right' width with '0'
        Then the following 'Width left' error 'Width left must have a value greater than or equal to 0' is not visible
        And the following 'Width right' error 'Width right must have a value greater than or equal to 0' is not visible
        # Maximum height
        When I fill 'left' maximum height with '0'
        And I fill 'right' maximum height with '0'
        Then the following 'Maximum height left' error is visible 'Maximum height left must have a value greater than 0'
        And the following 'Maximum height right' error is visible 'Maximum height right must have a value greater than 0'
        When I fill 'left' maximum height with '1'
        And I fill 'right' maximum height with '1'
        Then the following 'Maximum height left' error 'Maximum height left must have a value greater than 0' is not visible 
        And the following 'Maximum height right' error 'Maximum height right must have a value greater than 0' is not visible 
        # Average height
        When I fill 'left' average height with '0'
        And I fill 'right' average height with '0'
        Then the following 'Average height left' error is visible 'Average height left must have a value greater than 0'
        And the following 'Average height right' error is visible 'Average height right must have a value greater than 0'
         When I fill 'left' average height with '1'
        And I fill 'right' average height with '1'
        Then the following 'Average height left' error 'Average height left must have a value greater than 0' is not visible 
        And the following 'Average height right' error 'Average height right must have a value greater than 0' is not visible 
        # Minimum height
        When I fill 'left' minimum height with '-1'
        And I fill 'right' minimum height with '-1'
        Then the following 'Minimum height left' error is visible 'Minimum height left must have a value greater than or equal to 0'
        And the following 'Minimum height right' error is visible 'Minimum height right must have a value greater than or equal to 0'
        When I fill 'left' minimum height with '0'
        And I fill 'right' minimum height with '0'
        Then the following 'Minimum height left' error 'Minimum height left must have a value greater than or equal to 0' is not visible 
        And the following 'Minimum height right' error 'Minimum height right must have a value greater than or equal to 0' is not visible 
        # Porosity
        When I fill 'left' porosity with '-1'
        And I fill 'right' porosity with '101'
        Then the following 'Porosity left' error is visible 'Porosity left must have a value between 0 and 100 (inclusive)'
        And the following 'Porosity right' error is visible 'Porosity right must have a value between 0 and 100 (inclusive)'
        When I fill 'left' porosity with '0'
        And I fill 'right' porosity with '100'
        Then the following 'Porosity left' error 'Porosity left must have a value between 0 and 100 (inclusive)' is not visible
        And the following 'Porosity right' error 'Porosity right must have a value between 0 and 100 (inclusive)' is not visible
        # Coverage
        When I fill coverage with '-1'
        Then the following 'Coverage' error is visible 'Coverage must have a value between 0 and 100 (inclusive)'
        When I fill coverage with '101'
        Then the following 'Coverage' error is visible 'Coverage must have a value between 0 and 100 (inclusive)'
        When I fill coverage with '0'
        Then the following 'Coverage' error 'Coverage must have a value between 0 and 100 (inclusive)' is not visible
        When I fill coverage with '100'
        Then the following 'Coverage' error 'Coverage must have a value between 0 and 100 (inclusive)' is not visible
