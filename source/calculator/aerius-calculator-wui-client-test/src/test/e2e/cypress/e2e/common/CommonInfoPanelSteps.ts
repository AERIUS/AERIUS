/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";
import { CommonInfoPanelPage } from "./CommonInfoPanelPage";

Given("I validate receptor id {string} and location {string}", (receptorId: string, location: string) => {
  CommonInfoPanelPage.validateReceptorLocation(receptorId, location);
});

Given("I validate infomarker values", (dataTable: any) => {
  CommonInfoPanelPage.validateGenericLocationValues(dataTable);
});

Given("I validate background substance {string} value {string}", (substance: string, value: string) => {
  CommonInfoPanelPage.validateBackgroundValue(substance, value, 1);
});

Given("I validate background values", (dataTable: any) => {
  CommonInfoPanelPage.validateBackgroundValues(dataTable);
});

Given("I validate info marker results for calculation task {string}", (title: string, dataTable: any) => {
  CommonInfoPanelPage.validateResults(title, dataTable);
});
