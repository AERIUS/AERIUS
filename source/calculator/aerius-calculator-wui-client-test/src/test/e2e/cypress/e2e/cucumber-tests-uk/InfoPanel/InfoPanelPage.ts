/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class InfoPanelPage {
  static validateInfoPanelNatureAreas(dataTable: any) {
    dataTable.hashes().forEach((natureArea: any) => {
      cy.get('[aria-labelledby="' + natureArea.nature_area.toLowerCase().replaceAll(" ", "-") + '-title"]')
        .click({ force: true })
        .within(() => {
          cy.get('[id="infoPanelProtection"]')
            .within(() => {
              cy.get('[id="infoRowValue"]').contains(natureArea.designation);
            });
          cy.get('[id="infoPanelNatura2000AreaId"]')
            .within(() => {
              cy.get('[id="infoRowValue"]').contains(natureArea.siteNumber);
            });
          
          cy.get('[id="infoPanelSurface"]').should('not.exist');
          cy.get('[id="infoPanelContractor"]').should('not.exist');
          cy.get('[id="infoPanelStatus"]').should('not.exist');

          cy.get('.select-view-row').should("have.length", natureArea.habitatCount);
        });
    });
  }

  static openInfoPanelHabitats() {
    cy.get('[aria-controls="collapsibleHabitatType-content"]').click();
  }

  static validateInfoPanelHabitats(pollutant: string, dataTable: any) {
    if (dataTable == undefined) {
      cy.get('[id="collapsibleHabitatType-content"]')
        .within(() => {
          cy.get('.noResults').contains("No habitat types");
        });
    } else {
      cy.get('.criticalLevelKeySelectorRow > select').select(pollutant);
      dataTable.hashes().forEach((habitat: any) => {
        for(var propName in habitat) {
          let containText: string = habitat[propName];
          cy.get('[id="collapsibleHabitatType-content"]').contains(containText);
        }
      });
    }
  }
}
