#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Import files
    Test how importing files is handled.

    Scenario: Test enabled/disabled buttons
        Given I login with correct username and password
        Then I validate the status of the buttons on the import screen
        | id                                  | status   |
        | calculationLaunchButtonNewSituation | enabled  |
        | removeAll-import                    | disabled |
        | buttonOpenFileDialog                | enabled  |
        | buttonSaveimport                    | disabled |

    @Smoke
    Scenario: Test advanced importmodus generally
        # Start the application, upload a file and validate advanced importmodus generally (extra lines and default importOptions and scenarioOptions)
        Given I login with correct username and password
        And I select file 'AERIUS_gml_calculation_points.zip' to be imported, but I don't upload it, or open the collapsible panel yet
        Then the advanced import mode should be turned off
        When I enable the advanced importmodus
        Then the advanced import mode should be turned on
        And I expect the following options to be visible in the import list
        | situationName | importType        | importOptions        | situationType |
        | Situatie 1    | scenario          | Als nieuwe situatie  | Beoogd        |
        |               | calculationPoints | Voeg rekenpunten toe |               |

        # Upload another file and validate the usage options
        When I select file 'AERIUS_projectberekening_register.pdf' to be imported, but I don't upload it, or open the collapsible panel yet
        Then for the usage dropdown for the line of situation 'Situatie 1' of file 'AERIUS_gml_calculation_points.zip' I expect to see the option 'beoogd' under the header 'Samenvoegen met'
        And for the usage dropdown for the line of situation 'Situatie 1' of file 'AERIUS_gml_calculation_points.zip' I expect to see the option 'Negeer' under the header 'Overige'

        And for the usage dropdown for the line with no situation of file 'AERIUS_gml_calculation_points.zip' I expect to see the option 'Voeg rekenpunten toe' under the header 'Overige'
        And for the usage dropdown for the line with no situation of file 'AERIUS_gml_calculation_points.zip' I expect to see the option 'Negeer' under the header 'Overige'

        And for the usage dropdown for the line of situation 'beoogd' of file 'AERIUS_projectberekening_register.pdf' I expect to see the option 'Situatie 1' under the header 'Samenvoegen met'
        And for the usage dropdown for the line of situation 'beoogd' of file 'AERIUS_projectberekening_register.pdf' I expect to see the option 'Negeer' under the header 'Overige'

        # Import the files and validate there's a source
        When I try to import the file
        And I select scenarioType 'Tijdelijk'
        And I save the scenario updates
        And I open emission source list
        Then the emission source list has 1 sources

        # Go back to import screen, upload a file again and validate the import options now that there's an existing scenario
        When I click on the start page button
        And I select file 'AERIUS_projectberekening_register.pdf' to be imported, but I don't upload it, or open the collapsible panel yet
        Then for the usage dropdown for the line of situation 'beoogd' of file 'AERIUS_projectberekening_register.pdf' I expect to see the option 'Als nieuwe situatie'
        And for the usage dropdown for the line of situation 'beoogd' of file 'AERIUS_projectberekening_register.pdf' I expect to see the option 'beoogd' under the header 'Toevoegen aan bestaande situatie'
        And for the usage dropdown for the line of situation 'beoogd' of file 'AERIUS_projectberekening_register.pdf' I expect to see the option 'Negeer' under the header 'Overige'

        # Select merge option and validate expanded line changes
        When I select 'Situatie 1' as usage for scenario 'beoogd' of file 'AERIUS_projectberekening_register.pdf'
        And I expand the line with scenario 'beoogd' for file 'AERIUS_projectberekening_register.pdf'
        Then the situation type of the line with scenario 'beoogd' of import file 'AERIUS_projectberekening_register.pdf' should be 'Tijdelijk'
        And the detail info of the line with scenario 'beoogd' of import file 'AERIUS_projectberekening_register.pdf' should contain '1 emissiebron'
        And the detail info of the line with scenario 'beoogd' of import file 'AERIUS_projectberekening_register.pdf' should not contain '1 rekentaak (op basis van bestand)'

    Scenario: Test expandable lines - Standard
        # Start the application and upload 2 files
        Given I login with correct username and password
        When I select file 'AERIUS_import_gml_file.zip' to be imported, but I don't upload it, or open the collapsible panel yet
        And I select file 'emissionSourceFarAway.gml' to be imported, but I don't upload it, or open the collapsible panel yet

        # Assert files are uploaded and file lines are not expanded
        Then the file 'AERIUS_import_gml_file.zip' should be in the import list
        And the file 'emissionSourceFarAway.gml' should be in the import list
        And the line of the file 'AERIUS_import_gml_file.zip' should not be expanded
        And the line of the file 'emissionSourceFarAway.gml' should not be expanded

        # Expand 1 line and validate again
        When I expand the line for file 'AERIUS_import_gml_file.zip'
        Then the line of the file 'AERIUS_import_gml_file.zip' should be expanded
        Then the detail info of import file 'AERIUS_import_gml_file.zip' should contain '2 emissiebronnen'
        And the detail info of import file 'AERIUS_import_gml_file.zip' should contain '1 gebouw'
        And the line of the file 'emissionSourceFarAway.gml' should not be expanded

        # Expand second line and validate again
        When I expand the line for file 'emissionSourceFarAway.gml'
        Then the line of the file 'AERIUS_import_gml_file.zip' should be expanded
        And the line of the file 'emissionSourceFarAway.gml' should be expanded
        And the detail info of import file 'emissionSourceFarAway.gml' should contain '1 emissiebron'
        And the detail info of import file 'emissionSourceFarAway.gml' should not contain 'gebouw'

        # Close the second line and validate lines being open/closed and being in the import list
        When I close the line for file 'emissionSourceFarAway.gml'
        Then the file 'AERIUS_import_gml_file' should be in the import list
        And the file 'emissionSourceFarAway.gml' should be in the import list
        And the line of the file 'AERIUS_import_gml_file.zip' should be expanded
        And the line of the file 'emissionSourceFarAway.gml' should not be expanded

        # Delete file and validate
        When I remove the file 'emissionSourceFarAway.gml' from the import list
        Then the file 'emissionSourceFarAway.gml' should not be in the import list
        And the file 'AERIUS_import_gml_file.zip' should be in the import list

    Scenario: Test expandable lines - Advanced
        # Start the application with advanced importmodus on, upload files and check amount of import lines in between
        Given I login with correct username and password
        And I enable the advanced importmodus
        Then there should be 0 scenarios ready to imported
        When I select file 'AERIUS_gml_calculation_points.zip' to be imported, but I don't upload it, or open the collapsible panel yet
        Then there should be 2 scenarios ready to imported
        When I select file 'AERIUS_everything_import_file.gml' to be imported, but I don't upload it, or open the collapsible panel yet
        Then there should be 3 scenarios ready to imported
        When I select file 'AERIUS_projectberekening_register.pdf' to be imported, but I don't upload it, or open the collapsible panel yet
        Then there should be 4 scenarios ready to imported
        When I select file 'AERIUS_all_situation_types.zip' to be imported, but I don't upload it, or open the collapsible panel yet
        Then there should be 12 scenarios ready to imported

        # Expand lines and check contents
        When I expand the line with scenario 'Situatie 1' for file 'AERIUS_gml_calculation_points.zip'
        Then the header of the detail info of the line with scenario 'Situatie 1' of import file 'AERIUS_gml_calculation_points.zip' should be 'Situatie 1'
        And the detail info of the line with scenario 'Situatie 1' of import file 'AERIUS_gml_calculation_points.zip' should contain '1 emissiebron'
        And the detail info of the line with scenario 'Situatie 1' of import file 'AERIUS_gml_calculation_points.zip' should not contain '6 rekenpunten'

        When I expand line with no scenario for file 'AERIUS_gml_calculation_points.zip'
        Then the header of the detail info of the line with no scenario of import file 'AERIUS_gml_calculation_points.zip' should be 'Rekenpunten'
        And the detail info of the line with no scenario of import file 'AERIUS_gml_calculation_points.zip' should contain '6 rekenpunten'
        And the detail info of the line with no scenario of import file 'AERIUS_gml_calculation_points.zip' should contain 'Rekenpunten'
        And the detail info of the line with no scenario of import file 'AERIUS_gml_calculation_points.zip' should not contain '1 emissiebron'

        When I expand the line with scenario 'Situatie 1' for file 'AERIUS_everything_import_file.gml'
        Then the header of the detail info of the line with scenario 'Situatie 1' of import file 'AERIUS_everything_import_file.gml' should be 'Situatie 1'
        And the detail info of the line with scenario 'Situatie 1' of import file 'AERIUS_everything_import_file.gml' should contain '32 emissiebronnen'
        And the detail info of the line with scenario 'Situatie 1' of import file 'AERIUS_everything_import_file.gml' should contain '1 gebouw'

        When I expand the line with scenario 'beoogd' for file 'AERIUS_projectberekening_register.pdf'
        Then the header of the detail info of the line with scenario 'beoogd' of import file 'AERIUS_projectberekening_register.pdf' should be 'beoogd'
        And the detail info of the line with scenario 'beoogd' of import file 'AERIUS_projectberekening_register.pdf' should contain '1 emissiebron'
        And the detail info of the line with scenario 'beoogd' of import file 'AERIUS_projectberekening_register.pdf' should contain '1 rekentaak (op basis van bestand)'

        When I expand the line with scenario 'Situatie 4' for file 'AERIUS_all_situation_types.zip'
        Then the header of the detail info of the line with scenario 'Situatie 4' of import file 'AERIUS_all_situation_types.zip' should be 'Situatie 4'
        And the detail info of the line with scenario 'Situatie 4' of import file 'AERIUS_all_situation_types.zip' should contain '1 emissiebron'
        And the detail info of the line with scenario 'Situatie 4' of import file 'AERIUS_all_situation_types.zip' should contain '1 gebouw'

        # Select merge option and validate expanded line changes
        When I select 'Situatie 8' as usage for scenario 'beoogd' of file 'AERIUS_projectberekening_register.pdf'
        Then the header of the detail info of the line with scenario 'beoogd' of import file 'AERIUS_projectberekening_register.pdf' should be 'Situatie 8'
        And the detail info of the line with scenario 'beoogd' of import file 'AERIUS_projectberekening_register.pdf' should contain '1 emissiebron'
        And the detail info of the line with scenario 'beoogd' of import file 'AERIUS_projectberekening_register.pdf' should not contain '1 rekentaak (op basis van bestand)'

        # Reset usage and validate
        When I select 'Als nieuwe situatie' as usage for scenario 'beoogd' of file 'AERIUS_projectberekening_register.pdf'
        Then the header of the detail info of the line with scenario 'beoogd' of import file 'AERIUS_projectberekening_register.pdf' should be 'beoogd'
        And the detail info of the line with scenario 'beoogd' of import file 'AERIUS_projectberekening_register.pdf' should contain '1 emissiebron'
        And the detail info of the line with scenario 'beoogd' of import file 'AERIUS_projectberekening_register.pdf' should contain '1 rekentaak (op basis van bestand)'

        # Select situation type that's not the original and validate `rekentaak` is not imported and validate
        When I select 'Tijdelijk' as situation type for scenario 'beoogd' of file 'AERIUS_projectberekening_register.pdf'
        And the detail info of the line with scenario 'beoogd' of import file 'AERIUS_projectberekening_register.pdf' should contain '1 emissiebron'
        And the detail info of the line with scenario 'beoogd' of import file 'AERIUS_projectberekening_register.pdf' should not contain '1 rekentaak (op basis van bestand)'

        # Reset situation type and validate
        When I select 'Beoogd' as situation type for scenario 'beoogd' of file 'AERIUS_projectberekening_register.pdf'
        And the detail info of the line with scenario 'beoogd' of import file 'AERIUS_projectberekening_register.pdf' should contain '1 emissiebron'
        And the detail info of the line with scenario 'beoogd' of import file 'AERIUS_projectberekening_register.pdf' should contain '1 rekentaak (op basis van bestand)'

        # Test closing a line and validating
        When I close the line with scenario 'beoogd' for file 'AERIUS_projectberekening_register.pdf'
        Then the detail info of the line with scenario 'beoogd' of import file 'AERIUS_projectberekening_register.pdf' should not exist

    @Smoke
    Scenario: Download errors and warnings
        # Validate button shown/not shown
        Given I login with correct username and password
        When I select file 'emissionSourceFarAway.gml' to be imported, but I don't upload it yet
        Then the button to download warnings and errors should not be shown
        When I select file 'AERIUS_gml_error_warning.zip' to be imported, but I don't upload it yet
        Then the button to download warnings and errors should be shown
        When I remove the file 'AERIUS_gml_error_warning.zip' from the import list
        Then the button to download warnings and errors should not be shown

        # Test download of error/warning file
        When I select file 'AERIUS_gml_error_warning.zip' to be imported, but I don't upload it yet
        Then I download the errors and warnings and assert a CSV has been downloaded

    Scenario: Try importing a RCP file - Advanced
        # Validate default import settings for RCP file
        Given I login with correct username and password
        And I enable the advanced importmodus
        When I select file 'AERIUS_import_file.rcp' to be imported, but I don't upload it yet
        Then I expect the 'Naam' column to be empty
        And for the usage dropdown I expect to see the option 'Voeg rekenpunten toe' under the header 'Overige'
        And for the usage dropdown I expect to see the option 'Negeer' under the header 'Overige'
        And for the usage dropdown I expect the selected option to be 'Voeg rekenpunten toe'
        And I expect the 'Situatie type' column to be empty

    Scenario: Try importing an unsupported file - Advanced
        # Validate default import settings for unrecognized file
        Given I login with correct username and password
        And I enable the advanced importmodus
        When I select file 'jpg-example.jpg' to be imported, but I don't upload it yet
        Then I expect the 'Naam' column to be empty
        And for the usage dropdown I expect to see the option 'Negeer' under the header 'Overige'
        And I expect the 'Situatie type' column to be empty
        And button 'Importeer' is disabled

        # Set the only file to be imported to 'ignore' and try importing (It should work)
        When I select 'Negeer' as usage for file 'jpg-example.jpg' with no scenario
        And I try to import the file
        And I expect to see an empty import screen

    Scenario: Import BRN and check emissions - Standard
        # Try importing BRN without selecting pollutant
        Given I login with correct username and password
        When I select BRN file '1_source.brn' to be imported
        Then I expect to see a dropdown in which I can choose pollutants
        And button 'Importeer' is disabled
        And I remove the file '1_source.brn' from the import list

        # Import BRN file, choose NOx for the import and validate
        When I select BRN file '1_source.brn' to be imported as pollutant 'NOx' and import it
        Then I open emission source list
        And I select the source '(0)'
        Then the total 'generic' emission 'NOx' is '4.944,8 kg/j'
        And the total 'generic' emission 'NH3' is '0,0 kg/j'

        # Reset to empty import again
        Then I delete the current scenario

        # Import BRN file, choose NH3 for the import and validate
        When I select BRN file '1_source.brn' to be imported as pollutant 'NH3' and import it
        Then I open emission source list
        And I select the source '(0)'
        Then the total 'generic' emission 'NOx' is '0,0 kg/j'
        And the total 'generic' emission 'NH3' is '4.944,8 kg/j'

    @Smoke
    Scenario: Test BRN options - Advanced
        # Validate BRN in advanced import mode without selecting a pollutant. Then validate the available options
        Given I login with correct username and password
        And I enable the advanced importmodus
        When I select BRN file '1_source.brn' to be imported
        Then I expect the 'Naam' column to be empty
        And I expect the 'Gebruik' column to be empty
        And I expect the 'Situatie type' column to be empty
        And I expect to see a dropdown in which I can choose pollutants
        And button 'Importeer' is disabled
        And I remove the file '1_source.brn' from the import list

        # Upload BRN file again and select the pollutant 'NOx'. Then validate the available options again
        When I select BRN file '1_source.brn' to be imported as pollutant 'NOx'
        Then I expect the 'Naam' column to be empty
        And I expect the 'Gebruik' column to not be empty
        And I expect the 'Situatie type' column to not be empty
        And for the usage dropdown I expect to see the option 'Als nieuwe situatie'
        And for the usage dropdown I expect to see the option 'Negeer' under the header 'Overige'
        And for the situation type dropdown I expect the selected option to be 'Beoogd'

    Scenario: Import GML file with default situation type 'Beoogd'
        Given I login with correct username and password
        And I select file 'AERIUS_import_file.gml' to be imported
        Then the situation name is 'Situatie 1'
        And the situation type is 'Beoogd'
        And the calculation year is 'minCalculationYear'
        When I open the source list
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:57213,62 Y:427320,74' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '25,000' and diurnal variation type 'Dierverblijven' is correctly saved
        And the animal housing system with housing code 'HE1.1.2.1', number of animals '1000', factor '0,02', reduction '' and emission '20,0 kg/j' is correctly created
        When I select the source 'Bron 2'
        Then the source 'Bron 2' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:57245,55 Y:427432,47' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '150,0 kg/j'
        And the total 'generic' emission 'NH3' is '180,0 kg/j'
        When I select view mode 'BUILDING'
        And I select the building 'Gebouw 1'
        Then the building with name 'Gebouw 1', length '137,0 m  (105,0 m)', width '102,7 m  (87,2 m)', height '23,0 m  (20,0 m)' and orientation '89°' is saved and selected

    Scenario: Import zipped GML file with situation type 'Referentie'
        Given I login with correct username and password
        And I enable the advanced importmodus
        And I select file 'AERIUS_import_gml_file.zip' to be imported and select situation type 'Referentie', but I don't upload it yet
        And I choose to import the files
        Then the situation name is 'Situatie 1'
        And the situation type is 'Referentie'
        And the calculation year is 'minCalculationYear'
        When I open the source list
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:57213,62 Y:427320,74' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '25,000' and diurnal variation type 'Dierverblijven' is correctly saved
        And the animal housing system with housing code 'HE1.1.2.1', number of animals '1000', factor '0,02', reduction '' and emission '20,0 kg/j' is correctly created
        When I select the source 'Bron 2'
        Then the source 'Bron 2' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:57245,55 Y:427432,47' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '150,0 kg/j'
        And the total 'generic' emission 'NH3' is '180,0 kg/j'
        When I select view mode 'BUILDING'
        And I select the building 'Gebouw 1'
        Then the building with name 'Gebouw 1', length '137,0 m  (105,0 m)', width '102,7 m  (87,2 m)', height '23,0 m  (20,0 m)' and orientation '89°' is saved and selected

    Scenario: Import GML file that was extracted from a pdf
        Given I login with correct username and password
        And I select file 'AERIUS_import_file_GML_extracted_from_pdf.gml' to be imported
        Then the situation name is 'Situatie 1'
        And the situation type is 'Beoogd'
        And the calculation year is '2024'
        When I open the source list
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:252238,63 Y:462510,25' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '50,0 kg/j'
        And the total 'generic' emission 'NH3' is '50,0 kg/j'

    Scenario: Import PDF file with situation type 'Tijdelijk'
        Given I login with correct username and password
        And I enable the advanced importmodus
        And I select file 'AERIUS_import_file.pdf' to be imported and select situation type 'Tijdelijk', but I don't upload it yet
        When I choose to import the files
        Then the situation name is 'Situatie 1'
        And the situation type is 'Tijdelijk'
        And the calculation year is 'minCalculationYear'
        When I open the source list
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:57213,62 Y:427320,74' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '25,000' and diurnal variation type 'Dierverblijven' is correctly saved
        And the animal housing system with housing code 'HE1.1.2.1', number of animals '1000', factor '0,02', reduction '' and emission '20,0 kg/j' is correctly created
        When I select the source 'Bron 2'
        Then the source 'Bron 2' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:57245,55 Y:427432,47' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '150,0 kg/j'
        And the total 'generic' emission 'NH3' is '180,0 kg/j'
        When I select view mode 'BUILDING'
        And I select the building 'Gebouw 1'
        Then the building with name 'Gebouw 1', length '137,0 m  (105,0 m)', width '102,7 m  (87,2 m)', height '23,0 m  (20,0 m)' and orientation '89°' is saved and selected

    Scenario: Import an old PDF file (without attachment)
        Given I login with correct username and password
        And I enable the advanced importmodus
        And I select file 'AERIUS_import_old_pdf_file_no_attachment.pdf' to be imported and select situation type 'Tijdelijk', but I don't upload it yet
        When I choose to import the files
        Then the situation name is 'Situatie 1'
        And the situation type is 'Tijdelijk'
        And the calculation year is 'minCalculationYear'
        When I open the source list
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:57213,62 Y:427320,74' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '25,000' and diurnal variation type 'Dierverblijven' is correctly saved
        And the animal housing system with housing code 'HE1.1.2.1', number of animals '1000', factor '0,02', reduction '' and emission '20,0 kg/j' is correctly created
        When I select the source 'Bron 2'
        Then the source 'Bron 2' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:57245,55 Y:427432,47' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '150,0 kg/j'
        And the total 'generic' emission 'NH3' is '180,0 kg/j'
        When I select view mode 'BUILDING'
        And I select the building 'Gebouw 1'
        Then the building with name 'Gebouw 1', length '137,0 m  (105,0 m)', width '102,7 m  (87,2 m)', height '23,0 m  (20,0 m)' and orientation '89°' is saved and selected
        And I navigate to the 'Assessment points' tab
        Then the overview list has 87 assessment points

    @Smoke
    Scenario: Import zipped PDF file with situation type 'Saldering'
        Given I login with correct username and password
        And I enable the advanced importmodus
        And I select file 'AERIUS_import_pdf_file.zip' to be imported and select situation type 'Saldering', but I don't upload it yet
        When I choose to import the files
        Then the situation name is 'Situatie 1'
        And the situation type is 'Saldering'
        And the calculation year is 'minCalculationYear'
        When I open the source list
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:57213,62 Y:427320,74' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '25,000' and diurnal variation type 'Dierverblijven' is correctly saved
        And the animal housing system with housing code 'HE1.1.2.1', number of animals '1000', factor '0,02', reduction '' and emission '20,0 kg/j' is correctly created
        When I select the source 'Bron 2'
        Then the source 'Bron 2' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:57245,55 Y:427432,47' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '150,0 kg/j'
        And the total 'generic' emission 'NH3' is '180,0 kg/j'
        When I select view mode 'BUILDING'
        And I select the building 'Gebouw 1'
        Then the building with name 'Gebouw 1', length '137,0 m  (105,0 m)', width '102,7 m  (87,2 m)', height '23,0 m  (20,0 m)' and orientation '89°' is saved and selected

    @Smoke
    Scenario: Import RCP file
        Given I login with correct username and password
        And I select file 'AERIUS_import_file.rcp' to be imported
        Then calculation point '20001' is correctly saved
        And calculation point '20002' is correctly saved
        And calculation point '20003' is correctly saved
        And calculation point '20004' is correctly saved
        And calculation point '840001' is correctly saved
        And calculation point '840002' is correctly saved
        And calculation point '840003' is correctly saved
        And calculation point '50001' is correctly saved
        And calculation point '50002' is correctly saved
        And calculation point '50003' is correctly saved

    @Smoke
    Scenario: Import file with source and calculation points
        Given I login with correct username and password
        And I select file 'AERIUS_gml_calculation_points.zip' to be imported
        Then the situation name is 'Situatie 1'
        And the situation type is 'Beoogd'
        And the calculation year is 'minCalculationYear'
        When I open the source list
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:125324,48 Y:565359,04' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '5,0', heat content '0,000' and diurnal variation type 'Dierverblijven' is correctly saved
        And the animal housing system with housing code 'HA1.3', number of animals '1111', factor '9,2', reduction '' and emission '10,2 ton/j' is correctly created
        When I select menu calculation point
        Then calculation point 'Rekenpunt 1' is correctly saved
        And calculation point 'Rekenpunt 2' is correctly saved
        And calculation point 'Rekenpunt 3' is correctly saved
        And calculation point 'Rekenpunt 4' is correctly saved
        And calculation point 'Rekenpunt 5' is correctly saved
        And calculation point 'Rekenpunt 6' is correctly saved

    @Smoke
    Scenario: Import file with all emission source types and check on emission data
        Given I login with correct username and password
        And I select file 'AERIUS_everything_import_file.gml' to be imported

        # Road transportation source
        When I open the source list
        And I select the source 'Verkeersnetwerk'
        And I select the source 'Bron 7'
        Then the total 'road' emission 'NOx' is '0,3 kg/j'
        And the total 'road' emission 'NO2' is '77,9 g/j'
        And the total 'road' emission 'NH3' is '14,8 g/j'

        # Energy source
        When I select the source 'Bron 1'
        Then the total 'generic' emission 'NOx' is '4,0 kg/j'
        And the total 'generic' emission 'NH3' is '0,0 kg/j'

        # Agriculture (animal housing) source
        When I select the source 'Bron 2'
        Then the total 'animal housing' emission 'NOx' is '0,0 kg/j'
        Then the total 'animal housing' emission 'NH3' is '48,0 kg/j'

        # Agriculture (manure storage) source
        When I select the source 'Bron 3'
        Then the total 'generic' emission 'NOx' is '0,0 kg/j'
        Then the total 'generic' emission 'NH3' is '3,0 kg/j'

        # Agriculture (farmland) source
        When I select the source 'Bron 4'
        Then the total 'farmland' emission 'NOx' is '46,0 kg/j'
        Then the total 'farmland' emission 'NH3' is '238,0 kg/j'

        # Agriculture (horticulture) source
        When I select the source 'Bron 5'
        Then the total 'generic' emission 'NOx' is '2,0 kg/j'
        Then the total 'generic' emission 'NH3' is '3,0 kg/j'

        # Agriculture (Fireplaces, other) source
        When I select the source 'Bron 6'
        Then the total 'generic' emission 'NOx' is '23,0 kg/j'
        Then the total 'generic' emission 'NH3' is '0,0 kg/j'

        # Industry (waste processing) source
        When I select the source 'Bron 8'
        Then the total 'generic' emission 'NOx' is '23,0 kg/j'
        Then the total 'generic' emission 'NH3' is '0,0 kg/j'

        # Industry (food) source
        When I select the source 'Bron 9'
        Then the total 'generic' emission 'NOx' is '0,0 kg/j'
        Then the total 'generic' emission 'NH3' is '3,0 kg/j'

        # Industry (chemical) source
        When I select the source 'Bron 10'
        Then the total 'generic' emission 'NOx' is '0,0 kg/j'
        Then the total 'generic' emission 'NH3' is '23,0 kg/j'

        # Industry (building materials) source
        When I select the source 'Bron 11'
        Then the total 'generic' emission 'NOx' is '0,0 kg/j'
        Then the total 'generic' emission 'NH3' is '23,0 kg/j'

        # Industry (basic metals) source
        When I select the source 'Bron 12'
        Then the total 'generic' emission 'NOx' is '0,0 kg/j'
        Then the total 'generic' emission 'NH3' is '23,0 kg/j'

        # Industry (metalworking) source
        When I select the source 'Bron 13'
        Then the total 'generic' emission 'NOx' is '0,0 kg/j'
        Then the total 'generic' emission 'NH3' is '23,0 kg/j'

        # Industry (other) source
        When I select the source 'Bron 14'
        Then the total 'generic' emission 'NOx' is '0,0 kg/j'
        Then the total 'generic' emission 'NH3' is '23,0 kg/j'

        # Living and working (houses) source
        When I select the source 'Bron 15'
        Then the total 'generic' emission 'NOx' is '0,0 kg/j'
        Then the total 'generic' emission 'NH3' is '12,0 kg/j'

        # Living and working (recreation) source
        When I select the source 'Bron 16'
        Then the total 'generic' emission 'NOx' is '12,0 kg/j'
        Then the total 'generic' emission 'NH3' is '0,0 kg/j'

        # Living and working (offices and shops) source
        When I select the source 'Bron 17'
        Then the total 'generic' emission 'NOx' is '0,0 kg/j'
        Then the total 'generic' emission 'NH3' is '12,0 kg/j'

        # Rail transport (emplacement) source
        When I select the source 'Bron 18'
        Then the total 'generic' emission 'NOx' is '2,0 kg/j'
        Then the total 'generic' emission 'NH3' is '0,0 kg/j'

        # Rail transport (railway) source
        When I select the source 'Bron 19'
        Then the total 'generic' emission 'NOx' is '0,0 kg/j'
        Then the total 'generic' emission 'NH3' is '23,0 kg/j'

        # Air traffic (taking off) source
        When I select the source 'Bron 20'
        Then the total 'generic' emission 'NOx' is '0,0 kg/j'
        Then the total 'generic' emission 'NH3' is '23,0 kg/j'

        # Air traffic (landing) source
        When I select the source 'Bron 21'
        Then the total 'generic' emission 'NOx' is '0,0 kg/j'
        Then the total 'generic' emission 'NH3' is '2,0 kg/j'

        # Air traffic (taxiing) source
        When I select the source 'Bron 22'
        Then the total 'generic' emission 'NOx' is '0,0 kg/j'
        Then the total 'generic' emission 'NH3' is '2,0 kg/j'

        # Air traffic (airport grounds) source
        When I select the source 'Bron 23'
        Then the total 'generic' emission 'NOx' is '0,0 kg/j'
        Then the total 'generic' emission 'NH3' is '2,0 kg/j'

        # Shipping (maritime: docked) source
        When I select the source 'Bron 24'
        Then the total 'maritime shipping' emission 'NOx' is '22,5 kg/j'
        Then the total 'maritime shipping' emission 'NH3' is '0,0 kg/j'

        # Shipping (maritime: inland route) source
        When I select the source 'Bron 25'
        Then the total 'maritime shipping' emission 'NOx' is '5,4 kg/j'
        Then the total 'maritime shipping' emission 'NH3' is '0,0 kg/j'

        # Shipping (maritime: sea route) source
        When I select the source 'Bron 26'
        Then the total 'maritime shipping' emission 'NOx' is '5,8 kg/j'
        Then the total 'maritime shipping' emission 'NH3' is '0,0 kg/j'

        # Shipping (inland: docked) source
        When I select the source 'Bron 27'
        Then the total 'inland shipping' emission 'NOx' is '1,2 kg/j'
        Then the total 'inland shipping' emission 'NH3' is '0,0 kg/j'

        # Shipping (inland: route) source
        When I select the source 'Bron 28'
        Then the total 'inland shipping' emission 'NOx' is '3.075,2 ton/j'
        Then the total 'inland shipping' emission 'NH3' is '0,0 kg/j'

        # Mobile equipment (agriculture) source
        When I select the source 'Bron 29'
        Then the total 'mobile equipment' emission 'NOx' is '0,2 kg/j'
        Then the total 'mobile equipment' emission 'NH3' is '0,0 kg/j'

        # Mobile equipment (industry) source
        When I select the source 'Bron 30'
        Then the total 'mobile equipment' emission 'NOx' is '0,1 kg/j'
        Then the total 'mobile equipment' emission 'NH3' is '0,0 kg/j'

        # Mobile equipment (consumer) source
        When I select the source 'Bron 31'
        Then the total 'mobile equipment' emission 'NOx' is '0,2 kg/j'
        Then the total 'mobile equipment' emission 'NH3' is '0,0 kg/j'

        # Other source
        When I select the source 'Bron 32'
        Then the total 'generic' emission 'NOx' is '3,0 kg/j'
        Then the total 'generic' emission 'NH3' is '2,0 kg/j'

    Scenario: Import file with not supported calculation year in the past
        Given I login with correct username and password
        And I select file 'AERIUS_gml_2019.zip' to be imported
        Then the calculation year is 'minCalculationYear'
        When I open the source list
        And I select the source 'Bron 1'
        Then the total 'animal housing' emission 'NOx' is '0,0 kg/j'
        And the total 'animal housing' emission 'NH3' is '57,0 ton/j'
        When I select the source 'Bron 2'
        Then the total 'generic' emission 'NOx' is '1.234,0 kg/j'
        And the total 'generic' emission 'NH3' is '5.678,0 kg/j'

    Scenario: Import file with not supported calculation year in the future
        Given I login with correct username and password
        And I select file 'AERIUS_gml_2041.zip' to be imported
        Then the calculation year is '2040'
        When I open the source list
        And I select the source 'Bron 1'
        Then the total 'animal housing' emission 'NOx' is '0,0 kg/j'
        And the total 'animal housing' emission 'NH3' is '57,0 ton/j'
        When I select the source 'Bron 2'
        Then the total 'generic' emission 'NOx' is '1.234,0 kg/j'
        And the total 'generic' emission 'NH3' is '5.678,0 kg/j'

    Scenario: Import large gml file
        Given I login with correct username and password
        And I select file 'AERIUS_gml_large_source_file.zip' to be imported
        Then the calculation year is 'minCalculationYear'
        When I open the source list
        And I select the source 'Bron 1'
        Then the total 'generic' emission 'NOx' is '500,0 kg/j'
        And the total 'generic' emission 'NH3' is '300,0 kg/j'
        When I select the source 'Bron 2'
        Then the total 'generic' emission 'NOx' is '600,0 kg/j'
        And the total 'generic' emission 'NH3' is '400,0 kg/j'

    # Temporary disable this test is requires Cypress version 9.7.0
    # We use a testcontainer that locks the version to 9.1.0
    # Examen a solution

    # Scenario: Import file by drag and drop
    #    Given I login with correct username and password
    #    And I choose to import a source starting from the start page
    #    And I open the file dialog to import
    #    And I drag and drop file 'AERIUS_import_file.gml' to be imported
    #    Then the situation name is 'Situatie 1'
    #    And the situation type is 'Beoogd'
    #    And the calculation year is 'minCalculationYear'
    #    When I select the source 'Bron 1'
    #    Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Stalemissies' is correctly saved
    #    And location with coordinates 'X:57213,62 Y:427320,74' is correctly saved
    #    And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '25,000' and diurnal variation type 'Dierverblijven' is correctly saved
    #    And the animal housing system with housing code 'E1.5.1', number of animals '1000', factor '0,02', reduction '-' and emission '20,0 kg/j' is correctly created
    #    When I select the source 'Bron 2'
    #    Then the source 'Bron 2' with sector group 'Energie' is correctly saved
    #    And location with coordinates 'X:57245,55 Y:427432,47' is correctly saved
    #    And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
    #    And the total 'generic' emission 'NOx' is '150,0 kg/j'
    #    And the total 'generic' emission 'NH3' is '180,0 kg/j'
    #    When I select view mode 'BUILDING'
    #    And I select the building 'Gebouw 1'
    #    Then the building with name 'Gebouw 1', length '137,0 m  (105,0 m)', width '102,7 m  (87,2 m)', height '23,0 m  (20,0 m)' and orientation '89°' is saved and selected

    Scenario: Import file into scenario with a different year
        Given I login with correct username and password
        And I enable the advanced importmodus
        And I create an empty scenario from the start page
        Then I save the default calculation year

        When I navigate to the 'Home' tab
        Then I select file 'AERIUS_gml_wegverkeer.zip' to be imported, but I don't upload it, or open the collapsible panel yet
        And I select 'Situatie 1' as usage for scenario 'Situatie 1' of file 'AERIUS_gml_wegverkeer.zip'
        Then I choose to import the files

        # Ensure the calculation year is the one from the empty scenario, and not the one from the file
        Then the calculation year is the default calculation year

        When I open emission source list
        And I select the source 'Verkeersnetwerk'
        And I select the source 'Bron 1'

        # Check whether the emission is not the same as the one from 2024 (the reference year of the import file)
        Then the total 'road' emission 'NOx' is not '472,4 kg/j'

    Scenario: Advanced import mode - add new situation as situation type 'Beoogd'
        Given I login with correct username and password
        And I enable the advanced importmodus
        And I select file 'AERIUS_import_file.gml' to be imported, select import action 'Als nieuwe situatie' and situation type 'Beoogd', but I don't upload it yet
        And I choose to import the files
        Then the situation name is 'Situatie 1'
        And the situation type is 'Beoogd'
        And the calculation year is 'minCalculationYear'
        When I open the source list
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:57213,62 Y:427320,74' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '25,000' and diurnal variation type 'Dierverblijven' is correctly saved
        And the animal housing system with housing code 'HE1.1.2.1', number of animals '1000', factor '0,02', reduction '' and emission '20,0 kg/j' is correctly created
        When I select the source 'Bron 2'
        Then the source 'Bron 2' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:57245,55 Y:427432,47' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '150,0 kg/j'
        And the total 'generic' emission 'NH3' is '180,0 kg/j'
        When I select view mode 'BUILDING'
        And I select the building 'Gebouw 1'
        Then the building with name 'Gebouw 1', length '137,0 m  (105,0 m)', width '102,7 m  (87,2 m)', height '23,0 m  (20,0 m)' and orientation '89°' is saved and selected

    Scenario: Advanced import mode - add new situation as situation type 'Referentie'
        Given I login with correct username and password
        And I enable the advanced importmodus
        And I select file 'AERIUS_import_file.gml' to be imported, select import action 'Als nieuwe situatie' and situation type 'Referentie', but I don't upload it yet
        And I choose to import the files
        Then the situation name is 'Situatie 1'
        And the situation type is 'Referentie'
        And the calculation year is 'minCalculationYear'
        When I open the source list
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:57213,62 Y:427320,74' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '25,000' and diurnal variation type 'Dierverblijven' is correctly saved
        And the animal housing system with housing code 'HE1.1.2.1', number of animals '1000', factor '0,02', reduction '' and emission '20,0 kg/j' is correctly created
        When I select the source 'Bron 2'
        Then the source 'Bron 2' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:57245,55 Y:427432,47' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '150,0 kg/j'
        And the total 'generic' emission 'NH3' is '180,0 kg/j'
        When I select view mode 'BUILDING'
        And I select the building 'Gebouw 1'
        Then the building with name 'Gebouw 1', length '137,0 m  (105,0 m)', width '102,7 m  (87,2 m)', height '23,0 m  (20,0 m)' and orientation '89°' is saved and selected

    Scenario: Advanced import mode - add new situation as situation type 'Tijdelijk'
        Given I login with correct username and password
        And I enable the advanced importmodus
        And I select file 'AERIUS_import_file.gml' to be imported, select import action 'Als nieuwe situatie' and situation type 'Tijdelijk', but I don't upload it yet
        And I choose to import the files
        Then the situation name is 'Situatie 1'
        And the situation type is 'Tijdelijk'
        And the calculation year is 'minCalculationYear'
        When I open the source list
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:57213,62 Y:427320,74' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '25,000' and diurnal variation type 'Dierverblijven' is correctly saved
        And the animal housing system with housing code 'HE1.1.2.1', number of animals '1000', factor '0,02', reduction '' and emission '20,0 kg/j' is correctly created
        When I select the source 'Bron 2'
        Then the source 'Bron 2' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:57245,55 Y:427432,47' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '150,0 kg/j'
        And the total 'generic' emission 'NH3' is '180,0 kg/j'
        When I select view mode 'BUILDING'
        And I select the building 'Gebouw 1'
        Then the building with name 'Gebouw 1', length '137,0 m  (105,0 m)', width '102,7 m  (87,2 m)', height '23,0 m  (20,0 m)' and orientation '89°' is saved and selected

    @Smoke
    Scenario: Advanced import mode - add new situation as situation type 'Saldering'
        Given I login with correct username and password
        And I enable the advanced importmodus
        And I select file 'AERIUS_import_file.gml' to be imported, select import action 'Als nieuwe situatie' and situation type 'Saldering', but I don't upload it yet
        And I choose to import the files
        Then the situation name is 'Situatie 1'
        And the situation type is 'Saldering'
        And the calculation year is 'minCalculationYear'
        And the netting factor is '0.3'
        When I open the source list
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:57213,62 Y:427320,74' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '25,000' and diurnal variation type 'Dierverblijven' is correctly saved
        And the animal housing system with housing code 'HE1.1.2.1', number of animals '1000', factor '0,02', reduction '' and emission '20,0 kg/j' is correctly created
        When I select the source 'Bron 2'
        Then the source 'Bron 2' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:57245,55 Y:427432,47' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '150,0 kg/j'
        And the total 'generic' emission 'NH3' is '180,0 kg/j'
        When I select view mode 'BUILDING'
        And I select the building 'Gebouw 1'
        Then the building with name 'Gebouw 1', length '137,0 m  (105,0 m)', width '102,7 m  (87,2 m)', height '23,0 m  (20,0 m)' and orientation '89°' is saved and selected

    Scenario: Advanced import mode - add new situation to existing situation
        Given I login with correct username and password
        And I enable the advanced importmodus
        When I select file 'AERIUS_import_file.gml' to be imported, select import action 'Als nieuwe situatie' and situation type 'Beoogd', but I don't upload it yet
        And I choose to import the files
        Then the situation name is 'Situatie 1'
        And the situation type is 'Beoogd'
        And the calculation year is 'minCalculationYear'
        And I click on the start page button

        # Add new situation to existing Situation 1
        When I select file 'AERIUS_import_gml_file_saldering.zip' to be imported and select import action 'Situatie 1', but I don't upload it yet
        And I choose to import the files
        Then the situation name is 'Situatie 1'
        And the situation type is 'Beoogd'
        And the calculation year is 'minCalculationYear'
        When I open the source list
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:57213,62 Y:427320,74' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '25,000' and diurnal variation type 'Dierverblijven' is correctly saved
        And the animal housing system with housing code 'HE1.1.2.1', number of animals '1000', factor '0,02', reduction '' and emission '20,0 kg/j' is correctly created
        When I select the source 'Bron 2'
        Then the source 'Bron 2' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:57245,55 Y:427432,47' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '150,0 kg/j'
        And the total 'generic' emission 'NH3' is '180,0 kg/j'
        And I select the source 'Bron 3'
        Then the source 'Bron 3' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:57213,62 Y:427320,74' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '25,000' and diurnal variation type 'Dierverblijven' is correctly saved
        And the animal housing system with housing code 'HE1.1.2.1', number of animals '2000', factor '0,02', reduction '' and emission '40,0 kg/j' is correctly created
        When I select the source 'Bron 4'
        Then the source 'Bron 4' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:57245,55 Y:427432,47' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '400,0 kg/j'
        And the total 'generic' emission 'NH3' is '500,0 kg/j'
        When I select view mode 'BUILDING'
        And I select the building 'Gebouw 1'
        Then the building with name 'Gebouw 1', length '137,0 m  (105,0 m)', width '102,7 m  (87,2 m)', height '23,0 m  (20,0 m)' and orientation '89°' is saved and selected
        And I select the building 'Gebouw 2'
        Then the building with name 'Gebouw 2', length '137,0 m  (105,0 m)', width '102,7 m  (87,2 m)', height '15,0 m' and orientation '89°' is saved and selected

    Scenario: Advanced import mode - ignore adding new situation to existing situation
        Given I login with correct username and password
        And I enable the advanced importmodus
        And I select file 'AERIUS_import_file.gml' to be imported, select import action 'Als nieuwe situatie' and situation type 'Beoogd', but I don't upload it yet
        And I choose to import the files
        Then the situation name is 'Situatie 1'
        And the situation type is 'Beoogd'
        And the calculation year is 'minCalculationYear'
        And I click on the start page button
        # add new situation to existing Situation 1 and then ignore
        When I select file 'AERIUS_import_gml_file_saldering.zip' to be imported and select import action 'Negeer', but I don't upload it yet
        And I choose to import the files
        Then the situation name is 'Situatie 1'
        And the situation type is 'Beoogd'
        And the calculation year is 'minCalculationYear'
        When I open the source list
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:57213,62 Y:427320,74' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '25,000' and diurnal variation type 'Dierverblijven' is correctly saved
        And the animal housing system with housing code 'HE1.1.2.1', number of animals '1000', factor '0,02', reduction '' and emission '20,0 kg/j' is correctly created
        When I select the source 'Bron 2'
        Then the source 'Bron 2' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:57245,55 Y:427432,47' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '150,0 kg/j'
        And the total 'generic' emission 'NH3' is '180,0 kg/j'
        Then the next source does not exist 'Bron 3'
        And the next source does not exist 'Bron 4'

    Scenario: Advanced import mode - merge Situation 2 with Situation 1
        Given I login with correct username and password
        And I enable the advanced importmodus
        And I select file 'AERIUS_import_file.gml' to be imported, but I don't upload it yet
        # merge Situation 2 with Situation 1
        And I select file 'AERIUS_import_gml_file_saldering.zip' to be imported, but I don't upload it yet
        # when merging situations, situation type will automatically be the same as the situation that's merged with
        And I select import action 'Situatie 1' for file 'AERIUS_import_gml_file_saldering.zip'
        And I choose to import the files
        Then the situation name is 'Situatie 1'
        And the situation type is 'Beoogd'
        And the calculation year is 'minCalculationYear'
        When I open the source list
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:57213,62 Y:427320,74' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '25,000' and diurnal variation type 'Dierverblijven' is correctly saved
        And the animal housing system with housing code 'HE1.1.2.1', number of animals '1000', factor '0,02', reduction '' and emission '20,0 kg/j' is correctly created
        When I select the source 'Bron 2'
        Then the source 'Bron 2' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:57245,55 Y:427432,47' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '150,0 kg/j'
        And the total 'generic' emission 'NH3' is '180,0 kg/j'
        And I select the source 'Bron 3'
        Then the source 'Bron 3' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:57213,62 Y:427320,74' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '25,000' and diurnal variation type 'Dierverblijven' is correctly saved
        And the animal housing system with housing code 'HE1.1.2.1', number of animals '2000', factor '0,02', reduction '' and emission '40,0 kg/j' is correctly created
        When I select the source 'Bron 4'
        Then the source 'Bron 4' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:57245,55 Y:427432,47' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '400,0 kg/j'
        And the total 'generic' emission 'NH3' is '500,0 kg/j'
        When I select view mode 'BUILDING'
        And I select the building 'Gebouw 1'
        Then the building with name 'Gebouw 1', length '137,0 m  (105,0 m)', width '102,7 m  (87,2 m)', height '23,0 m  (20,0 m)' and orientation '89°' is saved and selected
        And I select the building 'Gebouw 2'
        Then the building with name 'Gebouw 2', length '137,0 m  (105,0 m)', width '102,7 m  (87,2 m)', height '15,0 m' and orientation '89°' is saved and selected

    Scenario: Advanced import mode - merge Situation 1 with Situation 2
        Given I login with correct username and password
        And I enable the advanced importmodus
        And I select file 'AERIUS_import_file.gml' to be imported, but I don't upload it yet
        # merge Situation 1 with Situation 2
        And I select file 'AERIUS_import_gml_file_saldering.zip' to be imported, but I don't upload it yet
        When I select import action 'Situatie 2' for file 'AERIUS_import_file.gml'
        And I choose to import the files
        Then the situation name is 'Situatie 2'
        And the situation type is 'Saldering'
        And the calculation year is 'minCalculationYear'
        And the netting factor is '0.3'
        When I open the source list
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:57213,62 Y:427320,74' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '25,000' and diurnal variation type 'Dierverblijven' is correctly saved
        And the animal housing system with housing code 'HE1.1.2.1', number of animals '1000', factor '0,02', reduction '' and emission '20,0 kg/j' is correctly created
        When I select the source 'Bron 2'
        Then the source 'Bron 2' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:57245,55 Y:427432,47' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '150,0 kg/j'
        And the total 'generic' emission 'NH3' is '180,0 kg/j'
        And I select the source 'Bron 3'
        Then the source 'Bron 3' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:57213,62 Y:427320,74' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '25,000' and diurnal variation type 'Dierverblijven' is correctly saved
        And the animal housing system with housing code 'HE1.1.2.1', number of animals '2000', factor '0,02', reduction '' and emission '40,0 kg/j' is correctly created
        When I select the source 'Bron 4'
        Then the source 'Bron 4' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:57245,55 Y:427432,47' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '400,0 kg/j'
        And the total 'generic' emission 'NH3' is '500,0 kg/j'
        When I select view mode 'BUILDING'
        And I select the building 'Gebouw 1'
        Then the building with name 'Gebouw 1', length '137,0 m  (105,0 m)', width '102,7 m  (87,2 m)', height '23,0 m  (20,0 m)' and orientation '89°' is saved and selected
        And I select the building 'Gebouw 2'
        Then the building with name 'Gebouw 2', length '137,0 m  (105,0 m)', width '102,7 m  (87,2 m)', height '15,0 m' and orientation '89°' is saved and selected

    Scenario: Warning is visible and file can still be imported
        Given I login with correct username and password
        And I enable the advanced importmodus
        When I select file 'AERIUS_import_file.gml' to be imported, but I don't upload it yet
        # Warning button
        When I click on the 'warning' button
        Then the following 'warning' text is visible: 'Het GML-bestand is geldig maar bevat niet de meest recente IMAER versie, gevonden versie \'V4_0\' verwacht \'V6_0\''
        # Warning tab
        When I click at the tab 'FILES'
        And I click at the tab 'WARNINGS'
        Then the following 'warning' text is visible: 'Het GML-bestand is geldig maar bevat niet de meest recente IMAER versie, gevonden versie \'V4_0\' verwacht \'V6_0\''
        When I choose to import the files
        Then the situation name is 'Situatie 1'
        And the situation type is 'Beoogd'
        And the calculation year is 'minCalculationYear'
        When I open the source list
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Dierhuisvesting' is correctly saved
        And location with coordinates 'X:57213,62 Y:427320,74' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '15,0', heat content '25,000' and diurnal variation type 'Dierverblijven' is correctly saved
        And the animal housing system with housing code 'HE1.1.2.1', number of animals '1000', factor '0,02', reduction '' and emission '20,0 kg/j' is correctly created
        When I select the source 'Bron 2'
        Then the source 'Bron 2' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:57245,55 Y:427432,47' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '150,0 kg/j'
        And the total 'generic' emission 'NH3' is '180,0 kg/j'
        When I select view mode 'BUILDING'
        And I select the building 'Gebouw 1'
        Then the building with name 'Gebouw 1', length '137,0 m  (105,0 m)', width '102,7 m  (87,2 m)', height '23,0 m  (20,0 m)' and orientation '89°' is saved and selected

    # TODO: put the validation results of this test in a datatable + check if this is also necessary for other tests
    Scenario: Import file with all types of situation
        Given I login with correct username and password
        And I enable the advanced importmodus
        And I select a file with multiple situations 'AERIUS_all_situation_types.zip' to be imported in advanced mode
        And I choose to import the files

        # Situatie 1 - Beoogd
        Then the situation name is 'Situatie 1'
        And the situation type is 'Beoogd'
        And the calculation year is 'minCalculationYear'
        When I open the source list
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:112854,73 Y:576494,99' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '1.122,0 kg/j'
        And the total 'generic' emission 'NH3' is '3.344,0 kg/j'
        When I select view mode 'BUILDING'
        And I select the building 'Gebouw 1'
        Then the building with name 'Gebouw 1', length '1.129,4 m  (105,0 m)', width '945,4 m  (87,2 m)', height '1,0 m' and orientation '77°' is saved and selected

        # Situatie 2 - Saldering
        When I select situation 'Situatie 2 - Saldering'
        Then the situation name is 'Situatie 2'
        And the situation type is 'Saldering'
        And the calculation year is 'minCalculationYear'
        And the netting factor is '0.4'
        When I open the source list
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:112854,73 Y:576494,99' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '3.242,0 kg/j'
        And the total 'generic' emission 'NH3' is '33,4 ton/j'
        When I select view mode 'BUILDING'
        And I select the building 'Gebouw 1'
        Then the building with name 'Gebouw 1', length '1.129,4 m  (105,0 m)', width '945,4 m  (87,2 m)', height '2,0 m' and orientation '77°' is saved and selected

        # Situatie 3 - Tijdelijk
        When I select situation 'Situatie 3 - Tijdelijk'
        Then the situation name is 'Situatie 3'
        And the situation type is 'Tijdelijk'
        And the calculation year is 'minCalculationYear'
        When I open the source list
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:112854,73 Y:576494,99' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '333,0 kg/j'
        And the total 'generic' emission 'NH3' is '3.344,0 kg/j'
        When I select view mode 'BUILDING'
        And I select the building 'Gebouw 1'
        Then the building with name 'Gebouw 1', length '1.129,4 m  (105,0 m)', width '945,4 m  (87,2 m)', height '3,0 m' and orientation '77°' is saved and selected

        # Situatie 4 - Referentie
        When I select situation 'Situatie 4 - Referentie'
        Then the situation name is 'Situatie 4'
        And the situation type is 'Referentie'
        And the calculation year is 'minCalculationYear'
        When I open the source list
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:112854,73 Y:576494,99' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '644,0 kg/j'
        And the total 'generic' emission 'NH3' is '6.633,0 kg/j'
        When I select view mode 'BUILDING'
        And I select the building 'Gebouw 1'
        Then the building with name 'Gebouw 1', length '1.129,4 m  (105,0 m)', width '945,4 m  (87,2 m)', height '4,0 m' and orientation '77°' is saved and selected

        # Situatie 5 - Beoogd
        When I select situation 'Situatie 5 - Beoogd'
        Then the situation name is 'Situatie 5'
        And the situation type is 'Beoogd'
        And the calculation year is 'minCalculationYear'
        When I open the source list
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:112854,73 Y:576494,99' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '8.662,0 kg/j'
        And the total 'generic' emission 'NH3' is '4.468,0 kg/j'
        When I select view mode 'BUILDING'
        And I select the building 'Gebouw 1'
        Then the building with name 'Gebouw 1', length '1.129,4 m  (105,0 m)', width '945,4 m  (87,2 m)', height '5,0 m' and orientation '77°' is saved and selected

        # Situatie 6 - Saldering
        When I select situation 'Situatie 6 - Saldering'
        Then the situation name is 'Situatie 6'
        And the situation type is 'Saldering'
        And the calculation year is 'minCalculationYear'
        And the netting factor is '0.3'
        When I open the source list
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:112854,73 Y:576494,99' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '1.111,0 kg/j'
        And the total 'generic' emission 'NH3' is '2.222,0 kg/j'
        When I select view mode 'BUILDING'
        And I select the building 'Gebouw 1'
        Then the building with name 'Gebouw 1', length '1.129,4 m  (105,0 m)', width '945,4 m  (87,2 m)', height '6,0 m' and orientation '77°' is saved and selected

        # Situatie 7 - Tijdelijk
        When I select situation 'Situatie 7 - Tijdelijk'
        Then the situation name is 'Situatie 7'
        And the situation type is 'Tijdelijk'
        And the calculation year is 'minCalculationYear'
        When I open the source list
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:112854,73 Y:576494,99' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '1.123,0 kg/j'
        And the total 'generic' emission 'NH3' is '4.444,0 kg/j'
        When I select view mode 'BUILDING'
        And I select the building 'Gebouw 1'
        Then the building with name 'Gebouw 1', length '1.129,4 m  (105,0 m)', width '945,4 m  (87,2 m)', height '7,0 m' and orientation '77°' is saved and selected

        # Situatie 8 - Referentie
        When I select situation 'Situatie 8 - Referentie'
        Then the situation name is 'Situatie 8'
        And the situation type is 'Referentie'
        And the calculation year is 'minCalculationYear'
        When I open the source list
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:112854,73 Y:576494,99' is correctly saved
        And source characteristics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And the total 'generic' emission 'NOx' is '1.133,0 kg/j'
        And the total 'generic' emission 'NH3' is '1.441,0 kg/j'
        When I select view mode 'BUILDING'
        And I select the building 'Gebouw 1'
        Then the building with name 'Gebouw 1', length '1.129,4 m  (105,0 m)', width '945,4 m  (87,2 m)', height '8,0 m' and orientation '77°' is saved and selected

    @Smoke
    Scenario: Buttons 'Delete all', 'Browse' and 'Import'
        Given I login with correct username and password
        When I am located at the introduction page
        # Buttons 'Delete all' and 'Import' are disabled when no files are selected
        Then button 'Alle verwijderen' is disabled
        And button 'Importeer' is disabled
        And button 'Bladeren' is enabled
        When I select file 'AERIUS_import_file.rcp' to be imported, but I don't upload it yet
        Then button 'Alle verwijderen' is enabled
        And button 'Importeer' is enabled
        And button 'Bladeren' is enabled
        When I select file 'AERIUS_import_file.gml' to be imported, but I don't upload it yet
        When I click on the 'Alle verwijderen' button
        Then file 'AERIUS_import_file.gml' is not visible in the import screen
        And file 'AERIUS_import_file.rcp' is not visible in the import screen
        And button 'Alle verwijderen' is disabled
        And button 'Importeer' is disabled
        And button 'Bladeren' is enabled

    Scenario: Only possible to add imported situation to an existing situation
        Given I login with correct username and password
        And I enable the advanced importmodus
        When I select file 'AERIUS_import_file.gml' to be imported, but I don't upload it yet
        Then it is not possible for file 'AERIUS_import_file.gml' to be added to a situation that does not exist
        When I select file 'AERIUS_import_file.rcp' to be imported, but I don't upload it yet
        Then it is not possible for file 'AERIUS_import_file.gml' to be added to a situation that does not exist

    Scenario: Advanced import mode - no import of emission sources
        Given I login with correct username and password
        And I enable the advanced importmodus
        When I select file 'AERIUS_import_file.gml' to be imported, but I don't upload it yet
        And I uncheck the box for importing element 'emissiebron'
        And I choose to import the files
        When I select view mode 'EMISSION_SOURCES'
        Then the emission source list has 0 sources
        When I select view mode 'BUILDING'
        Then the buildinglist has 1 buildings
        When I navigate to the 'Calculation jobs' tab
        Then the calculation jobs list has 0 calculation jobs

    Scenario: Advanced import mode - no import of building
        Given I login with correct username and password
        And I enable the advanced importmodus
        When I select file 'AERIUS_import_file.gml' to be imported, but I don't upload it yet
        And I uncheck the box for importing element 'gebouw'
        And I choose to import the files
        When I select view mode 'EMISSION_SOURCES'
        Then the emission source list has 2 sources
        When I select view mode 'BUILDING'
        Then the buildinglist has 0 buildings
        When I navigate to the 'Calculation jobs' tab
        Then the calculation jobs list has 0 calculation jobs

    Scenario: Advanced import mode - import gml only - validate no import jobs
        Given I login with correct username and password
        And I enable the advanced importmodus
        When I select file 'AERIUS_import_file.gml' to be imported, but I don't upload it yet
        And I validate import element availability
        | element          | available |
        | emissiebron      | true      |
        | gebouw           | true      |
        | rekentaak        | false     |

    @Smoke
    Scenario: Advanced import mode - import with calculation results - no import of calculation job
        Given I login with correct username and password
        And I enable the advanced importmodus
        When I select file 'AERIUS_import_calculation_result.gml' to be imported, but I don't upload it yet
        And I validate import element availability
        | element          | available |
        | emissiebron      | true      |
        | gebouw           | true      |
        | rekentaak        | true      |
        And I uncheck the box for importing element 'calculation job'
        And I choose to import the files
        When I select view mode 'EMISSION_SOURCES'
        Then the emission source list has 2 sources
        When I select view mode 'BUILDING'
        Then the buildinglist has 1 buildings
        When I navigate to the 'Calculation jobs' tab
        Then the calculation jobs list has 0 calculation jobs

    Scenario: Advanced import mode - import pdf - no import of calculation job
        Given I login with correct username and password
        And I enable the advanced importmodus
        When I select file 'AERIUS_import_calculation_job.pdf' to be imported, but I don't upload it yet
        And I validate import element availability
        | element          | available |
        | emissiebron      | true      |
        | gebouw           | true      |
        | rekentaak        | true      |
        And I uncheck the box for importing element 'calculation job'
        And I choose to import the files
        When I select view mode 'EMISSION_SOURCES'
        Then the emission source list has 2 sources
        When I select view mode 'BUILDING'
        Then the buildinglist has 1 buildings
        When I navigate to the 'Calculation jobs' tab
        Then the calculation jobs list has 0 calculation jobs

    Scenario: Error is visible and file can not be imported
        Given I login with correct username and password
        And I enable the advanced importmodus
        When I select file 'AERIUS_gml_error_warning.zip' to be imported, but I don't upload it yet
        # Error button
        When I click on the 'error' button
        Then the following 'error' text is visible: 'Het GML-bestand bevat een of meerdere foutieve waarden.'
        # Error tab
        When I click at the tab 'FILES'
        And I click at the tab 'ERRORS'
        Then the following 'error' text is visible: 'Het GML-bestand bevat een of meerdere foutieve waarden.'

    Scenario: Import gml with negative heat and validate the error message
        Given I login with correct username and password
        And I enable the advanced importmodus
        When I select file 'AERIUS_gml_negative_heatcontent.gml' to be imported, but I don't upload it yet
        # Error button
        When I click on the 'error' button
        Then the following 'error' text is visible: "De warmte inhoud van bron 'ES.1' (-10.0) valt niet in het geldige bereik van 0 tot en met 999."

    Scenario: Warning is visible when import file is from a previous not supported year
        Given I login with correct username and password
        And I enable the advanced importmodus
        And I select file 'AERIUS_calc_job_previous_not_supported_year.gml' to be imported, but I don't upload it yet
        # Warnings are visible
        When I click at the tab 'WARNINGS'
        Then the following 'warning' text is visible: "Het jaar '2023' is niet (meer) beschikbaar als rekenjaar en is automatisch aangepast."
        And the following 'warning' text is visible: "De resultaten in uw bestand zijn berekend met een oudere AERIUS-versie en worden om die reden niet geïmporteerd. U kunt de resultaten na het importeren opnieuw berekenen."
        # Calculation year is automatically set to a different year
        When I choose to import the files
        And I navigate to the 'Input' tab
        Then the calculation year is '2024'
        # Calculation job is imported, the results are not imported
        When I navigate to the 'Calculation jobs' tab
        Then I expect the following calculation job to exist 'Rekentaak 1 - AERIUS_calc_job_previous_not_supported_year'
        And I validate the status of the menu items
        | menuId                             | status   |
        | NAV_ICON-MENU-RESULTS              | disabled |

    Scenario: Validate that all old RAV codes can be imported without errors
        Given I login with correct username and password
        And I select file 'AERIUS_farmloding_legacy_rav.gml' to be imported, but I don't upload it yet
        When I click at the tab 'WARNINGS' which should have the title '411 waarschuwingen'
        When I click at the tab 'ERRORS' which should have the title '0 fouten'
        When I choose to import the files
        Then I open the source list and scroll to bottom
        Then there should be 408 sources

    Scenario: Validate that all animal housing and additional systems combinations can be imported without errors
        Given I login with correct username and password
        And I select file 'AERIUS_all_farm_animal_housing_combinations_with_additional_system.zip' to be imported, but I don't upload it yet
        When I click at the tab 'WARNINGS' which should have the title '0 waarschuwingen'
        When I click at the tab 'ERRORS' which should have the title '0 fouten'
        When I choose to import the files
        Then I open the source list and scroll to bottom
        Then there should be 3148 sources

    Scenario: Import PDF and validate the sources and calculation points
        # Start the application with advanced importmodus on, upload files and check amount of import lines in between
        Given I login with correct username and password
        And I enable the advanced importmodus
        When I select file 'AERIUS_calculation_points_results.pdf' to be imported, but I don't upload it, or open the collapsible panel yet
        Then there should be 2 scenarios ready to imported

        When I expand the line with scenario 'Situatie 1' for file 'AERIUS_calculation_points_results.pdf'
        Then the header of the detail info of the line with scenario 'Situatie 1' of import file 'AERIUS_calculation_points_results.pdf' should be 'Situatie 1'
        And the detail info of the line with scenario 'Situatie 1' of import file 'AERIUS_calculation_points_results.pdf' should contain '1 emissiebron'
        And the detail info of the line with scenario 'Situatie 1' of import file 'AERIUS_calculation_points_results.pdf' should contain '1 rekentaak (op basis van bestand)'
        And the detail info of the line with scenario 'Situatie 1' of import file 'AERIUS_calculation_points_results.pdf' should not contain '69 rekenpunten'

        When I expand line with no scenario for file 'AERIUS_calculation_points_results.pdf'
        Then the header of the detail info of the line with no scenario of import file 'AERIUS_calculation_points_results.pdf' should be 'Rekenpunten'
        And the detail info of the line with no scenario of import file 'AERIUS_calculation_points_results.pdf' should contain 'Rekenpunten'
        And the detail info of the line with no scenario of import file 'AERIUS_calculation_points_results.pdf' should contain '69 rekenpunten'
        And the detail info of the line with no scenario of import file 'AERIUS_calculation_points_results.pdf' should not contain 'gebouw'
        And the detail info of the line with no scenario of import file 'AERIUS_calculation_points_results.pdf' should not contain 'emissiebron'
        And the detail info of the line with no scenario of import file 'AERIUS_calculation_points_results.pdf' should not contain 'rekentaak'

    Scenario: Import legacy PDF and validate the sources and calculation points
        # Start the application with advanced importmodus on, upload legacy files and check amount of import lines in between
        Given I login with correct username and password
        And I enable the advanced importmodus
        When I select file 'AERIUS_import_old_pdf_file_no_attachment.pdf' to be imported, but I don't upload it, or open the collapsible panel yet
        Then there should be 2 scenarios ready to imported

        When I expand the line with scenario 'Situatie 1' for file 'AERIUS_import_old_pdf_file_no_attachment.pdf'
        Then the header of the detail info of the line with scenario 'Situatie 1' of import file 'AERIUS_import_old_pdf_file_no_attachment.pdf' should be 'Situatie 1'
        And the detail info of the line with scenario 'Situatie 1' of import file 'AERIUS_import_old_pdf_file_no_attachment.pdf' should contain '2 emissiebron'
        And the detail info of the line with scenario 'Situatie 1' of import file 'AERIUS_import_old_pdf_file_no_attachment.pdf' should contain '1 gebouw'
        And the detail info of the line with scenario 'Situatie 1' of import file 'AERIUS_import_old_pdf_file_no_attachment.pdf' should contain '1 rekentaak (op basis van bestand)'
        And the detail info of the line with scenario 'Situatie 1' of import file 'AERIUS_import_old_pdf_file_no_attachment.pdf' should not contain '69 rekenpunten'

        When I expand line with no scenario for file 'AERIUS_import_old_pdf_file_no_attachment.pdf'
        Then the header of the detail info of the line with no scenario of import file 'AERIUS_import_old_pdf_file_no_attachment.pdf' should be 'Rekenpunten'
        And the detail info of the line with no scenario of import file 'AERIUS_import_old_pdf_file_no_attachment.pdf' should contain 'Rekenpunten'
        And the detail info of the line with no scenario of import file 'AERIUS_import_old_pdf_file_no_attachment.pdf' should contain '87 rekenpunten'
        And the detail info of the line with no scenario of import file 'AERIUS_import_old_pdf_file_no_attachment.pdf' should not contain 'gebouw'
        And the detail info of the line with no scenario of import file 'AERIUS_import_old_pdf_file_no_attachment.pdf' should not contain 'emissiebron'
        And the detail info of the line with no scenario of import file 'AERIUS_import_old_pdf_file_no_attachment.pdf' should not contain 'rekentaak'