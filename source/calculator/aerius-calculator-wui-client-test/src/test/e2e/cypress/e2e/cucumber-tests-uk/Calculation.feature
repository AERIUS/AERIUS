#
# Crown Copyright
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Calculation

  @Smoke
  Scenario: Check calculation options greyed out
    Given I create a simple uk emission source from the start page
    And I navigate to the 'Calculation jobs' tab
    Then I validate the calculation job message 'There are no calculation jobs yet. Create a new calculation job by pressing the button below.'
    And I create a new calculation job

    # Quick run
    And I select the uk calculation method 'Quick run' and validate hint message is 'not visible'
    Then I expect the uk calculation option element 'permitAreaSelection' to be 'visible'
    Then I expect the uk calculation option element 'zoneOfInfluenceInput' to be 'visible'
    Then I expect the uk calculation options
      | element                       | state      | panel                   |
      | siteLocation                  | visible    | Meteorological settings |
      | metDatasetType                | visible    | Meteorological settings |
      | minMoninObukhovLength         | enabled    | Advanced settings       |
      | surfaceAlbedo                 | enabled    | Advanced settings       |
      | priestleyTaylorParameter      | enabled    | Advanced settings       |
      | plumeDepletionNH3             | exist      | Advanced settings       |
      | plumeDepletionNOX             | exist      | Advanced settings       |
      | complexTerrain                | exist      | Advanced settings       |
      | roadLocalFractionNO2Receptors | visible    | Advanced settings       |
      | roadLocalFractionNO2Points    | visible    | Advanced settings       |

    # Test fNO2 options
    And I select fNO2 option I expect element roadLocalFractionNO2 to have a specific state
      | type      | option                    | state      |
      | Receptors | Specified below           | visible    |
      | Receptors | Default based on location | not exist  |
      | Points    | Specified below           | visible    |
      | Points    | Default based on location | not exist  |
      | Points    | Use individual values     | not exist  |

    # Formal assessment
    And I select the uk calculation method 'Formal assessment' and validate hint message is 'visible'
    Then I expect the uk calculation option element 'permitAreaSelection' to be 'visible'
    Then I expect the uk calculation option element 'zoneOfInfluenceInput' to be 'not exist'
    Then I expect the uk calculation options
      | element                       | state      | panel                   |
      | siteLocation                  | visible    | Meteorological settings |
      | metDatasetType                | visible    | Meteorological settings |
      | minMoninObukhovLength         | enabled    | Advanced settings       |
      | surfaceAlbedo                 | enabled    | Advanced settings       |
      | priestleyTaylorParameter      | enabled    | Advanced settings       |
      | plumeDepletionNH3             | exist      | Advanced settings       |
      | plumeDepletionNOX             | exist      | Advanced settings       |
      | complexTerrain                | exist      | Advanced settings       |
      | roadLocalFractionNO2Receptors | visible    | Advanced settings       |
      | roadLocalFractionNO2Points    | visible    | Advanced settings       |

    # Custom assessment points
    And I select the uk calculation method 'Custom assessment points' and validate hint message is 'not visible'
    Then I expect the uk calculation option element 'permitAreaSelection' to be 'visible'
    Then I expect the uk calculation option element 'zoneOfInfluenceInput' to be 'not exist'
    Then I expect the uk calculation options
      | element                       | state      | panel                   |
      | siteLocation                  | visible    | Meteorological settings |
      | metDatasetType                | visible    | Meteorological settings |
      | minMoninObukhovLength         | enabled    | Advanced settings       |
      | surfaceAlbedo                 | enabled    | Advanced settings       |
      | priestleyTaylorParameter      | enabled    | Advanced settings       |
      | plumeDepletionNH3             | exist      | Advanced settings       |
      | plumeDepletionNOX             | exist      | Advanced settings       |
      | complexTerrain                | exist      | Advanced settings       |
      | roadLocalFractionNO2Receptors | visible    | Advanced settings       |
      | roadLocalFractionNO2Points    | visible    | Advanced settings       |

    # Custom assessment
    And I select the uk calculation method 'Custom assessment' and validate hint message is 'not visible'
    Then I expect the uk calculation option element 'permitAreaSelection' to be 'visible'
    Then I expect the uk calculation option element 'zoneOfInfluenceInput' to be 'visible'
    Then I expect the uk calculation options
      | element                       | state      | panel |
      | siteLocation                  | visible    | Meteorological settings |
      | metDatasetType                | visible    | Meteorological settings |
      | minMoninObukhovLength         | enabled    | Advanced settings       |
      | surfaceAlbedo                 | enabled    | Advanced settings       |
      | priestleyTaylorParameter      | enabled    | Advanced settings       |
      | plumeDepletionNH3             | exist      | Advanced settings       |
      | plumeDepletionNOX             | exist      | Advanced settings       |
      | complexTerrain                | exist      | Advanced settings       |
      | roadLocalFractionNO2Receptors | visible    | Advanced settings       |
      | roadLocalFractionNO2Points    | visible    | Advanced settings       |

    # MetLocation
    And I select the uk meteorological site location selection method 'NWP'
    Then I expect the uk calculation options
      | element                       | state      | panel |
      | siteLocation                  | visible    | Meteorological settings |
      | metDatasetType                | visible    | Meteorological settings |
      | minMoninObukhovLength         | enabled    | Advanced settings       |
      | surfaceAlbedo                 | enabled    | Advanced settings       |
      | priestleyTaylorParameter      | enabled    | Advanced settings       |
      | plumeDepletionNH3             | exist      | Advanced settings       |
      | plumeDepletionNOX             | exist      | Advanced settings       |
      | complexTerrain                | exist      | Advanced settings       |
      | roadLocalFractionNO2Receptors | visible    | Advanced settings       |
      | roadLocalFractionNO2Points    | visible    | Advanced settings       |
    And I select the uk meteorological site location selection method 'Observational'
    And I select meteorological data type 'Raw >90% Random'
    Then I expect the uk calculation options
      | element                       | state      | panel |
      | siteLocation                  | visible    | Meteorological settings |
      | metDatasetType                | visible    | Meteorological settings |
      | minMoninObukhovLength         | enabled    | Advanced settings       |
      | surfaceAlbedo                 | enabled    | Advanced settings       |
      | priestleyTaylorParameter      | enabled    | Advanced settings       |
      | plumeDepletionNH3             | exist      | Advanced settings       |
      | plumeDepletionNOX             | exist      | Advanced settings       |
      | complexTerrain                | exist      | Advanced settings       |
      | roadLocalFractionNO2Receptors | visible    | Advanced settings       |
      | roadLocalFractionNO2Points    | visible    | Advanced settings       |
    And I select the uk meteorological site location selection method 'Search'
    Then I expect the uk calculation options
      | element                       | state      | panel |
      | siteLocation                  | visible    | Meteorological settings |
      | metDatasetType                | visible    | Meteorological settings |
      | minMoninObukhovLength         | enabled    | Advanced settings       |
      | surfaceAlbedo                 | enabled    | Advanced settings       |
      | priestleyTaylorParameter      | enabled    | Advanced settings       |
      | plumeDepletionNH3             | exist      | Advanced settings       |
      | plumeDepletionNOX             | exist      | Advanced settings       |
      | complexTerrain                | exist      | Advanced settings       |
      | roadLocalFractionNO2Receptors | visible    | Advanced settings       |
      | roadLocalFractionNO2Points    | visible    | Advanced settings       |
    And I select the uk meteorological site location selection method 'Suggested'
    Then I expect the uk calculation options
      | element                       | state      | panel |
      | siteLocation                  | visible    | Meteorological settings |
      | metDatasetType                | visible    | Meteorological settings |
      | minMoninObukhovLength         | enabled    | Advanced settings       |
      | surfaceAlbedo                 | enabled    | Advanced settings       |
      | priestleyTaylorParameter      | enabled    | Advanced settings       |
      | plumeDepletionNH3             | exist      | Advanced settings       |
      | plumeDepletionNOX             | exist      | Advanced settings       |
      | complexTerrain                | exist      | Advanced settings       |
      | roadLocalFractionNO2Receptors | visible    | Advanced settings       |
      | roadLocalFractionNO2Points    | visible    | Advanced settings       |
