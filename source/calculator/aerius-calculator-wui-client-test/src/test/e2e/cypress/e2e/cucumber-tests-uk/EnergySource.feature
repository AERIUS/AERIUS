#
# Crown copyright
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Energy source

    Scenario: Create new energy source starting at the start page
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Energy' and sector 'Other'
        And I fill location with coordinates 'POINT(171561.58 621179.02)'
        And I choose source height '25' and source diameter '10'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data
        Then I select the source 'Source 1'
        Then the source 'Source 1' with sector group 'Energy' is correctly saved
        And location with coordinates 'X:171561.58 Y:621179.02' is correctly saved
        # Source characteristics for UK are not being displayed / are not specified at this time
        And the total 'generic' emission 'NOx' is '10.0 kg/y'
        And the total 'generic' emission 'NH3' is '20.0 kg/y'

    Scenario: Create new other source starting at the start page
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Other'
        And I fill location with coordinates 'POINT(171561.58 621179.02)'
        And I choose source height '25' and source diameter '10'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data
        Then I select the source 'Source 1'
        Then the source 'Source 1' with sector group 'Other' is correctly saved
        And location with coordinates 'X:171561.58 Y:621179.02' is correctly saved
        # Source characteristics for UK are not being displayed / are not specified at this time
        And the total 'generic' emission 'NOx' is '10.0 kg/y'
        And the total 'generic' emission 'NH3' is '20.0 kg/y'

    @Smoke
    Scenario: Check Efflux type options
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Other'
        And I open panel 'Source characteristics'
        Then The specified listbox should or should not contain the following values
            | listboxName | checkType   | listboxValue |
            | Efflux type | contain     | Velocity     |
            | Efflux type | contain     | Volume       |
            | Efflux type | not.contain | Mass         |
        And The 'Efflux type' listbox contains '2' items
