/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class TimeVaryingProfilePage {
  static testTimeVaryingProfileSettingsOfSectorGroup(dataTable: any) {
    dataTable.hashes().forEach((element: any) => {
      cy.get('[id="sourceListDetailSectorGroup"]').select(element.sectorGroup);
      this.testSectorHasTimeVaryingProfileSettings(element.sector, element.expectedGraphTitle, element.expectedTimeVaryingProfileOptions);
    });
  }

  static testSectorHasTimeVaryingProfileSettings(sector: string, expectedGraphTitle: string, expectedTimeVaryingProfileOptions: string) {
    if (sector != "") {
      cy.get('[id="sourceListDetailSector"]').select(sector);
    }

    cy.get('[id="collapsibleADMSCharacteristics-title"]').click({ force: true });

    // Check select options exist
    let options = expectedTimeVaryingProfileOptions.split(", ");
    options.forEach((option) => {
      cy.contains('[id="standardTimeVaryingProfile"] option', option);
    });

    // Check graph title
    cy.get('[id="timeVaryingProfileGraphTitle"]').contains(expectedGraphTitle);

    // Close ADMSCharacteristics
    cy.get('[id="collapsibleADMSCharacteristics"]').click();
  }

  static checkListOfPredefinedTimeVaryingProfilesExists(dataTable: any) {
    dataTable.hashes().forEach((expectedTimeVaryingProfile: any) => {
      cy.get(".standardProfiles").contains(expectedTimeVaryingProfile.name);
    });
  }

  static checkListOfCustomTimeVaryingProfilesExists(dataTable: any) {
    dataTable.hashes().forEach((expectedCustomTimeVaryingProfile: any) => {
      cy.get(".profiles").contains(expectedCustomTimeVaryingProfile.profileName);
    });
  }

  static selectTimeVaryingProfileFromList(timeVaryingProfileName: any) {
    let testId = "profile-system-profile-" + timeVaryingProfileName.toUpperCase().replaceAll(" ", "_");
    cy.get('[id="' + testId + '"').click();
  }

  static checkDetailPanelForTimeVaryingProfile(name: string, description: string, linesInGraph: string) {
    cy.get('[id="timeVaryingProfileName-THREE_DAY"]').contains(name);
    cy.get('[id="timeVaryingProfileDescription-THREE_DAY"]').contains(description);

    this.assertTimeVaryingProfileGraphHasAmountOfLines(linesInGraph);
  }

  static assertSelectedTimeVaryingProfile(expectedTimeVaryingProfile: string) {
    cy.get('[id="timeVaryingProfileName-THREE_DAY"]').should("have.text", expectedTimeVaryingProfile);
  }

  static assertTimeVaryingProfileGraphHasAmountOfLines(linesInGraph: string) {
    cy.get("time-varying-profile-graph-component").scrollIntoView().should("be.visible")
    cy.get("time-varying-profile-graph-component").shadow()
      .within(() => {
        cy.get(".graph").children("path").should("have.length", linesInGraph);
        cy.get(".legend").children().should("have.length", linesInGraph);
      });
  }

  static assertCustomTimeVaryingProfileGraphHasAmountOfLines(profileType: string, linesInGraph: string) {
    switch (profileType) {
      case "diurnal":
        cy.get('[id="timeVaryingProfileGraph-THREE_DAY"]').scrollIntoView().should("be.visible");
        cy.get('[id="timeVaryingProfileGraph-THREE_DAY"]').shadow()
          .within(() => {
            cy.get(".graph").children("path").should("have.length", linesInGraph);
            cy.get(".legend").children().should("have.length", linesInGraph);
          });
        break;
      case "monthly":
        cy.get('[id="timeVaryingProfileGraph-MONTHLY"]').scrollIntoView().should("be.visible");
        cy.get('[id="timeVaryingProfileGraph-MONTHLY"]').shadow()
          .within(() => {
            cy.get(".graph").children("path").should("have.length", linesInGraph);
            cy.get(".legend").children().should("have.length", linesInGraph);
          });
        break;
      default:
        throw new Error("Unknown profile type: " + profileType);
    }  
  }

  static checkLaunchButtonCustomProfile(profileType: string) {
    switch (profileType) {
      case "diurnal":
        cy.get('[id="calculationLaunchButtonNewProfile"]').should('exist');
        break;
      case "monthly":
        cy.get('[id="calculationLaunchButtonNewMontlyProfile"]').should('exist');
        break;
      default:
        throw new Error("Unknown profile type: " + profileType);
    }
  }

  static addCustomProfileWhenNoCustomProfiles(profileType: string) {
    switch (profileType) {
      case "diurnal":
        cy.get('[id="calculationLaunchButtonNewProfile"]').click();
        break;
      case "monthly":
        cy.get('[id="calculationLaunchButtonNewMontlyProfile"]').click();
        break;
      default:
        throw new Error("Unknown profile type: " + profileType);
    }
  }
  
  static addCustomProfile(profileType: string) {
    switch (profileType) {
      case "diurnal":
        cy.get('[id="time-varying-sourceListButtonNewSource"]').click();
        break;
      case "monthly":
        cy.get('[id="newMonthlyProfileButton"]').click();
        break;
      default:
        throw new Error("Unknown profile type: " + profileType);
    }
  }

  static editCustomProfile() {
    cy.get('[id="time-varying-sourceListButtonEditSource"]').click();
  }

  static copyCustomProfile() {
    cy.get('[id="time-varying-sourceListButtonCopySource"]').click();
  }

  static deleteCustomProfile() {
    cy.get('[id="time-varying-sourceListButtonDeleteSource"]').click();
  }

  static deleteAllCustomProfiles() {
    cy.get('[id="profileListButtonDeleteAllProfiles"]').click();
  }

  static clickOnButton(buttonName: string) {
    switch (buttonName) {
      case "save":
      case "confirm":
        cy.get('[id="buttonSave"]').click();
        break;     
      case "cancel":
        cy.get('[id="buttonCancel"]').click();
        break;
      case "import text input to profile":
        cy.get('[id="timeVaryingProfileImport-MONTHLY"]').click();
        break;
      case "export current profile to text area":
        cy.get('[id="timeVaryingProfileExport-MONTHLY"]').click();
        break;
      default:
        throw new Error("Unknown button: " + buttonName);
    }   
  }

  static clickOnButtonForProfileType(buttonName: string, profileType: string) {
    switch (buttonName) {
      case "Import text input to profile":
        switch (profileType) {
          case "diurnal":
            cy.get('[id="timeVaryingProfileImport-THREE_DAY"]').click();
            break;
          case "monthly":
            cy.get('[id="timeVaryingProfileImport-MONTHLY"]').click();
            break;
          case "import the text input to the profile":
            cy.get('[id="timeVaryingProfileImport-MONTHLY"]').click();
            break;
          default:
            throw new Error("Unknown profile type: " + profileType);
        }
        break;
      case "Export current profile to text area":
        switch (profileType) {
          case "diurnal":
            cy.get('[id="timeVaryingProfileExport-THREE_DAY"]').click();
          break;
          case "monthly":
          cy.get('[id="timeVaryingProfileExport-MONTHLY"]').click();
          break;
          default:
            throw new Error("Unknown profile type: " + profileType);
        }
        break;               
        default:
          throw new Error("Unknown button: " + buttonName);
    }   
  }

  static selectCustomProfile(selectedProfile: string) {
    cy.get('[id^="profile-TimeVaryingProfile"]').contains(selectedProfile).click();
  }

  static checkDetailsDiurnalProfile(dataTable: any) {
    const dataRow = dataTable.hashes()[0];

    cy.get('[id="timeVaryingProfileTitle-THREE_DAY"]').should("have.text", dataRow.profileTitle);
    cy.get('[id="timeVaryingProfileName-THREE_DAY"]').should("have.text", dataRow.profileName);
    cy.get('[id="timeVaryingProfileType-THREE_DAY"]').should("have.text", dataRow.profileType);
    cy.get('[id="diurnalTimeVaryingProfileTotalWeekday"]').should("have.text", dataRow.weekdays);
    cy.get('[id="diurnalTimeVaryingProfileTotalSaturday"]').should("have.text", dataRow.saturdays);
    cy.get('[id="diurnalTimeVaryingProfileTotalSunday"]').should("have.text", dataRow.sundays);
    cy.get('[id="diurnalTimeVaryingProfileTotal"]').should("have.text", dataRow.total);
    cy.get('[id="diurnalTimeVaryingProfileAverageWeekday"]').should("have.text", dataRow.averageWeekdays);
    cy.get('[id="diurnalTimeVaryingProfileAverageSaturday"]').should("have.text", dataRow.averageSaturdays);
    cy.get('[id="diurnalTimeVaryingProfileAverageSunday"]').should("have.text", dataRow.averageSundays);
    cy.get('[id="diurnalTimeVaryingProfileAverage"]').should("have.text", dataRow.average);
  }

  static checkDetailsMonthlyProfile(dataTable: any) {
    const dataRow = dataTable.hashes()[0];
    
    cy.get('[id="timeVaryingProfileTitle-MONTHLY"]').should("have.text", dataRow.profileTitle);
    cy.get('[id="timeVaryingProfileName-MONTHLY"]').should("have.text", dataRow.profileName);
    cy.get('[id="timeVaryingProfileType-MONTHLY"]').should("have.text", dataRow.profileType);      
    cy.get('[id="timeVaryingProfileTotalMonthly"]').should("have.text", dataRow.total);
    cy.get('[id="timeVaryingProfileAverageMonthly"]').should("have.text", dataRow.average);
  }

  static editProfileName(profileName: string) {
    cy.get('[id="timeVaryingProfileLabel"]').clear().type(profileName);
  }

  static editValuesForTimeOfDayForColumn(columnName: string, timeOfDay: string, inputValue: string) {
    let timeOfDayId = parseInt(timeOfDay);
    switch (columnName) {
      case "Weekday avg.":
        let testIdPrefixWeekday = "diurnalTimeVaryingTableInput-0-";
        cy.get('[id="' + testIdPrefixWeekday + timeOfDayId +'"]').clear().type(inputValue);   
        break;
      case "Saturday":
        let testIdPrefixSaturday = "diurnalTimeVaryingTableInput-1-";
        cy.get('[id="' + testIdPrefixSaturday + timeOfDayId +'"]').clear().type(inputValue);
        break;
      case "Sunday":
        let testIdPrefixSunday = "diurnalTimeVaryingTableInput-2-";
        cy.get('[id="' + testIdPrefixSunday + timeOfDayId +'"]').clear().type(inputValue).type('{enter}');
        break;
      default:
        throw new Error("Unknown column: "  + columnName);
    }
  }

  static editValuesForMonthlyProfile(monthInput: string, inputValue: string) {
    let testIdPrefixMonthly = "monthlyTimeVaryingTableInput-";
    let monthNumber = new Date(Date.parse(monthInput + " 1, 2012")).getMonth() + 1;
    cy.get('[id="' + testIdPrefixMonthly + (monthNumber) +'"]').clear().type(inputValue +'{enter}');
  }

  static selectInputMode(inputMode: string) {
    switch (inputMode) {
      case 'Raw input':
        cy.get('[id="tab-timeVaryingProfileInputMode-RAW_CSV"]').click();
        break;
      case 'Table':
        cy.get('[id="tab-timeVaryingProfileInputMode-TABLE"]').click();
        break;
      default:
        throw new Error("Unknown input mode: "  + inputMode); 
    } 
  }

  static pasteRawInput(profileType: string, input: string) {
    switch (profileType) {
      case 'diurnal':
        cy.get('[id="timeVaryingProfileRawInput-THREE_DAY"]').click().clear().type(input);
        break;
      case 'monthly':
        cy.get('[id="timeVaryingProfileRawInput-MONTHLY"]').click().clear().type(input);
        break;
      default:
       throw new Error("Unknown profile type: "  + profileType);            
    }
  }

  static assertButtonIsDisabledForProfileType(buttonName: string, profileType: string) {
    switch (buttonName) {
      case "Import text input to profile":
        switch (profileType) {
          case "diurnal":
            cy.get('[id="timeVaryingProfileImport-THREE_DAY"]').should("be.disabled");
            break;
          case "monthly":
            cy.get('[id="timeVaryingProfileImport-MONTHLY"]').should("be.disabled");
            break;
          default:
            throw new Error("Unknown profile type: " + profileType);       
        }
        break;  
      case "Export current profile to text area":
        switch (profileType) {
          case "diurnal":
            cy.get('[id="timeVaryingProfileExport-THREE_DAY"]').should("be.disabled");
            break;
          case "monthly":
            cy.get('[id="timeVaryingProfileExport-MONTHLY"]').should("be.disabled");
            break;
          default:
            throw new Error("Unknown profile type: " + profileType);       
        }
        break;
      default:
        throw new Error("Unknown button name: " + buttonName);
    }
  }

  static assertButtonIsEnabledForProfileType(buttonName: string, profileType: string) {
    switch (buttonName) {
      case "Import text input to profile":
        switch (profileType) {
          case "diurnal":
            cy.get('[id="timeVaryingProfileImport-THREE_DAY"]').should("be.enabled");
            break;
          case "monthly":
            cy.get('[id="timeVaryingProfileImport-MONTHLY"]').should("be.enabled");
            break;
          default:
            throw new Error("Unknown profile type: " + profileType);     
        }
        break;
      case "Export current profile to text area":
        switch (profileType) {
          case "diurnal":
            cy.get('[id="timeVaryingProfileExport-THREE_DAY"]').should("be.enabled");
            break;
          case "monthly":
            cy.get('[id="timeVaryingProfileExport-MONTHLY"]').should("be.enabled");
            break;
          default:
            throw new Error("Unknown profile type: " + profileType);    
        }
        break;
      default:
        throw new Error("Unknown button name: " + buttonName);  
    }
  }
 
  static assertErrorIsVisibleForProfileType(profileType: string, errorText: string) {
    switch (profileType) {
      case "diurnal":
        cy.get('[id="timeVaryingProfileImportSummary-THREE_DAY"]').should("have.text", errorText);
        break;
      case "monthly":
        cy.get('[id="timeVaryingProfileImportSummary-MONTHLY"]').should("have.text", errorText);
        break;
      default:
        throw new Error("Unknown profile type: " + profileType);    
    }
  }

  static assertNoticeIsVisibleForProfileType(profileType: string, noticeText: string) {
    switch (profileType) {
      case "diurnal":
        cy.get('[id="timeVaryingImportWellFormed-THREE_DAY"]').should("have.text", noticeText);
        break;
      case "monthly":
        cy.get('[id="timeVaryingImportWellFormed-MONTHLY"]').should("have.text", noticeText);
        break;
      default:
        throw new Error("Unknown profile type: " + profileType);    
    }
  }

  static assertErrorRequiredAmountIsVisible(errorText: string) {
    cy.get('[id="inputErrorMessage"]').should("have.text", errorText);
  }

  static assertNoticeRequiredAmountIsVisible(noticeText: string) {
    cy.get('[id="inputWarningMessage"]').should("have.text", noticeText);
  }

  static confirmAndAssertNoticeIsVisible(noticeText: string) {
    cy.get('[id="timeVaryingSaveEncumberedText"]').should("have.text", noticeText);
  }

  static assertExportedTextInTextArea(profileType: string, exportedText: string) {
    switch (profileType) {
      case "diurnal":
        cy.get('[id="timeVaryingProfileRawInput-THREE_DAY"]').should("have.value", exportedText);
        break;
      case "monthly":
      cy.get('[id="timeVaryingProfileRawInput-MONTHLY"]').should("have.value", exportedText);
        break;
      default:
        throw new Error("Unknown profile type: " + profileType);
      }
    }

    static assertGraphLineForType(graphLineType: string) {
      switch (graphLineType) {
        case "weekday":
          cy.get('[id="timeVaryingProfileGraph-THREE_DAY"]').scrollIntoView().should("be.visible");
          cy.get('[id="timeVaryingProfileGraph-THREE_DAY"]').shadow()
            .within(() => {
              cy.get(".graph").children("path").get('[id="time-varying-graph-line-weekday"]').should("exist")
            });
        break;
        case "saturday":
          cy.get('[id="timeVaryingProfileGraph-THREE_DAY"]').scrollIntoView().should("be.visible");
          cy.get('[id="timeVaryingProfileGraph-THREE_DAY"]').shadow()
            .within(() => {
              cy.get(".graph").children("path").get('[id="time-varying-graph-line-saturday"]').should("exist")
            });
        break;
        case "sunday":
          cy.get('[id="timeVaryingProfileGraph-THREE_DAY"]').scrollIntoView().should("be.visible");
          cy.get('[id="timeVaryingProfileGraph-THREE_DAY"]').shadow()
            .within(() => {
              cy.get(".graph").children("path").get('[id="time-varying-graph-line-sunday"]').should("exist")
            });
        break;
        case "monthly":
          cy.get('[id="timeVaryingProfileGraph-MONTHLY"]').scrollIntoView().should("be.visible");
          cy.get('[id="timeVaryingProfileGraph-MONTHLY"]').shadow()
            .within(() => {
              cy.get(".graph").children("path").get('[id="time-varying-graph-line-monthly"]').should("exist")
            });
        break;
        default:
          throw new Error("Unknown graph line type: " + graphLineType);
        }
      }
  }
