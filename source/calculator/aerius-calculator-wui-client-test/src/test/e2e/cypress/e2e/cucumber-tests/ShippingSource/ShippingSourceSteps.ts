/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";

import { ShippingSourcePage } from "./ShippingSourcePage";

Given(
  "I fill the maritime shipping description {string}, type {string}, visits {string}, time unit {string}, average residence time {string} hours and shorepower usage {string} percent",
  (description, shipCategory, shipPerTimeUnit, timeUnit, averageResidenceTime, shorePowerFactor) => {
    ShippingSourcePage.editMaritimeDescriptionCategoryShipPerTimeUnitTimeUnitAverageResidenceTimeShorePowerFactor(
      description,
      shipCategory,
      shipPerTimeUnit,
      timeUnit,
      averageResidenceTime,
      shorePowerFactor
    );
  }
);

Given(
  "I fill the maritime shipping description {string}, type {string}, visits {string} and time unit {string}",
  (description, shipCategory, shipPerTimeUnit, timeUnit) => {
    ShippingSourcePage.editMaritimeDescriptionCategoryShipPerTimeUnitTimeUnit(description, shipCategory, shipPerTimeUnit, timeUnit);
  }
);

Given(
  "I fill the inland shipping description {string}, type {string}, visits {string}, time unit {string}, average residence time {string} hour, shorepower usage {string} and percent laden {string} percent",
  (shipDescription, shipCategory, shipsPerTimeUnit, timeUnit, averageResidenceTime, shorePowerFactor, percentageLaden) => {
    ShippingSourcePage.editInlandDescriptionCategoryShipPerTimeUnitTimeUnitAverageResidenceTimeShorePowerFactorPercentageLaden(
      shipDescription,
      shipCategory,
      shipsPerTimeUnit,
      timeUnit,
      averageResidenceTime,
      shorePowerFactor,
      percentageLaden
    );
  }
);

Given("I fill the inland shipping description {string}, type {string}", (description, shipCategory) => {
  ShippingSourcePage.editInlandDescriptionCategory(description, shipCategory);
});

Given(
  "I fill the movement {string} with ships movements {string}, time unit {string} and percentage laden {string}",
  (movementType, shipMovements, timeUnit, percentageLaden) => {
    ShippingSourcePage.editMovementTypeShipMovementsTimeUnitPercentageLaden(movementType, shipMovements, timeUnit, percentageLaden);
  }
);

Given("I fill waterway {string} and direction {string}", (waterway, direction) => {
  ShippingSourcePage.editWaterwayDirection(waterway, direction);
});

Given("I select waterway {string}", (waterway) => {
  ShippingSourcePage.editWaterway(waterway);
});

Given("the waterway {string} should be selected", (waterwayDirection) => {
  ShippingSourcePage.assertWaterwayDirectionIsSelected(waterwayDirection);
});

Given("I open the shipping subsource panel", () => {
  ShippingSourcePage.openShippingSubsourcesCollapsible();
});

Given("the waterway direction option should be visible", () => {
  ShippingSourcePage.assertWaterwayDirectionOptionIsVisible(true);
});

Given("the waterway direction option should not be visible", () => {
  ShippingSourcePage.assertWaterwayDirectionOptionIsVisible(false);
});

Given(
  "maritime {string} shipping row {string} with description {string}, type {string}, visits {string}, time unit {string}, average residence time {string}, shorepower usage {string}, emission NOx {string} and emission NH3 {string} is correctly created",
  (shippingType, rowNumber, description, shipCategory, shipPerTimeUnit, timeUnit, averageResidenceTime, shorePowerFactor, emissionNOx, emissionNH3) => {
    ShippingSourcePage.maritimeDescriptionCategoryShipPerTimeUnitTimeUnitAverageResidenceTimeShorePowerFactorCreated(
      shippingType,
      rowNumber,
      description,
      shipCategory,
      shipPerTimeUnit,
      timeUnit,
      averageResidenceTime,
      shorePowerFactor,
      emissionNOx,
      emissionNH3
    );
  }
);

Given(
  "maritime {string} shipping row {string} with description {string}, type {string}, visits {string}, time unit {string}, emission NOX {string} and emission NH3 {string} is correctly saved",
  (shippingType, rowNumber, description, shipCategory, shipPerTimeUnit, timeUnit, emissionNOx, emissionNH3) => {
    ShippingSourcePage.maritimeDescriptionCategoryShipPerTimeUnitTimeUnitCreated(
      shippingType,
      rowNumber,
      description,
      shipCategory,
      shipPerTimeUnit,
      timeUnit,
      emissionNOx,
      emissionNH3
    );
  }
);

Given(
  "inland {string} shipping row {string} with description {string}, type {string}, visits {string}, time unit {string}, average residence time {string}, shorepower usage {string}, percent laden {string}, emission NOX {string} and emission NH3 {string} is correctly saved",
  (shippingType, rowNumber, description, shipCategory, shipPerTimeUnit, timeUnit, averageResidenceTime, shorePowerFactor, percentageLaden, emissionNOx, emissionNH3) => {
    ShippingSourcePage.inlandDescriptionCategoryShipPerTimeUnitTimeUnitAverageResidenceTimeShorePowerFactorPercentageLadenCreated(
      shippingType,
      rowNumber,
      description,
      shipCategory,
      shipPerTimeUnit,
      timeUnit,
      averageResidenceTime,
      shorePowerFactor,
      percentageLaden,
      emissionNOx,
      emissionNH3
    );
  }
);

Given("inland {string} shipping row {string} with type {string} is correctly saved", (shippingType, rowNumber, shipCategory) => {
  ShippingSourcePage.inlandDescriptionCategoryCreated(shippingType, rowNumber, shipCategory);
});

Given(
  "movement row {string} with direction {string}, description {string}, ships movements {string}, time unit {string} and percentage laden {string} is correctly saved",
  (rowNumber, movementType, description, shipMovements, timeUnit, percentageLaden, emissionNOx, emissionNH3) => {
    ShippingSourcePage.movementTypeDescriptionShipMovementsTimeUnitPercentageLadenCreated(
      rowNumber,
      movementType,
      description,
      shipMovements,
      timeUnit,
      percentageLaden,
      emissionNOx,
      emissionNH3
    );
  }
);

Given("waterway {string} with direction {string} is correctly saved", (waterway, direction) => {
  ShippingSourcePage.waterwayDirectionCreated(waterway, direction);
});

Given("I fill dock A with {string}", (dockA) => {
  ShippingSourcePage.fillDockA(dockA);
});

Given("I fill dock B with {string}", (dockB) => {
  ShippingSourcePage.fillDockB(dockB);
});

Given("dock A with value {string} is correctly saved", (dockA) => {
  ShippingSourcePage.dockACorrectlySaved(dockA);
});

Given("dock B with value {string} is correctly saved", (dockB) => {
  ShippingSourcePage.dockBCorrectlySaved(dockB);
});

Given("field {string} is not visible", (notVisibleField) => {
  ShippingSourcePage.fieldNotVisible(notVisibleField);
});

Given("maritime docked with description {string}, type {string}, visits {string}, time unit {string}, average residence time {string} and shorepower usage {string} is available for edit",
  (description, shipCategory, shipPerTimeUnit, timeUnit, averageResidenceTime, shorePowerPercentage) => {
    ShippingSourcePage.maritimeDescriptionCategoryShipPerTimeUnitTimeUnitInputFieldsHaveValues(
      description,
      shipCategory,
      shipPerTimeUnit,
      timeUnit,
      averageResidenceTime,
      shorePowerPercentage
      );
});

Given("inland docked with description {string}, type {string}, visits {string}, time unit {string}, average residence time {string}, shorepower usage {string} and percent laden {string} is available for edit",
  (description, shipCategory, shipPerTimeUnit, timeUnit, averageResidenceTime, shorePowerPercentage, ladenPercentage) => {
    ShippingSourcePage.inlandDescriptionCategoryShipPerTimeUnitTimeUnitSorePowerPercentageLadenPercentageInputFieldsHaveValues(
      description,
      shipCategory,
      shipPerTimeUnit,
      timeUnit,
      averageResidenceTime,
      shorePowerPercentage,
      ladenPercentage
    );
});
