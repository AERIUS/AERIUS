#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Import calculation results

    Scenario: Import GML file with project calculation results and validate the results on the map
        Given I login with correct username and password
        And I select file 'AERIUS_project_calculation_with_results.zip' to be imported
        Then the following notification is displayed 'Eén bestand is succesvol geïmporteerd.'
        Then the situation name is 'Situatie 1'
        And the situation type is 'Beoogd'
        And the calculation year is '2024'

        And I navigate to the 'Results' tab

        Then the following notification is displayed 'Rekentaak 1 - AERIUS_project_calculation_with_results is gestart.'
        Then the following notification is displayed 'Rekentaak 1 - AERIUS_project_calculation_with_results is voltooid.'

        Then I select situation with label 'Situatie 1 - Beoogd' and validate the 'Projectberekening' result screen with results for the result 'OwN2000-registratieset'  
        Then I validate nature area chart output for selected nature area with pollutant 'Deposition'
            | name                      | pathsCount |
            | Duinen en Lage Land Texel | 114        |

        Then I select situation with label 'Situatie 1 - Beoogd' and validate the 'Projectberekening' result screen with results for the result 'Relevante hexagonen'
        Then I validate nature area chart output for selected nature area with pollutant 'Deposition'
            | name                      | pathsCount |
            | Noordzeekustzone          | 420        |
 
        Then I select situation with label 'Situatie 1 - Beoogd' and validate the 'Situatieresultaat' result screen with results for the result 'OwN2000-registratieset'
        Then I validate nature area chart output for selected nature area with pollutant 'Deposition'
            | name                          | pathsCount | bbox                                                                                 |
            | Duinen Vlieland               | 88         | BBOX=121558.23390000034%2C577645.9905517712%2C138583.40969999786%2C597985.8023482285 |

        Then I select situation with label 'Situatie 1 - Beoogd' and validate the 'Situatieresultaat' result screen with results for the result 'Relevante hexagonen'
        Then I validate nature area chart output for selected nature area with pollutant 'Deposition'
            | name                          | pathsCount |
            | Duinen en Lage Land Texel     | 934        |

    @Smoke
    Scenario: Import GML file with calculation points results and validate the results
        Given I login with correct username and password
        And I select file 'AERIUS_calculation_points_results.gml' to be imported
        Then the following notification is displayed 'Eén bestand is succesvol geïmporteerd.'
        Then the situation name is 'Scenario 1'
        And the situation type is 'Beoogd'
        And the calculation year is '2024'

        And I navigate to the 'Results' tab
        Then the following notification is displayed 'Rekentaak 1 - AERIUS_calculation_points_results is gestart.'
        Then the following notification is displayed 'Rekentaak 1 - AERIUS_calculation_points_results is voltooid.'

        Then I validate that the situation 'Scenario 1 - Beoogd' has results combinations
            | resultSelectId     | resultSelectLabel                         |
            | resultsResultType  | Situatieresultaat                         |
            | resultsHexagonType | Eigen rekenpunten                         |
            | resultsResultType  | Projectberekening                         |
            | resultsHexagonType | Eigen rekenpunten                         |

        Then I select situation with label 'Scenario 1 - Beoogd' and validate the 'Projectberekening' result screen with results for the result 'Eigen rekenpunten'
            | elementId                      | expectedResult |
            | countCalculationPoints         | 15             |
            | countCalculationPointsIncrease | 15             |
            | countCalculationPointsDecrease | 0              |
            | maxIncrease                    | 516,00         |
            | maxDecrease                    | 0,00           |
        Then I validate the calculation point results
            | id | name                    | expectedResult |
            | 3  | Veluwe Lg14 (<1 km)     | 516,00         |
            | 5  | Veluwe ZGH9120 (<1 km)  | 43,68          |
            | 6  | Veluwe H9190 (<1 km)    | 15,77          |
            | 4  | Veluwe ZGLg14 (<1 km)   | 14,69          |
            | 7  | Veluwe H9120 (<1 km)    | 6,75           |
            | 8  | Veluwe ZGL4030 (<1 km)  | 4,13           |
            | 2  | Veluwe L4030 (<1 km)    | 3,40           |
            | 9  | Veluwe H6230dka (<1 km) | 2,08           |
            | 15 | Veluwe H4010A (2 km)    | 1,14           |
            | 16 | Veluwe H3130 (2 km)     | 1,04           |
            | 13 | Veluwe ZGLg13 (2 km)    | 0,60           |
            | 10 | Veluwe Lg09 (2 km)      | 0,54           |
            | 12 | Veluwe Lg13 (2 km)      | 0,51           |
            | 11 | Veluwe H2310 (2 km)     | 0,50           |
            | 14 | Veluwe H5130 (2 km)     | 0,47           |

    @Smoke
    Scenario: Import GML file with project calculation results and validate the results
        Given I login with correct username and password
        And I select file 'AERIUS_project_calculation_with_results.zip' to be imported
        Then the following notification is displayed 'Eén bestand is succesvol geïmporteerd.'
        Then the situation name is 'Situatie 1'
        And the situation type is 'Beoogd'
        And the calculation year is '2024'

        And I navigate to the 'Results' tab
        Then the following notification is displayed 'Rekentaak 1 - AERIUS_project_calculation_with_results is gestart.'
        Then the following notification is displayed 'Rekentaak 1 - AERIUS_project_calculation_with_results is voltooid.'

        Then I validate that the situation 'Situatie 1 - Beoogd' has results combinations
            | resultSelectId     | resultSelectLabel                            |
            | resultsResultType  | Situatieresultaat                            |
            | resultsHexagonType | OwN2000-registratieset                       |
            | resultsHexagonType | Relevante hexagonen                          |
            | resultsResultType  | Projectberekening                            |
            | resultsHexagonType | OwN2000-registratieset                       |
            | resultsHexagonType | OwN2000-registratieset(zonder randhexagonen) |
            | resultsHexagonType | OwN2000-registratieset(alleen randhexagonen) |
            | resultsHexagonType | Relevante hexagonen                          |
            | resultsHexagonType | Relevante hexagonen(zonder randhexagonen)    |
            | resultsHexagonType | Relevante hexagonen(alleen randhexagonen)    |

        Then I select situation with label 'Situatie 6 - Saldering' and validate the 'Situatieresultaat' result screen with results for the result 'OwN2000-registratieset'
            | elementId                      | expectedResult |
            | sumCartographicSurface         | 560,76         |
            | maxTotal                       | 1.838,70       |
            | maxContribution                | 0,28           |
        And I check if the deposition tab has 4 results
        And I check if the markers tab has 4 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the habitat tab has 4 results
        Then I select situation with label 'Situatie 6 - Saldering' and validate the 'Situatieresultaat' result screen with results for the result 'Relevante hexagonen'
            | elementId                      | expectedResult |
            | sumCartographicSurface         | 3.512,84       |
            | maxTotal                       | 1.838,70       |
            | maxContribution                | 0,28           |
        And I check if the deposition tab has 5 results
        And I check if the markers tab has 5 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the habitat tab has 5 results


        Then I select situation with label 'Situatie 8 - Referentie' and validate the 'Situatieresultaat' result screen with results for the result 'Relevante hexagonen'
            | elementId                      | expectedResult |
            | sumCartographicSurface         | 3.512,84       |
            | maxTotal                       | 1.838,69       |
            | maxContribution                | 0,27           |
        And I check if the deposition tab has 5 results
        And I check if the markers tab has 5 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the habitat tab has 5 results
        Then I select situation with label 'Situatie 8 - Referentie' and validate the 'Situatieresultaat' result screen with results for the result 'OwN2000-registratieset'
            | elementId                      | expectedResult |
            | sumCartographicSurface         | 560,76         |
            | maxTotal                       | 1.838,69       |
            | maxContribution                | 0,27           |
        And I check if the deposition tab has 4 results
        And I check if the markers tab has 4 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the habitat tab has 4 results

        Then I select situation with label 'Situatie 1 - Beoogd' and validate the 'Projectberekening' result screen with results for the result 'OwN2000-registratieset'
            | elementId                      | expectedResult |
            | sumCartographicSurface         | 457,22         |
            | maxTotal                       | 1.838,61       |
            | sumCartographicSurfaceIncrease | 457,22         |
            | maxIncrease                    | 0,06           |
            | sumCartographicSurfaceDecrease | 0,00           |
            | maxDecrease                    | -              |
        And I check if the deposition tab has 3 results
        And I check if the markers tab has 3 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the habitat tab has 3 results        
        And I check if omitted areas contains 1 results

        Then I select situation with label 'Situatie 1 - Beoogd' and validate the 'Projectberekening' result screen with results for the result 'Relevante hexagonen'
            | elementId                      | expectedResult |
            | sumCartographicSurface         | 2.791,25       |
            | maxTotal                       | 1.838,61       |
            | sumCartographicSurfaceIncrease | 2.791,25       |
            | maxIncrease                    | 0,06           |
            | sumCartographicSurfaceDecrease | 0,00           |
            | maxDecrease                    | -              |
        And I check if the deposition tab has 4 results
        And I check if the markers tab has 4 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the habitat tab has 4 results
        And I check if omitted areas contains 1 results

        Then I select situation with label 'Situatie 1 - Beoogd' and validate the 'Situatieresultaat' result screen with results for the result 'Relevante hexagonen'
            | elementId                      | expectedResult |
            | sumCartographicSurface         | 3.512,84       |
            | maxTotal                       | 1.838,82       |
            | maxContribution                | 0,60           |
        And I check if the deposition tab has 5 results
        And I check if the markers tab has 5 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the habitat tab has 5 results

        Then I select situation with label 'Situatie 1 - Beoogd' and validate the 'Situatieresultaat' result screen with results for the result 'OwN2000-registratieset'
            | elementId                      | expectedResult |
            | sumCartographicSurface         | 560,76         |
            | maxTotal                       | 1.838,82       |
            | maxContribution                | 0,60           |
        And I check if the deposition tab has 4 results
        And I check if the markers tab has 4 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the habitat tab has 4 results

    @Smoke
    Scenario: Import GML file with project calculation results and validate the results including recovery results
        Given I login with correct username and password
        And I select file 'AERIUS_project_calculation_recovery_with_results.zip' to be imported
        Then the following notification is displayed 'Eén bestand is succesvol geïmporteerd.'
        Then the situation name is 'Situatie 1'
        And the situation type is 'Beoogd'
        And the calculation year is '2024'

        And I navigate to the 'Results' tab
        Then the following notification is displayed 'Rekentaak 1 - AERIUS_project_calculation_recovery_with_results is gestart.'
        Then the following notification is displayed 'Rekentaak 1 - AERIUS_project_calculation_recovery_with_results is voltooid.'

        Then I select situation with label 'Situatie 1 - Beoogd' and validate the 'Projectberekening' result screen with results for the result 'Hexagonen met hersteldoelen'
        Then I validate nature area chart output for selected nature area with pollutant 'Deposition'
            | name       | pathsCount |
            | Binnenveld | 40         |
 
        Then I validate that the situation 'Situatie 1 - Beoogd' has results combinations
            | resultSelectId     | resultSelectLabel                                 |
            | resultsResultType  | Situatieresultaat                                 |
            | resultsHexagonType | OwN2000-registratieset                            |
            | resultsHexagonType | Relevante hexagonen                               |
            | resultsHexagonType | Hexagonen met hersteldoelen                       |
            | resultsResultType  | Projectberekening                                 |
            | resultsHexagonType | OwN2000-registratieset                            |
            | resultsHexagonType | OwN2000-registratieset(zonder randhexagonen)      |
            | resultsHexagonType | OwN2000-registratieset(alleen randhexagonen)      |
            | resultsHexagonType | Relevante hexagonen                               |
            | resultsHexagonType | Relevante hexagonen(zonder randhexagonen)         |
            | resultsHexagonType | Relevante hexagonen(alleen randhexagonen)         |
            | resultsHexagonType | Hexagonen met hersteldoelen                       |
            | resultsHexagonType | Hexagonen met hersteldoelen(zonder randhexagonen) |
            | resultsHexagonType | Hexagonen met hersteldoelen(alleen randhexagonen) |
        Then I validate that the situation 'Situatie 6 - Saldering' has results combinations
            | resultSelectId     | resultSelectLabel                                 |
            | resultsResultType  | Situatieresultaat                                 |
            | resultsHexagonType | OwN2000-registratieset                            |
            | resultsHexagonType | Relevante hexagonen                               |
            | resultsHexagonType | Hexagonen met hersteldoelen                       |
        Then I validate that the situation 'Situatie 8 - Referentie' has results combinations
            | resultSelectId     | resultSelectLabel                                 |
            | resultsResultType  | Situatieresultaat                                 |
            | resultsHexagonType | OwN2000-registratieset                            |
            | resultsHexagonType | Relevante hexagonen                               |
            | resultsHexagonType | Hexagonen met hersteldoelen                       |

        Then I select situation with label 'Situatie 6 - Saldering' and validate the 'Situatieresultaat' result screen with results for the result 'OwN2000-registratieset'
            | elementId                      | expectedResult |
            | sumCartographicSurface         | 27.620,51      |
            | maxTotal                       | 6.545,96       |
            | maxContribution                | 1,58           |
        And I check if the deposition tab has 4 results
        And I check if the markers tab has 4 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the habitat tab has 4 results
        Then I select situation with label 'Situatie 6 - Saldering' and validate the 'Situatieresultaat' result screen with results for the result 'Relevante hexagonen'
            | elementId                      | expectedResult |
            | sumCartographicSurface         | 27.859,82      |
            | maxTotal                       | 6.545,96       |
            | maxContribution                | 1,58           |
        And I check if the deposition tab has 4 results
        And I check if the markers tab has 4 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the habitat tab has 4 results

        Then I select situation with label 'Situatie 6 - Saldering' and validate the 'Situatieresultaat' result screen with results for the result 'Hexagonen met hersteldoelen'
            | elementId                      | expectedResult |
            | countReceptors                 | 20             |
            | maxTotal                       | 1.585,81       |
            | maxContribution                | 1,54           |
        And I check if the deposition tab has 1 results
        And I check if the markers tab has 1 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the extra assessment habitat tab has 1 results

        Then I select situation with label 'Situatie 8 - Referentie' and validate the 'Situatieresultaat' result screen with results for the result 'Relevante hexagonen'
            | elementId                      | expectedResult |
            | sumCartographicSurface         | 27.859,82      |
            | maxTotal                       | 6.545,95       |
            | maxContribution                | 1,58           |
        And I check if the deposition tab has 4 results
        And I check if the markers tab has 4 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the habitat tab has 4 results
        
        Then I select situation with label 'Situatie 8 - Referentie' and validate the 'Situatieresultaat' result screen with results for the result 'OwN2000-registratieset'
            | elementId                      | expectedResult |
            | sumCartographicSurface         | 27.620,51      |
            | maxTotal                       | 6.545,95       |
            | maxContribution                | 1,58           |
        And I check if the deposition tab has 4 results
        And I check if the markers tab has 4 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the habitat tab has 4 results        
        
        Then I select situation with label 'Situatie 8 - Referentie' and validate the 'Situatieresultaat' result screen with results for the result 'Hexagonen met hersteldoelen'
            | elementId                      | expectedResult |
            | countReceptors                 | 20             |
            | maxTotal                       | 1.585,75       |
            | maxContribution                | 1,49           |
        And I check if the deposition tab has 1 results
        And I check if the markers tab has 1 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the extra assessment habitat tab has 1 results

        Then I select situation with label 'Situatie 1 - Beoogd' and validate the 'Projectberekening' result screen with results for the result 'OwN2000-registratieset'
            | elementId                      | expectedResult |
            | sumCartographicSurface         | 27.606,45      |
            | maxTotal                       | 6.545,87       |
            | sumCartographicSurfaceIncrease | 27.606,19      |
            | maxIncrease                    | 0,20           |
            | sumCartographicSurfaceDecrease | 0,26           |
            | maxDecrease                    | 0,11           |
        And I check if the deposition tab has 4 results
        And I check if the markers tab has 4 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the habitat tab has 4 results        
        And I check if omitted areas contains 0 results

        Then I select situation with label 'Situatie 1 - Beoogd' and validate the 'Projectberekening' result screen with results for the result 'Relevante hexagonen'
            | elementId                      | expectedResult |
            | sumCartographicSurface         | 27.777,49      |
            | maxTotal                       | 6.545,87       |
            | sumCartographicSurfaceIncrease | 27.777,23      |
            | maxIncrease                    | 0,20           |
            | sumCartographicSurfaceDecrease | 0,26           |
            | maxDecrease                    | 0,11           |
        And I check if the deposition tab has 4 results
        And I check if the markers tab has 4 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the habitat tab has 4 results
        And I check if omitted areas contains 0 results

        Then I select situation with label 'Situatie 1 - Beoogd' and validate the 'Situatieresultaat' result screen with results for the result 'Relevante hexagonen'
            | elementId                      | expectedResult |
            | sumCartographicSurface         | 27.859,82      |
            | maxTotal                       | 6.546,08       |
            | maxContribution                | 3,24           |
        And I check if the deposition tab has 4 results
        And I check if the markers tab has 4 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the habitat tab has 4 results

        Then I select situation with label 'Situatie 1 - Beoogd' and validate the 'Situatieresultaat' result screen with results for the result 'OwN2000-registratieset'
            | elementId                      | expectedResult |
            | sumCartographicSurface         | 27.620,51      |
            | maxTotal                       | 6.546,08       |
            | maxContribution                | 3,24           |
        And I check if the deposition tab has 4 results
        And I check if the markers tab has 4 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the habitat tab has 4 results

        Then I select situation with label 'Situatie 1 - Beoogd' and validate the 'Situatieresultaat' result screen with results for the result 'Hexagonen met hersteldoelen'
            | elementId                      | expectedResult |
            | countReceptors                 | 20             |
            | maxTotal                       | 1.587,26       |
            | maxContribution                | 3,23           |
        And I check if the deposition tab has 1 results
        And I check if the markers tab has 1 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the extra assessment habitat tab has 1 results
        
        Then I select situation with label 'Situatie 1 - Beoogd' and validate the 'Projectberekening' result screen with results for the result 'Hexagonen met hersteldoelen'
            | elementId                      | expectedResult |
            | countReceptors                 | 20             |
            | maxTotal                       | 1.584,69       |
            | countReceptorsIncrease         | 20             |
            | maxIncrease                    | 0,21           |
            | countReceptorsDecrease         | 0              |
            | maxDecrease                    | -              |
        And I check if the deposition tab has 1 results
        And I check if the markers tab has 1 results and the text 'Hoogste totale depositie (mol N/ha/jr)'
        And I check if the extra assessment habitat tab has 1 results
