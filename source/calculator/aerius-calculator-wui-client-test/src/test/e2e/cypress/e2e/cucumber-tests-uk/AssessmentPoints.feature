#
# Crown copyright
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Assessment points

    @Smoke
    Scenario: Create a new assessment point in a different projection
        Given I login with correct username and password
        And I create a new source from the start page
        And I navigate to the 'Assessment points' tab
        And I create the first assessment point
        And I enter the name 'My Custom Assessment point'
        And I enter the coordinates '29903; POINT(107967.88 523921.47)'
        Then the convert notice should be visible
        And I select the converted WKT
        Then the entered coordinates should be 'POINT(-66132.97 698476.31)'
        And I enter the coordinates '29903; POINT(108967.88 523921.47)'
        And I save the assessment point
        Then the overview list has 1 assessment points
        Then the assessment point 'My Custom Assessment point' exists
        Then I select the assessment point 'My Custom Assessment point'
        And I update this assessment point
        Then the entered coordinates should be 'POINT(-65134.61 698389.27)'

    Scenario: Try creating a new assessment point with an unsupported projection
        Given I login with correct username and password
        And I create a new source from the start page
        And I navigate to the 'Assessment points' tab
        And I create the first assessment point
        And I enter the name 'My Custom Assessment point'
        And I enter the coordinates '1; POINT(107967.88 523921.47)'
        And I save the assessment point
        Then an error should be shown
        Then I should still be in the new assessment point screen

    Scenario: Create new assessment point and change category
        Given I login with correct username and password
        And I create a new source from the start page
        And I navigate to the 'Assessment points' tab
        And I select assessment point category 'ECOLOGY'
        And I select assessment point category 'HUMAN_HEALTH'
        And I select assessment point category 'MONITORING'
        And I select assessment point category 'null'

        And I create the first assessment point
        And I enter the name 'My Custom Calculation point'        
        And I enter the coordinates '29903; POINT(107967.88 523921.47)'
        And I open assessment point characteristics
        And I select assessment point category 'MONITORING'
        And I enter assessment point road height '-1'
        Then the following 'assessmentpoint road height' warning is visible 'Height must have a value between 0 and 3,000 (inclusive) '
        And I enter assessment point road height '3001.01'
        Then the following 'assessmentpoint road height' warning is visible 'Height must have a value between 0 and 3,000 (inclusive) '
        And I enter assessment point road height '3000'
        And I enter assessment point custom primary fraction '-1'
        Then the following 'asessmentpoint custom primary fraction' warning is visible 'Custom primary NO₂ fraction (fNO₂) must have a value between 0 and 1 (inclusive) '
        And I enter assessment point custom primary fraction '1.01'
        Then the following 'asessmentpoint custom primary fraction' warning is visible 'Custom primary NO₂ fraction (fNO₂) must have a value between 0 and 1 (inclusive) '
        And I enter assessment point custom primary fraction '1'
        And I save the assessment point

        Then the overview list has 1 assessment points
        Then I select the assessment point 'My Custom Calculation point'
        And I update this assessment point
        And I open assessment point characteristics
        Then the entered coordinates should be 'POINT(-66132.97 698476.31)'
        And assessment point category 'MONITORING', road height '3000' and custom primary fraction '1' are correctly saved
        And I save the assessment point

        And I update this assessment point
        And I open assessment point characteristics
        And I select assessment point category 'ECOLOGY'
        And I save the assessment point
        And I update this assessment point
        And I open assessment point characteristics
        # default value for Ecology road height 1
        And assessment point category 'ECOLOGY', road height '1' and custom primary fraction '1' are correctly saved
        And I save the assessment point

        And I update this assessment point
        And I open assessment point characteristics
        And I select assessment point category 'HUMAN_HEALTH'
        And I save the assessment point
        And I update this assessment point
        And I open assessment point characteristics
        # default value for human health road height 1.5
        And assessment point category 'HUMAN_HEALTH', road height '1.5' and custom primary fraction '1' are correctly saved
        And I save the assessment point

    # extra test to test boundary values: '0', null and values with digits (other values are already tested)
    Scenario: Create assessment point with different values
        Given I login with correct username and password
        And I create a new source from the start page
        And I navigate to the 'Assessment points' tab
        And I create the first assessment point
        And I enter the name 'My Custom Calculation point'        
        And I enter the coordinates 'POINT(309751.91 658702.93)'
        And I open assessment point characteristics
        And I select assessment point category 'MONITORING'
        # boundary value: '0'
        And I enter assessment point road height '0'
        And I enter assessment point custom primary fraction '0'
        And I save the assessment point
        When I select the assessment point 'My Custom Calculation point'
        And I update this assessment point
        Then the entered coordinates should be 'POINT(309751.91 658702.93)'
        And I open assessment point characteristics
        And assessment point category 'MONITORING', road height '0' and custom primary fraction '0' are correctly saved
        # value: 'null'
        When I enter assessment point road height '{backspace}'
        And I enter assessment point custom primary fraction '{backspace}'
        And I save the assessment point
        And I update this assessment point
        Then the entered coordinates should be 'POINT(309751.91 658702.93)'
        And I open assessment point characteristics
        And assessment point category 'MONITORING', road height '1.5' and custom primary fraction '' are correctly saved
        And I save the assessment point
        # value: number with digits         
        When I update this assessment point
        And I open assessment point characteristics        
        And I enter assessment point road height '29.99'
        And I enter assessment point custom primary fraction '0.99'
        And I save the assessment point
        And I update this assessment point
        Then the entered coordinates should be 'POINT(309751.91 658702.93)'
        And I open assessment point characteristics
        And assessment point category 'MONITORING', road height '29.99' and custom primary fraction '0.99' are correctly saved
        And I save the assessment point

    @Smoke
    Scenario: Validate the error handling of habitats of assessment points
        Given I login with correct username and password
        And I create a new source from the start page
        And I navigate to the 'Assessment points' tab
        And I create the first assessment point
        And I enter the coordinates '29903; POINT(107967.88 523921.47)'
        And I open assessment point habitats

        # Assessment points should have no habitats by default
        Then I assert that the habitats list does not exist
        And the empty habitats text and new habitat button should be visible

        # Add a habitat and validate the default values
        When I add the first habitat
        Then the habitat list exists and has 1 habitats
        And the selected habitat has the following data
        | name    | CLNOx | CLNOxEnabled | CLNH3 | CLNH3Enabled | CL | CLEnabled |
        | Habitat | 30    | true         | 3     | true         | 25 | true      |

        # Excluding all the CL's
        When I rename the habitat to 'Mijn habitat'
        And I disable the CLNOx value
        And I disable the CLNH3 value
        And I disable the CL value
        Then the selected habitat has the following data
        | name         | CLNOx | CLNOxEnabled | CLNH3 | CLNH3Enabled | CL | CLEnabled |
        | Mijn habitat | -     | false        | -     | false        | -  | false     |

        # Including and changes the values of the CL's
        When I enable the CLNOx value
        And I change the CLNOx value to '40.5'
        And I enable the CLNH3 value
        And I change the CLNH3 value to '4.3'
        And I enable the CL value
        And I change the CL value to '30.2'
        Then the selected habitat has the following data
        | name         | CLNOx | CLNOxEnabled | CLNH3 | CLNH3Enabled | CL   | CLEnabled |
        | Mijn habitat | 40.5  | true         | 4.3   | true         | 30.2 | true      |

        # Validating the error handling of the CL inputs
        When I change the CLNOx value to '-0.0001'
        And I change the CLNH3 value to '-0.0001'
        And I change the CL value to '-0.0001'
        And I save the assessment point
        Then the following 'CLNOx' error is visible 'Critical level NOₓ must have a value greater than or equal to 0'
        And the following 'CLNH3' error is visible 'Critical level NH₃ must have a value greater than or equal to 0'
        And the following 'CL' error is visible 'Critical load must have a value greater than or equal to 0'
        And the assigment point error container should exist

        When I change the CLNOx value to '0.0001'
        And I change the CLNH3 value to '0.0001'
        And I change the CL value to '0.0001'
        Then the 'CLNOx' error should not exist
        And the 'CLNH3' error should not exist
        And the 'CL' error should not exist
        And the assigment point error container should not exist

        # Testing adding, duplicating and deleting habitats
        When I add a habitat
        Then the habitat list exists and has 2 habitats

        When I duplicate the selected habitat
        Then the habitat list exists and has 3 habitats

        When I duplicate the selected habitat
        Then the habitat list exists and has 4 habitats

        When I delete the selected habitat
        Then the habitat list exists and has 3 habitats

        # Test whether this assessment point can be saved
        When I save the assessment point
        Then calculation point 'Assessment point 1' is correctly saved
