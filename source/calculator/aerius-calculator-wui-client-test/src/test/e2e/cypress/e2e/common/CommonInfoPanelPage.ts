/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { CommonSettings } from "./CommonSettings";

export class CommonInfoPanelPage {
  static clickOnMapAndAssertInfoMarker(ariaLabel: string) {
    cy.get('[aria-label="' + ariaLabel + '"]').click();
    // Trigger Openlayer native event
    cy.get('.ol-viewport')
      .trigger('pointerdown', {
        pointerId: 1
      });
    cy.wait(1000);
    // Y-coordinate for click in chart difference in product UK
    let ypos = Cypress.env("TEST_PROFILE") === "UK" ? 490 : 525;
    cy.get('.ol-viewport').click(690, ypos, { force: true });
    cy.get(".infoContainer > .loading").should("not.exist", { timeout: CommonSettings.getElementLongTimeOut });
  }

  static validateReceptorLocation(receptorId: string, location: string) {
    cy.get('[id="infoPanelReceptorId"]')
      .within(() => {
        cy.get('[id="infoRowValue"]').contains(receptorId);
      });
    cy.get('[id="infoPanelCoordinate"]')
      .within(() => {
        cy.get('[id="infoRowValue"]').contains(location);
      });
  }

  static validateGenericLocationValues(dataTable: any) {
    dataTable.hashes().forEach((elemenId: any) => {
      this.validateReceptorLocation(elemenId.infoPanelReceptorId, elemenId.infoPanelCoordinate);
      if (elemenId.infoPanelRelevantHexagon != undefined) {
        cy.get('[id="infoPanelRelevantHexagon"]')
          .within(() => {
            cy.get('[id="infoRowValue"]').contains(elemenId.infoPanelRelevantHexagon);
          });
      }
      if (elemenId.infoPanelExceedingHexagon) {
        cy.get('[id="infoPanelExceedingHexagon"]')
          .within(() => {
            cy.get('[id="infoRowValue"]').contains(elemenId.infoPanelExceedingHexagon);
          });
      }
      if (elemenId.infoPanelAboveCriticalLevel) {
        cy.get('[id="infoPanelAboveCriticalLevel"]')
        .within(() => {
          cy.get('[id="infoRowValue"]').contains(elemenId.infoPanelAboveCriticalLevel);
          });
      }
      if (elemenId.infoPanelExtraAssessment.infoPanelExtraAssessment) {
        cy.get('[id="infoPanelExtraAssessment"]')
          .within(() => {
            cy.get('[id="infoRowValue"]').contains(elemenId.infoPanelExtraAssessment);
          });
      }
    }  
  }

  static validateBackgroundValue(substance: string, value: string, row: number) {
    cy.get('[aria-controls="collapsibleResults-content"]', { timeout: CommonSettings.getElementLongTimeOut }).click();
    cy.get('[id="infoPanelBackground"] > .infoRow')
      .eq(row)
      .within(() => {
        cy.get('[id="infoRowLabel"]').should("contain.text", substance);
        cy.get('[id="infoRowValue"]').should("contain.text", value);
      });
    cy.get('[aria-controls="collapsibleResults-content"]').click();
  }

  static validateBackgroundValues(dataTable: any) {
    dataTable.hashes().forEach((background: any) => {
      this.validateBackgroundValue(background.pollutant, background.value, background.row);
    });    
  }

  static validateResults(title: string, dataTable: any) {
    let element = '[id="' + title.toLowerCase().replaceAll(" ", "-") + '-title"]' ;
    cy.get(element).click().contains(title);
    dataTable.hashes().forEach((results: any) => {
      cy.get('[id="' + results.element + '"]').contains(results.title);
      cy.get('[id="' + results.element + '"]').within( () => {
        cy.get('[id="infoRowLabel"]').eq(1).should("contain.text", results.infoRowLabel1);
        cy.get('[id="infoRowValue"]').eq(1).should("contain.text", results.infoRowValue1);
        
        if (results.infoRowLabel2 != undefined) {
          cy.get('[id="infoRowLabel"]').eq(2).should("contain.text", results.infoRowLabel2);
          cy.get('[id="infoRowValue"]').eq(2).should("contain.text", results.infoRowValue2);
        }
        if (results.infoRowLabel3 != undefined) {
          cy.get('[id="infoRowLabel"]').eq(3).should("contain.text", results.infoRowLabel3);
          cy.get('[id="infoRowValue"]').eq(3).should("contain.text", results.infoRowValue3);
        }
        });
    }
  }
}
