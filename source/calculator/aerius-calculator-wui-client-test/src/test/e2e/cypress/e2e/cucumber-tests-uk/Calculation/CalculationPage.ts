/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { CommonPage_UK } from "../../common/CommonPage_UK";

export class CalculationPage {

  static setCalculationOptionsTo(calculationOptionsMode: string) {
    cy.get('[id="tab-calculationOptionMode-' + calculationOptionsMode + '"]').click();
  }

  static selectFNO2Option(dataTable: any) {
    dataTable.hashes().forEach((data: any) => {
      let id = "roadLocalFractionNO2" + data.type;
      cy.get('[id="' + id + '"]').select(data.option);
      this.assertGenericElement("roadLocalFractionNO2", data.state);
    });
  }

  static assertElements(dataTable: any) {
    dataTable.hashes().forEach((element: any) => {
      CommonPage_UK.openCalculationJobPanel(element.panel);
      this.assertElement(element.element, element.state);
    });
  }

  static assertElement(element: string, state: string) {
    if (element == "meteorological years") {
      this.assertMeteorologicalYearsDisabled();
    } else {
      this.assertGenericElement(element, state);
    }
  }

  static assertGenericElement(element: string, state: string) {
    switch (state) {
      case "disabled":
        this.assertElementShouldBe(element, "be.disabled");
        break;
      case "enabled":
        this.assertElementShouldBe(element, "not.be.disabled");
        break;
      case "not exist":
        this.assertElementShouldBe(element, "not.exist");
        break;
      case "exist":
        this.assertElementShouldBe(element, "exist");
        break;
      case "visible":
        this.assertElementShouldBe(element, "be.visible");
        break;
      default:
        throw new Error("Unknown state for element: " + state);
    }
  }

  static assertElementShouldBe(elementId: string, state: string) {
    if (state == "not.exist") {
      cy.get('[id="' + elementId + '"]').should(state);
    } else {
      cy.get('[id="' + elementId + '"]').first().scrollIntoView().should(state);
    }
  }

  static assertMeteorologicalYearsDisabled() {
    cy.get("label")
      .contains("Meteorological years")
      .parent()
      .parent()
      .get(".checkboxContent")
      .find("label>input")
      .each((input) => {
        cy.wrap(input).should("be.disabled");
      });
  }
}
