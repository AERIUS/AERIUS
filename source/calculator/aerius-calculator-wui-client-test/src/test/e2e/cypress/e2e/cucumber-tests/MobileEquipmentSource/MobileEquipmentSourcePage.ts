/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class MobileEquipmentSourcePage {
  static inputDescriptionStageClassFuelUsageOperationHoursAdBlue(description: string, stageClass: string, fuelUsage: string, operationHours: string, adBlue: string) {
    cy.get('[id="collapsibleSubSources"] > .line > .title').click({
      force: true
    });
    cy.get('[id="sourceListButtonNewSource"] > .icon').click({ force: true });

    cy.get('[id="emissionSourceOffRoadMobileStandardDescription"]').clear({ force: true }).type(description);
    cy.get('[id="offRoadMobileCategory"]').select(stageClass, { force: true });

    if (fuelUsage.length > 0) {
      cy.get('[id="emissionSourceOffRoadMobileStandardLiterFuel"]').clear({ force: true }).type(fuelUsage);
    } else {
      cy.get('[id="emissionSourceOffRoadMobileStandardLiterFuel"]').should("not.exist");
    }
    if (operationHours.length > 0) {
      cy.get('[id="emissionSourceOffRoadMobileStandardOperatingHours"]').clear({ force: true }).type(operationHours);
    } else {
      cy.get('[id="emissionSourceOffRoadMobileStandardOperatingHours"]').should("not.exist");
    }
    if (adBlue.length > 0) {
      cy.get('[id="emissionSourceOffRoadMobileStandardLiterAdBlue"]').clear({ force: true }).type(adBlue);
    } else {
      cy.get('[id="emissionSourceOffRoadMobileStandardLiterAdBlue"]').should("not.exist");
    }
  }

  static editInputDescriptionStageClassFuelUsageOperationHoursAdBlue(description: string, stageClass: string, fuelUsage: string, operationHours: string, adBlue: string) {
    cy.get('[id="emissionSourceOffRoadMobileStandardDescription"]').clear({ force: true }).type(description);
    cy.get('[id="offRoadMobileCategory"]').select(stageClass, { force: true });

    if (fuelUsage.length > 0) {
      cy.get('[id="emissionSourceOffRoadMobileStandardLiterFuel"]').clear({ force: true }).type(fuelUsage);
    } else {
      cy.get('[id="emissionSourceOffRoadMobileStandardLiterFuel"]').should("not.exist");
    }
    if (operationHours.length > 0) {
      cy.get('[id="emissionSourceOffRoadMobileStandardOperatingHours"]').clear({ force: true }).type(operationHours);
    } else {
      cy.get('[id="emissionSourceOffRoadMobileStandardOperatingHours"]').should("not.exist");
    }
    if (adBlue.length > 0) {
      cy.get('[id="emissionSourceOffRoadMobileStandardLiterAdBlue"]').clear({ force: true }).type(adBlue);
    } else {
      cy.get('[id="emissionSourceOffRoadMobileStandardLiterAdBlue"]').should("not.exist");
    }
  }

  static descriptionStageClassFuelUsageOperationHoursAdBlueCreated(dataTable: any) {
    let index = 0
    dataTable.hashes().forEach((mobileEquipmentSource: any, index: number, description: string, stageClass: string, fuelUsage: string, operationHours: string, adBlue: string, emissionNOx: string, emissionNH3: string) => {  
      cy.get('[id="offroadDetailDescription-'+ index +'"]').should("have.text", mobileEquipmentSource.description);
      cy.get('[id="offroadDetailCategory-'+ index +'"]').should("have.text", mobileEquipmentSource.stageClass);
      cy.get('[id="offroadDetailFuelUsage-'+ index +'"]').should("have.text", mobileEquipmentSource.fuelUsage);      
      cy.get('[id="offroadDetailOperatingHours-'+ index +'"]').should("have.text", mobileEquipmentSource.operationHours);
      cy.get('[id="offroadDetailAdBlueUsage-'+ index +'"]').should("have.text", mobileEquipmentSource.adBlue);
      cy.get('[id="offroadDetailEmission-' + index +'-NOX"]').should("have.text", mobileEquipmentSource.emissionNOx);
      cy.get('[id="offroadDetailEmission-'+ index +'-NH3"]').should("have.text", mobileEquipmentSource.emissionNH3);
      index++;
    });
  }

  static blueRatioWarningisVisible() {
    cy.get(".fuelAdBlueRatioWarning").should("exist");
  }

  static blueRatioWarningisNotVisible() {
    cy.get(".fuelAdBlueRatioWarning").should("not.exist");
  }

  static chooseToAddNewStageClass(stageClass: string) {
    cy.get('[id="collapsibleSubSources"] > .line > .title').click({
      force: true
    });
    cy.get('[id="sourceListButtonNewSource"] > .icon').click({ force: true });
    cy.get('[id="emissionSourceOffRoadMobileStandardDescription"]').clear().type("XXX");
    cy.get('[id="offRoadMobileCategory"]').select(stageClass);
  }

  static fieldVisible(fieldName: string) {
    switch (fieldName) {
      case "Brandstofverbruik":
        cy.get('[id="emissionSourceOffRoadMobileStandardLiterFuel"]').should("exist", fieldName);
        break;
      case "Draaiuren":
        cy.get('[id="emissionSourceOffRoadMobileStandardOperatingHours"]').should("exist", fieldName);
        break;
      case "AdBlue verbruik":
        cy.get('[id="emissionSourceOffRoadMobileStandardLiterAdBlue"]').should("exist", fieldName);
        break;
      default:
        throw new Error("Unknown field name: " + fieldName);
    }
  }

  static fieldNotVisible(fieldName: string) {
    switch (fieldName) {
      case "Brandstofverbruik":
        cy.get('[id="emissionSourceOffRoadMobileStandardLiterFuel"]').should("not.exist", fieldName);
        break;
      case "Draaiuren":
        cy.get('[id="emissionSourceOffRoadMobileStandardOperatingHours"]').should("not.exist", fieldName);
        break;
      case "AdBlue verbruik":
        cy.get('[id="emissionSourceOffRoadMobileStandardLiterAdBlue"]').should("not.exist", fieldName);
        break;
      default:
        throw new Error("Unknown field name: " + fieldName);
    }
  }

  static selectStageClass(stageClass: string) {
    cy.get('[id="subSourceListDescription').should("contain", stageClass).click();
  }
}
