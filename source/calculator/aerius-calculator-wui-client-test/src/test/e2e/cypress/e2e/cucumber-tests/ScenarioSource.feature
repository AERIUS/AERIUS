#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Scenario source

    Scenario: Import GML scenario example composting proposed
        Given I login with correct username and password
        And I open the file dialog to import
        And I select file 'scenario/scenario_composting_proposed.gml' to be imported
        Then the situation type is 'Beoogd'
        And the calculation year is 'minCalculationYear'
        And I open emission source list
        And the emission source list has 8 sources
        And the roadnetwork has 19 sources

    Scenario: Import GML scenario example composting reference
        Given I login with correct username and password
        And I open the file dialog to import
        And I select file 'scenario/scenario_composting_reference.gml' to be imported
        Then the situation type is 'Referentie'
        And the calculation year is 'minCalculationYear'
        And I open emission source list
        And the emission source list has 12 sources
        And the roadnetwork has 19 sources

    Scenario: Import GML scenario example farmland reference
        Given I login with correct username and password
        And I open the file dialog to import
        And I select file 'scenario/scenario_farmland_proposed.gml' to be imported
        Then the situation type is 'Beoogd'
        And the calculation year is 'minCalculationYear'
        And I open emission source list
        And the emission source list has 44 sources
        And the roadnetwork has 0 sources

    Scenario: Import GML scenario example greenhouse proposed
        Given I login with correct username and password
        And I open the file dialog to import
        And I select file 'scenario/scenario_greenhouse_proposed.gml' to be imported
        Then the situation type is 'Beoogd'
        And the calculation year is 'minCalculationYear'
        And I open emission source list
        And the emission source list has 9 sources
        And the roadnetwork has 1 sources

    Scenario: Import GML scenario example greenhouse proposed
        Given I login with correct username and password
        And I open the file dialog to import
        And I select file 'scenario/scenario_greenhouse_reference.gml' to be imported
        Then the situation type is 'Referentie'
        And the calculation year is 'minCalculationYear'
        And I open emission source list
        And the emission source list has 6 sources
        And the roadnetwork has 2 sources

    Scenario: Import GML scenario example livestock farming netting
        Given I login with correct username and password
        And I open the file dialog to import
        And I select file 'scenario/scenario_livestock_farming_netting.gml' to be imported
        Then the situation type is 'Saldering'
        And the calculation year is 'minCalculationYear'
        And I open emission source list
        And the emission source list has 2 sources
        And the roadnetwork has 0 sources

    Scenario: Import GML scenario example livestock farming proposed
        Given I login with correct username and password
        And I open the file dialog to import
        And I select file 'scenario/scenario_livestock_farming_proposed.gml' to be imported
        Then the situation type is 'Beoogd'
        And the calculation year is 'minCalculationYear'
        And I open emission source list
        And the emission source list has 6 sources
        And the roadnetwork has 1 sources

    Scenario: Import GML scenario example livestock farming reference
        Given I login with correct username and password
        And I open the file dialog to import
        And I select file 'scenario/scenario_livestock_farming_reference.gml' to be imported
        Then the situation type is 'Referentie'
        And the calculation year is 'minCalculationYear'
        And I open emission source list
        And the emission source list has 5 sources
        And the roadnetwork has 1 sources

    Scenario: Import GML scenario example powerplant netting
        Given I login with correct username and password
        And I open the file dialog to import
        And I select file 'scenario/scenario_powerplant.gml' to be imported
        Then the situation type is 'Beoogd'
        And the calculation year is 'minCalculationYear'
        And I open emission source list
        And the emission source list has 17 sources
        And the roadnetwork has 3 sources

    Scenario: Import GML scenario example powerplant proposed
        Given I login with correct username and password
        And I open the file dialog to import
        And I select file 'scenario/scenario_smokehouse_proposed.gml' to be imported
        Then the situation type is 'Beoogd'
        And the calculation year is 'minCalculationYear'
        And I open emission source list
        And the emission source list has 8 sources
        And the roadnetwork has 27 sources

    @Smoke
    Scenario: Import GML scenario example powerplant reference
        Given I login with correct username and password
        And I open the file dialog to import
        And I select file 'scenario/scenario_smokehouse_reference.gml' to be imported
        Then the situation type is 'Referentie'
        And the calculation year is 'minCalculationYear'
        And I open emission source list
        And the emission source list has 8 sources
        And the roadnetwork has 18 sources

    Scenario: Import GML scenario example traffic network proposed
        Given I login with correct username and password
        And I open the file dialog to import
        And I select file 'scenario/scenario_traffic_network_proposed.gml' to be imported
        Then the situation type is 'Beoogd'
        And the calculation year is 'minCalculationYear'
        And I open emission source list
        And the emission source list has 1 sources
        And the roadnetwork has 100 sources

    Scenario: Import GML scenario example traffic network reference
        Given I login with correct username and password
        And I open the file dialog to import
        And I select file 'scenario/scenario_traffic_network_reference.gml' to be imported
        Then the situation type is 'Referentie'
        And the calculation year is 'minCalculationYear'
        And I open emission source list
        And the emission source list has 1 sources
        And the roadnetwork has 100 sources
