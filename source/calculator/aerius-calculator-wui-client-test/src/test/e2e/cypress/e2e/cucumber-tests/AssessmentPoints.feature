#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Assessment points
    Test everything to do with calculation points.

    Scenario: Assessment points should start empty
        Given I login with correct username and password
        And I create a new source from the start page
        And I navigate to the 'Assessment points' tab
        Then the overview list has 0 assessment points

    Scenario: Create a new assessment point
        Given I login with correct username and password
        And I create a new source from the start page
        And I navigate to the 'Assessment points' tab
        And I create the first assessment point
        And I enter the name 'Rekenpuntje'
        And I enter the coordinates 'POINT(107967.88 523921.47)'
        And I save the assessment point
        Then the overview list has 1 assessment points
        Then the assessment point 'Rekenpuntje' exists

    @Smoke
    Scenario: Check assessment points saving across tabs
        Given I login with correct username and password
        And I create a new source from the start page
        And I navigate to the 'Assessment points' tab
        And I create the first assessment point
        And I enter the name 'Rekenpuntje'
        And I enter the coordinates 'POINT(107967.88 523921.47)'
        And I save the assessment point
        And I navigate to the 'Input' tab
        And I navigate to the 'Assessment points' tab
        Then the overview list has 1 assessment points
        Then the assessment point 'Rekenpuntje' exists

    Scenario: Cancel creating a new assessment point
        Given I login with correct username and password
        And I create a new source from the start page
        And I navigate to the 'Assessment points' tab
        And I create the first assessment point
        And I enter the name 'OG_Rekenpuntje'
        And I enter the coordinates 'POINT(107967.88 523921.47)'
        And I save the assessment point
        And I create a new assessment point
        And I enter the name 'Rekenpuntje2'
        And I enter the coordinates 'POINT(107967.88 523921.47)'
        And I choose to cancel 'AssessmentPoint'
        Then the overview list has 1 assessment points
        Then the assessment point 'OG_Rekenpuntje' exists
        Then the assessment point 'Rekenpuntje2' does not exist

    Scenario: Create multiple assessment points
        Given I login with correct username and password
        And I create a new source from the start page
        And I navigate to the 'Assessment points' tab
        And I create the first assessment point
        And I enter the coordinates 'POINT(107967.88 523921.47)'
        And I save the assessment point
        And I create a new assessment point
        And I enter the coordinates 'POINT(225747.69 483894.04)'
        And I save the assessment point
        Then the overview list has 2 assessment points

    Scenario: Update an existing assessment points
        Given I login with correct username and password
        And I create a new source from the start page
        And I navigate to the 'Assessment points' tab
        And I create the first assessment point
        And I enter the name 'OG_Rekenpuntje'
        And I enter the coordinates 'POINT(107967.88 523921.47)'
        And I save the assessment point
        And I create a new assessment point
        And I enter the name 'Rekenpuntje2'
        And I enter the coordinates 'POINT(225747.69 483894.04)'
        And I save the assessment point
        And I select the assessment point 'Rekenpuntje'
        And I update this assessment point
        And I enter the name 'Rekenpuntje_vernieuwd'
        And I save the assessment point
        Then the overview list has 2 assessment points
        Then the assessment point 'OG_Rekenpuntje' does not exist
        Then the assessment point 'Rekenpuntje_vernieuwd' exists
        Then the assessment point 'Rekenpuntje2' exists

    Scenario: Delete all assessment points
        Given I login with correct username and password
        And I create a new source from the start page
        And I navigate to the 'Assessment points' tab
        And I create the first assessment point
        And I enter the name 'OG_Rekenpuntje'
        And I enter the coordinates 'POINT(107967.88 523921.47)'
        And I save the assessment point
        And I create a new assessment point
        And I enter the name 'Rekenpuntje2'
        And I enter the coordinates 'POINT(225747.69 483894.04)'
        And I save the assessment point
        Then the overview list has 2 assessment points
        And I delete all assessment points
        Then the overview list has 0 assessment points

    Scenario: Delete single assessment point
        Given I login with correct username and password
        And I create a new source from the start page
        And I navigate to the 'Assessment points' tab
        And I create the first assessment point
        And I enter the name 'OG_Rekenpuntje'
        And I enter the coordinates 'POINT(107967.88 523921.47)'
        And I save the assessment point
        And I create a new assessment point
        And I enter the name 'Rekenpuntje2'
        And I enter the coordinates 'POINT(225747.69 483894.04)'
        And I save the assessment point
        And I create a new assessment point
        And I enter the name 'Rekenpuntje3'
        And I enter the coordinates 'POINT(225757.69 483884.04)'
        And I save the assessment point
        And I select the assessment point 'Rekenpuntje2'
        And I delete this assessment point
        Then the assessment point 'OG_Rekenpuntje' exists
        Then the assessment point 'Rekenpuntje2' does not exist
        Then the assessment point 'Rekenpuntje3' exists

    Scenario: Add duplicate assessment point should show error
        Given I login with correct username and password
        And I create a new source from the start page
        And I navigate to the 'Assessment points' tab
        And I create the first assessment point
        And I enter the name 'OG_Rekenpuntje'
        And I enter the coordinates 'POINT(107967.88 523921.47)'
        And I save the assessment point
        And I create a new assessment point
        And I enter the name 'Rekenpuntje2'
        And I enter the coordinates 'POINT(107967.88 523921.47)'
        And I save the assessment point
        Then an error should be shown
        Then I should still be in the new assessment point screen

    Scenario: Try to create an assessment point with wrong info
        Given I login with correct username and password
        And I create a new source from the start page
        And I navigate to the 'Assessment points' tab
        And I create the first assessment point
        And I save the assessment point
        Then an error should be shown
        Then I should still be in the new assessment point screen
        And I enter the coordinates 'POINT(107967.88 523921.47)'
        And I save the assessment point
        Then the overview list has 1 assessment points
        And I create a new assessment point
        And I enter the coordinates 'POINT(225747.69 483894.04'
        And I save the assessment point
        Then an error should be shown
        Then I should still be in the new assessment point screen
        And I enter the coordinates 'LINESTRING(108146.39 524135.32,108211.78 523974.49)'
        And I save the assessment point
        Then an error should be shown
        Then I should still be in the new assessment point screen
        And I enter an empty name
        And I enter the coordinates 'POINT(107967.88 523921.47)'
        And I save the assessment point
        Then an error should be shown
        Then I should still be in the new assessment point screen

    Scenario: Create scenario with no emission source and automatically generate calculation points for it
        Given I login with correct username and password
        When I create an empty scenario from the start page
        And I navigate to the 'Assessment points' tab
        Then I open the panel for generating calculation points
        And I enable the generation of calculation points inside of the Netherlands
        When I determine the generated calculation points
        Then no calculation points should've been generated
        When I cancel the creation the generation of calculation points
        Then the overview list has 0 assessment points

    Scenario: Create scenario with an emission source far away from all nature areas and automatically generate calculation points for it
        Given I login with correct username and password
        When I select file 'emissionSourceFarAway.gml' to be imported
        And I navigate to the 'Assessment points' tab
        Then I open the panel for generating calculation points
        And I enable the generation of calculation points inside of the Netherlands
        When I determine the generated calculation points
        Then no calculation points should've been generated
        When I cancel the creation the generation of calculation points
        Then the overview list has 0 assessment points

    Scenario: Create scenario with an emission source and a really small radius and automatically generate calculation points for it
        Given I login with correct username and password
        When I select file 'emissionSourceMiddleOfVeluwe.gml' to be imported
        And I navigate to the 'Assessment points' tab
        Then I open the panel for generating calculation points
        And I enable the generation of calculation points inside of the Netherlands
        And I set the radius to '1' meters
        When I determine the generated calculation points
        Then calculation points should've been generated
        When I save the generated calculation points
        Then the overview list has 1 assessment points

    Scenario: Create scenario with an emission source try setting a too high or low radius to it before automatically generating calculation points for it
        Given I login with correct username and password
        When I select file 'emissionSourceMiddleOfVeluwe.gml' to be imported
        And I navigate to the 'Assessment points' tab
        Then I open the panel for generating calculation points
        And I enable the generation of calculation points inside of the Netherlands
        When I set the radius to '10.5' meters
        Then I expect to see a radius error
        When I set the radius to '0' meters
        Then I expect to see a radius error
        When I set the radius to '-10' meters
        Then I expect to see a radius error
        When I set the radius to '1000000' meters
        Then I expect to see a radius error
        When I set the radius to '10000' meters
        Then I expect to see no radius error
        When I determine the generated calculation points
        Then calculation points should've been generated
        When I save the generated calculation points
        Then the overview list has 34 assessment points

    Scenario: Create scenario with an emission source with effects inside and outside of NL and automatically generate calculation points for it
        Given I login with correct username and password
        When I select file 'emissionSourceZuidLimburg.gml' to be imported
        And I navigate to the 'Assessment points' tab
        Then I open the panel for generating calculation points
        And I enable the generation of calculation points inside of the Netherlands
        When I determine the generated calculation points
        Then calculation points should've been generated
        When I save the generated calculation points
        Then the overview list has 99 assessment points

    Scenario: Create scenario with an emission source with effects outside of NL and automatically generate calculation points for it
        Given I login with correct username and password
        When I select file 'emissionSourceZuidLimburg.gml' to be imported
        And I navigate to the 'Assessment points' tab
        Then I open the panel for generating calculation points
        When I determine the generated calculation points
        Then calculation points should've been generated
        When I save the generated calculation points
        Then the overview list has 31 assessment points

    Scenario: Create scenario with multiple emission sources and automatically generate calculation points for it
        Given I login with correct username and password
        When I select file 'emissionSources2SidesOfNL.gml' to be imported
        And I navigate to the 'Assessment points' tab
        Then I open the panel for generating calculation points
        And I enable the generation of calculation points inside of the Netherlands
        When I determine the generated calculation points
        Then calculation points should've been generated
        When I save the generated calculation points
        Then the overview list has 549 assessment points
    #NOTE: 550 assessment points seems excessive, but this is a known issue. We don't expect users to have sources this far apart with nothing in between.

    Scenario: Import 2 scenario's with emission sources on different places and automatically generate calculation points for both seperately
        Given I login with correct username and password
        When I select file '2ScenariosEmissionSources2SidesOfNL.zip' to be imported
        And I navigate to the 'Assessment points' tab
        Then I open the panel for generating calculation points
        And I enable the generation of calculation points inside of the Netherlands
        When I determine the generated calculation points
        Then calculation points should've been generated
        When I save the generated calculation points
        Then the overview list has 21 assessment points
        When I select the option to automatically generate points again
        And I enable the generation of calculation points inside of the Netherlands
        And I select the scenario 'Situatie 2 - Beoogd' as scenario to generate assessment points for
        When I determine the generated calculation points
        Then calculation points should've been generated
        When I save the generated calculation points
        Then the overview list has 106 assessment points

    # These tests fail because of this bug: https://aerius.atlassian.net/browse/AER-1114
    Scenario: Create scenario and import calculation points
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data
        And I click on the start page button
        And I select calculation point file 'AERIUS_GML_24_CalculationPoints.gml' to be imported
        And I navigate to the 'Assessment points' tab
        Then the overview list has 24 assessment points

    Scenario: Import calculation points and select multipe calculation points
        Given I login with correct username and password
        When I select calculation point file 'AERIUS_GML_24_CalculationPoints.gml' to be imported
        And I navigate to the 'Assessment points' tab   
        And I select the following assessment points using multiselect
            | assessmentId        |
            | calculationPoint-1  |
            | calculationPoint-7  |
            | calculationPoint-9  |
            | calculationPoint-11 |
        Then I check if the following assessment points are selected 
            | assessmentId        |
            | calculationPoint-1  |
            | calculationPoint-7  |
            | calculationPoint-9  |
            | calculationPoint-11 |
        And I check if the following assessment points are not selected 
            | assessmentId        |
            | calculationPoint-2  |
            | calculationPoint-8  |
            | calculationPoint-10 |
        When I deselect the following assessment points using multiselect
            | assessmentId       |
            | calculationPoint-1 |
            | calculationPoint-7 |
        Then I check if the following assessment points are selected 
            | assessmentId        |
            | calculationPoint-9  |
            | calculationPoint-11 |
        And I check if the following assessment points are not selected 
            | assessmentId       |
            | calculationPoint-1 |
            | calculationPoint-7 |

    @Smoke
    Scenario: Create scenario and add one calculation point and add calculation points with import
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data
        And I navigate to the 'Assessment points' tab
        And I create the first assessment point
        And I enter the name 'Rekenpuntje'
        And I enter the coordinates 'POINT(107967.88 523921.47)'
        And I save the assessment point
        When I click on the start page button
        And I select calculation point file 'AERIUS_GML_24_CalculationPoints.gml' to be imported
        And I navigate to the 'Assessment points' tab
        Then the overview list has 25 assessment points

    Scenario: Warning no option selected when creating assessment points automatically
        Given I login with correct username and password
        And I create a new source from the start page
        And I navigate to the 'Assessment points' tab
        And I open the panel for generating calculation points
        And I disable the generation of calculation points for assessment areas abroad
        And I determine the generated calculation points
        Then the following warning is shown: "Er is geen optie geselecteerd"

    Scenario: Warning no situation does not contain sources when creating assessment points automatically
        Given I login with correct username and password
        And I create a new source from the start page
        And I navigate to the 'Assessment points' tab
        And I open the panel for generating calculation points
        And I determine the generated calculation points
        Then the following warning is shown: "De gekozen situatie bevat geen bronnen, het is niet mogelijk om automatisch rekenpunten te bepalen."

    Scenario: Validate specific order of list of assessment points
        Given I login with correct username and password
        When I select file 'emissionSourceZuidLimburg.gml' to be imported
        And I navigate to the 'Assessment points' tab
        Then I open the panel for generating calculation points
        When I determine the generated calculation points
        Then calculation points should've been generated
        When I save the generated calculation points
        # Took some samples from the list for validation
        Then the following assessment points exists in the following order
         | rowId                      | name                                                |
         | calculationPoint-1-label   | Voerstreek (1 km)                                   |
         | calculationPoint-10-label  | Basse vallée du Geer (16 km)                        |
         | calculationPoint-20-label  | Münsterbachtal, Münsterbusch (21 km)                |
         | calculationPoint-30-label  | Schlangenberg (24 km)                               |
         | calculationPoint-31-label  | Jekervallei en bovenloop van de Demervallei (25 km) |
