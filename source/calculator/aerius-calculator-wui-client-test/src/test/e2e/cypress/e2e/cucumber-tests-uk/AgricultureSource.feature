#
# Crown copyright
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Agriculture source

    Scenario: Create new agriculture source without subsource validate warning - Animal housing emission
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Agriculture' and sector 'Animal housing'
        And I fill location with coordinates 'POINT(322447.2 594884.3)'
        And I choose source height '25' and source diameter '0.5'
        And I save the data
        Then the subsource error is given 'Add at least 1 subsource.'
        And the savesource error is given 'Fill in the highlighted fields to save your work'
        And the savesource button is disabled.

    @Smoke
    Scenario: Create new agriculture source - Manure storage
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Agriculture' and sector 'Manure storage'
        And I fill location with coordinates 'POINT(322447.2 594884.3)'
        And I choose source height '25' and source diameter '0.5'
        And I choose buoyancy 'TEMPERATURE' '20'
        And I choose efflux 'VELOCITY' '20'
        And I choose specific heat capacity '1100'
        And I fill the Manure storage category with 'Slurry - circular store (Pigs - No cover)', Area with value '101'
        And I save the data
        Then I select the source 'Source 1'
        Then the source 'Source 1' with sector group 'Agriculture' and sector 'Manure storage' is correctly saved
        And location with coordinates 'X:322447.2 Y:594884.3' is correctly saved
        And source characteristics with emission height '25.00 m' is correctly saved
        And source characteristics with source diameter '0.50 m' is correctly saved
        And source characteristics with buoyancy 'TEMPERATURE' '20.00 °C' is correctly saved
        And source characteristics with efflux 'VELOCITY' '20.00 m/s' is correctly saved
        And efflux type 'VOLUME' shouldn't exist
        And source characteristics with specific heat capacity '1,100.000 J/K/kg' is correctly saved
        And farmland category Manure storage with Description 'Slurry - circular store (Pigs - No cover)' and area '101 m²' is correctly created

    Scenario: Create new agriculture source - Farmland
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Agriculture' and sector 'Farmland'
        And I fill location with coordinates 'POINT(322447.2 594884.3)'
        And I choose source height '25' and source diameter '0.5'
        And I choose efflux 'VOLUME' '10.005'
        And I add a new farmland source
        And I fill the farmland category with 'Farmland grazing', option 'Beef calves (<1 year)', amount '120' and number of days with value '250'
        And I save the data
        Then I select the source 'Source 1'
        Then the source 'Source 1' with sector group 'Agriculture' and sector 'Farmland' is correctly saved
        And location with coordinates 'X:322447.2 Y:594884.3' is correctly saved
        And source characteristics with emission height '25.00 m' is correctly saved
        And source characteristics with source diameter '0.50 m' is correctly saved
        And source characteristics with efflux 'VOLUME' '10.01 m³/s' is correctly saved
        And efflux type 'VELOCITY' shouldn't exist
        And the farmland category with 'Farmland grazing', option 'Beef calves (<1 year)', amount '120' and number of days with value '250' is correctly saved

    Scenario: Create new agriculture source - Greenhouse horticulture
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Agriculture' and sector 'Greenhouse horticulture'
        And I fill location with coordinates 'POINT(322447.2 594884.3)'
        And I choose source height '25' and source diameter '0.5'
        And I fill the emission fields with NOx '0' and NH3 '0'
        And I save the data
        Then I select the source 'Source 1'
        Then the source 'Source 1' with sector group 'Agriculture' and sector 'Greenhouse horticulture' is correctly saved
        And location with coordinates 'X:322447.2 Y:594884.3' is correctly saved
        And source characteristics with emission height '25.00 m' is correctly saved
        And source characteristics with source diameter '0.50 m' is correctly saved
        And the total 'generic' emission 'NOx' is '0.0 kg/y'
        And the total 'generic' emission 'NH3' is '0.0 kg/y'

    Scenario: Create new agriculture source - Fireplaces, other
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Agriculture' and sector 'Fireplaces, other'
        And I fill location with coordinates 'POINT(322447.2 594884.3)'
        And I choose source height '25' and source diameter '0.5'
        And I fill the emission fields with NOx '99999' and NH3 '88888'
        And I save the data
        Then I select the source 'Source 1'
        Then the source 'Source 1' with sector group 'Agriculture' and sector 'Fireplaces, other' is correctly saved
        And location with coordinates 'X:322447.2 Y:594884.3' is correctly saved
        And source characteristics with emission height '25.00 m' is correctly saved
        And source characteristics with source diameter '0.50 m' is correctly saved
        And the total 'generic' emission 'NOx' is '100.0 tonne/y'
        And the total 'generic' emission 'NH3' is '88.9 tonne/y'

    Scenario: Import and copy existing agriculture source - Animal housing emissions
        Given I login with correct username and password
        And I choose to import a source starting from the start page
        And I open the file dialog to import
        And I select file 'UKAPAS_agriculture.gml' to be imported
        And I open emission source list
        When I select the source 'Source 1'
        Then the source 'Source 1' with sector group 'Agriculture' and sector 'Animal housing' is correctly saved
        And source characteristics with emission height '25.00 m' is correctly saved
        And source characteristics with source diameter '0.50 m' is correctly saved
        And location with coordinates 'X:136319.74 Y:577666.32' is correctly saved

    Scenario: Create new agriculture source - Animal housing emissions with housing code
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Agriculture' and sector 'Animal housing'
        And I fill location with coordinates 'POINT(322447.2 594884.3)'
        And I choose source height '25' and source diameter '0.5'
        And I add a animal housing system with housing code 'A 1.1', number of animals '100' and number of days '101'
        And I validate the subsource with housing code 'A1.1' with number of animals '100' and emission '717.1 kg/y'
        And I save the data
        Then I select the source 'Source 1'
        Then the source 'Source 1' with sector group 'Agriculture' and sector 'Animal housing' is correctly saved
        And location with coordinates 'X:322447.2 Y:594884.3' is correctly saved
        And the animal housing system with housing code 'A1.1', number of animals '100', factor '0.071 kg/animal/d', days '101' and emission '717.1 kg/y' is correctly created

    Scenario: Cancel animal housing emissions with housing code
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Agriculture' and sector 'Animal housing'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose source height '25' and source diameter '0.5'
        And I add a animal housing system with housing code 'E 1.5' and number of animals '101'
        And I validate the subsource with housing code 'E1.5' with number of animals '101' and emission '3.5 kg/y'
        And I save the data
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Agriculture' and sector 'Animal housing' is correctly saved
        And location with coordinates 'X:172203.2 Y:599120.32' is correctly saved
        And ADMS source characteristics with source height '25.00 m' and source diameter '0.50 m' is correctly saved
        When I choose to edit the source
        And I add a animal housing system with housing code 'D 1.1' and number of animals '99'
        When I choose to cancel
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Agriculture' and sector 'Animal housing' is correctly saved
        And location with coordinates 'X:172203.2 Y:599120.32' is correctly saved
        And ADMS source characteristics with source height '25.00 m' and source diameter '0.50 m' is correctly saved
        And the animal housing system with housing code 'E1.5', number of animals '101', factor '0.035 kg/animal/y', days '' and emission '3.5 kg/y' is correctly created
        And the housing code row count has '1' rows
 
    Scenario: Edit existing agriculture source - Animal housing emissions
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Agriculture' and sector 'Animal housing'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose source height '25' and source diameter '0.5'
        And I add a animal housing system with housing code 'E 1.5' and number of animals '50'
        And I validate the subsource with housing code 'E1.5' with number of animals '50' and emission '1.8 kg/y'
        And I save the data
        When I select the source 'Bron 1'
        And I choose to edit the source
        And I select animal housing system with housing code 'E1.5'
        And I edit the animal housing system to housing code 'E 1.5' and number of animals '100'
        And I validate the subsource with housing code 'E1.5' with number of animals '100' and emission '3.5 kg/y'
        And I save the data
        Then the source 'Bron 1' with sector group 'Agriculture' and sector 'Animal housing' is correctly saved
        And location with coordinates 'X:172203.2 Y:599120.32' is correctly saved
        And ADMS source characteristics with source height '25.00 m' and source diameter '0.50 m' is correctly saved
        And the animal housing system with housing code 'E1.5', number of animals '100', factor '0.035 kg/animal/y', days '' and emission '3.5 kg/y' is correctly created

    Scenario: Copy existing agriculture source - Animal housing emissions
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Agriculture' and sector 'Animal housing'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose source height '25' and source diameter '0.5'
        And I add a animal housing system with housing code 'E 1.5' and number of animals '100'
        And I validate the subsource with housing code 'E1.5' with number of animals '100' and emission '3.5 kg/y'
        And I save the data
        When I select the source 'Bron 1'
        And I copy the source 'Bron 1'
        And I close source detail screen
        When I select the source 'Bron 1 (1)'
        Then the source 'Bron 1 (1)' with sector group 'Agriculture' and sector 'Animal housing' is correctly saved
        And location with coordinates 'X:172203.2 Y:599120.32' is correctly saved
        And ADMS source characteristics with source height '25.00 m' and source diameter '0.50 m' is correctly saved
        And the animal housing system with housing code 'E1.5', number of animals '100', factor '0.035 kg/animal/y', days '' and emission '3.5 kg/y' is correctly created

    Scenario: Delete existing agriculture source - Animal housing emissions
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Agriculture' and sector 'Animal housing'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose source height '25' and source diameter '0.5'
        And I add a animal housing system with housing code 'E 1.5' and number of animals '100'
        And I save the data
        When I select the source 'Bron 1'
        And I copy the source 'Bron 1'
        And I copy the source 'Bron 1 (1)'
        Given I delete source 'Bron 1 (1)'
        Then the next sources exists 'Bron 1', 'Bron 1 (2)'
        And the next source does not exist 'Bron 1 (1)'

    Scenario: Delete existing animal housing system
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Agriculture' and sector 'Animal housing'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose source height '25' and source diameter '0.5'
        And I add a animal housing system with housing code 'E 1.5' and number of animals '100'
        And I save the data
        When I select the source 'Bron 1'
        And I choose to edit the source
        And I select animal housing system with housing code 'E1.5'
        And I delete the animal housing system
        Then the housing code is not visible

    Scenario: Copy existing animal housing system
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Agriculture' and sector 'Animal housing'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose source height '25' and source diameter '0.5'
        And I add a animal housing system with housing code 'E 1.5' and number of animals '100'
        And I save the data
        When I select the source 'Bron 1'
        And I choose to edit the source
        And I select animal housing system with housing code 'E1.5'
        And I copy the animal housing system
        And I save the data
        Then the animal housing system with housing code 'E1.5' is copied

    @Smoke
    Scenario: Create new agriculture source - Animal housing emissions with custom specifications
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Agriculture' and sector 'Animal housing'
        And I fill location with coordinates 'POINT(322447.2 594884.3)'
        And I choose source height '25' and source diameter '0.5'
        And I open the subsource dropdown
        When I add a animal housing system and fill in the custom specification fields with the following data
            | animalType    | factor | numberOfAnimals |
            | Cattle        | 5      | 100             |
            | Sheep         | 10     | 200             |
            | Goats         | 15     | 300             |
            | Pigs          | 20     | 400             |
            | Poultry       | 25     | 500             |
            | Turkeys       | 30     | 600             |
            | Horses        | 35     | 600             |
            | Ducks         | 40     | 700             |
            | Fur animals   | 45     | 800             |
            | Rabbits       | 50     | 900             |
            | Guinea fowl   | 55     | 1000            |
            | Ostrich       | 60     | 2000            |
            | Other         | 65     | 3000            |
        And I save the data
        When I select the source 'Source 1'
        Then the following animal housing systems with custom specification with days are created
            | description | numberOfAnimals | factor         | days     | emission       |
            | Cattle      | 100             | 5 kg/animal/y  |          | 500.0 kg/y     |
            | Sheep       | 200             | 10 kg/animal/y |          | 2,000.0 kg/y   |
            | Goats       | 300             | 15 kg/animal/y |          | 4,500.0 kg/y   |
            | Pigs        | 400             | 20 kg/animal/y |          | 8,000.0 kg/y   |
            | Poultry     | 500             | 25 kg/animal/y |          | 12.5 tonne/y   |
            | Turkeys     | 600             | 30 kg/animal/y |          | 18.0 tonne/y   |
            | Horses      | 600             | 35 kg/animal/y |          | 21.0 tonne/y   |
            | Ducks       | 700             | 40 kg/animal/y |          | 28.0 tonne/y   |
            | Fur animals | 800             | 45 kg/animal/y |          | 36.0 tonne/y   |
            | Rabbits     | 900             | 50 kg/animal/y |          | 45.0 tonne/y   |
            | Guinea fowl | 1000            | 55 kg/animal/y |          | 55.0 tonne/y   |
            | Ostrich     | 2000            | 60 kg/animal/y |          | 120.0 tonne/y  |
            | Other       | 3000            | 65 kg/animal/y |          | 195.0 tonne/y  |
        When I choose to edit the source
        Then the animal housing systems contains the following icons and data in the list
            | icon                    | amount | description | emission       |
            | icon-animal-cow         | 100    | Cattle      | 500.0 kg/y     |
            | icon-animal-sheep       | 200    | Sheep       | 2,000.0 kg/y   |
            | icon-animal-goat        | 300    | Goats       | 4,500.0 kg/y   |
            | icon-animal-pig         | 400    | Pigs        | 8,000.0 kg/y   |
            | icon-animal-chicken     | 500    | Poultry     | 12.5 tonne/y   |
            | icon-animal-turkey      | 600    | Turkeys     | 18.0 tonne/y   |
            | icon-animal-horse       | 600    | Horses      | 21.0 tonne/y   |
            | icon-animal-duck        | 700    | Ducks       | 28.0 tonne/y   |
            | icon-animal-mink        | 800    | Fur animals | 36.0 tonne/y   |
            | icon-animal-rabbit      | 900    | Rabbits     | 45.0 tonne/y   |
            | icon-animal-guinea-fowl | 1000   | Guinea fowl | 55.0 tonne/y   |
            | icon-animal-ostrich     | 2000   | Ostrich     | 120.0 tonne/y  |
            |                         | 3000   | Other       | 195.0 tonne/y  |

    @Smoke
    Scenario: Edit agriculture source - Animal housing emissions with custom specifications
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Source 1' and select sector group 'Agriculture' and sector 'Animal housing'
        And I fill location with coordinates 'POINT(322447.2 594884.3)'
        And I choose source height '25' and source diameter '0.5'
        And I add a animal housing system with housing code 'E 1.5' and number of animals '100'
        And I choose animal housing system of type 'Custom specification'
        And I fill in the fields with description 'Animal housing system with poultry', animal type 'Poultry', factor '5' and number of animals '15'
        And I validate the subsource with custom specification 'Animal housing system with poultry' with number of animals '15' and emission '75.0 kg/y'
        And I save the data
        When I select the source 'Source 1'
        Then the source 'Source 1' with sector group 'Agriculture' and sector 'Animal housing' is correctly saved
        Then the custom specification with description 'Animal housing system with poultry', number of animals '15', factor '5 kg/animal/y', days '' and emission '75.0 kg/y' is correctly created
        When I choose to edit the source
        And I select animal housing system with housing code 'Animal housing system with poultry'
        And I fill in the fields with description 'Animal housing system with rabbits', animal type 'Rabbits', factor '5' and number of animals '175'
        And I validate the subsource with custom specification 'Animal housing system with rabbits' with number of animals '175' and emission '875.0 kg/y'
        And I save the data
        Then the custom specification with description 'Animal housing system with rabbits', number of animals '175', factor '5 kg/animal/y', days '' and emission '875.0 kg/y' is correctly created
