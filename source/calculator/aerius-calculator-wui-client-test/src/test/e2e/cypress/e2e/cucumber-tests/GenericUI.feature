#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: GenericUI
    Generic UI tests

    @Smoke
    Scenario: Check correct styling for product
        Given I login with correct username and password
        Then the current styling is conform product 'CALCULATOR'

    @Smoke
    Scenario: Switch language to English and back to Dutch
        Given I login with correct username and password
        Given I save the current session id

        When I navigate to the 'Language' tab
        And I switch language to 'English'
        Then I navigate to the 'Language' tab

        When the current language should be 'English'
        And I switch language to 'Nederlands'
        And I navigate to the 'Language' tab
        Then the current language should be 'Nederlands'

    Scenario: Test the switch language panel more thoroughly
        Given I login with correct username and password
        Given I save the current session id

        # TODO: Enable to create an emission source to test the window confirm dialog with
        # # Create a scenario, so refreshing by selecting a different page shows a warning
        # When I create an empty scenario from the start page
        # And I select view mode 'EMISSION_SOURCES'
        # And I create a new source from the scenario input page
        # And I name the source 'Bron 1' and select sector group 'Energie'
        # And I fill location with coordinates 'POINT(172203.2 599120.32)'
        # And I save the data

        # Test closing using the close button
        When I navigate to the 'Language' tab
        And I close the panel
        Then the language screen should be closed

        # Test closing by choosing the current language
        When I navigate to the 'Language' tab
        And I switch language to 'Nederlands', not expecting a reload
        Then the language screen should be closed

        # TODO: Test whether a warning is shown when trying to choose a different language (as there's data that can be lost now with the emission)

    Scenario: Check preferences panel
        Given I login with correct username and password
        When I navigate to the 'Preferences' tab

        # Test default settings
        Then the setting for 'Advanced importing' should be 'off'
        And the setting for 'Automatically turning layers on or off' should be 'on'
        And the setting for 'Automatically switching manual pages' should be 'on'

        # Change settings and test whether they are saved
        When I change the setting for 'Advanced importing' to 'on'
        And I change the setting for 'Automatically turning layers on or off' to 'off'
        And I change the setting for 'Automatically switching manual pages' to 'off'
        And I close the panel
        Then the preferences panel is closed

        When I navigate to the 'Preferences' tab
        Then the setting for 'Advanced importing' should be 'on'
        And the setting for 'Automatically turning layers on or off' should be 'off'
        And the setting for 'Automatically switching manual pages' should be 'off'

        # Test closing by selecting a different tab
        When I navigate to the 'Language' tab
        Then the preferences panel is closed

        # TODO
        # Check linked manual

    @Smoke
    Scenario: Check text & link quickstart guide in opening screen
        Given I login with correct username and password
        And I validate documentation links
            | row | description      | additionalText            | url                                                                  |
            | 0   | Snel aan de slag | (wegwijzer)               | https://link.aerius.nl/documenten/calculator/snel_aan_de_slag_v3.pdf |
            | 1   | Handboek         | (uitgebreide toelichting) | https://link.aerius.nl/documenten/calculator/handboek_2024.pdf       |

    Scenario: Search functionality
        Given I login with correct username and password
        And I create an empty scenario from the start page
        Given I start a search with '6431533' and validate '1' categories and the following detail results are shown
            | result                                                 |
            | Receptor result 1:Receptor 6431533 - x:107459 y:522782 |
        Given I start a search with 'x:107459 y:522782' and validate '2' categories and the following detail results are shown
            | result                                                 |
            | Receptor result 1:Receptor 6431533 - x:107459 y:522782 |
            | Coördinaat result 1:x:107459 y:522782                  |
        Given I start a search with 'bilt' and validate '3' categories and the following detail results are shown
            | result                                                 |
            | Gemeente result 1:Gemeente De Bilt                     |
            | Plaats result 1:De Bilt, De Bilt, Utrecht              |
            | Plaats result 2:Bilthoven, De Bilt, Utrecht            |
            | Plaats result 3:Groenekan, De Bilt, Utrecht            |
            | Plaats result 4:Maartensdijk, De Bilt, Utrecht         |
            | Plaats result 5:Westbroek, De Bilt, Utrecht            |
            | Plaats result 6:Hollandsche Rading, De Bilt, Utrecht   |
            | Straat result 1:De Holle Bilt, De Bilt                 |
            | Straat result 2:Bilt, Stevensweert                     |
            | Straat result 3:Aeolusweg, De Bilt                     |

    Scenario: System info message
        Given I login with correct username and password
        Then I validate no system info message is shown
        Given I open aerius system info update
        And I enter system info message 'Kort systeem bericht !'
        Given I login with correct username and password
        Then I validate system info message 'Kort systeem bericht !'
        Given I open aerius system info update
        And I delete info message
        Given I login with correct username and password
        Then I validate no system info message is shown

    @Smoke
    Scenario: Create a new source, then try switching tab but cancel on confirm pop-up
        Given I login with correct username and password
        And I create a new source from the start page
        And I click to navigate to the 'Assessment points' tab, but press cancel on the confirm pop-up

        When I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Dierhuisvesting'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        Then no notification should be displayed

    Scenario: Start creating a new source, then switch tab, cancelling the creation of the source, and validate that the correct page is shown
        Given I login with correct username and password
        And I create a new source from the start page

        # Navigate away from the page. Cypress automatically presses OK on the browser warning pop-up
        When I navigate to the 'Assessment points' tab
        Then no notification should be displayed
        And the overview list has 0 assessment points

    Scenario: Create a new building, then try switching tab but cancel on confirm pop-up
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'BUILDING'
        And I create a new building from the input page
        And I click to navigate to the 'Assessment points' tab, but press cancel on the confirm pop-up

        When I name the building 'Gebouw'
        And I fill the building shape with WKT string 'POLYGON((208418.99 474219.42,208425.11 474188.05,208439.73 474162.26,208453.55 474171.03,208438.93 474198.15,208436.27 474218.62,208418.99 474219.42))'
        And I fill the building height with '20'
        Then no notification should be displayed

    Scenario: Start creating a new building, then switch tab, cancelling the creation of the building, and validate that the correct page is shown
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'BUILDING'
        And I create a new building from the input page

        # Navigate away from the page. Cypress automatically presses OK on the browser warning pop-up
        When I navigate to the 'Assessment points' tab
        Then no notification should be displayed
        And the overview list has 0 assessment points

    Scenario: Create a new assessment point, then try switching tab but cancel on confirm pop-up
        Given I login with correct username and password
        And I create a new source from the start page
        And I navigate to the 'Assessment points' tab
        And I create the first assessment point
        And I click to navigate to the 'Calculation jobs' tab, but press cancel on the confirm pop-up

        When I enter the name 'OG_Rekenpuntje'
        And I enter the coordinates 'POINT(107967.88 523921.47)'
        Then no notification should be displayed

    Scenario: Start creating a new assessment point, then switch tab, cancelling the creation of the assessment point, and validate that the correct page is shown
        Given I login with correct username and password
        And I create a new source from the start page
        And I navigate to the 'Assessment points' tab
        And I create the first assessment point

        # Navigate away from the page. Cypress automatically presses OK on the browser warning pop-up
        When I navigate to the 'Calculation jobs' tab
        Then no notification should be displayed
        And the calculation jobs list has 0 calculation jobs

    @Smoke
    Scenario: Check status of menu items
        Given I login with correct username and password
        Then I validate the status of the menu items
        | menuId                             | status   |
        | NAV_ICON-MENU-INPUT                | disabled |
        | NAV_ICON-MENU-ASSESSMENT-POINTS    | disabled |
        | NAV_ICON-MENU-CALCULATE            | disabled |
        | NAV_ICON-MENU-RESULTS              | disabled |
        | NAV_ICON-MENU-EXPORT               | disabled |
        | NAV_ICON-MENU-MANUAL1              | enabled  |
        | NAV_ICON-MENU-SETTINGS             | enabled  |
        | NAV_ICON-MENU-HOTKEYS              | enabled  |
        | NAV_ICON-MENU-MANUAL               | enabled  |
        | NAV_ICON-MENU-LANGUAGE             | enabled  |
        And I create a new source from the start page
        Then I validate the status of the menu items
        | menuId                             | status   |
        | NAV_ICON-MENU-INPUT                | enabled  |
        | NAV_ICON-MENU-ASSESSMENT-POINTS    | enabled  |
        | NAV_ICON-MENU-CALCULATE            | enabled  |
        | NAV_ICON-MENU-RESULTS              | disabled |
        | NAV_ICON-MENU-EXPORT               | enabled  |
        | NAV_ICON-MENU-MANUAL1              | enabled  |
        | NAV_ICON-MENU-SETTINGS             | enabled  |
        | NAV_ICON-MENU-HOTKEYS              | enabled  |
        | NAV_ICON-MENU-MANUAL               | enabled  |
        | NAV_ICON-MENU-LANGUAGE             | enabled  |

    @Smoke
    Scenario: New scenario - Standard behaviour
        Given I login with correct username and password
        And I create an empty scenario from the start page

        # Test privacy warning
        When I select the scenario name
        Then I expect the privacy warning to exist

        # Validate source and buildings panels are closed
        Then the emission source panel should be closed
        And the buildings panel should be closed

        # Delete scenario and validate no scenario's are left
        When I delete the current scenario
        Then I validate the status of the menu items
        | menuId              | status   |
        | NAV_ICON-MENU-HOME  | enabled  |
        | NAV_ICON-MENU-INPUT | disabled |

        # Import file
        When I select file 'AERIUS_everything_import_file.gml' to be imported

        # Change values and then duplicate, to validate whether duplication works as expected and set values are kept
        When I add situation label 'Test-situatie'
        And I select scenarioType 'OFF_SITE_REDUCTION'
        And I set the calculation year to 'minCalculationYear'
        And I fill in netting factor '0.45'
        And I save the scenario updates

        When I duplicate the situation
        Then the situation name is 'Test-situatie (1)'

        When I duplicate the situation
        Then the situation name is 'Test-situatie (2)'
        And the situation type is 'Saldering'
        And the calculation year is 'minCalculationYear'
        And the netting factor is '0.45'

        When I open emission source list
        And I open the buildings panel
        Then the emission source list has 32 sources
        And the buildinglist has 1 buildings
    
    Scenario: Test notification center
        Given I login with correct username and password
        
        # Default notification center state
        Then no notification should be displayed
        And the notification center should be closed

        # Notification center should open when a notification is displayed, and close after a while
        When I select file 'AERIUS_everything_import_file.gml' to be imported
        Then 1 notification should be displayed
        And the notification center should be open
        And I should see the notification 'Eén bestand is succesvol geïmporteerd.' with a date and time
        And the notification center should be closed

        # Delete notification. The notification center should close automatically once it has no notifications
        When I open the notification center
        And I delete the notification 'Eén bestand is succesvol geïmporteerd.'
        Then no notification should be displayed
        And the notification center should be closed

        # Test calculation job notification
        When I navigate to the 'Calculation jobs' tab
        And I create a new calculation job
        And I save the calculation job
        And I start the calculation

        Then 1 notification should be displayed

        When I open the notification center
        Then I should see the notification 'Rekentaak 1 is gestart.' with a date and time

        # Test export notification
        When I navigate to the 'Export' tab
        And I fill in the following email address 'test@aerius.nl'
        And I click on the button to export and don't catch the call
        And I click on the button to export and don't catch the call
        Then 2 notifications should be displayed
        And I should see the notification 'Uw export is succesvol gestart. U ontvangt een e-mail met downloadlink van de bestanden wanneer deze klaar is.' with a date and time

        When I cancel the export
        Then I should see the notification "Uw export '1 situaties' is geannuleerd." with a date and time
        Then 3 notifications should be displayed
        
        # Test clearing all notification
        When I click on remove all notifications
        Then no notification should be displayed
        And the notification center should be closed

    Scenario: Source characteristics - Standard behaviour
        Given I login with correct username and password
        And I create a new source from the start page
        Then I select a source and validate default characteristics
            | sectorGroup     | sector                     | emissionHeight | emissionHeatContent | diurnalVariationType                              |
            | Energie         |                            |             40 |                0.22 | Standaard Profiel Industrie                       |
            | Landbouw        | Dierhuisvesting            |              5 |                   0 | Dierverblijven                                    |
            | Landbouw        | Mestopslag                 |            1.5 |                   0 | Dierverblijven                                    |
            | Landbouw        | Landbouwgrond              |            0.5 |                   0 | Meststoffen                                       |
            | Landbouw        | Glastuinbouw               |              9 |                0.24 | Verwarming van Ruimten (Zonder Seizoenscorrectie) |
            | Landbouw        | Vuurhaarden, overig        |              9 |                   0 | Verwarming van Ruimten (Zonder Seizoenscorrectie) |
            | Verkeer         | Koude start: parkeergarage |            0.3 |                   0 | Licht Verkeer                                     |
            | Industrie       | Afvalverwerking            |              6 |                 0.1 | Continue Emissie                                  |
            | Industrie       | Voedings- en genotmiddelen |             15 |                0.34 | Standaard Profiel Industrie                       |
            | Industrie       | Chemische industrie        |             12 |                0.13 | Standaard Profiel Industrie                       |
            | Industrie       | Bouwmaterialen             |             17 |                0.44 | Standaard Profiel Industrie                       |
            | Industrie       | Basismetaal                |             13 |                0.05 | Standaard Profiel Industrie                       |
            | Industrie       | Metaalbewerkingsindustrie  |             10 |                   0 | Standaard Profiel Industrie                       |
            | Industrie       | Overig                     |             22 |                0.28 | Standaard Profiel Industrie                       |
            | Wonen en Werken | Woningen                   |              1 |               0.002 | Continue Emissie                                  |
            | Wonen en Werken | Recreatie                  |              1 |               0.002 | Continue Emissie                                  |
            | Wonen en Werken | Kantoren en winkels        |             11 |               0.014 | Standaard Profiel Industrie                       |
            | Railverkeer     | Spoorweg                   |              5 |                   0 | Standaard Profiel Industrie                       |
            | Railverkeer     | Emplacement                |            3.5 |                 0.2 | Standaard Profiel Industrie                       |
            | Luchtverkeer    | Stijgen                    |            450 |                   0 | Continue Emissie                                  |
            | Luchtverkeer    | Landen                     |            450 |                   0 | Continue Emissie                                  |
            | Luchtverkeer    | Taxiën                     |              6 |                0.04 | Continue Emissie                                  |
            | Luchtverkeer    | Bronnen luchthaventerrein  |              6 |                0.04 | Continue Emissie                                  |
            | Anders...       |                            |              0 |                   0 | Continue Emissie                                  |
