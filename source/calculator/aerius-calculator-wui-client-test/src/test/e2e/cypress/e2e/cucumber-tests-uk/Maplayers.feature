#
# Crown Copyright
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Maplayers

    Scenario: Total %CL and %CL maplayers should be visible
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select the mapbutton 'LayerPanel'
        Then the 'Total as percentage of critical level / critical load' maplayer should be visible
        Then the 'Percentage critical level / critical load' maplayer should be visible

    Scenario: Check map layer legend values
        Given I login with correct username and password

        # Import file with results so the `Calculation results` maplayer has a legend
        When I select file 'UKAPAS_in_combination_results.gml' to be imported
        And I select the mapbutton 'LayerPanel'

        # Calculation results (increase only)
        When I open maplayer 'Calculation results'
        Then I expect the map layer 'Calculation results' to have an empty legenda

        # Calculation results (increase and decrease)
        When I navigate to the 'Results' tab
        And I wait for the results spinner to resolve
        Then I expect the map layer 'Calculation results' to have title 'Deposition (kg N/ha/y)' with the following values
            | value              |
            | < -30.000          |
            | -30.000 to -20.000 |
            | -20.000 to -15.000 |
            | -15.000 to -10.000 |
            | -10.000 to -5.000  |
            | -5.000 to 0.000    |
            | 0.000 to 5.000     |
            | 5.000 to 10.000    |
            | 10.000 to 15.000   |
            | 15.000 to 20.000   |
            | 20.000 to 30.000   |
            | ≥ 30.000           |

        # PEC as percentage of critical level / critical load
        When I open maplayer 'PEC as percentage of critical level / critical load'
        Then I expect the map layer 'PEC as percentage of critical level / critical load' to have title 'PEC below 80% of CL is not represented on the map' with the following values
            | value                                     |
            | PEC nearing exceedance (80 to 100% of CL) |
            | PEC exceeding CL (≥ 100% of CL)           |

        # Road transport network
        When I open maplayer 'Road transport network'
        Then I expect the map layer 'Road transport network' for data type 'Road type' to have the following values
            | value                 |
            | Motorway (not London) |
            | Rural (not London)    |
            | Urban (not London)    |
            | London - Central      |
            | London - Inner        |
            | London - Outer        |
            | London - Motorway     |
            | Unknown               |
        And I expect the map layer 'Road transport network' for data type 'Link speed' to have title 'km/h' and the following values
            | value     |
            | 0 to 5    |
            | 5 to 50   |
            | 50 to 95  |
            | 95 to 140 |
            | ≥ 140     |
        And I expect the map layer 'Road transport network' for data type 'Traffic volume (per 24 hrs)' and 'Total vehicle movements' to have title 'Vehicle movements per day' and the following values
            | value            |
            | 0 to 40000       |
            | 40000 to 80000   |
            | 80000 to 120000  |
            | 120000 to 160000 |
            | ≥ 160000         |
        And I expect the map layer 'Road transport network' for data type 'Traffic volume (per 24 hrs)' and 'Car' to have title 'Vehicle movements per day' and the following values
            | value           |
            | 0 to 30000      |
            | 30000 to 60000  |
            | 60000 to 90000  |
            | 90000 to 120000 |
            | ≥ 120000        |
        And I expect the map layer 'Road transport network' for data type 'Traffic volume (per 24 hrs)' and 'Taxi (black cab)' to have title 'Vehicle movements per day' and the following values
            | value       |
            | 0 to 250    |
            | 250 to 500  |
            | 500 to 750  |
            | 750 to 1000 |
            | ≥ 1000      |
        And I expect the map layer 'Road transport network' for data type 'Traffic volume (per 24 hrs)' and 'Motorcycle' to have title 'Vehicle movements per day' and the following values
            | value       |
            | 0 to 250    |
            | 250 to 500  |
            | 500 to 750  |
            | 750 to 1000 |
            | ≥ 1000      |
        And I expect the map layer 'Road transport network' for data type 'Traffic volume (per 24 hrs)' and 'LGV' to have title 'Vehicle movements per day' and the following values
            | value          |
            | 0 to 5000      |
            | 5000 to 10000  |
            | 10000 to 15000 |
            | 15000 to 20000 |
            | ≥ 20000        |
        And I expect the map layer 'Road transport network' for data type 'Traffic volume (per 24 hrs)' and 'HGV' to have title 'Vehicle movements per day' and the following values
            | value         |
            | 0 to 3000     |
            | 3000 to 6000  |
            | 6000 to 9000  |
            | 9000 to 12000 |
            | ≥ 12000       |
        And I expect the map layer 'Road transport network' for data type 'Traffic volume (per 24 hrs)' and 'Bus and Coach' to have title 'Vehicle movements per day' and the following values
            | value        |
            | 0 to 500     |
            | 500 to 1000  |
            | 1000 to 1500 |
            | 1500 to 2000 |
            | ≥ 2000       |
        And I expect the map layer 'Road transport network' for data type 'Road barrier' and 'Type of barrier' to have the following values
            | value                   |
            | Noise barrier           |
            | Brick wall              |
            | Terraced properties     |
            | Semidetached properties |
            | Detached properties     |
            | Dense vegetation        |
            | Open vegetation         |
            | Other                   |
            | Traffic network         |
        And I expect the map layer 'Road transport network' for data type 'Road barrier' and 'Average height' to have title 'm' and the following values
            | value           |
            | 0 to 3          |
            | 3 to 6          |
            | ≥ 6             |
            | Traffic network |
        And I expect the map layer 'Road transport network' for data type 'Road barrier' and 'Porosity' to have title '%' and the following values
            | value           |
            | 0 to 5          |
            | 5 to 20         |
            | 20 to 40        |
            | 40 to 50        |
            | 50 to 80        |
            | 80 to 95        |
            | ≥ 95            |
            | Traffic network |
        And I expect the map layer 'Road transport network' for data type 'Emissions' and 'NOx' to have title 'g/km/s' and the following values
            | value      |
            | 0 to 0.1   |
            | 0.1 to 0.2 |
            | 0.2 to 0.3 |
            | 0.3 to 0.4 |
            | ≥ 0.4      |
        And I expect the map layer 'Road transport network' for data type 'Emissions' and 'NH3' to have title 'g/km/s' and the following values
            | value        |
            | 0 to 0.01    |
            | 0.01 to 0.02 |
            | 0.02 to 0.03 |
            | 0.03 to 0.04 |
            | ≥ 0.04       |

        # Nitrogen-sensitivity relevant habitat and species
        When I open maplayer 'Nitrogen-sensitivity relevant habitat and species'
        Then I expect the map layer 'Nitrogen-sensitivity relevant habitat and species' for data type 'Deposition NOₓ + NH₃' to have title 'Critical load, kg N/ha/y' and the following values
            | value |
            | 3     |
            | 5     |
            | 8     |
            | 10    |
            | 15    |
            | 20    |
        And I expect the map layer 'Nitrogen-sensitivity relevant habitat and species' for data type 'Concentration NOₓ' to have title 'Critical level, µg/m³' and the following values
            | value |
            | 30    |
        And I expect the map layer 'Nitrogen-sensitivity relevant habitat and species' for data type 'Concentration NH₃' to have title 'Critical level, µg/m³' and the following values
            | value |
            | 1     |
            | 3     |

        # Ecological sites (NOTE: The extra whitespace is unfortunately required to make the tests run)
        When I open maplayer 'Ecological sites'
        Then I expect the map layer 'Ecological sites' for data type 'Show all' to have the following values
            | value                                                                                                              |
            | Special Area of Conservation                                                                                       |
            | Special Protection Area                                                                                            |
            | Site of Special Scientific Interest or Area of                                         Special Scientific Interest |
            | Unassigned                                                                                                         |
        And I expect the map layer 'Ecological sites' for data type 'Special Area of Conservation' to have the following values
            | value                                                                                                              |
            | Special Area of Conservation                                                                                       |
            | Special Protection Area                                                                                            |
            | Site of Special Scientific Interest or Area of                                         Special Scientific Interest |
            | Unassigned                                                                                                         |
        And I expect the map layer 'Ecological sites' for data type 'Special Protection Area' to have the following values
            | value                                                                                                              |
            | Special Area of Conservation                                                                                       |
            | Special Protection Area                                                                                            |
            | Site of Special Scientific Interest or Area of                                         Special Scientific Interest |
            | Unassigned                                                                                                         |
        And I expect the map layer 'Ecological sites' for data type 'Site of Special Scientific Interest or Area of                                         Special Scientific Interest' to have the following values
            | value                                                                                                              |
            | Special Area of Conservation                                                                                       |
            | Special Protection Area                                                                                            |
            | Site of Special Scientific Interest or Area of                                         Special Scientific Interest |
            | Unassigned                                                                                                         |
        And I expect the map layer 'Ecological sites' for data type 'Unassigned' to have the following values
            | value                                                                                                              |
            | Special Area of Conservation                                                                                       |
            | Special Protection Area                                                                                            |
            | Site of Special Scientific Interest or Area of                                         Special Scientific Interest |
            | Unassigned                                                                                                         |

    Scenario: Validate maps
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select the mapbutton 'LayerPanel'
        Then I validate the map layers
            | id                                                                                        | title                                               |
            | TotalPercentageCriticalLevelResultLayer                                                   | PEC as percentage of critical level / critical load |
            | PercentageCriticalLevelResultLayer                                                        | Percentage critical level / critical load           |
            | HabitatAreasSensitivityLevelLayer                                                         | Nitrogen-sensitivity relevant habitat and species   |
            | nl.overheid.aerius.wui.application.geo.layers.results.CalculationMarkersLayer             | Markers                                             |
            | nl.overheid.aerius.wui.application.geo.layers.results.CalculationResultLayerGroup         | Calculation results                                 |
            | nl.overheid.aerius.wui.application.geo.layers.calculationpoint.CalculationPointLayerGroup | Input - assessment points                           |
            | nl.overheid.aerius.wui.application.geo.layers.source.RoadSourceGeometryLayer              | Road transport network                              |
            | nl.overheid.aerius.wui.application.geo.layers.job.MetSiteLayerGroup                       | Meteorological locations                            |
            | nl.overheid.aerius.wui.application.geo.layers.results.ArchiveProjectMarkersLayer          | In-combination Archive projects                     |
            | nl.overheid.aerius.wui.application.geo.layers.job.JobInputLayerGroup                      | Input - calculation job                             |
            | nl.overheid.aerius.wui.application.geo.layers.source.SourceLayerGroup                     | Input - scenario                                    |
            | nl.overheid.aerius.wui.application.geo.layers.BackgroundResultLayer                       | Background                                          |
            | calculator:wms_habitat_types                                                              | Habitat types                                       |
            | nl.overheid.aerius.wui.application.geo.layers.NatureAreasLayer                            | Ecological sites                                    |
            | nl.overheid.aerius.wui.application.geo.layers.zone.ZoneOfInfluenceLayer-title             | Zone of influence                                   |
            | nl.overheid.aerius.wui.application.geo.layers.zone.CalculationBoundaryLayer-title         | Calculation boundary                                |
            | RoadOnDemand                                                                              | Base Layer                                          |
        Then I validate the base layers
            | id                                 | title                        | copyright                                                             |
            | RoadOnDemand                       | Base Layer                   | Microsoft Corporation©                                                |
            | AerialWithLabelsOnDemand           | Base Layer (Aerial)          | Microsoft CorporationEarthstar Geographics  SIO©                      |
            | OrdnanceSurvey                     | Base Layer (Ordnance Survey) | Terms of Use                                                          |
            | RasterBasemaps_OSNI_Discoverer_50K | Base Layer (OSNI Discoverer) | Land & Property Services © Crown Copyright Licence No. NIMA S&LA577.2 |
            | VectorBasemaps_OSNIFusionBasemap   | Base Layer (OSNI Fusion)     | Land & Property Services © Crown Copyright Licence No. NIMA S&LA577.2 |
        Then I validate the critical level subselections contains filter
            | title                                     | cqlFilter                                                                                             |
            | PEC nearing exceedance (80 to 100% of CL) | NOT (total_percentage_cl >%3D 80 AND total_percentage_cl < 80)                                        |
            | PEC exceeding CL (≥ 100% of CL)           | NOT (total_percentage_cl >%3D 80 AND total_percentage_cl < 80) AND NOT (total_percentage_cl >%3D 100) |

    Scenario: Test default map layers for each menu item that has a map
        Given I login with correct username and password

        # Import file with results, so we can test the layers for the result panel as well
        When I select file 'UKAPAS_in_combination_results_without_archive_results.zip' to be imported
        And I select the mapbutton 'LayerPanel'
        Then the following maplayers should be visible
            | layer                                               | shouldBeEnabled |
            | PEC as percentage of critical level / critical load | no              |
            | Percentage critical level / critical load           | no              |
            | Nitrogen-sensitivity relevant habitat and species   | no              |
            | Markers                                             | no              |
            | Calculation results                                 | no              |
            | Input - assessment points                           | no              |
            | Road transport network                              | yes             |
            | Meteorological locations                            | no              |
            | In-combination Archive projects                     | no              |
            | Input - calculation job                             | no              |
            | Input - scenario                                    | yes             |
            | Background                                          | no              |
            | Habitat types                                       | no              |
            | Ecological sites                                    | yes             |
            | Zone of influence                                   | no              |
            | Calculation boundary                                | yes             |
        
        When I navigate to the 'Assessment points' tab
        Then the following maplayers should be visible
            | layer                                               | shouldBeEnabled |
            | PEC as percentage of critical level / critical load | no              |
            | Percentage critical level / critical load           | no              |
            | Nitrogen-sensitivity relevant habitat and species   | no              |
            | Markers                                             | no              |
            | Calculation results                                 | no              |
            | Input - assessment points                           | yes             |
            | Road transport network                              | no              |
            | Meteorological locations                            | no              |
            | In-combination Archive projects                     | no              |
            | Input - calculation job                             | no              |
            | Input - scenario                                    | no              |
            | Background                                          | no              |
            | Habitat types                                       | no              |
            | Ecological sites                                    | yes             |
            | Zone of influence                                   | no              |
            | Calculation boundary                                | yes             |

        When I navigate to the 'Results' tab
        Then the following maplayers should be visible
            | layer                                               | shouldBeEnabled |
            | PEC as percentage of critical level / critical load | no              |
            | Percentage critical level / critical load           | no              |
            | Nitrogen-sensitivity relevant habitat and species   | no              |
            | Markers                                             | no              |
            | Calculation results                                 | yes             |
            | Input - assessment points                           | no              |
            | Road transport network                              | no              |
            | Meteorological locations                            | no              |
            | In-combination Archive projects                     | yes             |
            | Input - calculation job                             | yes             |
            | Input - scenario                                    | no              |
            | Background                                          | no              |
            | Habitat types                                       | no              |
            | Ecological sites                                    | yes             |
            | Zone of influence                                   | yes             |
            | Calculation boundary                                | yes             |
        
        # Validate turning off the 'Automatically turn layers on or off' preference stops the maplayers from updating
        When I navigate to the 'Preferences' tab
        And I change the setting for 'Automatically turning layers on or off' to 'off'
        And I navigate to the 'Input' tab
        Then the following maplayers should be visible
            | layer                                               | shouldBeEnabled |
            | PEC as percentage of critical level / critical load | no              |
            | Percentage critical level / critical load           | no              |
            | Nitrogen-sensitivity relevant habitat and species   | no              |
            | Markers                                             | no              |
            | Calculation results                                 | yes             |
            | Input - assessment points                           | no              |
            | Road transport network                              | no              |
            | Meteorological locations                            | no              |
            | In-combination Archive projects                     | yes             |
            | Input - calculation job                             | yes             |
            | Input - scenario                                    | no              |
            | Background                                          | no              |
            | Habitat types                                       | no              |
            | Ecological sites                                    | yes             |
            | Zone of influence                                   | yes             |
            | Calculation boundary                                | yes             |
