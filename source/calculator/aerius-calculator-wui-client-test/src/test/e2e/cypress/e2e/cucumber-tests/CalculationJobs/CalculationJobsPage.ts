/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class CalculationJobsPage {
  static assertDataShownInScenarioOverview(dataTable: any) {
    let index = 0; // TODO: Make sure ordering is NOT random

    // TODO: Test ID's should be added and used here
    dataTable.hashes().forEach((element: any) => {
      cy.get(".situationsContainer > table > tbody > tr").eq(index).find("td").eq(1).should("have.text", element.name);
      cy.get(".situationsContainer > table > tbody > tr").eq(index).find("td").eq(2).should("have.text", element.situationType);
      cy.get(".situationsContainer > table > tbody > tr").eq(index).find("td").eq(3).should("have.text", element.year);
      cy.get(".situationsContainer > table > tbody > tr").eq(index).find("td").eq(4).should("have.text", element.nettingFactor);
      cy.get(".situationsContainer > table > tbody > tr").eq(index).find("td").eq(5).should("have.text", element.emissionSources);
      cy.get(".situationsContainer > table > tbody > tr").eq(index).find("td").eq(6).should("have.text", element.emissionNOx);
      cy.get(".situationsContainer > table > tbody > tr").eq(index).find("td").eq(7).should("have.text", element.emissionNH3);
      index++;
    });
  }

  static assertScenariosAreSelected(dataTable: any) {
    dataTable.hashes().forEach((row: any) => {
      cy.get("td").contains(row.scenarioName).parent().should("have.class", "highlighted");
    });
  }

  static assertNoScenariosSelected() {
    cy.get("td.selected").should("not.exist");
  }

  static selectSituation(situationType: string, situationName: string) {
    switch (situationType) {
      case "PROPOSED":
        cy.get(".collapsible-item.open").find('[id="proposedSituation"]').select(situationName);
        return;
      case "REFERENCE":
        cy.get(".collapsible-item.open").find('[id="referenceSituation"]').select(situationName);
        return;
      case "OFF_SITE_REDUCTION":
        cy.get(".collapsible-item.open").find('[id="off_site_reductionSituation"]').select(situationName);
        return;
      case "TEMPORARY":
        cy.get(".collapsible-item.open").find('[id="temporarySituation"]').select(situationName);
        return;
      default:
        cy.get(".collapsible-item.open").find('[id="situation"]').select(situationName);
    }
  }

  static assertSelectedScenarioIs(situationType: string, situationName: string) {
    switch (situationType) {
      case "PROPOSED":
        cy.get('[id="proposedSituation"]').find("option:selected").should("have.text", situationName);
        return;
      case "REFERENCE":
        cy.get('[id="referenceSituation"]').find("option:selected").should("have.text", situationName);
        return;
      case "OFF_SITE_REDUCTION":
        cy.get('[id="off_site_reductionSituation"]').find("option:selected").should("have.text", situationName);
        return;
      default:
        throw new Error("Unknown situationType: " + situationType);
    }
  }

  static assertDuplicateCalculationJobWarningIsShown(shouldBeShown: boolean) {
    if (shouldBeShown) {
      cy.get(".noticeContainer.warning").should("be.visible"); // TODO: Add test ID
    } else {
      cy.get(".noticeContainer.warning").should("not.exist"); // TODO: Add test ID
    }
  }

  static assertCalculationDoesNotExist(calculationJob: number) {
    cy.get('[id="calculationJob-' + calculationJob + '"]').should("not.exist");
  }

  static changeCalculationJobNameTo(name: string) {
    cy.get("#label").clear().type(name);
  }

  static assertCalculationJobWithTitleExists(title: string) {
    cy.get(".labelColumn").should("contain.text", title);
  }

  static selectCalculationPointsOption(calculationPointsOption: string) {
    cy.get(".collapsible-item.open").find('[id="scenarioResultType"]').select(calculationPointsOption);
  }

  static assertSituationDoesNotExist(situationType: string) {
    switch (situationType) {
      case "PROPOSED":
        cy.get('[id="proposedSituation"]').should("not.exist");
        return;
      case "TEMPORARY-LISTBOX":
        cy.get('[id="temporarySituation"]').should("not.exist");
        return;
      case "TEMPORARY-CHECKBOX":
        cy.get('[id^="otherSituation-TEMPORARY-"]').should("not.exist");
        return;
      default:
        throw new Error("Unknown situationType should not be visible: " + situationType);
    }
  }

  static assertCalculationNotSaved() {
    cy.get('[id="buttonSaveCalculationJobBottomButtons"]').should("have.class", "prevented");
  }

  static assertErrorContainerIsShown() {
    cy.get("#inputErrorMessage").should("be.visible");
  }

  static assertSelectedBaseCalculationJobIs(expectedBaseCalculationJob: string) {
    cy.get('[id="selectedJob"] > option:selected').should("have.text", expectedBaseCalculationJob);
  }

  static setBaseCalculationJob(calculationJob: string) {
    cy.get('[id="selectedJob"]').select(calculationJob);
  }

  static assertSelectedSituationIs(situationType: string, situationName: string) {
    switch (situationType) {
      case "PROPOSED":
        cy.get('[id="proposedSituation"]').find("option:selected").should("have.text", situationName);
        return;
      case "REFERENCE":
        cy.get('[id="exportReferenceSituation"]').find("option:selected").should("have.text", situationName);
        return;
      case "OFF_SITE_REDUCTION":
        cy.get('[id="exportNettingSituation"]').find("option:selected").should("have.text", situationName);
        return;
      default:
        throw new Error("Unknown situationType: " + situationType);
    }
  }

  static assertSituationsAreSelected(dataTable: any) {
    dataTable.hashes().forEach((situation: any) => {
      if (situation.shouldBeSelected == "true") {
        cy.get('[id="situationSummaryTable"] [id^="situationRow-"] > [id="situationName"]')
          .contains(situation.name)
          .parent()
          .should("have.class", "highlighted");
      } else {
        cy.get('[id="situationSummaryTable"] [id^="situationRow-"] > [id="situationName"]')
          .contains(situation.name)
          .parent()
          .should("not.have.class", "highlighted");
      }
    });
  }
}
