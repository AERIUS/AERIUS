/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { CommonPage } from "../../common/CommonPage";
import { CommonSettings } from "../../common/CommonSettings";

export class ImportFilesPage {
  static validateImportButtons(dataTable: any) {
    dataTable.hashes().forEach((importButton: any) => {
      if (importButton.status == "enabled") {
        cy.get('[id="'+ importButton.id + '"]').should("not.be.disabled");
      } else if (importButton.status == "disabled") {
        cy.get('[id="'+ importButton.id + '"]').should("be.disabled");
      } else {
        throw new Error("Unknown button status: " + importButton.status);
      }
    });
  }

  static verifyAdvancedImportTurnedOn() {
    cy.get(".fileContainer .line").find(".situationName").should("exist");
    cy.get(".fileContainer .line").find('[id="importActions"]').should("exist");
    cy.get(".fileContainer .line").find('[id="situationSelection"]').should("exist");
  }

  static verifyAdvancedImportTurnedOff() {
    cy.get(".fileContainer .line").find(".situationName").should("not.exist");
    cy.get(".fileContainer .line").find('[id="importActions"]').should("not.exist");
    cy.get(".fileContainer .line").find('[id="situationSelection"]').should("not.exist");
  }

  static validateImportListDefaultOptions(dataTable: any) {
    dataTable.hashes().forEach((row: any) => {
      // Get correct line
      cy.get(".fileContainer .situationName")
        .filter((index, element) => {
          return Cypress.$(element).text() === row.situationName;
        })
        .parent()
        .as("line");

      // Test
      if (row.importType === "scenario") {
        cy.get("@line").find('[id="importActions"]').should("contain.text", row.importOptions);
        cy.get("@line").find('[id="situationSelection"]').should("contain.text", row.situationType);
      } else if (row.importType === "calculationPoints") {
        cy.get("@line").find('[id="importActions"]').should("contain.text", row.importOptions);
        cy.get("@line").find('[id="situationSelection"]').should("not.exist");
      } else {
        throw new Error("Unknown importType: " + row.importType);
      }
    });
  }

  static assertUsageDropdownOptionShown(expectedOption: string, header: string) {
    if (header === "") {
      cy.get('[id="importActions"] > option').should("contain.text", expectedOption);
    } else {
      cy.get('[id="importActions"] > optgroup[label="' + header + '"] > option').should("contain.text", expectedOption);
    }
  }

  static assertSelectedUsageIs(expectedOption: string) {
    cy.get('[id="importActions"] option:selected').invoke("text").should("equal", expectedOption);
  }

  static assertUsageDropdownOptionShownAdvanced(scenarioName: string, fileName: string, option: string, header: string) {
    if (header === "") {
      cy.get(".fileContainer .situationName")
        .filter((index, element) => {
          return Cypress.$(element).text() === scenarioName && Cypress.$(element).parent().find(".name").text() === fileName;
        })
        .parent()
        .find('[id="importActions"] > option')
        .should("contain.text", option);
    } else {
      cy.get(".fileContainer .situationName")
        .filter((index, element) => {
          return Cypress.$(element).text() === scenarioName && Cypress.$(element).parent().find(".name").text() === fileName;
        })
        .parent()
        .find('[id="importActions"] optgroup[label="' + header + '"] > option')
        .should("contain.text", option);
    }
  }

  static assertSelectedSituationTypeIs(expectedOption: string) {
    cy.get('[id="situationSelection"] option:selected').invoke("text").should("equal", expectedOption);
  }

  static assertSelectedSituationTypeForLineIs(scenarioName: string, fileName: string, expectedOption: string) {
    cy.get(".fileContainer .situationName")
      .filter((index, element) => {
        return Cypress.$(element).text() === scenarioName && Cypress.$(element).parent().find(".name").text() === fileName;
      }).parent().find(".situationLabel").should("have.text", expectedOption);
  }

  static assertFileIsInToBeImportedList(fileName: string) {
    cy.get(".fileContainer .name").contains(fileName).should("exist");
  }

  static assertFileIsNotInToBeImportedList(fileName: string) {
    cy.get(".fileContainer .name").contains(fileName).should("not.exist");
  }

  static assertImportFileLineIsExpanded(fileName: string) {
    cy.get(".fileContainer .name").contains(fileName).parent().find(".collapsible").should("not.have.class", "expanded");
    cy.get(".fileContainer .name").contains(fileName).parent().parent().parent().parent().find(".statsContainer").should("not.exist");
  }

  static assertImportFileLineIsNotExpanded(fileName: string) {
    cy.get(".fileContainer .name").contains(fileName).parent().find(".collapsible").should("have.class", "expanded");
    cy.get(".fileContainer .name").contains(fileName).parent().parent().parent().parent().find(".statsContainer").should("exist");
  }

  static toggleLineFor(fileName: string) {
    cy.get('[title="' + fileName + '"]').click();
  }

  static selectSituationTypeForImportLine(situationType: string, scenarioName: string, fileName: string) {
    cy.get(".fileContainer .situationName")
      .filter((index, element) => {
        return Cypress.$(element).text() === scenarioName && Cypress.$(element).parent().find(".name").text() === fileName;
      })
      .parent()
      .find('[id="situationSelection"]')
      .select(situationType);
  }

  static removeFileFromImportList(fileName: string) {
    cy.get(".fileContainer .name").contains(fileName).parent().parent().parent().find(".removeContainer").click();
  }

  static assertImportFileDetailContains(fileName: string, expectedString: string) {
    cy.get(".fileContainer .name")
      .contains(fileName)
      .parent()
      .parent()
      .parent()
      .parent()
      .find(".statsContainer")
      .should("contain.text", expectedString);
  }

  static assertImportFileDetailDoesNotContain(fileName: string, unexpectedString: string) {
    cy.get(".fileContainer .name")
      .contains(fileName)
      .parent()
      .parent()
      .parent()
      .parent()
      .find(".statsContainer")
      .should("not.contain.text", unexpectedString);
  }

  static assertAdvancedImportFileDetailDoesNotContain(scenarioName: string, fileName: string, expectedString: string) {
    cy.get(".fileContainer .situationName")
      .filter((index, element) => {
        return Cypress.$(element).text() === scenarioName && Cypress.$(element).parent().find(".name").text() === fileName;
      })
      .parent()
      .parent()
      .parent()
      .find(".statsContainer")
      .should("not.contain.text", expectedString);
  }

  static assertAdvancedImportFileDetailDoesNotExist(scenarioName: string, fileName: string) {
    cy.get(".fileContainer .situationName")
      .filter((index, element) => {
        return Cypress.$(element).text() === scenarioName && Cypress.$(element).parent().find(".name").text() === fileName;
      })
      .parent()
      .parent()
      .parent()
      .find(".statsContainer")
      .should("not.exist");
  }

  static selectUsageForImportLine(usageScenario: string, scenarioName: string, fileName: string) {
    cy.get(".fileContainer .situationName")
      .filter((index, element) => {
        return Cypress.$(element).text() === scenarioName && Cypress.$(element).parent().find(".name").text() === fileName;
      })
      .parent()
      .find('[id="importActions"]')
      .select(usageScenario);
  }

  static downloadErrorsAndWarningsAndValidateCSVHasBeenDownloaded() {
    cy.get(".downloadLog").click();
    cy.task("readdir", { path: "cypress/downloads/", setTimeout: CommonSettings.fileioTimeOut }).then((files: any) => {
      files.forEach((fileName: any) => {
        expect(fileName.indexOf(".csv")).to.be.greaterThan(-1);
      });
    });
  }

  static tryToImportTheFile() {
    cy.get('[id="buttonSaveimport"]').click();
  }

  static selectBrnFileForImport(fileName: string) {
    const updatedFileName: string = CommonPage.getUpdatedFileName(fileName);
    const fileInput: any = cy.get('[id="fileInput"]');
    fileInput.selectFile(updatedFileName, { force: true });
  }

  static selectBrnFileForImportWithPollutant(fileName: string, pollutant: string, andImport: string) {
    cy.intercept("/api/v8/ui/import/*/validationresult").as("validationResult");
    this.selectBrnFileForImport(fileName);
    this.selectBrnPollutant(pollutant);
    CommonPage.waitForUploadToFinish();

    if (andImport) {
      // Upload imported file
      cy.get('[id="buttonSaveimport"]').click();
      cy.get("@validationResult").its("response.statusCode").should("eq", 200);
      // Wait for the pop-up message to appear so the buttons have time to become active
      cy.get(".message");
    }
  }

  static selectBrnPollutant(pollutant: string) {
    switch (pollutant) {
      case "NOx":
        cy.get('[id="substanceListInline"]').select("Substance [NOx:11]");
        break;
      case "NH3":
        cy.get('[id="substanceListInline"]').select("Substance [NH3:17]");
        break;
      default:
        throw new Error("Unknown pollutant: " + pollutant);
    }
  }

  static assertColumnIsEmpty(column: string) {
    switch (column) {
      case "Naam":
        cy.get(".situationName").should("have.text", "");
        break;
      case "Gebruik":
        cy.get('[id="importActions"]').should("not.exist");
        break;
      case "Situatie type":
        cy.get('[id="situationSelection"]').should("not.exist");
        break;
      default:
        throw new Error("Unknown column: " + column);
    }
  }

  static assertColumnIsNotEmpty(column: string) {
    switch (column) {
      case "Naam":
        cy.get(".situationName").should("not.have.text", "");
        break;
      case "Gebruik":
        cy.get('[id="importActions"]').should("exist");
        break;
      case "Situatie type":
        cy.get('[id="situationSelection"]').should("exist");
        break;
      default:
        throw new Error("Unknown column: " + column);
    }
  }

  static assertPollutantDropdownIsVisible() {
    cy.get('[id="substanceListInline"]').should("contain.text", "NOₓ").should("contain.text", "NH₃");
  }

  static assertErrorsAreShown() {
    cy.get(".fileContainer").should("have.class", "error");
    cy.get('[id="buttonSaveimport"]').should("have.class", "prevented");
    cy.get(".errorContainer").should("exist");
  }

  static assertImportScreenIsEmpty() {
    cy.get(".fileContainer").should("not.exist");
    cy.get(".errorContainer").should("not.exist");
  }

  static saveDefaultCalculationYear() {
    cy.get('[id="sourceListSituationYear"]').find("option:selected").invoke('text').then(($year) => {
      cy.wrap($year).as("situationYear");
    });
  }

  static assertCalculationYearIsTheDefault() {
    cy.get("@situationYear").then((situationYear) => {
      cy.get('[id="sourceListSituationYear"]').find("option:selected").should("have.text", situationYear);
    });
  }
}
