/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

class ImportFilesPage {
  static assertImportErrorsShown() {
    cy.get(".errorContainer").should("be.visible");
    cy.get('[id="fileErrorIconButton"]').should("be.visible");

    this.assertErrorTabHasSpatialReferenceSystemErrors();
  }

  static assertErrorTabHasSpatialReferenceSystemErrors() {
    cy.get('[id="tab-ERRORS"]').click();
    cy.get(".errors").should("exist");
    cy.get(".errors").children().should("have.length.above", 0);
    cy.get(".errors").children().contains("unsupported Spatial Reference System");
    cy.get('[id="tab-FILES"]').click();
  }

  static assertIntroductionScreenIsShown() {
    cy.get(".introductionContainer").should("be.visible");
  }

  static checkIdContainsText(id: string, expectedContent: string, elementHasToExist: boolean) {
    // Don't check for optional content if the cell is empty
    if (elementHasToExist || expectedContent != "") {
      cy.get('[id="' + id + '"]').should("have.text", expectedContent);
    }

    /*
     * TODO: There's no checking for elements being non existent or just not visible if the element doesn't have to exist.
     * This is because this is very inconsistent.
     */
  }

  static selectSource(sourceName: string) {
    // TODO: This parent element should have a testID.
    cy.get(".sources").contains(sourceName).scrollIntoView().click({ force: true });
  }

  static checkSourceADMSDetails(dataTable: any) {
    dataTable.hashes().forEach((dataRow: any) => {
      this.selectSource(dataRow.name);

      // General
      this.checkIdContainsText("sourceListDetailTitle", dataRow.name, true);
      this.checkIdContainsText("sourceListDetailSectorGroup", dataRow.sectorGroup, true);
      this.checkIdContainsText("sourceListDetailSector", dataRow.sector, false);
      this.checkIdContainsText("sourceListDetailLocation", dataRow.location, true);
      this.checkIdContainsText("sourceListDetailLocationStatistic", dataRow.locationDetail, false);

      // Characteristics
      this.checkIdContainsText("sourceADMSSourceTypeLabels", dataRow.sourceType, true);
      this.checkIdContainsText("sourceADMSElevationAngle", dataRow.elevationAngle, false);
      this.checkIdContainsText("sourceADMSHorizontalAngle", dataRow.horizontalAngle, false);
      this.checkIdContainsText("sourceADMSVerticalDimension", dataRow.verticalDimension, false);
      this.checkIdContainsText("sourceADMSHeight", dataRow.sourceHeight, true);
      this.checkIdContainsText("sourceADMSDiameter", dataRow.sourceDiameter, false);
      this.checkIdContainsText("sourceADMSWidth", dataRow.sourceWidth, false);
      this.checkIdContainsText("sourceADMSBuoyancyTypeDetailLabel", dataRow.buoyancy, false);
      this.checkIdContainsText("sourceADMSTemperature", dataRow.temperature, false);
      this.checkIdContainsText("sourceADMSDensity", dataRow.density, false);
      this.checkIdContainsText("sourceADMSEffluxTypeLabels", dataRow.effluxType, false);
      this.checkIdContainsText("sourceADMSVerticalVelocity", dataRow.velocity, false);
      this.checkIdContainsText("sourceADMSVolumetricFlowRate", dataRow.volumeticFlowRate, false);
      this.checkIdContainsText("sourceADMSSpecificHeatCapacity", dataRow.specificHeatCapacity, false);

      /*
       * TODO: There are no specific test ID's for each substance yet.
       * Once these have been added, emission checking should be added here as well.
       */

      // Time-varying profile
      this.checkIdContainsText("timeVaryingProfileName-THREE_DAY", dataRow.timeVaryingProfileName_THREE_DAY, true);
      this.checkIdContainsText("timeVaryingProfileType-THREE_DAY", dataRow.timeVaryingProfileType_THREE_DAY, true);
      this.checkIdContainsText("timeVaryingProfileName-MONTHLY", dataRow.timeVaryingProfileName_MONTHLY, true);
      this.checkIdContainsText("timeVaryingProfileType-MONTHLY", dataRow.timeVaryingProfileType_MONTHLY, true);

    });
  }

  static openRoadTransportNetworkPanel() {
    /*
     * TODO: There is a class that gets added when the panel opens, but that's on a different element. There should be a test ID added on an element with a testID in the future
     * so we can see whether to click the panel to open it or not (if it's already open).
     */
    cy.get('[id="roadNetworkDescription"]').click();
  }

  static checkSourceDetailsForTrafficSource(dataTable: any) {
    dataTable.hashes().forEach((dataRow: any) => {
      this.selectSource(dataRow.name);

      // General
      this.checkIdContainsText("sourceListDetailTitle", dataRow.name, true);
      this.checkIdContainsText("sourceListDetailSectorGroup", dataRow.sectorGroup, true);
      this.checkIdContainsText("sourceListDetailLocation", dataRow.location, true);
      this.checkIdContainsText("sourceListDetailLocationStatistic", dataRow.locationDetail, true);

      // Characteristics
      this.checkIdContainsText("roadArea", dataRow.roadArea, true);
      this.checkIdContainsText("roadType", dataRow.roadType, true);
      this.checkIdContainsText("roadTrafficDirection", dataRow.roadTrafficDirection, true);
      this.checkIdContainsText("roadWidth", dataRow.roadWidth, true);
      this.checkIdContainsText("roadElevation", dataRow.roadElevation, true);
      this.checkIdContainsText("roadGradient", dataRow.roadGradient, true);

      // Barrier type
      this.checkIdContainsText("barrierCoverage", dataRow.barrierCoverage, true);
      // TODO: Barrier info that is split between left and right does not have ID's yet, but once it does it should be included in this test

      /*
       * Traffic (Custom specification)
       * TODO: Every test ID has `-0`, even when there are multiple subsources. This makes testing with multiple subsources with the same specification harder.
       * The element we want to test doesn't have a test ID itself, so we need to get to it using this convoluted way.
       */
      cy.get('[id="detailRoadStandardVehicleTypeDescription-0-0"] > .row > div:first').should("have.text", dataRow.cars);
      cy.get('[id="detailRoadStandardVehicleTypeDescription-0-1"] > .row > div:first').should("have.text", dataRow.blackCabs);
      cy.get('[id="detailRoadStandardVehicleTypeDescription-0-2"] > .row > div:first').should("have.text", dataRow.motorcycles);
      cy.get('[id="detailRoadStandardVehicleTypeDescription-0-3"] > .row > div:first').should("have.text", dataRow.lgv);
      cy.get('[id="detailRoadStandardVehicleTypeDescription-0-4"] > .row > div:first').should("have.text", dataRow.hgv);
      cy.get('[id="detailRoadStandardVehicleTypeDescription-0-5"] > .row > div:first').should("have.text", dataRow.busAndCoach);

      // Traffic (Emission factor toolkit)
      this.checkIdContainsText("detailRoadCustomVehicleDescription-0", dataRow.trafficDescription, true);
      this.checkIdContainsText("detailRoadCustomVehicleAmount-0", dataRow.numberOfVehicles, true);

      // Emissions
      this.checkIdContainsText("detailRoadCustomVehicleSubstance-0-NOX", dataRow.noxEmissionPerVehicle, true);
      this.checkIdContainsText("detailRoadCustomVehicleSubstance-0-NH3", dataRow.nh3EmissionPerVehicle, true);

      // Time-varying profile
      this.checkIdContainsText("timeVaryingProfileName-THREE_DAY", dataRow.timeVaryingProfileName_THREE_DAY, true);
      this.checkIdContainsText("timeVaryingProfileType-THREE_DAY", dataRow.timeVaryingProfileType_THREE_DAY, true);
      this.checkIdContainsText("timeVaryingProfileName-MONTHLY", dataRow.timeVaryingProfileName_MONTHLY, true);
      this.checkIdContainsText("timeVaryingProfileType-MONTHLY", dataRow.timeVaryingProfileType_MONTHLY, true);
    });
  }

  static checkHousingEmissions(sourceName: string, dataTable: any) {
    this.selectSource(sourceName);

    dataTable.hashes().forEach((dataRow: any) => {
      if (dataRow.custom == "0") {
        this.checkIdContainsText("farmAnimalHousingDetailDescription", dataRow.description, true);
        cy.get('[id="farmAnimalHousingDetailStandardRow-0"] > [id="farmAnimalHousingDetailStandardCode"]').should(
          "have.text",
          dataRow.housingCode
        );
        cy.get('[id="farmAnimalHousingDetailStandardRow-0"] > [id="farmAnimalHousingDetailStandardNumberOfAnimals"]').should(
          "have.text",
          dataRow.nrOfAnimals
        );
        cy.get('[id="farmAnimalHousingDetailStandardRow-0"] > [id="farmAnimalHousingDetailStandardEmissionFactor"]').should(
          "have.text",
          " " + dataRow.emissionFactor
        );
        cy.get('[id="farmAnimalHousingDetailStandardRow-0"] > [id="farmAnimalHousingDetailStandardNumberOfDays"]').should(
          "have.text", 
          dataRow.days
        );
        cy.get('[id="farmAnimalHousingDetailStandardRow-0"] > [id="farmAnimalHousingDetailStandardEmission"]').should(
          "have.text",
          " " + dataRow.emission
        );
        // TODO: Remove the extra whitespace in the code
      } else {
        cy.get('[id="farmAnimalHousingCustomDetailRow-0"] > [id="farmAnimalHousingCustomDetailDescription"]').should("have.text", dataRow.housingCode);
        cy.get('[id="farmAnimalHousingCustomDetailRow-0"] > [id="farmAnimalHousingCustomDetailNumberOfAnimals"]').should("have.text", dataRow.nrOfAnimals);
        cy.get('[id="farmAnimalHousingCustomDetailRow-0"] > [id="farmAnimalHousingCustomDetailEmissionFactor"]').should("have.text", dataRow.emissionFactor);
        cy.get('[id="farmAnimalHousingCustomDetailRow-0"] > [id="farmAnimalHousingCustomDetailEmission"]').should("have.text", dataRow.emission);
      }

      /*
       * TODO: The index is always 0, instead of being the nth subsource
       * TODO2: The index is only on the parent instead of in the testID of the field containing the value, making it inconstent
       */
    });
  }

  static checkFarmlandSubsourceDetails(sourceName: string, dataTable: any) {
    this.selectSource(sourceName);
    let dataRowNr = 0;

    dataTable.hashes().forEach((dataRow: any) => {
      /*
       * TODO: Text currently has useless whitespace, f.e. "\n      Emission Farmland grazing\n    "
       * This check on text should be added once this issue has been fixed:
       *
       * switch (dataRow.subsource) {
       *   case "Farmland grazing":
       *     this.checkIdContainsText("farmlandDetailActivity-PASTURE", "Emission Farmland grazing", true);
       *     break;
       *   case "Manure application (animal)":
       *     this.checkIdContainsText("farmlandDetailActivity-MANURE", "Emission Manure application (animal)", true);
       *     break;
       *   case "Manure application (fertilizer)":
       *     this.checkIdContainsText("farmlandDetailActivity-FERTILIZER", "Emission Manure application (fertilizer)", true);
       *     break;
       *   case "Outdoor yards":
       *     this.checkIdContainsText("farmlandDetailActivity-OUTDOOR_YARDS", "Emission Outdoor yards", true);
       *     break;
       *   case "Organic processes":
       *     this.checkIdContainsText("farmlandDetailActivity-ORGANIC_PROCESSES", "Emission Organic processes", true);
       *     break;
       *   default:
       *     throw new Error("unknown subsource: " + dataRow.subSource);
       * }
       */

      if (
        dataRow.subSource == "Farmland grazing"
        || dataRow.subSource == "Manure application (animal)"
        || dataRow.subSource == "Outdoor yards"
      ) {
        this.checkIdContainsText("farmlandStandardDetailCategoryCode" + dataRowNr, dataRow.animalType, true);
        this.checkIdContainsText("farmlandStandardDetailNumberOfAnimals" + dataRowNr, dataRow.animalType, true);
        this.checkIdContainsText("farmlandStandardDetailEmissionFactor" + dataRowNr, dataRow.animalType, true);
        this.checkIdContainsText("farmlandStandardDetailNumberOfDays" + dataRowNr, dataRow.animalType, true);
        this.checkIdContainsText("farmlandStandardDetailEmissionNH3" + dataRowNr, dataRow.animalType, true);
      } else {
        // TODO: Implement once Test ID's are added.
      }

      dataRowNr++;
    });
    // TODO: Manure application (animal) has not been made yet: AER-857
  }

  static checkBuildingsInfluencingSources(dataTable: any) {
    dataTable.hashes().forEach((dataRow: any) => {
      this.selectSource(dataRow.source);

      // TODO: The styling of these elements is very inconsistent. This should be changed
      if (dataRow.isCircular == "1") {
        cy.get('[id="buildingDiameter"] > [id="label"]').should("have.text", dataRow.width);
        cy.get('[id="buildingHeight"] > div').should("have.text", dataRow.height);
      } else {
        cy.get('[id="buildingLength"] > div').should("have.text", dataRow.length);
        cy.get('[id="buildingWidth"] > div').should("have.text", dataRow.width);
        cy.get('[id="buildingHeight"] > div').should("have.text", dataRow.height);
        this.checkIdContainsText("buildingOrientation", dataRow.orientation, true);
      }
    });
  }

  static checkTimeVaryingProfiles(dataTable: any) {
    dataTable.hashes().forEach((dataRow: any) => {
      cy.get('[id="' + dataRow.profileId + '"]').click({ force: true });

      this.checkIdContainsText("timeVaryingProfileTitle-THREE_DAY", dataRow.profileTitle, true);
      this.checkIdContainsText("timeVaryingProfileName-THREE_DAY", dataRow.profileName, true);
      this.checkIdContainsText("timeVaryingProfileType-THREE_DAY", dataRow.profileType, true);
      this.checkIdContainsText("diurnalTimeVaryingProfileTotalWeekday", dataRow.weekdays, true);
      this.checkIdContainsText("diurnalTimeVaryingProfileTotalSaturday", dataRow.saturdays, true);
      this.checkIdContainsText("diurnalTimeVaryingProfileTotalSunday", dataRow.sundays, true);
      this.checkIdContainsText("diurnalTimeVaryingProfileTotal", dataRow.total, true);
      this.checkIdContainsText("diurnalTimeVaryingProfileAverageWeekday", dataRow.averageWeekdays, true);
      this.checkIdContainsText("diurnalTimeVaryingProfileAverageSaturday", dataRow.averageSaturdays, true);
      this.checkIdContainsText("diurnalTimeVaryingProfileAverageSunday", dataRow.averageSundays, true);
      this.checkIdContainsText("diurnalTimeVaryingProfileAverage", dataRow.average, true);
    });
  }
}

export default ImportFilesPage;
