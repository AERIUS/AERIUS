#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Scenario input

    @Smoke
    Scenario: Year range of emission factor
        Given I login with correct username and password
        And I create a new source from the start page
        And I cancel the new source creation
        Then the droplist calculation year contains the following years:
        |calculationYear   |
        |2024              |
        |2025              |
        |2026              |
        |2027              |
        |2028              |
        |2029              |
        |2030              |
        |2031              |
        |2032              |
        |2033              |
        |2034              |
        |2035              |
        |2036              |
        |2037              |
        |2038              |
        |2039              |
        |2040              |