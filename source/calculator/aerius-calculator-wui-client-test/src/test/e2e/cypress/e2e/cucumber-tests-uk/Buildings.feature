#
# Crown Copyright
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Buildings

    @Smoke
    Scenario: Create a new circular building
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'BUILDING'
        And I create a new building from the input page
        And I name the building 'Building'
        And I select building shape 'Circle'
        And I fill the building shape with WKT string 'POINT(528973.94 179614.64)'
        And I fill the building height with '24'
        And I fill the building diameter with '7'
        And I save the building
        Then the circular building with name 'Building', diameter '7.000 m' and height '24.000 m' is saved and selected

    Scenario: Import building and validate import (warning & error)
        Given I login with correct username and password
        Then I select file "UKAPAS_buildings.gml" to be imported, but I don't upload it yet
        And I expect 2 import 'errors' to be shown
        And I expect 1 import 'warnings' to be shown
        And I validate the following warnings
            | warning                                                                                                    | row |
            | The GML file is valid but is not the latest IMAER version, provided version 'V5_1' expected version 'V6_0' | 0   |

        And I validate the following errors
            | error                                                   | row |
            | Building 'Moidart Barn' has a negative height.          | 0   |
            | Building 'Moidart Circular Barn' has a negative height. | 1   |

    Scenario: Import building and validate import (warning only)
        Given I login with correct username and password
        Then I select file "UKAPAS_buildings_warning_only.gml" to be imported, but I don't upload it yet
        And I expect 1 import 'warnings' to be shown
        And I validate the following warnings
            | warning                                                                                                    | row |
            | The GML file is valid but is not the latest IMAER version, provided version 'V5_1' expected version 'V6_0' | 0   |

        Then Import the file
        And I select view mode 'BUILDING'
        Then the buildinglist has 4 buildings
        And I select the building with name 'Kirk Barn'
        Then the building with name 'Kirk Barn', length '72.911 m', width '34.264 m', height '0.002 m' and orientation '86°' is saved and selected
        And I select the building with name 'Kirk circular barn'
        Then the circular building with name 'Kirk circular barn', diameter '40.000 m' and height '10.000 m' is saved and selected
        And I select the building with name 'Moidart Barn'
        Then the building with name 'Moidart Barn', length '1,441.281 m  (1,000.000 m)', width '1,055.748 m  (1,000.000 m)', height '0.002 m' and orientation '128°' is saved and selected
        Then I validate building warning message "The dimensions of this building are outside the range supported by the building module of Calculator. The nearest values inside of the range will be used for this calculation instead."
        And I select the building with name 'Moidart Circular Barn'
        Then the circular building with name 'Moidart Circular Barn', diameter '35.199 m' and height '2.000 m' is saved and selected

    # TODO: Test with circular building with warning (>500.000 m height)

    Scenario: Test max amount of buildings warning
        Given I login with correct username and password
        Then I select file "UKAPAS_50_buildings.gml" to be imported
        And I select view mode 'BUILDING'

        # Reach too many buildings using duplication (twice)
        When I select the building with name 'Gebouw 1'
        And I duplicate the building
        Then the following notification is displayed 'There are too many buildings in the scenario, the maximum allowed is 50.'
        And I click on remove all notifications

        When I duplicate the building
        Then the following notification is displayed 'There are too many buildings in the scenario, the maximum allowed is 50.'
        And I click on remove all notifications

        # Reach too many buildings using new building creation
        When I create a new building from the building list
        Then the following notification is displayed 'There are too many buildings in the scenario, the maximum allowed is 50.'
