#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Buildings

    Scenario: Cancel creating a building
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'BUILDING'
        And I create a new building from the input page
        And I name the building 'Gebouw'
        And I fill the building shape with WKT string 'POLYGON((208418.99 474219.42,208425.11 474188.05,208439.73 474162.26,208453.55 474171.03,208438.93 474198.15,208436.27 474218.62,208418.99 474219.42))'
        And I fill the building height with '20'
        And I choose to cancel 'Building'
        Then the buildinglist has 0 buildings

    Scenario: Navigate back in browser while creating a building
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'BUILDING'
        And I create a new building from the input page
        And I name the building 'Gebouw'
        And I fill the building shape with WKT string 'POLYGON((208418.99 474219.42,208425.11 474188.05,208439.73 474162.26,208453.55 474171.03,208438.93 474198.15,208436.27 474218.62,208418.99 474219.42))'
        And I fill the building height with '20'
        And I navigate back in my browser
        And I select view mode 'BUILDING'
        Then the buildinglist has 0 buildings

    Scenario: Navigate away while creating a building
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'BUILDING'
        And I create a new building from the input page
        And I name the building 'Gebouw'
        And I fill the building shape with WKT string 'POLYGON((208418.99 474219.42,208425.11 474188.05,208439.73 474162.26,208453.55 474171.03,208438.93 474198.15,208436.27 474218.62,208418.99 474219.42))'
        And I fill the building height with '20'
        And I navigate to the 'Assessment points' tab
        And I navigate to the 'Input' tab
        And I select view mode 'BUILDING'
        Then the buildinglist has 0 buildings

    Scenario: Create a new building
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'BUILDING'
        And I create a new building from the input page
        And I name the building 'Gebouw'
        And I fill the building shape with WKT string 'POLYGON((208418.99 474219.42,208425.11 474188.05,208439.73 474162.26,208453.55 474171.03,208438.93 474198.15,208436.27 474218.62,208418.99 474219.42))'
        And I fill the building height with '20'
        And I save the building
        Then the building with name 'Gebouw', length '60,8 m', width '20,9 m', height '20,0 m' and orientation '160°' is saved and selected

    Scenario: Create a new building height 0 save and edit and save again
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'BUILDING'
        And I create a new building from the input page
        And I name the building 'Gebouw'
        And I fill the building shape with WKT string 'POLYGON((208418.99 474219.42,208425.11 474188.05,208439.73 474162.26,208453.55 474171.03,208438.93 474198.15,208436.27 474218.62,208418.99 474219.42))'
        And I save the building
        Then the following warning is shown: 'Dit gebouw heeft een hoogte van 0 meter'
        And I save the building
        Then the building with name 'Gebouw', length '60,8 m', width '20,9 m', height '0,0 m' and orientation '160°' is saved and selected
        And I edit the building
        And I save the building
        Then the following warning is shown: 'Dit gebouw heeft een hoogte van 0 meter'
        And I save the building

    Scenario: Create and update a building
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'BUILDING'
        And I create a new building from the input page
        And I name the building 'Gebouw'
        And I fill the building shape with WKT string 'POLYGON((208418.99 474219.42,208425.11 474188.05,208439.73 474162.26,208453.55 474171.03,208438.93 474198.15,208436.27 474218.62,208418.99 474219.42))'
        And I fill the building height with '20'
        And I save the building
        And I edit the building
        And I name the building 'Vogeleiland gebouw'
        And I fill the building shape with WKT string 'POLYGON((207625.2 474620.13,207627.14 474617.02,207633.44 474617.19,207635.96 474623.91,207629.4 474631.89,207626.3 474630.88,207625.2 474620.13))'
        And I fill the building height with '5'
        And I save the building
        Then the building with name 'Vogeleiland gebouw', length '15,0 m', width '10,3 m', height '5,0 m' and orientation '6°' is saved and selected

    Scenario: Create and duplicate a building
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'BUILDING'
        And I create a new building from the input page
        And I name the building 'Gebouw'
        And I fill the building shape with WKT string 'POLYGON((208418.99 474219.42,208425.11 474188.05,208439.73 474162.26,208453.55 474171.03,208438.93 474198.15,208436.27 474218.62,208418.99 474219.42))'
        And I fill the building height with '20'
        And I save the building
        And I duplicate the building
        Then the buildinglist has 2 buildings
        Then the building with name 'Gebouw (1)', length '60,8 m', width '20,9 m', height '20,0 m' and orientation '160°' is saved and selected

    Scenario: Delete all buildings
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'BUILDING'
        And I create a new building from the input page
        And I name the building 'Gebouw'
        And I fill the building shape with WKT string 'POLYGON((208418.99 474219.42,208425.11 474188.05,208439.73 474162.26,208453.55 474171.03,208438.93 474198.15,208436.27 474218.62,208418.99 474219.42))'
        And I fill the building height with '20'
        And I save the building
        And I duplicate the building
        And I delete all buildings
        Then the buildinglist has 0 buildings

    Scenario: Delete selected buildings
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'BUILDING'
        And I create a new building from the input page
        And I name the building 'Gebouw'
        And I fill the building shape with WKT string 'POLYGON((208418.99 474219.42,208425.11 474188.05,208439.73 474162.26,208453.55 474171.03,208438.93 474198.15,208436.27 474218.62,208418.99 474219.42))'
        And I fill the building height with '20'
        And I save the building
        And I duplicate the building
        Then the buildinglist has 2 buildings
        And I delete the building
        Then the buildinglist has 1 buildings
        And I select the building with name 'Gebouw'
        Then the building with name 'Gebouw', length '60,8 m', width '20,9 m', height '20,0 m' and orientation '160°' is saved and selected
        And I duplicate the building
        Then the buildinglist has 2 buildings
        # Deselect building 1
        And I select the building with name 'Gebouw (1)'
        And I select the building with name 'Gebouw'
        And I delete the building
        Then no building is selected
        And I select the building with name 'Gebouw (1)'
        Then the buildinglist has 1 buildings
        Then the building with name 'Gebouw (1)', length '60,8 m', width '20,9 m', height '20,0 m' and orientation '160°' is saved and selected
        And I delete the building
        Then the buildinglist has 0 buildings

    Scenario: Create an emission source with no building influence
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Energie' is correctly saved
        And the building the source is influenced by is 'Geen'

    Scenario: Create an emission source and a building it is influenced by, then delete the building influence
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I open the bronkenmerken panel
        And I toggle building influenced
        And I add a new building
        And I name the building 'Gebouw met invloed'
        And I fill the building shape with WKT string 'POLYGON((172166.77 599127.49,172167.57 599067.4,172269.05 599078.74,172261.69 599161.92,172166.77 599127.49))'
        And I fill the building height with '20'
        And I save the building
        Then I toggle building influenced
        And I choose emission height '10' and heat content '0'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Energie' is correctly saved
        And the building the source is influenced by is 'Geen'

    Scenario: Create an emission source and a building it is influenced by
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I open the bronkenmerken panel
        And I toggle building influenced
        And I add a new building
        And I name the building 'Gebouw met invloed'
        And I fill the building shape with WKT string 'POLYGON((172166.77 599127.49,172167.57 599067.4,172269.05 599078.74,172261.69 599161.92,172166.77 599127.49))'
        And I fill the building height with '20'
        And I save the building
        And I choose emission height '10' and heat content '0'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Energie' is correctly saved
        And the building with name 'Gebouw met invloed', length '104,0 m', width '83,5 m', height '20,0 m' and orientation '84°' is saved and selected for the emission source
        And the building the source is influenced by is 'Gebouw met invloed'

    Scenario: Create an emission source and add an existing building to it
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'BUILDING'
        And I create a new building from the input page
        And I name the building 'Gebouw met invloed'
        And I fill the building shape with WKT string 'POLYGON((172166.77 599127.49,172167.57 599067.4,172269.05 599078.74,172261.69 599161.92,172166.77 599127.49))'
        And I fill the building height with '20'
        And I save the building
        And I select view mode 'EMISSION_SOURCES'
        And I create a new source from the scenario input page
        And I name the source 'Bron 1' and select sector group 'Energie'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I open the bronkenmerken panel
        And I toggle building influenced
        And I select the building 'Gebouw met invloed' as the primary building
        And I choose emission height '10' and heat content '0'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Energie' is correctly saved
        And the building with name 'Gebouw met invloed', length '104,0 m', width '83,5 m', height '20,0 m' and orientation '84°' is saved and selected for the emission source
        And the building the source is influenced by is 'Gebouw met invloed'

    Scenario: Create an emission source with forced ventilation change temperature and add an existing building and remove the building
        Given I login with correct username and password
        And I create an empty scenario from the start page
        And I select view mode 'BUILDING'
        And I create a new building from the input page
        And I name the building 'Gebouw met invloed'
        And I fill the building shape with WKT string 'POLYGON((172166.77 599127.49,172167.57 599067.4,172269.05 599078.74,172261.69 599161.92,172166.77 599127.49))'
        And I fill the building height with '20'
        And I save the building
        And I select view mode 'EMISSION_SOURCES'
        And I create a new source from the scenario input page
        And I name the source 'Bron 1' and select sector group 'Energie'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I open the bronkenmerken panel
        And I choose heat content type 'Geforceerd', emission height '15', emission temperature '15.48', outflow diameter '0.3', outflow direction type 'Horizontaal' and outflow velocity '1.0'
        And I fill the emission fields with NOx '10' and NH3 '20'
        When I save the data
        Then I select the source 'Bron 1'
        And source characteristics with heat content type 'Geforceerd', building influence 'Geen', emission height '15,0 m', emission temperature '15,48 °C', outflow diameter '0,3', outflow direction type 'Horizontaal', outflow velocity '1,0' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved

        When I choose to edit the source
        And I open the bronkenmerken panel
        And I toggle building influenced
        And I select the building 'Gebouw met invloed' as the primary building
        And I choose heat content type 'Geforceerd', emission height '15', emission temperature '22.48', outflow diameter '0.3', outflow direction type 'Horizontaal' and outflow velocity '1.0'
        And I toggle building influenced
        And I save the data
        And source characteristics with heat content type 'Geforceerd', building influence 'Geen', emission height '15,0 m', emission temperature '22,48 °C', outflow diameter '0,3', outflow direction type 'Horizontaal', outflow velocity '1,0' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved

    @Smoke
    Scenario: Create an emission source and a building it is influenced by, duplicate it, delete the original and check the copy
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I open the bronkenmerken panel
        And I toggle building influenced
        And I add a new building
        And I name the building 'Gebouw met invloed'
        And I fill the building shape with WKT string 'POLYGON((172166.77 599127.49,172167.57 599067.4,172269.05 599078.74,172261.69 599161.92,172166.77 599127.49))'
        And I fill the building height with '20'
        And I save the building
        And I choose emission height '10' and heat content '0'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data
        Then I select the source 'Bron 1'
        And I copy the source 'Bron 1'
        Then the source 'Bron 1 (1)' with sector group 'Energie' is correctly saved
        And the building with name 'Gebouw met invloed', length '104,0 m', width '83,5 m', height '20,0 m' and orientation '84°' is saved and selected for the emission source
        And I delete source 'Bron 1'
        Then I select the source 'Bron 1 (1)'
        Then the source 'Bron 1 (1)' with sector group 'Energie' is correctly saved
        And the building with name 'Gebouw met invloed', length '104,0 m', width '83,5 m', height '20,0 m' and orientation '84°' is saved and selected for the emission source
        And the building the source is influenced by is 'Gebouw met invloed'

    @Smoke
    Scenario: Import building and validate import (containing warnings & errors)
        Given I login with correct username and password
        Then I select file "AERIUS_buildings.gml" to be imported, but I don't upload it yet
        And I expect 2 import 'errors' to be shown
        And I expect 2 import 'warnings' to be shown
        And I validate the following warnings
            | warning                                                                                                         | row |
            | Het GML-bestand is geldig maar bevat niet de meest recente IMAER versie, gevonden versie 'V5_1' verwacht 'V6_0' | 0   |
            | Gebouw 'Schapenlaan' heeft een hoogte van 0 meter.                                                              | 1   |
        And I validate the following errors
            | error                                               | row |
            | Gebouw 'Schapenlaan 2b' heeft een negatieve hoogte. | 0   |
            | Gebouw 'Schapenlaan 3c' heeft een negatieve hoogte. | 1   |

    Scenario: Import building and validate import (containing warnings only)
        Given I login with correct username and password
        Then I select file "AERIUS_buildings_warnings_only.gml" to be imported, but I don't upload it yet
        And I expect 4 import 'warnings' to be shown
        And I validate the following warnings
            | warning                                                                                                         | row |
            | Het GML-bestand is geldig maar bevat niet de meest recente IMAER versie, gevonden versie 'V5_1' verwacht 'V6_0' | 0   |
            | Gebouw 'Schapenlaan' heeft een hoogte van 0 meter.                                                              | 1   |
            | Gebouw 'Schapenlaan 2b' heeft een hoogte van 0 meter.                                                           | 2   |
            | Gebouw 'Schapenlaan 3c' heeft een hoogte van 0 meter.                                                           | 3   |
        Then Import the file
        When I select view mode 'BUILDING'
        Then the buildinglist has 3 buildings
        When I select the building with name 'Schapenlaan'
        Then the building with name 'Schapenlaan', length '119,3 m  (105,0 m)', width '111,1 m  (87,2 m)', height '0,0 m' and orientation '144°' is saved and selected
        And I validate building warning message "De gebouwdimensies vallen buiten het bereik van de gebouwmodule in Calculator. Er wordt gerekend met de dichtstbijzijnde waarden die binnen het bereik vallen (zie handboek)."
        When I select the building with name 'Schapenlaan 2b'
        Then the building with name 'Schapenlaan 2b', length '119,3 m  (105,0 m)', width '111,1 m  (87,2 m)', height '0,0 m' and orientation '144°' is saved and selected
        And I validate building warning message "De gebouwdimensies vallen buiten het bereik van de gebouwmodule in Calculator. Er wordt gerekend met de dichtstbijzijnde waarden die binnen het bereik vallen (zie handboek)."
        When I select the building with name 'Schapenlaan 3c'
        Then the building with name 'Schapenlaan 3c', length '0,6 m', width '0,3 m', height '0,0 m' and orientation '45°' is saved and selected
        And I validate building warning message ""
