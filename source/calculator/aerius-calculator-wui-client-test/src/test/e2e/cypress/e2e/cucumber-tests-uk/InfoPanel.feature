#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: InfoPanel
    InfoPanel popup tests based on selected location
    
    @Smoke
    Scenario: Use the search to find a hexagon 1529282 select it and validate infopanel content
        Given I login with correct username and password
        And I create a new source from the start page
        Then I start a search with '1529282' and validate '1' categories and the following detail results are shown
            | result                                                |
            | Receptor result 1:Receptor 1529282 - x:487731 y:95983 |
        And I select search result 'Receptor result 1:Receptor 1529282 - x:487731 y:95983'
        Then I click on the center of the map and open infoMarker
        And I validate receptor id '1529282' and location 'x:487732 y:95983'
        And I validate background values
            | row | pollutant            | value       |
            | 1   | Deposition NOₓ + NH₃ | -           |
            | 2   | Concentration NOₓ    | 7.922 µg/m³ |
            | 3   | Concentration NH₃    | -           |
        Then I open info panel habitats
        And I validate info panel habitas for pollutant 'Critical load nitrogen deposition'
            | habitatDescription                                                                            | cl  | mapped |
            | Pintail - Bird - Non breeding 1 - BH25:NVC000:EU000:NCL000:FW                                 | N/A | 4.0    |
            | Atriplex Portulacoides Saltmarsh - BH00:NVC75:A2.54; A2.55; A2.53:NCL017:NRSS                 | 20  | 4.0    |
            | Fagus Sylvatica - Mercurialis Perennis Woodland - BH00:NVC142:G1.6:NCL040:UMW                 | 10  | 4.0    |
            | Invertebrate Assemblage - Invertebrate - BH00:NVC000:EU000:NCL000:NC                          | N/A | 4.0    |
            | Black-Tailed Godwit - Bird - Non breeding - BH18:NVC000:A2.54; A2.55; A2.53:NCL017:NSH        | 20  | 4.0    |
            | Childing Pink - Vascular plant - BH00:NVC000:EU000:NCLNR:NRSS                                 | N/A | 4.0    |
            | Quercus Robur - Pteridium Aquilinum - Rubus Fruticosus Woodland - BH00:NVC216:G1.A:NCL042:UMW | 16  | 4.0    |
            | Rumex Crispus - Glaucium Flavum Shingle Community - BH00:NVC4:EU000:NCLNR:NRSS                | N/A | 4.0    |
            | Spartina Anglica Saltmarsh - BH00:NVC240:A2.54; A2.55; A2.53:NCL017:NRSS                      | 20  | 4.0    |
            | Little Tern - Breeding 1 - BH27:NVC000:B1.3:NCL021:NSH                                        | 10  | 4.0    |
            | Little Tern - Breeding 2 - BH29:NVC000:B1.4 - acid:NCL045:AG                                  | 8   | 4.0    |
            | Little Tern - Breeding 3 - BH30:NVC000:B1.4 - calcareous:NCL046:CG4                           | 10  | 4.0    |
            | Vascular Plant Assemblage - Vascular plant - BH00:NVC000:EU000:NCL000:NC                      | N/A | 4.0    |
        And I validate info panel nature areas
            | nature_area                   | designation                                                                | siteNumber| habitatCount |
            | Pagham Harbour (SPA)          | Special Protection Area                                                    | UK9012041 | 0            |
            | Solent and Dorset Coast (SPA) | Special Protection Area                                                    | UK9020330 | 0            |
            | Pagham Harbour (SSSI_ASSI)    | Site of Special Scientific Interest or Area of Special Scientific Interest | 1000620   | 13           |

    Scenario: Use the search to find a hexagon 8182910 select it and validate infopanel content
        Given I login with correct username and password
        And I create a new source from the start page
        Then I start a search with '8182910' and validate '1' categories and the following detail results are shown
            | result                                                 |
            | Receptor result 1:Receptor 8182910 - x:170581 y:496582 |
        And I select search result 'Receptor result 1:Receptor 8182910 - x:170581 y:496582'
        Then I click on the center of the map and open infoMarker

        And I validate receptor id '8182910' and location 'x:170581 y:496583'
        And I validate background values
            | row | pollutant            | value           |
            | 1   | Deposition NOₓ + NH₃ | 7.936 kg N/ha/y |
            | 2   | Concentration NOₓ    | 3.397 µg/m³     |
            | 3   | Concentration NH₃    | 0.994 µg/m³     |
        Then I open info panel habitats
        And I validate info panel habitas for pollutant 'Critical load nitrogen deposition'
            | habitatDescription                                                                             | cl  | mapped |
            | Intertidal mudflats and sandflats - BH18:NVC000:EU000:NCL000:NSH                               | N/A | 4.0    |
            | Lagoons - BH16:NVC2:A2.54; A2.55; A2.53:NCL017:NSH                                             | 20  | 4.0    |
            | Coastal shingle vegetation outside the reach of waves - BH27:NVC4:B1.4:NCL004:AG               | 8   | 4.0    |
            | Glasswort and other annuals colonising mud and sand - BH18:NVC6:A2.54; A2.55; A2.53:NCL017:NSH | 20  | 4.0    |
            | Atlantic salt meadows - BH18:NVC8:A2.54; A2.55; A2.53:NCL017:NSH                               | 20  | 4.0    |
            | Invertebrate Assemblage - Invertebrate - BH00:NVC000:EU000:NCL000:NC                           | N/A | 4.0    |
        And I validate info panel habitas for pollutant 'Critical level NOₓ'
            | habitatDescription                                                                             | cl  | mapped |
            | Intertidal mudflats and sandflats - BH18:NVC000:EU000:NCL000:NSH                               | N/A | 4.0    |
            | Lagoons - BH16:NVC2:A2.54; A2.55; A2.53:NCL017:NSH                                             | N/A | 4.0    |
            | Coastal shingle vegetation outside the reach of waves - BH27:NVC4:B1.4:NCL004:AG               | N/A | 4.0    |
            | Glasswort and other annuals colonising mud and sand - BH18:NVC6:A2.54; A2.55; A2.53:NCL017:NSH | N/A | 4.0    |
            | Atlantic salt meadows - BH18:NVC8:A2.54; A2.55; A2.53:NCL017:NSH                               | N/A | 4.0    |
            | Invertebrate Assemblage - Invertebrate - BH00:NVC000:EU000:NCL000:NC                           | N/A | 4.0    |
        And I validate info panel habitas for pollutant 'Critical level NH₃'
            | habitatDescription                                                                             | cl  | mapped |
            | Intertidal mudflats and sandflats - BH18:NVC000:EU000:NCL000:NSH                               | N/A | 4.0    |
            | Lagoons - BH16:NVC2:A2.54; A2.55; A2.53:NCL017:NSH                                             | N/A | 4.0    |
            | Coastal shingle vegetation outside the reach of waves - BH27:NVC4:B1.4:NCL004:AG               | N/A | 4.0    |
            | Glasswort and other annuals colonising mud and sand - BH18:NVC6:A2.54; A2.55; A2.53:NCL017:NSH | N/A | 4.0    |
            | Atlantic salt meadows - BH18:NVC8:A2.54; A2.55; A2.53:NCL017:NSH                               | N/A | 4.0    |
            | Invertebrate Assemblage - Invertebrate - BH00:NVC000:EU000:NCL000:NC                           | N/A | 4.0    |
        And I validate info panel nature areas
            | nature_area            | designation                                                                | siteNumber| habitatCount |
            | Strangford Lough (SAC) | Special Area of Conservation                                               | UK0016618 | 5            |
            | Strangford Lough (SPA) | Special Protection Area                                                    | UK9020111 | 0            |
            | Killard (SSSI_ASSI)    | Site of Special Scientific Interest or Area of Special Scientific Interest | ASSI086   | 1            |

    Scenario: Use the search to find a hexagon 7408226 outside nature area select it and validate infopanel content
        Given I login with correct username and password
        And I create a new source from the start page
        Then I start a search with '7408226' and validate '1' categories and the following detail results are shown
            | result                                                 |
            | Receptor result 1:Receptor 7408226 - x:172814 y:449946 |
        And I select search result 'Receptor result 1:Receptor 7408226 - x:172814 y:449946'
        Then I click on the center of the map and open infoMarker

        And I validate receptor id '7408226' and location 'x:172815 y:449947'
        And I validate background values
            | row | pollutant            | value |
            | 1   | Deposition NOₓ + NH₃ | -     |
            | 2   | Concentration NOₓ    | -     |
            | 3   | Concentration NH₃    | -     |
        Then I validate info panel habitas for pollutant 'Critical load nitrogen deposition'

    Scenario: Use the search to find a hexagon 8483121 with calculation results select it and validate infopanel content
        Given I login with correct username and password
        And I select file 'UKAPAS_in_combination_results_with_archive_results.zip' to be imported
        Then the following notification is displayed 'Single file imported successfully.'
        Then the situation name is 'Scenario 1'
        And the situation type is 'Project'
        And the calculation year is '2025'

        And I navigate to the 'Results' tab
        Then the following calculation job notification is displayed 'Calculation job 1 - UKAPAS_in_combination_results_with_archive_results' status 'started'
        Then the following calculation job notification is displayed 'Calculation job 1 - UKAPAS_in_combination_results_with_archive_results' status 'finished'

        And I navigate to the 'Input' tab
        Then I start a search with '8483121' and validate '1' categories and the following detail results are shown
            | result                                                 |
            | Receptor result 1:Receptor 8483121 - x:293793 y:514635 |
        And I select search result 'Receptor result 1:Receptor 8483121 - x:293793 y:514635'
        Then I click on the center of the map and open infoMarker

        And I validate receptor id '8483121' and location 'x:293794 y:514636'
        And I validate background values
            | row | pollutant            | value       |
            | 1   | Deposition NOₓ + NH₃ | -           |
            | 2   | Concentration NOₓ    | 3.499 µg/m³ |
            | 3   | Concentration NH₃    | -           |
        # Validate infopanel content - results are depending on calculation job type - In combination process contribution
        And I validate info marker results for calculation task 'Results Calculation job 1 - UKAPAS_in_combination_results_with_archive_results'
            | element                                          | title                                    | infoRowLabel1        | infoRowValue1   | infoRowLabel2     | infoRowValue2 | infoRowLabel3     | infoRowValue3 |
            | infoPanelResultScenarioType-PROJECT_CALCULATION  | Process contribution                     | Deposition NOₓ + NH₃ | 0.019 kg N/ha/y | Concentration NOₓ | 0.007 µg/m³   | Concentration NH₃ | 0.004 µg/m³   |
            | infoPanelResultScenarioType-IN_COMBINATION       | In combination                           | Deposition NOₓ + NH₃ | 0.019 kg N/ha/y | Concentration NOₓ | 0.007 µg/m³   | Concentration NH₃ | 0.004 µg/m³   |
            | infoPanelResultScenarioTypeTotal-IN_COMBINATION  | PEC (In combination + background)        | Deposition NOₓ + NH₃ | 0.019 kg N/ha/y | Concentration NOₓ | 3.506 µg/m³   | Concentration NH₃ | 0.004 µg/m³   |
            | infoPanelResultScenarioType-ARCHIVE_CONTRIBUTION | Archive contribution                     | Deposition NOₓ + NH₃ | -               | Concentration NOₓ | -             | Concentration NH₃ | -             |
            | infoPanelResultScenario-0                        | Scenario 1 - Project                     | Deposition NOₓ + NH₃ | 0.019 kg N/ha/y | Concentration NOₓ | 0.007 µg/m³   | Concentration NH₃ | 0.004 µg/m³   |

    Scenario: Validate infopanel content - results are depending on calculation job type - Process contribution
        Given I login with correct username and password
        And I select file 'UKAPAS_calc_job_process_contribution.zip' to be imported
        And the following notification is displayed 'Single file imported successfully.'
        And I navigate to the 'Results' tab
        And the following calculation job notification is displayed 'Calculation job 1 - UKAPAS_calc_job_process_contribution' status 'started'
        And the following calculation job notification is displayed 'Calculation job 1 - UKAPAS_calc_job_process_contribution' status 'finished'
        And I navigate to the 'Input' tab
        And I start a search with '11262067' and validate '1' categories and the following detail results are shown
            | result                                                  |
            | Receptor result 1:Receptor 11262067 - x:182679 y:681946 |
        And I select search result 'Receptor result 1:Receptor 11262067 - x:182679 y:681946'

        When I click on the center of the map and open infoMarker
        Then I validate receptor id '11262067' and location 'x:182679 y:681946'
        And I validate background values
            | row | pollutant            | value            |
            | 1   | Deposition NOₓ + NH₃ | 10.821 kg N/ha/y |
            | 2   | Concentration NOₓ    | 1.927 µg/m³      |
            | 3   | Concentration NH₃    | 0.489 µg/m³      |
        And I validate info marker results for calculation task 'Results Calculation job 1 - UKAPAS_calc_job_process_contribution'
            | element                                              | title                                    | infoRowLabel1        | infoRowValue1    | infoRowLabel2     | infoRowValue2 | infoRowLabel3     | infoRowValue3 |
            | infoPanelResultScenarioType-PROJECT_CALCULATION      | Process contribution                     | Deposition NOₓ + NH₃ | 0.194 kg N/ha/y  | Concentration NOₓ | 0.012 µg/m³   | Concentration NH₃ | 0.025 µg/m³   |
            | infoPanelResultScenarioTypeTotal-PROJECT_CALCULATION | PEC (Process contribution + background)  | Deposition NOₓ + NH₃ | 11.016 kg N/ha/y | Concentration NOₓ | 1.940 µg/m³   | Concentration NH₃ | 0.514 µg/m³   |
            | infoPanelResultScenario-0                            | Scenario 1 - Project                     | Deposition NOₓ + NH₃ | 0.194 kg N/ha/y  | Concentration NOₓ | 0.012 µg/m³   | Concentration NH₃ | 0.025 µg/m³   |

    Scenario: Validate infopanel content - results are depending on calculation job type - Max temporary effect
        Given I login with correct username and password
        And I select file 'UKAPAS_calc_job_max_temp_effect.zip' to be imported
        And the following notification is displayed 'Single file imported successfully.'
        And I navigate to the 'Results' tab
        And the following calculation job notification is displayed 'Calculation job 1 - UKAPAS_calc_job_max_temp_effect' status 'started'
        And the following calculation job notification is displayed 'Calculation job 1 - UKAPAS_calc_job_max_temp_effect' status 'finished'
        And I navigate to the 'Input' tab
        And I start a search with '11262067' and validate '1' categories and the following detail results are shown
            | result                                                  |
            | Receptor result 1:Receptor 11262067 - x:182679 y:681946 |
        And I select search result 'Receptor result 1:Receptor 11262067 - x:182679 y:681946'

        When I click on the center of the map and open infoMarker
        Then I validate receptor id '11262067' and location 'x:182679 y:681946'
        And I validate background values
            | row | pollutant            | value            |
            | 1   | Deposition NOₓ + NH₃ | 10.821 kg N/ha/y |
            | 2   | Concentration NOₓ    | 1.927 µg/m³      |
            | 3   | Concentration NH₃    | 0.489 µg/m³      |
        And I validate info marker results for calculation task 'Results Calculation job 1 - UKAPAS_calc_job_max_temp_effect'
            | element                                               | title                                       | infoRowLabel1        | infoRowValue1    | infoRowLabel2     | infoRowValue2 | infoRowLabel3     | infoRowValue3 |
            | infoPanelResultScenarioType-MAX_TEMPORARY_EFFECT      | Maximum temporary effect                    | Deposition NOₓ + NH₃ | 0.310 kg N/ha/y  | Concentration NOₓ | 0.020 µg/m³   | Concentration NH₃ | 0.040 µg/m³   |
            | infoPanelResultScenarioTypeTotal-MAX_TEMPORARY_EFFECT | PEC (Maximum temporary effect + background) | Deposition NOₓ + NH₃ | 11.131 kg N/ha/y | Concentration NOₓ | 1.947 µg/m³   | Concentration NH₃ | 0.529 µg/m³   |
            | infoPanelResultScenario-0                             | Temporary - Temporary                       | Deposition NOₓ + NH₃ | 0.310 kg N/ha/y  | Concentration NOₓ | 0.020 µg/m³   | Concentration NH₃ | 0.040 µg/m³   |

    Scenario: Validate infopanel content - results are depending on calculation job type - Single scenario
        Given I login with correct username and password
        And I select file 'UKAPAS_calc_job_single_scenario.zip' to be imported
        And the following notification is displayed 'Single file imported successfully.'
        And I navigate to the 'Results' tab
        And the following calculation job notification is displayed 'Calculation job 1 - UKAPAS_calc_job_single_scenario' status 'started'
        And the following calculation job notification is displayed 'Calculation job 1 - UKAPAS_calc_job_single_scenario' status 'finished'
        And I navigate to the 'Input' tab
        And I start a search with '11262067' and validate '1' categories and the following detail results are shown
            | result                                                  |
            | Receptor result 1:Receptor 11262067 - x:182679 y:681946 |
        And I select search result 'Receptor result 1:Receptor 11262067 - x:182679 y:681946'

        When I click on the center of the map and open infoMarker
        Then I validate receptor id '11262067' and location 'x:182679 y:681946'
        And I validate background values
            | row | pollutant            | value            |
            | 1   | Deposition NOₓ + NH₃ | 10.821 kg N/ha/y |
            | 2   | Concentration NOₓ    | 1.927 µg/m³      |
            | 3   | Concentration NH₃    | 0.489 µg/m³      |
        And I validate info marker results for calculation task 'Results Calculation job 1 - UKAPAS_calc_job_single_scenario'
            | element                                              | title                                    | infoRowLabel1        | infoRowValue1    | infoRowLabel2     | infoRowValue2 | infoRowLabel3     | infoRowValue3 |
            | infoPanelResultScenario-0                            | Scenario 1 - Project                     | Deposition NOₓ + NH₃ | 0.194 kg N/ha/y  | Concentration NOₓ | 0.012 µg/m³   | Concentration NH₃ | 0.025 µg/m³   |
