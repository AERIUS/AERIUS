/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class CommonGenericUIPage {
  static selectScenarioName() {
    cy.get('[id="sourceListSituationName"]').click();
  }

  static assertPrivacyWarningExists(language: string) {
    cy.get('[id="privacy-warning"]').should("exist");

    if (language === "NL") {
      cy.get('[id="privacy-warning"] > p').should("have.text", "Pas op met het invoeren van persoonlijke gegevens in dit veld");
    } else if (language === "UK") {
      cy.get('[id="privacy-warning"] > p').should("have.text", "Please be aware of entering personal data into this field i.e. John's farm");
    }
  }
  
  static assertSearchQueryResults(queryString: string, resultCount: number, dataTable: any) {
    cy.get(".icon-map-search-icon").click();
    cy.get(".search-input").clear().type(queryString, {delay: 0 });
    cy.get(".loading").should("have.css", "display", "block");
    cy.get(".resultsContainer").find(".title").should("have.length", resultCount);
    dataTable.hashes().forEach((search: any) => {
      cy.get('[aria-label^="' + search.result + '"]');
    });
    cy.get(".icon-map-search-icon").click();
  }

  static assertSelectSearchQueryResultsNoStatusCodeErrors(searchName: string) {
    cy.intercept("*").as("getOSNIDiscoverer");
    cy.get('[aria-label="' + searchName + '"]').click({force: true});
    cy.wait(1_000);
    cy.get('.ol-zoom-out').click();
    cy.wait("@getOSNIDiscoverer");
    cy.get('.ol-zoom-out').click();
    cy.wait("@getOSNIDiscoverer");

    cy.get("@getOSNIDiscoverer.all").then((requests: any) => {
      requests.forEach((request: any) => {
        if (request.hasOwnProperty("response")) {
          if (request.request.url.indexOf("google-analytics") > -1) {
            expect(request.response.statusCode).to.eq(204, "Validate url exists : " + request.request.url);
          } else {
            expect(request.response.statusCode).to.eq(200, "Validate url exists : " + request.request.url);
          }
        }
      })
    });
  }

  static clickOnSearchResult(value: string) {
    cy.get('[aria-label="' + value + '"]').click({ force: true });
  }

  static assertPreferencesPanelIsClosed() {
    cy.get(".settingsContainer").should("not.exist");
  }

  static assertNumberOfSources(numberOfSources: number) {
    cy.get(".sources > div").find("> div").should("have.length", numberOfSources);
  }
}
