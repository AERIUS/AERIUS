/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";
import { CommonImportFilesPage } from "./CommonImportFilesPage";

Given("the button to download warnings and errors should not be shown", () => {
  CommonImportFilesPage.assertDownloadWarningsAndErrorsButtonIsNotShown();
});

Given("the button to download warnings and errors should be shown", () => {
  CommonImportFilesPage.assertDownloadWarningsAndErrorsButtonIsShown();
});

Given(
  "the detail info of the line with scenario {string} of import file {string} should contain {string}",
  (scenarioName, fileName, expectedString) => {
    CommonImportFilesPage.assertAdvancedImportFileDetailContains(scenarioName, fileName, expectedString);
  }
);

Given(
  "I toggle the line with scenario {string} of import file {string} line {string}",
  (scenarioName, fileName, lineTitle) => {
    CommonImportFilesPage.toggleAdvancedImportFileDetailLine(scenarioName, fileName, lineTitle);
  }
);

Given(
  "I validate the selected import lines",
  (dataTable) => {
    CommonImportFilesPage.validateAdvancedImportFileDetailLine(dataTable);
  }
);

Given(
  "the header of the detail info of the line with scenario {string} of import file {string} should be {string}",
  (scenarioName, fileName, expectedString) => {
    CommonImportFilesPage.assertAdvancedImportFileDetailHeaderIs(scenarioName, fileName, expectedString);
  }
);

Given(
  "the header of the detail info of the line with no scenario of import file {string} should be {string}",
  (fileName, expectedString) => {
    CommonImportFilesPage.assertAdvancedImportFileDetailHeaderIs("", fileName, expectedString);
  }
);

Given("I expand the line with scenario {string} for file {string}", (scenarioName, fileName) => {
  CommonImportFilesPage.expandAdvancedLineFor(scenarioName, fileName);
});

Given("there should be {int} scenarios ready to imported", (expectedNrOfScenarios) => {
  CommonImportFilesPage.assertNumberOfScenarioToBeImported(expectedNrOfScenarios);
});

Given("I expand line with no scenario for file {string}", (fileName) => {
  CommonImportFilesPage.expandAdvancedLineFor("", fileName);
});

Given("the detail info of the line with no scenario of import file {string} should contain {string}", (fileName, expectedString) => {
  CommonImportFilesPage.assertAdvancedImportFileDetailContains("", fileName, expectedString);
});
