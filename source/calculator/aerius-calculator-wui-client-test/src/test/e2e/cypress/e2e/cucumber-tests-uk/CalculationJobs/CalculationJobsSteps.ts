/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";

import { CalculationJobsPage } from "./CalculationJobsPage";

Given("I expect the calculation distance value to be set", () => {
  CalculationJobsPage.assertCalculationDistanceValueIsSet();
});

Given("I expect the min. Monin-Obukhov length value to be set", () => {
  CalculationJobsPage.assertMinMoninObukhovLengthValueIsSet();
});

Given("I expect the surface albedo value to be set", () => {
  CalculationJobsPage.assertSurfaceAlbedoValueIsSet();
});

Given("I expect the Priestley-Taylor parameter value to be set", () => {
  CalculationJobsPage.assertPriestlyTaylorParameterValueIsSet();
});

Given("I expect the zone of influence error to be shown", () => {
  CalculationJobsPage.assertZoneOfInfluenceErrorIsShown(true);
});

Given("I expect the calculation distance error to be hidden", () => {
  CalculationJobsPage.assertZoneOfInfluenceErrorIsShown(false);
});

Given("I expect the min. Monin-Obukhov length error to be shown", () => {
  CalculationJobsPage.assertMinMoninObukhovLengthErrorIsShown(true);
});

Given("I expect the min. Monin-Obukhov length error to be hidden", () => {
  CalculationJobsPage.assertMinMoninObukhovLengthErrorIsShown(false);
});

Given("I expect the surface albedo error to be shown", () => {
  CalculationJobsPage.assertSurfaceAlbedoErrorIsShown(true);
});

Given("I expect the surface albedo error to be hidden", () => {
  CalculationJobsPage.assertSurfaceAlbedoErrorIsShown(false);
});

Given("I expect the Priestley-Taylor parameter error to be shown", () => {
  CalculationJobsPage.assertPriestleyTaylorParameterErrorIsShown(true);
});

Given("I expect the Priestley-Taylor parameter error to be hidden", () => {
  CalculationJobsPage.assertPriestleyTaylorParameterErrorIsShown(false);
});
