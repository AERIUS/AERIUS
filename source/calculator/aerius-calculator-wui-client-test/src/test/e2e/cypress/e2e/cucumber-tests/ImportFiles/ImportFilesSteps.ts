/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";
import { ImportFilesPage } from "./ImportFilesPage";
import { AssessmentPointsPage } from "../AssessmentPoints/AssessmentPointsPage";
import { CommonPage } from "../../common/CommonPage";
import { CommonAssessmentPointsPage } from "../../common/CommonAssessmentPointsPage";
import { CommonImportFilesPage } from "../../common/CommonImportFilesPage";

Given("I validate the status of the buttons on the import screen", (dataTable) => {
  ImportFilesPage.validateImportButtons(dataTable);
});

Given("the advanced import mode should be turned on", () => {
  ImportFilesPage.verifyAdvancedImportTurnedOn();
});

Given("the advanced import mode should be turned off", () => {
  ImportFilesPage.verifyAdvancedImportTurnedOff();
});

Given("I expect the following options to be visible in the import list", (dataTable) => {
  ImportFilesPage.validateImportListDefaultOptions(dataTable);
});

Given("for the usage dropdown I expect to see the option {string}", (expectedOption) => {
  ImportFilesPage.assertUsageDropdownOptionShown(expectedOption, "");
});

Given("for the usage dropdown I expect to see the option {string} under the header {string}", (expectedOption, header) => {
  ImportFilesPage.assertUsageDropdownOptionShown(expectedOption, header);
});

Given(
  "for the usage dropdown for the line of situation {string} of file {string} I expect to see the option {string}",
  (scenarioName, fileName, option) => {
    ImportFilesPage.assertUsageDropdownOptionShownAdvanced(scenarioName, fileName, option, "");
  }
);

Given(
  "for the usage dropdown for the line of situation {string} of file {string} I expect to see the option {string} under the header {string}",
  (scenarioName, fileName, option, header) => {
    ImportFilesPage.assertUsageDropdownOptionShownAdvanced(scenarioName, fileName, option, header);
  }
);

Given("for the usage dropdown I expect the selected option to be {string}", (expectedOption) => {
  ImportFilesPage.assertSelectedUsageIs(expectedOption);
});

Given(
  "for the usage dropdown for the line with no situation of file {string} I expect to see the option {string} under the header {string}",
  (fileName, mergeSituation, header) => {
    ImportFilesPage.assertUsageDropdownOptionShownAdvanced("", fileName, mergeSituation, header);
  }
);

Given("for the situation type dropdown I expect the selected option to be {string}", (expectedOption) => {
  ImportFilesPage.assertSelectedSituationTypeIs(expectedOption);
});

Given("the situation type of the line with scenario {string} of import file {string} should be {string}", (scenarioName, fileName, expectedOption) => {
  ImportFilesPage.assertSelectedSituationTypeForLineIs(scenarioName, fileName, expectedOption);
});

Given("the file {string} should be in the import list", (fileName) => {
  ImportFilesPage.assertFileIsInToBeImportedList(fileName);
});

Given("the file {string} should not be in the import list", (fileName) => {
  ImportFilesPage.assertFileIsNotInToBeImportedList(fileName);
});

Given("the line of the file {string} should not be expanded", (fileName) => {
  ImportFilesPage.assertImportFileLineIsExpanded(fileName);
});

Given("the line of the file {string} should be expanded", (fileName) => {
  ImportFilesPage.assertImportFileLineIsNotExpanded(fileName);
});

Given("I expand the line for file {string}", (fileName) => {
  ImportFilesPage.toggleLineFor(fileName);
});

Given("I close the line for file {string}", (fileName) => {
  ImportFilesPage.toggleLineFor(fileName);
});

Given("I expand the line with scenario {string} for file {string}", (scenarioName, fileName) => {
  CommonImportFilesPage.expandAdvancedLineFor(scenarioName, fileName);
});

Given("I close the line with scenario {string} for file {string}", (scenarioName, fileName) => {
  CommonImportFilesPage.expandAdvancedLineFor(scenarioName, fileName);
});

Given("I select {string} as situation type for scenario {string} of file {string}", (situationType, scenarioName, fileName) => {
  ImportFilesPage.selectSituationTypeForImportLine(situationType, scenarioName, fileName);
});

Given("I remove the file {string} from the import list", (fileName) => {
  ImportFilesPage.removeFileFromImportList(fileName);
});

Given("the detail info of import file {string} should contain {string}", (fileName, expectedString) => {
  ImportFilesPage.assertImportFileDetailContains(fileName, expectedString);
});

Given("the detail info of import file {string} should not contain {string}", (fileName, unexpectedString) => {
  ImportFilesPage.assertImportFileDetailDoesNotContain(fileName, unexpectedString);
});

Given(
  "the detail info of the line with scenario {string} of import file {string} should not contain {string}",
  (scenarioName, fileName, expectedString) => {
    ImportFilesPage.assertAdvancedImportFileDetailDoesNotContain(scenarioName, fileName, expectedString);
  }
);

Given("the detail info of the line with no scenario of import file {string} should not contain {string}", (fileName, expectedString) => {
  ImportFilesPage.assertAdvancedImportFileDetailDoesNotContain("", fileName, expectedString);
});

Given("the detail info of the line with scenario {string} of import file {string} should not exist", (scenarioName, fileName) => {
  ImportFilesPage.assertAdvancedImportFileDetailDoesNotExist(scenarioName, fileName);
});

Given("I select {string} as usage for scenario {string} of file {string}", (usageScenario, scenarioName, fileName) => {
  ImportFilesPage.selectUsageForImportLine(usageScenario, scenarioName, fileName);
});

Given("I select {string} as usage for file {string} with no scenario", (usageScenario, fileName) => {
  ImportFilesPage.selectUsageForImportLine(usageScenario, "", fileName);
});

Given("I download the errors and warnings and assert a CSV has been downloaded", () => {
  ImportFilesPage.downloadErrorsAndWarningsAndValidateCSVHasBeenDownloaded();
});

Given("I try to import the file", () => {
  ImportFilesPage.tryToImportTheFile();
});

Given("I select BRN file {string} to be imported", (fileName) => {
  ImportFilesPage.selectBrnFileForImport(fileName);
});

Given("I select BRN file {string} to be imported as pollutant {string}", (fileName, pollutant) => {
  ImportFilesPage.selectBrnFileForImportWithPollutant(fileName, pollutant, false);
});

Given("I select BRN file {string} to be imported as pollutant {string} and import it", (fileName, pollutant) => {
  ImportFilesPage.selectBrnFileForImportWithPollutant(fileName, pollutant, true);
});

Given("I expect the {string} column to be empty", (column) => {
  ImportFilesPage.assertColumnIsEmpty(column);
});

Given("I expect the {string} column to not be empty", (column) => {
  ImportFilesPage.assertColumnIsNotEmpty(column);
});

Given("I expect to see a dropdown in which I can choose pollutants", () => {
  ImportFilesPage.assertPollutantDropdownIsVisible();
});

Given("I expect to see errors", () => {
  ImportFilesPage.assertErrorsAreShown();
});

Given("I expect to see an empty import screen", () => {
  ImportFilesPage.assertImportScreenIsEmpty();
});

Given("calculation point {string} is correctly saved", (nameCalculationPoint) => {
  CommonAssessmentPointsPage.calculationPointCorrectlySaved(nameCalculationPoint);
});

Given("I select menu calculation point", () => {
  AssessmentPointsPage.selectMenuCalculationPoint();
});

Given("I add the calculation job name {string}", (jobName) => {
  CommonPage.addCalculationJobName(jobName);
});

Given("I save the default calculation year", () => {
  ImportFilesPage.saveDefaultCalculationYear();
});

Given("the calculation year is the default calculation year", () => {
  ImportFilesPage.assertCalculationYearIsTheDefault();
});
