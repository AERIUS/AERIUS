#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Road transportation - Road traffic

    Scenario: Validate error and warnings on road transport input
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Rijdend verkeer'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        Then I open collapsible panel 'Bronkenmerken'

        # "Normaal" can't have an elevation height
        And I choose road elevation type 'Normaal'
        Then I validate that I can't set an elevation height

        # Validate min/max for every other elevation type
        When I choose road elevation type 'Normale dijk >= 20 ° en < 45 °'
        Then I validate that the minimum elevation height is -6, and the max is 12, and decimals are not allowed
        When I choose road elevation type 'Steile dijk >= 45 °'
        Then I validate that the minimum elevation height is -6, and the max is 12, and decimals are not allowed
        When I choose road elevation type 'Viaduct'
        Then I validate that the minimum elevation height is -6, and the max is 12, and decimals are not allowed
        When I choose road elevation type 'Tunnelbak'
        Then I validate that the minimum elevation height is -6, and the max is 12, and decimals are not allowed

    Scenario: Validate Euro class gets reset to 'Custom' upon changing its values
        Given I login with correct username and password
        When I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Rijdend verkeer'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I choose road type 'Buitenweg' direction 'Van A naar B'
        And I click to add a emission source of type 'Eigen specificatie'
        And I choose Euro class 'Bestelauto - LPG - Euro-4' from custom
        Then the chosen specific Euro class should be 'Bestelauto - LPG - Euro-4'
        When I fill emission 'NO2' per vehicle '1'
        Then the chosen custom Euro class should be 'Anders'

    Scenario: Create a new road transportation source with a tunnel factor of 0
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Rijdend verkeer'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        Then I open collapsible panel 'Bronkenmerken'
        And I select tunnel factor 0
        And I choose road elevation type 'Normaal'
        And I choose road type 'Snelweg' direction 'Beide richtingen' 
        And I click to add a emission source of type 'Voorgeschreven factoren'
        And I choose maximum speed '100 km/uur'
        And I choose time unit '/uur'
        And I choose for traffic type 'Zwaar vrachtverkeer' the number of vehicles '20' and stagnation percentage '60'
        When I save the data
        And I select the source 'Verkeersnetwerk'
        And I select the source 'Bron 1'
        Then source characteristics with road type 'Snelweg', tunnel factor '0', elevation type 'Normaal' and elevation height '0 m' is correctly saved
        And the total 'road' emission 'NOx' is '0,0 kg/j'
        And the total 'road' emission 'NO2' is '0,0 kg/j'
        And the total 'road' emission 'NH3' is '0,0 kg/j'

    @Smoke
    Scenario: Create new road transportation source - freeways-standard
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Rijdend verkeer'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        Then I open collapsible panel 'Bronkenmerken'
        And I choose road elevation type 'Normaal'
        And I choose as road side barrier type from A to B 'Scherm', barrier height '0' and barrier distance '0'
        And I choose as road side barrier type from B to A 'Scherm', barrier height '0' and barrier distance '0'
        And I choose road type 'Snelweg' direction 'Beide richtingen' 
        And I click to add a emission source of type 'Voorgeschreven factoren'
        And I choose maximum speed '100 km/uur'
        And I choose time unit '/uur'
        And I choose for traffic type 'Licht verkeer' the number of vehicles '1.1' and stagnation percentage '2.2'
        And I choose for traffic type 'Middelzwaar vrachtverk.' the number of vehicles '3.3' and stagnation percentage '4.4'
        And I choose for traffic type 'Zwaar vrachtverkeer' the number of vehicles '5.5' and stagnation percentage '6.6'
        And I choose for traffic type 'Busverkeer' the number of vehicles '7.7' and stagnation percentage '8.8'
        When I save the data validate the warning and wait for refresh
        And I set the calculation year to '2030'
        And I save the scenario updates
        And I select the source 'Verkeersnetwerk'
        And I select the source 'Bron 1'
        And the source 'Bron 1' with sector group 'Verkeer' and sector 'Rijdend verkeer' is correctly saved
        And location with coordinates 'X:146296,7 Y:592777,17' is correctly saved
        And source characteristics with road type 'Snelweg', tunnel factor '1', elevation type 'Normaal' and elevation height '0 m' is correctly saved
        And road side barrier type from A to B 'Scherm', barrier height '0,0 m' and barrier distance '0,0 m' is correctly saved
        And road side barrier type from B to A 'Scherm', barrier height '0,0 m' and barrier distance '0,0 m' is correctly saved
        And direction 'Beide richtingen' is correctly saved
        Then the title for 'standard' road subsource '0' is 'Voorgeschreven factoren'
        And maximum speed '100 km/uur' is correctly saved
        And traffic type 'Licht verkeer' with number of vehicles '1,1' and stagnation percentage '2,2 %' is correctly saved
        And traffic type 'Middelzwaar vrachtverk.' with number of vehicles '3,3' and stagnation percentage '4,4 %' is correctly saved
        And traffic type 'Zwaar vrachtverkeer' with number of vehicles '5,5' and stagnation percentage '6,6 %' is correctly saved
        And traffic type 'Busverkeer' with number of vehicles '7,7' and stagnation percentage '8,8 %' is correctly saved
        And the header of the amount of 'standard' vehicles on row '0' contains '/uur'
        And the total 'road' emission 'NOx' is '6.363,5 kg/j'
        And the total 'road' emission 'NO2' is '1.684,1 kg/j'
        And the total 'road' emission 'NH3' is '268,2 kg/j'
        And I choose to edit the source
        Then I select the subsource '100 km/uur'
        And traffic type 'Licht verkeer' with number of vehicles '1.1' and stagnation percentage '2.2' is available for edit
        And traffic type 'Middelzwaar vrachtverk.' with number of vehicles '3.3' and stagnation percentage '4.4' is available for edit
        And traffic type 'Zwaar vrachtverkeer' with number of vehicles '5.5' and stagnation percentage '6.6' is available for edit
        And traffic type 'Busverkeer' with number of vehicles '7.7' and stagnation percentage '8.8' is available for edit

    Scenario: Create new road transportation source - non urban roads-standard
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Rijdend verkeer'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        Then I open collapsible panel 'Bronkenmerken'
        And I select tunnel factor 10
        And I choose road elevation type 'Normale dijk >= 20 ° en < 45 °' and elevation height '20'
        And I choose as road side barrier type from A to B 'Scherm', barrier height '10' and barrier distance '51'
        And I choose as road side barrier type from B to A 'Wal', barrier height '20' and barrier distance '200'
        And I choose road type 'Buitenweg' direction 'Van A naar B'
        And I click to add a emission source of type 'Voorgeschreven factoren'
        And I choose time unit '/etmaal'
        And I choose for traffic type 'Licht verkeer' the number of vehicles '100' and stagnation percentage '10'
        And I choose for traffic type 'Middelzwaar vrachtverk.' the number of vehicles '200' and stagnation percentage '20'
        And I choose for traffic type 'Zwaar vrachtverkeer' the number of vehicles '300' and stagnation percentage '30'
        And I choose for traffic type 'Busverkeer' the number of vehicles '400' and stagnation percentage '40'
        When I save the data validate the warning and wait for refresh
        And I set the calculation year to '2030'
        And I save the scenario updates
        And I select the source 'Verkeersnetwerk'
        And I select the source 'Bron 1'
        Then the title for 'standard' road subsource '0' is 'Voorgeschreven factoren'
        And the source 'Bron 1' with sector group 'Verkeer' and sector 'Rijdend verkeer' is correctly saved
        And location with coordinates 'X:146296,7 Y:592777,17' is correctly saved
        And source characteristics with road type 'Buitenweg', tunnel factor '10', elevation type 'Normale dijk >= 20 ° en < 45 °' and elevation height '20 m  (12 m)' is correctly saved
        And road side barrier type from A to B 'Scherm', barrier height '10,0 m', barrier height warning '6,0 m', barrier distance '51,0 m' and barrier distance warning '-' is correctly saved
        And road side barrier type from B to A 'Wal', barrier height '20,0 m', barrier height warning '6,0 m', barrier distance '200,0 m' and barrier distance warning '-' is correctly saved
        And direction 'Van A naar B' is correctly saved
        And traffic type 'Licht verkeer' with number of vehicles '100,0' and stagnation percentage '10,0 %' is correctly saved
        And traffic type 'Middelzwaar vrachtverk.' with number of vehicles '200,0' and stagnation percentage '20,0 %' is correctly saved
        And traffic type 'Zwaar vrachtverkeer' with number of vehicles '300,0' and stagnation percentage '30,0 %' is correctly saved
        And traffic type 'Busverkeer' with number of vehicles '400,0' and stagnation percentage '40,0 %' is correctly saved
        And the header of the amount of 'standard' vehicles on row '0' contains '/etmaal'
        And the total 'road' emission 'NOx' is '210,7 ton/j'
        And the total 'road' emission 'NO2' is '49,5 ton/j'
        And the total 'road' emission 'NH3' is '4.192,3 kg/j'

    Scenario: Create new road transportation source - urban roads - free flow
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Rijdend verkeer'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        Then I open collapsible panel 'Bronkenmerken'
        And I choose road elevation type 'Steile dijk >= 45 °' and elevation height '20'
        And I choose as road side barrier type from A to B 'Wal', barrier height '10' and barrier distance '30'
        And I choose as road side barrier type from B to A 'Scherm', barrier height '20' and barrier distance '40'
        And I choose road type 'Binnen bebouwde kom (doorstromend)' direction 'Van B naar A'
        And I click to add a emission source of type 'Voorgeschreven factoren'
        And I choose time unit '/maand'
        And I choose for traffic type 'Licht verkeer' the number of vehicles '50000' and stagnation percentage '100'
        And I choose for traffic type 'Middelzwaar vrachtverk.' the number of vehicles '100000' and stagnation percentage '100'
        And I choose for traffic type 'Zwaar vrachtverkeer' the number of vehicles '150000' and stagnation percentage '100'
        And I choose for traffic type 'Busverkeer' the number of vehicles '200000' and stagnation percentage '0'
        When I save the data validate the warning and wait for refresh
        And I set the calculation year to '2030'
        And I save the scenario updates
        And I select the source 'Verkeersnetwerk'
        And I select the source 'Bron 1'
        Then the title for 'standard' road subsource '0' is 'Voorgeschreven factoren'
        And the source 'Bron 1' with sector group 'Verkeer' and sector 'Rijdend verkeer' is correctly saved
        And location with coordinates 'X:146296,7 Y:592777,17' is correctly saved
        And source characteristics with road type 'Binnen bebouwde kom (doorstromend)', tunnel factor '1', elevation type 'Steile dijk >= 45 °' and elevation height '20 m  (12 m)' is correctly saved
        And road side barrier type from A to B 'Wal', barrier height '10,0 m', barrier height warning '6,0 m' and barrier distance '30,0 m' is correctly saved
        And road side barrier type from B to A 'Scherm', barrier height '20,0 m', barrier height warning '6,0 m' and barrier distance '40,0 m' is correctly saved
        And direction 'Van B naar A' is correctly saved
        And traffic type 'Licht verkeer' with number of vehicles '50.000,0' and stagnation percentage '100,0 %' is correctly saved
        And traffic type 'Middelzwaar vrachtverk.' with number of vehicles '100.000,0' and stagnation percentage '100,0 %' is correctly saved
        And traffic type 'Zwaar vrachtverkeer' with number of vehicles '150.000,0' and stagnation percentage '100,0 %' is correctly saved
        And traffic type 'Busverkeer' with number of vehicles '200.000,0' and stagnation percentage '0,0 %' is correctly saved
        And the header of the amount of 'standard' vehicles on row '0' contains '/maand'
        And the total 'road' emission 'NOx' is '537,4 ton/j'
        And the total 'road' emission 'NO2' is '135,5 ton/j'
        And the total 'road' emission 'NH3' is '5.927,7 kg/j'
        # when source is edited, emissions will be updated
        When I choose to edit the source
        And I select the subsource 'Voorgeschreven factoren'
        And I choose for traffic type 'Busverkeer' the number of vehicles '200000' and stagnation percentage '100'
        And I save the data validate the warning and wait for refresh
        And traffic type 'Licht verkeer' with number of vehicles '50.000,0' and stagnation percentage '100,0 %' is correctly saved
        And traffic type 'Middelzwaar vrachtverk.' with number of vehicles '100.000,0' and stagnation percentage '100,0 %' is correctly saved
        And traffic type 'Zwaar vrachtverkeer' with number of vehicles '150.000,0' and stagnation percentage '100,0 %' is correctly saved
        And traffic type 'Busverkeer' with number of vehicles '200.000,0' and stagnation percentage '100,0 %' is correctly saved
        And the header of the amount of 'standard' vehicles on row '0' contains '/maand'
        # emissions for type 'free flow' (this test) with 100% stagnation are equal to the emissions of type stagnating (see test for type 'stagnating')
        Then the total 'road' emission 'NOx' is '602,1 ton/j'
        And the total 'road' emission 'NO2' is '144,0 ton/j'
        And the total 'road' emission 'NH3' is '5.927,7 kg/j'

    Scenario: Create new road transportation source - urban roads - normal
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Rijdend verkeer'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        Then I open collapsible panel 'Bronkenmerken'
        And I choose road elevation type 'Viaduct' and elevation height '20'
        And I choose as road side barrier type from A to B 'Wal', barrier height '10' and barrier distance '30'
        And I choose as road side barrier type from B to A 'Scherm', barrier height '20' and barrier distance '40'
        And I choose road type 'Binnen bebouwde kom (normaal)' direction 'Van B naar A'
        And I click to add a emission source of type 'Voorgeschreven factoren'
        And I choose time unit '/maand'
        And I choose for traffic type 'Licht verkeer' the number of vehicles '15000' and stagnation percentage '100'
        And I choose for traffic type 'Middelzwaar vrachtverk.' the number of vehicles '100000' and stagnation percentage '100'
        And I choose for traffic type 'Zwaar vrachtverkeer' the number of vehicles '10000' and stagnation percentage '100'
        And I choose for traffic type 'Busverkeer' the number of vehicles '8000' and stagnation percentage '0'
        When I save the data validate the warning and wait for refresh
        And I set the calculation year to '2030'
        And I save the scenario updates
        And I select the source 'Verkeersnetwerk'
        And I select the source 'Bron 1'
        Then the title for 'standard' road subsource '0' is 'Voorgeschreven factoren'
        And the source 'Bron 1' with sector group 'Verkeer' and sector 'Rijdend verkeer' is correctly saved
        And location with coordinates 'X:146296,7 Y:592777,17' is correctly saved
        And source characteristics with road type 'Binnen bebouwde kom (normaal)', tunnel factor '1', elevation type 'Viaduct' and elevation height '20 m  (12 m)' is correctly saved
        And road side barrier type from A to B 'Wal', barrier height '10,0 m', barrier height warning '6,0 m' and barrier distance '30,0 m' is correctly saved
        And road side barrier type from B to A 'Scherm', barrier height '20,0 m', barrier height warning '6,0 m' and barrier distance '40,0 m' is correctly saved
        And direction 'Van B naar A' is correctly saved
        And traffic type 'Licht verkeer' with number of vehicles '15.000,0' and stagnation percentage '100,0 %' is correctly saved
        And traffic type 'Middelzwaar vrachtverk.' with number of vehicles '100.000,0' and stagnation percentage '100,0 %' is correctly saved
        And traffic type 'Zwaar vrachtverkeer' with number of vehicles '10.000,0' and stagnation percentage '100,0 %' is correctly saved
        And traffic type 'Busverkeer' with number of vehicles '8.000,0' and stagnation percentage '0,0 %' is correctly saved
        And the header of the amount of 'standard' vehicles on row '0' contains '/maand'
        And the total 'road' emission 'NOx' is '170,6 ton/j'
        And the total 'road' emission 'NO2' is '47,6 ton/j'
        And the total 'road' emission 'NH3' is '2.273,6 kg/j'

    Scenario: Create new road transportation source - urban roads - stagnating
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Rijdend verkeer'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        Then I open collapsible panel 'Bronkenmerken'
        And I choose road elevation type 'Tunnelbak' and elevation height '20'
        And I choose as road side barrier type from A to B 'Wal', barrier height '10' and barrier distance '30'
        And I choose as road side barrier type from B to A 'Scherm', barrier height '20' and barrier distance '40'
        And I choose road type 'Binnen bebouwde kom (stagnerend)' direction 'Van B naar A'
        And I click to add a emission source of type 'Voorgeschreven factoren'
        And I choose time unit '/maand'
        And I choose for traffic type 'Licht verkeer' the number of vehicles '50000' and stagnation percentage '100'
        And I choose for traffic type 'Middelzwaar vrachtverk.' the number of vehicles '100000' and stagnation percentage '100'
        And I choose for traffic type 'Zwaar vrachtverkeer' the number of vehicles '150000' and stagnation percentage '100'
        And I choose for traffic type 'Busverkeer' the number of vehicles '200000' and stagnation percentage '100'
        When I save the data validate the warning and wait for refresh
        And I set the calculation year to '2030'
        And I save the scenario updates
        And I select the source 'Verkeersnetwerk'
        And I select the source 'Bron 1'
        Then the title for 'standard' road subsource '0' is 'Voorgeschreven factoren'
        And the source 'Bron 1' with sector group 'Verkeer' and sector 'Rijdend verkeer' is correctly saved
        And location with coordinates 'X:146296,7 Y:592777,17' is correctly saved
        And source characteristics with road type 'Binnen bebouwde kom (stagnerend)', tunnel factor '1', elevation type 'Tunnelbak' and elevation height '20 m  (12 m)' is correctly saved
        And road side barrier type from A to B 'Wal', barrier height '10,0 m', barrier height warning '6,0 m' and barrier distance '30,0 m' is correctly saved
        And road side barrier type from B to A 'Scherm', barrier height '20,0 m', barrier height warning '6,0 m' and barrier distance '40,0 m' is correctly saved
        And direction 'Van B naar A' is correctly saved
        And traffic type 'Licht verkeer' with number of vehicles '50.000,0' and stagnation percentage '100,0 %' is correctly saved
        And traffic type 'Middelzwaar vrachtverk.' with number of vehicles '100.000,0' and stagnation percentage '100,0 %' is correctly saved
        And traffic type 'Zwaar vrachtverkeer' with number of vehicles '150.000,0' and stagnation percentage '100,0 %' is correctly saved
        And traffic type 'Busverkeer' with number of vehicles '200.000,0' and stagnation percentage '100,0 %' is correctly saved
        And the header of the amount of 'standard' vehicles on row '0' contains '/maand'
        And the total 'road' emission 'NOx' is '602,1 ton/j'
        And the total 'road' emission 'NO2' is '144,0 ton/j'
        And the total 'road' emission 'NH3' is '5.927,7 kg/j'

    Scenario: Create new road transportation source - freeways-standard - custom specification
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Rijdend verkeer'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        Then I open collapsible panel 'Bronkenmerken'
        And I choose road elevation type 'Tunnelbak' and elevation height '20'
        And I choose as road side barrier type from A to B 'Scherm', barrier height '10' and barrier distance '30'
        And I choose as road side barrier type from B to A 'Wal', barrier height '20' and barrier distance '40'
        And I choose road type 'Snelweg' direction 'Van A naar B'
        And I click to add a emission source of type 'Eigen specificatie'
        And I fill description with 'Eigen weg specificatie'
        And I choose time unit '/etmaal' with number of vehicles '100'
        And I fill emission 'NO2' per vehicle '0.710'
        And I fill emission 'NOX' per vehicle '0.8101'
        And I fill emission 'NH3' per vehicle '0.91011'
        When I save the data validate the warning and wait for refresh
        And I select the source 'Verkeersnetwerk'
        And I select the source 'Bron 1'
        Then the title for 'custom' road subsource '0' is 'Eigen specificatie'
        And the source 'Bron 1' with sector group 'Verkeer' and sector 'Rijdend verkeer' is correctly saved
        And location with coordinates 'X:146296,7 Y:592777,17' is correctly saved
        And source characteristics with road type 'Snelweg', tunnel factor '1', elevation type 'Tunnelbak' and elevation height '20 m  (12 m)' is correctly saved
        And road side barrier type from A to B 'Scherm', barrier height '10,0 m', barrier height warning '6,0 m' and barrier distance '30,0 m' is correctly saved
        And road side barrier type from B to A 'Wal', barrier height '20,0 m', barrier height warning '6,0 m' and barrier distance '40,0 m' is correctly saved
        And direction 'Van A naar B' is correctly saved
        And description 'Eigen weg specificatie' is correctly saved
        And time unit '/etmaal' with number of vehicles '100' is correctly saved
        And emission 'NO2' per vehicle '0,7 g/km' is correctly saved
        And emission 'NOX' per vehicle '0,8 g/km' is correctly saved
        And emission 'NH3' per vehicle '0,9 g/km' is correctly saved
        And the header of the amount of 'custom' vehicles on row '0' contains '/etmaal'
        And the total 'road' emission 'NOx' is '772,2 kg/j'
        And the total 'road' emission 'NO2' is '676,8 kg/j'
        And the total 'road' emission 'NH3' is '867,5 kg/j'

    Scenario: Create new road transportation source - non urban roads-standard - custom specification
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Rijdend verkeer'    
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        Then I open collapsible panel 'Bronkenmerken'
        And I choose road elevation type 'Tunnelbak' and elevation height '20'
        And I choose as road side barrier type from A to B 'Scherm', barrier height '10' and barrier distance '30'
        And I choose as road side barrier type from B to A 'Wal', barrier height '20' and barrier distance '40'
        And I choose road type 'Buitenweg' direction 'Van A naar B'
        And I click to add a emission source of type 'Eigen specificatie'
        And I fill description with 'Eigen weg specificatie'
        And I choose time unit '/etmaal' with number of vehicles '100'
        And I fill emission 'NO2' per vehicle '0.710'
        And I fill emission 'NOX' per vehicle '0.8101'
        And I fill emission 'NH3' per vehicle '0.91011'
        When I save the data validate the warning and wait for refresh
        And I select the source 'Verkeersnetwerk'
        And I select the source 'Bron 1'
        Then the title for 'custom' road subsource '0' is 'Eigen specificatie'
        And the source 'Bron 1' with sector group 'Verkeer' and sector 'Rijdend verkeer' is correctly saved
        And location with coordinates 'X:146296,7 Y:592777,17' is correctly saved
        And source characteristics with road type 'Buitenweg', tunnel factor '1', elevation type 'Tunnelbak' and elevation height '20 m  (12 m)' is correctly saved
        And road side barrier type from A to B 'Scherm', barrier height '10,0 m', barrier height warning '6,0 m' and barrier distance '30,0 m' is correctly saved
        And road side barrier type from B to A 'Wal', barrier height '20,0 m', barrier height warning '6,0 m' and barrier distance '40,0 m' is correctly saved
        And direction 'Van A naar B' is correctly saved
        And description 'Eigen weg specificatie' is correctly saved
        And time unit '/etmaal' with number of vehicles '100' is correctly saved
        And emission 'NO2' per vehicle '0,7 g/km' is correctly saved
        And emission 'NOX' per vehicle '0,8 g/km' is correctly saved
        And emission 'NH3' per vehicle '0,9 g/km' is correctly saved
        And the header of the amount of 'custom' vehicles on row '0' contains '/etmaal'
        And the total 'road' emission 'NOx' is '772,2 kg/j'
        And the total 'road' emission 'NO2' is '676,8 kg/j'
        And the total 'road' emission 'NH3' is '867,5 kg/j'

    Scenario: Create new road transportation source - urban roads-standard - custom specification
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Rijdend verkeer'       
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        Then I open collapsible panel 'Bronkenmerken'
        And I choose road elevation type 'Tunnelbak' and elevation height '-8'
        And I choose as road side barrier type from A to B 'Scherm', barrier height '10' and barrier distance '30'
        And I choose as road side barrier type from B to A 'Wal', barrier height '20' and barrier distance '40'
        And I choose road type 'Buitenweg' direction 'Van A naar B'
        And I click to add a emission source of type 'Eigen specificatie'
        And I fill description with 'Eigen weg specificatie'
        And I choose time unit '/etmaal' with number of vehicles '100'
        And I fill emission 'NO2' per vehicle '0.710'
        And I fill emission 'NOX' per vehicle '0.8101'
        And I fill emission 'NH3' per vehicle '0.91011'
        When I save the data validate the warning and wait for refresh
        And I select the source 'Verkeersnetwerk'
        And I select the source 'Bron 1'
        Then the title for 'custom' road subsource '0' is 'Eigen specificatie'
        And the source 'Bron 1' with sector group 'Verkeer' and sector 'Rijdend verkeer' is correctly saved
        And location with coordinates 'X:146296,7 Y:592777,17' is correctly saved
        And source characteristics with road type 'Buitenweg', tunnel factor '1', elevation type 'Tunnelbak' and elevation height '-8 m  (-6 m)' is correctly saved
        And road side barrier type from A to B 'Scherm', barrier height '10,0 m', barrier height warning '6,0 m' and barrier distance '30,0 m' is correctly saved
        And road side barrier type from B to A 'Wal', barrier height '20,0 m', barrier height warning '6,0 m' and barrier distance '40,0 m' is correctly saved
        And direction 'Van A naar B' is correctly saved
        And description 'Eigen weg specificatie' is correctly saved
        And time unit '/etmaal' with number of vehicles '100' is correctly saved
        And emission 'NO2' per vehicle '0,7 g/km' is correctly saved
        And emission 'NOX' per vehicle '0,8 g/km' is correctly saved
        And emission 'NH3' per vehicle '0,9 g/km' is correctly saved
        And the header of the amount of 'custom' vehicles on row '0' contains '/etmaal'
        And the total 'road' emission 'NOx' is '772,2 kg/j'
        And the total 'road' emission 'NO2' is '676,8 kg/j'
        And the total 'road' emission 'NH3' is '867,5 kg/j'

    Scenario: Create new road transportation source - non urban roads-standard - Euro class
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Rijdend verkeer'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        Then I open collapsible panel 'Bronkenmerken'
        And I choose road elevation type 'Tunnelbak' and elevation height '20'
        And I choose as road side barrier type from A to B 'Scherm', barrier height '10' and barrier distance '30'
        And I choose as road side barrier type from B to A 'Wal', barrier height '20' and barrier distance '40'
        And I choose road type 'Buitenweg' direction 'Van A naar B'
        And I click to add a emission source of type 'Eigen specificatie'
        And I choose Euro class 'Bestelauto - LPG - Euro-4' from custom
        And I choose time unit '/etmaal' with number of vehicles '100'
        When I save the data validate the warning and wait for refresh
        And I set the calculation year to '2030'
        And I save the scenario updates
        And I select the source 'Verkeersnetwerk'
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Verkeer' and sector 'Rijdend verkeer' is correctly saved
        And location with coordinates 'X:146296,7 Y:592777,17' is correctly saved
        And source characteristics with road type 'Buitenweg', tunnel factor '1', elevation type 'Tunnelbak' and elevation height '20 m  (12 m)' is correctly saved
        And road side barrier type from A to B 'Scherm', barrier height '10,0 m', barrier height warning '6,0 m' and barrier distance '30,0 m' is correctly saved
        And road side barrier type from B to A 'Wal', barrier height '20,0 m', barrier height warning '6,0 m' and barrier distance '40,0 m' is correctly saved
        And direction 'Van A naar B' is correctly saved
        And Euro class 'Bestelauto - LPG - Euro-4' is correctly saved
        And time unit '/etmaal' with number of vehicles '100' for Euro class is correctly saved
        And the total 'road' emission 'NOx' is '140,2 kg/j'
        And the total 'road' emission 'NO2' is '3,8 kg/j'
        And the total 'road' emission 'NH3' is '28,1 kg/j'

    @Smoke
    Scenario: Create new road transportation source - freeways-standard - Euro class modify emission value switch to custom
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Rijdend verkeer'    
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        Then I open collapsible panel 'Bronkenmerken'
        And I choose road elevation type 'Tunnelbak' and elevation height '20'
        And I choose as road side barrier type from A to B 'Scherm', barrier height '10' and barrier distance '30'
        And I choose as road side barrier type from B to A 'Wal', barrier height '20' and barrier distance '40'
        And I choose road type 'Snelweg' direction 'Van A naar B'
        And I click to add a emission source of type 'Eigen specificatie'
        And I choose Euro class 'Bestelauto - LPG - Euro-4' from custom
        And I choose time unit '/etmaal' with number of vehicles '100'
        # Entering any value will switch the form from Euro to Custom
        And I fill emission 'NO2' per vehicle '0' for Euro class
        # Add a wait for DOM form to be switched to custom value
        And I wait '100'
        And I fill emission 'NO2' per vehicle '0.1101'
        And I fill emission 'NOX' per vehicle '0.2101'
        And I fill emission 'NH3' per vehicle '0.31011'
        And I fill description with 'Eigen weg specificatie'
        When I save the data validate the warning and wait for refresh
        And I select the source 'Verkeersnetwerk'
        And I select the source 'Bron 1'
        Then the title for 'custom' road subsource '0' is 'Eigen specificatie'
        Then the source 'Bron 1' with sector group 'Verkeer' and sector 'Rijdend verkeer' is correctly saved
        And location with coordinates 'X:146296,7 Y:592777,17' is correctly saved
        And source characteristics with road type 'Snelweg', tunnel factor '1', elevation type 'Tunnelbak' and elevation height '20 m  (12 m)' is correctly saved
        And road side barrier type from A to B 'Scherm', barrier height '10,0 m', barrier height warning '6,0 m' and barrier distance '30,0 m' is correctly saved
        And road side barrier type from B to A 'Wal', barrier height '20,0 m', barrier height warning '6,0 m' and barrier distance '40,0 m' is correctly saved
        And direction 'Van A naar B' is correctly saved
        And description 'Eigen weg specificatie' is correctly saved
        And time unit '/etmaal' with number of vehicles '100' is correctly saved
        And emission 'NO2' per vehicle '0,1 g/km' is correctly saved
        And emission 'NOX' per vehicle '0,2 g/km' is correctly saved
        And emission 'NH3' per vehicle '0,3 g/km' is correctly saved
        And the header of the amount of 'custom' vehicles on row '0' contains '/etmaal'
        And the total 'road' emission 'NOx' is '200,3 kg/j'
        And the total 'road' emission 'NO2' is '104,9 kg/j'
        And the total 'road' emission 'NH3' is '295,6 kg/j'

    Scenario: Create new road transportation source - freeways-standard - Modify Euro class 'Other' switch to custom
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Rijdend verkeer'    
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        Then I open collapsible panel 'Bronkenmerken'
        And I choose road elevation type 'Tunnelbak' and elevation height '20'
        And I choose as road side barrier type from A to B 'Scherm', barrier height '10' and barrier distance '30'
        And I choose as road side barrier type from B to A 'Wal', barrier height '20' and barrier distance '40'
        And I choose road type 'Snelweg' direction 'Van A naar B'
        And I click to add a emission source of type 'Eigen specificatie'
        And I choose Euro class 'Bestelauto - LPG - Euro-4' from custom
        And I choose time unit '/jaar' with number of vehicles '100000'
        And I choose Euro class 'Trekker - diesel - zwaar - Euro-5 Step G - SCR - licht' from specific
        And I choose Euro class 'Anders' from specific
        And I fill emission 'NO2' per vehicle '0.1101'
        And I fill emission 'NOX' per vehicle '0.2101'
        And I fill emission 'NH3' per vehicle '0.31011'
        And I fill description with 'Eigen weg specificatie'
        When I save the data validate the warning and wait for refresh
        And I select the source 'Verkeersnetwerk'
        And I select the source 'Bron 1'
        Then the title for 'custom' road subsource '0' is 'Eigen specificatie'
        Then the source 'Bron 1' with sector group 'Verkeer' and sector 'Rijdend verkeer' is correctly saved
        And location with coordinates 'X:146296,7 Y:592777,17' is correctly saved
        And source characteristics with road type 'Snelweg', tunnel factor '1', elevation type 'Tunnelbak' and elevation height '20 m  (12 m)' is correctly saved
        And road side barrier type from A to B 'Scherm', barrier height '10,0 m', barrier height warning '6,0 m' and barrier distance '30,0 m' is correctly saved
        And road side barrier type from B to A 'Wal', barrier height '20,0 m', barrier height warning '6,0 m' and barrier distance '40,0 m' is correctly saved
        And direction 'Van A naar B' is correctly saved
        And description 'Eigen weg specificatie' is correctly saved
        And time unit '/jaar' with number of vehicles '100.000' is correctly saved
        And emission 'NO2' per vehicle '0,1 g/km' is correctly saved
        And emission 'NOX' per vehicle '0,2 g/km' is correctly saved
        And emission 'NH3' per vehicle '0,3 g/km' is correctly saved
        And the header of the amount of 'custom' vehicles on row '0' contains '/jaar'
        And the total 'road' emission 'NOx' is '548,7 kg/j'
        And the total 'road' emission 'NO2' is '287,5 kg/j'
        And the total 'road' emission 'NH3' is '809,9 kg/j'

    Scenario: Validation on input of road transportation source - Emission factor toolkit
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Rijdend verkeer'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        Then I open collapsible panel 'Bronkenmerken'
        And I choose road type 'Binnen bebouwde kom (doorstromend)' direction 'Van B naar A'
        And I click to add a emission source of type 'Voorgeschreven factoren'
        And I choose time unit '/maand'
        When I choose for traffic type 'Licht verkeer' the number of vehicles '-50000' and stagnation percentage '100'
        And I choose for traffic type 'Middelzwaar vrachtverk.' the number of vehicles '-100000' and stagnation percentage '100'
        And I choose for traffic type 'Zwaar vrachtverkeer' the number of vehicles '-150000' and stagnation percentage '100'
        And I choose for traffic type 'Busverkeer' the number of vehicles '-200000' and stagnation percentage '0'
        And I save the data
        Then the following 'error' text is visible: "(-50000) is geen geldige invoer"
        And the following 'error' text is visible: "(-100000) is geen geldige invoer"
        And the following 'error' text is visible: "(-150000) is geen geldige invoer"
        And the following 'error' text is visible: "(-200000) is geen geldige invoer"

    Scenario: Validation on input of road transportation source - Custom specification
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Verkeer' and sector 'Rijdend verkeer'    
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I choose road type 'Snelweg' direction 'Van A naar B'
        And I click to add a emission source of type 'Eigen specificatie'
        And I fill description with 'Eigen weg specificatie'        
        When I choose time unit '/jaar' with number of vehicles '-100000'        
        And I fill emission 'NO2' per vehicle '-0.1101'
        And I fill emission 'NOX' per vehicle '-0.2101'
        And I fill emission 'NH3' per vehicle '-0.31011'
        Then the following 'error' text is visible: "(-100000) is geen geldige invoer"
        And the following 'error' text is visible: "(-0.1101) is geen geldige invoer"
        And the following 'error' text is visible: "(-0.2101) is geen geldige invoer"
        And the following 'error' text is visible: "(-0.31011) is geen geldige invoer"        
