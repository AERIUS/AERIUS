/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

/*
* Legacy implementation for lodging system with RAV and BWL for product LBV / check
* Product LBV / check does not support new Housing emission for Omgevingswet
*/

import { Given } from "cypress-cucumber-preprocessor/steps";
import { CommonCheckPage }  from "./CommonCheckPage";

Given(
  "I add a lodging system with RAV code {string}, number of animals {string} and BWL code {string}",
  (ravCode, numberOfAnimals, bwlCode) => {
    CommonCheckPage.addLodgingSystemWithRavCodeNumberOfAnimalsBwlCode(ravCode, numberOfAnimals, bwlCode);
  }
);

Given(
  "I validate the subsource with RAV code {string} with number of animals {string} and emission {string}",
  (housingCode, numberOfAnimals, emission) => {
    CommonCheckPage.validateSubsourceWithDescriptionNumberOfAnimalsEmission(housingCode, numberOfAnimals, emission);
  }
);