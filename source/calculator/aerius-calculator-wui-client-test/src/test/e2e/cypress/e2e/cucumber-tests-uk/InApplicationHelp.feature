#
# Crown copyright
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: In-application help
    Test everything to do with in-application help

    Scenario: Test opening/closing in-application help
        Given I login with correct username and password
        Then the in-application help should not show

        When I press the 'manual' button
        Then the in-application help should show

        When I press the 'manual' button
        Then the in-application help should not show

        When I press the 'manual' button
        And I press the 'close' button
        Then the in-application help should not show

    Scenario: Test general usage of in-application help
        # Open application with in-application help and verify the availability of buttons
        Given I login with correct username and password
        When I press the 'manual' button
        Then a 'Table of contents' button should be visible
        And a 'close' button should be visible
        And the in-application help should have a scrollbar and slider

        # Test table of contents
        When I press the 'Table of contents' button
        Then the title 'Table of contents' should show

    Scenario: Check default pages for every page in the application
        Given I login with correct username and password

        When I press the 'manual' button
        Then the title 'Home' should show

        When I select file 'UKAPAS_in_combination_results_without_archive_results.zip' to be imported
        Then the title 'Input' should show

        When I navigate to the 'Assessment points' tab
        Then the title 'Assessment points' should show

        When I navigate to the 'Calculation jobs' tab
        Then the title 'Calculation jobs' should show

        # TODO: Results currently links to page thats waiting for content (5-3-2-results_distribution). Update test once that is fixed. 
        #When I navigate to the 'Results' tab
        #Then the title 'Resultaten' should show

        When I navigate to the 'Export' tab
        Then the title 'Export' should show

        When I navigate to the 'Next steps' tab
        Then the title 'Next steps' should show

        When I navigate to the 'Home' tab
        Then the title 'Home' should show

        # Settings (shouldn't change manual page)
        When I open the settings panel
        Then the title 'Home' should show

        # Test whether the automatic navigation can be turned off in the preferences
        When I change the setting for 'Automatically switching manual pages' to 'off'
        And I navigate to the 'Assessment points' tab
        Then the title 'Home' should show
