/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";
import { InfoPanelPage as InfoPanelPage } from "./InfoPanelPage";
import { CommonInfoPanelPage } from "../../common/CommonInfoPanelPage";

Given("I click on the center of the map and open infoMarker", () => {
  CommonInfoPanelPage.clickOnMapAndAssertInfoMarker("Infopanel");
});

Given("I validate info panel nature areas", (dataTable: any) => {
  InfoPanelPage.validateInfoPanelNatureAreas(dataTable);
});

Given("I open info panel habitats", () => {
  InfoPanelPage.openInfoPanelHabitats();
});

Given("I validate info panel habitas for pollutant {string}", (pollutant : string, dataTable: any) => {
  InfoPanelPage.validateInfoPanelHabitats(pollutant, dataTable);
});
