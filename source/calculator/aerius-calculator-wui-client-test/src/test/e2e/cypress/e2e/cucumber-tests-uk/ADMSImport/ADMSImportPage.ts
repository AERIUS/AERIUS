/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class ADMSImportPage {
  static SOURCEPATH = "./cypress/fixtures/adms/";
  static LF = "\n";
  static CRLF = "\r\n";
  static ADMSGROUP_SEPERATOR = "&";
  static PARAMETER_SEPERATOR = "=";
  static ADMSGROUP_SEPERATOR_END = "/";
  static PARAMETER_POLLUTANT = "OptPolName";

  static validateAdmsSourceWithOutput(source: string, dataTable: any) {
    // Find the output upl file
    cy.get("@jobKey").then((jobKey) => {
      cy.task("readdir", { path: "cypress/fixtures/download/" + jobKey + "/unzip/" }).then((files: any) => {
        let outputADMSFile = "cypress/fixtures/download/" + jobKey + "/unzip/" + files[0] + "/" + files[0] + ".upl";
        cy.wrap(outputADMSFile).as("outputADMSFile");
      });
    });
    this.validateAdmsSourceWithOutputADMSFile(source, dataTable);
  }

  static validateAdmsSourcePollutantWithOutput(source: string, pollutant: string, dataTable: any) {
    if (!["NOx", "NH3"].includes(pollutant)) {
      throw ("Parameter pollutant type not supported : " + pollutant);
    }

    // Find the output upl file based on OptPolName
    cy.get("@jobKey").then((jobKey) => {
      cy.task("readdir", { path: "cypress/fixtures/download/" + jobKey + "/unzip/" }).then((files: any) => {
        files.forEach((fileName: any) => {
          let outputADMSFile = "cypress/fixtures/download/" + jobKey + "/unzip/" + fileName + "/" + fileName + ".upl";
          // read the file and the determine the OptPolName parameter
          cy.readFile(outputADMSFile).then((outputAdmsFile: any) => {
            outputAdmsFile = outputAdmsFile.replaceAll(this.CRLF, this.LF);
            let optPolNamePos = outputAdmsFile.indexOf(this.PARAMETER_POLLUTANT) + 14;
            let optPolNameEndPos = outputAdmsFile.substring(optPolNamePos + 14).indexOf(this.LF);
            let optPolName = outputAdmsFile.substring(optPolNamePos, optPolNamePos + optPolNameEndPos).replaceAll('"',"");
            if (optPolName.trim() == pollutant) {
              cy.wrap(outputADMSFile).as("outputADMSFile");
            }
          });
        });
      });
    });
    this.validateAdmsSourceWithOutputADMSFile(source, dataTable);
  }

  static validateAdmsSourceWithOutputADMSFile(source: string, dataTable: any) {
    let skipParameters: any = [];
    dataTable.hashes().forEach((row: any) => {
      skipParameters.push(row.field);
    });

    cy.get("@outputADMSFile").then((outputADMSFile: any) => {
      // read all adms parameters and validate with in output
      cy.readFile(this.SOURCEPATH + source).then((inputAdmsFile: any) => {
        cy.readFile(outputADMSFile).then((outputAdmsFile: any) => {
          let inputObjsList: any = inputAdmsFile.replaceAll(this.CRLF, this.LF).replaceAll(this.ADMSGROUP_SEPERATOR_END, "").split(this.ADMSGROUP_SEPERATOR);
          let outputObjsList: any = outputAdmsFile.replaceAll(this.CRLF, this.LF).replaceAll(this.ADMSGROUP_SEPERATOR_END, "").split(this.ADMSGROUP_SEPERATOR);
          expect(inputObjsList.length).eq(outputObjsList.length, "Expect output ADMS category count ");
          
          inputObjsList.forEach((inputObjects: any, indexObjects: number) => {
            let outputObjects = outputObjsList[indexObjects];
            let pos = inputObjects.indexOf(this.LF);
            let objName: string = inputObjects.substring(0, inputObjects.indexOf(this.LF));
            let objList: any = inputObjects.substring(pos + 1).split(this.LF);
            let outputObjList: any = outputObjsList[indexObjects].substring(pos + 1).split(this.LF);

            expect(objList.length).eq(outputObjList.length, "Expect (" + objName + ") ADMS parameters count in category " + JSON.stringify(outputObjList));

            objList.forEach((object: any, indexObject: number) => {
              if (object.indexOf(this.PARAMETER_SEPERATOR) > -1) {
                let param = object.split(this.PARAMETER_SEPERATOR)[0].trim();
                let inputValue = this.getParamFromString(inputObjects, param, false);
                let outputValue = this.getParamFromString(outputObjects, param, false);

                if (!skipParameters.includes(param)) {
                  expect(outputValue).eq(inputValue, "Category " + objName + " parameter " + param + " expect " + inputValue);
                }
              }
            });
          });
        });
      });
    });
  }

  static getParamFromString(objects: any, parameter: string, failNotFound: boolean): string {
    let start: any = objects.indexOf(parameter);
    if (failNotFound && start < 0) {
      throw ("Parameter not found in string : " + parameter);
    }

    let end: string = objects.substring(start).indexOf(this.LF);
    let result = "";
    if (objects.substring(start, start + end).split(this.PARAMETER_SEPERATOR)[1] === "") {
      // Support for multiline values
      let subObjList = objects.substring(start + end + 1).replace(this.ADMSGROUP_SEPERATOR_END, "").split(this.LF);
      result = parameter.trim() + this.PARAMETER_SEPERATOR;
      subObjList.every((subObject: any) => {
        if (subObject.indexOf(this.PARAMETER_SEPERATOR) > -1) {
          return false;
        } else {
          result = result + subObject;
          return true;
        }
      });
    } else {
      result = objects.substring(start, start + end);
    }
    return result.replace(this.LF, "");
  }
}
