/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class ExportPage {
  static selectExportOPSInputFiles() {
    cy.get('[id="exportOPS"]').check({ force: true });
  }

  static selectSituation(situation: any, checked: boolean) {
    if (checked == true) {
      cy.get(".situationSelection").get(".inputContainer >").contains(situation).parent().parent().within(() => {
          cy.get('input[type="checkbox"]').check({ force: true });
        });
    } else {
      cy.get(".situationSelection").get(".inputContainer >").contains(situation).parent().parent().within(() => {
          cy.get('input[type="checkbox"]').uncheck({ force: true });
        });
    }
  }

  static cancelExport(calculationJob: string) {
    cy.intercept("POST", "api/v8/ui/calculation/*/cancel").as("cancelJob");
    cy.get(".historyItem >").contains(calculationJob).parent().within(() => {
        cy.get('[id="deleteButton"]').click();
      });
    cy.wait("@cancelJob");
  }

  static validateCalculationResultsCount(count: number) {
    cy.get(".historyContainer");
    cy.get(".historyContainer >").should("have.length", count);
  }
}
