#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: In-application help
    Test everything to do with in-application help

    Scenario: Check default manual pages for every page in the application
        Given I login with correct username and password

        When I press the 'manual' button
        Then the title 'Start' should show

        When I select file 'AERIUS_calculation_points_results.gml' to be imported
        Then the title 'Invoer' should show

        When I navigate to the 'Assessment points' tab
        Then the title 'Rekenpunten' should show

        When I navigate to the 'Calculation jobs' tab
        Then the title 'Rekentaken' should show

        # Resultaten should show thanks to the imported file with results
        # TODO: Results currently links to page thats waiting for content (5-3-2-results_distribution). Update test once that is fixed. 
        #When I navigate to the 'Results' tab
        #Then the title 'Resultaten' should show

        When I navigate to the 'Export' tab
        Then the title 'Exporteren' should show

        When I navigate to the 'Home' tab
        Then the title 'Start' should show

        # Settings (shouldn't change manual page)
        When I open the settings panel
        Then the title 'Start' should show

        # Test whether the automatic navigation can be turned off in the preferences
        When I change the setting for 'Automatically switching manual pages' to 'off'
        And I navigate to the 'Assessment points' tab
        Then the title 'Start' should show
