<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<imaer:FeatureCollectionCalculator xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:imaer="http://imaer.aerius.nl/6.0" xmlns:gml="http://www.opengis.net/gml/3.2" gml:id="NL.IMAER.Collection" xsi:schemaLocation="http://imaer.aerius.nl/6.0 https://imaer.aerius.nl/6.0/IMAER.xsd">
    <imaer:metadata>
        <imaer:AeriusCalculatorMetadata>
            <imaer:project>
                <imaer:ProjectMetadata>
                    <imaer:year>2025</imaer:year>
                </imaer:ProjectMetadata>
            </imaer:project>
            <imaer:situation>
                <imaer:SituationMetadata>
                    <imaer:name>Scenario 1</imaer:name>
                    <imaer:reference>RPHMhE27ytLf</imaer:reference>
                    <imaer:situationType>PROPOSED</imaer:situationType>
                </imaer:SituationMetadata>
            </imaer:situation>
            <imaer:calculation>
                <imaer:CalculationMetadata>
                    <imaer:method>QUICK_RUN</imaer:method>
                    <imaer:jobType>IN_COMBINATION_PROCESS_CONTRIBUTION</imaer:jobType>
                    <imaer:substance>NOX</imaer:substance>
                    <imaer:substance>NO2</imaer:substance>
                    <imaer:substance>NH3</imaer:substance>
                    <imaer:resultType>CONCENTRATION</imaer:resultType>
                    <imaer:resultType>DEPOSITION</imaer:resultType>
                    <imaer:maximumRange>5000.0</imaer:maximumRange>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>without_source_stacking</imaer:key>
                            <imaer:value>true</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>use_in_combination_archive</imaer:key>
                            <imaer:value>true</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_version</imaer:key>
                            <imaer:value>5.0.3.0-9166</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_permit_area</imaer:key>
                            <imaer:value>eng</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>road_local_fraction_no2_receptors_option</imaer:key>
                            <imaer:value>LOCATION_BASED</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>road_local_fraction_no2_points_option</imaer:key>
                            <imaer:value>LOCATION_BASED</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_min_monin_obukhov_length</imaer:key>
                            <imaer:value>30.0</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_surface_albedo</imaer:key>
                            <imaer:value>0.23</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_priestley_taylor_parameter</imaer:key>
                            <imaer:value>1.0</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_met_site_id</imaer:key>
                            <imaer:value>2224210</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_met_dataset_type</imaer:key>
                            <imaer:value>NWP_3KM2</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_met_years</imaer:key>
                            <imaer:value>2021</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_met_site_roughness</imaer:key>
                            <imaer:value>0.25236</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_met_site_min_monin_obukhov_length</imaer:key>
                            <imaer:value>1.0</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_met_site_surface_albedo</imaer:key>
                            <imaer:value>0.082</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_met_site_priestley_taylor_parameter</imaer:key>
                            <imaer:value>1.0</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_plume_depletion_nh3</imaer:key>
                            <imaer:value>false</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_plume_depletion_nox</imaer:key>
                            <imaer:value>false</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_spatially_varying_roughness</imaer:key>
                            <imaer:value>true</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_complex_terrain</imaer:key>
                            <imaer:value>false</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                </imaer:CalculationMetadata>
            </imaer:calculation>
            <imaer:version>
                <imaer:VersionMetadata>
                    <imaer:aeriusVersion>build-167_2024-06-11-02-05_20240611_03dfdcf1f9</imaer:aeriusVersion>
                    <imaer:databaseVersion>build-167_2024-06-11-02-05_03dfdcf1f9_calculator_uk_latest</imaer:databaseVersion>
                </imaer:VersionMetadata>
            </imaer:version>
        </imaer:AeriusCalculatorMetadata>
    </imaer:metadata>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="9999" gml:id="ES.1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 1</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>0.5</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>POINT</imaer:sourceType>
                    <imaer:diameter>0.001</imaer:diameter>
                    <imaer:buoyancyType>TEMPERATURE</imaer:buoyancyType>
                    <imaer:temperature>15.0</imaer:temperature>
                    <imaer:effluxType>VELOCITY</imaer:effluxType>
                    <imaer:verticalVelocity>15.0</imaer:verticalVelocity>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.1.POINT">
                            <gml:pos>295614.14194156666 514168.4315086523</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>550.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>1000.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8463488" gml:id="CP.8463488">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8463488</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8463488.POINT">
                    <gml:pos>294724.15977177396 513453.6046775412</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8463488">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294786.0 513561.0 294848.0 513454.0 294786.0 513346.0 294662.0 513346.0 294600.0 513454.0 294662.0 513561.0 294786.0 513561.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00636343</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.033089835</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0115699</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0011650268</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.0080989</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8461703" gml:id="CP.8461703">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8461703</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8461703.POINT">
                    <gml:pos>294538.03879995353 513346.1476843588</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8461703">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294600.0 513454.0 294662.0 513346.0 294600.0 513239.0 294476.0 513239.0 294414.0 513346.0 294476.0 513454.0 294600.0 513454.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.005141463</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.026869288</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00934812</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>9.413091E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006543685</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8527752" gml:id="CP.8527752">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8527752</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8527752.POINT">
                    <gml:pos>296213.1275463374 517322.05643210595</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8527752">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296275.0 517430.0 296337.0 517322.0 296275.0 517215.0 296151.0 517215.0 296089.0 517322.0 296151.0 517430.0 296275.0 517430.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00265924</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.013551487</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00483499</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>4.5440203E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003384493</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8527753" gml:id="CP.8527753">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8527753</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8527753.POINT">
                    <gml:pos>296585.3694899782 517322.05643210595</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8527753">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296647.0 517430.0 296709.0 517322.0 296647.0 517215.0 296523.0 517215.0 296461.0 517322.0 296523.0 517430.0 296647.0 517430.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00261198</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.014600968</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00474906</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>5.4196746E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003324342</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8459917" gml:id="CP.8459917">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8459917</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8459917.POINT">
                    <gml:pos>294351.91782813316 513238.69069117645</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8459917">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294414.0 513346.0 294476.0 513239.0 294414.0 513131.0 294290.0 513131.0 294228.0 513239.0 294290.0 513346.0 294414.0 513346.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00415907</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.02119462</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.007562</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>7.106872E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005293365</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8459918" gml:id="CP.8459918">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8459918</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8459918.POINT">
                    <gml:pos>294724.15977177396 513238.69069117645</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8459918">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294786.0 513346.0 294848.0 513239.0 294786.0 513131.0 294662.0 513131.0 294600.0 513239.0 294662.0 513346.0 294786.0 513346.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00531369</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.027631188</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00966126</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>9.728403E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00676288</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8525968" gml:id="CP.8525968">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8525968</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8525968.POINT">
                    <gml:pos>296399.24851815775 517214.5994389236</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8525968">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296461.0 517322.0 296523.0 517215.0 296461.0 517107.0 296337.0 517107.0 296275.0 517215.0 296337.0 517322.0 296461.0 517322.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00280591</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.0149555</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00510166</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>5.1371165E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003571162</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8458133" gml:id="CP.8458133">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8458133</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8458133.POINT">
                    <gml:pos>294538.03879995353 513131.2336979941</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8458133">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294600.0 513239.0 294662.0 513131.0 294600.0 513024.0 294476.0 513024.0 294414.0 513131.0 294476.0 513239.0 294600.0 513239.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00465895</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.023742009</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00847081</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>7.961039E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005929569</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8524182" gml:id="CP.8524182">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8524182</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8524182.POINT">
                    <gml:pos>296213.1275463374 517107.1424457412</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8524182">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296275.0 517215.0 296337.0 517107.0 296275.0 517000.0 296151.0 517000.0 296089.0 517107.0 296151.0 517215.0 296275.0 517215.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00298018</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.015186997</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00541851</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>5.092424E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003792957</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8522398" gml:id="CP.8522398">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8522398</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8522398.POINT">
                    <gml:pos>296399.24851815775 516999.68545255886</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8522398">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296461.0 517107.0 296523.0 517000.0 296461.0 516892.0 296337.0 516892.0 296275.0 517000.0 296337.0 517107.0 296461.0 517107.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00313607</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.018019859</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00570194</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>6.889882E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003991358</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8456348" gml:id="CP.8456348">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8456348</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8456348.POINT">
                    <gml:pos>294724.15977177396 513023.7767048118</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8456348">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294786.0 513131.0 294848.0 513024.0 294786.0 512916.0 294662.0 512916.0 294600.0 513024.0 294662.0 513131.0 294786.0 513131.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00466566</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.024261432</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.008483019</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>8.5419754E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005938113</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8454564" gml:id="CP.8454564">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8454564</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8454564.POINT">
                    <gml:pos>294910.2807435944 512916.3197116294</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8454564">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294972.0 513024.0 295034.0 512916.0 294972.0 512809.0 294848.0 512809.0 294786.0 512916.0 294848.0 513024.0 294972.0 513024.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00487362</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.025469538</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00886112</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>8.922708E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006202786</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8520612" gml:id="CP.8520612">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8520612</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8520612.POINT">
                    <gml:pos>296213.1275463374 516892.2284593765</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8520612">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296275.0 517000.0 296337.0 516892.0 296275.0 516785.0 296151.0 516785.0 296089.0 516892.0 296151.0 517000.0 296275.0 517000.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00336844</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.017515888</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00612444</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>6.167005E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.004287108</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8452778" gml:id="CP.8452778">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8452778</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8452778.POINT">
                    <gml:pos>294724.15977177396 512808.86271844705</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8452778">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294786.0 512916.0 294848.0 512809.0 294786.0 512701.0 294662.0 512701.0 294600.0 512809.0 294662.0 512916.0 294786.0 512916.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00409577</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.020978535</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00744685</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>6.998699E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005212795</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8518828" gml:id="CP.8518828">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8518828</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8518828.POINT">
                    <gml:pos>296399.24851815775 516784.7714661942</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8518828">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296461.0 516892.0 296523.0 516785.0 296461.0 516677.0 296337.0 516677.0 296275.0 516785.0 296337.0 516892.0 296461.0 516892.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00352461</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.020435689</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00640838</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>8.1736967E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.004485866</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8450994" gml:id="CP.8450994">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8450994</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8450994.POINT">
                    <gml:pos>294910.2807435944 512701.4057252647</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8450994">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294972.0 512809.0 295034.0 512701.0 294972.0 512594.0 294848.0 512594.0 294786.0 512701.0 294848.0 512809.0 294972.0 512809.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00440054</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.022768393</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00800098</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>8.056587E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005600686</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8517042" gml:id="CP.8517042">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8517042</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8517042.POINT">
                    <gml:pos>296213.1275463374 516677.3144730118</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8517042">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296275.0 516785.0 296337.0 516677.0 296275.0 516570.0 296151.0 516570.0 296089.0 516677.0 296151.0 516785.0 296275.0 516785.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00383688</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.020849606</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00697615</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>7.492943E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.004883305</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8449208" gml:id="CP.8449208">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8449208</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8449208.POINT">
                    <gml:pos>294724.15977177396 512593.9487320823</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8449208">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294786.0 512701.0 294848.0 512594.0 294786.0 512486.0 294662.0 512486.0 294600.0 512594.0 294662.0 512701.0 294786.0 512701.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00361797</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.018437175</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00657813</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>6.1822584E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.004604691</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8515257" gml:id="CP.8515257">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8515257</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8515257.POINT">
                    <gml:pos>296027.00657451694 516569.85747982946</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8515257">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296089.0 516677.0 296151.0 516570.0 296089.0 516462.0 295965.0 516462.0 295903.0 516570.0 295965.0 516677.0 296089.0 516677.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00403269</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.020550588</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00733215</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>6.890901E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005132505</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8515258" gml:id="CP.8515258">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8515258</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8515258.POINT">
                    <gml:pos>296399.24851815775 516569.85747982946</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8515258">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296461.0 516677.0 296523.0 516570.0 296461.0 516462.0 296337.0 516462.0 296275.0 516570.0 296337.0 516677.0 296461.0 516677.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00396944</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.02218917</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00721716</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>8.720783E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005052012</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8447424" gml:id="CP.8447424">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8447424</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8447424.POINT">
                    <gml:pos>294910.2807435944 512486.4917389</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8447424">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294972.0 512594.0 295034.0 512486.0 294972.0 512379.0 294848.0 512379.0 294786.0 512486.0 294848.0 512594.0 294972.0 512594.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00381188</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.019722667</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00693069</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>6.978858E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.004851483</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8513472" gml:id="CP.8513472">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8513472</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8513472.POINT">
                    <gml:pos>296213.1275463374 516462.4004866471</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8513472">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296275.0 516570.0 296337.0 516462.0 296275.0 516355.0 296151.0 516355.0 296089.0 516462.0 296151.0 516570.0 296275.0 516570.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00440712</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.023604535</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00801295</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>8.606549E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005609065</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8511687" gml:id="CP.8511687">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8511687</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8511687.POINT">
                    <gml:pos>296027.00657451694 516354.9434934647</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8511687">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296089.0 516462.0 296151.0 516355.0 296089.0 516247.0 295965.0 516247.0 295903.0 516355.0 295965.0 516462.0 296089.0 516462.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00471905</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.024048278</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00858009</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>8.06374E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006006063</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8445639" gml:id="CP.8445639">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8445639</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8445639.POINT">
                    <gml:pos>295096.40171541483 512379.03474571765</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8445639">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>295158.0 512486.0 295220.0 512379.0 295158.0 512272.0 295034.0 512272.0 294972.0 512379.0 295034.0 512486.0 295158.0 512486.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00408339</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.021552132</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00742435</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>7.475949E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005197045</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8443854" gml:id="CP.8443854">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8443854</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8443854.POINT">
                    <gml:pos>294910.2807435944 512271.5777525353</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8443854">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294972.0 512379.0 295034.0 512272.0 294972.0 512164.0 294848.0 512164.0 294786.0 512272.0 294848.0 512379.0 294972.0 512379.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00337287</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.017188145</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00613249</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>5.763437E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.004292743</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8509902" gml:id="CP.8509902">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8509902</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8509902.POINT">
                    <gml:pos>296213.1275463374 516247.48650028236</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8509902">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296275.0 516355.0 296337.0 516247.0 296275.0 516140.0 296151.0 516140.0 296089.0 516247.0 296151.0 516355.0 296275.0 516355.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00510445</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.02747215</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00928082</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>9.968344E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006496574</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8508117" gml:id="CP.8508117">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8508117</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8508117.POINT">
                    <gml:pos>296027.00657451694 516140.02950710006</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8508117">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296089.0 516247.0 296151.0 516140.0 296089.0 516033.0 295965.0 516033.0 295903.0 516140.0 295965.0 516247.0 296089.0 516247.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00559492</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.029093584</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0101726</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0010243299</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00712082</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8442069" gml:id="CP.8442069">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8442069</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8442069.POINT">
                    <gml:pos>295096.40171541483 512164.1207593529</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8442069">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>295158.0 512272.0 295220.0 512164.0 295158.0 512057.0 295034.0 512057.0 294972.0 512164.0 295034.0 512272.0 295158.0 512272.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00361219</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.01868947</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00656761</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>6.6132547E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.004597327</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8506331" gml:id="CP.8506331">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8506331</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8506331.POINT">
                    <gml:pos>295840.8856026965 516032.5725139177</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8506331">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>295903.0 516140.0 295965.0 516033.0 295903.0 515925.0 295779.0 515925.0 295717.0 516033.0 295779.0 516140.0 295903.0 516140.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00566369</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.029303933</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0102976</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>9.6778903E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00720832</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8440284" gml:id="CP.8440284">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8440284</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8440284.POINT">
                    <gml:pos>294910.2807435944 512056.66376617056</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8440284">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294972.0 512164.0 295034.0 512057.0 294972.0 511949.0 294848.0 511949.0 294786.0 512057.0 294848.0 512164.0 294972.0 512164.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00301675</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.015373358</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00548499</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>5.154903E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003839493</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8440285" gml:id="CP.8440285">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8440285</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8440285.POINT">
                    <gml:pos>295282.52268723527 512056.66376617056</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8440285">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>295345.0 512164.0 295407.0 512057.0 295345.0 511949.0 295220.0 511949.0 295158.0 512057.0 295220.0 512164.0 295345.0 512164.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00374381</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.019467812</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00680693</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>6.8542385E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.004764851</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8504547" gml:id="CP.8504547">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8504547</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8504547.POINT">
                    <gml:pos>296027.00657451694 515925.11552073533</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8504547">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296089.0 516033.0 296151.0 515925.0 296089.0 515818.0 295965.0 515818.0 295903.0 515925.0 295965.0 516033.0 296089.0 516033.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00674382</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.04120474</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0122615</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.001646229</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00858305</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8438499" gml:id="CP.8438499">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8438499</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8438499.POINT">
                    <gml:pos>295096.40171541483 511949.2067729882</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8438499">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>295158.0 512057.0 295220.0 511949.0 295158.0 511842.0 295034.0 511842.0 294972.0 511949.0 295034.0 512057.0 295158.0 512057.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00320369</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.0164093</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00582489</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>5.474348E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.004077423</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8504546" gml:id="CP.8504546">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8504546</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8504546.POINT">
                    <gml:pos>295654.7646308761 515925.11552073533</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8504546">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>295717.0 516033.0 295779.0 515925.0 295717.0 515818.0 295593.0 515818.0 295531.0 515925.0 295593.0 516033.0 295717.0 516033.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00524096</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.026707932</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00952901</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>8.955554E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006670307</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8438501" gml:id="CP.8438501">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8438501</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8438501.POINT">
                    <gml:pos>295840.8856026965 511949.2067729882</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8438501">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>295903.0 512057.0 295965.0 511949.0 295903.0 511842.0 295779.0 511842.0 295717.0 511949.0 295779.0 512057.0 295903.0 512057.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00323865</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.017598825</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00588846</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>6.3246774E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.004121922</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8502761" gml:id="CP.8502761">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8502761</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8502761.POINT">
                    <gml:pos>295840.8856026965 515817.65852755297</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8502761">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>295903.0 515925.0 295965.0 515818.0 295903.0 515710.0 295779.0 515710.0 295717.0 515818.0 295779.0 515925.0 295903.0 515925.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00702865</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.03691447</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0127794</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0012868217</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00894558</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8436715" gml:id="CP.8436715">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8436715</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8436715.POINT">
                    <gml:pos>295282.52268723527 511841.7497798059</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8436715">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>295345.0 511949.0 295407.0 511842.0 295345.0 511734.0 295220.0 511734.0 295158.0 511842.0 295220.0 511949.0 295345.0 511949.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00327115</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.01683988</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00594754</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>5.589617E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.004163278</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8502762" gml:id="CP.8502762">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8502762</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8502762.POINT">
                    <gml:pos>296213.1275463374 515817.65852755297</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8502762">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296275.0 515925.0 296337.0 515818.0 296275.0 515710.0 296151.0 515710.0 296089.0 515818.0 296151.0 515925.0 296275.0 515925.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00690706</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.038071714</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0125583</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0014331657</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00879081</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8436716" gml:id="CP.8436716">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8436716</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8436716.POINT">
                    <gml:pos>295654.7646308761 511841.7497798059</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8436716">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>295717.0 511949.0 295779.0 511842.0 295717.0 511734.0 295593.0 511734.0 295531.0 511842.0 295593.0 511949.0 295717.0 511949.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00332071</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.017181354</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00603765</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>6.0796115E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.004226355</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8436717" gml:id="CP.8436717">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8436717</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8436717.POINT">
                    <gml:pos>296027.00657451694 511841.7497798059</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8436717">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296089.0 511949.0 296151.0 511842.0 296089.0 511734.0 295965.0 511734.0 295903.0 511842.0 295965.0 511949.0 296089.0 511949.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.0026659</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.020724706</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0048471</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>9.436189E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00339297</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8500977" gml:id="CP.8500977">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8500977</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8500977.POINT">
                    <gml:pos>296027.00657451694 515710.2015343706</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8500977">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296089.0 515818.0 296151.0 515710.0 296089.0 515603.0 295965.0 515603.0 295903.0 515710.0 295965.0 515818.0 296089.0 515818.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00831291</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.049062796</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.015114375</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0019277931</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.010580063</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8500976" gml:id="CP.8500976">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8500976</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8500976.POINT">
                    <gml:pos>295654.7646308761 515710.2015343706</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8500976">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>295717.0 515818.0 295779.0 515710.0 295717.0 515603.0 295593.0 515603.0 295531.0 515710.0 295593.0 515818.0 295717.0 515818.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00655896</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.03342446</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0119254</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0011207716</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00834777</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8434929" gml:id="CP.8434929">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8434929</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8434929.POINT">
                    <gml:pos>295096.40171541483 511734.2927866235</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8434929">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>295158.0 511842.0 295220.0 511734.0 295158.0 511627.0 295034.0 511627.0 294972.0 511734.0 295034.0 511842.0 295158.0 511842.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00285736</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.014561106</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0051952</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>4.882553E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00363664</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8434930" gml:id="CP.8434930">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8434930</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8434930.POINT">
                    <gml:pos>295468.6436590557 511734.2927866235</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8434930">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>295531.0 511842.0 295593.0 511734.0 295531.0 511627.0 295407.0 511627.0 295345.0 511734.0 295407.0 511842.0 295531.0 511842.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00316409</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.016124202</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0057529</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>5.406691E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00402703</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8434931" gml:id="CP.8434931">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8434931</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8434931.POINT">
                    <gml:pos>295840.8856026965 511734.2927866235</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8434931">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>295903.0 511842.0 295965.0 511734.0 295903.0 511627.0 295779.0 511627.0 295717.0 511734.0 295779.0 511842.0 295903.0 511842.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00285698</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.01455917</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0051945</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>4.881895E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00363615</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8434932" gml:id="CP.8434932">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8434932</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8434932.POINT">
                    <gml:pos>296213.1275463374 511734.2927866235</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8434932">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296275.0 511842.0 296337.0 511734.0 296275.0 511627.0 296151.0 511627.0 296089.0 511734.0 296151.0 511842.0 296275.0 511842.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.0021875</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.01717625</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00397727</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>8.009824E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.002784089</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8499191" gml:id="CP.8499191">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8499191</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8499191.POINT">
                    <gml:pos>295840.8856026965 515602.74454118824</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8499191">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>295903.0 515710.0 295965.0 515603.0 295903.0 515495.0 295779.0 515495.0 295717.0 515603.0 295779.0 515710.0 295903.0 515710.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.008974622</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.060668442</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0163175</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0026289427</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.01142224</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8499190" gml:id="CP.8499190">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8499190</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8499190.POINT">
                    <gml:pos>295468.6436590557 515602.74454118824</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8499190">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>295531.0 515710.0 295593.0 515603.0 295531.0 515495.0 295407.0 515495.0 295345.0 515603.0 295407.0 515710.0 295531.0 515710.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00614352</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.031307377</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0111701</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0010497837</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.007819035</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8433145" gml:id="CP.8433145">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8433145</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8433145.POINT">
                    <gml:pos>295282.52268723527 511626.83579344116</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8433145">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>295345.0 511734.0 295407.0 511627.0 295345.0 511519.0 295220.0 511519.0 295158.0 511627.0 295220.0 511734.0 295345.0 511734.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00288118</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.014682493</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00523851</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>4.9232563E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003666957</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8433146" gml:id="CP.8433146">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8433146</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8433146.POINT">
                    <gml:pos>295654.7646308761 511626.83579344116</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8433146">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>295717.0 511734.0 295779.0 511627.0 295717.0 511519.0 295593.0 511519.0 295531.0 511627.0 295593.0 511734.0 295717.0 511734.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00290205</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.0147888465</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00527645</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>4.958913E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003693515</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8433147" gml:id="CP.8433147">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8433147</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8433147.POINT">
                    <gml:pos>296027.00657451694 511626.83579344116</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8433147">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296089.0 511734.0 296151.0 511627.0 296089.0 511519.0 295965.0 511519.0 295903.0 511627.0 295965.0 511734.0 296089.0 511734.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00240331</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.012747156</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00436965</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>4.400019E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003058755</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8497405" gml:id="CP.8497405">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8497405</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8497405.POINT">
                    <gml:pos>295282.52268723527 515495.28754800593</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8497405">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>295345.0 515603.0 295407.0 515495.0 295345.0 515388.0 295220.0 515388.0 295158.0 515495.0 295220.0 515603.0 295345.0 515603.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00541968</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.028182335</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00985397</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>9.922453E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.0068977773</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8433148" gml:id="CP.8433148">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8433148</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8433148.POINT">
                    <gml:pos>296399.24851815775 511626.83579344116</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8433148">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296461.0 511734.0 296523.0 511627.0 296461.0 511519.0 296337.0 511519.0 296275.0 511627.0 296337.0 511734.0 296461.0 511734.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.0018067</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.010475246</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0032849</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>3.9692761E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00229943</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8497407" gml:id="CP.8497407">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8497407</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8497407.POINT">
                    <gml:pos>296027.00657451694 515495.28754800593</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8497407">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296089.0 515603.0 296151.0 515495.0 296089.0 515388.0 295965.0 515388.0 295903.0 515495.0 295965.0 515603.0 296089.0 515603.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00986951</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.054913953</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0179446</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.00204785</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.012561185</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8497406" gml:id="CP.8497406">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8497406</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8497406.POINT">
                    <gml:pos>295654.7646308761 515495.28754800593</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8497406">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>295717.0 515603.0 295779.0 515495.0 295717.0 515388.0 295593.0 515388.0 295531.0 515495.0 295593.0 515603.0 295717.0 515603.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00850284</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.058584567</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.015459716</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0025945266</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.0108218</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8495619" gml:id="CP.8495619">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8495619</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8495619.POINT">
                    <gml:pos>295096.40171541483 515387.83055482357</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8495619">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>295158.0 515495.0 295220.0 515388.0 295158.0 515280.0 295034.0 515280.0 294972.0 515388.0 295034.0 515495.0 295158.0 515495.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.0045468835</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.023407355</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00826706</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>7.769544E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005786939</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8431362" gml:id="CP.8431362">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8431362</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8431362.POINT">
                    <gml:pos>296213.1275463374 511519.3788002588</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8431362">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296275.0 511627.0 296337.0 511519.0 296275.0 511412.0 296151.0 511412.0 296089.0 511519.0 296151.0 511627.0 296275.0 511627.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00201259</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.0117736515</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00365926</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>4.4216303E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.002561482</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8495621" gml:id="CP.8495621">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8495621</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8495621.POINT">
                    <gml:pos>295840.8856026965 515387.83055482357</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8495621">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>295903.0 515495.0 295965.0 515388.0 295903.0 515280.0 295779.0 515280.0 295717.0 515388.0 295779.0 515495.0 295903.0 515495.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.0108833</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.061686546</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.01978785</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.002391046</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.0138515</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8495620" gml:id="CP.8495620">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8495620</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8495620.POINT">
                    <gml:pos>295468.6436590557 515387.83055482357</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8495620">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>295531.0 515495.0 295593.0 515388.0 295531.0 515280.0 295407.0 515280.0 295345.0 515388.0 295407.0 515495.0 295531.0 515495.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.0073585</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.041134015</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0133791</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0015268412</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.0093654</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8429577" gml:id="CP.8429577">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8429577</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8429577.POINT">
                    <gml:pos>296027.00657451694 511411.92180707643</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8429577">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296089.0 511519.0 296151.0 511412.0 296089.0 511304.0 295965.0 511304.0 295903.0 511412.0 295965.0 511519.0 296089.0 511519.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.0021758</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.011087877</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00395599</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>3.7179186E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.002769193</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8493835" gml:id="CP.8493835">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8493835</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8493835.POINT">
                    <gml:pos>295282.52268723527 515280.3735616412</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8493835">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>295345.0 515388.0 295407.0 515280.0 295345.0 515173.0 295220.0 515173.0 295158.0 515280.0 295220.0 515388.0 295345.0 515388.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00611226</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.03194267</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0111132</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0011190437</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00777924</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8429578" gml:id="CP.8429578">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8429578</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8429578.POINT">
                    <gml:pos>296399.24851815775 511411.92180707643</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8429578">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296461.0 511519.0 296523.0 511412.0 296461.0 511304.0 296337.0 511304.0 296275.0 511412.0 296337.0 511519.0 296461.0 511519.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00168885</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.00873811</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00307064</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>3.091981E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.002149448</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8493834" gml:id="CP.8493834">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8493834</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8493834.POINT">
                    <gml:pos>294910.2807435944 515280.3735616412</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8493834">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294972.0 515388.0 295034.0 515280.0 294972.0 515173.0 294848.0 515173.0 294786.0 515280.0 294848.0 515388.0 294972.0 515388.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00410374</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.021019356</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00746135</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>7.012322E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005222942</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8493836" gml:id="CP.8493836">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8493836</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8493836.POINT">
                    <gml:pos>295654.7646308761 515280.3735616412</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8493836">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>295717.0 515388.0 295779.0 515280.0 295717.0 515173.0 295593.0 515173.0 295531.0 515280.0 295593.0 515388.0 295717.0 515388.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.01011669</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.055237126</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.018394</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0020991417</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.0128758</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8492049" gml:id="CP.8492049">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8492049</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8492049.POINT">
                    <gml:pos>295096.40171541483 515172.91656845884</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8492049">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>295158.0 515280.0 295220.0 515173.0 295158.0 515065.0 295034.0 515065.0 294972.0 515173.0 295034.0 515280.0 295158.0 515280.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00508288</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.02735606</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.009241607</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>9.926226E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006469125</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8427792" gml:id="CP.8427792">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8427792</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8427792.POINT">
                    <gml:pos>296213.1275463374 511304.46481389407</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8427792">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296275.0 511412.0 296337.0 511304.0 296275.0 511197.0 296151.0 511197.0 296089.0 511304.0 296151.0 511412.0 296275.0 511412.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00185454</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.0094507355</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0033719</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>3.168979E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00236033</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8492048" gml:id="CP.8492048">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8492048</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8492048.POINT">
                    <gml:pos>294724.15977177396 515172.91656845884</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8492048">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294786.0 515280.0 294848.0 515173.0 294786.0 515065.0 294662.0 515065.0 294600.0 515173.0 294662.0 515280.0 294786.0 515280.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.003257</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.016767036</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00592177</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>5.565398E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.004145239</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8427793" gml:id="CP.8427793">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8427793</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8427793.POINT">
                    <gml:pos>296585.3694899782 511304.46481389407</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8427793">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296647.0 511412.0 296709.0 511304.0 296647.0 511197.0 296523.0 511197.0 296461.0 511304.0 296523.0 511412.0 296647.0 511412.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00143175</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.007370649</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00260319</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>2.44653E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.001822233</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8490263" gml:id="CP.8490263">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8490263</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8490263.POINT">
                    <gml:pos>294538.03879995353 515065.4595752765</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8490263">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294600.0 515173.0 294662.0 515065.0 294600.0 514958.0 294476.0 514958.0 294414.0 515065.0 294476.0 515173.0 294600.0 515173.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00244676</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.013931852</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00444864</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>5.37547E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003114048</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8426007" gml:id="CP.8426007">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8426007</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8426007.POINT">
                    <gml:pos>296027.00657451694 511197.00782071176</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8426007">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296089.0 511304.0 296151.0 511197.0 296089.0 511090.0 295965.0 511090.0 295903.0 511197.0 295965.0 511304.0 296089.0 511304.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00197801</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.010079939</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00359638</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>3.3799498E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.002517466</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8490262" gml:id="CP.8490262">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8490262</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8490262.POINT">
                    <gml:pos>294165.7968563127 515065.4595752765</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8490262">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294228.0 515173.0 294290.0 515065.0 294228.0 514958.0 294104.0 514958.0 294042.0 515065.0 294104.0 515173.0 294228.0 515173.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00179218</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.00913295</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0032585</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>3.0624034E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00228095</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8426008" gml:id="CP.8426008">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8426008</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8426008.POINT">
                    <gml:pos>296399.24851815775 511197.00782071176</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8426008">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296461.0 511304.0 296523.0 511197.0 296461.0 511090.0 296337.0 511090.0 296275.0 511197.0 296337.0 511304.0 296461.0 511304.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00158054</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.0080544315</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00287371</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>2.7007703E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.002011597</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8490264" gml:id="CP.8490264">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8490264</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8490264.POINT">
                    <gml:pos>294910.2807435944 515065.4595752765</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8490264">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294972.0 515173.0 295034.0 515065.0 294972.0 514958.0 294848.0 514958.0 294786.0 515065.0 294848.0 515173.0 294972.0 515173.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00429412</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.022329424</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00780749</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>7.861755E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005465245</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8426009" gml:id="CP.8426009">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8426009</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8426009.POINT">
                    <gml:pos>296771.4904617986 511197.00782071176</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8426009">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296834.0 511304.0 296896.0 511197.0 296834.0 511090.0 296709.0 511090.0 296647.0 511197.0 296709.0 511304.0 296834.0 511304.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00123444</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.006419088</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00224444</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>2.2600389E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.001571108</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8488477" gml:id="CP.8488477">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8488477</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8488477.POINT">
                    <gml:pos>294351.91782813316 514958.0025820941</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8488477">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294414.0 515065.0 294476.0 514958.0 294414.0 514851.0 294290.0 514851.0 294228.0 514958.0 294290.0 515065.0 294414.0 515065.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.002180125</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.012923781</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00396386</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>5.055789E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.0027747042</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8488476" gml:id="CP.8488476">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8488476</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8488476.POINT">
                    <gml:pos>293979.6758844923 514958.0025820941</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8488476">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294042.0 515065.0 294104.0 514958.0 294042.0 514851.0 293918.0 514851.0 293856.0 514958.0 293918.0 515065.0 294042.0 515065.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00179544</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.009149563</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00326444</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>3.067986E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.002285108</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8424222" gml:id="CP.8424222">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8424222</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8424222.POINT">
                    <gml:pos>296213.1275463374 511089.5508275294</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8424222">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296275.0 511197.0 296337.0 511090.0 296275.0 510982.0 296151.0 510982.0 296089.0 511090.0 296151.0 511197.0 296275.0 511197.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00171268</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.008727817</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00311396</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>2.926562E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.002179772</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8488478" gml:id="CP.8488478">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8488478</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8488478.POINT">
                    <gml:pos>294724.15977177396 514958.0025820941</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8488478">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294786.0 515065.0 294848.0 514958.0 294786.0 514851.0 294662.0 514851.0 294600.0 514958.0 294662.0 515065.0 294786.0 515065.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00325526</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.0172659</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00591865</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>6.3571037E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.004143055</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8424223" gml:id="CP.8424223">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8424223</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8424223.POINT">
                    <gml:pos>296585.3694899782 511089.5508275294</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8424223">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296647.0 511197.0 296709.0 511090.0 296647.0 510982.0 296523.0 510982.0 296461.0 511090.0 296523.0 511197.0 296647.0 511197.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.0013556</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.006908138</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00246472</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>2.3163932E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.001725304</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8486693" gml:id="CP.8486693">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8486693</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8486693.POINT">
                    <gml:pos>294538.03879995353 514850.5455889118</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8486693">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294600.0 514958.0 294662.0 514851.0 294600.0 514743.0 294476.0 514743.0 294414.0 514851.0 294476.0 514958.0 294600.0 514958.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00255597</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.0133575</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00464723</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>4.679524E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003253058</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8486692" gml:id="CP.8486692">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8486692</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8486692.POINT">
                    <gml:pos>294165.7968563127 514850.5455889118</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8486692">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294228.0 514958.0 294290.0 514851.0 294228.0 514743.0 294104.0 514743.0 294042.0 514851.0 294104.0 514958.0 294228.0 514958.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00214756</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.011725677</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00390466</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>4.1939173E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.002733262</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8422438" gml:id="CP.8422438">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8422438</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8422438.POINT">
                    <gml:pos>296399.24851815775 510982.09383434703</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8422438">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296461.0 511090.0 296523.0 510982.0 296461.0 510875.0 296337.0 510875.0 296275.0 510982.0 296337.0 511090.0 296461.0 511090.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00147963</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.0075401943</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00269023</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>2.5283318E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.001883161</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8422439" gml:id="CP.8422439">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8422439</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8422439.POINT">
                    <gml:pos>296771.4904617986 510982.09383434703</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8422439">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296834.0 511090.0 296896.0 510982.0 296834.0 510875.0 296709.0 510875.0 296647.0 510982.0 296709.0 511090.0 296834.0 511090.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.0011771</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.006029106</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00214018</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>2.0113839E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.001498126</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8484907" gml:id="CP.8484907">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8484907</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8484907.POINT">
                    <gml:pos>294351.91782813316 514743.08859572944</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8484907">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294414.0 514851.0 294476.0 514743.0 294414.0 514636.0 294290.0 514636.0 294228.0 514743.0 294290.0 514851.0 294414.0 514851.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00247348</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.012990717</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.004497235</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>4.5284914E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003148065</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8484906" gml:id="CP.8484906">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8484906</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8484906.POINT">
                    <gml:pos>293979.6758844923 514743.08859572944</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8484906">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294042.0 514851.0 294104.0 514743.0 294042.0 514636.0 293918.0 514636.0 293856.0 514743.0 293918.0 514851.0 294042.0 514851.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00210576</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.01149745</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00382865</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>4.1122764E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.002680055</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8420653" gml:id="CP.8420653">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8420653</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8420653.POINT">
                    <gml:pos>296585.3694899782 510874.63684116467</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8420653">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296647.0 510982.0 296709.0 510875.0 296647.0 510767.0 296523.0 510767.0 296461.0 510875.0 296523.0 510982.0 296647.0 510982.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00128367</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.0065415823</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00233395</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>2.1934928E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.001633765</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8420654" gml:id="CP.8420654">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8420654</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8420654.POINT">
                    <gml:pos>296957.61143361905 510874.63684116467</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8420654">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>297020.0 510982.0 297082.0 510875.0 297020.0 510767.0 296896.0 510767.0 296834.0 510875.0 296896.0 510982.0 297020.0 510982.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.0010372</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.0059867185</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00188581</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>2.2786997E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.001320067</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8483121" gml:id="CP.8483121">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8483121</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8483121.POINT">
                    <gml:pos>293793.55491267185 514635.6316025471</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8483121">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>293856.0 514743.0 293918.0 514636.0 293856.0 514528.0 293732.0 514528.0 293669.0 514636.0 293732.0 514743.0 293856.0 514743.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00204259</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.0104090385</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00371381</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>3.490313E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.002599667</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8418868" gml:id="CP.8418868">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8418868</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8418868.POINT">
                    <gml:pos>296399.24851815775 510767.1798479823</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8418868">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296461.0 510875.0 296523.0 510767.0 296461.0 510660.0 296337.0 510660.0 296275.0 510767.0 296337.0 510875.0 296461.0 510875.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00138611</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.0070636165</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0025202</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>2.3685343E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00176414</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8418869" gml:id="CP.8418869">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8418869</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8418869.POINT">
                    <gml:pos>296771.4904617986 510767.1798479823</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8418869">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296834.0 510875.0 296896.0 510767.0 296834.0 510660.0 296709.0 510660.0 296647.0 510767.0 296709.0 510875.0 296834.0 510875.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00112379</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.005726834</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00204326</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>1.9202966E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.001430282</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8481336" gml:id="CP.8481336">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8481336</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8481336.POINT">
                    <gml:pos>293979.6758844923 514528.1746093647</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8481336">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294042.0 514636.0 294104.0 514528.0 294042.0 514421.0 293918.0 514421.0 293856.0 514528.0 293918.0 514636.0 294042.0 514636.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00242848</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.012754377</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00441542</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>4.4461072E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003090794</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8417083" gml:id="CP.8417083">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8417083</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8417083.POINT">
                    <gml:pos>296585.3694899782 510659.72285479994</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8417083">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296647.0 510767.0 296709.0 510660.0 296647.0 510552.0 296523.0 510552.0 296461.0 510660.0 296523.0 510767.0 296647.0 510767.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00121553</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.006194341</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00221006</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>2.0770586E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.001547042</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8417084" gml:id="CP.8417084">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8417084</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8417084.POINT">
                    <gml:pos>296957.61143361905 510659.72285479994</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8417084">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>297020.0 510767.0 297082.0 510660.0 297020.0 510552.0 296896.0 510552.0 296834.0 510660.0 296896.0 510767.0 297020.0 510767.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>9.94785E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.005974679</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0018087</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>2.3069426E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00126609</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8479551" gml:id="CP.8479551">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8479551</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8479551.POINT">
                    <gml:pos>293793.55491267185 514420.71761618234</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8479551">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>293856.0 514528.0 293918.0 514421.0 293856.0 514313.0 293732.0 514313.0 293669.0 514421.0 293732.0 514528.0 293856.0 514528.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00227106</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.011573322</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00412921</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>3.880714E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.002890447</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8415299" gml:id="CP.8415299">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8415299</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8415299.POINT">
                    <gml:pos>296771.4904617986 510552.26586161763</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8415299">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296834.0 510660.0 296896.0 510552.0 296834.0 510445.0 296709.0 510445.0 296647.0 510552.0 296709.0 510660.0 296834.0 510660.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00107249</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.0054654093</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00194997</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>1.8326208E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.001364979</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8415300" gml:id="CP.8415300">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8415300</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8415300.POINT">
                    <gml:pos>297143.7324054394 510552.26586161763</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8415300">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>297206.0 510660.0 297268.0 510552.0 297206.0 510445.0 297082.0 510445.0 297020.0 510552.0 297082.0 510660.0 297206.0 510660.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.9186E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.0046144836</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00162156</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>1.6328298E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.001135092</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8477766" gml:id="CP.8477766">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8477766</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8477766.POINT">
                    <gml:pos>293979.6758844923 514313.26062300004</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8477766">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294042.0 514421.0 294104.0 514313.0 294042.0 514206.0 293918.0 514206.0 293856.0 514313.0 293918.0 514421.0 294042.0 514421.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.0026655</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.013999206</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00484637</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>4.8800523E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003392459</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8413513" gml:id="CP.8413513">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8413513</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8413513.POINT">
                    <gml:pos>296585.3694899782 510444.80886843527</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8413513">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296647.0 510552.0 296709.0 510445.0 296647.0 510337.0 296523.0 510337.0 296461.0 510445.0 296523.0 510552.0 296647.0 510552.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00115031</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.00586198</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00209148</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>1.9656148E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.001464036</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8413514" gml:id="CP.8413514">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8413514</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8413514.POINT">
                    <gml:pos>296957.61143361905 510444.80886843527</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8413514">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>297020.0 510552.0 297082.0 510445.0 297020.0 510337.0 296896.0 510337.0 296834.0 510445.0 296896.0 510552.0 297020.0 510552.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>9.54956E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.0048664557</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00173628</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>1.6317907E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.001215396</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8411729" gml:id="CP.8411729">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8411729</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8411729.POINT">
                    <gml:pos>296771.4904617986 510337.3518752529</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8411729">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296834.0 510445.0 296896.0 510337.0 296834.0 510230.0 296709.0 510230.0 296647.0 510337.0 296709.0 510445.0 296834.0 510445.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00102316</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.0052140234</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00186029</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>1.7483378E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.001302203</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8411730" gml:id="CP.8411730">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8411730</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8411730.POINT">
                    <gml:pos>297143.7324054394 510337.3518752529</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8411730">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>297206.0 510445.0 297268.0 510337.0 297206.0 510230.0 297082.0 510230.0 297020.0 510337.0 297082.0 510445.0 297206.0 510445.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.59221E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.004490289</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00156222</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>1.5730775E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.001093554</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8474196" gml:id="CP.8474196">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8474196</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8474196.POINT">
                    <gml:pos>293979.6758844923 514098.3466366353</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8474196">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294042.0 514206.0 294104.0 514098.0 294042.0 513991.0 293918.0 513991.0 293856.0 514098.0 293918.0 514206.0 294042.0 514206.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00269078</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.013992056</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00489233</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>4.9263315E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003424631</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8409944" gml:id="CP.8409944">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8409944</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8409944.POINT">
                    <gml:pos>296957.61143361905 510229.89488207054</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8409944">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>297020.0 510337.0 297082.0 510230.0 297020.0 510122.0 296896.0 510122.0 296834.0 510230.0 296896.0 510337.0 297020.0 510337.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>9.16885E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.004672446</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00166706</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>1.5667363E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.001166942</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8472412" gml:id="CP.8472412">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8472412</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8472412.POINT">
                    <gml:pos>294165.7968563127 513990.88964345295</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8472412">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294228.0 514098.0 294290.0 513991.0 294228.0 513883.0 294104.0 513883.0 294042.0 513991.0 294104.0 514098.0 294228.0 514098.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.0029138</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.015227519</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00529788</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>5.334697E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003708514</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8408159" gml:id="CP.8408159">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8408159</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8408159.POINT">
                    <gml:pos>296771.4904617986 510122.4378888882</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8408159">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>296834.0 510230.0 296896.0 510122.0 296834.0 510015.0 296709.0 510015.0 296647.0 510122.0 296709.0 510230.0 296834.0 510230.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>9.76211E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.0049747713</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00177493</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>1.6681147E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.001242451</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8470626" gml:id="CP.8470626">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8470626</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8470626.POINT">
                    <gml:pos>293979.6758844923 513883.4326502706</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8470626">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294042.0 513991.0 294104.0 513883.0 294042.0 513776.0 293918.0 513776.0 293856.0 513883.0 293918.0 513991.0 294042.0 513991.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00248336</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.012848904</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0045152</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>4.2434753E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00316064</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8468842" gml:id="CP.8468842">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8468842</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8468842.POINT">
                    <gml:pos>294165.7968563127 513775.9756570882</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8468842">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294228.0 513883.0 294290.0 513776.0 294228.0 513669.0 294104.0 513669.0 294042.0 513776.0 294104.0 513883.0 294228.0 513883.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00271109</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.014097668</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00492925</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>4.963511E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003450477</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8465273" gml:id="CP.8465273">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8465273</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8465273.POINT">
                    <gml:pos>294538.03879995353 513561.06167072355</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8465273">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294600.0 513669.0 294662.0 513561.0 294600.0 513454.0 294476.0 513454.0 294414.0 513561.0 294476.0 513669.0 294600.0 513669.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00558241</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.029318817</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.01014983</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0010220368</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.007104879</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8465272" gml:id="CP.8465272">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8465272</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8465272.POINT">
                    <gml:pos>294165.7968563127 513561.06167072355</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8465272">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294228.0 513669.0 294290.0 513561.0 294228.0 513454.0 294104.0 513454.0 294042.0 513561.0 294104.0 513669.0 294228.0 513669.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.0029764625</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.015322829</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00541176</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>5.086076E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003788229</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="8463487" gml:id="CP.8463487">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8463487</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.8463487.POINT">
                    <gml:pos>294351.91782813316 513453.6046775412</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.8463487">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>294414.0 513561.0 294476.0 513454.0 294414.0 513346.0 294290.0 513346.0 294228.0 513454.0 294290.0 513561.0 294414.0 513561.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00386043</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.020074235</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00701897</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>7.067752E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.004913279</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8454564" subPointId="1" gml:id="SP.8454564_1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8454564_1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8454564_1.POINT">
                    <gml:pos>294848.25 512952.12</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00498257</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00905923</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006341461</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8492048" subPointId="3" gml:id="SP.8492048_3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8492048_3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8492048_3.POINT">
                    <gml:pos>294786.2 515208.75</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00344784</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0062688</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00438816</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8499191" subPointId="6" gml:id="SP.8499191_6">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8499191_6</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8499191_6.POINT">
                    <gml:pos>295902.94 515566.94</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00968342</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0176062</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.01232434</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8492048" subPointId="4" gml:id="SP.8492048_4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8492048_4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8492048_4.POINT">
                    <gml:pos>294662.12 515137.1</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00287071</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00521947</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003653629</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8456348" subPointId="1" gml:id="SP.8456348_1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8456348_1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8456348_1.POINT">
                    <gml:pos>294662.12 513059.6</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00453519</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00824581</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005772067</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8492048" subPointId="5" gml:id="SP.8492048_5">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8492048_5</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8492048_5.POINT">
                    <gml:pos>294724.16 515101.28</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00318975</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00579954</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.004059678</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8456348" subPointId="2" gml:id="SP.8456348_2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8456348_2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8456348_2.POINT">
                    <gml:pos>294724.16 513095.4</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00488612</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00888385</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006218695</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8492048" subPointId="6" gml:id="SP.8492048_6">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8492048_6</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8492048_6.POINT">
                    <gml:pos>294786.2 515137.1</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.0035196</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00639927</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.004479489</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8456348" subPointId="3" gml:id="SP.8456348_3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8456348_3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8456348_3.POINT">
                    <gml:pos>294786.2 513059.6</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00502857</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00914284</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006399988</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8456348" subPointId="4" gml:id="SP.8456348_4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8456348_4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8456348_4.POINT">
                    <gml:pos>294662.12 512987.97</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00430677</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0078305</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00548135</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8456348" subPointId="5" gml:id="SP.8456348_5">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8456348_5</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8456348_5.POINT">
                    <gml:pos>294724.16 512952.12</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00443692</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00806712</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005646984</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8456348" subPointId="6" gml:id="SP.8456348_6">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8456348_6</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8456348_6.POINT">
                    <gml:pos>294786.2 512987.97</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00480039</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00872799</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006109593</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8497407" subPointId="1" gml:id="SP.8497407_1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8497407_1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8497407_1.POINT">
                    <gml:pos>295964.97 515531.1</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.010149</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0184527</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.01291689</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8497407" subPointId="2" gml:id="SP.8497407_2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8497407_2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8497407_2.POINT">
                    <gml:pos>296027.0 515566.94</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00959002</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0174364</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.01220548</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8499191" subPointId="2" gml:id="SP.8499191_2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8499191_2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8499191_2.POINT">
                    <gml:pos>295840.88 515674.38</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00826469</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0150267</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.01051869</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8499191" subPointId="3" gml:id="SP.8499191_3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8499191_3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8499191_3.POINT">
                    <gml:pos>295902.94 515638.56</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00888568</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0161558</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.01130906</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8490264" subPointId="1" gml:id="SP.8490264_1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8490264_1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8490264_1.POINT">
                    <gml:pos>294848.25 515101.28</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00395265</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00718664</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005030648</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8499191" subPointId="4" gml:id="SP.8499191_4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8499191_4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8499191_4.POINT">
                    <gml:pos>295778.84 515566.94</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00896007</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.016291</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.0114037</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8490264" subPointId="2" gml:id="SP.8490264_2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8490264_2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8490264_2.POINT">
                    <gml:pos>294910.28 515137.1</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00431531</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00784602</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005492214</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8499191" subPointId="5" gml:id="SP.8499191_5">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8499191_5</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8499191_5.POINT">
                    <gml:pos>295840.88 515531.1</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00986869</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0179431</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.01256017</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8490264" subPointId="3" gml:id="SP.8490264_3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8490264_3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8490264_3.POINT">
                    <gml:pos>294972.3 515101.28</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.0048726</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00885927</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006201489</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8490264" subPointId="4" gml:id="SP.8490264_4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8490264_4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8490264_4.POINT">
                    <gml:pos>294848.25 515029.62</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00403592</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00733804</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005136628</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8499191" subPointId="1" gml:id="SP.8499191_1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8499191_1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8499191_1.POINT">
                    <gml:pos>295778.84 515638.56</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00818518</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0148821</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.01041747</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8490263" subPointId="6" gml:id="SP.8490263_6">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8490263_6</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8490263_6.POINT">
                    <gml:pos>294600.1 515029.62</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00265468</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00482669</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003378683</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8499190" subPointId="5" gml:id="SP.8499190_5">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8499190_5</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8499190_5.POINT">
                    <gml:pos>295468.66 515531.1</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00631334</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0114788</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00803516</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8499190" subPointId="6" gml:id="SP.8499190_6">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8499190_6</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8499190_6.POINT">
                    <gml:pos>295530.7 515566.94</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00656132</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0119297</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00835079</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8497406" subPointId="4" gml:id="SP.8497406_4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8497406_4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8497406_4.POINT">
                    <gml:pos>295592.72 515459.47</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00812278</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0147687</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.01033809</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8497406" subPointId="3" gml:id="SP.8497406_3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8497406_3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8497406_3.POINT">
                    <gml:pos>295716.8 515531.1</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00876466</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0159357</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.01115499</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8497406" subPointId="2" gml:id="SP.8497406_2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8497406_2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8497406_2.POINT">
                    <gml:pos>295654.75 515566.94</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00772227</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0140405</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00982835</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8497406" subPointId="1" gml:id="SP.8497406_1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8497406_1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8497406_1.POINT">
                    <gml:pos>295592.72 515531.1</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00742337</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.013497</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.0094479</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8468842" subPointId="5" gml:id="SP.8468842_5">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8468842_5</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8468842_5.POINT">
                    <gml:pos>294165.8 513704.34</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00274536</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00499157</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003494099</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8497406" subPointId="6" gml:id="SP.8497406_6">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8497406_6</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8497406_6.POINT">
                    <gml:pos>295716.8 515459.47</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00966519</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0175731</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.01230117</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8499190" subPointId="3" gml:id="SP.8499190_3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8499190_3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8499190_3.POINT">
                    <gml:pos>295530.7 515638.56</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00606662</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0110302</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00772114</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8490263" subPointId="1" gml:id="SP.8490263_1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8490263_1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8490263_1.POINT">
                    <gml:pos>294476.0 515101.28</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00223414</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00406206</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.002843442</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8497406" subPointId="5" gml:id="SP.8497406_5">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8497406_5</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8497406_5.POINT">
                    <gml:pos>295654.75 515423.66</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00931879</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0169433</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.01186031</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8499190" subPointId="4" gml:id="SP.8499190_4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8499190_4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8499190_4.POINT">
                    <gml:pos>295406.6 515566.94</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00563281</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0102415</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00716905</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8490263" subPointId="2" gml:id="SP.8490263_2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8490263_2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8490263_2.POINT">
                    <gml:pos>294538.03 515137.1</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00239463</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00435387</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003047709</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8468842" subPointId="2" gml:id="SP.8468842_2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8468842_2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8468842_2.POINT">
                    <gml:pos>294165.8 513847.62</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00284918</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00518033</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003626231</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8490263" subPointId="3" gml:id="SP.8490263_3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8490263_3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8490263_3.POINT">
                    <gml:pos>294600.1 515101.28</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00262063</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00476478</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003335346</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8454564" subPointId="5" gml:id="SP.8454564_5">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8454564_5</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8454564_5.POINT">
                    <gml:pos>294910.28 512844.7</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00488074</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00887406</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006211842</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8468842" subPointId="1" gml:id="SP.8468842_1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8468842_1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8468842_1.POINT">
                    <gml:pos>294103.75 513811.78</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00265498</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00482723</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003379061</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8490263" subPointId="4" gml:id="SP.8490263_4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8490263_4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8490263_4.POINT">
                    <gml:pos>294476.0 515029.62</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00229027</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00416412</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.002914884</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8454564" subPointId="4" gml:id="SP.8454564_4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8454564_4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8454564_4.POINT">
                    <gml:pos>294848.25 512880.5</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00475754</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00865008</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006055056</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8468842" subPointId="4" gml:id="SP.8468842_4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8468842_4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8468842_4.POINT">
                    <gml:pos>294103.75 513740.16</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00259483</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00471788</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003302516</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8490263" subPointId="5" gml:id="SP.8490263_5">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8490263_5</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8490263_5.POINT">
                    <gml:pos>294538.03 514993.8</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00248618</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00452032</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003164224</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8500977" subPointId="5" gml:id="SP.8500977_5">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8500977_5</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8500977_5.POINT">
                    <gml:pos>296027.0 515638.56</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00890018</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0161821</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.01132747</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8472412" subPointId="1" gml:id="SP.8472412_1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8472412_1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8472412_1.POINT">
                    <gml:pos>294103.75 514026.72</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00293907</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00534377</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003740639</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8500977" subPointId="4" gml:id="SP.8500977_4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8500977_4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8500977_4.POINT">
                    <gml:pos>295964.97 515674.38</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.0086383</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.015706</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.0109942</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8500977" subPointId="1" gml:id="SP.8500977_1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8500977_1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8500977_1.POINT">
                    <gml:pos>295964.97 515746.03</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00799958</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0145447</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.01018129</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8500977" subPointId="2" gml:id="SP.8500977_2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8500977_2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8500977_2.POINT">
                    <gml:pos>296027.0 515781.84</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00771358</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0140247</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00981729</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8493834" subPointId="4" gml:id="SP.8493834_4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8493834_4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8493834_4.POINT">
                    <gml:pos>294848.25 515244.56</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00371976</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00676321</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.004734247</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8493834" subPointId="3" gml:id="SP.8493834_3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8493834_3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8493834_3.POINT">
                    <gml:pos>294972.3 515316.2</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00416193</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00756714</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005296998</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8493834" subPointId="6" gml:id="SP.8493834_6">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8493834_6</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8493834_6.POINT">
                    <gml:pos>294972.3 515244.56</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.004392</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00798546</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005589822</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8472412" subPointId="5" gml:id="SP.8472412_5">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8472412_5</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8472412_5.POINT">
                    <gml:pos>294165.8 513919.25</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00295481</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00537237</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003760659</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8493834" subPointId="5" gml:id="SP.8493834_5">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8493834_5</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8493834_5.POINT">
                    <gml:pos>294910.28 515208.75</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00414126</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00752957</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005270699</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8472412" subPointId="4" gml:id="SP.8472412_4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8472412_4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8472412_4.POINT">
                    <gml:pos>294103.75 513955.06</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00284762</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00517749</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003624243</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8459918" subPointId="1" gml:id="SP.8459918_1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8459918_1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8459918_1.POINT">
                    <gml:pos>294662.12 513274.5</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00556644</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0101208</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00708456</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8484907" subPointId="2" gml:id="SP.8484907_2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8484907_2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8484907_2.POINT">
                    <gml:pos>294351.9 514814.72</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00247844</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00450625</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003154375</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8459918" subPointId="4" gml:id="SP.8459918_4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8459918_4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8459918_4.POINT">
                    <gml:pos>294662.12 513202.88</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00517234</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00940425</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006582975</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8459918" subPointId="5" gml:id="SP.8459918_5">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8459918_5</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8459918_5.POINT">
                    <gml:pos>294724.16 513167.06</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00520229</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00945872</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006621104</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8484907" subPointId="1" gml:id="SP.8484907_1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8484907_1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8484907_1.POINT">
                    <gml:pos>294289.88 514778.9</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00246852</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00448822</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003141754</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8500976" subPointId="4" gml:id="SP.8500976_4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8500976_4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8500976_4.POINT">
                    <gml:pos>295592.72 515674.38</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00629924</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0114532</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00801724</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8500976" subPointId="3" gml:id="SP.8500976_3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8500976_3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8500976_3.POINT">
                    <gml:pos>295716.8 515746.03</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00675425</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0122805</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00859635</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8492049" subPointId="1" gml:id="SP.8492049_1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8492049_1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8492049_1.POINT">
                    <gml:pos>295034.38 515208.75</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.0048994</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.008908</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.0062356</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8500976" subPointId="6" gml:id="SP.8500976_6">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8500976_6</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8500976_6.POINT">
                    <gml:pos>295716.8 515674.38</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00733538</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.013337</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.0093359</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8492049" subPointId="2" gml:id="SP.8492049_2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8492049_2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8492049_2.POINT">
                    <gml:pos>295096.4 515244.56</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00513822</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00934222</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006539554</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8500976" subPointId="5" gml:id="SP.8500976_5">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8500976_5</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8500976_5.POINT">
                    <gml:pos>295654.75 515638.56</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00708226</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0128768</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00901376</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8492049" subPointId="4" gml:id="SP.8492049_4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8492049_4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8492049_4.POINT">
                    <gml:pos>295034.38 515137.1</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00521103</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0094746</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00663222</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8500976" subPointId="2" gml:id="SP.8500976_2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8500976_2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8500976_2.POINT">
                    <gml:pos>295654.75 515781.84</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00604559</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.010992</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.0076944</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8500976" subPointId="1" gml:id="SP.8500976_1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8500976_1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8500976_1.POINT">
                    <gml:pos>295592.72 515746.03</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00583705</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0106128</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00742896</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8458133" subPointId="2" gml:id="SP.8458133_2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8458133_2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8458133_2.POINT">
                    <gml:pos>294538.03 513202.88</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00471444</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00857171</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006000197</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8458133" subPointId="3" gml:id="SP.8458133_3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8458133_3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8458133_3.POINT">
                    <gml:pos>294600.1 513167.06</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00478483</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00869969</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006089783</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8458133" subPointId="6" gml:id="SP.8458133_6">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8458133_6</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8458133_6.POINT">
                    <gml:pos>294600.1 513095.4</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00447757</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00814104</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005698728</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8459917" subPointId="3" gml:id="SP.8459917_3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8459917_3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8459917_3.POINT">
                    <gml:pos>294413.97 513274.5</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00431233</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00784061</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005488427</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8459917" subPointId="2" gml:id="SP.8459917_2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8459917_2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8459917_2.POINT">
                    <gml:pos>294351.9 513310.34</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00400581</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00728329</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005098303</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8463487" subPointId="1" gml:id="SP.8463487_1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8463487_1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8463487_1.POINT">
                    <gml:pos>294289.88 513489.44</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00350304</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00636916</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.004458412</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8461703" subPointId="6" gml:id="SP.8461703_6">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8461703_6</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8461703_6.POINT">
                    <gml:pos>294600.1 513310.34</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00544102</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00989276</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006924932</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8463487" subPointId="5" gml:id="SP.8463487_5">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8463487_5</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8463487_5.POINT">
                    <gml:pos>294351.9 513381.97</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00398494</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00724535</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005071745</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8463487" subPointId="4" gml:id="SP.8463487_4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8463487_4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8463487_4.POINT">
                    <gml:pos>294289.88 513417.78</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00360847</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00656086</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.004592602</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8493836" subPointId="2" gml:id="SP.8493836_2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8493836_2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8493836_2.POINT">
                    <gml:pos>295654.75 515352.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.0103439</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0188071</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.01316497</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8493836" subPointId="1" gml:id="SP.8493836_1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8493836_1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8493836_1.POINT">
                    <gml:pos>295592.72 515316.2</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00988948</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0179809</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.01258663</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8463487" subPointId="6" gml:id="SP.8463487_6">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8463487_6</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8463487_6.POINT">
                    <gml:pos>294413.97 513417.78</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00434528</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00790051</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005530357</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8495620" subPointId="1" gml:id="SP.8495620_1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8495620_1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8495620_1.POINT">
                    <gml:pos>295406.6 515423.66</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00654493</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0118999</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00832993</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8495620" subPointId="2" gml:id="SP.8495620_2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8495620_2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8495620_2.POINT">
                    <gml:pos>295468.66 515459.47</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00684176</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0124396</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00870772</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8486693" subPointId="2" gml:id="SP.8486693_2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8486693_2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8486693_2.POINT">
                    <gml:pos>294538.03 514922.2</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00259108</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00471106</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003297742</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8495619" subPointId="4" gml:id="SP.8495619_4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8495619_4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8495619_4.POINT">
                    <gml:pos>295034.38 515352.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00434413</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00789842</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005528894</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8495619" subPointId="5" gml:id="SP.8495619_5">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8495619_5</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8495619_5.POINT">
                    <gml:pos>295096.4 515316.2</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00481718</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0087585</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00613095</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8495619" subPointId="2" gml:id="SP.8495619_2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8495619_2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8495619_2.POINT">
                    <gml:pos>295096.4 515459.47</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00426778</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0077596</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00543172</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8486693" subPointId="1" gml:id="SP.8486693_1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8486693_1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8486693_1.POINT">
                    <gml:pos>294476.0 514886.38</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00252086</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00458339</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003208373</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8495619" subPointId="3" gml:id="SP.8495619_3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8495619_3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8495619_3.POINT">
                    <gml:pos>295158.44 515423.66</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00471987</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00858157</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006007099</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8495619" subPointId="6" gml:id="SP.8495619_6">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8495619_6</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8495619_6.POINT">
                    <gml:pos>295158.44 515352.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00503195</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00914899</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006404293</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8493835" subPointId="1" gml:id="SP.8493835_1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8493835_1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8493835_1.POINT">
                    <gml:pos>295220.47 515316.2</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00564475</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0102632</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00718424</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8493835" subPointId="3" gml:id="SP.8493835_3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8493835_3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8493835_3.POINT">
                    <gml:pos>295344.56 515316.2</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00675131</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0122751</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00859257</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8493835" subPointId="2" gml:id="SP.8493835_2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8493835_2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8493835_2.POINT">
                    <gml:pos>295282.53 515352.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00594073</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0108013</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00756091</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8495619" subPointId="1" gml:id="SP.8495619_1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8495619_1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8495619_1.POINT">
                    <gml:pos>295034.38 515423.66</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00410039</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00745525</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005218675</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8461703" subPointId="1" gml:id="SP.8461703_1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8461703_1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8461703_1.POINT">
                    <gml:pos>294476.0 513381.97</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00478172</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00869404</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006085828</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8461703" subPointId="4" gml:id="SP.8461703_4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8461703_4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8461703_4.POINT">
                    <gml:pos>294476.0 513310.34</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00471109</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00856563</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005995941</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8461703" subPointId="5" gml:id="SP.8461703_5">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8461703_5</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8461703_5.POINT">
                    <gml:pos>294538.03 513274.5</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00497612</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00904748</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006333236</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8461703" subPointId="2" gml:id="SP.8461703_2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8461703_2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8461703_2.POINT">
                    <gml:pos>294538.03 513417.78</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00525988</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00956342</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006694394</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8461703" subPointId="3" gml:id="SP.8461703_3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8461703_3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8461703_3.POINT">
                    <gml:pos>294600.1 513381.97</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00567895</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0103254</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00722778</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8465273" subPointId="5" gml:id="SP.8465273_5">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8465273_5</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8465273_5.POINT">
                    <gml:pos>294538.03 513489.44</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00524243</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00953168</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006672176</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8488478" subPointId="3" gml:id="SP.8488478_3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8488478_3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8488478_3.POINT">
                    <gml:pos>294786.2 514993.8</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00361603</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00657459</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.004602213</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8465273" subPointId="6" gml:id="SP.8465273_6">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8465273_6</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8465273_6.POINT">
                    <gml:pos>294600.1 513525.25</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.0058184</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0105789</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00740523</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8488478" subPointId="2" gml:id="SP.8488478_2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8488478_2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8488478_2.POINT">
                    <gml:pos>294724.16 515029.62</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00321961</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00585383</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.004097681</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8488478" subPointId="1" gml:id="SP.8488478_1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8488478_1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8488478_1.POINT">
                    <gml:pos>294662.12 514993.8</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00293014</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00532753</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003729271</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8465273" subPointId="3" gml:id="SP.8465273_3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8465273_3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8465273_3.POINT">
                    <gml:pos>294600.1 513596.88</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.0056864</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0103389</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00723723</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8497405" subPointId="3" gml:id="SP.8497405_3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8497405_3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8497405_3.POINT">
                    <gml:pos>295344.56 515531.1</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00540717</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00983121</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006881847</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8497405" subPointId="6" gml:id="SP.8497405_6">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8497405_6</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8497405_6.POINT">
                    <gml:pos>295344.56 515459.47</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00580306</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.010551</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.0073857</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8497405" subPointId="5" gml:id="SP.8497405_5">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8497405_5</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8497405_5.POINT">
                    <gml:pos>295282.53 515423.66</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00553149</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0100573</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00704011</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8497405" subPointId="4" gml:id="SP.8497405_4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8497405_4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8497405_4.POINT">
                    <gml:pos>295220.47 515459.47</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.004937</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00897636</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006283452</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8463488" subPointId="1" gml:id="SP.8463488_1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8463488_1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8463488_1.POINT">
                    <gml:pos>294662.12 513489.44</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00645486</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0117361</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00821527</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8488477" subPointId="6" gml:id="SP.8488477_6">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8488477_6</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8488477_6.POINT">
                    <gml:pos>294413.97 514922.2</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00232931</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00423512</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.002964584</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8463488" subPointId="4" gml:id="SP.8463488_4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8463488_4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8463488_4.POINT">
                    <gml:pos>294662.12 513417.78</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00627199</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0114036</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00798252</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8488477" subPointId="5" gml:id="SP.8488477_5">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8488477_5</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8488477_5.POINT">
                    <gml:pos>294351.9 514886.38</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00230882</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00419785</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.002938495</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8488477" subPointId="4" gml:id="SP.8488477_4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8488477_4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8488477_4.POINT">
                    <gml:pos>294289.88 514922.2</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00215217</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00391303</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.002739121</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8465272" subPointId="3" gml:id="SP.8465272_3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8465272_3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8465272_3.POINT">
                    <gml:pos>294227.84 513596.88</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00303616</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00552029</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003864203</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8488477" subPointId="3" gml:id="SP.8488477_3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8488477_3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8488477_3.POINT">
                    <gml:pos>294413.97 514993.8</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00220798</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00401451</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.002810157</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8488477" subPointId="2" gml:id="SP.8488477_2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8488477_2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8488477_2.POINT">
                    <gml:pos>294351.9 515029.62</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00205724</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00374043</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.002618301</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8465272" subPointId="5" gml:id="SP.8465272_5">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8465272_5</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8465272_5.POINT">
                    <gml:pos>294165.8 513489.44</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00294779</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00535962</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.003751734</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8488477" subPointId="1" gml:id="SP.8488477_1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8488477_1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8488477_1.POINT">
                    <gml:pos>294289.88 514993.8</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00202523</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00368224</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.002577568</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8465272" subPointId="6" gml:id="SP.8465272_6">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8465272_6</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8465272_6.POINT">
                    <gml:pos>294227.84 513525.25</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00314666</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.00572121</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.004004847</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8495621" subPointId="2" gml:id="SP.8495621_2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8495621_2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8495621_2.POINT">
                    <gml:pos>295840.88 515459.47</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.0108685</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.019761</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.0138327</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8465272" subPointId="2" gml:id="SP.8465272_2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8465272_2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8465272_2.POINT">
                    <gml:pos>294165.8 513632.7</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00277524</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0050459</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00353213</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8495621" subPointId="1" gml:id="SP.8495621_1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8495621_1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8495621_1.POINT">
                    <gml:pos>295778.84 515423.66</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.0108981</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0198147</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.01387029</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8495620" subPointId="6" gml:id="SP.8495620_6">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8495620_6</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8495620_6.POINT">
                    <gml:pos>295530.7 515352.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00853242</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0155135</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.01085945</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8495620" subPointId="3" gml:id="SP.8495620_3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8495620_3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8495620_3.POINT">
                    <gml:pos>295530.7 515423.66</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00777096</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.014129</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.0098903</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SubPoint receptorPointId="8495620" subPointId="4" gml:id="SP.8495620_4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>SP.8495620_4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="SP.8495620_4.POINT">
                    <gml:pos>295406.6 515352.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00710253</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0129137</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.00903959</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:level>1</imaer:level>
        </imaer:SubPoint>
    </imaer:featureMember>
</imaer:FeatureCollectionCalculator>
