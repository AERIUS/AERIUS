<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<imaer:FeatureCollectionCalculator xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:imaer="http://imaer.aerius.nl/5.1" xmlns:gml="http://www.opengis.net/gml/3.2" gml:id="NL.IMAER.Collection" xsi:schemaLocation="http://imaer.aerius.nl/5.1 https://imaer.aerius.nl/5.1/IMAER.xsd">
    <imaer:metadata>
        <imaer:AeriusCalculatorMetadata>
            <imaer:project>
                <imaer:ProjectMetadata>
                    <imaer:year>2022</imaer:year>
                </imaer:ProjectMetadata>
            </imaer:project>
            <imaer:situation>
                <imaer:SituationMetadata>
                    <imaer:name>Scenario 1</imaer:name>
                    <imaer:reference>RPHZtTdEKiac</imaer:reference>
                    <imaer:situationType>PROPOSED</imaer:situationType>
                    <imaer:nettingFactor>0.3</imaer:nettingFactor>
                </imaer:SituationMetadata>
            </imaer:situation>
            <imaer:version>
                <imaer:VersionMetadata>
                    <imaer:aeriusVersion>3.0-SNAPSHOT_20221107_9d2d1b0a85</imaer:aeriusVersion>
                    <imaer:databaseVersion>latest_9d2d1b0a85</imaer:databaseVersion>
                </imaer:VersionMetadata>
            </imaer:version>
        </imaer:AeriusCalculatorMetadata>
    </imaer:metadata>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Kirk Barn</imaer:label>
            <imaer:height>0.002</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.1.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>249815.1467350439 667641.2068523028 249880.08195778035 667638.6094433933 249890.4715934182 667612.6353542986 249822.93896177228 667607.4405364798 249815.1467350439 667641.2068523028</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Kirk circular barn</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.2.POINT">
                            <gml:pos>249766.3743579373 667588.3716830035</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:BuildingGeometry>
            </imaer:geometry>
            <imaer:diameter>40.0</imaer:diameter>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Moidart Barn</imaer:label>
            <imaer:height>0.002</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.3.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>249279.20447635726 668041.2291983028 249123.96066955436 668379.112777815 248443.62751621223 668712.4303630095 248192.49782873696 668990.9560163912 248713.02118095846 669653.0251924624 249105.69669228344 669520.6113572482 249091.99870933025 669073.1439141104 249498.37220360842 668867.6741698124 249279.20447635726 668041.2291983028</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Moidart Circular Barn</imaer:label>
            <imaer:height>2.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.4.POINT">
                            <gml:pos>249943.88776382696 667702.8137383659</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:BuildingGeometry>
            </imaer:geometry>
            <imaer:diameter>35.199</imaer:diameter>
        </imaer:Building>
    </imaer:featureMember>
</imaer:FeatureCollectionCalculator>
