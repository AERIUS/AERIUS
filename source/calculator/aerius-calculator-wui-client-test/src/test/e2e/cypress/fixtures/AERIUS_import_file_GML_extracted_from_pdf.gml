<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<imaer:FeatureCollectionCalculator
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:xlink="http://www.w3.org/1999/xlink"
	xmlns:imaer="http://imaer.aerius.nl/6.0"
	xmlns:gml="http://www.opengis.net/gml/3.2" gml:id="NL.IMAER.Collection" xsi:schemaLocation="http://imaer.aerius.nl/6.0 https://imaer.aerius.nl/6.0/IMAER.xsd">
	<imaer:metadata>
		<imaer:AeriusCalculatorMetadata>
			<imaer:project>
				<imaer:ProjectMetadata>
					<imaer:year>2024</imaer:year>
					<imaer:name>x</imaer:name>
					<imaer:corporation>x</imaer:corporation>
					<imaer:facilityLocation>
						<imaer:Address>
							<imaer:streetAddress>x</imaer:streetAddress>
							<imaer:postcode>x</imaer:postcode>
							<imaer:city>x</imaer:city>
						</imaer:Address>
					</imaer:facilityLocation>
					<imaer:description>xx</imaer:description>
				</imaer:ProjectMetadata>
			</imaer:project>
			<imaer:situation>
				<imaer:SituationMetadata>
					<imaer:name>Situatie 1</imaer:name>
					<imaer:reference>Ry3BQFjhWkSx</imaer:reference>
					<imaer:situationType>PROPOSED</imaer:situationType>
				</imaer:SituationMetadata>
			</imaer:situation>
			<imaer:version>
				<imaer:VersionMetadata>
					<imaer:aeriusVersion>pr-6680_2415e894_20240724_2415e89467</imaer:aeriusVersion>
					<imaer:databaseVersion>pr-6680_2415e894_2415e89467_calculator_nl_latest</imaer:databaseVersion>
				</imaer:VersionMetadata>
			</imaer:version>
		</imaer:AeriusCalculatorMetadata>
	</imaer:metadata>
	<imaer:featureMember>
		<imaer:EmissionSource sectorId="2100" gml:id="ES.1">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.1</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>Bron 1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.22</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>40.0</imaer:emissionHeight>
					<imaer:timeVaryingProfile>
						<imaer:StandardTimeVaryingProfile>
							<imaer:standardType>INDUSTRIAL_ACTIVITY</imaer:standardType>
						</imaer:StandardTimeVaryingProfile>
					</imaer:timeVaryingProfile>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.1.POINT">
							<gml:pos>252238.63210627515 462510.25372689054</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>50.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>50.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
		</imaer:EmissionSource>
	</imaer:featureMember>
</imaer:FeatureCollectionCalculator>