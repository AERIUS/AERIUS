﻿<?xml version="1.0" encoding="UTF-8"?>
<imaer:FeatureCollectionCalculator xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:imaer="http://imaer.aerius.nl/5.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" gml:id="NL.IMAER.Collection" xsi:schemaLocation="http://imaer.aerius.nl/5.1 https://imaer.aerius.nl/5.1/IMAER.xsd">
	<imaer:metadata>
		<imaer:AeriusCalculatorMetadata>
			<imaer:project>
				<imaer:ProjectMetadata>
					<imaer:year>2024</imaer:year>
				</imaer:ProjectMetadata>
			</imaer:project>
			<imaer:situation>
				<imaer:SituationMetadata>
					<imaer:name>Situatie 1</imaer:name>
					<imaer:reference>RrBtDdaVVjAo</imaer:reference>
					<imaer:situationType>PROPOSED</imaer:situationType>
				</imaer:SituationMetadata>
			</imaer:situation>
			<imaer:version>
				<imaer:VersionMetadata>
					<imaer:aeriusVersion>2023.2.1_20240702_c9370194cb</imaer:aeriusVersion>
					<imaer:databaseVersion>2023.2.1_c9370194cb_calculator_nl_stable</imaer:databaseVersion>
				</imaer:VersionMetadata>
			</imaer:version>
		</imaer:AeriusCalculatorMetadata>
	</imaer:metadata>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.1">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.1</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.1.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>570</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB93.06.009</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.2">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.2</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.2.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>1020</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.28</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.3">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.3</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.3.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>1020</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB93.03.003</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.4">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.4</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.4.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>920</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB94.02.015</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.5">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.5</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.5.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>1180</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.24</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.6">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.6</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.6.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>1100</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2009.11</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.7">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.7</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.7</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.7.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>1100</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.7">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2009.22</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.8">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.8</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.8</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.8.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>1180</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.8">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.14</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.9">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.9</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.9</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.9.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>600</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.9">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.30</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.10">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.10</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.10</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.10.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>700</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.10">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.31</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.11">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.11</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.11</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.11.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>1180</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.11">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.32</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.12">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.12</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.12</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.12.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>1220</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.12">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.33</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.13">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.13</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.13</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.13.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>700</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.13">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.34</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.14">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.14</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.14</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.14.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>700</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.14">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.35</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.15">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.15</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.15</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.15.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>1030</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.15">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.36</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.16">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.16</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.16</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.16.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>1170</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.16">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2012.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.17">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.17</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.17</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.17.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>510</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.17">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2012.02</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.18">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.18</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.18</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.18.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>800</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.18">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2012.04</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.19">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.19</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.19</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.19.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>1100</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.19">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2012.05</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.20">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.20</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.20</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.20.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>1010</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.20">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2012.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.21">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.21</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.21</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.21.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>700</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.21">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2013.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.22">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.22</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.22</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.22.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>1100</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.22">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2013.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.23">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.23</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.23</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.23.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>600</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.23">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2013.04</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.24">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.24</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.24</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.24.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>700</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.24">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2013.05</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.25">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.25</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.25</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.25.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>1030</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.25">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2013.06</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.26">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.26</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.26</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.26.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>800</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.26">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2013.07</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.27">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.27</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.27</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.27.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>800</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.27">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2014.02</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.28">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.28</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.28</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.28.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>600</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.28">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2015.05</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.29">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.29</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.29</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.29.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>990</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.29">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2015.06</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.30">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.30</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.30</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.30.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>800</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.30">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2017.06</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.31">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.31</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.31</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.31.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>810</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.31">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2018.02</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.32">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.32</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.32</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.32.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>910</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.32">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2018.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.33">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.33</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.33</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.33.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>710</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.33">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2018.06</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.34">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.34</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.34</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.34.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>900</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.34">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2018.07</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.35">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.35</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.35</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.35.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>830</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.35">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2019.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.36">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.36</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.36</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.36.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>840</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.36">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2021.05</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.37">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.37</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.37</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.37.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>640</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.37">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2021.06</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.38">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.38</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.38</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.38.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>890</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.38">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2021.07</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.39">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.39</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.39</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.39.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>300</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.39">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2021.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.40">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.40</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.40</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.40.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>620</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.40">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2022.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.41">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.41</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A1.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.41.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>1300</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A1.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.42">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.42</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A2.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.42.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>410</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A2.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.43">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.43</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A3.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.43.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>440</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A3.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.44">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.44</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A4.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.44.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>35</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A4.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2013.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.45">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.45</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A4.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.45.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>110</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A4.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2008.12</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.46">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.46</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A4.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.46.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>110</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A4.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2008.06</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.47">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.47</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A4.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.47.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>18</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A4.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2008.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.48">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.48</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A4.5.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.48.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>53</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A4.5.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2006.14</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.49">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.49</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A4.5.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.49.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>110</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A4.5.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2006.15</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.50">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.50</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A4.5.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.50.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>53</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A4.5.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2007.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.51">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.51</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A4.5.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.51.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>53</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A4.5.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2007.02</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.52">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.52</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A4.5.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.52.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>53</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A4.5.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.07</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.53">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.53</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A4.5.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.53.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>35</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A4.5.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.54">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.54</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A4.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.54.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>53</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A4.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2012.07</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.55">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.55</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A4.7</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.55.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>250</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A4.7">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2012.09</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.56">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.56</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A4.8</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.56.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>190</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A4.8">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2018.04</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.57">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.57</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A4.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.57.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>350</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A4.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.58">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.58</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A6.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.58.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>530</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A6.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.59">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.59</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>A7.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.59.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>620</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="A7.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.60">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.60</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>B1.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.60.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>70</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="B1.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.61">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.61</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C1.1.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.61.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>64</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C1.1.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.12</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.62">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.62</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C1.1.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.62.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>64</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C1.1.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2005.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.63">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.63</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C1.1.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.63.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>19</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C1.1.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.26</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.64">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.64</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C1.1.4.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.64.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>37</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C1.1.4.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2006.14</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.65">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.65</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C1.1.4.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.65.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>64</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C1.1.4.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2006.15</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.66">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.66</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C1.1.4.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.66.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>37</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C1.1.4.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2007.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.67">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.67</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C1.1.4.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.67.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>37</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C1.1.4.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2007.02</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.68">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.68</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C1.1.4.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.68.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>37</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C1.1.4.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.07</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.69">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.69</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C1.1.4.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.69.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>28</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C1.1.4.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.70">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.70</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C1.1.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.70.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>37</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C1.1.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2012.07</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.71">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.71</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C1.1.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.71.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>28</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C1.1.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2013.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.72">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.72</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C1.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.72.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>190</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C1.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.73">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.73</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C2.1.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.73.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>27</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C2.1.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2009.21</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.74">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.74</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C2.1.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.74.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>27</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C2.1.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2009.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.75">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.75</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C2.1.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.75.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>8</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C2.1.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2008.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.76">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.76</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C2.1.4.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.76.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>15</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C2.1.4.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2006.14</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.77">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.77</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C2.1.4.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.77.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>27</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C2.1.4.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2006.15</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.78">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.78</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C2.1.4.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.78.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>15</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C2.1.4.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2007.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.79">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.79</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C2.1.4.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.79.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>15</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C2.1.4.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.02</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.80">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.80</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C2.1.4.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.80.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>15</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C2.1.4.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.07</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.81">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.81</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C2.1.4.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.81.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>12</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C2.1.4.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.82">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.82</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C2.1.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.82.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>15</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C2.1.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2012.07</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.83">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.83</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C2.1.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.83.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>12</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C2.1.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2013.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.84">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.84</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C2.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.84.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>80</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C2.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.85">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.85</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C3.1.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.85.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>7</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C3.1.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2008.05</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.86">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.86</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C3.1.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.86.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>7</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C3.1.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2005.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.87">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.87</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C3.1.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.87.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>2</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C3.1.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2007.05</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.88">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.88</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C3.1.4.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.88.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>4</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C3.1.4.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2006.14</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.89">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.89</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C3.1.4.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.89.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>7</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C3.1.4.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2006.15</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.90">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.90</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C3.1.4.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.90.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>4</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C3.1.4.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2007.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.91">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.91</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C3.1.4.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.91.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>4</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C3.1.4.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2007.02</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.92">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.92</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C3.1.4.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.92.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>4</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C3.1.4.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.07</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.93">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.93</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C3.1.4.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.93.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>3</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C3.1.4.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.94">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.94</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C3.1.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.94.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>4</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C3.1.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2012.07</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.95">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.95</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C3.1.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.95.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>3</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C3.1.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2013.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.96">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.96</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>C3.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.96.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>20</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="C3.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.97">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.97</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.97.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>20</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB93.03.001</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.98">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.98</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.98.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>24</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB94.06.021/A97.01.049</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.99">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.99</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.99.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>15</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2006.07</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.100">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.100</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.4.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.100.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>26</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.4.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB96.03.033</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.101">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.101</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.4.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.101.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>33</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.4.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.14</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.102">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.102</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.102.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>39</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.16</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.103">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.103</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.103.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>18</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB96.04.038</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.104">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.104</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.7</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.104.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>25</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.7">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB96.04.038</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.105">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.105</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.8</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.105.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>23</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.8">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB96.06.040</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.106">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.106</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.9</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.106.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>21</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.9">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2008.04</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.107">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.107</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.10</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.107.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>21</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.10">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2006.05</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.108">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.108</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.11</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.108.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>17</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.11">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.12</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.109">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.109</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.12.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.109.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>17</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.12.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.13</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.110">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.110</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.12.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.110.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>21</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.12.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2004.06</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.111">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.111</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.12.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.111.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>18</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.12.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB99.06.072/A99.11.082</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.112">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.112</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.13</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.112.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>20</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.13">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.05</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.113">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.113</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.14</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.113.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>3</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.14">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.26</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.114">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.114</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.15.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.114.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>10</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.15.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2006.14</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.115">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.115</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.15.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.115.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>21</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.15.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2006.15</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.116">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.116</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.15.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.116.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>10</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.15.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2007.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.117">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.117</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.15.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.117.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>10</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.15.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2007.02</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.118">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.118</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.15.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.118.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>10</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.15.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.07</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.119">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.119</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.15.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.119.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>7</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.15.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.120">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.120</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.16</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.120.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>10</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.16">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2012.07</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.121">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.121</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.17</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.121.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>7</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.17">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2013.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.122">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.122</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.18</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.122.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>21</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.18">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2019.02</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.123">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.123</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.19</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.123.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>21</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.19">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2020.06</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.124">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.124</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.1.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.124.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>69</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.1.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.125">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.125</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.2.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.125.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>330</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.2.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB93.11.012/A99.11.077</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.126">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.126</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.2.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.126.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>370</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.2.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB94.02.014</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.127">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.127</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.2.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.127.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>400</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.2.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB94.04.018</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.128">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.128</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.2.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.128.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>310</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.2.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB94.06.019</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.129">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.129</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.2.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.129.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>320</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.2.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.06</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.130">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.130</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.2.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.130.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>400</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.2.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB95.12.032</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.131">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.131</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.2.7</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.131.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>500</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.2.7">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.17</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.132">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.132</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.2.8</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.132.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>310</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.2.8">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB96.04.037</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.133">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.133</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.2.9</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.133.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>250</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.2.9">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.18</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.134">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.134</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.2.10</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.134.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>250</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.2.10">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2008.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.135">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.135</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.2.11</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.135.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>250</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.2.11">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2014.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.136">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.136</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.2.12</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.136.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>240</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.2.12">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.15</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.137">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.137</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.2.13</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.137.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>290</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.2.13">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2006.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.138">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.138</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.2.14</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.138.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>290</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.2.14">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.07</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.139">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.139</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.2.15</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.139.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>42</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.2.15">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2007.05</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.140">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.140</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.2.16</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.140.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>290</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.2.16">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2004.07</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.141">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.141</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.2.17.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.141.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>130</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.2.17.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2006.14</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.142">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.142</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.2.17.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.142.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>250</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.2.17.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2006.15</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.143">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.143</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.2.17.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.143.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>130</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.2.17.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2007.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.144">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.144</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.2.17.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.144.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>130</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.2.17.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.02</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.145">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.145</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.2.17.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.145.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>130</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.2.17.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.07</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.146">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.146</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.2.17.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.146.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>83</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.2.17.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.147">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.147</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.2.18</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.147.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>130</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.2.18">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2012.07</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.148">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.148</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.2.19</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.148.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>83</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.2.19">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2013.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.149">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.149</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.2.20</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.149.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>130</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.2.20">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2018.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.150">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.150</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.2.21</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.150.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>250</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.2.21">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2020.06</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.151">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.151</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.2.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.151.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>830</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.2.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.152">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.152</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.3.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.152.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>240</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.3.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB95.02.027</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.153">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.153</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.3.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.153.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>180</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.3.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB95.06.028</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.154">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.154</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.3.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.154.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>250</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.3.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>bijindividuelehuisvesting:BB95.10.030</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.155">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.155</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.3.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.155.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>180</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.3.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>bijgroepshuisvesting:BB96.04.036/A98.10.061</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.156">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.156</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.3.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.156.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>220</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.3.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.19</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.157">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.157</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.3.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.157.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>130</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.3.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2008.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.158">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.158</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.3.7</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.158.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>130</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.3.7">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2014.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.159">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.159</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.3.8.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.159.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>220</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.3.8.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.16</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.160">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.160</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.3.8.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.160.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>220</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.3.8.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.17</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.161">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.161</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.3.9.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.161.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>230</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.3.9.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.162">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.162</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.3.9.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.162.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>250</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.3.9.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2006.09</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.163">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.163</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.3.10</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.163.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>260</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.3.10">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.09</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.164">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.164</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.3.11</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.164.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>21</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.3.11">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2008.09</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.165">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.165</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.3.12.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.165.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>63</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.3.12.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2006.14</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.166">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.166</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.3.12.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.166.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>130</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.3.12.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2006.15</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.167">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.167</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.3.12.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.167.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>63</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.3.12.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2007.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.168">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.168</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.3.12.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.168.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>63</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.3.12.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.02</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.169">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.169</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.3.12.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.169.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>63</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.3.12.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.07</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.170">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.170</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.3.12.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.170.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>42</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.3.12.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.171">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.171</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.3.13</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.171.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>63</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.3.13">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2012.07</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.172">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.172</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.3.14</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.172.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>42</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.3.14">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2013.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.173">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.173</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.3.15</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.173.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>220</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.3.15">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2008.11</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.174">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.174</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.3.16</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.174.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>150</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.3.16">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2019.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.175">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.175</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.3.17</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.175.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>130</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.3.17">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2020.06</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.176">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.176</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.3.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.176.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>420</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.3.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.177">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.177</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D1.3.101</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.177.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>420</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D1.3.101">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.178">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.178</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D2.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.178.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>170</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D2.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2015.04</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.179">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.179</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D2.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.179.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>170</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D2.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.14</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.180">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.180</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D2.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.180.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>28</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D2.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.26</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.181">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.181</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D2.4.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.181.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>83</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D2.4.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2006.14</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.182">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.182</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D2.4.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.182.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>170</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D2.4.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2006.15</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.183">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.183</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D2.4.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.183.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>83</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D2.4.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2007.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.184">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.184</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D2.4.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.184.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>83</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D2.4.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2009.12</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.185">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.185</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D2.4.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.185.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>83</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D2.4.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.07</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.186">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.186</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D2.4.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.186.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>55</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D2.4.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.187">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.187</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D2.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.187.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>83</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D2.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2012.07</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.188">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.188</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D2.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.188.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>55</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D2.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2013.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.189">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.189</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D2.7</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.189.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>170</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D2.7">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2020.06</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.190">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.190</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D2.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.190.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>550</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D2.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.191">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.191</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.191.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>450</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.21</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.192">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.192</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.192.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>450</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.23</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.193">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.193</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.193.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>160</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB93.11.011</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.194">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.194</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.3.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.194.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>170</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.3.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.25</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.195">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.195</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.3.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.195.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>140</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.3.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2019.05</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.196">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.196</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.196.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>100</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB95.02.025</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.197">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.197</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.197.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>130</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB95.10.029</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.198">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.198</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.6.1.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.198.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>150</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.6.1.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.19</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.199">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.199</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.6.1.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.199.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>120</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.6.1.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2004.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.200">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.200</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.6.2.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.200.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>160</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.6.2.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.20</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.201">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.201</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.6.2.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.201.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>240</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.6.2.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.202">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.202</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.7.1.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.202.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>100</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.7.1.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB97.07.056/A97.11.059</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.203">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.203</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.7.1.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.203.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>140</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.7.1.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2004.04</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.204">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.204</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.7.2.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.204.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>150</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.7.2.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2004.05</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.205">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.205</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.7.2.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.205.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>190</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.7.2.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.10</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.206">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.206</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.8</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.206.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>90</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.8">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2008.05</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.207">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.207</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.9</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.207.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>90</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.9">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2004.02</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.208">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.208</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.10.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.208.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>140</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.10.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.27</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.209">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.209</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.10.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.209.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>200</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.10.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.27</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.210">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.210</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.11</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.210.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>170</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.11">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.211">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.211</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.12</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.211.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>120</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.12">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB98.10.064</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.212">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.212</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.13</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.212.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>170</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.13">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB98.10.065</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.213">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.213</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.14</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.213.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>15</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.14">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2008.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.214">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.214</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.15.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.214.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>45</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.15.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2006.14</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.215">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.215</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.15.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.215.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>90</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.15.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2006.15</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.216">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.216</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.15.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.216.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>45</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.15.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2007.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.217">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.217</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.15.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.217.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>45</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.15.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.02</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.218">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.218</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.15.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.218.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>45</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.15.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.07</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.219">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.219</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.15.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.219.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>30</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.15.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.220">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.220</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.16</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.220.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>110</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.16">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2008.11</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.221">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.221</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.17</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.221.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>45</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.17">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2012.07</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.222">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.222</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.18</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.222.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>30</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.18">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2013.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.223">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.223</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.19</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.223.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>77</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.19">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2019.04</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.224">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.224</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.2.20</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.224.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>90</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.2.20">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2020.06</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.225">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.225</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.3.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.225.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>190</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.3.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.30</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.226">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.226</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.3.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.226.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>300</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.3.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.227">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.227</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>D3.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.227.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>300</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="D3.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.228">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.228</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E1.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.228.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>5</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E1.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.04</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.229">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.229</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E1.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.229.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>2</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E1.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB93.06.007</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.230">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.230</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E1.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.230.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>1</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E1.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB95.06.026</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.231">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.231</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E1.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.231.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>21</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E1.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.05</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.232">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.232</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E1.5.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.232.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>2</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E1.5.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB93.06.008</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.233">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.233</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E1.5.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.233.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>1</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E1.5.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB97.07.058</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.234">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.234</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E1.5.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.234.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E1.5.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.31</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.235">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.235</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E1.5.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.235.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E1.5.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.32</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.236">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.236</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E1.5.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.236.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>2</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E1.5.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2009.10</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.237">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.237</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E1.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.237.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>1</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E1.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB99.06.071</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.238">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.238</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E1.7</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.238.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>17</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E1.7">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.06</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.239">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.239</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E1.8.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.239.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>5</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E1.8.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2005.02</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.240">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.240</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E1.8.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.240.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>3</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E1.8.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2005.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.241">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.241</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E1.8.3.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.241.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>3</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E1.8.3.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2006.10</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.242">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.242</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E1.8.3.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.242.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>2</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E1.8.3.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2006.10</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.243">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.243</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E1.8.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.243.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>1</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E1.8.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2006.11</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.244">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.244</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E1.8.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.244.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>2</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E1.8.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2006.12</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.245">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.245</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E1.9</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.245.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>2</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E1.9">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2007.05</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.246">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.246</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E1.10</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.246.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>5</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E1.10">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.28</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.247">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.247</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E1.11</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.247.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>9</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E1.11">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2009.14</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.248">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.248</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E1.12</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.248.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>5</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E1.12">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.249">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.249</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E1.13</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.249.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>5</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E1.13">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2014.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.250">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.250</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E1.14</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.250.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>11</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E1.14">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2015.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.251">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.251</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E1.15</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.251.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>5</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E1.15">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2021.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.252">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.252</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E1.16</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.252.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>9</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E1.16">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.13</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.253">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.253</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E1.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.253.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>17</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E1.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.254">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.254</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E1.101</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.254.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>5</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E1.101">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.255">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.255</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.255.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>10</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.07</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.256">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.256</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.256.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>4</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB93.06.007</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.257">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.257</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.257.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>2</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB95.06.026</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.258">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.258</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.258.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>46</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.259">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.259</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.5.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.259.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>4</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.5.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB93.06.008</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.260">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.260</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.5.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.260.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>1</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.5.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB97.07.058</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.261">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.261</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.5.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.261.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.5.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.31</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.262">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.262</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.5.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.262.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.5.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.32</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.263">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.263</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.5.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.263.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>3</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.5.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2005.11</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.264">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.264</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.5.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.264.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>3</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.5.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2009.10</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.265">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.265</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.265.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>2</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB99.06.071</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.266">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.266</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.7</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.266.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>40</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.7">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.09</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.267">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.267</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.8</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.267.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>11</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.8">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.21</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.268">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.268</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.9.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.268.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>13</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.9.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.10</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.269">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.269</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.9.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.269.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>15</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.9.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.09</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.270">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.270</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.9.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.270.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>15</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.9.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.10</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.271">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.271</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.10</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.271.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>3</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.10">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2008.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.272">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.272</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.11.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.272.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>9</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.11.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2004.09</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.273">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.273</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.11.2.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.273.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>6</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.11.2.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2004.10</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.274">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.274</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.11.2.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.274.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>4</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.11.2.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2004.10</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.275">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.275</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.11.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.275.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>3</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.11.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2005.04</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.276">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.276</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.11.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.276.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>4</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.11.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2005.05</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.277">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.277</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.12.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.277.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>7</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.12.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2004.11</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.278">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.278</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.12.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.278.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>11</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.12.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2004.12</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.279">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.279</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.13</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.279.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>10</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.13">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2015.04</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.280">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.280</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.14</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.280.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>10</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.14">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.281">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.281</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.15</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.281.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>10</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.15">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2005.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.282">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.282</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.16</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.282.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>10</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.16">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2021.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.283">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.283</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.283.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>32</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.284">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.284</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E2.101</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.284.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>10</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E2.101">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.285">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.285</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E3.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.285.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>3</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E3.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2013.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.286">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.286</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E3.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.286.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>8</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E3.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.27</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.287">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.287</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E3.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.287.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>11</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E3.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2005.10</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.288">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.288</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E3.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.288.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>13</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E3.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2009.14</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.289">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.289</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E3.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.289.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>8</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E3.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.290">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.290</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E3.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.290.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>8</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E3.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2008.06</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.291">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.291</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E3.7</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.291.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>13</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E3.7">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.13</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.292">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.292</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E3.8</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.292.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>8</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E3.8">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.13</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.293">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.293</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E3.9</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.293.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>3</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E3.9">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2017.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.294">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.294</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E3.10</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.294.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>8</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E3.10">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2021.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.295">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.295</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E3.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.295.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>25</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E3.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.296">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.296</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E4.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.296.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>8</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E4.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB95.12.039</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.297">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.297</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E4.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.297.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>17</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E4.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.22</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.298">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.298</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E4.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.298.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>13</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E4.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.23</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.299">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.299</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E4.4.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.299.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>25</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E4.4.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2004.13</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.300">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.300</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E4.4.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.300.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>44</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E4.4.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2004.14</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.301">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.301</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E4.4.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.301.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>44</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E4.4.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.302">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.302</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E4.4.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.302.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>44</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E4.4.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.37</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.303">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.303</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E4.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.303.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>23</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E4.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB98.10.066</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.304">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.304</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E4.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.304.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>6</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E4.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2007.05</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.305">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.305</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E4.7</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.305.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>17</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E4.7">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2007.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.306">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.306</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E4.8</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.306.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>25</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E4.8">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2007.10</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.307">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.307</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E4.9</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.307.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>17</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E4.9">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.308">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.308</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E4.10</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.308.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>17</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E4.10">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2014.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.309">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.309</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E4.11</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.309.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>17</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E4.11">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2021.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.310">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.310</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E4.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.310.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>58</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E4.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.311">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.311</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.311.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB93.03.002/C96.10.048</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.312">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.312</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.312.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>1</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB94.04.016/A96.10.047</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.313">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.313</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.313.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB97.07.057</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.314">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.314</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.314.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>1</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2007.05</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.315">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.315</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.315.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>4</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.11</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.316">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.316</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.316.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>3</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2005.10</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.317">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.317</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.7</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.317.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>2</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.7">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.11</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.318">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.318</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.8</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.318.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>2</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.8">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2006.13</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.319">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.319</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.9.1.1.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.319.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>3</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.9.1.1.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2009.02</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.320">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.320</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.9.1.1.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.320.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>3</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.9.1.1.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2009.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.321">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.321</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.9.1.1.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.321.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>2</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.9.1.1.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2009.04</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.322">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.322</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.9.1.1.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.322.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>3</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.9.1.1.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2009.15</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.323">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.323</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.9.1.1.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.323.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>2</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.9.1.1.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2017.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.324">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.324</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.9.1.1.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.324.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>2</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.9.1.1.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2017.09</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.325">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.325</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.9.1.1.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.325.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>6</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.9.1.1.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2009.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.326">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.326</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.9.1.2.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.326.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>3</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.9.1.2.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2009.05</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.327">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.327</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.9.1.2.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.327.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>3</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.9.1.2.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2009.06</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.328">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.328</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.9.1.2.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.328.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>1</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.9.1.2.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2009.07</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.329">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.329</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.9.1.2.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.329.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>3</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.9.1.2.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2009.16</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.330">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.330</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.9.1.2.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.330.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>2</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.9.1.2.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2017.10</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.331">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.331</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.9.1.2.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.331.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>2</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.9.1.2.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2017.11</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.332">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.332</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.9.1.2.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.332.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>5</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.9.1.2.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2009.09</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.333">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.333</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.10</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.333.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>4</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.10">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2009.14</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.334">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.334</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.11</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.334.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>2</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.11">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.13</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.335">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.335</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.12</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.335.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>2</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.12">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.336">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.336</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.13</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.336.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>2</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.13">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2014.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.337">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.337</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.14</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.337.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>4</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.14">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.13</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.338">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.338</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.15</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.338.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>2</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.15">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2017.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.339">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.339</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.16</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.339.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>2</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.16">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2021.04</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.340">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.340</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>E5.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.340.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>7</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="E5.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.341">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.341</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F1.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.341.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>2</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F1.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2007.05</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.342">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.342</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F1.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.342.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>5</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F1.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2013.02</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.343">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.343</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F1.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.343.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>8</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F1.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2009.14</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.344">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.344</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F1.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.344.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>5</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F1.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.345">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.345</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F1.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.345.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>5</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F1.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2014.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.346">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.346</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F1.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.346.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>8</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F1.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.13</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.347">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.347</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F1.7</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.347.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>5</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F1.7">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.13</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.348">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.348</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F1.8</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.348.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>5</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F1.8">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2017.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.349">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.349</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F1.9</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.349.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>5</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F1.9">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2021.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.350">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.350</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F1.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.350.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>15</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F1.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.351">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.351</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F2.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.351.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>5</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F2.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2007.05</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.352">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.352</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F2.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.352.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>14</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F2.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.28</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.353">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.353</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F2.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.353.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>24</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F2.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2009.14</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.354">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.354</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F2.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.354.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>14</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F2.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.355">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.355</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F2.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.355.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>14</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F2.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2008.06</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.356">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.356</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F2.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.356.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>24</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F2.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.13</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.357">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.357</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F2.7</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.357.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>15</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F2.7">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.13</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.358">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.358</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F2.8</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.358.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>14</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F2.8">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2021.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.359">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.359</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F2.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.359.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>47</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F2.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.360">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.360</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F3.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.360.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>6</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F3.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2008.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.361">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.361</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F3.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.361.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>18</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F3.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.28</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.362">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.362</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F3.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.362.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>18</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F3.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.363">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.363</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F3.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.363.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>18</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F3.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2005.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.364">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.364</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F3.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.364.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>18</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F3.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2021.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.365">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.365</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F3.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.365.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>59</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F3.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.366">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.366</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F4.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.366.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>36</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F4.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2001.12</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.367">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.367</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F4.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.367.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>7</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F4.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2008.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.368">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.368</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F4.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.368.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>26</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F4.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2005.07</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.369">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.369</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F4.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.369.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>20</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F4.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2015.04</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.370">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.370</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F4.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.370.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>35</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F4.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2009.14</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.371">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.371</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F4.6</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.371.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>20</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F4.6">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.372">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.372</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F4.7</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.372.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>20</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F4.7">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2005.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.373">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.373</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F4.8</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.373.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>35</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F4.8">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.13</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.374">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.374</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F4.9</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.374.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>21</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F4.9">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.13</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.375">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.375</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F4.10</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.375.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>20</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F4.10">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2021.04</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.376">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.376</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>F4.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.376.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>68</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="F4.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.377">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.377</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>G1.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.377.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>3</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="G1.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2007.05</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.378">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.378</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>G1.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.378.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>10</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="G1.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2010.28</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.379">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.379</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>G1.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.379.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>10</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="G1.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2008.06</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.380">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.380</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>G1.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.380.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>10</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="G1.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.381">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.381</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>G1.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.381.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>10</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="G1.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2021.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.382">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.382</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>G1.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.382.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>32</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="G1.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.383">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.383</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>G2.1.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.383.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>2</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="G2.1.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2008.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.384">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.384</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>G2.1.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.384.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>6</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="G2.1.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.11</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.385">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.385</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>G2.1.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.385.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>6</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="G2.1.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2005.01</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.386">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.386</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>G2.1.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.386.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>6</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="G2.1.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2011.03</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.387">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.387</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>G2.1.5</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.387.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>6</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="G2.1.5">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2021.04</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.388">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.388</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>G2.1.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.388.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>21</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="G2.1.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.389">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.389</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>G2.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.389.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>2</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="G2.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.390">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.390</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>H1.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.390.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>58</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="H1.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.391">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.391</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>H1.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.391.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>25</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="H1.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BB94.02.013</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.392">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.392</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>I1.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.392.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>77</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="I1.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2005.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.393">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.393</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>I1.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.393.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>36</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="I1.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2013.02</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.394">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.394</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>I1.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.394.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>36</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="I1.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2008.06</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.395">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.395</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>I1.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.395.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>12</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="I1.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2008.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.396">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.396</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>I1.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.396.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>120</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="I1.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.397">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.397</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>I2.1</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.397.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>12</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="I2.1">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2005.09</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.398">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.398</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>I2.2</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.398.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>6</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="I2.2">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2013.02</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.399">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.399</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>I2.3</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.399.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>6</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="I2.3">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2008.06</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.400">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.400</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>I2.4</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.400.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>2</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="I2.4">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>BWL2013.08</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.401">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.401</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>I2.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.401.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>20</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="I2.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.402">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.402</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>K1.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.402.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>500</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="K1.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.403">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.403</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>K2.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.403.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>210</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="K2.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.404">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.404</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>K3.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.404.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>310</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="K3.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.405">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.405</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>K4.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.405.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>130</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="K4.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.406">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.406</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>L1.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.406.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>250</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="L1.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.407">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.407</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>L2.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.407.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>30</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="L2.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
	<imaer:featureMember>
		<imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.408">
			<imaer:identifier>
				<imaer:NEN3610ID>
					<imaer:namespace>NL.IMAER</imaer:namespace>
					<imaer:localId>ES.408</imaer:localId>
				</imaer:NEN3610ID>
			</imaer:identifier>
			<imaer:label>L3.100</imaer:label>
			<imaer:emissionSourceCharacteristics>
				<imaer:EmissionSourceCharacteristics>
					<imaer:heatContent>
						<imaer:SpecifiedHeatContent>
							<imaer:value>0.0</imaer:value>
						</imaer:SpecifiedHeatContent>
					</imaer:heatContent>
					<imaer:emissionHeight>5.0</imaer:emissionHeight>
				</imaer:EmissionSourceCharacteristics>
			</imaer:emissionSourceCharacteristics>
			<imaer:geometry>
				<imaer:EmissionSourceGeometry>
					<imaer:GM_Point>
						<gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.408.POINT">
							<gml:pos>116430.65821748896 460915.6499787199</gml:pos>
						</gml:Point>
					</imaer:GM_Point>
				</imaer:EmissionSourceGeometry>
			</imaer:geometry>
			<imaer:emission>
				<imaer:Emission substance="NH3">
					<imaer:value>180</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NOX">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="PM10">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:emission>
				<imaer:Emission substance="NO2">
					<imaer:value>0.0</imaer:value>
				</imaer:Emission>
			</imaer:emission>
			<imaer:farmLodging>
				<imaer:StandardFarmLodging farmLodgingType="L3.100">
					<imaer:numberOfAnimals>100</imaer:numberOfAnimals>
					<imaer:farmLodgingSystemDefinitionType>Overig</imaer:farmLodgingSystemDefinitionType>
				</imaer:StandardFarmLodging>
			</imaer:farmLodging>
		</imaer:FarmLodgingEmissionSource>
	</imaer:featureMember>
</imaer:FeatureCollectionCalculator>
