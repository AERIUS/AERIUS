<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<imaer:FeatureCollectionCalculator xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:imaer="http://imaer.aerius.nl/6.0" xmlns:gml="http://www.opengis.net/gml/3.2" gml:id="NL.IMAER.Collection" xsi:schemaLocation="http://imaer.aerius.nl/6.0 https://imaer.aerius.nl/6.0/IMAER.xsd">
    <imaer:metadata>
        <imaer:AeriusCalculatorMetadata>
            <imaer:project>
                <imaer:ProjectMetadata>
                    <imaer:year>2022</imaer:year>
                </imaer:ProjectMetadata>
            </imaer:project>
            <imaer:situation>
                <imaer:SituationMetadata>
                    <imaer:name>Scenario 1</imaer:name>
                    <imaer:reference>ReXTwFxm3QP9</imaer:reference>
                    <imaer:situationType>PROPOSED</imaer:situationType>
                </imaer:SituationMetadata>
            </imaer:situation>
            <imaer:version>
                <imaer:VersionMetadata>
                    <imaer:aeriusVersion>build-187_2024-06-26-02-04_20240626_e84b834f14</imaer:aeriusVersion>
                    <imaer:databaseVersion>build-187_2024-06-26-02-04_e84b834f14_calculator_uk_latest</imaer:databaseVersion>
                </imaer:VersionMetadata>
            </imaer:version>
        </imaer:AeriusCalculatorMetadata>
    </imaer:metadata>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="1900" gml:id="ES.1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 1</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>12.0</imaer:height>
                    <imaer:specificHeatCapacity>15.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>JET</imaer:sourceType>
                    <imaer:diameter>13.0</imaer:diameter>
                    <imaer:elevationAngle>31.0</imaer:elevationAngle>
                    <imaer:horizontalAngle>1.0</imaer:horizontalAngle>
                    <imaer:buoyancyType>TEMPERATURE</imaer:buoyancyType>
                    <imaer:temperature>15.0</imaer:temperature>
                    <imaer:effluxType>VELOCITY</imaer:effluxType>
                    <imaer:verticalVelocity>21.0</imaer:verticalVelocity>
                    <imaer:hourlyVariation>
                        <imaer:StandardTimeVaryingProfile>
                            <imaer:standardType>CONTINUOUS</imaer:standardType>
                        </imaer:StandardTimeVaryingProfile>
                    </imaer:hourlyVariation>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.1.POINT">
                            <gml:pos>436054.6909375 435483.36703125003</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>15.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>12.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="1920" gml:id="ES.2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 2</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>12.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>LINE</imaer:sourceType>
                    <imaer:width>0.001</imaer:width>
                    <imaer:buoyancyType>DENSITY</imaer:buoyancyType>
                    <imaer:density>1.0</imaer:density>
                    <imaer:effluxType>VOLUME</imaer:effluxType>
                    <imaer:volumetricFlowRate>12.0</imaer:volumetricFlowRate>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Curve>
                        <gml:LineString srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.2.CURVE">
                            <gml:posList>436033.21971841226 435507.8502698063 436055.4585538576 435504.7978806275</gml:posList>
                        </gml:LineString>
                    </imaer:GM_Curve>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="2100" gml:id="ES.3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 3</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>12.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>VOLUME</imaer:sourceType>
                    <imaer:verticalDimension>1.0</imaer:verticalDimension>
                    <imaer:buoyancyType>TEMPERATURE</imaer:buoyancyType>
                    <imaer:temperature>15.0</imaer:temperature>
                    <imaer:effluxType>VELOCITY</imaer:effluxType>
                    <imaer:verticalVelocity>15.0</imaer:verticalVelocity>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.3.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>436067.6681105726 435511.33871458203 436068.97627736354 435501.30943585176 436072.9007777362 435510.90265898505 436067.6681105726 435511.33871458203</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>512.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:FarmAnimalHousingSource sectorId="4110" gml:id="ES.4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 4</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>3.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>LINE</imaer:sourceType>
                    <imaer:width>1.0</imaer:width>
                    <imaer:buoyancyType>AMBIENT</imaer:buoyancyType>
                    <imaer:effluxType>VELOCITY</imaer:effluxType>
                    <imaer:verticalVelocity>15.0</imaer:verticalVelocity>
                    <imaer:hourlyVariation>
                        <imaer:ReferenceTimeVaryingProfile>
                            <imaer:customTimeVaryingProfile xlink:href="#DiurnalProfile.1"/>
                        </imaer:ReferenceTimeVaryingProfile>
                    </imaer:hourlyVariation>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Curve>
                        <gml:LineString srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.4.CURVE">
                            <gml:posList>436052.8376435238 435469.6550627914 436046.0149601997 435523.0325264449</gml:posList>
                        </gml:LineString>
                    </imaer:GM_Curve>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>10002.556</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:animalHousing>
                <imaer:StandardFarmAnimalHousing animalHousingType="A1.1" animalType="A1">
                    <imaer:numberOfAnimals>12</imaer:numberOfAnimals>
                    <imaer:numberOfDays>3</imaer:numberOfDays>
                </imaer:StandardFarmAnimalHousing>
            </imaer:animalHousing>
            <imaer:animalHousing>
                <imaer:CustomFarmAnimalHousing animalType="G">
                    <imaer:numberOfAnimals>2000</imaer:numberOfAnimals>
                    <imaer:description>Custom</imaer:description>
                    <imaer:emissionFactor>
                        <imaer:Emission substance="NH3">
                            <imaer:value>5.0</imaer:value>
                        </imaer:Emission>
                    </imaer:emissionFactor>
                    <imaer:emissionFactorType>PER_ANIMAL_PER_YEAR</imaer:emissionFactorType>
                </imaer:CustomFarmAnimalHousing>
            </imaer:animalHousing>
        </imaer:FarmAnimalHousingSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="4120" gml:id="ES.5">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.5</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 5</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>54.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>AREA</imaer:sourceType>
                    <imaer:buoyancyType>DENSITY</imaer:buoyancyType>
                    <imaer:density>2.0</imaer:density>
                    <imaer:effluxType>VOLUME</imaer:effluxType>
                    <imaer:volumetricFlowRate>52.0</imaer:volumetricFlowRate>
                    <imaer:hourlyVariation>
                        <imaer:StandardTimeVaryingProfile>
                            <imaer:standardType>CONTINUOUS</imaer:standardType>
                        </imaer:StandardTimeVaryingProfile>
                    </imaer:hourlyVariation>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.5.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>436058.85765822156 435521.4271891921 436055.84765087266 435506.9791539175 436079.9277096637 435506.17648529116 436076.31570084504 435521.02585487894 436058.85765822156 435521.4271891921</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>123.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>12.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ADMSRoad roadAreaType="Wal" roadType="Urb" sectorId="3100" gml:id="ES.6">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.6</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 6</imaer:label>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Curve>
                        <gml:LineString srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.6.CURVE">
                            <gml:posList>436075.11169790546 435534.4705543706 436095.1784135647 435497.5477975577</gml:posList>
                        </gml:LineString>
                    </imaer:GM_Curve>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>3975.746986215718</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>91442.17499185515</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:vehicles>
                <imaer:CustomVehicle>
                    <imaer:vehiclesPerTimeUnit>3.0</imaer:vehiclesPerTimeUnit>
                    <imaer:timeUnit>DAY</imaer:timeUnit>
                    <imaer:description>123</imaer:description>
                    <imaer:emissionFactor>
                        <imaer:Emission substance="NOX">
                            <imaer:value>23.0</imaer:value>
                        </imaer:Emission>
                    </imaer:emissionFactor>
                    <imaer:emissionFactor>
                        <imaer:Emission substance="NH3">
                            <imaer:value>1.0</imaer:value>
                        </imaer:Emission>
                    </imaer:emissionFactor>
                </imaer:CustomVehicle>
            </imaer:vehicles>
            <imaer:vehicles>
                <imaer:StandardVehicle vehicleType="Bus">
                    <imaer:vehiclesPerTimeUnit>6.0</imaer:vehiclesPerTimeUnit>
                    <imaer:timeUnit>MONTH</imaer:timeUnit>
                    <imaer:stagnationFactor>0.0</imaer:stagnationFactor>
                    <imaer:maximumSpeed>78</imaer:maximumSpeed>
                    <imaer:strictEnforcement>false</imaer:strictEnforcement>
                </imaer:StandardVehicle>
            </imaer:vehicles>
            <imaer:vehicles>
                <imaer:StandardVehicle vehicleType="Car">
                    <imaer:vehiclesPerTimeUnit>1.0</imaer:vehiclesPerTimeUnit>
                    <imaer:timeUnit>MONTH</imaer:timeUnit>
                    <imaer:stagnationFactor>0.0</imaer:stagnationFactor>
                    <imaer:maximumSpeed>78</imaer:maximumSpeed>
                    <imaer:strictEnforcement>false</imaer:strictEnforcement>
                </imaer:StandardVehicle>
            </imaer:vehicles>
            <imaer:vehicles>
                <imaer:StandardVehicle vehicleType="HGV">
                    <imaer:vehiclesPerTimeUnit>5.0</imaer:vehiclesPerTimeUnit>
                    <imaer:timeUnit>MONTH</imaer:timeUnit>
                    <imaer:stagnationFactor>0.0</imaer:stagnationFactor>
                    <imaer:maximumSpeed>78</imaer:maximumSpeed>
                    <imaer:strictEnforcement>false</imaer:strictEnforcement>
                </imaer:StandardVehicle>
            </imaer:vehicles>
            <imaer:vehicles>
                <imaer:StandardVehicle vehicleType="LGV">
                    <imaer:vehiclesPerTimeUnit>4.0</imaer:vehiclesPerTimeUnit>
                    <imaer:timeUnit>MONTH</imaer:timeUnit>
                    <imaer:stagnationFactor>0.0</imaer:stagnationFactor>
                    <imaer:maximumSpeed>78</imaer:maximumSpeed>
                    <imaer:strictEnforcement>false</imaer:strictEnforcement>
                </imaer:StandardVehicle>
            </imaer:vehicles>
            <imaer:vehicles>
                <imaer:StandardVehicle vehicleType="Mot">
                    <imaer:vehiclesPerTimeUnit>3.0</imaer:vehiclesPerTimeUnit>
                    <imaer:timeUnit>MONTH</imaer:timeUnit>
                    <imaer:stagnationFactor>0.0</imaer:stagnationFactor>
                    <imaer:maximumSpeed>78</imaer:maximumSpeed>
                    <imaer:strictEnforcement>false</imaer:strictEnforcement>
                </imaer:StandardVehicle>
            </imaer:vehicles>
            <imaer:vehicles>
                <imaer:StandardVehicle vehicleType="Tax">
                    <imaer:vehiclesPerTimeUnit>2.0</imaer:vehiclesPerTimeUnit>
                    <imaer:timeUnit>MONTH</imaer:timeUnit>
                    <imaer:stagnationFactor>0.0</imaer:stagnationFactor>
                    <imaer:maximumSpeed>78</imaer:maximumSpeed>
                    <imaer:strictEnforcement>false</imaer:strictEnforcement>
                </imaer:StandardVehicle>
            </imaer:vehicles>
            <imaer:roadManager>STATE</imaer:roadManager>
            <imaer:trafficDirection>BOTH</imaer:trafficDirection>
            <imaer:width>2.0</imaer:width>
            <imaer:elevation>1.0</imaer:elevation>
            <imaer:gradient>2.0</imaer:gradient>
            <imaer:coverage>51.0</imaer:coverage>
            <imaer:barrierLeft>
                <imaer:ADMSRoadSideBarrier>
                    <imaer:barrierType>NOISE_BARRIER</imaer:barrierType>
                    <imaer:distance>512.0</imaer:distance>
                    <imaer:averageHeight>6.0</imaer:averageHeight>
                    <imaer:maximumHeight>12.0</imaer:maximumHeight>
                    <imaer:minimumHeight>5.0</imaer:minimumHeight>
                    <imaer:porosity>5.0</imaer:porosity>
                </imaer:ADMSRoadSideBarrier>
            </imaer:barrierLeft>
        </imaer:ADMSRoad>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:FarmlandEmissionSource sectorId="4150" gml:id="ES.17_1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.17_1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 7</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:building xlink:href="#Building.1"/>
                    <imaer:height>12.0</imaer:height>
                    <imaer:specificHeatCapacity>100.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>POINT</imaer:sourceType>
                    <imaer:diameter>3.0</imaer:diameter>
                    <imaer:buoyancyType>TEMPERATURE</imaer:buoyancyType>
                    <imaer:temperature>15.0</imaer:temperature>
                    <imaer:effluxType>VELOCITY</imaer:effluxType>
                    <imaer:verticalVelocity>15.0</imaer:verticalVelocity>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.17_1.POINT">
                            <gml:pos>436021.18410022306 435546.96354876464</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>1.203679048E7</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>31.001</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:activity>
                <imaer:StandardFarmlandActivity standardActivityType="A.Grazing.3" activityType="PASTURE">
                    <imaer:numberOfAnimals>12000</imaer:numberOfAnimals>
                    <imaer:numberOfDays>340</imaer:numberOfDays>
                </imaer:StandardFarmlandActivity>
            </imaer:activity>
            <imaer:activity>
                <imaer:FarmlandActivity activityType="FERTILIZER">
                    <imaer:emission>
                        <imaer:Emission substance="NOX">
                            <imaer:value>31.0</imaer:value>
                        </imaer:Emission>
                    </imaer:emission>
                    <imaer:emission>
                        <imaer:Emission substance="NH3">
                            <imaer:value>24.0</imaer:value>
                        </imaer:Emission>
                    </imaer:emission>
                </imaer:FarmlandActivity>
            </imaer:activity>
            <imaer:activity>
                <imaer:FarmlandActivity activityType="ORGANIC_PROCESSES">
                    <imaer:emission>
                        <imaer:Emission substance="NOX">
                            <imaer:value>0.001</imaer:value>
                        </imaer:Emission>
                    </imaer:emission>
                    <imaer:emission>
                        <imaer:Emission substance="NH3">
                            <imaer:value>1.2E7</imaer:value>
                        </imaer:Emission>
                    </imaer:emission>
                </imaer:FarmlandActivity>
            </imaer:activity>
            <imaer:activity>
                <imaer:StandardFarmlandActivity standardActivityType="A.Yard.2.2" activityType="OUTDOOR_YARDS">
                    <imaer:numberOfAnimals>56</imaer:numberOfAnimals>
                    <imaer:numberOfDays>83</imaer:numberOfDays>
                </imaer:StandardFarmlandActivity>
            </imaer:activity>
        </imaer:FarmlandEmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="4320" gml:id="ES.8">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.8</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 8</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>12.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>AREA</imaer:sourceType>
                    <imaer:buoyancyType>AMBIENT</imaer:buoyancyType>
                    <imaer:effluxType>VELOCITY</imaer:effluxType>
                    <imaer:verticalVelocity>15.0</imaer:verticalVelocity>
                    <imaer:hourlyVariation>
                        <imaer:ReferenceTimeVaryingProfile>
                            <imaer:customTimeVaryingProfile xlink:href="#DiurnalProfile.2"/>
                        </imaer:ReferenceTimeVaryingProfile>
                    </imaer:hourlyVariation>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.8.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>436078.12170525437 435505.97581813455 436081.3323797598 435478.48441768147 436158.3885678911 435493.53445442586 436078.12170525437 435505.97581813455</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>123.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="4600" gml:id="ES.9">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.9</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 9</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>2.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>POINT</imaer:sourceType>
                    <imaer:diameter>1.0</imaer:diameter>
                    <imaer:buoyancyType>TEMPERATURE</imaer:buoyancyType>
                    <imaer:temperature>15.0</imaer:temperature>
                    <imaer:effluxType>VELOCITY</imaer:effluxType>
                    <imaer:verticalVelocity>15.0</imaer:verticalVelocity>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.9.POINT">
                            <gml:pos>436069.8943518341 435517.6145132169</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>12312.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="1050" gml:id="ES.10">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.10</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 10</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:building xlink:href="#Building.2"/>
                    <imaer:height>0.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>POINT</imaer:sourceType>
                    <imaer:diameter>12.0</imaer:diameter>
                    <imaer:buoyancyType>TEMPERATURE</imaer:buoyancyType>
                    <imaer:temperature>15.0</imaer:temperature>
                    <imaer:effluxType>VELOCITY</imaer:effluxType>
                    <imaer:verticalVelocity>15.0</imaer:verticalVelocity>
                    <imaer:hourlyVariation>
                        <imaer:StandardTimeVaryingProfile>
                            <imaer:standardType>CONTINUOUS</imaer:standardType>
                        </imaer:StandardTimeVaryingProfile>
                    </imaer:hourlyVariation>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.10.POINT">
                            <gml:pos>436096.6834172391 435528.24987251626</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="1100" gml:id="ES.11">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.11</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 11</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>1.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>LINE</imaer:sourceType>
                    <imaer:width>0.001</imaer:width>
                    <imaer:buoyancyType>TEMPERATURE</imaer:buoyancyType>
                    <imaer:temperature>15.0</imaer:temperature>
                    <imaer:effluxType>VELOCITY</imaer:effluxType>
                    <imaer:verticalVelocity>15.0</imaer:verticalVelocity>
                    <imaer:hourlyVariation>
                        <imaer:ReferenceTimeVaryingProfile>
                            <imaer:customTimeVaryingProfile xlink:href="#DiurnalProfile.1"/>
                        </imaer:ReferenceTimeVaryingProfile>
                    </imaer:hourlyVariation>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Curve>
                        <gml:LineString srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.11.CURVE">
                            <gml:posList>436034.17559796077 435532.06254849146 436026.1489116971 435489.52111129405</gml:posList>
                        </gml:LineString>
                    </imaer:GM_Curve>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>5.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="1300" gml:id="ES.12">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.12</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 12</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>12.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>LINE</imaer:sourceType>
                    <imaer:width>0.001</imaer:width>
                    <imaer:buoyancyType>DENSITY</imaer:buoyancyType>
                    <imaer:density>1.225</imaer:density>
                    <imaer:effluxType>VELOCITY</imaer:effluxType>
                    <imaer:verticalVelocity>15.0</imaer:verticalVelocity>
                    <imaer:hourlyVariation>
                        <imaer:ReferenceTimeVaryingProfile>
                            <imaer:customTimeVaryingProfile xlink:href="#DiurnalProfile.2"/>
                        </imaer:ReferenceTimeVaryingProfile>
                    </imaer:hourlyVariation>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Curve>
                        <gml:LineString srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.12.CURVE">
                            <gml:posList>436061.46633125725 435508.78515832685 436063.2723356666 435524.83853085415</gml:posList>
                        </gml:LineString>
                    </imaer:GM_Curve>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>123.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="1400" gml:id="ES.13">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.13</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 13</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>3.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>JET</imaer:sourceType>
                    <imaer:diameter>5.0</imaer:diameter>
                    <imaer:elevationAngle>12.0</imaer:elevationAngle>
                    <imaer:horizontalAngle>2.0</imaer:horizontalAngle>
                    <imaer:buoyancyType>TEMPERATURE</imaer:buoyancyType>
                    <imaer:temperature>15.0</imaer:temperature>
                    <imaer:effluxType>VELOCITY</imaer:effluxType>
                    <imaer:verticalVelocity>15.0</imaer:verticalVelocity>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.13.POINT">
                            <gml:pos>436057.15198739053 435524.2365293844</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="1500" gml:id="ES.14">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.14</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 14</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>2.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>AREA</imaer:sourceType>
                    <imaer:buoyancyType>TEMPERATURE</imaer:buoyancyType>
                    <imaer:temperature>15.0</imaer:temperature>
                    <imaer:effluxType>VELOCITY</imaer:effluxType>
                    <imaer:verticalVelocity>15.0</imaer:verticalVelocity>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.14.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>436035.5802680569 435537.48056171945 436038.7909425624 435521.8285235053 436054.44298077654 435528.4505396728 436035.5802680569 435537.48056171945</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="1700" gml:id="ES.15">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.15</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 15</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>5.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>LINE</imaer:sourceType>
                    <imaer:width>0.001</imaer:width>
                    <imaer:buoyancyType>DENSITY</imaer:buoyancyType>
                    <imaer:density>2.0</imaer:density>
                    <imaer:effluxType>VELOCITY</imaer:effluxType>
                    <imaer:verticalVelocity>15.0</imaer:verticalVelocity>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Curve>
                        <gml:LineString srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.15.CURVE">
                            <gml:posList>436031.16559061187 435567.3799680516 436027.7542489498 435545.90858229634</gml:posList>
                        </gml:LineString>
                    </imaer:GM_Curve>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>52.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="1800" gml:id="ES.16">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.16</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 16</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>2.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>AREA</imaer:sourceType>
                    <imaer:buoyancyType>TEMPERATURE</imaer:buoyancyType>
                    <imaer:temperature>3.0</imaer:temperature>
                    <imaer:effluxType>VELOCITY</imaer:effluxType>
                    <imaer:verticalVelocity>15.0</imaer:verticalVelocity>
                    <imaer:hourlyVariation>
                        <imaer:StandardTimeVaryingProfile>
                            <imaer:standardType>CONTINUOUS</imaer:standardType>
                        </imaer:StandardTimeVaryingProfile>
                    </imaer:hourlyVariation>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.16.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>435965.5474304064 435569.58730677416 435964.3434274668 435501.96247500274 436058.0549895952 435504.771815195 435965.5474304064 435569.58730677416</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>32.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="9999" gml:id="ES.17">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.17</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 17</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>23.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>AREA</imaer:sourceType>
                    <imaer:buoyancyType>DENSITY</imaer:buoyancyType>
                    <imaer:density>0.01</imaer:density>
                    <imaer:effluxType>VOLUME</imaer:effluxType>
                    <imaer:volumetricFlowRate>2.0</imaer:volumetricFlowRate>
                    <imaer:hourlyVariation>
                        <imaer:StandardTimeVaryingProfile>
                            <imaer:standardType>CONTINUOUS</imaer:standardType>
                        </imaer:StandardTimeVaryingProfile>
                    </imaer:hourlyVariation>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.17.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>435981.40013577713 435573.600649906 436001.8681857495 435462.6317123107 436126.2818228364 435463.0330466239 436111.4324532486 435560.3566175709 435981.40013577713 435573.600649906</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>1231.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>1231.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Building 1</imaer:label>
            <imaer:height>5.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.1.POINT">
                            <gml:pos>436051.63364058424 435486.109769632</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:BuildingGeometry>
            </imaer:geometry>
            <imaer:diameter>6.23</imaer:diameter>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Building 2</imaer:label>
            <imaer:height>2.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.2.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>436056.8509866556 435492.3304514863 436057.8543224386 435488.5177755111 436067.2856787984 435494.5377902088 436059.05832537817 435510.18982842297 436056.8509866556 435492.3304514863</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:NcaCustomCalculationPoint gml:id="CP.1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.1.POINT">
                    <gml:pos>208988.09663484574 676882.8751200773</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>Rekenpuntje</imaer:label>
            <imaer:height>1.6</imaer:height>
			<imaer:assessmentCategory>ECOLOGY</imaer:assessmentCategory>
            <imaer:roadLocalFractionNO2>1.0</imaer:roadLocalFractionNO2>
            <imaer:entityReference>
                <imaer:EntityReference>
                    <imaer:entityType>CRITICAL_LEVEL_ENTITY</imaer:entityType>
                    <imaer:description>Mijn habitat</imaer:description>
                    <imaer:criticalLevel>
                        <imaer:CriticalLevel resultType="CONCENTRATION" substance="NOX">
                            <imaer:value>31.0</imaer:value>
                        </imaer:CriticalLevel>
                    </imaer:criticalLevel>
                    <imaer:criticalLevel>
                        <imaer:CriticalLevel resultType="CONCENTRATION" substance="NH3">
                            <imaer:value>4.0</imaer:value>
                        </imaer:CriticalLevel>
                    </imaer:criticalLevel>
                    <imaer:criticalLevel>
                        <imaer:CriticalLevel resultType="DEPOSITION" substance="NOXNH3">
                            <imaer:value>26.0</imaer:value>
                        </imaer:CriticalLevel>
                    </imaer:criticalLevel>
                </imaer:EntityReference>
            </imaer:entityReference>
        </imaer:NcaCustomCalculationPoint>
    </imaer:featureMember>
    <imaer:definitions>
        <imaer:Definitions>
            <imaer:customTimeVaryingProfile>
                <imaer:CustomTimeVaryingProfile gml:id="DiurnalProfile.1">
                    <imaer:label>Custom diurnal profile</imaer:label>
                    <imaer:customType>THREE_DAY</imaer:customType>
                    <imaer:value>2.5</imaer:value>
                    <imaer:value>0.5</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>0.5</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>2.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>0.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.2</imaer:value>
                    <imaer:value>0.4</imaer:value>
                    <imaer:value>0.5</imaer:value>
                    <imaer:value>1.5</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>0.5</imaer:value>
                    <imaer:value>1.6</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.6</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>0.8</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.6</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>0.9</imaer:value>
                    <imaer:value>0.0</imaer:value>
                    <imaer:value>0.0</imaer:value>
                    <imaer:value>0.5</imaer:value>
                    <imaer:value>0.5</imaer:value>
                    <imaer:value>2.0</imaer:value>
                    <imaer:value>1.4</imaer:value>
                    <imaer:value>1.4</imaer:value>
                    <imaer:value>0.4</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>0.5</imaer:value>
                    <imaer:value>0.4</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                </imaer:CustomTimeVaryingProfile>
            </imaer:customTimeVaryingProfile>
            <imaer:customTimeVaryingProfile>
                <imaer:CustomTimeVaryingProfile gml:id="DiurnalProfile.2">
                    <imaer:label>Custom diurnal profile (1)</imaer:label>
                    <imaer:customType>THREE_DAY</imaer:customType>
                    <imaer:value>1.5</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>0.5</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                </imaer:CustomTimeVaryingProfile>
            </imaer:customTimeVaryingProfile>
        </imaer:Definitions>
    </imaer:definitions>
</imaer:FeatureCollectionCalculator>
