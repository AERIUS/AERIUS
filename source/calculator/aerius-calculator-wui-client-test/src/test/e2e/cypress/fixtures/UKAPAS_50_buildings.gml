<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<imaer:FeatureCollectionCalculator xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:imaer="http://imaer.aerius.nl/6.0" xmlns:gml="http://www.opengis.net/gml/3.2" gml:id="NL.IMAER.Collection" xsi:schemaLocation="http://imaer.aerius.nl/6.0 https://imaer.aerius.nl/6.0/IMAER.xsd">
    <imaer:metadata>
        <imaer:AeriusCalculatorMetadata>
            <imaer:project>
                <imaer:ProjectMetadata>
                    <imaer:year>2024</imaer:year>
                </imaer:ProjectMetadata>
            </imaer:project>
            <imaer:situation>
                <imaer:SituationMetadata>
                    <imaer:name>Situatie 1</imaer:name>
                    <imaer:reference>RwvMsgWrmjuQ</imaer:reference>
                    <imaer:situationType>PROPOSED</imaer:situationType>
                </imaer:SituationMetadata>
            </imaer:situation>
            <imaer:version>
                <imaer:VersionMetadata>
                    <imaer:aeriusVersion>build-169_2024-06-12-02-05_20240612_d430753781</imaer:aeriusVersion>
                    <imaer:databaseVersion>build-169_2024-06-12-02-05_d430753781_calculator_uk_latest</imaer:databaseVersion>
                </imaer:VersionMetadata>
            </imaer:version>
        </imaer:AeriusCalculatorMetadata>
    </imaer:metadata>
       <imaer:featureMember>
        <imaer:Building gml:id="Building.1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 1</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.1.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>580758 180963 580808 180963 580808 181013 580758 181013 580758 180963</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 2</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.2.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>580808 181013 580858 181013 580858 181063 580808 181063 580808 181013</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 3</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.3.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>580858 181063 580908 181063 580908 181113 580858 181113 580858 181063</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 4</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.4.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>580908 181113 580958 181113 580958 181163 580908 181163 580908 181113</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.5">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.5</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 5</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.5.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>580958 181163 581008 181163 581008 181213 580958 181213 580958 181163</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.6">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.6</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 6</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.6.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>581008 181213 581058 181213 581058 181263 581008 181263 581008 181213</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.7">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.7</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 7</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.7.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>581058 181263 581108 181263 581108 181313 581058 181313 581058 181263</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.8">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.8</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 8</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.8.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>581108 181313 581158 181313 581158 181363 581108 181363 581108 181313</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.9">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.9</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 9</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.9.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>581158 181363 581208 181363 581208 181413 581158 181413 581158 181363</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.10">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.10</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 10</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.10.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>581208 181413 581258 181413 581258 181463 581208 181463 581208 181413</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.11">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.11</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 11</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.11.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>581258 181463 581308 181463 581308 181513 581258 181513 581258 181463</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.12">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.12</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 12</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.12.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>581308 181513 581358 181513 581358 181563 581308 181563 581308 181513</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.13">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.13</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 13</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.13.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>581358 181563 581408 181563 581408 181613 581358 181613 581358 181563</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.14">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.14</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 14</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.14.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>581408 181613 581458 181613 581458 181663 581408 181663 581408 181613</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.15">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.15</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 15</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.15.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>581458 181663 581508 181663 581508 181713 581458 181713 581458 181663</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.16">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.16</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 16</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.16.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>581508 181713 581558 181713 581558 181763 581508 181763 581508 181713</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.17">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.17</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 17</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.17.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>581558 181763 581608 181763 581608 181813 581558 181813 581558 181763</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.18">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.18</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 18</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.18.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>581608 181813 581658 181813 581658 181863 581608 181863 581608 181813</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.19">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.19</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 19</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.19.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>581658 181863 581708 181863 581708 181913 581658 181913 581658 181863</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.20">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.20</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 20</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.20.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>581708 181913 581758 181913 581758 181963 581708 181963 581708 181913</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.21">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.21</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 21</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.21.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>581758 181963 581808 181963 581808 182013 581758 182013 581758 181963</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.22">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.22</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 22</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.22.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>581808 182013 581858 182013 581858 182063 581808 182063 581808 182013</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.23">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.23</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 23</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.23.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>581858 182063 581908 182063 581908 182113 581858 182113 581858 182063</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.24">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.24</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 24</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.24.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>581908 182113 581958 182113 581958 182163 581908 182163 581908 182113</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.25">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.25</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 25</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.25.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>581958 182163 582008 182163 582008 182213 581958 182213 581958 182163</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.26">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.26</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 26</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.26.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>582008 182213 582058 182213 582058 182263 582008 182263 582008 182213</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.27">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.27</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 27</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.27.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>582058 182263 582108 182263 582108 182313 582058 182313 582058 182263</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.28">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.28</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 28</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.28.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>582108 182313 582158 182313 582158 182363 582108 182363 582108 182313</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.29">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.29</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 29</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.29.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>582158 182363 582208 182363 582208 182413 582158 182413 582158 182363</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.30">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.30</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 30</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.30.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>582208 182413 582258 182413 582258 182463 582208 182463 582208 182413</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.31">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.31</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 31</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.31.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>582258 182463 582308 182463 582308 182513 582258 182513 582258 182463</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.32">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.32</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 32</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.32.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>582308 182513 582358 182513 582358 182563 582308 182563 582308 182513</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.33">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.33</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 33</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.33.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>582358 182563 582408 182563 582408 182613 582358 182613 582358 182563</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.34">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.34</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 34</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.34.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>582408 182613 582458 182613 582458 182663 582408 182663 582408 182613</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.35">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.35</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 35</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.35.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>582458 182663 582508 182663 582508 182713 582458 182713 582458 182663</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.36">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.36</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 36</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.36.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>582508 182713 582558 182713 582558 182763 582508 182763 582508 182713</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.37">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.37</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 37</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.37.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>582558 182763 582608 182763 582608 182813 582558 182813 582558 182763</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.38">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.38</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 38</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.38.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>582608 182813 582658 182813 582658 182863 582608 182863 582608 182813</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.39">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.39</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 39</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.39.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>582658 182863 582708 182863 582708 182913 582658 182913 582658 182863</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.40">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.40</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 40</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.40.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>582708 182913 582758 182913 582758 182963 582708 182963 582708 182913</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.41">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.41</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 41</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.41.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>582758 182963 582808 182963 582808 183013 582758 183013 582758 182963</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.42">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.42</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 42</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.42.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>582808 183013 582858 183013 582858 183063 582808 183063 582808 183013</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.43">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.43</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 43</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.43.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>582858 183063 582908 183063 582908 183113 582858 183113 582858 183063</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.44">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.44</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 44</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.44.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>582908 183113 582958 183113 582958 183163 582908 183163 582908 183113</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.45">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.45</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 45</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.45.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>582958 183163 583008 183163 583008 183213 582958 183213 582958 183163</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.46">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.46</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 46</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.46.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>583008 183213 583058 183213 583058 183263 583008 183263 583008 183213</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.47">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.47</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 47</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.47.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>583058 183263 583108 183263 583108 183313 583058 183313 583058 183263</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.48">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.48</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 48</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.48.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>583108 183313 583158 183313 583158 183363 583108 183363 583108 183313</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.49">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.49</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 49</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.49.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>583158 183363 583208 183363 583208 183413 583158 183413 583158 183363</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.50">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.50</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Gebouw 50</imaer:label>
            <imaer:height>10.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.50.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>583208 183413 583258 183413 583258 183463 583208 183463 583208 183413</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
</imaer:FeatureCollectionCalculator>
