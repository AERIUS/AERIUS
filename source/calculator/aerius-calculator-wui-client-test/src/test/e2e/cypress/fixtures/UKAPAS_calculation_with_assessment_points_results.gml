<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<imaer:FeatureCollectionCalculator xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:imaer="http://imaer.aerius.nl/5.1" xmlns:gml="http://www.opengis.net/gml/3.2" gml:id="NL.IMAER.Collection" xsi:schemaLocation="http://imaer.aerius.nl/5.1 https://imaer.aerius.nl/5.1/IMAER.xsd">
    <imaer:metadata>
        <imaer:AeriusCalculatorMetadata>
            <imaer:project>
                <imaer:ProjectMetadata>
                    <imaer:year>2023</imaer:year>
                </imaer:ProjectMetadata>
            </imaer:project>
            <imaer:situation>
                <imaer:SituationMetadata>
                    <imaer:name>Scenario 1</imaer:name>
                    <imaer:reference>RhaUEPv1ksme</imaer:reference>
                    <imaer:situationType>PROPOSED</imaer:situationType>
                </imaer:SituationMetadata>
            </imaer:situation>
            <imaer:calculation>
                <imaer:CalculationMetadata>
                    <imaer:method>CUSTOM_POINTS</imaer:method>
                    <imaer:jobType>PROCESS_CONTRIBUTION</imaer:jobType>
                    <imaer:substance>NOX</imaer:substance>
                    <imaer:substance>NO2</imaer:substance>
                    <imaer:substance>NH3</imaer:substance>
                    <imaer:resultType>CONCENTRATION</imaer:resultType>
                    <imaer:resultType>DEPOSITION</imaer:resultType>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>without_source_stacking</imaer:key>
                            <imaer:value>true</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_version</imaer:key>
                            <imaer:value>5.0.3.0-9166</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_permit_area</imaer:key>
                            <imaer:value>eng</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_meteo_years</imaer:key>
                            <imaer:value></imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>road_local_fraction_no2_receptors_option</imaer:key>
                            <imaer:value>LOCATION_BASED</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>road_local_fraction_no2_points_option</imaer:key>
                            <imaer:value>LOCATION_BASED</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_min_monin_obukhov_length</imaer:key>
                            <imaer:value>30.0</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_surface_albedo</imaer:key>
                            <imaer:value>0.23</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_priestley_taylor_parameter</imaer:key>
                            <imaer:value>1.0</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_met_site_id</imaer:key>
                            <imaer:value>2014001</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_met_site_roughness</imaer:key>
                            <imaer:value>0.04</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_met_site_min_monin_obukhov_length</imaer:key>
                            <imaer:value>16.34</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_met_site_surface_albedo</imaer:key>
                            <imaer:value>0.23</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_met_site_priestley_taylor_parameter</imaer:key>
                            <imaer:value>1.0</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_plume_depletion_nh3</imaer:key>
                            <imaer:value>false</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_plume_depletion_nox</imaer:key>
                            <imaer:value>false</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_spatially_varying_roughness</imaer:key>
                            <imaer:value>true</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_complex_terrain</imaer:key>
                            <imaer:value>false</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                </imaer:CalculationMetadata>
            </imaer:calculation>
            <imaer:version>
                <imaer:VersionMetadata>
                    <imaer:aeriusVersion>2023.1-SNAPSHOT_20231027_00875eeb94</imaer:aeriusVersion>
                    <imaer:databaseVersion>latest_00875eeb94_calculator_uk_latest</imaer:databaseVersion>
                </imaer:VersionMetadata>
            </imaer:version>
        </imaer:AeriusCalculatorMetadata>
    </imaer:metadata>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="1900" gml:id="ES.1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 1</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>0.5</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>POINT</imaer:sourceType>
                    <imaer:diameter>0.001</imaer:diameter>
                    <imaer:buoyancyType>TEMPERATURE</imaer:buoyancyType>
                    <imaer:temperature>15.0</imaer:temperature>
                    <imaer:effluxType>VELOCITY</imaer:effluxType>
                    <imaer:verticalVelocity>15.0</imaer:verticalVelocity>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.1.POINT">
                            <gml:pos>246455.8519227314 686457.9764062501</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>555.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:NcaCustomCalculationPoint gml:id="CP.1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.1.POINT">
                    <gml:pos>245682.73280094148 687409.542536173</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.0105251</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.05473052</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>Assessment point 1</imaer:label>
            <imaer:height>1.5</imaer:height>
            <imaer:roadLocalFractionNO2>0.0</imaer:roadLocalFractionNO2>
        </imaer:NcaCustomCalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:NcaCustomCalculationPoint gml:id="CP.2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.2.POINT">
                    <gml:pos>248179.6426032124 683395.0023952032</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00182806</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.009505912</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>Assessment point 2</imaer:label>
            <imaer:height>1.5</imaer:height>
            <imaer:roadLocalFractionNO2>0.0</imaer:roadLocalFractionNO2>
        </imaer:NcaCustomCalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:NcaCustomCalculationPoint gml:id="CP.3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.3.POINT">
                    <gml:pos>249375.18992163741 687489.7521888413</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.00194191</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.010097932</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>Assessment point 3</imaer:label>
            <imaer:height>1.5</imaer:height>
            <imaer:roadLocalFractionNO2>0.0</imaer:roadLocalFractionNO2>
        </imaer:NcaCustomCalculationPoint>
    </imaer:featureMember>
</imaer:FeatureCollectionCalculator>
