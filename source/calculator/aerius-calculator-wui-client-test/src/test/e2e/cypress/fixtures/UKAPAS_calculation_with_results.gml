<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<imaer:FeatureCollectionCalculator xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:imaer="http://imaer.aerius.nl/5.1" xmlns:gml="http://www.opengis.net/gml/3.2" gml:id="NL.IMAER.Collection" xsi:schemaLocation="http://imaer.aerius.nl/5.1 https://imaer.aerius.nl/5.1/IMAER.xsd">
    <imaer:metadata>
        <imaer:AeriusCalculatorMetadata>
            <imaer:project>
                <imaer:ProjectMetadata>
                    <imaer:year>2022</imaer:year>
                </imaer:ProjectMetadata>
            </imaer:project>
            <imaer:situation>
                <imaer:SituationMetadata>
                    <imaer:name>Scenario</imaer:name>
                    <imaer:reference>Rd3RjE4YWRkx</imaer:reference>
                    <imaer:situationType>PROPOSED</imaer:situationType>
                </imaer:SituationMetadata>
            </imaer:situation>
            <imaer:calculation>
                <imaer:CalculationMetadata>
                    <imaer:method>QUICK_RUN</imaer:method>
                    <imaer:jobType>PROCESS_CONTRIBUTION</imaer:jobType>
                    <imaer:substance>NOX</imaer:substance>
                    <imaer:substance>NO2</imaer:substance>
                    <imaer:substance>NH3</imaer:substance>
                    <imaer:resultType>CONCENTRATION</imaer:resultType>
                    <imaer:resultType>DEPOSITION</imaer:resultType>
                    <imaer:maximumRange>15000.0</imaer:maximumRange>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>without_source_stacking</imaer:key>
                            <imaer:value>true</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_version</imaer:key>
                            <imaer:value>5.0.3.0-9166</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_permit_area</imaer:key>
                            <imaer:value>eng</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_meteo_years</imaer:key>
                            <imaer:value></imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>road_local_fraction_no2_receptors_option</imaer:key>
                            <imaer:value>LOCATION_BASED</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>road_local_fraction_no2_points_option</imaer:key>
                            <imaer:value>LOCATION_BASED</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_min_monin_obukhov_length</imaer:key>
                            <imaer:value>30.0</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_surface_albedo</imaer:key>
                            <imaer:value>0.23</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_priestley_taylor_parameter</imaer:key>
                            <imaer:value>1.0</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_met_site_id</imaer:key>
                            <imaer:value>2014001</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_met_site_roughness</imaer:key>
                            <imaer:value>0.04</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_met_site_min_monin_obukhov_length</imaer:key>
                            <imaer:value>16.34</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_met_site_surface_albedo</imaer:key>
                            <imaer:value>0.23</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_met_site_priestley_taylor_parameter</imaer:key>
                            <imaer:value>1.0</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_plume_depletion_nh3</imaer:key>
                            <imaer:value>false</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_plume_depletion_nox</imaer:key>
                            <imaer:value>false</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_spatially_varying_roughness</imaer:key>
                            <imaer:value>true</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>adms_complex_terrain</imaer:key>
                            <imaer:value>false</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                </imaer:CalculationMetadata>
            </imaer:calculation>
            <imaer:version>
                <imaer:VersionMetadata>
                    <imaer:aeriusVersion>2023.1-SNAPSHOT_20231025_247b2986dc</imaer:aeriusVersion>
                    <imaer:databaseVersion>latest_247b2986dc_calculator_uk_latest</imaer:databaseVersion>
                </imaer:VersionMetadata>
            </imaer:version>
        </imaer:AeriusCalculatorMetadata>
    </imaer:metadata>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="1920" gml:id="ES.2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 2</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>12.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>LINE</imaer:sourceType>
                    <imaer:width>0.001</imaer:width>
                    <imaer:buoyancyType>DENSITY</imaer:buoyancyType>
                    <imaer:density>1.0</imaer:density>
                    <imaer:effluxType>VOLUME</imaer:effluxType>
                    <imaer:volumetricFlowRate>12.0</imaer:volumetricFlowRate>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Curve>
                        <gml:LineString srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.2.CURVE">
                            <gml:posList>436033.21971841226 435507.8502698063 436055.4585538576 435504.7978806275</gml:posList>
                        </gml:LineString>
                    </imaer:GM_Curve>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="2100" gml:id="ES.3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 3</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>12.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>VOLUME</imaer:sourceType>
                    <imaer:verticalDimension>1.0</imaer:verticalDimension>
                    <imaer:buoyancyType>TEMPERATURE</imaer:buoyancyType>
                    <imaer:temperature>15.0</imaer:temperature>
                    <imaer:effluxType>VELOCITY</imaer:effluxType>
                    <imaer:verticalVelocity>15.0</imaer:verticalVelocity>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.3.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>436067.6681105726 435511.33871458203 436068.97627736354 435501.30943585176 436072.9007777362 435510.90265898505 436067.6681105726 435511.33871458203</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>512.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:FarmLodgingEmissionSource sectorId="4110" gml:id="ES.4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 4</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>3.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>LINE</imaer:sourceType>
                    <imaer:width>1.0</imaer:width>
                    <imaer:buoyancyType>AMBIENT</imaer:buoyancyType>
                    <imaer:effluxType>VELOCITY</imaer:effluxType>
                    <imaer:verticalVelocity>15.0</imaer:verticalVelocity>
                    <imaer:diurnalVariation>
                        <imaer:ReferenceDiurnalVariation>
                            <imaer:customDiurnalVariation xlink:href="#DiurnalProfile.1"/>
                        </imaer:ReferenceDiurnalVariation>
                    </imaer:diurnalVariation>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Curve>
                        <gml:LineString srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.4.CURVE">
                            <gml:posList>436052.8376435238 435469.6550627914 436046.0149601997 435523.0325264449</gml:posList>
                        </gml:LineString>
                    </imaer:GM_Curve>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>10002.556</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:farmLodging>
                <imaer:StandardFarmLodging farmLodgingType="A1.1">
                    <imaer:numberOfAnimals>12</imaer:numberOfAnimals>
                    <imaer:numberOfDays>3</imaer:numberOfDays>
                    <imaer:farmLodgingSystemDefinitionType></imaer:farmLodgingSystemDefinitionType>
                </imaer:StandardFarmLodging>
            </imaer:farmLodging>
            <imaer:farmLodging>
                <imaer:CustomFarmLodging>
                    <imaer:numberOfAnimals>2000</imaer:numberOfAnimals>
                    <imaer:animalType>G</imaer:animalType>
                    <imaer:description>Custom</imaer:description>
                    <imaer:emissionFactor>
                        <imaer:Emission substance="NH3">
                            <imaer:value>5.0</imaer:value>
                        </imaer:Emission>
                    </imaer:emissionFactor>
                    <imaer:emissionFactorType>PER_ANIMAL_PER_YEAR</imaer:emissionFactorType>
                </imaer:CustomFarmLodging>
            </imaer:farmLodging>
        </imaer:FarmLodgingEmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="4120" gml:id="ES.5">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.5</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 5</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>54.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>AREA</imaer:sourceType>
                    <imaer:buoyancyType>DENSITY</imaer:buoyancyType>
                    <imaer:density>2.0</imaer:density>
                    <imaer:effluxType>VOLUME</imaer:effluxType>
                    <imaer:volumetricFlowRate>52.0</imaer:volumetricFlowRate>
                    <imaer:diurnalVariation>
                        <imaer:StandardDiurnalVariation>
                            <imaer:standardType>CONTINUOUS</imaer:standardType>
                        </imaer:StandardDiurnalVariation>
                    </imaer:diurnalVariation>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.5.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>436058.85765822156 435521.4271891921 436055.84765087266 435506.9791539175 436079.9277096637 435506.17648529116 436076.31570084504 435521.02585487894 436058.85765822156 435521.4271891921</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>123.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>12.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ADMSRoad roadAreaType="Wal" roadType="Urb" sectorId="3100" gml:id="ES.6">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.6</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 6</imaer:label>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Curve>
                        <gml:LineString srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.6.CURVE">
                            <gml:posList>436075.11169790546 435534.4705543706 436095.1784135647 435497.5477975577</gml:posList>
                        </gml:LineString>
                    </imaer:GM_Curve>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>3975.746986215718</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>91442.17499185515</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:vehicles>
                <imaer:CustomVehicle>
                    <imaer:vehiclesPerTimeUnit>3.0</imaer:vehiclesPerTimeUnit>
                    <imaer:timeUnit>DAY</imaer:timeUnit>
                    <imaer:description>123</imaer:description>
                    <imaer:emission>
                        <imaer:Emission substance="NOX">
                            <imaer:value>23.0</imaer:value>
                        </imaer:Emission>
                    </imaer:emission>
                    <imaer:emission>
                        <imaer:Emission substance="NH3">
                            <imaer:value>1.0</imaer:value>
                        </imaer:Emission>
                    </imaer:emission>
                </imaer:CustomVehicle>
            </imaer:vehicles>
            <imaer:vehicles>
                <imaer:StandardVehicle vehicleType="Bus">
                    <imaer:vehiclesPerTimeUnit>6.0</imaer:vehiclesPerTimeUnit>
                    <imaer:timeUnit>MONTH</imaer:timeUnit>
                    <imaer:stagnationFactor>0.0</imaer:stagnationFactor>
                    <imaer:maximumSpeed>78</imaer:maximumSpeed>
                    <imaer:strictEnforcement>false</imaer:strictEnforcement>
                </imaer:StandardVehicle>
            </imaer:vehicles>
            <imaer:vehicles>
                <imaer:StandardVehicle vehicleType="Car">
                    <imaer:vehiclesPerTimeUnit>1.0</imaer:vehiclesPerTimeUnit>
                    <imaer:timeUnit>MONTH</imaer:timeUnit>
                    <imaer:stagnationFactor>0.0</imaer:stagnationFactor>
                    <imaer:maximumSpeed>78</imaer:maximumSpeed>
                    <imaer:strictEnforcement>false</imaer:strictEnforcement>
                </imaer:StandardVehicle>
            </imaer:vehicles>
            <imaer:vehicles>
                <imaer:StandardVehicle vehicleType="HGV">
                    <imaer:vehiclesPerTimeUnit>5.0</imaer:vehiclesPerTimeUnit>
                    <imaer:timeUnit>MONTH</imaer:timeUnit>
                    <imaer:stagnationFactor>0.0</imaer:stagnationFactor>
                    <imaer:maximumSpeed>78</imaer:maximumSpeed>
                    <imaer:strictEnforcement>false</imaer:strictEnforcement>
                </imaer:StandardVehicle>
            </imaer:vehicles>
            <imaer:vehicles>
                <imaer:StandardVehicle vehicleType="LGV">
                    <imaer:vehiclesPerTimeUnit>4.0</imaer:vehiclesPerTimeUnit>
                    <imaer:timeUnit>MONTH</imaer:timeUnit>
                    <imaer:stagnationFactor>0.0</imaer:stagnationFactor>
                    <imaer:maximumSpeed>78</imaer:maximumSpeed>
                    <imaer:strictEnforcement>false</imaer:strictEnforcement>
                </imaer:StandardVehicle>
            </imaer:vehicles>
            <imaer:vehicles>
                <imaer:StandardVehicle vehicleType="Mot">
                    <imaer:vehiclesPerTimeUnit>3.0</imaer:vehiclesPerTimeUnit>
                    <imaer:timeUnit>MONTH</imaer:timeUnit>
                    <imaer:stagnationFactor>0.0</imaer:stagnationFactor>
                    <imaer:maximumSpeed>78</imaer:maximumSpeed>
                    <imaer:strictEnforcement>false</imaer:strictEnforcement>
                </imaer:StandardVehicle>
            </imaer:vehicles>
            <imaer:vehicles>
                <imaer:StandardVehicle vehicleType="Tax">
                    <imaer:vehiclesPerTimeUnit>2.0</imaer:vehiclesPerTimeUnit>
                    <imaer:timeUnit>MONTH</imaer:timeUnit>
                    <imaer:stagnationFactor>0.0</imaer:stagnationFactor>
                    <imaer:maximumSpeed>78</imaer:maximumSpeed>
                    <imaer:strictEnforcement>false</imaer:strictEnforcement>
                </imaer:StandardVehicle>
            </imaer:vehicles>
            <imaer:roadManager>STATE</imaer:roadManager>
            <imaer:trafficDirection>BOTH</imaer:trafficDirection>
            <imaer:width>2.0</imaer:width>
            <imaer:elevation>1.0</imaer:elevation>
            <imaer:gradient>2.0</imaer:gradient>
            <imaer:coverage>51.0</imaer:coverage>
            <imaer:barrierLeft>
                <imaer:ADMSRoadSideBarrier>
                    <imaer:barrierType>NOISE_BARRIER</imaer:barrierType>
                    <imaer:distance>512.0</imaer:distance>
                    <imaer:averageHeight>6.0</imaer:averageHeight>
                    <imaer:maximumHeight>12.0</imaer:maximumHeight>
                    <imaer:minimumHeight>5.0</imaer:minimumHeight>
                    <imaer:porosity>5.0</imaer:porosity>
                </imaer:ADMSRoadSideBarrier>
            </imaer:barrierLeft>
            <imaer:diurnalVariation>
                <imaer:StandardDiurnalVariation>
                    <imaer:standardType>UK_ROAD_2019</imaer:standardType>
                </imaer:StandardDiurnalVariation>
            </imaer:diurnalVariation>
        </imaer:ADMSRoad>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:FarmlandEmissionSource sectorId="4150" gml:id="ES.17_1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.17_1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 7</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:building xlink:href="#Building.1"/>
                    <imaer:height>12.0</imaer:height>
                    <imaer:specificHeatCapacity>100.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>POINT</imaer:sourceType>
                    <imaer:diameter>3.0</imaer:diameter>
                    <imaer:buoyancyType>TEMPERATURE</imaer:buoyancyType>
                    <imaer:temperature>15.0</imaer:temperature>
                    <imaer:effluxType>VELOCITY</imaer:effluxType>
                    <imaer:verticalVelocity>15.0</imaer:verticalVelocity>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.17_1.POINT">
                            <gml:pos>436021.18410022306 435546.96354876464</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>1.203679048E7</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>31.001</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:activity>
                <imaer:StandardFarmlandActivity standardActivityType="A.Grazing.3" activityType="PASTURE">
                    <imaer:numberOfAnimals>12000</imaer:numberOfAnimals>
                    <imaer:numberOfDays>340</imaer:numberOfDays>
                </imaer:StandardFarmlandActivity>
            </imaer:activity>
            <imaer:activity>
                <imaer:FarmlandActivity activityType="FERTILIZER">
                    <imaer:emission>
                        <imaer:Emission substance="NOX">
                            <imaer:value>31.0</imaer:value>
                        </imaer:Emission>
                    </imaer:emission>
                    <imaer:emission>
                        <imaer:Emission substance="NH3">
                            <imaer:value>24.0</imaer:value>
                        </imaer:Emission>
                    </imaer:emission>
                </imaer:FarmlandActivity>
            </imaer:activity>
            <imaer:activity>
                <imaer:FarmlandActivity activityType="ORGANIC_PROCESSES">
                    <imaer:emission>
                        <imaer:Emission substance="NOX">
                            <imaer:value>0.001</imaer:value>
                        </imaer:Emission>
                    </imaer:emission>
                    <imaer:emission>
                        <imaer:Emission substance="NH3">
                            <imaer:value>1.2E7</imaer:value>
                        </imaer:Emission>
                    </imaer:emission>
                </imaer:FarmlandActivity>
            </imaer:activity>
            <imaer:activity>
                <imaer:StandardFarmlandActivity standardActivityType="A.Yard.2.2" activityType="OUTDOOR_YARDS">
                    <imaer:numberOfAnimals>56</imaer:numberOfAnimals>
                    <imaer:numberOfDays>83</imaer:numberOfDays>
                </imaer:StandardFarmlandActivity>
            </imaer:activity>
        </imaer:FarmlandEmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="4320" gml:id="ES.8">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.8</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 8</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>12.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>AREA</imaer:sourceType>
                    <imaer:buoyancyType>AMBIENT</imaer:buoyancyType>
                    <imaer:effluxType>VELOCITY</imaer:effluxType>
                    <imaer:verticalVelocity>15.0</imaer:verticalVelocity>
                    <imaer:diurnalVariation>
                        <imaer:ReferenceDiurnalVariation>
                            <imaer:customDiurnalVariation xlink:href="#DiurnalProfile.2"/>
                        </imaer:ReferenceDiurnalVariation>
                    </imaer:diurnalVariation>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.8.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>436078.12170525437 435505.97581813455 436081.3323797598 435478.48441768147 436158.3885678911 435493.53445442586 436078.12170525437 435505.97581813455</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>123.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="4600" gml:id="ES.9">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.9</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 9</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>2.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>POINT</imaer:sourceType>
                    <imaer:diameter>1.0</imaer:diameter>
                    <imaer:buoyancyType>TEMPERATURE</imaer:buoyancyType>
                    <imaer:temperature>15.0</imaer:temperature>
                    <imaer:effluxType>VELOCITY</imaer:effluxType>
                    <imaer:verticalVelocity>15.0</imaer:verticalVelocity>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.9.POINT">
                            <gml:pos>436069.8943518341 435517.6145132169</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>12312.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="1050" gml:id="ES.10">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.10</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 10</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:building xlink:href="#Building.2"/>
                    <imaer:height>0.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>POINT</imaer:sourceType>
                    <imaer:diameter>12.0</imaer:diameter>
                    <imaer:buoyancyType>TEMPERATURE</imaer:buoyancyType>
                    <imaer:temperature>15.0</imaer:temperature>
                    <imaer:effluxType>VELOCITY</imaer:effluxType>
                    <imaer:verticalVelocity>15.0</imaer:verticalVelocity>
                    <imaer:diurnalVariation>
                        <imaer:StandardDiurnalVariation>
                            <imaer:standardType>CONTINUOUS</imaer:standardType>
                        </imaer:StandardDiurnalVariation>
                    </imaer:diurnalVariation>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.10.POINT">
                            <gml:pos>436096.6834172391 435528.24987251626</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="1100" gml:id="ES.11">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.11</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 11</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>1.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>LINE</imaer:sourceType>
                    <imaer:width>2.0</imaer:width>
                    <imaer:buoyancyType>TEMPERATURE</imaer:buoyancyType>
                    <imaer:temperature>15.0</imaer:temperature>
                    <imaer:effluxType>VELOCITY</imaer:effluxType>
                    <imaer:verticalVelocity>15.0</imaer:verticalVelocity>
                    <imaer:diurnalVariation>
                        <imaer:ReferenceDiurnalVariation>
                            <imaer:customDiurnalVariation xlink:href="#DiurnalProfile.1"/>
                        </imaer:ReferenceDiurnalVariation>
                    </imaer:diurnalVariation>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Curve>
                        <gml:LineString srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.11.CURVE">
                            <gml:posList>436034.17559796077 435532.06254849146 436026.1489116971 435489.52111129405</gml:posList>
                        </gml:LineString>
                    </imaer:GM_Curve>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>5.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="1300" gml:id="ES.12">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.12</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 12</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>12.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>LINE</imaer:sourceType>
                    <imaer:width>6.0</imaer:width>
                    <imaer:buoyancyType>DENSITY</imaer:buoyancyType>
                    <imaer:density>1.225</imaer:density>
                    <imaer:effluxType>VELOCITY</imaer:effluxType>
                    <imaer:verticalVelocity>15.0</imaer:verticalVelocity>
                    <imaer:diurnalVariation>
                        <imaer:ReferenceDiurnalVariation>
                            <imaer:customDiurnalVariation xlink:href="#DiurnalProfile.2"/>
                        </imaer:ReferenceDiurnalVariation>
                    </imaer:diurnalVariation>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Curve>
                        <gml:LineString srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.12.CURVE">
                            <gml:posList>436061.46633125725 435508.78515832685 436063.2723356666 435524.83853085415</gml:posList>
                        </gml:LineString>
                    </imaer:GM_Curve>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>123.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="1400" gml:id="ES.13">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.13</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 13</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>3.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>JET</imaer:sourceType>
                    <imaer:diameter>5.0</imaer:diameter>
                    <imaer:elevationAngle>12.0</imaer:elevationAngle>
                    <imaer:horizontalAngle>2.0</imaer:horizontalAngle>
                    <imaer:buoyancyType>TEMPERATURE</imaer:buoyancyType>
                    <imaer:temperature>15.0</imaer:temperature>
                    <imaer:effluxType>VELOCITY</imaer:effluxType>
                    <imaer:verticalVelocity>15.0</imaer:verticalVelocity>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.13.POINT">
                            <gml:pos>436057.15198739053 435524.2365293844</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="1500" gml:id="ES.14">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.14</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 14</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>2.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>AREA</imaer:sourceType>
                    <imaer:buoyancyType>TEMPERATURE</imaer:buoyancyType>
                    <imaer:temperature>15.0</imaer:temperature>
                    <imaer:effluxType>VELOCITY</imaer:effluxType>
                    <imaer:verticalVelocity>15.0</imaer:verticalVelocity>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.14.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>436035.5802680569 435537.48056171945 436038.7909425624 435521.8285235053 436054.44298077654 435528.4505396728 436035.5802680569 435537.48056171945</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="1700" gml:id="ES.15">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.15</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 15</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>5.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>LINE</imaer:sourceType>
                    <imaer:width>2.0</imaer:width>
                    <imaer:buoyancyType>DENSITY</imaer:buoyancyType>
                    <imaer:density>2.0</imaer:density>
                    <imaer:effluxType>VELOCITY</imaer:effluxType>
                    <imaer:verticalVelocity>15.0</imaer:verticalVelocity>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Curve>
                        <gml:LineString srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.15.CURVE">
                            <gml:posList>436031.16559061187 435567.3799680516 436027.7542489498 435545.90858229634</gml:posList>
                        </gml:LineString>
                    </imaer:GM_Curve>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>52.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="1800" gml:id="ES.16">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.16</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 16</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>2.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>AREA</imaer:sourceType>
                    <imaer:buoyancyType>TEMPERATURE</imaer:buoyancyType>
                    <imaer:temperature>3.0</imaer:temperature>
                    <imaer:effluxType>VELOCITY</imaer:effluxType>
                    <imaer:verticalVelocity>15.0</imaer:verticalVelocity>
                    <imaer:diurnalVariation>
                        <imaer:StandardDiurnalVariation>
                            <imaer:standardType>CONTINUOUS</imaer:standardType>
                        </imaer:StandardDiurnalVariation>
                    </imaer:diurnalVariation>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.16.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>435965.5474304064 435569.58730677416 435964.3434274668 435501.96247500274 436058.0549895952 435504.771815195 435965.5474304064 435569.58730677416</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>32.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="9999" gml:id="ES.17">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.17</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 17</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:ADMSSourceCharacteristics>
                    <imaer:height>23.0</imaer:height>
                    <imaer:specificHeatCapacity>1012.0</imaer:specificHeatCapacity>
                    <imaer:sourceType>AREA</imaer:sourceType>
                    <imaer:buoyancyType>DENSITY</imaer:buoyancyType>
                    <imaer:density>0.01</imaer:density>
                    <imaer:effluxType>VOLUME</imaer:effluxType>
                    <imaer:volumetricFlowRate>2.0</imaer:volumetricFlowRate>
                    <imaer:diurnalVariation>
                        <imaer:StandardDiurnalVariation>
                            <imaer:standardType>CONTINUOUS</imaer:standardType>
                        </imaer:StandardDiurnalVariation>
                    </imaer:diurnalVariation>
                </imaer:ADMSSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="ES.17.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>435981.40013577713 435573.600649906 436001.8681857495 435462.6317123107 436126.2818228364 435463.0330466239 436111.4324532486 435560.3566175709 435981.40013577713 435573.600649906</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>1231.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>1231.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Building 1</imaer:label>
            <imaer:height>5.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.1.POINT">
                            <gml:pos>436051.63364058424 435486.109769632</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:BuildingGeometry>
            </imaer:geometry>
            <imaer:diameter>6.23</imaer:diameter>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:Building gml:id="Building.2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>Building.2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Building 2</imaer:label>
            <imaer:height>2.0</imaer:height>
            <imaer:geometry>
                <imaer:BuildingGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="Building.2.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>436056.8509866556 435492.3304514863 436057.8543224386 435488.5177755111 436067.2856787984 435494.5377902088 436059.05832537817 435510.18982842297 436056.8509866556 435492.3304514863</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:BuildingGeometry>
            </imaer:geometry>
        </imaer:Building>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7073380" gml:id="CP.7073380">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7073380</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7073380.POINT">
                    <gml:pos>446040.5098617754 429744.60698848724</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7073380">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>446103.0 429852.0 446165.0 429745.0 446103.0 429637.0 445978.0 429637.0 445916.0 429745.0 445978.0 429852.0 446103.0 429852.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.79873</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>45.62257</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05425302</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.005784635</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.03174708</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7264333" gml:id="CP.7264333">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7264333</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7264333.POINT">
                    <gml:pos>430592.46920068056 441242.5052589992</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7264333">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>430655.0 441350.0 430717.0 441243.0 430655.0 441135.0 430530.0 441135.0 430468.0 441243.0 430530.0 441350.0 430655.0 441350.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>13.486</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>86.256454</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0921462</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.011411041</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.05408589</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7080520" gml:id="CP.7080520">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7080520</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7080520.POINT">
                    <gml:pos>446040.5098617754 430174.4349612167</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7080520">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>446103.0 430282.0 446165.0 430174.0 446103.0 430067.0 445978.0 430067.0 445916.0 430174.0 445978.0 430282.0 446103.0 430282.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.76241</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>43.99734</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05416896</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.005464626</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.03165697</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7080521" gml:id="CP.7080521">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7080521</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7080521.POINT">
                    <gml:pos>446412.7518054162 430174.4349612167</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7080521">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>446475.0 430282.0 446537.0 430174.0 446475.0 430067.0 446351.0 430067.0 446289.0 430174.0 446351.0 430282.0 446475.0 430282.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.45608</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>42.261063</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0517935</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0052226307</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.030255074</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7264331" gml:id="CP.7264331">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7264331</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7264331.POINT">
                    <gml:pos>429847.9853133989 441242.5052589992</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7264331">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>429910.0 441350.0 429972.0 441243.0 429910.0 441135.0 429786.0 441135.0 429724.0 441243.0 429786.0 441350.0 429910.0 441350.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>12.3321</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>84.64754</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0831687</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.011234712</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.048812617</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7271494" gml:id="CP.7271494">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7271494</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7271494.POINT">
                    <gml:pos>438409.5500171382 441672.3332317286</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7271494">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>438472.0 441780.0 438534.0 441672.0 438472.0 441565.0 438348.0 441565.0 438285.0 441672.0 438348.0 441780.0 438472.0 441780.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>19.4886</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>110.46139</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1542939</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.015592207</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.09032677</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7271495" gml:id="CP.7271495">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7271495</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7271495.POINT">
                    <gml:pos>438781.791960779 441672.3332317286</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7271495">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>438844.0 441780.0 438906.0 441672.0 438844.0 441565.0 438720.0 441565.0 438658.0 441672.0 438720.0 441780.0 438844.0 441780.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>19.2703</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>149.80731</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1551856</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.026123656</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.09080172</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7264321" gml:id="CP.7264321">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7264321</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7264321.POINT">
                    <gml:pos>426125.56587699044 441242.5052589992</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7264321">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>426188.0 441350.0 426250.0 441243.0 426188.0 441135.0 426064.0 441135.0 426001.0 441243.0 426064.0 441350.0 426188.0 441350.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.35375</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>46.843388</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.047249</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0058592665</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.027771669</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7267924" gml:id="CP.7267924">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7267924</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7267924.POINT">
                    <gml:pos>438409.5500171382 441457.41924536385</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7267924">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>438472.0 441565.0 438534.0 441457.0 438472.0 441350.0 438348.0 441350.0 438285.0 441457.0 438348.0 441565.0 438472.0 441565.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>20.3172</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>149.49396</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.162068</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.025471553</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.094859056</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7076950" gml:id="CP.7076950">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7076950</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7076950.POINT">
                    <gml:pos>446040.5098617754 429959.52097485197</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7076950">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>446103.0 430067.0 446165.0 429960.0 446103.0 429852.0 445978.0 429852.0 445916.0 429960.0 445978.0 430067.0 446103.0 430067.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.78086</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>47.338753</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05420783</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0060817483</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.031708803</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7084076" gml:id="CP.7084076">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7084076</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7084076.POINT">
                    <gml:pos>440829.12265080365 430389.34894758137</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7084076">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>440891.0 430497.0 440953.0 430389.0 440891.0 430282.0 440767.0 430282.0 440705.0 430389.0 440767.0 430497.0 440891.0 430497.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>14.8915</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>103.37679</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1138829</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.015913796</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.06637662</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7271470" gml:id="CP.7271470">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7271470</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7271470.POINT">
                    <gml:pos>429475.743369758 441672.3332317286</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7271470">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>429538.0 441780.0 429600.0 441672.0 429538.0 441565.0 429414.0 441565.0 429352.0 441672.0 429414.0 441780.0 429538.0 441780.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>11.4622</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>58.41137</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0764948</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.00603015</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.044913974</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7271471" gml:id="CP.7271471">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7271471</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7271471.POINT">
                    <gml:pos>429847.9853133989 441672.3332317286</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7271471">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>429910.0 441780.0 429972.0 441672.0 429910.0 441565.0 429786.0 441565.0 429724.0 441672.0 429786.0 441780.0 429910.0 441780.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>11.9568</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>60.931854</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0803606</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.006335036</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.047184836</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7271469" gml:id="CP.7271469">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7271469</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7271469.POINT">
                    <gml:pos>429103.5014261172 441672.3332317286</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7271469">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>429166.0 441780.0 429228.0 441672.0 429166.0 441565.0 429041.0 441565.0 428979.0 441672.0 429041.0 441780.0 429166.0 441780.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>10.9618</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>70.39668</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0726416</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.008998982</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.042653244</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7278633" gml:id="CP.7278633">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7278633</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7278633.POINT">
                    <gml:pos>438037.3080734973 442102.161204458</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7278633">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>438099.0 442210.0 438161.0 442102.0 438099.0 441995.0 437975.0 441995.0 437913.0 442102.0 437975.0 442210.0 438099.0 442210.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>18.209</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>99.42114</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1382623</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.013219521</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.081086434</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7278632" gml:id="CP.7278632">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7278632</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7278632.POINT">
                    <gml:pos>437665.0661298565 442102.161204458</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7278632">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>437727.0 442210.0 437789.0 442102.0 437727.0 441995.0 437603.0 441995.0 437541.0 442102.0 437603.0 442210.0 437727.0 442210.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>18.5069</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>107.30301</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1367617</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.01462593</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.080269635</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7084075" gml:id="CP.7084075">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7084075</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7084075.POINT">
                    <gml:pos>440456.8807071628 430389.34894758137</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7084075">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>440519.0 430497.0 440581.0 430389.0 440519.0 430282.0 440395.0 430282.0 440333.0 430389.0 440395.0 430497.0 440519.0 430497.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>16.08</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>102.4296</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1222796</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.015042617</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.071298786</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7271462" gml:id="CP.7271462">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7271462</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7271462.POINT">
                    <gml:pos>426497.8078206313 441672.3332317286</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7271462">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>426560.0 441780.0 426622.0 441672.0 426560.0 441565.0 426436.0 441565.0 426374.0 441672.0 426436.0 441780.0 426560.0 441780.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.74383</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>57.78446</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0499501</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.007882124</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.029353954</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7271461" gml:id="CP.7271461">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7271461</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7271461.POINT">
                    <gml:pos>426125.56587699044 441672.3332317286</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7271461">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>426188.0 441780.0 426250.0 441672.0 426188.0 441565.0 426064.0 441565.0 426001.0 441672.0 426064.0 441780.0 426188.0 441780.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.40081</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>43.102318</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04738899</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.005075816</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.027856955</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7275044" gml:id="CP.7275044">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7275044</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7275044.POINT">
                    <gml:pos>430964.71114432137 441887.2472180933</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7275044">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>431027.0 441995.0 431089.0 441887.0 431027.0 441780.0 430903.0 441780.0 430841.0 441887.0 430903.0 441995.0 431027.0 441995.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>12.8794</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>91.752846</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0895518</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.01311181</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.052586067</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7275043" gml:id="CP.7275043">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7275043</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7275043.POINT">
                    <gml:pos>430592.46920068056 441887.2472180933</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7275043">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>430655.0 441995.0 430717.0 441887.0 430655.0 441780.0 430530.0 441780.0 430468.0 441887.0 430530.0 441995.0 430655.0 441995.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>12.555</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>72.14103</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0860212</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.008721912</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.05052666</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7275042" gml:id="CP.7275042">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7275042</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7275042.POINT">
                    <gml:pos>430220.2272570397 441887.2472180933</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7275042">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>430282.0 441995.0 430344.0 441887.0 430282.0 441780.0 430158.0 441780.0 430096.0 441887.0 430158.0 441995.0 430282.0 441995.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>12.1754</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>73.758575</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0824112</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.009283495</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.048401956</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7275041" gml:id="CP.7275041">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7275041</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7275041.POINT">
                    <gml:pos>429847.9853133989 441887.2472180933</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7275041">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>429910.0 441995.0 429972.0 441887.0 429910.0 441780.0 429786.0 441780.0 429724.0 441887.0 429786.0 441995.0 429910.0 441995.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>11.7499</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>59.87749</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0788188</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.006215108</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.046291586</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7275040" gml:id="CP.7275040">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7275040</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7275040.POINT">
                    <gml:pos>429475.743369758 441887.2472180933</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7275040">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>429538.0 441995.0 429600.0 441887.0 429538.0 441780.0 429414.0 441780.0 429352.0 441887.0 429414.0 441995.0 429538.0 441995.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>11.3012</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>57.590916</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0752001</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.005929541</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.044164613</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7267902" gml:id="CP.7267902">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7267902</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7267902.POINT">
                    <gml:pos>430220.2272570397 441457.41924536385</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7267902">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>430282.0 441565.0 430344.0 441457.0 430282.0 441350.0 430158.0 441350.0 430096.0 441457.0 430158.0 441565.0 430282.0 441565.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>12.6792</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>73.84366</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0859763</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.008712077</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.05046968</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7267903" gml:id="CP.7267903">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7267903</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7267903.POINT">
                    <gml:pos>430592.46920068056 441457.41924536385</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7267903">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>430655.0 441565.0 430717.0 441457.0 430655.0 441350.0 430530.0 441350.0 430468.0 441457.0 430530.0 441565.0 430655.0 441565.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>13.1776</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>73.66278</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.090183</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.008631567</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.052944653</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7267900" gml:id="CP.7267900">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7267900</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7267900.POINT">
                    <gml:pos>429475.743369758 441457.41924536385</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7267900">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>429538.0 441565.0 429600.0 441457.0 429538.0 441350.0 429414.0 441350.0 429352.0 441457.0 429414.0 441565.0 429538.0 441565.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>11.6125</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>72.16007</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.077724</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.009188433</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.045625072</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7267901" gml:id="CP.7267901">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7267901</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7267901.POINT">
                    <gml:pos>429847.9853133989 441457.41924536385</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7267901">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>429910.0 441565.0 429972.0 441457.0 429910.0 441350.0 429786.0 441350.0 429724.0 441457.0 429786.0 441565.0 429910.0 441565.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>12.1522</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>63.823353</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0818349</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0069104205</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.04803907</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7275066" gml:id="CP.7275066">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7275066</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7275066.POINT">
                    <gml:pos>439154.03390441986 441887.2472180933</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7275066">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>439216.0 441995.0 439278.0 441887.0 439216.0 441780.0 439092.0 441780.0 439030.0 441887.0 439092.0 441995.0 439216.0 441995.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>18.2925</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>111.29157</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1478532</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.016592635</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.08651009</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7080505" gml:id="CP.7080505">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7080505</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7080505.POINT">
                    <gml:pos>440456.8807071628 430174.4349612167</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7080505">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>440519.0 430282.0 440581.0 430174.0 440519.0 430067.0 440395.0 430067.0 440333.0 430174.0 440395.0 430282.0 440519.0 430282.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>15.9138</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>112.956154</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1201229</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.017468898</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.07006054</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7267899" gml:id="CP.7267899">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7267899</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7267899.POINT">
                    <gml:pos>429103.5014261172 441457.41924536385</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7267899">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>429166.0 441565.0 429228.0 441457.0 429166.0 441350.0 429041.0 441350.0 428979.0 441457.0 429041.0 441565.0 429166.0 441565.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>11.0675</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>61.291817</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0735641</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0070406785</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.0431864</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7275065" gml:id="CP.7275065">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7275065</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7275065.POINT">
                    <gml:pos>438781.791960779 441887.2472180933</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7275065">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>438844.0 441995.0 438906.0 441887.0 438844.0 441780.0 438720.0 441780.0 438658.0 441887.0 438720.0 441995.0 438844.0 441995.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>18.5208</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>98.715866</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1479553</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.013286163</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.08658866</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7080506" gml:id="CP.7080506">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7080506</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7080506.POINT">
                    <gml:pos>440829.12265080365 430174.4349612167</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7080506">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>440891.0 430282.0 440953.0 430174.0 440891.0 430067.0 440767.0 430067.0 440705.0 430174.0 440767.0 430282.0 440891.0 430282.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>14.7653</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>80.61854</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1123283</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.010675924</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.06548441</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7084091" gml:id="CP.7084091">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7084091</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7084091.POINT">
                    <gml:pos>446412.7518054162 430389.34894758137</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7084091">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>446475.0 430497.0 446537.0 430389.0 446475.0 430282.0 446351.0 430282.0 446289.0 430389.0 446351.0 430497.0 446475.0 430497.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.43773</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>41.963673</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05179241</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0052222386</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.030252801</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7267893" gml:id="CP.7267893">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7267893</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7267893.POINT">
                    <gml:pos>426870.0497642721 441457.41924536385</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7267893">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>426932.0 441565.0 426994.0 441457.0 426932.0 441350.0 426808.0 441350.0 426746.0 441457.0 426808.0 441565.0 426932.0 441565.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.09483</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>60.193157</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05250478</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.008283971</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.030850481</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7271474" gml:id="CP.7271474">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7271474</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7271474.POINT">
                    <gml:pos>430964.71114432137 441672.3332317286</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7271474">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>431027.0 441780.0 431089.0 441672.0 431027.0 441565.0 430903.0 441565.0 430841.0 441672.0 430903.0 441780.0 431027.0 441780.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>13.2525</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>100.26842</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0919294</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.015009618</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.05397008</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7267891" gml:id="CP.7267891">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7267891</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7267891.POINT">
                    <gml:pos>426125.56587699044 441457.41924536385</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7267891">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>426188.0 441565.0 426250.0 441457.0 426188.0 441350.0 426064.0 441350.0 426001.0 441457.0 426064.0 441565.0 426188.0 441565.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.37818</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>50.83566</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04737389</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0066759516</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.027845472</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7271472" gml:id="CP.7271472">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7271472</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7271472.POINT">
                    <gml:pos>430220.2272570397 441672.3332317286</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7271472">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>430282.0 441780.0 430344.0 441672.0 430282.0 441565.0 430158.0 441565.0 430096.0 441672.0 430158.0 441780.0 430282.0 441780.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>12.4326</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>63.35653</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0842481</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0066414247</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.04946689</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7271473" gml:id="CP.7271473">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7271473</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7271473.POINT">
                    <gml:pos>430592.46920068056 441672.3332317286</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7271473">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>430655.0 441780.0 430717.0 441672.0 430655.0 441565.0 430530.0 441565.0 430468.0 441672.0 430530.0 441780.0 430655.0 441780.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>12.8702</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>65.58654</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.088077</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0069441725</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.051721826</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7285772" gml:id="CP.7285772">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7285772</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7285772.POINT">
                    <gml:pos>437665.0661298565 442531.98917718744</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7285772">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>437727.0 442639.0 437789.0 442532.0 437727.0 442425.0 437603.0 442425.0 437541.0 442532.0 437603.0 442639.0 437727.0 442639.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>17.2526</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>120.21612</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1256514</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.017689303</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.07378228</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7032328" gml:id="CP.7032328">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7032328</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7032328.POINT">
                    <gml:pos>447343.35666451836 427273.09614529315</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7032328">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>447405.0 427381.0 447467.0 427273.0 447405.0 427166.0 447281.0 427166.0 447219.0 427273.0 447281.0 427381.0 447405.0 427381.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.31876</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>35.81473</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04482533</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.004233379</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.02596687</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7028749" gml:id="CP.7028749">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7028749</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7028749.POINT">
                    <gml:pos>443993.1791717508 427058.1821589284</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7028749">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>444055.0 427166.0 444117.0 427058.0 444055.0 426951.0 443931.0 426951.0 443869.0 427058.0 443931.0 427166.0 444055.0 427166.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.45884</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>49.484215</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05964294</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.006335721</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.03477153</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7028748" gml:id="CP.7028748">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7028748</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7028748.POINT">
                    <gml:pos>443620.9372281099 427058.1821589284</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7028748">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>443683.0 427166.0 443745.0 427058.0 443683.0 426951.0 443559.0 426951.0 443497.0 427058.0 443559.0 427166.0 443683.0 427166.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.81066</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>56.811134</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.06198804</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0076202275</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.036118247</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7032320" gml:id="CP.7032320">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7032320</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7032320.POINT">
                    <gml:pos>444365.4211153916 427273.09614529315</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7032320">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>444427.0 427381.0 444490.0 427273.0 444427.0 427166.0 444303.0 427166.0 444241.0 427273.0 444303.0 427381.0 444427.0 427381.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.16835</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>50.758125</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05775184</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0067896727</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.033714052</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7032327" gml:id="CP.7032327">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7032327</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7032327.POINT">
                    <gml:pos>446971.1147208775 427273.09614529315</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7032327">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>447033.0 427381.0 447095.0 427273.0 447033.0 427166.0 446909.0 427166.0 446847.0 427273.0 446909.0 427381.0 447033.0 427381.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.50055</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>33.971874</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04611565</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0038583083</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.026821747</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7032326" gml:id="CP.7032326">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7032326</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7032326.POINT">
                    <gml:pos>446598.8727772366 427273.09614529315</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7032326">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>446661.0 427381.0 446723.0 427273.0 446661.0 427166.0 446537.0 427166.0 446475.0 427273.0 446537.0 427381.0 446661.0 427381.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.69083</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>39.141354</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04743017</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.004771978</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.02764441</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7275039" gml:id="CP.7275039">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7275039</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7275039.POINT">
                    <gml:pos>429103.5014261172 441887.2472180933</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7275039">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>429166.0 441995.0 429228.0 441887.0 429166.0 441780.0 429041.0 441780.0 428979.0 441887.0 429041.0 441995.0 429166.0 441995.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>10.8395</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>74.96598</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.071602</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.010082002</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.042052146</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7025179" gml:id="CP.7025179">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7025179</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7025179.POINT">
                    <gml:pos>443993.1791717508 426843.2681725637</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7025179">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>444055.0 426951.0 444117.0 426843.0 444055.0 426736.0 443931.0 426736.0 443869.0 426843.0 443931.0 426951.0 444055.0 426951.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.42193</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>45.983738</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05932826</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0052796137</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.034408327</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7087645" gml:id="CP.7087645">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7087645</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7087645.POINT">
                    <gml:pos>440456.8807071628 430604.2629339461</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7087645">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>440519.0 430712.0 440581.0 430604.0 440519.0 430497.0 440395.0 430497.0 440333.0 430604.0 440395.0 430712.0 440519.0 430712.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>16.2377</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>100.05671</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1241653</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.014577025</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.07238207</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7025178" gml:id="CP.7025178">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7025178</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7025178.POINT">
                    <gml:pos>443620.9372281099 426843.2681725637</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7025178">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>443683.0 426951.0 443745.0 426843.0 443683.0 426736.0 443559.0 426736.0 443497.0 426843.0 443559.0 426951.0 443683.0 426951.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.76923</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>49.475994</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0614946</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0061382195</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.035559144</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7087646" gml:id="CP.7087646">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7087646</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7087646.POINT">
                    <gml:pos>440829.12265080365 430604.2629339461</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7087646">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>440891.0 430712.0 440953.0 430604.0 440891.0 430497.0 440767.0 430497.0 440705.0 430604.0 440767.0 430712.0 440891.0 430712.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>15.0127</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>103.4375</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1152099</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0160967</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.06713952</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7282203" gml:id="CP.7282203">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7282203</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7282203.POINT">
                    <gml:pos>438037.3080734973 442317.0751908227</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7282203">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>438099.0 442425.0 438161.0 442317.0 438099.0 442210.0 437975.0 442210.0 437913.0 442317.0 437975.0 442425.0 438099.0 442425.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>17.5498</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>106.31669</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1322254</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.014876838</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.07756433</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7282202" gml:id="CP.7282202">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7282202</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7282202.POINT">
                    <gml:pos>437665.0661298565 442317.0751908227</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7282202">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>437727.0 442425.0 437789.0 442317.0 437727.0 442210.0 437603.0 442210.0 437541.0 442317.0 437603.0 442425.0 437727.0 442425.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>17.8569</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>125.81972</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1309563</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.019169537</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.07688112</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7351820" gml:id="CP.7351820">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7351820</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7351820.POINT">
                    <gml:pos>438595.67098895856 446507.8979249345</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7351820">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>438658.0 446615.0 438720.0 446508.0 438658.0 446400.0 438534.0 446400.0 438472.0 446508.0 438534.0 446615.0 438658.0 446615.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>9.99871</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>61.612053</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.069261</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.007808041</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.040709287</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7278611" gml:id="CP.7278611">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7278611</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7278611.POINT">
                    <gml:pos>429847.9853133989 442102.161204458</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7278611">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>429910.0 442210.0 429972.0 442102.0 429910.0 441995.0 429786.0 441995.0 429724.0 442102.0 429786.0 442210.0 429910.0 442210.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>11.5376</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>70.794716</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0772224</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.008710691</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.045415487</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7278610" gml:id="CP.7278610">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7278610</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7278610.POINT">
                    <gml:pos>429475.743369758 442102.161204458</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7278610">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>429538.0 442210.0 429600.0 442102.0 429538.0 441995.0 429414.0 441995.0 429352.0 442102.0 429414.0 442210.0 429538.0 442210.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>11.1301</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>70.60935</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0738618</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.009164207</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.04343638</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7278609" gml:id="CP.7278609">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7278609</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7278609.POINT">
                    <gml:pos>429103.5014261172 442102.161204458</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7278609">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>429166.0 442210.0 429228.0 442102.0 429166.0 441995.0 429041.0 441995.0 428979.0 442102.0 429041.0 442210.0 429166.0 442210.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>10.7047</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>60.95256</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0705009</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0071567334</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.041459467</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7028757" gml:id="CP.7028757">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7028757</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7028757.POINT">
                    <gml:pos>446971.1147208775 427058.1821589284</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7028757">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>447033.0 427166.0 447095.0 427058.0 447033.0 426951.0 446909.0 426951.0 446847.0 427058.0 446909.0 427166.0 447033.0 427166.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.46659</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>39.510864</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0459365</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.005124439</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.026717618</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7037675" gml:id="CP.7037675">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7037675</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7037675.POINT">
                    <gml:pos>444179.30014357116 427595.4671248402</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7037675">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>444241.0 427703.0 444303.0 427595.0 444241.0 427488.0 444117.0 427488.0 444055.0 427595.0 444117.0 427703.0 444241.0 427703.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.38916</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>42.969276</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05943072</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.004657788</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.034692302</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7037674" gml:id="CP.7037674">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7037674</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7037674.POINT">
                    <gml:pos>443807.05819993035 427595.4671248402</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7037674">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>443869.0 427703.0 443931.0 427595.0 443869.0 427488.0 443745.0 427488.0 443683.0 427595.0 443745.0 427703.0 443869.0 427703.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.73342</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>45.867924</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.06198988</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0052029043</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.036168955</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7037673" gml:id="CP.7037673">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7037673</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7037673.POINT">
                    <gml:pos>443434.8162562895 427595.4671248402</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7037673">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>443497.0 427703.0 443559.0 427595.0 443497.0 427488.0 443373.0 427488.0 443311.0 427595.0 443373.0 427703.0 443497.0 427703.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>9.1152</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>53.79791</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.06500461</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.006910798</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.037927657</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7100143" gml:id="CP.7100143">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7100143</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7100143.POINT">
                    <gml:pos>441759.72750990576 431356.4618862226</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7100143">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>441822.0 431464.0 441884.0 431356.0 441822.0 431249.0 441698.0 431249.0 441636.0 431356.0 441698.0 431464.0 441822.0 431464.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>13.1641</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>73.245056</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0990358</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.009403379</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.057678822</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7037679" gml:id="CP.7037679">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7037679</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7037679.POINT">
                    <gml:pos>445668.2679181345 427595.4671248402</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7037679">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>445730.0 427703.0 445792.0 427595.0 445730.0 427488.0 445606.0 427488.0 445544.0 427595.0 445606.0 427703.0 445730.0 427703.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.28188</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>49.793495</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05145148</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0069149695</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.030044185</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7037678" gml:id="CP.7037678">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7037678</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7037678.POINT">
                    <gml:pos>445296.0259744937 427595.4671248402</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7037678">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>445358.0 427703.0 445420.0 427595.0 445358.0 427488.0 445234.0 427488.0 445172.0 427595.0 445234.0 427703.0 445358.0 427703.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.5235</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>50.467636</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05315341</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0071434043</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.031036688</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7037677" gml:id="CP.7037677">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7037677</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7037677.POINT">
                    <gml:pos>444923.78403085284 427595.4671248402</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7037677">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>444986.0 427703.0 445048.0 427595.0 444986.0 427488.0 444862.0 427488.0 444800.0 427595.0 444862.0 427703.0 444986.0 427703.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.78599</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>45.548042</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05501573</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.005544118</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.03211747</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7037676" gml:id="CP.7037676">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7037676</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7037676.POINT">
                    <gml:pos>444551.54208721203 427595.4671248402</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7037676">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>444614.0 427703.0 444676.0 427595.0 444614.0 427488.0 444490.0 427488.0 444427.0 427595.0 444490.0 427703.0 444614.0 427703.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.07336</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>48.068787</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05707417</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0060708</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.033317603</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7041251" gml:id="CP.7041251">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7041251</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7041251.POINT">
                    <gml:pos>446412.7518054162 427810.3811112049</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7041251">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>446475.0 427918.0 446537.0 427810.0 446475.0 427703.0 446351.0 427703.0 446289.0 427810.0 446351.0 427918.0 446475.0 427918.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.88676</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>41.182823</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04860533</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0051632077</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.028336575</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7041250" gml:id="CP.7041250">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7041250</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7041250.POINT">
                    <gml:pos>446040.5098617754 427810.3811112049</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7041250">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>446103.0 427918.0 446165.0 427810.0 446103.0 427703.0 445978.0 427703.0 445916.0 427810.0 445978.0 427918.0 446103.0 427918.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.09927</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>36.731625</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05007222</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.004201176</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.029205255</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7041249" gml:id="CP.7041249">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7041249</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7041249.POINT">
                    <gml:pos>445668.2679181345 427810.3811112049</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7041249">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>445730.0 427918.0 445792.0 427810.0 445730.0 427703.0 445606.0 427703.0 445544.0 427810.0 445606.0 427918.0 445730.0 427918.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.32568</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>41.71242</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0516693</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0052086925</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.03017433</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7041248" gml:id="CP.7041248">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7041248</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7041248.POINT">
                    <gml:pos>445296.0259744937 427810.3811112049</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7041248">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>445358.0 427918.0 445420.0 427810.0 445358.0 427703.0 445234.0 427703.0 445172.0 427810.0 445234.0 427918.0 445358.0 427918.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.56788</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>47.813866</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05337338</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.006575771</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.031167747</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7103713" gml:id="CP.7103713">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7103713</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7103713.POINT">
                    <gml:pos>441759.72750990576 431571.3758725873</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7103713">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>441822.0 431679.0 441884.0 431571.0 441822.0 431464.0 441698.0 431464.0 441636.0 431571.0 441698.0 431679.0 441822.0 431679.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>13.3437</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>78.40758</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0998322</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.010597011</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.05815823</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7041252" gml:id="CP.7041252">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7041252</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7041252.POINT">
                    <gml:pos>446784.99374905706 427810.3811112049</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7041252">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>446847.0 427918.0 446909.0 427810.0 446847.0 427703.0 446723.0 427703.0 446661.0 427810.0 446723.0 427918.0 446847.0 427918.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.68564</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>40.32778</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04722035</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0050163716</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.027530715</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7034107" gml:id="CP.7034107">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7034107</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7034107.POINT">
                    <gml:pos>444923.78403085284 427380.55313847546</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7034107">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>444986.0 427488.0 445048.0 427381.0 444986.0 427273.0 444862.0 427273.0 444800.0 427381.0 444862.0 427488.0 444986.0 427488.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.74436</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>45.908566</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05478257</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0058271135</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.031980205</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7034106" gml:id="CP.7034106">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7034106</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7034106.POINT">
                    <gml:pos>444551.54208721203 427380.55313847546</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7034106">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>444614.0 427488.0 444676.0 427381.0 444614.0 427273.0 444490.0 427273.0 444427.0 427381.0 444490.0 427488.0 444614.0 427488.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.0327</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>46.155895</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05681135</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.005724767</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.033163987</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7034105" gml:id="CP.7034105">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7034105</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7034105.POINT">
                    <gml:pos>444179.30014357116 427380.55313847546</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7034105">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>444241.0 427488.0 444303.0 427381.0 444241.0 427273.0 444117.0 427273.0 444055.0 427381.0 444117.0 427488.0 444241.0 427488.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.34876</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>45.801296</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05902608</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.005617825</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.034458842</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7034104" gml:id="CP.7034104">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7034104</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7034104.POINT">
                    <gml:pos>443807.05819993035 427380.55313847546</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7034104">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>443869.0 427488.0 443931.0 427381.0 443869.0 427273.0 443745.0 427273.0 443683.0 427381.0 443745.0 427488.0 443869.0 427488.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.69166</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>44.518684</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.06150169</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0048183664</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.035888325</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7034111" gml:id="CP.7034111">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7034111</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7034111.POINT">
                    <gml:pos>446412.7518054162 427380.55313847546</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7034111">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>446475.0 427488.0 446537.0 427381.0 446475.0 427273.0 446351.0 427273.0 446289.0 427381.0 446351.0 427488.0 446475.0 427488.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.80871</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>43.194458</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0482185</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0056599923</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.028104635</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7034110" gml:id="CP.7034110">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7034110</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7034110.POINT">
                    <gml:pos>446040.5098617754 427380.55313847546</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7034110">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>446103.0 427488.0 446165.0 427381.0 446103.0 427273.0 445978.0 427273.0 445916.0 427381.0 445978.0 427488.0 446103.0 427488.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.01644</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>44.87715</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04967319</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.006111305</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.028966278</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7291128" gml:id="CP.7291128">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7291128</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7291128.POINT">
                    <gml:pos>437851.1871016769 442854.3601567345</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7291128">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>437913.0 442962.0 437975.0 442854.0 437913.0 442747.0 437789.0 442747.0 437727.0 442854.0 437789.0 442962.0 437913.0 442962.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>16.2421</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>100.928406</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.118638</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.014027318</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.069652505</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7037682" gml:id="CP.7037682">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7037682</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7037682.POINT">
                    <gml:pos>446784.99374905706 427595.4671248402</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7037682">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>446847.0 427703.0 446909.0 427595.0 446847.0 427488.0 446723.0 427488.0 446661.0 427595.0 446723.0 427703.0 446847.0 427703.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.64827</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>33.879585</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04703119</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0036810162</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.027417073</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7037681" gml:id="CP.7037681">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7037681</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7037681.POINT">
                    <gml:pos>446412.7518054162 427595.4671248402</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7037681">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>446475.0 427703.0 446537.0 427595.0 446475.0 427488.0 446351.0 427488.0 446289.0 427595.0 446351.0 427703.0 446475.0 427703.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.8471</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>36.851093</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04840854</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.004329841</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.028218465</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7037680" gml:id="CP.7037680">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7037680</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7037680.POINT">
                    <gml:pos>446040.5098617754 427595.4671248402</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7037680">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>446103.0 427703.0 446165.0 427595.0 446103.0 427488.0 445978.0 427488.0 445916.0 427595.0 445978.0 427703.0 446103.0 427703.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.05725</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>37.79863</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04986887</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0041836393</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.029083345</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7034103" gml:id="CP.7034103">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7034103</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7034103.POINT">
                    <gml:pos>443434.8162562895 427380.55313847546</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7034103">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>443497.0 427488.0 443559.0 427381.0 443497.0 427273.0 443373.0 427273.0 443311.0 427381.0 443373.0 427488.0 443497.0 427488.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>9.06269</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>50.42481</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.06423267</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.00611146</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.03748672</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7114442" gml:id="CP.7114442">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7114442</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7114442.POINT">
                    <gml:pos>448832.3244390817 432216.1178316814</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7114442">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>448894.0 432324.0 448956.0 432216.0 448894.0 432109.0 448770.0 432109.0 448708.0 432216.0 448770.0 432324.0 448894.0 432324.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>5.44421</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>36.94441</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.03948823</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.00531569</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.023095628</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7041247" gml:id="CP.7041247">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7041247</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7041247.POINT">
                    <gml:pos>444923.78403085284 427810.3811112049</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7041247">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>444986.0 427918.0 445048.0 427810.0 444986.0 427703.0 444862.0 427703.0 444800.0 427810.0 444862.0 427918.0 444986.0 427918.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.83028</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>49.268124</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05528379</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0064998884</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.03227513</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7041246" gml:id="CP.7041246">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7041246</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7041246.POINT">
                    <gml:pos>444551.54208721203 427810.3811112049</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7041246">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>444614.0 427918.0 444676.0 427810.0 444614.0 427703.0 444490.0 427703.0 444427.0 427810.0 444490.0 427918.0 444614.0 427918.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.11703</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>44.74107</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05741773</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0051428927</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.033517286</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7041245" gml:id="CP.7041245">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7041245</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7041245.POINT">
                    <gml:pos>444179.30014357116 427810.3811112049</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7041245">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>444241.0 427918.0 444303.0 427810.0 444241.0 427703.0 444117.0 427703.0 444055.0 427810.0 444117.0 427918.0 444241.0 427918.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.43252</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>49.330242</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05984717</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0063651092</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.034932822</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7041244" gml:id="CP.7041244">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7041244</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7041244.POINT">
                    <gml:pos>443807.05819993035 427810.3811112049</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7041244">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>443869.0 427918.0 443931.0 427810.0 443869.0 427703.0 443745.0 427703.0 443683.0 427810.0 443745.0 427918.0 443869.0 427918.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.77859</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>47.702858</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.06261529</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0056047174</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.036527097</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7121581" gml:id="CP.7121581">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7121581</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7121581.POINT">
                    <gml:pos>448460.08249544085 432645.94580441085</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7121581">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>448522.0 432753.0 448584.0 432646.0 448522.0 432538.0 448398.0 432538.0 448336.0 432646.0 448398.0 432753.0 448522.0 432753.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>5.49774</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>33.59119</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04073367</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.00456621</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.023807144</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7253671" gml:id="CP.7253671">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7253671</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7253671.POINT">
                    <gml:pos>448460.08249544085 440597.76329990505</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7253671">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>448522.0 440705.0 448584.0 440598.0 448522.0 440490.0 448398.0 440490.0 448336.0 440598.0 448398.0 440705.0 448522.0 440705.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.10921</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>38.91567</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05019507</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.006191385</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.029345838</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7118012" gml:id="CP.7118012">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7118012</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7118012.POINT">
                    <gml:pos>448832.3244390817 432431.0318180461</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7118012">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>448894.0 432538.0 448956.0 432431.0 448894.0 432324.0 448770.0 432324.0 448708.0 432431.0 448770.0 432538.0 448894.0 432538.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>5.37298</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>31.01284</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.03935964</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.004193155</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.02301276</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7118011" gml:id="CP.7118011">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7118011</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7118011.POINT">
                    <gml:pos>448460.08249544085 432431.0318180461</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7118011">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>448522.0 432538.0 448584.0 432431.0 448522.0 432324.0 448398.0 432324.0 448336.0 432431.0 448398.0 432538.0 448522.0 432538.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>5.57379</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>32.026997</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0408607</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0043529607</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.0238898</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7250100" gml:id="CP.7250100">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7250100</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7250100.POINT">
                    <gml:pos>448087.84055180004 440382.8493135403</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7250100">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>448150.0 440490.0 448212.0 440383.0 448150.0 440275.0 448026.0 440275.0 447964.0 440383.0 448026.0 440490.0 448150.0 440490.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.29161</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>36.315174</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05180849</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0055185915</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.030286985</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7062668" gml:id="CP.7062668">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7062668</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7062668.POINT">
                    <gml:pos>445296.0259744937 429099.8650293932</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7062668">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>445358.0 429207.0 445420.0 429100.0 445358.0 428992.0 445234.0 428992.0 445172.0 429100.0 445234.0 429207.0 445358.0 429207.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.54529</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>52.65608</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0597721</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.007045501</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.034984365</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7059098" gml:id="CP.7059098">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7059098</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7059098.POINT">
                    <gml:pos>445296.0259744937 428884.95104302844</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7059098">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>445358.0 428992.0 445420.0 428885.0 445358.0 428777.0 445234.0 428777.0 445172.0 428885.0 445234.0 428992.0 445358.0 428992.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.56139</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>52.310093</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05978331</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0067016375</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.034940757</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7257241" gml:id="CP.7257241">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7257241</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7257241.POINT">
                    <gml:pos>448460.08249544085 440812.6772862697</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7257241">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>448522.0 440920.0 448584.0 440813.0 448522.0 440705.0 448398.0 440705.0 448336.0 440813.0 448398.0 440920.0 448522.0 440920.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.20878</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>43.747063</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05082586</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0074099773</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.029718366</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7269710" gml:id="CP.7269710">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7269710</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7269710.POINT">
                    <gml:pos>438595.67098895856 441564.8762385462</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7269710">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>438658.0 441672.0 438720.0 441565.0 438658.0 441457.0 438534.0 441457.0 438472.0 441565.0 438534.0 441672.0 438658.0 441672.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>19.782</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>151.21361</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1586431</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.025819188</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.09283804</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7078735" gml:id="CP.7078735">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7078735</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7078735.POINT">
                    <gml:pos>446226.6308335958 430066.97796803433</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7078735">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>446289.0 430174.0 446351.0 430067.0 446289.0 429960.0 446165.0 429960.0 446103.0 430067.0 446165.0 430174.0 446289.0 430174.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.61539</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>45.93603</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05297562</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.005936449</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.030951247</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7269709" gml:id="CP.7269709">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7269709</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7269709.POINT">
                    <gml:pos>438223.42904531775 441564.8762385462</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7269709">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>438285.0 441672.0 438348.0 441565.0 438285.0 441457.0 438161.0 441457.0 438099.0 441565.0 438161.0 441672.0 438285.0 441672.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>19.997</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>115.42268</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1571726</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.016769508</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.09203397</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7266118" gml:id="CP.7266118">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7266118</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7266118.POINT">
                    <gml:pos>430406.3482288601 441349.96225218155</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7266118">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>430468.0 441457.0 430530.0 441350.0 430468.0 441243.0 430344.0 441243.0 430282.0 441350.0 430344.0 441457.0 430468.0 441457.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>13.0665</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>77.79794</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0890167</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.009520599</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.05225069</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7266119" gml:id="CP.7266119">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7266119</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7266119.POINT">
                    <gml:pos>430778.59017250093 441349.96225218155</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7266119">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>430841.0 441457.0 430903.0 441350.0 430841.0 441243.0 430717.0 441243.0 430655.0 441350.0 430717.0 441457.0 430841.0 441457.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>13.5925</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>84.11039</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0933334</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.011034705</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.054792717</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7266116" gml:id="CP.7266116">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7266116</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7266116.POINT">
                    <gml:pos>429661.86434157845 441349.96225218155</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7266116">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>429724.0 441457.0 429786.0 441350.0 429724.0 441243.0 429600.0 441243.0 429538.0 441350.0 429600.0 441457.0 429724.0 441457.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>11.9633</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>83.67132</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0803866</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.011312261</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.047183573</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7266117" gml:id="CP.7266117">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7266117</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7266117.POINT">
                    <gml:pos>430034.10628521926 441349.96225218155</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7266117">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>430096.0 441457.0 430158.0 441350.0 430096.0 441243.0 429972.0 441243.0 429910.0 441350.0 429972.0 441457.0 430096.0 441457.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>12.5227</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>76.513695</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.084661</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.00953081</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.049691394</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7082305" gml:id="CP.7082305">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7082305</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7082305.POINT">
                    <gml:pos>446226.6308335958 430281.89195439906</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7082305">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>446289.0 430389.0 446351.0 430282.0 446289.0 430174.0 446165.0 430174.0 446103.0 430282.0 446165.0 430389.0 446289.0 430389.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.59696</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>46.812466</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05295375</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0062289317</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.030929698</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7273280" gml:id="CP.7273280">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7273280</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7273280.POINT">
                    <gml:pos>438595.67098895856 441779.79022491095</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7273280">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>438658.0 441887.0 438720.0 441780.0 438658.0 441672.0 438534.0 441672.0 438472.0 441780.0 438534.0 441887.0 438658.0 441887.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>18.9941</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>101.238556</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.151253</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.013583977</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.08852957</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7273281" gml:id="CP.7273281">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7273281</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7273281.POINT">
                    <gml:pos>438967.9129325994 441779.79022491095</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7273281">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>439030.0 441887.0 439092.0 441780.0 439030.0 441672.0 438906.0 441672.0 438844.0 441780.0 438906.0 441887.0 439030.0 441887.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>18.7726</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>128.36703</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1514651</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.021247042</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.08862166</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7266139" gml:id="CP.7266139">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7266139</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7266139.POINT">
                    <gml:pos>438223.42904531775 441349.96225218155</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7266139">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>438285.0 441457.0 438348.0 441350.0 438285.0 441243.0 438161.0 441243.0 438099.0 441350.0 438161.0 441457.0 438285.0 441457.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>20.8743</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>131.3411</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1655054</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.019512543</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.09688934</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7075164" gml:id="CP.7075164">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7075164</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7075164.POINT">
                    <gml:pos>445854.38888995495 429852.0639816696</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7075164">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>445916.0 429960.0 445978.0 429852.0 445916.0 429745.0 445792.0 429745.0 445730.0 429852.0 445792.0 429960.0 445916.0 429960.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.95375</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>45.909046</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05550311</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0059197443</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.03248858</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7262549" gml:id="CP.7262549">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7262549</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7262549.POINT">
                    <gml:pos>430778.59017250093 441135.0482658168</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7262549">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>430841.0 441243.0 430903.0 441135.0 430841.0 441028.0 430717.0 441028.0 430655.0 441135.0 430717.0 441243.0 430841.0 441243.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>13.932</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>97.44041</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0955127</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.013440258</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.05605947</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7269678" gml:id="CP.7269678">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7269678</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7269678.POINT">
                    <gml:pos>426683.9287924517 441564.8762385462</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7269678">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>426746.0 441672.0 426808.0 441565.0 426746.0 441457.0 426622.0 441457.0 426560.0 441565.0 426622.0 441672.0 426746.0 441672.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.91535</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>55.977356</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05120072</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0075016497</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.030086027</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7276847" gml:id="CP.7276847">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7276847</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7276847.POINT">
                    <gml:pos>437478.9451580361 441994.7042112757</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7276847">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>437541.0 442102.0 437603.0 441995.0 437541.0 441887.0 437417.0 441887.0 437355.0 441995.0 437417.0 442102.0 437541.0 442102.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>19.0743</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>117.0399</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1393023</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.016466904</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.08176625</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7269677" gml:id="CP.7269677">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7269677</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7269677.POINT">
                    <gml:pos>426311.6868488109 441564.8762385462</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7269677">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>426374.0 441672.0 426436.0 441565.0 426374.0 441457.0 426250.0 441457.0 426188.0 441565.0 426250.0 441672.0 426374.0 441672.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.5583</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>51.09411</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0486345</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.006578839</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.028583763</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7273258" gml:id="CP.7273258">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7273258</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7273258.POINT">
                    <gml:pos>430406.3482288601 441779.79022491095</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7273258">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>430468.0 441887.0 430530.0 441780.0 430468.0 441672.0 430344.0 441672.0 430282.0 441780.0 430344.0 441887.0 430468.0 441887.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>12.5157</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>63.780006</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.085216</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0067189573</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.05004437</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7273259" gml:id="CP.7273259">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7273259</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7273259.POINT">
                    <gml:pos>430778.59017250093 441779.79022491095</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7273259">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>430841.0 441887.0 430903.0 441780.0 430841.0 441672.0 430717.0 441672.0 430655.0 441780.0 430717.0 441887.0 430841.0 441887.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>12.8958</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>81.14037</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.088883</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.01051348</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.05220458</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7273256" gml:id="CP.7273256">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7273256</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7273256.POINT">
                    <gml:pos>429661.86434157845 441779.79022491095</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7273256">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>429724.0 441887.0 429786.0 441780.0 429724.0 441672.0 429600.0 441672.0 429538.0 441780.0 429600.0 441887.0 429724.0 441887.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>11.6195</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>59.21297</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.077722</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.006127674</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.045640357</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7273257" gml:id="CP.7273257">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7273257</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7273257.POINT">
                    <gml:pos>430034.10628521926 441779.79022491095</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7273257">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>430096.0 441887.0 430158.0 441780.0 430096.0 441672.0 429972.0 441672.0 429910.0 441780.0 429972.0 441887.0 430096.0 441887.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>12.0834</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>61.577007</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0814478</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0064214813</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.0478287</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7085860" gml:id="CP.7085860">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7085860</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7085860.POINT">
                    <gml:pos>440643.0016789832 430496.80594076373</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7085860">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>440705.0 430604.0 440767.0 430497.0 440705.0 430389.0 440581.0 430389.0 440519.0 430497.0 440581.0 430604.0 440705.0 430604.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>15.5373</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>97.76069</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.118867</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.013953581</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.06928637</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7273254" gml:id="CP.7273254">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7273254</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7273254.POINT">
                    <gml:pos>428917.38045429677 441779.79022491095</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7273254">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>428979.0 441887.0 429041.0 441780.0 428979.0 441672.0 428855.0 441672.0 428793.0 441780.0 428855.0 441887.0 428979.0 441887.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>10.6585</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>60.6895</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0702515</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0071199406</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.041246325</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7085861" gml:id="CP.7085861">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7085861</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7085861.POINT">
                    <gml:pos>441015.2436226241 430496.80594076373</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7085861">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>441077.0 430604.0 441139.0 430497.0 441077.0 430389.0 440953.0 430389.0 440891.0 430497.0 440953.0 430604.0 441077.0 430604.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>14.407</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>100.0134</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1104407</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.015406729</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.064261645</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7273255" gml:id="CP.7273255">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7273255</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7273255.POINT">
                    <gml:pos>429289.6223979376 441779.79022491095</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7273255">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>429352.0 441887.0 429414.0 441780.0 429352.0 441672.0 429228.0 441672.0 429166.0 441780.0 429228.0 441887.0 429352.0 441887.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>11.1434</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>57.94568</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0740082</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0062515996</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.043459155</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7280418" gml:id="CP.7280418">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7280418</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7280418.POINT">
                    <gml:pos>437851.1871016769 442209.61819764035</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7280418">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>437913.0 442317.0 437975.0 442210.0 437913.0 442102.0 437789.0 442102.0 437727.0 442210.0 437789.0 442317.0 437913.0 442317.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>18.0014</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>117.0091</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1343316</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.017384345</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.07881554</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7273248" gml:id="CP.7273248">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7273248</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7273248.POINT">
                    <gml:pos>426683.9287924517 441779.79022491095</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7273248">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>426746.0 441887.0 426808.0 441780.0 426746.0 441672.0 426622.0 441672.0 426560.0 441780.0 426622.0 441887.0 426746.0 441887.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.93461</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>61.06476</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05131203</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.008385353</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.03015121</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7280417" gml:id="CP.7280417">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7280417</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7280417.POINT">
                    <gml:pos>437478.9451580361 442209.61819764035</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7280417">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>437541.0 442317.0 437603.0 442210.0 437541.0 442102.0 437417.0 442102.0 437355.0 442210.0 437417.0 442317.0 437541.0 442317.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>18.4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>115.7728</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1336162</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.015803581</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.07847252</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7269690" gml:id="CP.7269690">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7269690</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7269690.POINT">
                    <gml:pos>431150.8321161418 441564.8762385462</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7269690">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>431213.0 441672.0 431275.0 441565.0 431213.0 441457.0 431089.0 441457.0 431027.0 441565.0 431089.0 441672.0 431213.0 441672.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>13.6456</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>101.1139</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0950554</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.014971892</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.055757083</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7266107" gml:id="CP.7266107">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7266107</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7266107.POINT">
                    <gml:pos>426311.6868488109 441349.96225218155</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7266107">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>426374.0 441457.0 426436.0 441350.0 426374.0 441243.0 426250.0 441243.0 426188.0 441350.0 426250.0 441457.0 426374.0 441457.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.53494</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>40.944862</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04850564</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.004374261</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.02850796</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7269688" gml:id="CP.7269688">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7269688</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7269688.POINT">
                    <gml:pos>430406.3482288601 441564.8762385462</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7269688">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>430468.0 441672.0 430530.0 441565.0 430468.0 441457.0 430344.0 441457.0 430282.0 441565.0 430344.0 441672.0 430468.0 441672.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>12.7962</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>68.536446</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0871094</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.007847532</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.051143978</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7269689" gml:id="CP.7269689">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7269689</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7269689.POINT">
                    <gml:pos>430778.59017250093 441564.8762385462</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7269689">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>430841.0 441672.0 430903.0 441565.0 430841.0 441457.0 430717.0 441457.0 430655.0 441565.0 430717.0 441672.0 430841.0 441672.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>13.2398</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>80.89518</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0911512</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.010265767</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.053523287</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7269686" gml:id="CP.7269686">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7269686</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7269686.POINT">
                    <gml:pos>429661.86434157845 441564.8762385462</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7269686">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>429724.0 441672.0 429786.0 441565.0 429724.0 441457.0 429600.0 441457.0 429538.0 441565.0 429600.0 441672.0 429724.0 441672.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>11.7986</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>65.03388</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0791305</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0071281516</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.04645563</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7269687" gml:id="CP.7269687">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7269687</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7269687.POINT">
                    <gml:pos>430034.10628521926 441564.8762385462</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7269687">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>430096.0 441672.0 430158.0 441565.0 430096.0 441457.0 429972.0 441457.0 429910.0 441565.0 429972.0 441672.0 430096.0 441672.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>12.3089</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>64.64634</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0831124</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0070189065</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.048793234</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7269684" gml:id="CP.7269684">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7269684</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7269684.POINT">
                    <gml:pos>428917.38045429677 441564.8762385462</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7269684">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>428979.0 441672.0 429041.0 441565.0 428979.0 441457.0 428855.0 441457.0 428793.0 441565.0 428855.0 441672.0 428979.0 441672.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>10.7535</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>67.94061</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0710946</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.00880488</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.041733243</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7269685" gml:id="CP.7269685">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7269685</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7269685.POINT">
                    <gml:pos>429289.6223979376 441564.8762385462</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7269685">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>429352.0 441672.0 429414.0 441565.0 429352.0 441457.0 429228.0 441457.0 429166.0 441565.0 429228.0 441672.0 429352.0 441672.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>11.2778</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>71.25314</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0751074</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.008880356</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.04409532</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7082290" gml:id="CP.7082290">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7082290</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7082290.POINT">
                    <gml:pos>440643.0016789832 430281.89195439906</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7082290">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>440705.0 430389.0 440767.0 430282.0 440705.0 430174.0 440581.0 430174.0 440519.0 430282.0 440581.0 430389.0 440705.0 430389.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>15.3971</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>107.68732</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1171517</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.016375313</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.06830162</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7350034" gml:id="CP.7350034">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7350034</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7350034.POINT">
                    <gml:pos>438409.5500171382 446400.44093175215</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7350034">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>438472.0 446508.0 438534.0 446400.0 438472.0 446293.0 438348.0 446293.0 438285.0 446400.0 438348.0 446508.0 438472.0 446508.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>10.1765</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>67.73479</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0699111</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.009066601</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.041105323</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7280395" gml:id="CP.7280395">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7280395</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7280395.POINT">
                    <gml:pos>429289.6223979376 442209.61819764035</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7280395">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>429352.0 442317.0 429414.0 442210.0 429352.0 442102.0 429228.0 442102.0 429166.0 442210.0 429228.0 442317.0 429352.0 442317.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>10.8388</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>66.78869</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0715602</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.008476183</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.0420884</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7030543" gml:id="CP.7030543">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7030543</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7030543.POINT">
                    <gml:pos>447157.2356926979 427165.6391521108</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7030543">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>447219.0 427273.0 447281.0 427166.0 447219.0 427058.0 447095.0 427058.0 447033.0 427166.0 447095.0 427273.0 447219.0 427273.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.3923</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>38.225952</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04537721</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0047892244</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.026284091</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7030542" gml:id="CP.7030542">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7030542</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7030542.POINT">
                    <gml:pos>446784.99374905706 427165.6391521108</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7030542">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>446847.0 427273.0 446909.0 427166.0 446847.0 427058.0 446723.0 427058.0 446661.0 427166.0 446723.0 427273.0 446847.0 427273.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.57709</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>40.69903</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04668003</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0052181594</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.027206253</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7287558" gml:id="CP.7287558">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7287558</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7287558.POINT">
                    <gml:pos>437851.1871016769 442639.44617036975</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7287558">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>437913.0 442747.0 437975.0 442639.0 437913.0 442532.0 437789.0 442532.0 437727.0 442639.0 437789.0 442747.0 437913.0 442747.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>16.7883</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>95.59258</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1235047</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.012513934</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.07249411</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7287557" gml:id="CP.7287557">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7287557</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7287557.POINT">
                    <gml:pos>437478.9451580361 442639.44617036975</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7287557">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>437541.0 442747.0 437603.0 442639.0 437541.0 442532.0 437417.0 442532.0 437355.0 442639.0 437417.0 442747.0 437541.0 442747.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>17.1913</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>108.61463</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1232209</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.015274164</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.07239627</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7034113" gml:id="CP.7034113">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7034113</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7034113.POINT">
                    <gml:pos>447157.2356926979 427380.55313847546</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7034113">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>447219.0 427488.0 447281.0 427381.0 447219.0 427273.0 447095.0 427273.0 447033.0 427381.0 447095.0 427488.0 447219.0 427488.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.42524</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>32.743023</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04555156</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0035428451</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.026387943</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7034112" gml:id="CP.7034112">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7034112</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7034112.POINT">
                    <gml:pos>446784.99374905706 427380.55313847546</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7034112">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>446847.0 427488.0 446909.0 427381.0 446847.0 427273.0 446723.0 427273.0 446661.0 427381.0 446723.0 427488.0 446847.0 427488.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.61239</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>33.86866</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04685177</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0036665532</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.027309349</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7030535" gml:id="CP.7030535">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7030535</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7030535.POINT">
                    <gml:pos>444179.30014357116 427165.6391521108</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7030535">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>444241.0 427273.0 444303.0 427166.0 444241.0 427058.0 444117.0 427058.0 444055.0 427166.0 444117.0 427273.0 444241.0 427273.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.31058</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>47.53652</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05869608</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0059152716</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.034267593</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7030534" gml:id="CP.7030534">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7030534</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7030534.POINT">
                    <gml:pos>443807.05819993035 427165.6391521108</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7030534">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>443869.0 427273.0 443931.0 427166.0 443869.0 427058.0 443745.0 427058.0 443683.0 427166.0 443745.0 427273.0 443869.0 427273.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.65146</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>49.71129</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.06102105</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.006147339</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.035611972</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7030533" gml:id="CP.7030533">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7030533</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7030533.POINT">
                    <gml:pos>443434.8162562895 427165.6391521108</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7030533">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>443497.0 427273.0 443559.0 427166.0 443497.0 427058.0 443373.0 427058.0 443311.0 427166.0 443373.0 427273.0 443497.0 427273.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>9.01812</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>52.756</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.06352231</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0067563807</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.037080187</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7273247" gml:id="CP.7273247">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7273247</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7273247.POINT">
                    <gml:pos>426311.6868488109 441779.79022491095</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7273247">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>426374.0 441887.0 426436.0 441780.0 426374.0 441672.0 426250.0 441672.0 426188.0 441780.0 426250.0 441887.0 426374.0 441887.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.57936</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>47.098145</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04872022</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.005766796</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.028634965</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7276829" gml:id="CP.7276829">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7276829</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7276829.POINT">
                    <gml:pos>430778.59017250093 441994.7042112757</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7276829">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>430841.0 442102.0 430903.0 441995.0 430841.0 441887.0 430717.0 441887.0 430655.0 441995.0 430717.0 442102.0 430841.0 442102.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>12.5499</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>71.45913</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0866726</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0087946635</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.050948113</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7276828" gml:id="CP.7276828">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7276828</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7276828.POINT">
                    <gml:pos>430406.3482288601 441994.7042112757</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7276828">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>430468.0 442102.0 430530.0 441995.0 430468.0 441887.0 430344.0 441887.0 430282.0 441995.0 430344.0 442102.0 430468.0 442102.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>12.225</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>73.7412</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0832234</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0093822135</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.048916653</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7353604" gml:id="CP.7353604">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7353604</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7353604.POINT">
                    <gml:pos>438409.5500171382 446615.3549181169</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7353604">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>438472.0 446723.0 438534.0 446615.0 438472.0 446508.0 438348.0 446508.0 438285.0 446615.0 438348.0 446723.0 438472.0 446723.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>9.97698</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>59.143536</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0681565</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0073028826</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.040079482</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7276827" gml:id="CP.7276827">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7276827</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7276827.POINT">
                    <gml:pos>430034.10628521926 441994.7042112757</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7276827">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>430096.0 442102.0 430158.0 441995.0 430096.0 441887.0 429972.0 441887.0 429910.0 441995.0 429972.0 442102.0 430096.0 442102.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>11.8475</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>74.85251</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0797454</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.009438827</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.0468684</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7276826" gml:id="CP.7276826">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7276826</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7276826.POINT">
                    <gml:pos>429661.86434157845 441994.7042112757</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7276826">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>429724.0 442102.0 429786.0 441995.0 429724.0 441887.0 429600.0 441887.0 429538.0 441995.0 429600.0 442102.0 429724.0 442102.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>11.4331</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>69.26172</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0762632</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0085962275</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.044818707</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7276825" gml:id="CP.7276825">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7276825</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7276825.POINT">
                    <gml:pos>429289.6223979376 441994.7042112757</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7276825">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>429352.0 442102.0 429414.0 441995.0 429352.0 441887.0 429228.0 441887.0 429166.0 441995.0 429228.0 442102.0 429352.0 442102.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>10.9959</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>72.61692</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0727887</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.009435018</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.042775616</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7276824" gml:id="CP.7276824">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7276824</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7276824.POINT">
                    <gml:pos>428917.38045429677 441994.7042112757</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7276824">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>428979.0 442102.0 429041.0 441995.0 428979.0 441887.0 428855.0 441887.0 428793.0 441995.0 428855.0 442102.0 428979.0 442102.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>10.5477</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>62.801006</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0693134</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0074201985</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.04072333</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7283987" gml:id="CP.7283987">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7283987</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7283987.POINT">
                    <gml:pos>437478.9451580361 442424.5321840051</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7283987">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>437541.0 442532.0 437603.0 442425.0 437541.0 442317.0 437417.0 442317.0 437355.0 442425.0 437417.0 442532.0 437541.0 442532.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>17.7736</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>112.2936</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1281731</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.015885077</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.075291865</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7026964" gml:id="CP.7026964">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7026964</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7026964.POINT">
                    <gml:pos>443807.05819993035 426950.72516574606</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7026964">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>443869.0 427058.0 443931.0 426951.0 443869.0 426843.0 443745.0 426843.0 443683.0 426951.0 443745.0 427058.0 443869.0 427058.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.61227</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>53.740566</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.06058424</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.007069282</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.035102446</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7039467" gml:id="CP.7039467">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7039467</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7039467.POINT">
                    <gml:pos>446971.1147208775 427702.92411802255</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7039467">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>447033.0 427810.0 447095.0 427703.0 447033.0 427595.0 446909.0 427595.0 446847.0 427703.0 446909.0 427810.0 447033.0 427810.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.57059</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>36.21709</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04645771</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.004406264</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.027027322</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7039466" gml:id="CP.7039466">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7039466</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7039466.POINT">
                    <gml:pos>446598.8727772366 427702.92411802255</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7039466">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>446661.0 427810.0 446723.0 427703.0 446661.0 427595.0 446537.0 427595.0 446475.0 427703.0 446537.0 427810.0 446661.0 427810.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.76556</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>38.17129</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04780997</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0045439517</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.027871873</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7039465" gml:id="CP.7039465">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7039465</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7039465.POINT">
                    <gml:pos>446226.6308335958 427702.92411802255</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7039465">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>446289.0 427810.0 446351.0 427703.0 446289.0 427595.0 446165.0 427595.0 446103.0 427703.0 446165.0 427810.0 446289.0 427810.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.971</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>35.524216</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04922515</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0038526515</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.028695453</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7039464" gml:id="CP.7039464">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7039464</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7039464.POINT">
                    <gml:pos>445854.38888995495 427702.92411802255</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7039464">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>445916.0 427810.0 445978.0 427703.0 445916.0 427595.0 445792.0 427595.0 445730.0 427703.0 445792.0 427810.0 445916.0 427810.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.18937</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>36.637028</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05075156</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0039791693</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.029637787</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7101928" gml:id="CP.7101928">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7101928</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7101928.POINT">
                    <gml:pos>441573.6065380853 431463.91887940496</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7101928">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>441636.0 431571.0 441698.0 431464.0 441636.0 431356.0 441512.0 431356.0 441450.0 431464.0 441512.0 431571.0 441636.0 431571.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>13.6357</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>75.15998</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1027452</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.009755236</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.059837062</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7035887" gml:id="CP.7035887">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7035887</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7035887.POINT">
                    <gml:pos>443248.6952844691 427488.0101316578</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7035887">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>443311.0 427595.0 443373.0 427488.0 443311.0 427381.0 443187.0 427381.0 443125.0 427488.0 443187.0 427595.0 443311.0 427595.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>9.29526</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>57.035717</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0661559</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.007404516</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.038605403</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7101929" gml:id="CP.7101929">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7101929</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7101929.POINT">
                    <gml:pos>441945.84848172613 431463.91887940496</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7101929">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>442008.0 431571.0 442070.0 431464.0 442008.0 431356.0 441884.0 431356.0 441822.0 431464.0 441884.0 431571.0 442008.0 431571.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>12.896</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>84.159294</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0963181</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.012378086</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.056118634</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7039459" gml:id="CP.7039459">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7039459</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7039459.POINT">
                    <gml:pos>443993.1791717508 427702.92411802255</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7039459">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>444055.0 427810.0 444117.0 427703.0 444055.0 427595.0 443931.0 427595.0 443869.0 427703.0 443931.0 427810.0 444055.0 427810.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.57915</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>47.288277</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.06090955</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0057947948</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.035544347</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7039458" gml:id="CP.7039458">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7039458</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7039458.POINT">
                    <gml:pos>443620.9372281099 427702.92411802255</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7039458">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>443683.0 427810.0 443745.0 427703.0 443683.0 427595.0 443559.0 427595.0 443497.0 427703.0 443559.0 427810.0 443683.0 427810.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.94275</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>46.967323</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.06381799</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0053557074</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.037231196</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7039463" gml:id="CP.7039463">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7039463</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7039463.POINT">
                    <gml:pos>445482.14694631414 427702.92411802255</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7039463">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>445544.0 427810.0 445606.0 427703.0 445544.0 427595.0 445420.0 427595.0 445358.0 427703.0 445420.0 427810.0 445544.0 427810.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.42235</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>49.59614</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05239592</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.007042048</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.030596316</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7039462" gml:id="CP.7039462">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7039462</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7039462.POINT">
                    <gml:pos>445109.9050026733 427702.92411802255</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7039462">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>445172.0 427810.0 445234.0 427703.0 445172.0 427595.0 445048.0 427595.0 444986.0 427703.0 445048.0 427810.0 445172.0 427810.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.67388</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>53.072556</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05417077</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0075835856</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.031631224</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7039461" gml:id="CP.7039461">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7039461</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7039461.POINT">
                    <gml:pos>444737.66305903246 427702.92411802255</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7039461">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>444800.0 427810.0 444862.0 427703.0 444800.0 427595.0 444676.0 427595.0 444614.0 427703.0 444676.0 427810.0 444800.0 427810.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.94811</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>42.77673</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05615827</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0050303065</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.03278354</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7039460" gml:id="CP.7039460">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7039460</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7039460.POINT">
                    <gml:pos>444365.4211153916 427702.92411802255</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7039460">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>444427.0 427810.0 444490.0 427703.0 444427.0 427595.0 444303.0 427595.0 444241.0 427703.0 444303.0 427810.0 444427.0 427810.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.24946</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>43.11168</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05841195</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0049048476</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.034096956</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7289342" gml:id="CP.7289342">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7289342</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7289342.POINT">
                    <gml:pos>437665.0661298565 442746.9031635521</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7289342">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>437727.0 442854.0 437789.0 442747.0 437727.0 442639.0 437603.0 442639.0 437541.0 442747.0 437603.0 442854.0 437727.0 442854.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>16.6897</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>114.992035</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1207443</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.01700194</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.07091528</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7035898" gml:id="CP.7035898">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7035898</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7035898.POINT">
                    <gml:pos>447343.35666451836 427488.0101316578</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7035898">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>447405.0 427595.0 447467.0 427488.0 447405.0 427381.0 447281.0 427381.0 447219.0 427488.0 447281.0 427595.0 447405.0 427595.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.3512</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>39.136093</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0449915</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0052494793</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.026066236</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7035897" gml:id="CP.7035897">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7035897</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7035897.POINT">
                    <gml:pos>446971.1147208775 427488.0101316578</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7035897">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>447033.0 427595.0 447095.0 427488.0 447033.0 427381.0 446909.0 427381.0 446847.0 427488.0 446909.0 427595.0 447033.0 427595.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.53502</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>33.982105</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04628246</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0038727266</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.026921978</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7035896" gml:id="CP.7035896">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7035896</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7035896.POINT">
                    <gml:pos>446598.8727772366 427488.0101316578</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7035896">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>446661.0 427595.0 446723.0 427488.0 446661.0 427381.0 446537.0 427381.0 446475.0 427488.0 446537.0 427595.0 446661.0 427595.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.72773</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>34.28451</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04761693</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0037265446</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.027756179</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7032319" gml:id="CP.7032319">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7032319</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7032319.POINT">
                    <gml:pos>443993.1791717508 427273.09614529315</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7032319">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>444055.0 427381.0 444117.0 427273.0 444055.0 427166.0 443931.0 427166.0 443869.0 427273.0 443931.0 427381.0 444055.0 427381.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.49698</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>49.928253</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.06002953</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.006384175</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.035037458</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7032318" gml:id="CP.7032318">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7032318</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7032318.POINT">
                    <gml:pos>443620.9372281099 427273.09614529315</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7032318">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>443683.0 427381.0 443745.0 427273.0 443683.0 427166.0 443559.0 427166.0 443497.0 427273.0 443559.0 427381.0 443683.0 427381.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.85225</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>45.80154</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.06252556</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.005249273</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.036491297</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7032317" gml:id="CP.7032317">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7032317</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7032317.POINT">
                    <gml:pos>443248.6952844691 427273.09614529315</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7032317">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>443311.0 427381.0 443373.0 427273.0 443311.0 427166.0 443187.0 427166.0 443125.0 427273.0 443187.0 427381.0 443311.0 427381.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>9.23646</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>62.918766</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0653106</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.008774301</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.038122613</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7035891" gml:id="CP.7035891">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7035891</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7035891.POINT">
                    <gml:pos>444737.66305903246 427488.0101316578</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7035891">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>444800.0 427595.0 444862.0 427488.0 444800.0 427381.0 444676.0 427381.0 444614.0 427488.0 444676.0 427595.0 444800.0 427595.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.90547</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>52.413265</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05589695</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0071972683</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.032630313</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7035890" gml:id="CP.7035890">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7035890</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7035890.POINT">
                    <gml:pos>444365.4211153916 427488.0101316578</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7035890">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>444427.0 427595.0 444490.0 427488.0 444427.0 427381.0 444303.0 427381.0 444241.0 427488.0 444303.0 427595.0 444427.0 427595.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.20757</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>52.068825</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05803228</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.006822557</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.03387734</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7035889" gml:id="CP.7035889">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7035889</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7035889.POINT">
                    <gml:pos>443993.1791717508 427488.0101316578</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7035889">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>444055.0 427595.0 444117.0 427488.0 444055.0 427381.0 443931.0 427381.0 443869.0 427488.0 443931.0 427595.0 444055.0 427595.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.53691</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>43.72605</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.06046428</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.004737745</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.03528784</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7098359" gml:id="CP.7098359">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7098359</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7098359.POINT">
                    <gml:pos>441945.84848172613 431249.0048930402</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7098359">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>442008.0 431356.0 442070.0 431249.0 442008.0 431142.0 441884.0 431142.0 441822.0 431249.0 441884.0 431356.0 442008.0 431356.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>12.726</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>76.10148</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0955595</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.010675922</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.05566174</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7035888" gml:id="CP.7035888">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7035888</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7035888.POINT">
                    <gml:pos>443620.9372281099 427488.0101316578</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7035888">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>443683.0 427595.0 443745.0 427488.0 443683.0 427381.0 443559.0 427381.0 443497.0 427488.0 443559.0 427595.0 443683.0 427595.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.89498</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>48.104053</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.06311146</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0056507853</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.03682733</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7035895" gml:id="CP.7035895">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7035895</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7035895.POINT">
                    <gml:pos>446226.6308335958 427488.0101316578</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7035895">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>446289.0 427595.0 446351.0 427488.0 446289.0 427381.0 446165.0 427381.0 446103.0 427488.0 446165.0 427595.0 446289.0 427595.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.9307</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>41.08519</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04902491</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.005206761</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.028575605</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7035894" gml:id="CP.7035894">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7035894</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7035894.POINT">
                    <gml:pos>445854.38888995495 427488.0101316578</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7035894">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>445916.0 427595.0 445978.0 427488.0 445916.0 427381.0 445792.0 427381.0 445730.0 427488.0 445792.0 427595.0 445916.0 427595.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.14691</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>47.012375</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05054845</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0065103755</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.029516144</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7292912" gml:id="CP.7292912">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7292912</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7292912.POINT">
                    <gml:pos>437665.0661298565 442961.81714991684</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7292912">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>437727.0 443069.0 437789.0 442962.0 437727.0 442854.0 437603.0 442854.0 437541.0 442962.0 437603.0 443069.0 437727.0 443069.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>16.1628</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>92.45122</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.1161559</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.011779776</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.06824108</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7035892" gml:id="CP.7035892">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7035892</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7035892.POINT">
                    <gml:pos>445109.9050026733 427488.0101316578</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7035892">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>445172.0 427595.0 445234.0 427488.0 445172.0 427381.0 445048.0 427381.0 444986.0 427488.0 445048.0 427595.0 445172.0 427595.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.63057</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>47.019573</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05394627</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.006343417</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.03149817</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7116227" gml:id="CP.7116227">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7116227</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7116227.POINT">
                    <gml:pos>448646.2034672613 432323.57482486375</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7116227">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>448708.0 432431.0 448770.0 432324.0 448708.0 432216.0 448584.0 432216.0 448522.0 432324.0 448584.0 432431.0 448708.0 432431.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>5.50812</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>34.22746</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04014167</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0047274386</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.023474049</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7043035" gml:id="CP.7043035">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7043035</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7043035.POINT">
                    <gml:pos>446226.6308335958 427917.8381043872</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7043035">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>446289.0 428025.0 446351.0 427918.0 446289.0 427810.0 446165.0 427810.0 446103.0 427918.0 446165.0 428025.0 446289.0 428025.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.0126</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>38.471123</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04943076</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.004699087</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.028823446</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7043034" gml:id="CP.7043034">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7043034</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7043034.POINT">
                    <gml:pos>445854.38888995495 427917.8381043872</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7043034">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>445916.0 428025.0 445978.0 427918.0 445916.0 427810.0 445792.0 427810.0 445730.0 427918.0 445792.0 428025.0 445916.0 428025.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.23304</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>40.056576</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05097927</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0048542335</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.029775092</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7043033" gml:id="CP.7043033">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7043033</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7043033.POINT">
                    <gml:pos>445482.14694631414 427917.8381043872</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7043033">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>445544.0 428025.0 445606.0 427918.0 445544.0 427810.0 445420.0 427810.0 445358.0 427918.0 445420.0 428025.0 445544.0 428025.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.46738</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>42.907566</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05262827</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0053056707</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.030736128</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7105499" gml:id="CP.7105499">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7105499</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7105499.POINT">
                    <gml:pos>441945.84848172613 431678.8328657696</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7105499">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>442008.0 431786.0 442070.0 431679.0 442008.0 431571.0 441884.0 431571.0 441822.0 431679.0 441884.0 431786.0 442008.0 431786.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>13.069</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>75.434265</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0970419</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.010305269</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.056557097</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7043036" gml:id="CP.7043036">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7043036</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7043036.POINT">
                    <gml:pos>446598.8727772366 427917.8381043872</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7043036">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>446661.0 428025.0 446723.0 427918.0 446661.0 427810.0 446537.0 427810.0 446475.0 427918.0 446537.0 428025.0 446661.0 428025.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.80481</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>40.51584</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04800319</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0053689852</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.027992625</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7043031" gml:id="CP.7043031">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7043031</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7043031.POINT">
                    <gml:pos>444737.66305903246 427917.8381043872</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7043031">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>444800.0 428025.0 444862.0 427918.0 444800.0 427810.0 444676.0 427810.0 444614.0 427918.0 444676.0 428025.0 444800.0 428025.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>7.99365</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>53.82924</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05648145</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0075898967</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.03297661</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7043030" gml:id="CP.7043030">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7043030</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7043030.POINT">
                    <gml:pos>444365.4211153916 427917.8381043872</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7043030">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>444427.0 428025.0 444490.0 427918.0 444427.0 427810.0 444303.0 427810.0 444241.0 427918.0 444303.0 428025.0 444427.0 428025.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.29436</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>47.228085</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05881665</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.005927035</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.03433574</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7251886" gml:id="CP.7251886">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7251886</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7251886.POINT">
                    <gml:pos>448273.9615236204 440490.3063067227</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7251886">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>448336.0 440598.0 448398.0 440490.0 448336.0 440383.0 448212.0 440383.0 448150.0 440490.0 448212.0 440598.0 448336.0 440598.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.19886</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>41.420784</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05096053</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.006857177</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.029793086</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7251885" gml:id="CP.7251885">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7251885</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7251885.POINT">
                    <gml:pos>447901.7195799796 440490.3063067227</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7251885">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>447964.0 440598.0 448026.0 440490.0 447964.0 440383.0 447840.0 440383.0 447778.0 440490.0 447840.0 440598.0 447964.0 440598.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.49893</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>42.243046</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05338527</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0068820566</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.031201236</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7248316" gml:id="CP.7248316">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7248316</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7248316.POINT">
                    <gml:pos>448273.9615236204 440275.39232035796</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7248316">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>448336.0 440383.0 448398.0 440275.0 448336.0 440168.0 448212.0 440168.0 448150.0 440275.0 448212.0 440383.0 448336.0 440383.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.08996</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>34.201214</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05025775</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0050712987</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.029378396</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7119797" gml:id="CP.7119797">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7119797</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7119797.POINT">
                    <gml:pos>448646.2034672613 432538.4888112285</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7119797">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>448708.0 432646.0 448770.0 432538.0 448708.0 432431.0 448584.0 432431.0 448522.0 432538.0 448584.0 432646.0 448708.0 432646.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>5.43483</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>32.500282</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.04002362</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.004487516</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.023396851</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7060882" gml:id="CP.7060882">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7060882</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7060882.POINT">
                    <gml:pos>445109.9050026733 428992.4080362108</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7060882">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>445172.0 429100.0 445234.0 428992.0 445172.0 428885.0 445048.0 428885.0 444986.0 428992.0 445048.0 429100.0 445172.0 429100.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>8.74953</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>63.014114</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.06124403</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0092742825</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.03581772</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="7259026" gml:id="CP.7259026">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7259026</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::27700" gml:id="CP.7259026.POINT">
                    <gml:pos>448273.9615236204 440920.1342794521</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::27700" gml:id="NL.IMAER.REPR.7259026">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>448336.0 441028.0 448398.0 440920.0 448336.0 440813.0 448212.0 440813.0 448150.0 440920.0 448212.0 441028.0 448336.0 441028.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>6.40136</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>44.271805</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.05221073</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.007319168</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.030528335</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
    <imaer:definitions>
        <imaer:Definitions>
            <imaer:customDiurnalVariation>
                <imaer:CustomDiurnalVariation gml:id="DiurnalProfile.1">
                    <imaer:label>Custom diurnal profile</imaer:label>
                    <imaer:customType>THREE_DAY</imaer:customType>
                    <imaer:value>2.5</imaer:value>
                    <imaer:value>0.5</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>0.5</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>2.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>0.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.2</imaer:value>
                    <imaer:value>0.4</imaer:value>
                    <imaer:value>0.5</imaer:value>
                    <imaer:value>1.5</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>0.5</imaer:value>
                    <imaer:value>1.6</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.6</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>0.8</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.6</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>0.9</imaer:value>
                    <imaer:value>0.0</imaer:value>
                    <imaer:value>0.0</imaer:value>
                    <imaer:value>0.5</imaer:value>
                    <imaer:value>0.5</imaer:value>
                    <imaer:value>2.0</imaer:value>
                    <imaer:value>1.4</imaer:value>
                    <imaer:value>1.4</imaer:value>
                    <imaer:value>0.4</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>0.5</imaer:value>
                    <imaer:value>0.4</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                </imaer:CustomDiurnalVariation>
            </imaer:customDiurnalVariation>
            <imaer:customDiurnalVariation>
                <imaer:CustomDiurnalVariation gml:id="DiurnalProfile.2">
                    <imaer:label>Custom diurnal profile (1)</imaer:label>
                    <imaer:customType>THREE_DAY</imaer:customType>
                    <imaer:value>1.5</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>0.5</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                    <imaer:value>1.0</imaer:value>
                </imaer:CustomDiurnalVariation>
            </imaer:customDiurnalVariation>
        </imaer:Definitions>
    </imaer:definitions>
</imaer:FeatureCollectionCalculator>
