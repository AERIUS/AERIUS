/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
import admZip from "adm-zip";
import browserify from "@cypress/browserify-preprocessor";
import cucumber from "cypress-cucumber-preprocessor";
import { defineConfig } from "cypress";
import fs from 'fs';
import path from 'path';

const { configureVisualRegression } = require('cypress-visual-regression');
const { downloadFile } = require("cypress-downloadfile/lib/addPlugin");
const pdf = require("pdf-parse");
const unzipping = require("./cypress/support/unzipping");

const zipContent = (inputContent: String, outputFile: String, isFolder: boolean) => {
  try {
    const zip = new admZip();
    const fullPathInputContent = __dirname + "/" + inputContent;
   
    if (isFolder) {
      zip.addLocalFolder(fullPathInputContent);
    } else {
      zip.addLocalFile(fullPathInputContent);
    }
    zip.writeZip(outputFile);
    console.log(`Created ${outputFile} successfully from source ${fullPathInputContent}`);
    return true;
  } catch (e) {
    console.log(`Something went wrong with zipping the file. ${e}`);
    return false;
  }  
};

export default defineConfig({
  e2e: {
    baseUrl: "http://127.0.0.1:8080",
    specPattern: "**/*.feature",
    screenshotsFolder: './cypress/snapshots/actual',
    excludeSpecPattern: [],
    setupNodeEvents(on, config) {
      // Use browserify to allow for use of `import` and `export`
      const options = {
        ...browserify.defaultOptions,
        typescript: require.resolve("typescript"),
      };

      const setDefaultLocale = (locale: string) => {
        if (config.baseUrl?.toLowerCase().indexOf("locale") == -1) {
          if (config.baseUrl.indexOf("?") == -1) {
            config.baseUrl += "/?locale=" + locale;
          } else {
            config.baseUrl += "&locale=" + locale;
          }
        };
      };

      if (config.env.TEST_PROFILE == "UK") {
        config.specPattern = "**/cucumber-tests-uk/*.feature";
        if (config.env.SKIP_COOKIES_CHECK === false) {
          config.env.ACCEPT_ALL_COOKIES = "true";
        }
        setDefaultLocale("en");

      } else if (config.env.TEST_PROFILE == "NL" || config.env.TEST_PROFILE == "WNB") {
        config.specPattern = "**/cucumber-tests/*.feature";
        setDefaultLocale("nl");

      } else if (config.env.TEST_PROFILE == "LBV") {
        config.specPattern = "**/cucumber-tests-check/*.feature";
        setDefaultLocale("nl");

      } else {
        config.specPattern = [
          "**/cucumber-tests/*.feature"
        ];
        setDefaultLocale("nl");
      }
      
      // Visual regression
      configureVisualRegression(on);

      // Run using cucumber 
      on("file:preprocessor", cucumber.default(options));

      on("task", {
        // Delete a file
        deleteFile(fileName) {
          fs.unlink(fileName, (err) => {
            if (err) {
              console.error(err);
            }
          });
          return null;
        },

        // Get latest downloaded file
        getLatestDownloadedFile() {
          const downloadsFolder = `${__dirname}/downloads/`;
          const sortedFiles = fs
            .readdirSync(downloadsFolder)
            .filter((file: any) => fs.lstatSync(path.join(downloadsFolder, file)).isFile())
            .filter((file: any) => !file.includes(".crdownload")) // Filter out temp files
            .map((file: any) => ({
              file,
              mtime: fs.lstatSync(path.join(downloadsFolder, file)).mtime,
            }))
            .sort((a: any, b: any) => b.mtime.getTime() - a.mtime.getTime());

          return sortedFiles.length ? sortedFiles[0].file : undefined;
        },

        // Test is a file exist if not return null
        readFileMaybe(filename: any) {
          if (fs.existsSync(filename)) {
            return fs.readFileSync(filename, { encoding: 'base64', flag: 'r' });
          }
          return null;
        },

        // Return list of files in directory
        readFolder(path: any) {
          return fs.readdirSync(path) as String[];
        },

        // Unzip files to "fixtures/unzip" directory
        unzipping: unzipping.unzip,

        // Zip file
        zip({inputFile, outputFile}) {
          return zipContent(inputFile, outputFile, false);
        },

        // Zip files in folder
        zipFolder({inputFolder, outputFile}) {
          return zipContent(inputFolder, outputFile, true);
        },

        // support file download
        downloadFile: downloadFile,

        // read directory content same as readFolder ?
        readdir: readdir,

        // read pdf conent
        readPdf: readPdf
      });
      
      return config;
    },
  },

  chromeWebSecurity: false,
  reporter: "cypress-multi-reporters",
  reporterOptions: {
    configFile: "reporter-config.json",
    embeddedScreenshots: true,
    reportDir: "cypress/results",
    reportFilename: "report.html",
    overwrite: true,
    saveJson: true,
    charts: true,
    code: false,
  },
});

function readdir({ path }) {
  return fs.readdirSync(path);
}

function readPdf(pdfFile: string) {
  return new Promise((resolve) => {
    const fileId = pdfFile.Id;
    const pdfDirPath = path.resolve(pdfFile.dirPath);
    const filesInDir = fs.readdirSync(pdfDirPath);
    const dataBufferOfPdf = fs.readFileSync(pdfDirPath + "/" + filesInDir[fileId]);

    pdf(dataBufferOfPdf).then(function ({ text }) {
      resolve(text);
    });
  });
}
