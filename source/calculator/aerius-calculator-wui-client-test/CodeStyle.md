# Code style

To keep the automatic Cypress tests readable we use Prettier and Eslint. This document describes how to use these, and what custom rules are being used for the AERIUS application.

## How to use Prettier and Eslint

Prettier and Eslint can be ran both in an IDE and through a CLI. The recommended way to use them is to combine them:
Firstly use the IDE while writing code to check for issues as you go, and then, once you've finished your changes, run Prettier and Eslint one last time through the CLI for a last code styling commit.

### Using Prettier and Eslint in an IDE

Some IDE's can recognize the Prettier and Eslint settings and automatically highlight any issues directly as you write code.
It can be useful to use an IDE that can do these checks? f.e. InteliJ and Visual Studio code (with Extensions) support this.

### Using Prettier and Eslint in a CLI

Even if you're using an IDE that supports Prettier and Eslint it can still be a good idea to run them after you've made some changes.
To do this, firstly, `npm install` needs to be ran in the `AERIUS-II\source\calculator\aerius-calculator-wui-client-test\src\test\e2e` folder to install the required prettier and eslint packages.
Once these are installed, they can be used in the following ways (in the same folder):
NOTE: To run them in different folders or for specific files replace `.` to the relative path to the folder/file you want to test.

#### Running Prettier and Eslint sequentially

To run Prettier and Eslint sequentially, use the following commands:
NOTE: This only works if you use `.`, for other paths you'll need to run Prettier and Eslint seperately.

To audit the changes use the following command:
```bash
npm run format:audit
```

To check and then automatically fix the codestyle use the following command:
NOTE: Make sure to commit any changes beforehand. This WILL change your files, which is hard to revert without source control.
```bash
npm run format:fix
```

#### Prettier

To check for recommendations using Prettier without actually modifying the files, use:
```bash
npx prettier --check .
```

To automatically apply the Prettier recommendations use the following command:
NOTE: Make sure to commit any changes beforehand. This WILL change your files, which is hard to revert without source control.
```bash
npx prettier --write .
```

#### Eslint

To find issues with the code using ESLint use the following command:
```bash
npx eslint .
```

To fix the found issues with the code (where possible) using ESLint use the following command (if possible):
NOTE: Make sure to commit any changes beforehand. This WILL change your files, which is hard to revert without source control.
```bash
npx eslint . --fix
```

## Code styling decisions

The code styling decisions for Cypress are supposed to mimic the code styling decisions for the application itself as much as possible.
This includes choices like a tab-index of 2 spaces, and the choice for PascalCase for class names and camelCase for functions.
The full list of code style choices are those of Prettier and Eslint, combined with the custom rules for each:

### Prettier

[Prettier](https://prettier.io/) makes the the code prettier. It helps filter out code style issues, making the code more readable and consistent.
There are many default Prettier rules, for which it is hard to find a full list.
Some examples that I could find are described [here](https://prettier.io/docs/en/option-philosophy.html).
These, among many others, include the following rules:
1. Add/remove whitespace to follow the set print width.
2. Add a new line at the end of a file.
3. Use `"` quotes where possible.

Prettier only allows for a couple of custom rules, but is generally very opionated, which doesn't allow us all the options we want.
It does however make the code prettier in ways eslint can't, so we still want to use it.
Therefore we run prettier first, and eslint afterwards to clean up all the prettier settings which we couldn't adjust.

#### Custom prettier rules

The custom rules we use for prettier can be found in [Prettier code style](./src/test/e2e/.prettierrc.json).
Currently we're using the following custom rules:

1. trailingComma: none
2. printWidth": 140

##### Trailing comma's

Trailing comma's are automatically removed due to the "trailingComma: none" setting. This gives a cleaner look to the code.
Example:
```bash
{ "test", "test2", } -> { "test", "test2" }
```

##### Print width

This option changes the default allowed line width from 80 to 140.
With 80, the code got split up too much, making it unreadable in some cases.
140 seems to be a nice cut-off point, allowing for some more space, while still keeping the code from being too wide.

Example (default print width):
```bash
Given(
  "I select file {string} to be imported and select situation type {string}",
  (fileName, situationType) => {
    commonPage.selectFileForImportAndSelectSituationType(
      fileName,
      situationType
    );
  }
);
```

Example (140 print width):
```bash
Given("I select file {string} to be imported and select situation type {string}", (fileName, situationType) => {
  commonPage.selectFileForImportAndSelectSituationType(fileName, situationType);
});
```

### Eslint

[Eslint](https://eslint.org/) also fixes the code, but requires a bit more configuration. However, it is more versatile, even allowing for custom rules.
It can for example specifically test for Cypress issues, recognize unused variables and automatically convert multiline single line comments into multiline comments.
We run it after we've finished running prettier to find the last remaining issues (and possibly overwrite some of Prettiers opinionated decisions).
This also helps with debugging any found warnings/errors, since the code has already been cleaned up a bit before we see the warnings/errors.

Eslint has many default rules.
These are called the `recommended` eslint rules, and can be found [here](https://github.com/eslint/eslint/blob/main/conf/eslint-recommended.js), with explanation of these rules [here](https://eslint.org/docs/latest/rules/).

#### Custom Eslint settings

The custom eslint rules can be found in [Eslint code style](./src/test/e2e/.eslintrc.json).
Currently the following custom rules are being enforced:

1. env
    1. browser
    2. node
2. extends
    1. cypress/recommended
3. parserOptions
    1. ecmaVersion: latest
    2. sourceType: module
4. plugins: unused-imports
5. rules:
    1. no-constant-binary-expression: warn
    2. no-duplicate-imports: warn { includeExports: true }
    3. no-dupe-class-members: error
    4. no-unused-vars: warn
    5. no-use-before-define: error
    6. operator-linebreak: error, before
    7. spaced-comment: warn, always
    8. unused-imports/no-unused-imports: warn
    9. cypress/no-unnecessary-waiting: warn
    10. multiline-comment-style: warn

##### Environment

The `env` settings allow certain environments to be used.
1. `browser` allows the browser to be used during tests.
2. `node` allows Node.js environments to be used during tests.

##### Extends cypress/recommended

Extending Cypress sets some default values to `env`, `plugins` and `rules`, allowing for better codestyles specifically tailored for Cypress tests.
These rules can still be overwritten in the `rules`.

##### Parser options

Specifies which custom JavaScript language options eslint supports.
The default is a very old Javascript version with possibly a different syntax, hence why we overwrite it here.

1. `ecmaVersion: latest` automatically supports the latest version of ECMAscript (JavaScript standard).
2. `sourceType: module` makes sure eslint can parse JS files that are modules.

##### Plugins

The custom (not eslint official) plugins we use. We're currently only using the widely used `unused-imports` plugin (see rules for the implementation).

##### Rules

###### no-constant-binary-expression: warn

[Link](https://eslint.org/docs/latest/rules/no-constant-binary-expression)
Disallow expressions where the operation doesn't affect the value.
These do nothing but make the code needlessly complicated without any benefit.

Example of what's not allowed:
```bash
const value1 = +x == null;
const value2 = condition ? x : {} || DEFAULT;
const value3 = !foo == null;
const value4 = new Boolean(foo) === true;
const objIsEmpty = someObj === {};
const arrIsEmpty = someArr === [];
```

Example of what is allowed:
```bash
const value1 = x == null;
const value2 = (condition ? x : {}) || DEFAULT;
const value3 = !(foo == null);
const value4 = Boolean(foo) === true;
const objIsEmpty = Object.keys(someObj).length === 0;
const arrIsEmpty = someArr.length === 0;
```

###### no-duplicate-imports: warn

[Link](https://eslint.org/docs/latest/rules/no-duplicate-imports)
Disallow imports from the same source over multiple lines.
Using `--fix` while running eslint automatically merges these imports.

Examples of what's not allowed:
```bash
import { merge } from 'module';
import something from 'another-module';
import { find } from 'module';
```

Examples of what is allowed:
```bash
import { merge, find } from 'module';
import something from 'another-module';
```

###### no-dupe-class-members: error

[Link](https://eslint.org/docs/latest/rules/no-dupe-class-members)
Disallow duplicate class members.
This rule is needed since JavaScript doesn't automatically checks for this (TypeScript does).

Example of what's not allowed:
```bash
class Foo {
  let test;
  let test;
}
```

Example of what is allowed:
```bash
class Foo {
  let test;
  let test2;
}
```

###### no-unused-vars: warn

[Link](https://eslint.org/docs/latest/rules/no-unused-vars)
Disallow unused variables

Example of what's not allowed:
```bash
static foo(input)
  let test = 5; // test is unused
  return 10;    // input is unused
}
```

###### no-use-before-define: error

[Link](https://eslint.org/docs/latest/rules/no-use-before-define)
Disallow the use of variables before they are defined.

Example of use before definition:
```bash
cy.log(a);
let a = 10;
```

###### operator-linebreak: error, before

[Link](https://eslint.org/docs/latest/rules/operator-linebreak)
Enforce consistent linebreak style for operators, at the start of each line.
This makes sure the linebreak is consistent with the application itself, where we also use `before`.
Using `--fix` while running eslint automatically fixes these to the required style.

Example of what's not allowed:
```bash
var fullHeight = borderTop +
                 innerHeight +
                 borderBottom;
```

Example of what is allowed:
```bash
var fullHeight = borderTop
               + innerHeight
               + borderBottom;
```

###### spaced-comment: warn, always

[Link](https://eslint.org/docs/latest/rules/spaced-comment)
Enforce consistent spacing after the // or /* in a comment
After each comment start there should be a space.

Example of what's not allowed:
```bash
//This is a comment with no whitespace at the beginning
/*This is a comment with no whitespace at the beginning */
```

Example of what is allowed:
```bash
// This is a comment with a whitespace at the beginning
/* This is a comment with a whitespace at the beginning */
```

###### unused-imports/no-unused-imports: warn

[Link](https://github.com/sweepline/eslint-plugin-unused-imports)
This rule disallows imports that aren't used.
Errors/warnings for unused imports are already shown in eslint without this rule,
but this rule allows automatic fixing when using `--fix` while running eslint.

###### cypress/no-unnecessary-waiting: warn

[Eslint plugin documentation](https://npm.io/package/eslint-plugin-cypress)

[Cypress documentation link](https://docs.cypress.io/guides/references/best-practices#Unnecessary-Waiting)

Waiting in Cypress with `cy.wait(4000)` is almost never needed.
There's almost always a better way, which is why we want to catch its usage so we can reformat it.
By default this gave an error, but we've changed it to a warning, because there are still a few instances where use it currently.

Example of what's not allowed:
```bash
cy.intercept('GET', '/users', [{ name: 'Maggy' }, { name: 'Joan' }]);
cy.get('#fetch').click();
cy.wait(4000); // <--- this is unnecessary
cy.get('table tr').should('have.length', 2);
```

Example of what is allowed:
```bash
cy.intercept('GET', '/users', [{ name: 'Maggy' }, { name: 'Joan' }]).as(
  'getUsers'
);
cy.get('[data-testid="fetch-users"]').click();
cy.wait('@getUsers'); // <--- wait explicitly for this route to finish
cy.get('table tr').should('have.length', 2);
```

###### multiline-comment-style: warn

[Link](https://eslint.org/docs/latest/rules/multiline-comment-style)
Enforce a particular style for multiline comments
Using `--fix` while running eslint automatically changes these comments to the required style.

Example before:
```bash
// This comment
// spans 2 lines
```

Example after:
```bash
/*
 * This comment
 * spans 2 lines
 */
```
