/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import nl.overheid.aerius.AeriusVersion;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.i18n.SystemInfoMessageRepository;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Test class for {@link SystemInfoUpdateServlet}.
 */
@ExtendWith(MockitoExtension.class)
class SystemInfoUpdateServletTest extends BaseDBTest {

  private static final Pattern MESSAGE_PATTERN = Pattern.compile("\"message\":\\s*\"([^\"]*)\"");
  private static final Pattern VERSION_PATTERN = Pattern.compile("\"applicationVersion\":\\s\"([^\"]*)\"");
  private static final String MESSAGE_BODY = "messageBody";
  private static final String MESSAGE_TITLE = "messageTitle";
  private static final String UUID = "uuid";
  private static final String LOCALE = "locale";
  private static final String MESSAGE = "message";
  private static final String CLEAR_MESSAGE = "clearmessage";

  private static final String FAULT_PASSKEY = "non-existing-UUID";
  private static final String PASSKEY = "58cc5716-132e-4062-ab13-4bc63033aaf0";

  private @Mock HttpServletRequest request;
  private @Mock HttpServletResponse response;
  private @Mock RequestDispatcher dispatcher;
  private @Mock ServletOutputStream outputStream;

  private String locale;
  private SystemInfoUpdateServlet servlet;

  @Override
  @BeforeEach
  public void setUp() throws SQLException {
    servlet = new SystemInfoUpdateServlet() {
      @Override
      protected PMF getPmf() {
        return getPMF();
      }
    };
    // add key
    ConstantRepository.setConstantByKey(getPMF().getConnection(), ConstantsEnum.SYSTEM_INFO_PASSKEY.name(), PASSKEY);
    // clear old message
    SystemInfoMessageRepository.clearAllMessages(getPMF().getConnection());
    locale = LocaleUtils.DUTCH.getLanguage();
  }

  @Test
  void testGetJsp() throws IOException, ServletException {
    Mockito.when(request.getRequestDispatcher(SystemInfoUpdateServlet.WEB_INF_JSP_SYSTEM_SYSTEMINFO_JSP)).thenReturn(dispatcher);
    servlet.doGet(request, response);
    verify(outputStream, never()).write(any(byte[].class));
    verify(request.getRequestDispatcher(SystemInfoUpdateServlet.WEB_INF_JSP_SYSTEM_SYSTEMINFO_JSP), times(1)).forward(request, response);
  }

  @Test
  void testNoValidUUID() throws SQLException, ServletException, IOException {
    final String messageTitle = "Fout bij verwerken";
    final String messageBody = "Bericht is niet bijgewerkt, problemen met UUID.";
    assertUpdateSystemMessage("Systeem gaat voor onderhoud uit de lucht", FAULT_PASSKEY, messageTitle, messageBody, null, false);
  }

  @Test
  void testUpdateSystemMessage() throws SQLException, ServletException, IOException {
    final String messageTitle = "Bericht verwerkt.";
    final String messageBody = "Nieuw bericht is bijgewerkt.";
    assertUpdateSystemMessage("Systeem gaat voor onderhoud uit de lucht", PASSKEY, messageTitle, messageBody, null, true);
  }

  @Test
  void testUnsetSystemMessage() throws SQLException, ServletException, IOException {
    final String messageTitle = "Bericht verwerkt.";
    final String messageBody = "Nieuw bericht is bijgewerkt.";
    assertUpdateSystemMessage("", PASSKEY, messageTitle, messageBody, null, true);
  }

  @Test
  void testClearAllMessages() throws SQLException, ServletException, IOException {
    final String messageTitle = "Bericht verwerkt.";
    final String messageBody = "Nieuw bericht is bijgewerkt.";
    assertUpdateSystemMessage("", PASSKEY, messageTitle, messageBody, "on", true);
  }

  private void assertUpdateSystemMessage(final String message, final String passkey, final String messageTitle, final String messageBody,
      final String clearMessage, final boolean shouldSet) throws SQLException, ServletException, IOException {
    prepairRequest(message, passkey, clearMessage);
    prepairResponse(HttpStatus.SC_OK);
    prepairRequestMessage(messageTitle, messageBody);
    servlet.doPost(request, response);
    final String currentMessage = SystemInfoMessageRepository.getMessage(getPMF().getConnection(), locale);

    assertEquals(HttpStatus.SC_OK, response.getStatus(), "Expect status 200");
    assertEquals(messageTitle, request.getParameter(MESSAGE_TITLE), "Expect message title");
    assertEquals(messageBody, request.getParameter(MESSAGE_BODY), "Expect message body");

    if (shouldSet) {
      assertEquals(message, currentMessage, "New message is not set while we expect it to be set");
      assertDoGet(message);
    } else {
      assertNotEquals(message, currentMessage, "Message must not be changed but is :(");
    }
  }

  private void assertDoGet(final String newMessage) throws IOException, ServletException {
    Mockito.when(request.getParameter(MESSAGE)).thenReturn(MESSAGE);
    Mockito.when(response.getOutputStream()).thenReturn(outputStream);
    final AtomicReference<String> expectedMessage = new AtomicReference<String>("");
    doAnswer(new Answer<Void>() {
      @Override
      public Void answer(final InvocationOnMock invocation) {
        expectedMessage.set(new String(invocation.getArgument(0), StandardCharsets.UTF_8));
        return null;
      }
    }).when(outputStream).write(any(byte[].class));
    servlet.doGet(request, response);

    final Matcher messageMatcher = MESSAGE_PATTERN.matcher(expectedMessage.get());
    assertTrue(messageMatcher.find(), "Result should contain the message property");
    final String currentMessage = messageMatcher.group(1);
    assertEquals(newMessage, currentMessage, "Servlet doGet should return message from database");

    final Matcher versionMatcher = VERSION_PATTERN.matcher(expectedMessage.get());
    assertTrue(versionMatcher.find(), "Result should contain the version property");
    final String currentVersion = versionMatcher.group(1);
    assertEquals(AeriusVersion.getVersionNumber(), currentVersion, "Servlet doGet should return the current version");

    verify(request.getRequestDispatcher(SystemInfoUpdateServlet.WEB_INF_JSP_SYSTEM_SYSTEMINFO_JSP), times(1)).forward(request, response);
  }

  private void prepairRequest(final String newmessage, final String passKey, final String clearMessage) {
    Mockito.when(request.getParameter(MESSAGE)).thenReturn(newmessage);
    Mockito.when(request.getParameter(LOCALE)).thenReturn(locale);
    Mockito.when(request.getParameter(UUID)).thenReturn(passKey);
    Mockito.when(request.getParameter(CLEAR_MESSAGE)).thenReturn(clearMessage);
    Mockito.when(request.getRequestDispatcher(SystemInfoUpdateServlet.WEB_INF_JSP_SYSTEM_SYSTEMINFO_JSP)).thenReturn(dispatcher);
  }

  private void prepairRequestMessage(final String messageTitle, final String messageBody) {
    Mockito.when(request.getParameter(MESSAGE_TITLE)).thenReturn(messageTitle);
    Mockito.when(request.getParameter(MESSAGE_BODY)).thenReturn(messageBody);
  }

  private void prepairResponse(final int scOk) {
    Mockito.when(response.getStatus()).thenReturn(HttpStatus.SC_OK);
  }

}
