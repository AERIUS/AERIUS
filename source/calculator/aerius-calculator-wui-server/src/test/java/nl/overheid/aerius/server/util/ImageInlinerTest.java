/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * Test class for {@link ImageInliner}.
 */
class ImageInlinerTest {

  @Test
  void testReadPng() {
    final String inlinePng = ImageInliner.inline("govuk-crest.png");
    final String prefix = "data:image/png;base64,";

    assertEquals(prefix, inlinePng.substring(0, prefix.length()), "Prefix should match expected PNG data prefix");
    assertEquals(4802, inlinePng.length(), "Expected size of PNG doesn't match");
  }

  @Test
  void testReadSvg() {
    final String inlineSvg = ImageInliner.inline("assets/images/splash/splash-calculator.svg");
    final String prefix = "data:image/svg+xml;base64,";

    assertEquals(prefix, inlineSvg.substring(0, prefix.length()), "Prefix should match expected SVG data prefix");
    assertEquals(4782, inlineSvg.length(), "Expected size of SVG doesn't match");
  }

  @Test
  void testReadOther() {
    final String inline = ImageInliner.inline("no-image");

    assertEquals("", inline, "Unsupported image should return empty string");
  }

  @Test
  void testReadUnsupportedImage() {
    final String inline = ImageInliner.inline("no-image.jpg");

    assertEquals("", inline, "Unsupported JPG image should return empty string");
  }

}
