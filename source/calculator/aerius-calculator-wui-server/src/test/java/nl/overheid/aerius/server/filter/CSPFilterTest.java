/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.filter;

import static nl.overheid.aerius.server.filter.CSPFilter.CONTENT_SECURITY_POLICY_HEADER;
import static nl.overheid.aerius.server.filter.CSPFilter.GWT_SUPER_DEV_MODE_ADDRESS;
import static nl.overheid.aerius.server.filter.CSPFilter.SUPER_DEV_MODE_PARAM;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CSPFilterTest {

  @Mock private FilterChain filterChain;
  @Mock private FilterConfig filterConfig;

  @Captor private ArgumentCaptor<String> captor;

  @Test
  void testDoFilter() throws ServletException, IOException {
    final HttpServletRequest request = mock(HttpServletRequest.class);
    when(request.getServletPath()).thenReturn("/");
    final HttpServletResponse response = mock(HttpServletResponse.class);

    final CSPFilter cspFilter = new CSPFilter();
    cspFilter.init(filterConfig);
    cspFilter.doFilter(request, response, filterChain);

    verify(response).addHeader(eq(CONTENT_SECURITY_POLICY_HEADER), captor.capture());
    final String cspHeaderValue = captor.getValue();

    assertFalse(cspHeaderValue.isEmpty(), "CSP header should not be empty");

    verify(filterChain).doFilter(request, response);
  }

  @Test
  void testDoFilterForNonHttpServletResponse() throws ServletException, IOException {
    final ServletRequest request = mock(ServletRequest.class);
    final ServletResponse response = mock(ServletResponse.class);

    final CSPFilter cspFilter = new CSPFilter();
    cspFilter.init(filterConfig);
    cspFilter.doFilter(request, response, filterChain);

    verifyNoInteractions(request);
    verifyNoInteractions(response);
    verify(filterChain).doFilter(request, response);
  }

  @Test
  void testDoFilterForSuperDevMode() throws ServletException, IOException {
    doReturn("true").when(filterConfig).getInitParameter(SUPER_DEV_MODE_PARAM);
    final HttpServletRequest request = mock(HttpServletRequest.class);
    when(request.getServletPath()).thenReturn("/");
    final HttpServletResponse response = mock(HttpServletResponse.class);

    final CSPFilter cspFilter = new CSPFilter();
    cspFilter.init(filterConfig);
    cspFilter.doFilter(request, response, filterChain);

    verify(response).addHeader(eq(CONTENT_SECURITY_POLICY_HEADER), captor.capture());
    final String cspHeaderValue = captor.getValue();

    assertFalse(cspHeaderValue.isEmpty(), "CSP header should not be empty");
    assertTrue(cspHeaderValue.contains(GWT_SUPER_DEV_MODE_ADDRESS), "CSP should allow superdevmode when configured");

    verify(filterChain).doFilter(request, response);
  }

  @Test
  void testDoNotFilterForPrintRequests() throws ServletException, IOException {
    final HttpServletRequest request = mock(HttpServletRequest.class);
    when(request.getServletPath()).thenReturn("/print");
    final HttpServletResponse response = mock(HttpServletResponse.class);

    final CSPFilter cspFilter = new CSPFilter();
    cspFilter.init(filterConfig);
    cspFilter.doFilter(request, response, filterChain);

    verifyNoInteractions(response);
  }
}
