/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.test.BaseTestDatabase;

/**
 * Test class to test if StartupContextListener initialize runs ok.
 */
class StartupContextListenerTest extends BaseTestDatabase {

  @Test
  void testStartupContextListener() {
    final StartupContextListener listener = new StartupContextListener() {
      @Override
      protected PMF getPMF() {
        return BaseTestDatabase.getPMF();
      }
    };
    final ServletContext context = mock(ServletContext.class);
    final ServletContextEvent event = new ServletContextEvent(context);
    doReturn("").when(context).getRealPath(any());
    listener.contextInitialized(event);
    verify(context, only()).getRealPath(any());
  }
}
