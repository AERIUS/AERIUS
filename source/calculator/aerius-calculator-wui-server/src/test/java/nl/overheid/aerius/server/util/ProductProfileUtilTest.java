/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.shared.domain.ProductProfile;

/**
 * Test class for {@link ProductProfileUtil}.
 */
class ProductProfileUtilTest {

  @ParameterizedTest
  @MethodSource("data")
  void testLocalhost(final String env, final String host, final String productProfleParam, final ProductProfile expectedProfile) {
    final HttpServletRequest request = mock(HttpServletRequest.class);
    doReturn(host).when(request).getHeader("host");
    doReturn(productProfleParam).when(request).getParameter("productProfile");

    assertEquals(expectedProfile, ProductProfileUtil.getProductProfile(request, () -> env), "");
  }

  private static List<Arguments> data() {
    return List.of(
        Arguments.of(null, "localhost", null, ProductProfile.CALCULATOR),
        Arguments.of(null, "localhost", "LBV_POLICY", ProductProfile.LBV_POLICY),
        Arguments.of(null, "example.com", null, ProductProfile.CALCULATOR),
        Arguments.of(null, "example.com", "LBV_POLICY", ProductProfile.CALCULATOR),
        Arguments.of(null, "dev-regelingsondersteuning.aerius.nl", null, ProductProfile.LBV_POLICY),
        Arguments.of(null, "check-dev.aerius.nl", null, ProductProfile.LBV_POLICY),
        Arguments.of("LBV_POLICY", "localhost", null, ProductProfile.LBV_POLICY),
        Arguments.of("LBV_POLICY", "localhost", "LBV_POLICY", ProductProfile.LBV_POLICY),
        Arguments.of("LBV_POLICY", "calculator.aerius.nl", null, ProductProfile.LBV_POLICY),
        Arguments.of("LBV_POLICY", "regelingsondersteuning.aerius.nl", null, ProductProfile.LBV_POLICY),
        Arguments.of("CALCULATOR", "localhost", null, ProductProfile.CALCULATOR),
        Arguments.of("CALCULATOR", "localhost", "LBV_POLICY", ProductProfile.CALCULATOR),
        Arguments.of("CALCULATOR", "example.com", "LBV_POLICY", ProductProfile.CALCULATOR),
        Arguments.of("CALCULATOR", "calculator.aerius.nl", null, ProductProfile.CALCULATOR),
        Arguments.of("CALCULATOR", "regelingsondersteuning.aerius.nl", null, ProductProfile.CALCULATOR));
  }
}
