<%@page import="nl.overheid.aerius.i18n.AeriusExceptionMessages" isErrorPage="true"
%><%@include file="../variables.jsp"
%><%
  final String errorPage = Integer.toString(response.getStatus());
%><!doctype html><html lang="<%=lang%>">
<head>
<title><%=errorPage%></title>
<meta charset="utf-8">
<meta name="robots" content="noindex,nofollow">
<meta name="viewport" content="width=device-width, initial-scale=1">
<base href="${pageContext.request.contextPath}/">
<%
  final String title = errorPage;
  loading = false;
  request.getSession().setAttribute("splashLoadingText", new AeriusExceptionMessages(locale).getString(exception));
%>
<%@include file="/WEB-INF/jsp/header.jsp" %>
<%@include file="/WEB-INF/jsp/splash.jsp" %>
<%@include file="/WEB-INF/jsp/footer.jsp" %>
