<% final boolean checkProfile = productProfile == ProductProfile.LBV_POLICY;
%><link rel="icon" href="assets/images/favicon.ico" sizes="any">
<link rel="icon" href="assets/images/favicon-${productName}.svg" type="image/svg+xml">
<link rel="apple-touch-icon" href="assets/images/apple-touch-icon-${productName}.png">
<link rel="stylesheet" href="assets/fonts/noto-sans-tc-v26-latin.css?v=<%=timestamp%>" type="text/css">
<style>
:root {
  --color-splash-text: <%=checkProfile ? "#d16326" : "#164273"%>;
  --color-splash-base: <%=checkProfile ? "#ffe5cc" : "#e1eaf9"%>;
  --external-header: 0px;
}
.splash-base {
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  min-height: 100vh;
  background: var(--color-splash-base);
}

.splash-container {
  flex-grow: 1;
  height: calc(100vh - 400px);
  display: grid;
  grid-template-columns: 1fr;
  align-items: center;
  justify-items: center;
  position: relative;
}

.splash-title {
  font-size: 50px !important;
  color: var(--color-splash-text);
  text-align: center;
}

.splash-reg {
  font-size: 18px !important;
}

.splash-splash-container {
  display: flex;
  white-space: nowrap;
}

.splash-graph {
  padding: 20px;
}

@media (max-width: 799px) {
  .splash-graph {
    display: none;
  }
}

@media (min-width: 800px) {
  .splash-container {
    display: grid;
    grid-template-columns: 30% 25% 45%;
  }
  .splash-graph {
    padding: 0px;
    max-height: calc(100vh - 362px);
  }
}

@media (min-width: 1200px) {
  .splash-container {
    width: 100%;
    display: grid;
    grid-template-columns: minmax(300px, 1fr) minmax(200px, 1fr) minmax(300px, 1fr);
  }
}

.splash-loadingContainer {
  display: flex;
  align-items: center;
  color: var(--color-splash-text);
  font-size: 1.4em;
}

#splash-loader {
  padding-right: 25px;
  flex-shrink: 0;
  width: 80px;
  height: 80px;
}

.splash-product-logo {
   margin: 10px;
   width: 124px;
   height: 124px;

   box-sizing: border-box;
   border: 1px solid transparent;
   border-radius: 7px;
}

.splash-version p {
  margin: 0;
  font-size: 18px !important;
}

.splash-trademark {
  position: absolute;
  bottom: 16px;
  left: 113px;
  font-size: 9px !important;
  white-space: nowrap;
  color: var(--color-splash-text);
}

.splash-footer {
  width: 100%;
  display: flex;
  max-height: 400px;
  padding-bottom: 20px;
  background: white;
  flex-shrink: 0;
  flex-direction: column;
  align-items: center;
  justify-content: center;
}

.splash-rivm {
  margin-left: 275px;
  height: 111px;
}

.splash-attribution {
  color: #BBB;
  margin-top: 40px;
  margin-bottom: 40px;
  font-size: 16px !important;
}

.splash-provinces {
  width: 100%;

  /* Flex fallback */
  display: flex;
  flex-wrap: wrap;

  /* Supports Grid */
  display: grid;
  grid-template-columns: repeat(auto-fit, 8%);
  grid-auto-rows: 48px;
  align-items: center;
  justify-content: space-around;
}

@media (max-width: 799px) {
  .splash-provinces {
    grid-template-columns: repeat(auto-fit, 100px);
  }
}

.splash-uk {
  width: 100%;

  /* Flex fallback */
  display: flex;
  flex-wrap: wrap;

  /* Supports Grid */
  display: grid;
  grid-template-columns: repeat(auto-fit, 12.5%);
  grid-auto-rows: 48px;
  justify-items: center;
}
.splash-uk img {
  max-height: 65px;
}
</style>
