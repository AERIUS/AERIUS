<%@page import="nl.overheid.aerius.server.util.ImageInliner"%>
    <div class="splash-container">
      <h1 class="splash-title">${titleHtml}</h1>
      <img class="splash-graph" src="<%=ImageInliner.inline("assets/images/splash/aerius-splash-illustration-" + productName + ".svg")%>"></img>
      <div class="splash-loadingContainer">
        <% if (loading) { %><img id="splash-loader" src="<%=ImageInliner.inline("assets/images/splash/loading-spinner.svg")%>"></img><%
           } else { %><div id="splash-loader"></div><%}%>
        <img class="splash-product-logo" src="<%=ImageInliner.inline("assets/images/splash/splash-" + productName + ".svg")%>" alt="${title} Logo"></img>
        <div class="splash-version" id="text">
          <p>${messages['splashVersionPrefix']} <%=nl.overheid.aerius.AeriusVersion.getFriendlyVersionNumber()%></p>
          <p id="splash-message">${splashLoadingText}</p>
        </div>
      </div>
    <p class="splash-trademark">${messages['splashTrademark']}</p>
    </div>
    <div class="splash-footer">
      <img class="splash-rivm" src="<%=ImageInliner.inline("assets/images/splash/rivm-logo-nederlands.svg")%>" alt="RIVM"></img>
      <p class="splash-attribution">${messages['splashAttribution']}</p>
      <div class="splash-provinces">
        <img src="<%=ImageInliner.inline("assets/images/splash/province/Groningen.svg")%>" alt="Provincie Groningen"></img>
        <img src="<%=ImageInliner.inline("assets/images/splash/province/Friesland.svg")%>" alt="Provincie Friesland"></img>
        <img src="<%=ImageInliner.inline("assets/images/splash/province/Drenthe.svg")%>" alt="Provincie Drenthe"></img>
        <img src="<%=ImageInliner.inline("assets/images/splash/province/Overijssel.svg")%>" alt="Provincie Overijssel"></img>
        <img src="<%=ImageInliner.inline("assets/images/splash/province/Gelderland.svg")%>" alt="Provincie Gelderland"></img>
        <img src="<%=ImageInliner.inline("assets/images/splash/province/Utrecht.svg")%>" alt="Provincie Utrecht"></img>
        <img src="<%=ImageInliner.inline("assets/images/splash/province/Noord-Holland.svg")%>" alt="Provincie Noord-Holland"></img>
        <img src="<%=ImageInliner.inline("assets/images/splash/province/Zuid-Holland.svg")%>" alt="Provincie Zuid-Holland"></img>
        <img src="<%=ImageInliner.inline("assets/images/splash/province/Zeeland.svg")%>" alt="Provincie Zeeland"></img>
        <img src="<%=ImageInliner.inline("assets/images/splash/province/Noord-Brabant.svg")%>" alt="Provincie Noord-Brabant"></img>
        <img src="<%=ImageInliner.inline("assets/images/splash/province/Limburg.svg")%>" alt="Provincie Limburg"></img>
        <img src="<%=ImageInliner.inline("assets/images/splash/province/Flevoland.svg")%>" alt="Provincie Flevoland"></img>
      </div>
    </div>
