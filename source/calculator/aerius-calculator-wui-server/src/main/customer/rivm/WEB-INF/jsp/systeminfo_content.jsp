<%@page import="java.util.Locale,nl.overheid.aerius.util.LocaleUtils"
%><h1 class="splash-title">${titleHtml}</h1>
  <div class="message loginform">
    <div class="text-left">
      <h2>${messages['systemInfoEditorTitle']}</h2>
      <p>${messages['systemInfoEditorBody']}</p>
    </div>
    <div class="text-right">
      <form method="post" action="#">
        <input type="text" id="gwt-debug-message" name="message" placeholder="${messages['systemInfoEditorMessage']}" style="display:block;width:1024px"/>
        <select id="gwt-debug-locale" name="locale" placeholder="${messages['systemInfoEditorLocale']}">
        <% for (final Locale lo : LocaleUtils.KNOWN_LOCALES) {%>
          <option value="<%= lo.getLanguage() %>"><%=lo.getDisplayName() %></option>
        <% } %>
        </select>
        <p><input type="checkbox" id="gwt-debug-delete-all" name="clearmessage" placeholder="info" value="on" /> ${messages['systemInfoEditorClearMessage']}</p>
        <input type="text" maxlength="36" id="gwt-debug-uuid" name="uuid" placeholder="${messages['systemInfoEditorPasskey']}" />
        <input type="submit" id="gwt-debug-submit" value="${messages['systemInfoEditorSubmit']}" />
      </form>
      <% if (request.getAttribute("isSuccess") != null) { %>
        <p>${messages['systemInfoEditorSentText']}</p>
      <% } %>
      <% if (request.getAttribute("isError") != null) { %>
        <p style="color:red">${messages['systemInfoEditorErrorText']}</p>
      <% } %>
    </div>
  </div>
