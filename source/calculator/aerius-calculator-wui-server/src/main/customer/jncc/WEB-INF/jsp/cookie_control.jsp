<!-- Civic Cookie control -->
<script>
var cookiePolicyShouldOpen = false;
function toggleCookieNotice() {
  cookiePolicyShouldOpen = true;
}
function AerCookiePanel() {
  this.showCookieNotice = () => cookiePolicyShouldOpen;
}
</script>
