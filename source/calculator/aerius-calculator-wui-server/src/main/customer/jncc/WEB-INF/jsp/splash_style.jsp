<meta name="theme-color" content="blue">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="icon" sizes="48x48" href="/assets/images/favicon.ico">
<link rel="icon" sizes="any" href="/assets/images/favicon.svg" type="image/svg+xml">
<link rel="mask-icon" href="/assets/images/govuk-icon-mask.svg" color="#0b0c0c">
<link rel="apple-touch-icon" href="/assets/images/govuk-icon-180.png">
<link rel="manifest" href="/assets/manifest.json">
<link href="styles/all.css" rel="stylesheet">
<link rel="stylesheet" href="assets/fonts/noto-sans-tc-v26-latin.css?v=<%=timestamp%>" type="text/css">
<link rel="stylesheet" href="assets/fonts/gds-transport.css?v=<%=timestamp%>" type="text/css">
<style>
:root {
  --external-header: 106px;
}

.aerius-loaded > .govuk-phase-banner {
  max-width: 100% !important;
}

</style>
