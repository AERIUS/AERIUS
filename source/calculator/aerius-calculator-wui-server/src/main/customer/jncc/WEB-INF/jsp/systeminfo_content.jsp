<%@page import="java.util.Locale,nl.overheid.aerius.util.LocaleUtils"
%><div class="govuk-width-container app-width-container">
    <main class="govuk-main-wrapper app-main-class" id="main-content" role="main">
      <div class="govuk-grid-row" style="margin:0">
        <div>
          <h1 class="govuk-heading-xl ">${titleHtml}</h1>
          <h2 class="govuk-heading-m" id="splash-loader">${messages['systemInfoEditorTitle']}</h2>
          <p class="govuk-body">${messages['systemInfoEditorBody']}</p>

            <form method="post" action="#">
              <div class="govuk-form-group">
                <input class="govuk-input" type="text" id="gwt-debug-message" name="message" placeholder="${messages['systemInfoEditorMessage']}" style="display:block;width:1024px"/>
              </div>
              <div class="govuk-form-group">
                <select class="govuk-select" id="gwt-debug-locale" name="locale" placeholder="${messages['systemInfoEditorLocale']}">
                <% for (final Locale lo : LocaleUtils.KNOWN_LOCALES) {%>
                  <option value="<%= lo.getLanguage() %>" <%=lo.equals(LocaleUtils.getDefaultLocale()) ? "selected" : ""%>><%=lo.getDisplayName() %></option>
                <% } %>
                </select>
              </div>
              <div class="govuk-form-group">
                <div class="govuk-checkboxes__item">
                  <input class="govuk-checkboxes__input" type="checkbox" id="gwt-debug-delete-all" name="clearmessage" placeholder="info" value="on" />
                  <label class="govuk-label govuk-checkboxes__label" for="gwt-debug-delete-all">
                   ${messages['systemInfoEditorClearMessage']}</p>
                  </label>
                </div>
              </div>
              <div class="govuk-form-group">
                <input class="govuk-input" type="text" maxlength="36" id="gwt-debug-uuid" name="uuid" placeholder="${messages['systemInfoEditorPasskey']}" />
              </div>
              <div class="govuk-form-group">
                <input class="govuk-button" type="submit" id="gwt-debug-submit" value="${messages['systemInfoEditorSubmit']}" />
              </div>
            </div>
           </form>
        </div>
        <% if (request.getAttribute("isSuccess") != null) { %>
          <p class="govuk-body">${messages['systemInfoEditorSentText']}</p>
        <% } %>
        <% if (request.getAttribute("isError") != null) { %>
          <p class="govuk-error-message">${messages['systemInfoEditorErrorText']}</p>
        <% } %>
        </div>
      </div>
    </main>
  </div>
