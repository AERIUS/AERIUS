<%@page import="nl.overheid.aerius.server.util.ImageInliner"%>
  <div id="base">
    <div class="govuk-width-container app-width-container">
    <main class="govuk-main-wrapper app-main-class" id="main-content" role="main">
      <div class="govuk-grid-row app-masthead" style="margin:0">
        <div class="govuk-grid-column-one-third">
          <img class="splash-graph" src="<%=ImageInliner.inline("assets/images/splash/aerius-splash-illustration-" + productName + ".svg")%>"></img>
        </div>
        <div class="govuk-grid-column-two-thirds">
          <h1 class="govuk-heading-xl app-masthead__title">${titleHtml}</h1>
          <h2 class="govuk-heading-m app-masthead__title" id="splash-loader">${messages['splashLoadingTitleText']}</h2>
          <p class="govuk-body app-masthead__title" id="splash-message">${splashLoadingText}</p>
          <div style="display:none" id="splash-error">
            <p class="govuk-heading-m app-masthead__title">ERROR</p>
            <p class="govuk-body app-masthead__title govuk-!-padding-bottom-9">Application failed to load.</p>
            <p class="govuk-body app-masthead__title govuk-!-padding-top-9">For help, please refer to the <a class="govuk-body app-masthead__title govuk" style="color:#fff" href="https://prototype.aerius.uk/contact">Contact</a> page</p>
          </div>
        </div>
      </div>
    </main>
  </div>
