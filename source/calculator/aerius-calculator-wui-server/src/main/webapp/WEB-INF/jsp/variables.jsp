<%@page import="java.util.Date,java.util.Locale,nl.overheid.aerius.util.LocaleRequester,nl.overheid.aerius.server.i18n.AeriusMessages,
java.sql.Connection,nl.overheid.aerius.shared.constants.SharedConstantsEnum,nl.overheid.aerius.shared.domain.ProductProfile,
nl.overheid.aerius.enums.ConstantsEnum,nl.overheid.aerius.shared.domain.AeriusCustomer,nl.overheid.aerius.server.util.ProductProfileUtil,
javax.servlet.http.HttpSession,nl.overheid.aerius.db.common.ConstantRepository,nl.overheid.aerius.server.service.ServerPMF"
%><%
final Locale locale = LocaleRequester.initLocale(request, response);
String customer;
try (final Connection con = ServerPMF.getInstance().getConnection()) {
  customer = ConstantRepository.getEnum(con, ConstantsEnum.CUSTOMER, AeriusCustomer.class).name().toLowerCase();
} catch (final java.sql.SQLException e) {
  customer = "rivm";
}
final AeriusMessages messages = new AeriusMessages(customer, locale);
final String lang = locale.getLanguage();
final long timestamp = new Date().getTime();
final ProductProfile productProfile = ProductProfileUtil.getProductProfile(request);
final String productName = productProfile.getProductName().toLowerCase();

session.setAttribute("messages", messages);
session.setAttribute("customer", customer);
session.setAttribute("productName", productName);
session.setAttribute("title", messages.getString("title" + productProfile.getProductName()));
session.setAttribute("titleHtml", messages.getString("titleHtml" + productProfile.getProductName()));
session.setAttribute("splashLoadingText", messages.getString("splashLoadingText"));
boolean loading = true;
%>
