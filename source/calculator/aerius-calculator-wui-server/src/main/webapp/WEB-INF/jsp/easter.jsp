<%@page import="java.sql.Connection"
        import="java.sql.SQLException"
        import="nl.overheid.aerius.shared.constants.SharedConstantsEnum"
        import="nl.overheid.aerius.db.common.ConstantRepository"
        import="nl.overheid.aerius.server.service.ServerPMF"
%>
<%-- Load easter.js dynamically after OpenLayers is fully initialized. This avoids race conditions where the script might
  run before OpenLayers is ready. The default script loading behavior and 'defer' attribute are insufficient because
  OpenLayers does not initialize synchronously. --%>
<%
boolean easterModuleEnable = false;
try (final Connection con = ServerPMF.getInstance().getConnection()) {
    easterModuleEnable = ConstantRepository.getBoolean(con, SharedConstantsEnum.EASTER_MODULE_ENABLE, false);
} catch (final SQLException e) {
    throw new IllegalArgumentException(e);
}
%>
<% if (easterModuleEnable) { %><script src="components/easter.js?v=<%=timestamp%>"></script><% } %>
