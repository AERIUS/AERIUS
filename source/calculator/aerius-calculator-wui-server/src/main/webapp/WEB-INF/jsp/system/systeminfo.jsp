<%@include file="../variables.jsp"
%><%final String feedbackUrl;
try (final Connection con = ServerPMF.getInstance().getConnection()) {
  final String jiraCollectorId = ConstantRepository.getString(con, SharedConstantsEnum.COLLECT_COLLECTOR_ID);
  feedbackUrl = ConstantRepository.getString(con, SharedConstantsEnum.COLLECT_FEEDBACK_URL, false) + jiraCollectorId;
} catch (final java.sql.SQLException e) {
  throw new IllegalArgumentException(e);
}
%><!DOCTYPE html>
<html lang="<%=lang%>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>${messages['systemInfoEditorPageTitle']}</title>
<%@include file="/WEB-INF/jsp/header.jsp" %>
<%@include file="/WEB-INF/jsp/systeminfo_content.jsp" %>
<%@include file="/WEB-INF/jsp/footer.jsp" %>
