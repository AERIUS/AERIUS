(function () {
  const interval = 50,
    timeout = 10_000;
  const attemptLoadEaster = (retries = 0) => {
    if (typeof ol === "undefined" && retries < timeout / interval) {
      return setTimeout(() => attemptLoadEaster(retries + 1), interval);
    }
    typeof ol !== "undefined"
      ? document.head.appendChild(
          Object.assign(document.createElement("script"), {
            src: "https://nexus.aerius.nl/repository/js-releases/easter/1.0.2/easter.js",
          })
        )
      : console.warn(
          `Failed to load easter.js: OpenLayers not available after ${timeout} seconds`
        );
  };
  attemptLoadEaster();
})();
