class TimeVaryingProfileGraphComponent extends HTMLElement {
  constructor() {
    super();

    this.data = [];
    this.type = "";

    this.width = 300;
    this.height = 150;

    this.i18n = {
      "nl": {
        weekday: "Weekdag",
        saturday: "Zaterdag",
        sunday: "Zondag",
        monthly: "Maandelijks",
        months: Array.from({ length: 12 }, (_, month) => new Intl.DateTimeFormat('nl-NL', { month: 'short' }).format(new Date(2024, month, 1)))
      },
      "en": {
        weekday: "Weekday",
        saturday: "Saturday",
        sunday: "Sunday",
        monthly: "Monthly",
        months: Array.from({ length: 12 }, (_, month) => new Intl.DateTimeFormat('en-GB', { month: 'short' }).format(new Date(2024, month, 1)))
      }
    }

    this.locale = "nl";

    this.root = this.attachShadow({ mode: 'open' });
    this.root.innerHTML = `
      <style>
      :host {
        display: flex;
        flex-direction: column;
      }
      .graph {
        flex-grow: 1;
        margin: 20px 20px 20px 35px;
        overflow: visible;
      }
      .domain {
        stroke: none;
      }
      .tick > line {
        stroke: white;
        stroke-width: 1px;
      }
      .legend {
        display: flex;
        align-self: start;
        gap: var(--spacer);
        margin: var(--spacer);
      }
      .item {
        display: flex;
        align-items: center;
        gap: var(--half-spacer);
      }
      .box {
        width: 24px;
        height: 4px;
      }
      </style>`;

    this.svg = d3.create("svg")
      .classed("graph", true);
    this.svg.append("g")
      .classed("xAxis", true)
      .style("font-size", "var(--font-size)")
      .style("font-family", "var(--font-family)");
    this.svg.append("g")
      .classed("yAxis", true)
      .style("font-size", "var(--font-size)")
      .style("font-family", "var(--font-family)");

    this.legend = d3.create("div")
      .classed("legend", true);

    this.root.host.onresize = e => this.resize();
    this.root.appendChild(this.svg.node());
    this.root.appendChild(this.legend.node());
  }

  xFormat(d) {
    return d.toLocaleString(this.locale, { maximumSignificantDigits: 3 });
  }

  drawBarGraph() {
    this.width = this.overrideWidth ?? this.svg.node().clientWidth;
    this.height = this.overrideHeight ?? this.svg.node().clientHeight;

    let lines;
    if (this.type === 'MONTHLY') {
      lines = new Array(1)
      lines[0] = { key: "monthly", color: "#a1449d", values: this.data };
    } else {
      lines = new Array(3)
      lines[0] = { key: "weekday", color: "#e17000", values: this.data.slice(0, 24).concat(this.data.slice(0, 1)) };
      lines[1] = { key: "saturday", color: "#01689b", values: this.data.slice(24, 48).concat(this.data.slice(24, 25)) };
      lines[2] = { key: "sunday", color: "#275937", values: this.data.slice(48, 72).concat(this.data.slice(48, 49)) };
    }
    let x;
    let xAxis;

    if (this.type === 'MONTHLY') {
      x = d3.scaleLinear()
        .domain([0, 11])
        .range([ 0, this.width ]);
      xAxis = d3.axisBottom(x)
        .tickFormat((d, idx) => this.i18n[this.locale]["months"][idx])
        .ticks(12)
        .tickSize(-this.height);
    } else {
      x = d3.scaleLinear()
        .domain([0, 24])
        .range([ 0, this.width ]);
      xAxis = d3.axisBottom(x)
        .tickFormat((d, idx) => idx % 2 == 0 ? d : "")
        .ticks(24)
        .tickSize(-this.height);
    }
    this.svg.selectAll("g.xAxis")
      .transition()
      .attr("transform", "translate(0," + this.height + ")")
      .call(xAxis)
      .selectAll(".tick text")
      .attr("y", "0.5em");

    let graphMax = Math.max(2.5, d3.max(lines.flatMap(v => v.values)));

    // Y values
    let y = d3.scaleLinear()
      .domain([0, graphMax])
      .range([this.height, 0]);
    let yAxis = d3.axisLeft(y)
      .tickFormat(d => d)
      .ticks(5)
      .tickSize(-this.width);
    // Draw y axis
    this.svg.selectAll("g.yAxis")
      .transition()
      .call(yAxis)
      .selectAll("text")
      .attr("text-anchor", "start")
      .attr("x", "-30");

    // Draw the legend
    let legendSelection = this.legend.selectAll(".item")
      .data(lines, d => d.key);

    let items = legendSelection
      .enter()
      .append("div")
      .classed("item", true);
    items.append("div")
      .style("background", d => d.color)
      .classed("box", true);
    items.append("div")
      .classed("label", true)
      .html(d => this.i18n[this.locale][d.key]);
    legendSelection.exit().remove();

    // Draw the line
    let lineSelection = this.svg.selectAll(".line")
      .data(lines, d => d.key);

    let line = d3.line()
      .curve(d3.curveMonotoneX)
      .x((dx, idx) => x(idx))
      .y(dx => y(dx));

    lineSelection
      .enter()
      .append("path")
      .classed("line", true)
      .attr("d", d => line(d.values))
      .merge(lineSelection)
      .transition()
      .attr("fill", "none")
      .attr("id", d => "time-varying-graph-line-" + d.key)
      .attr("stroke", d => d.color)
      .attr("stroke-width", 2)
      .attr("d", d => line(d.values));

     lineSelection.exit().remove();
  }

  resize() {
    this.drawBarGraph();
  }

  handleData(data) {
    this.data = JSON.parse("[" + data + "]");

    this.drawBarGraph();
  }

  attributeChangedCallback(name, oldValue, newValue) {
    switch (name) {
      case 'locale':
        this.locale = newValue;
        break;
      case 'type':
        this.type = newValue;
        break;
      case 'data':
        setTimeout(() => this.handleData(newValue), 1);
        break;
      case 'width':
        this.overrideWidth = newValue;
        break;
      case 'height':
        this.overrideHeight = newValue;
        break;
    }
  }

  static get observedAttributes() {
    return ['locale', 'data', 'type', 'width', 'height'];
  }
}

customElements.define('time-varying-profile-graph-component', TimeVaryingProfileGraphComponent);
