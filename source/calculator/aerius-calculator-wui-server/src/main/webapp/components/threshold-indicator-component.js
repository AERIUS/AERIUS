class ThresholdIndicatorComponent extends HTMLElement {
  threshold = 100;
  thresholdMax = 200;

  constructor() {
    super();

    this.colorUnderThreshold = "#f19b55";
    this.colorOverThreshold = "#7999E4";
    this.percentage = 0;
    this.locale = "nl";

    const root = this.attachShadow({mode: 'open'});
    root.innerHTML = `<style>
    .container {
      display: flex;
      justify-items: center;
      flex-direction: column;
      margin: 0px 10px 0px 10px;
    }

    .graphContainer {
      height: 3em;
      display: flex;
      align-items: center;
    }

    .scoreBar {
      flex-grow: 1;
      position: relative;
      z-index: 1;
      height: 4px;
      background: #999999;
      border-radius: 5px;
    }

    .indicator {
      position: absolute;
      left: 50%;
      transform: translate(-50%, 0);
      width: 2px;
      background: #999999;
      height: 2.5em;
      margin-top: -17px;
      border-radius: 5px;
    }

    .score {
      width: 2em;
      height: 4px;
      position: relative;
      display: flex;
      align-items: center;
      justify-content: center;
      z-index: 0;

      color: white;
      font-weight: bold;
      font-size: 1em;
      line-height: 2em;
    }

    .score::before {
      content: '';
      display: block;
      position: absolute;
      z-index: -1;

      width: 2em;
      height: 2em;
      border-radius: 50%;

      background: inherit;
    }

    .gt999::before {
      width: 2.4em;
      height: 2.4em;
    }
    `;

    this.container = d3.create("div")
      .classed("container", true);

    this.graphContainer = this.container
      .append("div")
      .classed("graphContainer", true);

    this.thresholdContainer = this.graphContainer
      .append("div")
      .classed("scoreBar", true);
    this.thresholdIndicator = this.thresholdContainer
      .append("div")
      .classed("indicator", true);
    this.thresholdSpan = this.thresholdContainer
      .append("div")
      .classed("score", true);

    root.appendChild(this.container.node());
  }

  form(d) {
    return Number(d).toLocaleString(this.locale, {minimumFractionDigits: 0, maximumFractionDigits: 0});
  }

  redraw() {
    let xPerc = Math.min((this.percentage / this.thresholdMax) * 100, 100);

    let gt999 = Number(this.percentage) > 999;
    let prefix = gt999 ? ">" : "";
    // Update the score text
    let maxScoreUpdate = this.thresholdSpan
      .datum(this.percentage)
      .style("background", d => d >= this.threshold ? this.colorOverThreshold : this.colorUnderThreshold)
      .style("left", d => "calc(" + xPerc + "% - 1em)")
      .classed("gt999", gt999)
      .text(prefix + this.form(gt999 ? 999 : this.percentage));
  }

  // Only called for the disabled and open attributes due to observedAttributes
  attributeChangedCallback(name, oldValue, newValue) {
    switch (name) {
    case 'deposition':
    case 'percentage':
      this.percentage = Number(newValue);
      this.redraw();
      break;
    case 'colorunderthreshold':
      this.colorUnderThreshold = newValue;
      this.redraw();
      break;
    case 'coloroverthreshold':
      this.colorOverThreshold = newValue;
      this.redraw();
      break;
    case 'locale':
      this.locale = newValue;
      this.redraw();
      break;
    }
  }

  static get observedAttributes() {
    return ['deposition','percentage','colorunderthreshold','coloroverthreshold', 'locale'];
  }
}

customElements.define('threshold-indicator-component', ThresholdIndicatorComponent);
