class DepositionGraphElement extends HTMLElement {
	// Default colors mostly to show what those should look like, and to have something initialized
  static get DEFAULT_COLORS() {
    return [{
      "lowerBound": 0,
      "color": "#fffdb3"
    }, {
      "lowerBound": 5,
      "color": "#fde76a"
    }, {
      "lowerBound": 10,
      "color": "#feb66e"
    }];
  }

  constructor() {
    super();

    const MAX_NUMBER_OF_VALUES = 13;
    const WIDTH = 770;
    const HEIGHT = 280;
    const GRAPH_MARGIN_TOP = 50;
    const GRAPH_MARGIN_RIGHT = 50;
    const GRAPH_MARGIN_BOTTOM = 0;
    const GRAPH_MARGIN_LEFT = 50;
    this.barXMargin = 1;
    this.selectionBarXMargin = 6;

    this.maxNumberOfValues = MAX_NUMBER_OF_VALUES;
    this.colorsForMaxValues = DepositionGraphElement.DEFAULT_COLORS;

    this.d3Data = [];
    this.selectedLowerBounds = new Set();

    this.width = WIDTH;
    this.height = HEIGHT;

    this.xLabel = "";
    this.yLabel = "";
    this.locale = "nl";
    this.yaxisfactor = 10000;

    this.selectionBars = null;

    this.root = this.attachShadow({ mode: 'open' });
    this.root.innerHTML = `
      <style>
        .svg {
          height: 320px;
          background: #ffffff;
          padding: 10px;
          padding-top: 30px;
          overflow: visible;
        }

        .graph-background {
          fill: #f3f3f3;
        }

        .selection-bar {
          cursor: pointer;
          outline-offset: -10px;
        }
        .selection-bar:focus {
          outline: 2px dotted var(--theme-color);
        }
        .selection-bar:hover {
          outline: 2px solid black;
        }
        text {
          font-size: var(--font-size);
          font-family: var(--font-family);
        }

        .y-axis .tick > line {
          color: #ffffff;
        }
       </style>`;

    this.graphMargin = {
      top: GRAPH_MARGIN_TOP,
      right: GRAPH_MARGIN_RIGHT,
      bottom: GRAPH_MARGIN_BOTTOM,
      left: GRAPH_MARGIN_LEFT
    };

    this.svg = d3.create("svg")
      .classed("svg", true)
      .attr("width", this.width);

    this.root.host.onresize = e => this.resize();
    this.root.appendChild(this.svg.node());

    this.addGraphElementsToSvg();
    this.addLabelElementsToSvg();
  }

  isPositiveOnly() {
    return this.colorsForMaxValues[0].lowerBound == 0;
  }

  updateColorRanges(colorsForMaxValues) {
    this.colorsForMaxValues = JSON.parse(colorsForMaxValues, (key, value) => {
      if (value === "-Infinity") {
        return -Infinity;
      }
      return value === "Infinity" ? Infinity : value;
    });
    this.drawBarGraph();
  }

  addGraphElementsToSvg() {
    let graph = this.svg.append("g")
      .classed("graph", true);

    // Background
    graph.append('rect')
      .classed("graph-background", true)
      .attr("x", this.graphMargin.left)
      .attr("y", this.graphMargin.top)
      .attr("width", this.width - this.graphMargin.left - this.graphMargin.right)
      .attr("height", this.height - this.graphMargin.top - this.graphMargin.bottom);

    // X-axis
    graph.append("g")
      .classed("x-axis", true)
      .style("font-size", "var(--font-size)")
      .style("font-family", "var(--font-family)");

    // Y-axis
    graph.append("g")
      .classed("y-axis", true)
      .style("font-size", "var(--font-size)")
      .style("font-family", "var(--font-family)");


    // Color bars
    graph.append("g")
      .classed("bar-container", true);


    // Select bars
    graph.append("g")
      .classed("select-bar-container", true);
  }

  addLabelElementsToSvg() {
    // X-axis label
    this.xLabel = this.svg.append("g")
      .append("text")
      .attr("x", this.graphMargin.left)
      .attr("y", this.height + 50);

    // Y-axis label
    this.yLabel = this.svg.append("g")
      .append("text")
      .attr("x", 0)
      .attr("y", 20);
  }

  xFormat(d) {
    // Set The various kinds of infinity to an empty string
    if (d == Infinity || d == -Infinity
      || d == "Infinity" || d == "-Infinity") {
      return "";
    }

    return d.toLocaleString(this.locale, { maximumSignificantDigits: 3 });
  }

  drawBarGraph() {
    let me = this;
    this.svg.selectAll("g.bar-container").selectAll("rect").remove();
    this.svg.selectAll("g.select-bar-container").selectAll("rect").remove();

    let dataLength = this.d3Data.length;
    let barWidth = ((this.width - (this.graphMargin.left + this.graphMargin.right)) / dataLength) - (this.barXMargin * 2);

    let xValues = this.d3Data.map(graphData => graphData.lowerBound);
    xValues.push(Infinity);
    let yValues = this.d3Data.map(graphData => graphData.cartographicSurface / this.yaxisfactor);

    // X values
    let x = d3.scalePoint()
      .domain(xValues)
      .range([this.graphMargin.left, this.width - this.graphMargin.right]);
    let xAxis = d3.axisBottom(x)
      .tickFormat(d => this.xFormat(d));

    // Y values
    let y = d3.scaleLinear()
      .domain([0, d3.max(yValues) * 1.2])
      .range([this.height, (this.graphMargin.top + this.graphMargin.bottom)])
      .nice(); // Rounds the domain max value
    let yAxis = d3.axisLeft(y)
      .tickSize((this.graphMargin.left + this.graphMargin.right) - this.width)
      .tickFormat(d => d.toLocaleString(this.locale, { maximumSignificantDigits: 3 }));

    // Draw x-axis
    this.svg.selectAll("g.x-axis > .tick").remove();
    let xAxisElement = this.svg.selectAll("g.x-axis")
      .transition()
      .attr("transform", "translate(0," + (this.height + 10) + ")")
      .call(xAxis);

    // Remove last tick text (Infinity)
    let ticks = this.svg.selectAll("g.x-axis > .tick");
    ticks.filter((d, i) => d == Infinity).remove();

    // Draw y-axis
    let yAxisElement = this.svg.selectAll("g.y-axis")
      .transition()
      .attr("transform", "translate(50,0)")
      .call(yAxis);

    // Hide ticks and domains
    xAxisElement.selectAll(".tick")
      .selectAll("line")
      .attr("display", "none");
    xAxisElement.selectAll(".domain")
      .attr("display", "none");
    yAxisElement.selectAll(".domain")
      .attr("display", "none");

    // Draw bars
    let barContainer = this.svg.selectAll("g.bar-container");

    // Bars
    barContainer.selectAll("g")
      .data(this.d3Data)
      .enter()
      .append("rect")
      .attr("x", d => x(d.lowerBound) + this.barXMargin)
      .attr("y", d => y(d.cartographicSurface / this.yaxisfactor))
      .attr("width", barWidth)
      .attr("height", d => y(this.graphMargin.bottom) - y(d.cartographicSurface / this.yaxisfactor))
      .attr("fill", d => {
        let range = this.colorsForMaxValues.find(colorForMaxValue => colorForMaxValue.lowerBound == d.lowerBound);
        return range == null ? "#fff" : range.color;
      });

    // Select bars
    let selectBarContainer = this.svg.selectAll("g.select-bar-container");

    let width = barWidth + (2 * this.selectionBarXMargin);
    let height = this.height - this.graphMargin.top - this.graphMargin.bottom + (this.selectionBarXMargin * 2);

    let selectionGap = 1;
    let thing = this.selectionBarXMargin + selectionGap;

    this.selectionBars = selectBarContainer.selectAll("g")
      .data(this.d3Data)
      .enter()
      .append("rect")
      .classed("selection-bar", true)
      .attr("tabindex", "0")
      .attr("role", "button")
      .attr("aria-label", d => {
        let nextRangeIdx = this.colorsForMaxValues.findIndex(colorForMaxValue => colorForMaxValue.lowerBound == d.lowerBound) + 1;
        if (nextRangeIdx < this.colorsForMaxValues.length) {
          let nextRange = this.colorsForMaxValues[nextRangeIdx];
          return d.lowerBound + " - " + nextRange.lowerBound;
        } else {
          return d.lowerBound;
        }
      })
      .attr("x", d => x(d.lowerBound) - this.selectionBarXMargin)
      .attr("y", this.graphMargin.top - this.selectionBarXMargin)
      .attr("width", width)
      .attr("height", height)
      .attr("fill-opacity", 0) // Only show outline (stroke)
      .attr("stroke", "var(--theme-color)")
      .attr("stroke-width", 6)
      .attr("stroke-dasharray", [0, width + height + thing, width - thing - thing, 0])
      .attr("stroke-opacity", 0) // Hide by default
      .on("keypress", function(e, d) {
        if(e.key === "Enter"
          || e.code === "Space") {
          me.onSelect(d, d3.select(this));
        }
      })
      .on("click", function(e, d) {
        me.onSelect(d, d3.select(this))
      });
  }

  onSelect(d, elem) {
    let lowerBound = parseFloat(d.lowerBound);
    if (!this.selectedLowerBounds.has(lowerBound)) { // Select
      this.selectedLowerBounds.add(lowerBound);

      elem
        .attr("stroke-opacity", d => 1)
        .attr("aria-selected", "true");

      this.root.host.dispatchEvent(new CustomEvent("onBarSelect", {
        detail: {
          lowerBound: lowerBound,
          upperBound: this.getUpperBound(this.d3Data.indexOf(d))
        }
      }));
    } else { // Deselect
      this.selectedLowerBounds.delete(lowerBound);

      elem
        .attr("stroke-opacity", d => 0)
        .attr("aria-selected", "false");

      this.root.host.dispatchEvent(new CustomEvent("onBarDeselect", {
        detail: {
          lowerBound: lowerBound,
          upperBound: this.getUpperBound(this.d3Data.indexOf(d))
        }
      }));
    }
  }

  getUpperBound(index) {
    if (index === this.d3Data.length - 1) {
      return Infinity;
    } else {
      return parseFloat(this.d3Data[index + 1].lowerBound);
    }
  }

  resize() {
    this.drawBarGraph();
  }

  handleCreateGraphRequest(d3Data) {
    this.d3Data = JSON.parse(d3Data.replaceAll('`', '"'));

    this.drawBarGraph();
  }

  refreshSelectedRanges(selectArrayString) {
    this.selectedLowerBounds = new Set();
    let selectedLowerBoundsStringArray = selectArrayString.substring(1, selectArrayString.length - 1).split(", ");

    if (selectArrayString !== "[]") {
      selectedLowerBoundsStringArray
          .forEach(elem => this.selectedLowerBounds.add(parseFloat(elem)));
    }

    let me = this;

    // There is no data yet and thus no selectionBars selection
    if (this.selectionBars == null) {
      return;
    }

    this.selectionBars.filter(function(d) {
      return me.selectedLowerBounds.has(parseFloat(d.lowerBound));
    }).attr("stroke-opacity", 1);

    this.selectionBars.filter(function(d) {
      return !me.selectedLowerBounds.has(parseFloat(d.lowerBound));
    }).attr("stroke-opacity", 0);
  }

  attributeChangedCallback(name, oldValue, newValue) {
    switch (name) {
      case 'locale':
        this.locale = newValue;
        break;
      case 'xaxislabel':
        this.xLabel.html(newValue);
        break;
      case 'yaxislabel':
        this.yLabel.html(newValue);
        break;
      case 'yaxisfactor':
        this.yaxisfactor = newValue;
        break;
      case 'data':
        this.handleCreateGraphRequest(newValue);
        break;
      case 'selectedranges':
        this.refreshSelectedRanges(newValue);
        break;
      case 'colorrangedefinition':
        this.updateColorRanges(newValue);
        break;
    }
  }

  static get observedAttributes() {
    return ['locale', 'xaxislabel', 'yaxislabel', 'yaxisfactor', 'data', 'selectedranges', 'colorrangedefinition'];
  }
}

customElements.define('deposition-graph-component', DepositionGraphElement);
