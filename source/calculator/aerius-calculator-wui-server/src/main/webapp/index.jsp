<%@page import="java.sql.Connection,nl.overheid.aerius.db.common.ConstantRepository,nl.overheid.aerius.server.service.ServerPMF,nl.overheid.aerius.shared.constants.SharedConstantsEnum"
%><%final boolean isDevelopmentEnvironment = request.getAttribute("development_environment") != null;
final String feedback = (String) request.getParameter("feedback");
final boolean jiraCollectorEnabled;
final String jiraCollectorId;
final String feedbackUrl;
try (final Connection con = ServerPMF.getInstance().getConnection()) {
  final String collectFeedbackKey = ConstantRepository.getString(con, SharedConstantsEnum.COLLECT_FEEDBACK_KEY, false);
  // If feedback parameter is given and COLLECT_FEEDBACK_KEY is  not set in db the feedback link is shown.
  // If COLLECT_FEEDBACK_KEY is set in db than parameter value of feedback must match value in database.
  // If COLLECT_FEEDBACK is set to true in db feedback is always enabled.
  final boolean isFeedback = feedback != null && (collectFeedbackKey == null || collectFeedbackKey.equals(feedback));
  jiraCollectorEnabled = isFeedback || ConstantRepository.getBoolean(con, SharedConstantsEnum.COLLECT_FEEDBACK);

  // Get JIRA collector settings
  jiraCollectorId = ConstantRepository.getString(con, SharedConstantsEnum.COLLECT_COLLECTOR_ID);
  feedbackUrl = ConstantRepository.getString(con, SharedConstantsEnum.COLLECT_FEEDBACK_URL, false) + jiraCollectorId;
} catch (final java.sql.SQLException e) {
  throw new IllegalArgumentException(e);
}
%><%@include file="WEB-INF/jsp/variables.jsp"
%><!DOCTYPE html>
<html lang="<%=lang%>">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="noindex,nofollow">
<meta name="aerius:productProfile" content="<%=productProfile%>">
<meta name="gwt:property" content="locale=<%=lang%>">
<title>${title}<%= isDevelopmentEnvironment ? " (DEVELOPMENT MODE)" : "" %></title>
<base href="${pageContext.request.contextPath}/">
<link rel="stylesheet" href="assets/fonts/icons.css?v=<%=timestamp%>" type="text/css">
<link rel="stylesheet" href="styles/base.css?v=<%=timestamp%>" type="text/css">
<link rel="stylesheet" href="styles/customer.css?v=<%=timestamp%>" type="text/css">
<link rel="stylesheet" href="styles/ol.css?v=<%=timestamp%>" type="text/css">
<%@include file="/WEB-INF/jsp/lib_includes.jsp"%>
<script src="scripts/config<%= isDevelopmentEnvironment ? "_dev" : "" %>.js?v=<%=timestamp%>" type="text/javascript"></script><%
if (jiraCollectorEnabled) { %>
  <%@include file="WEB-INF/jsp/collect_feedback.jsp"%>
<% }
%><%@include file="/WEB-INF/jsp/header.jsp" %>
<%@include file="/WEB-INF/jsp/splash.jsp" %>
<%@include file="/WEB-INF/jsp/footer.jsp" %>
