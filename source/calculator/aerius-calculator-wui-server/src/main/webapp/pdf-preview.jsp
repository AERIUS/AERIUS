<%@page import="java.util.Date,java.util.Locale,nl.overheid.aerius.util.LocaleRequester,nl.overheid.aerius.shared.domain.ProductProfile,nl.overheid.aerius.shared.domain.PdfProductType"
%><%final boolean isDevelopmentEnvironment = request.getAttribute("development_environment") != null;
final Locale locale = LocaleRequester.initLocale(request, response);
final String lang = locale.getLanguage();
final long timestamp = new Date().getTime();
final ProductProfile productProfile = PdfProductType.OWN2000_LBV_POLICY == PdfProductType.safeValueOf(request.getParameter("pdfProductType"))
    ? ProductProfile.LBV_POLICY : ProductProfile.CALCULATOR;%><!DOCTYPE html>
<html lang="<%=lang%>">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="aerius:productProfile" content="<%=productProfile%>">
<meta name="gwt:property" content="locale=<%=lang%>">
<base href="${pageContext.request.contextPath}/">
<link rel="icon shortcut" type="image/x-icon" href="favicon.ico">
<link rel="stylesheet" href="assets/fonts/noto-sans-tc-v26-latin.css?v=<%=timestamp%>" type="text/css">
<link rel="stylesheet" href="assets/fonts/noto-sans-v27-latin-italic.css?v=<%=timestamp%>" type="text/css">
<link rel="stylesheet" href="assets/fonts/icons.css?v=<%=timestamp%>" type="text/css">
<link rel="stylesheet" href="styles/base.css?v=<%=timestamp%>" type="text/css">
<link rel="stylesheet" href="styles/customer.css?v=<%=timestamp%>" type="text/css">
<link rel="stylesheet" href="styles/print.css?v=<%=timestamp%>" type="text/css">
<link rel="stylesheet" href="styles/customer-print.css?v=<%=timestamp%>" type="text/css">
<link rel="stylesheet" href="styles/ol.css?v=<%=timestamp%>" type="text/css">
<%@include file="/WEB-INF/jsp/lib_includes.jsp" %>
<script src="scripts/config<%= isDevelopmentEnvironment ? "_dev" : "" %>.js?v=<%=timestamp%>" type="text/javascript"></script>
</head>
<body style="margin:0px;height:100%;">
  <div id="base"></div>
</body>
</html>
