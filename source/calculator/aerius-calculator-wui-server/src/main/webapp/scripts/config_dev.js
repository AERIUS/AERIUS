function AerConfig() {
  this.connectBaseUrl = "/api";
  this.geoserverBaseUrl = "/geoserver-calculator/wms";
  this.production = false;
  this.applicationVersion = "DEV";
}
