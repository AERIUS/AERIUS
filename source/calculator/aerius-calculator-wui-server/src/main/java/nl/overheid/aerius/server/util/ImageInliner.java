/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

/**
 * Util class to read images to be inlined in a HTML page as data.
 */
public class ImageInliner {

  private static final String PNG = ".png";
  private static final String PNG_DATA = "data:image/png;base64,";
  private static final String SVG = ".svg";
  private static final String SVG_DATA = "data:image/svg+xml;base64,";

  /**
   * Reads the SVG or PNG image from the path in the package and returns it as Base64 encoded SVG or PNG data string.
   *
   * @param path relative path to SVG/PNG image
   * @return Base64 encoded SVG/PNG data string
   */
  public static final String inline(final String path) {
    final int lastIndexOf = path.lastIndexOf('.');
    if (lastIndexOf < 0) {
      return "";
    }
    final String extension = path.substring(lastIndexOf, path.length());

    if (SVG.equalsIgnoreCase(extension)) {
      return inline(path, SVG_DATA);
    } else if (PNG.equalsIgnoreCase(extension)) {
      return inline(path, PNG_DATA);
    } else {
      return "";
    }
  }

  private static final String inline(final String path, final String prefix) {
    try (final InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(path)) {
      return prefix + Base64.getEncoder().encodeToString(is.readAllBytes());
    } catch (final IOException e) {
      return "";
    }
  }
}
