/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;

import com.google.gwt.logging.server.RemoteLoggingServiceImpl;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.i18n.DBMessages;

/**
 * Listener that is run during startup of application and can be used to initialize static variables.
 */
@WebListener
public class StartupContextListener implements ServletContextListener {

  private static final Logger LOGGER = LoggerFactory.getLogger(StartupContextListener.class);

  @Override
  public void contextDestroyed(final ServletContextEvent event) {
  }

  @Override
  public void contextInitialized(final ServletContextEvent event) {
    // Initialize the localized DB messages
    DBMessages.init(getPMF());
    LOGGER.info("Translations initialized.");

    // Pre-fetches the application contexts and theme configurations.
    ContextLoader.initApplicationConfiguration(getPMF());
    LOGGER.info("Application contexts initialized");

    // Initialize the client side stacktrace deobfuscator symbolmap location.
    new RemoteLoggingServiceImpl().setSymbolMapsDirectory(event.getServletContext().getRealPath("/WEB-INF/deploy/application/symbolMaps/"));
    LOGGER.info("SymbolMapDirectory for GWT deobfuscator set.");
    // Ensure java.util.logging gets sent to slf4j (used in gwt's remote logging servlet)
    SLF4JBridgeHandler.removeHandlersForRootLogger();
    SLF4JBridgeHandler.install();
  }

  protected PMF getPMF() {
    return ServerPMF.getInstance();
  }
}
