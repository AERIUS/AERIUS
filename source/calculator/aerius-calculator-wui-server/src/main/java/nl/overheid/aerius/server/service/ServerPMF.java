/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.common.BasePMF;

/**
 * Persistence Manager Factory manages the database connection.
 */
public final class ServerPMF extends BasePMF {
  private static final String DATASOURCE_NAME = "java:comp/env/jdbc/AeriusDB";
  private static final Logger LOG = LoggerFactory.getLogger(ServerPMF.class);

  private static ServerPMF instance;

  private final DataSource ds;

  private ServerPMF() throws NamingException {
    final Context initCtx = new InitialContext();
    ds = (DataSource) initCtx.lookup(DATASOURCE_NAME);
  }

  /**
   * Returns the instance of the PMF.
   * @return instance
   */
  public static ServerPMF getInstance() {
    if (instance == null) {
      try {
        instance = new ServerPMF();
      } catch (final NamingException e) {
        LOG.error("[DBManager#getConnection] NamingException. Data source for database connection isn't correct" , e);
        throw new IllegalArgumentException(e);
      }
    }
    return instance;
  }

  /**
   * Returns a new connection to the AeriusDB database.
   *
   * @return a database connection
   * @throws SQLException When retrieving a connection fails.
   */
  @Override
  public Connection getConnection() throws SQLException {
    try {
      return ds.getConnection();
    } catch (final SQLException e) {
      LOG.error("[DBManager#getConnection] SQLException. Problem getting a connection.", e);
      throw e;
    }
  }

  @Override
  public String toString() {
    return "ServerPMF [ds=" + ds + "]";
  }
}
