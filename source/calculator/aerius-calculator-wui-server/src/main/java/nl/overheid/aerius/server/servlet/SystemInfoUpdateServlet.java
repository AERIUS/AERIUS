/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.i18n.SystemInfoMessageRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.server.service.ServerPMF;
import nl.overheid.aerius.shared.RequestMappings;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Servlet to update database fields to show or hide system messages. use url
 * post command to update the database value
 */
@WebServlet(RequestMappings.SYSTEM_INFO_UPDATE)
public class SystemInfoUpdateServlet extends HttpServlet {

  protected static final String WEB_INF_JSP_SYSTEM_SYSTEMINFO_JSP = "/WEB-INF/jsp/system/systeminfo.jsp";

  private static final long serialVersionUID = 1L;

  private static final Logger LOG = LoggerFactory.getLogger(SystemInfoUpdateServlet.class);

  private static final String CHECKBOX_SELECTED_VALUE = "on";
  private static final String UUID = "uuid";
  private static final String LOCALE = "locale";
  private static final String MESSAGE = "message";
  private static final String CLEAR_MESSAGE = "clearmessage";

  @Override
  protected void doGet(final HttpServletRequest request, final HttpServletResponse response) {
    try {
      if (request.getParameter(MESSAGE) == null) {
        renderJsp(request, response);
      } else {
        // Keep old servlet call to getting the message to still be able to show this message to browsers that were open
        // during the system update and still point to the old url.
        // This call can probable be removed in the next major update after this change was deployed.
        SystemInfoServlet.setMessage(getPmf(), request, response);
      }
    } catch (final RuntimeException e) {
      LOG.error("SystemMessageUpdateServlet doGet failed:", e);
    }
  }

  @Override
  protected void doPost(final HttpServletRequest request, final HttpServletResponse response) {
    try {
      try (final Connection con = getPmf().getConnection()) {
        final String uuid = ConstantRepository.getString(con, ConstantsEnum.SYSTEM_INFO_PASSKEY);
        final String requestedLocale = request.getParameter(LOCALE);
        final String newMessage = request.getParameter(MESSAGE);
        final String clearMessage = request.getParameter(CLEAR_MESSAGE);

        if (uuid == null || uuid.length() == 0) {
          LOG.info("SystemMessageUpdateServlet update not allowd no UUID in database update");
          setError(request);
        } else if (request.getParameter(UUID) != null && request.getParameter(UUID).equals(uuid)) {
          if (getClearMessageValue(clearMessage)) {
            SystemInfoMessageRepository.clearAllMessages(con);
          } else {
            SystemInfoMessageRepository.updateMessage(con, newMessage, LocaleUtils.getSupportedLocaleOrDefault(requestedLocale));
          }
          setSuccess(request);
        } else {
          setError(request);
        }
      } catch (final SQLException e) {
        LOG.error("SystemMessageUpdateServlet update system info message failed:", e);
        setError(request);
      }
      renderJsp(request, response);
    } catch (final RuntimeException e) {
      LOG.error("SystemMessageUpdateServlet doPost failed:", e);
    }
  }

  private static void renderJsp(final HttpServletRequest request, final HttpServletResponse response) {
    try {
      request.getRequestDispatcher(WEB_INF_JSP_SYSTEM_SYSTEMINFO_JSP).forward(request, response);
    } catch (final ServletException | IOException e) {
      LOG.error("SystemMessageUpdateServlet redirect to systeminfo.jsp failed:", e);
    }
  }

  private static boolean getClearMessageValue(final String checkBoxValue) {
    return CHECKBOX_SELECTED_VALUE.equalsIgnoreCase(checkBoxValue);
  }

  private static void setSuccess(final HttpServletRequest request) {
    request.setAttribute("isSuccess", true);
  }

  private static void setError(final HttpServletRequest request) {
    request.setAttribute("isError", true);
  }

  // protected to be able to override in unit test.
  protected PMF getPmf() {
    return ServerPMF.getInstance();
  }

}
