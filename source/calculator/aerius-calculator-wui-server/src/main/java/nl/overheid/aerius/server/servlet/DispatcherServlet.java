/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gwt.user.client.rpc.IncompatibleRemoteServiceException;
import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.server.rpc.RPC;
import com.google.gwt.user.server.rpc.RPCRequest;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import nl.overheid.aerius.server.service.AeriusSession;
import nl.overheid.aerius.server.service.CalculatorRPCServiceRegistry;
import nl.overheid.aerius.server.service.CalculatorSession;
import nl.overheid.aerius.server.service.ServiceRegistry;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.service.v2.ServiceURLConstants;

/**
 * The Dispatcher servlet is responsible for encoding/decoding rpc calls and forwarding them to the correct service.
 */
@WebServlet(urlPatterns = { ServiceURLConstants.DISPATCHER_SERVLET_MAPPING })
public class DispatcherServlet extends RemoteServiceServlet {
  private static final Logger LOG = LoggerFactory.getLogger(RemoteServiceServlet.class);

  private static final long serialVersionUID = 1L;

  protected ServiceRegistry registry;

  @Override
  public void init(final ServletConfig config) throws ServletException {
    super.init(config);

    final AeriusSession calculatorSession = new CalculatorSession() {
      @Override
      public HttpServletRequest getRequest() {
        return getThreadLocalRequest();
      }
    };
    try {
      registry = new CalculatorRPCServiceRegistry(calculatorSession);
    } catch (SQLException | AeriusException | NamingException e) {
      throw new ServletException(e);
    }
  }

  @Override
  public String processCall(final String payload) throws SerializationException {
    try {
      final HttpServletRequest request = getThreadLocalRequest();

      final Object service = registry.findService(request);

      if (service == null) {
        return RPC.encodeResponseForFailure(null, new NullPointerException("No service found for <" + request.getRequestURL() + ">."));
      }

      final RPCRequest rpcRequest = RPC.decodeRequest(payload, service.getClass(), this);
      onAfterRequestDeserialized(rpcRequest);

      return RPC.invokeAndEncodeResponse(service, rpcRequest.getMethod(), rpcRequest.getParameters(), rpcRequest.getSerializationPolicy());
    } catch (final IncompatibleRemoteServiceException ex) {
      LOG.error("Incompatible remote version for request: {}", payload, ex);
      return RPC.encodeResponseForFailure(null, ex);
    }
  }
}
