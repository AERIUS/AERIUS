/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.util;

import java.util.Set;
import java.util.function.Supplier;

import javax.servlet.http.HttpServletRequest;

import nl.overheid.aerius.shared.domain.ProductProfile;

/**
 * Util to determine the product profile
 */
public final class ProductProfileUtil {

  private static final String AERIUS_PRODUCT_PROFILE_ENV = "AERIUS_PRODUCT_PROFILE";
  private static final String PRODUCT_PROFILE_PARAMETER = "productProfile";
  private static final String HOST = "host";
  private static final Set<String> AERIUS_CHECK = Set.of("check", "regelingsondersteuning");
  private static final String LOCALHOST = "localhost";

  private ProductProfileUtil() {
    // Util class
  }

  /**
   * Gets the Product profile. If the environment variable is set the product type will be based on that.
   * Otherwise it looks at the host header variable and derives the product profile from the host name.
   * If the host name is localhost it will be based on the productProfile parameter passed in the url.
   *
   * @param request
   * @return ProductProfile
   */
  public static ProductProfile getProductProfile(final HttpServletRequest request) {
    return getProductProfile(request, ProductProfileUtil::getEnv);
  }

  static ProductProfile getProductProfile(final HttpServletRequest request, final Supplier<String> envSupplier) {
    final String ptEnv = envSupplier.get();

    if (ptEnv == null) {
      final String host = request.getHeader(HOST);
      final boolean local = host != null && host.contains(LOCALHOST);
      final ProductProfile pp = host != null && AERIUS_CHECK.stream().anyMatch(host::contains) ? ProductProfile.LBV_POLICY
          : ProductProfile.CALCULATOR;

      return local ? ProductProfile.safeValueOf(request.getParameter(PRODUCT_PROFILE_PARAMETER)) : pp;
    } else {
      return ProductProfile.safeValueOf(ptEnv);
    }
  }

  private static String getEnv() {
    return System.getenv(AERIUS_PRODUCT_PROFILE_ENV);
  }
}
