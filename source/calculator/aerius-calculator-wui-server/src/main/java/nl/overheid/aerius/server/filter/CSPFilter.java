/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.filter;

import java.io.IOException;
import java.util.Optional;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Filter class to add Content-Security-Policy header on web responses.
 */
public class CSPFilter implements Filter {

  private static final String POLICY_FORMAT = "default-src 'none'; "
      + "font-src 'self' *.aerius.nl *.aerius.uk;"
      + "script-src 'self' 'unsafe-eval' 'unsafe-inline' *.aerius.nl *.aerius.uk aerius.atlassian.net cc.cdn.civiccomputing.com www.googletagmanager.com *.virtualearth.net %s;" // Allow aerius.atlassian.net for feedback funcitonality, cc.cdn.civiccomputing.com for Cookie control functionality, www.googletagmanager.com for Google Tag manager and *.virtualearth scripts for bing rest api usage
      + "connect-src 'self' *.aerius.nl *.aerius.uk *.civiccomputing.com *.google-analytics.com *.analytics.google.com *.googletagmanager.com;" // Allow *.civiccomputing.com for Cookie Control functionality and several different ones for Google Tag manager
      + "img-src 'self' *.aerius.nl *.aerius.uk *.pdok.nl *.virtualearth.net *.rijkswaterstaat.nl data:;" // Allow *.pdok and *.rijkswaterstaat for dutch maps and *.virtualearth for UK map data.
      + "style-src 'self' 'unsafe-inline' *.aerius.nl *.aerius.uk;"
      + "base-uri 'self';"
      + "form-action 'self' *.aerius.nl *.aerius.uk aerius.atlassian.net;"
      + "frame-src 'self' aerius.atlassian.net;"
      + "manifest-src 'self';";
  protected static final String SUPER_DEV_MODE_PARAM = "superDevMode";
  protected static final String GWT_SUPER_DEV_MODE_ADDRESS = "localhost:9876";
  protected static final String CONTENT_SECURITY_POLICY_HEADER = "Content-Security-Policy";

  /**
   * Excluded paths from CSP
   * - The locally generated PDF
   */
  private static final Set<String> EXCLUDED_PATHS = Set.of("/print");

  private String policy;

  @Override
  public void init(final FilterConfig filterConfig) {
    final boolean superDevMode = Optional.ofNullable(filterConfig.getInitParameter(SUPER_DEV_MODE_PARAM))
        .map(Boolean::parseBoolean)
        .orElse(false);

    policy = String.format(POLICY_FORMAT, superDevMode ? GWT_SUPER_DEV_MODE_ADDRESS : "");
  }

  @Override
  public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain filterChain)
      throws ServletException, IOException {
    if (request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
      if (applyCsp((HttpServletRequest) request)) {
        ((HttpServletResponse) response).addHeader(CONTENT_SECURITY_POLICY_HEADER, policy);
      }
    }
    filterChain.doFilter(request, response);
  }

  @Override
  public void destroy() {
    // Do nothing
  }

  private static boolean applyCsp(final HttpServletRequest request) {
    return !EXCLUDED_PATHS.contains(request.getServletPath());
  }
}
