/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.filter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import nl.overheid.aerius.shared.Constants;

/**
 * Sets some security headers on the response
 */
public class SecurityHeadersFilter implements Filter {

  private static final Map<String, String> FIXED_HEADERS = new HashMap<>();
  static {
    FIXED_HEADERS.put("X-Frame-Options", "DENY");
    FIXED_HEADERS.put("Referrer-Policy", "strict-origin");
    FIXED_HEADERS.put("X-Content-Type-Options", "nosniff");
    FIXED_HEADERS.put("Strict-Transport-Security", "max-age=" + Constants.HSTS_MAX_AGE);
  }

  @Override
  public void init(final FilterConfig filterConfig) {
    // Do nothing
  }

  @Override
  public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain filterChain)
      throws ServletException, IOException {
    if (response instanceof final HttpServletResponse httpServletResponse) {
      for (final Map.Entry<String, String> header : FIXED_HEADERS.entrySet()) {
        httpServletResponse.addHeader(header.getKey(), header.getValue());
      }
    }
    filterChain.doFilter(request, response);
  }

  @Override
  public void destroy() {
    // Do nothing
  }
}
