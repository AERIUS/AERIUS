/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.server.service;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;

import nl.overheid.aerius.server.service.v2.LegacyContextServlet;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.service.v2.ServiceURLConstants;

/**
 *
 */
public class CalculatorRPCServiceRegistry implements ServiceRegistry {
  private final Map<String, Object> services = new HashMap<>();

  public CalculatorRPCServiceRegistry(final AeriusSession session) throws SQLException, AeriusException, NamingException {
    addService(ServiceURLConstants.CONTEXT_SERVICE_RELATIVE_PATH, new LegacyContextServlet(session));
  }

  protected void addService(final String key, final Object value) {
    services.put(key, value);
  }

  protected void addServices(final Map<String, Object> services) {
    this.services.putAll(services);
  }

  @Override
  public Object findService(final HttpServletRequest request) {
    final String servicePath = request.getPathInfo();

    return services.get(servicePath);
  }
}
