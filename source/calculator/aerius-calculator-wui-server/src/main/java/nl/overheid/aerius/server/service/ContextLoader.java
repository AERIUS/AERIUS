/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.common.configuration.ApplicationConfigurationInitializer;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.AeriusCustomer;
import nl.overheid.aerius.shared.domain.ProductProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.LocaleDBUtils;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Loads all language specific contexts into a static map.
 */
public class ContextLoader {
  private static final Logger LOGGER = LoggerFactory.getLogger(ContextLoader.class);
  private static final Map<AeriusCustomer, List<ProductProfile>> SUPPORTED_PRODUCT_PROFILES = Map.of(
      AeriusCustomer.RIVM, List.of(ProductProfile.CALCULATOR, ProductProfile.LBV_POLICY),
      AeriusCustomer.JNCC, List.of(ProductProfile.CALCULATOR));

  private static final Map<Locale, Map<ProductProfile, ApplicationConfiguration>> APP_CONFIGURATIONS = new HashMap<>();
  private static Locale defaultLocale;

  public static void initApplicationConfiguration(final PMF pmf) {
    defaultLocale = LocaleDBUtils.getDefaultLocale(pmf);
    LocaleUtils.setDefaultLocale(defaultLocale.getLanguage());
    try (Connection con = pmf.getConnection()) {
      final AeriusCustomer customer = AeriusCustomer.safeValueOf(ConstantRepository.getString(con, ConstantsEnum.CUSTOMER));

      for (final Locale locale : LocaleUtils.KNOWN_LOCALES) {
        final DBMessagesKey messagesKey = new DBMessagesKey(locale);
        final Map<ProductProfile, ApplicationConfiguration> productProfileConfigurations = new EnumMap<>(ProductProfile.class);

        for (final ProductProfile productProfile : SUPPORTED_PRODUCT_PROFILES.get(customer)) {
          productProfileConfigurations.put(productProfile,
              ApplicationConfigurationInitializer.createApplicationConfiguration(con, messagesKey, customer, productProfile));
        }
        APP_CONFIGURATIONS.put(locale, productProfileConfigurations);
      }
    } catch (SQLException | AeriusException e) {
      LOGGER.error("Failed to initialize application configuration", e);
    }
  }

  public static ApplicationConfiguration getConfiguration(final ProductProfile productProfile, final Locale locale) {
    final Map<ProductProfile, ApplicationConfiguration> productProfileConfigurations = APP_CONFIGURATIONS.getOrDefault(locale,
        APP_CONFIGURATIONS.get(defaultLocale));
    return productProfileConfigurations.getOrDefault(productProfile, productProfileConfigurations.get(ProductProfile.CALCULATOR));
  }
}
