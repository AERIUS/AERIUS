/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.AeriusVersion;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.i18n.SystemInfoMessageRepository;
import nl.overheid.aerius.server.service.ServerPMF;
import nl.overheid.aerius.shared.RequestMappings;
import nl.overheid.aerius.util.LocaleRequester;

/**
 * Servlet to get the system info message.
 */
@WebServlet(RequestMappings.SYSTEM_INFO)
public class SystemInfoServlet extends HttpServlet {

  private static final String SIMPLE_MESSAGE_RESPONSE = "{\"message\":\"%s\", \"applicationVersion\": \"%s\"}";

  private static final long serialVersionUID = 2L;

  private static final Logger LOG = LoggerFactory.getLogger(SystemInfoServlet.class);

  @Override
  protected void doGet(final HttpServletRequest request, final HttpServletResponse response) {
    try {
      setMessage(getPmf(), request, response);
    } catch (final RuntimeException e) {
      LOG.error("SystemInfoServlet doGet failed:", e);
    }
  }

  public static void setMessage(final PMF pmf, final HttpServletRequest request, final HttpServletResponse response) {
    try (final Connection con = pmf.getConnection()) {
      final String message = SystemInfoMessageRepository.getMessage(con, LocaleRequester.getLocale(request, response));
      final String applicationVersion = AeriusVersion.getVersionNumber();

      response.setContentType("application/json");
      response.getOutputStream().write(String.format(SIMPLE_MESSAGE_RESPONSE, message, applicationVersion).getBytes());
      response.getOutputStream().flush();
    } catch (final IOException | SQLException e) {
      LOG.error("SystemInfoServlet getting system info message failed:", e);
    }
  }

  // protected to be able to override in unit test.
  protected PMF getPmf() {
    return ServerPMF.getInstance();
  }

}
