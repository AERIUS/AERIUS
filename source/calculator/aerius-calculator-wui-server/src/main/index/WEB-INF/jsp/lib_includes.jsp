<script src="components/graham_scan.min.js?v=<%=timestamp%>" type="text/javascript" async></script>
<script src="components/turf-kinks.min.js?v=<%=timestamp%>" type="text/javascript" async></script>
<%@include file="/WEB-INF/jsp/easter.jsp" %>
<script src="webjars/openlayers/${openlayers.version}/ol.js" type="text/javascript" async></script>
<script src="webjars/proj4/${proj4.version}/dist/proj4.js" type="text/javascript" async></script>
<script src="webjars/vue/${vue.version}/dist/vue<%= isDevelopmentEnvironment ? "" : ".min" %>.js" type="text/javascript" async></script>
<script src="webjars/d3js/${d3.version}/d3.js" type="text/javascript" async></script>
<script src="webjars/github-com-vuelidate-vuelidate/${vuelidate.version}/vuelidate.min.js" type="text/javascript" async></script>
<script src="webjars/github-com-vuelidate-vuelidate/${vuelidate.version}/validators.min.js" type="text/javascript" async></script>
<script src="webjars/uuid/${uuid.version}/dist/umd/uuidv4.min.js" type="text/javascript" async></script>
<script src="webjars/uuid/${uuid.version}/dist/umd/uuidValidate.min.js" type="text/javascript" async></script>
<script src="webjars/jsts/${jsts.version}/dist/jsts.min.js" type="text/javascript" async></script>
<script src="application/application.nocache.js?v=<%=timestamp%>" type="text/javascript" defer></script>
