/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.js.scenario;

import java.io.Serializable;

public class ImportMetaData implements Serializable {
  private static final long serialVersionUID = 1L;

  private final String name;
  private final String year;
  private final String srm2Year;

  private ImportMetaData(
      final String name,
      final String year,
      final String srm2Year) {
    this.name = name;
    this.year = year;
    this.srm2Year = srm2Year;
  }

  public static Builder builder() {
    return new ImportMetaData.Builder();
  }

  public String name() {
    return name;
  }

  public String year() {
    return year;
  }

  public String srm2Year() {
    return srm2Year;
  }

  @Override
  public String toString() {
    return "ImportMetaData{"
        + "name=" + name + ", "
        + "year=" + year + ", "
        + "srm2Year=" + srm2Year
        + "}";
  }

  @Override
  public boolean equals(final Object o) {
    if (o == this) {
      return true;
    }
    if (o != null && o.getClass() == this.getClass()) {
      final ImportMetaData that = (ImportMetaData) o;
      return this.name.equals(that.name())
          && this.year.equals(that.year())
          && (this.srm2Year == null ? (that.srm2Year() == null) : this.srm2Year.equals(that.srm2Year()));
    }
    return false;
  }

  @Override
  public int hashCode() {
    int h$ = 1;
    h$ *= 1000003;
    h$ ^= name.hashCode();
    h$ *= 1000003;
    h$ ^= year.hashCode();
    h$ *= 1000003;
    h$ ^= (srm2Year == null) ? 0 : srm2Year.hashCode();
    return h$;
  }

  public static final class Builder {
    private String name;
    private String year;
    private String srm2Year;

    Builder() {
    }

    public ImportMetaData.Builder name(final String name) {
      if (name == null) {
        throw new NullPointerException("Null name");
      }
      this.name = name;
      return this;
    }

    public ImportMetaData.Builder year(final String year) {
      if (year == null) {
        throw new NullPointerException("Null year");
      }
      this.year = year;
      return this;
    }

    public ImportMetaData.Builder srm2Year(final String srm2Year) {
      this.srm2Year = srm2Year;
      return this;
    }

    public ImportMetaData build() {
      String missing = "";
      if (this.name == null) {
        missing += " name";
      }
      if (this.year == null) {
        missing += " year";
      }
      if (!missing.isEmpty()) {
        throw new IllegalStateException("Missing required properties:" + missing);
      }
      return new ImportMetaData(
          this.name,
          this.year,
          this.srm2Year);
    }
  }
}
