## Testing the API interfaces with Cypress

Running the tests of the web user interface with Cypress can be done in 2 different ways.
First run with Maven, and second with Node.js.
This page describes how to start the tests.
The tests require some parameters, and some are optional.
The following parameters are available:

| Parameter      | Description                                              | Required |
|----------------|----------------------------------------------------------|----------|
| `base_url`     | URL to the AERIUS instance you want to test              | Required |
| `api_key`      | Connect API Key                                          | Optional |
| `max_duration` | Max time the test is expected to run. Default 30 minutes | Optional |

### Using Maven

#### Runs the unit tests in Docker container.

```bash
mvn test -DCYPRESS_BASE_URL=<base_url> [-DCYPRESS_API_KEY=<api_key>] [-DCYPRESS_MAX_TEST_DURATION_MINUTES=<max_duration>]
```

### Using Node.js

Execute the commands from the `src/test/e2e` directory.

#### The project is set up as a Node.js project. Install modules and library based on the data from package.json

```bash
npm install
```

#### Run Cypress

```bash
npx cypress run --config baseUrl=[base_url] --env API_KEY=[api_key]
```

#### Run Cypress interactive mode

```bash
npx cypress open --config baseUrl=[base_url] --env API_KEY=[api_key]
```

#### Show reports

```bash
node ./cypress/reports/cucumber-html-report.js
```

### Developing the Cypress code

For documentation on the code style in the Cypress tests, including how to automatically format code using eslint and prettier, read [Code style](../aerius-calculator-wui-client-test/CodeStyle.md).
