#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Connect API, Import tests UI
    Test the importing of files using the UI API (without using the UI itself)

    Scenario: Upload a GML file with an error and validate that it is validated as invalid
        Given I import the file 'GML_with_error.gml' and I expect the file to be validated as invalid

    Scenario: Test import of file that has nothing to do with AERIUS (testing with a JPG)
        Given I import the file 'jpg-example.jpg' and I expect the file to be validated as invalid

    Scenario: Upload a GML file with a warning and validate that it is validated as valid with warnings
        Given I import the file 'AERIUS_buildings_warnings_only.gml' and I expect the file to be validated as valid with warnings
        When I get the data of the imported file
        Then I expect the obtained data to contain 0 assessment points
        And I expect the obtained data to contain 0 emission sources
        And I expect the obtained data to contain 3 buildings

    Scenario: Upload a RCP file with no warnings or errors and validate that it is validated as valid with warnings
        Given I import the file '1-CalculationPoints.rcp'
        When I get the data of the imported file
        Then I expect the obtained data to contain 1 assessment points
        And I expect the obtained data to contain 0 emission sources
        And I expect the obtained data to contain 0 buildings

    Scenario: Import a GML with a source with emission, a receptorpoint with no deposition and a calculation
        Given I import the file 'SmallCalculationWithFarAwayAssessmentPoint.gml'
        When I get the data of the imported file
        Then I expect the obtained data to contain 1 assessment points
        And I expect the obtained data to contain 1 emission sources
        And I expect the obtained data to contain 0 buildings
