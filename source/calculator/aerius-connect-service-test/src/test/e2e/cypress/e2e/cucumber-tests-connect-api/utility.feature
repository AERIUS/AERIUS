#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Connect API, validate the source files with utility

    Scenario: Validate source zip file
        Given I validate the file 'klein.zip'
        And validate warnings
            | code | 
            | 5223 |
        And validate errors

    Scenario: Validate source gml file
        Given I validate the file 'CalculationPointsSource.gml'
        And validate warnings
            | code | 
            | 5223 |
        And validate errors

    Scenario: Validate source gml file that was extracted form a pdf
        Given I validate the file 'AERIUS_import_file_GML_extracted_from_pdf.gml'
        And validate warnings
        And validate errors

    Scenario: Validate source gml file animal housing conversion
        Given I validate the file 'CalculationAnimalHousingSource.gml'
        And validate warnings
            | code |
            | 5223 |
            | 5265 |
            | 5264 |
            | 5263 |
            | 5253 |
        And validate errors

    Scenario: Validate source pdf file
        Given I validate the new pdf file 'AERIUS_import_file.pdf'
        And validate warnings
            | code |
            | 5244 |
        And validate errors

    Scenario: Validate old source pdf file
        Given I validate the file 'AERIUS_import_old_pdf_file_no_attachment.pdf'
        And validate warnings
            | code |
            | 5223 |
            | 5240 |
            | 5244 |
            | 5263 |
        And validate errors
    Scenario: Validate source gml file that contains error
        Given I validate the file 'GML_with_error.gml'
        And validate warnings
        And validate errors
            | code | 
            | 5201 |
            | 5201 |

    Scenario: Validate source zip file as a report
        Given I validate the file 'klein.zip' with report option

    Scenario: Validate source zip file and convert
        Given I validate the file 'klein.zip' and convert

    Scenario: Convert source zip file
        Given I convert the file 'klein.zip'
