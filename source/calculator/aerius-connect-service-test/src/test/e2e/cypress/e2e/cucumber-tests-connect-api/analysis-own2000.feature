#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Connect API, check analysis calculations
    This feature tests calculations through Connect with analysis method

    Scenario: Analysis OWN2000 start calculate with gml file and custom receptors and cancel the calculation
        Given I get a Api key
        Given I prepare a analysis request for the file "AERIUS_road_industry_living_own_calculation_points.gml" and calculation points file "10-receptors.rcp"
        Given I select calculation year '2024'
        Given I want output type 'CSV'
        Given I use sector output 'true'
        Given I specify result type
            | resultType     |
            | CONCENTRATION  |
            | DEPOSITION     |
            | DRY_DEPOSITION |
            | WET_DEPOSITION |
        Given I specify meteo year '2020'
        Given I use subreceptors type 'DISABLED'
        Given I use custom ops options
            | rawInput | year | compCode | molWeight | phase | loss | diffCoeff | washout | convRate | roughness | chemistry |
            | false    | 2024 | 3        | 17.0      | 1     | 1    | 0.240     | 0       | EMEP     | 0.0       | PROGNOSIS |
        Then I execute the analysis calculation request
        And I check if the job is accepted
        Then I validate job has status 'INITIALIZED'
        Given I cancel the job
        Then I validate job has status 'CANCELLED'

    Scenario: Analysis OWN2000 start calculate with GML source file with custom receptors and use receptor height, then validate the error
        Given I get a Api key
        Given I prepare a analysis request for the file "AERIUS_road_industry_living_own_calculation_points.gml" and calculation points file "10-receptors.rcp"
        Given I select calculation year '2024'
        Given I want output type 'CSV'
        Given I use sector output 'true'
        Given I use receptor height
        Given I specify result type
            | resultType     |
            | DEPOSITION     |
        Given I specify meteo year '2020'
        Given I use subreceptors type 'DISABLED'
        Then I execute the analysis calculation request and validate error
            | errorMessage                                                                                                      |
            | The request contains the output-type 'DEPOSITION' which is not supported by AERIUS-Connect for this request-type. |
        Given I specify result type
            | resultType     |
            | CONCENTRATION  |
        Then I execute the analysis calculation request and validate error
            | errorMessage                                                                                                          |
            | The request has the option 'expectRcpHeight' set. However, the receptorfile supplied does not contain a height column.|

    Scenario: Analysis OWN2000 start calculate with GML source file and use stored custom receptors with receptor height
        Given I get a Api key
        Given I prepare a analysis request for the file "AERIUS_road_industry_living_own_calculation_points.gml" and use calculation points name "from5RCPWithHeight"
        Given I select calculation year '2024'
        Given I want output type 'CSV'
        Given I use sector output 'true'
        Given I use receptor height
        Given I specify result type
            | resultType        |
            | CONCENTRATION     |
        Given I specify meteo year '2020'
        Given I use subreceptors type 'DISABLED'

        # Create the receptorset
        Given I create a receptorset with label 'from5RCPWithHeight', description '5 custom receptor set' and expect receptor height from file '5-CalculationPointsWithHeight.rcp'
        Then I check receptorset with label 'from5RCPWithHeight' and description '5 custom receptor set'

        Then I execute the analysis calculation request
        And I check if the job is accepted
        Given I cancel the job
        Then I validate job has status 'CANCELLED'

        # Delete the receptorset
        Then I remove the receptorset with label 'from5RCPWithHeight'
        Then I check receptorsets does not contain "from5RCPWithHeight" and description 'custom receptor set'

    Scenario: Analysis OWN2000 start calculate with GML source file and custom receptors and validate the CSV output
        Given I get a Api key
        Given I prepare a analysis request for the file "AERIUS_road_industry_living_own_calculation_points.gml" and calculation points file "10-receptors.rcp"
        Given I select calculation year '2024'
        Given I want output type 'CSV'
        Given I use sector output 'true'
        Given I do not use source stacking
        Given I specify result type
            | resultType     |
            | CONCENTRATION  |
            | DEPOSITION     |
            | DRY_DEPOSITION |
            | WET_DEPOSITION |
        Given I specify meteo year '2020'
        Given I use subreceptors type 'DISABLED'
        Given I use custom ops options
            | rawInput | year | compCode | molWeight | phase | loss | diffCoeff | washout | convRate | roughness | chemistry |
            | false    | 2024 | 3        | 17.0      | 1     | 1    | 0.240     | 0       | EMEP     | 0.0       | PROGNOSIS |
        Then I execute the analysis calculation request
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results
        Then I validate metadata result "calculation for AERIUS_road_industry_living_own_calculation_points_gml receptors 10-receptors_rcp_resulttype-metadata.json" with content
            | ops_version   | srm_version | api_meteo_year | source_stacking | receptor_count | max_distance | sub_receptors | forced_aggregation | use_receptor_height |
            | 5.1.2.0-1     | SRM-2024    | 2020           | off             | 10             | on           | DISABLED      | off                | false               |
        Then I validate summary result "calculation for AERIUS_road_industry_living_own_calculation_points_gml receptors 10-receptors_rcp.summary" with content
            | row | result_type    | sector | substance |
            | 0   | concentration  | 1800   | nox       |
            | 1   | concentration  | 1800   | nh3       |
            | 2   | deposition     | 1800   | nox       |
            | 3   | deposition     | 1800   | nh3       |
            | 4   | dry_deposition | 1800   | nox       |
            | 5   | dry_deposition | 1800   | nh3       |
            | 6   | wet_deposition | 1800   | nox       |
            | 7   | wet_deposition | 1800   | nh3       |
        Then I validate result files
            | filename                                                                                                                        |
            | calculation for AERIUS_road_industry_living_own_calculation_points_gml receptors 10-receptors_rcp_resulttype-concentration.csv  |
            | calculation for AERIUS_road_industry_living_own_calculation_points_gml receptors 10-receptors_rcp_resulttype-deposition.csv     |
            | calculation for AERIUS_road_industry_living_own_calculation_points_gml receptors 10-receptors_rcp_resulttype-dry_deposition.csv |
            | calculation for AERIUS_road_industry_living_own_calculation_points_gml receptors 10-receptors_rcp_resulttype-wet_deposition.csv |

    Scenario: Analysis OWN2000 start calculate with BRN source file do not use max distance and custom receptors and validate the CSV output
        Given I get a Api key
        Given I prepare a analysis request for the file "3112_non-urban-road_standard_nox.brn" substance 'NOX' and calculation points file "10-receptors.rcp"
        Given I select calculation year '2024'
        Given I want output type 'CSV'
        Given I use sector output 'true'
        Given I specify result type
            | resultType     |
            | CONCENTRATION  |
            | DEPOSITION     |
            | DRY_DEPOSITION |
            | WET_DEPOSITION |
        Given I specify meteo year '2020'
        Given I use subreceptors type 'DISABLED'
        Given I do not use max distance
        Given I use custom ops options
            | rawInput | year | compCode | molWeight | phase | loss | diffCoeff | washout | convRate | roughness | chemistry |
            | false    | 2024 | 3        | 17.0      | 1     | 1    | 0.240     | 0       | EMEP     | 0.0       | PROGNOSIS |
        Then I execute the analysis calculation request
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results
        Then I validate metadata result "calculation for 3112_non-urban-road_standard_nox_brn receptors 10-receptors_rcp_resulttype-metadata.json" with content
            | ops_version   | srm_version | api_meteo_year | source_stacking | receptor_count | max_distance | sub_receptors | forced_aggregation | use_receptor_height |
            | 5.1.2.0-1     | SRM-2024    | 2020           | on              | 10             | off          | DISABLED      | off                | false               |
        Then I validate summary result "calculation for 3112_non-urban-road_standard_nox_brn receptors 10-receptors_rcp.summary" with content
            | row | result_type    | sector | substance |
            | 0   | concentration  | 1800   | nox       |
            | 1   | concentration  | 1800   | nh3       |
            | 2   | deposition     | 1800   | nox       |
            | 3   | deposition     | 1800   | nh3       |
            | 4   | dry_deposition | 1800   | nox       |
            | 5   | dry_deposition | 1800   | nh3       |
            | 6   | wet_deposition | 1800   | nox       |
            | 7   | wet_deposition | 1800   | nh3       |
        Then I validate result files
            | filename                                                                                                      |
            | calculation for 3112_non-urban-road_standard_nox_brn receptors 10-receptors_rcp_resulttype-concentration.csv  |
            | calculation for 3112_non-urban-road_standard_nox_brn receptors 10-receptors_rcp_resulttype-deposition.csv     |
            | calculation for 3112_non-urban-road_standard_nox_brn receptors 10-receptors_rcp_resulttype-dry_deposition.csv |
            | calculation for 3112_non-urban-road_standard_nox_brn receptors 10-receptors_rcp_resulttype-wet_deposition.csv |

    Scenario: Analysis OWN2000 start calculate with GML file and custom receptors
        Given I get a Api key
        Given I prepare a analysis request for the file "AERIUS_road_industry_living_own_calculation_points.gml" and calculation points file "10-receptors.rcp"
        Given I want output type 'GML'
        Given I use sector output 'true'
        Given I specify result type
            | resultType     |
            | CONCENTRATION  |
            | DEPOSITION     |
        Given I specify meteo year '2020'
        Given I use subreceptors type 'DISABLED'
        Given I use custom ops options
            | rawInput | year | compCode | molWeight | phase | loss | diffCoeff | washout | convRate | roughness | chemistry |
            | false    | 2024 | 3        | 17.0      | 1     | 1    | 0.240     | 0       | EMEP     | 0.0       | PROGNOSIS |
        Then I execute the analysis calculation request
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results
        Then I validate sector output seperate files
            | sector |
            | 3100   |
            | 1800   |
            | 8640   |

    Scenario: Validate if the bug that causes an error, when calculating outside a habitat area, is fixed. See also: Error in Connect when calculating outside habitat area with option subReceptors: "ENABLED" (AER-1325)
        Given I get a Api key
        Given I prepare a analysis request for the file "ValidateFixOpsCalculation.gml" and calculation points file "1-CalculationPoints.rcp"
        Given I want output type 'GML'
        Given I specify result type
            | resultType     |
            | CONCENTRATION  |
        Given I use subreceptors type 'ENABLED'
        Given I use custom ops options
            | rawInput | year | compCode | molWeight | phase | loss | diffCoeff | washout | convRate | roughness | chemistry |
            | false    | 2024 | 3        | 17.0      | 1     | 1    | 0.240     | 0       | EMEP     | 0.0       | PROGNOSIS |
        Then I execute the analysis calculation request
        And I check if the job is accepted
        And I wait till task is completed

    Scenario: Validate if the bug that causes an OPS calculation to fail because of an empty rcp file is fixed. See also: Subreceptors causing empty rcp file in OPS calculation (AER-1362)
        Given I get a Api key
        Given I prepare a analysis request for the file "ValidateFixOpsCalculation.gml" and calculation points file "7-CalculationPoints.rcp"
        Given I want output type 'GML'
        Given I specify result type
            | resultType     |
            | CONCENTRATION  |
        Given I use subreceptors type 'ENABLED'
        Given I use custom ops options
            | rawInput | year | compCode | molWeight | phase | loss | diffCoeff | washout | convRate | roughness | chemistry |
            | false    | 2024 | 3        | 17.0      | 1     | 1    | 0.240     | 0       | EMEP     | 0.0       | PROGNOSIS |
        Then I execute the analysis calculation request
        And I check if the job is accepted
        And I wait till task is completed

    Scenario: Analysis OWN2000 start calculate with GML file road default normal calculation
        Given I get a Api key
        Given I prepare a analysis request for the file "AERIUS_road.gml" and calculation points file "AERIUS_road_custompoints.rcp"
        Given I want output type 'GML'
        Given I use sector output 'true'
        Given I specify result type
            | resultType     |
            | CONCENTRATION  |
            | DEPOSITION     |
        Given I specify meteo year '2020'
        Given I use roadOPS type 'DEFAULT'
        Then I execute the analysis calculation request
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results
        Then I validate sector output seperate files
            | sector |
            | 3100   |
        And I will validate results with 'AERIUS_road_default_custompoints_results.gml'

    Scenario: Analysis OWN2000 start calculate with GML file road OPS_ROAD from 5 km distance with ops no source calculated up to 5 km distance
        Given I get a Api key
        Given I prepare a analysis request for the file "AERIUS_road.gml" and calculation points file "AERIUS_road_custompoints.rcp"
        Given I want output type 'GML'
        Given I use sector output 'true'
        Given I specify result type
            | resultType     |
            | CONCENTRATION  |
            | DEPOSITION     |
        Given I specify meteo year '2020'
        Given I use roadOPS type 'OPS_ROAD'
        Then I execute the analysis calculation request
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results
        Then I validate sector output seperate files
            | sector |
            | 3100   |
        And I will validate results with 'AERIUS_road_ops_road_custompoints_results.gml'

    Scenario: Analysis OWN2000 start calculate with GML file road calculates everything with ops
        Given I get a Api key
        Given I prepare a analysis request for the file "AERIUS_road.gml" and calculation points file "AERIUS_road_custompoints.rcp"
        Given I want output type 'GML'
        Given I use sector output 'true'
        Given I specify result type
            | resultType     |
            | CONCENTRATION  |
            | DEPOSITION     |
        Given I specify meteo year '2020'
        Given I use roadOPS type 'OPS_ALL'
        Then I execute the analysis calculation request
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results
        Then I validate sector output seperate files
            | sector |
            | 3100   |
        And I will validate results with 'AERIUS_road_ops_all_custompoints_results.gml'

    Scenario: Validate subreceptor 750 meter distance calculation with the use of a options file
        Given I get a Api key
        Given I prepare a analysis request for source file "analysis_scenario/brn_subreceptors/source_file.brn" calculation points file "analysis_scenario/brn_subreceptors/receptors.zip" and use options file "analysis_scenario/brn_subreceptors/options_750.json"
        Then I execute the analysis calculation request
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results and expect 6 files
        And I will validate summary with 'analysis_scenario/brn_subreceptors/ResultFiles_750m/ref-M23+2020_sub-nh3_gcnsector-4741_2bronnen_date-20230124_year-2030_rcp-ngev_zl1_3_750.summary'

    Scenario: Validate subreceptor no split calculation with the use of a options file
        Given I get a Api key
        Given I prepare a analysis request for source file "analysis_scenario/brn_subreceptors/source_file.brn" calculation points file "analysis_scenario/brn_subreceptors/receptors.zip" and use options file "analysis_scenario/brn_subreceptors/options_geensplit.json"
        Then I execute the analysis calculation request
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results and expect 6 files
        And I will validate summary with 'analysis_scenario/brn_subreceptors/ResultFiles_geensplit/ref-M23+2020_sub-nh3_gcnsector-4741_2bronnen_date-20230124_year-2030_rcp-ngev_zl1_3_no_split.summary'

    Scenario: Validate aera source scenario with the use of a options file
        Given I get a Api key
        Given I prepare a analysis request for source file "analysis_scenario/brn_oppervlaktebron/source_file.brn" calculation points file "analysis_scenario/brn_oppervlaktebron/nld_zl7-20181029.rcp" and use options file "analysis_scenario/brn_oppervlaktebron/options.json"
        Then I execute the analysis calculation request
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results and expect 6 files
        And I will validate summary with "analysis_scenario/brn_oppervlaktebron/ResultFiles/ref-BuitenlandLand56_year-2025_gcnsector-9999_substance-17_date-20171117_rcp-nld_zl7-20181029.summary"

    Scenario: Validate point source scenario with the use of a options file
        Given I get a Api key
        Given I prepare a analysis request for source file "analysis_scenario/brn_puntbron/source_file.brn" calculation points file "analysis_scenario/brn_puntbron/nld_zl7-20181029.rcp" and use options file "analysis_scenario/brn_puntbron/options.json"
        Then I execute the analysis calculation request
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results and expect 6 files
        And I will validate summary with "analysis_scenario/brn_puntbron/ResultFiles/ref-Luchtvaart_year-2026_substance-11_date-20150507_rcp-nld_zl7-20181029.summary"

    Scenario: Validate subreceptors source scenario with the use of a options file
        Given I get a Api key
        Given I prepare a analysis request for source file "analysis_scenario/brn_subrcp/source_file.brn" calculation points file "analysis_scenario/brn_subrcp/subreceptor.rcp" and use options file "analysis_scenario/brn_subrcp/options.json"
        Then I execute the analysis calculation request
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results and expect 6 files
        And I will validate summary with "analysis_scenario/brn_subrcp/ResultFiles/ref-subreceptors_substance-nh3_year-2030_rcp-subreceptor.summary"

    Scenario: Validate breadcrumb trail source scenario with the use of a options file
        Given I get a Api key
        Given I prepare a analysis request for source file "analysis_scenario/gml_kruimelweg/source_file.gml" calculation points file "analysis_scenario/gml_kruimelweg/nld_zl7-20181029.rcp" and use options file "analysis_scenario/gml_kruimelweg/options.json"
        Then I execute the analysis calculation request
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results and expect 6 files
        And I will validate summary with "analysis_scenario/gml_kruimelweg/ResultFiles/ref-kruimelweg_date-20220201_rcp-nld_zl7-20181029.summary"

    Scenario: Validate receptor with height source scenario with the use of a options file
        Given I get a Api key
        Given I create a receptorset with label "man_3", description "custom receptor set with height" and expect receptor height from file "analysis_scenario/gml_rcp_met_hoogte/man_3.rcp"
        Then I check receptorset with label 'man_3' and description 'custom receptor set'
        Given I prepare a analysis request for source file "analysis_scenario/gml_rcp_met_hoogte/source_file.gml" and use options file "analysis_scenario/gml_rcp_met_hoogte/options.json"
        Then I execute the analysis calculation request
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results and expect 3 files
        And I will validate summary with "analysis_scenario/gml_rcp_met_hoogte/ResultFiles/ref-Energie+gebouwinvloed_date-20220104_rcp-man_3.summary"
        Then I remove the receptorset with label 'man_3'
        Then I check receptorsets does not contain "man_3" and description 'custom receptor set'

    Scenario: Validate future year with height source scenario with the use of a options file
        Given I get a Api key
        Given I prepare a analysis request for source file "analysis_scenario/gml_toekomstjaar/source_file.gml" calculation points file "analysis_scenario/gml_toekomstjaar/nld_zl7-20181029.rcp" and use options file "analysis_scenario/gml_toekomstjaar/options.json"
        Then I execute the analysis calculation request
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results and expect 6 files
        And I will validate summary with "analysis_scenario/gml_toekomstjaar/ResultFiles/ref-euroklasses_year-2035_date-20220114_rcp-nld_zl7-20181029.summary"

    Scenario: Validate shipping with height source scenario with the use of a options file
        Given I get a Api key
        Given I prepare a analysis request for source file "analysis_scenario/gml_zeescheepvaart/source_file.gml" calculation points file "analysis_scenario/gml_zeescheepvaart/nld_zl7-20181029.rcp" and use options file "analysis_scenario/gml_zeescheepvaart/options.json"
        Then I execute the analysis calculation request
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results and expect 6 files
        And I will validate summary with "analysis_scenario/gml_zeescheepvaart/ResultFiles/ref-zeescheepvaart_date_20220105_rcp-nld_zl7-20181029.summary"
