/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class CalculationResultsPage {
  static validateResults(dataTable: any) {
    // Verify dataTable input
    if (dataTable.rawTable.length < 2) {
      throw new Error("The result validation is missing settings");
    } else if (dataTable.rawTable.length > 2) {
      throw new Error("The result validation can only have 1 line of settings");
    }

    dataTable.hashes().forEach((resultSettings: any) => {
      cy.get("@calculationJobKey").then((jobKey: any) => {
        cy.get("@situationCalculations").then((situationCalculations: any) => {
          // NOTE: This only work with a single situation, with some hardcoded settings for now. If you want to use other options, you can add them below
          const situationId = situationCalculations[0].situationId;
          const resultType = this.getResultTypeFromString(resultSettings.result);
          const summaryHexagonType = "CUSTOM_CALCULATION_POINTS";
          const summaryOverlappingHexagonType = "ALL_HEXAGONS";
          const emissionResultKey = this.getEmissionResultKeyFromString(resultSettings.pollutant);
          const extraParameters
            = "?summaryHexagonType="
            + summaryHexagonType
            + "&summaryOverlappingHexagonType="
            + summaryOverlappingHexagonType
            + "&emissionResultKey="
            + emissionResultKey;

          cy.request({
            method: "GET",
            url: Cypress.env("url_ui_calculation") + "/" + jobKey + "/" + situationId + "/" + resultType + extraParameters
          }).then((response) => {
            expect(response.body.statistics.countCalculationPoints).to.equal(+resultSettings.expectedPoints);
            expect(response.body.statistics.countCalculationPointsIncrease).to.equal(+resultSettings.expectedIncreasedPoints);
            expect(response.body.statistics.countCalculationPointsDecrease).to.equal(+resultSettings.expectedDecreasedPoints);
            expect(response.body.statistics.maxIncrease).to.equal(+resultSettings.expectedBiggestIncrease);
            expect(response.body.statistics.maxDecrease).to.equal(+resultSettings.expectedBiggestDecrease);
          });
        });
      });
    });
  }

  static getResultTypeFromString(result: string) {
    // Text from ScenarioResultType.java
    switch (result) {
      case "Projectberekening":
        return "PROJECT_CALCULATION";
      // Note: There are more result types which could be added here as needed
      default:
        throw new Error("Unknown result type " + result);
    }
  }

  static getEmissionResultKeyFromString(pollutant: string) {
    switch (pollutant) {
      case "NOx + NH3":
        return "NOXNH3_DEPOSITION";
      // Note: There are more pollutants which could be added here as needed
      default:
        throw new Error("Unknown pollutant " + pollutant);
    }
  }
}
