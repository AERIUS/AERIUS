/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { And, Given, Then, When } from "cypress-cucumber-preprocessor/steps";
import { CommonPage } from "../common/commonPage";

And("I get a Api key", () => {
  CommonPage.getApiKey();
});

And("I prepare a request for the file {string}", (filePath) => {
  CommonPage.prepareRequest(filePath, "GML", "REFERENCE");
});

And("I prepare a request for the file {string} with calculation point type {string}", (filePath, calculationPointType) => {
  CommonPage.prepareRequestCalculationPointType(filePath, "GML", "REFERENCE", calculationPointType);
});

And("I will validate results with {string}", (filePath) => {
  CommonPage.validateResultsFile(filePath);
});

And("I will validate json situation results {string} with {string}", (situation, filePath) => {
  CommonPage.validateJsonResultFile(situation, filePath);
});

And("I will validate summary with {string}", (filePath) => {
  CommonPage.validateSummaryResultFile(filePath);
});

And("I check if there is no edge hexagon appendix", () => {
  CommonPage.checkForEdgeHexagonAppendix();
});

Then(
  "I calculate these tests with assessment points file {string}, download the expected results and compare them to the calculated results",
  (receptorFilePath, dataTable) => {
    CommonPage.calculateTestsWithAssessmentPointsAndCompareToExpectedResults(receptorFilePath, dataTable);
  }
);

// PDF test
When("I prepare a request for the pdf file {string}", (filePath) => {
  CommonPage.prepareRequest(filePath, "PDF", "PROPOSED");
});

When("I prepare a request for the pdf file {string} with the situationtype {string}", (filePath, situationType) => {
  CommonPage.prepareRequest(filePath, "PDF", situationType);
});

When("I prepare a request for the pdf file {string} and calculation points file {string}", (filePathSource, filePathCalculationPoints) => {
  CommonPage.prepareRequestWithCalculationPoints(filePathSource, filePathCalculationPoints, "PDF", "PROPOSED");
});

Then("I convert the downloaded pdf with {string} in the name to text", (fileNamePart) => {
  CommonPage.readDownloadedPdfConvertToText(fileNamePart);
});

// PDF edgehexagon
When("I prepare a request for the edgehexagon pdf file {string} and {string}", (filePathProposed, filePathReference) => {
  CommonPage.prepareRequestEdgehexagons(filePathProposed, "DEFINED_BY_FILE", filePathReference, "PDF");
});

Then("I test the converted text of the PDF to have length around {int} with a precision of {float}", (length, precision) => {
  CommonPage.testTextLengthOfPdf(length, precision);
});

Then("I check the markers of the converted text to be Dutch", () => {
  CommonPage.checkMarkersResultPage();
  CommonPage.checkMarkersCardAndLegend();
});

Then("I check whether the results include custom calculation points", () => {
  CommonPage.checkCustomCalculationPointsIncludedInOverview();
  CommonPage.checkCustomCalculationPointsIncludedInResults();
});

Then("I convert the mocked pdf in directory {string} to text", (fileDir) => {
  CommonPage.useMockedPdf(fileDir);
});

Then("I check if the edge hexagon pdf file has the expected areas", (dataTable) => {
  CommonPage.checkEdgeHexagonsPdf(dataTable);
});

And("I test a sample of edge hexagons", (dataTable) => {
  CommonPage.checkEdgeHexagonsPdf(dataTable);
});

And("I check if the PDF has the titles I expect", (dataTable) => {
  CommonPage.checkEdgeHexagonsPdf(dataTable);
});

And("I check that the PDF has not have the following titles", (dataTable) => {
  CommonPage.checkPdfDoesNotHaveThisText(dataTable);
});

Given("I request an api-key", () => {
  CommonPage.createApiKeyValidEmail();
});

Then("The api-key request is authorized", () => {
  CommonPage.checkApiKeyResponse(200);
});

Given("I request a new API key for invalid e-mail adres {string}", (email) => {
  CommonPage.createApiKey(email);
});

Then("The api-key request is a failure", () => {
  CommonPage.checkApiKeyResponse(400);
});

Then("The api-key request is a failure with message {string}", (message) => {
  CommonPage.checkApiKeyResponseWithMessage(400, message);
});

Given("I create a receptorset with label {string} and description {string} from file {string}", (label, description, filePath) => {
  CommonPage.createReceptorSet(label, description, filePath, false, true);
});

Given(
  "I create a receptorset with label {string} and description {string} from file {string} no response validation",
  (label, description, filePath) => {
    CommonPage.createReceptorSet(label, description, filePath, false, false);
  }
);

Given(
  "I create a receptorset with label {string}, description {string} and expect receptor height from file {string}",
  (label, description, filePath) => {
    CommonPage.createReceptorSet(label, description, filePath, true, true);
  }
);

Then("I check receptorset with label {string} and description {string}", (label, description) => {
  CommonPage.validateReceptorSet(label, description, true);
});

Then("I remove the receptorset with label {string}", (label) => {
  CommonPage.removeReceptorSet(label);
});

Then("I check receptorsets does not contain {string} and description {string}", (label, description) => {
  CommonPage.validateReceptorSet(label, description, false);
});

Given("I validate the file {string}", (filePath) => {
  CommonPage.validateSource(filePath);
});

Given("I validate the new pdf file {string}", (filePath) => {
  CommonPage.validateSource(filePath);
});

Given("I validate the file {string} with report option", (filePath) => {
  CommonPage.validateSourceReport(filePath);
});

Given("I validate the file {string} and convert", (filePath) => {
  CommonPage.validateSourceConvert(filePath);
});

Given("I convert the file {string}", (filePath) => {
  CommonPage.convertSource(filePath);
});

And("validate warnings", (dataTable) => {
  CommonPage.validateResponseWarnings(dataTable);
});

And("validate errors", (dataTable) => {
  CommonPage.validateResponseErrors(dataTable);
});

And("validate error messages", (dataTable) => {
  CommonPage.validateResponseErrorMessages(dataTable);
});

And("I prepare a request for the file {string} and calculation points file {string}", (filePathSource, filePathCalculationPoints) => {
  CommonPage.prepareRequestWithCalculationPoints(filePathSource, filePathCalculationPoints, "GML", "REFERENCE");
});

And("I check if test is {int}", (code) => {
  CommonPage.checkRequest(code);
});

And("I check if the job is accepted", () => {
  CommonPage.getJob();
});

Given("I cancel the job", () => {
  CommonPage.editJob("/cancel");
});

Given("I delete the job", () => {
  CommonPage.deleteJob();
});

Given("I get all jobs", () => {
  CommonPage.getJobs();
});

Given("I remove all jobs with status {string}", (status) => {
  CommonPage.removeJobsOnStatus(status);
});

Given(
  "I prepare a analysis request for the file {string} and calculation points file {string}",
  (filePathSource, filePathCalculationPoints) => {
    CommonPage.prepareAnalyseRequestWithCalculationPoints(filePathSource, filePathCalculationPoints, "REFERENCE");
  }
);

Given(
  "I prepare a analysis request for the file {string} and use calculation points name {string}",
  (filePathSource, calculationPointsName) => {
    CommonPage.prepareAnalyseRequestWithCalculationPointsName(filePathSource, calculationPointsName, "REFERENCE");
  }
);

Given(
  "I prepare a analysis request for the file {string} substance {string} and calculation points file {string}",
  (filePathSource, substance, filePathCalculationPoints) => {
    CommonPage.prepareAnalyseRequestWithSubstanceCalculationPoints(filePathSource, substance, filePathCalculationPoints, "REFERENCE");
  }
);

Given(
  "I prepare a analysis request for source file {string} calculation points file {string} and use options file {string}",
  (filePathSource, filePathCalculationPoints, filePathOptions) => {
    CommonPage.prepareAnalyseRequestWithCalculationPointsAndOptions(filePathSource, filePathCalculationPoints, filePathOptions);
  }
);

Given("I prepare a analysis request for source file {string} and use options file {string}", (filePathSource, filePathOptions) => {
  CommonPage.prepareAnalyseRequestWithOptions(filePathSource, filePathOptions);
});

Given("I validate a summarize file request for the file {string} with expected result {string}", (filePathSource, filePathResults) => {
  CommonPage.prepareSummarizeFileRequestAndValidate(filePathSource, filePathResults);
});

Given("I prepare a register wnb request for the file {string}", (filePathSource) => {
  CommonPage.prepareRegisterRequest(filePathSource, "JSON");
});

Given("I prepare a register own2000 request for the file {string}", (filePathSource) => {
  CommonPage.prepareRegisterRequest(filePathSource, "JSON");
});

Given("I prepare a register permit request for the file {string}", (filePathSource) => {
  CommonPage.prepareRegisterRequest(filePathSource, "PDF");
});

Given("I select calculation year {string}", (calculationYear) => {
  CommonPage.updateAnalysisCalculationYear(calculationYear);
});

Given("I want output type {string}", (outputType) => {
  CommonPage.updateAnalysisOutputType(outputType);
});

Given("I use receptor height", () => {
  CommonPage.useReceptorHeight();
});

Given("I use sector output {string}", (isSectorOutput) => {
  CommonPage.updateAnalysisSectorOutput(isSectorOutput);
});

Given("I do not use source stacking", () => {
  CommonPage.updateStackingSource(false);
});

Given("I want to use forced aggregation", () => {
  CommonPage.useForcedAggregation(true);
});

Given("I specify result type", (dataTable) => {
  CommonPage.updateAnalysisResultType(dataTable);
});

Given("I specify meteo year {string}", (meteoPeriod) => {
  CommonPage.updateAnalysisMeteoYear(meteoPeriod);
});

Given("I use roadOPS type {string}", (roadOpsType) => {
  CommonPage.specifyRoadOps(roadOpsType);
});

Given("I use subreceptors type {string}", (subReceptorType) => {
  CommonPage.updateAnalysisSubReceptors(subReceptorType);
});

Given("I do not use max distance", () => {
  CommonPage.doNotUseMaxDistance();
});

Then("I execute the analysis calculation request", () => {
  CommonPage.checkRequest(200);
});

Then("I execute the analysis calculation request and validate error", (dataTable: any) => {
  CommonPage.checkRequest(400, dataTable);
});

Then("I execute the register calculation request", () => {
  CommonPage.checkRegisterRequest(200);
});

Then("I validate job has status {string}", (jobStatus) => {
  CommonPage.validateJobStatus(jobStatus);
});

And("I wait till task is completed", () => {
  CommonPage.checkTaskStatusCompleteAndDownload();
});

And("I can extract downloaded results", () => {
  CommonPage.extractDownloadedResults();
});

And("I can extract downloaded results and expect {int} files", (fileCount) => {
  CommonPage.extractDownloadedResultsAndValidateFilesCount(fileCount);
});

Given("I use custom ops options", (dataTable) => {
  CommonPage.setAnalysisOpsOptions(dataTable);
});

Then("I validate metadata result {string} with content", (fileName, dataTable) => {
  CommonPage.validateAnalysisCSVMetadata(fileName, dataTable);
});

Then("I validate summary result {string} with content", (fileName, dataTable) => {
  CommonPage.validateAnalysisCSVSummary(fileName, dataTable);
});

Then("I validate result files", (dataTable) => {
  CommonPage.validateAnalysisCSVResultFiles(dataTable);
});

Then("I validate sector output seperate files", (dataTable) => {
  CommonPage.validateAnalysisGMLSectorOutput(dataTable);
});

Given("I open the swagger documentation page", () => {
  CommonPage.openSwaggerDocumentation();
});

And("I validate documentation links", (dataTable) => {
  CommonPage.checkDocumentionLinks(dataTable);
});

And("I validate documentation pages", (dataTable) => {
  CommonPage.checkDocumentionSpecification(dataTable);
});

Then("I check if the job can be filtered on {string}", (expectedStatus) => {
  CommonPage.checkTaskStatusAndFilterOnStatus(expectedStatus);
});
