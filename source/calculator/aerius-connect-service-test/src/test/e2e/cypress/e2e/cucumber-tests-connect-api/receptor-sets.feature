#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Connect API, manage receptor sets

    Scenario: Create receptor set from RCP source, check availability and remove
        Given I create a receptorset with label 'man284' and description 'custom receptor set' from file 'man_284.rcp'
        Then I check receptorset with label 'man284' and description 'custom receptor set'
        Then I remove the receptorset with label 'man284'
        Then I check receptorsets does not contain "man284" and description 'custom receptor set'

    Scenario: Create receptor set from RCP source with receptor height, check availability and remove
        Given I create a receptorset with label 'fromRCPWithHeight', description '5 custom receptor set' and expect receptor height from file '5-CalculationPointsWithHeight.rcp'
        Then I check receptorset with label 'fromRCPWithHeight' and description '5 custom receptor set'
        Then I remove the receptorset with label 'fromRCPWithHeight'
        Then I check receptorsets does not contain 'fromRCPWithHeight' and description 'custom receptor set'
    
    Scenario: Create receptor set from GML, check availability and remove
        Given I create a receptorset with label 'fromGML' and description 'custom receptor set' from file '284_CalculationPointsSourceResults.gml'
        Then I check receptorset with label 'fromGML' and description 'custom receptor set'
        Then I remove the receptorset with label 'fromGML'
        Then I check receptorsets does not contain 'fromGML' and description 'custom receptor set'

    Scenario: Create receptor set from RCP source use receptorheight, check availability and remove
        Given I create a receptorset with label 'man_3_inclusive_height', description 'custom receptor set with height' and expect receptor height from file 'analysis_scenario/gml_rcp_met_hoogte/man_3.rcp'
        Then I check receptorset with label 'man_3_inclusive_height' and description 'custom receptor set with height'
        Then I remove the receptorset with label 'man_3_inclusive_height'
        Then I check receptorsets does not contain 'man_3_inclusive_height' and description 'custom receptor set with height'

    Scenario: Create receptor set from RCP source use receptorheight false, check the error for missing receptorheight setting
        Given I create a receptorset with label 'man_3_no_height' and description 'custom receptor set with height' from file 'analysis_scenario/gml_rcp_met_hoogte/man_3.rcp'
        And validate warnings
        And validate errors
            | code  |
            | 50023 |

    Scenario: Create receptor set from RCP source use receptorheight false, check the error for missing receptorheight setting
        Given I create a receptorset with label 'one CalculationPoint', description 'custom receptor set without height but height set' and expect receptor height from file '1-CalculationPoints.rcp'
        And validate warnings
        And validate errors
            | code  |
            | 50022 |

    Scenario: Create receptor set from empty RCP source validate the response
        Given I create a receptorset with label 'empty rcp' and description 'empty rcp' from file 'empty-receptors.rcp' no response validation
        And validate error messages
            | message                                                                  |
            | In order to add a receptorset at least 1 receptorpoint must be provided. |
