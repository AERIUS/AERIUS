#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Connect API, check if authorization is possible

    Scenario: Check if requesting an api-key is possible
        Given I request an api-key
        Then The api-key request is a failure with message 'Het genereren van API keys is niet toegestaan.'

    Scenario: Check register new user invalid e-mail format
        Given I request a new API key for invalid e-mail adres 'not-correct.@a.com'
        Then The api-key request is a failure
