#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Connect API, check register features

    Scenario: Validate and check summary
        Given I get a Api key
        Given I validate a summarize file request for the file "AERIUS_projectberekening_register.pdf" with expected result "AERIUS_projectberekening_register.json"
 
    Scenario: Calculate own2000 from pdf and return results in json
        Given I get a Api key
        Given I prepare a register own2000 request for the file "AERIUS_projectberekening_register.pdf"
        Then I execute the register calculation request
        And I check if the job is accepted
        Then I validate job has status 'INITIALIZED'
        Given I cancel the job
        Then I validate job has status 'CANCELLED'

    Scenario: Calculate own2000 from pdf and generate a permit PDF
        Given I get a Api key
        Given I prepare a register permit request for the file "AERIUS_projectberekening_register.pdf"
        Then I execute the register calculation request
        And I check if the job is accepted
        Then I validate job has status 'INITIALIZED'
        Given I cancel the job
        Then I validate job has status 'CANCELLED'

    Scenario: Calculate own2000 from pdf return results in json and validate result
        Given I get a Api key
        Given I prepare a register own2000 request for the file "AERIUS_projectberekening_register.pdf"
        Then I execute the register calculation request
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results
        Then I will validate json situation results 'SUMMARY' with 'AERIUS_projectberekening_register_summary-result.json'
        Then I will validate json situation results 'PROPOSED' with 'AERIUS_projectberekening_register_proposed-result.json'

    Scenario: Calculate own2000 from pdf generate a permit PDF and validate result
        Given I get a Api key
        Given I prepare a register permit request for the file "AERIUS_projectberekening_register.pdf"
        Then I execute the register calculation request
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results
        When I convert the downloaded pdf with 'AERIUS_projectberekening_' in the name to text
        Then I test the converted text of the PDF to have length around 2861 with a precision of 0.01
        And I check if the PDF has the titles I expect
            | titles:                                                                                         |
            | Bijlage bij besluit                                                                             |
            | Ruimte geregistreerd                                                                            |
            | Ruimte beschikbaar                                                                              |
            | Resultaten stikstofgevoelige Natura                                                             |
            | Disclaimer Hoewel verstrekte gegevens kunnen dienen ter onderbouwing van een vergunningaanvraag |

    Scenario: Calculate register measure from gml return results in json and validate result
        Given I get a Api key
        Given I prepare a register own2000 request for the file "AERIUS_measure_register.zip"
        Then I execute the register calculation request
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results
        Then I will validate json situation results 'PROPOSED' with 'AERIUS_measure_register_proposed-result.json'
        Then I will validate json situation results 'SUMMARY' with 'AERIUS_measure_register_summary-result.json'
        Then I will validate json situation results 'OFF_SITE_REDUCTION' with 'AERIUS_measure_register_off_side_reduction-result.json'

    Scenario: Calculate register project with off site reduction from gml return results in json and validate result
        Given I get a Api key
        Given I prepare a register own2000 request for the file "AERIUS_project_off_site_reduction_register.zip"
        Then I execute the register calculation request
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results
        Then I will validate json situation results 'PROPOSED' with 'AERIUS_project_off_site_reduction_register_proposed-result.json'
        Then I will validate json situation results 'SUMMARY' with 'AERIUS_project_off_site_reduction_register_summary-result.json'
        Then I will validate json situation results 'OFF_SITE_REDUCTION' with 'AERIUS_project_off_site_reduction_register_off_side_reduction-result.json'

    Scenario: Calculate register only off site reduction from gml return results in json and validate result
        Given I get a Api key
        Given I prepare a register own2000 request for the file "AERIUS_off_site_reduction_register.gml"
        Then I execute the register calculation request
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results
        Then I will validate json situation results 'SUMMARY' with 'AERIUS_off_site_reduction_register_summary-result.json'
        Then I will validate json situation results 'OFF_SITE_REDUCTION' with 'AERIUS_off_site_reduction_register_off_side_reduction-result.json'
