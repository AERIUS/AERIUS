/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";
import { CalculationPage } from "../calculation/calculationPage";
import { CalculationResultsPage }  from "../calculationResults/calculationResultsPage";
import { ImportPage } from "../import/importPage";
import { ExportPage } from "./exportPage";

Given("I import the file {string}", (filePath) => {
  ImportPage.importFile(filePath);
});

Given("I import the file {string} and I expect the file to be validated as valid with warnings", (filePath) => {
  ImportPage.importFile(filePath, true);
});

Given("I get the data of the imported file", () => {
  ImportPage.getDataFromImportedFile();
});

Given("I expect the obtained data to contain {int} assessment points", (expectedAmount) => {
  ImportPage.assertNumberOfAssessmentPoints(expectedAmount);
});

Given("I expect the obtained data to contain {int} emission sources", (expectedAmount) => {
  ImportPage.assertNumberOfEmissionSources(expectedAmount);
});

Given("I expect the obtained data to contain {int} buildings", (expectedAmount) => {
  ImportPage.assertNumberOfBuildings(expectedAmount);
});

Given("I start a calculation with the following settings and wait for the calculation to finish", (dataTable) => {
  CalculationPage.startCalculationAndWaitForItsCompletion(dataTable);
});

Given("I validate the results for the following settings", (dataTable) => {
  CalculationResultsPage.validateResults(dataTable);
});

Given("I export the calculation job with the following settings", (dataTable) => {
  ExportPage.exportCalculationJob(dataTable);
});

Given("I wait for the export to finish", () => {
  ExportPage.waitForExportedFileToBeReady();
});

Given("I download the exported file", () => {
  ExportPage.downloadExportedFile();
});

Given("I unzip the downloaded files contents", () => {
  ExportPage.unzipDownloadedFiles();
});

Given("I import the file that has just been exported", () => {
  ExportPage.importExportedFile();
});

Given("I expect the zipped file to have the name {string}", (expectedFilename) => {
  ExportPage.assertFilenameIs(expectedFilename, true);
});

Given("I expect the unzipped file to have the name {string}", (expectedFilename) => {
  ExportPage.assertFilenameIs(expectedFilename, false);
});
