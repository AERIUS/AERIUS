#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Connect API, check own2000 calculations
    This feature tests GML and PDF calculations with the own2000 method

    Scenario: Check request without api key
        Given I prepare a request for the file 'klein.zip'
        Then I check if test is 403

    Scenario: Check pdf request with incorrect situation type
        Given I get a Api key
        Given I prepare a request for the pdf file 'klein.zip' with the situationtype 'REFERENCE'
        Then I check if test is 400

    Scenario: OWN2000 start calculate with zip file and cancel job
        Given I get a Api key
        Given I prepare a request for the file 'klein.zip'
        Then I check if test is 200
        And I check if the job is accepted
        Given I cancel the job

    Scenario: OWN2000 start calculate with gml file and cancel job
        Given I get a Api key
        Given I prepare a request for the file 'SmallCalculation.gml'
        Then I check if test is 200
        And I check if the job is accepted
        Given I cancel the job

    Scenario: OWN2000 start calculate with gml file and use calculation point type OWN2000_RECEPTORS and cancel job
        Given I get a Api key
        Given I prepare a request for the file 'SmallCalculation.gml' with calculation point type 'OWN2000_RECEPTORS'
        Then I check if test is 200
        And I check if the job is accepted
        Given I cancel the job

    Scenario: OWN2000 start Calculate with gml source file and download result and compare emissions source and results
        Given I get a Api key
        Given I prepare a request for the file 'SmallCalculation.gml'
        Then I check if test is 200
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results
        And I will validate results with 'SmallCalculationResults.gml'

    Scenario: Upload real life GML, generate a PDF and check the generated PDF
        Given I get a Api key
        And I prepare a request for the pdf file 'real_life/input/Real_life_test-1-farmland.gml'
        Then I check if test is 200
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results
        Then I check if there is no edge hexagon appendix
        When I convert the downloaded pdf with 'AERIUS_projectberekening_' in the name to text
        Then I test the converted text of the PDF to have length around 18183 with a precision of 0.01
        Then I check the markers of the converted text to be Dutch
        And I check if the PDF has the titles I expect
            | titles:                                                                                         |
            | Projectberekening                                                                               |
            | Resultaten stikstofgevoelige Natura                                                             |
            | Disclaimer Hoewel verstrekte gegevens kunnen dienen ter onderbouwing van een vergunningaanvraag |
        When I convert the downloaded pdf with 'AERIUS_extra_beoordeling_' in the name to text
        Then I test the converted text of the PDF to have length around 2521 with a precision of 0.01
        And I check if the PDF has the titles I expect
            | titles:                                                                                         |
            | Bijlage projectberekening                                                                       |
            | Hulpmiddel beoordeling hexagonen                                                                |
            | met een hersteldoel                                                                             |
            | Resultaten hexagonen met hersteldoel situatie "Landbouwgronden"                                 |
            | Disclaimer Hoewel verstrekte gegevens kunnen dienen ter onderbouwing van een vergunningaanvraag |

    Scenario: OWN2000 start Calculate with gml source file and download result and compare for edge hexagons
        Given I get a Api key
        Given I prepare a request for the file 'EdgeHexagonOneSituation.gml'
        Then I check if test is 200
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results
        And I will validate results with 'EdgeHexagonOneSituationResult.gml'

    Scenario: Upload real life GML and custom points, generate a PDF and check the generated PDF
        Given I get a Api key
        And I prepare a request for the pdf file 'real_life/input/Real_life_test-7_2-greenhouses-Referentiesituatie.gml' and calculation points file 'man_284.rcp'
        Then I check if test is 200
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results
        When I convert the downloaded pdf with 'AERIUS_projectberekening_' in the name to text
        Then I test the converted text of the PDF to have length around 6525 with a precision of 0.01
        Then I check whether the results include custom calculation points
        And I check if the PDF has the titles I expect
            | titles:                                                                                         |
            | Projectberekening                                                                               |
            | Resultaten stikstofgevoelige Natura                                                             |
            | Er zijn geen resultaten voor deze weergave.                                                     |
            | Disclaimer Hoewel verstrekte gegevens kunnen dienen ter onderbouwing van een vergunningaanvraag |
        When I convert the downloaded pdf with 'AERIUS_extra_beoordeling_' in the name to text
        Then I test the converted text of the PDF to have length around 2210 with a precision of 0.01
        And I check if the PDF has the titles I expect
            | titles:                                                                                         |
            | Bijlage projectberekening                                                                       |
            | Hulpmiddel beoordeling hexagonen                                                                |
            | met een hersteldoel                                                                             |
            | Resultaten hexagonen met hersteldoel situatie "Referentiesituatie" (Beoogd)                     |
            | Er zijn geen resultaten voor deze weergave.                                                     |
            | Disclaimer Hoewel verstrekte gegevens kunnen dienen ter onderbouwing van een vergunningaanvraag |

    Scenario: Calculate custom points with gml source and download result export and compare emission source and results
        Given I get a Api key
        And I prepare a request for the file 'CalculationPointsSource.gml' and calculation points file '13_CalculationPoints.rcp'
        Then I check if test is 200
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results
        And I will validate results with '13_CalculationPointsSourceResults.gml'

    Scenario: Calculate custom points 284 man with gml source and download result export and compare emission source and results
        Given I get a Api key
        And I prepare a request for the file 'CalculationPointsSource.gml' and calculation points file 'man_284.rcp'
        Then I check if test is 200
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results
        And I will validate results with '284_CalculationPointsSourceResults.gml'

    Scenario: Calculate real life farmland with assessment points file 'man_284.rcp', download results export and compare emission source and results
        Given I get a Api key
        Then I calculate these tests with assessment points file 'man_284.rcp', download the expected results and compare them to the calculated results
            | filePath                                                                 | resultsPath                                                                        |
            | real_life/input/Real_life_test-1-farmland.gml                            | real_life/results/Real_life_test-1-farmland-results.gml                            |

    Scenario: Calculate real life industry with assessment points file 'man_284.rcp', download results export and compare emission source and results
        Given I get a Api key
        Then I calculate these tests with assessment points file 'man_284.rcp', download the expected results and compare them to the calculated results
            | filePath                                                                 | resultsPath                                                                        |
            | real_life/input/Real_life_test-2_1-industry-Beoogdesituatie.gml          | real_life/results/Real_life_test-2_1-industry-Beoogdesituatie-results.gml          |
            | real_life/input/Real_life_test-2_2-industry-Vergundesituatie.gml         | real_life/results/Real_life_test-2_2-industry-Vergundesituatie-results.gml         |

    Scenario: Calculate real life waste processing with assessment points file 'man_284.rcp', download results export and compare emission source and results
        Given I get a Api key
        Then I calculate these tests with assessment points file 'man_284.rcp', download the expected results and compare them to the calculated results
            | filePath                                                                 | resultsPath                                                                        |
            | real_life/input/Real_life_test-3_1-waste_processing-beoogdesituatie.gml  | real_life/results/Real_life_test-3_1-waste_processing-beoogdesituatie-results.gml  |
            | real_life/input/Real_life_test-3_2-waste_processing-NbwvergundmetNH3.gml | real_life/results/Real_life_test-3_2-waste_processing-NbwvergundmetNH3-results.gml |

    Scenario: Calculate real life port with assessment points file 'man_284.rcp', download results export and compare emission source and results
        Given I get a Api key
        Then I calculate these tests with assessment points file 'man_284.rcp', download the expected results and compare them to the calculated results
            | filePath                                                                 | resultsPath                                                                        |
            | real_life/input/Real_life_test-4-port-AangevraagdeSituatie.gml           | real_life/results/Real_life_test-4-port-AangevraagdeSituatie-results.gml           |

    Scenario: Calculate real life agriculture with assessment points file 'man_284.rcp', download results export and compare emission source and results
        Given I get a Api key
        Then I calculate these tests with assessment points file 'man_284.rcp', download the expected results and compare them to the calculated results
            | filePath                                                                 | resultsPath                                                                        |
            | real_life/input/Real_life_test-5_1-agriculture-Gewenst.gml               | real_life/results/Real_life_test-5_1-agriculture-Gewenst-results.gml               |
            | real_life/input/Real_life_test-5_2-agriculture-Referentie1997.gml        | real_life/results/Real_life_test-5_2-agriculture-Referentie1997-results.gml        |
            | real_life/input/Real_life_test-5_3-agriculture-Saldering.gml             | real_life/results/Real_life_test-5_3-agriculture-Saldering-results.gml             |

    Scenario: Calculate real life road network with assessment points file 'man_284.rcp', download results export and compare emission source and results
        Given I get a Api key
        Then I calculate these tests with assessment points file 'man_284.rcp', download the expected results and compare them to the calculated results
            | filePath                                                                 | resultsPath                                                                        |
            | real_life/input/Real_life_test-6_1-road_network-BeoogdeSituatie.gml      | real_life/results/Real_life_test-6_1-road_network-BeoogdeSituatie-results.gml      |
            | real_life/input/Real_life_test-6_2-road_network-Referentie.gml           | real_life/results/Real_life_test-6_2-road_network-Referentie-results.gml           |

    Scenario: Calculate real life greenhouse with assessment points file 'man_284.rcp', download results export and compare emission source and results
        Given I get a Api key
        Then I calculate these tests with assessment points file 'man_284.rcp', download the expected results and compare them to the calculated results
            | filePath                                                                 | resultsPath                                                                        |
            | real_life/input/Real_life_test-7_1-greenhouses-Beoogdgebruik.gml         | real_life/results/Real_life_test-7_1-greenhouses-Beoogdgebruik-results.gml         |
            | real_life/input/Real_life_test-7_2-greenhouses-Referentiesituatie.gml    | real_life/results/Real_life_test-7_2-greenhouses-Referentiesituatie-results.gml    |

    Scenario: Check the project calculation PDF and appendice PDF of a calculation with edge hexagons
        Given I get a Api key
        And I prepare a request for the edgehexagon pdf file 'EdgeHexagonTwoSituationsProposed.gml' and 'EdgeHexagonTwoSituationsReference.gml'
        Then I check if test is 200
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results and expect 3 files

        When I convert the downloaded pdf with 'AERIUS_randeffect_projectberekening_' in the name to text
        Then I test the converted text of the PDF to have length around 4550 with a precision of 0.01
        Then I check if the edge hexagon pdf file has the expected areas
            | area:           |
            | Bekendelle      |
            | Korenburgerveen |
            | Wooldse Veen    |

        #Disable pdf to txt conversion removes numbers

        #And I test a sample of edge hexagons
        #    | hexagons: |         |         |         |         |
        #    | 4059267   | 4060794 | 4060795 | 4062324 | 4062325 |
        #    | 4189211   | 4183095 | 4186153 | 4187682 | 4189210 |
        #    | 3970600   | 3964484 | 3966012 | 3967542 | 3969070 |

        #Disable pdf to txt conversion removes numbers

        And I check if the PDF has the titles I expect
            | titles:                                                                             |
           #| Resultaten stikstofgevoelige Natura 2000 gebieden situatie "Beoogd" (Beoogd)        |
            | Resultaten stikstofgevoelige Natura  gebieden situatie "Beoogd" (Beoogd)            |
            | incl. saldering e/o referentie zonder de hexagonen met een mogelijk randeect        |
            | Resultaten op alle hexagonen met mogelijk randeect voor situatie 'Beoogd' (Beoogd), |
            | incl referentie en eventueel saldering                                              |
        When I convert the downloaded pdf with 'AERIUS_extra_beoordeling_' in the name to text
        Then I test the converted text of the PDF to have length around 2223 with a precision of 0.01
        When I convert the downloaded pdf with 'AERIUS_projectberekening_' in the name to text
        Then I test the converted text of the PDF to have length around 5850 with a precision of 0.01

    Scenario: Check the project calculation PDF has only Dutch text and does not include English source names
        Given I get a Api key
        And I prepare a request for the edgehexagon pdf file 'EdgeHexagonTwoSituationsProposed.gml' and 'EdgeHexagonTwoSituationsReference.gml'
        Then I check if test is 200
        And I check if the job is accepted
        And I wait till task is completed
        And I can extract downloaded results and expect 3 files

        When I convert the downloaded pdf with 'AERIUS_projectberekening_' in the name to text
        And I check that the PDF has not have the following titles
            | titles:           |
            | Energy            |
            | Housing emissions |
            | Agriculture       |


# Example of mocked test. Run the normal test once, then use the generated file for testing instead of having the API generate a new file the whole time.
# Scenario: (MOCKED) Upload real life GML and custom points, generate a PDF and check the generated PDF
#     Given I get a Api key
#     When I convert the mocked pdf in directory 'cypress/fixtures/download/unzip/34181846-3a03-47fe-bc50-349d8ac13600/' to text
#     Then I test the converted text of the PDF to have length around 8439 with a precision of 0.01
#     When I read the txt of the expected converted PDF 'Real_life_test-7_2-greenhouses-Referentiesituatie.pdf.txt'
#     Then I check whether the results include custom calculation points
