#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Connect API, check the jobs methods

    Scenario: Calculate custom points with gml source cancel job and remove
        Given I get a Api key
        And I prepare a request for the file 'CalculationPointsSource.gml' and calculation points file '13_CalculationPoints.rcp'
        Then I check if test is 200
        And I check if the job is accepted
        Given I cancel the job
        Given I delete the job
    
    Scenario: Calculate custom points with gml source cancel job
        Given I get a Api key
        And I prepare a request for the file 'CalculationPointsSource.gml' and calculation points file '13_CalculationPoints.rcp'
        Then I check if test is 200
        And I check if the job is accepted
        Given I cancel the job

    Scenario: Remove all jobs status canceled
        Given I get a Api key
        And I remove all jobs with status 'CANCELLED'

    Scenario: Calculate custom points with gml source and check filtering on jobs
        Given I get a Api key
        And I prepare a request for the file 'CalculationPointsSource.gml' and calculation points file '13_CalculationPoints.rcp'
        Then I check if test is 200
        And I check if the job is accepted
        Then I check if the job can be filtered on 'INITIALIZED'
        And I check if the job can be filtered on 'COMPLETED'
        And I delete the job
        And I prepare a request for the file 'CalculationPointsSource.gml' and calculation points file '13_CalculationPoints.rcp'
        Then I check if test is 200
        Given I cancel the job
        Then I check if the job can be filtered on 'CANCELLED'
        Given I delete the job
