/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given } from "cypress-cucumber-preprocessor/steps";
import { ImportPage } from "./importPage";

Given("I import the file {string}", (filePath) => {
  ImportPage.importFile(filePath);
});

Given("I import the file {string} and I expect the file to be validated as valid with warnings", (filePath) => {
  ImportPage.importFile(filePath, true);
});

Given("I import the file {string} and I expect the file to be validated as invalid", (filePath) => {
  ImportPage.importFile(filePath, false, true);
});

Given("I get the data of the imported file", () => {
  ImportPage.getDataFromImportedFile();
});

Given("I expect the obtained data to contain {int} assessment points", (expectedAmount) => {
  ImportPage.assertNumberOfAssessmentPoints(expectedAmount);
});

Given("I expect the obtained data to contain {int} emission sources", (expectedAmount) => {
  ImportPage.assertNumberOfEmissionSources(expectedAmount);
});

Given("I expect the obtained data to contain {int} buildings", (expectedAmount) => {
  ImportPage.assertNumberOfBuildings(expectedAmount);
});
