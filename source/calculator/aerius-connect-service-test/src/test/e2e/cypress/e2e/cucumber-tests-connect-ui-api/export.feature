#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Connect API, Export tests UI
    Test the exporting of files using the UI API (without using the UI itself)

    Scenario: Import a GML with a source with emission, a receptorpoint with no deposition, calculate, export and then validate if every feature is still in the result (AER-1187)
        # Expect a warning due to this file being made in an older AERIUS-version
        Given I import the file 'SmallCalculationWithFarAwayAssessmentPoint.gml' and I expect the file to be validated as valid with warnings
        When I get the data of the imported file
        And I start a calculation with the following settings and wait for the calculation to finish
        | name              | jobType           | method                   | projectScenario | referenceScenario | offsiteReductionScenario |
        | AER-1187 calc job | Projectberekening | Alleen eigen rekenpunten | Situatie 1      | -                 | -                        |

        Then I validate the results for the following settings
        | calculationName   | scenarioName | result            | pollutant | view              | expectedPoints | expectedIncreasedPoints | expectedDecreasedPoints | expectedBiggestIncrease | expectedBiggestDecrease |
        | AER-1187 calc job | Situatie 1   | Projectberekening | NOx + NH3 | Eigen rekenpunten | 1              | 0                       | 0                       | 0                       | 0                       |

        Then I export the calculation job with the following settings
        | jobType    | method                   | exportType | withSources |
        | Berekening | Alleen eigen rekenpunten | GML        | true        |
        And I wait for the export to finish
        And I download the exported file
        And I unzip the downloaded files contents

        Then I import the file that has just been exported
        When I get the data of the imported file
        Then I expect the obtained data to contain 1 assessment points
        And I expect the obtained data to contain 1 emission sources
        And I expect the obtained data to contain 0 buildings
    
    Scenario: Test the filenames of a downloaded GML
        # Expect a warning due to this file being made in an older AERIUS-version
        Given I import the file 'SmallCalculationProject.gml' and I expect the file to be validated as valid with warnings
        And I get the data of the imported file

        Then I export the calculation job with the following settings
        | jobType    | method          | exportType | withSources |
        | Berekening | Own2000-methode | GML        | false       |
        And I wait for the export to finish
        And I download the exported file
        Then I expect the zipped file to have the name 'AERIUS_{datetime}_{reference}_Calculationjob1.zip'

        When I unzip the downloaded files contents
        Then I expect the unzipped file to have the name 'AERIUS_{datetime}_{reference}_Situatie1.gml'

    Scenario: Test the filenames of a downloaded PDF
        # Expect a warning due to this file being made in an older AERIUS-version
        Given I import the file 'SmallCalculationProject.gml' and I expect the file to be validated as valid with warnings
        And I get the data of the imported file

        Then I export the calculation job with the following settings
        | jobType    | method          | exportType | withSources |
        | Rapportage | Own2000-methode | PDF        | false       |
        And I wait for the export to finish
        And I download the exported file
        Then I expect the zipped file to have the name 'AERIUS_{datetime}_{reference}_Calculationjob1.zip'
        
        When I unzip the downloaded files contents
        Then I expect the unzipped file to have the name 'AERIUS_projectberekening_{datetime}_{reference}_Situatie1.pdf'
