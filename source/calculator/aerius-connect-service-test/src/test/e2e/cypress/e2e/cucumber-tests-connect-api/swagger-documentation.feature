#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Connect API, swagger documentation page

    Scenario: Check the links on swagger documentation page
        Given I open the swagger documentation page
        Then I validate documentation links
        | classId       | description                       | url                                                            |
        | info__tos     | Terms of service                  | https://link.aerius.nl/documenten/calculator/handboek_2024.pdf |
        | info__contact | AERIUS - Bij12 helpdesk - Website | https://link.aerius.nl/helpdesk                                |
        | info__license | Apache License, version 2.0       | https://www.apache.org/licenses/LICENSE-2.0                    |
        Then I validate documentation pages
        | pageId               | pageName                      | allowAuthorize |
        | v8/api-common.yaml   | Common API's                  | true           |
        | v8/api-own2000.yaml  | OwN2000 Calculations          | true           |
        | v8/api-lbv.yaml      | Lbv(-plus) calculations       | true           |
        | v8/api-analysis.yaml | Analysis                      | true           |

