/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { ImportPage } from "../import/importPage";
import { CommonPage } from "../../common/commonPage";

export class ExportPage {
  static fileReadTimeOut: number = 9999;

  static startCalculationAndWaitForItsCompletion(dataTable: any) {
    // Verify dataTable input
    if (dataTable.rawTable.length < 2) {
      throw new Error("The calculation is missing settings");
    } else if (dataTable.rawTable.length > 2) {
      throw new Error("The calculation can only have 1 line of settings");
    }

    // Build call body from calculation settings
    dataTable.hashes().forEach((calculationSettings: any) => {
      const theme = "OWN2000";
      const exportOptions: any = {
        appendices: [],
        exportType: "CALCULATION_UI", // Do the calculation without it being downloadable
        jobType: "CALCULATION",
        protectJob: false
      };

      const options: any = {
        calculatemaximumRange: 0
      };

      const customPoints: any = {
        type: "FeatureCollection",
        features: []
      };

      options.calculationJobType = "PROCESS_CONTRIBUTION";
      options.calculationMethod = this.getCalculationMethodFromString(calculationSettings.method);

      this.setCalculationScenarios(calculationSettings);
      cy.get("@calculationScenarios").then((calculationScenarios: any) => {
        if (options.calculationMethod === "CUSTOM_POINTS") {
          cy.get("@assessmentPoints").then((assessmentPoints: any) => {
            customPoints.features = assessmentPoints;
            this.sendStartCalculationCall(theme, exportOptions, options, calculationScenarios, customPoints);
          });
        } else {
          this.sendStartCalculationCall(theme, exportOptions, options, calculationScenarios, customPoints);
        }
      });
    });
  }

  static setCalculationScenarios(calculationSettings: any) {
    cy.get("@situations").then((situations) => {
      const calculationScenarios: [] = [];
      if (calculationSettings.projectScenario !== "-") {
        calculationScenarios.push(situations.find((situation: string) => (situation.name = calculationSettings.projectScenario)));
      }
      if (calculationSettings.referenceScenario !== "-") {
        calculationScenarios.push(situations.find((situation: any) => (situation.name = calculationSettings.referenceScenario)));
      }
      if (calculationSettings.offsiteReductionScenario !== "-") {
        calculationScenarios.push(situations.find((situation: any) => (situation.name = calculationSettings.offsiteReductionScenario)));
      }

      // NOTE: Temporary scenarios could be added to calculationScenarios here
      cy.wrap(calculationScenarios).as("calculationScenarios");
    });
  }

  static getJobTypeFromString(jobType: string) {
    switch (jobType) {
      case "Berekening":
        return "CALCULATION";
      case "Rapportage":
        return "REPORT";
      default:
        throw new Error("Unknown job type: " + jobType);
    }
  }

  static getExportTypeFromString(exportType: string, withSources: string) {
    switch (exportType) {
      case "GML":
        if (withSources === "true") {
          return "GML_WITH_RESULTS";
        }
        return "GML_SOURCES_ONLY";
      case "PDF":
        return "PDF_PAA";
      default:
        throw new Error("Unknown export type: " + exportType);
    }
  }

  static getCalculationJobTypeFromString(calculationJobType: string) {
    switch (calculationJobType) {
      case "Projectberekening":
        return "PROCESS_CONTRIBUTION";
      case "Maximaal tijdelijk effect":
        return "MAX_TEMPORARY_EFFECT";
      case "Enkele situatie":
        return "SINGLE_SCENARIO";
      default:
        throw new Error("Unknown calculation job type: " + calculationJobType);
    }
  }

  static getCalculationMethodFromString(method: string) {
    switch (method) {
      case "Own2000-methode":
        return "FORMAL_ASSESSMENT";
      case "Alleen eigen rekenpunten":
        return "CUSTOM_POINTS";
      default:
        throw new Error("Unknown calculation method: " + method);
    }
  }

  static sendStartCalculationCall(theme: string, exportOptions: string, options: any, calculationScenarios: any, customPoints: any) {
    const exportPayload = {
      theme: theme,
      exportOptions: exportOptions,
      scenario: {
        options: options,
        situations: calculationScenarios,
        customPoints: customPoints
      }
    };

    cy.request({
      url: Cypress.env("url_ui_calculation"),
      headers: { "Content-Type": "application/json" },
      method: "POST",
      body: JSON.stringify(exportPayload)
    }).then((response: any) => {
      cy.wrap(response.body).as("calculationJobKey");
      this.waitForCalculationToFinish(response.body);
    });
  }

  static waitForCalculationToFinish(jobKey: string) {
    const checkAndReload = (count: number) => {
      cy.request({
        method: "GET",
        url: Cypress.env("url_ui_calculation") + "/" + jobKey
      }).then((response) => {
        cy.log("Response from getting calculation: " + JSON.stringify(response));

        switch (response.body.jobProgress.state) {
          // Unfinished calculation
          case "INITIALIZED":
          case "QUEUING":
          case "RUNNING":
            this.handleUnfinishedCalculation(count);
            checkAndReload(count++);
            break;

          // Finished calculation
          case "COMPLETED":
            cy.log("response body: " + JSON.stringify(response.body));
            cy.wrap(response.body.situationCalculations).as("situationCalculations");
            break;

          // Errors
          case "UNDEFINED":
          case "CANCELLED":
          case "ERROR":
          case "DELETED":
            throw new Error(
              "Something went wrong during the calculation. The state of the calculation job is "
                + response.body.jobProgress.state
                + " at count "
                + count
            );
          default:
            throw new Error("Unknown validation status: " + response.body.jobProgress.state);
        }
      });
    };

    checkAndReload(0);
    cy.log("Calculation done!");
  }

  static handleUnfinishedCalculation(count: number) {
    if (count <= 60) {
      // Numbers are copied from CalculationStatusPoller.java
      cy.wait(15000);
    } else {
      throw new Error("The calculation has exceeded the time limit");
    }
  }

  static validateResults(dataTable: any) {
    // Verify dataTable input
    if (dataTable.rawTable.length < 2) {
      throw new Error("The result validation is missing settings");
    } else if (dataTable.rawTable.length > 2) {
      throw new Error("The result validation can only have 1 line of settings");
    }

    dataTable.hashes().forEach((resultSettings: any) => {
      cy.get("@calculationJobKey").then((jobKey) => {
        cy.get("@situationCalculations").then((situationCalculations: any) => {
          // NOTE: This only work with a single situation, with some hardcoded settings for now. If you want to use other options, you can add them below
          const situationId = situationCalculations[0].situationId;
          const resultType = this.getResultTypeFromString(resultSettings.result);
          const summaryHexagonType = "CUSTOM_CALCULATION_POINTS";
          const summaryOverlappingHexagonType = "ALL_HEXAGONS";
          const emissionResultKey = this.getEmissionResultKeyFromString(resultSettings.pollutant);
          const extraParameters
            = "?summaryHexagonType="
            + summaryHexagonType
            + "&summaryOverlappingHexagonType="
            + summaryOverlappingHexagonType
            + "&emissionResultKey="
            + emissionResultKey;

          cy.request({
            method: "GET",
            url: Cypress.env("url_ui_calculation") + "/" + jobKey + "/" + situationId + "/" + resultType + extraParameters
          }).then((response) => {
            expect(response.body.statistics.countCalculationPoints).to.equal(+resultSettings.expectedPoints);
            expect(response.body.statistics.countCalculationPointsIncrease).to.equal(+resultSettings.expectedIncreasedPoints);
            expect(response.body.statistics.countCalculationPointsDecrease).to.equal(+resultSettings.expectedDecreasedPoints);
            expect(response.body.statistics.maxIncrease).to.equal(+resultSettings.expectedBiggestIncrease);
            expect(response.body.statistics.maxDecrease).to.equal(+resultSettings.expectedBiggestDecrease);
          });
        });
      });
    });
  }

  static getResultTypeFromString(result: string) {
    // Text from ScenarioResultType.java
    switch (result) {
      case "Projectberekening":
        return "PROJECT_CALCULATION";
      // Note: There are more result types which could be added here as needed
      default:
        throw new Error("Unknown result type " + result);
    }
  }

  static getEmissionResultKeyFromString(pollutant: string) {
    switch (pollutant) {
      case "NOx + NH3":
        return "NOXNH3_DEPOSITION";
      // Note: There are more pollutants which could be added here as needed
      default:
        throw new Error("Unknown pollutant " + pollutant);
    }
  }

  static exportCalculationJob(dataTable: any) {
    // Verify dataTable input
    if (dataTable.rawTable.length < 2) {
      throw new Error("The export is missing settings");
    } else if (dataTable.rawTable.length > 2) {
      throw new Error("The export can only have 1 line of settings");
    }

    // Build call body from calculation settings
    dataTable.hashes().forEach((calculationSettings: any) => {
      const theme = "OWN2000";
      const exportOptions: any = {
        appendices: [],
        demoMode: false,
        email: Cypress.env("email"),
        exportType: this.getExportTypeFromString(calculationSettings.exportType, calculationSettings.withSources), // Generates a result GML
        jobType: this.getJobTypeFromString(calculationSettings.jobType), // NOTE: Different from calculationJobType!!!
        name: "Calculation job 1",
        protectJob: false
      };

      cy.wrap(calculationSettings.withSources).as("withSources");

      const options: any = {
        calculatemaximumRange: 0
      };

      const customPoints: any = {
        type: "FeatureCollection",
        features: []
      };

      options.calculationJobType = "PROCESS_CONTRIBUTION";
      options.calculationMethod = this.getCalculationMethodFromString(calculationSettings.method);

      if (options.calculationMethod === "CUSTOM_POINTS") {
        cy.get("@assessmentPoints").then((assessmentPoints) => {
          customPoints.features = assessmentPoints;
          this.getExportCallPayload(theme, exportOptions, options, customPoints);
        });
      } else {
        this.getExportCallPayload(theme, exportOptions, options, customPoints);
      }
    });
  }

  static waitForExportedFileToBeReady() {
    cy.get("@exportPayload").then((exportPayload) => {
      cy.request({
        url: Cypress.env("url_ui_calculation"),
        headers: { "Content-Type": "application/json" },
        method: "POST",
        body: JSON.stringify(exportPayload)
      }).then((response) => {
        cy.wrap(response.body).as("exportJobKey");
        this.waitForExportToFinishAndDownloadResult(response.body);
      });
    });
  }

  static getExportCallPayload(theme: string, exportOptions: string, options: any, customPoints: any) {
    cy.get("@withSources").then((withSources: any) => {
      // Overwrite situations with calculationScenarios if they're required
      if (withSources === "true") {
        cy.get("@calculationScenarios").then((calculationScenarios) => {
          cy.wrap(calculationScenarios).as("situations");
        });
      }

      cy.get("@situations").then((situations) => {
        // Get payload
        const payload = {
          theme: theme,
          exportOptions: exportOptions,
          scenario: {
            options: options,
            situations: situations,
            customPoints: customPoints,
          }
        };

      cy.wrap(payload).as("exportPayload");
      });
    });
  }

  static waitForExportToFinishAndDownloadResult(jobKey: string) {
    const checkAndReload = (count: number) => {
      cy.request({
        method: "GET",
        url: Cypress.env("url_ui_calculation") + "/" + jobKey
      }).then((response: any) => {
        switch (response.body.jobProgress.state) {
          // Unfinished export
          case "INITIALIZED":
          case "PREPARING":
          case "CALCULATING":
          case "QUEUING":
          case "RUNNING":
          case "POST_PROCESSING":
            this.handleUnfinishedCalculation(count);
            checkAndReload(count++);
            break;

          // Finished export
          case "COMPLETED":
            cy.wrap(response.body.jobProgress.resultUrl).as("resultUrl");
            return ;

          // Errors
          case "UNDEFINED":
          case "CANCELLED":
          case "ERROR":
          case "DELETED":
            throw new Error(
              "Something went wrong during the export. The state of the job is " + response.body.jobProgress.state + " at count " + count
            );
          default:
            throw new Error("Unknown validation status: " + response.body.jobProgress.state);
        }
      });
    };

    checkAndReload(0);
    cy.log("Export done!");
  }

  static downloadExportedFile() {
    cy.get("@exportJobKey").then((exportJobKey) => {
      cy.get("@resultUrl").then((resultUrl) => {
        cy.intercept('file').as('testFile');
        cy.request({method: 'GET',  url: resultUrl, encoding:'binary'})
          .then(response => {
            const zipFilename = response.headers['content-disposition'].split('filename="')[1].slice(0, -1);
            cy.wrap(zipFilename).as("zipFilename");
            cy.writeFile('./cypress/fixtures/download/' + exportJobKey + '/' + zipFilename, response.body, 'binary');
          });
      });
    });
  }

  static unzipDownloadedFiles() {
    cy.get("@exportJobKey").then((exportJobKey) => {
      cy.get("@zipFilename").then((zipFilename) => {
        cy.task("unzipping", {
          outputPath: "./cypress/fixtures/download/" + exportJobKey + "/t/",
          inputFilePath: "./cypress/fixtures/download/" + exportJobKey + "/" + zipFilename,
          timeout: CommonPage.fileReadTimeOut
        }).then((unzippedFiles) => {
          // Save filenames
          const fileNames: any = Array.prototype.slice.call(unzippedFiles).map(function (file: any) {
            return file.path;
          });
          cy.wrap(fileNames).as("exportedFileNames");
        });
      });
    });
  }

  static importExportedFile() {
    cy.get("@exportJobKey").then((exportJobKey) => {
      cy.get("@exportedFileNames").then((exportedFileNames) => {
        ImportPage.importFile("download/" + exportJobKey + "/t/" + exportedFileNames[0]);
      });
    });
  }

  static assertFilenameIs(expectedFilename: string, isZipFile: boolean) {
    if (isZipFile) {
      cy.get("@zipFilename").then((zipFilename: any) => {
        const adjustedZipFilenameString = this.filenameRegexReplacer(zipFilename);

        assert.equal(expectedFilename, adjustedZipFilenameString);
      });
    } else {
      cy.get("@exportedFileNames").then((exportedFileNames) => {
        var count = 0;
        exportedFileNames.forEach((filename: any) => {
          const adjustedFilenameString = this.filenameRegexReplacer(filename);
          if (expectedFilename == adjustedFilenameString) {
            count++;
          }
        })
        assert.equal(count, 1);
      });
    }
  }

  static filenameRegexReplacer(filename: string) {
    // Regular expression to match datetime and reference strings
    const adjustFilenameRegex = /(\d{14})_([A-Za-z0-9]+)/;

    // Replace datetime and reference strings with placeholders
    return filename.replace(adjustFilenameRegex, '{datetime}_{reference}');
  }
}
