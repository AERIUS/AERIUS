/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class CalculationPage {
  static startCalculationAndWaitForItsCompletion(dataTable: any) {
    // Verify dataTable input
    if (dataTable.rawTable.length < 2) {
      throw new Error("The calculation is missing settings");
    } else if (dataTable.rawTable.length > 2) {
      throw new Error("The calculation can only have 1 line of settings");
    }

    // Build call body from calculation settings
    dataTable.hashes().forEach((calculationSettings: any) => {
      const theme: string = "OWN2000";
      const exportOptions: any = {
        appendices: [],
        exportType: "CALCULATION_UI", // Do the calculation without it being downloadable
        jobType: "CALCULATION",
        protectJob: false
      };

      const options: any = {
        calculatemaximumRange: 0
      };

      const customPoints: any = {
        type: "FeatureCollection",
        features: []
      };

      options.calculationJobType = "PROCESS_CONTRIBUTION";
      options.calculationMethod = this.getCalculationMethodFromString(calculationSettings.method);

      this.setCalculationScenarios(calculationSettings);
      cy.get("@calculationScenarios").then((calculationScenarios) => {
        if (options.calculationMethod === "CUSTOM_POINTS") {
          cy.get("@assessmentPoints").then((assessmentPoints) => {
            customPoints.features = assessmentPoints;
            this.sendStartCalculationCall(theme, exportOptions, options, calculationScenarios, customPoints);
          });
        } else {
          this.sendStartCalculationCall(theme, exportOptions, options, calculationScenarios, customPoints);
        }
      });
    });
  }

  static setCalculationScenarios(calculationSettings: any) {
    cy.get("@situations").then((situations) => {
      const calculationScenarios: any = [];
      if (calculationSettings.projectScenario !== "-") {
        calculationScenarios.push(situations.find((situation: any) => (situation.name = calculationSettings.projectScenario)));
      }
      if (calculationSettings.referenceScenario !== "-") {
        calculationScenarios.push(situations.find((situation: any) => (situation.name = calculationSettings.referenceScenario)));
      }
      if (calculationSettings.offsiteReductionScenario !== "-") {
        calculationScenarios.push(situations.find((situation: any) => (situation.name = calculationSettings.offsiteReductionScenario)));
      }

      // NOTE: Temporary scenarios could be added to calculationScenarios here
      cy.wrap(calculationScenarios).as("calculationScenarios");
    });
  }

  static getCalculationMethodFromString(method: string) {
    switch (method) {
      case "Wnb-methode":
        return "FORMAL_ASSESSMENT";
      case "Alleen eigen rekenpunten":
        return "CUSTOM_POINTS";
      default:
        throw new Error("Unknown calculation method: " + method);
    }
  }

  static sendStartCalculationCall(theme: string, exportOptions: string, options: any, calculationScenarios: any, customPoints: any) {
    const payload = {
      theme: theme,
      exportOptions: exportOptions,
      scenario: {
        options: options,
        situations: calculationScenarios,
        customPoints: customPoints
      }
    };

    cy.request({
      url: Cypress.env("url_ui_calculation"),
      headers: { "Content-Type": "application/json" },
      method: "POST",
      body: JSON.stringify(payload)
    }).then((response: any) => {
      cy.wrap(response.body).as("calculationJobKey");
      this.waitForCalculationToFinish(response.body);
    });
  }

  static waitForCalculationToFinish(jobKey: string) {
    const checkAndReload = (count: number) => {
      cy.request({
        method: "GET",
        url: Cypress.env("url_ui_calculation") + "/" + jobKey
      }).then((response: any) => {
        cy.log("Response from getting calculation: " + JSON.stringify(response));

        switch (response.body.jobProgress.state) {
          // Unfinished calculation
          case "CALCULATING":
          case "INITIALIZED":
          case "PREPARING":
          case "QUEUING":
          case "RUNNING":
          case "POST_PROCESSING":
            this.handleUnfinishedCalculation(count);
            checkAndReload(++count);
            break;

          // Finished calculation
          case "COMPLETED":
            cy.log("response body: " + JSON.stringify(response.body));
            cy.wrap(response.body.situationCalculations).as("situationCalculations");
            break;

          // Errors
          case "UNDEFINED":
          case "CANCELLED":
          case "ERROR":
          case "DELETED":
            throw new Error(
              "Something went wrong during the calculation. The state of the calculation job is "
                + response.body.jobProgress.state
                + " at count "
                + count
            );
          default:
            throw new Error("Unknown validation status: " + response.body.jobProgress.state);
        }
      });
    };

    checkAndReload(0);
    cy.log("Calculation done!");
  }

  static handleUnfinishedCalculation(count: number) {
    if (count <= 4) {
      // Numbers are copied from CalculationStatusPoller.java
      cy.wait(15000);
    } else {
      throw new Error("The calculation has exceeded the time limit");
    }
  }
}
