/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class CommonPage {
  static calculationYear: number = 2024;
  static fileReadTimeOut = 80_000;
  static visitDocumentTimeOut = 60_000;
  static options: any = {};
  static file: any = [];
  static files: any = [];
  static filePaths: any = [];
  static calculationResults: string = "";
  static apiKey: string = "";
  static url: string = "";
  static jobKey: string = "";
  static fileText: string = "";
  static lastResponse: any = "";
  static outputOptions: any = "";
  static receptorSet: any = "";

  static getApiKey() {
    this.apiKey = Cypress.env("API_KEY");
  }

  static getUrl(endpoint: string) {
    return Cypress.env(endpoint);
  }

  static createApiKeyValidEmail() {
    this.createApiKey(Cypress.env("email"));
  }

  static createApiKey(email: string) {
    cy.request({
      method: "post",
      failOnStatusCode: false,
      url: this.getUrl("url_api_key"),
      body: {
        email: email
      }
    }).as("shortlink");
  }

  static checkApiKeyResponse(responseCode: string) {
    cy.get("@shortlink").then((response: any) => {
      expect(response.status).to.eq(responseCode, JSON.stringify(response.body));
    });
  }

  static checkApiKeyResponseWithMessage(responseCode: string, message: string) {
    this.checkApiKeyResponse(responseCode);
    cy.get("@shortlink").then((response: any) => {
      expect(response.body.message).to.contain(message, JSON.stringify(response.body));
    });
  }

  static prepareRequest(filePath: string, outputType: string, situationType: string) {
    this.options = {
      calculationYear: this.calculationYear,
      outputType: outputType,
      name: this.formatFileName(filePath)
    };
    this.files = [{ fileName: filePath, situation: situationType }];
    this.filePaths = [filePath];
    this.url = this.getUrl("url_calculation");
  }

  static prepareRequestCalculationPointType(filePath: string, outputType: string, situationType: string, calculationPointsType: string) {
    this.prepareRequest(filePath, outputType, situationType);
    this.options.calculationPointsType = calculationPointsType;
  }

  static prepareRequestEdgehexagons(filePathProposed: string, situationType: string, filePathReference: string, outputType: string) {
    this.options = {
      calculationYear: this.calculationYear,
      outputType: outputType,
      name: this.formatFileName(filePathProposed),
      appendices: ["EDGE_EFFECT_REPORT"]
    };
    this.files = [
      { fileName: filePathProposed, situation: situationType },
      { fileName: filePathReference, situation: situationType }
    ];
    this.filePaths = [filePathProposed, filePathReference];
    this.url = this.getUrl("url_calculation");
  }

  static prepareRequestWithCalculationPoints(filePathsource: string, filePathCalculationPoints: string, outputType: string, situationType: string) {
    this.options = {
      calculationYear: this.calculationYear,
      outputType: outputType,
      name: "calculation for " + this.formatFileName(filePathsource) + " calculation points " + this.formatFileName(filePathCalculationPoints),
      calculationPointsType: "CUSTOM_POINTS"
    };
    this.files = [
      { fileName: filePathsource, situation: situationType },
      { fileName: filePathCalculationPoints, situation: situationType }
    ];
    this.filePaths = [filePathsource, filePathCalculationPoints];
    this.url = this.getUrl("url_calculation");
  }

  static prepareAnalyseRequestWithCalculationPoints(filePathsource: string, filePathCalculationPoints: string, situationType: string) {
    this.prepareAnalyseRequest(filePathsource, filePathCalculationPoints);
    this.files = [
      { fileName: filePathsource, situation: situationType },
      { fileName: filePathCalculationPoints, situation: "ALL", expectRcpHeight: true }
    ];
    this.filePaths = [filePathsource, filePathCalculationPoints];
  }

  static prepareAnalyseRequestWithCalculationPointsName(filePathsource: string, calculationPointsName: string, situationType: string) {
    this.prepareAnalyseRequest(filePathsource, calculationPointsName);
    this.options.receptorSetName = calculationPointsName
    this.files = [
      { fileName: filePathsource, situation: situationType },
    ];
    this.filePaths = [filePathsource];
  }

  static prepareAnalyseRequestWithSubstanceCalculationPoints(filePathsource: string, substance: string, filePathCalculationPoints: string, situationType: string) {
    this.prepareAnalyseRequest(filePathsource, filePathCalculationPoints);
    this.files = [
      { fileName: filePathsource, situation: situationType, substance: substance },
      { fileName: filePathCalculationPoints, situation: "ALL" }
    ];
    this.filePaths = [filePathsource, filePathCalculationPoints];
  }

  static prepareAnalyseRequestWithOptions(filePathsource: string, filePathOptions: string) {
    this.prepareAnalyseRequestWithOptionCommon(filePathsource, filePathOptions);
    this.files = [
      { fileName: filePathsource, situation: "REFERENCE", substance: "NH3" }
    ];
    this.filePaths = [filePathsource];
  }

  static prepareAnalyseRequestWithCalculationPointsAndOptions(filePathsource: string, filePathCalculationPoints: string, filePathOptions: string) {
    this.prepareAnalyseRequestWithOptionCommon(filePathsource, filePathOptions);
    this.files = [
      { fileName: filePathsource, situation: "REFERENCE", substance: "NH3" },
      { fileName: filePathCalculationPoints, situation: "ALL" }
    ];
    this.filePaths = [filePathsource, filePathCalculationPoints];
  }

  static prepareAnalyseRequestWithOptionCommon(filePathsource: string, filePathOptions: string) {
    this.url = this.getUrl("url_analysis_calculation");
    cy.readFile("cypress/fixtures/" + filePathOptions, {
      log: false,
      timeout: this.fileReadTimeOut
    }).then((jsonInput) => {
      cy.log("input json : ", jsonInput);
      this.options = jsonInput;
    });
  }


  static prepareAnalyseRequest(filePathsource: string, filePathCalculationPoints: string) {
    this.outputOptions = {
      sectorOutput: false,
      resultTypes: ["CONCENTRATION", "DEPOSITION"]
    };
    this.options = {
      calculationYear: this.calculationYear,
      outputType: "GML",
      name: "calculation for " + this.formatFileName(filePathsource) + " receptors " + this.formatFileName(filePathCalculationPoints),
      calculationPointsType: "CUSTOM_POINTS",
      outputOptions: this.outputOptions
    };
    this.url = this.getUrl("url_analysis_calculation");
  }

  static prepareSummarizeFileRequestAndValidate(filePathSource: string, filePathResults: string) {
    this.url = this.getUrl("url_register_analyse_file");
    this.uploadSingleSource(filePathSource, this.url);
    cy.get("@shortlink").then((response: any) => {
      expect(response.response.statusCode).to.eq(200, JSON.stringify(response.response.body));
      this.lastResponse = response.response.body;
      cy.readFile("cypress/fixtures/register_json_results/AERIUS_projectberekening_register.json", {
        log: false,
        timeout: this.fileReadTimeOut
      }).then((jsonInput) => {
        expect(JSON.stringify(this.lastResponse)).to.eq(JSON.stringify(jsonInput));
      });
    });
  }

  static prepareRegisterRequest(filePathSource: string, outputType: string) {
    this.options = {
      calculationYear: this.calculationYear,
      outputType: outputType,
      name: this.formatFileName(filePathSource)
    };
    this.files = [
      { fileName: filePathSource, situation: "DEFINED_BY_FILE" }
    ];
    this.filePaths = [filePathSource];

    if (outputType == "JSON") {
      this.url = this.getUrl("url_register_calculation");
    } else {
      this.url = this.getUrl("url_register_calculation_permit");
      cy.readFile("cypress/fixtures/register_json_results/RegisterCalculationResults.json", {
        log: false,
        timeout: this.fileReadTimeOut
      }).then((jsonInput) => {
        this.calculationResults = jsonInput;
      });
    }
  }

  static updateAnalysisCalculationYear(calculationYear: string) {
    this.options.calculationYear = calculationYear;
  }

  static updateAnalysisOutputType(outputType: string) {
    this.options.outputType = outputType;
  }

  static useReceptorHeight() {
    this.options.useReceptorHeight = true;
  }

  static updateAnalysisSectorOutput(isSectorOutput: string) {
    this.options.outputOptions.sectorOutput = isSectorOutput == "true";
  }

  static updateStackingSource(stacking: boolean) {
    this.options.stacking = stacking;
  }

  static useForcedAggregation(aggregate: boolean) {
    this.options.aggregate = aggregate;
  }

  static updateAnalysisResultType(dataTable: any) {
    var resultTypes: any = [];
    dataTable.hashes().forEach((row) => {
      if (["CONCENTRATION", "DEPOSITION", "DRY_DEPOSITION", "WET_DEPOSITION"].includes(row.resultType)) {
        resultTypes.push(row.resultType);
      } else {
        throw new Error("Unknown resultType used :" + row.resultType);
      }
    });
    cy.log(JSON.stringify(resultTypes));
    this.options.outputOptions.resultTypes = resultTypes;
  }

  static updateAnalysisMeteoYear(meteoYear: string) {
    this.options.meteoYear = meteoYear;
  }

  static specifyRoadOps(roadOpsType: string) {
    switch (roadOpsType) {
      case "DEFAULT":
      case "OPS_ROAD":
      case "OPS_ALL":
        this.options.roadOPS = roadOpsType;
        return;
      default:
        throw new Error("Unknown roadOPS method: " + roadOpsType);
    }
  }

  static updateAnalysisSubReceptors(subReceptorType: string) {
    if (["DISABLED", "ENABLED", "ENABLED_OUTSIDE_HABITATS"].includes(subReceptorType)) {
      this.options.subReceptors = subReceptorType;
    } else {
      throw new Error("Unknown subReceptor setting used :" + subReceptorType);
    }
  }

  static doNotUseMaxDistance() {
    this.options.withOwN2000MaxDistance = false;
  }

  static setAnalysisOpsOptions(dataTable: any) {
    expect(dataTable.hashes().length).be.eq(1, "Expect only one row for ops settings.");
    let opsOptions: any = {};
    dataTable.hashes().forEach((opsOption: any) => {
      opsOptions.rawInput = opsOption.rawInput == "true" ? true : false;
      opsOptions.year = opsOption.year;
      opsOptions.compCode = opsOption.compCode;
      opsOptions.molWeight = opsOption.molWeight;
      opsOptions.phase = opsOption.phase;
      opsOptions.loss = opsOption.loss;
      if (opsOption.diffCoeff != "null") {
        opsOptions.diffCoeff = opsOption.diffCoeff;
      }
      if (opsOption.washout != "null") {
        opsOptions.washout = opsOption.washout;
      }
      if (opsOption.convRate != "null") {
        opsOptions.convRate = opsOption.convRate;
      }
      opsOptions.roughness = opsOption.roughness;
      if (["ACTUAL", "PROGNOSIS"].includes(opsOption.chemistry)) {
        opsOptions.chemistry = opsOption.chemistry;
      } else {
        throw new Error("Unknown opsOptions chemistry used :" + opsOption.chemistry);
      }
    });
    cy.log("OpsOptions =" + JSON.stringify(opsOptions));
    this.options.opsOptions = opsOptions;
  }

  static validateAnalysisCSVMetadata(fileName: string, dataTable: any) {
    cy.get("@jobKeyShort").then((jobKeyShort) => {
      let filePathsource = this.zipUnpackPath(jobKeyShort) + fileName;
      cy.readFile(filePathsource, {
        log: false,
        timeout: this.fileReadTimeOut
      }).then((jsonInput) => {
        dataTable.hashes().forEach((row: any) => {
          expect(row.ops_version).to.eq(jsonInput.ops_version, "Validate ops_version");
          expect(row.srm_version).to.eq(jsonInput.srm_version, "Validate srm_version");
          if (row.api_meteo_year != "") {
            expect(row.api_meteo_year).to.eq(jsonInput.api_meteo_year, "Validate api_meteo_year");
          }
          expect(row.source_stacking).to.eq(jsonInput.source_stacking, "Validate source_stacking");
          expect(row.receptor_count).to.eq(jsonInput.receptor_count, "Validate receptor_count");
          expect(row.max_distance).to.eq(jsonInput.max_distance, "Validate max_distance");
          expect(row.sub_receptors).to.eq(jsonInput.sub_receptors, "Validate sub_receptors");
          expect(row.forced_aggregation).to.eq(jsonInput.forced_aggregation, "Validate forced_aggregation");
          expect(row.use_receptor_height).to.eq(jsonInput.use_receptor_height, "Validate use_receptor_height");
        });
      });
    });
  }

  static validateAnalysisCSVSummary(fileName: string, dataTable: any) {
    cy.get("@jobKeyShort").then((jobKeyShort) => {
      let filePathsource = this.zipUnpackPath(jobKeyShort) + fileName;
      cy.readFile(filePathsource, {
        log: false,
        timeout: this.fileReadTimeOut
      }).then((csvInput: string) => {
        let result = this.csvToJson(csvInput);
        dataTable.hashes().forEach((row: any) => {
          expect(result[row.row].result_type).to.eq(row.result_type, "Validate row.result_type");
          expect(result[row.row].sector).to.eq(row.sector, "Validate row.sector");
          expect(result[row.row].substance).to.eq(row.substance, "Validate row.substance");
        });
      });
    });
  }

  static validateAnalysisCSVResultFiles(dataTable: any) {
    dataTable.hashes().forEach((row: any) => {
      cy.get("@jobKeyShort").then((jobKeyShort) => {
        let filePathsource = this.zipUnpackPath(jobKeyShort) + row.filename;
        cy.readFile(filePathsource, {
          log: false,
          timeout: this.fileReadTimeOut
        }).then((csvInput) => {
          expect(csvInput).not.eq(0);
        });
      });
    });
  }

  static validateAnalysisGMLSectorOutput(dataTable: any) {
    cy.get("@jobKeyShort").then((jobKeyShort: any) => {
      cy.task("readdir", { path: this.zipUnpackPath(jobKeyShort) }).then((files: any) => {
        expect(files.length).to.be.equal(dataTable.hashes().length + 1, "Validate total count of files");
        var sectorCount = 0;
        dataTable.hashes().forEach((row: any) => {
          if (files.find((fileName: any) => fileName.includes(row.sector + ".gml")) != undefined) {
            sectorCount++;
          }
        });
        expect(sectorCount).to.be.equal(dataTable.hashes().length, "Validate sector output files");
      });
    });
  }

  static checkRequest(code: any, dataTableErrors = null) {
    var formData = new FormData();
    cy.log("we post to " + this.url);
    this.checkPostRequest(code, formData, dataTableErrors);
  }

  static checkRegisterRequest(code: any) {
    var formData = new FormData();
    if (this.url == this.getUrl("url_register_calculation_permit")) {
      cy.log("calculationResults : " + JSON.stringify(this.calculationResults));
      formData.set("calculationResults", new Blob([JSON.stringify(this.calculationResults)], { type: "application/json" }));
    }
    this.checkPostRequest(code, formData);
  }

  static checkPostRequest(code: string, formData: any, dataTableErrors = null) {
    formData.set("options", new Blob([JSON.stringify(this.options)], { type: "application/json" }));
    formData.set("files", new Blob([JSON.stringify(this.files)], { type: "application/json" }));
    const method = "POST";
    this.filePaths.forEach((filePath: any) => {
      cy.log("lets send a request " + filePath);
      this.addFileToForm(formData, filePath, "fileParts");
    });
    cy.log("options : " + JSON.stringify(this.options));
    cy.log("files : " + JSON.stringify(this.files));
    cy.log("url : " + this.url);

    cy.form_request(this.url, this.apiKey, method, formData).then((response: any) => {
      cy.log("form response : " + JSON.stringify(response.response));
      cy.wrap(response.response.body.jobKey).as("jobKey");
      // Add extra key to use in directory name and fix log filename error
      cy.wrap(Math.floor(Date.now() / 1_000).toString(36)).as("jobKeyShort");
      expect(response.response.statusCode).to.eq(code, JSON.stringify(response.response));
      cy.log(response.response.body.message)
      if (dataTableErrors != null) {
        dataTableErrors.hashes().forEach((row: any) => {
          expect(response.response.body.message).to.contain(row.errorMessage, "validate error message contains");
        });
      }
    });
  }

  static editJob(edit: string) {
    cy.get("@jobKey").then((jobKey) => {
      cy.request({
        url: this.getUrl("url_job") + jobKey + edit,
        headers: { "api-key": this.apiKey },
        method: "post"
      });
    });
  }

  static deleteJob() {
    cy.get("@jobKey").then((jobKey) => {
      cy.request({
        url: this.getUrl("url_job") + jobKey,
        headers: { "api-key": this.apiKey },
        method: "DELETE"
      });
    });
  }

  static validateJobStatus(jobStatus: string) {
    cy.get("@jobKey").then((jobKey) => {
      cy.request({
        url: this.getUrl("url_job") + jobKey,
        headers: { "api-key": this.apiKey }
      }).then((response) => {
        expect(response.body.numberOfPointsCalculated).not.to.be.null;
        expect(response.body.jobKey.charAt(0)).to.eq("t", "First character should be t");
        expect(response.body.jobKey).to.have.lengthOf(33);
        expect(response.body.status).not.to.be.null;
        if (jobStatus == "INITIALIZED" && response.body.status == "RUNNING") {
          // due to timing a job can already be status RUNNING when testing for INITIALIZED
          expect(response.body.status).to.eq("RUNNING", "Validate jobstatus INITIALIZED but got RUNNING due to timing issues");
        } else {
          expect(response.body.status).to.eq(jobStatus, "Validate jobStatus");
        }
      });
    });
  }

  static getJob() {
    cy.get("@jobKey").then((jobKey) => {
      cy.request({
        url: this.getUrl("url_job") + jobKey,
        headers: { "api-key": this.apiKey }
      }).then((response) => {
        expect(response.body.numberOfPointsCalculated).not.to.be.null;
        expect(response.body.jobKey).to.have.lengthOf(33);
        expect(response.body.status).not.to.be.null;
      });
    });
  }

  static getJobs() {
    cy.request({
      url: this.getUrl("url_job"),
      headers: { "api-key": this.apiKey }
    }).as("getAllJobs");
  }

  static checkTaskStatusAndFilterOnStatus(expectedStatus: string) {
    this.checkTaskStatus(expectedStatus)
    this.filterOnStatus(expectedStatus)
  }

  static checkTaskStatus(expectedStatus: string) {
    cy.get("@jobKey").then((jobKey) => {
      const checkAndReload = (count: number) => {
        cy.request({
          method: "GET",
          url: this.getUrl("url_job") + jobKey,
          headers: { "api-key": this.apiKey }
        }).then((response) => {
          let body = response.body;
          expect(body)
            .to.have.nested.property("status")
            .not.equal("ERROR", "Job had an exception before complete (" + JSON.stringify(body) + ")");
          if (body["status"] == expectedStatus) {
            cy.log("Job has the expected status");
          // any calculation should be finished within 3 minutes otherwise cancel  
          } else if (count <= 18) {
            cy.log("Count: " + count + ", number of points calculated " + response.body["numberOfPointsCalculated"]);
            cy.wait(10_000);
            checkAndReload(count + 1);
          } else {
            throw new Error("Validation of task status exceeds time limit. Number of point calculated " + response.body["numberOfPointsCalculated"]);
          }
        });
      };
      checkAndReload(0);
    });
  }

  static filterOnStatus(expectedStatus: string) {
    cy.get("@jobKey").then((jobKey) => {
      cy.request({
        method: "GET",
        url: this.getUrl("url_job") + "?status=" + expectedStatus,
        headers: { "api-key": this.apiKey }
      }).then((response) => {
        let filteredJobs: any = [];
        response.body.forEach((body: any) => {
          filteredJobs.push(body["jobKey"]);
        })
        expect(filteredJobs).to.include(jobKey);
      })
    })
  }

  static removeJobsOnStatus(status: string) {
    this.getJobs();
    cy.get("@getAllJobs").then((response: any) => {
      response.body.forEach((job: any) => {
        if (job.status == status) {
          cy.wrap(job.jobKey).as("jobKey");
          this.deleteJob();
          cy.log("Job " + job.jobKey + " with status " + status + " deleted");
        }
      });
    });
  }

  static checkTaskStatusCompleteAndDownload() {
    this.checkTaskStatus("COMPLETED");
    cy.get("@jobKey").then((jobKey) => {
      cy.get("@jobKeyShort").then((jobKeyShort) => {
        cy.request({
          method: "GET",
          url: this.getUrl("url_job") + jobKey,
          headers: { "api-key": this.apiKey }
        }).then((response) => {
          const body = response.body;
          if (body["status"] == "COMPLETED") {
            const filePath = body["resultUrl"];
            cy.downloadFile(filePath, "./cypress/fixtures/download/" + jobKeyShort + "/", "result.zip");
          }
        });
        cy.readFile("./cypress/fixtures/download/" + jobKeyShort + "/" + "result.zip");
        cy.log("download done");
      });
    });
  }

  static validateSource(filePath: string) {
    this.url = this.getUrl("url_utility_validate");
    this.uploadSource(filePath, this.url);
    cy.get("@shortlink").then((response: any) => {
      expect(response.response.statusCode).to.eq(200, JSON.stringify(response.response));
      this.lastResponse = response.response.body;
    });
  }

  static validateSourceReport(filePath: string) {
    this.url = this.getUrl("url_utility_validate_report");
    this.uploadSource(filePath, this.url);
    cy.get("@shortlink").then((response: any) => {
      expect(response.response.statusCode).to.eq(200, JSON.stringify(response.response.body));
      expect(response.response.body).not.to.be.empty;
      this.lastResponse = response.response.body;
    });
  }

  static validateSourceConvert(filePath: string) {
    this.url = this.getUrl("url_utility_validate_and_convert");
    this.uploadSource(filePath, this.url);
    cy.get("@shortlink").then((response: any) => {
      expect(response.response.statusCode).to.eq(200, JSON.stringify(response.response.body));
      expect(response.response.body.filePart).not.to.be.null;
      expect(response.response.body.successful).not.to.be.null;
      expect(response.response.body.errors).not.to.be.null;
      expect(response.response.body.warnings).not.to.be.null;
      this.lastResponse = response.response.body;
    });
  }

  static convertSource(filePath: string) {
    this.url = this.getUrl("url_utility_convert");
    this.uploadSource(filePath, this.url);
    cy.get("@shortlink").then((response: any) => {
      expect(response.response.statusCode).to.eq(200, JSON.stringify(response));
      this.lastResponse = response.response.body;
    });
  }

  static uploadSource(filePath: string, url: string) {
    var formData: any = new FormData();
    formData.set("strict", false);
    const method = "POST";
    this.addFileToForm(formData, filePath, "filePart");
    cy.form_request(url, this.apiKey, method, formData).as("shortlink");
  }

  static uploadSingleSource(filePath: string, url: string) {
    var formData = new FormData();
    const method = "POST";
    this.addFileToForm(formData, filePath, "file");
    cy.form_request(url, this.apiKey, method, formData, "nl").as("shortlink");
  }
  
  static addFileToForm(formData: FormData, filePath: string, field: string) {
    if (this.isPdfFile(filePath)) {
      cy.fixture(filePath, "binary")
        .as("file")
        .then((file) => {
          let localFile: any = "";
          localFile = Cypress.Blob.binaryStringToBlob(file);
          formData.append(field, localFile, filePath);
        });
    } else {
      cy.fixture(filePath)
        .as("file")
        .then((file) => {
          let localFile: any = "";
          if (this.isZipFile(filePath)) {
            localFile = Cypress.Blob.base64StringToBlob(file);
          } else {
            localFile = new Blob([file], { type: "application/xml" });
          }
          formData.append(field, localFile, filePath);
        });
    }
  }

  static validateResponseWarnings(dataTable: any) {
    if (dataTable == null) {
      expect(this.lastResponse.warnings).to.have.lengthOf(0);
    } else {
      var warningsExpected = dataTable.hashes().length;
      var warningsResponse = this.lastResponse.warnings.length;
      var warningsMatch: any = 0;
      expect(warningsResponse).to.eq(warningsExpected, "Warning content " + JSON.stringify(this.lastResponse.warnings));
      dataTable.hashes().forEach((warning: any) => {
        this.lastResponse.warnings.forEach((row: any) => {
          if (row.code == warning.code) {
            warningsMatch++;
          }
        });
      });
    }
    expect(warningsResponse).to.eq(warningsMatch, "Response list should have expected warnings : " + JSON.stringify(dataTable));
  }

  static validateResponseErrors(dataTable: any) {
    if (dataTable == null) {
      expect(this.lastResponse.errors).to.have.lengthOf(0);
    } else {
      var errorsExpected = dataTable.hashes().length;
      var errorsResponse = this.lastResponse.errors.length;
      var errorsMatch: any = 0;
      expect(errorsResponse).to.eq(errorsExpected, "Response list should have length expected");
      dataTable.hashes().forEach((error: any) => {
        for (let row of this.lastResponse.errors) {
          if (row.code == error.code) {
            errorsMatch++;
            break;
          }
        }
      });
    }
    expect(errorsResponse).to.eq(errorsMatch, "Response list should have length expected errors validation : " + JSON.stringify(dataTable));
  }

  static validateResponseErrorMessages(dataTable: any) {
    expect(dataTable.hashes().length).be.eq(1, "Expect only one row for error message.");
    dataTable.hashes().forEach((error: any) => {
      expect(this.lastResponse.status).to.eq(400);
      expect(this.lastResponse.message).to.contains(dataTable.hashes()[0].message);
    });    
  }

  static extractDownloadedResults() {
    cy.get("@jobKeyShort").then((jobKeyShort: any) => {
      cy.task("unzipping", {
        outputPath: this.zipUnpackPath(jobKeyShort),
        inputFilePath: this.downloadFilePath(jobKeyShort),
        timeout: this.fileReadTimeOut
      });
    });
  }

  static extractDownloadedResultsAndValidateFilesCount(fileCount: number)  {
    this.extractDownloadedResults();
    cy.get("@jobKeyShort").then((jobKeyShort: any) => {
      cy.task("readdir", { path: this.zipUnpackPath(jobKeyShort) }).then((files: any) => {
        expect(files.length).to.be.equal(fileCount, "Expected filecount does not match");
      });
    });
  }

  static downloadFilePath(jobKeyShort: string) {
    return "./cypress/fixtures/download/" + jobKeyShort + "/" + "result.zip"
  }

  static zipUnpackPath(jobKeyShort: string) {
    return "./cypress/fixtures/download/" + jobKeyShort + "/t/";
  }

  static checkForEdgeHexagonAppendix() {
    cy.get("@jobKeyShort").then((jobKeyShort: any) => {
      cy.task("readdir", { path: this.zipUnpackPath(jobKeyShort) }).then((files: any) => {
        expect(files.length).to.be.equal(2);
        expect(files[0]).to.match(/extra_beoordeling/);
        expect(files[1]).to.match(/projectberekening/);
      });
    });
  }

  static validateJsonResultFile(situation: string, fileSource: string) {
    cy.get("@jobKeyShort").then((jobKeyShort: any) => {
      let resultDir = this.zipUnpackPath(jobKeyShort);
      let filePathSource = "cypress/fixtures/register_json_results/" + fileSource;
      cy.task("readdir", {
        path: resultDir,
        timeout: this.fileReadTimeOut
      }).then((files: any) => {
        // find situation to compaire
        files.forEach((file: any) => {
          if (file.includes(situation)) {
            let filePathResult = resultDir + file;
            this.compareJsonResults(filePathSource, filePathResult);
          }
        });
      });
    });
  }

  static validateSummaryResultFile(filePath: string) {
    cy.get("@jobKeyShort").then((jobKeyShort: any) => {
      let resultDir = this.zipUnpackPath(jobKeyShort);
      let filePathSource = "./cypress/fixtures/" + filePath;
      cy.task("readdir", {
        path: resultDir,
        timeout: this.fileReadTimeOut
      }).then((files: any) => {
        // find situation to compaire
        files.forEach((file: any) => {
          if (file.includes(".summary")) {
            let filePathResult = resultDir + file;
            this.compareJsonResults(filePathSource, filePathResult);
          }
        });
      });
    });
  }

  static compareJsonResults(filePathSource: string, filePathResult: string) {
    cy.readFile(filePathSource, {
      log: false,
      timeout: this.fileReadTimeOut
    }).then((inputCsv: string) => {
      cy.readFile(filePathResult, {
        log: false,
        timeout: this.fileReadTimeOut
      }).then((resultCsv: string) => {
        let inputJson = this.csvToJson(inputCsv);
        let resultJson = this.csvToJson(resultCsv);
        for (var i = 0; i < inputJson.length; i++){
          var obj = resultJson[i];
          for (var key in obj){
            expect(obj[key]).to.be.equal(resultJson[i][key], "validate row " + i + " value " + key);
          }
        };
      });
    });
  }

  static validateResultsFile(filePathsource: string) {
    cy.get("@jobKeyShort").then((jobKeyShort: any) => {
      let resultDir = this.zipUnpackPath(jobKeyShort);
      let filePathNameSource = "cypress/fixtures/" + filePathsource;
      cy.log("source file " + filePathNameSource);
      cy.task("readdir", {
        path: resultDir,
        timeout: this.fileReadTimeOut
      }).then((files: any) => {
        let filePathResult = resultDir + files[0];
        cy.log("result file " + filePathResult);
        this.compareEmissionSourcesAndDepositionResults(filePathNameSource, filePathResult);
      });
    });
  }

  static compareEmissionSourcesAndDepositionResults(filePathsource: string, filePathCompaire: string) {
    cy.readFile(filePathsource, {
      log: false,
      timeout: this.fileReadTimeOut
    }).then((inputXML: any) => {
      let resultOrigin = this.readXMLtoJson(inputXML);
      cy.readFile(filePathCompaire, {
        log: false,
        timeout: this.fileReadTimeOut
      }).then((inputXML2) => {
        let resultCalculated = this.readXMLtoJson(inputXML2);
        // SOURCE
        let i = 0;
        let startTime;
        resultCalculated.emissionSources.forEach((row: any) => {
          expect(row.sectorId).to.be.equal(resultOrigin.emissionSources[i].sectorId, "row " + i );
          expect(row.substance).to.be.equal(resultOrigin.emissionSources[i].substance, "row " + i );
          expect(row.value).to.be.equal(resultOrigin.emissionSources[i].value, "source in " + filePathsource);
          i++;
        });
        // OWN2000
        i = 0;
        startTime = Date.now();
        resultCalculated.receptorPoint.forEach((row: any) => {
          let logCompareData = JSON.stringify(row);
          if (row.receptorPointId != resultOrigin.receptorPoint[i].receptorPointId) {
            expect(row.receptorPointId).to.be.equal(resultOrigin.receptorPoint[i].receptorPointId, logCompareData);
          }
          if (row.substance != resultOrigin.receptorPoint[i].substance) {
            expect(row.substance).to.be.equal(resultOrigin.receptorPoint[i].substance, logCompareData);
          }
          if (row.resultType != resultOrigin.receptorPoint[i].resultType) {
            expect(row.resultType).to.be.equal(resultOrigin.receptorPoint[i].resultType, logCompareData);
          }
          if (row.value != resultOrigin.receptorPoint[i].value) {
            expect(row.value).to.be.equal(resultOrigin.receptorPoint[i].value, logCompareData);
          }         
          if (row.edgeEffect != resultOrigin.receptorPoint[i].edgeEffect) {
            expect(row.edgeEffect).to.be.equal(resultOrigin.receptorPoint[i].edgeEffect, logCompareData);
          }
          i++;
        });
        cy.log("ReceptorPoint forEach count : " + i + " Elapsed time : " + (Date.now() - startTime) );

        // CUSTOM_POINTS
        i = 0;
        resultCalculated.calculationPoint.forEach((row: any) => {
          expect(row.label).to.be.equal(resultOrigin.calculationPoint[i].label);
          expect(row.substance).to.be.equal(resultOrigin.calculationPoint[i].substance);
          expect(row.value).to.be.equal(resultOrigin.calculationPoint[i].value, "label " + row.label + " substance " + row.substance + " custom calculationPoint in " + filePathsource);
          i++;
        });

        // Length should be the same
        expect(resultCalculated.emissionSources.length).to.be.equal(resultOrigin.emissionSources.length, "Emission source length");
        expect(resultCalculated.receptorPoint.length).to.be.equal(resultOrigin.receptorPoint.length, "Receptor point length");
        expect(resultCalculated.calculationPoint.length).to.be.equal(resultOrigin.calculationPoint.length, "Calculation point length");

      });
    });
  }

  static calculateTestsWithAssessmentPointsAndCompareToExpectedResults(receptorFilePath: string, dataTable: any) {
    dataTable.hashes().forEach((test: any) => {
      this.prepareRequestWithCalculationPoints(test.filePath, receptorFilePath, "GML", "REFERENCE");
      this.checkRequest(200);
      this.getJob();
      this.checkTaskStatusCompleteAndDownload();
      this.extractDownloadedResults();
      cy.log(test.resultsPath);
      this.validateResultsFile(test.resultsPath);
    });
  }

  static readXMLtoJson(inputXML: any) {
    let es: any = [];
    let esRow: number = 0;
    let rp: any = [];
    let cp:any = [];
    let xmlDoc: any = Cypress.$.parseXML(inputXML);
    let childNodes: any = xmlDoc.documentElement.childNodes;
    let calculatedEdgeEffect: any;
    childNodes.forEach((childNode: any) => {
      if (childNode.nodeName == "imaer:featureMember") {
        let featureMembers = childNode.childNodes;
        featureMembers.forEach((featureMember: any) => {
          if (featureMember.nodeName.endsWith("EmissionSource")) {
            // SOURCE
            let emissionsource = featureMember.childNodes;
            emissionsource.forEach((source: any, index: any) => {
              if (source.nodeName == "imaer:emission") {
                let emissions = source.childNodes;
                emissions.forEach((emission: any) => {
                  if (emission.nodeName == "imaer:Emission") {
                    let valuesNodes = emission.childNodes;
                    valuesNodes.forEach((valueNode: any, index: any) => {
                      if (valueNode.nodeName == "imaer:value") {
                        es.push({
                          index: index,
                          id: esRow,
                          sectorId: featureMember.getAttribute("sectorId"),
                          substance: emission.getAttribute("substance"),
                          value: valueNode.firstChild.nodeValue
                        });
                        esRow++;
                      }
                    });
                  }
                });
              }
            }); 
          } else if (featureMember.nodeName == "imaer:ReceptorPoint") {
            // OWN2000
            let receptorPoint = featureMember.childNodes;
            // First determine edgeEffect
            receptorPoint.forEach((point: any) => {
              if (point.nodeName == "imaer:edgeEffect") {
                calculatedEdgeEffect = point.firstChild.nodeValue;
              }
            });
            receptorPoint.forEach((point: any) => {
              if (point.nodeName == "imaer:result") {
                let results = point.childNodes;
                results.forEach((result: any, index: any) => {
                  if (result.nodeName == "imaer:CalculationResult") {
                    let valuesNodes = result.childNodes;
                    valuesNodes.forEach((valueNode: any, index: any) => {
                      if (valueNode.nodeName == "imaer:value") {
                        rp.push({
                          index: index,
                          receptorPointId: featureMember.getAttribute("receptorPointId"),
                          resultType: result.getAttribute("resultType"),
                          substance: result.getAttribute("substance"),
                          value: valueNode.firstChild.nodeValue,
                          edgeEffect: calculatedEdgeEffect
                        });
                      }
                    });
                  }
                });
              }
            });
          } else if (featureMember.nodeName == "imaer:CalculationPoint") {
            // CUSTOM_POINTS
            let receptorPoint = featureMember.childNodes;
            receptorPoint.forEach((point: any, index: any) => {
              if (point.nodeName == "imaer:result") {
                let results = point.childNodes;
                results.forEach((result: any) => {
                  if (result.nodeName == "imaer:CalculationResult") {
                    let valuesNodes = result.childNodes;
                    valuesNodes.forEach((valueNode: any, index: any) => {
                      if (valueNode.nodeName == "imaer:value") {
                        cp.push({
                          index: index,
                          label: featureMember.getAttribute("gml:id"),
                          resultType: result.getAttribute("resultType"),
                          substance: result.getAttribute("substance"),
                          value: valueNode.firstChild.nodeValue
                        });
                      }
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
    return {
      emissionSources: es,
      receptorPoint: rp,
      calculationPoint: cp
    };
  }
  
  static isPdfFile(filePath: string) {
    return filePath.toUpperCase().includes(".PDF");
  }

  static isZipFile(filePath: string) {
    return filePath.toUpperCase().includes(".ZIP");
  }

  static readDownloadedPdfConvertToText(fileId: string) {
    cy.get("@jobKeyShort").then((jobKeyShort: any) => {
      cy.task("readPdf", { dirPath:this.zipUnpackPath(jobKeyShort), Id: fileId }).then((text: any) => {
        expect(text).to.not.be.null;

        this.fileText = this.getCleanText(text);
        cy.log("PDF has been converted to text:");
        cy.log(this.fileText);
      });
      // TODO: The strings `ff` en `fi` don't get read in readPdf()
    });
  }

  /*
   * Tester function: Use a mocked PDF to do the same as above function
   * Use the readDownloadedPdfConvertToText() function above once and then keep using the resulting PDF with this function until the test is made to speed up development
   */
  static useMockedPdf(fileDir: string) {
    cy.task("readPdf", fileDir).then((text: any) => {
      expect(text).to.not.be.null;

      this.fileText = this.getCleanText(text);
      cy.log("PDF converted to text has been loaded from mocked file:");
      cy.log(this.fileText);
    });
  }

  static testTextLengthOfPdf(length: number, precision: number) {
    // Test length to be around the right length (can vary due to different dates/times or small adjustments in PDF generation)
    expect(this.fileText.length).to.be.greaterThan(length * (1 - precision), "to be greater than");
    expect(this.fileText.length).to.be.lessThan(length * (1 + precision), "to be less than");
    cy.log("general tests for generated text of PDF have been performed");
  }

  static checkMarkersResultPage() {
    let resultsStartString = "Resultaten Hoogste bijdrageHexagonGebied";
    let resultsEndString = "Projectberekening";

    expect(this.fileText).to.contain(resultsStartString);
    expect(this.fileText).to.contain(resultsEndString);

    let results = this.fileText.substring(
      this.fileText.indexOf(resultsStartString),
      this.fileText.indexOf(resultsEndString, this.fileText.indexOf(resultsStartString)) // Find first occurance of end string AFTER first string
    );

    expect(results.length).to.be.lessThan(500);
    expect(results).to.contain("Gekarteerd oppervlak met toename (ha)");
    expect(results).to.contain("Gekarteerd oppervlak met afname (ha)");
    expect(results).to.contain("Grootste toename");
    expect(results).to.contain("Grootste afname");
  }

  static checkMarkersCardAndLegend() {
    //let cardWithLegendStartString = "Hoogste af- en toename op (bijna) overbelaste stikstofgevoelige Natura 2000 gebieden";
    let cardWithLegendStartString = "Hoogste af- en toename op (bijna) overbelaste stikstofgevoelige Natura  gebieden";
    let cardWithLegendEndString = "De letters bij de bronlabels op de kaart geven bij welke type situaties de bronnen horen:";

    expect(this.fileText).to.contain(cardWithLegendStartString);
    expect(this.fileText).to.contain(cardWithLegendEndString);

    let cardWithLegend = this.fileText.substring(
      this.fileText.indexOf(cardWithLegendStartString),
      this.fileText.indexOf(cardWithLegendEndString)
    );

    expect(cardWithLegend.length).to.be.lessThan(500);
    expect(cardWithLegend).to.contain("Habitatrichtlijn");
    expect(cardWithLegend).to.contain("Vogelrichtlijn");
    expect(cardWithLegend).to.contain("Vogelrichtlijn, Habitatrichtlijn");
    expect(cardWithLegend).to.contain("Grootste afname (projectberekening)");
    expect(cardWithLegend).to.contain("Grootste toename (projectberekening)");
    expect(cardWithLegend).to.contain("Hoogste totaal (achtergrond + projectberekening)");
  }

  static checkCustomCalculationPointsIncludedInOverview() {
    let startString = "Rekencon";
    let endString = "Totale emissie";

    expect(this.fileText).to.contain(startString);
    expect(this.fileText).to.contain(endString);

    let overviewConfigurationSubstring = this.fileText.substring(this.fileText.indexOf(startString), this.fileText.indexOf(endString));

    expect(overviewConfigurationSubstring.length).to.be.lessThan(100);
    expect(overviewConfigurationSubstring).to.contain("incl. eigen rekenpunten");
  }

  static checkCustomCalculationPointsIncludedInResults() {
    expect(this.fileText).to.contain("Per eigen rekenpunt");
  }

  static checkEdgeHexagonsPdf(dataTable: any) {
    dataTable.rows().forEach((expectedResult: any) => {
      let index: any = 0;
      for (index in expectedResult) {
        expect(this.fileText).to.contain(expectedResult[index]);
      }
    });
  }

  static checkPdfDoesNotHaveThisText(dataTable: any) {
    dataTable.rows().forEach((expectedResult: any) => {
      let index: any = 0;
      for (index in expectedResult) {
        expect(this.fileText).to.not.contain(expectedResult[index]);
      }
    });
  }

  static getCleanText(inputText: string) {
    return inputText.split("\n").join(" ").split("\u0000").join("").split("CONCEPT").join("").trim();
  }

  static csvToJson(data: string) {
    var lines = data.toString().split("\n");
    var result = [];
    var headers = lines[0].split(";");
    for (var i = 1; i < lines.length; i++) {
      var obj: any = {};
      var currentline = lines[i].split(";");
      if (currentline.length == headers.length) {
        for (var j = 0; j < headers.length; j++) {
          obj[headers[j]] = currentline[j] == undefined ? currentline[j] : currentline[j].replace("\r","");
        }
        result.push(obj);
      }
    }
    return result;
  }

  static createReceptorSet(receptorSetName: string, description: string, filePath: string, expectRcpHeight = "false", statusCodeValidation = true) {
    var formData = new FormData();
    this.receptorSet = {
      name: receptorSetName,
      description: description,
      expectRcpHeight: expectRcpHeight
    };
    const url = this.getUrl("url_receptor_sets");
    this.apiKey = Cypress.env("API_KEY");
    const method = "POST";

    formData.set("receptorSet", new Blob([JSON.stringify(this.receptorSet)], { type: "application/json" }));
    this.addFileToForm(formData, filePath, "filePart");
    cy.form_request(url, this.apiKey, method, formData).as("shortlink");

    cy.get("@shortlink").then((response: any) => {
      this.lastResponse = response.response.body;
      cy.log(JSON.stringify(this.lastResponse));
      if (statusCodeValidation) {
        expect(response.response.statusCode).to.eq(200, JSON.stringify(response.response.body));
      }
    });
  }

  static removeReceptorSet(label: string) {
    this.requestReceptorSet(this.getUrl("url_receptor_sets") + "/" + label, "DELETE");
    cy.get("@shortlink").then((response: any) => {
      expect(response.status).to.eq(200, JSON.stringify(response.body));
    });
  }

  static validateReceptorSet(label: string, description: string, isAvailable: string) {
    let found = false;
    this.requestReceptorSet(this.getUrl("url_receptor_sets"), "GET");
    cy.get("@shortlink").then((response: any) => {
      expect(response.status).to.eq(200, JSON.stringify(response.body));
      response.body.forEach((row: any) => {
        if (row.name == label && row.description == description) {
          found = true;
        }
      });
      if (found && !isAvailable) {
        throw new Error("receptorSet entry could " + isAvailable ? "not be found" : "should not exist");
      }
    });
  }

  static requestReceptorSet(url: string, method: string) {
    this.apiKey = Cypress.env("API_KEY");
    cy.request({
      method: method,
      headers: { "api-key": this.apiKey },
      failOnStatusCode: false,
      url: url
    }).as("shortlink");
  }

  static openSwaggerDocumentation() {
    cy.visit(this.getUrl("url_swagger_page"), { timeout: this.visitDocumentTimeOut });
  }

  static checkDocumentionLinks(dataTable: any) {
    dataTable.hashes().forEach((element: any) => {
      cy.get("." + element.classId);
      cy.get("." + element.classId).find("a").contains(element.description);
      cy.get("." + element.classId).find("a[target='_blank']").should("have.attr", "href").and("include", element.url);
      cy.request({
        url: element.url,
        failOnStatusCode: false
      }).then((response: any) => {
        expect(response.status).to.eq(200, "Validate url exists : " + element.url);
      });
    });
  }

  static checkDocumentionSpecification(dataTable: any) {
    var swaggerUrl = this.getUrl("url_swagger_page");
    dataTable.hashes().forEach((page: any) => {
      cy.intercept(swaggerUrl + page.pageId).as("getContext");
      cy.visit(swaggerUrl + "?urls.primaryName=" + page.pageName).as("shortlink");
      cy.wait("@getContext", { requestTimeout: 20_000 });

      cy.get(".information-container.wrapper");
      cy.get("select").first().find("option").should("have.length", dataTable.hashes().length); 
      cy.get(".info").first().find("a").contains(page.pageId);
      if (page.allowAuthorize == "true") {
        cy.get(".btn.authorize.unlocked");
      } else {
        cy.get(".btn.authorize.unlocked").should("not.exist");
      }     
    });
  }

  static formatFileName(fileName: string) {
    return fileName.replace(".", "_");
  }
}
