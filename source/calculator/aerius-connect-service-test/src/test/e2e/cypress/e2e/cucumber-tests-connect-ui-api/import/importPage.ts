/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

export class ImportPage {
  static importFile(filePath: string, expectWarnings: boolean = false, expectErrors: boolean = false) {
    // Create data object to send to the API
    var formData = new FormData();
    formData.append("importProperties", '{"year": "0","theme": "OWN2000"}');

    // Add the file at the filePath
    cy.fixture(filePath)
      .as("file")
      .then((file) => {
        const localFile: any = new Blob([file], { type: "application/xml", name: filePath });
        formData.append("filePart", localFile, filePath);
      });

    // Send the request
    cy.form_request(Cypress.env("url_ui_import"), "", "POST", formData).then((response: any) => {
      cy.wrap([response.response.body]).as("fileCodes");
      this.waitForUploadToFinish(response.response.body, expectWarnings, expectErrors);
    });
  }

  static waitForUploadToFinish(fileCode: string, expectWarnings: boolean, expectErrors: boolean = false) {
    // Creates function that polls the validation result for the calculation until it finishes
    const checkAndReload = (count: number) => {
      cy.request({
        method: "GET",
        url: Cypress.env("url_ui_import") + "/" + fileCode + "/validationresult"
      }).then((response) => {
        const body: any = response.body;

        switch (body.validationStatus) {
          // Unfinished import
          case "PENDING":
            this.handlePendingFile(count);
            checkAndReload(count++);
            break;

          // Finished import
          case "VALID":
            this.handleValidFile(body.childFileCodes, expectWarnings, expectErrors);
            break;
          case "VALID_WITH_WARNINGS":
            this.handleValidFileWithWarnings(body.childFileCodes, body.warnings, expectWarnings, expectErrors);
            break;
          case "INVALID":
            this.handleInvalidFile(body.error, expectErrors);
            break;

          // Errors
          case "FAILED":
            throw new Error("Upload failed (" + body.error + ")");
          default:
            throw new Error("Unknown validation status: " + body.validationStatus);
        }
      });
    };

    // Calls the function
    checkAndReload(0);
    cy.log("Download done!");
  }

  static handlePendingFile(count: number) {
    // Wait a set amount of time before repolling
    if (count <= 30) {
      // Same values as in FileValidationPoller.java
      cy.wait(200);
    } else {
      throw new Error("Upload exceeds time limit");
    }
  }

  static handleValidFile(childFileCodes: any, expectWarnings: boolean, expectErrors: boolean) {
    if (expectWarnings) {
      throw new Error("Upload validated succesfully, but warnings were expected");
    }
    if (expectErrors) {
      throw new Error("Upload validated succesfully, but errors were expected");
    }

    cy.log("Uploaded file is valid!");
    this.handleChildFiles(childFileCodes);
  }

  static handleValidFileWithWarnings(childFileCodes: any, warnings: any, expectWarnings: boolean, expectErrors: boolean) {
    // Allow the `created with an older version of AERIUS` warning
    if (warnings.length == 1 && warnings[0].reason === 5223) {
      cy.log("Uploaded file is made using an older version of AERIUS, but valid (with warnings)");
    } else if (!expectWarnings) {
      throw new Error("Upload validated as valid with warnings, but no warnings were expected");
    } else if (expectErrors) {
      throw new Error("Upload validated succesfully as valid with warnings, but errors were expected");
    } else {
      cy.log("Uploaded file is valid (with warnings)!");
    }

    this.handleChildFiles(childFileCodes, expectWarnings);
  }

  static handleInvalidFile(errorMessage: string, expectErrors: boolean) {
    if (!expectErrors) {
      throw new Error("Upload was upload and verified as invalid (" + errorMessage + ")");
    }
    cy.log("Uploaded file is invalid, as expected!");
  }

  static handleChildFiles(childFileCodes: [], expectWarnings: boolean = false) {
    // Start a poller for each child file if there are any
    if (childFileCodes.length > 0) {
      this.addToWrappedArray("fileCodes", childFileCodes);

      childFileCodes.forEach((childFileCode) => {
        this.waitForUploadToFinish(childFileCode, expectWarnings);
      });
    }
  }

  static getDataFromImportedFile() {
    cy.get("@fileCodes").then((fileCodes: any) => {
      cy.wrap([]).as("assessmentPoints");
      cy.wrap([]).as("emissionSources");
      cy.wrap([]).as("buildings");
      cy.wrap([]).as("calculationResults");
      cy.wrap([]).as("situations");

      fileCodes.forEach((fileCode: string) => {
        cy.request({
          method: "GET",
          url: Cypress.env("url_ui_import") + "/" + fileCode + "/json"
        }).then((response) => {
          this.addToWrappedArray("assessmentPoints", response.body.calculationPoints.features);
          this.addToWrappedArray("emissionSources", response.body.situation.emissionSources.features);
          this.addToWrappedArray("buildings", response.body.situation.buildings.features);

          if (response.body.situation.reference != null) {
            // Only add real situations
            this.addToWrappedArray("situations", [response.body.situation]);
          }

          if (response.body.situationResults != null) {
            this.addToWrappedArray("calculationResults", response.body.situationResults.results);
          }
        });
      });
    });
  }

  static addToWrappedArray(arrayToAddTo: string, newFeatures: any) {
    cy.get("@" + arrayToAddTo).then((array: any) => {
      cy.wrap(array.concat(newFeatures)).as(arrayToAddTo);
    });
  }

  static assertNumberOfAssessmentPoints(expectedAmount: number) {
    cy.get("@assessmentPoints").then((assessmentPoints) => {
      expect(assessmentPoints).to.have.lengthOf(expectedAmount);
    });
  }

  static assertNumberOfEmissionSources(expectedAmount: number) {
    cy.get("@emissionSources").then((emissionSources) => {
      expect(emissionSources).to.have.lengthOf(expectedAmount);
    });
  }

  static assertNumberOfBuildings(expectedAmount: number) {
    cy.get("@buildings").then((buildings) => {
      expect(buildings).to.have.lengthOf(expectedAmount);
    });
  }
}
