/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

beforeEach(() => {
  const url = Cypress.config("baseUrl") + "api/";
  const username = Cypress.env("USERNAME");
  const password = Cypress.env("PASSWORD");

  if (username && password) {
    // open root url to catch status 302 redirect AWS cognito login
    cy.clearCookies();
    cy.request({
      url: url,
      followRedirect: false,
      failOnStatusCode: false
      }).then((response) => {
      if (response.status == 302) {
        cy.log("got a 302 redirect to cognito with redirect url");
        const redirectUrl = response.redirectedToUrl;
        cy.visit(redirectUrl);
        cy.get("body").then((body) => {
          if (body.find("input[name=username]:visible").length > 0) {
            cy.get("input[name=username]:visible").type(username);
            cy.get("input[name=password]:visible").type(password);
            cy.get("input[name=signInSubmitButton]:visible").click();
          } else {
            cy.log("cognito user credentials still in cache");
          }
        });
      }
    });
  }
});
