const decompress = require("decompress");

const unzip = ({ outputPath, inputFilePath }) => decompress(inputFilePath, outputPath).then(files => {
  return files;
});

module.exports = {
  unzip
};
