<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<imaer:FeatureCollectionCalculator xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:imaer="http://imaer.aerius.nl/6.0" xmlns:gml="http://www.opengis.net/gml/3.2" gml:id="NL.IMAER.Collection" xsi:schemaLocation="http://imaer.aerius.nl/6.0 https://imaer.aerius.nl/6.0/IMAER.xsd">
    <imaer:metadata>
        <imaer:AeriusCalculatorMetadata>
            <imaer:project>
                <imaer:ProjectMetadata>
                    <imaer:year>2024</imaer:year>
                </imaer:ProjectMetadata>
            </imaer:project>
            <imaer:situation>
                <imaer:SituationMetadata>
                    <imaer:name>Situatie 1</imaer:name>
                    <imaer:reference>RmKz5spnUbvp</imaer:reference>
                    <imaer:situationType>REFERENCE</imaer:situationType>
                </imaer:SituationMetadata>
            </imaer:situation>
            <imaer:calculation>
                <imaer:CalculationMetadata>
                    <imaer:method>CUSTOM_POINTS</imaer:method>
                    <imaer:substance>NOX</imaer:substance>
                    <imaer:substance>NO2</imaer:substance>
                    <imaer:substance>NH3</imaer:substance>
                    <imaer:resultType>CONCENTRATION</imaer:resultType>
                    <imaer:resultType>DEPOSITION</imaer:resultType>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>meteo_year</imaer:key>
                            <imaer:value>2020</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>with_max_distance</imaer:key>
                            <imaer:value>true</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>sub_receptors_mode</imaer:key>
                            <imaer:value>DISABLED</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>sub_receptor_zoom_level</imaer:key>
                            <imaer:value>1</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                </imaer:CalculationMetadata>
            </imaer:calculation>
            <imaer:version>
                <imaer:VersionMetadata>
                    <imaer:aeriusVersion>build-413_2024-07-04-01-02_20240704_7bcd3a74ee</imaer:aeriusVersion>
                    <imaer:databaseVersion>build-413_2024-07-04-01-02_7bcd3a74ee_calculator_nl_latest</imaer:databaseVersion>
                </imaer:VersionMetadata>
            </imaer:version>
        </imaer:AeriusCalculatorMetadata>
    </imaer:metadata>
    <imaer:featureMember>
        <imaer:SRM2Road roadAreaType="NL" roadType="URBAN_ROAD_FREE_FLOW" sectorId="3100" gml:id="ES.1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Bron 1</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:EmissionSourceCharacteristics>
                    <imaer:heatContent>
                        <imaer:SpecifiedHeatContent>
                            <imaer:value>0.0</imaer:value>
                        </imaer:SpecifiedHeatContent>
                    </imaer:heatContent>
                    <imaer:emissionHeight>2.5</imaer:emissionHeight>
                    <imaer:spread>2.5</imaer:spread>
                </imaer:EmissionSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Curve>
                        <gml:LineString srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.1.CURVE">
                            <gml:posList>118076.0952300074 499902.18391813984 117896.33327981223 499525.26369998866 117606.39465046515 499200.53243511997 117710.7725570301 498875.8011702513 117919.52837015998 498533.6735876217 118215.26577209399 498278.5275937963 118621.17985317988 498243.73495827464</gml:posList>
                        </gml:LineString>
                    </imaer:GM_Curve>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>32.86496249566702</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>1505.2467236688058</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>61.310666149237974</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>339.06021100366337</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:vehicles>
                <imaer:StandardVehicle vehicleType="AUTO_BUS">
                    <imaer:vehiclesPerTimeUnit>100.0</imaer:vehiclesPerTimeUnit>
                    <imaer:timeUnit>DAY</imaer:timeUnit>
                    <imaer:stagnationFactor>0.0</imaer:stagnationFactor>
                    <imaer:maximumSpeed>0</imaer:maximumSpeed>
                    <imaer:strictEnforcement>false</imaer:strictEnforcement>
                </imaer:StandardVehicle>
            </imaer:vehicles>
            <imaer:vehicles>
                <imaer:StandardVehicle vehicleType="HEAVY_FREIGHT">
                    <imaer:vehiclesPerTimeUnit>200.0</imaer:vehiclesPerTimeUnit>
                    <imaer:timeUnit>DAY</imaer:timeUnit>
                    <imaer:stagnationFactor>0.0</imaer:stagnationFactor>
                    <imaer:maximumSpeed>0</imaer:maximumSpeed>
                    <imaer:strictEnforcement>false</imaer:strictEnforcement>
                </imaer:StandardVehicle>
            </imaer:vehicles>
            <imaer:vehicles>
                <imaer:StandardVehicle vehicleType="LIGHT_TRAFFIC">
                    <imaer:vehiclesPerTimeUnit>400.0</imaer:vehiclesPerTimeUnit>
                    <imaer:timeUnit>DAY</imaer:timeUnit>
                    <imaer:stagnationFactor>0.0</imaer:stagnationFactor>
                    <imaer:maximumSpeed>0</imaer:maximumSpeed>
                    <imaer:strictEnforcement>false</imaer:strictEnforcement>
                </imaer:StandardVehicle>
            </imaer:vehicles>
            <imaer:vehicles>
                <imaer:StandardVehicle vehicleType="NORMAL_FREIGHT">
                    <imaer:vehiclesPerTimeUnit>300.0</imaer:vehiclesPerTimeUnit>
                    <imaer:timeUnit>DAY</imaer:timeUnit>
                    <imaer:stagnationFactor>0.0</imaer:stagnationFactor>
                    <imaer:maximumSpeed>0</imaer:maximumSpeed>
                    <imaer:strictEnforcement>false</imaer:strictEnforcement>
                </imaer:StandardVehicle>
            </imaer:vehicles>
            <imaer:roadManager>STATE</imaer:roadManager>
            <imaer:trafficDirection>BOTH</imaer:trafficDirection>
            <imaer:elevation>NORMAL</imaer:elevation>
        </imaer:SRM2Road>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.0">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.0</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.0.POINT">
                    <gml:pos>117798.0 498718.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.034060813</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>2.984344</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>1.5600178</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>14.421869</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.933318</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>CP.1.POINT</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1.POINT">
                    <gml:pos>117585.0 498742.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.0042046383</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.25181457</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.19257647</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>1.3137804</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.12016677</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>CP.2.POINT</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.2.POINT">
                    <gml:pos>117237.0 502479.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>2.635236E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.008948049</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.012069632</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.041762765</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.0060320734</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>CP.3.POINT</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.3.POINT">
                    <gml:pos>118613.0 503215.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>2.0961123E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.0077981646</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.009600394</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.035750423</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.004817707</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>CP.4.POINT</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.4.POINT">
                    <gml:pos>118653.0 498173.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>0.0051648924</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.50375366</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.236557</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>2.354141</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.17160575</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>CP.5.POINT</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.5">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.5</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.5.POINT">
                    <gml:pos>118588.0 497629.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>9.2664757E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.046390604</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.042441346</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.28958184</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.03083854</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>CP.6.POINT</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.6">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.6</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.6.POINT">
                    <gml:pos>119607.0 498194.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>5.945011E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.02946181</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.02722872</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.17432296</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.019453352</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>CP.7.POINT</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.7">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.7.POINT">
                    <gml:pos>118589.0 496421.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>2.3290481E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.00904886</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0106672635</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.057886094</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.007628042</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>CP.8.POINT</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.8">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.8.POINT">
                    <gml:pos>119491.0 495791.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>1.8772899E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.0070544314</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.008598167</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.046049666</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006294073</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>CP.9.POINT</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.9">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.9</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.9.POINT">
                    <gml:pos>122790.0 496475.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>5.013411E-5</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.0040273834</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0038444903</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.022805369</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>6.3218985E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>CP.10.POINT</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.10">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.10</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.10.POINT">
                    <gml:pos>125500.0 495723.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>2.126E-5</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.002157</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.001958</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.013</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>CP.11.POINT</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.11">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.11</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.11.POINT">
                    <gml:pos>115405.0 499242.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>2.681E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.030288648</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.012279236</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.06971638</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006490123</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>CP.12.POINT</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.12">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.12</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.12.POINT">
                    <gml:pos>115072.0 499113.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>2.333232E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.018701319</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0106864255</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.050039694</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.005738703</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>CP.13.POINT</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.13">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.13</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.13.POINT">
                    <gml:pos>114782.0 497535.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>2.2640183E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.014985615</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.01036942</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.052708354</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.006451671</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>CP.14.POINT</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.14">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.14</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.14.POINT">
                    <gml:pos>114647.0 497785.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>2.2519016E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.010846889</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.010313924</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.05403462</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.0062877857</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>CP.15.POINT</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.15">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.15</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.15.POINT">
                    <gml:pos>113784.0 499117.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NH3">
                    <imaer:value>1.2842045E-4</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.0045401608</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0058817794</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.025628803</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.0031441364</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>CP.16.POINT</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
</imaer:FeatureCollectionCalculator>
