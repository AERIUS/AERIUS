<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<imaer:FeatureCollectionCalculator xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:imaer="http://imaer.aerius.nl/6.0" xmlns:gml="http://www.opengis.net/gml/3.2" gml:id="NL.IMAER.Collection" xsi:schemaLocation="http://imaer.aerius.nl/6.0 https://imaer.aerius.nl/6.0/IMAER.xsd">
    <imaer:metadata>
        <imaer:AeriusCalculatorMetadata>
            <imaer:project>
                <imaer:ProjectMetadata>
                    <imaer:year>2024</imaer:year>
                </imaer:ProjectMetadata>
            </imaer:project>
            <imaer:situation>
                <imaer:SituationMetadata>
                    <imaer:name>Aangevraagde Situatie</imaer:name>
                    <imaer:reference>RXTi6zdRKbzJ</imaer:reference>
                    <imaer:situationType>REFERENCE</imaer:situationType>
                </imaer:SituationMetadata>
            </imaer:situation>
            <imaer:calculation>
                <imaer:CalculationMetadata>
                    <imaer:method>CUSTOM_POINTS</imaer:method>
                    <imaer:substance>NOX</imaer:substance>
                    <imaer:substance>NO2</imaer:substance>
                    <imaer:substance>NH3</imaer:substance>
                    <imaer:resultType>DEPOSITION</imaer:resultType>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>with_max_distance</imaer:key>
                            <imaer:value>true</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>sub_receptors_mode</imaer:key>
                            <imaer:value>ENABLED_RECEPTORS_ONLY</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                    <imaer:option>
                        <imaer:CalculationOption>
                            <imaer:key>sub_receptor_zoom_level</imaer:key>
                            <imaer:value>1</imaer:value>
                        </imaer:CalculationOption>
                    </imaer:option>
                </imaer:CalculationMetadata>
            </imaer:calculation>
            <imaer:version>
                <imaer:VersionMetadata>
                    <imaer:aeriusVersion>build-413_2024-07-04-01-02_20240704_7bcd3a74ee</imaer:aeriusVersion>
                    <imaer:databaseVersion>build-413_2024-07-04-01-02_7bcd3a74ee_calculator_nl_latest</imaer:databaseVersion>
                </imaer:VersionMetadata>
            </imaer:version>
        </imaer:AeriusCalculatorMetadata>
    </imaer:metadata>
    <imaer:featureMember>
        <imaer:MooringMaritimeShippingEmissionSource sectorId="7510" gml:id="ES.1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Zeeschepen</imaer:label>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.1.POINT">
                            <gml:pos>84681.77489783871 434366.94328212604</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>24419.8987802</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:mooringMaritimeShipping>
                <imaer:StandardMooringMaritimeShipping shipType="S10000">
                    <imaer:description>Bulkschip GT 10.000 - 29.999</imaer:description>
                    <imaer:averageResidenceTime>14</imaer:averageResidenceTime>
                    <imaer:shorePowerFactor>0.9</imaer:shorePowerFactor>
                    <imaer:shipsPerTimeUnit>165</imaer:shipsPerTimeUnit>
                    <imaer:timeUnit>YEAR</imaer:timeUnit>
                </imaer:StandardMooringMaritimeShipping>
            </imaer:mooringMaritimeShipping>
            <imaer:mooringMaritimeShipping>
                <imaer:StandardMooringMaritimeShipping shipType="S30000">
                    <imaer:description>Bulkschip GT 30.000 - 59.999</imaer:description>
                    <imaer:averageResidenceTime>14</imaer:averageResidenceTime>
                    <imaer:shorePowerFactor>0.7</imaer:shorePowerFactor>
                    <imaer:shipsPerTimeUnit>825</imaer:shipsPerTimeUnit>
                    <imaer:timeUnit>YEAR</imaer:timeUnit>
                </imaer:StandardMooringMaritimeShipping>
            </imaer:mooringMaritimeShipping>
            <imaer:mooringMaritimeShipping>
                <imaer:StandardMooringMaritimeShipping shipType="S60000">
                    <imaer:description>Bulkschip GT 60.000 - 99.999</imaer:description>
                    <imaer:averageResidenceTime>14</imaer:averageResidenceTime>
                    <imaer:shorePowerFactor>0.0</imaer:shorePowerFactor>
                    <imaer:shipsPerTimeUnit>110</imaer:shipsPerTimeUnit>
                    <imaer:timeUnit>YEAR</imaer:timeUnit>
                </imaer:StandardMooringMaritimeShipping>
            </imaer:mooringMaritimeShipping>
        </imaer:MooringMaritimeShippingEmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:MooringInlandShippingEmissionSource sectorId="7610" gml:id="ES.2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Binnenvaartschepen</imaer:label>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.2.POINT">
                            <gml:pos>84729.04289536495 434538.1300839778</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>489.72</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:mooringInlandShipping>
                <imaer:StandardMooringInlandShipping shipType="M10">
                    <imaer:description>Binnenvaart</imaer:description>
                    <imaer:averageResidenceTime>6</imaer:averageResidenceTime>
                    <imaer:shorePowerFactor>0.0</imaer:shorePowerFactor>
                    <imaer:shipsPerTimeUnit>700</imaer:shipsPerTimeUnit>
                    <imaer:timeUnit>YEAR</imaer:timeUnit>
                    <imaer:percentageLaden>65</imaer:percentageLaden>
                </imaer:StandardMooringInlandShipping>
            </imaer:mooringInlandShipping>
        </imaer:MooringInlandShippingEmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="3720" gml:id="ES.3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Railverkeer</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:EmissionSourceCharacteristics>
                    <imaer:heatContent>
                        <imaer:CalculatedHeatContent>
                            <imaer:emissionTemperature>80.0</imaer:emissionTemperature>
                            <imaer:outflowDiameter>0.1</imaer:outflowDiameter>
                            <imaer:outflowVelocity>0.1</imaer:outflowVelocity>
                            <imaer:outflowDirection>VERTICAL</imaer:outflowDirection>
                            <imaer:outflowVelocityType>ACTUAL_FLOW</imaer:outflowVelocityType>
                        </imaer:CalculatedHeatContent>
                    </imaer:heatContent>
                    <imaer:emissionHeight>1.0</imaer:emissionHeight>
                    <imaer:spread>2.5</imaer:spread>
                    <imaer:timeVaryingProfile>
                        <imaer:StandardTimeVaryingProfile>
                            <imaer:standardType>INDUSTRIAL_ACTIVITY</imaer:standardType>
                        </imaer:StandardTimeVaryingProfile>
                    </imaer:timeVaryingProfile>
                </imaer:EmissionSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Curve>
                        <gml:LineString srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.3.CURVE">
                            <gml:posList>83634.21387158167 434154.8760499812 83585.66836060878 434452.53668305185</gml:posList>
                        </gml:LineString>
                    </imaer:GM_Curve>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>170.4</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SRM2Road roadAreaType="NL" roadType="NON_URBAN_ROAD_NATIONAL" sectorId="3100" gml:id="ES.4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Vrachtwagens (binnen terrein)</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:EmissionSourceCharacteristics>
                    <imaer:heatContent>
                        <imaer:SpecifiedHeatContent>
                            <imaer:value>0.0</imaer:value>
                        </imaer:SpecifiedHeatContent>
                    </imaer:heatContent>
                    <imaer:emissionHeight>2.5</imaer:emissionHeight>
                    <imaer:spread>2.5</imaer:spread>
                </imaer:EmissionSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Curve>
                        <gml:LineString srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.4.CURVE">
                            <gml:posList>83613.18610584456 434481.45576470427 83648.00441692374 434273.5407071172 83758.42820348914 434294.43169376464 83775.33995458474 434183.0130983113 84578.15072718175 434307.3642093085 84669.67314487559 434600.83283126156 84653.75620266797 434663.5057912041 83621.1445769484 434482.4505735923</gml:posList>
                        </gml:LineString>
                    </imaer:GM_Curve>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>78.72687844070799</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:vehicles>
                <imaer:CustomVehicle>
                    <imaer:vehiclesPerTimeUnit>5000.0</imaer:vehiclesPerTimeUnit>
                    <imaer:timeUnit>YEAR</imaer:timeUnit>
                    <imaer:description>Tankwagen</imaer:description>
                    <imaer:emissionFactor>
                        <imaer:Emission substance="NO2">
                            <imaer:value>0.0</imaer:value>
                        </imaer:Emission>
                    </imaer:emissionFactor>
                    <imaer:emissionFactor>
                        <imaer:Emission substance="PM10">
                            <imaer:value>0.0</imaer:value>
                        </imaer:Emission>
                    </imaer:emissionFactor>
                    <imaer:emissionFactor>
                        <imaer:Emission substance="NOX">
                            <imaer:value>5.9</imaer:value>
                        </imaer:Emission>
                    </imaer:emissionFactor>
                    <imaer:emissionFactor>
                        <imaer:Emission substance="NH3">
                            <imaer:value>0.0</imaer:value>
                        </imaer:Emission>
                    </imaer:emissionFactor>
                </imaer:CustomVehicle>
            </imaer:vehicles>
            <imaer:trafficDirection>A_TO_B</imaer:trafficDirection>
            <imaer:elevation>NORMAL</imaer:elevation>
        </imaer:SRM2Road>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="9999" gml:id="ES.5">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.5</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Personenauto's (binnen terrein)</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:EmissionSourceCharacteristics>
                    <imaer:heatContent>
                        <imaer:SpecifiedHeatContent>
                            <imaer:value>0.0</imaer:value>
                        </imaer:SpecifiedHeatContent>
                    </imaer:heatContent>
                    <imaer:emissionHeight>0.0</imaer:emissionHeight>
                    <imaer:spread>0.0</imaer:spread>
                    <imaer:timeVaryingProfile>
                        <imaer:StandardTimeVaryingProfile>
                            <imaer:standardType>CONTINUOUS</imaer:standardType>
                        </imaer:StandardTimeVaryingProfile>
                    </imaer:timeVaryingProfile>
                </imaer:EmissionSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.5.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>83764.46054504713 434292.70447645494 83868.49203809634 434311.01632898755 83876.18616282327 434273.3597581828 83771.17197791708 434255.199410417 83764.46054504713 434292.70447645494</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SRM2Road roadAreaType="NL" roadType="NON_URBAN_ROAD_NATIONAL" sectorId="3100" gml:id="ES.6">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.6</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Vrachtwagen (buiten terrein)</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:EmissionSourceCharacteristics>
                    <imaer:heatContent>
                        <imaer:SpecifiedHeatContent>
                            <imaer:value>0.0</imaer:value>
                        </imaer:SpecifiedHeatContent>
                    </imaer:heatContent>
                    <imaer:emissionHeight>2.5</imaer:emissionHeight>
                    <imaer:spread>2.5</imaer:spread>
                </imaer:EmissionSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Curve>
                        <gml:LineString srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.6.CURVE">
                            <gml:posList>83612.51428163823 434486.4925227259 83612.51428163823 434486.4925227259 83612.24 434486.63 83571.2 434476.17 83554.3 434468.12 83547.86 434455.24 83543.84 434430.3 83648.62 433855.79 83663.4 433801.62 83674.48 433762.22 83723.72 433704.35 83760.66 433674.8 83795.13 433660.03 83823.45 433640.33 83855.46 433610.78 83880.09 433575.08 83892.4 433541.84 83908.4 433481.51 84077.42 432521.7 83941.14 432515.75 83709.67 432511.97 83571.07 432505.03 83441.27 432462.84 83326.62 432417.96 83246.58 432392.0 83116.25 432340.62</gml:posList>
                        </gml:LineString>
                    </imaer:GM_Curve>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>1926.4147603777578</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:vehicles>
                <imaer:CustomVehicle>
                    <imaer:vehiclesPerTimeUnit>140000.0</imaer:vehiclesPerTimeUnit>
                    <imaer:timeUnit>YEAR</imaer:timeUnit>
                    <imaer:description>Tankwagens</imaer:description>
                    <imaer:emissionFactor>
                        <imaer:Emission substance="NO2">
                            <imaer:value>0.0</imaer:value>
                        </imaer:Emission>
                    </imaer:emissionFactor>
                    <imaer:emissionFactor>
                        <imaer:Emission substance="PM10">
                            <imaer:value>0.0</imaer:value>
                        </imaer:Emission>
                    </imaer:emissionFactor>
                    <imaer:emissionFactor>
                        <imaer:Emission substance="NOX">
                            <imaer:value>4.4</imaer:value>
                        </imaer:Emission>
                    </imaer:emissionFactor>
                    <imaer:emissionFactor>
                        <imaer:Emission substance="NH3">
                            <imaer:value>0.0</imaer:value>
                        </imaer:Emission>
                    </imaer:emissionFactor>
                </imaer:CustomVehicle>
            </imaer:vehicles>
            <imaer:trafficDirection>BOTH</imaer:trafficDirection>
            <imaer:elevation>NORMAL</imaer:elevation>
        </imaer:SRM2Road>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:SRM2Road roadAreaType="NL" roadType="NON_URBAN_ROAD_NATIONAL" sectorId="3100" gml:id="ES.7">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.7</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Personenauto's (buiten parkeerplaats)</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:EmissionSourceCharacteristics>
                    <imaer:heatContent>
                        <imaer:SpecifiedHeatContent>
                            <imaer:value>0.0</imaer:value>
                        </imaer:SpecifiedHeatContent>
                    </imaer:heatContent>
                    <imaer:emissionHeight>2.5</imaer:emissionHeight>
                    <imaer:spread>2.5</imaer:spread>
                </imaer:EmissionSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Curve>
                        <gml:LineString srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.7.CURVE">
                            <gml:posList>83758.70859338525 434290.2672008998 83650.06629493913 434274.1720455744 83612.24267992454 434486.62809586903 83571.2000338449 434476.16624490754 83554.30012075328 434468.1186672449 83547.86205862314 434455.2425429846 83543.83826979181 434430.2950522303 83648.62228683928 433855.78915764636 83663.39658027586 433801.6167483789 83674.4773003533 433762.218632548 83723.72494514192 433704.3526499214 83760.66067873339 433674.8040630482 83795.13403008542 433660.0297696116 83823.45142583888 433640.3307116962 83855.46239495148 433610.78212482296 83880.0862173458 433575.0775823512 83892.39812854295 433541.8354221189 83908.40361309923 433481.5070572529 84077.42358591879 432521.70297129045 83941.13953113573 432515.75406413723 83709.67296190101 432511.9683959488 83571.06757756445 432505.02772073867 83441.27323967582 432462.84456092486 83326.62157454086 432417.95735240506 83246.58173284287 432391.99848482735 83116.24658521304 432340.6215594131</gml:posList>
                        </gml:LineString>
                    </imaer:GM_Curve>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>98.29604647625145</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:vehicles>
                <imaer:CustomVehicle>
                    <imaer:vehiclesPerTimeUnit>94900.0</imaer:vehiclesPerTimeUnit>
                    <imaer:timeUnit>YEAR</imaer:timeUnit>
                    <imaer:description>Personenauto's</imaer:description>
                    <imaer:emissionFactor>
                        <imaer:Emission substance="NO2">
                            <imaer:value>0.0</imaer:value>
                        </imaer:Emission>
                    </imaer:emissionFactor>
                    <imaer:emissionFactor>
                        <imaer:Emission substance="PM10">
                            <imaer:value>0.0</imaer:value>
                        </imaer:Emission>
                    </imaer:emissionFactor>
                    <imaer:emissionFactor>
                        <imaer:Emission substance="NOX">
                            <imaer:value>0.3</imaer:value>
                        </imaer:Emission>
                    </imaer:emissionFactor>
                    <imaer:emissionFactor>
                        <imaer:Emission substance="NH3">
                            <imaer:value>0.0</imaer:value>
                        </imaer:Emission>
                    </imaer:emissionFactor>
                </imaer:CustomVehicle>
            </imaer:vehicles>
            <imaer:trafficDirection>BOTH</imaer:trafficDirection>
            <imaer:elevation>NORMAL</imaer:elevation>
        </imaer:SRM2Road>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="1300" gml:id="ES.8">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.8</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Boiler 1</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:EmissionSourceCharacteristics>
                    <imaer:heatContent>
                        <imaer:CalculatedHeatContent>
                            <imaer:emissionTemperature>100.0</imaer:emissionTemperature>
                            <imaer:outflowDiameter>0.5</imaer:outflowDiameter>
                            <imaer:outflowVelocity>0.2</imaer:outflowVelocity>
                            <imaer:outflowDirection>VERTICAL</imaer:outflowDirection>
                            <imaer:outflowVelocityType>ACTUAL_FLOW</imaer:outflowVelocityType>
                        </imaer:CalculatedHeatContent>
                    </imaer:heatContent>
                    <imaer:emissionHeight>12.0</imaer:emissionHeight>
                    <imaer:timeVaryingProfile>
                        <imaer:StandardTimeVaryingProfile>
                            <imaer:standardType>INDUSTRIAL_ACTIVITY</imaer:standardType>
                        </imaer:StandardTimeVaryingProfile>
                    </imaer:timeVaryingProfile>
                </imaer:EmissionSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.8.POINT">
                            <gml:pos>83783.92086300373 434476.90929434955</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>1411.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="1300" gml:id="ES.9">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.9</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Boiler 2</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:EmissionSourceCharacteristics>
                    <imaer:heatContent>
                        <imaer:CalculatedHeatContent>
                            <imaer:emissionTemperature>100.0</imaer:emissionTemperature>
                            <imaer:outflowDiameter>0.5</imaer:outflowDiameter>
                            <imaer:outflowVelocity>0.2</imaer:outflowVelocity>
                            <imaer:outflowDirection>VERTICAL</imaer:outflowDirection>
                            <imaer:outflowVelocityType>ACTUAL_FLOW</imaer:outflowVelocityType>
                        </imaer:CalculatedHeatContent>
                    </imaer:heatContent>
                    <imaer:emissionHeight>12.0</imaer:emissionHeight>
                    <imaer:timeVaryingProfile>
                        <imaer:StandardTimeVaryingProfile>
                            <imaer:standardType>INDUSTRIAL_ACTIVITY</imaer:standardType>
                        </imaer:StandardTimeVaryingProfile>
                    </imaer:timeVaryingProfile>
                </imaer:EmissionSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.9.POINT">
                            <gml:pos>83814.05410954414 434344.0998003381</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>1411.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="1300" gml:id="ES.10">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.10</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Boiler 3</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:EmissionSourceCharacteristics>
                    <imaer:heatContent>
                        <imaer:CalculatedHeatContent>
                            <imaer:emissionTemperature>100.0</imaer:emissionTemperature>
                            <imaer:outflowDiameter>0.5</imaer:outflowDiameter>
                            <imaer:outflowVelocity>0.2</imaer:outflowVelocity>
                            <imaer:outflowDirection>VERTICAL</imaer:outflowDirection>
                            <imaer:outflowVelocityType>ACTUAL_FLOW</imaer:outflowVelocityType>
                        </imaer:CalculatedHeatContent>
                    </imaer:heatContent>
                    <imaer:emissionHeight>12.0</imaer:emissionHeight>
                    <imaer:timeVaryingProfile>
                        <imaer:StandardTimeVaryingProfile>
                            <imaer:standardType>INDUSTRIAL_ACTIVITY</imaer:standardType>
                        </imaer:StandardTimeVaryingProfile>
                    </imaer:timeVaryingProfile>
                </imaer:EmissionSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.10.POINT">
                            <gml:pos>83786.15295534006 434400.64613952505</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>1411.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="1300" gml:id="ES.11">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.11</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Stoomketel</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:EmissionSourceCharacteristics>
                    <imaer:heatContent>
                        <imaer:CalculatedHeatContent>
                            <imaer:emissionTemperature>100.0</imaer:emissionTemperature>
                            <imaer:outflowDiameter>0.5</imaer:outflowDiameter>
                            <imaer:outflowVelocity>0.2</imaer:outflowVelocity>
                            <imaer:outflowDirection>VERTICAL</imaer:outflowDirection>
                            <imaer:outflowVelocityType>ACTUAL_FLOW</imaer:outflowVelocityType>
                        </imaer:CalculatedHeatContent>
                    </imaer:heatContent>
                    <imaer:emissionHeight>12.0</imaer:emissionHeight>
                    <imaer:timeVaryingProfile>
                        <imaer:StandardTimeVaryingProfile>
                            <imaer:standardType>INDUSTRIAL_ACTIVITY</imaer:standardType>
                        </imaer:StandardTimeVaryingProfile>
                    </imaer:timeVaryingProfile>
                </imaer:EmissionSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.11.POINT">
                            <gml:pos>84156.65051082829 434492.5262502024</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>1411.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:OffRoadMobileSourceEmissionSource sectorId="3220" gml:id="ES.12">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.12</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Mobiele werktuigen</imaer:label>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.12.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>84659.20176173779 434668.5218948812 84678.87507679478 434600.5595337752 84585.87395107084 434301.88284154644 83654.07421064441 434158.8041865865 83605.78516459544 434471.7887443113 84659.20176173779 434668.5218948812</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.285675</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>717.85</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:offRoadMobileSource>
                <imaer:StandardOffRoadMobileSource offRoadMobileSourceType="SIIIB5675DSN">
                    <imaer:description>Reachstacker</imaer:description>
                    <imaer:literFuelPerYear>2000</imaer:literFuelPerYear>
                    <imaer:operatingHoursPerYear>4500</imaer:operatingHoursPerYear>
                </imaer:StandardOffRoadMobileSource>
            </imaer:offRoadMobileSource>
            <imaer:offRoadMobileSource>
                <imaer:StandardOffRoadMobileSource offRoadMobileSourceType="SIIIA75560DSN">
                    <imaer:description>Mobiele kraan</imaer:description>
                    <imaer:literFuelPerYear>25090</imaer:literFuelPerYear>
                    <imaer:operatingHoursPerYear>1000</imaer:operatingHoursPerYear>
                </imaer:StandardOffRoadMobileSource>
            </imaer:offRoadMobileSource>
            <imaer:offRoadMobileSource>
                <imaer:StandardOffRoadMobileSource offRoadMobileSourceType="SIIIA5675DSN">
                    <imaer:description>Heftruck</imaer:description>
                    <imaer:literFuelPerYear>7000</imaer:literFuelPerYear>
                    <imaer:operatingHoursPerYear>2000</imaer:operatingHoursPerYear>
                </imaer:StandardOffRoadMobileSource>
            </imaer:offRoadMobileSource>
            <imaer:offRoadMobileSource>
                <imaer:StandardOffRoadMobileSource offRoadMobileSourceType="SII5675DSN">
                    <imaer:description>Terminal trekker</imaer:description>
                    <imaer:literFuelPerYear>4000</imaer:literFuelPerYear>
                    <imaer:operatingHoursPerYear>800</imaer:operatingHoursPerYear>
                </imaer:StandardOffRoadMobileSource>
            </imaer:offRoadMobileSource>
        </imaer:OffRoadMobileSourceEmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="1300" gml:id="ES.13">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.13</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Brandwaterpomp 1</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:EmissionSourceCharacteristics>
                    <imaer:heatContent>
                        <imaer:CalculatedHeatContent>
                            <imaer:emissionTemperature>100.0</imaer:emissionTemperature>
                            <imaer:outflowDiameter>0.4</imaer:outflowDiameter>
                            <imaer:outflowVelocity>0.0</imaer:outflowVelocity>
                            <imaer:outflowDirection>VERTICAL</imaer:outflowDirection>
                            <imaer:outflowVelocityType>ACTUAL_FLOW</imaer:outflowVelocityType>
                        </imaer:CalculatedHeatContent>
                    </imaer:heatContent>
                    <imaer:emissionHeight>3.0</imaer:emissionHeight>
                    <imaer:timeVaryingProfile>
                        <imaer:StandardTimeVaryingProfile>
                            <imaer:standardType>INDUSTRIAL_ACTIVITY</imaer:standardType>
                        </imaer:StandardTimeVaryingProfile>
                    </imaer:timeVaryingProfile>
                </imaer:EmissionSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.13.POINT">
                            <gml:pos>83925.99323477052 434370.04994529305</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>7.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="1300" gml:id="ES.14">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.14</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Brandwaterpomp 2</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:EmissionSourceCharacteristics>
                    <imaer:heatContent>
                        <imaer:CalculatedHeatContent>
                            <imaer:emissionTemperature>100.0</imaer:emissionTemperature>
                            <imaer:outflowDiameter>0.4</imaer:outflowDiameter>
                            <imaer:outflowVelocity>0.0</imaer:outflowVelocity>
                            <imaer:outflowDirection>VERTICAL</imaer:outflowDirection>
                            <imaer:outflowVelocityType>ACTUAL_FLOW</imaer:outflowVelocityType>
                        </imaer:CalculatedHeatContent>
                    </imaer:heatContent>
                    <imaer:emissionHeight>3.0</imaer:emissionHeight>
                    <imaer:timeVaryingProfile>
                        <imaer:StandardTimeVaryingProfile>
                            <imaer:standardType>INDUSTRIAL_ACTIVITY</imaer:standardType>
                        </imaer:StandardTimeVaryingProfile>
                    </imaer:timeVaryingProfile>
                </imaer:EmissionSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.14.POINT">
                            <gml:pos>83928.93323477052 434355.34994529304</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>7.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="1300" gml:id="ES.15">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.15</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Brandwaterpomp 3</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:EmissionSourceCharacteristics>
                    <imaer:heatContent>
                        <imaer:CalculatedHeatContent>
                            <imaer:emissionTemperature>100.0</imaer:emissionTemperature>
                            <imaer:outflowDiameter>0.4</imaer:outflowDiameter>
                            <imaer:outflowVelocity>0.0</imaer:outflowVelocity>
                            <imaer:outflowDirection>VERTICAL</imaer:outflowDirection>
                            <imaer:outflowVelocityType>ACTUAL_FLOW</imaer:outflowVelocityType>
                        </imaer:CalculatedHeatContent>
                    </imaer:heatContent>
                    <imaer:emissionHeight>3.0</imaer:emissionHeight>
                    <imaer:timeVaryingProfile>
                        <imaer:StandardTimeVaryingProfile>
                            <imaer:standardType>INDUSTRIAL_ACTIVITY</imaer:standardType>
                        </imaer:StandardTimeVaryingProfile>
                    </imaer:timeVaryingProfile>
                </imaer:EmissionSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.15.POINT">
                            <gml:pos>83930.19323477052 434340.43994529307</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>7.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="1300" gml:id="ES.16">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.16</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>DVI (20MW)</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:EmissionSourceCharacteristics>
                    <imaer:heatContent>
                        <imaer:CalculatedHeatContent>
                            <imaer:emissionTemperature>100.0</imaer:emissionTemperature>
                            <imaer:outflowDiameter>1.5</imaer:outflowDiameter>
                            <imaer:outflowVelocity>5.4</imaer:outflowVelocity>
                            <imaer:outflowDirection>VERTICAL</imaer:outflowDirection>
                            <imaer:outflowVelocityType>ACTUAL_FLOW</imaer:outflowVelocityType>
                        </imaer:CalculatedHeatContent>
                    </imaer:heatContent>
                    <imaer:emissionHeight>20.0</imaer:emissionHeight>
                    <imaer:timeVaryingProfile>
                        <imaer:StandardTimeVaryingProfile>
                            <imaer:standardType>INDUSTRIAL_ACTIVITY</imaer:standardType>
                        </imaer:StandardTimeVaryingProfile>
                    </imaer:timeVaryingProfile>
                </imaer:EmissionSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.16.POINT">
                            <gml:pos>84162.22592430786 434429.3462281225</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>1254.7</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="1300" gml:id="ES.17">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.17</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>DVI (2MW)</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:EmissionSourceCharacteristics>
                    <imaer:heatContent>
                        <imaer:CalculatedHeatContent>
                            <imaer:emissionTemperature>100.0</imaer:emissionTemperature>
                            <imaer:outflowDiameter>0.5</imaer:outflowDiameter>
                            <imaer:outflowVelocity>4.9</imaer:outflowVelocity>
                            <imaer:outflowDirection>VERTICAL</imaer:outflowDirection>
                            <imaer:outflowVelocityType>ACTUAL_FLOW</imaer:outflowVelocityType>
                        </imaer:CalculatedHeatContent>
                    </imaer:heatContent>
                    <imaer:emissionHeight>20.0</imaer:emissionHeight>
                    <imaer:timeVaryingProfile>
                        <imaer:StandardTimeVaryingProfile>
                            <imaer:standardType>INDUSTRIAL_ACTIVITY</imaer:standardType>
                        </imaer:StandardTimeVaryingProfile>
                    </imaer:timeVaryingProfile>
                </imaer:EmissionSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.17.POINT">
                            <gml:pos>84162.22592430786 434438.2984373088</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>627.4</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:InlandShippingEmissionSource sectorId="7620" gml:id="ES.InlandMooringRoute.0">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.InlandMooringRoute.0</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Binnenvaartschepen; Route 1</imaer:label>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Curve>
                        <gml:LineString srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.InlandMooringRoute.0.CURVE">
                            <gml:posList>84730.32822889074 434531.41306207963 84867.95118487923 435006.4950471358</gml:posList>
                        </gml:LineString>
                    </imaer:GM_Curve>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>522.2843565021432</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:inlandShipping>
                <imaer:StandardInlandShipping shipType="M10">
                    <imaer:description>Binnenvaart</imaer:description>
                    <imaer:numberOfShipsAtoBperTimeUnit>0</imaer:numberOfShipsAtoBperTimeUnit>
                    <imaer:numberOfShipsBtoAperTimeUnit>700</imaer:numberOfShipsBtoAperTimeUnit>
                    <imaer:percentageLadenAtoB>0</imaer:percentageLadenAtoB>
                    <imaer:percentageLadenBtoA>65</imaer:percentageLadenBtoA>
                    <imaer:timeUnitShipsAtoB>YEAR</imaer:timeUnitShipsAtoB>
                    <imaer:timeUnitShipsBtoA>YEAR</imaer:timeUnitShipsBtoA>
                </imaer:StandardInlandShipping>
            </imaer:inlandShipping>
            <imaer:inlandShipping>
                <imaer:StandardInlandShipping shipType="M10">
                    <imaer:description>Binnenvaart</imaer:description>
                    <imaer:numberOfShipsAtoBperTimeUnit>700</imaer:numberOfShipsAtoBperTimeUnit>
                    <imaer:numberOfShipsBtoAperTimeUnit>0</imaer:numberOfShipsBtoAperTimeUnit>
                    <imaer:percentageLadenAtoB>65</imaer:percentageLadenAtoB>
                    <imaer:percentageLadenBtoA>0</imaer:percentageLadenBtoA>
                    <imaer:timeUnitShipsAtoB>YEAR</imaer:timeUnitShipsAtoB>
                    <imaer:timeUnitShipsBtoA>YEAR</imaer:timeUnitShipsBtoA>
                </imaer:StandardInlandShipping>
            </imaer:inlandShipping>
            <imaer:waterway>
                <imaer:InlandWaterway>
                    <imaer:type>CEMT_Va</imaer:type>
                </imaer:InlandWaterway>
            </imaer:waterway>
            <imaer:mooringA xlink:href="#ES.2"/>
        </imaer:InlandShippingEmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:MaritimeShippingEmissionSource movementType="INLAND" sectorId="7520" gml:id="ES.MaritimeInlandMooringRoute.0">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.MaritimeInlandMooringRoute.0</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Zeeschepen; Route 1</imaer:label>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Curve>
                        <gml:LineString srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.MaritimeInlandMooringRoute.0.CURVE">
                            <gml:posList>84683.19707957961 434365.5114165045 84965.98397544638 434987.64258741133</gml:posList>
                        </gml:LineString>
                    </imaer:GM_Curve>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>8827.31908696645</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:maritimeShipping>
                <imaer:StandardMaritimeShipping shipType="S10000">
                    <imaer:description>Bulkschip GT 10.000 - 29.999</imaer:description>
                    <imaer:shipsPerTimeUnit>330</imaer:shipsPerTimeUnit>
                    <imaer:timeUnit>YEAR</imaer:timeUnit>
                </imaer:StandardMaritimeShipping>
            </imaer:maritimeShipping>
            <imaer:maritimeShipping>
                <imaer:StandardMaritimeShipping shipType="S30000">
                    <imaer:description>Bulkschip GT 30.000 - 59.999</imaer:description>
                    <imaer:shipsPerTimeUnit>1650</imaer:shipsPerTimeUnit>
                    <imaer:timeUnit>YEAR</imaer:timeUnit>
                </imaer:StandardMaritimeShipping>
            </imaer:maritimeShipping>
            <imaer:maritimeShipping>
                <imaer:StandardMaritimeShipping shipType="S60000">
                    <imaer:description>Bulkschip GT 60.000 - 99.999</imaer:description>
                    <imaer:shipsPerTimeUnit>220</imaer:shipsPerTimeUnit>
                    <imaer:timeUnit>YEAR</imaer:timeUnit>
                </imaer:StandardMaritimeShipping>
            </imaer:maritimeShipping>
            <imaer:mooringA xlink:href="#ES.1"/>
        </imaer:MaritimeShippingEmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.20001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.20001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.20001.POINT">
                    <gml:pos>265529.0 522873.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>20001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.20002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.20002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.20002.POINT">
                    <gml:pos>265689.0 521926.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>20002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.20003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.20003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.20003.POINT">
                    <gml:pos>262815.0 520979.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>20003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.20004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.20004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.20004.POINT">
                    <gml:pos>267969.0 522527.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>20004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.840001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.840001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.840001.POINT">
                    <gml:pos>169237.0 446517.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>840001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.840002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.840002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.840002.POINT">
                    <gml:pos>169009.0 446400.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>840002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.840003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.840003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.840003.POINT">
                    <gml:pos>169578.0 446528.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>840003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.50001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.50001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.50001.POINT">
                    <gml:pos>111889.0 422955.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>50001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.50002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.50002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.50002.POINT">
                    <gml:pos>114925.0 424136.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>50002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.50003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.50003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.50003.POINT">
                    <gml:pos>116311.0 424375.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>50003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.870001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.870001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.870001.POINT">
                    <gml:pos>219285.0 486667.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>870001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.870002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.870002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.870002.POINT">
                    <gml:pos>218250.0 487048.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>870002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.880001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.880001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.880001.POINT">
                    <gml:pos>198296.0 395478.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>880001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.880002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.880002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.880002.POINT">
                    <gml:pos>198220.0 395778.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>880002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.540001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.540001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.540001.POINT">
                    <gml:pos>85215.0 380093.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>540001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.540002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.540002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.540002.POINT">
                    <gml:pos>86523.0 378577.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>540002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.540003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.540003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.540003.POINT">
                    <gml:pos>84833.0 378505.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>540003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.80001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.80001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.80001.POINT">
                    <gml:pos>197027.0 326222.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>80001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.80002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.80002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.80002.POINT">
                    <gml:pos>198575.0 326295.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>80002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.80003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.80003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.80003.POINT">
                    <gml:pos>198527.0 326788.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>80003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.90001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.90001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.90001.POINT">
                    <gml:pos>250562.0 464076.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>90001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.90002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.90002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.90002.POINT">
                    <gml:pos>250316.0 463295.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>90002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.90003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.90003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.90003.POINT">
                    <gml:pos>251953.0 464012.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>90003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.90004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.90004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.90004.POINT">
                    <gml:pos>250553.0 461382.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>90004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.90005">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.90005</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.90005.POINT">
                    <gml:pos>250963.0 460026.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>90005</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.90006">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.90006</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.90006.POINT">
                    <gml:pos>250325.0 464476.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>90006</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.60001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.60001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.60001.POINT">
                    <gml:pos>232125.0 477861.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>60001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.60002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.60002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.60002.POINT">
                    <gml:pos>232199.0 477806.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>60002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.60003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.60003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.60003.POINT">
                    <gml:pos>232180.0 477468.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>60003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.60004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.60004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.60004.POINT">
                    <gml:pos>230585.0 476671.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>60004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.110001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.110001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.110001.POINT">
                    <gml:pos>269374.0 486136.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>110001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.110002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.110002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.110002.POINT">
                    <gml:pos>268125.0 488019.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>110002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.110003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.110003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.110003.POINT">
                    <gml:pos>268581.0 488230.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>110003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.110004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.110004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.110004.POINT">
                    <gml:pos>265758.0 473976.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>110004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.110005">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.110005</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.110005.POINT">
                    <gml:pos>266414.0 476179.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>110005</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.120001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.120001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.120001.POINT">
                    <gml:pos>237521.0 571428.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>120001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.120002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.120002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.120002.POINT">
                    <gml:pos>239587.0 560971.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>120002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.120003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.120003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.120003.POINT">
                    <gml:pos>241084.0 560456.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>120003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.120004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.120004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.120004.POINT">
                    <gml:pos>243309.0 558909.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>120004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.130001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.130001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.130001.POINT">
                    <gml:pos>213942.0 544403.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>130001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.130002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.130002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.130002.POINT">
                    <gml:pos>215715.0 545190.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>130002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.130003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.130003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.130003.POINT">
                    <gml:pos>218264.0 547136.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>130003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.130004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.130004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.130004.POINT">
                    <gml:pos>216420.0 549209.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>130004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.3190001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.3190001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.3190001.POINT">
                    <gml:pos>110435.0 559545.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>3190001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.3190002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.3190002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.3190002.POINT">
                    <gml:pos>109762.0 559308.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>3190002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.3190003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.3190003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.3190003.POINT">
                    <gml:pos>115583.0 566427.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>3190003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.3190004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.3190004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.3190004.POINT">
                    <gml:pos>122029.0 568599.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>3190004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.3190005">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.3190005</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.3190005.POINT">
                    <gml:pos>122190.0 566317.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>3190005</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.230001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.230001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.230001.POINT">
                    <gml:pos>224021.0 535797.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>230001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.230002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.230002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.230002.POINT">
                    <gml:pos>222710.0 534315.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>230002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.230003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.230003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.230003.POINT">
                    <gml:pos>222557.0 532832.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>230003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.230004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.230004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.230004.POINT">
                    <gml:pos>220740.0 535257.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>230004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.1360001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1360001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1360001.POINT">
                    <gml:pos>118540.0 510003.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>1360001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.1360002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1360002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1360002.POINT">
                    <gml:pos>119621.0 511063.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>1360002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.1360003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1360003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1360003.POINT">
                    <gml:pos>120300.0 512205.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>1360003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.250001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.250001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.250001.POINT">
                    <gml:pos>220663.0 558740.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>250001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.250002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.250002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.250002.POINT">
                    <gml:pos>219830.0 557199.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>250002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.250003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.250003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.250003.POINT">
                    <gml:pos>227485.0 558347.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>250003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.250004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.250004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.250004.POINT">
                    <gml:pos>222597.0 557881.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>250004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.280001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.280001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.280001.POINT">
                    <gml:pos>192538.0 311124.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>280001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.280002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.280002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.280002.POINT">
                    <gml:pos>192628.0 309020.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>280002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.280003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.280003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.280003.POINT">
                    <gml:pos>187993.0 318159.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>280003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.300001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.300001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.300001.POINT">
                    <gml:pos>55086.0 418000.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>300001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.300002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.300002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.300002.POINT">
                    <gml:pos>57268.0 415687.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>300002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.300003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.300003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.300003.POINT">
                    <gml:pos>57750.0 419167.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>300003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.320001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.320001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.320001.POINT">
                    <gml:pos>183695.0 373170.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>320001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.320002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.320002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.320002.POINT">
                    <gml:pos>185292.0 374595.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>320002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.320003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.320003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.320003.POINT">
                    <gml:pos>186166.0 373471.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>320003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.320004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.320004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.320004.POINT">
                    <gml:pos>184902.0 372610.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>320004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.320005">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.320005</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.320005.POINT">
                    <gml:pos>183882.0 371572.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>320005</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.320006">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.320006</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.320006.POINT">
                    <gml:pos>184883.0 373481.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>320006</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.320007">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.320007</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.320007.POINT">
                    <gml:pos>185309.0 373118.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>320007</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.320008">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.320008</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.320008.POINT">
                    <gml:pos>185936.0 373600.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>320008</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.320009">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.320009</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.320009.POINT">
                    <gml:pos>185941.0 372268.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>320009</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.980001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.980001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.980001.POINT">
                    <gml:pos>186407.0 582584.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>980001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.980002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.980002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.980002.POINT">
                    <gml:pos>187724.0 583013.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>980002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.980003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.980003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.980003.POINT">
                    <gml:pos>189369.0 581819.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>980003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.330001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.330001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.330001.POINT">
                    <gml:pos>81206.0 417901.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>4.307E-6</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0473799</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>330001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.350001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.350001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.350001.POINT">
                    <gml:pos>145850.0 395539.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>350001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.350002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.350002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.350002.POINT">
                    <gml:pos>145597.0 397300.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>350002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.350003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.350003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.350003.POINT">
                    <gml:pos>145775.0 398603.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>350003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.350004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.350004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.350004.POINT">
                    <gml:pos>147161.0 398233.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>350004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.370001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.370001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.370001.POINT">
                    <gml:pos>95218.0 479098.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>370001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.370002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.370002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.370002.POINT">
                    <gml:pos>92828.0 479788.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>370002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.370003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.370003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.370003.POINT">
                    <gml:pos>93311.0 478962.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>370003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.370004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.370004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.370004.POINT">
                    <gml:pos>93666.0 478675.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>370004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.370005">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.370005</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.370005.POINT">
                    <gml:pos>98722.0 493907.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>370005</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.370006">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.370006</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.370006.POINT">
                    <gml:pos>100724.0 493896.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>370006</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.370007">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.370007</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.370007.POINT">
                    <gml:pos>102142.0 492833.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>370007</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.380001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.380001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.380001.POINT">
                    <gml:pos>37197.0 412718.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>380001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.380002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.380002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.380002.POINT">
                    <gml:pos>38359.0 413376.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>380002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.380003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.380003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.380003.POINT">
                    <gml:pos>42056.0 415825.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>380003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.380004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.380004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.380004.POINT">
                    <gml:pos>41880.0 415596.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>380004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.390001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.390001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.390001.POINT">
                    <gml:pos>242700.0 444969.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>390001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.390002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.390002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.390002.POINT">
                    <gml:pos>242299.0 445255.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>390002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.390003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.390003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.390003.POINT">
                    <gml:pos>241403.0 444915.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>390003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.400001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.400001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.400001.POINT">
                    <gml:pos>77319.0 405825.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>400001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.190001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.190001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.190001.POINT">
                    <gml:pos>226908.0 577838.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>190001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.190002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.190002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.190002.POINT">
                    <gml:pos>229139.0 577993.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>190002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.190003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.190003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.190003.POINT">
                    <gml:pos>224308.0 577001.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>190003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.420001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.420001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.420001.POINT">
                    <gml:pos>140388.0 406423.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>420001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.420002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.420002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.420002.POINT">
                    <gml:pos>138359.0 406359.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>420002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.420003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.420003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.420003.POINT">
                    <gml:pos>135027.0 405802.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>420003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.420004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.420004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.420004.POINT">
                    <gml:pos>132258.0 407482.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>420004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.420005">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.420005</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.420005.POINT">
                    <gml:pos>131545.0 402132.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>420005</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.430001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.430001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.430001.POINT">
                    <gml:pos>208845.0 392445.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>430001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.430002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.430002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.430002.POINT">
                    <gml:pos>209804.0 393282.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>430002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.430003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.430003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.430003.POINT">
                    <gml:pos>204443.0 400971.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>430003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.430004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.430004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.430004.POINT">
                    <gml:pos>202307.0 402419.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>430004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.430005">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.430005</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.430005.POINT">
                    <gml:pos>198475.0 406946.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>430005</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.440001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.440001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.440001.POINT">
                    <gml:pos>30343.0 402010.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>440001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.440002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.440002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.440002.POINT">
                    <gml:pos>30283.0 401599.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>440002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.440003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.440003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.440003.POINT">
                    <gml:pos>30551.0 401093.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>440003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.440004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.440004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.440004.POINT">
                    <gml:pos>27382.0 400928.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>440004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.450001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.450001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.450001.POINT">
                    <gml:pos>187819.0 379945.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>450001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.450002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.450002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.450002.POINT">
                    <gml:pos>189524.0 381205.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>450002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.450003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.450003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.450003.POINT">
                    <gml:pos>191433.0 382742.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>450003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.450004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.450004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.450004.POINT">
                    <gml:pos>191797.0 381151.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>450004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.450005">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.450005</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.450005.POINT">
                    <gml:pos>192577.0 380469.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>450005</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.450006">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.450006</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.450006.POINT">
                    <gml:pos>192778.0 380583.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>450006</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.470001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.470001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.470001.POINT">
                    <gml:pos>208462.0 351450.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>470001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.470002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.470002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.470002.POINT">
                    <gml:pos>204386.0 353400.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>470002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.470003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.470003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.470003.POINT">
                    <gml:pos>206974.0 354984.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>470003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.460001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.460001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.460001.POINT">
                    <gml:pos>83742.0 463635.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>460001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.460002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.460002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.460002.POINT">
                    <gml:pos>84905.0 462971.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>460002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.460003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.460003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.460003.POINT">
                    <gml:pos>85466.0 461791.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>460003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.460004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.460004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.460004.POINT">
                    <gml:pos>82429.0 459930.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>460004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.480001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.480001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.480001.POINT">
                    <gml:pos>137600.0 476750.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>480001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.480002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.480002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.480002.POINT">
                    <gml:pos>137155.0 479567.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>480002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.9900001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.9900001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.9900001.POINT">
                    <gml:pos>239778.0 464579.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>9900001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.9900002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.9900002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.9900002.POINT">
                    <gml:pos>239973.0 464390.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>9900002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.490001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.490001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.490001.POINT">
                    <gml:pos>114976.0 461718.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>490001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.490002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.490002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.490002.POINT">
                    <gml:pos>116045.0 462330.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>490002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.490003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.490003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.490003.POINT">
                    <gml:pos>116978.0 462837.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>490003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.500001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.500001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.500001.POINT">
                    <gml:pos>105325.0 510825.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>500001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.500002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.500002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.500002.POINT">
                    <gml:pos>103398.0 510730.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>500002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.500003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.500003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.500003.POINT">
                    <gml:pos>102406.0 510052.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>500003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.1140001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1140001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1140001.POINT">
                    <gml:pos>135256.0 464899.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>1140001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.1140002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1140002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1140002.POINT">
                    <gml:pos>135978.0 475260.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>1140002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.1140003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1140003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1140003.POINT">
                    <gml:pos>133701.0 472548.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>1140003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.530001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.530001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.530001.POINT">
                    <gml:pos>42970.0 402181.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>530001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.530002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.530002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.530002.POINT">
                    <gml:pos>42368.0 402298.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>530002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.530003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.530003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.530003.POINT">
                    <gml:pos>53712.0 395791.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>530003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.530004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.530004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.530004.POINT">
                    <gml:pos>51914.0 395785.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>530004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.530005">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.530005</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.530005.POINT">
                    <gml:pos>69922.0 402174.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>530005</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.530006">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.530006</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.530006.POINT">
                    <gml:pos>70541.0 401721.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>530006</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.780001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.780001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.780001.POINT">
                    <gml:pos>155134.0 500498.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>780001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.780002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.780002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.780002.POINT">
                    <gml:pos>157011.0 496856.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>780002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.780003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.780003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.780003.POINT">
                    <gml:pos>154833.0 493144.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>780003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.780004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.780004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.780004.POINT">
                    <gml:pos>151074.0 494131.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>780004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.570001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.570001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.570001.POINT">
                    <gml:pos>190784.0 539393.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>570001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.570002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.570002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.570002.POINT">
                    <gml:pos>188888.0 544899.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>570002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.570003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.570003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.570003.POINT">
                    <gml:pos>187211.0 542649.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>570003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.580001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.580001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.580001.POINT">
                    <gml:pos>222159.0 484212.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>580001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.580002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.580002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.580002.POINT">
                    <gml:pos>223423.0 483981.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>580002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.580003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.580003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.580003.POINT">
                    <gml:pos>225192.0 483563.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>580003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.580004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.580004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.580004.POINT">
                    <gml:pos>224799.0 481945.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>580004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.580005">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.580005</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.580005.POINT">
                    <gml:pos>227202.0 484219.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>580005</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.580006">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.580006</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.580006.POINT">
                    <gml:pos>228349.0 483506.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>580006</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.1210001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1210001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1210001.POINT">
                    <gml:pos>182683.0 364235.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>1210001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.1210002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1210002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1210002.POINT">
                    <gml:pos>183924.0 364226.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>1210002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.160001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.160001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.160001.POINT">
                    <gml:pos>208053.0 610925.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>160001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.160002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.160002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.160002.POINT">
                    <gml:pos>208552.0 611123.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>160002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.160003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.160003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.160003.POINT">
                    <gml:pos>210562.0 610423.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>160003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.160004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.160004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.160004.POINT">
                    <gml:pos>210560.0 611941.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>160004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.160005">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.160005</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.160005.POINT">
                    <gml:pos>204748.0 610518.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>160005</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.160006">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.160006</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.160006.POINT">
                    <gml:pos>207449.0 612005.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>160006</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.620001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.620001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.620001.POINT">
                    <gml:pos>175422.0 314402.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>620001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.620002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.620002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.620002.POINT">
                    <gml:pos>175568.0 314150.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>620002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.620003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.620003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.620003.POINT">
                    <gml:pos>175832.0 315977.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>620003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.610001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.610001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.610001.POINT">
                    <gml:pos>256342.0 496407.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>610001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.610002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.610002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.610002.POINT">
                    <gml:pos>256270.0 495791.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>610002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.1220001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1220001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1220001.POINT">
                    <gml:pos>229628.0 459372.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>1220001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.1220002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1220002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1220002.POINT">
                    <gml:pos>229608.0 459470.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>1220002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.1220003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1220003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1220003.POINT">
                    <gml:pos>229406.0 458969.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>1220003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.630001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.630001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.630001.POINT">
                    <gml:pos>172502.0 377895.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>630001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.630002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.630002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.630002.POINT">
                    <gml:pos>172353.0 379431.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>630002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.630003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.630003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.630003.POINT">
                    <gml:pos>173171.0 379262.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>630003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.630004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.630004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.630004.POINT">
                    <gml:pos>174071.0 379079.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>630004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.630005">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.630005</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.630005.POINT">
                    <gml:pos>173751.0 378600.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>630005</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.630006">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.630006</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.630006.POINT">
                    <gml:pos>168813.0 379162.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>630006</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.630007">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.630007</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.630007.POINT">
                    <gml:pos>170014.0 379303.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>630007</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.630008">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.630008</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.630008.POINT">
                    <gml:pos>171566.0 379819.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>630008</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.180001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.180001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.180001.POINT">
                    <gml:pos>157795.0 604610.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>180001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.180002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.180002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.180002.POINT">
                    <gml:pos>157475.0 602569.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>180002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.180003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.180003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.180003.POINT">
                    <gml:pos>157655.0 602051.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>180003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.180004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.180004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.180004.POINT">
                    <gml:pos>159376.0 605307.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>180004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.180005">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.180005</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.180005.POINT">
                    <gml:pos>160095.0 604819.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>180005</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.180006">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.180006</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.180006.POINT">
                    <gml:pos>160639.0 603937.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>180006</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.180007">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.180007</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.180007.POINT">
                    <gml:pos>156306.0 603077.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>180007</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.150001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.150001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.150001.POINT">
                    <gml:pos>199857.0 560251.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>150001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.150002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.150002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.150002.POINT">
                    <gml:pos>200010.0 561062.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>150002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.150003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.150003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.150003.POINT">
                    <gml:pos>196668.0 562584.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>150003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.640001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.640001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.640001.POINT">
                    <gml:pos>227999.0 501427.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>640001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.640002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.640002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.640002.POINT">
                    <gml:pos>231014.0 502752.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>640002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.640003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.640003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.640003.POINT">
                    <gml:pos>228963.0 505288.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>640003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.640004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.640004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.640004.POINT">
                    <gml:pos>233239.0 501974.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>640004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.640005">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.640005</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.640005.POINT">
                    <gml:pos>224581.0 498984.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>640005</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.640006">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.640006</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.640006.POINT">
                    <gml:pos>223770.0 496782.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>640006</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.650004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.650004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.650004.POINT">
                    <gml:pos>193673.0 449921.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>650004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.650012">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.650012</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.650012.POINT">
                    <gml:pos>192114.0 449408.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>650012</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.650013">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.650013</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.650013.POINT">
                    <gml:pos>192117.0 449205.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>650013</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.650014">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.650014</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.650014.POINT">
                    <gml:pos>192778.0 449309.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>650014</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.650001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.650001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.650001.POINT">
                    <gml:pos>190202.0 463934.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>650001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.650002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.650002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.650002.POINT">
                    <gml:pos>187065.0 463857.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>650002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.650003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.650003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.650003.POINT">
                    <gml:pos>199091.0 492981.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>650003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.650005">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.650005</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.650005.POINT">
                    <gml:pos>172466.0 471627.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>650005</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.650010">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.650010</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.650010.POINT">
                    <gml:pos>179505.0 467061.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>650010</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.650011">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.650011</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.650011.POINT">
                    <gml:pos>181752.0 464441.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>650011</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.650007">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.650007</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.650007.POINT">
                    <gml:pos>177022.0 466199.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>650007</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.650008">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.650008</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.650008.POINT">
                    <gml:pos>177775.0 466566.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>650008</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.650009">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.650009</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.650009.POINT">
                    <gml:pos>178418.0 466505.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>650009</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.200001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.200001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.200001.POINT">
                    <gml:pos>130683.0 589499.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>200001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.200002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.200002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.200002.POINT">
                    <gml:pos>128626.0 588566.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>200002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.200003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.200003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.200003.POINT">
                    <gml:pos>129089.0 588310.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>200003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.200004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.200004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.200004.POINT">
                    <gml:pos>129601.0 587717.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>200004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.200005">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.200005</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.200005.POINT">
                    <gml:pos>130124.0 589503.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>200005</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.200006">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.200006</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.200006.POINT">
                    <gml:pos>131235.0 588179.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>200006</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.680001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.680001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.680001.POINT">
                    <gml:pos>65954.0 427534.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>3.952E-6</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0653563</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>680001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.680002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.680002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.680002.POINT">
                    <gml:pos>63815.0 431454.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>5.535E-6</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0879518</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>680002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.680003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.680003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.680003.POINT">
                    <gml:pos>62280.0 434766.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>3.069E-6</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0492175</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>680003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.680004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.680004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.680004.POINT">
                    <gml:pos>63417.0 437301.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>2.861E-6</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0439257</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>680004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.730001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.730001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.730001.POINT">
                    <gml:pos>69308.0 373836.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>730001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.730002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.730002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.730002.POINT">
                    <gml:pos>72716.0 376513.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>730002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.730003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.730003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.730003.POINT">
                    <gml:pos>53200.0 385583.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>730003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.1300001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1300001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1300001.POINT">
                    <gml:pos>256893.0 463814.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>1300001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.1300002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1300002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1300002.POINT">
                    <gml:pos>256399.0 463487.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>1300002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.1310001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1310001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1310001.POINT">
                    <gml:pos>248412.0 436646.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>1310001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.1310002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1310002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1310002.POINT">
                    <gml:pos>248661.0 436338.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>1310002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.1310003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1310003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1310003.POINT">
                    <gml:pos>248666.0 436139.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>1310003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.1320001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1320001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1320001.POINT">
                    <gml:pos>115544.0 503168.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>1320001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.1320002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1320002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1320002.POINT">
                    <gml:pos>117097.0 503548.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>1320002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.1320003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1320003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1320003.POINT">
                    <gml:pos>118918.0 503577.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>1320003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.1320004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1320004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1320004.POINT">
                    <gml:pos>116646.0 498945.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>1320004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.8000001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8000001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.8000001.POINT">
                    <gml:pos>60604.0 390239.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>8000001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.8000002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8000002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.8000002.POINT">
                    <gml:pos>59378.0 390152.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>8000002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.210001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.210001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.210001.POINT">
                    <gml:pos>107740.0 537375.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>210001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.210002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.210002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.210002.POINT">
                    <gml:pos>108438.0 537085.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>210002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.210003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.210003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.210003.POINT">
                    <gml:pos>108935.0 536621.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>210003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.210004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.210004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.210004.POINT">
                    <gml:pos>107602.0 535542.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>210004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.210005">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.210005</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.210005.POINT">
                    <gml:pos>104711.0 527362.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>210005</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.1310000">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1310000</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1310000.POINT">
                    <gml:pos>187319.0 394720.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>1310000</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.2350000">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.2350000</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.2350000.POINT">
                    <gml:pos>83562.0 383318.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>2350000</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.4440000">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.4440000</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.4440000.POINT">
                    <gml:pos>95216.0 479092.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>4440000</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.5380000">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.5380000</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.5380000.POINT">
                    <gml:pos>132294.0 535208.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>5380000</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.6330000">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.6330000</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.6330000.POINT">
                    <gml:pos>117414.0 461226.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>6330000</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.7220000">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7220000</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.7220000.POINT">
                    <gml:pos>238480.0 456650.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>7220000</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.7380000">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7380000</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.7380000.POINT">
                    <gml:pos>177004.0 458203.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>7380000</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.9290000">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.9290000</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.9290000.POINT">
                    <gml:pos>259029.0 544289.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>9290000</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.9910001">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.9910001</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.9910001.POINT">
                    <gml:pos>170803.0 443612.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>9910001</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.9910002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.9910002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.9910002.POINT">
                    <gml:pos>170941.0 443675.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>9910002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.9910003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.9910003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.9910003.POINT">
                    <gml:pos>171113.0 443750.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>9910003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.9910000">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.9910000</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.9910000.POINT">
                    <gml:pos>171063.0 443675.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>9910000</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.9910010">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.9910010</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.9910010.POINT">
                    <gml:pos>171063.0 443675.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>9910010</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.9910011">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.9910011</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.9910011.POINT">
                    <gml:pos>171063.0 443675.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>9910011</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.9910012">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.9910012</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.9910012.POINT">
                    <gml:pos>171063.0 443675.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>9910012</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.9920002">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.9920002</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.9920002.POINT">
                    <gml:pos>73332.0 451860.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>4.507E-6</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0513983</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>9920002</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.9920003">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.9920003</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.9920003.POINT">
                    <gml:pos>73445.0 451686.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>4.226E-6</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0485273</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>9920003</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.9920004">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.9920004</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.9920004.POINT">
                    <gml:pos>73466.0 451391.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>4.972E-6</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0608696</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>9920004</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.9920005">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.9920005</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.9920005.POINT">
                    <gml:pos>73500.0 451632.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>4.233E-6</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0486292</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>9920005</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.9920006">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.9920006</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.9920006.POINT">
                    <gml:pos>73500.0 451632.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>4.233E-6</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0486292</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>9920006</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.9920007">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.9920007</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.9920007.POINT">
                    <gml:pos>73500.0 451632.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>4.233E-6</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0486292</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>9920007</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.20000">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.20000</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.20000.POINT">
                    <gml:pos>265758.0 521910.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>20000</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.20010">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.20010</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.20010.POINT">
                    <gml:pos>265758.0 521910.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>20010</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.20020">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.20020</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.20020.POINT">
                    <gml:pos>265758.0 521910.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>20020</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.1140000">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1140000</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1140000.POINT">
                    <gml:pos>133436.0 472463.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>1140000</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.1140010">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1140010</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1140010.POINT">
                    <gml:pos>133436.0 472463.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>1140010</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.1310011">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1310011</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1310011.POINT">
                    <gml:pos>187319.0 394720.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>1310011</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.1340011">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1340011</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1340011.POINT">
                    <gml:pos>181358.0 324032.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>1340011</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.2300011">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.2300011</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.2300011.POINT">
                    <gml:pos>138429.0 392187.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>2300011</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.2310011">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.2310011</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.2310011.POINT">
                    <gml:pos>123187.0 397589.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>2310011</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.3180011">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.3180011</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.3180011.POINT">
                    <gml:pos>40775.0 368528.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>3180011</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.4340011">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.4340011</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.4340011.POINT">
                    <gml:pos>90061.0 441634.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>2.641E-5</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.271014</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>4340011</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.4440011">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.4440011</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.4440011.POINT">
                    <gml:pos>95216.0 479092.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>4440011</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.5380011">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.5380011</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.5380011.POINT">
                    <gml:pos>132294.0 535208.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>5380011</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.6280011">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.6280011</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.6280011.POINT">
                    <gml:pos>140537.0 456786.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>6280011</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.7320011">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7320011</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.7320011.POINT">
                    <gml:pos>177759.0 476022.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>7320011</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.9290011">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.9290011</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.9290011.POINT">
                    <gml:pos>259029.0 544289.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>9290011</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.9340011">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.9340011</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.9340011.POINT">
                    <gml:pos>214268.0 594143.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>9340011</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.2350011">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.2350011</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.2350011.POINT">
                    <gml:pos>83562.0 383318.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>2350011</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.4330011">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.4330011</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.4330011.POINT">
                    <gml:pos>82002.0 436307.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NH3">
                    <imaer:value>9.551E-5</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:result>
                <imaer:CalculationResult resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.42746583</imaer:value>
                </imaer:CalculationResult>
            </imaer:result>
            <imaer:label>4330011</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.6270011">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.6270011</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.6270011.POINT">
                    <gml:pos>141900.0 459100.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>6270011</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:CalculationPoint gml:id="CP.1000000">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1000000</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1000000.POINT">
                    <gml:pos>175683.0 473707.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>1000000</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMember>
</imaer:FeatureCollectionCalculator>
