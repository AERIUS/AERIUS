#!/bin/bash

# This script processes vue gwt annotations for the given file

# Load the classpath
CLASSPATH=$(cat classpath.txt)

# Add source, resource, and generated code directories to the classpath
CLASSPATH="src/main/java:src/main/resources:target/generated-sources/annotations:$CLASSPATH"

JAVA_FILE=$1

log_message() {
  local message="$1"
  echo "$(date +"%Y-%m-%d %H:%M:%S") - $message"
}

log_message "Processing:  $JAVA_FILE"

# Run javac with annotation processing
if javac -classpath "$CLASSPATH" -processorpath "$CLASSPATH" -processor com.axellience.vuegwt.processors.VueGwtProcessor $JAVA_FILE -d target/generated-sources/annotations > /dev/null
then
  log_message "Completed:   $JAVA_FILE"
else
  log_message "Error:       $JAVA_FILE"
fi
