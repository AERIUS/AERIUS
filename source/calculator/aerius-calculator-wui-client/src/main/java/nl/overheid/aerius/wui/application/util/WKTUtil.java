/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.util;

import java.util.List;
import java.util.stream.Collectors;

import com.google.gwt.regexp.shared.RegExp;

import ol.OLFactory;
import ol.format.Wkt;
import ol.format.WktReadOptions;
import ol.geom.Geometry;
import ol.geom.GeometryType;
import ol.geom.LineString;
import ol.proj.Projection;

import nl.aerius.geo.wui.util.OL3GeometryUtil;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.wui.application.i18n.M;

public class WKTUtil {
  // This pattern matches either a decimal number or a whole number
  private static final RegExp NUMBER_REGEXP = RegExp.compile("(-?\\d+\\.\\d+|-?\\d+)", "g");

  public static final String SELECTED_PROJECTION = "defaultProjectionConversion";
  private static final String EPSG_PRE_TEXT = "EPSG:";
  private static final Wkt WKT = new Wkt();

  public static class GeometryResult {
    private Geometry geometry = null;
    private String errorMessage = null;
    private String wktInput = null;
    private String wktOutput = null;
    private String wktInputBare = null;
    private String epsgInput = null;
    private String epsgOutput = null;

    public Geometry getGeometry() {
      return geometry;
    }

    public void setGeometry(final Geometry geometry) {
      this.geometry = geometry;
    }

    public String getErrorMessage() {
      return errorMessage;
    }

    public void setErrorMessage(final String errorMessage) {
      this.errorMessage = errorMessage;
    }

    public String getWktInput() {
      return wktInput;
    }

    public void setWktInput(final String wktInput) {
      this.wktInput = wktInput;
    }

    public void setWktInputBare(final String wktInputBare) {
      this.wktInputBare = wktInputBare;
    }

    public String getWktInputBare() {
      return wktInputBare;
    }

    public void setWktOutput(final String wktOutput) {
      this.wktOutput = wktOutput;
    }

    public String getWktOutput() {
      return wktOutput;
    }

    public void setEpsgInput(final String epsgInput) {
      this.epsgInput = epsgInput;
    }

    public String getEpsgInput() {
      return epsgInput;
    }

    public void setEpsgOutput(final String epsgOutput) {
      this.epsgOutput = epsgOutput;
    }

    public String getEpsgOutput() {
      return epsgOutput;
    }
  }

  private static String targetEpsgCode;

  /**
   * Cleans the input WKT string to ensure coordinates adhere to specified formatting rules:
   * - A maximum of @decimals decimal places for each coordinate.
   * - Unnecessary trailing zeros are removed.
   */
  public static String cleanWktInput(final String wktInput, final int decimals) {
    return wktInput;
    // TODO AER-2361: enable actual clean up when powers that be decide it is time.
//    MatchResult matcher;
//    final StringBuilder sb = new StringBuilder(wktInput);
//    final ArrayList<String> matches = new ArrayList<>();
//    while ((matcher = NUMBER_REGEXP.exec(wktInput)) != null) {
//      if (matcher.getGroup(1) != null) {
//        matches.add(matcher.getGroup(0));
//      }
//    }
//
//    for (final String match : matches) {
//      final String formatted = formatNumber(match, decimals);
//      sb.replace(sb.indexOf(match), sb.indexOf(match) + match.length(), formatted);
//    }
//
//    return sb.toString();
  }

  /**
   * Formats a number string to have up to @decimals decimal places without trailing zeroes.
   * This simple approach rounds to @decimals decimal places manually without BigDecimal.
   */
  private static String formatNumber(final String numberStr, final int decimals) {
    if (!numberStr.contains(".")) {
      return numberStr;
    }

    final double number = Double.parseDouble(numberStr);
    final double factor = Math.pow(10, decimals);
    final double roundedNumber = Math.round(number * factor) / factor;
    String formattedNumber = String.valueOf(roundedNumber);

    // Remove trailing zeroes and possible redundant decimal point.
    formattedNumber = formattedNumber.replaceAll("0+$", "").replaceAll("\\.$", "");

    return formattedNumber;
  }

  /**
   * Reads the wkt geometry, up to 3 decimals will be interpreted, while 2 decimals will be formatted.
   */
  public static GeometryResult tryReadGeometry(
      final String wktInput, final List<String> supportedConversionCodes, final String defaultConversion,
      final Theme theme, final GeometryType typeSuggestion) {
    final GeometryResult result = new GeometryResult();
    result.setWktInput(wktInput);
    result.setEpsgOutput(targetEpsgCode);

    String wktPart;
    String sridPart;

    final String cleanWktInput = cleanWktInput(wktInput, 3);
    final String cleanAndCompliantWktPart = cleanWktInput(cleanWktInput, 2);

    final String[] parts = cleanWktInput.split(";", 2);
    if (parts.length == 1) {
      if (defaultConversion != null && !defaultConversion.isEmpty()) {
        sridPart = defaultConversion.replace(EPSG_PRE_TEXT, "");
      } else {
        sridPart = null;
      }
      wktPart = parts[0];
    } else {
      sridPart = parts[0];
      wktPart = parts[1];
    }

    result.setWktInputBare(wktPart);
    if (wktPart.isEmpty()) {
      return result;
    }
    try {
      final Geometry geometry;
      if (sridPart == null) {

        geometry = WKT.readGeometry(cleanWktInput);
        result.setEpsgInput(targetEpsgCode);
      } else {
        final String[] sridParts = sridPart.split("=", 2);

        final String epsgCodePart = sridParts.length == 1 ? sridParts[0] : sridParts[1];
        final String epsgCode = "EPSG:" + epsgCodePart;

        result.setEpsgInput(epsgCode);
        result.setEpsgOutput(targetEpsgCode);

        if (supportedConversionCodes.contains(epsgCode)) {
          final WktReadOptions opts = OLFactory.createOptions();
          opts.setDataProjection(Projection.get(epsgCode));
          opts.setFeatureProjection(Projection.get(targetEpsgCode));
          geometry = WKT.readGeometry(wktPart, opts);
        } else {
          geometry = null;
          result.setErrorMessage(M.messages().esLocationConversionNotSupported(supportedConversionCodes.stream()
              .map(v -> v.replace("EPSG:", ""))
              .collect(Collectors.joining(", "))));
        }
      }

      if (geometry instanceof LineString && ((LineString) geometry).getCoordinates().length <= 1) {
        // Add extra check for line strings with 0 or only 1 coordinate. OpenLayers doesn't give an error, but it's not a valid geometry.
        throw new IllegalArgumentException("Not enough coordinates in linestring geometry");
      }
      if (sridPart == null && !convertToPlainText(cleanAndCompliantWktPart).equals(convertToPlainText(OL3GeometryUtil.toWktString(geometry)))) {
        // if wktString input does not equal wktString output show format error
        throw new IllegalArgumentException("Converted geometry does not equal input");
      }

      result.setGeometry(geometry);
      result.setWktOutput(geometry == null ? null : OL3GeometryUtil.toWktString(geometry));
    } catch (final Exception e) {
      result.setErrorMessage(M.messages().geometryInvalid(theme, typeSuggestion == null ? null : typeSuggestion));
    }

    return result;
  }

  public static void initGridProjection(final String epsgCode) {
    targetEpsgCode = epsgCode;
  }

  private static String convertToPlainText(final String geometry) {
    return geometry.replace(" ", "");
  }
}
