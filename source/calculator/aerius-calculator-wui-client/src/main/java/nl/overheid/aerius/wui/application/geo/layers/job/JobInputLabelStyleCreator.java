/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.job;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.DoubleSupplier;
import java.util.stream.Collectors;

import ol.Feature;
import ol.style.Style;

import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.domain.ImaerFeature;
import nl.overheid.aerius.wui.application.domain.ItemWithStringIdUtil;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.geo.icons.labels.Label;
import nl.overheid.aerius.wui.application.geo.layers.FeatureType;
import nl.overheid.aerius.wui.application.geo.layers.LabelFeature;
import nl.overheid.aerius.wui.application.geo.layers.LabelStyle;
import nl.overheid.aerius.wui.application.geo.layers.LayerStyleUtil;
import nl.overheid.aerius.wui.application.geo.layers.base.ClusteredLabelStyleCreator;
import nl.overheid.aerius.wui.application.i18n.M;

/**
 * Creates the style for situation grouped {@link EmissionSourceFeature} and {@link BuildingFeature} markers.
 */
class JobInputLabelStyleCreator extends ClusteredLabelStyleCreator<LabelFeature> {

  private static final String SITUATION_FEATURE_KEY_SEPARATOR = "_";

  public JobInputLabelStyleCreator(final DoubleSupplier iconScale) {
    super(iconScale, false, null);
  }

  @Override
  protected Style[] createClusteredStyles(final ImaerFeature feature, final LabelFeature[] features, final double resolution, final LabelStyle labelStyle) {
    final List<Style> styles = new ArrayList<>();

    collectStyles(resolution, features, styles, FeatureType.SOURCE, LabelStyle.EMISSION_SOURCE);
    collectStyles(resolution, features, styles, FeatureType.BUILDING, LabelStyle.BUILDING);
    return styles.toArray(new Style[styles.size()]);
  }

  private void collectStyles(final double resolution, final LabelFeature[] features, final List<Style> styles, final FeatureType featureType,
      final LabelStyle labelStyle) {
    final LabelFeature[] filteredFeatures = Arrays.stream(features)
        .filter(x -> x.getFeatureType() == featureType)
        .toArray(LabelFeature[]::new);

    if (filteredFeatures.length > 0) {
      styles.add(super.createClusteredStyles(null, filteredFeatures, resolution, labelStyle)[0]);
    }
  }

  @Override
  protected Style[] createStyle(final ImaerFeature feature, final List<Label> labels, final LabelStyle labelStyle) {
    return new Style[] {LayerStyleUtil.createStyle(labels, labelStyle.getLabelIconStyle(), false, getIconScale())};
  }

  @Override
  protected Label createClusteredLabel(final LabelFeature[] features, final LabelStyle labelStyle, final Character[] indicators) {
    final List<Character> actualIndicators = Arrays.stream(features)
        .map(JobInputLabelStyleCreator::determineIndicator)
        .distinct()
        .sorted()
        .collect(Collectors.toList());
    return super.createClusteredLabel(features, labelStyle, actualIndicators.toArray(new Character[actualIndicators.size()]));
  }

  @Override
  protected String clusterLabel(final int count, final LabelStyle labelStyle) {
    return labelStyle == LabelStyle.BUILDING ? M.messages().buildingMarkerClusterLabel(count) :  M.messages().esMarkerClusterLabel(count);
  }

  @Override
  protected Label createUnclusteredLabel(final LabelFeature f, final LabelStyle labelStyle) {
    final char indicator = determineIndicator(f);

    return new Label(createLabelName(f), labelStyle.getLabelBackgroundColor(f), labelStyle.getLabelTextColor(), new Character[] {indicator});
  }

  private static char determineIndicator(final LabelFeature feature) {
    final SituationType situationType = feature.getSituationType();

    return situationType == null ? 'U' : M.messages().layerJobSituationTypeIndicator(situationType).charAt(0);
  }

  @Override
  protected String createLabelName(final ImaerFeature f) {
    final String label = isShowNameLabels() ? f.getLabel() : f.getId().split(SITUATION_FEATURE_KEY_SEPARATOR)[1];

    return ItemWithStringIdUtil.isTemporaryItem(f) ? ItemWithStringIdUtil.getStringId(f) : label;
  }

  @Override
  public Feature createLabelFeature(final Feature feature) {
    // Because the features are already converted to LabelFeature objects this mapping function just returns the feature.
    return feature;
  }
}
