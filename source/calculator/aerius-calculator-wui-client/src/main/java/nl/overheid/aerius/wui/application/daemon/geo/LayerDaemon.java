/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.geo;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Singleton;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.layer.Base;

import nl.aerius.geo.command.LayerHiddenCommand;
import nl.aerius.geo.command.LayerOpacityCommand;
import nl.aerius.geo.domain.IsLayer;
import nl.aerius.geo.event.LayerAddedEvent;
import nl.aerius.geo.event.LayerRemovedEvent;
import nl.aerius.geo.event.LayerVisibleEvent;
import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.event.BasicEventComponent;

@Singleton
public class LayerDaemon extends BasicEventComponent implements Daemon {
  interface LayerDaemonEventBinder extends EventBinder<LayerDaemon> {
  }

  private final LayerDaemonEventBinder EVENT_BINDER = GWT.create(LayerDaemonEventBinder.class);

  private Set<IsLayer<?>> layers = new HashSet<>();

  @EventHandler
  public void onLayerAddedEvent(LayerAddedEvent e) {
    layers.add(e.getValue());
  }

  @EventHandler
  public void onLayerRemovedEvent(LayerRemovedEvent e) {
    layers.remove(e.getValue());
  }

  @EventHandler
  public void onLayerVisibleEvent(LayerVisibleEvent e) {
    final IsLayer<?> layer = e.getValue();
    layer.getInfoOptional()
        .map(v -> v.getBundle()).ifPresent(bundle -> {
          // Find all layers in the same bundle, that are visible, and that are not the event layer
          final List<IsLayer<?>> visibleLayers = layers.stream()
              .filter(v -> v.getInfoOptional().map(inf -> bundle.equals(inf.getBundle())).orElse(false))
              .filter(v -> ((Base) v.asLayer()).getVisible())
              .filter(v -> v != layer)
              .collect(Collectors.toList());

          if (!visibleLayers.isEmpty()) {
            final IsLayer<?> firstVisibleLayer = visibleLayers.get(0);

            // Copy the opacity of the first visible layer
            final double opacity = ((Base) firstVisibleLayer.asLayer()).getOpacity();
            eventBus.fireEvent(new LayerOpacityCommand(layer, opacity));

            // Hide the rest
            visibleLayers.forEach(v -> eventBus.fireEvent(new LayerHiddenCommand(v)));
          }
        });
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
