/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.service;

import javax.inject.Inject;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Singleton;

import nl.aerius.wui.util.RequestUtil;
import nl.overheid.aerius.shared.RequestMappings;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationInfo;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;

@Singleton
public class ExportServiceAsyncImpl implements ExportServiceAsync {
  @Inject EnvironmentConfiguration cfg;
  @Inject HeaderHelper hdr;

  @Override
  public void retrieveExportStatus(final String exportCode, final AsyncCallback<CalculationInfo> callback) {
    final String requestUrl = RequestUtil.prepareUrl(cfg.getConnectBaseUrl(), RequestMappings.CONNECT_UI_CALCULATE_INFO,
        RequestMappings.JOB_KEY, exportCode);
    RequestUtil.doGetWithHeaders(requestUrl, hdr.defaultHeaders(), AeriusRequestCallback.createAsync(callback));
  }

}
