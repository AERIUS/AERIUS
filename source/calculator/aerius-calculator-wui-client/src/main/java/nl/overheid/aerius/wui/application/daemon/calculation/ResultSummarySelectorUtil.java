/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.calculation;

import java.util.Arrays;
import java.util.List;

import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.context.result.ResultSummaryContext;

/**
 * Util class to construct set the SituationResultsKey data depending on the input.
 */
final class ResultSummarySelectorUtil {

  // The order of this list is important. It determines the order of what situation type will be shown when present in a calculation.
  private static final List<SituationType> SITUATION_TYPES_ORDERED = Arrays.asList(SituationType.PROPOSED, SituationType.TEMPORARY,
      SituationType.REFERENCE, SituationType.OFF_SITE_REDUCTION, SituationType.COMBINATION_PROPOSED, SituationType.COMBINATION_REFERENCE);

  private ResultSummarySelectorUtil() {
    // Util class
  }

  /**
   * Sets the SituationResultsKey data and force the ResultType to SituationResult.
   *
   * @param executionContext executionContext
   */
  public static void setContextWithSituationResult(final CalculationExecutionContext executionContext) {
    setContext(executionContext, true);
  }

  /**
   * Sets the SituationResultsKey data based on the calculation in the context..
   *
   * @param executionContext executionContext
   */
  public static void setContext(final CalculationExecutionContext executionContext) {
    setContext(executionContext, false);
  }

  private static void setContext(final CalculationExecutionContext executionContext, final boolean forceSituationResultType) {
    final CalculationJobType jobType = executionContext.getJobContext().getCalculationOptions().getCalculationJobType();
    for (final SituationType situationType : SITUATION_TYPES_ORDERED) {
      if (setIfNotNull(executionContext.getResultContext(), executionContext.getJobContext().getSituationComposition(), jobType, situationType,
          forceSituationResultType)) {
        return;
      }
    }
  }

  private static boolean setIfNotNull(final ResultSummaryContext context, final SituationComposition situationComposition,
      final CalculationJobType jobType, final SituationType situationType, final boolean forceSituationResultType) {
    final SituationContext situationContext = situationComposition.getSituation(situationType);

    if (situationContext != null) {
      context.setSituationHandle(SituationContext.handleFromSituation(situationContext));
      setResultType(context, jobType, situationType, forceSituationResultType);
      setHexagonType(context);
      return true;
    }
    return false;
  }

  private static void setResultType(final ResultSummaryContext context, final CalculationJobType jobType, final SituationType situationType,
      final boolean forceSituationResultType) {
    if (forceSituationResultType) {
      context.setResultType(ScenarioResultType.SITUATION_RESULT);
    } else {
      context.setResultType(ScenarioResultType.getDefault(jobType, situationType));
    }
  }

  private static void setHexagonType(final ResultSummaryContext context) {
    context.setHexagonType(context.getAvailableSummaryHexagonTypes().stream().findFirst().orElse(SummaryHexagonType.EXCEEDING_HEXAGONS));
  }
}
