/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.development;

import java.util.List;

import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.EmissionCalculationMethod;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.SectorProperties;
import nl.overheid.aerius.shared.domain.sector.SectorPropertiesSet;
import nl.overheid.aerius.shared.domain.sector.category.SimpleCategory;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.geojson.GeometryType;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.ADMSRoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.domain.source.ColdStartESFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.GenericESFeature;
import nl.overheid.aerius.wui.application.domain.source.RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.SRM2RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.road.StandardVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.ValuesPerVehicleType;
import nl.overheid.aerius.wui.application.domain.source.util.EmissionSourceFeatureUtil;

public class DummyEmissionSources {
  private static ApplicationConfiguration conf;
  private static ApplicationConfiguration themeConf;

  public static BuildingFeature createBuilding(final int index) {
    final BuildingFeature simpleFeature = BuildingFeature.create(1);
    final String label = "Gebouw " + index;
    final HashNumber num = HashNumber.create(label);
    simpleFeature.setGeometry(DeterministicGeometry.get(num, GeometryType.POLYGON));
    simpleFeature.setHeight(5.5);
    simpleFeature.setLabel(label);
    return simpleFeature;
  }

  public static EmissionSourceFeature createAdmsFarmEnergySource(final int i) {
    final GenericESFeature simpleFeature = GenericESFeature.create(CharacteristicsType.ADMS);
    addSimpleSettings(simpleFeature, "Farm", i, 2100);
    return simpleFeature;
  }

  public static EmissionSourceFeature createOpsFarmEnergySource(final int i) {
    final GenericESFeature simpleFeature = GenericESFeature.create(CharacteristicsType.OPS);
    addSimpleSettings(simpleFeature, "Boederij", i, 2100);
    return simpleFeature;
  }

  public static EmissionSourceFeature createAgriSource(final int i, final Sector sector, final CharacteristicsType type) {
    final SectorPropertiesSet sectorPropertiesSet = themeConf.getSectorPropertiesSet();

    final SectorProperties sectorProperties = sectorPropertiesSet.get(sector.getSectorId());
    final EmissionSourceFeature src = EmissionSourceFeatureUtil.createEmissionSourceFeature(sectorProperties, type);
    final String label = "Source " + i;
    src.setSectorId(sector.getSectorId());
    src.setLabel(label);
    src.setGeometry(DeterministicGeometry.get(HashNumber.create(label)));
    return src;
  }

  public static EmissionSourceFeature createColdStartSource(final int i, final List<Integer> sectors, final CharacteristicsType type) {
    final EmissionCalculationMethod ecm = EmissionCalculationMethod.COLD_START_OTHER;

    final Integer sectorId = sectors.stream()
        .filter(v -> themeConf.getSectorPropertiesSet().get(v).getMethod() == ecm)
        .findFirst().get();

    final EmissionSourceFeature src = ColdStartESFeature.create(type, true);
    final String label = "Cold start source " + i;
    src.setSectorId(sectorId);
    src.setLabel(label);
    src.setGeometry(DeterministicGeometry.get(HashNumber.create(label)));
    return src;
  }

  public static EmissionSourceFeature createAdmsRoadSource(final int i) {
    final ADMSRoadESFeature roadFeature = ADMSRoadESFeature.create();
    addSimpleLine(roadFeature, "Simple road segment", i);

    final String code = conf.getSectorCategories().getRoadEmissionCategories().getAreas().iterator().next().getCode();
    final List<SimpleCategory> roadTypes = conf.getSectorCategories().getRoadEmissionCategories().getRoadTypes(code);
    roadFeature.setRoadAreaCode(code);
    roadFeature.setRoadTypeCode(roadTypes.iterator().next().getCode());
    roadFeature.setWidth(2D);

    final StandardVehicles vehicles1 = StandardVehicles.create();
    vehicles1.setTimeUnit(TimeUnit.MONTH);

    vehicles1.setMaximumSpeed(120);

    final HashNumber number = HashNumber.create("Vehicles" + i);
    final List<SimpleCategory> types = conf.getSectorCategories().getRoadEmissionCategories().getVehicleTypes();
    types.forEach(v -> {
      final ValuesPerVehicleType type = ValuesPerVehicleType.create();
      type.setVehiclesPerTimeUnit(number.nextInt(500));
      vehicles1.setValuesPerVehicleType(v.getCode(), type);
    });

    roadFeature.getSubSources().push(vehicles1);
    roadFeature.setSectorId(3100);

    return roadFeature;
  }

  public static EmissionSourceFeature createOpsRoadSource(final int i) {
    final SRM2RoadESFeature roadFeature = SRM2RoadESFeature.create();
    addSimpleLine(roadFeature, "Wegsegment", i);
    roadFeature.setSectorId(3100);
    return roadFeature;
  }

  private static void addSimpleLine(final RoadESFeature feature, final String label, final int index) {
    final HashNumber num = HashNumber.create(label + " > " + index);
    feature.setLabel(label + index);
    feature.setGeometry(DeterministicGeometry.get(num, GeometryType.LINESTRING));
  }

  public static void addSimpleSettings(final EmissionSourceFeature feature, final String label, final int index, final int sectorId) {
    final HashNumber num = HashNumber.create(label + " > " + index);
    feature.setGeometry(DeterministicGeometry.get(num));
    feature.setLabel(label + " " + index);
    feature.setEmission(Substance.NOX, num.nextInt(1000));
    feature.setEmission(Substance.NH3, num.nextInt(2000));
    feature.setSectorId(sectorId);
  }

  /**
   * Static init because we don't want to tie this thing into the injection model
   */
  public static void init(final ApplicationContext applicationContext) {
    conf = applicationContext.getConfiguration();
    themeConf = applicationContext.getConfiguration();
  }
}
