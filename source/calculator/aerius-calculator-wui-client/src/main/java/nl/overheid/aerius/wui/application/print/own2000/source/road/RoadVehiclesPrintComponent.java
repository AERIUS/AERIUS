/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.print.own2000.source.road;

import com.axellience.vuegwt.core.annotations.component.Component;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.wui.application.components.source.road.vehicles.RoadVehiclesDetailComponent;
import nl.overheid.aerius.wui.application.domain.source.road.VehicleType;

@Component
public class RoadVehiclesPrintComponent extends RoadVehiclesDetailComponent {

  @JsMethod
  public String getSpecificVehicleDescription(final String specificVehicles) {
    return applicationContext.getConfiguration().getSectorCategories().determineOnRoadMobileSourceCategoryByCode(specificVehicles).getDescription();
  }

  @JsMethod
  public boolean isCustomSeperatorVisible() {
    return (hasVehicleType(VehicleType.STANDARD) || hasVehicleType(VehicleType.STANDARD_CS)) && hasVehicleType(VehicleType.CUSTOM);
  }

  @JsMethod
  public boolean isSpecificSeperatorVisible() {
    return (hasVehicleType(VehicleType.STANDARD) || hasVehicleType(VehicleType.STANDARD_CS) || hasVehicleType(VehicleType.CUSTOM))
        && hasVehicleType(VehicleType.SPECIFIC);
  }

}
