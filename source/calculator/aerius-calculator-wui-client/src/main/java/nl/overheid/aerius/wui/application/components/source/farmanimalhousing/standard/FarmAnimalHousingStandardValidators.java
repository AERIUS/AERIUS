/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.farmanimalhousing.standard;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.aerius.vuelidate.ValidationOptions;
import nl.aerius.vuelidate.Validations;
import nl.aerius.vuelidate.ValidatorBuilder;
import nl.overheid.aerius.shared.ImaerConstants;

/**
 * Validators for FarmAnimalHousingStandard editing.
 */
class FarmAnimalHousingStandardValidators extends ValidationOptions<FarmAnimalHousingStandardEmissionComponent> {

  protected static final int MIN_NUMBER_OF_DAYS = 0;
  protected static final int MAX_NUMBER_OF_DAYS = ImaerConstants.DAYS_PER_YEAR;

  @JsType(isNative = true, namespace = JsPackage.GLOBAL)
  static class FarmAnimalHousingStandardValidations extends Validations {
    public @JsProperty Validations farmAnimalHousingCodeV;
    public @JsProperty Validations numberOfAnimalsV;
    public @JsProperty Validations numberOfDaysV;
  }

  @Override
  protected void constructValidators(final Object options, final ValidatorInstaller v) {
    final FarmAnimalHousingStandardEmissionComponent instance = Js.uncheckedCast(options);

    v.install("farmAnimalHousingCodeV", () -> instance.farmAnimalHousingCodeV = null,
        ValidatorBuilder.create()
            .required());
    v.install("numberOfAnimalsV", () -> instance.numberOfAnimalsV = null,
        ValidatorBuilder.create()
            .required()
            .integer()
            .minValue(0));
    v.install("numberOfDaysV", () -> instance.numberOfDaysV = null,
        ValidatorBuilder.create()
            .minValue(MIN_NUMBER_OF_DAYS)
            .maxValue(MAX_NUMBER_OF_DAYS));
  }
}
