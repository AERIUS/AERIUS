/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.export;

import java.util.Date;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.wui.application.domain.calculation.JobProgress;

public class HistoryRecord {
  private String jobKey = null;

  private String label = null;
  private Date date = null;

  private @JsProperty JobProgress jobProgress = null;

  private String exportHash = null;

  public String getJobKey() {
    return jobKey;
  }

  public void setJobKey(final String jobKey) {
    this.jobKey = jobKey;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(final String label) {
    this.label = label;
  }

  public Date getDateTime() {
    return date;
  }

  public void setDatetime(final Date date) {
    this.date = date;
  }

  public String getResultUrl() {
    return jobProgress == null ? null : jobProgress.getResultUrl();
  }

  public void setErrorMessage(final String errorMessage) {
    jobProgress.setErrorMessage(errorMessage);
  }

  public String getErrorMessage() {
    return jobProgress.getErrorMessage();
  }

  public void setJobProgress(final JobProgress jobProgress) {
    this.jobProgress = jobProgress;
  }

  public JobProgress getJobProgress() {
    return jobProgress;
  }

  public JobState getStatus() {
    return jobProgress == null ? JobState.UNDEFINED : jobProgress.getState();
  }

  public int getQueuePosition() {
    return jobProgress == null || jobProgress.getState() != JobState.QUEUING ? 0 : jobProgress.getQueuePosition();
  }

  public void setExportHash(final String exportHash) {
    this.exportHash = exportHash;
  }

  public String getExportHash() {
    return exportHash;
  }
}
