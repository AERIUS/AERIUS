/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.resources.images.markers;

import com.google.gwt.resources.client.ClientBundle.Source;
import com.google.gwt.resources.client.DataResource;
import com.google.gwt.resources.client.DataResource.MimeType;

public interface MarkerResources {

  @Source("map-infomarker.svg")
  @MimeType("image/svg+xml")
  DataResource markerInfo();

  /* Scenario contribution */

  @Source("combinable/sc-hcon.svg")
  @MimeType("image/svg+xml")
  DataResource scHcon();

  @Source("combinable/sc-htot.svg")
  @MimeType("image/svg+xml")
  DataResource scHtot();

  @Source("combinable/sc-hcon-htot.svg")
  @MimeType("image/svg+xml")
  DataResource scHconHtot();

  /* Process contribution */

  @Source("combinable/pc-hcon-hcum.svg")
  @MimeType("image/svg+xml")
  DataResource pcHconHcum();

  @Source("combinable/pc-hcum.svg")
  @MimeType("image/svg+xml")
  DataResource pcHcum();

  @Source("combinable/pc-hdec.svg")
  @MimeType("image/svg+xml")
  DataResource pcHdec();

  @Source("combinable/pc-hdec-hcum.svg")
  @MimeType("image/svg+xml")
  DataResource pcHdecHcum();

  @Source("combinable/pc-hdec-htot.svg")
  @MimeType("image/svg+xml")
  DataResource pcHdecHtot();

  @Source("combinable/pc-hinc.svg")
  @MimeType("image/svg+xml")
  DataResource pcHinc();

  @Source("combinable/pc-hinc-hcum.svg")
  @MimeType("image/svg+xml")
  DataResource pcHincHcum();

  @Source("combinable/pc-hinc-htot.svg")
  @MimeType("image/svg+xml")
  DataResource pcHincHtot();

  @Source("combinable/pc-htot.svg")
  @MimeType("image/svg+xml")
  DataResource pcHtot();

  @Source("combinable/pc-htot-hcon-hcum.svg")
  @MimeType("image/svg+xml")
  DataResource pcHtotHconHcum();

  @Source("combinable/pc-htot-hcum-hinc.svg")
  @MimeType("image/svg+xml")
  DataResource pcHtotHcumHinc();

  @Source("combinable/pc-htot-hcum-hdec.svg")
  @MimeType("image/svg+xml")
  DataResource pcHtotHcumHdec();

  @Source("combinable/pc-htot-hcum.svg")
  @MimeType("image/svg+xml")
  DataResource pcHtotHcum();

  /* Max temp effect (OwN2000/NCA) */

  @Source("combinable/mt-hmax.svg")
  @MimeType("image/svg+xml")
  DataResource mtHmax();

  @Source("combinable/mt-htot.svg")
  @MimeType("image/svg+xml")
  DataResource mtHtot();

  @Source("combinable/mt-hmax-htot.svg")
  @MimeType("image/svg+xml")
  DataResource mtHmaxHtot();

  /* In combination */

  @Source("combinable/ic-hcon-hcum.svg")
  @MimeType("image/svg+xml")
  DataResource icHconHcum();

  @Source("combinable/ic-hcum.svg")
  @MimeType("image/svg+xml")
  DataResource icHcum();

  @Source("combinable/ic-hdec.svg")
  @MimeType("image/svg+xml")
  DataResource icHdec();

  @Source("combinable/ic-hdec-hcum.svg")
  @MimeType("image/svg+xml")
  DataResource icHdecHcum();

  @Source("combinable/ic-hdec-htot.svg")
  @MimeType("image/svg+xml")
  DataResource icHdecHtot();

  @Source("combinable/ic-hinc.svg")
  @MimeType("image/svg+xml")
  DataResource icHinc();

  @Source("combinable/ic-hinc-hcum.svg")
  @MimeType("image/svg+xml")
  DataResource icHincHcum();

  @Source("combinable/ic-hinc-htot.svg")
  @MimeType("image/svg+xml")
  DataResource icHincHtot();

  @Source("combinable/ic-htot.svg")
  @MimeType("image/svg+xml")
  DataResource icHtot();

  @Source("combinable/ic-htot-hcon-hcum.svg")
  @MimeType("image/svg+xml")
  DataResource icHtotHconHcum();

  @Source("combinable/ic-htot-hcum.svg")
  @MimeType("image/svg+xml")
  DataResource icHtotHcum();

  @Source("combinable/ic-htot-hcum-hinc.svg")
  @MimeType("image/svg+xml")
  DataResource icHtotHcumHinc();

  @Source("combinable/ic-htot-hcum-hdec.svg")
  @MimeType("image/svg+xml")
  DataResource icHtotHcumHdec();

  /* Archive contribution */

  @Source("combinable/ac-hcon-hcum.svg")
  @MimeType("image/svg+xml")
  DataResource acHconHcum();

  @Source("combinable/ac-hcum.svg")
  @MimeType("image/svg+xml")
  DataResource acHcum();

  @Source("combinable/ac-hdec.svg")
  @MimeType("image/svg+xml")
  DataResource acHdec();

  @Source("combinable/ac-hdec-hcum.svg")
  @MimeType("image/svg+xml")
  DataResource acHdecHcum();

  @Source("combinable/ac-hdec-htot.svg")
  @MimeType("image/svg+xml")
  DataResource acHdecHtot();

  @Source("combinable/ac-hinc.svg")
  @MimeType("image/svg+xml")
  DataResource acHinc();

  @Source("combinable/ac-hinc-hcum.svg")
  @MimeType("image/svg+xml")
  DataResource acHincHcum();

  @Source("combinable/ac-hinc-htot.svg")
  @MimeType("image/svg+xml")
  DataResource acHincHtot();

  @Source("combinable/ac-htot.svg")
  @MimeType("image/svg+xml")
  DataResource acHtot();

  @Source("combinable/ac-htot-hcon-hcum.svg")
  @MimeType("image/svg+xml")
  DataResource acHtotHconHcum();

  @Source("combinable/ac-htot-hcum.svg")
  @MimeType("image/svg+xml")
  DataResource acHtotHcum();

  @Source("combinable/ac-htot-hcum-hinc.svg")
  @MimeType("image/svg+xml")
  DataResource acHtotHcumHinc();

  @Source("combinable/ac-htot-hcum-hdec.svg")
  @MimeType("image/svg+xml")
  DataResource acHtotHcumHdec();

  /* Situatieresultaat */

  @Source("combinable/sr-hbij.svg")
  @MimeType("image/svg+xml")
  DataResource srHbij();

  @Source("combinable/sr-htot.svg")
  @MimeType("image/svg+xml")
  DataResource srHtot();

  @Source("combinable/sr-hbij-htot.svg")
  @MimeType("image/svg+xml")
  DataResource srHbijHtot();

  /* Projectberekening */

  @Source("combinable/pb-htoe.svg")
  @MimeType("image/svg+xml")
  DataResource pbHtoe();

  @Source("combinable/pb-hafn.svg")
  @MimeType("image/svg+xml")
  DataResource pbHafn();

  @Source("combinable/pb-htot.svg")
  @MimeType("image/svg+xml")
  DataResource pbHtot();

  @Source("combinable/pb-htoe-htot.svg")
  @MimeType("image/svg+xml")
  DataResource pbHtoeHtot();

  @Source("combinable/pb-hafn-htot.svg")
  @MimeType("image/svg+xml")
  DataResource pbHafnHtot();

  /* Cumulatieberekening */

  @Source("combinable/cb-htoe.svg")
  @MimeType("image/svg+xml")
  DataResource cbHtoe();

  @Source("combinable/cb-hafn.svg")
  @MimeType("image/svg+xml")
  DataResource cbHafn();

  @Source("combinable/cb-htot.svg")
  @MimeType("image/svg+xml")
  DataResource cbHtot();

  @Source("combinable/cb-htoe-htot.svg")
  @MimeType("image/svg+xml")
  DataResource cbHtoeHtot();

  @Source("combinable/cb-hafn-htot.svg")
  @MimeType("image/svg+xml")
  DataResource cbHafnHtot();

  /* Depositievracht */

  @Source("combinable/ds-procurement-policy-pass.svg")
  @MimeType("image/svg+xml")
  DataResource dsProcurementPolicyPass();

  @Source("combinable/ds-procurement-policy-fail.svg")
  @MimeType("image/svg+xml")
  DataResource dsProcurementPolicyFail();

  /* PDF */

  @Source("site-location-marker.svg")
  @MimeType("image/svg+xml")
  DataResource siteLocationMarker();

}
