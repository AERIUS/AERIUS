/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.wui.vue.ReactivityUtil;
import nl.overheid.aerius.wui.vue.SkipReactivityValidations;

@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public abstract class AbstractSubSource {
  private @JsProperty JsPropertyMap<Double> emissions;

  @JsOverlay
  public static final void initAbstractSubSource(final AbstractSubSource props) {
    ReactivityUtil.ensureJsPropertySupplied(props, "emissions", props::setEmissions, () -> Js.cast(JsPropertyMap.of()));
  }

  @JsOverlay
  public final JsPropertyMap<Double> getEmissions() {
    return emissions;
  }

  @JsOverlay
  public final void setEmissions(final JsPropertyMap<Double> emissions) {
    this.emissions = emissions;
  }

  @JsOverlay
  public final boolean hasEmission(final Substance substance) {
    return emissions == null ? false : emissions.get(substance.name()) != null;
  }

  @JsOverlay
  @SkipReactivityValidations
  public final double getEmission(final Substance substance) {
    final Object emission = emissions == null ? null : emissions.get(substance.name());
    return emission == null ? 0.0 : Js.coerceToDouble(emission);
  }

  @JsOverlay
  public final void setEmission(final Substance substance, final double emission) {
    if (emissions != null) {
      emissions.set(substance.name(), emission);
    } else {
      setEmissions(Js.uncheckedCast(JsPropertyMap.of(substance.name(), emission)));
    }
  }
}
