/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.manure;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.overheid.aerius.wui.application.domain.source.AbstractSubSource;
import nl.overheid.aerius.wui.vue.ReactivityUtil;

/**
 * Client side implementation of AbstractManureStorage.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public abstract class ManureStorage extends AbstractSubSource {

  private String manureStorageType;
  private Double tonnes;
  private Double metersSquared;
  // Backend uses Integer, but that doesn't play well with GWT's serializing (doesn't get mapped as a 'number').
  private Double numberOfDays;

  public static final @JsOverlay void initManureStorage(final ManureStorage props, final ManureStorageType type) {
    AbstractSubSource.initAbstractSubSource(props);

    ReactivityUtil.ensureJsProperty(props, "manureStorageType", props::setManureStorageType, type);
    ReactivityUtil.ensureJsPropertyAsNull(props, "tonnes", props::setTonnes);
    ReactivityUtil.ensureJsPropertyAsNull(props, "metersSquared", props::setMetersSquared);
    ReactivityUtil.ensureJsPropertyAsNull(props, "numberOfDays", props::setNumberOfDays);
  }

  public static final @JsOverlay void initTypeDependent(final ManureStorage props, final ManureStorageType type) {
    switch (type) {
    case CUSTOM:
      CustomManureStorage.init(Js.uncheckedCast(props));
      break;
    case STANDARD:
      StandardManureStorage.init(Js.uncheckedCast(props));
      break;
    default:
      throw new RuntimeException("Unknown type: " + type);
    }
  }

  @JsOverlay
  public final ManureStorageType getManureStorageType() {
    return ManureStorageType.safeValueOf(manureStorageType);
  }

  @JsOverlay
  public final void setManureStorageType(final ManureStorageType manureStorageType) {
    this.manureStorageType = manureStorageType.name();
  }

  @JsOverlay
  public final Double getTonnes() {
    return tonnes;
  }

  @JsOverlay
  public final void setTonnes(final Double tonnes) {
    this.tonnes = tonnes;
  }

  @JsOverlay
  public final Double getMetersSquared() {
    return metersSquared;
  }

  @JsOverlay
  public final void setMetersSquared(final Double metersSquared) {
    this.metersSquared = metersSquared;
  }

  @JsOverlay
  public final Double getNumberOfDays() {
    return numberOfDays;
  }

  @JsOverlay
  public final void setNumberOfDays(final Double numberOfDays) {
    this.numberOfDays = numberOfDays;
  }

}
