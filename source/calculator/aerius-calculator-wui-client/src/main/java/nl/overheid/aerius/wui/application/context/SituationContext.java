/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import ol.Feature;
import ol.format.GeoJson;
import ol.format.GeoJsonFeatureOptions;

import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.context.calculation.SituationHandle;
import nl.overheid.aerius.wui.application.domain.geo.FeatureUtil;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.TimeVaryingProfile;
import nl.overheid.aerius.wui.application.domain.source.calculation.EmissionSourceCalculator;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.util.UUID;
import nl.overheid.aerius.wui.application.util.json.JsonBuilder;
import nl.overheid.aerius.wui.application.util.json.JsonSerializable;

/**
 * Object containing situation specific information.
 *
 * @see nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation
 */
@JsType
public class SituationContext implements JsonSerializable {

  private final String situationNumber;
  private @JsProperty String id = UUID.uuidv4();
  private @JsProperty int year = 0;
  private @JsProperty String name = null;
  private @JsProperty SituationType type = null;
  private @JsProperty Double nettingFactor = null;
  private @JsProperty List<EmissionSourceFeature> sources = new ArrayList<>();
  private @JsProperty List<BuildingFeature> buildings = new ArrayList<>();
  private @JsProperty List<TimeVaryingProfile> timeVaryingProfiles = new ArrayList<>();

  public SituationContext(final String situationNumber) {
    this.situationNumber = situationNumber;
  }

  public void copyFrom(final SituationContext original) {
    type = original.getType();
    year = original.getYear();
    nettingFactor = original.getNettingFactor();

    setSources(original.getSources().stream()
        .map(source -> FeatureUtil.clone(source))
        .collect(Collectors.toList()));
    setBuildings(original.getBuildings().stream()
        .map(building -> FeatureUtil.clone(building))
        .collect(Collectors.toList()));
    setTimeVaryingProfiles(original.getTimeVaryingProfiles().stream()
        .map(profile -> profile.copy())
        .collect(Collectors.toList()));
  }

  public String getSituationNumber() {
    return situationNumber;
  }

  public String getId() {
    return id;
  }

  /**
   * Replaces the auto-generated id.
   * Only use if you can guarantee the new id will be unique in the UI.
   */
  public void replaceId(final String id) {
    this.id = id;
  }

  public int getYear() {
    return year;
  }

  public void setYear(final int year) {
    this.year = year;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public SituationType getType() {
    return type;
  }

  public void setType(final SituationType type) {
    this.type = type;
  }

  public Double getNettingFactor() {
    return nettingFactor;
  }

  public void setNettingFactor(final Double nettingFactor) {
    this.nettingFactor = nettingFactor;
  }

  /**
   * Return a guarded java.util.List ensuring the underlying list does not get
   * modified externally.
   *
   * This should be used for viewing.
   */
  public List<EmissionSourceFeature> getSources() {
    return new ArrayList<>(sources);
  }

  public int getSourcesSize() {
    return sources.size();
  }

  public List<EmissionSourceFeature> getOriginalSources() {
    return sources;
  }

  @Deprecated
  public void setSources(final List<EmissionSourceFeature> sources) {
    this.sources = new ArrayList<>(sources);
  }

  /**
   * Return a guarded java.util.List ensuring the underlying list does not get
   * modified externally.
   *
   * This should be used for viewing.
   */
  public List<BuildingFeature> getBuildings() {
    // Return a guarded list so this list does not get modified externally
    return new ArrayList<>(buildings);
  }

  public int getBuildingsSize() {
    return buildings.size();
  }

  /**
   * Return the unguarded/unmodified list of buildings. To be used for editing only.
   */
  public List<BuildingFeature> getOriginalBuildings() {
    return buildings;
  }

  @Deprecated
  public void setBuildings(final List<BuildingFeature> buildings) {
    this.buildings = new ArrayList<>(buildings);
  }

  public List<TimeVaryingProfile> getTimeVaryingProfiles() {
    return new ArrayList<>(timeVaryingProfiles);
  }

  public int getTimeVaryingProfilesSize() {
    return timeVaryingProfiles.size();
  }

  /**
   * Return the unguarded/unmodified list of time-varying profiles. To be used for editing only.
   */
  public List<TimeVaryingProfile> getOriginalTimeVaryingProfiles() {
    return timeVaryingProfiles;
  }

  @Deprecated
  public void setTimeVaryingProfiles(final List<TimeVaryingProfile> profiles) {
    this.timeVaryingProfiles = new ArrayList<>(profiles);
  }

  public double getTotalEmission(final Substance substance) {
    return EmissionSourceCalculator.sum(getSources(), substance);
  }

  public EmissionSourceFeature findSource(final String gmlId) {
    return getSources().stream()
        .filter(v -> gmlId.equals(v.getGmlId()))
        .findFirst().orElse(null);
  }

  public BuildingFeature findBuilding(final String gmlId) {
    return getBuildings().stream()
        .filter(v -> gmlId.equals(v.getGmlId()))
        .findFirst().orElse(null);
  }

  public BuildingFeature findBuildingById(final String id) {
    return getBuildings().stream()
        .filter(v -> id.equals(v.getId()))
        .findFirst().orElse(null);
  }

  public TimeVaryingProfile findTimeVaryingProfile(final String gmlId) {
    if (gmlId == null) {
      return null;
    }
    return getTimeVaryingProfiles().stream()
        .filter(v -> gmlId.equals(v.getGmlId()))
        .findFirst().orElse(null);
  }

  @Override
  public int hashCode() {
    return java.util.Objects.hash(id);
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    final SituationContext other = (SituationContext) obj;
    return java.util.Objects.equals(id, other.id);
  }

  @Override
  public Object toJSON() {
    return new JsonBuilder(this)
        .include("id", "year", "name", "nettingFactor")
        .set("type", type.toString())
        .set("emissionSources",
            new GeoJson().writeFeaturesObject(sources.toArray(new Feature[sources.size()]), new GeoJsonFeatureOptions()))
        .set("buildings", new GeoJson().writeFeaturesObject(buildings.toArray(new Feature[buildings.size()]), new GeoJsonFeatureOptions()))
        .set("definitions", JsPropertyMap.of("customTimeVaryingProfiles", timeVaryingProfiles.toArray(new TimeVaryingProfile[timeVaryingProfiles.size()])))
        .build();
  }

  public static SituationHandle handleFromSituation(final SituationContext context) {
    final SituationHandle handle = new SituationHandle();
    handle.setId(context.getId());
    handle.setType(context.getType());
    handle.setName(context.getName());
    handle.setSources(context.getSourcesSize());
    handle.putHasEmissions(Substance.NOX, context.getTotalEmission(Substance.NOX) > 0.0);
    handle.putHasEmissions(Substance.NH3, context.getTotalEmission(Substance.NH3) > 0.0);
    return handle;
  }

  public static String formatName(final SituationContext situation) {
    return situation.getName() + " - " + M.messages().situationType(situation.getType());
  }

  public static boolean isEmpty(final SituationContext situation) {
    return situation.getBuildings().isEmpty()
        && situation.getSources().isEmpty()
        && situation.getTimeVaryingProfiles().isEmpty();
  }

  /**
   * Ensures the situation type is never null or UNKNOWN.
   *
   * If the situation type is set to null,
   *   don't set it.
   * If it is already null,
   *   set it to PROPOSED.
   * If it is not null and also not UNKNOWN,
   *   leave it be.
   */
  public static void safeSetSituationType(final SituationContext situation, final SituationType type) {
    if (type != null && type != SituationType.UNKNOWN) {
      situation.setType(type);
    } else {
      if (situation.getType() != null && situation.getType() != SituationType.UNKNOWN) {
        return;
      } else {
        situation.setType(SituationType.PROPOSED);
      }
    }
  }

  public static boolean hasAnyContent(final SituationContext sit) {
    return !sit.getSources().isEmpty()
        || !sit.getBuildings().isEmpty()
        || !sit.getTimeVaryingProfiles().isEmpty();
  }
}
