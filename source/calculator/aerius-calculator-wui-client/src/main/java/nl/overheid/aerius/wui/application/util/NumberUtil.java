/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.util;

import java.util.Comparator;
import java.util.function.Function;

public final class NumberUtil {
  private static final double DEFAULT_TOLERANCE = 0.00001;

  private NumberUtil() {
  }

  /**
   * Compare two double floats for exact equality.
   */
  public static boolean equalExactly(final Double a, final Double b) {
    return equalEnough(a, b, 0D);
  }

  /**
   * Forgivingly compare two double floats for equality using a default tolerance.
   */
  public static boolean equalEnough(final Double a, final Double b) {
    return equalEnough(a, b, DEFAULT_TOLERANCE);
  }

  /**
   * Forgivingly compare two double floats for equality using the given tolerance.
   */
  public static boolean equalEnough(final Double a, final Double b, final double tolerance) {
    return a == null || b == null
        ? a == null && b == null
        : Math.abs(a - b) <= tolerance;
  }

  /**
   * Forgivingly compare two double floats for equality using the given tolerance, exclusive.
   */
  public static boolean equalEnoughExclusive(final Double a, final Double b, final double tolerance) {
    return a == null || b == null
        ? a == null && b == null
        : Math.abs(a - b) < tolerance;
  }

  /**
   * Round the value to the given number of digits.
   *
   * @param value value to round
   * @param digits number of digits to round
   * @return rounded value
   */
  public static double round(final double value, final int digits) {
    return Math.round(value * Math.pow(10, digits)) / Math.pow(10, digits);
  };

  /**
   * Safely sorts numbers(double) values.
   *
   * @param func function to get the double
   * @param asc if true sorts ascending, otherwise descending
   * @return safely sort numbers
   */
  public static <T> Comparator<T> sortComparator(final Function<T, Number> func, final boolean asc) {
    return (o1, o2) -> (asc ? 1 : -1) * Double.compare(safeNumber(func, o1), safeNumber(func, o2));
  }

  private static <T> double safeNumber(final Function<T, Number> func, final T o1) {
    final Number value = func.apply(o1);

    return value == null ? Double.NaN : value.doubleValue();
  }
}
