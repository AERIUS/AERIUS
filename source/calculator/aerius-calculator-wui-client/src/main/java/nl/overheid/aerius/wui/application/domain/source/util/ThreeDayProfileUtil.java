/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.util;

import java.util.List;

import nl.aerius.wui.dev.GWTProd;
import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfileType;
import nl.overheid.aerius.wui.application.domain.source.ADMSCharacteristics;

/**
 * Util class for three day (diurnal) time-varying profile value handling.
 */
public class ThreeDayProfileUtil extends TimeVaryingProfileUtilBase {

  public static final int WEEKDAY = 0;
  public static final int SATURDAY = 1;
  public static final int SUNDAY = 2;

  public static final long HOURS_IN_DAY = 24;
  private static final double DAYS_IN_WEEK = 7;
  private static final double WEEKDAYS_IN_WEEK = 5;

  public ThreeDayProfileUtil() {
    super(CustomTimeVaryingProfileType.THREE_DAY);
  }

  public String getValue(final List<Double> values, final int day, final int hour) {
    if (checkAndwarnInvalidSize(values)) {
      final int idx = getThreeDayIndex(day, hour);
      final double num = values.get(idx);

      return formattedPreciseValue(num);
    } else {
      return "";
    }
  }

  /**
   * @param day 0 for weekday, 1 for saturday, 2 for sunday
   */
  public void setInput(final List<Double> values, final int day, final int hour, final double value) {
    if (checkAndwarnInvalidSize(values)) {
      setInputAtIndex(values, getThreeDayIndex(day, hour), value);
    }
  }

  private static int getThreeDayIndex(final int day, final int hour) {
    return (int) (day * HOURS_IN_DAY + hour);
  }

  public String getTotal(final List<Double> values, final int day) {
    if (day <= 2) {
      final double total = values.stream()
          .mapToDouble(v -> v)
          .skip(HOURS_IN_DAY * day)
          .limit(HOURS_IN_DAY).sum() * (day == 0 ? WEEKDAYS_IN_WEEK : 1);
      return formattedPreciseValue(total);
    } else {
      GWTProd.error("Day requested that we cannot handle: " + day);
      return null;
    }
  }

  public String getThreeDayAverage(final List<Double> values, final int day) {
    if (day <= 2) {
      final double average = values.stream()
          .mapToDouble(v -> v)
          .skip(HOURS_IN_DAY * day)
          .limit(HOURS_IN_DAY).average()
          .orElse(0D);
      return formattedPreciseValue(average);
    } else {
      GWTProd.error("Day requested that we cannot handle: " + day);
      return null;
    }
  }

  @Override
  public String getCustomTimeVaryingProfileId(final ADMSCharacteristics chars) {
    return chars.getCustomHourlyTimeVaryingProfileId();
  }

  @Override
  public void setCustomTimeVaryingProfileId(final ADMSCharacteristics chars, final String id) {
    chars.setCustomHourlyTimeVaryingProfileId(id);
  }

  @Override
  public String getStandardTimeVaryingProfileCode(final ADMSCharacteristics chars) {
    return chars.getStandardHourlyTimeVaryingProfileCode();
  }

  @Override
  public void setStandardTimeVaryingProfileCode(final ADMSCharacteristics chars, final String code) {
    chars.setStandardHourlyTimeVaryingProfileCode(code);
  }

  @Override
  protected double getValuesTotalRaw(final List<Double> values) {
    // First 24 values are day 1-5
    final double totalWeekDays = values.stream().mapToDouble(v -> v).limit(HOURS_IN_DAY).sum() * WEEKDAYS_IN_WEEK;
    final double totalWeekend = values.stream().mapToDouble(v -> v).skip(HOURS_IN_DAY).sum();

    return totalWeekDays + totalWeekend;
  }

  @Override
  protected double getValuesAverageRaw(final List<Double> values) {
    final double totalWeekDays = values.stream().mapToDouble(v -> v).limit(HOURS_IN_DAY).average().orElse(0D) * WEEKDAYS_IN_WEEK;
    final double totalSaturday = values.stream().mapToDouble(v -> v).skip(HOURS_IN_DAY).limit(HOURS_IN_DAY).average().orElse(0D);
    final double totalSunday = values.stream().mapToDouble(v -> v).skip(HOURS_IN_DAY * 2).average().orElse(0D);

    // Do a weighted average
    return (totalWeekDays + totalSaturday + totalSunday) / DAYS_IN_WEEK;
  }
}
