/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.importer.domain;

import java.util.Objects;

import nl.overheid.aerius.wui.application.context.SituationContext;

public class ImportAction {
  public enum ActionType {
    /**
     *
     */
    ADD_TO_EXISTING_SITUATION,
    /**
     *
     */
    ADD_TO_VIRTUAL_SITUATION,
    /**
     *
     */
    ADD_NEW,
    /**
     *
     */
    ADD_CALCULATION_POINTS,
    /**
    *
    */
    ADD_ARCHIVE_CONTRIBUTION,
    /**
     *
     */
    IGNORE
  }

  private ActionType type = null;

  private SituationContext existingSituation = null;
  private VirtualSituation virtualSituation = null;

  public ImportAction(final ActionType type) {
    this.type = type;
  }

  public static ImportAction mergeIntoVirtualSituation(final VirtualSituation situation) {
    final ImportAction act = new ImportAction(ActionType.ADD_TO_VIRTUAL_SITUATION);
    act.setVirtualSituation(situation);
    return act;
  }

  public static ImportAction mergeIntoExistingSituation(final SituationContext situation) {
    final ImportAction act = new ImportAction(ActionType.ADD_TO_EXISTING_SITUATION);
    act.setExistingSituation(situation);
    return act;
  }

  public static ImportAction addNew() {
    return new ImportAction(ActionType.ADD_NEW);
  }

  public static ImportAction ignore() {
    return new ImportAction(ActionType.IGNORE);
  }

  public static ImportAction addCalculationPoints() {
    return new ImportAction(ActionType.ADD_CALCULATION_POINTS);
  }

  public static ImportAction addArchive() {
    return new ImportAction(ActionType.ADD_ARCHIVE_CONTRIBUTION);
  }

  public ActionType getType() {
    return type;
  }

  public void setExistingSituation(final SituationContext existingSituation) {
    this.existingSituation = existingSituation;
  }

  public SituationContext getExistingSituation() {
    return existingSituation;
  }

  public void setVirtualSituation(final VirtualSituation virtualSituation) {
    this.virtualSituation = virtualSituation;
  }

  public VirtualSituation getVirtualSituation() {
    return virtualSituation;
  }

  @Override
  public int hashCode() {
    return Objects.hash(existingSituation, type, virtualSituation);
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    final ImportAction other = (ImportAction) obj;
    return Objects.equals(existingSituation, other.existingSituation) && type == other.type
        && Objects.equals(virtualSituation, other.virtualSituation);
  }
}
