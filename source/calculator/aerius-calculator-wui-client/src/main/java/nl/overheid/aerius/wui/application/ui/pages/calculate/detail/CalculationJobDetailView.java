/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.calculate.detail;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasDestroyed;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.HorizontalCollapse;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobDeselectCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.divider.DividerComponent;
import nl.overheid.aerius.wui.application.components.input.InputWarningComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationPreparationContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.domain.DevelopmentPressureUtil;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.application.ui.main.LeftPanelCloseEvent;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    InputWarningComponent.class,
    CalculationJobDetailHeaderView.class,
    CalculationJobDetailScenariosView.class,
    CalculationJobDetailDevelopmentPressureSearchView.class,
    CalculationJobDetailMeteorologicalSettingsView.class,
    CalculationJobDetailAdvancedSettingsView.class,
    HorizontalCollapse.class,
    DividerComponent.class,
    ButtonIcon.class
})
public class CalculationJobDetailView extends BasicVueComponent implements HasCreated, HasDestroyed {
  private static final CalculationJobDetailViewEventBinder EVENT_BINDER = GWT.create(CalculationJobDetailViewEventBinder.class);

  interface CalculationJobDetailViewEventBinder extends EventBinder<CalculationJobDetailView> {}

  @Prop EventBus eventBus;
  @Inject @Data CalculationPreparationContext prepContext;
  @Inject @Data ScenarioContext scenarioContext;
  @Inject @Data ApplicationContext applicationContext;

  private HandlerRegistration handlers;

  @Override
  public void created() {
    handlers = EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Override
  public void destroyed() {
    handlers.removeHandler();
  }

  @Computed("hasDetailJob")
  public boolean hasDetailJob() {
    return prepContext.getLooseSelection() != null;
  }

  @Computed("detailJob")
  public CalculationJobContext getDetailJob() {
    return prepContext.getLooseSelection();
  }

  @JsMethod
  protected List<String> getSimilars() {
    final String jobHash = CalculationJobContext.calculateJobHash(getDetailJob());
    return prepContext.getJobs().stream()
        .filter(v -> v != getDetailJob())
        .filter(v -> CalculationJobContext.calculateJobHash(v).equals(jobHash))
        .map(v -> v.getName())
        .collect(Collectors.toList());
  }

  @Computed("hasJobDuplicates")
  protected boolean hasJobDuplicates() {
    final String jobHash = CalculationJobContext.calculateJobHash(getDetailJob());
    return prepContext.getJobs().stream()
        .filter(v -> v != getDetailJob())
        .anyMatch(v -> CalculationJobContext.calculateJobHash(v).equals(jobHash));
  }

  @Computed("isCalculationSourceDiscrepancy")
  protected boolean isCalculationSourceDiscrepancy() {
    final CalculationExecutionContext calculation = prepContext.getActiveCalculation(getDetailJob());
    if (calculation == null) {
      return false;
    }

    return ScenarioContext.isCalculationStale(scenarioContext, getDetailJob(), calculation);
  }

  @Computed("situationComposition")
  public SituationComposition getSituationComposition() {
    return getDetailJob().getSituationComposition();
  }

  @Computed("calculationSetOptions")
  public CalculationSetOptions getCalculationSetOptions() {
    return getDetailJob().getCalculationOptions();
  }

  @Computed("isShowDevelopmentPressureSearch")
  protected boolean isShowDevelopmentPressureSearch() {
    return DevelopmentPressureUtil.expectsDevelopmentPressureSearch(applicationContext, getCalculationSetOptions().getCalculationJobType());
  }

  @Computed("theme")
  public Theme getTheme() {
    return applicationContext.getTheme();
  }

  @EventHandler
  public void onLeftPanelCloseEvent(final LeftPanelCloseEvent e) {
    close();
  }

  @JsMethod
  public void close() {
    eventBus.fireEvent(new CalculationJobDeselectCommand());
  }

}
