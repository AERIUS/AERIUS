/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.ui.pages.export.types;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.input.CheckBoxComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ExportContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.ui.pages.emissionsource.ErrorWarningIconComponent;
import nl.overheid.aerius.wui.application.ui.pages.export.metadata.ExportMetaDataComponent;
import nl.overheid.aerius.wui.application.ui.pages.export.types.components.OtherSituationCompositor;
import nl.overheid.aerius.wui.application.ui.pages.export.types.options.AdditionalOptionsComponent;
import nl.overheid.aerius.wui.config.ApplicationFlags;

@Component(components = {
    CheckBoxComponent.class,
    CollapsiblePanel.class,
    VerticalCollapse.class,
    LabeledInputComponent.class,
    ExportMetaDataComponent.class,
    ValidationBehaviour.class,
    ErrorWarningIconComponent.class,
    OtherSituationCompositor.class,
    AdditionalOptionsComponent.class,
})
public class ExportInputView extends ErrorWarningValidator {
  enum Panel {
    SITUATIONS, META_DATA,
  }

  @Prop boolean metaDataEnabled;

  @Inject @Data ScenarioContext scenarioContext;
  @Inject @Data ExportContext exportContext;
  @Inject @Data ApplicationFlags flags;

  @Data Panel openPanel = Panel.SITUATIONS;

  @JsMethod
  public boolean isOpen(final Panel panel) {
    return openPanel == panel;
  }

  @JsMethod
  public void openErrorPanel() {
    if (displayProblems) {
      if (isInvalid()) {
        openPanel = Panel.SITUATIONS;
      } else if (hasInvalidMetaData() || hasMetaDataWarning()) {
        openPanel = Panel.META_DATA;
      }
    }

    displayProblems(displayProblems);
  }

  @JsMethod
  public void openPanel(final Panel panel) {
    openPanel = panel;
  }

  @JsMethod
  public void closePanel() {
    openPanel = null;
  }

  @Computed
  public List<SituationContext> getSituations() {
    return scenarioContext.getSituations();
  }

  @Computed
  public SituationComposition getSituationComposition() {
    return exportContext.getSituationComposition();
  }

  @Computed
  public boolean getADMSExport() {
    return exportContext.getExportType() == ExportType.ADMS;
  }

  @Computed
  public void setADMSExport(final boolean checked) {
    setExportType(checked, ExportType.ADMS);
  }

  @Computed
  public boolean getOPSExport() {
    return exportContext.getExportType() == ExportType.OPS;
  }

  @Computed
  public void setOPSExport(final boolean checked) {
    setExportType(checked, ExportType.OPS);
  }

  private void setExportType(final boolean checked, final ExportType exportType) {
    if (checked) {
      exportContext.setExportType(exportType);
    } else if (exportContext.getExportType() == exportType) {
      exportContext.setExportType(null);
    }
  }

  @Computed
  public boolean isIncludeScenarioMetaData() {
    return exportContext.isIncludeScenarioMetaData();
  }

  public boolean hasInvalidMetaData() {
    return metaDataEnabled
        && exportContext.isIncludeScenarioMetaData()
        && hasChildError("metadata");
  }

  public boolean hasMetaDataWarning() {
    return metaDataEnabled
        && hasChildWarning("metadata");
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return hasInvalidMetaData();
  }

  @JsMethod
  public void refreshProblems(final boolean value) {
    displayProblems(value);
    openErrorPanel();
  }
}
