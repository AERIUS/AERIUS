/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source;

import static jsinterop.annotations.JsPackage.GLOBAL;

import ol.Feature;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.overheid.aerius.wui.application.domain.ImaerFeature;
import nl.overheid.aerius.wui.vue.ReactivityUtil;

/**
 * Base class Building Feature. This class extends openlayers {@link Feature}
 * class.
 *
 * NOTE: This class can't be implemented with java fields as the variables
 * should be in the Feature properties
 */
@JsType(namespace = GLOBAL, name = "ol.Feature", isNative = true)
public class BuildingFeature extends ImaerFeature {

  public final static @JsOverlay BuildingFeature create(final double defaultHeight) {
    final BuildingFeature feature = new BuildingFeature();
    init(feature, defaultHeight);
    return feature;
  }

  public final static @JsOverlay void init(final BuildingFeature feature, final double defaultHeight) {
    ImaerFeature.initImaer(feature);
    ReactivityUtil.ensureDefaultDouble(feature::getHeight, feature::setHeight, defaultHeight);
    ReactivityUtil.ensureDefault(feature::getDiameter, feature::setDiameter);
  }

  public final @JsOverlay Double getHeight() {
    return get("height") == null ? null : Js.coerceToDouble(get("height"));
  }

  public final @JsOverlay void setHeight(final Double height) {
    set("height", height == null ? null : height);
  }

  public final @JsOverlay Double getDiameter() {
    return get("diameter") == null ? null : Js.coerceToDouble(get("diameter"));
  }

  public final @JsOverlay void setDiameter(final Double diameter) {
    set("diameter", diameter == null ? null : diameter);
  }
}
