/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.error;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

/**
 * Representation of the response when Connect is returning an error.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class ConnectServerError {

  private String error;
  private String message;
  private String path;
  private int status;

  public final @JsOverlay String getError() {
    return error;
  }

  public final @JsOverlay String getMessage() {
    return message;
  }

  public final @JsOverlay String getPath() {
    return path;
  }

  public final @JsOverlay int getStatus() {
    return status;
  }

}
