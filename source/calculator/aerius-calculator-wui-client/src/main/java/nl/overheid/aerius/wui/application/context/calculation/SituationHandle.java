/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context.calculation;

import java.util.HashMap;
import java.util.Map;

import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.scenario.SituationType;

@JsType
public class SituationHandle {
  private String id = null;
  private String name = null;
  private SituationType type = null;
  private int sources = 0;
  private final @JsProperty Map<Substance, Boolean> hasEmmissions = new HashMap<>();

  public String getId() {
    return id;
  }

  public void setId(final String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public SituationType getType() {
    return type;
  }

  public void setType(final SituationType type) {
    this.type = type;
  }

  public int getSources() {
    return sources;
  }

  public void setSources(final int sources) {
    this.sources = sources;
  }

  public void putHasEmissions(final Substance substance, final boolean hasEmission) {
    hasEmmissions.put(substance, hasEmission);
  }

  public boolean isHasEmissions(final Substance substance) {
    return  hasEmmissions.getOrDefault(substance, Boolean.FALSE);
  }
}
