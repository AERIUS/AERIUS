/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.farm;

import static jsinterop.annotations.JsPackage.GLOBAL;

import elemental2.core.JsArray;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.wui.vue.ReactivityUtil;

/**
 * Client side implementation of StandardFarmLodging props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class StandardFarmLodging extends FarmLodging {
  private String farmLodgingCode;
  private String systemDefinitionCode;
  private JsArray<LodgingFodderMeasure> fodderMeasures;
  private JsArray<AdditionalLodgingSystem> additionalLodgingSystems;
  private JsArray<ReductiveLodgingSystem> reductiveLodgingSystems;

  public static final @JsOverlay StandardFarmLodging create() {
    final StandardFarmLodging props = Js.uncheckedCast(JsPropertyMap.of());
    init(props);
    return props;
  }

  public static final @JsOverlay void init(final StandardFarmLodging props) {
    FarmLodging.init(props, FarmLodgingType.STANDARD);
    ReactivityUtil.ensureJsProperty(props, "farmLodgingCode", props::setFarmLodgingCode, "");
    ReactivityUtil.ensureJsProperty(props, "systemDefinitionCode", props::setSystemDefinitionCode, "");

    ReactivityUtil.ensureJsPropertySupplied(props, "fodderMeasures", props::setFodderMeasures, () -> new JsArray<>());
    ReactivityUtil.ensureJsPropertySupplied(props, "additionalLodgingSystems", props::setAdditionalLodgingSystems, () -> new JsArray<>());
    ReactivityUtil.ensureJsPropertySupplied(props, "reductiveLodgingSystems", props::setReductiveLodgingSystems, () -> new JsArray<>());
  }

  public final @JsOverlay String getFarmLodgingCode() {
    return farmLodgingCode;
  }

  public final @JsOverlay void setFarmLodgingCode(final String farmLodgingCode) {
    this.farmLodgingCode = farmLodgingCode;
  }

  public final @JsOverlay String getSystemDefinitionCode() {
    return systemDefinitionCode;
  }

  public final @JsOverlay void setSystemDefinitionCode(final String systemDefinitionCode) {
    this.systemDefinitionCode = systemDefinitionCode;
  }

  public final @JsOverlay JsArray<LodgingFodderMeasure> getFodderMeasures() {
    return fodderMeasures;
  }

  public final @JsOverlay void setFodderMeasures(final JsArray<LodgingFodderMeasure> fodderMeasures) {
    this.fodderMeasures = fodderMeasures;
  }

  public final @JsOverlay JsArray<AdditionalLodgingSystem> getAdditionalLodgingSystems() {
    return additionalLodgingSystems;
  }

  public final @JsOverlay void setAdditionalLodgingSystems(final JsArray<AdditionalLodgingSystem> additionalLodgingSystems) {
    this.additionalLodgingSystems = additionalLodgingSystems;
  }

  public final @JsOverlay JsArray<ReductiveLodgingSystem> getReductiveLodgingSystems() {
    return reductiveLodgingSystems;
  }

  public final @JsOverlay void setReductiveLodgingSystems(final JsArray<ReductiveLodgingSystem> reductiveLodgingSystems) {
    this.reductiveLodgingSystems = reductiveLodgingSystems;
  }
}
