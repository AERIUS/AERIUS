/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.tooltip;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.Vue;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.google.gwt.user.client.DOM;

import elemental2.dom.ClientRect;
import elemental2.dom.DomGlobal;
import elemental2.dom.HTMLDivElement;

import nl.aerius.wui.util.SchedulerUtil;

@Component
public class TooltipComponent implements IsVueComponent {
  @Prop String title;
  @Prop boolean disabled;

  @Prop String id;

  @Data boolean showing;

  @Data String tooltipTop;
  @Data String tooltipLeft;

  @Ref HTMLDivElement container;
  @Ref HTMLDivElement tooltip;

  @PropDefault("id")
  String idDefault() {
    return DOM.createUniqueId();
  }

  @PropDefault("disabled")
  boolean disabledDefault() {
    return false;
  }

  @Watch("showing")
  public void onShowingChange(final boolean neww) {
    if (neww) {
      Vue.nextTick(() -> {
        final ClientRect containerRect = container.getBoundingClientRect();
        final ClientRect tooltipRect = tooltip.getBoundingClientRect();

        tooltipTop = containerRect.bottom + 3 + "px";
        tooltipLeft = Math.min(Math.max(0, containerRect.left + containerRect.width / 2 - tooltipRect.width / 2), DomGlobal.window.innerWidth) + "px";

        SchedulerUtil.repeatUntil(() -> {
          if (!showing) {
            return false;
          }

          if (container == null) {
            return false;
          }

          final boolean hovering = container.matches(":hover");
          if (!hovering) {
            showing = false;
          }

          return hovering;
        }, 50);
      });
    }
  }
}
