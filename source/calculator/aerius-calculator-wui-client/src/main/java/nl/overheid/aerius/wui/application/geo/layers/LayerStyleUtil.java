/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers;

import java.util.ArrayList;
import java.util.List;

import ol.Feature;
import ol.style.Icon;
import ol.style.IconOptions;
import ol.style.Style;
import ol.style.StyleOptions;

import nl.aerius.geo.icon.DynamicSvgIcon;
import nl.aerius.geo.wui.util.OL3GeometryUtil;
import nl.overheid.aerius.wui.application.geo.icons.labels.Label;
import nl.overheid.aerius.wui.application.geo.icons.labels.LabelIconStyle;
import nl.overheid.aerius.wui.application.geo.icons.labels.LabelSvgIconBuilder;
import nl.overheid.aerius.wui.application.util.LruCache;

/**
 * Util class to create styled labels.
 */
public class LayerStyleUtil {

  private static final LabelSvgIconBuilder LABEL_SVG_ICON_BUILDER = new LabelSvgIconBuilder();
  private static final LruCache<String, Style> LABEL_STYLE_CACHE = new LruCache<>(200);

  // Minimum number of pixels between source labels
  public static final int CLUSTER_MINIMUM_PIXEL_DISTANCE = 80;

  // Resolution below which to switch to labeling individual sources
  public static final double CLUSTER_MINIMUM_RESOLUTION = 1.5;

  private LayerStyleUtil() {
    // Util class.
  }

  /**
   * Creates  a Label give the feature, name and label style.
   *
   * @param feature feature feature to use geometry of
   * @param name name of the label
   * @param labelStyle label style
   * @return new Label object
   */
  public static Label createLabel(final Feature feature, final String name, final LabelStyle labelStyle) {
    return new Label(name, labelStyle.getLabelBackgroundColor(feature), labelStyle.getLabelTextColor());
  }

  /**
   * Creates a new styled Label.
   *
   * @param feature feature
   * @param name name of the label
   * @param labelStyle label style
   * @param hasOutline if true use outline on label
   * @param scale label icon scale
   * @return new style array
   */
  public static Style[] createStyle(final Feature feature, final String name, final LabelStyle labelStyle, final boolean hasOutline, final double scale) {
    final List<Label> labels = new ArrayList<>();

    labels.add(createLabel(feature, name, labelStyle));
    return createStyle(feature, labels, labelStyle.getLabelIconStyle(), hasOutline, scale);
  }

  /**
   * Creates a new Style for the given list of labels and use the middle point of the given geometry to position the styled label.
   *
   * @param feature feature to get the geometry from
   * @param labels list of labels
   * @param labelIconStyle icon stule
   * @param hasOutline if true use outline on label
   * @param scale label icon scale
   * @return new style array
   */
  public static Style[] createStyle(final Feature feature, final List<Label> labels, final LabelIconStyle labelIconStyle, final boolean hasOutline, final double scale) {
    final Style style = createStyle(labels, labelIconStyle, hasOutline, scale);

    style.setGeometry(OL3GeometryUtil.getMiddlePointOfGeometry(feature.getGeometry()));
    return new Style[] {style};
  }

  /**
   * Creates a new Style for the given list of labels.
   *
   * @param labels list of labels
   * @param labelIconStyle icon stule
   * @param hasOutline if true use outline on label
   * @param scale label icon scale
   * @return new style array
   */
  public static Style createStyle(final List<Label> labels, final LabelIconStyle labelIconStyle, final boolean hasOutline, final double scale) {
    final String styleKey = createStyleKey(labels, labelIconStyle, hasOutline, scale);
    return LABEL_STYLE_CACHE.computeIfAbsent(styleKey, key -> {

      final DynamicSvgIcon svg = LABEL_SVG_ICON_BUILDER.getSvgIcon(labels, hasOutline, labelIconStyle);
      final IconOptions iconOptions = new IconOptions();
      iconOptions.setSrc(svg.getSrc());
      iconOptions.setAnchor(svg.getAnchorInPx());
      iconOptions.setAnchorXUnits("pixels");
      iconOptions.setAnchorYUnits("pixels");
      iconOptions.setScale(scale);
      final Icon icon = new Icon(iconOptions);

      final StyleOptions styleOptions = new StyleOptions();
      styleOptions.setImage(icon);
      return new Style(styleOptions);
    });
  }

  /**
   * Constructs a key that should uniquely identify an icon, making sure the style is not re-created on panning / zooming
   *
   * @param labels list of labels
   * @param labelIconStyle icon stule
   * @param hasOutline if true use outline on label
   * @param scale label icon scale
   * @return unique key used for caching the style
   */
  private static String createStyleKey(final List<Label> labels, final LabelIconStyle labelIconStyle, final boolean hasOutline, final double scale) {
    final StringBuilder cacheKeyBuilder = new StringBuilder(labelIconStyle.name() + "." + hasOutline + "." + scale);
    for (final Label label : labels) {
      label.appendCacheKey(cacheKeyBuilder);
    }
    return cacheKeyBuilder.toString();
  }
}
