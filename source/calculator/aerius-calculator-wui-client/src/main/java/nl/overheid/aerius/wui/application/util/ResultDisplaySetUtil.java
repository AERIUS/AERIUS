/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.summary.OverlappingHexagonType;
import nl.overheid.aerius.shared.domain.summary.ProcurementPolicy;
import nl.overheid.aerius.shared.domain.summary.ResultDisplaySet;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationHandle;
import nl.overheid.aerius.wui.application.context.result.ResultSummaryContext;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.application.i18n.ApplicationMessages;
import nl.overheid.aerius.wui.application.i18n.M;

public final class ResultDisplaySetUtil {

  private ResultDisplaySetUtil() {}

  /**
   * Returns the content of the dropdown 'Display' or 'Weergave' in the results UI
   *
   * @param applicationContext
   * @param resultType
   * @param hexagonTypes
   * @param situations
   * @return the available display sets
   */
  public static List<ResultDisplaySet> getAvailableResultDisplaySets(final ApplicationContext applicationContext,
      final ScenarioResultType resultType, final List<SummaryHexagonType> hexagonTypes, final List<SituationHandle> situations) {
    final List<ResultDisplaySet> displaySets = new ArrayList<>();
    final Theme theme = applicationContext.getTheme();

    if (resultType == ScenarioResultType.DEPOSITION_SUM) {
      return getProcurementPolicies(theme);
    } else {
      for (final SummaryHexagonType hexagonType : hexagonTypes) {
        final List<OverlappingHexagonType> overlappingHexagonTypes = getOverlappingHexagonTypes(applicationContext, resultType, hexagonType,
            situations);
        for (final OverlappingHexagonType overlappingHexagonType : overlappingHexagonTypes) {
          displaySets.add(new ResultDisplaySet(hexagonType, overlappingHexagonType, null));
        }
      }
    }
    return displaySets;
  }

  private static List<ResultDisplaySet> getProcurementPolicies(final Theme theme) {
    return Arrays.stream(ProcurementPolicy.values())
        .filter(policy -> policy.getTheme() == theme)
        .map(policy -> new ResultDisplaySet(policy.getSummaryHexagonType(), policy.getOverlappingHexagonType(), policy))
        .collect(Collectors.toList());
  }

  private static List<OverlappingHexagonType> getOverlappingHexagonTypes(final ApplicationContext applicationContext,
      final ScenarioResultType resultType, final SummaryHexagonType hexagonType, final List<SituationHandle> situations) {
    final List<OverlappingHexagonType> overlappingHexagonTypes = new ArrayList<>();
    overlappingHexagonTypes.add(OverlappingHexagonType.ALL_HEXAGONS);

    if (applicationContext.isAppFlagEnabled(AppFlag.SHOWS_OVERLAPPING_EFFECTS)
        && hasOverlappingEffects(resultType, hexagonType, situations)) {
      overlappingHexagonTypes.add(OverlappingHexagonType.OVERLAPPING_HEXAGONS_ONLY);
      overlappingHexagonTypes.add(OverlappingHexagonType.NON_OVERLAPPING_HEXAGONS_ONLY);
    }

    return overlappingHexagonTypes;
  }

  private static boolean hasOverlappingEffects(final ScenarioResultType resultType, final SummaryHexagonType hexagonType,
      final List<SituationHandle> situations) {
    if (hexagonType == SummaryHexagonType.CUSTOM_CALCULATION_POINTS) {
      return false;
    }

    // only show on comparison calculation
    if (resultType != ScenarioResultType.PROJECT_CALCULATION && resultType != ScenarioResultType.MAX_TEMPORARY_EFFECT) {
      return false;
    }

    // only show if a reference situation is available
    return situations.stream().anyMatch(s -> s.getType() == SituationType.REFERENCE);
  }

  /**
   * Returns true if the scenario in the situationResultKey has emissions. If it has no emissions it can never have results.
   *
   * @param situationResultsKey The scenario to check
   * @return true if the scenario related to the emission substance has emissions
   */
  public static boolean couldHaveResults(final SituationResultsKey situationResultsKey) {
    final SituationHandle situationHandle = situationResultsKey.getSituationHandle();
    final Substance substance = situationResultsKey.getEmissionResultKey().getSubstance();

    if (substance == Substance.NOXNH3) {
      return situationHandle.isHasEmissions(Substance.NOX) || situationHandle.isHasEmissions(Substance.NH3);
    } else {
      return situationHandle.isHasEmissions(substance);
    }
  }

  /**
   * Returns the specific message to give when no results can or should be displayed.
   * Specifically for Archive or In-Combination projects a different message is given, depending on if there are emissions in the scenario,
   * or if there could be results, but it is an archive project with no results
   *
   * @param resultSummaryContext context to check
   * @return Specific message for the user on why there are no results
   */
  public static String noCalculationResultsText(final ResultSummaryContext resultSummaryContext) {
    final ApplicationMessages i18n = M.messages();
    final SituationResultsKey situationResultsKey = resultSummaryContext.getSituationResultsKey();
    final ScenarioResultType resultType = situationResultsKey.getResultType();
    final boolean archiveContribution = resultType == ScenarioResultType.ARCHIVE_CONTRIBUTION;

    if ((archiveContribution || resultType == ScenarioResultType.IN_COMBINATION) && !couldHaveResults(situationResultsKey)) {
      return i18n.resultsNoResultsCalculatedNoEmissionsIC();
    } else if (archiveContribution && couldHaveResults(situationResultsKey)) {
      return i18n.resultsNoResultsArchiveContribution();
    } else {
      return i18n.resultsNoResultsCalculated();
    }
  }
}
