/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.source.coldstart.standard;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.sector.category.SimpleCategory;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.InputWithUnitComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.coldstart.standard.ColdStartStandardVehicleTypeValidators.RoadStandardVehicleTypeValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.domain.source.road.ValuesPerColdStartVehicleTypes;
import nl.overheid.aerius.wui.vue.DebugDirective;

/**
 * Shows number of vehicles and stagnationfraction editor.
 */
@Component(customizeOptions = {
    ColdStartStandardVehicleTypeValidators.class
}, directives = {
    ValidateDirective.class,
    DebugDirective.class,
}, components = {
    LabeledInputComponent.class,
    InputWithUnitComponent.class,
    InputErrorComponent.class,
    ValidationBehaviour.class
})
public class ColdStartStandardVehicleTypeRowComponent extends ErrorWarningValidator implements IsVueComponent, HasCreated,
    HasValidators<RoadStandardVehicleTypeValidations> {
  @Prop SimpleCategory vehicleType;
  @Prop @JsProperty ValuesPerColdStartVehicleTypes valuesPerVehicleType;
  @Prop TimeUnit timeUnit;

  @Data String numberOfVehiclesV;

  @JsProperty(name = "$v") RoadStandardVehicleTypeValidations validations;

  @Watch(value = "valuesPerVehicleType", isImmediate = true)
  public void onvaluesPerVehicleTypeChange() {
    final String code = vehicleType.getCode();
    numberOfVehiclesV = Double.toString(valuesPerVehicleType.hasValuePerVehicleType(code) ? getNumberOfVehiclesByCode(vehicleType.getCode()) : 0D);
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Override
  @Computed
  public RoadStandardVehicleTypeValidations getV() {
    return validations;
  }

  @Computed
  protected String getNumberOfVehicles() {
    return numberOfVehiclesV;
  }

  @Computed
  protected void setNumberOfVehicles(final String number) {
    ValidationUtil.setSafeDoubleValue(numberOfVehicles -> {
      valuesPerVehicleType.setValuePerVehicleType(vehicleType.getCode(), numberOfVehicles);
    }, number, 0D);
    numberOfVehiclesV = number;
  }

  @Computed("isNumberOfVehiclesInvalid")
  protected boolean isNumberOfVehiclesInvalid() {
    return validations.numberOfVehiclesV.invalid;
  }

  @Computed("isInvalid")
  protected boolean isInvalid() {
    return isNumberOfVehiclesInvalid();
  }

  @JsMethod
  protected String errorNumberOfVehicles() {
    return i18n.errorNotValidEntry(numberOfVehiclesV);
  }

  private Double getNumberOfVehiclesByCode(final String code) {
    // FIXME Avoid setting a value when getting a value
    if (!valuesPerVehicleType.hasValuePerVehicleType(code)) {
      valuesPerVehicleType.setValuePerVehicleType(code, 0D);
    }
    return valuesPerVehicleType.getValuePerVehicleType(code);
  }
}
