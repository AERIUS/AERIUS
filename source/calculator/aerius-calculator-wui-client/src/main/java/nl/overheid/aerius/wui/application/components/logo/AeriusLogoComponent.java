/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.logo;

import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.google.gwt.resources.client.DataResource;

import nl.overheid.aerius.shared.domain.ProductProfile;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.daemon.misc.PersistablePreferencesContext;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component
public class AeriusLogoComponent extends BasicVueComponent {
  @Inject ApplicationContext context;
  @Inject PersistablePreferencesContext preferencesContext;

  @Computed
  public String getLogo() {
    return Optional.ofNullable(context.getTheme() == Theme.NCA ? null : logoNL())
        .map(v -> v.getSafeUri().asString())
        .orElse(null);
  }

  private DataResource logoNL() {
    DataResource logo;
    if (context.getProductProfile() == ProductProfile.LBV_POLICY) {
      logo = preferencesContext.isCollapsedMenu() ? img.logoCheck() : img.logoCheckWide();
    } else {
      logo = preferencesContext.isCollapsedMenu() ? img.logoCalculatorNarrow() : img.logoCalculatorWide();
    }
    return logo;
  }
}
