/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.characteristics.timevarying;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.characteristics.TimeVaryingProfiles;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileCreateNewCommand;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.ComplexifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.components.toggle.ToggleButtons;
import nl.overheid.aerius.wui.application.components.tooltip.TooltipComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.source.ADMSCharacteristics;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.TimeVaryingProfile;
import nl.overheid.aerius.wui.application.domain.source.util.TimeVaryingProfileUtilBase;

/**
 * Shows a list of which the user can pick the time-varying profiles available, or select Add new to create a new profile.
 * It also shows the graph of the profile.
 */
@Component(components = {
    LabeledInputComponent.class,
    VerticalCollapse.class,
    ComplexifiedListBoxComponent.class,
    TooltipComponent.class,
    ToggleButtons.class,
    ValidationBehaviour.class,
    InputErrorComponent.class,
})
public class TimeVaryingProfileSelectorComponent extends ErrorWarningValidator {

  static class Option {
    public final TimeVaryingProfileViewMode viewmode;
    public final String key;
    public final String value;

    public Option(final TimeVaryingProfileViewMode viewmode, final String key, final String value) {
      this.viewmode = viewmode;
      this.key = key;
      this.value = value;
    }

    @Override
    public String toString() {
      return "Option [viewmode=" + viewmode + ", key=" + key + ", value=" + value + "]";
    }
  }

  public enum TimeVaryingProfileOption {
    SECTOR_DEFAULT, CONTINUOUS;

    public static TimeVaryingProfileOption safeValueOf(final String value) {
      TimeVaryingProfileOption correct = null;
      if (value != null) {
        for (final TimeVaryingProfileOption type : values()) {
          if (type.name().equalsIgnoreCase(value)) {
            correct = type;
          }
        }
      }
      return correct;
    }
  }

  public enum TimeVaryingProfileViewMode {
    CUSTOM, STANDARD
  }

  @Prop EventBus eventBus;

  @Prop(required = true) SituationContext situation;
  @Prop(required = true) EmissionSourceFeature source;
  @Prop(required = true) TimeVaryingProfileUtilBase util;

  @Inject @Data ScenarioContext scenarioContext;
  @Inject @Data ApplicationContext applicationContext;

  private String selectedKey;

  @Computed
  public String getTimeVaryingProfileType() {
    final TimeVaryingProfile profile = getTimeVaryingProfile();

    return profile == null ? null : profile.getType().name();
  }

  @Computed
  public Double[] getTimeVaryingProfileData() {
    final TimeVaryingProfile profile = getTimeVaryingProfile();
    return profile.getValues().toArray(new Double[profile.getValues().size()]);
  }

  @Computed
  public TimeVaryingProfile getTimeVaryingProfile() {
    // If invalid, don't show the default (continuous)
    if (isInvalid()) {
      return null;
    }
    final TimeVaryingProfiles timeVaryingProfiles = applicationContext.getConfiguration().getSectorCategories().getTimeVaryingProfiles();

    return Optional.ofNullable(util.findTimeVaryingProfile(source, situation, timeVaryingProfiles))
        .orElse(util.getContinuousProfile());
  }

  @Computed
  public List<Option> getListOptions() {
    final List<Option> options = new ArrayList<>();
    // Custom options
    for (final TimeVaryingProfile profile : scenarioContext.getActiveSituation().getTimeVaryingProfiles()) {
      if (util.isType(profile.getType())) {
        options.add(new Option(TimeVaryingProfileViewMode.CUSTOM, profile.getGmlId(), profile.getLabel()));
      }
    }
    // Predefined profile options
    options.add(new Option(TimeVaryingProfileViewMode.STANDARD, TimeVaryingProfileOption.CONTINUOUS.name(),
        i18n.timeVaryingProfileStandardOption(TimeVaryingProfileOption.CONTINUOUS)));
    options.add(new Option(TimeVaryingProfileViewMode.STANDARD, TimeVaryingProfileOption.SECTOR_DEFAULT.name(),
        i18n.timeVaryingProfileStandardOption(TimeVaryingProfileOption.SECTOR_DEFAULT)));
    return options;
  }

  @Computed("groupFunction")
  public Function<Object, String> groupFunction() {
    return v -> v instanceof Option ? i18n.timeVaryingProfiles(((Option) v).viewmode) : "";
  }

  @Computed("keyFunction")
  public Function<Object, Object> keyFunction() {
    return v -> v instanceof Option ? createKey(((Option) v).viewmode, ((Option) v).key) : v;
  }

  @Computed("nameFunction")
  public Function<Object, String> nameFunction() {
    return v -> v instanceof Option ? ((Option) v).value : "";
  }

  @JsMethod
  public boolean checked(final Object v) {
    return selectedKey != null && v instanceof Option && selectedKey.equals(((Option) v).key);
  }

  @Computed
  public String getSelectedKey() {
    if (selectedKey == null) {
      final String cp = util.getCustomTimeVaryingProfileId(getCharacteristics());

      if (cp == null) {
        final String sp = util.getStandardTimeVaryingProfileCode(getCharacteristics());

        if (sp == null) {
          selectedKey = createKey(TimeVaryingProfileViewMode.STANDARD, TimeVaryingProfileOption.SECTOR_DEFAULT.name());
        } else {
          selectedKey = createKey(TimeVaryingProfileViewMode.STANDARD, sp);
        }
      } else {
        selectedKey = createKey(TimeVaryingProfileViewMode.CUSTOM, cp);
      }
    }
    return selectedKey;
  }

  private static String createKey(final TimeVaryingProfileViewMode mode, final String id) {
    return mode.name() + '-' + id;
  }

  @JsMethod
  public void selectNew(final Object v) {
    if ("CREATE_NEW".equals(v)) {
      selectedKey = null;
      eventBus.fireEvent(new TimeVaryingProfileCreateNewCommand(util.getType()));
    }
  }

  @JsMethod
  public void selectKey(final Object v) {
    if (v instanceof Option) {
      final Option option = (Option) v;
      final String key = option.key;

      if (option.viewmode == TimeVaryingProfileViewMode.STANDARD) {
        selectStandardProfile(TimeVaryingProfileOption.SECTOR_DEFAULT.name().equals(key) ? null : key);
      } else {
        selectCustomProfile(key);
      }
      selectedKey = createKey(option.viewmode, key);
    }
  }

  private void selectStandardProfile(final String key) {
    util.setCustomTimeVaryingProfileId(getCharacteristics(), null);
    util.setStandardTimeVaryingProfileCode(getCharacteristics(), key);
  }

  private void selectCustomProfile(final String profile) {
    util.setStandardTimeVaryingProfileCode(getCharacteristics(), null);
    util.setCustomTimeVaryingProfileId(getCharacteristics(), profile);
  }

  @Computed
  public String getLocale() {
    return LocaleInfo.getCurrentLocale().getLocaleName();
  }

  private ADMSCharacteristics getCharacteristics() {
    return (ADMSCharacteristics) source.getCharacteristics();
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    // Currently, the time-varying profile can never be invalid (null == sector default, no sector default == continuous)
    return false;
  }
}
