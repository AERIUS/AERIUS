/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.result;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.wui.application.domain.info.AssessmentArea;

@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class SituationResultsAreaSummary {

  private AssessmentArea assessmentArea;
  // Indication if the area is sensitive to the requested pollutant
  private boolean sensitive;
  // percentage critical load information
  private SurfaceChartResults[] percentageCriticalLoadChartResults;
  // emission result chart information
  private SurfaceChartResults[] emissionResultChartResults;
  // table information
  private SituationResultsStatistics statistics;
  // habitat information
  private SituationResultsHabitatSummary[] habitatSummaries;
  // individual receptor information
  private SituationResultsEdgeReceptor[] edgeReceptors;

  public final @JsOverlay AssessmentArea getAssessmentArea() {
    return assessmentArea;
  }

  public final @JsOverlay boolean isSensitive() {
    return sensitive;
  }

  public final @JsOverlay SurfaceChartResults[] getEmissionResultChartResults() {
    return emissionResultChartResults;
  }

  public final @JsOverlay SituationResultsStatistics getStatistics() {
    return statistics;
  }

  public final @JsOverlay SituationResultsHabitatSummary[] getHabitatSummaries() {
    return habitatSummaries;
  }

  public final @JsOverlay SurfaceChartResults[] getPercentageCriticalLoadResults() {
    return percentageCriticalLoadChartResults;
  }

  public final @JsOverlay SituationResultsEdgeReceptor[] getEdgeReceptors() {
    return edgeReceptors;
  }

}
