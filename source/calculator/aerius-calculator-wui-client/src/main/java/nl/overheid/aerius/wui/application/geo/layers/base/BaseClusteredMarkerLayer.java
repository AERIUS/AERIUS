/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.base;

import java.util.List;

import ol.geom.Point;
import ol.source.Cluster;
import ol.source.FullClusterOptions;
import ol.source.Source;
import ol.source.Vector;

import nl.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.wui.application.domain.ImaerFeature;
import nl.overheid.aerius.wui.application.geo.layers.LayerStyleUtil;

/**
 * Abstract base class to show clustered markers on the map.
 */
public abstract class BaseClusteredMarkerLayer<F extends ImaerFeature> extends BaseMarkerLayer<F> {

  protected BaseClusteredMarkerLayer(final LayerInfo info, final int zIndex, final LabelStyleCreator<F> labelStyleProvider) {
    super(info, zIndex, labelStyleProvider);
  }

  @Override
  public Source refreshLayer() {
    final Source source = super.refreshLayer();

    if (source instanceof Cluster) {
      final Vector clusterSource = ((Cluster) source).getSource();

      if (clusterSource != null) {
        clusterSource.changed();
      }
    }
    return source;
  }

  @Override
  protected void setFeatures(final VectorFeaturesWrapper vectorObject, final List<F> features) {
    vectorObject.setMapFunction(getLabelStyleProvider()::createLabelFeature);
    vectorObject.setFeatures(features);

    final FullClusterOptions clusterOptions = new FullClusterOptions();
    clusterOptions.setMinDistance(LayerStyleUtil.CLUSTER_MINIMUM_PIXEL_DISTANCE);
    clusterOptions.setDistance(LayerStyleUtil.CLUSTER_MINIMUM_PIXEL_DISTANCE);
    clusterOptions.setSource(vectorObject.getVector());
    clusterOptions.setGeometryFunction(feature -> {
      if (feature.getId().equals(getLabelStyleProvider().getEditableFeatureId())) {
        return null;
      }
      return (Point) feature.getGeometry();
    });
    final Cluster layer = new Cluster(clusterOptions);
    setLayerVector(vectorObject.setLayerVector(layer));
  }
}
