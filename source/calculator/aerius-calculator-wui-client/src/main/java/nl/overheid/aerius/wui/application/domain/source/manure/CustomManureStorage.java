/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.manure;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;
import nl.overheid.aerius.wui.application.domain.source.EmissionFactors;
import nl.overheid.aerius.wui.vue.ReactivityUtil;
import nl.overheid.aerius.wui.vue.SkipReactivityValidations;

/**
 * Client side implementation of CustomManureStorage props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class CustomManureStorage extends ManureStorage {

  private String description;
  private String animalCode;
  private String farmEmissionFactorType;
  private EmissionFactors emissionFactors;

  public static final @JsOverlay CustomManureStorage create() {
    final CustomManureStorage props = Js.uncheckedCast(JsPropertyMap.of());
    init(props);
    return props;
  }

  public static final @JsOverlay void init(final CustomManureStorage props) {
    ManureStorage.initManureStorage(props, ManureStorageType.CUSTOM);
    ReactivityUtil.ensureJsProperty(props, "description", props::setDescription, "");
    ReactivityUtil.ensureJsPropertyAsNull(props, "animalCode", props::setAnimalCode);
    ReactivityUtil.ensureJsProperty(props, "farmEmissionFactorType", props::setFarmEmissionFactorType, FarmEmissionFactorType.PER_TONNES_PER_YEAR);
    ReactivityUtil.ensureJsPropertySupplied(props, "emissionFactors", props::setEmissionFactors, EmissionFactors::create);
    ReactivityUtil.ensureInitialized(props::getEmissionFactors, EmissionFactors::init);
  }

  @JsOverlay
  public final String getDescription() {
    return description;
  }

  @JsOverlay
  public final void setDescription(final String description) {
    this.description = description;
  }

  @JsOverlay
  public final String getAnimalCode() {
    return animalCode;
  }

  @JsOverlay
  public final void setAnimalCode(final String animalCode) {
    this.animalCode = animalCode;
  }

  @JsOverlay
  public final FarmEmissionFactorType getFarmEmissionFactorType() {
    return FarmEmissionFactorType.valueOf(farmEmissionFactorType);
  }

  @JsOverlay
  public final void setFarmEmissionFactorType(final FarmEmissionFactorType farmEmissionFactorType) {
    this.farmEmissionFactorType = farmEmissionFactorType.name();
  }

  @JsOverlay
  @SkipReactivityValidations
  public final double getEmissionFactor(final Substance substance) {
    return emissionFactors.getEmissionFactor(substance);
  }

  @JsOverlay
  public final void setEmissionFactor(final Substance substance, final double emission) {
    emissionFactors.setEmissionFactor(substance, emission);
  }

  @JsOverlay
  public final EmissionFactors getEmissionFactors() {
    return emissionFactors;
  }

  @JsOverlay
  public final void setEmissionFactors(final EmissionFactors emissionFactors) {
    this.emissionFactors = emissionFactors;
  }

}
