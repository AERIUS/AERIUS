/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.print.nca.overview;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.wui.application.context.PrintContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.print.common.PrintSection;
import nl.overheid.aerius.wui.application.print.common.scenario.PrintScenarioSummarizedBuildingsComponent;
import nl.overheid.aerius.wui.application.print.common.scenario.PrintScenarioSummarizedSourcesComponent;
import nl.overheid.aerius.wui.application.print.common.scenario.PrintScenarioTimeVaryingProfilesComponent;
import nl.overheid.aerius.wui.application.print.common.scenario.details.PrintScenarioBuildingsComponent;
import nl.overheid.aerius.wui.application.print.common.scenario.details.PrintScenarioSourcesComponent;
import nl.overheid.aerius.wui.vue.BasicVueComponent;
import nl.overheid.aerius.wui.vue.StyleDirective;

@Component(components = {
    PrintSection.class,
    PrintScenarioSummarizedSourcesComponent.class,
    PrintScenarioSummarizedBuildingsComponent.class,
    PrintScenarioTimeVaryingProfilesComponent.class,
    PrintScenarioBuildingsComponent.class,
    PrintScenarioSourcesComponent.class,
}, directives = {
    StyleDirective.class,
})
public class NcaScenarioOverviewComponent extends BasicVueComponent {
  @Prop EventBus eventBus;
  @Inject @Data PrintContext context;

  @Inject @Data ScenarioContext scenarioContext;

  @JsMethod
  public List<String> getLongDummyContent(final int paragraphs) {
    final String baseParagraph = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n" +
        "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. \n" +
        "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \n" +
        "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \n" +
        "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
    final List<String> lst = new ArrayList<>();
    for (int i = 0; i < paragraphs; i++) {
      lst.add(baseParagraph);
    }
    return lst;
  }
}
