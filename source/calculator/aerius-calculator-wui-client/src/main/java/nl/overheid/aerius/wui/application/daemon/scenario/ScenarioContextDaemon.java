/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.scenario;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import jsinterop.base.Js;

import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.place.PlaceController;
import nl.aerius.wui.util.NotificationUtil;
import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.command.RestoreScenarioContextCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingAddCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingCreateNewCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingDeleteCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingDuplicateSelectedCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobAddCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationPointDeleteAllCommand;
import nl.overheid.aerius.wui.application.command.calculation.RestoreCalculationJobContextCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointAddCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointsAddCommand;
import nl.overheid.aerius.wui.application.command.situation.SituationChangeNettingFactorCommand;
import nl.overheid.aerius.wui.application.command.situation.SituationChangeTypeCommand;
import nl.overheid.aerius.wui.application.command.situation.SituationChangeYearCommand;
import nl.overheid.aerius.wui.application.command.situation.SituationDeleteCommand;
import nl.overheid.aerius.wui.application.command.situation.SituationRenameCommand;
import nl.overheid.aerius.wui.application.command.situation.SwitchSituationCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceAddCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceDeleteCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileAddCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileDeleteCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ExportContext;
import nl.overheid.aerius.wui.application.context.PrintContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationPreparationContext;
import nl.overheid.aerius.wui.application.domain.ClientConsecutiveIdUtil;
import nl.overheid.aerius.wui.application.domain.ClientGmlIdUtil;
import nl.overheid.aerius.wui.application.domain.geo.CalculationPointUtil;
import nl.overheid.aerius.wui.application.domain.importer.ImportParcelConverter;
import nl.overheid.aerius.wui.application.domain.importer.ImportParcelSituation;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.InlandShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.MaritimeShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.TimeVaryingProfile;
import nl.overheid.aerius.wui.application.event.ImportCompleteEvent;
import nl.overheid.aerius.wui.application.event.RestoreScenarioContextEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingListChangeEvent;
import nl.overheid.aerius.wui.application.event.calculationpoint.CalculationPointsAddedEvent;
import nl.overheid.aerius.wui.application.event.situation.SituationSwitchEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceListChangeEvent;
import nl.overheid.aerius.wui.application.i18n.ApplicationMessages;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.place.CalculationPointsPlace;
import nl.overheid.aerius.wui.application.place.HomePlace;
import nl.overheid.aerius.wui.application.place.ScenarioInputListPlace;
import nl.overheid.aerius.wui.application.service.ScenarioServiceAsync;

@Singleton
public class ScenarioContextDaemon extends BasicEventComponent implements Daemon {
  private static final ScenarioContextDaemonEventBinder EVENT_BINDER = GWT.create(ScenarioContextDaemonEventBinder.class);

  interface ScenarioContextDaemonEventBinder extends EventBinder<ScenarioContextDaemon> {}

  private static final String SOURCE_PREFIX = "ES.";
  private static final String BUILDING_PREFIX = "Building.";
  private static final String TIME_VARYING_PROFILE_PREFIX = "TimeVaryingProfile.";

  private @Inject PlaceController placeController;
  private @Inject ScenarioServiceAsync service;
  private @Inject ScenarioContext scenarioContext;
  private @Inject ExportContext exportContext;
  private @Inject ApplicationContext applicationContext;
  private @Inject ImportParcelConverter converter;
  private @Inject CalculationPreparationContext prepContext;
  private @Inject SituationDaemon situationDaemon;
  private @Inject ApplicationMessages i18n;

  @EventHandler
  public void onSwitchSituationCommand(final SwitchSituationCommand c) {
    final SituationContext situation = c.getValue();
    c.setSilent(!scenarioContext.setActiveSituationId(Optional.ofNullable(situation)
        .map(v -> v.getId())
        .orElse(null)));
  }

  @EventHandler
  public void onSituationSwitchEvent(final SituationSwitchEvent e) {
    final SituationContext situation = e.getValue();
    if (situation == null) {
      return;
    }
    refreshInputLists(situation);
  }

  private void refreshInputLists(final SituationContext situation) {
    eventBus.fireEvent(new EmissionSourceListChangeEvent(situation.getSources(),
        situation.getId()));
    eventBus.fireEvent(new BuildingListChangeEvent(situation.getBuildings(),
        situation.getId()));
  }

  @EventHandler
  public void onSituationRenameCommand(final SituationRenameCommand c) {
    c.getValue().setName(c.getNewName());
  }

  @EventHandler
  public void onSituationChangeTypeCommand(final SituationChangeTypeCommand c) {
    SituationContext.safeSetSituationType(c.getValue(), c.getNewType());
  }

  @EventHandler
  public void onSituationChangeYearCommand(final SituationChangeYearCommand c) {
    c.getValue().setYear(c.getNewYear());
  }

  @EventHandler
  public void onSituationChangeNettingFactorCommand(final SituationChangeNettingFactorCommand c) {
    c.getValue().setNettingFactor(c.getNewNettingFactor());
  }

  @EventHandler
  public void onSituationDeleteCommand(final SituationDeleteCommand c) {
    c.setSilent(!scenarioContext.deleteSituation(c.getValue()));
  }

  @EventHandler
  public void onTimeVaryingProfileAddCommand(final TimeVaryingProfileAddCommand c) {
    final SituationContext situation = c.getSituation() == null ? scenarioContext.getActiveSituation() : c.getSituation();
    final List<TimeVaryingProfile> timeVaryingProfiles = situation.getOriginalTimeVaryingProfiles();

    c.setSituation(situation);

    final TimeVaryingProfile profile = c.getValue();
    ClientGmlIdUtil.setGmlId(timeVaryingProfiles, profile, TIME_VARYING_PROFILE_PREFIX);
    ClientConsecutiveIdUtil.addAndSetId(timeVaryingProfiles, profile);
  }

  @EventHandler
  public void onTimeVaryingProfileDeleteCommand(final TimeVaryingProfileDeleteCommand c) {
    final SituationContext situation = c.getSituation() == null ? scenarioContext.getActiveSituation() : c.getSituation();

    // First, unlink the time-varying profile entity
    ScenarioContext.cleanTimeVaryingProfileLinkedSources(scenarioContext, situation.getId(), c.getValue().getGmlId());

    // Then, remove the time-varying profile entity
    c.setSituation(situation);
    final List<TimeVaryingProfile> profiles = situation.getOriginalTimeVaryingProfiles();
    c.setSilent(!profiles.remove(c.getValue()));
  }

  @EventHandler(handles = {BuildingCreateNewCommand.class, BuildingDuplicateSelectedCommand.class})
  public void onBuildingCreateNewCommand() {
    final List<BuildingFeature> buildings = scenarioContext.getActiveSituation().getOriginalBuildings();

    final int maximumBuildings = applicationContext.getConfiguration().getBuildingLimits().buildingMaximumPerSituation();
    if (buildings.size() >= maximumBuildings) {
      NotificationUtil.broadcastWarning(eventBus, i18n.buildingSituationLimit(maximumBuildings));
    }
  }

  @EventHandler
  public void onBuildingAddCommand(final BuildingAddCommand c) {
    final SituationContext situation = c.getSituation() == null ? scenarioContext.getActiveSituation() : c.getSituation();
    final List<BuildingFeature> buildings = situation.getOriginalBuildings();

    c.setSituation(situation);

    final BuildingFeature feature = c.getValue();
    ClientGmlIdUtil.setGmlId(buildings, feature, BUILDING_PREFIX);
    ClientConsecutiveIdUtil.addAndSetId(buildings, feature);
  }

  @EventHandler
  public void onBuildingDeleteCommand(final BuildingDeleteCommand c) {
    final SituationContext situation = c.getSituation() == null ? scenarioContext.getActiveSituation() : c.getSituation();

    // First, unlink the building entity
    ScenarioContext.findBuildingLinkedSources(scenarioContext, situation.getId(), c.getValue().getGmlId()).stream()
        .forEach(v -> v.getCharacteristics().setBuildingGmlId(null));

    // Then, remove the building entity
    c.setSituation(situation);
    final List<BuildingFeature> buildings = situation.getOriginalBuildings();
    c.setSilent(!buildings.remove(c.getValue()));
  }

  @EventHandler
  public void onEmissionSourceAddCommand(final EmissionSourceAddCommand c) {
    final SituationContext situation = c.getSituation() == null ? scenarioContext.getActiveSituation() : c.getSituation();
    final List<EmissionSourceFeature> sources = situation.getOriginalSources();

    c.setSituation(situation);
    final EmissionSourceFeature sourceToAdd = c.getValue();
    if (sourceToAdd.getGmlId() == null || "".equals(sourceToAdd.getGmlId())) {
      ClientGmlIdUtil.setGmlId(sources, sourceToAdd, SOURCE_PREFIX);
    }
    ClientConsecutiveIdUtil.addAndSetId(sources, sourceToAdd);
  }

  @EventHandler
  public void onEmissionSourceDeleteCommand(final EmissionSourceDeleteCommand c) {
    final SituationContext situation = c.getSituation() == null ? scenarioContext.getActiveSituation() : c.getSituation();
    final List<EmissionSourceFeature> sources = situation.getOriginalSources();
    final EmissionSourceFeature feature = c.getValue();
    boolean success = false;

    if (EmissionSourceType.SHIPPING_INLAND_DOCKED == feature.getEmissionSourceType()) {
      success = tryRemoveInlandDockingId(feature, sources);
    } else if (EmissionSourceType.SHIPPING_MARITIME_DOCKED == feature.getEmissionSourceType()) {
      success = tryRemoveMaritimeDockingId(feature, sources);
    } else {
      success = true;
      // no-op
    }

    if (success) {
      removeDevelopmentPressureSearchSourceId(situation, feature.getGmlId());
      c.setSituation(situation);
      c.setSilent(!sources.remove(c.getValue()));
    } else {
      c.setSilent(true);
    }
  }

  @EventHandler
  public void onImportCompleteEvent(final ImportCompleteEvent e) {
    if (!scenarioContext.hasActiveSituation()) {
      if (scenarioContext.hasSituations()) {
        final SituationContext firstSituation = scenarioContext.getSituations().iterator().next();
        eventBus.fireEvent(new SwitchSituationCommand(firstSituation));
      }
    } else {
      refreshInputLists(scenarioContext.getActiveSituation());
    }

    if (placeController.getPlace() instanceof HomePlace) {
      if (scenarioContext.hasSituations()) {
        placeController.goTo(new ScenarioInputListPlace(applicationContext.getTheme()));
      } else if (!scenarioContext.getCalculationPoints().isEmpty()) {
        placeController.goTo(new CalculationPointsPlace(applicationContext.getTheme()));
      }
    }
  }

  @EventHandler
  public void onRestoreScenarioContextCommand(final RestoreScenarioContextCommand c) {
    c.silence();
    service.importScenario(c.getValue(), true, propertyMap -> {
      exportContext.setScenarioMetaData(Js.cast(propertyMap.get("metaData")));
      final ImportParcelSituation[] situations = Js.cast(propertyMap.get("situations"));
      // For print jobs the situation id is passed in the url, but for reloading calculations it's not.
      final String commandSituationId = c.getSituationId();

      for (final ImportParcelSituation situation : situations) {
        if (commandSituationId.isEmpty()) {
          // Reload job in the UI
          final SituationContext sc = situationDaemon
              .createAndAddSituation(newSituation -> converter.createSituationFromParcel(situation, newSituation));

          // Set the situation id so the user will see a scenario selected when switching to the sources lists page.
          c.setSituationId(sc.getId());
        } else {
          // Load job for printing to pdf
          if (isRelevantForOwN2000Export(situation, commandSituationId)) {
            final SituationContext situationContext = converter.createSituationFromParcel(situation,
                new SituationContext(String.valueOf(scenarioContext.getSituations().size())));

            situationContext.replaceId(situation.getId());
            scenarioContext.addSituation(scenarioContext.getSituations().size(), situationContext);
          }
        }
      }

      final CalculationPointFeature[] calculationPoints = converter.getCalculationPoints(propertyMap.get("customPoints"));
      if (calculationPoints.length > 0) {
        eventBus.fireEvent(new CalculationPointsAddCommand(Arrays.asList(calculationPoints)));
      }

      eventBus.fireEvent(c.getEvent());
    });
  }

  @EventHandler
  public void onRestoreScenarioContextEvent(final RestoreScenarioContextEvent e) {
    final SituationContext situation = scenarioContext.getSituation(e.getSituationId());

    // Set active situation to the proposed situation this Project Calculation PDF is rendering.
    // Primary purpose is showing the right markers on the result summary map.
    eventBus.fireEvent(new SwitchSituationCommand(situation));

    eventBus.fireEvent(new CalculationJobAddCommand(true));

    SchedulerUtil.delay(() -> eventBus.fireEvent(new RestoreCalculationJobContextCommand(prepContext.getActiveJob(), e.getValue())), 1);
  }

  @EventHandler
  public void onDeleteAllCalculationPointsCommand(final CalculationPointDeleteAllCommand c) {
    scenarioContext.getCalculationPoints().clear();
  }

  @EventHandler
  public void onAddCalculationPointsCommand(final CalculationPointsAddCommand c) {
    c.silence();
    final List<CalculationPointFeature> addedCalculationPoints = scenarioContext.addCalculationPointsIgnoreOverlapping(c.getValue());
    if (addedCalculationPoints.size() < c.getValue().size()) {
      NotificationUtil.broadcastMessage(eventBus, M.messages().importCalculationPointDuplicatesMessage());
    }
    eventBus.fireEvent(new CalculationPointsAddedEvent(addedCalculationPoints));
  }

  @EventHandler
  public void onCalculationPointAddCommand(final CalculationPointAddCommand c) {
    CalculationPointUtil.addAndSetId(scenarioContext.getCalculationPoints(), c.getValue());
  }

  private static boolean isRelevantForOwN2000Export(final ImportParcelSituation situation, final String situationId) {
    if (situation.getType() == SituationType.PROPOSED && !situation.getId().equals(situationId)) {
      return false;
    }
    return PrintContext.PROJECT_CALCULATION_SITUATION_TYPES.contains(situation.getType());
  }

  private static boolean tryRemoveInlandDockingId(final EmissionSourceFeature dockingFeature, final List<EmissionSourceFeature> sources) {
    final String dockingId = dockingFeature.getGmlId();

    // List of labels of inlandShipping routing sources that have a dock connected to the given dockingFeature
    final List<String> inlandShippingSources = sources.stream()
        .filter(feature -> feature.getEmissionSourceType() == EmissionSourceType.SHIPPING_INLAND)
        .map(shipping -> (InlandShippingESFeature) shipping)
        .filter(shipping -> dockingId.equals(shipping.getMooringAId()) || dockingId.equals(shipping.getMooringBId()))
        .map(InlandShippingESFeature::getLabel)
        .collect(Collectors.toList());

    final String deleteConfirm = M.messages().esShipRoutingDeleteConfirm(String.join(", ", inlandShippingSources));
    if (!inlandShippingSources.isEmpty() && !Window.confirm(deleteConfirm)) {
      return false;
    }

    removeInlandMooringID(dockingId, sources);
    return true;
  }

  private static void removeInlandMooringID(final String dockingId, final List<EmissionSourceFeature> sources) {
    for (final EmissionSourceFeature feature : sources) {
      if (EmissionSourceType.SHIPPING_INLAND == feature.getEmissionSourceType()) {
        final InlandShippingESFeature shipping = (InlandShippingESFeature) feature;
        if (dockingId.equals(shipping.getMooringAId())) {
          shipping.setMooringAId(null);
        } else if (dockingId.equals(shipping.getMooringBId())) {
          shipping.setMooringBId(null);
        } else {
          // no-op
        }
      }
    }
  }

  private static boolean tryRemoveMaritimeDockingId(final EmissionSourceFeature dockingFeature, final List<EmissionSourceFeature> sources) {
    final String dockingId = dockingFeature.getGmlId();

    // List of labels of maritimeShipping routing sources that have a dock connected to the given dockingFeature
    final List<String> maritimeShippingSources = sources.stream()
        .filter(feature -> feature.getEmissionSourceType() == EmissionSourceType.SHIPPING_MARITIME_INLAND
            || feature.getEmissionSourceType() == EmissionSourceType.SHIPPING_MARITIME_MARITIME)
        .map(shipping -> (MaritimeShippingESFeature) shipping)
        .filter(shipping -> dockingId.equals(shipping.getMooringAId()) || dockingId.equals(shipping.getMooringBId()))
        .map(MaritimeShippingESFeature::getLabel)
        .collect(Collectors.toList());

    final String deleteConfirm = M.messages().esShipRoutingDeleteConfirm(String.join(", ", maritimeShippingSources));
    if (!maritimeShippingSources.isEmpty() && !Window.confirm(deleteConfirm)) {
      return false;
    }

    removeMaritimeMooringID(dockingId, sources);
    return true;
  }

  private static void removeMaritimeMooringID(final String dockingId, final List<EmissionSourceFeature> sources) {
    for (final EmissionSourceFeature feature : sources) {
      if (EmissionSourceType.SHIPPING_MARITIME_INLAND == feature.getEmissionSourceType()
          || EmissionSourceType.SHIPPING_MARITIME_MARITIME == feature.getEmissionSourceType()) {
        final MaritimeShippingESFeature shipping = (MaritimeShippingESFeature) feature;
        if (dockingId.equals(shipping.getMooringAId())) {
          shipping.setMooringAId(null);
        } else if (dockingId.equals(shipping.getMooringBId())) {
          shipping.setMooringBId(null);
        } else {
          // no-op
        }
      }
    }
  }

  private void removeDevelopmentPressureSearchSourceId(final SituationContext situation, final String idToRemove) {
    if (applicationContext.getTheme() == Theme.NCA) {
      for (final CalculationJobContext job : prepContext.getJobs()) {
        // If the job has the correct situation as the proposed one,
        // and the job has the idToRemove in its list of development pressure sources,
        // then remove the source from that list.
        if (job.getSituationComposition().getSituation(SituationType.PROPOSED) == situation
            && job.getCalculationOptions().getNcaCalculationOptions().getDevelopmentPressureSourceIds().contains(idToRemove)) {
          final List<String> developmentPressureSourceIds = new ArrayList<>(
              job.getCalculationOptions().getNcaCalculationOptions().getDevelopmentPressureSourceIds());
          developmentPressureSourceIds.remove(idToRemove);
          job.getCalculationOptions().getNcaCalculationOptions().setDevelopmentPressureSourceIds(developmentPressureSourceIds);
        }
      }
    }
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
