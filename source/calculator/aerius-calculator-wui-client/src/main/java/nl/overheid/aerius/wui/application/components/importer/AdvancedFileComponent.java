/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.importer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Emit;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.HorizontalCollapse;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsibleButton;
import nl.overheid.aerius.wui.application.components.importer.content.InlineFileSettings;
import nl.overheid.aerius.wui.application.components.importer.content.SingleFileContentsComponent;
import nl.overheid.aerius.wui.application.components.importer.domain.ImportAction;
import nl.overheid.aerius.wui.application.components.importer.domain.ImportAction.ActionType;
import nl.overheid.aerius.wui.application.components.importer.domain.VirtualSituation;
import nl.overheid.aerius.wui.application.components.input.drop.ComplexifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.components.tooltip.TooltipComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.importer.FileContext;
import nl.overheid.aerius.wui.application.context.importer.ImportContext;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;

@Component(components = {
    VerticalCollapse.class,
    SimplifiedListBoxComponent.class,
    ComplexifiedListBoxComponent.class,
    CollapsibleButton.class,
    InlineFileSettings.class,
    HorizontalCollapse.class,
    TooltipComponent.class,
    ButtonIcon.class,
    ValidationBehaviour.class,
    FileUploadComponent.class,
    SingleFileContentsComponent.class,
})
public class AdvancedFileComponent extends ErrorWarningValidator {
  @Prop FileUploadStatus file;
  @Prop EventBus eventBus;

  @Data boolean contentVisible = false;

  @Data @Inject ApplicationContext applicationContext;
  @Data @Inject ScenarioContext scenarioContext;
  @Data @Inject FileContext fileContext;
  @Data @Inject ImportContext importContext;

  @Computed("canHaveSituationSelection")
  public boolean canHaveSituationSelection() {
    final ImportAction action = file.getImportAction();
    return action != null
        && action.getType() == ActionType.ADD_TO_VIRTUAL_SITUATION
        && VirtualSituation.isSameFileHandle(action.getVirtualSituation(), file);
  }

  @Computed
  public List<SituationType> getSituationOptions() {
    return applicationContext.getConfiguration().getAvailableSituationTypes();
  }

  @Computed
  public SituationType getSituationType() {
    return Optional.ofNullable(file.getImportAction())
        .map(v -> v.getVirtualSituation())
        .map(v -> v.getSituationType())
        .filter(v -> v != SituationType.UNKNOWN)
        .orElse(SituationType.PROPOSED);
  }

  @Computed
  public SituationType getFinalSituationType() {
    return Optional.ofNullable(file.getImportAction())
        .map(v -> v.getVirtualSituation())
        .map(v -> v.getSituationType())
        .filter(v -> v != SituationType.UNKNOWN)
        .orElse(Optional.ofNullable(file.getImportAction())
            .map(v -> v.getExistingSituation())
            .map(v -> v.getType())
            .orElse(null));
  }

  @JsMethod
  public void selectFileSituationType(final SituationType type) {
    file.getImportAction().getVirtualSituation().setSituationType(type);
  }

  @JsMethod
  public boolean hasContent(final FileUploadStatus file) {
    return file.getErrors().isEmpty()
        && file.getSituationStats() != null;
  }

  @JsMethod
  public void toggleContent() {
    if (hasContent(file)) {
      contentVisible = !contentVisible;
    }
  }

  @JsMethod
  public boolean hasFileStats(final FileUploadStatus file) {
    return file.getSituationStats() != null;
  }

  @Computed("isUserInput")
  public boolean isUserInput() {
    return file.isUserInput();
  }

  @Computed("canHaveFileAction")
  public boolean canHaveFileAction() {
    return file.isComplete() && file.isValidated();
  }

  @Computed
  public String getLabelFromType(final ImportAction action) {
    String group;

    switch (action.getType()) {
    case ADD_NEW:
      group = null;
      break;
    case ADD_TO_EXISTING_SITUATION:
      group = M.messages().importActionGroupAddToExisting();
      break;
    case ADD_TO_VIRTUAL_SITUATION:
      final VirtualSituation virtualSituation = action.getVirtualSituation();
      if (VirtualSituation.isSameFileHandle(virtualSituation, file)) {
        group = null;
      } else {
        group = M.messages().importActionGroupAddToVirtual();
      }
      break;
    case IGNORE:
    case ADD_ARCHIVE_CONTRIBUTION:
    case ADD_CALCULATION_POINTS:
      group = M.messages().importActionGroupMisc();
      break;
    default:
      group = null;
    }

    return group;
  }

  @Computed("importActionToKey")
  public Function<Object, Object> importActionToKey() {
    return v -> {
      final ImportAction importAction = (ImportAction) v;
      return importAction.getType() + "_"
          + (importAction.getExistingSituation() == null ? "null" : importAction.getExistingSituation().getId()) + "_"
          + (importAction.getVirtualSituation() == null ? "null" : importAction.getVirtualSituation().getId());
    };
  }

  @Computed
  public List<ImportAction> getImportActions() {
    final List<ImportAction> actions = new ArrayList<>();

    if (!file.isFailed() && !file.hasErrors() && file.getSituationStats() != null) {
      if (file.getSituationStats().isArchive()) {
        actions.add(ImportAction.addArchive());
      } else if (file.getSituationStats().getCalculationPoints() == 0) {
        final List<ImportAction> mergers = scenarioContext.getSituations().stream()
            .map(v -> ImportAction.mergeIntoExistingSituation(v))
            .collect(Collectors.toList());
        final List<ImportAction> acquisitions = importContext.getVirtualSituations().stream()
            .map(v -> ImportAction.mergeIntoVirtualSituation(v))
            .collect(Collectors.toList());

        final ImportAction currentAction = file.getImportAction();
        if (canSelectAddNewAction(currentAction)) {
          actions.add(ImportAction.addNew());
        }

        actions.addAll(mergers);
        actions.addAll(acquisitions);
      } else {
        actions.add(ImportAction.addCalculationPoints());
      }
    }

    actions.add(ImportAction.ignore());

    // Sort so that the "is same file handle" always appears first
    actions.sort((o1, o2) -> {
      final boolean o1Same = o1.getVirtualSituation() != null && VirtualSituation.isSameFileHandle(o1.getVirtualSituation(), file);
      final boolean o2Same = o2.getVirtualSituation() != null && VirtualSituation.isSameFileHandle(o2.getVirtualSituation(), file);

      return Boolean.compare(o2Same, o1Same);
    });

    return actions;
  }

  /**
   * <pre>
   * If there is no current action,
   * or if the current action is not to add it to a virtual situation,
   * or if the current action's virtual situation is not the original file handle
   * return true
   * </pre>
   *
   * In a nutshell, "add new" is not enabled if the present configuration already accomplishes the same thing as the "add new" action would
   */
  private boolean canSelectAddNewAction(final ImportAction currentAction) {
    return currentAction == null
        || currentAction.getType() != ActionType.ADD_TO_VIRTUAL_SITUATION
        || !VirtualSituation.isSameFileHandle(currentAction.getVirtualSituation(), file);
  }

  @JsMethod
  public String formatImportActionLabel(final ImportAction action) {
    String label;
    switch (action.getType()) {
    case ADD_TO_EXISTING_SITUATION:
      label = M.messages().importAddToExisting(action.getExistingSituation().getName());
      break;
    case ADD_TO_VIRTUAL_SITUATION:
      final VirtualSituation virtualSituation = action.getVirtualSituation();
      if (VirtualSituation.isSameFileHandle(virtualSituation, file)) {
        label = M.messages().importActionAddAsNew();
      } else {
        label = M.messages().importAddToVirtual(virtualSituation.getName());
      }
      break;
    case ADD_ARCHIVE_CONTRIBUTION:
      label = M.messages().importAddArchive();
      break;
    case ADD_CALCULATION_POINTS:
      label = M.messages().importAddCalculationPoints();
      break;
    case ADD_NEW:
      label = M.messages().importActionAddAsNew();
      break;
    case IGNORE:
      label = M.messages().importIgnore();
      break;
    default:
      throw new RuntimeException("Unknown action type: " + action.getType());
    }

    return label;
  }

  @JsMethod
  public void selectImportAction(final ImportAction action) {
    eventBus.fireEvent(new ImportActionSelectCommand(action, file));
  }

  @Computed
  public ImportAction getSelectedImportAction() {
    return file.getImportAction();
  }

  @Computed
  protected String getSelectedFile() {
    return file.getFileCode();
  }

  @JsMethod
  public String getIconForFile(final FileUploadStatus file) {
    return FileUploadStatus.getIconForFile(file);
  }

  @Computed
  public boolean hasInvalidAction() {
    return true;
  }

  @Emit
  @JsMethod
  public void displayErrors() {}

  @Emit
  @JsMethod
  public void displayWarnings() {}

  @JsMethod
  public void removeFile() {
    vue().$emit("remove-file", file);
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return file.getImportAction() == null
        || hasChildErrors();
  }
}
