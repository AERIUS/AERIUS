/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.misc;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import elemental2.dom.DomGlobal;
import elemental2.dom.Element;

import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.event.PlaceChangeEvent;
import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.wui.application.place.HomePlace;

/**
 * Daemon responsible for various accessibility-related tasks
 */
public class AccessibilityDaemon extends BasicEventComponent {
  private static final AccessibilityDaemonEventBinder EVENT_BINDER = GWT.create(AccessibilityDaemonEventBinder.class);

  interface AccessibilityDaemonEventBinder extends EventBinder<AccessibilityDaemon> {}

  @EventHandler
  public void onPlaceChangeEvent(final PlaceChangeEvent e) {
    if (e.getValue() instanceof HomePlace) {
      return;
    }

    // Move focus to body (except when rendering pdf)
    SchedulerUtil.delay(() -> {
      final Element main = DomGlobal.document.querySelector("main");
      if (main != null) {
        main.focus();
      }
    });
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
