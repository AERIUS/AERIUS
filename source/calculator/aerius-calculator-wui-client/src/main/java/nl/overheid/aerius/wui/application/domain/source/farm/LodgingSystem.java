/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.farm;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

/**
 * Client side implementation of LodgingSystem props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public abstract class LodgingSystem {

  private String lodgingSystemType;
  private String lodgingSystemCode;
  private String systemDefinitionCode;

  protected static final @JsOverlay void init(final LodgingSystem props, final LodgingSystemType type) {
    props.setLodgingSystemType(type);
    props.setLodgingSystemCode("");
    props.setSystemDefinitionCode("");
  }

  public final @JsOverlay LodgingSystemType getLodgingSystemType() {
    return LodgingSystemType.safeValueOf(lodgingSystemType);
  }

  public final @JsOverlay void setLodgingSystemType(final LodgingSystemType type) {
    this.lodgingSystemType = type.name();
  }

  public final @JsOverlay String getLodgingSystemCode() {
    return lodgingSystemCode;
  }

  public final @JsOverlay void setLodgingSystemCode(final String lodgingSystemCode) {
    this.lodgingSystemCode = lodgingSystemCode;
  }

  public final @JsOverlay String getSystemDefinitionCode() {
    return systemDefinitionCode;
  }

  public final @JsOverlay void setSystemDefinitionCode(final String systemDefinitionCode) {
    this.systemDefinitionCode = systemDefinitionCode;
  }
}
