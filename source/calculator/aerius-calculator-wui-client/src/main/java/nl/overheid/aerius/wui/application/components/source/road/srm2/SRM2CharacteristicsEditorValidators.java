/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.road.srm2;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.aerius.vuelidate.ValidationOptions;
import nl.aerius.vuelidate.Validations;
import nl.aerius.vuelidate.ValidatorBuilder;
import nl.overheid.aerius.srm2.conversion.Sigma0Calculator;

/**
 * Validators for roadside barrier.
 */
class SRM2CharacteristicsEditorValidators extends ValidationOptions<SRM2CharacteristicsEditorComponent> {

  public static final double TUNNEL_FACTOR_MINIMUM = 0D;
  public static final double TUNNEL_FACTOR_MAXIMUM = 999.9;

  @JsType(isNative = true, namespace = JsPackage.GLOBAL)
  static class SRM2CharacteristicsValidations extends Validations {
    public @JsProperty Validations tunnelFactorV;
    public @JsProperty Validations porosityV;
    public @JsProperty Validations elevationHeightV;
    public @JsProperty Validations elevationHeightWarningV;
  }

  @Override
  protected void constructValidators(final Object options, final ValidatorInstaller v) {
    final SRM2CharacteristicsEditorComponent instance = Js.uncheckedCast(options);
    v.install("tunnelFactorV", () -> instance.tunnelFactorV = null, ValidatorBuilder.create()
        .minValue(TUNNEL_FACTOR_MINIMUM)
        .maxValue(TUNNEL_FACTOR_MAXIMUM)
        .required());
    v.install("porosityV", () -> instance.porosityV = null, ValidatorBuilder.create()
        .required()
        .decimal()
        .minValue(0d));
    v.install("elevationHeightV", () -> instance.elevationHeightV = null, ValidatorBuilder.create()
        .required()
        .integer());
    v.install("elevationHeightWarningV", () -> instance.elevationHeightWarningV = null, ValidatorBuilder.create()
        .required()
        .integer()
        .minValue(Sigma0Calculator.MIN_ROAD_HEIGHT)
        .maxValue(Sigma0Calculator.MAX_ROAD_HEIGHT));
  }
}
