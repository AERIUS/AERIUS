/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.util.json;

import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

/**
 * Utility class to make a custom representation of an object
 * for JSON serialization
 */
@JsType
@SuppressWarnings("rawtypes")
public class JsonBuilder {

  private final JsPropertyMap sourceProperties;
  private final JsPropertyMap result = JsPropertyMap.of();

  /**
   * Create a new JsonBuilder
   * @param sourceObject the object to base the representation on
   */
  public JsonBuilder(final Object sourceObject) {
    this.sourceProperties = Js.asPropertyMap(sourceObject);
  }

  /**
   * Include certain properties in the representation
   * @param keys property names to include
   * @return this
   */
  @SuppressWarnings("unchecked")
  public JsonBuilder include(String... keys) {
    for (String key : keys) {
      result.set(key, sourceProperties.get(key));
    }
    return this;
  }

  /**
   * Override a certain property in the representation
   * @param key property name to override
   * @param value property value to override
   * @return this
   */
  @SuppressWarnings("unchecked")
  public JsonBuilder set(String key, Object value) {
    result.set(key, value);
    return this;
  }

  /**
   * Return the final representation
   * @return representation
   */
  public Object build() {
    return result;
  }

}
