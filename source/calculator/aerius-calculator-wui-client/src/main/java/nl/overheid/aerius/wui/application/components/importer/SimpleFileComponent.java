/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.importer;

import java.util.List;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Emit;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import elemental2.dom.File;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.wui.vue.transition.HorizontalCollapse;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsibleButton;
import nl.overheid.aerius.wui.application.components.importer.content.InlineFileSettings;
import nl.overheid.aerius.wui.application.components.importer.content.MultipleFileContentsComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;

@Component(components = {
    FileUploadComponent.class,
    HorizontalCollapse.class,
    ValidationBehaviour.class,
    CollapsibleButton.class,
    VerticalCollapse.class,
    InlineFileSettings.class,
    MultipleFileContentsComponent.class,
})
public class SimpleFileComponent extends ErrorWarningValidator {
  @Prop EventBus eventBus;
  @Prop File originalFile;
  @Prop @JsProperty List<FileUploadStatus> files;

  @Data boolean contentVisible = false;

  @JsMethod
  public void toggleContent() {
    if (hasContent(getFile())) {
      contentVisible = !contentVisible;
    }
  }

  @JsMethod
  public boolean hasContent(final FileUploadStatus file) {
    return file.getErrors().isEmpty()
        && file.getSituationStats() != null;
  }

  @Computed
  public FileUploadStatus getFile() {
    return files.get(0);
  }

  @Emit
  @JsMethod
  public void displayErrors() {}

  @Emit
  @JsMethod
  public void displayWarnings() {}

  @JsMethod
  public void removeFile() {
    files.forEach(file -> vue().$emit("remove-file", file));
  }

  @JsMethod
  public String getIconForFile(final FileUploadStatus file) {
    return FileUploadStatus.getIconForFile(file);
  }

  @Computed("showErrors")
  public boolean showErrors() {
    return files.stream().anyMatch(FileUploadStatus::hasErrors);
  }

  @Computed("showWarnings")
  public boolean showWarnings() {
    return files.stream().anyMatch(FileUploadStatus::hasWarnings) && files.stream().noneMatch(FileUploadStatus::hasErrors);
  }

  @Computed("isValid")
  public boolean isValid() {
    return files.stream().allMatch(v -> !v.hasWarnings() && !v.hasErrors()) && !hasChildErrors() && !hasChildWarnings();
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return files.stream().anyMatch(v -> v.getImportAction() == null)
        || hasChildErrors();
  }
}
