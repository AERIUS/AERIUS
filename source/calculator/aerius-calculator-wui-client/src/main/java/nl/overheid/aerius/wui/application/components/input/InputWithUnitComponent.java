/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.input;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

@Component
public class InputWithUnitComponent implements IsVueComponent {
  private static final String DEFAULT_UNIT_MARGIN = "20";

  /**
   * Unit specifier for the input value.
   */
  @Prop(required = true) String unit;
  /**
   * Human readable unit description for the input value
   */
  @Prop(required = true) String unitDescription;
  /**
   * Optional margin for the unit. Set if the default puts the unit too much to
   * the right of left.
   */
  @Prop String unitMargin;
  /**
   * Whether to include input warning|error styles (default: yes)
   */
  @Prop boolean inputStyles;
  /**
   * Override for showing warning status.
   */
  @Prop boolean showWarning;
  /**
   * Override for showing error status.
   */
  @Prop boolean showError;

  @PropDefault("inputStyles")
  boolean inputStylesDefault() {
    return true;
  }

  @PropDefault("showError")
  boolean showErrorDefault() {
    return false;
  }

  @PropDefault("showWarning")
  boolean showWarningDefault() {
    return false;
  }

  @Computed
  String unitStyle() {
    return "margin-left: -" + (unitMargin == null ? DEFAULT_UNIT_MARGIN : unitMargin) + "px";
  }

}
