/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.export.types.options;

import java.util.List;
import java.util.Map.Entry;
import java.util.function.Function;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.backgrounddata.MetSite;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.wui.application.components.input.CheckBoxComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ExportContext;
import nl.overheid.aerius.wui.application.domain.calculation.ADMSOptions;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.application.ui.pages.export.types.options.AdditionalOptionsValidators.AdditionalOptionsValidations;
import nl.overheid.aerius.wui.config.ApplicationFlags;

@Component(components = {
    LabeledInputComponent.class,
    CheckBoxComponent.class,
    ValidationBehaviour.class,
    SimplifiedListBoxComponent.class,
}, customizeOptions = {
    AdditionalOptionsValidators.class,
})
public class AdditionalOptionsComponent extends ErrorWarningValidator implements HasCreated, HasValidators<AdditionalOptionsValidations> {
  @Inject @Data ApplicationContext appContext;
  @Inject @Data ExportContext exportContext;
  @Inject @Data ApplicationFlags flags;

  @Prop CalculationSetOptions options;

  @Data String metSiteIdentifierV;

  @JsProperty(name = "$v") AdditionalOptionsValidations validation;

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Override
  @Computed
  public AdditionalOptionsValidations getV() {
    return validation;
  }

  @Computed
  public boolean getADMSExport() {
    return exportContext.getExportType() == ExportType.ADMS;
  }

  @Computed
  public void setADMSExport(final boolean checked) {
    setExportType(checked, ExportType.ADMS);
  }

  @Computed
  public boolean getOPSExport() {
    return exportContext.getExportType() == ExportType.OPS;
  }

  @Computed
  public void setOPSExport(final boolean checked) {
    setExportType(checked, ExportType.OPS);
  }

  private void setExportType(final boolean checked, final ExportType exportType) {
    if (checked) {
      exportContext.setExportType(exportType);
    } else if (exportContext.getExportType() == exportType) {
      exportContext.setExportType(null);
    }
  }

  @SuppressWarnings("rawtypes")
  @Computed("entryKey")
  public Function<Object, Object> entryKey(final Object obj) {
    return v -> String.valueOf(((Entry) v).getKey());
  }

  @SuppressWarnings("rawtypes")
  @Computed("entryValue")
  public Function<Object, String> entryValue(final Object obj) {
    return v -> String.valueOf(((Entry) v).getValue());
  }

  @JsMethod
  public void selectSiteLocation(final String siteLocationKey) {
    metSiteIdentifierV = siteLocationKey;
    ValidationUtil.setSafeIntegerValue(v -> getAdmsOptions().setMetSiteId(v), siteLocationKey, 0);
  }

  @Computed
  public String getMeteoSiteLocation() {
    return String.valueOf(getAdmsOptions().getMetSiteId());
  }

  private ADMSOptions getAdmsOptions() {
    return options.getNcaCalculationOptions().getAdmsOptions();
  }

  @Computed
  public List<MetSite> getSiteLocations() {
    return appContext.getConfiguration().getMetSites();
  }

  @Computed("isMetSiteInvalid")
  public boolean isMetSiteInvalid() {
    return validation.metSiteIdentifier.invalid
        || !getSiteLocations().stream().anyMatch(v -> v.getId() == getAdmsOptions().getMetSiteId());
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return getADMSExport() && isMetSiteInvalid();
  }
}
