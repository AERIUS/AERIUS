/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source;

import java.util.stream.Stream;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.Coordinate;
import ol.Feature;
import ol.OLFactory;
import ol.layer.LayerGroupOptions;
import ol.layer.VectorLayerOptions;
import ol.style.Fill;
import ol.style.Icon;
import ol.style.IconOptions;
import ol.style.Stroke;
import ol.style.Style;
import ol.style.StyleOptions;

import nl.aerius.geo.domain.IsLayer;
import nl.aerius.geo.domain.LayerInfo;
import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.shared.domain.geo.HexagonUtil;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.daemon.geo.InfoMarkerSelectionEvent;
import nl.overheid.aerius.wui.application.domain.geo.InfoMarkerOptions;
import nl.overheid.aerius.wui.application.event.InfoPopupHiddenEvent;
import nl.overheid.aerius.wui.application.resources.R;

public class InfoMarkerLayer extends BasicEventComponent implements IsLayer<ol.layer.Group> {
  private static final InfoMarkerLayerEventBinder EVENT_BINDER = GWT.create(InfoMarkerLayerEventBinder.class);

  interface InfoMarkerLayerEventBinder extends EventBinder<InfoMarkerLayer> {}

  private static final Stroke INFO_STYLE_STROKE = OLFactory.createStroke(OLFactory.createColor(21, 124, 177, 1), 2);
  private static final Fill INFO_STYLE_FILL = OLFactory.createFill(OLFactory.createColor(208, 229, 239, 0.4));

  public static final Style INFO_VECTOR_STYLE;
  public static final Style INFO_MARKER_STYLE;
  static {
    final StyleOptions vectorStyleOptions = new StyleOptions();
    vectorStyleOptions.setStroke(INFO_STYLE_STROKE);
    vectorStyleOptions.setFill(INFO_STYLE_FILL);
    INFO_VECTOR_STYLE = new Style(vectorStyleOptions);

    final IconOptions markerOptions = new IconOptions();
    final String img = R.images().markerInfo().getSafeUri().asString();
    markerOptions.setSrc(img);
    markerOptions.setAnchor(new double[] {26, 64});

    markerOptions.setAnchorXUnits("pixels");
    markerOptions.setAnchorYUnits("pixels");
    final Icon icon = new Icon(markerOptions);
    final StyleOptions markerStyleOptions = new StyleOptions();
    markerStyleOptions.setImage(icon);
    INFO_MARKER_STYLE = new Style(markerStyleOptions);
  }

  private final ol.source.Vector vectorSource;
  private final ol.source.Vector markerSource;

  private final ol.layer.Group groupLayer;

  private final ApplicationContext context;
  private final ReceptorUtil util;

  @Inject
  public InfoMarkerLayer(final EventBus eventBus, final ApplicationContext context) {
    this.context = context;
    // TODO Refactor eventbus injection, the MapLayoutPanel should probably set
    // these (for HasEventBus interfaces)
    setEventBus(eventBus);

    util = new ReceptorUtil(context.getConfiguration().getReceptorGridSettings());

    vectorSource = new ol.source.Vector();
    final VectorLayerOptions vectorLayerOptions = OLFactory.createOptions();
    vectorLayerOptions.setStyle((feature, v) -> getVectorStyle(feature, v));
    final ol.layer.Vector vectorLayer = new ol.layer.Vector(vectorLayerOptions);
    vectorLayer.setSource(vectorSource);

    markerSource = new ol.source.Vector();
    final VectorLayerOptions markerLayerOptions = OLFactory.createOptions();
    markerLayerOptions.setStyle((feature, v) -> getMarkerStyle(feature, v));
    final ol.layer.Vector markerLayer = new ol.layer.Vector(markerLayerOptions);
    markerLayer.setSource(markerSource);

    final ol.Collection<ol.layer.Base> layers = new ol.Collection<>();
    layers.push(vectorLayer);
    layers.push(markerLayer);

    final LayerGroupOptions groupOptions = new LayerGroupOptions();
    groupOptions.setLayers(layers);

    groupLayer = new ol.layer.Group(groupOptions);
  }

  private Style[] getVectorStyle(final Feature feature, final double resolution) {
    return new Style[] {INFO_VECTOR_STYLE};
  }

  private Style[] getMarkerStyle(final Feature feature, final double v) {
    return new Style[] {INFO_MARKER_STYLE};
  }

  @EventHandler
  public void onInfoMarkerSelectionEvent(final InfoMarkerSelectionEvent c) {
    final InfoMarkerOptions options = c.getValue();
    final Point recPoint = util.getPointFromReceptorId(options.getReceptorId());
    final Polygon hexagon = HexagonUtil.createHexagon(recPoint, context.getConfiguration().getReceptorGridSettings().getZoomLevel1());
    final Coordinate[][] coordinates = Stream.of(hexagon.getCoordinates())
        .map(v -> convertCoordinates(v))
        .toArray(len -> new Coordinate[len][]);
    final ol.geom.Polygon poly = new ol.geom.Polygon(coordinates);
    final Feature polyFeature = new ol.Feature(poly);
    vectorSource.clear(true);
    vectorSource.addFeature(polyFeature);

    final ol.geom.Point point = new ol.geom.Point(new Coordinate(recPoint.getX(), recPoint.getY()));
    final Feature pointFeature = new ol.Feature(point);
    markerSource.clear(true);
    markerSource.addFeature(pointFeature);
  }

  @EventHandler
  public void onInfoPopupHiddenEvent(final InfoPopupHiddenEvent e) {
    vectorSource.clear(true);
    markerSource.clear(true);
  }

  private Coordinate[] convertCoordinates(final double[][] coordinates) {
    return Stream.of(coordinates)
        .map(v -> new Coordinate(v[0], v[1]))
        .toArray(len -> new Coordinate[len]);
  }

  @Override
  public ol.layer.Group asLayer() {
    return groupLayer;
  }

  @Override
  public LayerInfo getInfo() {
    return null;
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
