/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.source.road.adms;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;
import jsinterop.base.Js;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategories;
import nl.overheid.aerius.shared.domain.sector.category.SimpleCategory;
import nl.overheid.aerius.shared.domain.v2.source.road.ADMSRoadSideBarrierType;
import nl.overheid.aerius.wui.application.components.divider.DividerComponent;
import nl.overheid.aerius.wui.application.components.source.DetailStatComponent;
import nl.overheid.aerius.wui.application.components.source.road.vehicles.RoadVehiclesDetailComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.domain.source.ADMSCharacteristics;
import nl.overheid.aerius.wui.application.domain.source.ADMSRoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.road.ADMSRoadSideBarrier;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "adms-road-characteristics", components = {
    DetailStatComponent.class,
    VerticalCollapse.class,
    RoadVehiclesDetailComponent.class,
    DividerComponent.class
})
public class ADMSRoadCharacteristicsComponent extends BasicVueComponent {
  @Prop ADMSRoadESFeature source;
  @Prop @JsProperty List<Substance> substances;
  @Prop String totalEmissionTitle;

  @Data @Inject ScenarioContext context;
  @Data @Inject ApplicationContext applicationContext;

  @Data ADMSCharacteristics characteristics;

  public RoadEmissionCategories getRoadEmissionCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getRoadEmissionCategories();
  }

  @Watch(value = "source", isImmediate = true)
  public void watchSource() {
    characteristics = source == null ? null : Js.uncheckedCast(source.getCharacteristics());
  }

  @Computed
  public ADMSCharacteristics getChars() {
    return characteristics;
  }

  @Computed("hasAnyBarrier")
  public boolean hasAnyBarrier() {
    return source.getBarrierLeft() != null
        || source.getBarrierRight() != null;
  }

  @Computed
  public List<SimpleCategory> getAreas() {
    return getRoadEmissionCategories().getAreas();
  }

  @Computed
  protected String getRoadArea() {
    return getAreas().stream()
        .filter(v -> v.getCode().equals(source.getRoadAreaCode()))
        .map(v -> v.getDescription())
        .findFirst()
        .orElse("-");
  }

  @Computed
  public List<SimpleCategory> getRoadTypes() {
    return getRoadEmissionCategories().getRoadTypes(source.getRoadAreaCode());
  }

  @Computed
  protected String getRoadType() {
    return getRoadTypes().stream()
        .filter(v -> v.getCode().equals(source.getRoadTypeCode()))
        .map(v -> v.getDescription())
        .findFirst()
        .orElse("-");
  }

  @Computed
  protected ADMSRoadSideBarrierType getBarrierLeftType() {
    return source.getBarrierLeft() == null ? ADMSRoadSideBarrierType.NONE : source.getBarrierLeft().getBarrierType();
  }

  @Computed
  protected ADMSRoadSideBarrierType getBarrierRightType() {
    return source.getBarrierRight() == null ? ADMSRoadSideBarrierType.NONE : source.getBarrierRight().getBarrierType();
  }

  @JsMethod
  public String barrierLeft(final Function<ADMSRoadSideBarrier, String> func) {
    return Optional.ofNullable(source.getBarrierLeft())
        .map(func)
        .orElse("-");
  }

  @JsMethod
  public String barrierRight(final Function<ADMSRoadSideBarrier, String> func) {
    return Optional.ofNullable(source.getBarrierRight())
        .map(func)
        .orElse("-");
  }
}
