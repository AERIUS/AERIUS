/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.coldstart.specific;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.annotations.component.Watch;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.ColdStartSourceCategory;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.road.RoadNumberOfVehiclesComponent;
import nl.overheid.aerius.wui.application.components.source.road.factor.EmissionFactorRowComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.EmissionFactors;
import nl.overheid.aerius.wui.application.domain.source.road.RoadSourceUtil;
import nl.overheid.aerius.wui.application.domain.source.road.SpecificVehicles;

@Component(components = {
    LabeledInputComponent.class,
    RoadNumberOfVehiclesComponent.class,
    EmissionFactorRowComponent.class,
    ValidationBehaviour.class
})
public class ColdStartSpecificEmissionComponent extends ErrorWarningValidator {
  @Prop SpecificVehicles specificVehicles;
  @Prop int year;
  @Data @JsProperty EmissionFactors emissionFactors = EmissionFactors.create();

  @Data @Inject ApplicationContext applicationContext;

  @Ref RoadNumberOfVehiclesComponent roadNumberOfVehiclesComponent;

  @Computed
  public List<ColdStartSourceCategory> getCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getColdStartCategories().getSpecificCategories();
  }

  @Watch(value = "specificVehicles", isImmediate = true)
  public void onSpecificVehiclesChange() {
    emissionFactors = RoadSourceUtil.updateEmissionFactors(getCategories(), specificVehicles.getVehicleCode(), getSubstances(), year);
  }

  @Computed("vehicleCode")
  protected String getVehicleCode() {
    return specificVehicles.getVehicleCode();
  }

  @Computed("vehicleCode")
  protected void setVehicleCode(final String vehicleCode) {
    if (!vehicleCode.isEmpty()) {
      specificVehicles.setVehicleCode(vehicleCode);
      emissionFactors = RoadSourceUtil.updateEmissionFactors(getCategories(), specificVehicles.getVehicleCode(), getSubstances(), year);
    } else {
      valueModified();
    }
  }

  @Computed("substances")
  protected List<Substance> getSubstances() {
    return applicationContext.getConfiguration().getSubstances();
  }

  @JsMethod
  protected void valueModified() {
    vue().$emit("switchVehicleType", emissionFactors);
  }
}
