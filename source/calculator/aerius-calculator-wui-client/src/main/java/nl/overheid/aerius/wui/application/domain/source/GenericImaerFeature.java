/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source;

import static jsinterop.annotations.JsPackage.GLOBAL;

import ol.Feature;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.wui.application.domain.ImaerFeature;

/**
 * Generic IMAER Feature. This class extends openlayers {@link Feature}
 * class.
 *
 * NOTE: This class can't be implemented with java fields as the variables
 * should be in the Feature properties
 */
@JsType(namespace = GLOBAL, name = "ol.Feature", isNative = true)
public class GenericImaerFeature extends ImaerFeature {
  public final static @JsOverlay GenericImaerFeature create() {
    final GenericImaerFeature feature = new GenericImaerFeature();
    init(feature);
    return feature;
  }

  public final static @JsOverlay void init(final GenericImaerFeature feature) {
    ImaerFeature.initImaer(feature);
  }
}
