/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.icons.labels;

/**
 * Represents the various label icon styles that are available
 */
public enum LabelIconStyle {
  EMISSION_SOURCE(8, 40, LabelPointerLocation.TOP_LEFT),
  BUILDING(8, 40, LabelPointerLocation.BOTTOM_RIGHT),
  CALCULATION_POINT(15, 30, LabelPointerLocation.TOP_RIGHT),
  MET_SITE(8, 40, LabelPointerLocation.BOTTOM_LEFT),
  ARCHIVE_PROJECT(15, 30, LabelPointerLocation.BOTTOM);

  private final int textBoxRadius;
  private final int textBoxMinWidth;
  private final LabelPointerLocation pointerLocation;

  LabelIconStyle(final int textBoxRadius, final int textBoxMinWidth, final LabelPointerLocation pointerLocation) {
    this.textBoxRadius = textBoxRadius;
    this.textBoxMinWidth = textBoxMinWidth;
    this.pointerLocation = pointerLocation;
  }

  int getTextBoxRadius() {
    return textBoxRadius;
  }

  int getTextBoxMinWidth() {
    return textBoxMinWidth;
  }

  LabelPointerLocation getPointerLocation() {
    return pointerLocation;
  }

  int getTextBoxNoOffset(final int textBoxHeight) {
    return textBoxHeight - (2 * textBoxRadius);
  }

  int getLabelOffset(final int labelBorderWidth) {
    return labelBorderWidth + textBoxRadius;
  }
}
