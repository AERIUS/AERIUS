/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.print.own2000;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.print.own2000.results.ResultSummaryCalculationPointsTable;
import nl.overheid.aerius.wui.application.print.own2000.results.ResultSummaryMap;
import nl.overheid.aerius.wui.application.print.own2000.results.ResultSummaryTable;
import nl.overheid.aerius.wui.vue.BasicVueEventComponent;

@Component(components = {
    ResultSummaryMap.class,
    ResultSummaryTable.class,
    ResultSummaryCalculationPointsTable.class,
})
public class Results extends BasicVueEventComponent {

  @Prop EventBus eventBus;
  @Prop boolean includeMap;
  @Prop boolean includeCalculationPoints;
  @Inject @Data ScenarioContext context;

  public static final String ID = "results";

  @PropDefault("includeMap")
  boolean defaultIncludeMap() {
    return true;
  }

  @PropDefault("includeCalculationPoints")
  boolean defaultIncludeCalculationPoints() {
    return true;
  }
}
