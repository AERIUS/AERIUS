/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.farm;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.wui.vue.ReactivityUtil;

/**
 * Client size implementation of StandardAdditionalHousingSystem
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class StandardAdditionalHousingSystem extends AdditionalHousingSystem {

  private String additionalSystemCode;

  public static final @JsOverlay StandardAdditionalHousingSystem create() {
    final StandardAdditionalHousingSystem props = Js.uncheckedCast(JsPropertyMap.of());

    init(props);
    return props;
  }

  public static final @JsOverlay void init(final StandardAdditionalHousingSystem props) {
    AdditionalHousingSystem.init(props, AdditionalSystemType.STANDARD);

    ReactivityUtil.ensureJsProperty(props, "additionalSystemCode", props::setAdditionalSystemCode, "");
  }

  public final @JsOverlay String getAdditionalSystemCode() {
    return additionalSystemCode;
  }

  public final @JsOverlay void setAdditionalSystemCode(final String additionalSystemCode) {
    this.additionalSystemCode = additionalSystemCode;
  }

}
