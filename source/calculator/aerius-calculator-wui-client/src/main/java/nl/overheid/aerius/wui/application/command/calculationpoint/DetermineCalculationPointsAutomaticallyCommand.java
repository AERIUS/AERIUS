/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.command.calculationpoint;

import java.util.List;

import nl.aerius.wui.command.StatelessCommand;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;

/**
 * Request a calculation point determination by the daemon
 */
public class DetermineCalculationPointsAutomaticallyCommand extends StatelessCommand {

  private final List<EmissionSourceFeature> sources;
  private final boolean pointsAbroad;
  private final Integer radiusInland;

  public DetermineCalculationPointsAutomaticallyCommand(final List<EmissionSourceFeature> sources, final boolean pointsAbroad, final Integer radiusInland) {
    this.sources = sources;
    this.pointsAbroad = pointsAbroad;
    this.radiusInland = radiusInland;
  }

  public List<EmissionSourceFeature> getSources() {
    return sources;
  }

  public boolean isPointsAbroad() {
    return pointsAbroad;
  }

  public Integer getRadiusInland() {
    return radiusInland;
  }
}
