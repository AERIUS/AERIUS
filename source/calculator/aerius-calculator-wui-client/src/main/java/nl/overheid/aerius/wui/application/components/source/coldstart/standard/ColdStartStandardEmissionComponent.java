/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.coldstart.standard;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.sector.category.SimpleCategory;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.wui.application.components.input.InputWithUnitComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.MinimalInputComponent;
import nl.overheid.aerius.wui.application.components.source.coldstart.standard.ColdStartStandardValidators.ColdStartStandardValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.road.ColdStartStandardVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.ValuesPerColdStartVehicleTypes;
import nl.overheid.aerius.wui.vue.DebugDirective;

@Component(customizeOptions = {
    ColdStartStandardValidators.class
}, directives = {
    ValidateDirective.class,
    DebugDirective.class
}, components = {
    LabeledInputComponent.class,
    MinimalInputComponent.class,
    InputWithUnitComponent.class,
    ColdStartStandardVehicleTypeRowComponent.class,
    ValidationBehaviour.class
})
public class ColdStartStandardEmissionComponent extends ErrorWarningValidator implements HasCreated,
    HasValidators<ColdStartStandardValidations> {
  @Prop @JsProperty ColdStartStandardVehicles standardVehicles;

  @Data @Inject ApplicationContext applicationContext;

  @JsProperty(name = "$v") ColdStartStandardValidations validations;

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Override
  @Computed
  public ColdStartStandardValidations getV() {
    return validations;
  }

  @Computed
  protected String getTimeUnit() {
    return standardVehicles.getTimeUnit().name();
  }

  @Computed
  protected void setTimeUnit(final String timeUnit) {
    standardVehicles.setTimeUnit(TimeUnit.valueOf(timeUnit));
  }

  @Computed("valuesPerVehicleType")
  protected ValuesPerColdStartVehicleTypes getValuesPerVehicleType() {
    return standardVehicles.getValuesPerVehicleTypes();
  }

  @Computed("vehicleTypes")
  protected List<SimpleCategory> getVehicleTypes() {
    return applicationContext.getConfiguration().getSectorCategories().getRoadEmissionCategories().getVehicleTypes();
  }

  @Computed("isInvalid")
  protected boolean isInvalid() {
    return validations.invalid;
  }
}
