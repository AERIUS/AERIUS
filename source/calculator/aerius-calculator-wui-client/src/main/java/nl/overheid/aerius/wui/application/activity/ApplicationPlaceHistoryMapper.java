/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.activity;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import nl.aerius.wui.history.PlaceHistoryMapper;
import nl.aerius.wui.place.PlaceTokenizer;
import nl.aerius.wui.place.TokenizedPlace;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.wui.application.place.ApplicationTokenizers;
import nl.overheid.aerius.wui.application.place.PrintPlace;
import nl.overheid.aerius.wui.application.place.ThemePlaceProvider;

public class ApplicationPlaceHistoryMapper implements PlaceHistoryMapper {
  private final Map<String, PlaceTokenizer<? extends TokenizedPlace>> tokenizers = new LinkedHashMap<>();

  public ApplicationPlaceHistoryMapper() {
    init(ApplicationTokenizers.DEFAULT_PLACE);
    init(PrintPlace.createTokenizer());

    init(ThemePlaceProvider.tokenizers(Theme.OWN2000));
    init(ThemePlaceProvider.tokenizers(Theme.NCA));
  }

  private void init(final List<PlaceTokenizer<?>> tokenizers) {
    tokenizers.forEach(this::init);
  }

  private void init(final PlaceTokenizer<?> tokenizer) {
    tokenizers.put(tokenizer.getPrefix(), tokenizer);
  }

  @Override
  public String getToken(final TokenizedPlace value) {
    return value.getToken();
  }

  @Override
  public TokenizedPlace getPlace(final String fullToken) {
    final String trimmedToken = trimToken(fullToken);

    return tokenizers.keySet().stream()
        .filter(v -> trimmedToken.equals(v))
        .findFirst()
        .map(v -> tokenizers.get(v).getPlace(fullToken.substring(v.length())))
        .orElse(null);
  }

  private String trimToken(final String fullToken) {
    final String path = fullToken.split("\\?")[0];

    return trimEnd(trimStart(path));
  }

  private static String trimEnd(final String token) {
    if (token.isEmpty()) {
      return "";
    }

    return token.lastIndexOf("/") == token.length() - 1 ? trimEnd(token.substring(0, token.length() - 1)) : token;
  }

  private static String trimStart(final String token) {
    return token.indexOf("/") == 0 ? trimStart(token.substring(1)) : token;
  }
}
