/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.road;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.v2.source.road.ADMSRoadSideBarrierType;
import nl.overheid.aerius.wui.vue.ReactivityUtil;

/**
 * Client side implementation of ADMSRoadSideBarrier props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class ADMSRoadSideBarrier {

  /**
   * Type of the barrier.
   */
  private String barrierType;
  private double width;
  private double maximumHeight;
  private double averageHeight;
  private double minimumHeight;
  private double porosity;

  public static final @JsOverlay ADMSRoadSideBarrier create() {
    final ADMSRoadSideBarrier props = new ADMSRoadSideBarrier();
    init(props);
    return props;
  }

  public static final @JsOverlay void init(final ADMSRoadSideBarrier props) {
    ReactivityUtil.ensureJsProperty(props, "barrierType", props::setBarrierType, ADMSRoadSideBarrierType.NONE);
    ReactivityUtil.ensureJsProperty(props, "width", props::setWidth, 0D);
    ReactivityUtil.ensureJsProperty(props, "maximumHeight", props::setMaximumHeight, 0D);
    ReactivityUtil.ensureJsProperty(props, "averageHeight", props::setAverageHeight, 0D);
    ReactivityUtil.ensureJsProperty(props, "minimumHeight", props::setMinimumHeight, 0D);

    final Double defaultPorosity = ADMSRoadSideBarrierType.NONE.getDefaultPorosity();
    final double defaultPorosityNum = defaultPorosity == null ? 0D : defaultPorosity;
    ReactivityUtil.ensureJsProperty(props, "porosity", props::setPorosity, defaultPorosityNum);
  }

  public final @JsOverlay ADMSRoadSideBarrierType getBarrierType() {
    return barrierType == null || barrierType.isEmpty() ? null : ADMSRoadSideBarrierType.valueOf(barrierType);
  }

  public final @JsOverlay void setBarrierType(final ADMSRoadSideBarrierType barrierType) {
    this.barrierType = barrierType == null ? "" : barrierType.name();
  }

  public final @JsOverlay double getWidth() {
    return width;
  }

  public final @JsOverlay void setWidth(final double width) {
    this.width = width;
  }

  public final @JsOverlay double getMaximumHeight() {
    return maximumHeight;
  }

  public final @JsOverlay void setMaximumHeight(final double maximumHeight) {
    this.maximumHeight = maximumHeight;
  }

  public final @JsOverlay double getAverageHeight() {
    return averageHeight;
  }

  public final @JsOverlay void setAverageHeight(final double averageHeight) {
    this.averageHeight = averageHeight;
  }

  public final @JsOverlay double getMinimumHeight() {
    return minimumHeight;
  }

  public final @JsOverlay void setMinimumHeight(final double minimumHeight) {
    this.minimumHeight = minimumHeight;
  }

  public final @JsOverlay double getPorosity() {
    return porosity;
  }

  public final @JsOverlay void setPorosity(final double porosity) {
    this.porosity = porosity;
  }
}
