/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.manure.standard;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.sector.category.FarmSourceCategory;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.suggest.SuggestListBoxComponent;
import nl.overheid.aerius.wui.application.components.input.suggest.SuggestListBoxData;
import nl.overheid.aerius.wui.application.components.source.manure.base.ManureStorageBaseEmissionComponent;
import nl.overheid.aerius.wui.application.components.source.manure.standard.ManureStorageStandardValidators.ManureStorageStandardValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.manure.StandardManureStorage;
import nl.overheid.aerius.wui.vue.DebugDirective;

@Component(customizeOptions = ManureStorageStandardValidators.class, directives = {
    ValidateDirective.class,
    DebugDirective.class
}, components = {
    LabeledInputComponent.class,
    ValidationBehaviour.class,
    SuggestListBoxComponent.class,
    ManureStorageBaseEmissionComponent.class,
})
public class ManureStorageStandardEmissionComponent extends ErrorWarningValidator
    implements HasCreated, HasValidators<ManureStorageStandardValidations> {
  @Prop @JsProperty StandardManureStorage standardManureStorage;
  @Prop @JsProperty List<FarmSourceCategory> farmSourceCategories;

  @Data @Inject ApplicationContext applicationContext;

  @Data String manureStorageCodeV;

  @JsProperty(name = "$v") ManureStorageStandardValidations validation;

  @Override
  @Computed
  public ManureStorageStandardValidations getV() {
    return validation;
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Watch(value = "standardManureStorage", isImmediate = true)
  public void onStandardManureStorageChange(final StandardManureStorage neww) {
    if (neww == null) {
      return;
    }

    manureStorageCodeV = neww.getManureStorageCode();
  }

  @Computed
  public List<SuggestListBoxData> getCategoryCodes() {
    return farmSourceCategories.stream()
        .map(v -> new SuggestListBoxData(v.getCode(), v.getDescription(), v.getCode() + " " + v.getDescription(), false))
        .collect(Collectors.toList());
  }

  @JsMethod
  public void unselectCategoryCode() {
    standardManureStorage.setManureStorageCode(null);
    manureStorageCodeV = null;
  }

  @JsMethod
  void selectCategoryCode(final String code) {
    standardManureStorage.setManureStorageCode(code);
    manureStorageCodeV = code;
  }

  @Computed
  protected String getManureStorageCategoryCode() {
    return manureStorageCodeV;
  }

  @Computed
  protected FarmEmissionFactorType getFarmEmissionFactorType() {
    return farmSourceCategories.stream()
        .filter(x -> x.getCode().equalsIgnoreCase(manureStorageCodeV))
        .map(FarmSourceCategory::getFarmEmissionFactorType)
        .findFirst()
        .orElse(null);
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return validation.manureStorageCodeV.invalid;
  }

}
