/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.importer;

import jsinterop.base.Js;

import nl.aerius.wui.dev.GWTProd;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.daemon.calculation.CalculationPreparationDaemon;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.application.domain.source.ADMSRoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.FarmLodgingESFeature;
import nl.overheid.aerius.wui.application.domain.source.FarmAnimalHousingESFeature;
import nl.overheid.aerius.wui.application.domain.source.FarmlandESFeature;
import nl.overheid.aerius.wui.application.domain.source.GenericESFeature;
import nl.overheid.aerius.wui.application.domain.source.InlandMaritimeShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.InlandShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.ManureStorageESFeature;
import nl.overheid.aerius.wui.application.domain.source.MaritimeMaritimeShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.MooringInlandShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.MooringMaritimeShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.OffRoadMobileESFeature;
import nl.overheid.aerius.wui.application.domain.source.SRM1RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.SRM2RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.combustion.CombustionPlantESFeature;

public final class ImportParcelInitializer {
  private ImportParcelInitializer() {}

  /**
   * Call the canonical initializer for each source type
   */
  public static void postProcessEmissionSource(final EmissionSourceFeature source) {
    final EmissionSourceType type = source.getEmissionSourceType();
    switch (type) {
    case FARM_LODGE:
      FarmLodgingESFeature.init(Js.uncheckedCast(source));
    case FARM_ANIMAL_HOUSING:
      FarmAnimalHousingESFeature.init(Js.uncheckedCast(source));
      break;
    case FARMLAND:
      FarmlandESFeature.init(Js.uncheckedCast(source));
      break;
    case MANURE_STORAGE:
      ManureStorageESFeature.init(Js.uncheckedCast(source));
      break;
    case GENERIC:
      GenericESFeature.init(Js.uncheckedCast(source));
      break;
    case MEDIUM_COMBUSTION_PLANT:
      CombustionPlantESFeature.init(Js.uncheckedCast(source));
      break;
    case ADMS_ROAD:
      ADMSRoadESFeature.init(Js.uncheckedCast(source));
      break;
    case SRM1_ROAD:
      SRM1RoadESFeature.init(Js.uncheckedCast(source));
      break;
    case SRM2_ROAD:
      SRM2RoadESFeature.init(Js.uncheckedCast(source));
      break;
    case SHIPPING_MARITIME_MARITIME:
      MaritimeMaritimeShippingESFeature.init(Js.uncheckedCast(source));
      break;
    case SHIPPING_MARITIME_INLAND:
      InlandMaritimeShippingESFeature.init(Js.uncheckedCast(source));
      break;
    case SHIPPING_INLAND:
      InlandShippingESFeature.init(Js.uncheckedCast(source));
      break;
    case SHIPPING_INLAND_DOCKED:
      MooringInlandShippingESFeature.init(Js.uncheckedCast(source));
      break;
    case OFFROAD_MOBILE:
      OffRoadMobileESFeature.init(Js.uncheckedCast(source));
      break;
    case SHIPPING_MARITIME_DOCKED:
      MooringMaritimeShippingESFeature.init(Js.uncheckedCast(source));
      break;
    default:
      GWTProd.warn("Unknown initializer for source: ", source);
    }
  }

  /**
   * Call initializer for the options object
   */
  public static void postProcessCalculationOptions(final CalculationSetOptions options, final ApplicationContext applicationContext) {
    CalculationSetOptions.init(options);
    CalculationPreparationDaemon.postInit(applicationContext, options, false);
  }
}
