/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context.importer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.inject.Singleton;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.wui.application.components.importer.domain.VirtualSituation;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;

/**
 * Simple state object for some accounting relating to import actions
 */
@Singleton
public class ImportContext {
  private final @JsProperty Map<String, VirtualSituation> virtualSituations = new HashMap<>();

  public VirtualSituation addVirtualSituation(final VirtualSituation sit) {
    final VirtualSituation prev = virtualSituations.putIfAbsent(sit.getId(), sit);
    return prev == null ? sit : prev;
  }

  public List<VirtualSituation> getVirtualSituations() {
    return new ArrayList<>(virtualSituations.values());
  }

  public void reset() {
    virtualSituations.clear();
  }

  public void prune(final Set<VirtualSituation> usedVirtualSituations) {
    final HashSet<VirtualSituation> sits = new HashSet<>(virtualSituations.values());
    usedVirtualSituations.forEach(v -> sits.remove(v));
    sits.forEach(v -> virtualSituations.remove(v.getId()));
  }

  public static String formatNextVirtualSituationName(final FileUploadStatus file, final ScenarioContext scenarioContext,
      final ImportContext importContext) {
    final int situationsSize = scenarioContext.getSituations().size();
    final int virtualSize = importContext.getVirtualSituations().size();
    return Optional.ofNullable(file.getSituationName())
        .orElse(M.messages().importVirtualSituationEmptyName(situationsSize + virtualSize + 1));
  }
}
