/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.notice;

import java.util.function.Supplier;

public class NoticeRule {
  private Supplier<Boolean> showing = null;
  private Supplier<String> notice = null;

  public NoticeRule(final Supplier<Boolean> showNotice, final Supplier<String> notice) {
    this.showing = showNotice;
    this.notice = notice;
  }

  public static NoticeRule create(final Supplier<Boolean> showNotice, final Supplier<String> notice) {
    return new NoticeRule(showNotice, notice);
  }

  public Supplier<Boolean> getShowNotice() {
    return showing;
  }

  public void setShowNotice(final Supplier<Boolean> showNotice) {
    this.showing = showNotice;
  }

  public Supplier<String> getNotice() {
    return notice;
  }

  public void setNotice(final Supplier<String> notice) {
    this.notice = notice;
  }

  public boolean hasNotice() {
    return showing.get();
  }

  public boolean showing() {
    return showing.get();
  }

  public String message() {
    return notice.get();
  }
}
