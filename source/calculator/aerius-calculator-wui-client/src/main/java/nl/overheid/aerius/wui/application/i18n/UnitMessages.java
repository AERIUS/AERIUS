/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.i18n;

import com.google.gwt.i18n.client.Messages.Select;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings.DepositionValueDisplayType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.wui.application.ui.pages.results.GeneralizedEmissionResultType;

public interface UnitMessages {

  String pollutant(@Select Substance substance);

  String pollutantErk(@Select EmissionResultKey key);

  String unitC(String str);
  String unitHourPerY(String amount);
  String unitKgPerS(String amount);
  String unitKgPerY(String amount);
  String unitLiterPerY(String amount);
  String unitTonnesPerY(String amount);
  String unitGramPerY(String amount);
  String unitKm(String str);
  String unitM(String str);
  String unitKgPerCubicMeter(String str);
  String unitCubicMPerS(String str);
  String unitMPerS(String str);
  String unitMW(String str);
  String unitOrientation(String str);
  String unitPercentage(String str);
  String unitHa(String str);
  String unitDegrees(String str);
  String unitJoulePerKPerKg(String str);
  String unitMetersCubed(String str);
  String unitMetersSquared(String str);
  String unitSingularCelsius();
  String unitSingularCelsiusDescription();
  String unitSingularDegrees();
  String unitSingularDegreesDescription();
  String unitSingularMgPerNmCubed();
  String unitSingularMW();
  String unitSingularMWDescription();
  String unitSingularMeter();
  String unitSingularMeterDescription();
  String unitSingularMeterPerSecond();
  String unitSingularMeterPerSecondDescription();
  String unitSingularMetersCubed();
  String unitSingularMetersCubedDescription();
  String unitSingularMetersSquared();
  String unitSingularMetersSquaredDescription();
  String unitSingularCubicMeterPerSecond();

  String unitSingularCubicMeterPerSecondDescription();
  String unitSingularKgPerCubicMeter();
  String unitSingularKgPerCubicMeterDescription();
  String unitSingularKgPerS();
  String unitSingularKgPerSDescription();
  String unitSingularKgPerY();
  String unitSingularKgPerYDescription();
  String unitSingularPercentageCL();
  String unitSingularKmPerH();
  String unitSingularKmPerHDescription();
  String unitSingularJoulePerKPerKg();
  String unitSingularJoulePerKPerKgDescription();
  String unitSingularPercentage();
  String unitSingularPercentageDescription();
  String unitSingularGramPerKilometers();
  String unitSingularGramPerKilometersDescription();
  String unitSingularGramPerKilometersPerSecond();
  String unitSingularGramPerKilometersPerSecondDescription();
  String unitSingularGramPerColdStart();
  String unitSingularGramPerColdStartDescription();
  String unitSingularLiterPerYear();
  String unitSingularLiterPerYearDescription();
  String unitSingularHourPerYear();
  String unitSingularHourPerYearDescription();
  String unitSingularHour();
  String unitSingularHourDescription();
  String unitSingularHectares();
  String unitSingularTonPerY();
  String unitSingularTonnes();
  String unitSingularTonnesDescription();
  String unitSingularGramPerY();
  String unitSingularOrientation();

  String unitConcentration(String str);
  String unitCriticalLevel();
  String unitCriticalLevelDescription();
  String unitCriticalLoad();
  String unitCriticalLoadDescription();

  String unitDeposition(@Select DepositionValueDisplayType displayType, String str);

  String unitSingularResult(@Select GeneralizedEmissionResultType resultType, @Select DepositionValueDisplayType displayType);

  String decimalNumberCapped(double value, @Select int digits);
  String decimalNumberFixed(double value, @Select int digits);

  String point(double x, double y);

  String xOutOfY(int x, int y);

  String unitTime(@Select TimeUnit timeunit);
  String valueNotApplicable();

}
