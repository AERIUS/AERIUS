/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.nca.pages.calculate.options;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.backgrounddata.MetSite;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.InputWarningComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.domain.calculation.ADMSOptions;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;

@Component(components = {
    ValidationBehaviour.class,
    InputWarningComponent.class,
    InputErrorComponent.class,
    LabeledInputComponent.class,
    SimplifiedListBoxComponent.class,
})
public class MetSiteSearchComponent extends ErrorWarningValidator {
  private static final int SEARCH_SIZE_MAX = 6;

  @Prop(required = true) CalculationSetOptions options;
  @Prop(required = true) @JsProperty List<MetSite> siteLocations;

  @Data String metSiteFilterText;

  /**
   * Computes a list of MetSite locations that match the filter criteria
   * specified in 'metSiteFilterText'.
   *
   * <ul>
   * <li>Matches any of the words in the filter text against the site names
   * (case-insensitive).</li>
   * <li>Includes sites if at least one word from the filter text is present in
   * the site name.</li>
   * <li>Sorts the resulting sites by the number of matched words, with sites
   * containing more matches ranked higher.</li>
   * </ul>
   *
   * @return a sorted list of MetSite locations matching the filter criteria.
   */
  @Computed
  public List<MetSite> getSearchSiteLocations() {
    final List<String> filterWords = Arrays.asList(Optional.ofNullable(metSiteFilterText)
        .orElse("")
        .toLowerCase().split("\\s+"));

    return siteLocations.stream()
        .filter(v -> filterWords.stream().anyMatch(word -> v.getName().toLowerCase().contains(word)))
        .sorted(
            Comparator.comparingInt((final MetSite v) -> (int) filterWords.stream()
                .filter(word -> v.getName().toLowerCase().contains(word)).count()).reversed())
        .collect(Collectors.toList());
  }

  @Computed
  public int getSearchSize() {
    return Math.min(getSearchSiteLocations().size(), SEARCH_SIZE_MAX);
  }

  @Computed
  public String getMeteoSiteLocation() {
    return String.valueOf(getAdmsOptions().getMetSiteId());
  }

  @JsMethod
  public void toggleSiteLocation(final MetSite siteLocation) {
    if (getAdmsOptions().getMetSiteId() == siteLocation.getId()) {
      getAdmsOptions().setMetSiteId(0);
    } else {
      getAdmsOptions().setMetSiteId(siteLocation.getId());
    }
  }

  private ADMSOptions getAdmsOptions() {
    return options.getNcaCalculationOptions().getAdmsOptions();
  }

  @Computed("isMetSiteInvalid")
  public boolean isMetSiteInvalid() {
    return getAdmsOptions().getMetSiteId() == 0
        || !siteLocations.stream()
            .anyMatch(v -> v.getId() == getAdmsOptions().getMetSiteId());
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return isMetSiteInvalid();
  }
}
