/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.service;

import java.util.Arrays;

import javax.inject.Inject;

import com.google.gwt.user.client.rpc.AsyncCallback;

import ol.format.GeoJson;
import ol.format.GeoJsonFeatureOptions;

import elemental2.core.Global;

import nl.aerius.wui.service.ForwardedAsyncCallback;
import nl.aerius.wui.util.RequestUtil;
import nl.overheid.aerius.shared.RequestMappings;
import nl.overheid.aerius.wui.application.domain.calculationpoint.CalculationPointPlacementResult;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;

public class CalculationPointServiceAsyncImpl implements CalculationPointServiceAsync {

  @Inject EnvironmentConfiguration cfg;
  @Inject HeaderHelper hdr;

  private final GeoJson geojson = new GeoJson();

  @Override
  public void determineCalculationPoints(final CalculationPointDetermineRequest determineRequest,
      final AsyncCallback<CalculationPointPlacementResult> callback) {

    final String requestUrl = RequestUtil.prepareUrl(cfg.getConnectBaseUrl(), RequestMappings.CONNECT_UI_CALCULATION_POINTS_DETERMINE);

    RequestUtil.doPost(requestUrl, determineRequest.toJSONString(), hdr.defaultHeaders(),
        new ForwardedAsyncCallback<CalculationPointPlacementResult, String>(callback) {

          @Override
          public CalculationPointPlacementResult convert(final String content) {

            return (CalculationPointPlacementResult) Global.JSON.parse(content, (key, value) -> {
              if (key.equals("computedCalculationPoints")) {
                // JSON parsing on the features doesn't result in correct objects, need the geojson parser for that
                return Arrays.asList((CalculationPointFeature[]) geojson.readFeatures(value, new GeoJsonFeatureOptions()));
              } else
                return value;
            });
          }

        });

  }

}
