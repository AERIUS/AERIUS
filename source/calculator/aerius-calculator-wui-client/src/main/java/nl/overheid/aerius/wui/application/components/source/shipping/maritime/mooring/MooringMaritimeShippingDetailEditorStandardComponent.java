/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.shipping.maritime.mooring;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.sector.category.MaritimeShippingCategory;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.wui.application.components.input.InputWithUnitComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.maritime.mooring.MooringMaritimeValidators.MaritimeMovementValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.shipping.maritime.StandardMooringMaritimeShipping;
import nl.overheid.aerius.wui.application.util.NumberUtil;
import nl.overheid.aerius.wui.vue.DebugDirective;

@Component(name = "aer-mooring-maritime-shipping-detail-editor-standard", customizeOptions = {
    MooringMaritimeValidators.class
}, directives = {
    ValidateDirective.class,
    DebugDirective.class
}, components = {
    LabeledInputComponent.class,
    InputWithUnitComponent.class,
    ValidationBehaviour.class
})
public class MooringMaritimeShippingDetailEditorStandardComponent extends ErrorWarningValidator implements HasCreated,
    HasValidators<MaritimeMovementValidations> {
  @Prop @JsProperty StandardMooringMaritimeShipping standardMooringMaritimeShipping;
  @Data @Inject ApplicationContext applicationContext;

  @Data String descriptionV;
  @Data String shipsPerTimeUnitV;
  @Data String averageResidenceTimeV;
  @Data String shorePowerFactorV;
  @Data String shipCodeV;

  @JsProperty(name = "$v") MaritimeMovementValidations validation;

  @Watch(value = "standardMooringMaritimeShipping", isImmediate = true)
  public void onStandardMooringMaritimeShippingChange(final StandardMooringMaritimeShipping standardMooringMaritimeShipping) {
    descriptionV = standardMooringMaritimeShipping.getDescription();
    shipCodeV = standardMooringMaritimeShipping.getShipCode();
    shipsPerTimeUnitV = Integer.toString(standardMooringMaritimeShipping.getShipsPerTimeUnit());
    averageResidenceTimeV = Integer.toString(standardMooringMaritimeShipping.getAverageResidenceTime());
    shorePowerFactorV = Double.toString(NumberUtil.round(standardMooringMaritimeShipping.getShorePowerFactor() * 100D, 1));
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Computed
  public MaritimeMovementValidations getV() {
    return validation;
  }

  @Computed
  protected String getDescription() {
    return descriptionV;
  }

  @Computed
  protected void setDescription(final String description) {
    standardMooringMaritimeShipping.setDescription(description);
    this.descriptionV = description;
  }

  @JsMethod
  protected String descriptionError() {
    return i18n.errorDescriptionRequired();
  }

  @Computed
  public List<MaritimeShippingCategory> getShipCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getMaritimeShippingCategories();
  }

  @Computed
  public String getMaritimeShippingCategory() {
    return shipCodeV;
  }

  @Computed
  public void setMaritimeShippingCategory(final String shipCode) {
    standardMooringMaritimeShipping.setShipCode(shipCode);
    this.shipCodeV = shipCode;
  }

  @JsMethod
  protected String shipCategoryRequiredError() {
    return i18n.errorShipCategoryRequired();
  }

  @Computed
  protected String getShipsPerTimeUnit() {
    return shipsPerTimeUnitV;
  }

  @Computed
  protected void setShipsPerTimeUnit(final String amount) {
    ValidationUtil.setSafeIntegerValue(standardMooringMaritimeShipping::setShipsPerTimeUnit, amount, 0);
    this.shipsPerTimeUnitV = amount;
  }

  @JsMethod
  protected String shipsPerTimeUnitError() {
    return i18n.errorNotValidEntry(shipsPerTimeUnitV);
  }

  @Computed
  protected String getTimeUnit() {
    return standardMooringMaritimeShipping.getTimeUnit().name();
  }

  @Computed
  protected void setTimeUnit(final String timeUnit) {
    standardMooringMaritimeShipping.setTimeUnit(TimeUnit.valueOf(timeUnit));
  }

  @Computed
  protected String getAverageResidenceTime() {
    return averageResidenceTimeV;
  }

  @Computed
  public void setAverageResidenceTime(final String averageResidenceTime) {
    ValidationUtil.setSafeIntegerValue(standardMooringMaritimeShipping::setAverageResidenceTime, averageResidenceTime, 0);
    this.averageResidenceTimeV = averageResidenceTime;
  }

  @JsMethod
  protected String averageResidenceTimeError() {
    return i18n.errorNotValidEntry(averageResidenceTimeV);
  }

  @Computed
  protected String getShorePowerFactor() {
    return shorePowerFactorV;
  }

  @Computed
  public void setShorePowerFactor(final String shorePowerFactor) {
    ValidationUtil.setSafeDoubleValue(standardMooringMaritimeShipping::setShorePowerFactor, shorePowerFactor, 0);
    standardMooringMaritimeShipping.setShorePowerFactor(standardMooringMaritimeShipping.getShorePowerFactor() / 100.0);
    this.shorePowerFactorV = shorePowerFactor;
  }

  @Computed("isInvalid")
  protected boolean isInvalid() {
    return validation.descriptionV.invalid ||
        validation.shipCodeV.invalid ||
        validation.shipsPerTimeUnitV.invalid ||
        validation.averageResidenceTimeV.invalid ||
        validation.shorePowerFactorV.invalid;
  }

  @JsMethod
  protected String shorePowerFactorError() {
    return i18n.errorShipShorePowerFactor();
  }
}
