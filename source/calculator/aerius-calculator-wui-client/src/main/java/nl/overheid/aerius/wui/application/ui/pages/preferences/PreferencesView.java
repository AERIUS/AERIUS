/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.ui.pages.preferences;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;
import com.google.web.bindery.event.shared.EventBus;

import elemental2.dom.HTMLElement;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.NavigationContext;
import nl.overheid.aerius.wui.application.daemon.misc.PersistablePreferencesContext;
import nl.overheid.aerius.wui.application.daemon.misc.PreferencesDaemon;
import nl.overheid.aerius.wui.application.daemon.misc.UserSettingChangeCommand;
import nl.overheid.aerius.wui.application.daemon.misc.UserSettingRemoveCommand;
import nl.overheid.aerius.wui.application.util.AccessibilityUtil;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component(components = {
    PreferenceLineComponent.class,
    SimplifiedListBoxComponent.class,
    ButtonIcon.class,
})
public class PreferencesView extends BasicVueView implements HasMounted {
  public enum ImportMode {
    BASIC, ADVANCED;

    public static ImportMode safeValueOf(final String mode) {
      try {
        return valueOf(mode);
      } catch (final NoSuchElementException e) {
        return null;
      }
    }
  }

  @Ref HTMLElement main;

  @Inject @Data ApplicationContext applicationContext;
  @Inject @Data NavigationContext navigationContext;
  @Inject @Data PersistablePreferencesContext preferencesContext;

  @Prop EventBus eventBus;

  @Override
  public void mounted() {
    SchedulerUtil.delay(() -> AccessibilityUtil.findFirstFocusable(main).focus());
  }

  @Computed("hasProjectionSetting")
  public boolean hasProjectionSetting() {
    return !getAvailableProjections().isEmpty();
  }

  @Computed
  public String getNativeProjection() {
    return applicationContext.getConfiguration().getReceptorGridSettings().getEPSG().getEpsgCode();
  }

  @Computed
  public List<String> getAvailableProjections() {
    return applicationContext.getConfiguration().getConvertibleGeometrySystems();
  }

  @Computed
  public List<ImportMode> getImportModes() {
    return Arrays.asList(ImportMode.values());
  }

  @JsMethod
  public void setSelectedProjection(final String projection) {
    eventBus.fireEvent(new UserSettingChangeCommand(PreferencesDaemon.SELECTED_PROJECTION, projection));
  }

  @JsMethod
  public void removeSelectedProjection() {
    eventBus.fireEvent(new UserSettingRemoveCommand(PreferencesDaemon.SELECTED_PROJECTION));
  }

  @Computed("advancedImport")
  public boolean getAdvancedImport() {
    return preferencesContext.getImportMode() == ImportMode.ADVANCED;
  }

  @JsMethod
  public void setAdvancedImport(final boolean advanced) {
    eventBus.fireEvent(new UserSettingChangeCommand(PreferencesDaemon.ADVANCED_USER_IMPORT, advanced
        ? ImportMode.ADVANCED
        : ImportMode.BASIC));
  }

  @Computed("layerAutoSwitch")
  public boolean getLayerAutoSwitch() {
    return preferencesContext.isLayerAutoSwitch();
  }

  @JsMethod
  public void setLayerAutoSwitch(final boolean autoSwitch) {
    eventBus.fireEvent(new UserSettingChangeCommand(PreferencesDaemon.LAYER_AUTO_SWITCH, autoSwitch));
  }

  @Computed("manualAutoSwitch")
  public boolean getManualAutoSwitch() {
    return preferencesContext.isManualAutoSwitch();
  }

  @JsMethod
  public void setManualAutoSwitch(final boolean autoSwitch) {
    eventBus.fireEvent(new UserSettingChangeCommand(PreferencesDaemon.MANUAL_AUTO_SWITCH, autoSwitch));
  }

  @Computed("loginEnabledEnabled")
  public boolean loginEnabledEnabled() {
    return applicationContext.getConfiguration().isDisplayLoginSettings();
  }

  @Computed("loginEnabled")
  public boolean isLoginEnabled() {
    return preferencesContext.isLoginEnabled();
  }

  @Computed("manualEnabled")
  public boolean isManualEnabled() {
    return applicationContext.getConfiguration().hasManual();
  }

  @JsMethod
  public void setLoginEnabled(final boolean autoSwitch) {
    eventBus.fireEvent(new UserSettingChangeCommand(PreferencesDaemon.ENABLE_LOGIN, autoSwitch));
  }

  @JsMethod
  public void closePopout() {
    navigationContext.deactivatePopout();
  }
}
