/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.notification;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasDestroyed;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import elemental2.dom.HTMLElement;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.util.Notification;
import nl.aerius.wui.util.SchedulerUtil;
import nl.aerius.wui.vue.directives.VectorDirective;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.wui.application.command.NotificationsClearCommand;
import nl.overheid.aerius.wui.application.command.misc.CloseNotificationDisplayCommand;
import nl.overheid.aerius.wui.application.components.map.NotificationButtonChangeCommand;
import nl.overheid.aerius.wui.application.context.NotificationContext;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.util.ScreenUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    NotificationComponent.class,
    VerticalCollapse.class,
    VerticalCollapseGroup.class
}, directives = {
    VectorDirective.class
})
public class NotificationPanelComponent extends BasicVueComponent implements HasCreated, HasDestroyed {
  private static final NotificationPanelComponentEventBinder EVENT_BINDER = GWT.create(NotificationPanelComponentEventBinder.class);

  interface NotificationPanelComponentEventBinder extends EventBinder<NotificationPanelComponent> {}

  @Prop EventBus eventBus;
  @Prop boolean active;

  @Inject @Data NotificationContext context;

  @Ref HTMLElement notificationPanel;

  private HandlerRegistration handlers;

  @PropDefault("active")
  boolean activeDefault() {
    return false;
  }

  @Override
  public void created() {
    handlers = EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  public void onNotificationButtonChangeCommand(final NotificationButtonChangeCommand c) {
    final HTMLElement btn = c.getValue();
    btn.parentNode.insertBefore(notificationPanel, btn.nextSibling);
    SchedulerUtil.delay(() -> {
      ScreenUtil.attachElementLeft(btn, notificationPanel, ScreenUtil.OFFSET_LEFT);
    });
  }

  @Computed
  public List<Notification> getNotifications() {
    return context.getNotifications();
  }

  @JsMethod
  public void removeAll() {
    eventBus.fireEvent(new NotificationsClearCommand());
  }

  @JsMethod
  public String getInactiveMessageText() {
    return context.getNotificationCount() > 0
        ? M.messages().notificationNumberOfMessages(context.getNotificationCount())
        : M.messages().notificationNoMessages();
  }

  @JsMethod
  public void close() {
    eventBus.fireEvent(new CloseNotificationDisplayCommand());
  }

  @Override
  public void destroyed() {
    if (handlers != null) {
      handlers.removeHandler();
    }
  }
}
