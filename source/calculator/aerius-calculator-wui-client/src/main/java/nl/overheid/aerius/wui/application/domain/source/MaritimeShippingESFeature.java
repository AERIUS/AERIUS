/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source;

import static jsinterop.annotations.JsPackage.GLOBAL;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.shared.domain.v2.source.shipping.maritime.ShippingMovementType;
import nl.overheid.aerius.wui.application.domain.source.shipping.maritime.MaritimeShipping;
import nl.overheid.aerius.wui.vue.ReactivityUtil;

/**
 * Abstract feature object for Maritime Shipping Emission Sources.
 */
@JsType(namespace = GLOBAL, name = "ol.Feature", isNative = true)
public abstract class MaritimeShippingESFeature extends EmissionSubSourceFeature<MaritimeShipping> implements HasMoorings {
  public static @JsOverlay void initMaritime(final MaritimeShippingESFeature feature, final EmissionSourceType type,
      final ShippingMovementType movementType) {
    EmissionSubSourceFeature.initSubSourceFeature(feature, null, type);
    ReactivityUtil.ensureDefault(feature::getMovementType, feature::setMovementType, movementType);
    ReactivityUtil.ensureDefault(feature::getMooringAId, feature::setMooringAId);
    ReactivityUtil.ensureDefault(feature::getMooringBId, feature::setMooringBId);
  }

  @JsonIgnore
  public final @JsOverlay ShippingMovementType getMovementType() {
    return Js.uncheckedCast(get("movementType"));
  }

  public final @JsOverlay void setMovementType(final ShippingMovementType movementType) {
    set("movementType", movementType);
  }

  public final @JsOverlay String getMooringAId() {
    return get("mooringAId");
  }

  public final @JsOverlay void setMooringAId(final String mooringAId) {
    set("mooringAId", mooringAId);
  }

  public final @JsOverlay String getMooringBId() {
    return get("mooringBId");
  }

  public final @JsOverlay void setMooringBId(final String mooringBId) {
    set("mooringBId", mooringBId);
  }

}
