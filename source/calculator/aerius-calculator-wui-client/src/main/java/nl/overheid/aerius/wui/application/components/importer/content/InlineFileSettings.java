/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.importer.content;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.HorizontalCollapse;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.command.importer.RefreshFileCommand;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;

@Component(components = {
    HorizontalCollapse.class,
    SimplifiedListBoxComponent.class,
    ValidationBehaviour.class,
})
public class InlineFileSettings extends ErrorWarningValidator {
  @Prop(required = true) FileUploadStatus file;
  @Prop(required = true) EventBus eventBus;

  @Inject @Data ApplicationContext applicationContext;

  @Computed
  public List<Substance> getSubstances() {
    return applicationContext.getConfiguration().getImportSubstances();
  }

  @Computed
  public List<SituationType> getSituationTypes() {
    return applicationContext.getConfiguration().getAvailableSituationTypes();
  }

  @Computed("requiresSubstanceSelector")
  public boolean requiresSubstanceSelector() {
    // Currently this flag always means substance selection
    return file != null && file.isUserInput();
  }

  @Computed
  public Substance getSelectedSubstance() {
    return file.getSubstance();
  }

  @JsMethod
  public void selectSubstance(final Substance substance) {
    file.setSubstance(substance);
    eventBus.fireEvent(new RefreshFileCommand(file));
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return requiresSubstanceSelector() && file.getSubstance() == null;
  }
}
