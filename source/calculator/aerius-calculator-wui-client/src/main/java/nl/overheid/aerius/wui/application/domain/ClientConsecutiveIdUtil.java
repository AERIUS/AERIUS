/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain;

import java.util.List;

import nl.overheid.aerius.shared.util.ConsecutiveIdUtil;

public class ClientConsecutiveIdUtil {
  /**
   * Finds the next available ID in the list, assigns it to the HasStringId and adds
   * it to a list
   *
   * @param collection The collection to be added to
   * @param item    The item to be added
   */
  public static <F extends ItemWithStringId> void addAndSetId(final List<F> collection, final F item) {
    final ConsecutiveIdUtil.IndexIdPair newId = getConsecutiveIdUtil(collection).generate();

    item.persistId(newId.getId());
    collection.add(newId.getIndex(), item);
  }

  public static <F extends ItemWithStringId> ConsecutiveIdUtil getConsecutiveIdUtil(final List<F> collection) {
    return new ConsecutiveIdUtil(
        i -> Integer.toString(i + 1),
        i -> collection.get(i).retrieveId(),
        () -> collection.size());
  }

  /**
   * Finds the next available ID in the list and returns it
   *
   * @param collection The feature collection to be added to
   */
  public static <F extends ItemWithStringId> String getNextLabel(final List<F> collection) {
    return getConsecutiveIdUtil(collection).generate().getId();
  }
}
