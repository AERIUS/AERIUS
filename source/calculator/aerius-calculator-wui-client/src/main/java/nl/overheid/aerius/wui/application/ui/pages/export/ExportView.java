/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.ui.pages.export;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.Vue;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.aerius.wui.vue.directives.VectorDirective;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.export.ExportAppendix;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.wui.application.command.ResetEdgeEffectReportExportSettingsCommand;
import nl.overheid.aerius.wui.application.command.exporter.ExportStartCommand;
import nl.overheid.aerius.wui.application.command.misc.NavigateExternalContentViewCommand;
import nl.overheid.aerius.wui.application.components.input.CheckBoxComponent;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.InputWarningComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.modify.ModifyCancelSaveComponent;
import nl.overheid.aerius.wui.application.components.notice.NoticeRule;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.toggle.ToggleButtons;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ExportContext;
import nl.overheid.aerius.wui.application.context.ExportContext.ClientJobType;
import nl.overheid.aerius.wui.application.context.ExternalContentContext;
import nl.overheid.aerius.wui.application.context.GenericValidationContext;
import nl.overheid.aerius.wui.application.context.NavigationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.UserContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.domain.export.ExportOptions;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.ui.pages.calculate.CalculateValidations;
import nl.overheid.aerius.wui.application.ui.pages.export.ExportValidators.ExportValidations;
import nl.overheid.aerius.wui.application.ui.pages.export.types.ExportCalculationView;
import nl.overheid.aerius.wui.application.ui.pages.export.types.ExportInputView;
import nl.overheid.aerius.wui.application.ui.pages.export.types.ExportPdfView;
import nl.overheid.aerius.wui.config.ApplicationFlags;

@Component(customizeOptions = {
    ExportValidators.class
}, directives = {
    VectorDirective.class,
}, components = {
    LabeledInputComponent.class,
    InputWarningComponent.class,
    InputErrorComponent.class,
    ExportHistoryComponent.class,
    ExportInputView.class,
    ExportCalculationView.class,
    ExportPdfView.class,
    ToggleButtons.class,
    CheckBoxComponent.class,
    ModifyCancelSaveComponent.class,
    VerticalCollapse.class,
})
public class ExportView extends ErrorWarningValidator implements HasCreated, HasValidators<ExportValidations> {
  @Prop EventBus eventBus;

  @Inject @Data ApplicationContext appContext;
  @Inject @Data ScenarioContext scenarioContext;
  @Inject @Data ExportContext exportContext;
  @Inject @Data UserContext userContext;
  @Inject @Data ApplicationFlags flags;

  @Inject @Data GenericValidationContext validationContext;
  @Inject NavigationContext navigationContext;

  @Data String emailV;
  @Data boolean protectExport;

  @JsProperty(name = "$v") ExportValidations validation;

  @Computed
  public Theme getTheme() {
    return appContext.getTheme();
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
    validationContext.reset();
    emailV = exportContext.getEmail();
  }

  @Computed
  List<ClientJobType> getAvailableJobTypes() {
    return Arrays.asList(ClientJobType.SOURCES, ClientJobType.CALCULATION, ClientJobType.REPORT);
  }

  @Override
  @Computed
  public ExportValidations getV() {
    return validation;
  }

  @Computed
  public List<SituationContext> getSituations() {
    return scenarioContext.getSituations();
  }

  @Computed
  public void setEmail(final String email) {
    exportContext.setEmail(email);
    emailV = email;
  }

  @Computed
  public String getEmail() {
    return exportContext.getEmail();
  }

  @Computed
  public ClientJobType getToggleSelect() {
    return exportContext.getJobType();
  }

  @Computed("metaDataEnabled")
  protected boolean metaDataEnabled() {
    return appContext.getTheme() != Theme.NCA;
  }

  @Computed("showPrivacyStatement")
  protected boolean isShowPrivacyStatement() {
    return appContext.isAppFlagEnabled(AppFlag.SHOW_ABOUT_MENU)
        && appContext.isAppFlagEnabled(AppFlag.SHOW_PRIVACY_STATEMENT);
  }

  @JsMethod
  public void navigatePrivacyPolicy() {
    eventBus.fireEvent(new NavigateExternalContentViewCommand(ExternalContentContext.PRIVACY_STATEMENT));
  }

  @Computed
  public boolean isLoggedIn() {
    return userContext.isLoggedIn();
  }

  @JsMethod
  public void setToggle(final ClientJobType jobType) {
    exportContext.setJobType(jobType);
    eventBus.fireEvent(new ResetEdgeEffectReportExportSettingsCommand());
  }

  @Computed
  public String getExportDescription() {
    final String description;

    switch (getToggleSelect()) {
    case SOURCES:
      description = M.messages().exportTypeDescriptionSources();
      break;
    case CALCULATION:
      description = M.messages().exportTypeDescriptionCalculation();
      break;
    case REPORT:
      description = M.messages().exportTypeDescriptionReport(appContext.getTheme());
      break;
    default:
      description = null;
      break;
    }

    return description;
  }

  @JsMethod
  public String emailError() {
    final String email = getEmail();
    return email == null || "".equals(email)
        ? i18n.validationRequired()
        : i18n.errorEmail(email);
  }

  @Computed
  public Stream<NoticeRule> getExportErrors() {
    final SituationComposition composition = exportContext.getSituationComposition();
    // Return an empty stream when there is no composition
    if (composition == null) {
      return Stream.of();
    }

    final int situationLimit = appContext.getConfiguration().getMaxNumberOfSituations();
    final ClientJobType type = getToggleSelect();

    return Stream.of(
        NoticeRule.create(
            () -> CalculateValidations.hasTooFewSituations(composition),
            () -> M.messages().calculateExportSituationNoneSelected()),
        NoticeRule.create(
            () -> (CalculateValidations.hasTooManySituationsForType(composition, exportContext.getCalculationOptions().getCalculationJobType())
                || CalculateValidations.hasTooManySituations(composition, appContext.getConfiguration()))
                && type != ClientJobType.SOURCES,
            () -> M.messages().calculateSituationLimit(situationLimit)),
        NoticeRule.create(
            () -> CalculateValidations.hasNoEmissionsInAnyOfComposition(composition)
                && type != ClientJobType.SOURCES,
            () -> i18n.exportErrorNoEmissions()),
        NoticeRule.create(
            () -> CalculateValidations.hasInvalidCustomPointsOption(exportContext.getCalculationOptions(), scenarioContext)
                && type != ClientJobType.SOURCES
                && type != ClientJobType.REPORT,
            () -> i18n.calculateInvalidCustomPointsOption()),
        NoticeRule.create(
            () -> CalculateValidations.isInvalidFormalAssessmentCalculation(composition)
                && type == ClientJobType.REPORT
                && exportContext.getCalculationJob().getCalculationOptions().getCalculationJobType() == CalculationJobType.PROCESS_CONTRIBUTION,
            () -> i18n.calculateExportPermitProposedRequired()));
  }

  @Computed
  public List<NoticeRule> getExportErrorsShowing() {
    return getExportErrors()
        .filter(v -> v.showing() && displayProblems)
        .collect(Collectors.toList());
  }

  @Computed
  public Stream<NoticeRule> getExportWarnings() {
    return Stream.of();
  }

  @Computed
  public List<NoticeRule> getExportWarningsShowing() {
    return getExportWarnings()
        .filter(v -> v.showing() && displayProblems)
        .collect(Collectors.toList());
  }

  @JsMethod
  public void setExportNotificationState(final boolean value) {
    if (value) {
      // Unset and set in a tick because we want to trigger a particular response in listeners
      validationContext.setDisplayFormProblems(!value);
      Vue.nextTick(() -> validationContext.setDisplayFormProblems(value));
    } else {
      validationContext.setDisplayFormProblems(value);
    }
    displayProblems(value);
  }

  @JsMethod
  public void startExport() {
    final ClientJobType jobType = exportContext.getJobType();
    final CalculationJobType calculationJobType = exportContext.getCalculationOptions().getCalculationJobType();
    if (ClientJobType.REPORT == jobType && calculationJobType == CalculationJobType.PROCESS_CONTRIBUTION) {
      exportContext.setExportType(ExportType.PDF_PAA);
    } else if (ClientJobType.REPORT == jobType && calculationJobType == CalculationJobType.DEPOSITION_SUM) {
      exportContext.setExportType(ExportType.PDF_PROCUREMENT_POLICY);
    } else if (ClientJobType.REPORT == jobType && calculationJobType == CalculationJobType.SINGLE_SCENARIO) {
      exportContext.setExportType(ExportType.PDF_SINGLE_SCENARIO);
    } else if (ClientJobType.CALCULATION == jobType) {
      // If exporting ADMS or OPS source files the export type is already set and should not be updated.
      // Do nothing.
      if (!isModelSourceExport()) {
        exportContext.setExportType(ExportType.GML_WITH_RESULTS);
      }
    } else if (ClientJobType.SOURCES == jobType) {
      exportContext.setExportType(ExportType.GML_SOURCES_ONLY);
    }
    final CalculationJobContext calculationJob = exportContext.getCalculationJob();

    startExport(jobType, exportContext.getExportType(), exportContext.getAppendices(), exportContext.getEmail(),
        calculationJob == null ? null : calculationJob.getName());
  }

  private void startExport(final ClientJobType clientJobType, final ExportType exportType, final Set<ExportAppendix> appendices, final String email,
      final String name) {
    final ExportOptions exportOptions = new ExportOptions();
    exportOptions.setEmail(email);
    exportOptions.setProtectJob(protectExport);
    exportOptions.setJobType(clientJobType.toJobType());
    exportOptions.setExportType(exportType);
    exportOptions.setDemoMode(flags.isDemoMode());
    exportOptions.setAppendices(appendices);
    exportOptions.setName(name);
    eventBus.fireEvent(new ExportStartCommand(exportOptions));
  }

  /**
   * @return true if export type is ADMS or OPS
   */
  private boolean isModelSourceExport() {
    return exportContext.getExportType() == ExportType.ADMS
        || exportContext.getExportType() == ExportType.OPS;
  }

  @Computed("isWarnings")
  public boolean isWarnings() {
    return getExportWarnings()
        .anyMatch(v -> v.hasNotice());
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return validation.emailV.invalid
        || getExportErrors()
            .anyMatch(v -> v.hasNotice())
        || exportContext.getSituationComposition() == null
        || exportContext.getCalculationOptions() == null
        || hasChildError("export");
  }

  @JsMethod
  public void closeWarning() {
    setExportNotificationState(false);
  }

  @JsMethod
  public void closeError() {
    setExportNotificationState(false);
  }
}
