/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.shipping.inland;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasDestroyed;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.geom.Geometry;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.wui.application.command.DetermineWaterwayCommand;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.toggle.ToggleButtons;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.shipping.inland.InlandWaterway;
import nl.overheid.aerius.wui.application.domain.source.shipping.inland.WaterwayDirection;
import nl.overheid.aerius.wui.application.event.WaterwayResultsEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourcePersistGeometryEvent;
import nl.overheid.aerius.wui.vue.BasicVueEventComponent;
import nl.overheid.aerius.wui.vue.DebugDirective;

@Component(name = "aer-inland-shipping-waterway-editor", directives = {
    DebugDirective.class
}, components = {
    LabeledInputComponent.class,
    ToggleButtons.class,
})
public class WaterwayComponent extends BasicVueEventComponent implements HasDestroyed, HasMounted {
  private static final WaterwayComponentEventBinder EVENT_BINDER = GWT.create(WaterwayComponentEventBinder.class);

  interface WaterwayComponentEventBinder extends EventBinder<WaterwayComponent> {}

  @Prop EventBus eventBus;
  @Prop Geometry geometry;
  @Prop @JsProperty InlandWaterway waterway;

  @Data WaterwayDirection toggleSelect;
  @Data @Inject ApplicationContext applicationContext;

  private boolean listening;

  @Override
  public void created() {
    setEventBus(eventBus);
    listening = true;
  }

  @Watch(value = "waterway", isImmediate = true)
  public void onWaterwayChange() {
    if (waterway.getDirection() != null) {
      setToggle(waterway.getDirection());
    }
  }

  @EventHandler
  public void onEmissionSourcePersistGeometryEvent(final EmissionSourcePersistGeometryEvent e) {
    determineWaterway(e.getValue());
  }

  @EventHandler
  public void onWaterwayResultsEvent(final WaterwayResultsEvent c) {
    setWaterwayCode(c.getValue().getWaterwayCode());
    setToggle(c.getValue().getDirection());
  }

  @Computed("isDirectionRelevant")
  public boolean isDirectionRelevant() {
    if (!waterway.getWaterwayCode().isEmpty()) {
      final InlandWaterwayCategory result = applicationContext.getConfiguration().getSectorCategories().getInlandShippingCategories()
          .getWaterwayCategoryByCode(waterway.getWaterwayCode());
      return result != null && result.isDirectionRelevant();
    }
    return false;
  }

  @Computed
  protected List<InlandWaterwayCategory> getWaterwayCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getInlandShippingCategories().getWaterwayCategories();
  }

  @Computed
  protected String getWaterwayCode() {
    return waterway.getWaterwayCode();
  }

  @Computed
  protected void setWaterwayCode(final String waterwayCode) {
    waterway.setWaterwayCode(waterwayCode);
    if (isDirectionRelevant()) {
      waterway.setDirection(WaterwayDirection.UPSTREAM);
    }
  }

  @JsMethod
  protected void setToggle(final WaterwayDirection waterwayDirection) {
    toggleSelect = waterwayDirection;
    waterway.setDirection(waterwayDirection);
  }

  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }

  @Override
  public void mounted() {
    // Try to determine waterway once, otherwise wait for geometry to be drawn.
    determineWaterway(geometry);
  }

  @Override
  public void destroyed() {
    listening = false;
  }

  private void determineWaterway(final Geometry geometry) {
    // Only determine if not already set and if we actually have a geometry to base it on.
    if (listening && getWaterwayCode().isEmpty() && geometry != null) {
      eventBus.fireEvent(new DetermineWaterwayCommand(geometry));
    }
  }
}
