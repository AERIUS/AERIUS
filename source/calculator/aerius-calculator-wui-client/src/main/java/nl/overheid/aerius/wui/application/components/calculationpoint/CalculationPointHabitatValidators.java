/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.calculationpoint;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.aerius.vuelidate.ValidationOptions;
import nl.aerius.vuelidate.Validations;
import nl.aerius.vuelidate.ValidatorBuilder;

/**
 * Validators for habitats or species of a calculation point editing.
 */
class CalculationPointHabitatValidators extends ValidationOptions<CalculationPointHabitatDetailView> {

  private static final int MAX_NAME_LENGTH = 32;

  @JsType(isNative = true, namespace = JsPackage.GLOBAL)
  static class CalculationPointHabitatValidations extends Validations {
    public @JsProperty Validations nameV;
    public @JsProperty Validations criticalLevelNOxV;
    public @JsProperty Validations criticalLevelNH3V;
    public @JsProperty Validations criticalLoadV;
  }

  @Override
  protected void constructValidators(final Object options, final ValidatorInstaller v) {
    final CalculationPointHabitatDetailView instance = Js.uncheckedCast(options);

    v.install("nameV", () -> instance.nameV = null, ValidatorBuilder.create()
        .required()
        .maxLength(MAX_NAME_LENGTH));
    v.install("criticalLevelNOxV", () -> instance.criticalLevelNOxV = null, ValidatorBuilder.create()
        .decimal()
        .minValue(0));
    v.install("criticalLevelNH3V", () -> instance.criticalLevelNH3V = null, ValidatorBuilder.create()
        .decimal()
        .minValue(0));
    v.install("criticalLoadV", () -> instance.criticalLoadV = null, ValidatorBuilder.create()
        .decimal()
        .minValue(0));
  }
}
