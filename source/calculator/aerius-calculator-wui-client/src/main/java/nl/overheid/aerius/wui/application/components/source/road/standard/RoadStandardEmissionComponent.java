/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.road.standard;

import java.util.HashSet;
import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategories;
import nl.overheid.aerius.shared.domain.sector.category.SimpleCategory;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.shared.domain.v2.source.road.TrafficDirection;
import nl.overheid.aerius.wui.application.components.input.InputWithUnitComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.MinimalInputComponent;
import nl.overheid.aerius.wui.application.components.source.road.standard.RoadStandardValidators.RoadStandardValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.road.StandardVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.ValuesPerVehicleTypes;
import nl.overheid.aerius.wui.vue.DebugDirective;

@Component(customizeOptions = {
    RoadStandardValidators.class
}, directives = {
    ValidateDirective.class,
    DebugDirective.class
}, components = {
    LabeledInputComponent.class,
    MinimalInputComponent.class,
    InputWithUnitComponent.class,
    RoadStandardVehicleTypeRowComponent.class,
    ValidationBehaviour.class
})
public class RoadStandardEmissionComponent extends ErrorWarningValidator implements HasCreated,
    HasValidators<RoadStandardValidations> {

  /**
   * When maximum speed is strictly enforced (for instance by using 'traject controle'), add this marker.
   */
  private static final String STRICT = "s";

  @Prop @JsProperty StandardVehicles standardVehicles;
  @Prop @JsProperty String roadAreaCode;
  @Prop @JsProperty String roadTypeCode;
  @Prop TrafficDirection trafficDirection;
  @Data String roadSpeedV;

  @Prop(required = true) boolean stagnationEnabled;
  @Prop(required = true) boolean maximumSpeedDropDown;

  @Data @Inject ApplicationContext applicationContext;

  @JsProperty(name = "$v") RoadStandardValidations validations;

  @Watch(value = "standardVehicles", isImmediate = true)
  public void onStandardVehiclesChange() {
    final int maximumSpeed = standardVehicles.getMaximumSpeed();
    if (maximumSpeed > 0) {
      roadSpeedV = standardVehicles.isStrictEnforcement() ? STRICT + maximumSpeed : String.valueOf(maximumSpeed);
    } else {
      roadSpeedV = "";
    }
  }

  @Watch(value = "roadTypeCode", isImmediate = true)
  public void onRoadTypeCodeChange() {
    onStandardVehiclesChange();
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Override
  @Computed
  public RoadStandardValidations getV() {
    return validations;
  }

  @Computed
  protected String getTimeUnit() {
    return standardVehicles.getTimeUnit().name();
  }

  @Computed
  protected void setTimeUnit(final String timeUnit) {
    standardVehicles.setTimeUnit(TimeUnit.valueOf(timeUnit));
  }

  @Computed
  protected String getMaximumSpeed() {
    return roadSpeedV;
  }

  @Computed
  protected void setMaximumSpeed(final String maximumSpeed) {
    try {
      final int maximumSpeedInt = Integer.parseInt(maximumSpeed.replace(STRICT, ""));
      standardVehicles.setMaximumSpeed(maximumSpeedInt);
      standardVehicles.setStrictEnforcement(maximumSpeed.contains(STRICT));
    } catch (final NumberFormatException e) {
      // Ignore, let Vuelidate pick this up
    }
    roadSpeedV = maximumSpeed;
  }

  @Computed
  protected HashSet<String> getMaximumSpeedCategories() {
    final HashSet<String> list = new HashSet<>();
    final List<Integer> strictSpeeds = getRoadCategories().getMaximumSpeedCategoriesStrict(roadAreaCode, roadTypeCode);
    for (final Integer speed : getRoadCategories().getMaximumSpeedCategories(roadAreaCode, roadTypeCode)) {
      list.add(String.valueOf(speed));
      if (strictSpeeds.contains(speed)) {
        list.add(STRICT + speed);
      }
    }
    return list;
  }

  private RoadEmissionCategories getRoadCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getRoadEmissionCategories();
  }

  @Computed("valuesPerVehicleType")
  protected ValuesPerVehicleTypes getValuesPerVehicleType() {
    return standardVehicles.getValuesPerVehicleTypes();
  }

  @Computed("vehicleTypes")
  protected List<SimpleCategory> getVehicleTypes() {
    return applicationContext.getConfiguration().getSectorCategories().getRoadEmissionCategories().getVehicleTypes();
  }

  @Computed("isInvalid")
  protected boolean isInvalid() {
    return isSpeedCategoriesAvailable() && validations.roadSpeedV.invalid;
  }

  @JsMethod
  protected String getMaximumSpeedMessage(final String maximumSpeed) {
    final int max = Integer.parseInt(maximumSpeed.replace(STRICT, ""));
    return StandardRoadCategoryUtil.roadCategoryDisplayText(i18n, max, maximumSpeed.contains(STRICT));
  }

  @JsMethod
  protected String roadSpeedRequiredError() {
    return i18n.errorRoadSpeedRequired();
  }

  @JsMethod
  protected boolean isSpeedCategoriesAvailable() {
    final List<Integer> speedCategories = getRoadCategories().getMaximumSpeedCategories(roadAreaCode, roadTypeCode);
    return speedCategories.size() > 1;
  }
}
