/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.geo;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import nl.aerius.geo.domain.IsLayer;
import nl.overheid.aerius.wui.application.geo.layers.IsMarkerLayer;

@Singleton
public class ScenarioLayerContext {
  private final List<IsLayer<?>> scenarioLayers = new ArrayList<>();
  private final List<IsLayer<?>> resultLayers = new ArrayList<>();
  private final List<IsLayer<?>> calculateLayers = new ArrayList<>();

  private IsMarkerLayer<?> sourceLayerGroup;
  private IsLayer<?> roadInputLayer;
  private IsMarkerLayer<?> jobInputLayerGroup;
  private IsMarkerLayer<?> calculationPointLayerGroup;
  private IsLayer<?> resultLayerGroup;
  private IsLayer<?> metSiteLayer;
  private IsLayer<?> archiveProjectMarkerLayer;

  public void setSourceLayerGroup(final IsMarkerLayer<?> layer) {
    this.sourceLayerGroup = layer;
    scenarioLayers.add(layer);
  }

  public void setRoadInputLayer(final IsLayer<?> layer) {
    this.roadInputLayer = layer;
    scenarioLayers.add(layer);
  }

  public void setJobInputLayerGroup(final IsMarkerLayer<?> layer) {
    this.jobInputLayerGroup = layer;
    scenarioLayers.add(layer);
  }

  public void setCalculationPointLayerGroup(final IsMarkerLayer<?> layer) {
    this.calculationPointLayerGroup = layer;
    scenarioLayers.add(layer);
  }

  public void addResultsLayer(final IsLayer<?> layer) {
    resultLayers.add(layer);
  }

  public void setResultLayerGroup(final IsLayer<?> layer) {
    this.resultLayerGroup = layer;
    resultLayers.add(layer);
  }

  public void setMetSiteLayer(final IsLayer<?> metSiteLayer) {
    this.metSiteLayer = metSiteLayer;
    calculateLayers.add(metSiteLayer);
  }

  public void setArchiveProjectMarkerLayer(final IsLayer<?> archiveProjectMarkerLayer) {
    this.archiveProjectMarkerLayer = archiveProjectMarkerLayer;
  }

  public IsMarkerLayer<?> getSourceLayerGroup() {
    return sourceLayerGroup;
  }

  public IsLayer<?> getRoadInputLayer() {
    return roadInputLayer;
  }

  public IsMarkerLayer<?> getJobInputLayerGroup() {
    return jobInputLayerGroup;
  }

  public IsMarkerLayer<?> getCalculationPointLayerGroup() {
    return calculationPointLayerGroup;
  }

  public IsLayer<?> getResultLayerGroup() {
    return resultLayerGroup;
  }

  public IsLayer<?> getMetSiteLayer() {
    return metSiteLayer;
  }

  public IsLayer<?> getArchiveProjectMarkerLayer() {
    return archiveProjectMarkerLayer;
  }

  public List<IsLayer<?>> getInputLayers() {
    return scenarioLayers;
  }

  public List<IsLayer<?>> getResultLayers() {
    return resultLayers;
  }

  public List<IsLayer<?>> getCalculateLayers() {
    return calculateLayers;
  }

  public void clear() {
    scenarioLayers.clear();
    resultLayers.clear();
  }
}
