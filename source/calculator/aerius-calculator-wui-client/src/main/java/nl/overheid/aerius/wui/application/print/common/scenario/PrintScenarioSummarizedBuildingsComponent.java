/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.print.common.scenario;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.sector.SectorIcon;
import nl.overheid.aerius.shared.util.ColorUtil;
import nl.overheid.aerius.wui.application.components.common.StyledPointComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.print.common.PrintSection;
import nl.overheid.aerius.wui.application.util.SectorColorUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

/**
 * Basic component outputting a table displaying summarized buildings of a particular scenario
 */
@Component(components = {
    PrintSection.class,
    StyledPointComponent.class,
})
public class PrintScenarioSummarizedBuildingsComponent extends BasicVueComponent {
  @Prop SituationContext situation;

  @Inject @Data ApplicationContext appContext;

  @Computed
  public List<BuildingFeature> getBuildings() {
    return situation.getBuildings();
  }

  @JsMethod
  public List<EmissionSourceFeature> getBuildingSources(final BuildingFeature feature) {
    return situation.getSources().stream()
        .filter(v -> v.getCharacteristics().isBuildingInfluence())
        .filter(v -> v.getCharacteristics().getBuildingGmlId() == feature.getGmlId())
        .collect(Collectors.toList());
  }

  @JsMethod
  public String getSectorColor(final EmissionSourceFeature source) {
    return ColorUtil.webColor(getSectorIcon(source).getColor());
  }

  @JsMethod
  public SectorIcon getSectorIcon(final EmissionSourceFeature source) {
    return SectorColorUtil.getSectorIcon(appContext.getConfiguration(), source.getSectorId());
  }
}
