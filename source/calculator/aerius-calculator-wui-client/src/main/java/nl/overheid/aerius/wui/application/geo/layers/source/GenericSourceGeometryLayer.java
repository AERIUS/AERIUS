/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source;

import javax.inject.Inject;

import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;

import ol.color.Color;
import ol.style.Style;

import nl.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.wui.application.domain.ItemWithStringIdUtil;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.road.RoadEmissionSourceTypes;
import nl.overheid.aerius.wui.application.geo.layers.FeatureStyle;
import nl.overheid.aerius.wui.application.geo.layers.LabelStyle;
import nl.overheid.aerius.wui.application.geo.util.GeoType;

/**
 * Layer to display generic source geometries on the map.
 */
public class GenericSourceGeometryLayer extends SourceGeometryLayer {

  @Inject
  public GenericSourceGeometryLayer(@Assisted final LayerInfo info, final EventBus eventBus, @Assisted final int zIndex) {
    super(info, eventBus, zIndex);
  }

  @Override
  protected Style[] createStyle(final EmissionSourceFeature feature, final double resolution) {
    if (GeoType.POINT.is(feature.getGeometry())) {
      return getPointStyle(Color.getColorFromString(LabelStyle.EMISSION_SOURCE.getLabelBackgroundColor(feature)));
    }

    if (RoadEmissionSourceTypes.isRoadSource(feature)) {
      return NO_RENDERING;
    }

    if (isBeingObsoleted(feature.getId(), getGeometryEditingFeatureId())) {
      return FeatureStyle.OBSOLETE_STYLING;
    }

    if (ItemWithStringIdUtil.isTemporaryFeature(feature)) {
      return FeatureStyle.EDIT_STYLING;
    }

    return FeatureStyle.DEFAULT_STYLING;
  }

  /**
   * Return whether the feature id matches the id being edited (which will be
   * prepended with -)
   */
  private boolean isBeingObsoleted(final String featureId, final String editingId) {
    if (editingId != null && ItemWithStringIdUtil.isTemporaryLabel(editingId)) {
      final String cleanId = ItemWithStringIdUtil.cleanStringId(editingId);

      return featureId.equals(cleanId);
    }

    return false;
  }
}
