/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.shipping.inland;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.wui.vue.ReactivityUtil;

/**
 * Client side implementation of CustomMooringInlandShipping props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class CustomMooringInlandShipping extends MooringInlandShipping {

  private CustomInlandShippingEmissionProperties emissionProperties;

  public static final @JsOverlay CustomMooringInlandShipping create() {
    final CustomMooringInlandShipping props = Js.uncheckedCast(JsPropertyMap.of());
    init(props);
    return props;
  }

  public static final @JsOverlay void init(final CustomMooringInlandShipping props) {
    MooringInlandShipping.init(props, MooringInlandShippingType.CUSTOM);
    ReactivityUtil.ensureJsPropertySupplied(props, "emissionProperties", props::setEmissionProperties, CustomInlandShippingEmissionProperties::create);
    ReactivityUtil.ensureInitialized(props::getEmissionProperties, CustomInlandShippingEmissionProperties::init);
  }

  public final @JsOverlay CustomInlandShippingEmissionProperties getEmissionProperties() {
    return emissionProperties;
  }

  public final @JsOverlay void setEmissionProperties(final CustomInlandShippingEmissionProperties emissionProperties) {
    this.emissionProperties = emissionProperties;
  }

}
