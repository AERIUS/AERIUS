/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.importer.content;

import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.js.file.SituationStats;
import nl.overheid.aerius.wui.application.components.input.CheckBoxComponent;
import nl.overheid.aerius.wui.application.components.source.DetailStatComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.importer.FileContext;
import nl.overheid.aerius.wui.application.domain.importer.ImportFilter;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    VerticalCollapse.class,
    CheckBoxComponent.class,
    DetailStatComponent.class,
})
public class SingleFileContentsComponent extends BasicVueComponent {
  @Prop(required = true) SituationStats stats;
  @Prop(required = true) FileUploadStatus file;
  @Prop(required = true) EventBus eventBus;

  @Inject @Data FileContext fileContext;
  @Inject ApplicationContext applicationContext;

  @JsMethod
  public void selectFilter(final ImportFilter filter, final boolean select) {
    if (select) {
      file.getFilters().add(filter);
    } else {
      file.getFilters().remove(filter);
    }
  }

  @Computed("hasSectionTitle")
  public boolean hasSectionTitle() {
    return getSectionTitle() != null;
  }

  @Computed
  public String getSectionTitle() {
    if (file.getSituationStats().isArchive()) {
      return M.messages().importFileContentArchiveContributionHeader();
    } else if (file.getSituationStats().getCalculationPoints() > 0) {
      return M.messages().importFileContentCalculationPointsHeader();
    } else {
      return Optional.ofNullable(file.getImportAction())
          .map(v -> v.getVirtualSituation())
          .map(v -> v.getName())
          .orElse(file.getSituationName());
    }
  }

  @Computed("canHaveCalculationJobImport")
  public boolean canHaveCalculationJobImport() {
    return FileUploadStatus.canHaveCalculationJobImport(file, fileContext.getFiles(),
        t -> applicationContext.getConfiguration().isAllowed(t));
  }

  @JsMethod
  public void selectCalculationJobFilter(final boolean select) {
    eventBus.fireEvent(new ImportFilterGroupSelectionCommand(file, ImportFilter.CALCULATION_JOB, select));
    if (!select) {
      this.selectResultsFilter(select);
    }
  }

  @JsMethod
  public void selectResultsFilter(final boolean select) {
    if (!select || file.getFilters().isAllowed(ImportFilter.CALCULATION_JOB)) {
      eventBus.fireEvent(new ImportFilterGroupSelectionCommand(file, ImportFilter.RESULTS, select));
    }
  }

  @JsMethod
  public boolean getFilter(final ImportFilter filter) {
    return file.getFilters().isAllowed(filter);
  }
}
