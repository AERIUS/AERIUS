/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.farmland;

import elemental2.core.JsArray;

import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.wui.application.components.modify.ModifyListActions;
import nl.overheid.aerius.wui.application.domain.source.farmland.FarmlandActivity;
import nl.overheid.aerius.wui.application.domain.source.farmland.StandardFarmlandActivity;
import nl.overheid.aerius.wui.application.domain.source.util.EmissionSourceFeatureUtil;

public class FarmlandEmissionEditorActivity extends BasicEventComponent implements ModifyListActions {
  private FarmlandEmissionEditorComponent view = null;
  private FarmlandActivity selectedSource = null;

  @Override
  public void newEntity() {
    final FarmlandActivity newActivity = StandardFarmlandActivity.create(null);
    final JsArray<FarmlandActivity> activities = view.source.getSubSources();
    activities.push(newActivity);
    view.selectedIndex = activities.length - 1;
    view.source.setSubSources(activities);
    edit();
  }

  @Override
  public void copy() {
    final JsArray<FarmlandActivity> subSources = view.source.getSubSources();
    subSources.push(EmissionSourceFeatureUtil.clone(selectedSource));
    view.source.setSubSources(subSources);
    resetSelected();
    view.selectedIndex = subSources.length - 1;
    edit();
  }

  @Override
  public void delete() {
    final JsArray<FarmlandActivity> subSources = view.source.getSubSources();
    final Integer id = view.selectedIndex;
    if (id.equals(subSources.length)) {
      subSources.pop();
    } else {
      subSources.splice(id, 1);
    }
    view.source.setSubSources(subSources);
    resetSelected();
  }

  @Override
  public void edit() {
    view.editSource();
  }

  public void setView(final FarmlandEmissionEditorComponent view) {
    this.view = view;
  }

  public void resetSelected() {
    view.resetSelected();
  }

  public void selectedSource(final FarmlandActivity selectedSource) {
    this.selectedSource = selectedSource;
  }
}
