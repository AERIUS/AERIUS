/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.scenario;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.wui.application.command.calculation.CalculationPointDeleteAllCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointDeleteCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointDeleteSelectedCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointDeselectCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointEditCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointEditSelectedCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointSelectCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointToggleSelectCommand;
import nl.overheid.aerius.wui.application.command.source.DeselectAllFeaturesCommand;
import nl.overheid.aerius.wui.application.context.CalculationPointListContext;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;
import nl.overheid.aerius.wui.application.event.situation.SituationSwitchEvent;

public class CalculationPointListDaemon extends BasicEventComponent {
  private static final CalculationPointListDaemonEventBinder EVENT_BINDER = GWT.create(CalculationPointListDaemonEventBinder.class);

  interface CalculationPointListDaemonEventBinder extends EventBinder<CalculationPointListDaemon> {
  }

  @Inject private CalculationPointListContext context;

  @EventHandler
  public void onDeselectAllFeaturesCommand(final DeselectAllFeaturesCommand c) {
    context.reset();
  }

  @EventHandler
  public void onCalculationPointEditSelectedCommand(final CalculationPointEditSelectedCommand c) {
    if (!context.hasSingleSelection()) {
      return;
    }

    eventBus.fireEvent(new CalculationPointEditCommand(context.getSingleSelection()));
  }

  @EventHandler
  public void onSituationSwitchEvent(final SituationSwitchEvent e) {
    context.reset();
  }

  @EventHandler
  public void onCalculationPointDeleteSelectedCommand(final CalculationPointDeleteSelectedCommand c) {
    if (!context.hasSelection()) {
      return;
    }

    context.getSelections().forEach(v -> eventBus.fireEvent(new CalculationPointDeleteCommand(v)));
    context.reset();
  }

  @EventHandler
  public void onCalculationPointToggleSelectCommand(final CalculationPointToggleSelectCommand c) {
    final CalculationPointFeature point = c.getValue();

    if (context.isSelected(point)) {
      eventBus.fireEvent(new CalculationPointDeselectCommand(point));
    } else {
      eventBus.fireEvent(new CalculationPointSelectCommand(point, c.isMulti()));
    }
  }

  @EventHandler
  public void onCalculationPointDeselectCommand(final CalculationPointDeselectCommand c) {
    context.removeSelection(c.getValue());
  }

  @EventHandler
  public void onCalculationPointSelectCommand(final CalculationPointSelectCommand c) {
    final CalculationPointFeature point = c.getValue();

    if (c.isMulti()) {
      context.addSelection(point);
    } else {
      context.setSingleSelection(point);
    }
  }

  @EventHandler
  public void onCalculationPointsDeleteAllCommand(final CalculationPointDeleteAllCommand c) {
    context.reset();
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }

}
