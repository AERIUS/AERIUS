/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.results;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.result.CalculationMarker;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.wui.application.context.result.MarkerSortOrder;
import nl.overheid.aerius.wui.application.geo.layers.LegendPair;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.resources.images.markers.MarkerResource;
import nl.overheid.aerius.wui.application.resources.images.markers.MarkerResourceUtil;

/**
 * Describes the relation between a set of calculation markers and their
 * combination image.
 */
public final class CalculationMarkers {
  private static final Map<Set<CalculationMarker>, BiFunction<Theme, ScenarioResultType, MarkerResource>> COMBINATIONS = new HashMap<>();

  private CalculationMarkers() {}

  public static void init() {
    pair(MarkerResourceUtil::getHtot,
        CalculationMarker.M1_HIGHEST_TOTAL);
    pair(MarkerResourceUtil::getHconHtot,
        CalculationMarker.M1_HIGHEST_TOTAL,
        CalculationMarker.M2A_HIGHEST_CONTRIBUTION);
    pair(MarkerResourceUtil::getHincHtot,
        CalculationMarker.M1_HIGHEST_TOTAL,
        CalculationMarker.M2B_HIGHEST_INCREASE);
    pair(MarkerResourceUtil::getHtotHcumHinc,
        CalculationMarker.M1_HIGHEST_TOTAL,
        CalculationMarker.M2B_HIGHEST_INCREASE,
        CalculationMarker.M4_MAX_CUMULATION);
    pair(MarkerResourceUtil::getHtotHcumHdec,
        CalculationMarker.M1_HIGHEST_TOTAL,
        CalculationMarker.M2C_HIGHEST_DECREASE,
        CalculationMarker.M4_MAX_CUMULATION);
    pair(MarkerResourceUtil::getHdecHtot,
        CalculationMarker.M1_HIGHEST_TOTAL,
        CalculationMarker.M2C_HIGHEST_DECREASE);
    pair(MarkerResourceUtil::getHtotHcum,
        CalculationMarker.M1_HIGHEST_TOTAL,
        CalculationMarker.M4_MAX_CUMULATION);
    pair(MarkerResourceUtil::getHmaxHtot,
        CalculationMarker.M1_HIGHEST_TOTAL,
        CalculationMarker.M5_MAX_TOTAL_WITH_EFFECT);
    pair(MarkerResourceUtil::getHtotHconHcum,
        CalculationMarker.M1_HIGHEST_TOTAL,
        CalculationMarker.M2A_HIGHEST_CONTRIBUTION,
        CalculationMarker.M4_MAX_CUMULATION);

    pair(MarkerResourceUtil::getHcon,
        CalculationMarker.M2A_HIGHEST_CONTRIBUTION);
    pair(MarkerResourceUtil::getHconHcum,
        CalculationMarker.M2A_HIGHEST_CONTRIBUTION,
        CalculationMarker.M4_MAX_CUMULATION);
    pair(MarkerResourceUtil::getHinc,
        CalculationMarker.M2B_HIGHEST_INCREASE);
    pair(MarkerResourceUtil::getHincHcum,
        CalculationMarker.M2B_HIGHEST_INCREASE,
        CalculationMarker.M4_MAX_CUMULATION);
    pair(MarkerResourceUtil::getHdec,
        CalculationMarker.M2C_HIGHEST_DECREASE);
    pair(MarkerResourceUtil::getHdecHcum,
        CalculationMarker.M2C_HIGHEST_DECREASE,
        CalculationMarker.M4_MAX_CUMULATION);

    pair(MarkerResourceUtil::getHcum,
        CalculationMarker.M4_MAX_CUMULATION);

    pair(MarkerResourceUtil::getHmax,
        CalculationMarker.M5_MAX_TOTAL_WITH_EFFECT);

    pair(MarkerResourceUtil::getDsProcurementPolicyPass,
        CalculationMarker.PROCUREMENT_POLICY_PASS);
    pair(MarkerResourceUtil::getDsProcurementPolicyFail,
        CalculationMarker.PROCUREMENT_POLICY_FAIL);
  }

  private static void pair(final BiFunction<Theme, ScenarioResultType, MarkerResource> func, final CalculationMarker... markers) {
    COMBINATIONS.put(comb(markers), func);
  }

  private static Set<CalculationMarker> comb(final CalculationMarker... markers) {
    return Stream.of(markers).collect(Collectors.toSet());
  }

  public static MarkerResource getSingleMarkerImage(final CalculationMarker marker, final ScenarioResultType resultType, final Theme theme) {
    return Optional.of(COMBINATIONS)
        .map(v -> v.get(new HashSet<>(Arrays.asList(marker))))
        .map(v -> v.apply(theme, resultType))
        .orElseThrow(() -> new RuntimeException("Could not find marker: "
            + resultType.name() + " > " + theme.name() + " > "
            + marker.name()));
  }

  public static MarkerResource getCombinableMarkerImage(final Set<CalculationMarker> markers, final ScenarioResultType resultType,
      final Theme theme) {
    return Optional.of(COMBINATIONS)
        .map(v -> v.get(markers))
        .map(v -> v.apply(theme, resultType))
        .orElseThrow(() -> new RuntimeException("Could not find combination: "
            + resultType.name() + " > " + theme.name() + " > "
            + markers.stream()
                .map(v -> v.name())
                .collect(Collectors.joining(","))));
  }

  public static List<LegendPair> createMarkerLegend(final Set<CalculationMarker> calculationMarkers,
      final ScenarioResultType scenarioResultType, final Theme theme) {
    return calculationMarkers.stream()
        .map(marker -> new LegendPair(getSingleMarkerImage(marker, scenarioResultType, theme).getDataResource(),
            M.messages().resultsCalculationMarker(scenarioResultType, marker, M.messages().resultsTotalName(theme)),
            new MarkerSortOrder(marker, scenarioResultType)))
        .sorted()
        .collect(Collectors.toList());
  }
}
