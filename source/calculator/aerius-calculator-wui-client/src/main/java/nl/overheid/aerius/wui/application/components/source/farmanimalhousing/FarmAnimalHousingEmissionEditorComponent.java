/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.farmanimalhousing;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.base.Js;

import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalHousingCategories;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalHousingCategory;
import nl.overheid.aerius.wui.application.components.modify.ModifyListComponent;
import nl.overheid.aerius.wui.application.components.source.SubSourceEditorComponent;
import nl.overheid.aerius.wui.application.components.source.SubSourceValidationBehaviour;
import nl.overheid.aerius.wui.application.components.source.validation.SubSourceEmptyError;
import nl.overheid.aerius.wui.application.components.source.validation.SubSourceValidatedRowComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.FarmAnimalHousingESFeature;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmAnimalHousing;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmAnimalHousingType;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmAnimalHousingUtil;
import nl.overheid.aerius.wui.application.domain.source.farm.StandardFarmAnimalHousing;

/**
 * Component to show the list of Farm Animal Housing sub sources.
 */
@Component(components = {
    ModifyListComponent.class,
    SubSourceValidationBehaviour.class,
    FarmAnimalHousingEmissionEditorRowComponent.class,
    SubSourceValidatedRowComponent.class,
    SubSourceEmptyError.class,
    VerticalCollapseGroup.class,
})
public class FarmAnimalHousingEmissionEditorComponent extends SubSourceEditorComponent implements HasCreated {
  @Prop FarmAnimalHousingESFeature source;
  @Data FarmAnimalHousingEmissionEditorActivity presenter;

  @Inject @Data ApplicationContext applicationContext;

  @Override
  public void created() {
    presenter = new FarmAnimalHousingEmissionEditorActivity();
    presenter.setView(this);
  }

  @Computed("farmSource")
  protected FarmAnimalHousingESFeature getFarmSource() {
    return source;
  }

  @JsMethod
  protected boolean hasAnyIncompatibleCombinations() {
    return source.getSubSources().asList().stream()
        .anyMatch(v -> hasIncompatibleCombinations(v));
  }

  @JsMethod
  protected boolean hasIncompatibleCombinations(final FarmAnimalHousing source) {
    if (FarmAnimalHousingType.STANDARD != source.getAnimalHousingType()) {
      return false;
    }

    final StandardFarmAnimalHousing standardFarmAnimalHousing = Js.uncheckedCast(source);
    final FarmAnimalHousingCategory farmAnimalHousingCategory = getCategories()
        .determineHousingCategoryByCode(standardFarmAnimalHousing.getAnimalHousingCode());
    if (farmAnimalHousingCategory == null) {
      // This is a null-check more than anything else - if there is no selected
      // farm animal housing category, return false (i.e. the source is invalid, and we can't
      // determine if there are incompatible combinations, but we don't want a warning
      // sign to show up to reflect that, and will assume an error sign will show up
      // elsewhere)
      return false;
    } else {
      return FarmAnimalHousingUtil.hasIncompatibleCombinations(standardFarmAnimalHousing, getCategories());
    }
  }

  @JsMethod
  protected FarmAnimalHousingCategories getCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getFarmAnimalHousingCategories();
  }

  protected boolean hasWarnings(final FarmAnimalHousing source, final int index) {
    return hasIncompatibleCombinations(source)
        || super.hasWarnings(index);
  }

  @JsMethod
  protected void selectSource(final int index, final FarmAnimalHousing selectedSource) {
    if (selectedIndex.equals(index)) {
      selectedIndex = -1;
      resetSelected();
    } else {
      selectedIndex = index;
      presenter.selectedSource(selectedSource);
      editSource();
    }
  }
}
