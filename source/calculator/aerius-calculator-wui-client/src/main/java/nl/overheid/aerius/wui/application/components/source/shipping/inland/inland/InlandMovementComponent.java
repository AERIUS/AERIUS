/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.shipping.inland.inland;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.InputWithUnitComponent;
import nl.overheid.aerius.wui.application.components.input.MinimalInputComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.inland.inland.InlandMovementValidators.InlandMovementValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.shipping.inland.InlandShipping;
import nl.overheid.aerius.wui.vue.DebugDirective;

@Component(customizeOptions = {
    InlandMovementValidators.class
}, directives = {
    ValidateDirective.class,
    DebugDirective.class
}, components = {
    InputWithUnitComponent.class,
    InputErrorComponent.class,
    MinimalInputComponent.class,
    ValidationBehaviour.class
})
public class InlandMovementComponent extends ErrorWarningValidator implements HasCreated,
    HasValidators<InlandMovementValidations> {
  @Prop @JsProperty InlandShipping source;
  @Prop InlandMovementType movementType;
  @Data String movementV;
  @Data String percentageLadenV;

  @Data @Inject ApplicationContext applicationContext;

  @JsProperty(name = "$v") InlandMovementValidations validation;

  public enum InlandMovementType {
    /*
     * Shipmovement from A to B
     */
    A_TO_B,
    /*
     * Shipmovement from B to A
     */
    B_TO_A
  }

  @Override
  @Computed
  public InlandMovementValidations getV() {
    return validation;
  }

  @Watch(value = "source", isImmediate = true)
  public void onSourceChange() {
    movementV = Integer.toString(isAtoB() ? source.getMovementsAtoBPerTimeUnit() : source.getMovementsBtoAPerTimeUnit());
    percentageLadenV = Integer.toString(isAtoB() ? source.getPercentageLadenAtoB() : source.getPercentageLadenBtoA());
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Computed
  protected String getMovement() {
    movementV = Integer.toString(isAtoB() ? source.getMovementsAtoBPerTimeUnit() : source.getMovementsBtoAPerTimeUnit());
    return movementV;
  }

  @Computed
  protected void setMovement(final String movement) {
    if (isAtoB()) {
      ValidationUtil.setSafeIntegerValue(source::setMovementsAtoBPerTimeUnit, movement, 0);
    } else {
      ValidationUtil.setSafeIntegerValue(source::setMovementsBtoAPerTimeUnit, movement, 0);
    }
    movementV = movement;
  }

  @Computed
  protected String getTimeUnit() {
    return isAtoB() ? source.getTimeUnitAtoB().name() : source.getTimeUnitBtoA().name();
  }

  @Computed
  protected void setTimeUnit(final String timeUnit) {
    if (isAtoB()) {
      source.setTimeUnitAtoB(TimeUnit.valueOf(timeUnit));
    } else {
      source.setTimeUnitBtoA(TimeUnit.valueOf(timeUnit));
    }
  }

  @Computed
  protected String getPercentageLaden() {
    percentageLadenV = Integer.toString(isAtoB() ? source.getPercentageLadenAtoB() : source.getPercentageLadenBtoA());
    return percentageLadenV;
  }

  @Computed
  protected void setPercentageLaden(final String percentageLaden) {
    if (isAtoB()) {
      ValidationUtil.setSafeIntegerValue(source::setPercentageLadenAtoB, percentageLaden, 0);
    } else {
      ValidationUtil.setSafeIntegerValue(source::setPercentageLadenBtoA, percentageLaden, 0);
    }
    percentageLadenV = percentageLaden;
  }

  @Computed("isInvalid")
  protected boolean isInvalid() {
    return validation.movementV.invalid || validation.percentageLadenV.invalid;
  }

  private boolean isAtoB() {
    return InlandMovementType.A_TO_B.equals(movementType);
  }

  @JsMethod
  public String movementError() {
    return i18n.errorNotValidEntry(movementV);
  }

  @JsMethod
  public String percentageLadenError() {
    return i18n.errorShipLaden();
  }
}
