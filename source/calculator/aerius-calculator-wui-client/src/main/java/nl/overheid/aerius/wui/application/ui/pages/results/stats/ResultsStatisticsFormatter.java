/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.results.stats;

import javax.inject.Inject;

import com.google.inject.Singleton;

import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsStatistics;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;

@Singleton
public class ResultsStatisticsFormatter {

  @Inject ApplicationContext applicationContext;

  public String formatStatisticType(final SituationResultsStatistics statistics, final ResultStatisticType type) {
    return formatStatisticType(statistics, type, false);
  }

  public String formatStatisticType(final SituationResultsStatistics statistics, final ResultStatisticType type, final boolean includeUnit) {
    final EmissionResultValueDisplaySettings conf = applicationContext.getConfiguration().getEmissionResultValueDisplaySettings();
    if (statistics != null && type != null && statistics.hasValue(type)) {
      switch (type) {
      // (ha gekarteerd)
      case SUM_CARTOGRAPHIC_SURFACE:
      case SUM_CARTOGRAPHIC_SURFACE_INCREASE:
      case SUM_CARTOGRAPHIC_SURFACE_DECREASE:
        return includeUnit
            ? MessageFormatter.formatSurfaceToHectareWithUnit(statistics.getValue(type))
            : MessageFormatter.formatSurfaceToHectareInternal(statistics.getValue(type));
      // (mol/ha/jr)
      case MAX_CONTRIBUTION:
      case MAX_TOTAL:
      case MAX_INCREASE:
      case MAX_DECREASE:
      case MAX_TEMP_INCREASE:
        return includeUnit
            ? MessageFormatter.formatDepositionWithUnit(statistics.getValue(type), conf)
            : MessageFormatter.formatDeposition(statistics.getValue(type), conf);
      // (mol/jr)
      case SUM_CONTRIBUTION:
      case PROCUREMENT_POLICY_THRESHOLD_VALUE:
        return includeUnit
            ? MessageFormatter.formatDepositionSumWithUnit(statistics.getValue(type), conf)
            : MessageFormatter.formatDepositionSum(statistics.getValue(type), conf);
      case COUNT_CALCULATION_POINTS:
      case COUNT_CALCULATION_POINTS_DECREASE:
      case COUNT_CALCULATION_POINTS_INCREASE:
      case COUNT_RECEPTORS:
      case COUNT_RECEPTORS_DECREASE:
      case COUNT_RECEPTORS_INCREASE:
        return statistics.getValue(type).toString();
      case DEVELOPMENT_SPACE_DEMAND_CHECK:
        return statistics.getValue(type).toString();
      case MAX_PERCENTAGE_CRITICAL_LEVEL:
      case MAX_TOTAL_PERCENTAGE_CRITICAL_LEVEL:
      case PROCUREMENT_POLICY_THRESHOLD_PERCENTAGE:
        return MessageFormatter.formatPercentageWithUnit(statistics.getValue(type), 0);
      }
    }
    return "-";
  }
}
