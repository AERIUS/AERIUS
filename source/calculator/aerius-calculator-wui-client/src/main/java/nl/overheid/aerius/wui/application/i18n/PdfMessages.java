/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.i18n;

import com.google.gwt.i18n.client.Messages.Select;

import nl.overheid.aerius.shared.domain.PdfProductType;
import nl.overheid.aerius.shared.domain.summary.ProcurementPolicy;

public interface PdfMessages {

  String pdfPermitDemandCheckColumtitle();

  String pdfProjectCalculationCoverPageTocOverview();
  String pdfProjectCalculationCoverPageTocSituationsSummary();
  String pdfProjectCalculationCoverPageTocSituationSummary();
  String pdfProjectCalculationCoverPageTocResults();
  String pdfProjectCalculationCoverPageTocResultsPerAssessmentArea();
  String pdfProjectCalculationCoverPageTocResultsWithoutEdgeEffect();
  String pdfProjectCalculationCoverPageTocResultsWithEdgeEffect();
  String pdfProjectCalculationCoverPageTocEmissionSourceDetails();
  String pdfProjectCalculationCoverPageDescription(@Select PdfProductType type);
  String pdfProjectCalculationCoverPageNote(@Select PdfProductType type);

  String pdfOverviewContactDetails();
  String pdfOverviewContactDetailsCorporation();
  String pdfOverviewContactDetailsLocation();
  String pdfOverviewAppendix();
  String pdfOverviewAppendixDescription();
  String pdfOverviewAppendixReference();
  String pdfOverviewAppendixDate();
  String pdfOverviewActivity();
  String pdfOverviewActivityTitle();
  String pdfOverviewActivityDescription();
  String pdfOverviewCalculation();
  String pdfOverviewCalculationReference();
  String pdfOverviewCalculationDate();
  String pdfOverviewCalculationConfiguration();
  String pdfOverviewCalculationConfigurationOwN2000();
  String pdfOverviewCalculationConfigurationOwN2000Plus();
  String pdfOverviewTotalEmission();
  String pdfOverviewTotalEmissionCalculationYear();
  String pdfOverviewTotalEmissionEmissionNH3();
  String pdfOverviewTotalEmissionEmissionNOx();
  String pdfOverviewResults();
  String pdfOverviewResultsHexagon();
  String pdfOverviewResultsArea();
  String pdfOverviewResultsAreaCartographic();
  String pdfOverviewResultsAreaWithIncrease();
  String pdfOverviewResultsAreaWithDecrease();

  String pdfOverviewProcurementFail();
  String pdfOverviewProcurementPass();

  String pdfOverviewResultsProcurementPolicyMaxContribution();
  String pdfOverviewResultsProcurementPolicyNumberOfAreas(int numberOfAreas);
  String pdfOverviewResultsProcurementPolicyCountReceptors();
  String pdfOverviewResultsProcurementPolicyHexagonSet();
  String pdfOverviewResultsProcurementPolicySumContribution();
  String pdfOverviewResultsProcurementPolicyAboveThreshold();

  String pdfOverviewOffSiteReduction();
  String pdfOverviewNettingFactor();

  String pdfSituationRekenjaarLabel();
  String pdfSituationOverviewEmissionSources();
  String pdfSituationOverviewEmissionNH3Header();
  String pdfSituationOverviewEmissionNOxHeader();
  String pdfSituationOverviewBuildings();
  String pdfSituationOverviewBuildingsEnvelopeHeader();
  String pdfSituationOverviewBuildingsEnvelope(String length, String width, String height, String orientation);
  String pdfSituationOverviewBuildingsEnvelopeCorrected(String length, String width, String height);

  String pdfResultSummaryMapIntro(@Select PdfProductType type);
  String pdfResultSummaryMapOutro();

  String pdfResultSummaryTableIntro(String situation, @Select PdfProductType type);
  String pdfResultSummaryTableTotal();
  String pdfResultSummaryTablePerArea();
  String pdfResultSummaryTablePerCalculationPoint();
  String pdfResultSummaryEdgeEffectTableTitle(String situation);

  String pdfResultsProcurementHeader(@Select ProcurementPolicy policy);

  String pdfEmissionSourceDetailsAnimalHousingTableDescription();
  String pdfEmissionSourceDetailsLodgingTableDescription();
  String pdfEmissionSourceDetailsFarmlandTableDescription();
  String pdfEmissionSourceDetailsShippingTableDescription();
  String pdfEmissionSourceDetailsOffroadTableDescription();
  String pdfEmissionSourceDetailsSRM2RoadDetailsTableDescription();
  String pdfEmissionSourceDetailsSRM2RoadTableDescription();
  String pdfEmissionSourcesLimitReachedMessage(String pdfEmissionSourceLimit, String uiEmissionSourceLimit);
  String pdfEmissionSourcesSRM2RoadLimitReachedMessage(String pdfEmissionSourceSRM2RoadLimit, String uiEmissionSourceLimit);

  String pdfDisclaimer(@Select PdfProductType ppt);
  String pdfDisclaimerLabel();
  String pdfCalculationBaseLabel();
  String pdfCalculationBase();
  String pdfCalculationBaseAeriusVersion();
  String pdfCalculationBaseDatabaseVersion();
  String pdfCalculationBaseMoreInfo();
}
