/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.road.adms;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.v2.source.road.ADMSRoadSideBarrierType;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.InputWarningComponent;
import nl.overheid.aerius.wui.application.components.input.InputWithUnitComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.source.road.adms.ADMSBarrierEditorValidators.ADMSBarrierEditorValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.domain.source.ADMSRoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.road.ADMSRoadSideBarrier;

@Component(name = "adms-barrier-editor", customizeOptions = {
    ADMSBarrierEditorValidators.class,
}, directives = {
    ValidateDirective.class
}, components = {
    LabeledInputComponent.class,
    InputWithUnitComponent.class,
    InputErrorComponent.class,
    InputWarningComponent.class,
    ValidationBehaviour.class,
    VerticalCollapse.class,
    SimplifiedListBoxComponent.class,
})
public class ADMSBarrierEditorComponent extends ErrorWarningValidator implements HasCreated, HasValidators<ADMSBarrierEditorValidations> {
  @Prop ADMSRoadESFeature source;
  @Prop Double roadWidth;

  @Data String barrierLeftWidthV;
  @Data String barrierLeftMaximumHeightV;
  @Data String barrierLeftAverageHeightV;
  @Data String barrierLeftMinimumHeightV;
  @Data String barrierLeftPorosityV;

  @Data String barrierRightWidthV;
  @Data String barrierRightMaximumHeightV;
  @Data String barrierRightAverageHeightV;
  @Data String barrierRightMinimumHeightV;
  @Data String barrierRightPorosityV;

  @JsProperty(name = "$v") ADMSBarrierEditorValidations validation;

  @Override
  @Computed
  public ADMSBarrierEditorValidations getV() {
    return validation;
  }

  @Computed
  public List<ADMSRoadSideBarrierType> getRoadBarrierTypes() {
    return Arrays.asList(ADMSRoadSideBarrierType.values());
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Watch(value = "source", isImmediate = true)
  public void onSourceChange(final ADMSRoadESFeature neww) {
    if (neww == null) {
      return;
    }

    if (hasBarrierLeft()) {
      barrierLeftWidthV = String.valueOf(neww.getBarrierLeft().getWidth());
      barrierLeftMaximumHeightV = String.valueOf(neww.getBarrierLeft().getMaximumHeight());
      barrierLeftAverageHeightV = String.valueOf(neww.getBarrierLeft().getAverageHeight());
      barrierLeftMinimumHeightV = String.valueOf(neww.getBarrierLeft().getMinimumHeight());
      barrierLeftPorosityV = String.valueOf(neww.getBarrierLeft().getPorosity());
    }

    if (hasBarrierRight()) {
      barrierRightWidthV = String.valueOf(neww.getBarrierRight().getWidth());
      barrierRightMaximumHeightV = String.valueOf(neww.getBarrierRight().getMaximumHeight());
      barrierRightAverageHeightV = String.valueOf(neww.getBarrierRight().getAverageHeight());
      barrierRightMinimumHeightV = String.valueOf(neww.getBarrierRight().getMinimumHeight());
      barrierRightPorosityV = String.valueOf(neww.getBarrierRight().getPorosity());
    }
  }

  @Computed
  protected ADMSRoadSideBarrierType getBarrierLeftType() {
    return source.getBarrierLeft() == null ? ADMSRoadSideBarrierType.NONE : source.getBarrierLeft().getBarrierType();
  }

  @JsMethod
  protected void selectBarrierLeftType(final ADMSRoadSideBarrierType barrierType) {
    if (ADMSRoadSideBarrierType.NONE == barrierType) {
      source.setBarrierLeft(null);
      barrierLeftWidthV = "";
      barrierLeftMaximumHeightV = "";
      barrierLeftAverageHeightV = "";
      barrierLeftMinimumHeightV = "";
      barrierLeftPorosityV = "";
    } else {
      ensureBarrierLeftCreated();
      source.getBarrierLeft().setBarrierType(barrierType);
      Optional.ofNullable(barrierType.getDefaultPorosity())
          .ifPresent(v -> setBarrierLeftPorosity(String.valueOf(v)));
    }
  }

  @Computed
  protected ADMSRoadSideBarrierType getBarrierRightType() {
    return source.getBarrierRight() == null ? ADMSRoadSideBarrierType.NONE : source.getBarrierRight().getBarrierType();
  }

  @JsMethod
  protected void selectBarrierRightType(final ADMSRoadSideBarrierType barrierType) {
    if (ADMSRoadSideBarrierType.NONE == barrierType) {
      source.setBarrierRight(null);
      barrierRightWidthV = "";
      barrierRightMaximumHeightV = "";
      barrierRightAverageHeightV = "";
      barrierRightMinimumHeightV = "";
      barrierRightPorosityV = "";
    } else {
      ensureBarrierRightCreated();
      source.getBarrierRight().setBarrierType(barrierType);
      Optional.ofNullable(barrierType.getDefaultPorosity())
          .ifPresent(v -> setBarrierRightPorosity(String.valueOf(v)));
    }
  }

  private void ensureBarrierLeftCreated() {
    if (!hasBarrierLeft()) {
      final ADMSRoadSideBarrier barrier = ADMSRoadSideBarrier.create();
      source.setBarrierLeft(barrier);
      barrierLeftWidthV = String.valueOf(barrier.getWidth());
      barrierLeftMaximumHeightV = String.valueOf(barrier.getMaximumHeight());
      barrierLeftAverageHeightV = String.valueOf(barrier.getAverageHeight());
      barrierLeftMinimumHeightV = String.valueOf(barrier.getMinimumHeight());
      barrierLeftPorosityV = String.valueOf(barrier.getPorosity());
    }
  }

  @Computed("hasBarrierLeft")
  public boolean hasBarrierLeft() {
    return source.getBarrierLeft() != null;
  }

  private void ensureBarrierRightCreated() {
    if (!hasBarrierRight()) {
      final ADMSRoadSideBarrier barrier = ADMSRoadSideBarrier.create();
      source.setBarrierRight(barrier);
      barrierRightWidthV = String.valueOf(barrier.getWidth());
      barrierRightMaximumHeightV = String.valueOf(barrier.getMaximumHeight());
      barrierRightAverageHeightV = String.valueOf(barrier.getAverageHeight());
      barrierRightMinimumHeightV = String.valueOf(barrier.getMinimumHeight());
      barrierRightPorosityV = String.valueOf(barrier.getPorosity());
    }
  }

  @Computed("hasBarrierRight")
  public boolean hasBarrierRight() {
    return source.getBarrierRight() != null;
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return (hasBarrierLeft() && isBarrierLeftInvalid())
        || (hasBarrierRight() && isBarrierRightInvalid());
  }

  @Computed("isBarrierLeftInvalid")
  public boolean isBarrierLeftInvalid() {
    return isBarrierLeftWidthInvalid()
        || validation.barrierLeftMaximumHeightV.invalid
        || isBarrierLeftAverageHeightInvalid()
        || validation.barrierLeftMinimumHeightV.invalid
        || validation.barrierLeftPorosityV.invalid;
  }

  @Computed("isBarrierRightInvalid")
  public boolean isBarrierRightInvalid() {
    return isBarrierRightWidthInvalid()
        || validation.barrierRightMaximumHeightV.invalid
        || isBarrierRightAverageHeightInvalid()
        || validation.barrierRightMinimumHeightV.invalid
        || validation.barrierRightPorosityV.invalid;
  }

  /** BOILER PLATE **/

  // Width

  @Computed
  protected String getBarrierLeftWidth() {
    return barrierLeftWidthV;
  }

  @Computed
  protected void setBarrierLeftWidth(final String value) {
    ensureBarrierLeftCreated();
    ValidationUtil.setSafeDoubleValue(v -> source.getBarrierLeft().setWidth(v), value, 0D);
    barrierLeftWidthV = value;
  }

  @JsMethod
  protected boolean isBarrierLeftWidthInvalid() {
    return validation.barrierLeftWidthV.invalid || isWidthInvalid(source.getBarrierLeft());
  }

  protected String barrierLeftWidthError() {
    return validation.barrierLeftWidthV.invalid ? i18n.ivDoubleLowerlimit(i18n.sourceRoadBarrierLeftWidth(), 0D)
        : barrierLeftWidthToSmallError();
  }

  private String barrierLeftWidthToSmallError() {
    final ADMSRoadSideBarrier barrier = source.getBarrierLeft();
    return barrier == null ? "" : i18n.ivRoadBarrierWidthToSmall(i18n.sourceRoadBarrierLeftTitle(), barrier.getWidth(), roadWidth);
  }

  @Computed
  protected String getBarrierRightWidth() {
    return barrierRightWidthV;
  }

  @Computed
  protected void setBarrierRightWidth(final String value) {
    ensureBarrierRightCreated();
    ValidationUtil.setSafeDoubleValue(v -> source.getBarrierRight().setWidth(v), value, 0D);
    barrierRightWidthV = value;
  }

  @JsMethod
  protected boolean isBarrierRightWidthInvalid() {
    return validation.barrierRightWidthV.invalid || isWidthInvalid(source.getBarrierRight());
  }

  protected String barrierRightWidthError() {
    return validation.barrierRightWidthV.invalid ? i18n.ivDoubleLowerlimit(i18n.sourceRoadBarrierRightWidth(), 0D)
        : barrierRightWidthToSmallError();
  }

  private String barrierRightWidthToSmallError() {
    final ADMSRoadSideBarrier barrier = source.getBarrierRight();
    return barrier == null ? null : i18n.ivRoadBarrierWidthToSmall(i18n.sourceRoadBarrierRightTitle(), barrier.getWidth(), roadWidth);
  }

  /**
   * Width on each side of the canyon with a canyon wall should be greater than half the road carriageway width.
   *
   * @return true if invalid
   */
  private boolean isWidthInvalid(final ADMSRoadSideBarrier barrier) {
    return barrier != null && (barrier.getWidth() * 2) < roadWidth;
  }

  // Maximum height

  @Computed
  protected String getBarrierLeftMaximumHeight() {
    return barrierLeftMaximumHeightV;
  }

  @Computed
  protected void setBarrierLeftMaximumHeight(final String value) {
    ensureBarrierLeftCreated();
    ValidationUtil.setSafeDoubleValue(v -> source.getBarrierLeft().setMaximumHeight(v), value, 0D);
    barrierLeftMaximumHeightV = value;
  }

  protected String barrierLeftMaximumHeightError() {
    return i18n.ivDoubleGreaterThan(i18n.sourceRoadBarrierLeftMaximumHeight(), 0D);
  }

  @Computed
  protected String getBarrierRightMaximumHeight() {
    return barrierRightMaximumHeightV;
  }

  @Computed
  protected void setBarrierRightMaximumHeight(final String value) {
    ensureBarrierRightCreated();
    ValidationUtil.setSafeDoubleValue(v -> source.getBarrierRight().setMaximumHeight(v), value, 0D);
    barrierRightMaximumHeightV = value;
  }

  protected String barrierRightMaximumHeightError() {
    return i18n.ivDoubleGreaterThan(i18n.sourceRoadBarrierRightMaximumHeight(), 0D);
  }

  // Average

  @Computed
  protected String getBarrierLeftAverageHeight() {
    return barrierLeftAverageHeightV;
  }

  @Computed
  protected void setBarrierLeftAverageHeight(final String value) {
    ensureBarrierLeftCreated();
    ValidationUtil.setSafeDoubleValue(v -> source.getBarrierLeft().setAverageHeight(v), value, 0D);
    barrierLeftAverageHeightV = value;
  }

  @JsMethod
  protected boolean isBarrierLeftAverageHeightInvalid() {
    return validation.barrierLeftAverageHeightV.invalid || isAverageHeightInvalid(source.getBarrierLeft());
  }

  protected String barrierLeftAverageHeightError() {
    return validation.barrierLeftAverageHeightV.invalid ? i18n.ivDoubleGreaterThan(i18n.sourceRoadBarrierLeftAverageHeight(), 0D)
        : barrierLeftAverageHeightToLowError();
  }

  private String barrierLeftAverageHeightToLowError() {
    final ADMSRoadSideBarrier barrier = source.getBarrierLeft();
    return barrier == null ? null
        : i18n.ivRoadBarrierAverageBelowMinimum(i18n.sourceRoadBarrierLeftTitle(), barrier.getAverageHeight(), barrier.getMinimumHeight());
  }

  @Computed
  protected String getBarrierRightAverageHeight() {
    return barrierRightAverageHeightV;
  }

  @Computed
  protected void setBarrierRightAverageHeight(final String value) {
    ensureBarrierRightCreated();
    ValidationUtil.setSafeDoubleValue(v -> source.getBarrierRight().setAverageHeight(v), value, 0D);
    barrierRightAverageHeightV = value;
  }

  @JsMethod
  protected boolean isBarrierRightAverageHeightInvalid() {
    return validation.barrierRightAverageHeightV.invalid || isAverageHeightInvalid(source.getBarrierRight());
  }

  protected String barrierRightAverageHeightError() {
    return validation.barrierRightAverageHeightV.invalid ? i18n.ivDoubleGreaterThan(i18n.sourceRoadBarrierRightAverageHeight(), 0D)
        : barrierRightAverageHeightToLowError();
  }

  private String barrierRightAverageHeightToLowError() {
    final ADMSRoadSideBarrier barrier = source.getBarrierRight();
    return barrier == null ? null
        : i18n.ivRoadBarrierAverageBelowMinimum(i18n.sourceRoadBarrierRightTitle(), barrier.getAverageHeight(), barrier.getMinimumHeight());
  }

  /**
   * Average height needs to be greater than the minimum canyon height, otherwise no canyon will be modelled for this side.
   *
   * @return true if invalid
   */
  private boolean isAverageHeightInvalid(final ADMSRoadSideBarrier barrier) {
    return barrier != null && barrier.getAverageHeight() < barrier.getMinimumHeight();
  }

  // Minimum

  @Computed
  protected String getBarrierLeftMinimumHeight() {
    return barrierLeftMinimumHeightV;
  }

  @Computed
  protected void setBarrierLeftMinimumHeight(final String value) {
    ensureBarrierLeftCreated();
    ValidationUtil.setSafeDoubleValue(v -> source.getBarrierLeft().setMinimumHeight(v), value, 0D);
    barrierLeftMinimumHeightV = value;
  }

  protected String barrierLeftMinimumHeightError() {
    return i18n.ivDoubleLowerlimit(i18n.sourceRoadBarrierLeftMinimumHeight(), 0D);
  }

  @Computed
  protected String getBarrierRightMinimumHeight() {
    return barrierRightMinimumHeightV;
  }

  @Computed
  protected void setBarrierRightMinimumHeight(final String value) {
    ensureBarrierRightCreated();
    ValidationUtil.setSafeDoubleValue(v -> source.getBarrierRight().setMinimumHeight(v), value, 0D);
    barrierRightMinimumHeightV = value;
  }

  protected String barrierRightMinimumHeightError() {
    return i18n.ivDoubleLowerlimit(i18n.sourceRoadBarrierRightMinimumHeight(), 0D);
  }

  //Porosity

  @Computed
  protected String getBarrierLeftPorosity() {
    return barrierLeftPorosityV;
  }

  @Computed
  protected void setBarrierLeftPorosity(final String value) {
    ensureBarrierLeftCreated();
    ValidationUtil.setSafeDoubleValue(v -> source.getBarrierLeft().setPorosity(v), value, 0D);
    barrierLeftPorosityV = value;
  }

  protected String barrierLeftPorosityError() {
    return i18n.ivDoubleRangeBetween(i18n.sourceRoadBarrierLeftPorosity(), 0, 100);
  }

  @Computed
  protected String getBarrierRightPorosity() {
    return barrierRightPorosityV;
  }

  @Computed
  protected void setBarrierRightPorosity(final String value) {
    ensureBarrierRightCreated();
    ValidationUtil.setSafeDoubleValue(v -> source.getBarrierRight().setPorosity(v), value, 0D);
    barrierRightPorosityV = value;
  }

  protected String barrierRightPorosityError() {
    return i18n.ivDoubleRangeBetween(i18n.sourceRoadBarrierRightPorosity(), 0, 100);
  }
}
