/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.layer.item;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.gwt.dom.client.InputElement;
import com.google.web.bindery.event.shared.EventBus;

import elemental2.dom.Event;

import jsinterop.annotations.JsMethod;

import nl.aerius.geo.command.LayerHiddenCommand;
import nl.aerius.geo.command.LayerOpacityCommand;
import nl.aerius.geo.command.LayerVisibilityToggleCommand;
import nl.aerius.geo.command.LayerVisibleCommand;
import nl.aerius.geo.domain.LayerInfo;
import nl.aerius.geo.domain.LayerItem;
import nl.aerius.geo.domain.LayerOptions;
import nl.aerius.geo.domain.legend.Legend;
import nl.aerius.wui.vue.directives.VectorDirective;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.legend.LayerLegendComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "layer-item", components = {
    CollapsiblePanel.class,
    LayerLegendComponent.class,
    SimplifiedListBoxComponent.class,
}, directives = {
    VectorDirective.class
})
public class LayerItemTemplate extends BasicVueComponent {
  @Inject @Data ApplicationContext applicationContext;

  @Prop LayerItem item;
  @Prop EventBus eventBus;

  @Data boolean open;

  @Computed
  public double getOpacity() {
    return item.getOpacity() * 100;
  }

  @JsMethod
  public void onLayerItemClick() {
    eventBus.fireEvent(new LayerVisibilityToggleCommand(item.getLayer()));
  }

  @JsMethod
  public void updateOpacity(final Event value) {
    final double opacity = Double.parseDouble(((InputElement) value.target).getValue()) / 100;

    if (opacity > 0 && !item.isVisible()) {
      eventBus.fireEvent(new LayerVisibleCommand(item.getLayer()));
    }
    if (opacity <= 0 && item.isVisible()) {
      eventBus.fireEvent(new LayerHiddenCommand(item.getLayer()));
    }

    eventBus.fireEvent(new LayerOpacityCommand(item.getLayer(), opacity));
  }

  @Computed
  public String getToggleImage() {
    return item.isVisible()
        ? img.toggleLayerOn().getSafeUri().asString()
        : img.toggleLayerOff().getSafeUri().asString();
  }

  @Computed
  public Legend getLegend() {
    return item.getLayer().getInfoOptional()
        .map(LayerInfo::getLegend)
        .orElse(null);
  }

  @Computed
  public LayerOptions[] getLayerOptions() {
    return item.getLayer().getInfoOptional()
        .map(LayerInfo::getOptions)
        .orElse(new LayerOptions[0]);
  }

  @JsMethod
  public String getNameOfLayerOption(final LayerOptions options, final Object option) {
    if (option == LayerOptions.NO_OPTION_SELECTED) {
      return M.messages().layerSelectOption();
    } else {
      return options.getNameOf(option);
    }
  }

}
