/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.job;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.Feature;
import ol.color.Color;
import ol.geom.Point;
import ol.source.Vector;
import ol.style.Style;

import nl.aerius.geo.domain.LayerInfo;
import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.ApplicationLimits;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobSelectCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobToggleCommand;
import nl.overheid.aerius.wui.application.command.calculation.TemporaryCalculationJobAddCommand;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationPreparationContext;
import nl.overheid.aerius.wui.application.domain.ItemWithStringIdUtil;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.event.CalculationJobCompositionChangedEvent;
import nl.overheid.aerius.wui.application.geo.layers.FeatureStyle;
import nl.overheid.aerius.wui.application.geo.layers.FeatureType;
import nl.overheid.aerius.wui.application.geo.layers.LabelStyle;
import nl.overheid.aerius.wui.application.geo.layers.base.BaseGeometryLayer;
import nl.overheid.aerius.wui.application.geo.layers.base.VectorFeaturesWrapper;
import nl.overheid.aerius.wui.application.geo.util.GeoType;
import nl.overheid.aerius.wui.application.geo.util.GeoUtil;

/**
 * Layer to show the geometry of both {@link EmissionSourceFeature} and {@link BuildingFeature} objecsts of all situations combined on the map.
 * The source objects will be copied to LabelFeature objects that will be shown on the map.
 * The id of these features will be a combination of situation id, feature id and situation type.
 * Because these 3 parameters are needed to construct the id used by the standard add/remove methods.
 */
public class JobInputGeometryLayer extends BaseGeometryLayer<Feature> implements JobInputLayer {
  interface JobInputGeometryLayerEventBinder extends EventBinder<JobInputGeometryLayer> {
  }

  private static final JobInputGeometryLayerEventBinder EVENT_BINDER = GWT.create(JobInputGeometryLayerEventBinder.class);

  private final VectorFeaturesWrapper vectorObject = new VectorFeaturesWrapper();

  private final CalculationPreparationContext context;
  private Map<String, SituationType> situationTypes = new HashMap<>();

  @Inject
  public JobInputGeometryLayer(@Assisted final LayerInfo info, final EventBus eventBus, @Assisted final int zIndex,
      final CalculationPreparationContext context) {
    super(info, zIndex);
    this.context = context;
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Override
  protected Style[] createStyle(final Feature feature, final double resolution) {
    if (GeoType.POINT.is(feature.getGeometry())) {
      return getPointStyle(Color.getColorFromString(LabelStyle.EMISSION_SOURCE.getLabelBackgroundColor(feature)));
    }

    if (ItemWithStringIdUtil.isTemporaryFeature(feature)) {
      return BaseGeometryLayer.NO_RENDERING;
    }

    return FeatureStyle.DEFAULT_STYLING;
  }

  @EventHandler(handles = {CalculationJobToggleCommand.class, CalculationJobSelectCommand.class, TemporaryCalculationJobAddCommand.class,
      CalculationJobCompositionChangedEvent.class})
  void onCalculationChange() {
    SchedulerUtil.delay(() -> selectJob(context.getActiveOrTemporaryJob()));
  }

  private void selectJob(final CalculationJobContext jobContext) {
    setFeatures(vectorObject, Collections.emptyList());
    if (jobContext == null) {
      return;
    }
    situationTypes = jobContext.getSituationComposition().getSituations().stream()
        .collect(Collectors.toMap(SituationContext::getId, SituationContext::getType));
    jobContext.getSituationComposition().getSituations().forEach(sc -> addSituationFeatures(vectorObject.getVector(), sc));
  }

  private void addSituationFeatures(final Vector vector, final SituationContext situation) {
    addFeatures(vector, situation.getSources(), situation.getId(), FeatureType.SOURCE);
    addFeatures(vector, situation.getBuildings(), situation.getId(), FeatureType.BUILDING);
  }

  private void addFeatures(final Vector vector, final List<? extends Feature> features, final String situationId, final FeatureType featureType) {
    final List<Feature> featuresToAdd = features.stream()
        .limit(Math.max(0, ApplicationLimits.SOURCE_LAYER_DRAW_LIMIT - vector.getFeatures().length))
        .map(f -> createLocalFeature(f, situationId, featureType))
        .collect(Collectors.toList());

    vector.addFeatures(featuresToAdd.toArray(new Feature[featuresToAdd.size()]));
  }

  @Override
  public void onAdd(final String situationId, final Feature feature, final FeatureType featureType) {
    if (situationId != null && situationTypes.containsKey(situationId)) {
      vectorObject.addFeature(createLocalFeature(feature, situationId, featureType));
    }
  }

  @Override
  public void onUpdate(final String situationId, final Feature feature, final FeatureType featureType) {
    onDelete(situationId, feature, featureType);
    onAdd(situationId, feature, featureType);
  }

  @Override
  public void onDelete(final String situationId, final Feature feature, final FeatureType featureType) {
    vectorObject.removeFeatureById(situationFeatureKey(situationId, feature, featureType));
  }

  private Feature createLocalFeature(final Feature originalFeature, final String situationId, final FeatureType featureType) {
    final Feature copy = new Feature(originalFeature.getGeometry());

    copy.setId(situationFeatureKey(situationId, originalFeature, featureType));
    if (originalFeature instanceof BuildingFeature
        && originalFeature.getGeometry() instanceof Point
        && ((BuildingFeature) originalFeature).getDiameter() != null) {
      copy.setGeometry(GeoUtil.toCircleGeometry((Point) originalFeature.getGeometry(), ((BuildingFeature) originalFeature).getDiameter()));
    }
    return copy;
  }
}
