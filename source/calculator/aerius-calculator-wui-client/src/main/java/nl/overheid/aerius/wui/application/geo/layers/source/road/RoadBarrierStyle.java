/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source.road;

import java.util.function.Function;

import ol.OLFactory;
import ol.style.Style;

import nl.aerius.geo.domain.legend.ColorLabelsLegend;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.shared.domain.v2.source.road.ADMSRoadSideBarrierType;
import nl.overheid.aerius.wui.application.domain.source.ADMSRoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.SRM2RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.road.ADMSRoadSideBarrier;
import nl.overheid.aerius.wui.application.domain.source.road.SRM2RoadSideBarrier;
import nl.overheid.aerius.wui.application.geo.layers.source.RoadSourceGeometryLayer;
import nl.overheid.aerius.wui.application.i18n.M;

public abstract class RoadBarrierStyle {

  protected static final String ROAD_STYLE_COLOR = "#000000";
  protected static final Style ROAD_STYLE =
      OLFactory.createStyle(OLFactory.createStroke(OLFactory.createColor(ROAD_STYLE_COLOR), RoadSourceGeometryLayer.DEFAULT_ROAD_WIDTH));

  protected static final int ROAD_BARRIER_WIDTH = 4;
  protected static final double ROAD_BARRIER_STYLE_DISTANCE = 20D;

  protected RoadBarrierStyle() {
  }

  protected static ColorLabelsLegend withRoadNetworkLabel(final ColorLabelsLegend legend) {
    legend.addItem(M.messages().layerRoadNetwork(), ROAD_STYLE_COLOR, null);
    return legend;
  }

  protected static double getBarrierDistanceOnMap(final BarrierSide barrierSide, final Double resolution) {
    if (barrierSide == BarrierSide.LEFT) {
      return -ROAD_BARRIER_STYLE_DISTANCE * resolution;
    } else {
      return ROAD_BARRIER_STYLE_DISTANCE * resolution;
    }
  }

  protected static boolean showBarrier(final RoadESFeature source, final BarrierSide barrierSide) {
    return getBarrierProperty(source, barrierSide, false, srmBarrier ->
            srmBarrier != null && srmBarrier.getHeight() > 0, admsBarrier ->
            admsBarrier != null && admsBarrier.getBarrierType() != null && admsBarrier.getBarrierType() != ADMSRoadSideBarrierType.NONE
    );
  }

  protected static <T> T getBarrierProperty(final RoadESFeature source, final BarrierSide barrierSide,
      final T emptyResult, final Function<SRM2RoadSideBarrier, T> srmFn, final Function<ADMSRoadSideBarrier, T> admsFn) {
    if (source == null) {
      return emptyResult;
    } else if (source.getEmissionSourceType() == EmissionSourceType.SRM2_ROAD) {

      final SRM2RoadSideBarrier barrier;
      if (barrierSide == BarrierSide.LEFT) {
        barrier = ((SRM2RoadESFeature) source).getBarrierLeft();
      } else {
        barrier = ((SRM2RoadESFeature) source).getBarrierRight();
      }

      return srmFn.apply(barrier);
    } else if (source.getEmissionSourceType() == EmissionSourceType.ADMS_ROAD) {

      final ADMSRoadSideBarrier barrier;
      if (barrierSide == BarrierSide.LEFT) {
        barrier = ((ADMSRoadESFeature) source).getBarrierLeft();
      } else {
        barrier = ((ADMSRoadESFeature) source).getBarrierRight();
      }

      return admsFn.apply(barrier);
    } else {
      return emptyResult;
    }
  }

  protected enum BarrierSide {
    LEFT, RIGHT
  }
}
