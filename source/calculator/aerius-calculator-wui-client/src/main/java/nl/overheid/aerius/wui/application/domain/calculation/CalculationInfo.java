/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.calculation;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.ServerUsage;

@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class CalculationInfo {
  private JobProgress jobProgress;
  private String serverUsage;
  private @JsProperty SituationCalculation[] situationCalculations;
  private ArchiveMetaData archiveMetaData;

  public static final @JsOverlay CalculationInfo create() {
    final CalculationInfo info = new CalculationInfo();
    info.setJobProgress(null);
    info.setServerUsage(ServerUsage.UNKNOWN.name());
    info.setSituationCalculations(null);
    info.setArchiveMetaData(null);
    return info;
  }

  public final @JsOverlay JobProgress getJobProgress() {
    return jobProgress;
  }

  public final @JsOverlay void setJobProgress(final JobProgress jobProgress) {
    this.jobProgress = jobProgress;
  }

  public final @JsOverlay ServerUsage getCalculationServerUsage() {
    return ServerUsage.safeValueOf(serverUsage);
  }

  public final @JsOverlay void setServerUsage(final String serverUsage) {
    this.serverUsage = serverUsage;
  }

  public final @JsOverlay SituationCalculation[] getSituationCalculations() {
    return situationCalculations;
  }

  public final @JsOverlay void setSituationCalculations(final SituationCalculation[] situationCalculations) {
    this.situationCalculations = situationCalculations;
  }

  public final @JsOverlay ArchiveMetaData getArchiveMetaData() {
    return archiveMetaData;
  }

  public final @JsOverlay void setArchiveMetaData(final ArchiveMetaData archiveMetaData) {
    this.archiveMetaData = archiveMetaData;
  }

}
