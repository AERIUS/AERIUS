/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context.result;

import javax.inject.Singleton;

import nl.overheid.aerius.shared.domain.result.MainTab;
import nl.overheid.aerius.shared.domain.result.ResultView;

@Singleton
public class ResultViewContext {
  private MainTab selectedMainTab = MainTab.SCENARIO_RESULTS;
  private ResultView resultViewTab = ResultView.DEPOSITION_DISTRIBUTION;

  public MainTab getSelectedMainTab() {
    return selectedMainTab;
  }

  public void setSelectedMainTab(MainTab selectedMainTab) {
    this.selectedMainTab = selectedMainTab;
  }

  public ResultView getResultViewTab() {
    return resultViewTab;
  }

  public void setResultViewTab(final ResultView resultsViewTab) {
    this.resultViewTab = resultsViewTab;
  }
}
