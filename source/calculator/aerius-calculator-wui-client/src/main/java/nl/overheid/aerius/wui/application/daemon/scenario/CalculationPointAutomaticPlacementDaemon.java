/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.scenario;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.util.NotificationUtil;
import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.domain.v2.point.AssessmentCategory;
import nl.overheid.aerius.wui.application.command.calculationpoint.CloseCalculationPointAutomaticPlacement;
import nl.overheid.aerius.wui.application.command.calculationpoint.DetermineCalculationPointsAutomaticallyCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.OpenCalculationPointAutomaticPlacement;
import nl.overheid.aerius.wui.application.command.calculationpoint.ResetCalculationPointsAutomaticallyCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.CalculationPointAutomaticPlacementContext;
import nl.overheid.aerius.wui.application.context.CalculationPointAutomaticPlacementContext.PlacementStatus;
import nl.overheid.aerius.wui.application.daemon.misc.PersistablePreferencesContext;
import nl.overheid.aerius.wui.application.domain.calculationpoint.CalculationPointPlacementResult;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;
import nl.overheid.aerius.wui.application.domain.source.NcaCustomCalculationPointFeature;
import nl.overheid.aerius.wui.application.i18n.ApplicationMessages;
import nl.overheid.aerius.wui.application.service.AeriusRequestCallback;
import nl.overheid.aerius.wui.application.service.CalculationPointDetermineRequest;
import nl.overheid.aerius.wui.application.service.CalculationPointServiceAsync;

/**
 * Daemon for the automatic placement of calculation points
 */
@Singleton
public class CalculationPointAutomaticPlacementDaemon extends BasicEventComponent implements Daemon {
  private static final CalculationPointAutomaticPlacementDaemonEventBinder EVENT_BINDER = GWT
      .create(CalculationPointAutomaticPlacementDaemonEventBinder.class);

  interface CalculationPointAutomaticPlacementDaemonEventBinder extends EventBinder<CalculationPointAutomaticPlacementDaemon> {}

  @Inject CalculationPointAutomaticPlacementContext context;
  @Inject CalculationPointServiceAsync calculationPointService;
  @Inject ApplicationMessages i18n;

  @Inject PersistablePreferencesContext prefContext;
  @Inject ApplicationContext appContext;

  private String currentDetermineRequest = null;

  @EventHandler
  public void onOpenCalculationPointAutomaticPlacement(final OpenCalculationPointAutomaticPlacement e) {
    context.setAutomaticPlacementOpen(true);
  }

  @EventHandler
  public void onCloseCalculationPointAutomaticPlacement(final CloseCalculationPointAutomaticPlacement e) {
    context.setAutomaticPlacementOpen(false);
  }

  @EventHandler
  public void onDetermineCalculationPointsAutomaticallyCommand(final DetermineCalculationPointsAutomaticallyCommand c) {
    if (!c.isPointsAbroad() && c.getRadiusInland() == null) {
      context.setPlacementStatus(PlacementStatus.WARN);
      context.setPlacementMessage(i18n.calculationPointsPlacementErrorNoOptionSelected());
      return;
    }
    if (c.getSources().isEmpty()) {
      context.setPlacementStatus(PlacementStatus.WARN);
      context.setPlacementMessage(i18n.calculationPointsPlacementNoSources());
      return;
    }
    if (context.isDetermining()) {
      NotificationUtil.broadcastWarning(eventBus, i18n.calculationPointsPlacementWarningBusy());
      return;
    }

    final CalculationPointDetermineRequest request = new CalculationPointDetermineRequest(c.getSources(), c.isPointsAbroad(), c.getRadiusInland());

    context.setDetermining(true);
    final String thisRequest = request.toJSONString();
    currentDetermineRequest = thisRequest;

    calculationPointService.determineCalculationPoints(request,
        AeriusRequestCallback.create(result -> determineCalculationPointsCallback(thisRequest, result),
            e -> determineCalculationPointsErrorCallback(thisRequest, e)));
  }

  private void determineCalculationPointsCallback(final String thisRequest, final CalculationPointPlacementResult result) {
    // don't process out of order results
    if (thisRequest.equals(currentDetermineRequest)) {
      currentDetermineRequest = null;
      context.setDetermining(false);

      final AssessmentCategory assessmentCategory = prefContext.getAssessmentCategory();

      // The calculation points with characteristics are, for now, NCA-specific
      final boolean useNcaPoints = appContext.isAppFlagEnabled(AppFlag.HAS_CALCULATION_POINTS_CHARACTERISTICS);

      result.getComputedCalculationPoints().forEach(v -> {
        if (useNcaPoints) {
          NcaCustomCalculationPointFeature.init((NcaCustomCalculationPointFeature) v, assessmentCategory);
        } else {
          CalculationPointFeature.initCalculationPointFeature(v, v.getCalculationPointType());
        }
      });

      context.setPlacementResult(result);
      context.setPlacementStatus(PlacementStatus.INFO);
      if (result.getComputedCalculationPoints().isEmpty()) {
        context.setPlacementMessage(i18n.calculationPointsPlacementNoResultText());
      } else {
        context.setPlacementMessage(i18n.calculationPointsPlacementResultText(String.valueOf(result.getComputedCalculationPoints().size()),
            String.valueOf(result.getAssessmentAreas())));
      }
    }
  }

  private void determineCalculationPointsErrorCallback(final String thisRequest, final Throwable e) {
    if (thisRequest.equals(currentDetermineRequest)) {
      eventBus.fireEvent(new ResetCalculationPointsAutomaticallyCommand());
      NotificationUtil.broadcastError(eventBus, e);
    }
  }

  @EventHandler
  public void onResetCalculationPointsAutomaticallyCommand(final ResetCalculationPointsAutomaticallyCommand c) {
    context.setPlacementResult(CalculationPointAutomaticPlacementContext.createEmptyPlacementResult());
    context.setPlacementStatus(null);
    context.setPlacementMessage("");
    context.setDetermining(false);
    currentDetermineRequest = null;
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
