/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.modify;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    VerticalCollapse.class,
})
public class ModifyCancelSaveComponent extends BasicVueComponent implements HasCreated {
  @Prop boolean saveDisabled;
  @Prop boolean encumbered;
  @Prop boolean prevented;
  @Prop boolean hideSaveButton;

  @Prop String debugId;

  @Prop boolean noPadding;

  @Prop boolean hasCancel;

  @Prop boolean confirm = false;

  @Prop String saveText;

  @Data String saveTextData;

  @PropDefault("debugId")
  String debugIdDefault() {
    return "";
  }

  @PropDefault("hideSaveButton")
  boolean hideSaveButton() {
    return false;
  }

  @PropDefault("noPadding")
  boolean noPadding() {
    return false;
  }

  @Watch(value = "saveText", isImmediate = true)
  public void onSaveTextModify(final String neww) {
    saveTextData = neww;
  }

  @Override
  public void created() {
    if (saveText == null) {
      saveTextData = i18n.esButtonSave();
    }
  }

  @PropDefault("hasCancel")
  public boolean hasCancelDefault() {
    return true;
  }

  @PropDefault("encumbered")
  public boolean encumberedDefault() {
    return false;
  }

  @PropDefault("prevented")
  public boolean preventedDefault() {
    return false;
  }

  @PropDefault("confirm")
  public boolean confirmDefault() {
    return false;
  }

  @PropDefault("saveDisabled")
  public boolean saveDisabledDefault() {
    return false;
  }

  @Watch("prevented")
  public void onPreventedChange() {
    vue().$emit("confirm", false);
  }

  @Watch("encumbered")
  public void onEncumberedChange(final boolean neww) {
    if (!neww) {
      vue().$emit("confirm", false);
    }
  }

  @JsMethod
  public void save() {
    vue().$emit("try-save");

    if (prevented) {
      vue().$emit("confirm", true);
      return;
    }

    if (encumbered) {
      if (confirm) {
        vue().$emit("save");
        vue().$emit("confirm", false);
      } else {
        vue().$emit("confirm", true);
      }
    } else {
      vue().$emit("save");
    }
  }
}
