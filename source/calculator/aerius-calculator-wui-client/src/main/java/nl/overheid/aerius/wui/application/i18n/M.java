/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.i18n;

import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.shared.DateTimeFormat;

/**
 * Global class to messages interfaces.
 */
public final class M {

  private static void init() {
    DEFAULT_DATE_TIME_FORMATTER = DateTimeFormat.getFormat(M.messages().notificationDateTimeFormat());
    DEFAULT_DATE_FORMATTER = DateTimeFormat.getFormat(M.messages().notificationDateFormat());
    COMPACT_DATE_FORMATTER = DateTimeFormat.getFormat(M.messages().notificationCompactDateTimeFormat());
  }

  private static DateTimeFormat DEFAULT_DATE_TIME_FORMATTER;
  private static DateTimeFormat DEFAULT_DATE_FORMATTER;
  private static DateTimeFormat COMPACT_DATE_FORMATTER;

  private static final ApplicationMessages APPLICATION_MESSAGES = GWT.create(ApplicationMessages.class);
  static {
    BaseM.init(APPLICATION_MESSAGES);
    init();
  }

  public static ApplicationMessages messages() {
    return APPLICATION_MESSAGES;
  }

  public static String formatDate(final Date date) {
    return date == null ? M.messages().nullDateDefault() : DEFAULT_DATE_FORMATTER.format(date);
  }

  public static String formatDateTime(final Date date) {
    return date == null ? M.messages().nullDateDefault() : DEFAULT_DATE_TIME_FORMATTER.format(date);
  }

  public static String formatCompactDateTime(final Date date) {
    return date == null ? M.messages().nullDateDefault() : COMPACT_DATE_FORMATTER.format(date);
  }

  public static String getErrorMessage(final Throwable e) {
    return BaseM.getErrorMessage(e);
  }
}
