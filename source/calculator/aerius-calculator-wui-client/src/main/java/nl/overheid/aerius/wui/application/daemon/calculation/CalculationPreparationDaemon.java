/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.calculation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.ProductProfile;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.MetDatasetType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.ADMSLimits;
import nl.overheid.aerius.wui.application.command.calculation.CalculationDeleteJobCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobAddCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobClearCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobDeselectCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobDuplicateCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobEditCancelCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobEditCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobEditSaveCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobPeekCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobSelectCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobToggleCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobTypeChangedCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobUnpeekCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobsDeleteAllCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationRemoveCommand;
import nl.overheid.aerius.wui.application.command.calculation.TemporaryCalculationJobAddCommand;
import nl.overheid.aerius.wui.application.command.calculation.VerifyCalculationJobDefaultsCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ExportContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationPreparationContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.domain.DevelopmentPressureUtil;
import nl.overheid.aerius.wui.application.domain.calculation.ADMSOptions;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.application.domain.calculation.NCACalculationOptions;
import nl.overheid.aerius.wui.application.event.SelectionResetEvent;
import nl.overheid.aerius.wui.application.event.SwitchActiveCalculationEvent;
import nl.overheid.aerius.wui.application.event.situation.SituationChangedTypeEvent;
import nl.overheid.aerius.wui.application.event.situation.SituationDeletedEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.ui.pages.calculate.CalculateValidations;
import nl.overheid.aerius.wui.application.util.NumberUtil;

/**
 * This daemon is responsible for managing configuration state before starting a
 * calculation.
 */
public class CalculationPreparationDaemon extends BasicEventComponent implements Daemon {
  private static final CalculationPreparationDaemonEventBinder EVENT_BINDER = GWT.create(CalculationPreparationDaemonEventBinder.class);

  interface CalculationPreparationDaemonEventBinder extends EventBinder<CalculationPreparationDaemon> {}

  @Inject private ApplicationContext applicationContext;
  @Inject private CalculationPreparationContext preparationContext;
  @Inject private CalculationContext calculationContext;
  @Inject private ScenarioContext scenarioContext;
  @Inject private ExportContext exportContext;

  @EventHandler
  public void onCalculationJobAddCommand(final CalculationJobAddCommand c) {
    c.getValue().accept(createAndAddCalculationJob(c.isIncludeOptionalSituations()));
  }

  @EventHandler
  public void onCalculationJobEditCommand(final CalculationJobEditCommand c) {
    editCalculationJob(preparationContext.getActiveJob());
  }

  @EventHandler
  public void onCalculationJobDuplicateCommand(final CalculationJobDuplicateCommand c) {
    duplicateCalculationJob(preparationContext.getActiveJob());
  }

  @EventHandler
  public void onCalculationJobEditSaveCommand(final CalculationJobEditSaveCommand c) {
    if (preparationContext.getTemporaryJobId() != null) {
      saveUpdatedCalculationJob(preparationContext.getTemporaryJob());
    } else {
      saveNewCalculationJob(preparationContext.getTemporaryJob());
    }
  }

  @EventHandler
  public void onCalculationJobEditCancelCommand(final CalculationJobEditCancelCommand c) {
    preparationContext.clearTemporaryJob();
  }

  @EventHandler
  public void onTemporaryCalculationJobAddCommand(final TemporaryCalculationJobAddCommand c) {
    createNewTemporaryCalculationJob();
  }

  @EventHandler
  public void onVerifyCalculationJobDefaultsCommand(final VerifyCalculationJobDefaultsCommand c) {
    // Make sure jobs contain correct/valid values
    preparationContext.getJobs().forEach(job -> initJobValues(job, false, false));
  }

  @EventHandler
  public void onCalculationJobSelectCommand(final CalculationJobSelectCommand c) {
    selectJob(c.getValue());
  }

  @EventHandler
  public void onCalculationJobToggleCommand(final CalculationJobToggleCommand c) {
    if (preparationContext.getActiveJob() == c.getValue()) {
      deselectJob();
    } else {
      selectJob(c.getValue());
    }
  }

  @EventHandler
  public void onCalculationJobDeselectCommand(final CalculationJobDeselectCommand c) {
    deselectJob();
  }

  private void deselectJob() {
    preparationContext.clearActiveJob();
    calculationContext.reset();
    eventBus.fireEvent(new SwitchActiveCalculationEvent());
    eventBus.fireEvent(new SelectionResetEvent());
  }

  private void selectJob(final CalculationJobContext job) {
    preparationContext.setActiveJob(job);

    final CalculationExecutionContext calculation = preparationContext.getActiveCalculation();
    calculationContext.setActiveCalculation(calculation);
    eventBus.fireEvent(new SwitchActiveCalculationEvent());
  }

  @EventHandler
  public void onCalculationJobPeekCommand(final CalculationJobPeekCommand c) {
    preparationContext.setPeekJob(c.getValue());
  }

  @EventHandler
  public void onCalculationJobUnpeekCommand(final CalculationJobUnpeekCommand c) {
    SchedulerUtil.delay(() -> preparationContext.clearPeekJob(c.getValue()));
  }

  @EventHandler
  public void onCalculationDeleteJobCommand(final CalculationDeleteJobCommand c) {
    Optional.ofNullable(preparationContext.removeJob(c.getValue()))
        .ifPresent(v -> eventBus.fireEvent(new CalculationRemoveCommand(v)));
    deselectJob();
  }

  @EventHandler
  public void onCalculationJobsDeleteAllCommand(final CalculationJobsDeleteAllCommand c) {
    preparationContext.removeAllJobs();
    calculationContext.removeAllJobs();
    deselectJob();
  }

  @EventHandler
  public void onCalculationJobClearCommand(final CalculationJobClearCommand c) {
    deselectJob();
  }

  @EventHandler
  public void onSituationDeletedEvent(final SituationDeletedEvent e) {
    exportContext.removeSourceInputSituation(e.getValue());
    preparationContext.getJobs().stream()
        .forEach(job -> handleSituationDeletion(job, e.getValue()));
  }

  private static void handleSituationDeletion(final CalculationJobContext jobContext, final SituationContext deletedSituation) {
    final SituationComposition situationComposition = jobContext.getSituationComposition();
    if (situationComposition.isActiveSituation(deletedSituation)) {
      // When removing the proposed situation for a job, for good measure reset the development pressure sources (even if job didn't have any).
      if (situationComposition.getSituation(SituationType.PROPOSED) == deletedSituation) {
        jobContext.getCalculationOptions().getNcaCalculationOptions().setDevelopmentPressureSourceIds(new ArrayList<>());
      }
      jobContext.getSituationComposition().removeSituation(deletedSituation);
    }
  }

  @EventHandler
  public void onSituationChangedTypeEvent(final SituationChangedTypeEvent e) {
    final SituationContext changedSituation = e.getValue();

    for (final CalculationJobContext job : preparationContext.getJobs()) {
      final SituationComposition jobSituationComposition = job.getSituationComposition();
      final CalculationJobType calculationJobType = job.getCalculationOptions().getCalculationJobType();
      final SituationType newSituationType = changedSituation.getType();

      if (jobSituationComposition.isActiveSituation(changedSituation) &&
          (calculationJobType.isIllegal(newSituationType) ||
              (jobSituationComposition.getSituation(newSituationType) != null && !calculationJobType.isPlural(newSituationType)))) {
        jobSituationComposition.removeSituation(changedSituation);
      }
    }
  }

  @EventHandler
  public void onCalculationJobTypeChangedCommand(final CalculationJobTypeChangedCommand c) {
    final CalculationJobContext temporaryJob = preparationContext.getTemporaryJob();

    final CalculationJobType newCalculationJobType = c.getValue();
    temporaryJob.getCalculationOptions().setCalculationJobType(newCalculationJobType);

    // Remove invalid situation types
    final SituationComposition situationComposition = temporaryJob.getSituationComposition();
    final Set<SituationContext> situations = situationComposition.getSituations();
    newCalculationJobType.getIllegalSituationTypes().forEach(st -> situations.stream()
        .filter(sit -> sit.getType() == st)
        .forEach(situationComposition::removeSituation));

    // Remove additional situations if there are too many
    situationComposition.removeSituations(situations.size() - CalculateValidations.getMaxNumberOfSituations(newCalculationJobType,
        applicationContext.getConfiguration()));

    // Set default situation selections for required types
    initJobValues(temporaryJob, true, false);
  }

  private CalculationJobContext createAndAddCalculationJob(final boolean includeOptionalSituations) {
    final CalculationJobContext job = new CalculationJobContext();

    job.setName(M.messages().calculationTitle(String.valueOf(preparationContext.getJobs().size() + 1)));
    initJob(job, includeOptionalSituations);

    preparationContext.addJob(job);
    deselectJob();
    fireJobSelectCommand(job);
    return job;
  }

  private void saveNewCalculationJob(final CalculationJobContext job) {
    preparationContext.addJob(job);

    deselectJob();
    preparationContext.clearTemporaryJob();
    fireJobSelectCommand(job);
  }

  private void saveUpdatedCalculationJob(final CalculationJobContext sourceJob) {
    final CalculationJobContext targetJob = preparationContext.getJobs().stream()
        .filter(j -> j.getId().equals(preparationContext.getTemporaryJobId()))
        .findFirst()
        .orElse(sourceJob);

    preparationContext.replaceJob(sourceJob, targetJob);
    deselectJob();
    preparationContext.clearTemporaryJob();
    fireJobSelectCommand(targetJob);
  }

  private void createNewTemporaryCalculationJob() {
    final CalculationJobContext job = new CalculationJobContext();

    job.setName(M.messages().calculationTitle(String.valueOf(preparationContext.getJobs().size() + 1)));
    initJob(job, false);

    deselectJob();
    preparationContext.setTemporaryJob(job);
  }

  private void initJob(final CalculationJobContext job, final boolean includeOptionalSituations) {
    final ApplicationConfiguration theme = applicationContext.getConfiguration();
    final CalculationSetOptions options = job.getCalculationOptions();

    options.setCalculationMethod(theme.getDefaultCalculationMethod());
    postInit(applicationContext, options, true);
    initJobValues(job, true, includeOptionalSituations);
  }

  private void initJobValues(final CalculationJobContext jobContext, final boolean setDefault, final boolean includeOptionalSituations) {
    final SituationComposition situationComposition = jobContext.getSituationComposition();
    final CalculationJobType calculationJobType = jobContext.getCalculationOptions().getCalculationJobType();
    final Integer situationsMax = CalculateValidations.getMaxNumberOfSituations(calculationJobType, applicationContext.getConfiguration());

    final Set<SituationType> situationTypes = includeOptionalSituations
        ? Arrays.stream(SituationType.values()).filter(st -> !calculationJobType.isIllegal(st)).collect(Collectors.toSet())
        : calculationJobType.getRequiredSituationTypes();

    for (final SituationType validSituationType : situationTypes) {
      if (situationComposition.getSituations().size() < situationsMax) {
        setSituationDefault(validSituationType, situationComposition, setDefault);
      }
    }
    if (setDefault) {
      DevelopmentPressureUtil.initDevelopmentPressureSources(applicationContext, situationComposition, jobContext.getCalculationOptions());
    }
  }

  public static void postInit(final ApplicationContext applicationContext, final CalculationSetOptions options,
      final boolean setParameterDefaults) {
    if (applicationContext.getTheme() == Theme.NCA) {
      initNCA(applicationContext.getConfiguration(), options, setParameterDefaults);
    }
    if (applicationContext.getProductProfile() == ProductProfile.LBV_POLICY) {
      options.setCalculationJobType(CalculationJobType.DEPOSITION_SUM);
    }
  }

  private static void initNCA(final ApplicationConfiguration themeConfig, final CalculationSetOptions options,
      final boolean setParameterDefaults) {
    if (NumberUtil.equalEnough(0D, options.getCalculateMaximumRange())) {
      options.setCalculateMaximumRange(themeConfig.getCalculationDistanceDefault());
    }

    // Set defaults as required
    final NCACalculationOptions ncaOptions = options.getNcaCalculationOptions();
    final ADMSOptions admsOptions = ncaOptions.getAdmsOptions();
    if (admsOptions.getMetDatasetType() == null) {
      admsOptions.setMetDatasetType(MetDatasetType.OBS_RAW_GT_90PCT);
    }
    if (setParameterDefaults) {
      admsOptions.setSurfaceAlbedo(ADMSLimits.SURFACE_ALBEDO_DEFAULT);
      admsOptions.setPriestleyTaylorParameter(ADMSLimits.PRIESTLEY_TAYLOR_PARAMETER_DEFAULT);
      admsOptions.setMinMoninObukhovLength(ADMSLimits.MIN_MONIN_OBUKHOV_LENGTH_DEFAULT);
      admsOptions.setPlumeDepletionNH3(false);
      admsOptions.setPlumeDepletionNOX(false);
      admsOptions.setComplexTerrain(false);
      admsOptions.setMetSiteId(0);
    }
  }

  private void editCalculationJob(final CalculationJobContext job) {
    final CalculationJobContext copy = job.copy();

    preparationContext.setTemporaryJobId(job.getId());
    preparationContext.setTemporaryJob(copy);
    deselectJob();
  }

  private void duplicateCalculationJob(final CalculationJobContext job) {
    final CalculationJobContext copy = new CalculationJobContext();

    job.copyIntoDuplicate(copy);

    preparationContext.addJob(copy);
    fireJobSelectCommand(copy);
  }

  private void fireJobSelectCommand(final CalculationJobContext job) {
    eventBus.fireEvent(new CalculationJobSelectCommand(job));
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }

  private void setSituationDefault(final SituationType type, final SituationComposition composition, final boolean setDefault) {
    final List<SituationContext> situations = getSituations(type);
    final SituationContext situation = composition.getSituation(type);
    if ((situation != null && situations.stream().noneMatch(v -> v.equals(situation)))
        || (setDefault && situations.size() == 1)) {
      situations.stream().findFirst().ifPresent(composition::addSituation);
    }
  }

  public List<SituationContext> getSituations() {
    return scenarioContext.getSituations();
  }

  public List<SituationContext> getSituations(final SituationType type) {
    return getSituations().stream()
        .filter(v -> type == v.getType())
        .collect(Collectors.toList());
  }

}
