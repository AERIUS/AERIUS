/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.domain.source.offroad.OffRoadMobileSource;

/**
 * Feature object for Off Road Mobile Emission Sources with OPS characteristics.
 */
@JsType(namespace = GLOBAL, name = "ol.Feature", isNative = true)
public class OffRoadMobileESFeature extends EmissionSubSourceFeature<OffRoadMobileSource> {
  /**
   * @return Returns Feature {@link OffRoadMobileESFeature} object.
   */
  public static @JsOverlay OffRoadMobileESFeature create() {
    final OffRoadMobileESFeature feature = new OffRoadMobileESFeature();
    init(feature);
    return feature;
  }

  public static @JsOverlay void init(final OffRoadMobileESFeature feature) {
    EmissionSubSourceFeature.initSubSourceFeature(feature, null, EmissionSourceType.OFFROAD_MOBILE);
    EmissionSubSourceFeature.initSubSources(feature, v -> OffRoadMobileSource.initTypeDependent(v, v.getOffroadType()));
  }
}
