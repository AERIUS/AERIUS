/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.farmanimalhousing.standard.additional;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.farmanimalhousing.standard.additional.AnimalStandardAdditionalCustomValidators.AnimalStandardAdditionalCustomValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.domain.source.farm.CustomAdditionalHousingSystem;

/**
 * Component to edit a StandardAnimalHousing Custom Additional Technique.
 */
@Component(customizeOptions = {
    AnimalStandardAdditionalCustomValidators.class,
},
directives = {
    ValidateDirective.class,
},
components = {
    LabeledInputComponent.class,
    ValidationBehaviour.class,
})
public class AnimalStandardAdditionalCustomRowComponent extends ErrorWarningValidator
    implements HasCreated, HasValidators<AnimalStandardAdditionalCustomValidations> {
  @Prop(required = true) @JsProperty CustomAdditionalHousingSystem system;
  @Prop(required = true) int index;

  @JsProperty(name = "$v") AnimalStandardAdditionalCustomValidations validation;

  @Data String reductionV;
  @Data String explanationV;

  @Override
  @Computed
  public AnimalStandardAdditionalCustomValidations getV() {
    return validation;
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Watch(value = "system", isImmediate = true)
  protected void onCustomAdditionalHousingSystemChange(final CustomAdditionalHousingSystem neww) {
    if (neww == null) {
      return;
    }
    reductionV = String.valueOf(neww.getEmissionReductionFactor(Substance.NH3) * 100.0);
    explanationV = neww.getDescription();
  }

  @Computed
  protected void setReduction(final String reduction) {
    ValidationUtil.setSafeDoubleValue(v -> system.setEmissionReductionFactor(Substance.NH3, v / 100.0), reduction, 0);
    reductionV = reduction;
  }

  @Computed
  protected String getReduction() {
    return reductionV;
  }

  @Computed
  protected void setExplanation(final String explanation) {
    system.setDescription(explanation);
    explanationV = explanation;
  }

  @Computed
  protected String getExplanation() {
    return explanationV;
  }

  @Computed("isInvalid")
  protected boolean isInvalid() {
    return validation.reductionV.invalid
        || validation.explanationV.invalid;
  }
}
