/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.nca.pages.timevarying;

import java.util.List;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfileType;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.domain.source.TimeVaryingProfile;
import nl.overheid.aerius.wui.application.domain.source.util.TimeVaryingProfileUtil;
import nl.overheid.aerius.wui.application.domain.source.util.TimeVaryingProfileUtilBase;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.ui.nca.pages.timevarying.raw.DiurnalProfileRawInterpreter;
import nl.overheid.aerius.wui.application.ui.nca.pages.timevarying.raw.MonthlyProfileRawInterpreter;
import nl.overheid.aerius.wui.application.ui.nca.pages.timevarying.raw.TimeVaryingProfileInterpretException;
import nl.overheid.aerius.wui.application.ui.nca.pages.timevarying.raw.TimeVaryingProfileRawInterpreter;

@Component(components = {
    ValidationBehaviour.class,
})
public class TimeVaryingRawInputComponent extends ErrorWarningValidator {
  @Prop TimeVaryingProfile profile;

  @Data String inputData;
  @Data @JsProperty List<Double> values;

  @Data TimeVaryingProfileInterpretException error;

  private TimeVaryingProfileRawInterpreter<?> rawInterpreter;

  @Watch(value = "profile", isImmediate = true)
  public void onProfileChange(final TimeVaryingProfile neww) {
    rawInterpreter = neww.getType() == CustomTimeVaryingProfileType.MONTHLY
        ? MonthlyProfileRawInterpreter.INSTANCE
        : DiurnalProfileRawInterpreter.INSTANCE;
    if (neww.isSystemProfile()) {
      inputData = rawInterpreter.encode(profile.getValues());
    }
  }

  @Watch("inputData")
  public void onInputDataChange() {
    if (inputData.isEmpty()) {
      error = null;
      values = null;
    } else {
      try {
        values = rawInterpreter.tryInterpret(inputData);
        error = null;
      } catch (final TimeVaryingProfileInterpretException e) {
        values = null;
        error = e;
      }
    }
  }

  @Computed
  public String getErrorMessage() {
    return error == null ? "" : M.messages().timeVaryingProfileImportError(error.getReason(), error.getTextCause());
  }

  @Computed
  public String getTotal() {
    if (values == null) {
      return "-";
    }

    return util().getValuesTotalPreciseText(values);
  }

  @Computed
  public String getMax() {
    return util().getValuesMaxText();
  }

  @Computed("canFlush")
  public boolean canFlush() {
    return values != null
        && profile.getType() != null
        && values.size() == profile.getType().getExpectedNumberOfValues()
        && error == null;
  }

  @Computed("isImportSameAsCurrent")
  public boolean isImportSameAsCurrent() {
    return values != null && values.equals(profile.getValues());
  }

  @JsMethod
  public void flushToProfile() {
    if (!canFlush()) {
      return;
    }

    profile.setValues(values);
  }

  @JsMethod
  public void flushToText() {
    inputData = rawInterpreter.encode(profile.getValues());
  }

  @JsMethod
  public String getDebugId(final String debugId) {
    return debugId + '-' + profile.getType().name();
  }

  private TimeVaryingProfileUtilBase util() {
    return TimeVaryingProfileUtil.getTimeVaryingProfileUtilBase(profile.getType());
  }

}
