/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.service;

import java.util.function.Consumer;
import java.util.function.Function;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

import elemental2.core.Global;

import jsinterop.base.Js;

import nl.aerius.wui.init.ExceptionalPirateCache;
import nl.overheid.aerius.wui.application.error.ConnectServerError;
import nl.overheid.aerius.wui.application.error.ConnectServerException;

/**
 * An AERIUS-specific request callback that can automatically parse and handle AeriusException
 */
public class AeriusRequestCallback<T> implements AsyncCallback<T> {
  private final Consumer<T> success;
  private final Consumer<Throwable> failure;

  public AeriusRequestCallback() {
    this(null, null);
  }

  public AeriusRequestCallback(final Consumer<T> success) {
    this(success, null);
  }

  public AeriusRequestCallback(final Consumer<T> success, final Consumer<Throwable> failure) {
    this.success = success;
    this.failure = failure;
  }

  @Override
  public void onSuccess(final T result) {
    if (success != null) {
      success.accept(result);
    }
  }

  @Override
  public final void onFailure(final Throwable caught) {
    final Throwable ex = constructAeriusException(caught);

    if (failure == null) {
      // Hide in the exception cache, then wrap into a RuntimeException and throw it
      ExceptionalPirateCache.hide(ex);
      throw new RuntimeException(ex);
    }

    failure.accept(ex);
  }

  private Throwable constructAeriusException(final Throwable caught) {
    Throwable returnThrowable = caught;
    if (!(caught instanceof ConnectServerException)) {
      try {
        // Try to parse as a ConnectServerError
        final ConnectServerError serverError = Js.cast(Global.JSON.parse(caught.getMessage()));
        returnThrowable = new ConnectServerException(serverError.getMessage());
        // Not sure what the correct catch would be, as it's something that is thrown by a native method.
      } catch (final Exception failed) {
        // If parsing failed, just return the caught one. It'll be converted appropriately later on.
      }
    }
    return returnThrowable;
  }

  public static <T> AeriusRequestCallback<T> create() {
    return new AeriusRequestCallback<T>();
  }

  public static <T> AeriusRequestCallback<T> create(final Consumer<T> success) {
    return new AeriusRequestCallback<T>(success);
  }

  public static <T> AeriusRequestCallback<T> create(final Consumer<T> success, final Consumer<Throwable> failure) {
    return new AeriusRequestCallback<T>(success, failure);
  }

  public static <T> AeriusRequestCallback<String> createRawAsync(final AsyncCallback<T> callback, final Function<String, T> parser) {
    return create(s -> {
      try {
        callback.onSuccess(parser.apply(s));
      } catch (final RuntimeException e) {
        GWT.log("Got runtime exception in onSuccess call", e);
        callback.onFailure(e);
      }
    }, e -> callback.onFailure(e));
  }

  @SuppressWarnings("unchecked")
  public static <T> AeriusRequestCallback<String> createAsync(final AsyncCallback<T> callback) {
    return createRawAsync(callback, s -> (T) Js.uncheckedCast(Global.JSON.parse(s)));
  }

  /**
   * Merges two {@code AsyncCallback}s to one which will call both.
   * @param callback1 First callback to call
   * @param callback2 Second callback to call
   * @return Merged callback
   * @param <T>
   */
  public static <T> AsyncCallback<T> merge(final AsyncCallback<T> callback1, final AsyncCallback<T> callback2) {
    return new AsyncCallback<T>() {
      @Override public void onSuccess(final T t) {
        callback1.onSuccess(t);
        callback2.onSuccess(t);
      }

      @Override public void onFailure(final Throwable throwable) {
        callback1.onFailure(throwable);
        callback2.onFailure(throwable);
      }
    };
  }
}
