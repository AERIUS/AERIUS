/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.importer.content;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.wui.application.components.importer.domain.ImportAction.ActionType;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component
public class MultipleFileContentsComponent extends BasicVueComponent {
  @Prop(required = true) @JsProperty List<FileUploadStatus> files;

  @Inject ApplicationContext applicationContext;

  @JsMethod
  public boolean hasSectionTitle(final FileUploadStatus status, final int idx) {
    return getSectionTitle(status, idx) != null;
  }

  @JsMethod
  public String getSectionTitle(final FileUploadStatus file, final int idx) {
    // This seems a bit hacky, but it's okay because it doesn't really go wrong
    // Basically: Only for the first occurrence of calculation points, is a header shown
    // The first occurrence of calculation points is determined by the sort order
    // In all other cases, the situation name is shown
    if (file.getSituationStats().isArchive()) {
      return M.messages().importFileContentArchiveContributionHeader();
    } else if (file.getSituationStats().getCalculationPoints() > 0
        && files.stream()
            .filter(v -> !isAddCalculationPoints(v))
            .count() == idx) {
      return M.messages().importFileContentOtherHeader();
    } else {
      return Optional.ofNullable(file.getImportAction())
          .map(v -> v.getVirtualSituation())
          .map(v -> v.getName())
          .orElse(file.getSituationName());
    }
  }

  @JsMethod
  public boolean canHaveCalculationJobImport(final FileUploadStatus file) {
    return FileUploadStatus.canHaveCalculationJobImport(file, files,
        t -> applicationContext.getConfiguration().isAllowed(t));
  }

  private static boolean isAddCalculationPoints(final FileUploadStatus fus) {
    return Optional.ofNullable(fus.getImportAction())
        .map(v -> v.getType())
        .orElse(null) == ActionType.ADD_CALCULATION_POINTS;
  }
}
