/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context.calculation;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import com.google.gwt.user.client.Timer;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.wui.application.context.result.ResultSummaryContext;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationInfo;
import nl.overheid.aerius.wui.application.domain.summary.DecisionFrameworkResults;
import nl.overheid.aerius.wui.application.domain.summary.JobSummary;
import nl.overheid.aerius.wui.application.util.UUID;

/**
 * A context object representing the metadata and parameters for the execution of a particular calculation
 */
public class CalculationExecutionContext {
  private String executionInputHash = null;

  private String internalId = null;
  private String jobKey = null;
  // Keep track if this an imported job with results
  private boolean importedJobWithResults = false;

  private CalculationInfo calculationInfo = null;
  private JobSummary jobSummary = null;
  private DecisionFrameworkResults decisionFrameworkResults = null;
  private ResultSummaryContext resultContext = null;
  private CalculationJobContext jobContext = null;

  /**
   * Need this primarily for the.... name
   */
  private @JsProperty List<SituationHandle> situations = new ArrayList<>();

  private Timer lastUpdateTimer = null;
  private int lastUpdate = 0;

  public CalculationExecutionContext() {
    this.internalId = UUID.uuidv4();
    this.calculationInfo = CalculationInfo.create();
    this.resultContext = new ResultSummaryContext();
    this.lastUpdateTimer = new Timer() {
      @Override
      public void run() {
        lastUpdate++;
      }
    };
  }

  public String getJobKey() {
    return jobKey;
  }

  public void setJobKey(final String jobKey) {
    this.jobKey = jobKey;
  }

  public JobState getJobState() {
    return Optional.ofNullable(calculationInfo)
        .map(v -> v.getJobProgress())
        .map(v -> v.getState())
        .orElse(JobState.UNDEFINED);
  }

  public String getJobErrorMessage() {
    return Optional.ofNullable(calculationInfo)
        .map(v -> v.getJobProgress())
        .map(v -> v.getErrorMessage())
        .orElse("");
  }

  @JsMethod
  public boolean isNone() {
    return getJobState() == JobState.UNDEFINED;
  }

  public boolean isError() {
    return getJobState() == JobState.ERROR;
  }

  @JsMethod
  public boolean isCalculationCompleted() {
    return getJobState() == JobState.COMPLETED;
  }

  public boolean isCancelled() {
    return getJobState() == JobState.CANCELLED;
  }

  public boolean isRunning() {
    final JobState jobState = getJobState();

    return !jobState.isFinalState();
  }

  @JsMethod
  public boolean isCalculationFinalState() {
    return Optional.of(getJobState())
        .map(v -> v.isFinalState())
        .orElseThrow(() -> new RuntimeException("Could not determine job state finality."));
  }

  @JsMethod
  public boolean isCalculating() {
    final JobState jobState = getJobState();

    return jobState == JobState.CALCULATING || jobState == JobState.POST_PROCESSING;
  }

  public void setCalculationInfo(final CalculationInfo calculationInfo) {
    this.calculationInfo = calculationInfo;
  }

  public CalculationInfo getCalculationInfo() {
    return calculationInfo;
  }

  public JobSummary getJobSummary() {
    return jobSummary;
  }

  public void setJobSummary(final JobSummary jobSummary) {
    this.jobSummary = jobSummary;
  }

  public DecisionFrameworkResults getDecisionFrameworkResults() {
    return decisionFrameworkResults;
  }

  public void setDecisionFrameworkResults(final DecisionFrameworkResults decisionFrameworkResults) {
    this.decisionFrameworkResults = decisionFrameworkResults;
  }

  public ResultSummaryContext getResultContext() {
    return resultContext;
  }

  public List<SituationHandle> getSituations() {
    return situations;
  }

  public void setSituations(final List<SituationHandle> situations) {
    this.situations = situations;
  }

  public String getExecutionInputHash() {
    return executionInputHash;
  }

  public void setExecutionInputHash(final String inputHash) {
    this.executionInputHash = inputHash;
  }

  public String getInternalCalculationId() {
    return internalId;
  }

  public void setInternalCalculationId(final String id) {
    this.internalId = id;
  }

  public CalculationJobContext getJobContext() {
    return jobContext;
  }

  public void setJobContext(final CalculationJobContext jobContext) {
    this.jobContext = jobContext;
  }

  @Override
  public int hashCode() {
    return Objects.hash(internalId);
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final CalculationExecutionContext other = (CalculationExecutionContext) obj;
    return Objects.equals(internalId, other.internalId);
  }

  // This is a computed util method and as such it is made a public static
  public static SituationHandle findHandleFromId(final CalculationExecutionContext context, final String id) {
    return context.getSituations().stream()
        .filter(v -> id.equals(v.getId()))
        .findFirst().orElse(null);
  }

  public void startLastUpdateTimer() {
    lastUpdateTimer.scheduleRepeating(1000);
  }

  public void stopLastUpdateTimer() {
    lastUpdateTimer.cancel();
  }

  public void resetLastUpdate() {
    lastUpdate = 0;
  }

  public int getLastUpdate() {
    return lastUpdate;
  }

  public boolean isImportedJobWithResults() {
    return importedJobWithResults;
  }

  public void setImportedJobWithResults() {
    importedJobWithResults = true;
  }
}
