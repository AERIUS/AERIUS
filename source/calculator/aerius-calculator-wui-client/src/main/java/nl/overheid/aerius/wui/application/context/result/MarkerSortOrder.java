/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context.result;

import java.util.Comparator;

import nl.overheid.aerius.shared.domain.result.CalculationMarker;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;

public class MarkerSortOrder implements Comparable<MarkerSortOrder> {
  private static final Comparator<MarkerSortOrder> BY_MARKER = Comparator.comparing(v -> v.getMarker().getOrder());
  private static final Comparator<MarkerSortOrder> BY_TYPE = Comparator.comparing(v -> v.getResultType().getOrder());

  private final CalculationMarker marker;
  private final ScenarioResultType resultType;

  public MarkerSortOrder(final CalculationMarker marker, final ScenarioResultType resultType) {
    this.marker = marker;
    this.resultType = resultType;
  }

  @Override
  public int compareTo(final MarkerSortOrder other) {
    return BY_MARKER.thenComparing(BY_TYPE)
        .compare(this, other);
  }

  private CalculationMarker getMarker() {
    return marker;
  }

  private ScenarioResultType getResultType() {
    return resultType;
  }
}
