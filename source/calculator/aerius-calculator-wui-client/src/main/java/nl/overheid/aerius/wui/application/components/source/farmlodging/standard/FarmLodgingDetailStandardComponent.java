/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.farmlodging.standard;

import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategories;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.farm.AdditionalLodgingSystem;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmLodgingStandardUtil;
import nl.overheid.aerius.wui.application.domain.source.farm.LodgingFodderMeasure;
import nl.overheid.aerius.wui.application.domain.source.farm.ReductiveLodgingSystem;
import nl.overheid.aerius.wui.application.domain.source.farm.StandardFarmLodging;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "aer-farmlodging-detail-standard")
public class FarmLodgingDetailStandardComponent extends BasicVueComponent {

  private static final int EMISSION_FACTOR_PRECISION = 3;

  @Prop @JsProperty protected StandardFarmLodging source;
  @Data int index;

  @Data @Inject ApplicationContext applicationContext;

  @JsMethod
  public FarmLodgingCategories getCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getFarmLodgingCategories();
  }

  @Computed
  public String getFarmLodgingEmissionFactor() {
    final double emissionFactor = FarmLodgingStandardUtil.getEmissionFactor(getCategories(), source, Substance.NH3);
    final FarmEmissionFactorType emissionFactorType = FarmLodgingStandardUtil.getEmissionFactorType(getCategories(), source);
    final String emissionFactorValue = MessageFormatter.toCapped(emissionFactor, EMISSION_FACTOR_PRECISION);
    if (hasFarmReductiveLodgingSystemCategories()) {
      return emissionFactor > 0 ? emissionFactorValue : "-";
    } else {
      return emissionFactor > 0 ? emissionFactorValue + " " + i18n.esFarmEmissionFactorType(emissionFactorType) : "-";
    }
  }

  @Computed
  protected String emission() {
    return emission2String(FarmLodgingStandardUtil.getFlatEmission(getCategories(), source, Substance.NH3));
  }

  @Computed("constrained")
  protected boolean isConstrained() {
    return FarmLodgingStandardUtil.isConstrained(getCategories(), source);
  }

  @Computed
  protected String getFarmLodgingUnconstrainedEmissionFactor() {
    return MessageFormatter.toCapped(FarmLodgingStandardUtil.getUnconstrainedEmissionFactor(getCategories(), source, Substance.NH3),
        EMISSION_FACTOR_PRECISION);
  }

  @Computed
  protected String unconstrainedEmission() {
    return emission2String(FarmLodgingStandardUtil.getUnconstrainedFlatEmission(getCategories(), source, Substance.NH3));
  }

  @Computed
  String getFarmLodingNumberOfDays() {
    final FarmEmissionFactorType emissionFactorType = FarmLodgingStandardUtil.getEmissionFactorType(getCategories(), source);
    return emissionFactorType == FarmEmissionFactorType.PER_ANIMAL_PER_DAY ? Integer.toString(source.getNumberOfDays()) : "-";
  }

  @JsMethod
  protected String additionalFactor(final String lodgingSystemCode) {
    return MessageFormatter.toCapped(Optional.ofNullable(getCategories().determineAdditionalLodgingSystemByCode(lodgingSystemCode))
        .map(v -> v.getEmissionFactor())
        .orElse(0.0), EMISSION_FACTOR_PRECISION);
  }

  @JsMethod
  protected String additionalEmission(final AdditionalLodgingSystem lodgingSystem) {
    return emission2String(FarmLodgingStandardUtil.getEmissionsAdditional(getCategories(), source, Substance.NH3, lodgingSystem));
  }

  @JsMethod
  public boolean hasFarmReductiveLodgingSystemCategories() {
    return FarmLodgingStandardUtil.hasFarmReductiveLodgingSystemCategories(getCategories());
  }

  @JsMethod
  protected String reductiveReduction(final String lodgingSystemCode) {
    final double reductionFactor = Optional.ofNullable(getCategories().determineReductiveLodgingSystemByCode(lodgingSystemCode))
        .map(v -> v.getReductionFactor())
        .orElse(0.0);
    return MessageFormatter.formatPercentageWithUnit(reductionFactor * 100, 0);
  }

  @JsMethod
  protected String reductiveEmission(final ReductiveLodgingSystem lodgingSystem) {
    return emission2String(FarmLodgingStandardUtil.getEmissionsReductive(getCategories(), source, Substance.NH3, lodgingSystem));
  }

  @JsMethod
  protected String fodderReduction(final String fodderMeasureCode) {
    final double reductionFactorTotal = Optional.ofNullable(getCategories().determineLodgingFodderMeasureByCode(fodderMeasureCode))
        .map(v -> v.getReductionFactorTotal())
        .orElse(0.0);
    return MessageFormatter.formatPercentageWithUnit(reductionFactorTotal * 100, 0);
  }

  @JsMethod
  protected String fodderEmission(final LodgingFodderMeasure lodgingFodderMeasure) {
    return emission2String(FarmLodgingStandardUtil.getEmissionsFodder(getCategories(), source, Substance.NH3, lodgingFodderMeasure));
  }

  @Computed
  protected String detail() {
    final FarmLodgingCategory category = getCategories().determineFarmLodgingCategoryByCode(source.getFarmLodgingCode());
    return category == null ? "" : category.getDescription();
  }

  @Computed
  protected String classOnlyEmission() {
    return "";
  }

  protected String emission2String(final double emission) {
    return MessageFormatter.formatEmissionWithUnitSmart(emission);
  }
}
