/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.geo.layers.calculationpoint;

import java.util.Collections;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPoint;
import nl.overheid.aerius.wui.application.context.MapConfigurationContext;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;
import nl.overheid.aerius.wui.application.event.DeleteAllCalculationPointsEvent;
import nl.overheid.aerius.wui.application.event.calculationpoint.CalculationPointAddedEvent;
import nl.overheid.aerius.wui.application.event.calculationpoint.CalculationPointDeleteEvent;
import nl.overheid.aerius.wui.application.event.calculationpoint.CalculationPointSaveEvent;
import nl.overheid.aerius.wui.application.event.calculationpoint.CalculationPointsAddedEvent;
import nl.overheid.aerius.wui.application.geo.layers.LabelStyle;
import nl.overheid.aerius.wui.application.geo.layers.base.BaseClusteredMarkerLayer;
import nl.overheid.aerius.wui.application.geo.layers.base.ClusteredLabelStyleCreator;
import nl.overheid.aerius.wui.application.geo.layers.base.VectorFeaturesWrapper;
import nl.overheid.aerius.wui.application.i18n.M;

/**
 * Layer to show the {@link CalculationPoint}s on the map.
 */
public class CalculationPointMarkerLayer extends BaseClusteredMarkerLayer<CalculationPointFeature> {
  interface CalculationPointMarkerLayerEventBinder extends EventBinder<CalculationPointMarkerLayer> {}

  private static final CalculationPointMarkerLayerEventBinder EVENT_BINDER = GWT.create(CalculationPointMarkerLayerEventBinder.class);

  private final VectorFeaturesWrapper vectorObject = new VectorFeaturesWrapper();

  @Inject
  public CalculationPointMarkerLayer(@Assisted final int zIndex, final EventBus eventBus, final MapConfigurationContext mapConfigurationContext) {
    super(new LayerInfo(), zIndex, createLabelStyleCreator(mapConfigurationContext));
    getInfo().setName(CalculationPointMarkerLayer.class.getName());

    setFeatures(vectorObject, Collections.emptyList());
    asLayer().setVisible(true);
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  private static ClusteredLabelStyleCreator<CalculationPointFeature> createLabelStyleCreator(final MapConfigurationContext mapConfigurationContext) {
    return new ClusteredLabelStyleCreator<CalculationPointFeature>(mapConfigurationContext::getIconScale, false, LabelStyle.CALCULATION_POINT) {
      @Override
      protected String clusterLabel(final int count, final LabelStyle labelStyle) {
        return M.messages().calculationPointMarkerClusterLabel(count);
      }
    };
  }

  @EventHandler
  public void onCalculationPointSaveEvent(final CalculationPointSaveEvent e) {
    vectorObject.updateFeature(e.getValue());
  }

  @EventHandler
  public void onDeleteAllCalculationPointsEvent(final DeleteAllCalculationPointsEvent e) {
    vectorObject.clear();
    clear();
  }

  @EventHandler
  public void onCalculationPointsAddedEvent(final CalculationPointsAddedEvent e) {
    vectorObject.addFeatures(e.getValue());
  }

  @EventHandler
  public void onCalculationPointAddedEvent(final CalculationPointAddedEvent e) {
    vectorObject.addFeature(e.getValue());
  }

  @EventHandler
  public void onRemoveCalculationPointCommand(final CalculationPointDeleteEvent e) {
    vectorObject.removeFeature(e.getValue());
  }
}
