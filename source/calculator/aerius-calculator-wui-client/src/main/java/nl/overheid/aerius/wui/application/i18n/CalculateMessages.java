/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.i18n;

import com.google.gwt.i18n.client.Messages.Select;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.calculation.RoadLocalFractionNO2Option;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.scenario.SituationType;

/**
 * Text specific for calculate settings pages.
 */
public interface CalculateMessages {
  String calculationJob();
  String calculationTitle(String id);
  String calculationAddJob();

  String calculationJobPanelCalculationJobSettings();
  String calculationJobPanelDevelopmentPressureSearch();
  String calculationJobPanelMeteorologicalSettings();
  String calculationJobPanelAdvancedSettings();

  String calculationJobDetailScenarios();
  String calculationJobDetailDevelopmentPressureSearch();
  String calculationJobDetailMeteorologicalSettings();
  String calculationJobDetailAdvancedSettings();

  String calculationJobDetailDPSSelectedSources();

  String calculateApplicationVersion();
  String calculateEmptyJobs();
  String calculationJobMayStop();
  String calculationJobMayCancel();
  String calculationJobsDeleteWarning();
  String calculationStatus(@Select JobState state);
  String calculationJobsTitle();
  String calculationJobsDeleteButton();
  String calculationJobTypeLabel();
  String calculationJobTypeExplanation(@Select CalculationJobType calculationJobType);
  String calculationJobType(@Select CalculationJobType calculationJobType);
  String calculationJobDevelopmentPressureSearchHeader();
  String calculationJobDevelopmentPressureSearchAmount(String amountOfTotal);
  String calculationJobProjectCategory();
  String calculationMethod(@Select Theme theme, @Select CalculationMethod calculationMethod);
  String calculateScenarioSummaryTitle();
  String calculateScenarioIndexHeader();
  String calculateScenarioHeader();
  String calculateScenarioTypeHeader();
  String calculateYearHeader();
  String calculateNettingHeader();

  String calculateEmissionSourcesHeader();
  String calculateSituationSelectorLabel(@Select SituationType situationType);

  String calculateMetDatasetTypeFlowOption(@Select String option);
  String calculateMetSiteFlowOption(@Select String option);
  String calculateMeteoDataTypeLabel();
  String calculateMeteoSiteTypeLabel();
  String calculateMetDatasetType(@Select String datasetType);
  String calculateMetSearchPlaceholder();
  String calculateMetSuggestNoResults();
  String calculateMetSearchNoResults();
  String calculateMeteoSiteYearsLabel();
  String calculateMetYearSelectionCriteria();
  String calculateMetYearSelectRecents(int numberOfYears);
  String calculateMeteoSiteRemarks();

  String metSiteMarkerClusterLabel(int numberOfMetSites);

  String calculateDispersionOptionsTitle();
  String calculateCompositionHasNoEmissions();
  String calculateType();
  String calculationMethodFormalAssessmentExplanation();
  String calculatePermitAreaLabel();
  String calculateZoneOfInfluenceLabel();
  String calculateMeteoSiteLocationsLabel();
  String calculateObukhovLengthLabel();
  String calculateSurfaceAlbedoLabel();
  String calculatePriestleyTaylorLabel();
  String calculatePlumeDepletionLabel();
  String calculateComplexTerrainLabel();
  String calculateEmissionResultKey(@Select EmissionResultKey emissionResultKey);
  String calculateIncludedScenario();
  String calculateInCombinationAssessment();
  String calculateInCombinationUserScenarios();
  String calculateInCombinationArchiveEnable();
  String calculateEmptyListSign();
  String calculateSummaryCalculated(String surface);
  String calculateSummaryViewResults();
  String calculateSummaryStartCalculation();
  String calculateSummaryCancelCalculation();
  String calculateSummaryRemoveCalculation();
  String calculateSituationNoneSelected();
  String calculateInCombinationCompositionInvalid();
  String calculateDevelopmentPressureSearchMissingProposed();

  String calculateSituationsMissing(String types);
  String calculateExportSituationNoneSelected();
  String calculateExportPermitProposedRequired();
  String calculateSituationLimit(int limit);

  String calculatePrimaryfNO2Title();
  String calculatePrimaryfNO2CustomLabel();
  String calculatePrimaryfNO2Option(@Select RoadLocalFractionNO2Option fNO2Option);
  String calculatePrimaryfNO2PointsLabel();
  String calculatePrimaryfNO2ReceptorPointsLabel();

  String calculateInvalidCustomPointsOption();
  String calculateSourcesSomeHaveNoEmissions();

  String calculationJobStatusTooltipCompleted();
  String calculationJobStatusTooltipFailed(String erorrMessage);
  String calculationJobStatusTooltipQueued();
  String calculationJobStatusTooltipRunning();

  String calculationStaleWarning();
}
