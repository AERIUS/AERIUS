/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.util;

import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.shared.domain.PdfProductType;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.ui.pages.results.GeneralizedSurfaceType;

public class ResultStatisticUtil {

  private ResultStatisticUtil() {
    // Util class
  }

  public static List<ResultStatisticType> getStatisticsForSummary(final Theme theme, final ScenarioResultType resultType,
      final SummaryHexagonType hexagonType, final EmissionResultKey emissionResultKey) {
    return getStatistics(theme, resultType, hexagonType, true, null, emissionResultKey);
  }

  public static List<ResultStatisticType> getStatisticsForResult(final Theme theme, final ScenarioResultType resultType,
      final SummaryHexagonType hexagonType, final EmissionResultKey emissionResultKey) {
    return getStatistics(theme, resultType, hexagonType, false, null, emissionResultKey);
  }

  public static List<ResultStatisticType> getStatisticsForPdfResult(final Theme theme, final ScenarioResultType resultType,
      final SummaryHexagonType hexagonType, final PdfProductType pdfProductType, final EmissionResultKey emissionResultKey) {
    return getStatistics(theme, resultType, hexagonType, false, pdfProductType, emissionResultKey);
  }

  private static List<ResultStatisticType> getStatistics(final Theme theme, final ScenarioResultType resultType,
      final SummaryHexagonType hexagonType, final boolean summary, final PdfProductType pdfProductType, final EmissionResultKey emissionResultKey) {
    if (hexagonType == SummaryHexagonType.CUSTOM_CALCULATION_POINTS) {
      return getValuesForScenarioResultTypeCustomPoints(resultType);
    }
    return getValuesForScenarioResultTypeHexagons(theme, resultType, hexagonType, summary, pdfProductType, emissionResultKey);
  }

  /**
   * NOTE: Ideally this is defined statically or using very few parameters, but until the features hitting this code are less subject to change /
   * volatile, it would take a huge amount effort to maintain that. So for now, we can allow some logic and complexity in this method / util.
   */
  private static List<ResultStatisticType> getValuesForScenarioResultTypeHexagons(final Theme theme, final ScenarioResultType resultType,
      final SummaryHexagonType hexagonType, final boolean summary, final PdfProductType pdfProductType, final EmissionResultKey emissionResultKey) {
    final List<ResultStatisticType> resultStatisticTypes = new ArrayList<>();

    // Always add either the sum of cartographic surface or the number of points.
    resultStatisticTypes.add(surfaceOrPoints(hexagonType, emissionResultKey));

    switch (resultType) {
    case SITUATION_RESULT:
      addSituationResultStatistics(resultStatisticTypes);
      break;
    case PROJECT_CALCULATION:
    case IN_COMBINATION:
    case ARCHIVE_CONTRIBUTION:
      addComparisonResultStatistics(resultStatisticTypes, theme, hexagonType, summary, pdfProductType, emissionResultKey);
      break;
    case MAX_TEMPORARY_EFFECT:
      addTemporaryEffectStatistics(resultStatisticTypes);
      break;
    case DEPOSITION_SUM:
      addDepositionSumStatistics(resultStatisticTypes, hexagonType);
    }

    return resultStatisticTypes;
  }

  private static void addSituationResultStatistics(final List<ResultStatisticType> resultStatisticTypes) {
    resultStatisticTypes.add(ResultStatisticType.MAX_TOTAL);
    resultStatisticTypes.add(ResultStatisticType.MAX_CONTRIBUTION);
  }

  private static void addTemporaryEffectStatistics(final List<ResultStatisticType> resultStatisticTypes) {
    resultStatisticTypes.add(ResultStatisticType.MAX_TOTAL);
    resultStatisticTypes.add(ResultStatisticType.MAX_TEMP_INCREASE);
  }

  private static void addDepositionSumStatistics(final List<ResultStatisticType> resultStatisticTypes, final SummaryHexagonType hexagonType) {
    if (hexagonType != SummaryHexagonType.EXTRA_ASSESSMENT_HEXAGONS) {
      resultStatisticTypes.add(0, ResultStatisticType.COUNT_RECEPTORS);
    }
    resultStatisticTypes.add(ResultStatisticType.SUM_CONTRIBUTION);
    resultStatisticTypes.add(ResultStatisticType.MAX_CONTRIBUTION);
  }

  private static void addComparisonResultStatistics(final List<ResultStatisticType> resultStatisticTypes, final Theme theme,
      final SummaryHexagonType hexagonType, final boolean summary, final PdfProductType pdfProductType,
      final EmissionResultKey emissionResultKey) {
    // For the summary, not all values are shown
    if (summary) {
      if (theme == Theme.NCA) {
        // For the NCA theme, let the MAX_PERCENTAGE_CRITICAL_LEVEL statistic take precedence over the MAX_TOTAL statistic
        resultStatisticTypes.add(ResultStatisticType.MAX_PERCENTAGE_CRITICAL_LEVEL);
      } else {
        resultStatisticTypes.add(ResultStatisticType.MAX_TOTAL);
      }
    } else {
      resultStatisticTypes.add(ResultStatisticType.MAX_TOTAL);

      // Do not include for OwN2000 theme when assembling for print
      if (!(theme == Theme.OWN2000 && pdfProductType != null)) {
        resultStatisticTypes.add(ResultStatisticType.MAX_PERCENTAGE_CRITICAL_LEVEL);
      }
    }
    resultStatisticTypes.add(surfaceOrPointsIncrease(hexagonType, emissionResultKey));
    resultStatisticTypes.add(ResultStatisticType.MAX_INCREASE);

    // do not add the decrease in OwN2000 permit print
    if (pdfProductType != PdfProductType.OWN2000_PERMIT) {
      resultStatisticTypes.add(surfaceOrPointsDecrease(hexagonType, emissionResultKey));
      resultStatisticTypes.add(ResultStatisticType.MAX_DECREASE);
    }
  }

  private static ResultStatisticType surfaceOrPoints(final SummaryHexagonType hexagonType, final EmissionResultKey emissionResultKey) {
    final GeneralizedSurfaceType surfaceType =
        GeneralizedSurfaceType.from(emissionResultKey.getEmissionResultType(), hexagonType);
    return surfaceType.getStatisticType();
  }

  private static ResultStatisticType surfaceOrPointsIncrease(final SummaryHexagonType hexagonType, final EmissionResultKey emissionResultKey) {
    final GeneralizedSurfaceType surfaceType =
        GeneralizedSurfaceType.from(emissionResultKey.getEmissionResultType(), hexagonType);
    ResultStatisticType returnType;
    switch (surfaceType) {
    case POINTS:
      returnType = ResultStatisticType.COUNT_CALCULATION_POINTS_INCREASE;
      break;
    case RECEPTORS:
      returnType = ResultStatisticType.COUNT_RECEPTORS_INCREASE;
      break;
    case SURFACE:
    default:
      returnType = ResultStatisticType.SUM_CARTOGRAPHIC_SURFACE_INCREASE;
      break;
    }
    return returnType;
  }

  private static ResultStatisticType surfaceOrPointsDecrease(final SummaryHexagonType hexagonType, final EmissionResultKey emissionResultKey) {
    final GeneralizedSurfaceType surfaceType =
        GeneralizedSurfaceType.from(emissionResultKey.getEmissionResultType(), hexagonType);
    ResultStatisticType returnType;
    switch (surfaceType) {
    case POINTS:
      returnType = ResultStatisticType.COUNT_CALCULATION_POINTS_DECREASE;
      break;
    case RECEPTORS:
      returnType = ResultStatisticType.COUNT_RECEPTORS_DECREASE;
      break;
    case SURFACE:
    default:
      returnType = ResultStatisticType.SUM_CARTOGRAPHIC_SURFACE_DECREASE;
      break;
    }
    return returnType;
  }

  private static List<ResultStatisticType> getValuesForScenarioResultTypeCustomPoints(final ScenarioResultType resultType) {
    final List<ResultStatisticType> resultStatisticTypes = new ArrayList<>();
    resultStatisticTypes.add(ResultStatisticType.COUNT_CALCULATION_POINTS);

    switch (resultType) {
    case SITUATION_RESULT:
      resultStatisticTypes.add(ResultStatisticType.MAX_CONTRIBUTION);
      break;
    case PROJECT_CALCULATION:
      resultStatisticTypes.add(ResultStatisticType.COUNT_CALCULATION_POINTS_INCREASE);
      resultStatisticTypes.add(ResultStatisticType.COUNT_CALCULATION_POINTS_DECREASE);
      resultStatisticTypes.add(ResultStatisticType.MAX_INCREASE);
      resultStatisticTypes.add(ResultStatisticType.MAX_DECREASE);
      break;
    case MAX_TEMPORARY_EFFECT:
      resultStatisticTypes.add(ResultStatisticType.MAX_TEMP_INCREASE);
      break;
    default:
      break;
    }

    return resultStatisticTypes;
  }

  public static List<ResultStatisticType> getStatisticsForHabitat(final ScenarioResultType resultType) {
    final List<ResultStatisticType> resultStatisticTypes = new ArrayList<>();

    switch (resultType) {
    case SITUATION_RESULT:
      resultStatisticTypes.add(ResultStatisticType.MAX_TOTAL);
      resultStatisticTypes.add(ResultStatisticType.MAX_CONTRIBUTION);
      break;
    case PROJECT_CALCULATION:
    case IN_COMBINATION:
    case ARCHIVE_CONTRIBUTION:
      resultStatisticTypes.add(ResultStatisticType.MAX_PERCENTAGE_CRITICAL_LEVEL);
      resultStatisticTypes.add(ResultStatisticType.MAX_TOTAL);
      resultStatisticTypes.add(ResultStatisticType.MAX_INCREASE);
      resultStatisticTypes.add(ResultStatisticType.MAX_DECREASE);
      break;
    case MAX_TEMPORARY_EFFECT:
      resultStatisticTypes.add(ResultStatisticType.MAX_TEMP_INCREASE);
      resultStatisticTypes.add(ResultStatisticType.MAX_TOTAL);
      break;
    case DEPOSITION_SUM:
      resultStatisticTypes.add(ResultStatisticType.PROCUREMENT_POLICY_THRESHOLD_PASS);
      resultStatisticTypes.add(ResultStatisticType.PROCUREMENT_POLICY_THRESHOLD_FAIL);
      break;
    }

    return resultStatisticTypes;
  }
}
