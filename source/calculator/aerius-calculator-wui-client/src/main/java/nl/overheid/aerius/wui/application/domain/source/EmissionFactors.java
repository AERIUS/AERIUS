/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.wui.vue.SkipReactivityValidations;

@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class EmissionFactors implements JsPropertyMap<Double> {

  public static final @JsOverlay EmissionFactors create() {
    return Js.uncheckedCast(JsPropertyMap.of());
  }

  public static final @JsOverlay void init(final EmissionFactors props, final Substance... substances) {
    // Initialize to the requested substances to 0
    for (final Substance substance : substances) {
      if (props.get(substance.name()) == null) {
        props.setEmissionFactor(substance, 0D);
      }
    }
  }

  @SkipReactivityValidations
  public final @JsOverlay double getEmissionFactor(final Substance substance) {
    final Object emission = get(substance.name());
    return emission == null ? 0.0 : Js.coerceToDouble(emission);
  }

  public final @JsOverlay void setEmissionFactor(final Substance substance, final double emission) {
    set(substance.name(), emission);
  }
}
