/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.calculate.options;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.sector.category.SimpleCategory;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobTypeChangedCommand;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.InputHintComponent;
import nl.overheid.aerius.wui.application.components.input.InputWithUnitComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.notice.NoticeRule;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.domain.DevelopmentPressureUtil;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.application.event.CalculationJobCompositionChangedEvent;
import nl.overheid.aerius.wui.application.ui.pages.calculate.CalculateValidations;
import nl.overheid.aerius.wui.application.ui.pages.calculate.options.CalculationJobSettingsValidators.CalculationMethodSelectionValidations;
import nl.overheid.aerius.wui.application.ui.pages.export.types.components.OtherSituationCompositor;

@Component(customizeOptions = {
    CalculationJobSettingsValidators.class
}, components = {
    LabeledInputComponent.class,
    SimplifiedListBoxComponent.class,
    VerticalCollapse.class,
    ValidationBehaviour.class,
    InputWithUnitComponent.class,
    InputHintComponent.class,
    InputErrorComponent.class,
    CalculationJobInCombinationCompositorComponent.class,
    OtherSituationCompositor.class,
})
public class CalculationJobSettingsComponent extends ErrorWarningValidator
    implements HasValidators<CalculationMethodSelectionValidations>, HasCreated {
  @Prop(required = true) CalculationSetOptions calculationOptions;
  @Prop(required = true) SituationComposition composition;

  @Prop EventBus eventBus;

  @Inject @Data ApplicationContext applicationContext;
  @Inject @Data ScenarioContext scenarioContext;

  @Data String zoneOfInfluenceV;
  @Data String permitAreaV;
  @Data String projectCategoryV;
  @Data Function<Object, Object> idFromSit = v -> v == null ? "null" : ((SituationContext) v).getId();

  @JsProperty(name = "$v") CalculationMethodSelectionValidations validation;

  @Override
  public void created() {
    VuelidateUtil.proxy(this);

    zoneOfInfluenceV = String.valueOf(calculationOptions.getCalculateMaximumRange());
    permitAreaV = getPermitArea();
    projectCategoryV = getProjectCategory();
  }

  @Computed("isShowFormalAssessmentExplanation")
  public boolean isShowFormalAssessmentExplanation() {
    final boolean isSlightlyPedantic = applicationContext.getTheme() == Theme.NCA;
    final boolean isFormalAssessment = getCalculationMethod() == CalculationMethod.FORMAL_ASSESSMENT;
    return isSlightlyPedantic && isFormalAssessment;
  }

  @Override
  @Computed
  public CalculationMethodSelectionValidations getV() {
    return validation;
  }

  @Computed("hasPermitAreaOption")
  public boolean hasPermitAreaOption() {
    return applicationContext.isAppFlagEnabled(AppFlag.HAS_CALCULATION_PERMIT_AREA);
  }

  @Computed("hasZoneOfInfluenceOption")
  public boolean hasZoneOfInfluenceOption() {
    return getCalculationMethod() == CalculationMethod.NATURE_AREA || getCalculationMethod() == CalculationMethod.QUICK_RUN;
  }

  @Watch(value = "calculationOptions", isImmediate = true)
  public void onCalculationOptionsChange(final CalculationSetOptions neww) {
    zoneOfInfluenceV = String.valueOf(neww.getCalculateMaximumRange());
  }

  @Computed
  public void setZoneOfInfluence(final String val) {
    this.zoneOfInfluenceV = val;
    ValidationUtil.setSafeDoubleValue(v -> calculationOptions.setCalculateMaximumRange(v), val, 0D);
  }

  @JsMethod
  protected String zoneOfInfluenceError() {
    return i18n.ivDoubleLowerlimit(i18n.calculateZoneOfInfluenceLabel(), 0);
  }

  @Computed("isZoneOfInfluenceInvalid")
  public boolean isZoneOfInfluenceInvalid() {
    return hasZoneOfInfluenceOption() && validation.zoneOfInfluence.invalid;
  }

  @Computed
  public String getZoneOfInfluence() {
    return zoneOfInfluenceV;
  }

  @Computed
  public Function<Object, Object> getCategoryKeyFunction() {
    return category -> ((SimpleCategory) category).getCode();
  }

  @Computed
  public Function<Object, String> getCategoryNameFunction() {
    return category -> ((SimpleCategory) category).getName();
  }

  @Computed
  public List<SimpleCategory> getProjectCategories() {
    return applicationContext.getConfiguration().getCalculationProjectCategories();
  }

  @Computed
  public boolean isProjectCategoryInvalid() {
    return hasProjectCategoryOption() && validation.projectCategory.invalid;
  }

  @JsMethod
  protected boolean hasProjectCategoryOption() {
    return applicationContext.isAppFlagEnabled(AppFlag.HAS_CALCULATION_PROJECT_CATEGORY);
  }

  @Computed
  public String getProjectCategory() {
    return !hasProjectCategoryOption()
        || calculationOptions.getNcaCalculationOptions().getProjectCategory() == null
            ? ""
            : calculationOptions.getNcaCalculationOptions().getProjectCategory();
  }

  @JsMethod
  public void selectProjectCategory(final String projectCategory) {
    projectCategoryV = projectCategory;
    calculationOptions.getNcaCalculationOptions().setProjectCategory(projectCategory);

    updateZoneOfInfluence();
  }

  private void updateZoneOfInfluence() {
    final Integer zoneOfInfluence = applicationContext.getConfiguration().getDefaultCalculationDistances()
        .getOrDefault(calculationOptions.getNcaCalculationOptions().getPermitArea(), new HashMap<>())
        .getOrDefault(projectCategoryV, null);
    if (zoneOfInfluence != null) {
      setZoneOfInfluence(zoneOfInfluence.toString());
    }
  }

  @Computed
  public List<SimpleCategory> getPermitAreas() {
    return applicationContext.getConfiguration().getCalculationPermitAreas();
  }

  @Computed
  public String getPermitArea() {
    return !hasPermitAreaOption()
        || calculationOptions.getNcaCalculationOptions().getPermitArea() == null
            ? ""
            : calculationOptions.getNcaCalculationOptions().getPermitArea();
  }

  @JsMethod
  public void selectPermitArea(final String permitArea) {
    permitAreaV = permitArea;
    calculationOptions.getNcaCalculationOptions().setPermitArea(permitArea);

    updateZoneOfInfluence();
  }

  @Computed
  public List<CalculationJobType> getCalculationJobTypes() {
    return applicationContext.getConfiguration().getCalculationJobTypes();
  }

  @Computed
  public CalculationJobType getCalculationJobType() {
    return calculationOptions.getCalculationJobType();
  }

  @JsMethod
  public void selectCalculationJobType(final CalculationJobType calculationJobType) {
    eventBus.fireEvent(new CalculationJobTypeChangedCommand(calculationJobType));
  }

  @Computed
  public List<CalculationMethod> getCalculationMethods() {
    return applicationContext.getConfiguration().getCalculationMethods();
  }

  @JsMethod
  public void selectCalculationMethod(final CalculationMethod calculationMethod) {
    calculationOptions.setCalculationMethod(calculationMethod);
  }

  @Computed
  public CalculationMethod getCalculationMethod() {
    return calculationOptions.getCalculationMethod();
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return isProjectCategoryInvalid()
        || isZoneOfInfluenceInvalid()
        || isPermitAreaInvalid()
        || getCalculateErrors().anyMatch(v -> v.hasNotice());
  }

  @Computed
  public boolean isPermitAreaInvalid() {
    return hasPermitAreaOption() && validation.permitArea.invalid;
  }

  @Computed("hasDropdownPerSituationType")
  public boolean hasDropdownPerSituationType() {
    return calculationOptions.getCalculationJobType() != CalculationJobType.SINGLE_SCENARIO;
  }

  @Computed("hasMultiSelect")
  public boolean hasMultiSelect() {
    return !calculationOptions.getCalculationJobType().getPluralSituationTypes().isEmpty();
  }

  @Computed
  public List<SituationType> getSingleSelectSituationTypes() {
    return applicationContext.getConfiguration().getAvailableSituationTypes().stream()
        .filter(st -> !calculationOptions.getCalculationJobType().isIllegal(st))
        .filter(st -> !calculationOptions.getCalculationJobType().isPlural(st))
        .collect(Collectors.toList());
  }

  @JsMethod
  public List<SituationContext> getOptions(final SituationType situationType) {
    return scenarioContext.getSituations().stream()
        .filter(sit -> sit.getType() == situationType)
        .collect(Collectors.toList());
  }

  @JsMethod
  public SituationContext getSelectedSituation(final SituationType situationType) {
    return composition.getSituation(situationType);
  }

  @Computed
  public SituationContext getSelectedSituation() {
    return composition.getSituations().stream().findFirst().orElse(null);
  }

  @JsMethod
  public void selectSituation(final SituationType type, final SituationContext situationContext) {
    if (type == null) {
      composition.clear();
      if (situationContext != null) {
        composition.addSituation(situationContext);
      }
    } else {
      final boolean shouldUpdateDevelopmentPressureSources = type == SituationType.PROPOSED && composition.getSituation(type) != situationContext;
      composition.setSituation(type, situationContext);
      if (shouldUpdateDevelopmentPressureSources) {
        DevelopmentPressureUtil.initDevelopmentPressureSources(applicationContext, composition, calculationOptions);
      }
    }
    eventBus.fireEvent(new CalculationJobCompositionChangedEvent());
  }

  @JsMethod
  public String getName(final SituationType situationType) {
    return situationType.name().toLowerCase(Locale.ROOT) + "Situation";
  }

  @Computed
  public List<SituationContext> getValidSituations() {
    return scenarioContext.getSituations().stream()
        .filter(situationContext -> !calculationOptions.getCalculationJobType().isIllegal(situationContext.getType())).collect(Collectors.toList());
  }

  @Computed
  public List<SituationContext> getMultiSelectSituations() {
    return getValidSituations().stream()
        .filter(v -> calculationOptions.getCalculationJobType().getPluralSituationTypes().contains(v.getType()))
        .collect(Collectors.toList());
  }

  @Computed("isInCombination")
  public boolean isInCombination() {
    return calculationOptions.getCalculationJobType() == CalculationJobType.IN_COMBINATION_PROCESS_CONTRIBUTION;
  }

  @Computed
  protected List<NoticeRule> getCalculateErrorsShowing() {
    return getCalculateErrors()
        .filter(v -> v.showing())
        .collect(Collectors.toList());
  }

  @Computed
  protected Stream<NoticeRule> getCalculateErrors() {
    final CalculationSetOptions opts = calculationOptions;
    final SituationComposition comp = composition;
    final ApplicationConfiguration conf = applicationContext.getConfiguration();

    return Stream.of(
        NoticeRule.create(
            () -> CalculateValidations.hasMissingRequiredSituation(comp, opts.getCalculationJobType()),
            () -> i18n.calculateSituationsMissing(CalculateValidations.formatRequiredSituationTypes(comp, opts.getCalculationJobType()))),
        NoticeRule.create(
            () -> CalculateValidations.hasTooFewSituations(comp),
            () -> i18n.calculateSituationNoneSelected()),
        NoticeRule.create(
            () -> CalculateValidations.hasTooManySituationsForType(comp, getCalculationJobType())
                || CalculateValidations.hasTooManySituations(comp, conf),
            () -> i18n.calculateSituationLimit(
                CalculateValidations.getMaxNumberOfSituations(getCalculationJobType(), conf))),
        NoticeRule.create(
            () -> CalculateValidations.hasNoEmissionsInAnyOfComposition(comp),
            () -> i18n.calculateCompositionHasNoEmissions()),
        NoticeRule.create(
            () -> CalculateValidations.hasInvalidCustomPointsOption(opts, scenarioContext),
            () -> i18n.calculateInvalidCustomPointsOption()),
        NoticeRule.create(
            () -> CalculateValidations.isIncompleteInCombinationJob(opts, comp),
            () -> i18n.calculateInCombinationCompositionInvalid()));
  }
}
