/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.manure.base;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.manure.base.ManureStorageBaseValidators.ManureStorageBaseValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.domain.source.manure.ManureStorage;
import nl.overheid.aerius.wui.vue.DebugDirective;

@Component(customizeOptions = ManureStorageBaseValidators.class, directives = {
    ValidateDirective.class,
    DebugDirective.class
}, components = {
    LabeledInputComponent.class,
    ValidationBehaviour.class,
    VerticalCollapse.class
})
public class ManureStorageBaseEmissionComponent extends ErrorWarningValidator implements HasCreated, HasValidators<ManureStorageBaseValidations> {
  @Prop @JsProperty ManureStorage manureStorage;
  @Prop @JsProperty FarmEmissionFactorType emissionFactorType;

  @Data String tonnesV;
  @Data String metersSquaredV;
  @Data String numberOfDaysV;

  @JsProperty(name = "$v") ManureStorageBaseValidations validation;

  @Override
  @Computed
  public ManureStorageBaseValidations getV() {
    return validation;
  }

  @Watch(value = "manureStorage", isImmediate = true)
  public void onManureStorageChange() {
    onChange();
  }

  @Watch(value = "emissionFactorType", isImmediate = true)
  public void onEmissionFactortypeChange() {
    onChange();
  }

  private void onChange() {
    if (emissionFactorType == null || manureStorage == null) {
      return;
    }
    if (FarmEmissionFactorType.expectsTonnes(emissionFactorType)) {
      tonnesV = manureStorage.getTonnes() == null ? "" : String.valueOf(manureStorage.getTonnes());
    } else {
      tonnesV = "";
      manureStorage.setTonnes(null);
    }
    if (FarmEmissionFactorType.expectsMetersSquared(emissionFactorType)) {
      metersSquaredV = manureStorage.getMetersSquared() == null ? "" : String.valueOf(manureStorage.getMetersSquared());
    } else {
      metersSquaredV = "";
      manureStorage.setMetersSquared(null);
    }
    if (FarmEmissionFactorType.expectsNumberOfDays(emissionFactorType)) {
      numberOfDaysV = manureStorage.getNumberOfDays() == null ? "" : String.valueOf(manureStorage.getNumberOfDays());
    } else {
      numberOfDaysV = "";
      manureStorage.setNumberOfDays(null);
    }
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Computed
  protected String getTonnes() {
    return tonnesV;
  }

  @Computed
  protected void setTonnes(final String tonnes) {
    ValidationUtil.setSafeDoubleOptionalValue(v -> manureStorage.setTonnes(v), tonnes);
    this.tonnesV = tonnes;
  }

  @Computed
  protected String getMetersSquared() {
    return metersSquaredV;
  }

  @Computed
  protected void setMetersSquared(final String metersSquared) {
    ValidationUtil.setSafeDoubleOptionalValue(v -> manureStorage.setMetersSquared(v), metersSquared);
    this.metersSquaredV = metersSquared;
  }

  @Computed
  protected String getNumberOfDays() {
    return numberOfDaysV;
  }

  @Computed
  protected void setNumberOfDays(final String numberOfDays) {
    ValidationUtil.setSafeDoubleOptionalValue(v -> manureStorage.setNumberOfDays(v), numberOfDays);
    this.numberOfDaysV = numberOfDays;
  }

  @JsMethod
  protected String tonnesConversionError() {
    return errorDoubleMessage(tonnesV, ManureStorageBaseValidators.MIN_TONNES, ManureStorageBaseValidators.MAX_TONNES);
  }

  @JsMethod
  protected String metersSquaredConversionError() {
    return errorDoubleMessage(metersSquaredV, ManureStorageBaseValidators.MIN_METERS_SQUARED, ManureStorageBaseValidators.MAX_METERS_SQUARED);
  }

  @JsMethod
  protected String numberOfDaysConversionError() {
    return errorIntMessage(numberOfDaysV, ManureStorageBaseValidators.MIN_NUMBER_OF_DAYS, ManureStorageBaseValidators.MAX_NUMBER_OF_DAYS);
  }

  private String errorIntMessage(final String field, final int min, final int max) {
    return field == null || field.isEmpty()
        ? i18n.validationRequired()
        : i18n.ivIntegerRangeBetween(field, min, max);
  }

  private String errorDoubleMessage(final String field, final int min, final int max) {
    return field == null || field.isEmpty()
        ? i18n.validationRequired()
        : i18n.ivDoubleRangeBetween(field, min, max);
  }

  @JsMethod
  protected boolean expectsTonnes() {
    return emissionFactorType != null && FarmEmissionFactorType.expectsTonnes(emissionFactorType);
  }

  @JsMethod
  protected boolean expectsMetersSquared() {
    return emissionFactorType != null && FarmEmissionFactorType.expectsMetersSquared(emissionFactorType);
  }

  @JsMethod
  protected boolean expectsNumberOfDays() {
    return emissionFactorType != null && FarmEmissionFactorType.expectsNumberOfDays(emissionFactorType);
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return isInvalidTonnes()
        || isInvalidMetersSquared()
        || isInvalidNumberOfDays();
  }

  private boolean isInvalidTonnes() {
    return expectsTonnes() && validation.tonnesV.invalid;
  }

  private boolean isInvalidMetersSquared() {
    return expectsMetersSquared() && validation.metersSquaredV.invalid;
  }

  private boolean isInvalidNumberOfDays() {
    return expectsNumberOfDays() && validation.numberOfDaysV.invalid;
  }

}
