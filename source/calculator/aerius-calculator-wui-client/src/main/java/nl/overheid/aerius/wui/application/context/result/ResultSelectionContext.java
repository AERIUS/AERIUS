/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context.result;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.inject.Singleton;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.js.calculation.ResultsGraphRange;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.wui.application.domain.result.CustomCalculationPointResult;

@Singleton
public class ResultSelectionContext {
  private int selectedAssessmentArea = 0;

  private ResultStatisticType habitatStatisticType = ResultStatisticType.MAX_CONTRIBUTION;

  private String highlightHabitatType;
  @JsProperty private final List<String> selectedHabitatTypes = new ArrayList<>();
  private boolean extraAssessmentHabitats;

  @JsProperty private final List<ResultsGraphRange> selectedBounds = new ArrayList<>();

  private CustomCalculationPointResult selectedCustomPointResult = null;

  public int getSelectedAssessmentArea() {
    return selectedAssessmentArea;
  }

  public void setSelectedAssessmentArea(final int selectedAssessmentArea) {
    this.selectedAssessmentArea = selectedAssessmentArea;
  }

  public void reset() {
    selectedAssessmentArea = 0;
    selectedBounds.clear();
    selectedHabitatTypes.clear();
  }

  public List<String> getSelectedHabitatTypes() {
    return selectedHabitatTypes;
  }

  public List<Double> getSelectedLowerBoundValues() {
    return selectedBounds.stream().map(b -> b.getLowerBound()).collect(Collectors.toList());
  }

  public List<ResultsGraphRange> getSelectedBounds() {
    return Collections.unmodifiableList(selectedBounds);
  }

  public void addSelectedBound(final ResultsGraphRange val) {
    selectedBounds.add(val);
  }

  public void removeSelectedBound(final ResultsGraphRange val) {
    selectedBounds.removeIf(b -> Objects.equals(b.getLowerBound(), val.getLowerBound()));
  }

  public void clearSelectedBounds() {
    selectedBounds.clear();
  }

  public ResultStatisticType getHabitatStatisticType() {
    return habitatStatisticType;
  }

  public void setHabitatStatistic(final ResultStatisticType habitatStatisticType) {
    this.habitatStatisticType = habitatStatisticType;
  }

  public boolean isAssessmentAreaSelected() {
    return selectedAssessmentArea != 0;
  }

  public boolean isSelectedHabitatTypeCode(final String value) {
    return selectedHabitatTypes.contains(value);
  }

  public void addSelectedHabitatTypeCode(final String value) {
    selectedHabitatTypes.add(value);
  }

  public void removeSelectedHabitatType(final String value) {
    selectedHabitatTypes.remove(value);
  }

  public boolean hasSelectedHabitatTypes() {
    return !selectedHabitatTypes.isEmpty();
  }

  public boolean setHighlightedHabitatType(final String value) {
    if (highlightHabitatType != null && highlightHabitatType.equals(value)) {
      return false;
    } else {
      highlightHabitatType = value;
      return true;
    }
  }

  public boolean removeHighlightedHabitatType(final String value) {
    if (highlightHabitatType != null && highlightHabitatType.equals(value)) {
      highlightHabitatType = null;
      return true;
    } else {
      return false;
    }
  }

  public String getHighlightHabitatType() {
    return highlightHabitatType;
  }

  public boolean hasHighlightHabitatType() {
    return highlightHabitatType != null;
  }

  public boolean isExtraAssessmentHabitats() {
    return extraAssessmentHabitats;
  }

  public void setExtraAssessmentHabitats(final boolean extraAssessmentHabitats) {
    this.extraAssessmentHabitats = extraAssessmentHabitats;
  }

  public CustomCalculationPointResult getSelectedCustomPointResult() {
    return selectedCustomPointResult;
  }

  public void setSelectedCustomPointResult(final CustomCalculationPointResult selectedCustomPointResult) {
    this.selectedCustomPointResult = selectedCustomPointResult;
  }
}
