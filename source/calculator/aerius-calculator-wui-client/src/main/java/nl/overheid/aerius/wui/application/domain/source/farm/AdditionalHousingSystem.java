/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.farm;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.overheid.aerius.wui.vue.SkipReactivityValidations;

/**
 * Client side implementation of AdditionalHousingSystem props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public abstract class AdditionalHousingSystem {

  private String additionalSystemType;

  public static final @JsOverlay void init(final AdditionalHousingSystem props, final AdditionalSystemType type) {
    props.setAdditionalSystemType(type);
  }

  @SkipReactivityValidations
  public static final @JsOverlay void initTypeDependent(final FarmAnimalHousing props, final AdditionalSystemType type) {
    switch (type) {
      case CUSTOM:
        CustomAdditionalHousingSystem.init(Js.uncheckedCast(props));
        break;
      case STANDARD:
        StandardFarmAnimalHousing.init(Js.uncheckedCast(props));
        break;
      default:
        throw new IllegalStateException("Unknown type: " + type);
    }
  }

  public final @JsOverlay AdditionalSystemType getAdditionalSystemType() {
    return AdditionalSystemType.safeValueOf(additionalSystemType);
  }

  public final @JsOverlay void setAdditionalSystemType(final AdditionalSystemType additionalSystemType) {
    this.additionalSystemType = additionalSystemType.name();
  }
}
