/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.place;

import java.util.function.Supplier;

import nl.aerius.wui.place.ApplicationPlace;
import nl.aerius.wui.place.PlaceTokenizer;
import nl.overheid.aerius.shared.domain.Theme;

/**
 * Base place class for all places related to theme specific pages.
 */
public abstract class MainThemePlace extends ApplicationPlace {

  public static class Tokenizer<T extends MainThemePlace> extends ApplicationPlace.Tokenizer<T> {
    private final Theme theme;

    public Tokenizer(final Supplier<T> supplier, final Theme theme, final String ... postfix) {
      super(supplier, prepend(theme.getKey(), postfix));
      this.theme = theme;
    }

    public Theme getTheme() {
      return theme;
    }
  }

  public MainThemePlace(final PlaceTokenizer<? extends ApplicationPlace> tokenizer) {
    super(tokenizer);
  }

  public abstract <T> T visit(final PlaceVisitor<T> visitor);

  public <P extends MainThemePlace> P copyTo(final P copy) {
    return copy;
  }

  public Theme getTheme() {
    return ((Tokenizer<?>) getTokenizer()).getTheme();
  }
}
