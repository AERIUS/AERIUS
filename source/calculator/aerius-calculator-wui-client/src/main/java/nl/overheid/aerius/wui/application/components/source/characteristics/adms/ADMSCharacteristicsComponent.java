/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.source.characteristics.adms;

import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;
import jsinterop.base.Js;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.BuoyancyType;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.SourceType;
import nl.overheid.aerius.wui.application.components.divider.DividerComponent;
import nl.overheid.aerius.wui.application.components.source.DetailStatComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.domain.source.ADMSCharacteristics;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.geo.util.GeoType;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "adms-characteristics", components = {
    VerticalCollapse.class,
    DetailStatComponent.class,
    DividerComponent.class
})
public class ADMSCharacteristicsComponent extends BasicVueComponent {
  @Prop @JsProperty protected EmissionSourceFeature source;
  @Data protected ADMSCharacteristics characteristics;

  @Inject @Data ScenarioContext context;
  @Inject @Data protected ApplicationContext applicationContext;

  @Watch(value = "source", isImmediate = true)
  public void watchSource() {
    characteristics = source == null ? null : Js.uncheckedCast(source.getCharacteristics());
  }

  @Computed
  public ADMSCharacteristics getChars() {
    return characteristics;
  }

  @Computed
  public SourceType getSourceType() {
    return characteristics.getSourceType();
  }

  @Computed
  public Sector getSector() {
    return applicationContext.getConfiguration().getSectorCategories().getSectorById(source.getSectorId());
  }

  @Computed("isPointSource")
  protected boolean isPointSource() {
    return GeoType.POINT.is(source.getGeometry());
  }

  @Computed("isPolygonSource")
  protected boolean isPolygonSource() {
    return GeoType.POLYGON.is(source.getGeometry());
  }

  @Computed("isLineSource")
  protected boolean isLineSource() {
    return GeoType.LINE_STRING.is(source.getGeometry());
  }

  @Computed("isVolumeSource")
  protected boolean isVolumeSource() {
    return SourceType.VOLUME == characteristics.getSourceType();
  }

  @Computed("isJetSource")
  protected boolean isJetSource() {
    return SourceType.JET == characteristics.getSourceType();
  }

  @Computed
  protected BuoyancyType getBuoyancyType() {
    return characteristics.getBuoyancyType() == null ? null : characteristics.getBuoyancyType();
  }

  @JsMethod
  protected boolean isBuoyancyType(final BuoyancyType type) {
    return !isVolumeSource() && characteristics.getBuoyancyType() == type;
  }

  @Computed("isVerticalVelocity")
  protected boolean isVerticalVelocity() {
    return !isVolumeSource() && characteristics.getEffluxType() != null && characteristics.getEffluxType().isVelocity();
  }

  @Computed("isVolumetricFlowRate")
  protected boolean isVolumetricFlowRate() {
    return !isVolumeSource() && characteristics.getEffluxType() != null && characteristics.getEffluxType().isVolumeFlux();
  }

  @Computed("isMassFlux")
  protected boolean isMassFlux() {
    return !isVolumeSource() && characteristics.getEffluxType() != null && characteristics.getEffluxType().isMassFlux();
  }

  @JsMethod
  public String getBuildingName() {
    if (isCType(source.getCharacteristicsType())) {
      return Optional.ofNullable((ADMSCharacteristics) source.getCharacteristics())
          .map(ADMSCharacteristics::getBuildingGmlId)
          .map(v -> context.getActiveSituation().findBuilding(v).getLabel())
          .orElse(null);
    } else {
      return null;
    }
  }

  private boolean isCType(final CharacteristicsType type) {
    return source.getCharacteristicsType() == type;
  }
}
