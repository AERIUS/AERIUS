/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source;

import javax.inject.Inject;

import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;

import ol.geom.Geometry;
import ol.geom.MultiPoint;
import ol.style.Style;
import ol.style.StyleOptions;

import nl.aerius.geo.domain.LayerInfo;
import nl.aerius.geo.wui.util.OL3GeometryUtil;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.geo.interactions.FeatureDrawStyle;

/**
 * Interactive layer to edit the geometry of a emission source.
 */
public class ModifySourceGeometryLayer extends SourceGeometryLayer {

  @Inject
  public ModifySourceGeometryLayer(@Assisted final LayerInfo info, final EventBus eventBus, @Assisted final int zIndex) {
    super(info, eventBus, zIndex);
  }

  @Override
  protected Style[] createStyle(final EmissionSourceFeature feature, final double resolution) {
    if (feature.getId().equals(getGeometryEditingFeatureId())) {
      return new Style[] { FeatureDrawStyle.EDIT_STYLE, getEditVertexStyle(feature.getGeometry()) };
    }

    if (feature.getId().equals(getEditableFeatureId()) && LineDirectionStyle.isStyleApplicable(feature)) {
      return LineDirectionStyle.renderStyle(feature);
    } else {
      return NO_RENDERING;
    }
  }

  private Style getEditVertexStyle(final Geometry geometry) {
    final StyleOptions styleOptions = new StyleOptions();
    styleOptions.setImage(FeatureDrawStyle.EDIT_POINTER);
    styleOptions.setGeometry(new MultiPoint(OL3GeometryUtil.getCoordinatesOfGeometry(geometry)));

    return new Style(styleOptions);
  }
}
