/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source.road;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.OptionalInt;
import java.util.function.Function;
import java.util.stream.Collectors;

import ol.OLFactory;
import ol.style.Style;

import nl.aerius.geo.domain.legend.ColorLabelsLegend;
import nl.aerius.geo.domain.legend.LegendType;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.legend.ColorRange;
import nl.overheid.aerius.shared.domain.legend.ColorRangeType;
import nl.overheid.aerius.shared.domain.legend.ColorRangesLegend;
import nl.overheid.aerius.wui.application.domain.source.RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.road.StandardVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.VehicleType;
import nl.overheid.aerius.wui.application.i18n.M;

public class RoadMaxSpeedStyle implements RoadStyle {

  private static final int UNKNOWN_MAX_SPEED = 0;
  private static final String UNKNOWN_COLOR = "#707070";

  private static final int DEFAULT_LINE_WIDTH = 6;
  private static final int DEFAULT_ICON_HEIGHT = 10;

  private static final int STRICT_ENFORCEMENT_LINE_WIDTH = 1;
  private static final int STRICT_ENFORCEMENT_ICON_HEIGHT = 2;
  private static final String STRICT_ENFORCEMENT_COLOR = "#000000";

  private final Map<Integer, List<Style>> styleMap;
  private final Map<Integer, List<Style>> styleMapStrict;
  private final List<ColorRange> colorRange;

  public RoadMaxSpeedStyle(final Function<ColorRangeType, List<ColorRange>> colorRangeFunction) {
    colorRange = colorRangeFunction.apply(ColorRangeType.ROAD_MAX_SPEED);
    this.styleMap = colorRange.stream()
        .collect(Collectors.toMap(
            cr -> (int) cr.getLowerValue(),
            cr -> createStyle(cr, false)));
    this.styleMap.put(UNKNOWN_MAX_SPEED, Collections.singletonList(
        OLFactory.createStyle(OLFactory.createStroke(OLFactory.createColor(UNKNOWN_COLOR), DEFAULT_LINE_WIDTH))));

    this.styleMapStrict = colorRange.stream()
        .collect(Collectors.toMap(
            cr -> (int) cr.getLowerValue(),
            cr -> createStyle(cr, true)));
  }

  private static List<Style> createStyle(final ColorRange colorRange, final boolean strict) {
    final List<Style> styles = new ArrayList<>();
    styles.add(OLFactory.createStyle(OLFactory.createStroke(OLFactory.createColor(colorRange.getColor()), DEFAULT_LINE_WIDTH)));
    if (strict) {
      styles.add(OLFactory.createStyle(OLFactory.createStroke(OLFactory.createColor(STRICT_ENFORCEMENT_COLOR), STRICT_ENFORCEMENT_LINE_WIDTH)));
    }
    return styles;
  }

  @Override
  public List<Style> getStyle(final RoadESFeature feature, final String vehicleTypeCode, final double resolution) {
    final OptionalInt maxSpeed = feature.getSubSources().asList().stream()
        .filter(e -> e.getVehicleType() == VehicleType.STANDARD)
        .mapToInt(v -> ((StandardVehicles) v).getMaximumSpeed())
        .max();
    final int lowerValueRange;
    final Map<Integer, List<Style>> correctStyleMap;
    if (maxSpeed.isPresent()) {
      final boolean hasStrict = feature.getSubSources().asList().stream()
          .filter(e -> e.getVehicleType() == VehicleType.STANDARD)
          .filter(v -> ((StandardVehicles) v).getMaximumSpeed() == maxSpeed.getAsInt())
          .map(v -> ((StandardVehicles) v).isStrictEnforcement())
          .anyMatch(t -> t);
      correctStyleMap = hasStrict ? styleMapStrict : styleMap;
      // Correct range is the one with highest lower bound that is equal or below the max speed.
      lowerValueRange = correctStyleMap.keySet().stream()
          .mapToInt(x -> x)
          .filter(x -> x <= maxSpeed.getAsInt())
          .max().orElse(UNKNOWN_MAX_SPEED);
    } else {
      correctStyleMap = styleMap;
      lowerValueRange = UNKNOWN_MAX_SPEED;
    }
    return correctStyleMap.get(lowerValueRange);
  }

  @Override
  public ColorLabelsLegend getLegend(final ApplicationConfiguration appThemeConfiguration) {
    if (appThemeConfiguration.getTheme() == Theme.NCA) {
      return new ColorRangesLegend(
          colorRange,
          String::valueOf,
          M.messages().colorRangesLegendBetweenText(),
          LegendType.LINE,
          M.messages().unitSingularKmPerH());
    } else {
      final List<String> labels = colorRange.stream()
          .map(ColorRange::getLowerValue)
          .map(speed -> M.messages().esRoadCategory(speed.intValue()))
          .collect(Collectors.toList());
      final List<String> colors = colorRange.stream()
          .map(ColorRange::getColor)
          .collect(Collectors.toList());
      final List<Integer> iconSizes = colorRange.stream()
          .map(x -> DEFAULT_ICON_HEIGHT)
          .collect(Collectors.toList());

      // Add unknown
      labels.add(M.messages().esRoadCategoryUnknown());
      colors.add(UNKNOWN_COLOR);
      iconSizes.add(DEFAULT_ICON_HEIGHT);

      // Add strict enforcement
      labels.add(M.messages().esRoadStrictEnforcement());
      colors.add(STRICT_ENFORCEMENT_COLOR);
      iconSizes.add(STRICT_ENFORCEMENT_ICON_HEIGHT);

      return new ColorLabelsLegend(
          labels.toArray(new String[labels.size()]),
          colors.toArray(new String[colors.size()]),
          LegendType.LINE,
          iconSizes.toArray(new Integer[iconSizes.size()]),
          null);
    }
  }

}
