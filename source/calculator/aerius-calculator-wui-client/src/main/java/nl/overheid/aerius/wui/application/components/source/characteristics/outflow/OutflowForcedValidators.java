/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.characteristics.outflow;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.aerius.vuelidate.ValidationOptions;
import nl.aerius.vuelidate.Validations;
import nl.aerius.vuelidate.ValidatorBuilder;
import nl.overheid.aerius.shared.domain.ops.OPSLimits;

/**
 * Validators class for {@link OutflowForcedEditorComponent}.
 */
class OutflowForcedValidators extends ValidationOptions<OutflowForcedEditorComponent> {

  @JsType(isNative = true, namespace = JsPackage.GLOBAL)
  static class OutflowForcedValidations extends Validations {
    public @JsProperty Validations emissionTemperatureV;
    public @JsProperty Validations emissionTemperatureWithBuildingV;
    public @JsProperty Validations outflowDiameterV;
    public @JsProperty Validations outflowDiameterWithBuildingV;
    public @JsProperty Validations outflowVelocityV;
    public @JsProperty Validations outflowVelocityWithBuildingV;
  }

  @Override
  protected void constructValidators(final Object options, final ValidatorInstaller v) {
    final OutflowForcedEditorComponent instance = Js.uncheckedCast(options);

    v.install("emissionTemperatureV", () -> instance.emissionTemperatureV = null, ValidatorBuilder.create()
        .required()
        .decimal()
        .minValue(0)
        .maxValue(OPSLimits.SOURCE_EMISSION_TEMPERATURE_MAXIMUM));
    v.install("emissionTemperatureWithBuildingV", () -> instance.emissionTemperatureWithBuildingV = null, ValidatorBuilder.create()
        .maxValue(OPSLimits.SOURCE_EMISSION_TEMPERATURE_MAXIMUM));

    v.install("outflowDiameterV", () -> instance.outflowDiameterV = null, ValidatorBuilder.create()
        .required()
        .decimal()
        .minValue(OPSLimits.SCOPE_NO_BUILDING_OUTFLOW_DIAMETER_MINIMUM)
        .maxValue(OPSLimits.SCOPE_NO_BUILDING_OUTFLOW_DIAMETER_MAXIMUM));
    v.install("outflowDiameterWithBuildingV", () -> instance.outflowDiameterWithBuildingV = null, ValidatorBuilder.create()
        .maxValue(OPSLimits.SCOPE_BUILDING_OUTFLOW_DIAMETER_MAXIMUM));

    v.install("outflowVelocityV", () -> instance.outflowVelocityV = null, ValidatorBuilder.create()
        .required()
        .decimal()
        .minValue(OPSLimits.SCOPE_NO_BUILDING_OUTFLOW_VELOCITY_MINIMUM)
        .maxValue(OPSLimits.SCOPE_NO_BUILDING_OUTFLOW_VELOCITY_MAXIMUM));
    v.install("outflowVelocityWithBuildingV", () -> instance.outflowVelocityWithBuildingV = null, ValidatorBuilder.create()
        .maxValue(OPSLimits.SCOPE_BUILDING_OUTFLOW_VELOCITY_MAXIMUM));
  }
}
