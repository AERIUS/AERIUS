/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;
import com.google.web.bindery.event.shared.binder.GenericEvent;

import ol.OLFactory;
import ol.layer.Image;
import ol.layer.Layer;
import ol.source.ImageWms;
import ol.source.ImageWmsOptions;
import ol.source.ImageWmsParams;

import nl.aerius.geo.domain.IsLayer;
import nl.aerius.geo.domain.LayerInfo;
import nl.aerius.geo.domain.LayerOptions;
import nl.aerius.geo.domain.legend.ColorLabelsLegend;
import nl.aerius.geo.domain.legend.LegendType;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.legend.ColorRange;
import nl.overheid.aerius.shared.domain.legend.ColorRangeType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.application.command.result.ChangeSituationResultKeyCommand;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.event.situation.SituationChangedYearEvent;
import nl.overheid.aerius.wui.application.event.situation.SituationSwitchEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.ui.pages.results.GeneralizedEmissionResultType;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;

/**
 * Provides the habitat areas sensitivity layer with a selection list
 */
public class HabitatAreasSensitivityLevelLayer implements IsLayer<Layer> {

  private static final HabitatAreasSensitivityLevelLayerEventBinder EVENT_BINDER = GWT.create(HabitatAreasSensitivityLevelLayerEventBinder.class);

  interface HabitatAreasSensitivityLevelLayerEventBinder extends EventBinder<HabitatAreasSensitivityLevelLayer> {}

  private static final String WMS_HABITAT_AREAS_SENSITIVITY_LEVEL_VIEW = "calculator:wms_habitat_areas_sensitivity_level_view";

  private final Image layer;
  private final ImageWmsParams imageWmsParams;
  private final ImageWmsOptions imageWmsOptions;
  private ImageWms imageWms;
  private final ApplicationConfiguration themeConfiguration;

  private EmissionResultKey emissionResultKey = EmissionResultKey.NOXNH3_DEPOSITION;
  private ColorRangeType colorRangeType = ColorRangeType.HABITAT_AREAS_SENSITIVITY_LEVEL_DEPOSITION;

  private final LayerInfo info;
  private LayerOptions erkOptions;

  @Inject ScenarioContext scenarioContext;

  @Inject
  public HabitatAreasSensitivityLevelLayer(final EventBus eventBus, @Assisted final ApplicationConfiguration themeConfiguration,
      final EnvironmentConfiguration configuration, @Assisted final int zIndex) {
    imageWmsParams = OLFactory.createOptions();
    this.themeConfiguration = themeConfiguration;

    this.info = new LayerInfo();
    this.info.setName(this.getClass().getSimpleName());
    this.info.setTitle(M.messages().layerHabitatAreasSensitivityLevel());
    this.info.setOptions(constructLayerOptions());

    updateLegend();

    imageWmsOptions = OLFactory.createOptions();
    imageWmsOptions.setUrl(configuration.getGeoserverBaseUrl());
    imageWmsOptions.setParams(imageWmsParams);

    layer = new Image();
    layer.setZIndex(zIndex);
    layer.setVisible(false);
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @EventHandler(handles = {SituationChangedYearEvent.class, SituationSwitchEvent.class})
  public void onUpdateLayer(final GenericEvent e) {
    updateLayer();
  }

  @EventHandler
  public void onChangeSituationResultKeyCommand(final ChangeSituationResultKeyCommand c) {
    erkOptions.select(c.getValue().getEmissionResultKey());
  }

  private LayerOptions[] constructLayerOptions() {
    final List<LayerOptions> options = new ArrayList<>();
    final List<EmissionResultKey> emissionResultKeys = themeConfiguration.getEmissionResultKeys();

    erkOptions = LayerOptions.createFromEnum(emissionResultKeys.toArray(new EmissionResultKey[0]),
        ls -> M.messages().calculateEmissionResultKey(ls),
        v -> this.setEmissionResultKey(v), emissionResultKey);
    options.add(erkOptions);

    return options.toArray(new LayerOptions[0]);
  }

  private void setEmissionResultKey(final EmissionResultKey emissionResultKey) {
    this.emissionResultKey = emissionResultKey;
    updateColorRangeType();
    updateLayer();
    asLayer().getSource().changed();
  }

  private void updateColorRangeType() {
    switch (emissionResultKey) {
    case NOX_CONCENTRATION:
      colorRangeType = ColorRangeType.HABITAT_AREAS_SENSITIVITY_LEVEL_CONCENTRATION_NOX;
      break;
    case NH3_CONCENTRATION:
      colorRangeType = ColorRangeType.HABITAT_AREAS_SENSITIVITY_LEVEL_CONCENTRATION_NH3;
      break;
    default:
      colorRangeType = ColorRangeType.HABITAT_AREAS_SENSITIVITY_LEVEL_DEPOSITION;
      break;
    }
  }

  private String buildViewParams() {
    String viewParams = "";
    viewParams += "color_range_type:" + this.colorRangeType.name().toLowerCase(Locale.ROOT);
    viewParams += ";emission_result_type:" + emissionResultKey.getEmissionResultType().name().toLowerCase(Locale.ROOT);
    viewParams += ";substance_id:" + emissionResultKey.getSubstance().getId();
    return viewParams;
  }

  private void updateLayer() {
    imageWmsParams.setViewParams(buildViewParams());
    imageWmsParams.setLayers(WMS_HABITAT_AREAS_SENSITIVITY_LEVEL_VIEW);

    if (imageWms == null) {
      imageWms = new ImageWms(imageWmsOptions);
      layer.setSource(imageWms);
    }

    imageWms.refresh();

    updateLegend();
  }

  private void updateLegend() {
    final List<ColorRange> colorRange = themeConfiguration.getColorRange(colorRangeType);
    final String[] labels = colorRange.stream()
        .map(ColorRange::getLowerValue)
        .map(itemValue -> M.messages().decimalNumberCapped(itemValue, 0))
        .toArray(String[]::new);
    final String[] colors = colorRange.stream()
        .map(ColorRange::getColor)
        .toArray(String[]::new);
    final GeneralizedEmissionResultType resultType = GeneralizedEmissionResultType.fromEmissionResultType(emissionResultKey.getEmissionResultType());
    final String description = M.messages().layerHabitatAreasSensitivityLevelDescription(resultType,
        M.messages().emissionResultUnit(resultType, themeConfiguration.getEmissionResultValueDisplaySettings().getDisplayType()));
    this.info.setLegend(new ColorLabelsLegend(labels, colors, LegendType.CIRCLE, description));
  }

  @Override
  public Layer asLayer() {
    return layer;
  }

  @Override
  public LayerInfo getInfo() {
    return info;
  }

}
