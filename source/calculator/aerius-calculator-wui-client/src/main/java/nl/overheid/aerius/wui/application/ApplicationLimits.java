/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application;

/**
 * Some sanity limits, when these limits are applied functionality will
 * decrease, however the application will still continue to perform and not
 * freeze into perpetuity.
 *
 * Limits should only be used to trade-off non-critical features (for example:
 * source editing won't work well anymore, however a user can still calculate
 * and export).
 *
 * DEVELOPER NOTES: Limits at the time of developing this (5000-ish) seem quite
 * high (about 10 seconds to load 50k sources) and will likely have to be
 * fine-tuned later.
 */
public class ApplicationLimits {
  /**
   * Draw limit for the number of sources on the map
   */
  public static final int SOURCE_LAYER_DRAW_LIMIT = 100_000;

  /**
   * Draw limit for the number of sources in the emission source list
   */
  public static final int SCENARIO_INPUT_LIST_DRAW_LIMIT = 5_000;

  /**
   * Draw limit for number of sources in PDF source list, and emission source details lists.
   */
  public static final int PRINT_EMISSION_SOURCE_LIMIT = 250;

  /**
   * Draw limit for number of road subsource sources in PDF source list, and emission source details lists.
   */
  public static final int PRINT_SRM2_ROAD_EMISSION_SOURCE_LIMIT = 10;

  /**
   * The maximum number of unfinished calculations the UI will allow to execute.
   */
  public static final int ACTIVE_CALCULATION_LIMIT = 3;
}
