/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.util;

import java.util.List;

import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfileType;
import nl.overheid.aerius.wui.application.domain.source.ADMSCharacteristics;

/**
 * Util class for Monthly time-varying profile value handling.
 */
public class MonthlyProfileUtil extends TimeVaryingProfileUtilBase {

  protected MonthlyProfileUtil() {
    super(CustomTimeVaryingProfileType.MONTHLY);
  }

  public String getValue(final List<Double> values, final int month) {
    return checkAndwarnInvalidSize(values) ? formattedPreciseValue(values.get(month)) : "";
  }

  public void setInput(final List<Double> values, final int monthInt, final double value) {
    if (checkAndwarnInvalidSize(values)) {
      setInputAtIndex(values, monthInt, value);
    }
  }

  @Override
  public String getCustomTimeVaryingProfileId(final ADMSCharacteristics chars) {
    return chars.getCustomMonthlyTimeVaryingProfileId();
  }

  @Override
  public void setCustomTimeVaryingProfileId(final ADMSCharacteristics chars, final String id) {
    chars.setCustomMonthlyTimeVaryingProfileId(id);
  }

  @Override
  public String getStandardTimeVaryingProfileCode(final ADMSCharacteristics chars) {
    return chars.getStandardMonthlyTimeVaryingProfileCode();
  }

  @Override
  public void setStandardTimeVaryingProfileCode(final ADMSCharacteristics chars, final String code) {
    chars.setStandardMonthlyTimeVaryingProfileCode(code);
  }

  @Override
  protected double getValuesTotalRaw(final List<Double> values) {
    return values.stream().mapToDouble(v -> v).sum();
  }

  @Override
  protected double getValuesAverageRaw(final List<Double> values) {
    return getValuesTotalRaw(values) / values.size();
  }

}
