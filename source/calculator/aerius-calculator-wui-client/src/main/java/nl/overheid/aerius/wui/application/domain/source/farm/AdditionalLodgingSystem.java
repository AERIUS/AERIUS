/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.farm;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

/**
 * Client side implementation of AdditionalLodgingSystem props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class AdditionalLodgingSystem extends LodgingSystem {

  private int numberOfAnimals;

  public static final @JsOverlay AdditionalLodgingSystem create() {
    final AdditionalLodgingSystem props = new AdditionalLodgingSystem();
    init(props, LodgingSystemType.ADDITIONAL);
    props.setNumberOfAnimals(0);
    return props;
  }

  public final @JsOverlay int getNumberOfAnimals() {
    return numberOfAnimals;
  }

  public final @JsOverlay void setNumberOfAnimals(final int numberOfAnimals) {
    this.numberOfAnimals = numberOfAnimals;
  }
}
