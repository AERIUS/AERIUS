/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.print.own2000.source.shipping;

import com.axellience.vuegwt.core.annotations.component.Component;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.components.source.shipping.inland.detail.InlandShippingDetailComponent;

@Component
public class InlandShippingDetailPrintComponent extends InlandShippingDetailComponent {

  @JsMethod
  public boolean hasSubSources() {
    if (source.getEmissionSourceType() == EmissionSourceType.SHIPPING_INLAND) {
      return getInlandSource().getSubSources().length > 0;
    }
    if (source.getEmissionSourceType() == EmissionSourceType.SHIPPING_INLAND_DOCKED) {
      return getMooringInlandSource().getSubSources().length > 0;
    }

    return false;
  }
}
