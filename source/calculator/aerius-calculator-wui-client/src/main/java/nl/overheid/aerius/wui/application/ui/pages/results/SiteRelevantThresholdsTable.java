/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.results;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.application.command.geo.ClearAssessmentAreaCommand;
import nl.overheid.aerius.wui.application.command.geo.SelectAssessmentAreaCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.button.IconComponent;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.result.ResultSelectionContext;
import nl.overheid.aerius.wui.application.domain.summary.AssessmentAreaThresholdResults;
import nl.overheid.aerius.wui.application.geo.util.GeoUtil;
import nl.overheid.aerius.wui.application.util.JsObjectsUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    CollapsiblePanel.class,
    ButtonIcon.class,
    IconComponent.class,
    DecisionThresholdsTable.class,
})
public class SiteRelevantThresholdsTable extends BasicVueComponent {
  @Prop EventBus eventBus;

  @Prop AssessmentAreaThresholdResults areaThresholdResults;

  @Data @Inject ResultSelectionContext selectionContext;
  @Data @Inject ApplicationContext applicationContext;

  @JsMethod
  public void openPanel() {
    final int id = areaThresholdResults.getAssessmentArea().getId();
    eventBus.fireEvent(new SelectAssessmentAreaCommand(id));
  }

  @JsMethod
  public void closePanel() {
    eventBus.fireEvent(new ClearAssessmentAreaCommand());
  }

  @JsMethod
  public boolean isOpen() {
    return areaThresholdResults.getAssessmentArea().getId() == selectionContext.getSelectedAssessmentArea();
  }

  @JsMethod
  public String getAreaTitle() {
    return areaThresholdResults.getAssessmentArea()
        .getTitle(applicationContext.isAppFlagEnabled(AppFlag.SHOW_ASSESSMENT_AREA_DIRECTIVES));
  }

  @JsMethod
  public void zoomToArea() {
    eventBus.fireEvent(GeoUtil.createMapSetExtendCommand(areaThresholdResults.getAssessmentArea().getBounds()));
  }

  @Computed("aboveSiteRelevantThreshold")
  public boolean isAboveSiteRelevantThreshold() {
    boolean aboveThreshold = false;
    if (areaThresholdResults != null) {
      for (final EmissionResultKey key : applicationContext.getConfiguration().getEmissionResultKeys()) {
        if (JsObjectsUtil.hasValueForKey(areaThresholdResults.getResults(), key)
            && JsObjectsUtil.getValueForKey(areaThresholdResults.getResults(), key).getFraction() > 1.0) {
          aboveThreshold = true;
        }
      }
    }
    return aboveThreshold;
  }

}
