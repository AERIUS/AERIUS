/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.google.inject.assistedinject.Assisted;

import ol.OLFactory;
import ol.layer.Image;
import ol.layer.Layer;
import ol.source.ImageWms;
import ol.source.ImageWmsOptions;
import ol.source.ImageWmsParams;

import nl.aerius.geo.domain.IsLayer;
import nl.aerius.geo.domain.LayerInfo;
import nl.aerius.geo.domain.LayerOptions;
import nl.aerius.geo.domain.legend.ColorLabelsLegend;
import nl.aerius.geo.domain.legend.LegendType;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.legend.ColorItem;
import nl.overheid.aerius.shared.domain.legend.ColorItemType;
import nl.overheid.aerius.shared.domain.sector.category.SimpleCategory;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;

/**
 * Provides the nature areas layer
 */
public class NatureAreasLayer implements IsLayer<Layer> {

  private static final String WMS_NATURE_AREAS_VIEW = "calculator:wms_nature_areas_view";
  private static final double DEFAULT_OPACITY = 0.8;
  private static final String SHOW_ALL = "";
  private static final String LEGEND_ICON_OUTLINE_COLOR = "#808080";

  private final Image layer;
  private final ImageWmsParams imageWmsParams;
  private final ImageWmsOptions imageWmsOptions;
  private ImageWms imageWms;
  private final ApplicationConfiguration config;
  private final List<SimpleCategory> categories;

  private String selectedDirective = SHOW_ALL;

  private final LayerInfo info;

  @Inject ScenarioContext scenarioContext;

  @Inject
  public NatureAreasLayer(@Assisted final ApplicationConfiguration config, final EnvironmentConfiguration configuration) {
    imageWmsParams = OLFactory.createOptions();
    this.config = config;
    this.categories = config.getNatureAreaDirectives();

    this.info = new LayerInfo();
    this.info.setName(this.getClass().getCanonicalName());
    this.info.setTitle(M.messages().layerNatureAreas());
    this.info.setOptions(constructLayerOptions());

    updateLegend();

    imageWmsOptions = OLFactory.createOptions();
    imageWmsOptions.setUrl(configuration.getGeoserverBaseUrl());
    imageWmsOptions.setParams(imageWmsParams);

    layer = new Image();
    layer.setVisible(true);
    layer.setOpacity(DEFAULT_OPACITY);

    updateLayer();
  }

  private LayerOptions[] constructLayerOptions() {
    final List<LayerOptions> options = new ArrayList<>();
    if (config.getTheme() == Theme.NCA) {
      final List<Object> selectionOptions = new ArrayList<>();
      selectionOptions.add(SHOW_ALL);
      categories.stream()
          .map(SimpleCategory::getCode)
          .forEachOrdered(selectionOptions::add);
      final LayerOptions layerOptions = new LayerOptions(selectionOptions,
          ls -> ls == null || SHOW_ALL.equals(ls)
              ? M.messages().layerShowAll()
              : categories.stream().filter(x -> ls.equals(x.getCode()))
                  .map(SimpleCategory::getName)
                  .findFirst().orElse("Mismatch"),
          v -> this.setDirective((String) v), selectedDirective);
      options.add(layerOptions);
    } else {
      // Don't show the dropdown, and ensure selected directive is set to show everything.
      selectedDirective = SHOW_ALL;
    }

    return options.toArray(new LayerOptions[0]);
  }

  private void setDirective(final String directive) {
    this.selectedDirective = directive;
    updateLayer();
    asLayer().getSource().changed();
  }

  private String buildViewParams() {
    String viewParams = "";
    if (selectedDirective != null && !SHOW_ALL.equals(selectedDirective)) {
      viewParams += "directive_code:" + selectedDirective;
    }
    return viewParams;
  }

  private void updateLayer() {
    imageWmsParams.setViewParams(buildViewParams());
    imageWmsParams.setLayers(WMS_NATURE_AREAS_VIEW);

    if (imageWms == null) {
      imageWms = new ImageWms(imageWmsOptions);
      layer.setSource(imageWms);
    }

    imageWms.refresh();
  }

  private void updateLegend() {
    final List<ColorItem> colorItems = config.getColorItems(ColorItemType.NATURA2000_DIRECTIVE);
    final String[] labels = colorItems.stream()
        .map(ColorItem::getItemValue)
        .map(itemValue -> categories.stream().filter(x -> itemValue.equals(x.getCode()))
            .map(SimpleCategory::getName)
            .findFirst().orElse("Unknown"))
        .toArray(String[]::new);
    final String[] colors = colorItems.stream()
        .map(ColorItem::getColor)
        .toArray(String[]::new);
    this.info.setLegend(new ColorLabelsLegend(labels, colors, LEGEND_ICON_OUTLINE_COLOR, LegendType.CIRCLE));
  }

  @Override
  public Layer asLayer() {
    return layer;
  }

  @Override
  public LayerInfo getInfo() {
    return info;
  }

}
