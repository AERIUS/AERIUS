/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import javax.inject.Singleton;

import com.axellience.vuegwt.core.client.Vue;
import com.axellience.vuegwt.core.client.tools.VueGWTTools;

@Singleton
public class GenericValidationContext {
  private boolean displayFormProblems = false;

  public boolean isDisplayFormProblems() {
    return displayFormProblems;
  }

  public void setDisplayFormProblems(final boolean displayFormProblems) {
    this.displayFormProblems = displayFormProblems;
  }

  /**
   * Perform a hard reset
   */
  public void reset() {
    if (displayFormProblems) {
      // Reactivity will automatically be triggered because the value changes
      displayFormProblems = false;
    } else {
      // Find and manually trigger the reactivity of the field name
      final String fieldName = VueGWTTools.getFieldName(this, () -> displayFormProblems = true);
      Vue.set(this, fieldName, false);
    }
  }
}
