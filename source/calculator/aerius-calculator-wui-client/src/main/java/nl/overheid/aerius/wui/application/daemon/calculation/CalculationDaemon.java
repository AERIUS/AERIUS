/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.calculation;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import javax.inject.Singleton;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.future.AppAsyncCallback;
import nl.aerius.wui.util.NotificationUtil;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.util.ApplicationSectionsUtil;
import nl.overheid.aerius.wui.application.ApplicationLimits;
import nl.overheid.aerius.wui.application.command.calculation.CalculationCancelCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationDeleteCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationDeleteJobCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobImportResultsCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobSelectCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobStartCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationRemoveCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationResetCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationStartCommand;
import nl.overheid.aerius.wui.application.command.calculation.RestoreCalculationJobContextCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.PrintContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationPreparationContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.context.result.ResultSummaryContext;
import nl.overheid.aerius.wui.application.domain.DevelopmentPressureUtil;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationInfo;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.application.domain.calculation.JobProgress;
import nl.overheid.aerius.wui.application.domain.calculation.NCACalculationOptions;
import nl.overheid.aerius.wui.application.domain.export.ExportOptions;
import nl.overheid.aerius.wui.application.domain.importer.SituationWithFileId;
import nl.overheid.aerius.wui.application.event.CalculationCancelEvent;
import nl.overheid.aerius.wui.application.event.CalculationCompleteEvent;
import nl.overheid.aerius.wui.application.event.CalculationDeleteEvent;
import nl.overheid.aerius.wui.application.event.CalculationStartEvent;
import nl.overheid.aerius.wui.application.event.CalculationStatusEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.service.AeriusRequestCallback;
import nl.overheid.aerius.wui.application.service.CalculationServiceAsync;
import nl.overheid.aerius.wui.application.ui.pages.calculate.CalculateValidations;
import nl.overheid.aerius.wui.config.ApplicationFlags;

@Singleton
public class CalculationDaemon extends BasicEventComponent implements Daemon {
  private static final CalculationDaemonEventBinder EVENT_BINDER = GWT.create(CalculationDaemonEventBinder.class);

  interface CalculationDaemonEventBinder extends EventBinder<CalculationDaemon> {}

  private @Inject CalculationPreparationContext prepContext;
  private @Inject CalculationStatusPoller calculationStatusPoller;
  private @Inject CalculationServiceAsync calculationService;
  private @Inject ScenarioContext scenarioContext;
  private @Inject CalculationContext calculationContext;
  private @Inject ApplicationContext applicationContext;
  private @Inject PrintContext printOwN2000Context;
  private @Inject ApplicationFlags flags;

  @EventHandler
  public void onCalculationJobStartCommand(final CalculationJobStartCommand c) {
    final CalculationJobContext jobContext = c.getValue();
    final List<String> problems = findCalculationStartProblems(jobContext);
    if (!problems.isEmpty()) {
      // Broadcast an error for each problem
      problems.stream()
          // .limit(1) // Limit to one notification (?)
          .forEach(problem -> NotificationUtil.broadcastError(eventBus, problem));
      return;
    }

    // Silently cancel and delete the existing calculation, if any
    Optional.ofNullable(prepContext.getActiveCalculation(jobContext)).ifPresent(v -> eventBus.fireEvent(new CalculationCancelCommand(v)));

    // Store this calculation as the latest calculation for the associated job
    final CalculationExecutionContext calculation = calculationFromScenarioContext(jobContext, v -> {
    });
    prepContext.setLatestCalculation(jobContext, calculation);

    // Start the calculation if there are no problems
    onCalculationStartCommand(new CalculationStartCommand(calculation, jobContext));

    eventBus.fireEvent(new CalculationJobSelectCommand(jobContext));
  }

  @EventHandler
  public void onCalculationStartCommand(final CalculationStartCommand c) {
    final CalculationExecutionContext calculation = c.getValue();
    final CalculationJobContext jobContext = c.getJobContext();

    // Assemble export options for UI calculations
    final ExportOptions exportOptions = new ExportOptions();
    exportOptions.setJobType(JobType.CALCULATION);
    exportOptions.setExportType(ExportType.CALCULATION_UI);
    exportOptions.setDemoMode(flags.isDemoMode());
    exportOptions.setName(jobContext.getName());

    final SituationComposition situationComposition = jobContext.getSituationComposition();
    final CalculationSetOptions calculationOptions = jobContext.getCalculationOptions();

    c.silence();
    calculationService.startCalculation(situationComposition, calculationOptions, exportOptions, scenarioContext, null,
        applicationContext.getTheme(), null, AppAsyncCallback.create(jobKey -> {
          calculation.setJobKey(jobKey);
          calculationContext.initializeCalculation(calculation);

          eventBus.fireEvent(c.createEvent(c.getValue()));
          calculationStatusPoller.start(calculation);
        }, e -> handleCalculationStartError(jobContext, calculation, e)));
  }

  private void handleCalculationStartError(final CalculationJobContext job, final CalculationExecutionContext calculation, final Throwable e) {
    final String message = e.getMessage();
    final JobProgress jobProgress = new JobProgress();
    jobProgress.setState(JobState.ERROR);
    jobProgress.setErrorMessage(message);
    calculation.getCalculationInfo().setJobProgress(jobProgress);
    NotificationUtil.broadcastError(eventBus, M.messages().notificationCalculationStartError(job.getName(), message));
  }

  @EventHandler
  void onCalculationJobImportResultsCommand(final CalculationJobImportResultsCommand c) {
    final CalculationJobContext jobContext = c.getValue();
    // Store this calculation as the latest calculation for the associated job
    final CalculationExecutionContext calculation = calculationFromScenarioContext(jobContext, v -> {
    });
    prepContext.setLatestCalculation(jobContext, calculation);

    // Start the calculation if there are no problems
    startImportResults(calculation, c.getSituationWithFileIds(), c.getArchiveContributionFileId(), jobContext);

    eventBus.fireEvent(new CalculationJobSelectCommand(jobContext));
  }

  private void startImportResults(final CalculationExecutionContext calculation, final List<SituationWithFileId> situationWithFileIds,
      final String archiveContributionFileId, final CalculationJobContext jobContext) {
    final SituationComposition situationComposition = jobContext.getSituationComposition();

    calculationService.importResults(scenarioContext, situationComposition, situationWithFileIds, archiveContributionFileId,
        applicationContext.getTheme(),
        AppAsyncCallback.create(jobKey -> {
          calculation.setJobKey(jobKey);
          calculationContext.initializeCalculation(calculation);

          eventBus.fireEvent(new CalculationStartEvent(calculation));
          calculationStatusPoller.start(calculation);
        }, e -> handleCalculationStartError(jobContext, calculation, e)));
  }

  @EventHandler
  void onRestoreCalculationContextCommand(final RestoreCalculationJobContextCommand c) {
    // Silence because this event is being called in the callback
    c.silence();

    final CalculationJobContext jobContext = c.getValue();
    final CalculationExecutionContext calculation = calculationFromScenarioContext(jobContext, execution -> execution.setJobKey(c.getJobKey()));

    prepContext.setLatestCalculation(jobContext, calculation);

    // Set these values specifically for rendering map markers in Project
    // Calculation PDF.
    final ResultSummaryContext resultContext = calculation.getResultContext();
    resultContext.setResultType(printOwN2000Context.getScenarioResultType());
    resultContext.setHexagonType(printOwN2000Context.getSummaryHexagonType());
    resultContext.setOverlappingHexagonType(printOwN2000Context.getOverlappingHexagonType());

    calculationContext.setActiveCalculation(calculation);
    calculationService.getCalculationStatus(c.getJobKey(), AeriusRequestCallback.create(v -> {
      // Broadcast status events to set calculation status context.
      eventBus.fireEvent(new CalculationStatusEvent(c.getJobKey(), v));
      eventBus.fireEvent(c.getEvent());
    }));
  }

  private CalculationExecutionContext calculationFromScenarioContext(final CalculationJobContext jobContext,
      final Consumer<CalculationExecutionContext> calculationBuilder) {
    return createAndAddCalculation(execution -> {
      final String executionInputHash = ScenarioContext.calculateExecutionInputHashFromJob(scenarioContext, jobContext);

      execution.setExecutionInputHash(executionInputHash);

      final List<SituationContext> situations = ScenarioContext.filterSituations(scenarioContext, jobContext.getSituationComposition());
      execution.setSituations(situations.stream()
          .map(SituationContext::handleFromSituation)
          .collect(Collectors.toList()));

      // Store a copy, because we don't want changes to this object by other jobs to affect it
      execution.setJobContext(jobContext.copy());

      calculationBuilder.accept(execution);

      if (!situations.isEmpty()) {
        ResultSummarySelectorUtil.setContextWithSituationResult(execution);
      }
    });
  }

  /**
   * Create a new calculation and add to the context
   *
   * @param calculationBuilder function to change situation properties before the situation is added
   */
  public CalculationExecutionContext createAndAddCalculation(final Consumer<CalculationExecutionContext> calculationBuilder) {
    final CalculationExecutionContext calculationJobContext = new CalculationExecutionContext();

    // Set default values
    calculationBuilder.accept(calculationJobContext);

    calculationContext.addCalculation(calculationJobContext);

    setAvailableSummaryHexagonTypes(calculationJobContext, scenarioContext, applicationContext);
    return calculationJobContext;
  }

  private static void setAvailableSummaryHexagonTypes(final CalculationExecutionContext calculationJobContext,
      final ScenarioContext scenarioContext, final ApplicationContext applicationContext) {
    final ResultSummaryContext resultContext = calculationJobContext.getResultContext();
    final CalculationMethod calculationMethod = calculationJobContext.getJobContext().getCalculationOptions().getCalculationMethod();
    final List<SummaryHexagonType> supportedHexagonTypes = applicationContext.getConfiguration().getSupportedSummaryHexagonTypes();

    if (calculationMethod == CalculationMethod.FORMAL_ASSESSMENT || calculationMethod == CalculationMethod.NATURE_AREA
        || calculationMethod == CalculationMethod.QUICK_RUN) {
      resultContext.addSummaryHexagonTypeIfSupported(SummaryHexagonType.EXCEEDING_HEXAGONS, supportedHexagonTypes);
      resultContext.addSummaryHexagonTypeIfSupported(SummaryHexagonType.RELEVANT_HEXAGONS, supportedHexagonTypes);
      resultContext.addSummaryHexagonTypeIfSupported(SummaryHexagonType.EXTRA_ASSESSMENT_HEXAGONS, supportedHexagonTypes);
    }
    if (!scenarioContext.getCalculationPoints().isEmpty()
        && ApplicationSectionsUtil.isCustomPointsUsed(calculationJobContext.getJobContext().getCalculationOptions().getCalculationJobType())) {
      resultContext.addSummaryHexagonTypeIfSupported(SummaryHexagonType.CUSTOM_CALCULATION_POINTS, supportedHexagonTypes);
    }
  }

  @EventHandler
  public void onCalculationStatusEvent(final CalculationStatusEvent e) {
    final String jobKey = e.getJobKey();

    final CalculationExecutionContext calculation = calculationContext.getInitializedCalculation(jobKey);
    if (calculation == null) {
      GWTProd.warn("Can't find job for key: " + jobKey);
      return;
    }
    calculation.resetLastUpdate();
    final CalculationInfo calculationInfo = e.getValue();

    calculation.setCalculationInfo(calculationInfo);

    switch (calculationInfo.getJobProgress().getState()) {
    case UNDEFINED:
    case CREATED:
    case QUEUING:
    case PREPARING:
    case CALCULATING:
    case POST_PROCESSING:
      // Do nothing
      break;
    case DELETED:
      // This does not happen because the api response will return a 404.
      // If the service ever differentiates between a non-existent and a deleted job,
      // this could be used to fire a CalculationDeletedEvent and respond
      // appropriately. Until then, just log a warning
      GWTProd.warn("Job status is DELETED which is unexpected.");
      break;
    case CANCELLED:
      eventBus.fireEvent(new CalculationCancelEvent(calculation));
      break;
    case ERROR:
      NotificationUtil.broadcastError(eventBus,
          M.messages().notificationCalculationError(getJobName(calculation), calculationInfo.getJobProgress().getErrorMessage()));
      break;
    case COMPLETED:
      eventBus.fireEvent(new CalculationCompleteEvent(calculation));
      break;
    }
  }

  @EventHandler
  public void onCalculationStartEvent(final CalculationStartEvent e) {
    e.getValue().startLastUpdateTimer();
    NotificationUtil.broadcastMessage(eventBus, M.messages().notificationCalculationStarted(getJobName(e.getValue())), true);
  }

  @EventHandler
  public void onCalculationCompleteEvent(final CalculationCompleteEvent e) {
    e.getValue().stopLastUpdateTimer();
    ResultSummarySelectorUtil.setContext(e.getValue());
    NotificationUtil.broadcastMessage(eventBus, M.messages().notificationCalculationCompleted(getJobName(e.getValue())), true);
  }

  @EventHandler
  public void onCalculationRemoveCommand(final CalculationRemoveCommand c) {
    if (c.getValue().getJobState().isFinalState()) {
      eventBus.fireEvent(new CalculationDeleteCommand(c.getValue()));
    } else {
      eventBus.fireEvent(new CalculationCancelCommand(c.getValue()));
    }
  }

  @EventHandler
  public void onCalculationCancelCommand(final CalculationCancelCommand c) {
    final CalculationExecutionContext calculation = c.getValue();
    if (calculation.getJobKey() != null) {
      c.silence();
      calculationService.cancelCalculation(calculation.getJobKey(),
          AppAsyncCallback.create(res -> eventBus.fireEvent(c.getEvent()), e -> {
            eventBus.fireEvent(c.getEvent());
            NotificationUtil.broadcastError(eventBus, M.messages().notificationCalculationCancelledError(getJobName(calculation), e.getMessage()));
          }));
    }
  }

  @EventHandler
  void onCalculationCancelEvent(final CalculationCancelEvent e) {
    e.getValue().stopLastUpdateTimer();
    final CalculationExecutionContext calc = e.getValue();
    calculationContext.reset(calc);
    eventBus.fireEvent(new CalculationDeleteCommand(calc));
  }

  /**
   * If a Job is deleted also delete an calculation that is related to that job.
   */
  @EventHandler
  void onCalculationDeleteJobCommand(final CalculationDeleteJobCommand c) {
    final CalculationExecutionContext calc = prepContext.getActiveCalculation(c.getValue());

    if (calc != null) {
      eventBus.fireEvent(new CalculationCancelEvent(calc));
    }
  }

  @EventHandler
  void onCalculationDeleteCommand(final CalculationDeleteCommand c) {
    final CalculationExecutionContext job = c.getValue();
    if (job.getJobKey() != null) {
      c.silence();
      calculationService.deleteCalculation(job.getJobKey(),
          AppAsyncCallback.create(res -> eventBus.fireEvent(c.getEvent()), e -> eventBus.fireEvent(c.getEvent())));
    }
  }

  @EventHandler
  void onCalculationResetCommand(final CalculationResetCommand c) {
    purge(c.getValue());
  }

  @EventHandler
  void onCalculationDeleteEvent(final CalculationDeleteEvent e) {
    e.getValue().stopLastUpdateTimer();
    purge(e.getValue());
  }

  private static String getJobName(final CalculationExecutionContext calc) {
    return calc.getJobContext().getName();
  }

  private void purge(final CalculationExecutionContext executionContext) {
    calculationStatusPoller.remove(executionContext);
    calculationContext.removeCalculation(executionContext);
    prepContext.removeCalculation(executionContext);
  }

  /**
   * Collate all immediate problems before starting a calculation.
   *
   * Note: some or ideally (though not necessarily) all of these problems have already been reported to the user proactively.
   * This collation should be considered a fallback.
   */
  private List<String> findCalculationStartProblems(final CalculationJobContext jobContext) {
    final List<String> problems = new ArrayList<>();
    validateSituations(problems, jobContext);

    final CalculationSetOptions calculationOptions = jobContext.getCalculationOptions();
    validateNcaOptions(problems, calculationOptions);

    validateMetOptions(problems, calculationOptions);

    validateIdenticalCalculations(problems, jobContext);

    validateActiveCalculations(problems);

    return problems;
  }

  private void validateMetOptions(final List<String> problems, final CalculationSetOptions calculationOptions) {
    if (CalculateValidations.hasInvalidMetSite(applicationContext, calculationOptions)) {
      problems.add(M.messages().exportMetSiteSelectionInvalid());
    }
    if (CalculateValidations.hasInvalidMetYear(applicationContext, calculationOptions)) {
      problems.add(M.messages().exportMetYearSelectionInvalid());
    }
  }

  private void validateNcaOptions(final List<String> problems, final CalculationSetOptions calculationOptions) {
    final NCACalculationOptions ncaCalculationOptions = calculationOptions.getNcaCalculationOptions();
    if (CalculateValidations.hasInvalidPermitArea(applicationContext, ncaCalculationOptions)) {
      problems.add(M.messages().errorPermitAreaRequired());
    }
    if (CalculateValidations.hasInvalidProjectCategory(applicationContext, ncaCalculationOptions)) {
      problems.add(M.messages().errorProjectCategoryRequired());
    }
    if (DevelopmentPressureUtil.expectsDevelopmentPressureSearch(applicationContext, calculationOptions.getCalculationJobType())
        && CalculateValidations.hasNoSelectedDevelopmentPressureSources(ncaCalculationOptions)) {
      problems.add(M.messages().errorNoSourcesSelectedDevelopmentPressureSearch());
    }
  }

  private void validateIdenticalCalculations(final List<String> problems, final CalculationJobContext jobContext) {
    // Check if an identical calculation doth already exist.
    final String executionInputHash = ScenarioContext.calculateExecutionInputHashFromJob(scenarioContext, jobContext);
    calculationContext.getCalculations().stream()
        .filter(v -> executionInputHash.equals(v.getExecutionInputHash()))
        .filter(v -> !v.isError())
        .filter(v -> !v.isCancelled())
        .findFirst()
        .ifPresent(calc -> problems.add(M.messages().notificationCalculationAlreadyExists(getJobName(calc))));
  }

  private void validateActiveCalculations(final List<String> problems) {
    final long unfinishedCalculations = calculationContext.getCalculations().stream()
        .filter(v -> !v.getJobState().isFinalState())
        .count();
    if (unfinishedCalculations >= ApplicationLimits.ACTIVE_CALCULATION_LIMIT) {
      problems.add(M.messages().notificationCalculationMaximumAllowed(ApplicationLimits.ACTIVE_CALCULATION_LIMIT));
    }
  }

  private void validateSituations(final List<String> problems, final CalculationJobContext jobContext) {
    final List<SituationContext> situations = ScenarioContext.filterSituations(scenarioContext, jobContext.getSituationComposition());
    problems.addAll(situations.stream()
        .filter(v -> v.getSources().isEmpty()
            && v.getBuildings().isEmpty()
            && v.getTimeVaryingProfiles().isEmpty())
        .map(v -> M.messages().notificationCalculationNotStartedNoObjects(v.getName()))
        .collect(Collectors.toList()));

    if (CalculateValidations.hasNoEmissionsInAnyOfComposition(jobContext.getSituationComposition())) {
      problems.add(M.messages().calculateCompositionHasNoEmissions());
    }

    final int situationLimit = applicationContext.getConfiguration().getMaxNumberOfSituations();
    if (situations.size() > situationLimit) {
      problems.add(M.messages().calculateSituationLimit(situationLimit));
    }
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
