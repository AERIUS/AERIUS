/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;

/**
 * Util for development pressure related functionality.
 */
public final class DevelopmentPressureUtil {

  private DevelopmentPressureUtil() {
    // Utility class
  }

  public static void initDevelopmentPressureSources(final ApplicationContext applicationContext, final SituationComposition composition,
      final CalculationSetOptions calculationOptions) {
    final CalculationJobType calculationJobType = calculationOptions.getCalculationJobType();
    if (hasDevelopmentPressureSearchEnabled(applicationContext)) {
      if (expectsDevelopmentPressureSearch(applicationContext, calculationJobType)
          && composition.hasSituation(SituationType.PROPOSED)) {
        final List<String> developmentPressureSourceIds = composition.getSituation(SituationType.PROPOSED).getSources().stream()
            .map(s -> s.getGmlId())
            .collect(Collectors.toList());
        calculationOptions.getNcaCalculationOptions().setDevelopmentPressureSourceIds(developmentPressureSourceIds);
      } else {
        calculationOptions.getNcaCalculationOptions().setDevelopmentPressureSourceIds(new ArrayList<>());
      }
    }
  }

  public static boolean expectsDevelopmentPressureSearch(final ApplicationContext context, final CalculationJobType calculationJobType) {
    return hasDevelopmentPressureSearchEnabled(context)
        && (calculationJobType == CalculationJobType.PROCESS_CONTRIBUTION
            || calculationJobType == CalculationJobType.IN_COMBINATION_PROCESS_CONTRIBUTION);
  }

  private static boolean hasDevelopmentPressureSearchEnabled(final ApplicationContext context) {
    return context.isAppFlagEnabled(AppFlag.HAS_DEVELOPMENT_PRESSURE_SEARCH);
  }

}
