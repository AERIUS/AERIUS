/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.print.own2000;

import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.PdfProductType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.summary.ProcurementPolicy;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ExportContext;
import nl.overheid.aerius.wui.application.context.PrintContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.context.result.ResultSummaryContext;
import nl.overheid.aerius.wui.application.domain.importer.ScenarioMetaData;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsSummary;
import nl.overheid.aerius.wui.application.event.RestoreCalculationJobContextEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.application.print.own2000.results.ProcurementPolicyOverview;
import nl.overheid.aerius.wui.application.service.CalculationServiceAsync;
import nl.overheid.aerius.wui.application.ui.pages.results.stats.ResultsStatisticsFormatter;
import nl.overheid.aerius.wui.vue.BasicVueEventComponent;

@Component(components = {
    ProcurementPolicyOverview.class
})
public class Overview extends BasicVueEventComponent implements HasMounted {
  public static final OverviewEventBinder EVENT_BINDER = GWT.create(OverviewEventBinder.class);
  public static final String ID = "overview";
  private static final DateTimeFormat dateTimeFormat = DateTimeFormat.getFormat("dd MMMM yyyy, HH:mm");

  private static final ScenarioMetaData EMPTY_METADATA = ScenarioMetaData.create();

  interface OverviewEventBinder extends EventBinder<Overview> {}

  @Inject @Data ApplicationContext appContext;
  @Inject CalculationServiceAsync calculationService;
  @Inject PrintContext context;
  @Inject ResultsStatisticsFormatter resultsStatisticsFormatter;
  @Inject @Data ScenarioContext scenarioContext;
  @Inject @Data ExportContext exportContext;
  @Inject @Data CalculationContext calculationContext;
  @Data @JsProperty Map<String, SituationResultsSummary> projectResultSummaries = new HashMap<>();
  @Data @JsProperty Map<String, SituationResultsSummary> situationResultsSummaries = new HashMap<>();
  @Prop EventBus eventBus;
  @Prop boolean includeResults;
  @Prop boolean includePolicyResults;
  @Prop boolean includeAppendix;
  @Prop boolean includeActivity;

  @Override
  public void mounted() {
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Computed
  public Map<ProcurementPolicy, SituationResultsSummary> getDepositionSumResultsSummaries() {
    return getResultsSummaries(ScenarioResultType.DEPOSITION_SUM).entrySet().stream()
        .collect(Collectors.toMap(es -> es.getKey().getProcurementPolicy(), Map.Entry::getValue));
  }

  @JsMethod
  public Map<SituationResultsKey, SituationResultsSummary> getResultsSummaries(final ScenarioResultType resultType) {
    return Optional.ofNullable(calculationContext)
        .map(CalculationContext::getActiveCalculation)
        .map(CalculationExecutionContext::getResultContext)
        .map(ResultSummaryContext::getAvailableResults)
        .map(situationResultsKeySituationResultsSummaryMap -> situationResultsKeySituationResultsSummaryMap.entrySet().stream()
            .filter(entry -> entry.getKey().getResultType() == resultType)
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)))
        .orElseGet(HashMap::new);
  }

  @Computed
  public String getCalculationConfiguration() {
    return Optional.ofNullable(calculationContext.getActiveCalculation())
        .map(CalculationExecutionContext::getResultContext)
        .map(ResultSummaryContext::containsCalculationPointResults)
        .orElse(false) ? i18n.pdfOverviewCalculationConfigurationOwN2000Plus() : i18n.pdfOverviewCalculationConfigurationOwN2000();
  }

  @Computed
  public ScenarioMetaData getScenarioMetaData() {
    return exportContext.getScenarioMetaData();
  }

  @Computed("isScenarioMetaDataEmpty")
  public boolean isScenarioMetaDataEmpty() {
    return EMPTY_METADATA.equals(exportContext.getScenarioMetaData());
  }

  @Computed
  public String getTimestamp() {
    return dateTimeFormat.format(new Date());
  }

  @Computed
  public String getReference() {
    return context.getReference();
  }

  @Computed
  public boolean showAreaCalculated() {
    return context.getPdfProductType() == PdfProductType.OWN2000_SINGLE_SCENARIO;
  }

  @Computed
  public boolean showIncreases() {
    return context.getPdfProductType() != PdfProductType.OWN2000_SINGLE_SCENARIO;
  }

  @Computed
  public boolean showDecreases() {
    return context.getPdfProductType() != PdfProductType.OWN2000_PERMIT && context.getPdfProductType() != PdfProductType.OWN2000_SINGLE_SCENARIO;
  }

  @JsMethod
  public String getLongSituationName(final SituationContext situationContext) {
    return situationContext.getName() + " - " + M.messages().situationType(situationContext.getType());
  }

  @JsMethod
  public String formatEmission(final double emission) {
    return emission > 0 ? MessageFormatter.formatEmissionWithUnitSmart(emission) : "-";
  }

  @JsMethod
  public String formatDepositionValue(final SituationResultsSummary resultsSummary, final ResultStatisticType statisticsType) {
    return Optional.ofNullable(resultsSummary)
        .map(summary -> summary.getStatistics())
        .map(statistics -> resultsStatisticsFormatter.formatStatisticType(statistics, statisticsType, true))
        .orElse("");
  }

  @Computed
  public String getFormattedNettingFactor() {
    return getSituations().stream()
        .filter(situationContext -> situationContext.getType() == SituationType.OFF_SITE_REDUCTION)
        .map(situationContext -> situationContext.getNettingFactor())
        .map(nettingFactor -> MessageFormatter.toFixed(nettingFactor, 2))
        .findFirst().orElse(null);
  }

  @Computed
  public List<SituationContext> getSituations() {
    return scenarioContext.getSituations().stream()
        .sorted(Comparator.comparingInt(o -> PrintContext.PROJECT_CALCULATION_SITUATION_TYPES.indexOf(o.getType())))
        .collect(Collectors.toList());
  }

  @Computed
  public SituationResultsSummary getCurrentSituationSummary() {
    return situationResultsSummaries.get(scenarioContext.getActiveSituation().getId());
  }

  @JsMethod
  public String getHexagonId(final SituationResultsSummary summary, final ResultStatisticType type) {
    return Optional.ofNullable(summary)
        .map(resultsSummary -> resultsSummary.getStatisticReceptors())
        .map(receptors -> receptors.getValue(type))
        .map(receptor -> String.valueOf(receptor.getReceptorId()))
        .orElse(null);
  }

  @JsMethod
  public String getAreaName(final SituationResultsSummary summary, final ResultStatisticType type) {
    return Optional.ofNullable(summary)
        .map(resultsSummary -> resultsSummary.getStatisticReceptors())
        .map(receptors -> receptors.getValue(type))
        .map(receptor -> String.valueOf(receptor.getAssessmentAreaName()))
        .orElse(null);
  }

  @JsMethod
  public String getFormattedProjectResultStatistic(final ResultStatisticType resultStatisticType) {
    return Optional.ofNullable(scenarioContext.getActiveSituation())
        .map(SituationContext::getId)
        .map(projectResultSummaries::get)
        .map(SituationResultsSummary::getStatistics)
        .map(statistics -> resultsStatisticsFormatter.formatStatisticType(statistics, resultStatisticType, true))
        .orElse("");
  }

  // ToDo: this method shouldn't exist, and contents should be handled in a Daemon.
  //  Maybe by putting relevant SituationResultKeys into the PrintContext?
  @EventHandler
  public void onRestoreCalculationContextEvent(final RestoreCalculationJobContextEvent e) {
    if (context.getPdfProductType() == PdfProductType.OWN2000_CALCULATOR) {
      for (final SituationContext situation : scenarioContext.getSituations()) {
        if (situation.getType() == SituationType.PROPOSED) {
          calculationService.getSituationResultsSummary(context.getJobKey(),
              new SituationResultsKey(SituationContext.handleFromSituation(situation), ScenarioResultType.PROJECT_CALCULATION,
                  appContext.getConfiguration().getEmissionResultKeys().get(0), context.getSummaryHexagonType(),
                  context.getOverlappingHexagonType(), null),
              resultsSummary -> projectResultSummaries.put(situation.getId(), resultsSummary));
        }
        calculationService.getSituationResultsSummary(context.getJobKey(),
            new SituationResultsKey(SituationContext.handleFromSituation(situation), ScenarioResultType.SITUATION_RESULT,
                appContext.getConfiguration().getEmissionResultKeys().get(0), context.getSummaryHexagonType(),
                context.getOverlappingHexagonType(), null),
            resultsSummary -> situationResultsSummaries.put(situation.getId(), resultsSummary));
      }
    } else {
      for (final SituationContext situation : scenarioContext.getSituations()) {
        calculationService.getSituationResultsSummary(context.getJobKey(),
            new SituationResultsKey(SituationContext.handleFromSituation(situation), ScenarioResultType.SITUATION_RESULT,
                appContext.getConfiguration().getEmissionResultKeys().get(0), context.getSummaryHexagonType(),
                context.getOverlappingHexagonType(), null),
            resultsSummary -> situationResultsSummaries.put(situation.getId(), resultsSummary));
      }
    }
  }
}
