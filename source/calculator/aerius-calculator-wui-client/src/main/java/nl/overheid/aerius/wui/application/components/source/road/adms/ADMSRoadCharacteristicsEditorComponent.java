/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.road.adms;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.ADMSLimits;
import nl.overheid.aerius.wui.application.components.input.CheckBoxComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.road.adms.ADMSRoadCharacteristicsEditorValidators.ADMSRoadCharacteristicsEditorValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.ADMSCharacteristics;
import nl.overheid.aerius.wui.application.domain.source.ADMSRoadESFeature;

/**
 * Editor component of {@link ADMSCharacteristics}.
 */
@Component(customizeOptions = {
    ADMSRoadCharacteristicsEditorValidators.class
}, directives = {
    ValidateDirective.class
}, components = {
    ADMSBarrierEditorComponent.class,
    ValidationBehaviour.class,
    CheckBoxComponent.class,
    VerticalCollapse.class,
    LabeledInputComponent.class,
})
public class ADMSRoadCharacteristicsEditorComponent extends ErrorWarningValidator
    implements HasCreated, HasValidators<ADMSRoadCharacteristicsEditorValidations> {
  @Prop EventBus eventBus;
  @Prop(required = true) ADMSRoadESFeature source;

  @Inject @Data ApplicationContext applicationContext;

  @Data String widthV;
  @Data String elevationV;
  @Data String gradientV;
  @Data String coverageV;

  @Data boolean roadTunnel = false;

  // Validations
  @JsProperty(name = "$v") ADMSRoadCharacteristicsEditorValidations validation;

  @Override
  @Computed
  public ADMSRoadCharacteristicsEditorValidations getV() {
    return validation;
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Watch(value = "source", isImmediate = true)
  public void onSourceChange(final ADMSRoadESFeature neww) {
    if (neww == null) {
      return;
    }

    widthV = String.valueOf(neww.getWidth());
    elevationV = String.valueOf(neww.getElevation());
    gradientV = String.valueOf(neww.getGradient());
    coverageV = String.valueOf(neww.getCoverage());
  }

  @Computed("hasAnyBarrier")
  public boolean hasAnyBarrier() {
    return source.getBarrierLeft() != null
        || source.getBarrierRight() != null;
  }

  @Computed
  public boolean getBarriersEnabled() {
    return !roadTunnel;
  }

  @Computed
  protected String getWidth() {
    return widthV;
  }

  @Computed
  protected void setWidth(final String width) {
    ValidationUtil.setSafeDoubleValue(v -> source.setWidth(v), width, 0D);
    widthV = width;
  }

  @Computed
  protected String getElevation() {
    return elevationV;
  }

  @Computed
  protected void setElevation(final String elevation) {
    ValidationUtil.setSafeDoubleValue(v -> source.setElevation(v), elevation, 0D);
    elevationV = elevation;
  }

  @Computed
  protected String getGradient() {
    return gradientV;
  }

  @Computed
  protected void setGradient(final String gradient) {
    ValidationUtil.setSafeDoubleValue(v -> source.setGradient(v), gradient, 0D);
    gradientV = gradient;
  }

  @Computed
  protected String getCoverage() {
    return coverageV;
  }

  @Computed
  protected void setCoverage(final String coverage) {
    ValidationUtil.setSafeDoubleValue(v -> source.setCoverage(v), coverage, 0D);
    coverageV = coverage;
  }

  @Computed
  public String getWidthError() {
    return i18n.ivDoubleRangeBetween(i18n.sourceRoadWidthLabel(), ADMSLimits.ROAD_WIDTH_MIN, ADMSLimits.ROAD_WIDTH_MAX);
  }

  @Computed
  public String getElevationError() {
    return i18n.ivDoubleRangeBetween(i18n.sourceRoadElevationLabel(), ADMSLimits.ROAD_ELEVATION_MIN, ADMSLimits.ROAD_ELEVATION_MAX);
  }

  @Computed
  public String getGradientError() {
    return i18n.ivDoubleRangeBetween(i18n.sourceRoadGradientLabel(), ADMSLimits.ROAD_GRADIENT_MIN, ADMSLimits.ROAD_GRADIENT_MAX);
  }

  @Computed
  public String getCoverageError() {
    return i18n.ivDoubleRangeBetween(i18n.sourceRoadCoverageLabel(), ADMSLimits.ROAD_COVERAGE_MIN, ADMSLimits.ROAD_COVERAGE_MAX);
  }

  @Computed("isInvalid")
  protected boolean isInvalid() {
    return validation.widthV.invalid
        || validation.elevationV.invalid
        || validation.gradientV.invalid
        || validation.coverageV.invalid;
  }
}
