/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon;

import javax.inject.Inject;

import com.google.web.bindery.event.shared.EventBus;

import nl.aerius.geo.daemon.LayerContextDaemon;
import nl.aerius.search.wui.daemon.SearchDaemonAsynchronous;
import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.daemon.DaemonBootstrapper;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.event.HasEventBus;
import nl.overheid.aerius.wui.application.daemon.calculation.CalculationDaemon;
import nl.overheid.aerius.wui.application.daemon.calculation.CalculationPreparationDaemon;
import nl.overheid.aerius.wui.application.daemon.calculation.ResultLayerDaemon;
import nl.overheid.aerius.wui.application.daemon.calculation.ResultSelectionDaemon;
import nl.overheid.aerius.wui.application.daemon.calculation.ResultsSummaryDaemon;
import nl.overheid.aerius.wui.application.daemon.export.ExportDaemon;
import nl.overheid.aerius.wui.application.daemon.file.FileContextDaemon;
import nl.overheid.aerius.wui.application.daemon.file.ImportDaemon;
import nl.overheid.aerius.wui.application.daemon.flags.ApplicationFlagDaemon;
import nl.overheid.aerius.wui.application.daemon.geo.ApplicationLayerDaemon;
import nl.overheid.aerius.wui.application.daemon.geo.BuildingMapDaemon;
import nl.overheid.aerius.wui.application.daemon.geo.CalculationPointMapDaemon;
import nl.overheid.aerius.wui.application.daemon.geo.EmissionSourceMapDaemon;
import nl.overheid.aerius.wui.application.daemon.geo.InfoMarkerDaemon;
import nl.overheid.aerius.wui.application.daemon.geo.LayerDaemon;
import nl.overheid.aerius.wui.application.daemon.geo.MapDaemon;
import nl.overheid.aerius.wui.application.daemon.geo.PanelDaemon;
import nl.overheid.aerius.wui.application.daemon.misc.AccessibilityDaemon;
import nl.overheid.aerius.wui.application.daemon.misc.ColorThemeDaemon;
import nl.overheid.aerius.wui.application.daemon.misc.CompatibilityDaemon;
import nl.overheid.aerius.wui.application.daemon.misc.EasterDaemon;
import nl.overheid.aerius.wui.application.daemon.misc.ExternalContentDaemon;
import nl.overheid.aerius.wui.application.daemon.misc.HotKeyDaemon;
import nl.overheid.aerius.wui.application.daemon.misc.ManualDaemon;
import nl.overheid.aerius.wui.application.daemon.misc.NotificationDaemon;
import nl.overheid.aerius.wui.application.daemon.misc.PlaceContextDaemon;
import nl.overheid.aerius.wui.application.daemon.misc.PreferencesDaemon;
import nl.overheid.aerius.wui.application.daemon.misc.SystemInfoDaemon;
import nl.overheid.aerius.wui.application.daemon.navigation.ApplicationExitDaemon;
import nl.overheid.aerius.wui.application.daemon.navigation.NavigationDaemon;
import nl.overheid.aerius.wui.application.daemon.print.PrintOwN2000ContextDaemon;
import nl.overheid.aerius.wui.application.daemon.scenario.BuildingEditorDaemon;
import nl.overheid.aerius.wui.application.daemon.scenario.BuildingListDaemon;
import nl.overheid.aerius.wui.application.daemon.scenario.CalculationPointAutomaticPlacementDaemon;
import nl.overheid.aerius.wui.application.daemon.scenario.CalculationPointEditorDaemon;
import nl.overheid.aerius.wui.application.daemon.scenario.CalculationPointListDaemon;
import nl.overheid.aerius.wui.application.daemon.scenario.CalculationPointValidationDaemon;
import nl.overheid.aerius.wui.application.daemon.scenario.EmissionSourceEditorDaemon;
import nl.overheid.aerius.wui.application.daemon.scenario.EmissionSourceEmissionsDaemon;
import nl.overheid.aerius.wui.application.daemon.scenario.EmissionSourceListDaemon;
import nl.overheid.aerius.wui.application.daemon.scenario.EmissionSourceValidationDaemon;
import nl.overheid.aerius.wui.application.daemon.scenario.InlandWaterwayDeamon;
import nl.overheid.aerius.wui.application.daemon.scenario.RoadNetworkDeamon;
import nl.overheid.aerius.wui.application.daemon.scenario.ScenarioContextDaemon;
import nl.overheid.aerius.wui.application.daemon.scenario.SituationDaemon;
import nl.overheid.aerius.wui.application.daemon.scenario.TimeVaryingProfileEditorDaemon;
import nl.overheid.aerius.wui.application.daemon.scenario.TimeVaryingProfileListDaemon;
import nl.overheid.aerius.wui.application.daemon.search.SearchActionDaemon;

/**
 * Root of all Deamons. This class registers all deamons.
 */
public class ApplicationDaemonBootstrapper extends BasicEventComponent implements DaemonBootstrapper {
  @Inject private PlaceContextDaemon placeContextDaemon;
  @Inject private SystemInfoDaemon systemInfoDaemon;
  @Inject private LayerContextDaemon layerContextDaemon;
  @Inject private FileContextDaemon fileContextDaemon;
  @Inject private ExportDaemon exportDaemon;
  @Inject private MapDaemon mapDaemon;
  @Inject private LayerDaemon layerDaemon;

  @Inject private NotificationDaemon notificationDaemon;
  @Inject private CompatibilityDaemon compatibilityDaemon;
  @Inject private ColorThemeDaemon colorThemeDaemon;

  @Inject private CalculationPreparationDaemon calculationPreparationDaemon;
  @Inject private CalculationDaemon calculationDaemon;
  @Inject private CalculationPointValidationDaemon calculationPointValidationDaemon;
  @Inject private ResultsSummaryDaemon resultsSummaryDaemon;
  @Inject private ResultSelectionDaemon resultsSelectionDaemon;
  @Inject private ResultLayerDaemon resultsLayerDaemon;
  @Inject private ApplicationLayerDaemon applicationLayerDaemon;

  @Inject private ApplicationExitDaemon applicationExitDaemon;
  @Inject private AccessibilityDaemon accessibilityDaemon;

  @Inject private NavigationDaemon navigationDaemon;
  @Inject private ManualDaemon manualDaemon;
  @Inject private ExternalContentDaemon externalContentDaemon;

  @Inject private ImportDaemon importDaemon;
  @Inject private ScenarioContextDaemon scenarioContextDaemon;
  @Inject private SituationDaemon situationDaemon;

  @Inject private UserDaemon userDaemon;
  @Inject private ApplicationFlagDaemon applicationFlagDaemon;
  @Inject private PreferencesDaemon preferencesDaemon;

  @Inject private InfoMarkerDaemon infoMarkerDaemon;
  @Inject private PanelDaemon panelDaemon;

  @Inject private BuildingEditorDaemon buildingEditorDaemon;
  @Inject private BuildingListDaemon buildingListDaemon;
  @Inject private BuildingMapDaemon buildingMapDaemon;

  @Inject private TimeVaryingProfileEditorDaemon timeVaryingProfileEditorDaemon;
  @Inject private TimeVaryingProfileListDaemon timeVaryingProfileListDaemon;

  @Inject private EmissionSourceValidationDaemon emissionSourceValidationDaemon;
  @Inject private EmissionSourceEditorDaemon emissionSourceEditorDaemon;
  @Inject private EmissionSourceListDaemon emissionSourceListDaemon;
  @Inject private EmissionSourceEmissionsDaemon emissionSourceEmissionsDaemon;
  @Inject private EmissionSourceMapDaemon emissionSourceMapDaemon;
  @Inject private RoadNetworkDeamon roadNetworkDeamon;
  @Inject private InlandWaterwayDeamon inlandWaterwayDeamon;

  @Inject private SearchActionDaemon searchActionDaemon;
  @Inject private SearchDaemonAsynchronous searchDaemon;

  @Inject private CalculationPointEditorDaemon calculationPointEditorDaemon;
  @Inject private CalculationPointListDaemon calculationPointListDaemon;
  @Inject private CalculationPointAutomaticPlacementDaemon calculationPointAutomaticPlacementDaemon;
  @Inject private CalculationPointMapDaemon calculationPointMapDaemon;

  @Inject private PrintOwN2000ContextDaemon printOwN2000ContextDaemon;

  @Inject private HotKeyDaemon hotkeyDaemon;

  @Inject private EasterDaemon easterDaemon;

  @Override
  public void setEventBus(final EventBus eventBus) {
    setEventBus(eventBus,
        placeContextDaemon,
        systemInfoDaemon,
        layerContextDaemon,
        fileContextDaemon,
        exportDaemon,
        calculationPreparationDaemon,
        calculationDaemon,
        calculationPointValidationDaemon,
        resultsSummaryDaemon,
        resultsSelectionDaemon,
        resultsLayerDaemon,
        applicationLayerDaemon,
        mapDaemon,
        layerDaemon,
        notificationDaemon,
        compatibilityDaemon,
        applicationExitDaemon,
        accessibilityDaemon,
        colorThemeDaemon,
        navigationDaemon,
        manualDaemon,
        externalContentDaemon,
        importDaemon,
        scenarioContextDaemon,
        situationDaemon,
        infoMarkerDaemon,
        panelDaemon,
        userDaemon,
        applicationFlagDaemon,
        preferencesDaemon,
        buildingEditorDaemon,
        buildingListDaemon,
        buildingMapDaemon,
        timeVaryingProfileEditorDaemon,
        timeVaryingProfileListDaemon,
        emissionSourceValidationDaemon,
        emissionSourceEditorDaemon,
        emissionSourceListDaemon,
        emissionSourceEmissionsDaemon,
        emissionSourceMapDaemon,
        roadNetworkDeamon,
        inlandWaterwayDeamon,
        searchActionDaemon,
        searchDaemon,
        calculationPointEditorDaemon,
        calculationPointListDaemon,
        calculationPointAutomaticPlacementDaemon,
        calculationPointMapDaemon,
        printOwN2000ContextDaemon,
        hotkeyDaemon,
        easterDaemon);
  }

  @Override
  public void setEventBus(final EventBus eventBus, final HasEventBus... children) {
    super.setEventBus(eventBus, children);

    for (final HasEventBus child : children) {
      if (child instanceof Daemon) {
        ((Daemon) child).init();
      }
    }
  }
}
