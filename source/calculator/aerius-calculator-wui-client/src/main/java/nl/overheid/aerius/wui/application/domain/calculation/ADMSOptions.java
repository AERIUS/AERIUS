/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.calculation;

import static jsinterop.annotations.JsPackage.GLOBAL;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.calculation.MetDatasetType;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.ADMSLimits;
import nl.overheid.aerius.wui.application.util.json.JsonBuilder;
import nl.overheid.aerius.wui.vue.ReactivityUtil;

/**
 * Contains client side variant of ADMS specific options for a calculation.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class ADMSOptions {

  private @JsProperty double minMoninObukhovLength;
  private @JsProperty double surfaceAlbedo;
  private @JsProperty double priestleyTaylorParameter;

  private @JsProperty boolean plumeDepletionNH3;
  private @JsProperty boolean plumeDepletionNOX;
  private @JsProperty boolean complexTerrain;

  private @JsProperty String metDatasetType;
  private @JsProperty int metSiteId;
  private @JsProperty String[] metYears;

  public static final @JsOverlay ADMSOptions create() {
    final ADMSOptions options = new ADMSOptions();
    init(options);
    return options;
  }

  public static final @JsOverlay void init(final ADMSOptions options) {
    ReactivityUtil.ensureDefault(options::getMinMoninObukhovLength, options::setMinMoninObukhovLength,
        ADMSLimits.MIN_MONIN_OBUKHOV_LENGTH_DEFAULT);
    ReactivityUtil.ensureDefault(options::getSurfaceAlbedo, options::setSurfaceAlbedo,
        ADMSLimits.SURFACE_ALBEDO_DEFAULT);
    ReactivityUtil.ensureDefault(options::getPriestleyTaylorParameter, options::setPriestleyTaylorParameter,
        ADMSLimits.PRIESTLEY_TAYLOR_PARAMETER_DEFAULT);

    ReactivityUtil.ensureDefault(options::isPlumeDepletionNH3, options::setPlumeDepletionNH3, false);
    ReactivityUtil.ensureDefault(options::isPlumeDepletionNOX, options::setPlumeDepletionNOX, false);
    ReactivityUtil.ensureDefault(options::isComplexTerrain, options::setComplexTerrain, false);

    ReactivityUtil.ensureDefault(options::getMetDatasetType, options::setMetDatasetType);
    ReactivityUtil.ensureDefault(options::getMetSiteId, options::setMetSiteId, 0);
    ReactivityUtil.ensureJsPropertySupplied(options, "metYears", options::setMetYears, () -> new ArrayList<>());
  }

  public final @JsOverlay double getMinMoninObukhovLength() {
    return minMoninObukhovLength;
  }

  public final @JsOverlay void setMinMoninObukhovLength(final double minMoninObukhovLength) {
    this.minMoninObukhovLength = minMoninObukhovLength;
  }

  public final @JsOverlay double getSurfaceAlbedo() {
    return surfaceAlbedo;
  }

  public final @JsOverlay void setSurfaceAlbedo(final double surfaceAlbedo) {
    this.surfaceAlbedo = surfaceAlbedo;
  }

  public final @JsOverlay double getPriestleyTaylorParameter() {
    return priestleyTaylorParameter;
  }

  public final @JsOverlay void setPriestleyTaylorParameter(final double priestleyTaylorParameter) {
    this.priestleyTaylorParameter = priestleyTaylorParameter;
  }

  public final @JsOverlay boolean isPlumeDepletionNH3() {
    return plumeDepletionNH3;
  }

  public final @JsOverlay void setPlumeDepletionNH3(final boolean plumeDepletionNH3) {
    this.plumeDepletionNH3 = plumeDepletionNH3;
  }

  public final @JsOverlay boolean isPlumeDepletionNOX() {
    return plumeDepletionNOX;
  }

  public final @JsOverlay void setPlumeDepletionNOX(final boolean plumeDepletionNOX) {
    this.plumeDepletionNOX = plumeDepletionNOX;
  }

  public final @JsOverlay boolean isComplexTerrain() {
    return complexTerrain;
  }

  public final @JsOverlay void setComplexTerrain(final boolean complexTerrain) {
    this.complexTerrain = complexTerrain;
  }

  public final @JsOverlay int getMetSiteId() {
    return metSiteId;
  }

  public final @JsOverlay void setMetSiteId(final int metSiteId) {
    this.metSiteId = metSiteId;
  }

  public final @JsOverlay MetDatasetType getMetDatasetType() {
    return MetDatasetType.safeValueOf(metDatasetType);
  }

  public final @JsOverlay void setMetDatasetType(final MetDatasetType metDatasetType) {
    this.metDatasetType = metDatasetType == null ? null : metDatasetType.name();
  }

  public final @JsOverlay List<String> getMetYears() {
    return metYears == null ? new ArrayList<>() : Arrays.asList(metYears);
  }

  public final @JsOverlay void setMetYears(final List<String> metYears) {
    this.metYears = metYears == null ? new String[] {} : metYears.toArray(new String[metYears.size()]);
  }

  public final @JsOverlay Object toJSON() {
    return new JsonBuilder(this)
        .include("minMoninObukhovLength", "surfaceAlbedo", "priestleyTaylorParameter", "plumeDepletionNH3", "plumeDepletionNOX",
            "complexTerrain", "metSiteId", "metDatasetType", "metYears")
        .build();
  }
}
