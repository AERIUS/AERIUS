/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.road;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;

import elemental2.core.JsArray;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.sector.category.OnRoadMobileSourceCategory;
import nl.overheid.aerius.wui.application.components.source.road.custom.RoadCustomEmissionComponent;
import nl.overheid.aerius.wui.application.components.source.road.specific.RoadSpecificEmissionComponent;
import nl.overheid.aerius.wui.application.components.source.road.standard.RoadStandardEmissionComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.components.toggle.ToggleButtons;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.domain.source.EmissionFactors;
import nl.overheid.aerius.wui.application.domain.source.RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.road.CustomVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.SpecificVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.VehicleType;
import nl.overheid.aerius.wui.application.domain.source.road.Vehicles;
import nl.overheid.aerius.wui.application.domain.source.util.EmissionSourceFeatureUtil;

@Component(components = {
    RoadCustomEmissionComponent.class,
    RoadStandardEmissionComponent.class,
    RoadSpecificEmissionComponent.class,
    ValidationBehaviour.class,
    ToggleButtons.class,
    VerticalCollapse.class,
})
public class RoadDetailEditorComponent extends ErrorWarningValidator {
  @Prop RoadESFeature source;
  @Prop Vehicles subSource;
  @Prop int selectedIndex;
  @Prop(required = true) boolean stagnationEnabled;
  @Prop(required = true) boolean maximumSpeedDropDown;
  @Data VehicleType selectedVehicleType;

  @Data @Inject ApplicationContext applicationContext;
  @Data @Inject ScenarioContext scenarioContext;

  @Computed
  public List<OnRoadMobileSourceCategory> getCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getOnRoadMobileSourceCategories();
  }

  @Computed
  List<VehicleType> getVehicleTypes() {
    return Arrays.asList(VehicleType.CUSTOM, VehicleType.STANDARD);
  }

  /**
   * The CUSTOM and SPECIFIC types are rolled into 1 tab, so 'correct' the
   * returned type here to reflect that.
   */
  @Computed
  VehicleType getCorrectedSelectedVehicleType() {
    return selectedVehicleType == VehicleType.CUSTOM || selectedVehicleType == VehicleType.SPECIFIC
        ? VehicleType.CUSTOM
        : selectedVehicleType;
  }

  @Computed
  public int getYear() {
    return scenarioContext.getActiveSituation().getYear();
  }

  @Watch(value = "subSource", isImmediate = true)
  public void onValueChange(final Integer neww, final Integer old) {
    if (neww == null) {
      return;
    }

    selectedVehicleType = subSource.getVehicleType();
  }

  @JsMethod
  protected boolean isSourceAvailable() {
    return subSource != null;
  }

  @JsMethod
  protected void selectTab(final VehicleType vehicleType) {
    selectedVehicleType = vehicleType;
    updateRow(EmissionSourceFeatureUtil.convertVehiclesType(vehicleType, subSource,
        applicationContext.getConfiguration().getEmissionSubstancesRoad(),
        applicationContext.getConfiguration().getSectorCategories().getRoadEmissionCategories().getVehicleTypesCodes()));
  }

  @JsMethod
  protected void switchVehicleTypeCustom(final EmissionFactors emissionFactors) {
    final CustomVehicles newVehicle = (CustomVehicles) EmissionSourceFeatureUtil.convertVehiclesType(VehicleType.CUSTOM, subSource,
        applicationContext.getConfiguration().getEmissionSubstancesRoad(),
        applicationContext.getConfiguration().getSectorCategories().getRoadEmissionCategories().getVehicleTypesCodes());
    newVehicle.setVehiclesPerTimeUnit(((SpecificVehicles) subSource).getVehiclesPerTimeUnit());
    newVehicle.setEmissionFactors(emissionFactors);
    selectedVehicleType = VehicleType.CUSTOM;
    updateRow(newVehicle);
  }

  @JsMethod
  protected void switchVehicleTypeSpecific(final String vehicleCode) {
    final SpecificVehicles newVehicle = (SpecificVehicles) EmissionSourceFeatureUtil.convertVehiclesType(
        VehicleType.SPECIFIC,
        subSource,
        applicationContext.getConfiguration().getEmissionSubstancesRoad(),
        applicationContext.getConfiguration().getSectorCategories().getRoadEmissionCategories().getVehicleTypesCodes());
    newVehicle.setVehiclesPerTimeUnit(((CustomVehicles) subSource).getVehiclesPerTimeUnit());
    newVehicle.setVehicleCode(vehicleCode);
    selectedVehicleType = VehicleType.SPECIFIC;
    updateRow(newVehicle);
  }

  private void updateRow(final Vehicles convertVehiclesType) {
    final JsArray<Vehicles> vehicles = source.getSubSources();
    vehicles.splice(selectedIndex, 1, convertVehiclesType);
  }
}
