/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.print.nca;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.PrintContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component
public class NcaTableOfContentsPage extends BasicVueComponent implements HasCreated {
  private static final String OVERVIEW_PAGE_KEY = "";
  private static final String MET_PAGE_KEY = "";
  private static final String RESULTS_PAGE_KEY = "";

  @Data String calculationType = "In-combination process contribution";

  @Data @JsProperty List<TableOfContentsItem> items;

  @Inject @Data PrintContext context;

  @Inject @Data EnvironmentConfiguration cfg;
  @Inject @Data ApplicationContext applicationContext;
  @Inject @Data ScenarioContext scenarioContext;

  @Computed("hasSituation")
  public boolean hasSituation() {
    return !scenarioContext.getSituations().isEmpty();
  }

  @Computed
  public String getName() {
    return scenarioContext.getSituations().stream().findFirst().get().getName();
  }

  @Computed
  public String getYear() {
    return String.valueOf(scenarioContext.getSituations().stream().findFirst().get().getYear());
  }

  @Override
  public void created() {
    items = new ArrayList<>();
    items.add(new TableOfContentsItem(1, "Overview", OVERVIEW_PAGE_KEY));
    items.add(new TableOfContentsItem(2, "Meteorological (Met) Data Information", MET_PAGE_KEY));
    items.add(new TableOfContentsItem(3, "Modelling results", RESULTS_PAGE_KEY));
  }

  @Computed
  public String getApplicationVersion() {
    return cfg.getApplicationVersion();
  }

  @Computed
  public String getDatabaseVersion() {
    return applicationContext.getConfiguration().getSettings().getSetting(SharedConstantsEnum.DATABASE_VERSION);
  }
}
