/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.results;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.geom.Point;
import ol.style.Icon;
import ol.style.IconOptions;
import ol.style.Style;
import ol.style.StyleOptions;

import nl.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.wui.application.context.MapConfigurationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.GenericImaerFeature;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceAddedEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceDeletedEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceListChangeEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceUpdatedEvent;
import nl.overheid.aerius.wui.application.geo.layers.base.BaseVectorLayer;
import nl.overheid.aerius.wui.application.geo.layers.base.VectorFeaturesWrapper;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.resources.images.markers.MarkerResource;
import nl.overheid.aerius.wui.application.resources.images.markers.MarkerResourceUtil;
import nl.overheid.aerius.wui.application.ui.nca.pages.calculate.options.TemporaryGeometryUtil;

/**
 * Provides a layer with the site location pin for use in the PDF report.
 */
public class SiteLocationLayer extends BaseVectorLayer<GenericImaerFeature> {
  interface SiteLocationLayerEventBinder extends EventBinder<SiteLocationLayer> {
  }

  private static final SiteLocationLayerEventBinder EVENT_BINDER = GWT.create(SiteLocationLayerEventBinder.class);

  private final VectorFeaturesWrapper vectorObject = new VectorFeaturesWrapper();

  private final ScenarioContext scenarioContext;
  private final MapConfigurationContext mapConfigurationContext;

  @Inject
  public SiteLocationLayer(final EventBus eventBus, @Assisted final int zIndex,
      final MapConfigurationContext mapConfigurationContext, final ScenarioContext scenarioContext) {
    super(new LayerInfo(), zIndex);
    this.mapConfigurationContext = mapConfigurationContext;
    this.scenarioContext = scenarioContext;
    getInfo().setName(SiteLocationLayer.class.getName());
    getInfo().setTitle(M.messages().layerSiteLocation());

    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  private void update() {
    final List<EmissionSourceFeature> features = scenarioContext.getActiveSituation().getSources();
    if (features == null || features.isEmpty()) {
      setFeatures(vectorObject, Collections.emptyList());
      return;
    }

    final Point middlePoint = TemporaryGeometryUtil.getMiddleOfFeatures(features);
    if (middlePoint == null) {
      return;
    }

    final GenericImaerFeature siteLocationFeature = GenericImaerFeature.create();

    siteLocationFeature.setId("site_location_id");
    siteLocationFeature.setGeometry(middlePoint);

    final List<GenericImaerFeature> siteLocationFeatureList = new ArrayList<>();
    siteLocationFeatureList.add(siteLocationFeature);
    setFeatures(vectorObject, siteLocationFeatureList);
  }

  @Override
  protected Style[] createStyle(final GenericImaerFeature feature, final double resolution) {
    final IconOptions iconOptions = new IconOptions();
    final MarkerResource markerResource = MarkerResourceUtil.getSiteLocationMarker();

    iconOptions.setSrc(markerResource.getDataResource().getSafeUri().asString());
    iconOptions.setAnchor(markerResource.getAnchor());
    iconOptions.setScale(mapConfigurationContext.getIconScale());

    final Icon icon = new Icon(iconOptions);
    final StyleOptions styleOptions = new StyleOptions();
    styleOptions.setImage(icon);
    final Style style = new Style(styleOptions);

    return new Style[] {style};
  }

  // NOTE: These event handlers are used to test the location on the map in-application. They are not needed for the PDF report
  @EventHandler
  public void onEmissionSourceDeletedEvent(final EmissionSourceDeletedEvent e) {
    update();
  }

  @EventHandler
  public void onEmissionSourceAddedEvent(final EmissionSourceAddedEvent e) {
    update();
  }

  @EventHandler
  public void onEmissionSourceUpdatedEvent(final EmissionSourceUpdatedEvent e) {
    update();
  }

  @EventHandler
  public void onEmissionSourceListChangeEvent(final EmissionSourceListChangeEvent e) {
    update();
  }

  // TODO: Catch event when PDF launches so the layer gets updated for use in the PDF

}
