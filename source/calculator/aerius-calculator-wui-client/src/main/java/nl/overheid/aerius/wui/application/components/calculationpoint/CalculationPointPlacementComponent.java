/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.calculationpoint;

import java.util.List;
import java.util.function.Function;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointsAddCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CloseCalculationPointAutomaticPlacement;
import nl.overheid.aerius.wui.application.command.calculationpoint.DetermineCalculationPointsAutomaticallyCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.ResetCalculationPointsAutomaticallyCommand;
import nl.overheid.aerius.wui.application.components.input.CheckBoxComponent;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.InputWithUnitComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.CalculationPointAutomaticPlacementContext;
import nl.overheid.aerius.wui.application.context.CalculationPointAutomaticPlacementContext.PlacementStatus;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component(components = {
    SimplifiedListBoxComponent.class,
    VerticalCollapse.class,
    CheckBoxComponent.class,
    LabeledInputComponent.class,
    InputWithUnitComponent.class,
    InputErrorComponent.class,
})
public class CalculationPointPlacementComponent extends BasicVueView implements HasCreated, HasMounted {

  private static final int MIN_INLAND_RADIUS = 1;

  @Prop EventBus eventBus;
  @Inject @Data ScenarioContext scenarioContext;
  @Inject @Data ApplicationContext applicationContext;
  @Inject @Data CalculationPointAutomaticPlacementContext placementContext;

  @Data SituationContext selectedSituation;

  @Data boolean pointsAbroad = true;
  @Data boolean pointsInland = false;
  @Data int inlandRadius = 0;

  @Override
  public void created() {
    selectedSituation = scenarioContext.getActiveSituation();
    inlandRadius = getMaxInlandRadius();
  }

  @Computed("showAbroadOption")
  public boolean showAbroadOption() {
    return applicationContext.isAppFlagEnabled(AppFlag.SHOW_ABROAD_OPTIONS);
  }

  @Computed
  public List<SituationContext> getSituations() {
    return scenarioContext.getSituations();
  }

  @Computed("situationToKey")
  public Function<Object, Object> situationToKey() {
    return v -> ((SituationContext) v).getId();
  }

  @Computed("isDetermining")
  public boolean isDetermining() {
    return placementContext.isDetermining();
  }

  @JsMethod
  public void determinePlacement() {
    eventBus.fireEvent(
        new DetermineCalculationPointsAutomaticallyCommand(selectedSituation.getSources(), pointsAbroad, pointsInland ? inlandRadius : null));
  }

  @JsMethod
  public void reset() {
    eventBus.fireEvent(new ResetCalculationPointsAutomaticallyCommand());
  }

  @JsMethod
  public void clickCancel() {
    eventBus.fireEvent(new ResetCalculationPointsAutomaticallyCommand());
    eventBus.fireEvent(new CloseCalculationPointAutomaticPlacement());
  }

  @JsMethod
  public void clickAccept() {
    eventBus.fireEvent(new CalculationPointsAddCommand(placementContext.getPlacementResult().getComputedCalculationPoints()));
    eventBus.fireEvent(new ResetCalculationPointsAutomaticallyCommand());
    eventBus.fireEvent(new CloseCalculationPointAutomaticPlacement());
  }

  @Computed("messageClass")
  public String messageClass() {
    final PlacementStatus ps = placementContext.getPlacementStatus();
    return ps == PlacementStatus.INFO ? "notice" : (ps == PlacementStatus.WARN ? "warning" : null);
  }

  @Computed("message")
  public String getMessage() {
    return placementContext.getPlacementMessage();
  }

  @Computed("isRadiusInvalid")
  public boolean isRadiusInvalid() {
    return pointsInland && (inlandRadius < MIN_INLAND_RADIUS || inlandRadius > getMaxInlandRadius() || inlandRadius != Math.round(inlandRadius));
  }

  @JsMethod
  protected String getInlandRadiusValidationError() {
    return i18n.ivIntegerRangeBetween(M.messages().calculationPointsPlacementInlandRadius(), MIN_INLAND_RADIUS, getMaxInlandRadius());
  }

  @Computed("maxInlandRadius")
  public Integer getMaxInlandRadius() {
    return applicationContext.getSetting(SharedConstantsEnum.DEFAULT_CALCULATION_POINTS_PLACEMENT_RADIUS);
  }

  @Watch(value = "pointsAbroad")
  public void resetOnPointsAbroad() {
    reset();
  }

  @Watch(value = "pointsInland")
  public void resetOnPointsInland() {
    reset();
  }

  @Watch(value = "inlandRadius")
  public void resetOnInlandRadius() {
    reset();
  }

  @Watch(value = "selectedSituation")
  public void resetOnSelectedSituation() {
    reset();
  }

  @Override
  public void mounted() {
    pointsAbroad = showAbroadOption();
    pointsInland = !showAbroadOption();
  }
}
