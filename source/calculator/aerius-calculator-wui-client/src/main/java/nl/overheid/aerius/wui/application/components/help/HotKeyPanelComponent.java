/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.help;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasDestroyed;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import elemental2.dom.HTMLDialogElement;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.directives.VectorDirective;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.wui.application.command.misc.DisplayHotkeysCommand;
import nl.overheid.aerius.wui.application.command.misc.ToggleHotkeysCommand;
import nl.overheid.aerius.wui.application.command.misc.ToggleHotkeysEnabledCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.daemon.misc.HotkeyContext;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    HotKeyLineComponent.class,
}, directives = {
    VectorDirective.class
})
public class HotKeyPanelComponent extends BasicVueComponent implements IsVueComponent, HasCreated, HasMounted, HasDestroyed {
  private static final HelpPanelEventBinder EVENT_BINDER = GWT.create(HelpPanelEventBinder.class);

  interface HelpPanelEventBinder extends EventBinder<HotKeyPanelComponent> {}

  @Inject @Data EnvironmentConfiguration cfg;
  @Inject @Data ApplicationContext context;
  @Inject @Data HotkeyContext hotkeyContext;

  @Ref HTMLDialogElement modal;

  @Prop EventBus eventBus;

  @Data boolean displayHelp;

  private HandlerRegistration handlers;

  @EventHandler
  public void onToggleHelpCommand(final ToggleHotkeysCommand c) {
    displayHelp = !displayHelp;
  }

  @EventHandler
  public void onDisplayHotkeysCommand(final DisplayHotkeysCommand c) {
    displayHelp = c.getValue();
  }

  @JsMethod
  public void toggleHotkeysEnabled() {
    eventBus.fireEvent(new ToggleHotkeysEnabledCommand());
  }

  @JsMethod
  public void closeHelp() {
    displayHelp = false;
  }

  @Watch("displayHelp")
  public void onDisplayHelpChange() {
    if (displayHelp) {
      modal.showModal();
    } else {
      modal.close();
    }
  }

  @Override
  public void created() {
    handlers = EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Override
  public void mounted() {
    modal.addEventListener("close", e -> {
      displayHelp = false;
    });
  }

  @Computed
  public String getApplicationVersion() {
    return cfg.getApplicationVersion();
  }

  @Computed
  public String getDatabaseVersion() {
    return context.getConfiguration().getSettings().getSetting(SharedConstantsEnum.DATABASE_VERSION);
  }

  @Override
  public void destroyed() {
    if (handlers != null) {
      handlers.removeHandler();
    }
  }
}
