/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.calculate.detail;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.backgrounddata.MetSite;
import nl.overheid.aerius.wui.application.components.divider.DividerComponent;
import nl.overheid.aerius.wui.application.components.source.DetailStatComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.domain.calculation.ADMSOptions;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    DividerComponent.class,
    DetailStatComponent.class
})
public class CalculationJobDetailMeteorologicalSettingsView extends BasicVueComponent {
  private static final String DATASET_TYPE_OBS = "OBS";
  private static final String DATASET_TYPE_NWP = "NWP";

  @Inject @Data ApplicationContext applicationContext;

  @Prop CalculationSetOptions calculationSetOptions;
  @Prop SituationComposition situationComposition;
  @Prop Theme theme;

  @Computed("metSiteType")
  public String getMetSiteType() {
    final String datasetType = calculationSetOptions.getNcaCalculationOptions().getAdmsOptions().getMetDatasetType().isNwp()
        ? DATASET_TYPE_NWP
        : DATASET_TYPE_OBS;

    return M.messages().calculateMetDatasetTypeFlowOption(datasetType);
  }

  @Computed("metSiteDataType")
  public String getMetSiteDataType() {
    return M.messages().calculateMetDatasetType(
        calculationSetOptions.getNcaCalculationOptions().getAdmsOptions().getMetDatasetType().name());
  }

  @Computed("metSiteLocation")
  public String getMetSiteLocation() {
    final int metSiteId = calculationSetOptions.getNcaCalculationOptions().getAdmsOptions().getMetSiteId();

    return getSiteLocations().stream()
        .filter(v -> v.getId() == metSiteId)
        .map(CalculationJobDetailMeteorologicalSettingsView::formatMetSite)
        .findFirst()
        .orElse(null);
  }

  private List<MetSite> getSiteLocations() {
    final ADMSOptions admsOptions = calculationSetOptions.getNcaCalculationOptions().getAdmsOptions();
    return applicationContext.getConfiguration().getMetSites()
        .stream()
        .filter(v -> v.getDatasets().stream()
            .anyMatch(dataset -> dataset.getDatasetType() == admsOptions.getMetDatasetType()))
        .collect(Collectors.toList());
  }

  private static String formatMetSite(final MetSite site) {
    return site.getName();
  }

  @Computed("metYears")
  public String getMetYearsString() {
    return calculationSetOptions.getNcaCalculationOptions().getAdmsOptions().getMetYears().stream()
        .sorted()
        .collect(Collectors.joining(", "));
  }
}
