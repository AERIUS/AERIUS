/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.info;

import static jsinterop.annotations.JsPackage.GLOBAL;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.domain.result.EmissionResults;

@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class StaticReceptorInfo {

  private @JsProperty Natura2000Info[] naturaInfo;
  private @JsProperty HabitatInfo[] habitatTypeInfo;
  private @JsProperty String[] hexagonTypes;
  private @JsProperty EmissionResults backgroundEmissionResults;

  public final @JsOverlay Natura2000Info[] getNaturaInfo() {
    return naturaInfo;
  }

  public final @JsOverlay List<HabitatInfo> getHabitatTypeInfo() {
    return Arrays.asList(habitatTypeInfo);
  }

  public final @JsOverlay SummaryHexagonType[] getHexagonTypes() {
    return Stream.of(hexagonTypes).map(v -> SummaryHexagonType.valueOf(v)).collect(Collectors.toList()).toArray(new SummaryHexagonType[0]);
  }

  public final @JsOverlay EmissionResults getBackgroundEmissionResults() {
    return backgroundEmissionResults;
  }
}
