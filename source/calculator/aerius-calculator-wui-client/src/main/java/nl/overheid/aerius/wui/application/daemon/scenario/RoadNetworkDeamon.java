/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.scenario;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.wui.application.command.source.DeselectAllFeaturesCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceToggleHighlightCommand;
import nl.overheid.aerius.wui.application.context.RoadNetworkContext;

@Singleton
public class RoadNetworkDeamon extends BasicEventComponent implements Daemon {

  private static final RoadNetworkDeamonEventBinder EVENT_BINDER = GWT.create(RoadNetworkDeamonEventBinder.class);

  interface RoadNetworkDeamonEventBinder extends EventBinder<RoadNetworkDeamon> {}

  @Inject private RoadNetworkContext roadNetworkContext;

  @EventHandler
  public void onEmissionSourceToggleHighlightCommand(final EmissionSourceToggleHighlightCommand c) {
    roadNetworkContext.toggleHighlight(c.getValue());
  }

  @EventHandler
  public void onDeselectAllFeaturesCommand(final DeselectAllFeaturesCommand c) {
    roadNetworkContext.reset();
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }

}
