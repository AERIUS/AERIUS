/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.combustion;

import java.util.Arrays;
import java.util.List;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.source.combustion.CombustionPlantValidators.CombustionPlantValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.domain.source.combustion.CombustionPlantActivity;
import nl.overheid.aerius.wui.application.domain.source.combustion.CombustionPlantESFeature;
import nl.overheid.aerius.wui.vue.DebugDirective;

@Component(customizeOptions = {
    CombustionPlantValidators.class
}, directives = {
    ValidateDirective.class,
    DebugDirective.class
}, components = {
    LabeledInputComponent.class,
    ValidationBehaviour.class,
    SimplifiedListBoxComponent.class,
})
public class CombustionPlantDetailEditorComponent extends ErrorWarningValidator
    implements HasCreated, HasValidators<CombustionPlantValidations> {
  @Prop CombustionPlantESFeature source;
  @Prop CombustionPlantActivity subSource;

  @Data String descriptionV;
  @Data String startDateV;
  @Data String operatingHoursV;
  @Data String thermalInputV;
  @Data String applicableELVV;

  @JsProperty(name = "$v") CombustionPlantValidations validation;

  @Watch(value = "subSource", isImmediate = true)
  public void onStandardSubSourceChange() {
    descriptionV = String.valueOf(subSource.getDescription());
    startDateV = String.valueOf(subSource.getStartDate());
    operatingHoursV = String.valueOf(subSource.getOperatingHours());
    thermalInputV = String.valueOf(subSource.getThermalInput());
    applicableELVV = String.valueOf(subSource.getApplicableELV());
  }

  @Computed
  public List<String> getPlantTypes() {
    return Arrays.asList("Boiler", "Engine", "Gas turbine");
  }

  @Computed
  public String getPlantType() {
    return subSource.getPlantType();
  }

  @JsMethod
  public void selectPlantType(final String plantType) {
    subSource.setPlantType(plantType);
  }

  @Computed
  public List<String> getFuelTypes() {
    return Arrays.asList("Solid biomass", "Other solid fuels", "Gas oil", "Liquid fuels other than gas oil", "Natural gas",
        "Gaseous fuels other than natural gas", "Combination of 2 or more fuel types");
  }

  @Computed
  public String getFuelType() {
    return subSource.getFuelType();
  }

  @JsMethod
  public void selectFuelType(final String fuelType) {
    subSource.setFuelType(fuelType);
  }

  @JsMethod
  public boolean isSourceAvailable() {
    return subSource != null;
  }

  @Computed
  protected String getDescription() {
    return subSource.getDescription();
  }

  @Computed
  protected void setDescription(final String description) {
    subSource.setDescription(description);
    this.descriptionV = description;
  }

  @JsMethod
  protected String descriptionError() {
    return i18n.errorDescriptionRequired();
  }

  @Computed
  protected String getStartDate() {
    return startDateV;
  }

  @Computed
  protected void setStartDate(final String startDate) {
    subSource.setStartDate(startDate);
    this.startDateV = startDate;
  }

  @Computed
  protected String getOperatingHours() {
    return operatingHoursV;
  }

  @Computed
  protected void setOperatingHours(final String value) {
    this.operatingHoursV = value;
    ValidationUtil.setSafeIntegerValue(v -> subSource.setOperatingHours(v), value, 0);
  }

  @Computed
  protected String getThermalInput() {
    return thermalInputV;
  }

  @Computed
  protected void setThermalInput(final String value) {
    this.thermalInputV = value;
    ValidationUtil.setSafeDoubleValue(v -> subSource.setThermalInput(v), value, 0D);
  }

  @Computed
  protected String getApplicableELV() {
    return applicableELVV;
  }

  @Computed
  protected void setApplicableELV(final String value) {
    this.applicableELVV = value;
    ValidationUtil.setSafeIntegerValue(v -> subSource.setApplicableELV(v), value, 0);
  }

  @Override
  @Computed
  public CombustionPlantValidations getV() {
    return validation;
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }
}
