/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers;

import ol.OLFactory;
import ol.style.Circle;
import ol.style.Fill;
import ol.style.Stroke;
import ol.style.Style;

/**
 * Provides the default styling for features in AERIUS
 * Some feature types require special styling, but this style can be used as a default
 * for emission sources, building features, ...
 */
public final class FeatureStyle {

  private static final Stroke WHITE_STROKE = OLFactory.createStroke(OLFactory.createColor(255, 255, 255, 1), 1);
  private static final Stroke GREY_STROKE = OLFactory.createStroke(OLFactory.createColor(0, 0, 0, 0.5), 2);
  private static final Stroke BLACK_STROKE = OLFactory.createStroke(OLFactory.createColor(0, 0, 0, 1), 2);

  private static final Fill WHITE_25_FILL = OLFactory.createFill(OLFactory.createColor(255, 255, 255, 0.25));
  private static final Fill WHITE_50_FILL = OLFactory.createFill(OLFactory.createColor(255, 255, 255, 0.5));
  public static final Fill BLACK_FILL = OLFactory.createFill(OLFactory.createColor(0, 0, 0, 1));

  private static final Stroke EDIT_STYLE_STROKE = OLFactory.createStroke(OLFactory.createColor(21, 124, 177, 1), 2);

  /**
   * Default styling for features in AERIUS
   * Consists of a white 50% fill and a black 2px border
   */
  public static final Style DEFAULT_STYLE = OLFactory.createStyle(WHITE_50_FILL, BLACK_STROKE);
  public static final Style[] DEFAULT_STYLING = new Style[] {DEFAULT_STYLE};

  public static final double POINT_COLOR_RADIUS = 2.0;
  public static final double POINT_RADIUS = 1.5;
  private static final Circle POINT_CIRCLE = OLFactory.createCircleStyle(BLACK_FILL, WHITE_STROKE,
      POINT_COLOR_RADIUS + POINT_RADIUS);
  public static final Style POINT_STYLE = OLFactory.createStyle(POINT_CIRCLE);

  public static final Style OBSOLETE_STYLE = OLFactory.createStyle(WHITE_25_FILL, GREY_STROKE);
  public static final Style[] OBSOLETE_STYLING = new Style[] {OBSOLETE_STYLE};

  public static final Style EDIT_STYLE = OLFactory.createStyle(WHITE_50_FILL, EDIT_STYLE_STROKE);
  public static final Style[] EDIT_STYLING = new Style[] {EDIT_STYLE};

  private FeatureStyle() {
  }

}
