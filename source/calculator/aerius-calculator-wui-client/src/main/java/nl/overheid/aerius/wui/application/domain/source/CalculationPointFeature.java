/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.domain.source;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.overheid.aerius.shared.domain.v2.point.CalculationPointType;
import nl.overheid.aerius.wui.application.domain.ImaerFeature;
import nl.overheid.aerius.wui.vue.ReactivityUtil;

@JsType(namespace = GLOBAL, name = "ol.Feature", isNative = true)
public class CalculationPointFeature extends ImaerFeature {
  public static final @JsOverlay CalculationPointFeature create() {
    final CalculationPointFeature feature = new CalculationPointFeature();
    initCalculationPointFeature(feature, CalculationPointType.CUSTOM_CALCULATION_POINT);
    return feature;
  }

  public static final @JsOverlay void initCalculationPointFeature(final CalculationPointFeature feature, final CalculationPointType type) {
    ImaerFeature.initImaer(feature);
    feature.setCalculationPointType(type);
    ReactivityUtil.ensureJsProperty(feature.getProperties(), "customPointId", feature::setCustomPointId, 0);
    ReactivityUtil.ensureDefault(feature::getResults, feature::setResults);
  }

  public final @JsOverlay int getCustomPointId() {
    return Js.coerceToInt(get("customPointId"));
  }

  public final @JsOverlay void setCustomPointId(final int customPointId) {
    setId(String.valueOf(customPointId));
    set("customPointId", Double.valueOf(customPointId));
  }

  public final @JsOverlay CalculationPointType getCalculationPointType() {
    return CalculationPointType.safeValueOf(get("calculationPointType"));
  }

  public final @JsOverlay void setCalculationPointType(final CalculationPointType calculationPointType) {
    set("calculationPointType", calculationPointType.getName());
  }

  public final @JsOverlay Object getResults() {
    return get("results");
  }

  public final @JsOverlay void setResults(final Object results) {
    set("results", results);
  }
}
