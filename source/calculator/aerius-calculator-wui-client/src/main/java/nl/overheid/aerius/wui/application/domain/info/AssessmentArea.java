/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.info;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class AssessmentArea {

  private int id;
  private String name;
  private String directiveCodes;
  private BBox bounds;

  public final @JsOverlay int getId() {
    return id;
  }

  public final @JsOverlay String getName() {
    return name;
  }

  public final @JsOverlay String getDirectiveCodes() {
    return directiveCodes;
  }

  public final @JsOverlay BBox getBounds() {
    return bounds;
  }

  public final @JsOverlay String getTitle(final boolean showDirectives) {
    if (directiveCodes == null || !showDirectives) {
      return name;
    } else {
      return name + " (" + directiveCodes + ")";
    }
  }
}
