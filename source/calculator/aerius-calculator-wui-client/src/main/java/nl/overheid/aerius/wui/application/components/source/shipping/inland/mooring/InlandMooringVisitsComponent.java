/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.shipping.inland.mooring;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.inland.mooring.InlandMooringVisitsValidators.InlandMooringVisitsValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.domain.source.shipping.inland.MooringInlandShipping;
import nl.overheid.aerius.wui.vue.DebugDirective;

@Component(name = "aer-inland-mooring-visits",
customizeOptions = {
    InlandMooringVisitsValidators.class
}, directives = {
    ValidateDirective.class,
    DebugDirective.class
}, components = {
    LabeledInputComponent.class,
    ValidationBehaviour.class
})
public class InlandMooringVisitsComponent extends ErrorWarningValidator implements HasCreated,
  HasValidators<InlandMooringVisitsValidations> {
  @Prop @JsProperty MooringInlandShipping mooringInlandShipping;
  @Data String inlandVisitsV;

  @JsProperty(name = "$v") InlandMooringVisitsValidations validation;

  @Computed
  public InlandMooringVisitsValidations getV() {
    return validation;
  }

  @Watch(value = "mooringInlandShipping", isImmediate = true)
  public void onMooringInlandShippingChange() {
    inlandVisitsV = Integer.toString(mooringInlandShipping.getShipsPerTimeUnit());
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Computed
  protected String getTimeUnit() {
    return mooringInlandShipping.getTimeUnit().name();
  }

  @Computed
  protected void setTimeUnit(final String timeUnit) {
    mooringInlandShipping.setTimeUnit(TimeUnit.valueOf(timeUnit));
  }

  @Computed
  protected void setInlandVisits(final String inlandVisits) {
    ValidationUtil.setSafeIntegerValue(mooringInlandShipping::setShipsPerTimeUnit, inlandVisits, 0);
    inlandVisitsV = inlandVisits;
  }

  @Computed
  protected String getInlandVisits() {
    inlandVisitsV = Integer.toString(mooringInlandShipping.getShipsPerTimeUnit());
    return inlandVisitsV;
  }

  @Computed("isInvalid")
  protected boolean isInvalid() {
    return validation.inlandVisitsV.invalid;
  }

  @JsMethod
  public String inlandVisitsError() {
    return i18n.errorNotValidEntry(inlandVisitsV);
  }
}
