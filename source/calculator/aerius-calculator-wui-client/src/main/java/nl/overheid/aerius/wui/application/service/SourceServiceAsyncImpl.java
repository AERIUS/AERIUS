/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.service;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Singleton;

import ol.format.GeoJson;
import ol.format.GeoJsonFeatureOptions;
import ol.geom.Geometry;

import elemental2.core.JsArray;

import nl.aerius.wui.service.ForwardedAsyncCallback;
import nl.aerius.wui.util.RequestUtil;
import nl.overheid.aerius.shared.RequestMappings;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.shipping.inland.InlandWaterway;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;

@Singleton
public class SourceServiceAsyncImpl implements SourceServiceAsync {

  @Inject EnvironmentConfiguration cfg;
  @Inject HeaderHelper hdr;

  private final GeoJson geojson = new GeoJson();

  @Override
  public void refreshEmissions(final List<EmissionSourceFeature> sources, final int year,
      final AsyncCallback<List<EmissionSourceFeature>> callback) {
    final String requestUrl = RequestUtil.prepareUrl(cfg.getConnectBaseUrl(), RequestMappings.CONNECT_UI_REFRESH_EMISSIONS);
    final String sourceString = geojson.writeFeatures(sources.toArray(new EmissionSourceFeature[sources.size()]), new GeoJsonFeatureOptions());

    final String refreshEmissionsRequest = "{"
        + "\"sources\": " + sourceString + ","
        + "\"year\": " + year
        + "}";

    RequestUtil.doPost(requestUrl, refreshEmissionsRequest, hdr.defaultHeaders(),
        new ForwardedAsyncCallback<List<EmissionSourceFeature>, String>(callback) {

          @Override
          public List<EmissionSourceFeature> convert(final String content) {
            return Arrays.asList((EmissionSourceFeature[]) geojson.readFeatures(content, new GeoJsonFeatureOptions()));
          }

        });
  }

  @Override
  public void suggestInlandShippingWaterway(final Geometry geometry, final AsyncCallback<JsArray<InlandWaterway>> callback) {
    final String requestUrl = RequestUtil.prepareUrl(cfg.getConnectBaseUrl(), RequestMappings.CONNECT_UI_SUGGEST_INLAND_SHIPPING_WATERWAY);
    final String suggestInlandShippingWaterwayRequest = geojson.writeGeometry(geometry, null);

    RequestUtil.doPost(requestUrl, suggestInlandShippingWaterwayRequest, hdr.defaultHeaders(), AeriusRequestCallback.createAsync(callback));
  }

}
