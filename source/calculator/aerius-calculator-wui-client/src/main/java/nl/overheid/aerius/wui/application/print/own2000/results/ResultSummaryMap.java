/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.print.own2000.results;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import jsinterop.annotations.JsProperty;
import jsinterop.base.Js;

import nl.aerius.geo.command.LayerHiddenCommand;
import nl.aerius.geo.command.RenderSyncCommand;
import nl.aerius.geo.daemon.LayerContext;
import nl.aerius.geo.domain.LayerInfo;
import nl.aerius.geo.domain.legend.Legend;
import nl.aerius.geo.wui.OL3MapComponent;
import nl.overheid.aerius.shared.domain.result.CalculationMarker;
import nl.overheid.aerius.shared.domain.result.ResultView;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.command.result.ChangeSituationResultKeyCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.MapConfigurationContext;
import nl.overheid.aerius.wui.application.context.PrintContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.daemon.geo.ScenarioLayerContext;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.application.event.RestoreCalculationJobContextEvent;
import nl.overheid.aerius.wui.application.event.ResultsViewSelectionCommand;
import nl.overheid.aerius.wui.application.event.SituationResultsSelectedEvent;
import nl.overheid.aerius.wui.application.geo.layers.LegendPair;
import nl.overheid.aerius.wui.application.geo.layers.NatureAreasLayer;
import nl.overheid.aerius.wui.application.geo.layers.results.CalculationMarkers;
import nl.overheid.aerius.wui.application.geo.layers.results.CalculationResultLayer;
import nl.overheid.aerius.wui.application.print.CompleteTracker;
import nl.overheid.aerius.wui.application.util.ResultStatisticUtil;
import nl.overheid.aerius.wui.vue.BasicVueEventComponent;

@Component(components = {
    OL3MapComponent.class,
    ColorLabelsLegendPrintComponent.class
})
public class ResultSummaryMap extends BasicVueEventComponent implements HasMounted {
  private static final ResultSummaryMapEventBinder EVENT_BINDER = GWT.create(ResultSummaryMapEventBinder.class);

  interface ResultSummaryMapEventBinder extends EventBinder<ResultSummaryMap> {}

  @Prop EventBus eventBus;

  @SuppressWarnings("rawtypes") @Data @JsProperty List<LegendPair> markerLegend = new ArrayList<>();

  @Inject LayerContext layerContext;
  @Inject MapConfigurationContext mapConfigurationContext;
  @Inject ApplicationContext appContext;
  @Inject @Data PrintContext printOwN2000Context;
  @Inject ScenarioContext scenarioContext;
  @Inject ScenarioLayerContext scenarioLayerContext;
  @Inject CalculationContext calculationContext;
  @Inject CompleteTracker completeTracker;

  @Ref OL3MapComponent map;

  private SituationResultsKey proposedSituationResultsKey;
  private CompleteTracker.Task task;

  @Override
  public void created() {
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Override
  public void mounted() {
    mapConfigurationContext.setIconScale(0.7);
    map.attach();
    task = completeTracker.createTask();
  }

  @Computed
  public Legend getLegend() {
    return layerContext.getLayerItems().stream()
        .filter(layerItem -> layerItem.getLayer().getInfoOptional()
            .map(info -> info.getName().equals(NatureAreasLayer.class.getCanonicalName()))
            .orElse(false))
        .findAny()
        .flatMap(layerItem -> layerItem.getLayer().getInfoOptional())
        .map(LayerInfo::getLegend)
        .orElse(null);
  }

  @EventHandler
  public void onRestoreCalculationContextEvent(final RestoreCalculationJobContextEvent e) {
    // Ensure the markers are shown on the map
    eventBus.fireEvent(new ResultsViewSelectionCommand(ResultView.MARKERS));

    // Select proposed situation results for OwN2000 project calculation PDF
    proposedSituationResultsKey = new SituationResultsKey(
        SituationContext.handleFromSituation(scenarioContext.getActiveSituation()),
        printOwN2000Context.getScenarioResultType(),
        printOwN2000Context.getEmissionResultKey(),
        printOwN2000Context.getSummaryHexagonType(),
        printOwN2000Context.getOverlappingHexagonType(),
        printOwN2000Context.getProcurementPolicy());
    eventBus.fireEvent(new ChangeSituationResultKeyCommand(proposedSituationResultsKey));
  }

  @EventHandler
  public void onSituationResultsSelectedEvent(final SituationResultsSelectedEvent e) {
    if (e.getValue().equals(proposedSituationResultsKey)) {
      eventBus.fireEvent(new PrintViewZoomCommand(proposedSituationResultsKey));

      final List<ResultStatisticType> statTypes = ResultStatisticUtil.getStatisticsForPdfResult(appContext.getTheme(),
          printOwN2000Context.getScenarioResultType(), SummaryHexagonType.RELEVANT_HEXAGONS, printOwN2000Context.getPdfProductType(),
          printOwN2000Context.getEmissionResultKey());

      final Set<CalculationMarker> calculationMarkers = CalculationMarker.getCalculationMarkers(
          appContext.getConfiguration().getResultViews(), proposedSituationResultsKey.getResultType(),
          statTypes, printOwN2000Context.getPdfProductType());

      markerLegend = Js.uncheckedCast(CalculationMarkers.createMarkerLegend(calculationMarkers, printOwN2000Context.getScenarioResultType(),
          appContext.getTheme()));
    }

    // Hide the calculation results layer every time, because selecting results also changes visibility of layers.
    layerContext.getLayerItems().stream()
        .filter(layerItem -> layerItem.getLayer().getInfoOptional()
            .map(info -> info.getName().equals(CalculationResultLayer.class.getCanonicalName()))
            .orElse(false))
        .forEach(calculationResultsLayerItem -> eventBus.fireEvent(new LayerHiddenCommand(calculationResultsLayerItem.getLayer())));
  }

  @EventHandler
  public void on(final PrintViewZoomEvent e) {
    if (e.getValue().equals(proposedSituationResultsKey) && !task.isComplete()) {
      eventBus.fireEvent(new RenderSyncCommand(() -> task.complete()));
    }
  }
}
