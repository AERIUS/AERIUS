/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.road;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.wui.application.domain.source.EmissionFactors;
import nl.overheid.aerius.wui.vue.ReactivityUtil;
import nl.overheid.aerius.wui.vue.SkipReactivityValidations;

/**
 * Client side implementation of CustomVehicles props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class CustomVehicles extends Vehicles {

  private String description;
  private double vehiclesPerTimeUnit;
  private String vehicleCode;
  private EmissionFactors emissionFactors;

  public static final @JsOverlay CustomVehicles create() {
    final CustomVehicles props = Js.uncheckedCast(JsPropertyMap.of());
    init(props);
    return props;
  }

  public static final @JsOverlay void init(final CustomVehicles props) {
    Vehicles.initVehicles(props, VehicleType.CUSTOM);

    ReactivityUtil.ensureJsProperty(props, "description", props::setDescription, "");
    ReactivityUtil.ensureJsProperty(props, "vehicleCode", props::setVehicleCode, "");
    ReactivityUtil.ensureJsProperty(props, "vehiclesPerTimeUnit", props::setVehiclesPerTimeUnit, 0D);
    ReactivityUtil.ensureJsProperty(props, "emissionFactors", props::setEmissionFactors, EmissionFactors.create());
    ReactivityUtil.ensureInitialized(props::getEmissionFactors, EmissionFactors::init);
  }

  public final @JsOverlay String getDescription() {
    return description;
  }

  public final @JsOverlay void setDescription(final String description) {
    this.description = description;
  }

  public final @JsOverlay String getVehicleCode() {
    return vehicleCode;
  }

  public final @JsOverlay void setVehicleCode(final String vehicleType) {
    this.vehicleCode = vehicleType;
  }

  public final @JsOverlay double getVehiclesPerTimeUnit() {
    return vehiclesPerTimeUnit;
  }

  public final @JsOverlay void setVehiclesPerTimeUnit(final double vehiclesPerTimeUnit) {
    this.vehiclesPerTimeUnit = vehiclesPerTimeUnit;
  }

  @SkipReactivityValidations
  public final @JsOverlay double getVehiclesPer(final TimeUnit targetTimeUnit) {
    return this.getTimeUnit() == targetTimeUnit ? vehiclesPerTimeUnit : getTimeUnit().toUnit(vehiclesPerTimeUnit, targetTimeUnit);
  }

  @SkipReactivityValidations
  public final @JsOverlay double getEmissionFactor(final Substance substance) {
    return emissionFactors.getEmissionFactor(substance);
  }

  public final @JsOverlay void setEmissionFactor(final Substance substance, final double emission) {
    emissionFactors.setEmissionFactor(substance, emission);
  }

  public final @JsOverlay void setEmissionFactors(final EmissionFactors emissionFactors) {
    this.emissionFactors = emissionFactors;
  }

  public final @JsOverlay EmissionFactors getEmissionFactors() {
    return emissionFactors;
  }

}
