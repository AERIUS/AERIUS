/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.source.farmlodging;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Emit;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategories;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.farm.CustomFarmLodging;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmLodging;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmLodgingType;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmLodgingUtil;
import nl.overheid.aerius.wui.application.domain.source.farm.StandardFarmLodging;
import nl.overheid.aerius.wui.application.resources.util.FarmIconUtil;
import nl.overheid.aerius.wui.application.resources.util.FarmIconUtil.AdditionalIcon;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component
public class FarmLodgingEmissionEditorRowComponent extends BasicVueComponent {
  @Prop @JsProperty FarmLodging source;

  @Prop boolean selected;

  @Inject @Data ApplicationContext applicationContext;

  @JsMethod
  @Emit
  public void select() {}

  @Computed
  public String getIcon() {
    return FarmIconUtil.getFarmLodgingIcon(source);
  }

  @Computed("additionalIcon")
  public AdditionalIcon additionalIcon() {
    return FarmIconUtil.getFarmLodgingAdditionalIcon(source, getCategories());
  }

  @JsMethod
  public FarmLodgingCategories getCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getFarmLodgingCategories();
  }

  @Computed
  public int getNumberOfAnimals() {
    return source.getNumberOfAnimals();
  }

  @Computed
  public String getDescription() {
    if (hasStandardEmissions()) {
      return ((StandardFarmLodging) source).getFarmLodgingCode();
    } else {
      return ((CustomFarmLodging) source).getDescription();
    }
  }

  @Computed
  public double getEmission() {
    return FarmLodgingUtil.getEmission(getCategories(), source);
  }

  private boolean hasStandardEmissions() {
    return source.getFarmLodgingType() == FarmLodgingType.STANDARD;
  }
}
