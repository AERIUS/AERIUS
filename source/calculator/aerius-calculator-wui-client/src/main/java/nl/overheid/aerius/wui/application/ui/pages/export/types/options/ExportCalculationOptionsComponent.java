/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.export.types.options;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ExportContext;
import nl.overheid.aerius.wui.application.ui.nca.pages.calculate.options.CalculationJobMeteorologicalSettingsComponent;

@Component(components = {
    LabeledInputComponent.class,
    SimplifiedListBoxComponent.class,
    CalculationJobMeteorologicalSettingsComponent.class,
    ValidationBehaviour.class,
})
public class ExportCalculationOptionsComponent extends ErrorWarningValidator {
  @Inject @Data ApplicationContext applicationContext;
  @Inject @Data ExportContext exportContext;

  @Computed("isNCA")
  public boolean isNCA() {
    return applicationContext.getTheme() == Theme.NCA;
  }

  @Computed
  public List<CalculationMethod> getCalculationMethods() {
    return applicationContext.getConfiguration().getCalculationMethods();
  }

  @JsMethod
  public void selectCalculationMethod(final CalculationMethod calculationMethod) {
    exportContext.getCalculationOptions().setCalculationMethod(calculationMethod);
  }

  @Computed("calculationMethod")
  public CalculationMethod getCalculationMethod() {
    return exportContext.getCalculationOptions().getCalculationMethod();
  }
}
