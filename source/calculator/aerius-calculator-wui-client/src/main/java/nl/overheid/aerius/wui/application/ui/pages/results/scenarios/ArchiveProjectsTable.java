/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.results.scenarios;

import java.util.Arrays;
import java.util.Date;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.wui.application.components.common.StyledPointComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.domain.calculation.ArchiveMetaData;
import nl.overheid.aerius.wui.application.domain.calculation.ArchiveProject;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    StyledPointComponent.class
})
public class ArchiveProjectsTable extends BasicVueComponent {

  private static final int REGISTER_PROJECTS_LINK_LIMIT = 35;
  private static final String REGISTER_PROJECTS_LINK_PATH = "/archive?projectIds=";

  @Prop EventBus eventBus;

  @Data @Inject CalculationContext calculationContext;
  @Data @Inject ApplicationContext appContext;

  @Computed
  public CalculationExecutionContext getCalculationJob() {
    return calculationContext.getActiveCalculation();
  }

  @Computed
  public ArchiveMetaData getMetaData() {
    return getCalculationJob().getCalculationInfo().getArchiveMetaData();
  }

  @Computed
  public String getRetrievalDateTime() {
    final Date date = DateTimeFormat.getFormat(DateTimeFormat.PredefinedFormat.ISO_8601).parse(getMetaData().getRetrievalDateTime());
    return M.formatDateTime(date);
  }

  @Computed
  public ArchiveProject[] getArchiveProjects() {
    return getMetaData().getArchiveProjects();
  }

  @JsMethod
  public String displayId(final ArchiveProject archiveProject) {
    return getMetaData().determineDisplayId(archiveProject);
  }

  @JsMethod
  public String formatNullableString(final String value) {
    if (value == null || value.length() == 0) {
      return "-";
    } else {
      return value;
    }
  }

  @JsMethod
  public String formatEmission(final double emission) {
    return emission > 0 ? MessageFormatter.formatEmissionWithUnitSmart(emission) : "-";
  }

  @JsMethod
  public boolean hasRegisterLink() {
    return appContext.getConfiguration().hasRegister() && getMetaData().getArchiveProjects().length > 0;
  }

  @JsMethod
  public String getRegisterLink() {
    final String registerBaseUrl = appContext.getConfiguration().getRegisterBaseUrl();
    final String projectIds = Arrays.stream(getMetaData().getArchiveProjects())
        .map(ArchiveProject::getId)
        .limit(REGISTER_PROJECTS_LINK_LIMIT)
        .collect(Collectors.joining(","));
    return registerBaseUrl + REGISTER_PROJECTS_LINK_PATH + projectIds;
  }
}
