/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.nca.pages.timevarying;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfileType;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileEditCancelCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileEditSaveCommand;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.InputWarningComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.modify.ModifyCancelSaveComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.toggle.ToggleButtons;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.TimeVaryingProfileEditorContext;
import nl.overheid.aerius.wui.application.domain.source.TimeVaryingProfile;
import nl.overheid.aerius.wui.application.domain.source.util.TimeVaryingProfileUtil;
import nl.overheid.aerius.wui.application.domain.source.util.TimeVaryingProfileUtilBase;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.util.NumberUtil;

@Component(components = {
    CollapsiblePanel.class,
    ModifyCancelSaveComponent.class,
    LabeledInputComponent.class,
    ToggleButtons.class,
    ThreeDayTableInputComponent.class,
    MonthlyTableInputComponent.class,
    TimeVaryingRawInputComponent.class,
    InputErrorComponent.class,
    InputWarningComponent.class,
})
public class TimeVaryingProfileView extends ErrorWarningValidator {
  private static final double DIURNAL_TOLERANCE = 0.5;
  private static final double MONTHLY_TOLERANCE = 0.1;

  public static enum InputMode {
    RAW_CSV, TABLE
  }

  @Prop EventBus eventBus;

  @Inject @Data ApplicationContext appContext;
  @Inject @Data ScenarioContext scenarioContext;
  @Inject @Data TimeVaryingProfileEditorContext context;

  @Data boolean confirmState;

  @Data InputMode selectedInputMode = InputMode.TABLE;

  @Computed
  public List<InputMode> getInputModes() {
    return Arrays.asList(InputMode.values());
  }

  @JsMethod
  public void setToggle(final InputMode mode) {
    selectedInputMode = mode;
  }

  @Computed
  public String getTimeVaryingProfileTitle() {
    final TimeVaryingProfile profile = getTimeVaryingProfile();
    if (profile.isSystemProfile()) {
      return profile.getLabel();
    }
    final TimeVaryingProfile match = scenarioContext.getActiveSituation().findTimeVaryingProfile(profile.getGmlId());

    final String type = i18n.timeVaryingProfileTitle(profile.getType());
    return match == null ? i18n.esButtonNewSource(type) : i18n.timeVaryingProfileTitleEdit(type);
  }

  @Computed
  public TimeVaryingProfile getTimeVaryingProfile() {
    return context.getTimeVaryingProfile();
  }

  @Computed("isTimeVaryingProfileAvailable")
  public boolean isTimeVaryingProfileAvailable() {
    return context.isEditing();
  }

  @Computed
  public CustomTimeVaryingProfileType getProfileType() {
    return getTimeVaryingProfile().getType();
  }

  @Computed
  public String getLabel() {
    return getTimeVaryingProfile().getLabel();
  }

  @Computed
  public void setLabel(final String label) {
    getTimeVaryingProfile().setLabel(label);
  }

  @JsMethod
  public void cancel() {
    // If editing system profile (which means only in view mode, always allow cancel)
    if (getTimeVaryingProfile().isSystemProfile() || Window.confirm(M.messages().timeVaryingProfileMayCancel())) {
      eventBus.fireEvent(new TimeVaryingProfileEditCancelCommand());
    }
  }

  @JsMethod
  public void save() {
    eventBus.fireEvent(new TimeVaryingProfileEditSaveCommand(context.getTimeVaryingProfile()));
  }

  @JsMethod
  public void setConfirmState(final boolean confirmState) {
    this.confirmState = confirmState;
  }

  @Computed
  public String getTotalErrorMessage() {
    return M.messages().timeVaryingProfileTotalSummaryError(util().getValuesTotalPreciseText(getTimeVaryingProfile().getValues()),
        util().getValuesMaxText());
  }

  @Computed("hasInvalidName")
  public boolean hasInvalidName() {
    return getLabel() == null || getLabel().trim().isEmpty();
  }

  @Computed("isTotalSatisfactory")
  public boolean isTotalSatisfactory() {
    // Max delta (inclusive both ways)
    return NumberUtil.equalEnough(util().getValuesTotal(getTimeVaryingProfile().getValues()), (double) util().getValuesMax(), tolerance());
  }

  private double tolerance() {
    return getProfileType() == CustomTimeVaryingProfileType.MONTHLY ? MONTHLY_TOLERANCE : DIURNAL_TOLERANCE;
  }

  @Computed
  public String getTotalWarningMessage() {
    return M.messages().timeVaryingProfileTotalSummaryWarning(util().getValuesTotalPreciseText(getTimeVaryingProfile().getValues()),
        util().getValuesMaxText());
  }

  @Computed("isTotalExact")
  public boolean isTotalExact() {
    return NumberUtil.equalEnoughExclusive(util().getValuesTotal(getTimeVaryingProfile().getValues()),
        (double) util().getValuesMax(),
        TimeVaryingProfileUtilBase.SMALLEST);
  }

  @Computed("hasValidationWarnings")
  public boolean hasValidationWarnings() {
    return isTotalSatisfactory() && !isTotalExact();
  }

  @Computed("hasValidationErrors")
  public boolean hasValidationErrors() {
    return !isTotalSatisfactory()
        || !isAllValuesValid()
        || hasInvalidName();
  }

  @Computed
  public String getAllValuesValidErrorMessage() {
    return M.messages().timeVaryingProfileAllValuesValidError();
  }

  @Computed("isAllValuesValid")
  public boolean isAllValuesValid() {
    return getTimeVaryingProfile().getValues().stream().allMatch(v -> v >= 0)
        && getTimeVaryingProfile().getValues().size() == getProfileType().getExpectedNumberOfValues();
  }

  private TimeVaryingProfileUtilBase util() {
    return TimeVaryingProfileUtil.getTimeVaryingProfileUtilBase(getProfileType());
  }
}
