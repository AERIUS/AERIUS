/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.road;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.road.RoadNumberOfVehiclesValidators.RoadNumberOfVehiclesValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.domain.source.road.CustomVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.RoadTypeMode;
import nl.overheid.aerius.wui.application.domain.source.road.SpecificVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.VehicleType;
import nl.overheid.aerius.wui.application.domain.source.road.Vehicles;
import nl.overheid.aerius.wui.vue.DebugDirective;

@Component(customizeOptions = {
    RoadNumberOfVehiclesValidators.class
}, directives = {
    ValidateDirective.class,
    DebugDirective.class
}, components = {
    LabeledInputComponent.class,
    ValidationBehaviour.class
})
public class RoadNumberOfVehiclesComponent extends ErrorWarningValidator implements HasCreated,
    HasValidators<RoadNumberOfVehiclesValidations> {
  @Prop @JsProperty Vehicles vehicles;
  @Prop RoadTypeMode mode;
  @Data String numberOfVehiclesV;

  @JsProperty(name = "$v") RoadNumberOfVehiclesValidations validations;

  @Watch(value = "vehicles", isImmediate = true)
  public void onVehiclesChange() {
    if (vehicles.getVehicleType() == VehicleType.CUSTOM) {
      numberOfVehiclesV = String.valueOf(((CustomVehicles) vehicles).getVehiclesPer(vehicles.getTimeUnit()));
    } else {
      numberOfVehiclesV = String.valueOf(((SpecificVehicles) vehicles).getVehiclesPer(vehicles.getTimeUnit()));
    }
  }

  @Computed
  public RoadTypeMode getRoadTypeMode() {
    return mode;
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Override
  @Computed
  public RoadNumberOfVehiclesValidations getV() {
    return validations;
  }

  @Computed
  protected void setTimeUnit(final String timeUnit) {
    vehicles.setTimeUnit(TimeUnit.valueOf(timeUnit));
  }

  @Computed
  protected String getTimeUnit() {
    return vehicles.getTimeUnit().name();
  }

  @Computed
  protected String getNumberOfVehicles() {
    return numberOfVehiclesV;
  }

  @Computed
  protected void setNumberOfVehicles(final String numberOfVehicles) {
    if (vehicles.getVehicleType() == VehicleType.CUSTOM) {
      ValidationUtil.setSafeDoubleValue(((CustomVehicles) vehicles)::setVehiclesPerTimeUnit, numberOfVehicles, 0D);
    } else {
      ValidationUtil.setSafeDoubleValue(((SpecificVehicles) vehicles)::setVehiclesPerTimeUnit, numberOfVehicles, 0D);
    }
    numberOfVehiclesV = numberOfVehicles;
  }

  @JsMethod
  protected String numberOfVehiclesConversionError() {
    return i18n.errorNotValidEntry(numberOfVehiclesV);
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return validations.numberOfVehiclesV.invalid;
  }
}
