/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.importer;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Emit;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.HorizontalCollapse;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.tooltip.TooltipComponent;
import nl.overheid.aerius.wui.application.ui.main.FadeTransition;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    FadeTransition.class,
    HorizontalCollapse.class,
    TooltipComponent.class,
    ButtonIcon.class,
})
public class FileUploadComponent extends BasicVueComponent {
  @Prop boolean loading;
  @Prop double completeRatio;

  @Prop boolean pending;
  @Prop boolean valid;
  @Prop boolean showInvalid;
  @Prop boolean showErrors;
  @Prop boolean showWarnings;

  @Computed
  public String completePercentage() {
    return Math.round(completeRatio * 100) + "%";
  }

  @JsMethod
  @Emit
  public void removeFile() {}

  @Emit
  @JsMethod
  public void displayErrors() {}

  @Emit
  @JsMethod
  public void displayWarnings() {}
}
