/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.nav;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasActivated;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import elemental2.dom.DomGlobal;
import elemental2.dom.Element;
import elemental2.dom.Event;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.command.PlaceChangeRequestCommand;
import nl.aerius.wui.event.PlaceChangeEvent;
import nl.aerius.wui.place.Place;
import nl.aerius.wui.place.PlaceController;
import nl.overheid.aerius.wui.application.components.logo.AeriusLogoComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.NavigationContext;
import nl.overheid.aerius.wui.application.daemon.misc.PersistablePreferencesContext;
import nl.overheid.aerius.wui.application.daemon.misc.PreferencesDaemon;
import nl.overheid.aerius.wui.application.daemon.misc.UserSettingChangeCommand;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.place.HomePlace;
import nl.overheid.aerius.wui.application.util.AccessibilityUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    AeriusLogoComponent.class,
    NavigationItemComponent.class,
    AnchorItemComponent.class,
    EasterActivatorComponent.class
})
public class NavigationComponent extends BasicVueComponent implements IsVueComponent, HasActivated, HasCreated, HasMounted {
  interface NavigationComponentEventBinder extends EventBinder<NavigationComponent> {
  }

  private final NavigationComponentEventBinder EVENT_BINDER = GWT.create(NavigationComponentEventBinder.class);

  @Prop(required = true) EventBus eventBus;

  @Inject @Data ApplicationContext applicationContext;
  @Inject @Data PersistablePreferencesContext preferencesContext;
  @Inject @Data NavigationContext context;
  @Inject PlaceController placeController;

  @Data Place place;
  @Data String href;

  @JsMethod
  public void goHome() {
    eventBus.fireEvent(new PlaceChangeRequestCommand(new HomePlace(applicationContext.getTheme())));
  }

  @Computed
  public NavigationItem getMenuCollapseItem() {
    return NavigationItem.createSimpleItem(
        M.messages().menuCollapse(),
        "icon-menu-collapse",
        p -> false,
        p -> toggleMenuCollapse())
        .withLabelLeft(true);
  }

  @JsMethod
  public void toggleMenuCollapse() {
    eventBus.fireEvent(new UserSettingChangeCommand(PreferencesDaemon.COLLAPSED_MENU, !preferencesContext.isCollapsedMenu()));
  }

  @Override
  public void mounted() {
    href = Window.Location.getHref();
  }

  @Computed
  public List<NavigationItem> getTopItems() {
    return context.getTopItems();
  }

  @Computed
  public List<NavigationItem> getBottomItems() {
    return context.getBottomItems();
  }

  @EventHandler
  public void onPlaceChangeEvent(final PlaceChangeEvent e) {
    place = e.getValue();
    href = Window.Location.getHref();
  }

  @JsMethod
  public void skipLinkClick(final Event event) {
    event.preventDefault();
    Optional.ofNullable(AccessibilityUtil.findFirstFocusable(DomGlobal.document.querySelector("main")))
        .ifPresent(Element::focus);
  }

  @Override
  public void created() {
    activated();
  }

  @Override
  public void activated() {
    place = placeController.getPlace();

    if (eventBus != null) {
      EVENT_BINDER.bindEventHandlers(this, eventBus);
    }
  }
}
