/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.misc;

import java.util.Map;
import java.util.function.Function;

import javax.inject.Inject;
import javax.inject.Singleton;

import nl.aerius.wui.place.Place;
import nl.overheid.aerius.shared.domain.result.MainTab;
import nl.overheid.aerius.shared.domain.result.ResultView;
import nl.overheid.aerius.shared.domain.sector.EmissionCalculationMethod;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.EmissionSourceEditorContext;
import nl.overheid.aerius.wui.application.context.result.ResultViewContext;
import nl.overheid.aerius.wui.application.place.BuildingPlace;
import nl.overheid.aerius.wui.application.place.CalculatePlace;
import nl.overheid.aerius.wui.application.place.CalculationPointsPlace;
import nl.overheid.aerius.wui.application.place.EmissionSourcePlace;
import nl.overheid.aerius.wui.application.place.ExportPlace;
import nl.overheid.aerius.wui.application.place.HomePlace;
import nl.overheid.aerius.wui.application.place.NextStepsPlace;
import nl.overheid.aerius.wui.application.place.ResultsPlace;
import nl.overheid.aerius.wui.application.place.ScenarioInputListPlace;
import nl.overheid.aerius.wui.application.place.TimeVaryingProfilePlace;

@Singleton
public final class ManualHelperMinion {
  static final String MANUAL_TABLE_OF_CONTENTS = "/pages/toc.html";
  private static final String MANUAL_1_HOME = "/pages/1-home.html";
  private static final String MANUAL_2_SOURCES = "/pages/2-sources.html";
  private static final String MANUAL_2_1_1_INPUT_SOURCE = "/pages/2-1-1-input-source.html";
  private static final String MANUAL_2_1_2_SOURCE_SECTOR_GROUP = "/pages/2-1-2-source-sector-group-:group.html";
  private static final String MANUAL_2_1_3_SOURCE_EMISSION_CALCULATION_METHOD = "/pages/2-1-3-source-sector-:emission_calculation_method.html";
  private static final String MANUAL_2_2_BUILDING_CREATE = "/pages/2-2-building-create.html";
  private static final String MANUAL_2_3_1_TVP_DIURNAL_CREATE = "/pages/2-3-1-tvp-diurnal-create.html";
  private static final String MANUAL_2_3_2_TVP_MONTHLY_CREATE = "/pages/2-3-2-tvp-monthly-create.html";
  private static final String MANUAL_3_CALCULATION_POINTS = "/pages/3-calculation-points.html";
  private static final String MANUAL_4_CALCULATION_JOBS = "/pages/4-calculation-jobs.html";
  private static final String MANUAL_5_1_SCENARIOS = "/pages/5-1-results_scenarios.html";
  private static final String MANUAL_5_2_SUMMARY = "/pages/5-2-results_summary.html";
  private static final String MANUAL_5_3_1_RESULTS_CL = "/pages/5-3-1-results_cl.html";
  private static final String MANUAL_5_3_2_RESULTS_DISTRIBUTION = "/pages/5-3-2-results_distribution.html";
  private static final String MANUAL_5_3_3_RESULTS_MARKERS = "/pages/5-3-3-results_markers.html";
  private static final String MANUAL_5_3_4_RESULTS_HABITAT_SPECIES = "/pages/5-3-4-results_habitat_and_species.html";
  private static final String MANUAL_5_3_4_RESULTS_PROCUREMENT_POLICY = "/pages/5-3-4-results_procurement_policy.html";
  private static final String MANUAL_5_4_DECISION_FRAMEWORK = "/pages/5-4-decision_framework.html";
  private static final String MANUAL_6_EXPORT = "/pages/6-export.html";
  private static final String MANUAL_7_NEXT_STEPS = "/pages/7-next-steps.html";

  @Inject ResultViewContext resultContext;
  @Inject EmissionSourceEditorContext emissionEditorContext;
  @Inject ApplicationContext appContext;

  public void initManualPages(final Map<Class<?>, String> manualPages) {
    manualPages.put(HomePlace.class, MANUAL_1_HOME);
    manualPages.put(ScenarioInputListPlace.class, MANUAL_2_SOURCES);
    manualPages.put(BuildingPlace.class, MANUAL_2_2_BUILDING_CREATE);
    manualPages.put(CalculationPointsPlace.class, MANUAL_3_CALCULATION_POINTS);
    manualPages.put(CalculatePlace.class, MANUAL_4_CALCULATION_JOBS);
    manualPages.put(ExportPlace.class, MANUAL_6_EXPORT);
    manualPages.put(NextStepsPlace.class, MANUAL_7_NEXT_STEPS);
  }

  public void initCustomPages(final Map<Class<?>, Function<Place, String>> customPages) {
    customPages.put(TimeVaryingProfilePlace.class, this::doCreateTimeVaryingProfileNavigation);
    customPages.put(EmissionSourcePlace.class, this::doCreateEmissionSourceNavigation);
    customPages.put(ResultsPlace.class, this::doResultsPageNavigation);
  }

  public String doCreateTimeVaryingProfileNavigation(final Place place) {
    final TimeVaryingProfilePlace tvpPlace = (TimeVaryingProfilePlace) place;

    final String page;
    switch (tvpPlace.getType()) {
    case THREE_DAY:
      page = MANUAL_2_3_1_TVP_DIURNAL_CREATE;
      break;
    case DAY:
      page = MANUAL_2_3_1_TVP_DIURNAL_CREATE;
      break;
    default:
    case MONTHLY:
      page = MANUAL_2_3_2_TVP_MONTHLY_CREATE;
      break;
    }

    return page;
  }

  public String doCreateEmissionSourceNavigation(final Place place) {
    final int sectorId = emissionEditorContext.getSource().getSectorId();

    final String page;
    if (sectorId == 0) {
      final SectorGroup sectorGroup = emissionEditorContext.getSelectedSectorGroup();
      if (sectorGroup == null) {
        page = MANUAL_2_1_1_INPUT_SOURCE;
      } else {
        page = MANUAL_2_1_2_SOURCE_SECTOR_GROUP.replace(":group", sectorGroup.name());
      }
    } else {
      final EmissionCalculationMethod method = appContext.getConfiguration().getEmisionSourceUIViewType().get(sectorId);
      page = MANUAL_2_1_3_SOURCE_EMISSION_CALCULATION_METHOD.replace(":emission_calculation_method", method.name());
    }
    return page;
  }

  public String doResultsPageNavigation(final Place place) {
    final MainTab mainTab = resultContext.getSelectedMainTab();

    final String page;
    switch (mainTab) {
    default:
    case CALCULATED_INFO:
      page = MANUAL_5_1_SCENARIOS;
      break;
    case SUMMARY:
      page = MANUAL_5_2_SUMMARY;
      break;
    case DECISION_FRAMEWORK:
      page = MANUAL_5_4_DECISION_FRAMEWORK;
      break;
    case SCENARIO_RESULTS:
      final ResultView resultViewTab = resultContext.getResultViewTab();
      switch (resultViewTab) {
      default:
      case PERCENTAGE_CRITICAL_LEVELS:
        page = MANUAL_5_3_1_RESULTS_CL;
        break;
      case DEPOSITION_DISTRIBUTION:
        page = MANUAL_5_3_2_RESULTS_DISTRIBUTION;
        break;
      case MARKERS:
        page = MANUAL_5_3_3_RESULTS_MARKERS;
        break;
      case HABITAT_TYPES:
        page = MANUAL_5_3_4_RESULTS_HABITAT_SPECIES;
        break;
      case PROCUREMENT_POLICY_THRESHOLD:
        page = MANUAL_5_3_4_RESULTS_PROCUREMENT_POLICY;
        break;
      }
      break;
    }

    return page;
  }
}
