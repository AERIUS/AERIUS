/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.service;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.ImplementedBy;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationInfo;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.application.domain.export.ExportOptions;
import nl.overheid.aerius.wui.application.domain.importer.ScenarioMetaData;
import nl.overheid.aerius.wui.application.domain.importer.SituationWithFileId;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsSummary;
import nl.overheid.aerius.wui.application.domain.summary.DecisionFrameworkResults;
import nl.overheid.aerius.wui.application.domain.summary.JobSummary;

@ImplementedBy(CalculationServiceAsyncImpl.class)
public interface CalculationServiceAsync {

  void startCalculation(SituationComposition situationComposition, CalculationSetOptions calculationOptions,
      ExportOptions exportOptions, ScenarioContext scenarioContext, ScenarioMetaData scenarioMetaData, Theme theme, String jobKey,
      AsyncCallback<String> callback);

  void importResults(ScenarioContext scenarioContext, SituationComposition situationComposition, List<SituationWithFileId> situationWithFileIds,
      String archiveContributionFileId, Theme theme, AsyncCallback<String> callback);

  void getCalculationStatus(String calculationCode, AsyncCallback<CalculationInfo> callback);

  void cancelCalculation(String calculationCode, AsyncCallback<String> callback);

  void deleteCalculation(String calculationCode, AsyncCallback<String> callback);

  void deleteCalculationViaPost(String calculationCode);

  void getSituationResultsSummary(String calculationCode, SituationResultsKey situationResultsKey,
      AsyncCallback<SituationResultsSummary> callback);

  void getCalculationSummary(String calculationCode, AsyncCallback<JobSummary> callback);

  void getDecisionFrameworkResults(String calculationCode, AsyncCallback<DecisionFrameworkResults> callback);
}
