/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import javax.inject.Singleton;

@Singleton
public class CalculationPointValidationContext {
  private boolean characteristicsError = false;
  private boolean nameError = false;
  private boolean locationError = false;
  private boolean locationNotUnique = false;
  private boolean detailError = false;

  public boolean getCharacteristicsError() {
    return characteristicsError;
  }

  public void setCharacteristicsError(final boolean characteristicsError) {
    this.characteristicsError = characteristicsError;
  }

  public boolean getLocationError() {
    return locationError;
  }

  public void setLocationError(final boolean locationError) {
    this.locationError = locationError;
  }

  public boolean getLocationNotUnique() {
    return locationNotUnique;
  }

  public void setLocationNotUnique(final boolean locationNotUnique) {
    this.locationNotUnique = locationNotUnique;
  }

  public boolean getNameError() {
    return nameError;
  }

  public void setNameError(final boolean nameError) {
    this.nameError = nameError;
  }

  public boolean getDetailError() {
    return detailError;
  }

  public void setDetailError(final boolean detailError) {
    this.detailError = detailError;
  }

  public boolean hasErrors() {
    return characteristicsError
        || nameError
        || locationError
        || locationNotUnique
        || detailError;
  }

  public void reset() {
    characteristicsError = false;
    nameError = false;
    locationError = false;
    locationNotUnique = false;
    detailError = false;
  }
}
