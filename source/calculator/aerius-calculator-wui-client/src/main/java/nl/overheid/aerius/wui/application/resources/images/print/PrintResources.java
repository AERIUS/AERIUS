/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.resources.images.print;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.DataResource;

public interface PrintResources {

  @ClientBundle.Source("cover-illustration-calculator.svg")
  @DataResource.MimeType("image/svg+xml")
  DataResource printCoverIllustrationOwN2000();

  @ClientBundle.Source("cover-illustration-calculator-appendix.svg")
  @DataResource.MimeType("image/svg+xml")
  DataResource printCoverIllustrationOwN2000Appendix();

  @ClientBundle.Source("cover-illustration-register.svg")
  @DataResource.MimeType("image/svg+xml")
  DataResource printCoverIllustrationOwN2000Register();

  @ClientBundle.Source("cover-illustratie-lbv.svg")
  @DataResource.MimeType("image/svg+xml")
  DataResource printCoverIllustrationOwN2000LbvPolicy();

  @ClientBundle.Source("cover-illustration-calculator-uk.svg")
  @DataResource.MimeType("image/svg+xml")
  DataResource printCoverIllustrationNca();
}
