/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.i18n;

import com.google.gwt.i18n.client.Messages.Select;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings.DepositionValueDisplayType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.source.road.RoadLayerStyle;
import nl.overheid.aerius.wui.application.domain.geo.LabelDisplayMode;
import nl.overheid.aerius.wui.application.ui.pages.results.GeneralizedEmissionResultType;

public interface LayerPanelMessages {
  String layerPanelTitle();

  String layerPanelInactiveText();

  String layerCalculationBoundary();

  String layerEmissionSources();

  String layerMetSites();

  String layerZoneOfInfluence();

  /**
   * @param radius km
   */
  String layerZoneOfInfluenceLegendText(double radius);

  String layerJobInput();

  String layerJobSituationTypeIndicator(@Select SituationType situationType);

  String layerResults();

  String layerConcentrationResultsUnit();

  String layerDepositionResultsUnit(@Select DepositionValueDisplayType displayType);

  String layerBackgroundResults();

  String layerHabitatAreasSensitivityLevel();

  String layerHabitatAreasSensitivityLevelDescription(@Select GeneralizedEmissionResultType resultType, String unit);

  String layerHabitatExtraAssessmentCombined();

  String layerNitrogenSensitivityExtraAssessmentCombined();

  String layerNatureAreas();

  String layerPercentageCL();

  String layerTotalPercentageCL(String totalText);

  String layerTotalPercentageCLLegendNote(String totalText);

  String layerTotalPercentageCLItemLabelNearingExceedance(String totalText);

  String layerTotalPercentageCLItemLabelExceedance(String totalText);

  String layerCalculationPointsResults();

  String layerSiteLocation();

  String layerMarkers();

  String layerRoad(@Select RoadLayerStyle roadLayerStyle);

  String layerRoadNetwork();

  String layerRoadEmissionUnit(@Select Theme theme);

  String layerRoadElevationHeightNegative(String range);

  String layerRoadElevationHeightPositive(String range);

  String layerRoadElevationHeightZero();

  String layerRoadVolumeUnit();

  String layerSelectOption();

  String layerShowAll();

  String layerItemLabel(@Select String label);

  String labelsPanelTitle();

  String labelDisplayMode(@Select LabelDisplayMode mode);

  String layerTitleOff();

  String layerTitleOn();

  String layerArchiveProjectMarkersTitle();

  String layerArchiveProjectMarkersLegend();

}
