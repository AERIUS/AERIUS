/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.file;

import java.util.Arrays;
import java.util.Optional;

import javax.inject.Inject;

import elemental2.dom.File;

import jsinterop.base.Js;

import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.future.AppAsyncCallback;
import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.js.file.FileValidationStatus;
import nl.overheid.aerius.js.file.ValidationStatus;
import nl.overheid.aerius.wui.application.components.importer.ImportActionSelectCommand;
import nl.overheid.aerius.wui.application.components.importer.domain.ImportAction;
import nl.overheid.aerius.wui.application.context.importer.FileContext;
import nl.overheid.aerius.wui.application.service.FileServiceAsync;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;

public class FileValidationPoller extends BasicEventComponent {

  private static final int RETRY_DELAY_MS = 200;

  @Inject private FileContext context;
  @Inject private FileServiceAsync service;

  private boolean running;

  public void start() {
    if (running) {
      return;
    }

    running = true;
    SchedulerUtil.delay(() -> doFetch());
  }

  public boolean isRunning() {
    return running;
  }

  private void doFetch() {
    final Optional<FileUploadStatus> fileOptionalJson = getPendingFile();
    if (fileOptionalJson.isEmpty()) {
      running = false;
      return;
    }
    final FileUploadStatus file = fileOptionalJson.get();

    service.fetchValidationResult(file.getFileCode(), AppAsyncCallback.create(
        fileValidation -> {
          if (fileValidation.getValidationStatus() != ValidationStatus.PENDING) {
            for (final String childFileCode : fileValidation.getChildFileCodes()) {
              addNewChildFile(childFileCode, file.getFile());
            }
            processAsValidation(fileValidation);
          }
          tryNext();
        }, e -> {
          processAsError(file.getFileCode());
          tryNext();
        }));
  }

  private void addNewChildFile(final String fileCode, final File file) {
    final FileUploadStatus newFile = new FileUploadStatus();
    newFile.setFileCode(fileCode);
    newFile.setFile(file);
    newFile.setComplete(true);
    newFile.setCompleteRatio(1D);
    newFile.setPending(true);
    context.add(newFile);
  }

  private void processAsError(final String fileCode) {
    context.findFile(fileCode)
        .ifPresent(file -> {
          file.clear();
          file.setValidated(true);
          file.setFailed(true);
          file.setPending(false);
          file.setImportAction(ImportAction.ignore());
        });
  }

  private void processAsValidation(final FileValidationStatus fileValidation) {
    context.findFile(fileValidation.getFileCode())
        .ifPresent(file -> processFileValidationStatus(fileValidation, file));
  }

  private void processFileValidationStatus(final FileValidationStatus fileValidation, final FileUploadStatus file) {
    file.clear();
    file.setSituationName(fileValidation.getSituationName());
    file.setSituationType(fileValidation.getSituationType());
    file.setCalculationJobType(fileValidation.getCalculationJobType());
    file.setSituationStats(fileValidation.getSituationStats());
    file.getErrors().addAll(Js.cast(Arrays.asList(fileValidation.getErrors())));
    file.getWarnings().addAll(Js.cast(Arrays.asList(fileValidation.getWarnings())));

    if (!file.getErrors().isEmpty()) {
      // User must explicitly ignore this / set his own action
    } else if (file.getSituationStats().isArchive()) {
      file.setImportAction(ImportAction.addArchive());
    } else if (file.getSituationStats().getCalculationPoints() == 0) {
      eventBus.fireEvent(new ImportActionSelectCommand(ImportAction.addNew(), file));
    } else {
      file.setImportAction(ImportAction.addCalculationPoints());
    }
    file.setValidated(true);
    file.setPending(false);
  }

  private void tryNext() {
    if (hasPendingFiles()) {
      SchedulerUtil.delay(() -> doFetch(), RETRY_DELAY_MS);
    } else {
      running = false;
    }
  }

  private Optional<FileUploadStatus> getPendingFile() {
    return context.getFiles().stream()
        .filter(v -> v.getFileCode() != null && v.isPending())
        .findFirst();
  }

  private boolean hasPendingFiles() {
    return context.getFiles().stream()
        .anyMatch(v -> !v.isValidated());
  }
}
