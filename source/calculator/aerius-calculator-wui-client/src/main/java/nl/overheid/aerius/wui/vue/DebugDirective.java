/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.vue;

import com.axellience.vuegwt.core.annotations.directive.Directive;
import com.axellience.vuegwt.core.client.directive.VueDirective;
import com.axellience.vuegwt.core.client.vnode.VNode;
import com.axellience.vuegwt.core.client.vnode.VNodeDirective;

import elemental2.dom.Element;

import jsinterop.base.JsPropertyMap;

import nl.aerius.wui.dev.GWTProd;
import nl.overheid.aerius.wui.application.util.Semaphore;

/**
 * Example usage:
 *
 * <pre>
 * &lt;div v-debug="testId.BUTTON_CALCULATE" &#47;&gt;
 * </pre>
 */
@Directive
public class DebugDirective extends VueDirective {
  @Override
  public void inserted(final Element el, final VNodeDirective binding, final VNode vnode) {
    super.inserted(el, binding, vnode);

    final String id = flatten(findId(binding));
    if (el.getAttribute("id") != null) {
      // Warn if the id does not match (if it does match, that's fine -- probably
      // doubling up v-debug and id, but that's legitimate)
      if (!el.getAttribute("id").equals(id)) {
        GWTProd.warn("Not completing debug directive for element because it already has an id: [" + el.getAttribute("id") + "] vs [" + id + "]");
        GWTProd.log(el);
      }
      return;
    }

    if (id != null) {
      el.setAttribute("id", id);
    } else {
      GWTProd.warn("Could not complete debug directive.");
      GWTProd.log(el);
    }
  }

  private static String flatten(final String id) {
    return id == null ? null : id.replaceAll(" ", "-");
  }

  public static String findId(final VNodeDirective binding) {
    if (binding.getArg() != null) { // Option one is the arg (v-debug:arg)
      return binding.getArg();
    } else if (binding.getValue() != null) { // Option two is the expression value (v-debug="expression")
      return binding.getValue().toString();
    } else { // Option three is the modifier (v-debug.modifier)
      final Semaphore<String> sem = new Semaphore<>();
      @SuppressWarnings("unchecked")
      final JsPropertyMap<String> modifiers = binding.getModifiers();
      modifiers.forEach(key -> {
        sem.setValue(key);
      });
      if (sem.getValue() != null) {
        return sem.getValue();
      }
    }

    // Return null if no id can be found
    return null;
  }
}
