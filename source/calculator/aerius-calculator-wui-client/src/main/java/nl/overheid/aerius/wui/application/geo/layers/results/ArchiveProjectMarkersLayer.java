/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.results;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.DoubleSupplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;
import com.google.web.bindery.event.shared.binder.GenericEvent;

import ol.format.GeoJson;
import ol.format.GeoJsonOptions;

import nl.aerius.geo.domain.LayerInfo;
import nl.aerius.geo.domain.legend.Legend;
import nl.aerius.geo.domain.legend.TextLegend;
import nl.overheid.aerius.wui.application.context.MapConfigurationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.domain.calculation.ArchiveMetaData;
import nl.overheid.aerius.wui.application.domain.calculation.ArchiveProject;
import nl.overheid.aerius.wui.application.domain.source.GenericImaerFeature;
import nl.overheid.aerius.wui.application.event.CalculationCancelEvent;
import nl.overheid.aerius.wui.application.event.CalculationCompleteEvent;
import nl.overheid.aerius.wui.application.event.CalculationDeleteEvent;
import nl.overheid.aerius.wui.application.event.SwitchActiveCalculationEvent;
import nl.overheid.aerius.wui.application.geo.layers.LabelFeature;
import nl.overheid.aerius.wui.application.geo.layers.LabelStyle;
import nl.overheid.aerius.wui.application.geo.layers.base.BaseClusteredMarkerLayer;
import nl.overheid.aerius.wui.application.geo.layers.base.ClusteredLabelStyleCreator;
import nl.overheid.aerius.wui.application.geo.layers.base.VectorFeaturesWrapper;
import nl.overheid.aerius.wui.application.i18n.M;

/**
 * Provides a layer with the in-combination archive projects location pins.
 */
public class ArchiveProjectMarkersLayer extends BaseClusteredMarkerLayer<GenericImaerFeature> {
  interface ArchiveProjectMarkersLayerEventBinder extends EventBinder<ArchiveProjectMarkersLayer> {
  }

  private static final ArchiveProjectMarkersLayerEventBinder EVENT_BINDER = GWT.create(ArchiveProjectMarkersLayerEventBinder.class);

  private final VectorFeaturesWrapper vectorObject = new VectorFeaturesWrapper();

  private final CalculationContext calculationContext;

  @Inject
  public ArchiveProjectMarkersLayer(final EventBus eventBus, @Assisted final int zIndex,
      final MapConfigurationContext mapConfigurationContext, final CalculationContext calculationContext) {
    super(new LayerInfo(), zIndex, new ArchiveProjectLabelStyleCreator(mapConfigurationContext::getIconScale));
    this.calculationContext = calculationContext;
    getInfo().setName(ArchiveProjectMarkersLayer.class.getName());
    getInfo().setTitle(M.messages().layerArchiveProjectMarkersTitle());
    final Legend legend = new TextLegend(M.messages().layerArchiveProjectMarkersLegend());
    getInfo().setLegend(legend);

    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  private void update() {
    final Optional<ArchiveMetaData> archiveMetaDataOptional = Optional.ofNullable(calculationContext.getActiveCalculation())
        .map(x -> x.getCalculationInfo())
        .map(x -> x.getArchiveMetaData());
    final Optional<ArchiveProject[]> archiveProjectsOptional = archiveMetaDataOptional
        .map(x -> x.getArchiveProjects());
    if (archiveProjectsOptional.isEmpty()) {
      setFeatures(vectorObject, Collections.emptyList());
      return;
    }

    final List<GenericImaerFeature> archiveProjectFeatureList = new ArrayList<>();
    for (final ArchiveProject archiveProject : archiveProjectsOptional.get()) {
      if (archiveProject.getCentroid() == null) {
        continue;
      }
      final GenericImaerFeature siteLocationFeature = GenericImaerFeature.create();

      siteLocationFeature.setId(archiveMetaDataOptional.get().determineDisplayId(archiveProject));
      siteLocationFeature.setLabel(archiveProject.getName());
      siteLocationFeature.setGeometry(new GeoJson().readGeometry(archiveProject.getCentroid(), new GeoJsonOptions()));
      archiveProjectFeatureList.add(siteLocationFeature);
    }

    setFeatures(vectorObject, archiveProjectFeatureList);
  }

  @EventHandler(handles = {CalculationCompleteEvent.class, CalculationDeleteEvent.class, CalculationCancelEvent.class,
      SwitchActiveCalculationEvent.class})
  public void onCalculationCompleteEvent(final GenericEvent e) {
    update();
  }

  private static class ArchiveProjectLabelStyleCreator extends ClusteredLabelStyleCreator<GenericImaerFeature> {
    public ArchiveProjectLabelStyleCreator(final DoubleSupplier iconScale) {
      super(iconScale, false, LabelStyle.ARCHIVE_PROJECT);
    }

    @Override
    protected String clusterLabel(final LabelFeature[] features, final LabelStyle labelStyle) {
      return Stream.of(features).map(LabelFeature::getId).collect(Collectors.joining(" "));
    }
  }

}
