/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.shipping.maritime;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;

import elemental2.core.JsArray;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.domain.source.MaritimeShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.shipping.maritime.MaritimeShipping;
import nl.overheid.aerius.wui.application.domain.source.shipping.maritime.MaritimeShippingType;
import nl.overheid.aerius.wui.application.domain.source.util.EmissionSourceFeatureUtil;

@Component(name = "aer-maritime-shipping-detail-editor",
components = {
    MaritimeStandardEmissionComponent.class,
    ValidationBehaviour.class
})
public class MaritimeShippingDetailEditorComponent extends ErrorWarningValidator {
  @Prop MaritimeShippingESFeature source;
  @Prop MaritimeShipping subSource;

  @Data MaritimeShippingType toggleSelect;

  @Watch(value = "source", isImmediate = true)
  public void onSourceChange(final Integer neww, final Integer old) {
    if (subSource != null) {
      toggleSelect = subSource.getMaritimeShippingType();
    }
  }

  @JsMethod
  protected boolean isSourceAvailable() {
    return subSource != null;
  }

  @JsMethod
  public boolean isESType(final EmissionSourceType type) {
    return source != null && source.getEmissionSourceType() == type;
  }

  @JsMethod
  protected void setToggle(final MaritimeShippingType maritimeShippingType) {
    toggleSelect = maritimeShippingType;
    final JsArray<MaritimeShipping> shipping = source.getSubSources();
    shipping.splice(shipping.indexOf(subSource), 1,
        EmissionSourceFeatureUtil.convertMaritimeShippingType(maritimeShippingType, subSource));
  }
}
