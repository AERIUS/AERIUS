/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.result;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;

/**
 * Data class to contain result values for a single point.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class EmissionResults {

  private @JsProperty JsPropertyMap<Object> hashMap;

  /**
   * Assumes that, in a set of emission results, for a given emission result type,
   * if a combined NH3+NOx value exist, no values exist for NOx and/or NH3 separately,
   * and if NOx and/or NH3 values exist separately, no combined NH3+NOx values exists.
   * @param key The key to get the result for.
   * @return Always returns a value; if no value exists for the given key, it returns 0.
   */
  public final @JsOverlay double getEmissionResultValue(final EmissionResultKey key) {
    double er = 0.0;
    if (key != null) {
      final Substance substance = key.getSubstance();

      if (substance == Substance.NOXNH3 && !hashMap.has(key.name()) && key.getEmissionResultType() == EmissionResultType.DEPOSITION) {
        for (final EmissionResultKey erk : key.hatch()) {
          er += Js.asDouble(hashMap.get(erk.name()));
        }
      } else if (hashMap.has(key.name())) {
        final Double erd = Js.asDouble(hashMap.get(key.name()));
        er = erd == null ? 0.0 : erd;
      }
    }
    return er;
  }

  public final @JsOverlay boolean hasKey(EmissionResultKey emissionResultKey) {
    return hashMap.has(emissionResultKey.name());
  }

}
