/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.results.custompoints;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasDestroyed;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import elemental2.dom.Element;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings;
import nl.overheid.aerius.wui.application.command.result.SelectCustomCalculationPointResultCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.context.result.ResultSelectionContext;
import nl.overheid.aerius.wui.application.context.result.ResultSummaryContext;
import nl.overheid.aerius.wui.application.domain.result.CustomCalculationPointResult;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.application.service.ColorRangeService;
import nl.overheid.aerius.wui.application.ui.pages.results.habitat.ResultsHabitatListView;
import nl.overheid.aerius.wui.vue.BasicVueEventComponent;

@Component(components = {
    CollapsiblePanel.class,
    SimplifiedListBoxComponent.class,
    ButtonIcon.class,
    ResultsHabitatListView.class,
    VerticalCollapseGroup.class
})
public class CustomPointsView extends BasicVueEventComponent implements HasMounted, HasCreated, HasDestroyed {

  private static final CustomPointsViewEventBinder EVENT_BINDER = GWT.create(CustomPointsViewEventBinder.class);

  interface CustomPointsViewEventBinder extends EventBinder<CustomPointsView> {}

  private HandlerRegistration handlers;

  @Prop protected EventBus eventBus;

  @Data @Inject ApplicationContext applicationContext;
  @Data @Inject CalculationContext calculationContext;
  @Data @Inject protected ScenarioContext scenarioContext;
  @Inject ColorRangeService colorRangeService;
  @Data @Inject ResultSelectionContext resultSelectionContext;

  @Data CustomPointsSortColumn sortColumn = CustomPointsSortColumn.VALUE;
  @Data Boolean sortAscending = false;

  @Data @JsProperty Map<Integer, String> customPointNames = new HashMap<>();

  @JsMethod
  public String formatValueFor(final Double value) {
    return MessageFormatter.formatDeposition(value, applicationContext.getConfiguration().getEmissionResultValueDisplaySettings());
  }

  @Computed
  public EmissionResultValueDisplaySettings.DepositionValueDisplayType getDepositionType() {
    return applicationContext.getConfiguration().getEmissionResultValueDisplaySettings().getDisplayType();
  }

  @JsMethod
  public ResultSummaryContext getResultContext() {
    return Optional.ofNullable(getCalculationExecution())
        .map(v -> v.getResultContext())
        .orElseThrow(() -> new RuntimeException("Could not retrieve result context."));
  }

  @JsMethod
  public List<CustomCalculationPointResult> getCalculationPoints() {
    final Comparator<CustomCalculationPointResult> comparator = (p1, p2) -> {
      if (sortColumn == CustomPointsSortColumn.ID) {
        return Integer.compare(p1.getCustomPointId(), p2.getCustomPointId()) * (Boolean.TRUE.equals(sortAscending) ? 1 : -1);
      } else if (sortColumn == CustomPointsSortColumn.VALUE) {
        return Double.compare(
            Optional.ofNullable(p1.getResult()).orElse(0.0),
            Optional.ofNullable(p2.getResult()).orElse(0.0))
            * (Boolean.TRUE.equals(sortAscending) ? 1 : -1);
      } else {
        return 0;
      }
    };
    return Arrays.stream(getCustomPointResults()).sorted(comparator).collect(Collectors.toList());
  }

  protected CustomCalculationPointResult[] getCustomPointResults() {
    return getResultContext().getActiveResultSummary().getCustomPointResults();
  }

  @JsMethod
  public void clickId() {
    if (sortColumn == CustomPointsSortColumn.ID) {
      sortAscending = !sortAscending;
    } else {
      sortColumn = CustomPointsSortColumn.ID;
      sortAscending = true;
    }
  }

  @JsMethod
  public void clickValue() {
    if (sortColumn == CustomPointsSortColumn.VALUE) {
      sortAscending = !sortAscending;
    } else {
      sortColumn = CustomPointsSortColumn.VALUE;
      sortAscending = false;
    }
  }

  @JsMethod
  public String getColorForValue(final double value) {
    return colorRangeService.getColorForResult(value);
  }

  @JsMethod
  public String getCustomPointName(final int customPointId) {
    return customPointNames.get(customPointId);
  }

  @JsMethod
  public void clickPoint(final CustomCalculationPointResult point) {
    if (point.hasResult()) {
      eventBus.fireEvent(new SelectCustomCalculationPointResultCommand(point,
          SelectCustomCalculationPointResultCommand.SelectionSource.LIST));
    }
  }

  @JsMethod
  public boolean isSelected(final CustomCalculationPointResult point) {
    return resultSelectionContext.getSelectedCustomPointResult() == point;
  }

  @Computed
  public CalculationExecutionContext getCalculationExecution() {
    return calculationContext.getActiveCalculation();
  }

  @EventHandler
  public void onSelectCustomCalculationPointResultCommand(final SelectCustomCalculationPointResultCommand command) {
    if (command.isSelectedFromMap()) {
      ((Element[]) vue().$ref(refForPoint(command.getValue())))[0].scrollIntoView();
    }
  }

  @JsMethod
  public String refForPoint(final CustomCalculationPointResult point) {
    return "point-" + point.getCustomPointId();
  }

  @Override
  public void created() {
    handlers = EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Override
  public void destroyed() {
    handlers.removeHandler();
  }

  @Override
  public void mounted() {
    customPointNames = new HashMap<>();
    updateCustomPointLabels();
  }

  protected void updateCustomPointLabels() {
    scenarioContext.getCalculationPoints().forEach(cp -> customPointNames.put(cp.getCustomPointId(), cp.getLabel()));
  }
}
