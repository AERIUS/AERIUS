/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.road;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.wui.application.domain.source.base.LinearReference;

/**
 * Client side implementation of SRM1LinearReference props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class SRM1LinearReference extends LinearReference {

  private Double tunnelFactor;

  public static final @JsOverlay SRM1LinearReference create() {
    final SRM1LinearReference props = new SRM1LinearReference();
    props.setFromPosition(0.0);
    props.setToPosition(0.0);
    props.setTunnelFactor(1.0);
    return props;
  }

  public final @JsOverlay Double getTunnelFactor() {
    return tunnelFactor;
  }

  public final @JsOverlay void setTunnelFactor(final Double tunnelFactor) {
    this.tunnelFactor = tunnelFactor;
  }

}
