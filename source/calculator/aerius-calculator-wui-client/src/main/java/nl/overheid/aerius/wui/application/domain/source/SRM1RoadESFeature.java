/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadManager;
import nl.overheid.aerius.wui.application.domain.source.road.Vehicles;
/**
 * feature object for SRM1 Road Emission sources.
 */
@JsType(namespace = GLOBAL, name = "ol.Feature", isNative = true)
public class SRM1RoadESFeature extends RoadESFeature {

  /**
   * @return Returns a new {@link SRM1RoadESFeature} object.
   */
  public static @JsOverlay SRM1RoadESFeature create() {
    final SRM1RoadESFeature feature = new SRM1RoadESFeature();
    init(feature);

    // Not entirely clear if this does anything currently, setting to PROVINCE to make sure the previous behaviour is preserved
    feature.setRoadManager(RoadManager.PROVINCE);
    return feature;
  }

  public static @JsOverlay void init(final SRM1RoadESFeature feature) {
    RoadESFeature.initRoadFeature(feature, null, EmissionSourceType.SRM1_ROAD);
    EmissionSubSourceFeature.initSubSources(feature, v -> Vehicles.initTypeDependent(v, v.getVehicleType()));
  }

}
