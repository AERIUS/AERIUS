/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.print.nca.results;

import java.util.ArrayList;
import java.util.List;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.application.print.common.PrintSection;

@Component(components = {
    PrintSection.class,
})
public class NcaPrintAreaResultsComponent implements IsVueComponent {
  @Prop(required = true) EmissionResultKey substance;

  @JsMethod
  public List<String> getShortDummyContent(final int paragraphs) {
    final String baseParagraph = "Lorem ipsum";
    final List<String> lst = new ArrayList<>();
    for (int i = 0; i < paragraphs; i++) {
      lst.add(baseParagraph);
    }
    return lst;
  }
}
