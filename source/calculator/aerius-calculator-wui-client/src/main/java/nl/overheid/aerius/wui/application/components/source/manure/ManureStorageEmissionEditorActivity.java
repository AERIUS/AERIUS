/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.manure;

import elemental2.core.JsArray;

import nl.overheid.aerius.wui.application.components.modify.ModifyListActions;
import nl.overheid.aerius.wui.application.components.source.SubSourceEmissionEditorPresenter;
import nl.overheid.aerius.wui.application.domain.source.manure.ManureStorage;
import nl.overheid.aerius.wui.application.domain.source.manure.StandardManureStorage;

public class ManureStorageEmissionEditorActivity extends SubSourceEmissionEditorPresenter<ManureStorageEmissionEditorComponent, ManureStorage>
    implements ModifyListActions {
  @Override
  protected ManureStorage createSubSource() {
    return StandardManureStorage.create();
  }

  @Override
  protected JsArray<ManureStorage> getSubSources() {
    return view.source.getSubSources();
  }
}
