/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.shipping.inland;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.wui.vue.ReactivityUtil;

/**
 * Client side implementation of CustomInlandShipping props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class CustomInlandShipping extends InlandShipping {

  private CustomInlandShippingEmissionProperties emissionPropertiesAtoB;
  private CustomInlandShippingEmissionProperties emissionPropertiesBtoA;

  public static final @JsOverlay CustomInlandShipping create() {
    final CustomInlandShipping props = Js.uncheckedCast(JsPropertyMap.of());
    init(props);
    return props;
  }

  public static final @JsOverlay void init(final CustomInlandShipping props) {
    InlandShipping.initInlandShipping(props, InlandShippingType.CUSTOM);
    ReactivityUtil.ensureJsPropertySupplied(props, "emissionPropertiesAtoB", props::setEmissionPropertiesAtoB,
        CustomInlandShippingEmissionProperties::create);
    ReactivityUtil.ensureJsPropertySupplied(props, "emissionPropertiesBtoA", props::setEmissionPropertiesBtoA,
        CustomInlandShippingEmissionProperties::create);

    ReactivityUtil.ensureInitialized(props::getEmissionPropertiesAtoB, CustomInlandShippingEmissionProperties::init);
    ReactivityUtil.ensureInitialized(props::getEmissionPropertiesBtoA, CustomInlandShippingEmissionProperties::init);
  }

  public final @JsOverlay CustomInlandShippingEmissionProperties getEmissionPropertiesAtoB() {
    return emissionPropertiesAtoB;
  }

  public final @JsOverlay void setEmissionPropertiesAtoB(final CustomInlandShippingEmissionProperties emissionPropertiesAtoB) {
    this.emissionPropertiesAtoB = emissionPropertiesAtoB;
  }

  public final @JsOverlay CustomInlandShippingEmissionProperties getEmissionPropertiesBtoA() {
    return emissionPropertiesBtoA;
  }

  public final @JsOverlay void setEmissionPropertiesBtoA(final CustomInlandShippingEmissionProperties emissionPropertiesBtoA) {
    this.emissionPropertiesBtoA = emissionPropertiesBtoA;
  }

}
