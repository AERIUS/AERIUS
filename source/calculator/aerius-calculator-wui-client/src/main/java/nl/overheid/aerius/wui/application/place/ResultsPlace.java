/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.place;

import java.util.function.Supplier;

import nl.aerius.wui.place.PlaceTokenizer;
import nl.overheid.aerius.shared.domain.Theme;

/**
 * Place to display theme calculation results.
 */
public class ResultsPlace extends MainThemePlace {

  private static final String PLACE_KEY_RESULTS = "results";

  public static class Tokenizer extends MainThemePlace.Tokenizer<ResultsPlace> {
    public Tokenizer(final Supplier<ResultsPlace> provider, final Theme theme, final String... postfix) {
      super(provider, theme, postfix);
    }
  }

  public <P extends MainThemePlace> ResultsPlace(final Theme theme) {
    this(createTokenizer(theme));
  }

  public <P extends MainThemePlace> ResultsPlace(final PlaceTokenizer<P> tokenizer) {
    super(tokenizer);
  }

  public static PlaceTokenizer<ResultsPlace> createTokenizer(final Theme theme) {
    return new Tokenizer(() -> new ResultsPlace(theme), theme, PLACE_KEY_RESULTS);
  }

  @Override
  public <T> T visit(final PlaceVisitor<T> visitor) {
    return visitor.apply(this);
  }
}
