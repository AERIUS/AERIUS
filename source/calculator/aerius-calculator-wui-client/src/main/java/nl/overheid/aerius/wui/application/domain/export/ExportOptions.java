/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.domain.export;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import elemental2.core.Global;

import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.export.ExportAppendix;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.wui.application.util.json.JsonBuilder;
import nl.overheid.aerius.wui.application.util.json.JsonSerializable;

@JsType
public class ExportOptions implements JsonSerializable {

  private @JsProperty String jobType = JobType.REPORT.name();
  private @JsProperty String name;
  private @JsProperty String email;
  private @JsProperty boolean protectJob = false;
  private @JsProperty String exportType;
  private @JsProperty Set<ExportAppendix> appendices = new HashSet<>();
  private @JsProperty boolean demoMode = false;

  public JobType getJobType() {
    return JobType.valueOf(jobType);
  }

  public void setJobType(final JobType jobType) {
    this.jobType = jobType.name();
  }

  public void setProtectJob(final boolean protectJob) {
    this.protectJob = protectJob;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(final String email) {
    this.email = email;
  }

  public ExportType getExportType() {
    return ExportType.valueOf(exportType);
  }

  public void setExportType(final ExportType exportType) {
    this.exportType = exportType.name();
  }

  public Set<ExportAppendix> getAppendices() {
    return appendices;
  }

  public void setAppendices(final Set<ExportAppendix> appendices) {
    this.appendices = appendices;
  }

  public boolean isDemoMode() {
    return demoMode;
  }

  public void setDemoMode(final boolean demoMode) {
    this.demoMode = demoMode;
  }

  @Override
  public Object toJSON() {
    return new JsonBuilder(this)
        .include("jobType", "name", "email", "protectJob", "exportType", "demoMode")
        .set("appendices", Global.JSON.parse("["
            + appendices.stream()
                .map(s -> "\"" + s.name() + "\"")
                .collect(Collectors.joining(","))
            + "]"))
        .build();
  }
}
