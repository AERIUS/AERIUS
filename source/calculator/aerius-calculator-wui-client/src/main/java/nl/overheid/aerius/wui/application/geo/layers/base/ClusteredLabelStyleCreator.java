/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.base;

import java.util.ArrayList;
import java.util.List;
import java.util.function.DoubleSupplier;

import ol.style.Style;

import nl.overheid.aerius.wui.application.domain.ImaerFeature;
import nl.overheid.aerius.wui.application.geo.icons.labels.Label;
import nl.overheid.aerius.wui.application.geo.layers.LabelFeature;
import nl.overheid.aerius.wui.application.geo.layers.LabelStyle;
import nl.overheid.aerius.wui.application.geo.layers.LayerStyleUtil;

/**
 * Creates the styling for a clustered layer. The markers on the marker layer are features converted to LabelFeature objects.
 */
public class ClusteredLabelStyleCreator<F extends ImaerFeature> extends LabelStyleCreator<F> {

  private static final String CLUSTERED_LABEL_BACKGROUND_COLOR = "#333333";
  private static final Character[] EMPTY_INDICATORS = new Character[0];

  public ClusteredLabelStyleCreator(final DoubleSupplier iconScale, final boolean hasOutline, final LabelStyle labelStyle) {
    super(iconScale, hasOutline, labelStyle);
  }

  @Override
  public Style[] createStyle(final ImaerFeature feature, final double resolution) {
    final LabelFeature[] features = feature.get("features");

    if (features == null || features.length == 0) {
      // This method can be called without any sources. So we just return null as
      // there is nothing to show.
      return null;
    }
    return createClusteredStyles(feature, features, resolution, getLabelStyle());
  }

  /**
   * Creates the style for all features clustered or unclustered.
   *
   * @param feature feature feature to use geometry of
   * @param features list of features either clusters them or creates a label per feature
   * @param resolution icon resolution
   * @param labelStyle style of label
   * @return new Style object
   */
  protected Style[] createClusteredStyles(final ImaerFeature feature, final LabelFeature[] features, final double resolution,
      final LabelStyle labelStyle) {
    final boolean isClustered = features.length > 1 && resolution > LayerStyleUtil.CLUSTER_MINIMUM_RESOLUTION;
    final List<Label> markers = new ArrayList<>();

    if (isClustered) {
      markers.add(createClusteredLabel(features, labelStyle, EMPTY_INDICATORS));
    } else {
      for (final LabelFeature f : features) {
        markers.add(createUnclusteredLabel(f, labelStyle));
      }
    }
    return createStyle(feature, markers, labelStyle);
  }

  /**
   * Creates a clusted label from the given features.
   *
   * @param features features to cluster
   * @param labelStyle style to use for the label
   * @param indicators indicators to display on the label
   * @return new Label
   */
  protected Label createClusteredLabel(final LabelFeature[] features, final LabelStyle labelStyle, final Character[] indicators) {
    return new Label(clusterLabel(features, labelStyle), getClusteredLabelBackground(features, labelStyle), labelStyle.getLabelTextColor(),
        indicators);
  }

  /**
   * Creates an unclusted label.
   *
   * @param feature feature feature to use geometry of
   * @param labelStyle style to use for the label
   * @param indicators indicators to display on the label
   * @return new Label
   */
  protected Label createUnclusteredLabel(final LabelFeature feature, final LabelStyle labelStyle) {
    return LayerStyleUtil.createLabel(feature.getReferenceFeature(), createLabelName(feature), labelStyle);
  }

  /**
   * Creates a Style object given a list of Label objects.
   *
   * @param feature feature feature to use geometry of
   * @param labels labels to construct the style for
   * @param labelStyle style to use for the label
   * @param labelStyle
   * @return new Style object
   */
  protected Style[] createStyle(final ImaerFeature feature, final List<Label> labels, final LabelStyle labelStyle) {
    return LayerStyleUtil.createStyle(feature, labels, labelStyle.getLabelIconStyle(), isHasOutline(), getIconScale());
  }

  /**
   * Returns the name to put on the clustered label. By default uses number of features as String.
   *
   * @param features in cluster
   * @param labelStyle label style (used if label needs to be different based on the style).
   * @return clustered label name
   */
  protected String clusterLabel(final LabelFeature[] features, final LabelStyle labelStyle) {
    return clusterLabel(features.length, labelStyle);
  }

  /**
   * Returns the name to put on the clustered label. Returns the number of features as String.
   *
   * @param count number of features in cluster
   * @param labelStyle label style (used if label needs to be different based on the style).
   * @return clustered label name
   */
  protected String clusterLabel(final int count, final LabelStyle labelStyle) {
    return String.valueOf(count);
  }

  private static String getClusteredLabelBackground(final LabelFeature[] sources, final LabelStyle labelStyle) {
    final String clusteredColor = labelStyle.getLabelBackgroundColor(sources[0].getReferenceFeature());

    for (int i = 1; i < sources.length; i++) {
      if (!clusteredColor.equals(labelStyle.getLabelBackgroundColor(sources[i].getReferenceFeature()))) {
        return CLUSTERED_LABEL_BACKGROUND_COLOR;
      }
    }
    return clusteredColor;
  }
}
