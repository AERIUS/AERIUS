/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.command.misc;

public enum ColorTheme {
  DEFAULT_BLUE("#1579A0", "#D0E5EF"),
  UK_BLUE("#1d70b8", "#c5dee7"),
  ORANGE("#D16326", "#FFE5CC");

  private final String themeColor;
  private final String themeColorLight;

  private ColorTheme(final String theme, final String themeLight) {
    this.themeColor = theme;
    this.themeColorLight = themeLight;
  }

  public String getThemeColor() {
    return themeColor;
  }

  public String getThemeColorLight() {
    return themeColorLight;
  }
}
