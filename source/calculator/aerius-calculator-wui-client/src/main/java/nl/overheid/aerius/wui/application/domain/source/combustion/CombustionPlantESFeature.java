/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.combustion;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.domain.source.EmissionSubSourceFeature;
import nl.overheid.aerius.wui.vue.ReactivityUtil;

/**
 * Feature object for Medium Combustion Plant Sources.
 */
@JsType(namespace = GLOBAL, name = "ol.Feature", isNative = true)
public class CombustionPlantESFeature extends EmissionSubSourceFeature<CombustionPlantActivity> {
  /**
   * @return Returns a new {@link CombustionPlantESFeature} object.
   */
  public static @JsOverlay CombustionPlantESFeature create() {
    final CombustionPlantESFeature feature = new CombustionPlantESFeature();
    init(feature);
    return feature;
  }

  public static final @JsOverlay void init(final CombustionPlantESFeature feature) {
    EmissionSubSourceFeature.initSubSourceFeature(feature, CharacteristicsType.ADMS, EmissionSourceType.MEDIUM_COMBUSTION_PLANT);
    ReactivityUtil.ensureDefault(feature::getArea, feature::setArea);

    EmissionSubSourceFeature.initSubSources(feature, v -> CombustionPlantActivity.init(v));
  }

  public final @JsOverlay String getArea() {
    return get("area");
  }

  public final @JsOverlay void setArea(final String area) {
    set("area", area);
  }
}
