/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.importer;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import ol.format.GeoJson;

import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.SourceType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.ClientConsecutiveIdUtil;
import nl.overheid.aerius.wui.application.domain.source.ADMSCharacteristics;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.HasMoorings;
import nl.overheid.aerius.wui.application.domain.source.OPSCharacteristics;
import nl.overheid.aerius.wui.application.domain.source.TimeVaryingProfile;

public class ImportParcelConverter {
  private static final GeoJson GEO_JSON = new GeoJson();

  private static final Predicate<EmissionSourceFeature> IS_SHIPPING_FEATURE = v -> v
      .getEmissionSourceType() == EmissionSourceType.SHIPPING_MARITIME_MARITIME
      || v.getEmissionSourceType() == EmissionSourceType.SHIPPING_MARITIME_INLAND
      || v.getEmissionSourceType() == EmissionSourceType.SHIPPING_INLAND;

  private static final Predicate<EmissionSourceFeature> IS_MOORING = v -> v
      .getEmissionSourceType() == EmissionSourceType.SHIPPING_INLAND_DOCKED
      || v.getEmissionSourceType() == EmissionSourceType.SHIPPING_MARITIME_DOCKED;

  public SituationContext createSituationFromParcel(final ImportParcelSituation situation, final SituationContext situationContext) {
    final String newSituationName = situation.getName();
    if (newSituationName != null && !newSituationName.isEmpty()) {
      situationContext.setName(newSituationName);
    }

    SituationContext.safeSetSituationType(situationContext, situation.getType());
    if (situation.getYear() != 0) {
      situationContext.setYear(situation.getYear());
    }
    situationContext.setNettingFactor(situation.getNettingFactor());

    addParcelToSituation(situation, situationContext, ImportFilters.all());
    return situationContext;
  }

  public CalculationPointFeature[] getCalculationPoints(final Object unparsedCalculationPoints) {
    return (CalculationPointFeature[]) GEO_JSON.readFeatures(unparsedCalculationPoints);
  }

  public void addParcelToSituation(final ImportParcelSituation parcel, final SituationContext situation, final ImportFilters filters) {
    final InternalImportProperties importProps = new InternalImportProperties();
    importProps.allowImportBuildings = filters.isAllowed(ImportFilter.BUILDINGS);
    importProps.allowImportTimeVaryingProfiles = filters.isAllowed(ImportFilter.TIME_VARYING_PROFILES);

    if (importProps.allowImportTimeVaryingProfiles) {
      importProps.timeVaryingProfileRemappings = addTimeVaryingProfiles(parcel, situation);
    }

    importProps.buildingRemappings = ensureUniqueBuildingMapping(parcel, situation);
    importProps.mooringRemappings = ensureUniqueMooringMapping(parcel, situation);

    if (filters.isAllowed(ImportFilter.SOURCES)) {
      addEmissionSources(parcel, situation, importProps);
    }
    if (importProps.allowImportBuildings) {
      addBuildings(parcel, situation, importProps);
    }
  }

  private static Map<String, String> addTimeVaryingProfiles(final ImportParcelSituation parcel, final SituationContext situation) {
    final Map<String, String> remappings = new HashMap<>();

    final List<TimeVaryingProfile> profiles = situation.getOriginalTimeVaryingProfiles();
    final Map<TimeVaryingProfile, String> existingProfilesMap = profiles
        .stream()
        .collect(Collectors.toMap(Function.identity(), v -> v.getGmlId()));
    final Set<String> existingProfileIds = new HashSet<>(existingProfilesMap.values());

    for (final TimeVaryingProfile profile : parcel.getTimeVaryingProfiles()) {
      // Check if the exact profile (label, type and values) already exists in the situation. If so, use that one for all the sources to import.
      // Otherwise add the profile, and make sure it does not have a conflicting GML ID.
      final String existingMatchId = existingProfilesMap.get(profile);
      final String importedId = profile.getGmlId();
      if (existingMatchId != null) {
        remappings.put(importedId, existingMatchId);
      } else {
        ClientConsecutiveIdUtil.addAndSetId(profiles, profile);
        if (existingProfileIds.contains(importedId)) {
          final String newId = produceUniqueNewKey(importedId, existingProfileIds);
          profile.setGmlId(newId);
          remappings.put(importedId, newId);
        }
      }
    }
    return remappings;
  }

  private static Map<String, String> ensureUniqueMooringMapping(final ImportParcelSituation parcel,
      final SituationContext situation) {
    if (parcel.getEmissionSources() == null) {
      return new HashMap<>();
    }
    final Stream<String> parcelMoores = Stream.of((EmissionSourceFeature[]) GEO_JSON.readFeatures(parcel.getEmissionSources()))
        .filter(IS_MOORING)
        .map(v -> v.getGmlId());

    final Set<String> existingMoores = situation.getSources().stream()
        .filter(IS_MOORING)
        .map(v -> v.getGmlId())
        .collect(Collectors.toSet());

    return ensureUniqueIdMapping(parcelMoores, existingMoores);
  }

  /**
   * Return a map specifying the ids that must be remapped to another value.
   */
  private static Map<String, String> ensureUniqueBuildingMapping(final ImportParcelSituation parcel, final SituationContext situation) {
    if (parcel.getBuildings() == null) {
      return new HashMap<>();
    }
    final Stream<String> parcelBuildingIds = Stream.of((BuildingFeature[]) GEO_JSON.readFeatures(parcel.getBuildings()))
        .map(v -> v.getGmlId());

    final Set<String> existingBuildingIds = situation.getBuildings()
        .stream().map(v -> v.getGmlId())
        .collect(Collectors.toSet());

    return ensureUniqueIdMapping(parcelBuildingIds, existingBuildingIds);
  }

  private static Map<String, String> ensureUniqueIdMapping(final Stream<String> parcelIds,
      final Set<String> existingIds) {
    return parcelIds
        .filter(existingIds::contains)
        .collect(Collectors.toMap(v -> v, v -> produceUniqueNewKey(v, existingIds)));
  }

  /**
   * Recursively increment the key with a remapping postfix in order to ensure a
   * unique key is produced.
   *
   * @return a unique key
   */
  private static String produceUniqueNewKey(final String key, final Set<String> existingKeys) {
    final String newKey = smartIncrementedRemapping(key);
    if (existingKeys.contains(newKey)) {
      return produceUniqueNewKey(newKey, existingKeys);
    } else {
      return newKey;
    }
  }

  private static String smartIncrementedRemapping(final String key) {
    if (key.matches(".+-remap-[1-9]+$")) {
      final int endBracket = key.lastIndexOf("-");
      final String numPortion = key.substring(endBracket + 1);

      try {
        final int num = Integer.parseInt(numPortion);
        final String labelBare = key.substring(0, key.lastIndexOf("-") + 1);
        return labelBare + (num + 1);
      } catch (final NumberFormatException e) {
        return key + "-remap-1";
      }
    } else {
      return key + "-remap-1";
    }
  }

  private static void addEmissionSources(final ImportParcelSituation parcel, final SituationContext situation,
      final InternalImportProperties importProps) {
    final EmissionSourceFeature[] sources = (EmissionSourceFeature[]) GEO_JSON.readFeatures(parcel.getEmissionSources());

    Stream.of(sources).forEach(source -> addEmissionSource(source, situation, importProps));
  }

  private static void addEmissionSource(final EmissionSourceFeature source, final SituationContext situation,
      final InternalImportProperties importProps) {
    if (!importProps.allowImportBuildings) {
      severBuildingLink(source);
    }
    if (!importProps.allowImportTimeVaryingProfiles) {
      severTimeVaryingProfileLink(source);
    }

    // Remove building link if the link is incompatible (e.g. a line source must not have a building link)
    ensureBuildingCompatible(source);

    // Try to remap the building id according to the remappings specified (note:
    // this won't do anything if building links have already been severed)
    tryBuildingRemap(source, importProps.buildingRemappings);

    // Try to remap the mooring ids according to the remappings specificied
    tryMooringRemap(source, importProps.mooringRemappings);

    // Try to remap the time varying emission profile ids according to the remappings specificied
    tryTimeVaryingProfileRemap(source, importProps.timeVaryingProfileRemappings);

    // Post-process the sources so known properties aren't undefined
    ImportParcelInitializer.postProcessEmissionSource(source);

    ClientConsecutiveIdUtil.addAndSetId(situation.getOriginalSources(), source);
  }

  private static void ensureBuildingCompatible(final EmissionSourceFeature source) {
    if (source.getCharacteristicsType() == CharacteristicsType.ADMS
        && ((ADMSCharacteristics) source.getCharacteristics()).getSourceType() != SourceType.POINT) {
      source.getCharacteristics().setBuildingGmlId(null);
    }
  }

  private static void tryMooringRemap(final EmissionSourceFeature source, final Map<String, String> mooringRemappings) {
    if (mooringRemappings == null) {
      return;
    }

    // Remap the moore gml id
    if (IS_MOORING.test(source)) {
      final String sourceGmlId = source.getGmlId();
      if (mooringRemappings.containsKey(sourceGmlId)) {
        source.setGmlId(mooringRemappings.get(sourceGmlId));
      }
    }

    // Remap the shipping feature's moores
    if (IS_SHIPPING_FEATURE.test(source)) {
      final HasMoorings hasMoores = (HasMoorings) source;
      if (mooringRemappings.containsKey(hasMoores.retrieveMooringAId())) {
        hasMoores.persistMooringAId(mooringRemappings.get(hasMoores.retrieveMooringAId()));
      }

      if (mooringRemappings.containsKey(hasMoores.retrieveMooringBId())) {
        hasMoores.persistMooringBId(mooringRemappings.get(hasMoores.retrieveMooringBId()));
      }
    }
  }

  private static void tryBuildingRemap(final EmissionSourceFeature source, final Map<String, String> buildingRemappings) {
    if (buildingRemappings == null) {
      return;
    }

    if (source.getCharacteristicsType() == CharacteristicsType.OPS) {
      final OPSCharacteristics characteristics = (OPSCharacteristics) source.getCharacteristics();

      if (buildingRemappings.containsKey(characteristics.getBuildingGmlId())) {
        characteristics.setBuildingGmlId(buildingRemappings.get(characteristics.getBuildingGmlId()));
      }
    } else if (source.getCharacteristicsType() == CharacteristicsType.ADMS) {
      final ADMSCharacteristics characteristics = (ADMSCharacteristics) source.getCharacteristics();

      if (buildingRemappings.containsKey(characteristics.getBuildingGmlId())) {
        characteristics.setBuildingGmlId(buildingRemappings.get(characteristics.getBuildingGmlId()));
      }
    }
  }

  private static void tryTimeVaryingProfileRemap(final EmissionSourceFeature source, final Map<String, String> remappings) {
    if (remappings == null) {
      return;
    }

    if (source.getCharacteristicsType() == CharacteristicsType.ADMS) {
      final ADMSCharacteristics characteristics = (ADMSCharacteristics) source.getCharacteristics();
      trySpecificTimeVaryingProfileRemap(remappings, characteristics.getCustomHourlyTimeVaryingProfileId(),
          characteristics::setCustomHourlyTimeVaryingProfileId);
      trySpecificTimeVaryingProfileRemap(remappings, characteristics.getCustomMonthlyTimeVaryingProfileId(),
          characteristics::setCustomMonthlyTimeVaryingProfileId);
    }
  }

  private static void trySpecificTimeVaryingProfileRemap(final Map<String, String> remappings, final String value, final Consumer<String> setter) {
    if (value != null && remappings.containsKey(value)) {
      setter.accept(remappings.get(value));
    }
  }

  private static void severBuildingLink(final EmissionSourceFeature source) {
    if (source.getCharacteristicsType() == CharacteristicsType.OPS) {
      ((OPSCharacteristics) source.getCharacteristics()).setBuildingGmlId(null);
    } else if (source.getCharacteristicsType() == CharacteristicsType.ADMS) {
      ((ADMSCharacteristics) source.getCharacteristics()).setBuildingGmlId(null);
    }
  }

  private static void severTimeVaryingProfileLink(final EmissionSourceFeature source) {
    if (source.getCharacteristicsType() == CharacteristicsType.ADMS) {
      ((ADMSCharacteristics) source.getCharacteristics()).setCustomHourlyTimeVaryingProfileId(null);
      ((ADMSCharacteristics) source.getCharacteristics()).setCustomMonthlyTimeVaryingProfileId(null);
    }
  }

  private static void addBuildings(final ImportParcelSituation parcel, final SituationContext situation, final InternalImportProperties importProps) {
    final BuildingFeature[] parcelBuildings = (BuildingFeature[]) GEO_JSON.readFeatures(parcel.getBuildings());
    final List<BuildingFeature> situationBuildings = situation.getOriginalBuildings();
    Stream.of(parcelBuildings).forEach(building -> {
      ClientConsecutiveIdUtil.addAndSetId(situationBuildings, building);

      // Remap the building gml id if it is specified it should be remapped
      if (importProps.buildingRemappings != null && importProps.buildingRemappings.containsKey(building.getGmlId())) {
        building.setGmlId(importProps.buildingRemappings.get(building.getGmlId()));
      }
    });
  }

  private static class InternalImportProperties {

    private boolean allowImportBuildings;
    private boolean allowImportTimeVaryingProfiles;
    private Map<String, String> buildingRemappings;
    private Map<String, String> mooringRemappings;
    private Map<String, String> timeVaryingProfileRemappings;

  }
}
