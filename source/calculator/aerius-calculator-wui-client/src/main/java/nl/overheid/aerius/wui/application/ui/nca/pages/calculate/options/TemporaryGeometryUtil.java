/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.nca.pages.calculate.options;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import ol.Coordinate;
import ol.Feature;
import ol.geom.Point;

import nl.aerius.geo.wui.util.OL3GeometryUtil;
import nl.overheid.aerius.shared.domain.backgrounddata.MetSite;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;

/**
 * Temporary and deprecated because not sure yet if this is the centering algo
 * to use, and until that is less unclear I'd rather keep it out of the
 * gwt-client-geo-ol3 project where this would eventually land
 */
@Deprecated
public final class TemporaryGeometryUtil {
  private static final int SUGGESTION_LIMIT = 6;

  private TemporaryGeometryUtil() {}

  /**
   * Take the simplest middle of the given features: first taking the middle of
   * all features, then the middle of those middles, does not take into
   * account clustering or weighing the points etc.
   */
  public static Point getMiddleOfFeatures(final List<? extends Feature> features) {
    if (features.isEmpty()) {
      return null;
    }

    double sumX = 0;
    double sumY = 0;

    for (final Feature feature : features) {
      final Point middlePoint = OL3GeometryUtil.getMiddlePointOfGeometry(feature.getGeometry());
      final Coordinate coordinates = middlePoint.getCoordinates();
      sumX += coordinates.getX();
      sumY += coordinates.getY();
    }

    final int count = features.size();
    final double averageX = sumX / count;
    final double averageY = sumY / count;
    final Coordinate averageCoordinate = new Coordinate(averageX, averageY);

    return new Point(averageCoordinate);
  }

  private static Point getCentroidOfRelevantScenario(final CalculationJobType calculationJobType, final SituationComposition composition) {
    SituationContext situation;
    switch (calculationJobType) {
    case PROCESS_CONTRIBUTION:
      situation = composition.getSituation(SituationType.PROPOSED);
      break;
    case MAX_TEMPORARY_EFFECT:
      situation = composition.getSituation(SituationType.REFERENCE);
      break;
    case IN_COMBINATION_PROCESS_CONTRIBUTION:
      situation = composition.getSituation(SituationType.PROPOSED);
      break;
    case SINGLE_SCENARIO:
      situation = composition.getSituations().stream().findFirst().orElse(null);
      break;
    default:
    case DEPOSITION_SUM:
      situation = null;
      break;
    }

    if (situation == null) {
      return null;
    }

    return TemporaryGeometryUtil.getMiddleOfFeatures(situation.getSources());
  }

  public static List<MetSiteWithDistance> getSuggestedSiteLocations(final List<MetSite> siteLocations, final CalculationJobType jobType,
      final SituationComposition composition) {
    final Point scenarioCentroid = getCentroidOfRelevantScenario(jobType, composition);
    return scenarioCentroid == null
        ? new ArrayList<>()
        : siteLocations.stream()
            .map(site -> new MetSiteWithDistance(site, calculateDistance(site.getX(), site.getY(),
                scenarioCentroid.getCoordinates().getX(), scenarioCentroid.getCoordinates().getY())))
            .sorted(Comparator.comparingDouble((final MetSiteWithDistance site) -> site.getDistance()))
            .limit(SUGGESTION_LIMIT)
            .collect(Collectors.toList());

  }

  private static double calculateDistance(final double x1, final double y1, final double x2, final double y2) {
    final double deltaX = x1 - x2;
    final double deltaY = y1 - y2;
    return Math.sqrt(deltaX * deltaX + deltaY * deltaY);
  }
}
