/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.misc;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.future.AppAsyncCallback;
import nl.aerius.wui.util.RequestUtil;
import nl.overheid.aerius.wui.application.command.misc.NavigateExternalContentViewCommand;
import nl.overheid.aerius.wui.application.command.misc.RequestExternalContentCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ExternalContentContext;
import nl.overheid.aerius.wui.application.util.ManualUtil;

@Singleton
public class ExternalContentDaemon extends BasicEventComponent {
  private static final ExternalContentDaemonEventBinder EVENT_BINDER = GWT.create(ExternalContentDaemonEventBinder.class);

  interface ExternalContentDaemonEventBinder extends EventBinder<ExternalContentDaemon> {}

  private @Inject ExternalContentContext externalContent;
  private @Inject ApplicationContext appContext;

  @EventHandler
  public void on(final RequestExternalContentCommand c) {
    final String url = c.getValue();
    final String ultimateUrl = RequestUtil.prepareUrl(ManualUtil.getManualUrl(appContext),
        url.replace(".html", ".raw.html"));
    externalContent.setError(c.getValue(), null);
    RequestUtil.doGet(ultimateUrl, AppAsyncCallback.create(
        v -> externalContent.setContent(c.getValue(), v),
        e -> externalContent.setError(c.getValue(), e)));
  }

  @EventHandler
  public void onNavigateExternalContentViewCommand(final NavigateExternalContentViewCommand c) {
    externalContent.setActiveExternalContent(c.getValue());
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
