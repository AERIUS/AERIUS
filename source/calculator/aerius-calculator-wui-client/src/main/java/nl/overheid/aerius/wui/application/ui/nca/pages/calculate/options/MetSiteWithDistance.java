/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.nca.pages.calculate.options;

import nl.overheid.aerius.shared.domain.backgrounddata.MetSite;

public class MetSiteWithDistance {
  private final double distance;
  private final MetSite site;

  public MetSiteWithDistance(final MetSite site, final double distance) {
    super();
    this.distance = distance;
    this.site = site;
  }

  public double getDistance() {
    return distance;
  }

  public MetSite getSite() {
    return site;
  }
}
