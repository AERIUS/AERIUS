/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.scenario;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.wui.application.command.situation.SituationDeleteCommand;
import nl.overheid.aerius.wui.application.command.source.DeselectAllFeaturesCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileAddCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileDeleteAllCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileDeleteCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileDeleteSelectedCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileDeselectCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileDuplicateSelectedCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileEditCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileEditSelectedCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfilePeekFeatureCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileSelectCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileToggleSelectCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileUnpeekFeatureCommand;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.TimeVaryingProfileListContext;
import nl.overheid.aerius.wui.application.domain.geo.FeatureUtil;
import nl.overheid.aerius.wui.application.domain.source.ADMSCharacteristics;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.TimeVaryingProfile;
import nl.overheid.aerius.wui.application.event.SelectionResetEvent;
import nl.overheid.aerius.wui.application.event.situation.SituationSwitchEvent;
import nl.overheid.aerius.wui.application.i18n.M;

public class TimeVaryingProfileListDaemon extends BasicEventComponent {
  private static final TimeVaryingProfileListDaemonEventBinder EVENT_BINDER = GWT.create(TimeVaryingProfileListDaemonEventBinder.class);

  interface TimeVaryingProfileListDaemonEventBinder extends EventBinder<TimeVaryingProfileListDaemon> {}


  @Inject private TimeVaryingProfileListContext context;
  @Inject private ScenarioContext scenarioContext;

  @EventHandler
  public void onDeselectAllFeaturesCommand(final DeselectAllFeaturesCommand c) {
    resetSelection();
  }

  @EventHandler
  public void onTimeVaryingProfileEditSelectedCommand(final TimeVaryingProfileEditSelectedCommand c) {
    if (!context.hasSingleSelection()) {
      return;
    }

    eventBus.fireEvent(new TimeVaryingProfileEditCommand(context.getSingleSelection()));
  }

  @EventHandler
  public void onSituationSwitchEvent(final SituationSwitchEvent e) {
    resetSelection();
  }

  @EventHandler
  public void onTimeVaryingProfileDuplicateSelectedCommand(final TimeVaryingProfileDuplicateSelectedCommand c) {
    final List<TimeVaryingProfile> newSelection = new ArrayList<>();
    final Set<String> existingLabels = getExistingLabels();

    context.getSelections().forEach(profile -> {
      final TimeVaryingProfile copy = profile.copy();
      copy.setLabel(FeatureUtil.smartIncrementLabel(copy.getLabel(), existingLabels));
      existingLabels.add(copy.getLabel());

      copy.setDescription(null);
      copy.setSystemProfile(false);
      eventBus.fireEvent(new TimeVaryingProfileAddCommand(copy));
      newSelection.add(copy);
    });

    // Delay because of what is described in EmissionSourceEditorDaemon:120
    SchedulerUtil.delay(() -> {
      resetSelection();
      newSelection.forEach(v -> context.addSelection(v));
    });
  }

  private Set<String> getExistingLabels() {
    return scenarioContext.getActiveSituation().getSources().stream()
        .map(source -> source.getLabel())
        .collect(Collectors.toSet());
  }

  @EventHandler
  public void onTimeVaryingProfileDeleteSelectedCommand(final TimeVaryingProfileDeleteSelectedCommand c) {
    if (!context.hasSelection()) {
      return;
    }
    final List<EmissionSourceFeature> admsSources = ScenarioContext
        .findADMSCharacteristicsSources(scenarioContext, scenarioContext.getActiveSituationId()).collect(Collectors.toList());
    final String confirm = getRemoveConfirmations(admsSources, context.getSelections().stream());

    if (confirm.isEmpty() || Window.confirm(confirm)) {
      context.getSelections().forEach(v -> eventBus.fireEvent(new TimeVaryingProfileDeleteCommand(v)));
      resetSelection();
    }
  }

  @EventHandler
  public void onTimeVaryingProfileToggleSelectCommand(final TimeVaryingProfileToggleSelectCommand c) {
    final TimeVaryingProfile profile = c.getValue();

    if (context.isSelected(profile)) {
      eventBus.fireEvent(new TimeVaryingProfileDeselectCommand(profile));
    } else {
      eventBus.fireEvent(new TimeVaryingProfileSelectCommand(profile, c.isMulti()));
    }
  }

  @EventHandler
  public void onTimeVaryingProfileDeselectCommand(final TimeVaryingProfileDeselectCommand c) {
    context.removeSelection(c.getValue());
  }

  @EventHandler
  public void onTimeVaryingProfileSelectCommand(final TimeVaryingProfileSelectCommand c) {
    final TimeVaryingProfile profile = c.getValue();

    if (c.isMulti()) {
      context.addSelection(profile);
    } else {
      context.setSingleSelection(profile);
    }
  }

  @EventHandler
  public void onTimeVaryingProfilePeekFeatureCommand(final TimeVaryingProfilePeekFeatureCommand c) {
    context.setPeekSelection(c.getValue());
  }

  @EventHandler
  public void onTimeVaryingProfileUnpeekFeatureCommand(final TimeVaryingProfileUnpeekFeatureCommand c) {
    SchedulerUtil.delay(() -> context.clearPeekSelection(c.getValue()));
  }

  @EventHandler
  public void onDeleteAllTimeVaryingProfilesCommand(final TimeVaryingProfileDeleteAllCommand c) {
    final SituationContext situation = scenarioContext.getActiveSituation();
    final List<EmissionSourceFeature> admsSources = ScenarioContext
        .findADMSCharacteristicsSources(scenarioContext, situation.getId()).collect(Collectors.toList());
    final String confirm = getRemoveConfirmations(admsSources, situation.getTimeVaryingProfiles().stream());

    if (confirm.isEmpty() || Window.confirm(confirm)) {
      admsSources.forEach(s -> {
        ((ADMSCharacteristics) s.getCharacteristics()).setCustomHourlyTimeVaryingProfileId(null);
        ((ADMSCharacteristics) s.getCharacteristics()).setCustomMonthlyTimeVaryingProfileId(null);
      });

      situation.getOriginalTimeVaryingProfiles().clear();
      resetSelection();
    }
  }

  @EventHandler
  public void onSituationDeleteCommand(final SituationDeleteCommand c) {
    if (c.getValue().getTimeVaryingProfiles().contains(context.getSingleSelection())) {
      resetSelection();
    }
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }

  private String getRemoveConfirmations(final List<EmissionSourceFeature> admsSources, final Stream<TimeVaryingProfile> profiles) {
    return profiles
        .map(profile -> {
          // Do a comprehensive search of all sources, to see if this profile is linked
          final List<EmissionSourceFeature> sourceMatches = admsSources.stream()
              .filter(s -> profile.getGmlId().equals(((ADMSCharacteristics) s.getCharacteristics()).getCustomHourlyTimeVaryingProfileId()) ||
                  profile.getGmlId().equals(((ADMSCharacteristics) s.getCharacteristics()).getCustomMonthlyTimeVaryingProfileId()))
              .collect(Collectors.toList());

          if (sourceMatches.isEmpty()) {
            return null;
          } else if (sourceMatches.size() == 1) {
            final EmissionSourceFeature src = sourceMatches.get(0);
            return M.messages().timeVaryingProfileRemoveConfirm(profile.getGmlId(), profile.getLabel(), src.getId(), src.getLabel());
          } else {
            return M.messages().timeVaryingProfileRemoveMultipleConfirm(profile.getGmlId(), profile.getLabel(), sourceMatches.size());
          }
        })
        .filter(v -> v != null)
        .collect(Collectors.joining("\n\n"));
  }

  private void resetSelection() {
    context.reset();
    eventBus.fireEvent(new SelectionResetEvent());
  }
}
