/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.i18n;

import com.google.gwt.i18n.client.Messages.Select;

import nl.overheid.aerius.shared.domain.v2.point.AssessmentCategory;

public interface CalculationPointMessages {

  String calculationPointHabitat();
  String calculationPointAssignHabitatTitle();
  String calculationPointHabitatDefaultLabel();
  String calculationPointHabitatDetailCL();
  String calculationPointHabitatDetailCLNOx();
  String calculationPointHabitatDetailCLNH3();
  String calculationPointHabitatDetailInclude();
  String calculationPointHabitatDetailName();
  String calculationPointHabitatDetailTitle();
  String calculationPointId();
  String calculationPointName();
  String calculationPointsMayStop();
  String calculationPointsCustomLabel();
  String calculationPointsDefaultLabel(String nextLabel);
  String calculationPointsEditTitle();
  String calculationPointsNewHabitatTitle();
  String calculationPointsNewTitle();
  String calculationPointsLocation();
  String calculationPointsLocationButtonTitle();
  String calculationPointsLocationDrawExplain();
  String calculationPointsLocationDirectExplain();
  String calculationPointsPlacementTitle();
  String calculationPointsPlacementType();
  String calculationPointsPlacementTypeInland();
  String calculationPointsPlacementTypeAbroad();
  String calculationPointsPlacementSituation();
  String calculationPointsPlacementErrorNoOptionSelected();
  String calculationPointsPlacementNoSources();
  String calculationPointsPlacementWarningBusy();
  String calculationPointsPlacementButtonDetermine();
  String calculationPointsPlacementButtonAccept();
  String calculationPointsPlacementButtonCancel();
  String calculationPointsPlacementResultText(String numberOfPoints, String numberOfAreas);
  String calculationPointsPlacementNoResultText();
  String calculationPointsPlacementInlandRadius();
  String calculationPointsDeleteButton();
  String calculationPointsDeleteWarning();
  String calculationPointsEmptyInfo();
  String calculationPointsHabitatEmptyInfo();
  String calculationPointsLayerTitle();

  String calculationPointsCharacteristicsTitle();
  String calculationPointsCharacteristicsCategoryTitle();
  String calculationPointsCharacteristicsCategory(@Select AssessmentCategory assessmentCategory);
  String calculationPointsCharacteristicsCategoryNone();
  String calculationPointsCharacteristicsCategoryExplainText();
  String calculationPointsCharacteristicsHeight();
  String calculationPointsCharacteristicsFractionNO2();

  String importCalculationPointDuplicatesMessage();
  String calculationPointMarkerClusterLabel(int numberOfPoints);
}
