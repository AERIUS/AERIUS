/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.farmland.standard;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.aerius.vuelidate.ValidationOptions;
import nl.aerius.vuelidate.Validations;
import nl.aerius.vuelidate.ValidatorBuilder;
import nl.overheid.aerius.shared.ImaerConstants;

/**
 * Validators for FarmlandStandard editing.
 */
class FarmlandStandardValidators extends ValidationOptions<FarmlandStandardEmissionComponent> {

  protected static final int MIN_NUMBER_OF_ANIMALS = 0;
  protected static final int MAX_NUMBER_OF_ANIMALS = 100_000;
  protected static final int MIN_NUMBER_OF_DAYS = 1;
  protected static final int MAX_NUMBER_OF_DAYS = ImaerConstants.DAYS_PER_YEAR;
  protected static final int MIN_TONNES = 0;
  protected static final int MAX_TONNES = 1_000_000;
  protected static final int MIN_METERS_CUBED = 0;
  protected static final int MAX_METERS_CUBED = 1_000_000;
  protected static final int MIN_NUMBER_OF_APPLICATIONS = 0;
  protected static final int MAX_NUMBER_OF_APPLICATIONS = 365;

  @JsType(isNative = true, namespace = JsPackage.GLOBAL)
  static class FarmlandStandardValidations extends Validations {
    public @JsProperty Validations farmlandCategoryCodeV;
    public @JsProperty Validations numberOfAnimalsV;
    public @JsProperty Validations numberOfDaysV;
    public @JsProperty Validations tonnesV;
    public @JsProperty Validations metersCubedV;
    public @JsProperty Validations numberOfApplicationsV;
  }

  @Override
  protected void constructValidators(final Object options, final ValidatorInstaller v) {
    final FarmlandStandardEmissionComponent instance = Js.uncheckedCast(options);

    v.install("farmlandCategoryCodeV", () -> instance.farmlandCategoryCodeV = null,
        ValidatorBuilder.create()
            .required());
    v.install("numberOfAnimalsV", () -> instance.numberOfAnimalsV = null,
        ValidatorBuilder.create()
            .required()
            .integer()
            .minValue(MIN_NUMBER_OF_ANIMALS)
            .maxValue(MAX_NUMBER_OF_ANIMALS));
    v.install("numberOfDaysV", () -> instance.numberOfDaysV = null,
        ValidatorBuilder.create()
            .required()
            .integer()
            .minValue(MIN_NUMBER_OF_DAYS)
            .maxValue(MAX_NUMBER_OF_DAYS));
    v.install("tonnesV", () -> instance.tonnesV = null,
        ValidatorBuilder.create()
            .required()
            .decimal()
            .minValue(MIN_TONNES)
            .maxValue(MAX_TONNES));
    v.install("metersCubedV", () -> instance.metersCubedV = null,
        ValidatorBuilder.create()
            .required()
            .decimal()
            .minValue(MIN_METERS_CUBED)
            .maxValue(MAX_METERS_CUBED));
    v.install("numberOfApplicationsV", () -> instance.numberOfApplicationsV = null,
        ValidatorBuilder.create()
            .required()
            .integer()
            .minValue(MIN_NUMBER_OF_APPLICATIONS)
            .maxValue(MAX_NUMBER_OF_APPLICATIONS));
  }
}
