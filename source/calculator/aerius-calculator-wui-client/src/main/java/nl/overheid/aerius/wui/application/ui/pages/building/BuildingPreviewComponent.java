/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.building;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.v2.building.BuildingLimits;
import nl.overheid.aerius.wui.application.geo.util.OrientedEnvelope;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    VerticalCollapse.class
})
public class BuildingPreviewComponent extends BasicVueComponent {
  @Prop BuildingLimits buildingLimits;
  @Prop OrientedEnvelope envelope;
  @Prop String backgroundColor;

  @PropDefault("backgroundColor")
  public String backgroundColorDefault() {
    return "var(--back-color)";
  }

  @Computed
  public String getAspectRatio() {
    return envelope.getLength() + " / " + envelope.getWidth();
  }

  @Computed
  public String getWidth() {
    return i18n.unitM(i18n.decimalNumberFixed(envelope.getWidth(), buildingLimits.buildingDigitsPrecision()));
  }

  @Computed
  public String getLength() {
    return i18n.unitM(i18n.decimalNumberFixed(envelope.getLength(), buildingLimits.buildingDigitsPrecision()));
  }
}
