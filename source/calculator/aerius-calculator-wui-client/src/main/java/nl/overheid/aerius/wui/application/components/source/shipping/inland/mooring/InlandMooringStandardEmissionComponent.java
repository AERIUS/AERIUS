/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.shipping.inland.mooring;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.inland.mooring.InlandMooringStandardValidators.InlandMooringStandardValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.shipping.inland.StandardMooringInlandShipping;
import nl.overheid.aerius.wui.application.util.NumberUtil;
import nl.overheid.aerius.wui.vue.DebugDirective;

@Component(name = "aer-inland-mooring-standard", customizeOptions = {
    InlandMooringStandardValidators.class
}, directives = {
    ValidateDirective.class,
    DebugDirective.class
}, components = {
    LabeledInputComponent.class,
    InlandMooringVisitsComponent.class,
    ValidationBehaviour.class
})
public class InlandMooringStandardEmissionComponent extends ErrorWarningValidator implements HasCreated,
    HasValidators<InlandMooringStandardValidations> {
  @Prop @JsProperty StandardMooringInlandShipping ship;
  @Data String descriptionV;
  @Data String shipCodeV;
  @Data String averageResidenceTimeV;
  @Data String shorePowerFactorV;
  @Data String percentageLadenV;

  @Data @Inject ApplicationContext applicationContext;

  @JsProperty(name = "$v") InlandMooringStandardValidations validation;

  @Computed
  public InlandMooringStandardValidations getV() {
    return validation;
  }

  @Watch(value = "ship", isImmediate = true)
  public void onShipChange() {
    descriptionV = ship.getDescription();
    shipCodeV = ship.getShipCode();
    averageResidenceTimeV = Integer.toString(ship.getAverageResidenceTime());
    shorePowerFactorV = Double.toString(NumberUtil.round(ship.getShorePowerFactor() * 100D, 1));
    percentageLadenV = Integer.toString(ship.getPercentageLaden());
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Computed
  protected String getDescription() {
    return descriptionV;
  }

  @Computed
  protected void setDescription(final String description) {
    ship.setDescription(description);
    descriptionV = description;
  }

  @Computed
  public List<InlandShippingCategory> getShipCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getInlandShippingCategories().getShipCategories();
  }

  @Computed
  public String getInlandShippingCategory() {
    return shipCodeV;
  }

  @Computed
  public void setInlandShippingCategory(final String shipCode) {
    ship.setShipCode(shipCode);
    shipCodeV = shipCode;
  }

  @Computed
  protected String getAverageResidenceTime() {
    return averageResidenceTimeV;
  }

  @Computed
  protected void setAverageResidenceTime(final String averageResidenceTime) {
    ValidationUtil.setSafeIntegerValue(ship::setAverageResidenceTime, averageResidenceTime, 0);
    averageResidenceTimeV = averageResidenceTime;
  }

  @Computed
  protected String getShorePowerFactor() {
    return shorePowerFactorV;
  }

  @Computed
  protected void setShorePowerFactor(final String shorePowerFactor) {
    ValidationUtil.setSafeDoubleValue(ship::setShorePowerFactor, shorePowerFactor, 0);
    ship.setShorePowerFactor(ship.getShorePowerFactor() / 100.0);
    shorePowerFactorV = shorePowerFactor;
  }

  @Computed
  protected String getPercentageLaden() {
    return percentageLadenV;
  }

  @Computed
  protected void setPercentageLaden(final String percentageLaden) {
    ValidationUtil.setSafeIntegerValue(ship::setPercentageLaden, percentageLaden, 0);
    percentageLadenV = percentageLaden;
  }

  @Computed("isInvalid")
  protected boolean isInvalid() {
    return validation.descriptionV.invalid
        || validation.shipCodeV.invalid
        || validation.averageResidenceTimeV.invalid
        || validation.shorePowerFactorV.invalid
        || validation.percentageLadenV.invalid;
  }

  @JsMethod
  protected String descriptionError() {
    return i18n.errorDescriptionRequired();
  }

  @JsMethod
  protected String shipCategoryRequiredError() {
    return i18n.errorShipCategoryRequired();
  }

  @JsMethod
  public String averageResidenceTimeError() {
    return i18n.errorNotValidEntry(averageResidenceTimeV);
  }

  @JsMethod
  public String shorePowerFactorError() {
    return i18n.errorShipShorePowerFactor();
  }

  @JsMethod
  public String percentageLadenError() {
    return i18n.errorShipLaden();
  }
}
