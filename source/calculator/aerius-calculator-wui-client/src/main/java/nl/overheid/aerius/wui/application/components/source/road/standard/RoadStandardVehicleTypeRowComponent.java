/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.source.road.standard;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.sector.category.SimpleCategory;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.InputWithUnitComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.road.standard.RoadStandardVehicleTypeValidators.RoadStandardVehicleTypeValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.domain.source.road.ValuesPerVehicleType;
import nl.overheid.aerius.wui.application.domain.source.road.ValuesPerVehicleTypes;
import nl.overheid.aerius.wui.application.util.NumberUtil;
import nl.overheid.aerius.wui.vue.DebugDirective;

/**
 * Shows number of vehicles and stagnationfraction editor.
 */
@Component(customizeOptions = {
    RoadStandardVehicleTypeValidators.class
}, directives = {
    ValidateDirective.class,
    DebugDirective.class,
}, components = {
    LabeledInputComponent.class,
    InputWithUnitComponent.class,
    InputErrorComponent.class,
    ValidationBehaviour.class
})
public class RoadStandardVehicleTypeRowComponent extends ErrorWarningValidator implements IsVueComponent, HasCreated,
    HasValidators<RoadStandardVehicleTypeValidations> {
  @Prop SimpleCategory vehicleType;
  @Prop @JsProperty ValuesPerVehicleTypes valuesPerVehicleType;
  @Prop TimeUnit timeUnit;

  @Prop(required = true) boolean stagnationEnabled;

  @Data String numberOfVehiclesV;
  @Data String stagnationFractionV;

  @JsProperty(name = "$v") RoadStandardVehicleTypeValidations validations;

  @Watch(value = "valuesPerVehicleType", isImmediate = true)
  public void onvaluesPerVehicleTypeChange() {
    numberOfVehiclesV = Double.toString(getVehicle() != null ? getVehicle().getVehiclesPerTimeUnit() : 0D);
    stagnationFractionV = Double.toString(getVehicle() != null ? NumberUtil.round(getVehicle().getStagnationFraction() * 100D, 2) : 0D);
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Override
  @Computed
  public RoadStandardVehicleTypeValidations getV() {
    return validations;
  }

  @Computed
  protected String getNumberOfVehicles() {
    return numberOfVehiclesV;
  }

  @Computed
  protected void setNumberOfVehicles(final String number) {
    final ValuesPerVehicleType vehicle = getVehicle();
    if (vehicle != null) {
      ValidationUtil.setSafeDoubleValue(numberOfVehicles -> {
        vehicle.setVehiclesPerTimeUnit(numberOfVehicles);
      }, number, 0D);
    }
    numberOfVehiclesV = number;
  }

  @Computed
  protected String getStagnationFraction() {
    return stagnationFractionV;
  }

  @Computed
  protected void setStagnationFraction(final String number) {
    final ValuesPerVehicleType vehicle = getVehicle();
    if (vehicle != null) {
      ValidationUtil.setSafeDoubleValue(stagnationFraction -> {
        vehicle.setStagnationFraction(stagnationFraction / 100D);
      }, number, 0D);
    }
    stagnationFractionV = number;
  }

  @Computed("isNuberOfVehiclesInvalid")
  protected boolean isNuberOfVehiclesInvalid() {
    return validations.numberOfVehiclesV.invalid;
  }

  @Computed("isStagnationFractionInvalid")
  protected boolean isStagnationFractionInvalid() {
    return validations.stagnationFractionV.invalid;
  }

  @Computed("isInvalid")
  protected boolean isInvalid() {
    return isNuberOfVehiclesInvalid() || isStagnationFractionInvalid();
  }

  @JsMethod
  protected String errorNumberOfVehicles() {
    return i18n.errorNotValidEntry(numberOfVehiclesV);
  }

  @JsMethod
  protected String errorStagnationFraction() {
    return i18n.errorStagnationFraction();
  }

  private ValuesPerVehicleType getVehicle() {
    // FIXME Avoid setting a value when getting a value
    if (!valuesPerVehicleType.hasValuePerVehicleType(vehicleType.getCode())) {
      valuesPerVehicleType.setValuePerVehicleType(vehicleType.getCode(), ValuesPerVehicleType.create());
    }
    return valuesPerVehicleType.getValuePerVehicleType(vehicleType.getCode());
  }
}
