/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.characteristics.outflow;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;
import jsinterop.base.Js;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.ops.HeatContentType;
import nl.overheid.aerius.shared.domain.ops.OPSLimits;
import nl.overheid.aerius.shared.domain.ops.OutflowDirectionType;
import nl.overheid.aerius.wui.application.components.input.InputWithUnitComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.MinimalInputComponent;
import nl.overheid.aerius.wui.application.components.source.characteristics.outflow.OutflowForcedValidators.OutflowForcedValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.OPSCharacteristics;
import nl.overheid.aerius.wui.application.util.NumberUtil;

@Component(customizeOptions = {
    OutflowForcedValidators.class
}, directives = {
    ValidateDirective.class
}, components = {
    InputWithUnitComponent.class,
    LabeledInputComponent.class,
    MinimalInputComponent.class,
    ValidationBehaviour.class,
})
public class OutflowForcedEditorComponent extends ErrorWarningValidator implements HasCreated, HasValidators<OutflowForcedValidations> {
  @Prop EmissionSourceFeature source;
  @Prop boolean buildingInfluence;

  @Data OPSCharacteristics characteristics;

  // Validations
  @JsProperty(name = "$v") OutflowForcedValidations validation;
  public @Data String emissionTemperatureV;
  public @Data String emissionTemperatureWithBuildingV;
  public @Data String outflowDiameterV;
  public @Data String outflowDiameterWithBuildingV;
  public @Data String outflowVelocityV;
  public @Data String outflowVelocityWithBuildingV;

  @Computed
  public OutflowForcedValidations getV() {
    return validation;
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Watch(value = "source", isImmediate = true)
  public void watchSource() {
    characteristics = source == null ? null : Js.uncheckedCast(source.getCharacteristics());
  }

  @Watch(value = "characteristics", isImmediate = true)
  public void watchCharacteristics() {
    emissionTemperatureV = String.valueOf(characteristics.getEmissionTemperature());
    emissionTemperatureWithBuildingV = String.valueOf(characteristics.getEmissionTemperature());
    outflowDiameterV = String.valueOf(characteristics.getOutflowDiameter());
    outflowDiameterWithBuildingV = String.valueOf(characteristics.getOutflowDiameter());
    outflowVelocityV = String.valueOf(characteristics.getOutflowVelocity());
    outflowVelocityWithBuildingV = String.valueOf(characteristics.getOutflowVelocity());
    buildingInfluence = characteristics.isBuildingInfluence();
  }

  @Computed
  public boolean getBuildingInfluence() {
    return characteristics.isBuildingInfluence();
  }

  @Computed
  public String getEmissionTemperature() {
    return emissionTemperatureV;
  }

  @Computed
  public void setEmissionTemperature(final String emissionTemperature) {
    ValidationUtil.setSafeDoubleValue(v -> characteristics.setEmissionTemperature(v), emissionTemperature, 0D);
    emissionTemperatureV = emissionTemperature;
    emissionTemperatureWithBuildingV = emissionTemperature;
  }

  @Computed("isEmissionTemperatureWarning")
  public boolean isEmissionTemperatureWarning() {
    final boolean isWithinBounds = !NumberUtil.equalEnough(OPSLimits.SOURCE_EMISSION_TEMPERATURE_MINIMUM, characteristics.getEmissionTemperature())
        && validation.emissionTemperatureWithBuildingV.error;
    return (buildingInfluence && isWithinBounds) ||
        (buildingInfluence && !NumberUtil.equalEnough(OPSLimits.AVERAGE_SURROUNDING_TEMPERATURE, characteristics.getEmissionTemperature()));
  }

  @Computed("isEmissionTemperatureError")
  public boolean isEmissionTemperatureError() {
    return validation.emissionTemperatureV.error;
  }

  @Computed("isEmissionTemperatureInvalid")
  public boolean isEmissionTemperatureInvalid() {
    return validation.emissionTemperatureV.invalid;
  }

  @JsMethod
  public String getEmissionTemperatureWarning() {
    return i18n.ivBuildingInfluenceExact(i18n.sourceEmissionTemperatureLabel(), String.valueOf(OPSLimits.AVERAGE_SURROUNDING_TEMPERATURE));
  }

  @JsMethod
  public String getEmissionTemperatureError() {
    return i18n.ivDoubleRangeBetween(i18n.sourceEmissionTemperatureLabel(), 0, OPSLimits.SOURCE_EMISSION_TEMPERATURE_MAXIMUM);
  }

  @Computed
  public String getOutflowDiameter() {
    return outflowDiameterV;
  }

  @Computed
  public void setOutflowDiameter(final String outflowDiameter) {
    ValidationUtil.setSafeDoubleValue(v -> characteristics.setOutflowDiameter(v), outflowDiameter, 0.0);
    outflowDiameterV = outflowDiameter;
    outflowDiameterWithBuildingV = outflowDiameter;
  }

  @Computed("isOutflowDiameterWarning")
  public boolean isOutflowDiameterWarning() {
    return buildingInfluence && validation.outflowDiameterWithBuildingV.invalid;
  }

  @Computed("isOutflowDiameterError")
  public boolean isOutflowDiameterError() {
    return validation.outflowDiameterV.error;
  }

  @Computed("isOutflowDiameterInvalid")
  public boolean isOutflowDiameterInvalid() {
    return validation.outflowDiameterV.invalid;
  }

  @JsMethod
  public String getOutflowDiameterWarning() {
    return i18n.ivBuildingInfluenceRangeBetween(i18n.sourceOutflowDiameterLabel(),
        OPSLimits.SCOPE_BUILDING_OUTFLOW_DIAMETER_MINIMUM, OPSLimits.SCOPE_BUILDING_OUTFLOW_DIAMETER_MAXIMUM);
  }

  @JsMethod
  public String getOutflowDiameterError() {
    return i18n.ivDoubleRangeBetween(i18n.sourceOutflowDiameterLabel(),
        OPSLimits.SCOPE_NO_BUILDING_OUTFLOW_DIAMETER_MINIMUM, OPSLimits.SCOPE_NO_BUILDING_OUTFLOW_DIAMETER_MAXIMUM);

  }

  @Computed
  public String getOutflowVelocity() {
    return outflowVelocityV;
  }

  @Computed
  public void setOutflowVelocity(final String outflowVelocity) {
    ValidationUtil.setSafeDoubleValue(v -> characteristics.setOutflowVelocity(v), outflowVelocity, 0D);
    outflowVelocityV = outflowVelocity;
    outflowVelocityWithBuildingV = outflowVelocity;
  }

  @Computed("isOutflowVelocityWarning")
  public boolean isOutflowVelocityWarning() {
    return buildingInfluence && validation.outflowVelocityWithBuildingV.invalid;
  }

  @Computed("isOutflowVelocityError")
  public boolean isOutflowVelocityError() {
    return validation.outflowVelocityV.error;
  }

  @Computed("isOutflowVelocityInvalid")
  public boolean isOutflowVelocityInvalid() {
    return validation.outflowVelocityV.invalid;
  }

  @JsMethod
  public String getOutflowVelocityError() {
    return i18n.ivDoubleRangeBetween(i18n.sourceOutflowVelocityLabel(),
        OPSLimits.SCOPE_NO_BUILDING_OUTFLOW_VELOCITY_MINIMUM, OPSLimits.SCOPE_NO_BUILDING_OUTFLOW_VELOCITY_MAXIMUM);
  }

  @JsMethod
  public String getOutflowVelocityWarning() {
    return i18n.ivBuildingInfluenceRangeBetween(i18n.sourceOutflowVelocityLabel(),
        OPSLimits.SCOPE_BUILDING_OUTFLOW_VELOCITY_MINIMUM, OPSLimits.SCOPE_BUILDING_OUTFLOW_VELOCITY_MAXIMUM);
  }

  @JsMethod
  public boolean isForcedHeat() {
    return characteristics.getHeatContentType() == HeatContentType.FORCED;
  }

  @Computed
  public String getOutflowDirectionType() {
    OutflowDirectionType odt = characteristics.getOutflowDirection();

    if (odt == null) {
      odt = OutflowDirectionType.VERTICAL;
    }
    return odt.name();
  }

  @Computed
  public void setOutflowDirectionType(final String selected) {
    characteristics.setOutflowDirection(OutflowDirectionType.safeValueOf(selected));
  }
}
