/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.notification;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasDestroyed;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.util.Notification;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.wui.application.command.NotificationRemoveCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    ButtonIcon.class
})
class NotificationComponent extends BasicVueComponent implements HasCreated, HasDestroyed {
  private static final NotificationComponentEventBinder EVENT_BINDER = GWT.create(NotificationComponentEventBinder.class);

  interface NotificationComponentEventBinder extends EventBinder<NotificationComponent> {}

  @Prop EventBus eventBus;
  @Prop Notification notification;
  @Prop int idx;

  private HandlerRegistration handlers;

  @Override
  public void created() {
    handlers = EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Override
  public void destroyed() {
    if (handlers != null) {
      handlers.removeHandler();
    }
  }

  @Computed
  boolean showDate() {
    return notification != null && !notification.isPersistentMessage();
  }

  @Computed
  boolean canRemove() {
    return notification != null && !notification.isPersistentMessage();
  }

  @JsMethod
  public String getExceptionReference(final Notification notification) {
    return String.valueOf(((AeriusException) notification.getException()).getReference());
  }

  @JsMethod
  boolean hasExceptionReference(final Notification notification) {
    return notification.getException() instanceof AeriusException;
  }

  @JsMethod
  public void remove(final Notification notification) {
    eventBus.fireEvent(new NotificationRemoveCommand(notification));
  }
}
