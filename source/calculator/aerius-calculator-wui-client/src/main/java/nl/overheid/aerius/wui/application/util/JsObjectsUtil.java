/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.util;

import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

/**
 * Util to help with Js objects.
 */
public final class JsObjectsUtil {

  private JsObjectsUtil() {
  }

  public static boolean hasValueForKey(final JsPropertyMap<?> results, final Enum<?> key) {
    return key != null && results.has(key.name());
  }

  public static <T> T getValueForKey(final JsPropertyMap<T> results, final Enum<?> key) {
    T er = null;
    if (key != null) {
      final Object temp = results.get(key.name());
      if (temp != null) {
        er = Js.cast(temp);
      }
    }
    return er;
  }

}
