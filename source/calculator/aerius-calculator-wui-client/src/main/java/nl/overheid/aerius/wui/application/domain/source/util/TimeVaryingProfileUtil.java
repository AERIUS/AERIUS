/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.util;

import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import nl.overheid.aerius.shared.domain.characteristics.StandardTimeVaryingProfile;
import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfileType;
import nl.overheid.aerius.wui.application.domain.source.TimeVaryingProfile;
import nl.overheid.aerius.wui.application.i18n.M;

public final class TimeVaryingProfileUtil {
  private static final String SYSTEM_PROFILE_KEY = "system-profile-";

  public static final ThreeDayProfileUtil THREE_DAY_PROFILE_UTIL = new ThreeDayProfileUtil();
  public static final MonthlyProfileUtil MONTHLY_PROFILE_UTIL = new MonthlyProfileUtil();

  private TimeVaryingProfileUtil() {
  }

  public static TimeVaryingProfileUtilBase getTimeVaryingProfileUtilBase(final CustomTimeVaryingProfileType type) {
    return type == CustomTimeVaryingProfileType.MONTHLY ? MONTHLY_PROFILE_UTIL : THREE_DAY_PROFILE_UTIL;
  }

  public static TimeVaryingProfile create(final CustomTimeVaryingProfileType type) {
    final TimeVaryingProfile profile = TimeVaryingProfile.create(type);
    init(profile);
    return profile;
  }

  public static void init(final TimeVaryingProfile profile) {
    profile.setValues(new ArrayList<>(IntStream.range(0, profile.getType().getExpectedNumberOfValues())
        .mapToDouble(v -> 1D)
        .boxed()
        .collect(Collectors.toList())));
  }

  public static TimeVaryingProfile fromStandardTimeVaryingProfile(final StandardTimeVaryingProfile v, final String code) {
    final TimeVaryingProfile profile = TimeVaryingProfile.create(v.getType());
    // Prefix with unique key to ensure no coincidental key duplicates with custom profiles
    profile.setId(SYSTEM_PROFILE_KEY + code);
    profile.setGmlId(SYSTEM_PROFILE_KEY + code);
    profile.setLabel(M.messages().timeVaryingProfileSectorDefaultLabel(v.getName()));
    profile.setDescription(v.getDescription());
    profile.setSystemProfile(true);
    profile.setType(v.getType());
    profile.setValues(v.getValues());
    return profile;
  }
}
