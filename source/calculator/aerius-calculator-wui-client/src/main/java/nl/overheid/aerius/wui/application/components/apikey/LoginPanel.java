/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.apikey;

import java.util.Arrays;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.wui.application.command.api.ApiKeyLoginCommand;
import nl.overheid.aerius.wui.application.command.api.ApiKeyLogoutCommand;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.input.CheckBoxComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.context.UserContext;
import nl.overheid.aerius.wui.application.util.UUID;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    LabeledInputComponent.class,
    CheckBoxComponent.class,
    CollapsiblePanel.class,
})
public class LoginPanel extends BasicVueComponent implements HasMounted {

  protected static final int[] UUID_DASH_INDICES = new int[] {8, 13, 18, 23};
  protected static final int API_KEY_LENGTH = 32;

  @Prop EventBus eventBus;

  @Inject @Data UserContext userContext;

  @Data String apiKey;
  @Data boolean persistent;
  @Data boolean showValidations = false;

  @Override
  public void mounted() {
    apiKey = userContext.getApiKey();
  }

  @Computed
  public boolean isValidApiKeyFormat() {
    if ( apiKey == null || apiKey.isEmpty() || apiKey.length() != API_KEY_LENGTH) {
      return false;
    }
    final StringBuilder stringBuilder = new StringBuilder(apiKey);
    Arrays.stream(UUID_DASH_INDICES).forEach(idx -> stringBuilder.insert(idx, '-'));
    return UUID.uuidValidate(stringBuilder.toString());
  }

  @JsMethod
  public void login() {
    if (!isValidApiKeyFormat()) {
      return;
    }
    eventBus.fireEvent(new ApiKeyLoginCommand(apiKey, persistent));
  }

  @JsMethod
  public void logout() {
    eventBus.fireEvent(new ApiKeyLogoutCommand());
  }

  @Computed("isLoggedIn")
  public boolean isLoggedIn() {
    return userContext.isLoggedIn();
  }

  @Computed
  public boolean isAuthenticationError() {
    return userContext.isAuthenticationError();
  }
}
