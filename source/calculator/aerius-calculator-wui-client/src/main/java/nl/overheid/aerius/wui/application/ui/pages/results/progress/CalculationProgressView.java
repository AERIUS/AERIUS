/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.results.progress;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.directives.VectorDirective;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.ServerUsage;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.wui.application.command.calculation.CalculationCancelCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationResetCommand;
import nl.overheid.aerius.wui.application.components.button.IconComponent;
import nl.overheid.aerius.wui.application.components.progress.JobProgressBarComponent;
import nl.overheid.aerius.wui.application.components.progress.JobProgressStatusComponent;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationInfo;
import nl.overheid.aerius.wui.application.domain.calculation.JobProgress;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component(components = {
    IconComponent.class,
    JobProgressBarComponent.class,
    JobProgressStatusComponent.class,
    VerticalCollapse.class
}, directives = {
    VectorDirective.class,
})
public class CalculationProgressView extends BasicVueView {
  @Prop EventBus eventBus;
  @Prop(required = true) CalculationExecutionContext job;
  @Prop(required = true) CalculationInfo calculationInfo;

  @Computed
  protected ServerUsage getServerUsage() {
    return calculationInfo.getCalculationServerUsage();
  }

  @Computed("isWarning")
  protected boolean isWarning() {
    final ServerUsage serverUsage = getServerUsage();
    return serverUsage == ServerUsage.ABOVE_AVERAGE
        || serverUsage == ServerUsage.VERY_HIGH;
  }

  @Computed
  protected JobState getState() {
    final JobProgress jobProgress = calculationInfo.getJobProgress();

    return jobProgress == null ? JobState.UNDEFINED : jobProgress.getState();
  }

  @Computed
  protected JobProgress getJobProgress() {
    return calculationInfo.getJobProgress();
  }

  @Computed
  protected String getLastUpdate() {
    return i18n.calculationStateLastUpdate(job.getLastUpdate());
  }

  @JsMethod
  protected void resetCalculation() {
    eventBus.fireEvent(new CalculationResetCommand(job));
  }

  @JsMethod
  protected void stopCalculation() {
    eventBus.fireEvent(new CalculationCancelCommand(job));
  }
}
