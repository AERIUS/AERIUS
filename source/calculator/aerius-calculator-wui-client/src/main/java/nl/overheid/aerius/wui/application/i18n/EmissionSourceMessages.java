/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.i18n;

import com.google.gwt.i18n.client.Messages.Select;

import ol.geom.GeometryType;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.context.InputTypeViewMode;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.sector.category.AnimalType;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.SourceType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.shared.domain.v2.source.road.SRM2RoadSideBarrierType;
import nl.overheid.aerius.shared.domain.v2.source.road.TrafficDirection;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;
import nl.overheid.aerius.wui.application.components.source.farmlodging.systems.LodgingStackType;
import nl.overheid.aerius.wui.application.components.source.shipping.inland.inland.InlandMovementComponent.InlandMovementType;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmAnimalHousingType;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmLodgingType;
import nl.overheid.aerius.wui.application.domain.source.offroad.OffRoadType;
import nl.overheid.aerius.wui.application.domain.source.road.RoadTypeMode;
import nl.overheid.aerius.wui.application.domain.source.shipping.inland.WaterwayDirection;

/**
 * Text specific for emission source pages.
 */
public interface EmissionSourceMessages {
  String sectorGroup(@Select SectorGroup sectorGroup);

  String inputViewModeNoInputs();
  String inputViewMode(@Select InputTypeViewMode inputTypeViewMode);

  String esEmissionLabel(String substance);

  String esMayStop();
  String esMayCancel();

  String esButtonCalculate(String typeText);
  String esButtonCancel();
  String esButtonConfirm();
  String esButtonCopy(String typeText);
  String esButtonDelete(String typeText);
  String esButtonDeleteAllSources();
  String esButtonDeleteAllSourcesWarning();
  String esButtonEdit(String typeText);
  String esButtonExport(String typeText);
  String esButtonNewSource(String typeText);
  String esButtonSave();

  String esModifyTypeSource();
  String esModifyTypeSubSource();
  String esModifyTypeSubSourceNumbered(int num);
  String esModifyTypeBuilding();
  String esModifyTypeCalculationPoint();

  String esSubSourceEmptyError();

  String esButtonSaveEncumberedText();
  String esButtonSavePreventedText();

  String esEmissionSourceEmptyInfo();

  String esLabelDefaultPrefix(String sourceId);
  String esLabelName();
  String esLabelSectorGroup();
  String esLabelSector();

  String esTitle();
  String esADMSSourceTitle(@Select SourceType type);
  String esSubstance();

  String esMarkerClusterLabel(int numberOfSources);

  String esLocation();
  String esLocationDrawExplain();
  String esLocationConversion(String originEpsg, String targetEpsg);
  String esLocationConfirmConverted();
  String esLocationConversionNotSupported(String supportedEpsgs);
  String esAddGeometryType(@Select GeometryType point, @Select Theme theme);
  String esLocationAssessmentPointNotUnique();
  String esLocationDirectExplain();
  String esLocationDirectPlaceholder(@Select String geometryType);
  String esLocationGeometryConcaveWarning();
  String esLocationGeometryTooLargeWarning(@Select String point, String limit);
  String esLocationLineStringLength(String str);
  String esLocationPolygonArea(String str);
  String esLocationGeometryLabel(@Select String geometryType);

  String esLabelSectorGroupName();
  String esLabelSectorName();
  String esLabelSourceLocation();
  String esDefaultValues();

  String esCollapsiblePanelTitle(@Select EmissionSourceType sourceType);
  String esMooringMaritimeShippingDescription();
  String esMooringMaritimeShippingAverageResidenceTime();
  String esMooringMaritimeShippingShorePowerFactor();
  String esDetailTypeTitle(@Select EmissionSourceType sourceType);
  String esGeometryType(@Select Theme theme, @Select String geometryType);
  String esGeometryTypeIllegal();

  String esTotalEmission(String sector, String sectorGroup);
  String esTotalEmissionSimple(String sector);

  // Farms
  String esFarmEmissionFactorTypeLabel();
  String esFarmEmissionFactorType(@Select FarmEmissionFactorType farmEmissionFactorType);
  String esFarmEmissionFactorTypeDescription(@Select FarmEmissionFactorType farmEmissionFactorType);

  // Lodging
  String esLodgingDetailTitle();
  String esLodgingDetailFarmSystem();
  String esLodgingDetailAmount();
  String esLodgingDetailFactor();
  String esLodgingDetailNumberOfDays();
  String esLodgingDetailReductionFactor();
  String esLodgingDetailReductionReduction();
  String esLodgingDetailEmission();
  String esFarmLodgingConstrainedFootNote();
  String esFarmLodgingEmissionFactor();
  String esFarmLodgingStandardAdditionalStackTypePlaceholder();
  String esFarmLodgingSelectType(@Select FarmLodgingType type);
  String esFarmLodgingStandardWarningRAVStacking();
  String esFarmLodgingBWLUnavailable();

  // Lodging Custom
  String esFarmLodgingCustomDescription();
  String esFarmLodgingCustomFactor();
  String esFarmLodgingCustomNumberOfAnimals();

  // Lodging Standard
  String esFarmLodgingStandardLodgingCode();
  String esFarmLodgingStandardLodgingCodeCheckWarning();
  String esFarmLodgingStandardLodgingCodePlaceholder();
  String esFarmLodgingStandardNumberOfAnimals();
  String esFarmLodgingStandardNumberOfDays();
  String esFarmLodgingStandardSystemDefinitionCode();
  String esFarmLodgingStandardAdditionalType(@Select LodgingStackType additionalType);
  String esFarmLodgingStandardFodderMeasureDelete();
  String esFarmLodgingStandardFodderMeasurePlaceholder();
  String esFarmLodgingStandardReductiveSystemPlaceholder();
  String esFarmLodgingStandardReductiveSystemDelete();
  String esFarmLodgingStandardAdditionalSystemPlaceholder();
  String esFarmLodgingStandardAdditionalSystemDelete();

  // Farm Animal Housing
  String esFarmAnimalHousingDetailTitle();
  String esFarmAnimalHousingDetailFarmSystem();
  String esFarmAnimalHousingDetailAmount();
  String esFarmAnimalHousingDetailEmissionFactor();
  String esFarmAnimalHousingDetailFactor();
  String esFarmAnimalHousingDetailNumberOfDays();
  String esFarmAnimalHousingDetailReductionFactor();
  String esFarmAnimalHousingDetailReductionReduction();
  String esFarmAnimalHousingDetailEmission();
  String esFarmAnimalHousingConstrainedFootNote();
  String esFarmAnimalHousingStandardAdditionalCustomAirScrubber();
  String esFarmAnimalHousingStandardAdditionalCustomOther();
  String esFarmAnimalHousingStandardAdditionalExplanation();
  String esFarmAnimalHousingStandardAdditionalExplanationError();
  String esFarmAnimalHousingStandardAdditionalReductionError();
  String esFarmAnimalHousingStandardAdditionalReductionPercentage();
  String esFarmAnimalHousingStandardAdditionalTechnique();
  String esFarmAnimalHousingSelectType(@Select FarmAnimalHousingType type);
  String esFarmAnimalHousingWarningStacking();
  String esFarmAnimalHousingLingeringCodeError(String value);

  // Farm Animal Housing Custom
  String esFarmAnimalHousingCustom();
  String esFarmAnimalHousingCustomDescription();
  String esFarmAnimalHousingCustomFactor();
  String esFarmAnimalHousingCustomNumberOfAnimals();
  String esAnimalTypeLabel();
  String esAnimalType(@Select AnimalType animalType);

  // Farm Animal Housing Standard
  String esFarmAnimalHousingStandardCode();
  String esFarmAnimalHousingStandardCodeCheckWarning();
  String esFarmAnimalHousingStandardCodePlaceholder();
  String esFarmAnimalHousingStandardNumberOfAnimals();
  String esFarmAnimalHousingStandardNumberOfDays();
  String esFarmAnimalHousingStandardAdditionalSystemDelete();

  // Farmland
  String esFarmlandDetailTitle();
  String esFarmlandTypeLabel();
  String esFarmlandStandardType(@Select String activityCode);
  String esFarmlandStandardDetailNumberOfAnimals();
  String esFarmlandStandardDetailNumberOfDays();
  String esFarmlandStandardDetailNumberOfApplications();
  String esFarmlandStandardDetailTonnes();
  String esFarmlandStandardDetailMetersCubed();
  String esFarmlandStandardDetailEmission();
  String esFarmlandStandardNumberOfAnimals();
  String esFarmlandStandardNumberOfDays();
  String esFarmlandStandardPlaceholder();
  String esFarmlandStandardTonnes();
  String esFarmlandStandardMetersCubed();
  String esFarmlandStandardNumberOfApplications();

  // Manure Storage
  String esManureStorageDetailTitle();
  String esManureStorageDetailType();
  String esManureStorageSelectType(@Select nl.overheid.aerius.wui.application.domain.source.manure.ManureStorageType type);
  String esManureStorageDetailTonnes();
  String esManureStorageDetailMetersSquared();
  String esManureStorageDetailNumberOfDays();
  String esManureStorageDetailFactor();
  String esManureStorageDetailEmission();
  String esManureStorageTonnes();
  String esManureStorageMetersSquared();
  String esManureStorageNumberOfDays();
  String esManureStorageStandardPlaceholder();
  String esManureStorageStandardType();
  String esManureStorageCustomDescription();
  String esManureStorageCustomFactor();

  // Road
  String esRoadNetworkShowMoreSources(int increment);
  String esRoadNetwork();
  String esRoadDetailTitle();
  String esRoadDetailLabel();
  String esRoadDetailRoadType();
  String esRoadDetailTunnelFactor();
  String esRoadDetailElevation();
  String esRoadDetailElevationHeight();
  String esRoadDetailBarrier();
  String esRoadDetailBarrierType();
  String esRoadDetailBarrierHeight();
  String esRoadDetailBarrierDistanceToRoad();
  String esRoadDetailDrivingDirection();
  String esRoadDetailStandardVehiclesSpeedTitle();
  String esRoadDetailStandardVehiclesTitle(@Select RoadTypeMode mode);
  String esRoadDetailStandardStagnationTitle();
  String esRoadDetailCustomLabel();
  String esRoadDetailCustomEmissionPerVehicle();
  String esRoadDetailEuroClass();
  String esRoadDetailTrafficTitle(@Select RoadTypeMode mode);
  String esRoadDetailTrafficTitleExtended(@Select RoadTypeMode mode);
  String esRoadArea();
  String esRoadType();
  String esRoadDrivingDirection(@Select TrafficDirection direction);
  String esRoadSelectType(@Select nl.overheid.aerius.wui.application.domain.source.road.VehicleType standard);
  String esRoadCustomDescription();
  String esRoadCustomNumberOfVehicles(@Select RoadTypeMode mode);
  String esRoadCustomEmissionFactorTitle();
  String esRoadStandardDrivingDirectionWarning();
  String esRoadStandardRoadSpeed();
  String esRoadStandardVehicles();
  String esRoadStandardStagnation();
  String esRoadCategory(int speed);
  String esRoadCategoryStrict(int speed);
  String esRoadStrictEnforcement();
  String esRoadCategoryUnknown();
  String esRoadEuroClass();
  String esRoadEuroClassSubSourceRow(String euroclass);
  String esRoadMobileSourceCategoriesPlaceHolder();
  String esRoadBarrierType(@Select SRM2RoadSideBarrierType barrierType);
  String esRoadVehicleTypeLabel();
  String esRoadVehicleTypeLegendaTotal();
  String esRoadDetailVehicleTypeTitle();
  String esShippingDescription();
  String esShippingStandardShipCode();
  String esShippingMovements();
  String esShippingAverageResidenceTime();
  String esShippingShorePowerFactor();
  String esShippingPercentageLaden();
  String esShippingVisits();

  // Cold start
  String esColdStartVehicles();

  // Maritime
  String esMaritimeStandardShipCode();

  // Mobile Equipment
  String esMobileEquipmentDetailTitle();
  String esMobileEquipmentDetailCategory();
  String esMobileEquipmentDetailFuelUsage();
  String esMobileEquipmentDetailOperatingHours();
  String esMobileEquipmentDetailAdblueUsage();

  // Offroad Mobile
  String esOffRoadMobileSelectStandard();
  String esOffRoadMobileSelectCustom();
  String esOffRoadMobileStandardDescription();
  String esOffRoadMobileCategoryLabel();
  String esOffRoadMobileCategory(@Select OffRoadType type);
  String esOffRoadMobileStandardLiterFuel();
  String esOffRoadMobileStandardOperatingHours();
  String esOffRoadMobileStandardLiterAdBlue();
  String esOffRoadStandardFuelAdblueRatioWarning(double maxRatio);

  // Shipping
  String esInlandShippingDetailTitle();
  String esMaritimeShippingDetailTitle();
  String esWaterwayCategory();
  String esWaterwayDirection();
  String esWaterwayDirectionDetail();
  String esWaterwayDirectionType(@Select WaterwayDirection waterwayDirection);
  String esMovement(@Select InlandMovementType movement);
  String esMovementAmountOfShips();
  String esMovementLoaded();
  String esShipDescription();
  String esShipsVisitsPerTimeUnit();
  String esShipsMovementsPerTimeUnit();
  String esShipTimeunit();
  String esShipPercentageLoad();
  String esShipAverageResidenceTime();
  String esShipShorePowerFactor();
  String esShipRoutingDeleteConfirm(String route);
}
