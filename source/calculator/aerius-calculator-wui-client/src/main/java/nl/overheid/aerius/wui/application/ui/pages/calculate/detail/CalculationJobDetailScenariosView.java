/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.calculate.detail;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.components.divider.DividerComponent;
import nl.overheid.aerius.wui.application.components.source.DetailStatComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    DividerComponent.class,
    DetailStatComponent.class
})
public class CalculationJobDetailScenariosView extends BasicVueComponent {

  @Prop SituationComposition situationComposition;
  @Prop CalculationSetOptions calculationSetOptions;

  @Inject @Data ApplicationContext applicationContext;

  @Computed
  public List<SituationType> getSingleSelectSituationTypes() {
    return applicationContext.getConfiguration().getAvailableSituationTypes().stream()
        .filter(st -> !calculationSetOptions.getCalculationJobType().isIllegal(st))
        .filter(st -> !calculationSetOptions.getCalculationJobType().isPlural(st))
        .collect(Collectors.toList());
  }

  @Computed
  public List<SituationType> getPluralSelectSituationTypes() {
    return applicationContext.getConfiguration().getAvailableSituationTypes().stream()
        .filter(st -> !calculationSetOptions.getCalculationJobType().isIllegal(st))
        .filter(st -> calculationSetOptions.getCalculationJobType().isPlural(st))
        .filter(st -> situationComposition.hasSituation(st))
        .collect(Collectors.toList());
  }

  @JsMethod
  public String getSituationName(final SituationType type) {
    return situationComposition.getSituation(type) == null
        ? M.messages().emptySign()
        : situationComposition.getSituation(type).getName();
  }

  @JsMethod
  public String getSituationNames(final SituationType type) {
    return situationComposition.getSituationsByType(type).isEmpty()
        ? M.messages().emptySign()
        : situationComposition.getSituationsByType(type).stream()
            .map(SituationContext::getName)
            .collect(Collectors.joining(", "));
  }

}
