/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.Vue;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasRender;
import com.axellience.vuegwt.core.client.vnode.VNode;
import com.axellience.vuegwt.core.client.vnode.builder.VNodeBuilder;

import elemental2.core.JsArray;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.context.EmissionSourceValidationContext;
import nl.overheid.aerius.wui.application.context.GenericValidationContext;

@Component(hasTemplate = false)
public class SubSourceValidationBehaviour extends ErrorWarningValidator implements HasRender, IsVueComponent {
  @Inject @Data EmissionSourceValidationContext validationContext;
  @Inject @Data GenericValidationContext genericValidationContext;

  @Prop boolean warnings;
  @Prop boolean errors;

  @Prop int selectedIndex;
  @Prop JsArray<?> subSources;

  @Data @JsProperty List<?> oldSubSources;

  @Data @JsProperty Map<Integer, ?> subSourcesIndex = new HashMap<>();

  @Data @JsProperty Map<Integer, Boolean> subSourceWarnings = new HashMap<>();
  @Data @JsProperty Map<Integer, Boolean> subSourceErrors = new HashMap<>();

  @Computed
  public boolean isDisplayFormProblems() {
    return genericValidationContext.isDisplayFormProblems();
  }

  @Watch(value = "isDisplayFormProblems()", isImmediate = true)
  public void onDisplayFormProblemsChange(final boolean neww) {
    if (neww) {
      vue().$emit("touch");
    } else {
      vue().$emit("reset");
    }

    vue().$emit("display-problems", neww);
  }

  @PropDefault("errors")
  boolean errorsDefault() {
    return false;
  }

  @PropDefault("warnings")
  boolean warningsDefault() {
    return false;
  }

  @Watch(value = "errors", isImmediate = true)
  public void onErrorsChange(final boolean neww) {
    emitErrors();
  }

  @Watch(value = "warnings", isImmediate = true)
  public void onWarningsChange(final boolean neww) {
    emitWarnings();
  }

  /**
   * Horribly roundabout but robust way to accurately reassign the errors/warnings
   * on change.
   *
   * When the subSources change, the error states associated with each index may
   * no longer accurate. This method remaps the known index-based warnings/errors
   * to the last-known subsource objects. It's a rather expensive operation but
   * since no more than a handful of items exist in these lists it shouldn't
   * really be noticeable.
   */
  @Watch(value = "subSources", isImmediate = true)
  public void onSubSourcesChange(final JsArray<?> neww) {
    if (oldSubSources == null) {
      oldSubSources = new ArrayList<>(neww.asList());
      return;
    }

    final Map<Integer, Boolean> newSubSourceWarnings = new HashMap<>();
    final Map<Integer, Boolean> newSubSourceErrors = new HashMap<>();

    for (int i = 0; i < neww.length; i++) {
      final Object objNew = neww.getAt(i);

      final int oldIndex = oldSubSources.indexOf(objNew);
      if (oldIndex == -1) {
        continue;
      } else {
        newSubSourceWarnings.put(i, subSourceWarnings.get(oldIndex));
        newSubSourceErrors.put(i, subSourceErrors.get(oldIndex));
      }
    }

    subSourceWarnings.clear();
    subSourceWarnings.putAll(newSubSourceWarnings);
    subSourceErrors.clear();
    subSourceErrors.putAll(newSubSourceErrors);

    if (selectedIndex != -1) {
      subSourceErrors.put(selectedIndex, validationContext.getDetailEditorError());
      subSourceWarnings.put(selectedIndex, validationContext.getDetailEditorWarning());
    }

    emitAllErrors();
    emitAllWarnings();

    oldSubSources = new ArrayList<>(neww.asList());
  }

  @Computed("getDetailError")
  public boolean getDetailError() {
    return validationContext.getDetailEditorError();
  }

  @Watch("selectedIndex")
  public void onSelectedIndexChange() {
    if (selectedIndex == -1) {
      return;
    }

    onDetailErrorChange(validationContext.getDetailEditorError());
    onDetailWarningChange(validationContext.getDetailEditorWarning());
    Vue.nextTick(() -> {
      onDetailErrorChange(validationContext.getDetailEditorError());
      onDetailWarningChange(validationContext.getDetailEditorWarning());
    });
  }

  @Watch(value = "getDetailError()", isImmediate = true)
  public void onDetailErrorChange(final boolean neww) {
    subSourceErrors.put(selectedIndex, neww);
    emitAllErrors();
  }

  private void emitAllErrors() {
    emitErrors();
    vue().$emit("subSourceErrors", subSourceErrors);
  }

  private void emitErrors() {
    final boolean anyErrors = subSourceErrors.values().stream().anyMatch(this::getBoolean)
        || errors
        || subSources.length == 0;

    emitErrors(anyErrors);
  }

  @Computed
  public boolean getDetailWarning() {
    return validationContext.getDetailEditorWarning();
  }

  @Watch(value = "getDetailWarning()", isImmediate = true)
  public void onDetailWarningChange(final boolean neww) {
    subSourceWarnings.put(selectedIndex, neww);
    emitAllWarnings();
  }

  private void emitAllWarnings() {
    emitWarnings();
    vue().$emit("subSourceWarnings", subSourceWarnings);
  }

  private void emitWarnings() {
    emitWarnings(subSourceWarnings.values().stream().anyMatch(this::getBoolean)
        || warnings);
  }

  private boolean getBoolean(final Boolean v) {
    return Boolean.TRUE.equals(v);
  }

  @Override
  public VNode render(final VNodeBuilder builder) {
    return builder.el();
  }
}
