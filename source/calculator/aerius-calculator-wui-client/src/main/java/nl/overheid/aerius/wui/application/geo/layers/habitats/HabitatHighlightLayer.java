/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.habitats;

import javax.inject.Inject;

import ol.OLFactory;
import ol.layer.Image;
import ol.layer.Layer;
import ol.source.ImageWms;
import ol.source.ImageWmsOptions;
import ol.source.ImageWmsParams;

import nl.aerius.geo.domain.IsLayer;
import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.wui.application.context.result.ResultSelectionContext;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;

/**
 * Provides the habitat selection layer
 */
public abstract class HabitatHighlightLayer extends BasicEventComponent implements IsLayer<Layer> {
  public static final String STYLE_SELECT = "wms_habitat_area_select";
  public static final String STYLE_HOVER = "wms_habitat_area_hover";

  private static final String WMS_HABITAT_AREA_LAYER = "calculator:wms_habitats_view";
  private static final String WMS_EXTRA_ASSESSMENT_HEXAGONS_LAYER = "calculator:wms_extra_assessment_hexagons_view";

  private final Image layer;

  private final ImageWmsParams imageWmsParams;
  private final ImageWmsOptions imageWmsOptions;
  private ImageWms imageWms;

  @Inject ResultSelectionContext selectionContext;

  private final String style;

  @Inject
  public HabitatHighlightLayer(final String style, final EnvironmentConfiguration configuration) {
    this.style = style;
    imageWmsParams = OLFactory.createOptions();

    imageWmsOptions = OLFactory.createOptions();
    imageWmsOptions.setUrl(configuration.getGeoserverBaseUrl());
    imageWmsOptions.setParams(imageWmsParams);

    layer = new Image();
  }

  private String buildViewParams() {
    final String habitatTypes = getHabitatTypes();

    String viewParams = "";
    // -1 to not show anything. 0 will show everything (at least for extra assessment hexagons).
    if (habitatTypes != null && !habitatTypes.isEmpty()) {
      viewParams += "habitatTypeId:" +  habitatTypes;
    }
    viewParams += ";assessmentAreaId:" + selectionContext.getSelectedAssessmentArea();
    return viewParams;
  }

  protected abstract String getHabitatTypes();

  protected void updateLayer() {
    final String params = buildViewParams();

    imageWmsParams.setViewParams(params);
    imageWmsParams.setLayers(selectionContext.isExtraAssessmentHabitats() ? WMS_EXTRA_ASSESSMENT_HEXAGONS_LAYER : WMS_HABITAT_AREA_LAYER);
    imageWmsParams.setStyles(style);

    if (imageWms == null) {
      imageWms = new ImageWms(imageWmsOptions);
      layer.setSource(imageWms);
    }

    final boolean visible = isVisible();

    layer.setVisible(visible);
    imageWms.refresh();
  }

  protected boolean isVisible() {
    return true;
  }

  @Override
  public Layer asLayer() {
    return layer;
  }
}
