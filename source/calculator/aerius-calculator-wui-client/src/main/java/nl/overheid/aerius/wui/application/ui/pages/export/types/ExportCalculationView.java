/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.ui.pages.export.types;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.wui.application.command.exporter.ExportPrepareCommand;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.input.CheckBoxComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ExportContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationPreparationContext;
import nl.overheid.aerius.wui.application.ui.pages.emissionsource.ErrorWarningIconComponent;
import nl.overheid.aerius.wui.application.ui.pages.export.metadata.ExportMetaDataComponent;
import nl.overheid.aerius.wui.application.ui.pages.export.types.components.CalculationJobSelectorComponent;
import nl.overheid.aerius.wui.application.ui.pages.export.types.options.AdditionalOptionsComponent;
import nl.overheid.aerius.wui.application.ui.pages.export.types.options.ExportCalculationOptionsComponent;

@Component(components = {
    CheckBoxComponent.class,
    CollapsiblePanel.class,
    VerticalCollapse.class,
    LabeledInputComponent.class,
    ExportMetaDataComponent.class,
    ValidationBehaviour.class,
    ErrorWarningIconComponent.class,
    AdditionalOptionsComponent.class,
    CalculationJobSelectorComponent.class,
    ExportCalculationOptionsComponent.class,
})
public class ExportCalculationView extends ErrorWarningValidator implements HasMounted {
  enum Panel {
    META_DATA,
  }

  @Prop boolean metaDataEnabled;

  @Inject @Data CalculationPreparationContext preparationContext;
  @Inject @Data ScenarioContext scenarioContext;
  @Inject @Data ExportContext exportContext;

  @Data Panel openPanel = null;

  @Prop EventBus eventBus;

  @Override
  public void mounted() {
    final List<CalculationJobContext> jobs = preparationContext.getJobs();
    if (jobs.size() == 1) {
      eventBus.fireEvent(new ExportPrepareCommand(jobs.get(0), false));
    }
  }

  @JsMethod
  public boolean isOpen(final Panel panel) {
    return openPanel == panel;
  }

  @JsMethod
  public void openPanel(final Panel panel) {
    openPanel = panel;
  }

  @JsMethod
  public void closePanel() {
    openPanel = null;
  }

  @JsMethod
  public void openErrorPanel() {
    if (displayProblems) {
      if (hasInvalidMetaData() || hasMetaDataWarning()) {
        openPanel = Panel.META_DATA;
      }
    }

    displayProblems(displayProblems);
  }

  private boolean hasInvalidCalculationJob() {
    return hasChildError("job-selection");
  }

  private boolean hasInvalidAdditionalOptions() {
    return hasChildError("additional-options");
  }

  public boolean hasInvalidMetaData() {
    return metaDataEnabled
        && exportContext.isIncludeScenarioMetaData()
        && hasChildError("metadata");
  }

  public boolean hasMetaDataWarning() {
    return metaDataEnabled
        && hasChildWarning("metadata");
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return hasInvalidCalculationJob()
        || hasInvalidAdditionalOptions()
        || hasInvalidMetaData();
  }

  @JsMethod
  public void refreshProblems(final boolean value) {
    displayProblems(value);
    openErrorPanel();
  }
}
