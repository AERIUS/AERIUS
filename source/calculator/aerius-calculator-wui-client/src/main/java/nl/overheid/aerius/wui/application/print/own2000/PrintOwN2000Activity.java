/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.print.own2000;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.wui.application.place.PrintPlace;
import nl.overheid.aerius.wui.application.print.PrintActivity;
import nl.overheid.aerius.wui.application.print.PrintPresenter;

public class PrintOwN2000Activity extends PrintActivity<PrintPresenter, PrintOwN2000View, PrintOwN2000ViewFactory> {

  @Inject
  public PrintOwN2000Activity(final @Assisted PrintPlace place) {
    super(PrintOwN2000ViewFactory.get(), place);
  }
}
