/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.bootstrap;

import com.axellience.vuegwt.core.client.observer.VueGWTObserver;

import ol.layer.Layer;

import nl.aerius.wui.dev.GWTProd;

/**
 * This observer replicates an expression executed in VueGWTObserverManager:79 for each object that is to be made reactive.
 *
 * In circumstances so far limited only to the OpenLayers Map object, this can fail which will crash the entire application.
 *
 * The failure happens, as far as we know, because GWT and OL are both independently minified/obfuscated, which apparently leads to strange conflicts.
 *
 * The current hypothesis is because GWT's obfuscated .getClass() method is present in OL's minified object, the conflict arises, because .getClass()
 * will not return a Class object leading to a ClassCastException.
 *
 * In this observer, this (and any other) exception is caught, and then the object is skipped by returning true. A warning is dumped into the console
 * notifying this happened.
 *
 * This resolves the issue where the application crashes entirely, instead opting for the alternative where an object will not be made reactive.
 *
 * In case of OL's Map object, the object is not expected to become reactive anyway.
 */
public class OpenLayersObserver extends VueGWTObserver {
  @Override
  public boolean observe(final Object object) {
    if (object instanceof Layer) {
      GWTProd.log("Bailing out from Layer.");
      return true;
    }

    try {
      object.getClass().getCanonicalName();
    } catch (final Exception e) {
      GWTProd.warn("Could not observe object for which the canonical name cannot be retrieved. Skipping.", object);
      return true;
    }
    return false;
  }
}
