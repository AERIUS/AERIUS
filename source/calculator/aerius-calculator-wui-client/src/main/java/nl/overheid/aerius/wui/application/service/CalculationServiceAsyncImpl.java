/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Singleton;

import elemental2.core.Global;
import elemental2.core.JsArray;
import elemental2.dom.DomGlobal;

import jsinterop.base.Js;

import nl.aerius.wui.future.AppAsyncCallback;
import nl.aerius.wui.util.RequestUtil;
import nl.overheid.aerius.shared.RequestMappings;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.daemon.navigation.FlightOfTheNavigator;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationInfo;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.application.domain.export.ExportOptions;
import nl.overheid.aerius.wui.application.domain.importer.ScenarioMetaData;
import nl.overheid.aerius.wui.application.domain.importer.SituationWithFileId;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsSummary;
import nl.overheid.aerius.wui.application.domain.summary.DecisionFrameworkResults;
import nl.overheid.aerius.wui.application.domain.summary.JobSummary;
import nl.overheid.aerius.wui.application.print.CompleteTracker;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;

@Singleton
public class CalculationServiceAsyncImpl implements CalculationServiceAsync {

  @Inject EnvironmentConfiguration cfg;
  @Inject HeaderHelper hdr;
  @Inject ApplicationContext appContext;
  @Inject CompleteTracker completeTracker;

  @Override
  public void startCalculation(final SituationComposition situationComposition, final CalculationSetOptions calculationOptions,
      final ExportOptions exportOptions, final ScenarioContext scenarioContext, final ScenarioMetaData scenarioMetaData, final Theme theme,
      final String jobKey, final AsyncCallback<String> callback) {
    final String requestUrl = RequestUtil.prepareUrl(cfg.getConnectBaseUrl(), RequestMappings.CONNECT_UI_CALCULATE);
    final CalculationSetOptions correctedCalculationOptions = calculationOptions == null
        ? null
        : getValidCalculationOptionsFromContext(calculationOptions, exportOptions);
    final String scenarioJsonString = scenarioContext.toJSONString(scenarioMetaData, situationComposition, correctedCalculationOptions);
    final String calculationRequest = "{"
        + "\"theme\": \"" + theme.getKey().toUpperCase() + "\","
        + "\"scenario\": " + scenarioJsonString + ","
        + "\"exportOptions\": " + exportOptions.toJSONString() + ","
        + "\"jobKey\": " + (jobKey == null ? null : ("\"" + jobKey + "\""))
        + "}";

    RequestUtil.doPost(requestUrl, calculationRequest, hdr.defaultHeaders(), AeriusRequestCallback.createRawAsync(callback, s -> s));
  }

  @Override
  public void importResults(final ScenarioContext scenarioContext, final SituationComposition situationComposition,
      final List<SituationWithFileId> situationWithFileIds, final String archiveContributionFileId, final Theme theme,
      final AsyncCallback<String> callback) {
    final String requestUrl = RequestUtil.prepareUrl(cfg.getConnectBaseUrl(), RequestMappings.CONNECT_UI_IMPORT_RESULTS);

    final JsArray<SituationWithFileId> situationWithFileIdArray = new JsArray<>();
    situationWithFileIds.forEach(situationWithFileIdArray::push);
    final String archivePart = archiveContributionFileId == null ? ""
        : (",\"archiveContributionFileId\":\"" + archiveContributionFileId + "\"");

    final String importResultsRequest = "{"
        + "\"theme\": \"" + theme.getKey().toUpperCase() + "\","
        + "\"scenario\": " + scenarioContext.toJSONString(null, situationComposition, null) + ","
        + "\"situationWithFileIds\": " + Global.JSON.stringify(situationWithFileIdArray)
        + archivePart
        + "}";

    RequestUtil.doPost(requestUrl, importResultsRequest, hdr.defaultHeaders(), AeriusRequestCallback.createRawAsync(callback, s -> s));
  }

  @Override
  public void getCalculationStatus(final String jobKey, final AsyncCallback<CalculationInfo> callback) {
    final String requestUrl = RequestUtil.prepareUrl(cfg.getConnectBaseUrl(), RequestMappings.CONNECT_UI_CALCULATE_INFO,
        RequestMappings.JOB_KEY, jobKey);
    RequestUtil.doGetWithHeaders(requestUrl, hdr.defaultHeaders(), AeriusRequestCallback.createAsync(callback));
  }

  @Override
  public void cancelCalculation(final String jobKey, final AsyncCallback<String> callback) {
    final String requestUrl = RequestUtil.prepareUrl(cfg.getConnectBaseUrl(), RequestMappings.CONNECT_UI_CALCULATE_CANCEL,
        RequestMappings.JOB_KEY, jobKey);
    RequestUtil.doPost(requestUrl, "", hdr.defaultHeaders(), AeriusRequestCallback.createRawAsync(callback, s -> s));
  }

  @Override
  public void deleteCalculation(final String jobKey, final AsyncCallback<String> callback) {
    final String requestUrl = RequestUtil.prepareUrl(cfg.getConnectBaseUrl(), RequestMappings.CONNECT_UI_CALCULATE_DELETE,
        RequestMappings.JOB_KEY, jobKey);
    RequestUtil.doDelete(requestUrl, hdr.defaultHeaders(), AeriusRequestCallback.createRawAsync(callback, s -> s));
  }

  @Override
  public void deleteCalculationViaPost(final String jobKey) {
    final FlightOfTheNavigator navigator = Js.uncheckedCast(DomGlobal.navigator);
    final String requestUrl = RequestUtil.prepareUrl(cfg.getConnectBaseUrl(), RequestMappings.CONNECT_UI_CALCULATE_DELETE_WITH_POST,
        RequestMappings.JOB_KEY, jobKey);
    navigator.sendBeacon(requestUrl, null);
  }

  @Override
  public void getSituationResultsSummary(final String jobKey, final SituationResultsKey situationResultsKey,
      final AsyncCallback<SituationResultsSummary> callback) {
    // Only start a task if not NCA (TODO: Use a different mechanism)
    final CompleteTracker.Task task = appContext.getTheme() == Theme.NCA ? null : completeTracker.createTask();
    final Map<String, String> queryParams = new HashMap<>();
    if (situationResultsKey.getHexagonType() != null) {
      queryParams.put(RequestMappings.CONNECT_HEXAGON_TYPE_PARAM, situationResultsKey.getHexagonType().name());
    }
    if (situationResultsKey.getOverlappingHexagonType() != null) {
      queryParams.put(RequestMappings.CONNECT_OVERLAPPING_HEXAGON_TYPE_PARAM, situationResultsKey.getOverlappingHexagonType().name());
    }
    if (situationResultsKey.getProcurementPolicy() != null) {
      queryParams.put(RequestMappings.CONNECT_PROCUREMENT_POLICY_PARAM, situationResultsKey.getProcurementPolicy().name());
    }
    if (situationResultsKey.getEmissionResultKey() != null) {
      queryParams.put(RequestMappings.CONNECT_EMISSION_RESULT_KEY_PARAM, situationResultsKey.getEmissionResultKey().name());
    }
    final String requestUrl = RequestUtil.prepareUrl(cfg.getConnectBaseUrl(), RequestMappings.CONNECT_UI_CALCULATION_SUMMARY,
        RequestMappings.JOB_KEY, jobKey,
        RequestMappings.SITUATION_CODE, situationResultsKey.getSituationHandle().getId(),
        RequestMappings.RESULT_TYPE, situationResultsKey.getResultType().name());
    final AsyncCallback<String> wrappedCallback = AeriusRequestCallback.merge(AeriusRequestCallback.createAsync(callback),
        AppAsyncCallback.create(t -> {
          if (task != null) {
            task.complete();
          }
        }, e -> callback.onFailure(e)));
    RequestUtil.doGet(requestUrl, queryParams, hdr.defaultHeaders(), wrappedCallback);

  }

  @Override
  public void getCalculationSummary(final String jobKey, final AsyncCallback<JobSummary> callback) {
    final String requestUrl = RequestUtil.prepareUrl(cfg.getConnectBaseUrl(), RequestMappings.CONNECT_RESULTS_SUMMARY, RequestMappings.JOB_KEY,
        jobKey);
    RequestUtil.doGetWithHeaders(requestUrl, hdr.defaultHeaders(), AeriusRequestCallback.createAsync(callback));
  }

  @Override
  public void getDecisionFrameworkResults(final String jobKey, final AsyncCallback<DecisionFrameworkResults> callback) {
    final String requestUrl = RequestUtil.prepareUrl(cfg.getConnectBaseUrl(), RequestMappings.CONNECT_DECISION_FRAMEWORK_RESULTS,
        RequestMappings.JOB_KEY, jobKey);
    RequestUtil.doGetWithHeaders(requestUrl, hdr.defaultHeaders(), AeriusRequestCallback.createAsync(callback));
  }

  /**
   * Ensure the combination of export- and calculation options are valid. If not, create new options without altering
   * the existing one.
   */
  public static CalculationSetOptions getValidCalculationOptionsFromContext(final CalculationSetOptions calculationOptions,
      final ExportOptions exportOptions) {
    // This enforces the condition that if the export type is PAA, the calculation method must be FORMAL_ASSESSMENT
    // however this is unenforceable on the client and as such also happens on the server.
    if (exportOptions.getExportType() == ExportType.PDF_PAA
        && calculationOptions.getCalculationMethod() != CalculationMethod.FORMAL_ASSESSMENT) {
      final CalculationSetOptions newCalculationOptions = calculationOptions.copy();

      newCalculationOptions.setCalculationMethod(CalculationMethod.FORMAL_ASSESSMENT);
      return newCalculationOptions;
    }

    return calculationOptions;
  }
}
