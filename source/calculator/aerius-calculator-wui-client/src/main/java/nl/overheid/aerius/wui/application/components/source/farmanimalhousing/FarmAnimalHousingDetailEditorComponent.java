/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.farmanimalhousing;

import java.util.Arrays;
import java.util.List;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;

import elemental2.core.JsArray;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.wui.application.components.source.farmanimalhousing.custom.FarmAnimalHousingCustomEmissionComponent;
import nl.overheid.aerius.wui.application.components.source.farmanimalhousing.standard.FarmAnimalHousingDetailStandardComponent;
import nl.overheid.aerius.wui.application.components.source.farmanimalhousing.standard.FarmAnimalHousingStandardEmissionComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.components.toggle.ToggleButtons;
import nl.overheid.aerius.wui.application.domain.source.FarmAnimalHousingESFeature;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmAnimalHousing;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmAnimalHousingType;
import nl.overheid.aerius.wui.application.domain.source.util.EmissionSourceFeatureUtil;
import nl.overheid.aerius.wui.vue.DebugDirective;

/**
 * Component to edit the a {@link FarmAnimalHousing} sub source.
 * This component shows the selection of standard of custom.
 */
@Component(components = {
    FarmAnimalHousingStandardEmissionComponent.class,
    FarmAnimalHousingDetailStandardComponent.class,
    FarmAnimalHousingCustomEmissionComponent.class,
    ToggleButtons.class,
    ValidationBehaviour.class,
}, directives = {
    DebugDirective.class
})
public class FarmAnimalHousingDetailEditorComponent extends ErrorWarningValidator {
  @Prop FarmAnimalHousingESFeature source;
  @Prop FarmAnimalHousing subSource;

  @Data FarmAnimalHousingType toggleSelect;

  @Watch(value = "subSource", isImmediate = true)
  protected void onValueChange(final Integer neww, final Integer old) {
    if (subSource != null) {
      toggleSelect = subSource.getAnimalHousingType();
    }
  }

  @Computed
  protected List<FarmAnimalHousingType> getFarmAnimalHousingTypes() {
    return Arrays.asList(FarmAnimalHousingType.values());
  }

  @JsMethod
  protected boolean isSourceAvailable() {
    return subSource != null;
  }

  @JsMethod
  protected void selectTab(final FarmAnimalHousingType type) {
    toggleSelect = type;
    final JsArray<FarmAnimalHousing> farmAnimalHousings = source.getSubSources();

    farmAnimalHousings.splice(farmAnimalHousings.indexOf(subSource), 1, EmissionSourceFeatureUtil.createFarmAnimalHousingType(type));
  }
}
