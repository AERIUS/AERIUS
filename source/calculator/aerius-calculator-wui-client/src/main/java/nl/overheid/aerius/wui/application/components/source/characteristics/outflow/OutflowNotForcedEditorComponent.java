/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.characteristics.outflow;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;
import jsinterop.base.Js;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.ops.OPSLimits;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.characteristics.outflow.OutflowNotForcedValidators.OutflowNotForcedValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.OPSCharacteristics;
import nl.overheid.aerius.wui.application.util.NumberUtil;

@Component(customizeOptions = {
    OutflowNotForcedValidators.class
}, directives = {
    ValidateDirective.class
}, components = {
    LabeledInputComponent.class,
    ValidationBehaviour.class,
})
public class OutflowNotForcedEditorComponent extends ErrorWarningValidator implements HasCreated, HasValidators<OutflowNotForcedValidations> {
  @Prop EmissionSourceFeature source;
  @Data OPSCharacteristics characteristics;

  // Validations
  @JsProperty(name = "$v") OutflowNotForcedValidations validation;
  @Data String heatContentV;
  @Data String heatContentWithBuildingV;

  @Override
  @Computed
  public OutflowNotForcedValidations getV() {
    return validation;
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Computed("isBuildingInfluence")
  public boolean isBuildingInfluence() {
    return characteristics == null ? false : characteristics.isBuildingInfluence();
  }

  @Watch(value = "source", isImmediate = true)
  public void watchSource() {
    characteristics = source == null ? null : Js.uncheckedCast(source.getCharacteristics());
  }

  @Watch(value = "characteristics", isImmediate = true)
  public void watchCharacteristics() {
    heatContentV = String.valueOf(characteristics.getHeatContent());
    heatContentWithBuildingV = String.valueOf(characteristics.getHeatContent());
  }

  @Computed
  protected String getHeatContent() {
    return heatContentV;
  }

  @Computed
  protected void setHeatContent(final String heatContent) {
    ValidationUtil.setSafeDoubleValue(v -> characteristics.setHeatContent(v), heatContent, 0D);
    heatContentV = heatContent;
    heatContentWithBuildingV = heatContent;
  }

  /**
   * The heat content is invalid if it is negative, except if it is -999
   */
  @Computed("isHeatContentInvalid")
  protected boolean isHeatContentInvalid() {
    return !NumberUtil.equalEnough((double) OPSLimits.SOURCE_HEAT_CONTENT_MINIMUM, characteristics.getHeatContent())
        && validation.heatContentV.invalid;
  }

  /**
   * If there is a building influence, and if there isn't already an error, show
   * building-specific warning
   */
  @Computed("isHeatContentWarning")
  protected boolean isHeatContentWarning() {
    return isBuildingInfluence()
        && validation.heatContentWithBuildingV.invalid
        && !isHeatContentInvalid();
  }

  @JsMethod
  protected String getHeatContentError() {
    return i18n.ivExactHeatContent(i18n.sourceHeatContentLabel(), 0, OPSLimits.SOURCE_HEAT_CONTENT_MAXIMUM,
        OPSLimits.SOURCE_HEAT_CONTENT_MINIMUM);
  }

  @JsMethod
  protected String getHeatContentWarning() {
    return i18n.ivBuildingInfluenceExactHeatContent(i18n.sourceHeatContentLabel(), String.valueOf(0));
  }
}
