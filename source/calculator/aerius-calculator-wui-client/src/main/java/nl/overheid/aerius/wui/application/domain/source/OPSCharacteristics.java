/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.domain.source;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.shared.domain.ops.DiurnalVariation;
import nl.overheid.aerius.shared.domain.ops.HeatContentType;
import nl.overheid.aerius.shared.domain.ops.OPSLimits;
import nl.overheid.aerius.shared.domain.ops.OutflowDirectionType;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.wui.vue.ReactivityUtil;

/**
 * Client side implementation of OPS props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class OPSCharacteristics extends SourceCharacteristics {
  private String heatContentType;
  private double heatContent;
  private double emissionTemperature;
  private double outflowDiameter;
  private double outflowVelocity;
  private String outflowDirection;
  private double emissionHeight;
  private int diameter;
  private double spread;
  private String diurnalVariation;
  private int particleSizeDistribution;

  public static final @JsOverlay OPSCharacteristics create() {
    final OPSCharacteristics props = Js.uncheckedCast(JsPropertyMap.of());
    initOPS(props);
    return props;
  }

  public static final @JsOverlay void initOPS(final OPSCharacteristics props) {
    SourceCharacteristics.init(props, CharacteristicsType.OPS);
    ReactivityUtil.ensureJsProperty(props, "heatContentType", props::setHeatContentType, HeatContentType.NOT_FORCED);
    ReactivityUtil.ensureJsProperty(props, "heatContent", props::setHeatContent, 0D);
    ReactivityUtil.ensureJsProperty(props, "emissionTemperature", props::setEmissionTemperature, OPSLimits.AVERAGE_SURROUNDING_TEMPERATURE);
    ReactivityUtil.ensureJsProperty(props, "outflowDiameter", props::setOutflowDiameter, OPSLimits.SCOPE_NO_BUILDING_OUTFLOW_DIAMETER_MINIMUM);
    ReactivityUtil.ensureJsProperty(props, "outflowVelocity", props::setOutflowVelocity, 0D);
    ReactivityUtil.ensureJsProperty(props, "outflowDirection", props::setOutflowDirection, OutflowDirectionType.VERTICAL);
    ReactivityUtil.ensureJsProperty(props, "emissionHeight", props::setEmissionHeight, 5D);
    ReactivityUtil.ensureJsProperty(props, "diameter", props::setDiameter, 0);
    ReactivityUtil.ensureJsProperty(props, "spread", props::setSpread, 0D);
    ReactivityUtil.ensureJsProperty(props, "diurnalVariation", props::setDiurnalVariation, DiurnalVariation.CONTINUOUS);
    ReactivityUtil.ensureJsProperty(props, "particleSizeDistribution", props::setParticleSizeDistribution, 0);
  }

  public final @JsOverlay HeatContentType getHeatContentType() {
    return HeatContentType.safeValueOf(heatContentType);
  }

  public final @JsOverlay void setHeatContentType(final HeatContentType heatContentType) {
    this.heatContentType = heatContentType == null ? null : heatContentType.name();
  }

  public final @JsOverlay double getHeatContent() {
    return heatContent;
  }

  public final @JsOverlay void setHeatContent(final double heatContent) {
    this.heatContent = heatContent;
  }

  public final @JsOverlay double getEmissionTemperature() {
    return emissionTemperature;
  }

  public final @JsOverlay void setEmissionTemperature(final double emissionTemperature) {
    this.emissionTemperature = emissionTemperature;
  }

  public final @JsOverlay double getOutflowDiameter() {
    return outflowDiameter;
  }

  public final @JsOverlay void setOutflowDiameter(final double outflowDiameter) {
    this.outflowDiameter = outflowDiameter;
  }

  public final @JsOverlay double getOutflowVelocity() {
    return outflowVelocity;
  }

  public final @JsOverlay void setOutflowVelocity(final double outflowVelocity) {
    this.outflowVelocity = outflowVelocity;
  }

  public final @JsOverlay OutflowDirectionType getOutflowDirection() {
    return OutflowDirectionType.safeValueOf(outflowDirection);
  }

  public final @JsOverlay void setOutflowDirection(final OutflowDirectionType outflowDirection) {
    this.outflowDirection = outflowDirection.name();
  }

  public final @JsOverlay double getEmissionHeight() {
    return emissionHeight;
  }

  public final @JsOverlay void setEmissionHeight(final double emissionHeight) {
    this.emissionHeight = emissionHeight;
  }

  public final @JsOverlay int getDiameter() {
    return diameter;
  }

  public final @JsOverlay void setDiameter(final int diameter) {
    this.diameter = diameter;
  }

  public final @JsOverlay double getSpread() {
    return spread;
  }

  public final @JsOverlay void setSpread(final double spread) {
    this.spread = spread;
  }

  public final @JsOverlay DiurnalVariation getDiurnalVariation() {
    return DiurnalVariation.safeValueOf(diurnalVariation);
  }

  public final @JsOverlay void setDiurnalVariation(final DiurnalVariation diurnalVariation) {
    setDiurnalVariation(diurnalVariation.name());
  }

  public final @JsOverlay void setDiurnalVariation(final String diurnalVariation) {
    this.diurnalVariation = diurnalVariation;
  }

  public final @JsOverlay int getParticleSizeDistribution() {
    return particleSizeDistribution;
  }

  public final @JsOverlay void setParticleSizeDistribution(final int particleSizeDistribution) {
    this.particleSizeDistribution = particleSizeDistribution;
  }
}
