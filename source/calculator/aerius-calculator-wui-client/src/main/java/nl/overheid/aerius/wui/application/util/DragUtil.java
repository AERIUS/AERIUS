/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.util;

import elemental2.dom.DomGlobal;
import elemental2.dom.Event;
import elemental2.dom.HTMLElement;
import elemental2.dom.KeyboardEvent;
import elemental2.dom.MouseEvent;

import nl.aerius.wui.util.ComputedStyleUtil;

public final class DragUtil {
  private DragUtil() {}

  public static class DragHandler {
    private static final int KEY_MOVE_AMOUNT = 25;

    private static final double LOCAL_BOUND_POW2 = Math.pow(25, 2);

    private double dockStartX;
    private double dockStartY;

    private double dragStartX;
    private double dragStartY;

    private double dragX;
    private double dragY;

    private boolean docked = true;

    private final HTMLElement target;

    private final Runnable onDock;
    private final Runnable onUndock;

    public DragHandler(final HTMLElement target, final Runnable onDock, final Runnable onUndock) {
      this.target = target;
      this.onDock = onDock;
      this.onUndock = onUndock;
    }

    private Object onMouseDown(final Event downEvent) {
      final MouseEvent mouseEvent = (MouseEvent) downEvent;

      // Determine the panel's natural top/left using a forced render and re-render
      // This shouldn't lead to any visual artifacts because it happens within the
      // same message in the JS event loop queue
      final String initialLeft = target.style.left;
      final String initialTop = target.style.top;
      target.style.left = null;
      target.style.top = null;
      ComputedStyleUtil.forceStyleRender(target);
      dockStartX = target.offsetLeft;
      dockStartY = target.offsetTop;

      target.style.left = initialLeft;
      target.style.top = initialTop;
      ComputedStyleUtil.forceStyleRender(target);

      dragStartX = target.offsetLeft;
      dragStartY = target.offsetTop;

      dragX = mouseEvent.clientX;
      dragY = mouseEvent.clientY;

      DomGlobal.document.documentElement.onmouseup = upEvent -> onMouseUp(upEvent);
      DomGlobal.document.documentElement.onmousemove = moveEvent -> onMouseMove(moveEvent);

      return null;
    }

    public Object onKeyDown(final Event moveEvent) {
      final KeyboardEvent e = (KeyboardEvent) moveEvent;

      switch (e.key) {
      case "ArrowUp":
        prevent(e);
        moveDelta(0, -KEY_MOVE_AMOUNT);
        break;
      case "ArrowDown":
        prevent(e);
        moveDelta(0, KEY_MOVE_AMOUNT);
        break;
      case "ArrowLeft":
        prevent(e);
        moveDelta(-KEY_MOVE_AMOUNT, 0);
        break;
      case "ArrowRight":
        prevent(e);
        moveDelta(KEY_MOVE_AMOUNT, 0);
        break;
      case "Escape":
        prevent(e);
        dock();
        resetPosition();
        break;
      }

      return null;
    }

    private void prevent(final KeyboardEvent e) {
      e.preventDefault();
      e.stopPropagation();
    }

    private void moveDelta(final int deltaX, final int deltaY) {
      final double leftNow = target.offsetLeft;
      final double topNow = target.offsetTop;

      undock();
      setPosition(leftNow + deltaX, topNow + deltaY);

    }

    private Object onMouseMove(final Event moveEvent) {
      final MouseEvent mouseEvent = (MouseEvent) moveEvent;
      mouseEvent.preventDefault();

      final double deltaX = dragX - mouseEvent.clientX;
      final double deltaY = dragY - mouseEvent.clientY;

      final double dragTargetX = dragStartX - deltaX;
      final double dragTargetY = dragStartY - deltaY;

      final double dockDeltaX = Math.abs(dockStartX - dragTargetX);
      final double dockDeltaY = Math.abs(dockStartY - dragTargetY);

      if (withinOriginBounds(dockDeltaX, dockDeltaY)) {
        // If we are about to dock, notify
        dock();
        resetPosition();
      } else {
        undock();
        setPosition(dragTargetX, dragTargetY);
      }

      return null;
    }

    private void resetPosition() {
      target.style.left = null;
      target.style.top = null;
    }

    private void setPosition(final double dragTargetX, final double dragTargetY) {
      // Position using the css clamp function, so we don't need fancy mumbo jumbo to
      // fix the popup location on window resizes.
      target.style.setProperty("--left", dragTargetX + "px");
      target.style.setProperty("--top", dragTargetY + "px");
      target.style.setProperty("--width", target.offsetWidth + "px");
      target.style.setProperty("--height", Math.max(50, target.clientHeight) + "px");
      target.style.left = "clamp(var(--half-spacer), var(--left), calc(100vw - var(--width) - var(--half-spacer)))";
      target.style.top = "clamp(var(--half-spacer), var(--top), calc(100vh - var(--height) - var(--half-spacer)))";
    }

    private void undock() {
      if (onUndock != null && docked) {
        onUndock.run();
        docked = false;
      }
    }

    private void dock() {
      if (onDock != null && !docked) {
        onDock.run();
        docked = true;
      }
    }

    private boolean withinOriginBounds(final double deltaX, final double deltaY) {
      // Do pythagorean check for local proximity
      return LOCAL_BOUND_POW2 > Math.pow(deltaX, 2) + Math.pow(deltaY, 2);
    }

    private Object onMouseUp(final Event e) {
      DomGlobal.document.documentElement.onmouseup = null;
      DomGlobal.document.documentElement.onmousemove = null;
      return null;
    }
  }

  public static DragHandler draggable(final HTMLElement target, final HTMLElement handle) {
    return draggable(target, handle, null, null);
  }

  public static DragHandler draggable(final HTMLElement target, final HTMLElement handle, final Runnable onDock, final Runnable onUndock) {
    final DragHandler handler = new DragHandler(target, onDock, onUndock);

    handle.onmousedown = e -> handler.onMouseDown(e);
    handle.onkeydown = e -> handler.onKeyDown(e);

    return handler;
  }

  public static void reset(final HTMLElement elem) {
    elem.style.left = null;
    elem.style.top = null;
  }
}
