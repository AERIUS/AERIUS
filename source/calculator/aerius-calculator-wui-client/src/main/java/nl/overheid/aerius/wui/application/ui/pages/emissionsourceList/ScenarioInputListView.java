/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.emissionsourceList;

import java.util.List;
import java.util.Set;
import java.util.function.Function;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.geo.command.MapResizeSequenceCommand;
import nl.aerius.wui.place.PlaceController;
import nl.overheid.aerius.shared.domain.context.InputTypeViewMode;
import nl.overheid.aerius.wui.application.command.situation.CreateEmptySituationCommand;
import nl.overheid.aerius.wui.application.command.situation.DuplicateSituationCommand;
import nl.overheid.aerius.wui.application.command.situation.SituationDeleteCommand;
import nl.overheid.aerius.wui.application.command.situation.SwitchSituationCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.tooltip.TooltipComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.ui.nca.pages.emisionsourceList.timevarying.TimeVaryingProfileListComponent;
import nl.overheid.aerius.wui.application.ui.pages.emissionsourceList.building.BuildingListComponent;
import nl.overheid.aerius.wui.application.ui.pages.emissionsourceList.situation.SituationEditorView;
import nl.overheid.aerius.wui.application.ui.pages.emissionsourceList.source.EmissionSourceListComponent;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component(components = {
    SimplifiedListBoxComponent.class,
    TooltipComponent.class,
    ButtonIcon.class,

    SituationEditorView.class,

    CollapsiblePanel.class,
    EmissionSourceListComponent.class,
    BuildingListComponent.class,
    TimeVaryingProfileListComponent.class,
})
public class ScenarioInputListView extends BasicVueView {
  @Prop EventBus eventBus;

  @Inject @Data ScenarioContext context;
  @Inject @Data ScenarioInputListContext inputListContext;
  @Inject @Data ApplicationContext appContext;

  @Inject PlaceController placeController;

  @Computed("hasAnySituationInput")
  public boolean hasAnySituationInput() {
    return SituationContext.hasAnyContent(context.getActiveSituation());
  }

  @JsMethod
  public String getName(final SituationContext situation) {
    return SituationContext.formatName(situation);
  }

  @Computed
  public List<SituationContext> getSituations() {
    return context.getSituations();
  }

  @Computed("situationToKey")
  public Function<Object, Object> situationToKey() {
    return v -> ((SituationContext) v).getId();
  }

  @JsMethod
  public boolean isActiveSituation(final SituationContext situation) {
    return context.isActiveSituation(situation);
  }

  @Computed
  public Set<InputTypeViewMode> getAvailableViewModes() {
    return appContext.getConfiguration().getInputTypeViewModes();
  }

  @JsMethod
  public boolean hasType(final InputTypeViewMode type) {
    return getAvailableViewModes().contains(type);
  }

  @JsMethod
  public void selectViewMode(final InputTypeViewMode inputTypeViewMode) {
    inputListContext.setViewMode(inputTypeViewMode);
    eventBus.fireEvent(new MapResizeSequenceCommand());
  }

  @JsMethod
  public boolean isOpen(final InputTypeViewMode type) {
    return inputListContext.getViewMode() == type;
  }

  @JsMethod
  public void selectSituation(final SituationContext situation) {
    eventBus.fireEvent(new SwitchSituationCommand(situation));
  }

  @JsMethod
  public void removeSituation() {
    if (Window.confirm(i18n.navigationSituationDeleteConfirm(getActiveSituation().getName()))) {
      eventBus.fireEvent(new SituationDeleteCommand(getActiveSituation()));
    }
  }

  @JsMethod
  public void newSituation() {
    eventBus.fireEvent(new CreateEmptySituationCommand(true));
  }

  @JsMethod
  public void duplicateSituation() {
    eventBus.fireEvent(new DuplicateSituationCommand(getActiveSituation()));
  }

  @Computed
  public SituationContext getActiveSituation() {
    return context.getActiveSituation();
  }

  @Computed("hasActiveSituation")
  public boolean hasActiveSituation() {
    return context.hasActiveSituation();
  }
}
