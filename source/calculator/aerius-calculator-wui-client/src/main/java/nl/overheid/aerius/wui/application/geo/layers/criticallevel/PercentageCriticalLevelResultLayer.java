/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.criticallevel;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;

import nl.aerius.geo.domain.legend.Legend;
import nl.aerius.geo.domain.legend.LegendType;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.legend.ColorRangeType;
import nl.overheid.aerius.shared.domain.legend.ColorRangesLegend;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;

public class PercentageCriticalLevelResultLayer extends BasePCLResultLayer {

  private static final PercentageCriticalLevelEventBinder EVENT_BINDER = GWT.create(PercentageCriticalLevelEventBinder.class);

  interface PercentageCriticalLevelEventBinder extends EventBinder<PercentageCriticalLevelResultLayer> {}

  @Inject
  public PercentageCriticalLevelResultLayer(@Assisted final ApplicationConfiguration themeConfiguration, final EventBus eventBus,
      final EnvironmentConfiguration configuration, final CalculationContext calculationContext) {
    super("_percentage_cl_", M.messages().layerPercentageCL(), themeConfiguration, eventBus, configuration, calculationContext);

    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Override
  protected Legend createLegend() {
    ColorRangeType colorRangeType = null;

    switch (resultType) {
    case PROJECT_CALCULATION:
      colorRangeType = ColorRangeType.PROJECT_CALCULATION_PERCENTAGE_CRITICAL_LOAD;
      break;
    case IN_COMBINATION:
      colorRangeType = ColorRangeType.IN_COMBINATION_PERCENTAGE_CRITICAL_LOAD;
      break;
    case ARCHIVE_CONTRIBUTION:
      colorRangeType = ColorRangeType.ARCHIVE_CONTRIBUTION_PERCENTAGE_CRITICAL_LOAD;
      break;
    }

    final EmissionResultType resultType = emissionResultKey.getEmissionResultType();

    return new ColorRangesLegend(
        themeConfiguration.getColorRange(colorRangeType),
        String::valueOf,
        M.messages().colorRangesLegendBetweenText(),
        resultType == EmissionResultType.DEPOSITION
            ? LegendType.HEXAGON
            : LegendType.CIRCLE,
        M.messages().unitSingularPercentageCL());
  }
}
