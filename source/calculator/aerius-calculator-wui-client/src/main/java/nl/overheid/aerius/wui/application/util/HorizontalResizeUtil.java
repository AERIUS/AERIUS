/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.util;

import java.util.function.Consumer;

import elemental2.dom.DomGlobal;
import elemental2.dom.Element;
import elemental2.dom.EventListener;
import elemental2.dom.MouseEvent;

import jsinterop.base.Js;

public final class HorizontalResizeUtil {
  private final double dragStartX;
  private final Consumer<Integer> deltaRunner;
  private final Element handle;

  private final EventListener mouseMoveCallback = e -> drag(Js.uncheckedCast(e));
  private final EventListener stopDragCallback = e -> {
    stopResizeDrag();
    releasePointerCapture(Js.uncheckedCast(e));
  };
  private final EventListener visibilityChangeCallback = e -> visibilityChange();

  private HorizontalResizeUtil(final Element handle, final MouseEvent ev, final Consumer<Integer> deltaRunner) {
    this.handle = handle;
    this.deltaRunner = deltaRunner;

    setPointerCapture(handle, ev);
    DomGlobal.document.body.addEventListener("pointermove", mouseMoveCallback);
    DomGlobal.document.addEventListener("visibilitychange", visibilityChangeCallback);
    DomGlobal.document.body.addEventListener("pointerup", stopDragCallback);
    dragStartX = ev.pageX;
  }

  public static void startResizeDrag(final Element handle, final MouseEvent ev, final Consumer<Integer> deltaRunner) {
    new HorizontalResizeUtil(handle, ev, deltaRunner);
  }

  private void visibilityChange() {
    if (DomGlobal.document.hidden) {
      stopResizeDrag();
    }
  }

  private void stopResizeDrag() {
    DomGlobal.document.body.removeEventListener("pointermove", mouseMoveCallback);
    DomGlobal.document.removeEventListener("visibilitychange", visibilityChangeCallback);
    DomGlobal.document.body.removeEventListener("pointerup", stopDragCallback);
  }

  private void releasePointerCapture(final MouseEvent ev) {
    releasePointerCapture(handle, ev);
  }

  private void drag(final MouseEvent ev) {
    final int delta = (int) (ev.pageX - dragStartX);
    deltaRunner.accept(delta);
  }

  private static native void setPointerCapture(Element handle, MouseEvent ev) /*-{
    handle.setPointerCapture(ev.pointerId);
  }-*/;

  private static native void releasePointerCapture(Element handle, MouseEvent ev) /*-{
    handle.releasePointerCapture(ev.pointerId);
  }-*/;

  public static native String getPropertyValue(Element el, String prop) /*-{
    return getComputedStyle(el).getPropertyValue(prop);
  }-*/;
}
