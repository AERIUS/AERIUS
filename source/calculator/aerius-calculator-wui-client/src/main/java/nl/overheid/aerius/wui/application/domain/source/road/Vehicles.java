/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.road;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.wui.application.domain.source.AbstractSubSource;
import nl.overheid.aerius.wui.vue.ReactivityUtil;
import nl.overheid.aerius.wui.vue.SkipReactivityValidations;

/**
 * Client side implementation of Road Vehicles.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public abstract class Vehicles extends AbstractSubSource {
  private String vehicleType;
  private String timeUnit;

  public static final @JsOverlay void initVehicles(final Vehicles props, final VehicleType type) {
    AbstractSubSource.initAbstractSubSource(props);
    props.setVehicleType(type);
    ReactivityUtil.ensureJsProperty(props, "timeUnit", props::setTimeUnit, TimeUnit.DAY);
  }

  @SkipReactivityValidations
  public static final @JsOverlay void initTypeDependent(final Vehicles props, final VehicleType type) {
    switch (type) {
    case CUSTOM:
      CustomVehicles.init(Js.uncheckedCast(props));
      break;
    case SPECIFIC:
      SpecificVehicles.init(Js.uncheckedCast(props));
      break;
    case STANDARD:
      StandardVehicles.init(Js.uncheckedCast(props));
      break;
    default:
      throw new RuntimeException("Unknown type: " + type);
    }
  }

  @SkipReactivityValidations
  public static final @JsOverlay void initColdStartVehiclesTypeDependent(final Vehicles props, final VehicleType type) {
    switch (type) {
    case STANDARD_CS:
      ColdStartStandardVehicles.init(Js.uncheckedCast(props));
      break;
    case CUSTOM:
    case SPECIFIC:
    default:
      initTypeDependent(props, type);
      break;
    }
  }

  public final @JsOverlay VehicleType getVehicleType() {
    return VehicleType.safeValueOf(vehicleType);
  }

  public final @JsOverlay void setVehicleType(final VehicleType type) {
    this.vehicleType = type.name();
  }

  public final @JsOverlay TimeUnit getTimeUnit() {
    return TimeUnit.valueOf(timeUnit);
  }

  public final @JsOverlay void setTimeUnit(final TimeUnit timeUnit) {
    this.timeUnit = timeUnit.name();
  }

}
