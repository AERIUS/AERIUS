/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.nca.pages.calculate.options;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.aerius.vuelidate.ValidationOptions;
import nl.aerius.vuelidate.Validations;
import nl.aerius.vuelidate.ValidatorBuilder;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.ADMSLimits;

class CalculationOptionsValidators extends ValidationOptions<CalculationJobMeteorologicalSettingsComponent> {
  @JsType(isNative = true, namespace = JsPackage.GLOBAL)
  static class CalculationOptionsValidations extends Validations {

    public @JsProperty Validations minMoninObukhovLength;
    public @JsProperty Validations surfaceAlbedo;
    public @JsProperty Validations priestleyTaylorParameter;
    public @JsProperty Validations roadLocalFractionNO2;
  }

  @Override
  protected void constructValidators(final Object options, final ValidatorInstaller v) {
    final CalculationJobAdvancedSettingsComponent instance = Js.uncheckedCast(options);
    v.install("minMoninObukhovLength", () -> instance.minMoninObukhovLengthV = null, ValidatorBuilder.create()
        .required()
        .decimal()
        .minValue(ADMSLimits.MIN_MONIN_OBUKHOV_LENGTH_MIN)
        .maxValue(ADMSLimits.MIN_MONIN_OBUKHOV_LENGTH_MAX));
    v.install("surfaceAlbedo", () -> instance.surfaceAlbedoV = null, ValidatorBuilder.create()
        .required()
        .decimal()
        .minValue(ADMSLimits.SURFACE_ALBEDO_MIN)
        .maxValue(ADMSLimits.SURFACE_ALBEDO_MAX));
    v.install("priestleyTaylorParameter", () -> instance.priestleyTaylorParameterV = null, ValidatorBuilder.create()
        .required()
        .decimal()
        .minValue(ADMSLimits.PRIESTLEY_TAYLOR_PARAMETER_MIN)
        .maxValue(ADMSLimits.PRIESTLEY_TAYLOR_PARAMETER_MAX));
    v.install("roadLocalFractionNO2", () -> instance.roadLocalFractionNO2V = null, ValidatorBuilder.create()
        .decimal()
        .minValue(0.0)
        .maxValue(1.0));
  }
}
