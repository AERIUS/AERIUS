/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.scenario;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.geom.GeometryType;

import nl.aerius.wui.command.PlaceChangeCommand;
import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.place.PlaceController;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.context.InputTypeViewMode;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.SourceType;
import nl.overheid.aerius.shared.util.ConsecutiveIdUtil;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceAddCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceCreateNewCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceCreateNewEvent;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditCancelCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditCancelEvent;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditSaveCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditSaveEvent;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceFeatureSelectCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceFinishDrawGeometryCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourcePersistGeometryCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceSectorChangeCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceSourceTypeChangeCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceUpdateSourceTypeCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSubSourceDeselectFeatureCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSubSourceSelectFeatureCommand;
import nl.overheid.aerius.wui.application.command.source.SectorGroupSelectionCommand;
import nl.overheid.aerius.wui.application.command.source.TemporaryEmissionSourceAddCommand;
import nl.overheid.aerius.wui.application.command.source.TemporaryEmissionSourceClearCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.EmissionSourceEditorContext;
import nl.overheid.aerius.wui.application.context.EmissionSourceValidationContext;
import nl.overheid.aerius.wui.application.context.GenericValidationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.domain.ClientConsecutiveIdUtil;
import nl.overheid.aerius.wui.application.domain.geo.FeatureUtil;
import nl.overheid.aerius.wui.application.domain.source.ADMSCharacteristics;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.GenericESFeature;
import nl.overheid.aerius.wui.application.domain.source.SourceCharacteristics;
import nl.overheid.aerius.wui.application.domain.source.util.EmissionSourceFeatureUtil;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceUpdatedEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.place.BuildingPlace;
import nl.overheid.aerius.wui.application.place.EmissionSourcePlace;
import nl.overheid.aerius.wui.application.place.ScenarioInputListPlace;
import nl.overheid.aerius.wui.application.place.TimeVaryingProfilePlace;

public class EmissionSourceEditorDaemon extends BasicEventComponent {
  private static final EmissionSourceEditorDaemonEventBinder EVENT_BINDER = GWT.create(EmissionSourceEditorDaemonEventBinder.class);

  interface EmissionSourceEditorDaemonEventBinder extends EventBinder<EmissionSourceEditorDaemon> {}

  @Inject private ApplicationContext applicationContext;
  @Inject private ScenarioContext scenarioContext;
  @Inject private GenericValidationContext genericValidationContext;
  @Inject private EmissionSourceEditorContext editorContext;
  @Inject private EmissionSourceValidationContext validationContext;

  @Inject private PlaceController placeController;

  private EmissionSourceFeature originalEmissionSource;
  private String temporarySituationId;
  private EmissionSourceFeature temporaryEmissionSource;

  @EventHandler
  public void onSectorGroupSelectionCommand(final SectorGroupSelectionCommand c) {
    eventBus.fireEvent(new EmissionSubSourceDeselectFeatureCommand());

    editorContext.setSelectedSectorGroup(c.getValue());

    final List<Sector> lst = getAvailableSectors();
    if (lst.size() == 1) {
      final Sector sector = lst.get(0);
      eventBus.fireEvent(new EmissionSourceSectorChangeCommand(sector));
    } else {
      editorContext.getSource().setSectorId(0); // Resets the sector ID
    }
  }

  public List<Sector> getAvailableSectors() {
    final SectorGroup selectedSectorGroup = editorContext.getSelectedSectorGroup();
    return applicationContext.getConfiguration().getSectorCategories().getSectors().stream()
        .filter(v -> v.getSectorGroup() == selectedSectorGroup)
        .collect(Collectors.toList());
  }

  @EventHandler
  public void onEmissionSourceCreateNewCommand(final EmissionSourceCreateNewCommand c) {
    final EmissionSourceFeature source = createNewEmissionSource();

    final List<EmissionSourceFeature> sources = scenarioContext.getActiveSituation().getSources();
    final String nextLabel = ClientConsecutiveIdUtil.getNextLabel(sources);
    source.setLabel(M.messages().esLabelDefaultPrefix(nextLabel));

    final ConsecutiveIdUtil.IndexIdPair newId = ClientConsecutiveIdUtil.getConsecutiveIdUtil(sources).generate();
    source.setId(newId.getId());

    beginEmissionSourceEdit(source);
  }

  @EventHandler
  public void onEmissionSourceSourceTypeChangeCommand(final EmissionSourceSourceTypeChangeCommand c) {
    final SourceCharacteristics chars = c.getValue().getCharacteristics();

    // For ADMS Sources, sever building link and set source type
    if (chars.getCharacteristicsType() == CharacteristicsType.ADMS) {
      if (chars.isBuildingInfluence()) {
        chars.setBuildingGmlId(null);
      }

      EmissionSourceFeatureUtil.updateADMSSourceType((ADMSCharacteristics) chars, c.getType());
    }
  }

  @EventHandler
  public void onEmissionSourceCreateNewEvent(final EmissionSourceCreateNewEvent c) {
    placeController.goTo(new EmissionSourcePlace(applicationContext.getTheme()));
  }

  @EventHandler
  public void onEmissionSourceEditCommand(final EmissionSourceEditCommand c) {
    final EmissionSourceFeature clone = FeatureUtil.clone(c.getValue());

    beginEmissionSourceEdit(clone, c.getValue());
  }

  private EmissionSourceFeature createNewEmissionSource() {
    final EmissionSourceFeature emissionSource = GenericESFeature.create(applicationContext.getConfiguration().getCharacteristicsType());

    final String nextLabel = ClientConsecutiveIdUtil.getNextLabel(scenarioContext.getActiveSituation().getSources());
    emissionSource.setLabel(M.messages().esLabelDefaultPrefix(nextLabel));

    /**
     * TODO/FIXME OLD PROBLEM (doesn't exist here anymore, but the underlying issue
     * still does)
     *
     * <pre>
     *  Hmm, not very happy with a command being fired from inside, effectively, a command.
     *  But this is the many-th time I've seen it, so it must make at least intuitive sense to do so.
     *  Currently, this command is deferred until after the previous command's event is handled.
     *  Maybe have to look into re-ordering the way the command bus prioritises commands over
     *  events so that if indeed this is a common and expected pattern that no strange things
     *  can happen.
     *  i.e. for the situation where 'commandB' is fired from within the handler that handles 'commandA',
     *  maybe the order should be:
     *  commandA -> commandB -> commandA'sEvent -> commandB'sEvent
     *  rather than the current
     *  commandA -> commandA'sEvent -> commandB -> commandB'sEvent
     * </pre>
     */

    return emissionSource;
  }

  @EventHandler
  public void onEmissionSourceUpdateSourceTypeCommand(final EmissionSourceUpdateSourceTypeCommand c) {
    final EmissionSourceFeature source = editorContext.getSource();
    updateSourceType(source);
  }

  private void updateSourceType(final EmissionSourceFeature source) {
    // For ADMS Characteristics, synchronize the source type defaults to selection
    if (source.getCharacteristicsType() != CharacteristicsType.ADMS
        || source.getGeometry() == null) {
      return;
    }

    final ADMSCharacteristics chars = (ADMSCharacteristics) source.getCharacteristics();
    final SourceType sourceType = chars.getSourceType();
    final GeometryType geometryType = GeometryType.valueOf(source.getGeometry().getType());

    // If the geometry is a point, and the source type is not either a jet or point,
    // reset it to point
    if (geometryType == GeometryType.Point
        && sourceType != SourceType.JET && sourceType != SourceType.POINT) {
      updateSourceType(source, SourceType.POINT);
    } else

    // If the geometry is a polygon, and the source type is not either an area or
    // volume, reset it to area
    if (geometryType == GeometryType.Polygon
        && sourceType != SourceType.AREA && sourceType != SourceType.VOLUME) {
      updateSourceType(source, SourceType.AREA);
    } else

    // If the geometry is a line, and the source type is not either a line or road,
    // reset it to line
    if (geometryType == GeometryType.LineString
        && sourceType != SourceType.LINE && sourceType != SourceType.ROAD) {
      updateSourceType(source, SourceType.LINE);
    }
  }

  private void updateSourceType(final EmissionSourceFeature source, final SourceType type) {
    final ADMSCharacteristics chars = (ADMSCharacteristics) source.getCharacteristics();
    chars.setSourceType(type);
    eventBus.fireEvent(new EmissionSourceSourceTypeChangeCommand(source, type));
  }

  @EventHandler
  public void onEmissionSourceFinishDrawGeometryCommand(final EmissionSourceFinishDrawGeometryCommand c) {
    validationContext.setSectorRequiredHintWarning(true);
  }

  @EventHandler
  public void onEmissionSourcePersistGeometryCommand(final EmissionSourcePersistGeometryCommand c) {
    validationContext.setSectorRequiredHintWarning(true);
    final EmissionSourceFeature source = editorContext.getSource();

    if (source == null) {
      // TODO Remove when confident this will no longer occur, this gets hit first
      // when a regression is implemented
      GWTProd.warn("Something is wishing for us to modify a source in some way, but a source isn't being edited.");
    } else {
      source.setGeometry(c.getValue());
      eventBus.fireEvent(new EmissionSourceUpdateSourceTypeCommand());
      eventBus.fireEvent(new EmissionSourceUpdatedEvent(source, scenarioContext.getActiveSituation()));
    }
  }

  @EventHandler
  public void onEmissionSourceEditSaveCommand(final EmissionSourceEditSaveCommand c) {
    if (originalEmissionSource == null) {
      // Not a copy, so purge IDs so they can be set properly
      final EmissionSourceFeature source = FeatureUtil.cloneWithoutIds(editorContext.getSource());

      eventBus.fireEvent(new EmissionSourceAddCommand(source, scenarioContext.getActiveSituation()));
    } else {
      FeatureUtil.copy(editorContext.getSource(), originalEmissionSource);
      eventBus.fireEvent(new EmissionSourceFeatureSelectCommand(originalEmissionSource, false));
      eventBus.fireEvent(new EmissionSourceUpdatedEvent(originalEmissionSource, scenarioContext.getActiveSituation()));
      originalEmissionSource = null;
    }

    finishEmissionSourceEdit(editorContext.getSource());
  }

  @EventHandler
  public void onEmissionSourceEditSaveEvent(final EmissionSourceEditSaveEvent e) {
    exitEmissionSourcePlace();
  }

  @EventHandler
  public void onEmissionSourceEditCancelCommand(final EmissionSourceEditCancelCommand c) {
    finishEmissionSourceEdit(editorContext.getSource());
  }

  @EventHandler
  public void onEmissionSourceEditCancelEvent(final EmissionSourceEditCancelEvent c) {
    exitEmissionSourcePlace();
  }

  @EventHandler
  public void onPlaceChangeCommand(final PlaceChangeCommand e) {
    if (editorContext.isEditing()
        && !(e.getValue() instanceof EmissionSourcePlace)
        && !(e.getValue() instanceof TimeVaryingProfilePlace)
        && !(e.getValue() instanceof BuildingPlace)
        && !e.isCancelled()) {
      final EmissionSourceEditCancelCommand cmd = new EmissionSourceEditCancelCommand();
      cmd.silence();
      eventBus.fireEvent(cmd);
    }
  }

  private void exitEmissionSourcePlace() {
    placeController.goTo(new ScenarioInputListPlace(applicationContext.getTheme(), InputTypeViewMode.EMISSION_SOURCES));
  }

  @EventHandler
  public void onEmissionSubSourceDeselectFeatureCommand(final EmissionSubSourceDeselectFeatureCommand c) {
    editorContext.resetSubSourceSelection();
    genericValidationContext.reset();
  }

  @EventHandler
  public void onEmissionSubSourceSelectFeatureCommand(final EmissionSubSourceSelectFeatureCommand c) {
    editorContext.setSubSourceSelection(c.getValue());
    genericValidationContext.reset();
  }

  @EventHandler
  public void onChangeEmissionSourceSectorCommand(final EmissionSourceSectorChangeCommand c) {
    final ApplicationConfiguration themeConfig = applicationContext.getConfiguration();
    final EmissionSourceFeature newSource = EmissionSourceFeatureUtil.changeEmissionsSourceFeature(c.getValue(), editorContext.getSource(),
        themeConfig.getSectorPropertiesSet(), themeConfig.getCharacteristicsType());

    swapSource(newSource);
  }

  private void swapSource(final EmissionSourceFeature newSource) {
    eventBus.fireEvent(new TemporaryEmissionSourceClearCommand(temporaryEmissionSource, temporarySituationId));
    temporaryEmissionSource = newSource;
    editorContext.setSource(scenarioContext.getActiveSituation(), newSource);
    genericValidationContext.reset();
    eventBus.fireEvent(new TemporaryEmissionSourceAddCommand(temporaryEmissionSource, temporarySituationId));
  }

  private void beginEmissionSourceEdit(final EmissionSourceFeature source) {
    beginEmissionSourceEdit(source, null);
  }

  private void beginEmissionSourceEdit(final EmissionSourceFeature source, final EmissionSourceFeature original) {
    if (temporaryEmissionSource != null) {
      // Ideally we shouldn't be cleaning up (although it can be legitimate), so warn
      // about this occurring
      GWTProd.warn("Clearing temporary emission source at a point where ideally we shouldn't be doing that.");
      finishEmissionSourceEdit(temporaryEmissionSource);
    }

    editorContext.setSource(scenarioContext.getActiveSituation(), source);
    final SectorGroup sectorGroupFromSectorId = applicationContext.getConfiguration().getSectorCategories()
        .findSectorGroupFromSectorId(source.getSectorId());
    editorContext.setSelectedSectorGroup(sectorGroupFromSectorId);
    genericValidationContext.reset();

    temporaryEmissionSource = source;
    temporaryEmissionSource.setId("-" + source.getId());
    temporarySituationId = scenarioContext.getActiveSituationId();
    eventBus.fireEvent(new TemporaryEmissionSourceAddCommand(source, temporarySituationId));

    if (original != null) {
      originalEmissionSource = original;
    } else {
      originalEmissionSource = null;
    }
  }

  private void finishEmissionSourceEdit(final EmissionSourceFeature source) {
    if (temporaryEmissionSource != null) {
      eventBus.fireEvent(new TemporaryEmissionSourceClearCommand(temporaryEmissionSource, temporarySituationId));
      temporaryEmissionSource = null;
      temporarySituationId = null;
    }

    editorContext.reset();
    genericValidationContext.reset();
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
