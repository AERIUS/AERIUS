/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.source.farmlodging.systems.types;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Emit;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategories;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.MinimalInputComponent;
import nl.overheid.aerius.wui.application.components.input.suggest.SuggestListBoxComponent;
import nl.overheid.aerius.wui.application.components.input.suggest.SuggestListBoxData;
import nl.overheid.aerius.wui.application.components.input.suggest.SuggestUtil;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.components.tooltip.TooltipComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.farm.LodgingFodderMeasure;

@Component(components = {
    MinimalInputComponent.class,
    ButtonIcon.class,
    SuggestListBoxComponent.class,
    TooltipComponent.class,
    ValidationBehaviour.class,
    InputErrorComponent.class,
})
public class FarmFodderMeasureRowComponent extends ErrorWarningValidator implements IsVueComponent {
  @Prop @JsProperty FarmLodgingCategory farmLodgingCategory;
  @Prop @JsProperty LodgingFodderMeasure lodgingSystem;
  @Prop Integer index;

  @Data @Inject ApplicationContext applicationContext;

  @Data @JsProperty List<SuggestListBoxData> fodderMeasureOptions = new ArrayList<>();

  @Data boolean displayProblems;

  @JsMethod
  public FarmLodgingCategories getCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getFarmLodgingCategories();
  }

  @Watch(value = "farmLodgingCategory", isImmediate = true)
  public void onFarmLodgingCategoryChange(final FarmLodgingCategory neww) {
    if (neww == null) {
      fodderMeasureOptions = new ArrayList<>();
    } else {
      fodderMeasureOptions = getCategories().getFarmLodgingFodderMeasureCategories().stream()
          .map(v -> new SuggestListBoxData(v.getCode(), v.getName(),
              v.getName() + " " + v.getDescription(), !v.canApplyToFarmLodgingCategory(neww)))
          .sorted(SuggestUtil.WARN)
          .collect(Collectors.toList());
    }
  }

  @Override
  @JsMethod
  public void displayProblems(final boolean displayProblems) {
    this.displayProblems = displayProblems;
  }

  @JsMethod
  @Emit(value = "delete")
  public void deleteRow() {}

  @JsMethod
  public void unselectFodderMeasureCode() {
    lodgingSystem.setFodderMeasureCode("");
  }

  @JsMethod
  void selectFodderMeasureCode(final String value) {
    lodgingSystem.setFodderMeasureCode(value);
  }

  @Computed("fodderMeasureCodeInvalid")
  public boolean fodderMeasureCodeInvalid() {
    return lodgingSystem.getFodderMeasureCode() == null
        || lodgingSystem.getFodderMeasureCode().isEmpty();
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return fodderMeasureCodeInvalid();
  }
}
