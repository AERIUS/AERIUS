/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.shipping.inland;

import elemental2.core.JsArray;

import nl.overheid.aerius.wui.application.components.modify.ModifyListActions;
import nl.overheid.aerius.wui.application.components.source.SubSourceEmissionEditorPresenter;
import nl.overheid.aerius.wui.application.domain.source.shipping.inland.MooringInlandShipping;
import nl.overheid.aerius.wui.application.domain.source.shipping.inland.StandardMooringInlandShipping;

public class InlandMooringShippingEmissionEditorPresenter extends
    SubSourceEmissionEditorPresenter<InlandMooringShippingEmissionEditorComponent, MooringInlandShipping>
    implements ModifyListActions {
  @Override
  protected MooringInlandShipping createSubSource() {
    return StandardMooringInlandShipping.create();
  }

  @Override
  protected JsArray<MooringInlandShipping> getSubSources() {
    return view.source.getSubSources();
  }
}
