/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.ui.pages.calculationpoints;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.EventBus;

import elemental2.dom.MouseEvent;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.shared.domain.v2.point.AssessmentCategory;
import nl.overheid.aerius.wui.application.command.calculation.CalculationPointDeleteAllCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointCreateNewCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointDeleteSelectedCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointEditSelectedCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointToggleSelectCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.OpenCalculationPointAutomaticPlacement;
import nl.overheid.aerius.wui.application.command.source.FeatureZoomCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.modify.ModifyListComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.CalculationPointListContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.daemon.misc.PersistablePreferencesContext;
import nl.overheid.aerius.wui.application.daemon.misc.PreferencesDaemon;
import nl.overheid.aerius.wui.application.daemon.misc.UserSettingChangeCommand;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;
import nl.overheid.aerius.wui.application.util.DoubleClickUtil;
import nl.overheid.aerius.wui.vue.BasicVueEventComponent;

@Component(components = {
    CalculationPointRowComponent.class,
    LabeledInputComponent.class,
    ModifyListComponent.class,
    SimplifiedListBoxComponent.class,
    VerticalCollapse.class,
    VerticalCollapseGroup.class,
    ButtonIcon.class,
})
public class CalculationPointsListComponent extends BasicVueEventComponent {

  private final DoubleClickUtil<CalculationPointFeature> doubleClickUtil = new DoubleClickUtil<>();

  @Prop EventBus eventBus;
  @Inject @Data CalculationPointListContext listContext;
  @Inject @Data ScenarioContext scenarioContext;
  @Inject @Data ApplicationContext applicationContext;
  @Inject @Data PersistablePreferencesContext prefContext;

  @Computed
  public List<CalculationPointFeature> getCalculationPoints() {
    return scenarioContext.getCalculationPoints();
  }

  @Computed("hasCalculationPoints")
  public boolean hasCalculationPoints() {
    return !getCalculationPoints().isEmpty();
  }

  @Computed
  public List<AssessmentCategory> getAssessmentCategories() {
    return applicationContext.getConfiguration().getAssessmentCategories();
  }

  @Computed
  public AssessmentCategory getAssessmentCategory() {
    return prefContext.getAssessmentCategory();
  }

  @JsMethod
  public void selectAssessmentCategory(final AssessmentCategory category) {
    eventBus.fireEvent(new UserSettingChangeCommand(PreferencesDaemon.ASSESSMENT_POINT_CATEGORY, category));
  }

  @Computed("areCategoriesEnabled")
  public boolean areCategoriesEnabled() {
    return !getAssessmentCategories().isEmpty();
  }

  @JsMethod
  public void deleteAllCalculationPoints() {
    if (Window.confirm(i18n.calculationPointsDeleteWarning())) {
      eventBus.fireEvent(new CalculationPointDeleteAllCommand());
    }
  }

  @JsMethod
  public void selectPoint(final CalculationPointFeature point) {
    eventBus.fireEvent(new CalculationPointToggleSelectCommand(point, false));
  }

  @JsMethod
  public void multiSelectPoint(final CalculationPointFeature point) {
    eventBus.fireEvent(new CalculationPointToggleSelectCommand(point, true));
  }

  @JsMethod
  public void focusPoint(final CalculationPointFeature point) {
    eventBus.fireEvent(new FeatureZoomCommand(point));
  }

  @JsMethod
  public void clickPoint(final CalculationPointFeature point, final MouseEvent ev) {
    if (ev.ctrlKey) {
      multiSelectPoint(point);
    } else {
      doubleClickUtil.click(point, this::selectPoint, this::focusPoint);
    }
  }

  @JsMethod
  public void newCalculationPoint() {
    eventBus.fireEvent(new CalculationPointCreateNewCommand());
  }

  @JsMethod
  public void editCalculationPoint() {
    eventBus.fireEvent(new CalculationPointEditSelectedCommand());
  }

  @JsMethod
  public void deleteCalculationPoint() {
    eventBus.fireEvent(new CalculationPointDeleteSelectedCommand());
  }

  @JsMethod
  public void openAutomaticPlacementView() {
    eventBus.fireEvent(new OpenCalculationPointAutomaticPlacement());
  }
}
