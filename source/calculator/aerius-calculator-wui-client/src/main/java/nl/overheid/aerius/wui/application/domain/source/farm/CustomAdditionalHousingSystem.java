/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.farm;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.wui.application.domain.source.EmissionFactors;
import nl.overheid.aerius.wui.vue.ReactivityUtil;
import nl.overheid.aerius.wui.vue.SkipReactivityValidations;

/**
 * Client side implementation of CustomAdditionalHousingSystem props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class CustomAdditionalHousingSystem extends AdditionalHousingSystem {

  private String description;
  private boolean airScrubber;
  private EmissionFactors emissionReductionFactors;

  public static final @JsOverlay CustomAdditionalHousingSystem create() {
    final CustomAdditionalHousingSystem props = Js.uncheckedCast(JsPropertyMap.of());

    init(props);
    return props;
  }

  public static final @JsOverlay void init(final CustomAdditionalHousingSystem props) {
    AdditionalHousingSystem.init(props, AdditionalSystemType.CUSTOM);

    ReactivityUtil.ensureJsProperty(props, "description", props::setDescription, "");
    ReactivityUtil.ensureJsProperty(props, "airScrubber", props::setAirScrubber, false);
    ReactivityUtil.ensureJsPropertySupplied(props, "emissionReductionFactors", props::setEmissionReductionFactors, EmissionFactors::create);
    ReactivityUtil.ensureInitialized(props::emissionReductionFactors, v -> EmissionFactors.init(v, Substance.NH3));
  }

  public final @JsOverlay String getDescription() {
    return description;
  }

  public final @JsOverlay void setDescription(final String description) {
    this.description = description;
  }

  public final @JsOverlay boolean isAirScrubber() {
    return airScrubber;
  }

  public final @JsOverlay void setAirScrubber(final boolean airScrubber) {
    this.airScrubber = airScrubber;
  }


  @SkipReactivityValidations
  public final @JsOverlay double getEmissionReductionFactor(final Substance substance) {
    return emissionReductionFactors.getEmissionFactor(substance);
  }

  public final @JsOverlay void setEmissionReductionFactor(final Substance substance, final double emission) {
    emissionReductionFactors.setEmissionFactor(substance, emission);
  }

  public final @JsOverlay void setEmissionReductionFactors(final EmissionFactors emissionReductionFactor) {
    this.emissionReductionFactors = emissionReductionFactor;
  }

  private final @JsOverlay EmissionFactors emissionReductionFactors() {
    return emissionReductionFactors;
  }

  public final @JsOverlay EmissionFactors getEmissionReductionFactors() {
    return emissionReductionFactors;
  }
}
