/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.farm;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.overheid.aerius.wui.application.domain.source.AbstractSubSource;
import nl.overheid.aerius.wui.vue.ReactivityUtil;
import nl.overheid.aerius.wui.vue.SkipReactivityValidations;

/**
 * Client side implementation of FarmLodging.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public abstract class FarmLodging extends AbstractSubSource {
  private String farmLodgingType;
  private int numberOfAnimals;
  private int numberOfDays;

  public static final @JsOverlay void init(final FarmLodging props, final FarmLodgingType type) {
    AbstractSubSource.initAbstractSubSource(props);
    props.setFarmLodgingType(type);
    ReactivityUtil.ensureJsProperty(props, "numberOfAnimals", props::setNumberOfAnimals, 0);
    ReactivityUtil.ensureJsProperty(props, "numberOfDays", props::setNumberOfDays, 0);
  }

  @SkipReactivityValidations
  public static final @JsOverlay void initTypeDependent(final FarmLodging props, final FarmLodgingType type) {
    switch (type) {
    case CUSTOM:
      CustomFarmLodging.init(Js.uncheckedCast(props));
      break;
    case STANDARD:
      StandardFarmLodging.init(Js.uncheckedCast(props));
      break;
    default:
      throw new RuntimeException("Unknown type: " + type);
    }
  }

  public final @JsOverlay FarmLodgingType getFarmLodgingType() {
    return FarmLodgingType.safeValueOf(farmLodgingType);
  }

  public final @JsOverlay void setFarmLodgingType(final FarmLodgingType type) {
    this.farmLodgingType = type.name();
  }

  public final @JsOverlay int getNumberOfAnimals() {
    return numberOfAnimals;
  }

  public final @JsOverlay void setNumberOfAnimals(final int numberOfAnimals) {
    this.numberOfAnimals = numberOfAnimals;
  }

  public final @JsOverlay int getNumberOfDays() {
    return numberOfDays;
  }

  public final @JsOverlay void setNumberOfDays(final int numberOfDays) {
    this.numberOfDays = numberOfDays;
  }
}
