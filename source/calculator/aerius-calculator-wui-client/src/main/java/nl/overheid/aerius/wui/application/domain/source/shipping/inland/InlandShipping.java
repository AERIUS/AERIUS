/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.shipping.inland;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.wui.application.domain.source.shipping.base.AbstractShipping;
import nl.overheid.aerius.wui.vue.ReactivityUtil;
import nl.overheid.aerius.wui.vue.SkipReactivityValidations;

/**
 * Client side implementation of Inland Shipping.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public abstract class InlandShipping extends AbstractShipping {

  private String inlandShippingType;

  private String timeUnitAtoB;
  private String timeUnitBtoA;
  private int movementsAtoBPerTimeUnit;
  private int movementsBtoAPerTimeUnit;
  private int percentageLadenAtoB;
  private int percentageLadenBtoA;

  public static final @JsOverlay void initInlandShipping(final InlandShipping props, final InlandShippingType type) {
    AbstractShipping.initAbstractShipping(props);
    props.setInlandShippingType(type);

    ReactivityUtil.ensureJsProperty(props, "timeUnitAtoB", props::setTimeUnitAtoB, TimeUnit.YEAR);
    ReactivityUtil.ensureJsProperty(props, "timeUnitBtoA", props::setTimeUnitBtoA, TimeUnit.YEAR);
    ReactivityUtil.ensureJsProperty(props, "movementsAtoBPerTimeUnit", props::setMovementsAtoBPerTimeUnit, 0);
    ReactivityUtil.ensureJsProperty(props, "movementsBtoAPerTimeUnit", props::setMovementsBtoAPerTimeUnit, 0);
    ReactivityUtil.ensureJsProperty(props, "percentageLadenAtoB", props::setPercentageLadenAtoB, 0);
    ReactivityUtil.ensureJsProperty(props, "percentageLadenBtoA", props::setPercentageLadenBtoA, 0);
  }

  @SkipReactivityValidations
  public static final @JsOverlay void initTypeDependent(final InlandShipping props, final InlandShippingType type) {
    switch (type) {
    case CUSTOM:
      CustomInlandShipping.init(Js.uncheckedCast(props));
      break;
    case STANDARD:
      StandardInlandShipping.init(Js.uncheckedCast(props));
      break;
    default:
      throw new RuntimeException("Unknown type: " + type);
    }
  }

  public final @JsOverlay InlandShippingType getInlandShippingType() {
    return InlandShippingType.safeValueOf(inlandShippingType);
  }

  public final @JsOverlay void setInlandShippingType(final InlandShippingType type) {
    this.inlandShippingType = type.name();
  }

  public final @JsOverlay TimeUnit getTimeUnitAtoB() {
    return TimeUnit.valueOf(timeUnitAtoB);
  }

  public final @JsOverlay void setTimeUnitAtoB(final TimeUnit timeUnitAtoB) {
    this.timeUnitAtoB = timeUnitAtoB.name();
  }

  public final @JsOverlay TimeUnit getTimeUnitBtoA() {
    return TimeUnit.valueOf(timeUnitBtoA);
  }

  public final @JsOverlay void setTimeUnitBtoA(final TimeUnit timeUnitBtoA) {
    this.timeUnitBtoA = timeUnitBtoA.name();
  }

  public final @JsOverlay int getMovementsAtoBPerTimeUnit() {
    return movementsAtoBPerTimeUnit;
  }

  public final @JsOverlay void setMovementsAtoBPerTimeUnit(final int movementsAtoBPerTimeUnit) {
    this.movementsAtoBPerTimeUnit = movementsAtoBPerTimeUnit;
  }

  public final @JsOverlay int getMovementsBtoAPerTimeUnit() {
    return movementsBtoAPerTimeUnit;
  }

  public final @JsOverlay void setMovementsBtoAPerTimeUnit(final int movementsBtoAPerTimeUnit) {
    this.movementsBtoAPerTimeUnit = movementsBtoAPerTimeUnit;
  }

  public final @JsOverlay int getPercentageLadenAtoB() {
    return percentageLadenAtoB;
  }

  public final @JsOverlay void setPercentageLadenAtoB(final int percentageLadenAtoB) {
    this.percentageLadenAtoB = percentageLadenAtoB;
  }

  public final @JsOverlay int getPercentageLadenBtoA() {
    return percentageLadenBtoA;
  }

  public final @JsOverlay void setPercentageLadenBtoA(final int percentageLadenBtoA) {
    this.percentageLadenBtoA = percentageLadenBtoA;
  }

}
