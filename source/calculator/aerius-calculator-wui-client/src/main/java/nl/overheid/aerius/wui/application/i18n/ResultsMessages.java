/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.i18n;

import java.util.List;

import com.google.gwt.i18n.client.Messages.Select;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings.DepositionValueDisplayType;
import nl.overheid.aerius.shared.domain.result.CalculationMarker;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.MainTab;
import nl.overheid.aerius.shared.domain.result.ResultView;
import nl.overheid.aerius.shared.domain.summary.OverlappingHexagonType;
import nl.overheid.aerius.shared.domain.summary.ProcurementPolicy;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.ui.pages.results.GeneralizedEmissionResultType;
import nl.overheid.aerius.wui.application.ui.pages.results.GeneralizedSurfaceType;

/**
 * Text specific for results pages.
 */
public interface ResultsMessages {
  String resultsTitle();

  String resultsScenarioLabel();
  String resultsResultLabel();
  String resultsSubstanceLabel();
  String resultsHexagonTypeLabel();
  String resultsTotalName(@Select Theme theme);

  String calculateFromResultsRepeat();

  String resultsJobDuplicateWarningText(List<String> others);
  String resultsDiscrepancyWarningText();
  String resultsDropdown(@Select ScenarioResultType type);
  String resultsHexagonType(@Select Theme theme, @Select SummaryHexagonType hexagonType);
  String resultsOverlappingHexagonType(@Select OverlappingHexagonType overlappingHexagonType);
  String resultsProcurementPolicy(@Select ProcurementPolicy procurementPolicy);
  String resultsBeingLoaded();
  String resultsErrorOccurred(String errorMsg);
  String resultsNoCalculationSelected();
  String resultsNoCalculationJobSelected();
  String resultsNoResultsCalculated();
  /**
   * When no relevant results are found in the archive for a pollutant(s).
   */
  String resultsNoResultsArchiveContribution();
  /**
   * The user's inputs scenarios have no emissions for a particular pollutant.
   * For the ‘Archive contribution’ and ‘In combination’ result types
   */
  String resultsNoResultsCalculatedNoEmissionsIC();


  String resultsViewMainTabLabel(@Select MainTab tab);
  String resultsViewTabLabel(@Select ResultView tab);
  String resultsPercentageCriticalLoadTitle();
  String resultsPercentageCriticalLevelTitle();
  String resultsChartTitle();
  String resultsNatureAreaTitle();
  String resultsHabitatsTitle();
  String resultsHabitatsGroupLabel();
  String resultsEdgeEffectsTitle();
  String resultsHabitatTable();
  String resultsHabitatTypeCode();
  String resultsHabitatTypeName();
  String resultsStatistic(@Select GeneralizedEmissionResultType resultType, @Select ResultStatisticType type,
      @Select DepositionValueDisplayType displayType, String totalText);
  String resultsStatisticWithoutUnit(@Select ResultStatisticType type, String totalText);
  String resultsScenarioResultType(@Select GeneralizedEmissionResultType generalizedType, @Select ScenarioResultType type,
      @Select DepositionValueDisplayType displayType);

  String resultsHabitatMinimumCriticalLevel(@Select Theme theme, @Select GeneralizedEmissionResultType resultType,
      @Select DepositionValueDisplayType displayType);

  String resultsGraphXAxisLabel(@Select DepositionValueDisplayType displayType, @Select GeneralizedEmissionResultType resultType);
  String resultsGraphYAxisLabel(@Select GeneralizedSurfaceType resultType);
  String resultsCriticalLoadXAxisLabel();
  String resultsCriticalLoadYAxisLabel(@Select GeneralizedSurfaceType surfaceType);
  String resultsZoomToAreaTitle();
  String resultsTableColumnSelection();

  String resultsEdgeEffectReceptorId();

  String resultsEdgeEffectReceptorOffSiteReduction(@Select DepositionValueDisplayType displayType);
  String resultsEdgeEffectReceptorReference(@Select DepositionValueDisplayType displayType);
  String resultsEdgeEffectReceptorProposed(@Select ScenarioResultType resultType, @Select DepositionValueDisplayType displayType);

  String resultsCalculationMarker(@Select ScenarioResultType type, @Select CalculationMarker calculationMarker, String totalText);

  String resultsPercentageCriticalLoadNoLoadSpecified();
  String resultsPercentageCriticalLoadNotSensitive();
  String resultsPercentageCriticalLoadNotSupported();

  String resultsCalculatedSituations();

  String resultsArchiveProjects();
  String resultsArchiveRetrieval(String retrievalDateTime);
  String resultsArchiveResultsLinkText();
  String resultsArchiveResultsLinkDescription();
  String resultsArchiveId();
  String resultsArchiveName();
  String resultsArchiveType();
  String resultsArchiveProjectsNonFound();
  String resultsArchivePermitReference();
  String resultsArchivePlanningReference();
  String resultsArchiveNetEmissions(String substance);

  String resultsProcurementPolicyDescription();
  String resultsProcurementPolicyArea();
  String resultsProcurementPolicyPercentage();

  String resultsSummaryTableOmittedAreaTitle(@Select Theme theme);

  String resultsSummaryMaxContribution(@Select EmissionResultKey substance, @Select DepositionValueDisplayType displayType);
  String resultsSummaryPollutant();
  String resultsSummaryProcessContributions();
  String resultsSummaryScenario();
  String resultsSummaryScenarioType();
  String resultsSummaryScenarioContributions();
  String resultsSummarySite();

  String emissionResultUnit(@Select GeneralizedEmissionResultType resultType, @Select DepositionValueDisplayType displayType);
}
