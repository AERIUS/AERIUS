/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.calculate;

import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.wui.application.components.notification.FloatingUtilityComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationPreparationContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.ui.pages.SituationSummaryView;
import nl.overheid.aerius.wui.application.ui.pages.calculate.map.CalculationJobMapView;

@Component(components = {
    SituationSummaryView.class,
    CalculationJobMapView.class,
    FloatingUtilityComponent.class,
})
public class CalculationSituationSummary implements IsVueComponent {
  @Inject @Data ApplicationContext appContext;
  @Inject @Data CalculationPreparationContext prepContext;

  @Prop EventBus eventBus;

  @Computed("isShowMap")
  public boolean isShowMap() {
    return appContext.isAppFlagEnabled(AppFlag.SHOW_MAP_ON_JOB_PAGE);
  }

  @Computed
  public SituationComposition getSituationComposition() {
    return Optional.ofNullable(prepContext.getActiveJob())
        .map(v -> v.getSituationComposition())
        .orElse(Optional.ofNullable(prepContext.getTemporaryJob())
            .map(v -> v.getSituationComposition())
            .orElse(null));
  }
}
