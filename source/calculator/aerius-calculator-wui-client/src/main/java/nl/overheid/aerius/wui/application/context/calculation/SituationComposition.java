/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context.calculation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.context.SituationContext;

@JsType
public class SituationComposition {
  private final @JsProperty Set<SituationContext> situations = new HashSet<>();

  public Set<SituationContext> getSituations() {
    return new HashSet<>(situations);
  }

  public boolean hasSituation(final SituationType type) {
    return situations.stream()
        .filter(Objects::nonNull)
        .anyMatch(v -> v.getType() == type);
  }

  public SituationContext getSituation(final SituationType type) {
    return situations.stream()
        .filter(Objects::nonNull)
        .filter(v -> v.getType() == type)
        .findFirst()
        .orElse(null);
  }

  public Set<SituationContext> getSituationsByType(final SituationType type) {
    return situations.stream()
        .filter(Objects::nonNull)
        .filter(v -> v.getType() == type)
        .collect(Collectors.toSet());
  }

  public void setSituations(final Collection<SituationContext> situations) {
    this.situations.clear();
    if (situations != null) {
      this.situations.addAll(situations);
    }
  }

  public void setSituation(final SituationType type, final SituationContext situationContext) {
    this.situations.stream()
        .filter(Objects::nonNull)
        .filter(sc -> sc.getType() == type)
        .findFirst()
        .ifPresent(this.situations::remove);

    if (situationContext != null) {
      this.situations.add(situationContext);
    }
  }

  public void addSituation(final SituationContext situation) {
    situations.add(situation);
  }

  public void removeSituation(final SituationContext situation) {
    situations.remove(situation);
  }

  public void removeSituations(final int amount) {
    situations.removeAll(
        situations.stream()
            .limit(Math.max(0, amount))
            .collect(Collectors.toList()));
  }

  public void clear() {
    situations.clear();
  }

  public boolean isActiveSituation(final SituationContext situation) {
    return situations.contains(situation);
  }

  @Override
  public int hashCode() {
    return Objects.hash(getSituations());
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    final SituationComposition other = (SituationComposition) obj;
    return Objects.equals(getSituations(), other.getSituations());
  }

  public SituationComposition copy() {
    final SituationComposition composition = new SituationComposition();
    final List<SituationContext> originalSituations = new ArrayList<>(this.situations);
    composition.setSituations(originalSituations);
    return composition;
  }
}
