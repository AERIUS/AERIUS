/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.activity;

import nl.overheid.aerius.wui.application.place.MainThemePlace;
import nl.overheid.aerius.wui.application.place.PrintPlace;
import nl.overheid.aerius.wui.application.print.nca.PrintNcaActivity;
import nl.overheid.aerius.wui.application.print.own2000.PrintOwN2000Activity;
import nl.overheid.aerius.wui.application.ui.nca.NcaThemeActivity;
import nl.overheid.aerius.wui.application.ui.own2000.OwN2000ThemeActivity;
import nl.overheid.aerius.wui.application.ui.unsupported.NotSupportedActivity;

public interface ApplicationActivityFactory {
  NotSupportedActivity createNotSupportedActivity();

  NcaThemeActivity createNcaActivity(MainThemePlace place);

  OwN2000ThemeActivity createOwN2000Activity(MainThemePlace place);

  PrintOwN2000Activity createPrintOwN2000Activity(PrintPlace place);

  PrintNcaActivity createPrintNcaActivity(PrintPlace place);
}
