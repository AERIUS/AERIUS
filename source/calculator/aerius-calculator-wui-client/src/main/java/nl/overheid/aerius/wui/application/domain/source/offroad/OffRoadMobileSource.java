/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.offroad;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.domain.source.AbstractSubSource;
import nl.overheid.aerius.wui.vue.ReactivityUtil;
import nl.overheid.aerius.wui.vue.SkipReactivityValidations;

/**
 * Client side implementation of OffRoadMobileSource.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public abstract class OffRoadMobileSource extends AbstractSubSource {

  private String offRoadMobileSourceType;
  private String emissionSourceType;
  private String description;
  private String offRoadMobileSourceCode;
  private double literFuelPerYear;
  private double operatingHoursPerYear;
  private double literAdBluePerYear;

  public static final @JsOverlay void init(final OffRoadMobileSource props, final OffRoadType offRoadType) {
    AbstractSubSource.initAbstractSubSource(props);
    ReactivityUtil.ensureJsProperty(props, "offRoadMobileSourceType", props::setOffroadType, offRoadType);
    ReactivityUtil.ensureJsProperty(props, "emissionSourceType", props::setEmissionSourceType, EmissionSourceType.OFFROAD_MOBILE);
    ReactivityUtil.ensureJsProperty(props, "description", props::setDescription, "");
    ReactivityUtil.ensureJsProperty(props, "offRoadMobileSourceCode", props::setCategory, "");
    ReactivityUtil.ensureJsProperty(props, "literFuelPerYear", props::setFuelUsage, 0D);
    ReactivityUtil.ensureJsProperty(props, "operatingHoursPerYear", props::setOperatingHours, 0D);
    ReactivityUtil.ensureJsProperty(props, "literAdBluePerYear", props::setAdBlueUsage, 0D);
  }

  @SkipReactivityValidations
  public static final @JsOverlay void initTypeDependent(final OffRoadMobileSource props, final OffRoadType type) {
    switch (type) {
    case CUSTOM:
      CustomOffRoadMobileSource.init(Js.uncheckedCast(props));
      break;
    case STANDARD:
      StandardOffRoadMobileSource.init(Js.uncheckedCast(props));
      break;
    default:
      throw new RuntimeException("Unknown type: " + type);
    }
  }

  public final @JsOverlay OffRoadType getOffroadType() {
    return OffRoadType.safeValueOf(offRoadMobileSourceType);
  }

  public final @JsOverlay void setOffroadType(final OffRoadType offroadType) {
    this.offRoadMobileSourceType = offroadType.name();
  }

  public final @JsOverlay EmissionSourceType getEmissionSourceType() {
    return EmissionSourceType.safeValueOf(emissionSourceType);
  }

  public final @JsOverlay void setEmissionSourceType(final EmissionSourceType emissionSourceType) {
    this.emissionSourceType = emissionSourceType.name();
  }

  public final @JsOverlay String getDescription() {
    return description;
  }

  public final @JsOverlay void setDescription(final String description) {
    this.description = description;
  }

  public final @JsOverlay String getCategory() {
    return offRoadMobileSourceCode;
  }

  public final @JsOverlay void setCategory(final String category) {
    this.offRoadMobileSourceCode = category;
  }

  public final @JsOverlay double getFuelUsage() {
    return literFuelPerYear;
  }

  public final @JsOverlay void setFuelUsage(final double fuelUsage) {
    this.literFuelPerYear = fuelUsage;
  }

  public final @JsOverlay double getOperatingHours() {
    return operatingHoursPerYear;
  }

  public final @JsOverlay void setOperatingHours(final double operatingHours) {
    this.operatingHoursPerYear = operatingHours;
  }

  public final @JsOverlay double getAdBlueUsage() {
    return literAdBluePerYear;
  }

  public final @JsOverlay void setAdBlueUsage(final double adBlueUsage) {
    this.literAdBluePerYear = adBlueUsage;
  }

}
