/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import jsinterop.annotations.JsProperty;

import nl.aerius.wui.util.Notification;

@Singleton
public class NotificationContext {
  private boolean showing;

  private final @JsProperty List<Notification> notifications = new ArrayList<>();

  private @JsProperty Notification persistentNotification = null;

  public void addNotification(final Notification notification) {
    if (notification.isPersistentMessage()) {
      persistentNotification = notification;
    } else {
      notifications.add(0, notification);
    }
  }

  public List<Notification> getNotifications() {
    return new ArrayList<>(notifications);
  }

  public boolean hasPersistentNotification() {
    return persistentNotification != null;
  }

  public Notification getPersistentNotification() {
    return persistentNotification;
  }

  public boolean isShowing() {
    return showing;
  }

  public void setShowing(final boolean showing) {
    this.showing = showing;
  }

  public int getNotificationCount() {
    return notifications.size();
  }

  public void removeNotification(final Notification notification) {
    notifications.remove(notification);
  }

  public void reset() {
    notifications.clear();
  }
}
