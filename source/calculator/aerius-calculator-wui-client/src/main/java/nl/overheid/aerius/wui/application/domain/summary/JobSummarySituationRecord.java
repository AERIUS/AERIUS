/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.summary;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.application.domain.info.AssessmentArea;

@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class JobSummarySituationRecord {

  private AssessmentArea assessmentArea;
  private String calculationReference;
  private @JsProperty JsPropertyMap<Object> maxContributions;

  public final @JsOverlay AssessmentArea getAssessmentArea() {
    return assessmentArea;
  }

  public final @JsOverlay String getCalculationReference() {
    return calculationReference;
  }

  public final @JsOverlay double getMaxContributions(final EmissionResultKey key) {
    double er = 0.0;
    if (key != null) {
      final Object temp = maxContributions.get(key.name());
      if (temp != null) {
        er = Js.asDouble(temp);
      }
    }
    return er;
  }
}
