/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.nca.pages.timevarying;

import com.axellience.vuegwt.core.client.Vue;
import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;
import com.google.web.bindery.event.shared.binder.GenericEvent;

import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.place.PlaceController;
import nl.overheid.aerius.shared.domain.context.InputTypeViewMode;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileEditCancelCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileEditSaveCommand;
import nl.overheid.aerius.wui.application.components.map.MapComponentFactory;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.TimeVaryingProfileEditorContext;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.place.ScenarioInputListPlace;
import nl.overheid.aerius.wui.application.ui.main.MainView.FlexView;
import nl.overheid.aerius.wui.application.ui.nca.pages.emisionsourceList.timevarying.TimeVaryingProfileEditorViewFactory;
import nl.overheid.aerius.wui.application.ui.pages.ThemeDelegatedActivity;
import nl.overheid.aerius.wui.application.ui.pages.ThemeView;

/**
 * Activity for editing a single time-varying profile.
 */
public class TimeVaryingProfileActivity extends BasicEventComponent implements ThemeDelegatedActivity, TimeVaryingProfilePresenter {
  private static final TimeVaryingActivityEventBinder EVENT_BINDER = GWT.create(TimeVaryingActivityEventBinder.class);

  interface TimeVaryingActivityEventBinder extends EventBinder<TimeVaryingProfileActivity> {}

  @Inject ApplicationContext applicationContext;
  @Inject TimeVaryingProfileEditorContext editorContext;
  @Inject PlaceController placeController;

  private boolean userCancelled;
  private HandlerRegistration handlers;

  @Override
  public void onStart(final ThemeView view) {
    if (editorContext.isEditing()) {
      view.setDelegatedPresenter(this);
      view.setLeftView(TimeVaryingProfileViewFactory.get(), FlexView.RIGHT);
      view.setSoftMiddle(false);
      view.setMiddleView(TimeVaryingProfileEditorViewFactory.get());
      view.setRightView(MapComponentFactory.get());
    } else {
      Vue.nextTick(() -> placeController.goTo(getScenarioInputListPlace()));
    }
  }

  @EventHandler(handles = {TimeVaryingProfileEditCancelCommand.class, TimeVaryingProfileEditSaveCommand.class})
  public void onTimeVaryingProfileEditCancelCommand(final GenericEvent c) {
    userCancelled = true;
  }

  @Override
  public String mayStop() {
    final boolean cancelled = userCancelled;
    userCancelled = false;
    return cancelled ? null : M.messages().timeVaryingProfileMayStop();
  }

  @Override
  public void onStop() {
    final TimeVaryingProfileEditCancelCommand cmd = new TimeVaryingProfileEditCancelCommand();
    cmd.silence();
    eventBus.fireEvent(cmd);
    handlers.removeHandler();
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    handlers = super.setEventBus(eventBus, this, EVENT_BINDER);
  }

  private ScenarioInputListPlace getScenarioInputListPlace() {
    return new ScenarioInputListPlace(applicationContext.getTheme(), InputTypeViewMode.TIME_VARYING_PROFILES);
  }
}
