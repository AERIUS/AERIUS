/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.calculation;

import java.util.List;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;

/**
 * Util class for performing calculations on EmissionSource objetcs.
 */
public class EmissionSourceCalculator {

  /**
   * Calculates the total emission of the give list of emission source objects for
   * the give substance.
   *
   * @param features
   * @param substance
   * @return sum of emissions.
   */
  public static double sum(final List<EmissionSourceFeature> features, final Substance substance) {
    double sum = 0;
    for (int i = 0; i < features.size(); i++) {
      sum += features.get(i).getEmission(substance);
    }
    return sum;
  }
}
