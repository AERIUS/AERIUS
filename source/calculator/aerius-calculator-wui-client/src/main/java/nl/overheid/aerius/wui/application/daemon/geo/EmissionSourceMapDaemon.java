/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.geo;

import java.util.Optional;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.geo.command.InteractionAddedCommand;
import nl.aerius.geo.command.InteractionRemoveCommand;
import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.event.PlaceChangeEvent;
import nl.overheid.aerius.wui.application.command.source.EmissionSourcePersistGeometryCommand;
import nl.overheid.aerius.wui.application.context.EmissionSourceEditorContext;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceDrawGeometryCommand;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceModifyGeometryCommand;
import nl.overheid.aerius.wui.application.geo.interactions.FeatureDrawInteraction;
import nl.overheid.aerius.wui.application.geo.interactions.FeatureModifyInteraction;
import nl.overheid.aerius.wui.application.place.EmissionSourcePlace;

/**
 * Manages interactions with emission sources on the map
 */
@Singleton
public class EmissionSourceMapDaemon extends BasicEventComponent implements Daemon {
  private static final EmissionSourceMapDaemonEventBinder EVENT_BINDER = GWT.create(EmissionSourceMapDaemonEventBinder.class);

  interface EmissionSourceMapDaemonEventBinder extends EventBinder<EmissionSourceMapDaemon> {}

  private FeatureDrawInteraction drawInteraction;
  private FeatureModifyInteraction modifyInteraction;
  private HandlerRegistration modifyHandlerRegistration;

  @Inject private EmissionSourceEditorContext context;

  /**
   * Adds or removes interaction to draw the geometry of an emission source
   *
   * @param e
   */
  @EventHandler
  public void onEmissionSourceDrawGeometry(final EmissionSourceDrawGeometryCommand e) {
    if (drawInteraction != null) {
      eventBus.fireEvent(new InteractionRemoveCommand(drawInteraction));
    }

    if (e.getValue() != null) {
      drawInteraction = new FeatureDrawInteraction(e.getValue(), e.getStartCallback(), geom -> {
        eventBus.fireEvent(new EmissionSourcePersistGeometryCommand(geom, context.getSource()));
        Optional.ofNullable(e.getFinishCallback())
            .ifPresent(v -> v.accept(geom));
      });
      eventBus.fireEvent(new InteractionAddedCommand(drawInteraction));
    }
  }

  /**
   * Adds or removes interaction to modify the geometry of an emission source
   *
   * @param e
   */
  @EventHandler
  public void onEmissionSourceModifyGeometry(final EmissionSourceModifyGeometryCommand e) {
    if (modifyInteraction != null) {
      eventBus.fireEvent(new InteractionRemoveCommand(modifyInteraction));
      modifyHandlerRegistration.removeHandler();
      modifyInteraction = null;
    }

    if (e.getValue() != null) {
      modifyInteraction = new FeatureModifyInteraction(e.getValue());
      eventBus.fireEvent(new InteractionAddedCommand(modifyInteraction));
      modifyHandlerRegistration = e.getValue().addChangeListener(f -> {
        e.getGeometryCallback().onSuccess(e.getValue().getGeometry());
        eventBus.fireEvent(new EmissionSourcePersistGeometryCommand(e.getValue().getGeometry(), context.getSource()));
      });
    }
  }

  @EventHandler
  public void onPlaceChange(final PlaceChangeEvent e) {
    if (!(e.getValue() instanceof EmissionSourcePlace)) {
      if (drawInteraction != null) {
        eventBus.fireEvent(new EmissionSourceDrawGeometryCommand());
      }
      if (modifyInteraction != null) {
        eventBus.fireEvent(new EmissionSourceModifyGeometryCommand());
      }
    }
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
