/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.ui.pages.language;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;
import com.google.gwt.http.client.UrlBuilder;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.user.client.Window.Location;
import com.google.web.bindery.event.shared.EventBus;

import elemental2.dom.HTMLElement;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.shared.Constants;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.context.NavigationContext;
import nl.overheid.aerius.wui.application.util.AccessibilityUtil;
import nl.overheid.aerius.wui.vue.BasicVueView;

/**
 * View for changing the locale
 */
@Component(components = {
    ButtonIcon.class,
})
public class LanguageView extends BasicVueView implements HasCreated, HasMounted {
  @Inject @Data NavigationContext navigationContext;

  @Prop EventBus eventBus;

  @Data String locale;

  @Ref HTMLElement main;

  @Override
  public void created() {
    locale = LocaleInfo.getCurrentLocale().getLocaleName();
  }

  @Override
  public void mounted() {
    SchedulerUtil.delay(() -> AccessibilityUtil.findFirstFocusable(main).focus());
  }

  @JsMethod
  public void selectLanguage(final Language language) {
    if (language.getLocale().equals(locale)) {
      closePopout();
    } else {
      final UrlBuilder bldr = Location.createUrlBuilder();
      bldr.setParameter(Constants.LANGUAGE_PARAMETER, language.getLocale());

      Location.replace(bldr.buildString());
    }
  }

  @JsMethod
  public void closePopout() {
    navigationContext.deactivatePopout();
  }
}
