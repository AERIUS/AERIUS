/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.file;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;
import com.google.web.bindery.event.shared.binder.GenericEvent;

import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.future.AppAsyncCallback;
import nl.overheid.aerius.js.exception.AeriusJsExceptionMessage;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.wui.application.command.importer.DownloadValidationReportCommand;
import nl.overheid.aerius.wui.application.command.importer.RefreshFileCommand;
import nl.overheid.aerius.wui.application.command.importer.RegisterFileCommand;
import nl.overheid.aerius.wui.application.command.importer.UnregisterFileCommand;
import nl.overheid.aerius.wui.application.command.importer.UnregisterFilesCommand;
import nl.overheid.aerius.wui.application.components.importer.ImportActionSelectCommand;
import nl.overheid.aerius.wui.application.components.importer.content.ImportFilterGroupSelectionCommand;
import nl.overheid.aerius.wui.application.components.importer.domain.ImportAction;
import nl.overheid.aerius.wui.application.components.importer.domain.ImportAction.ActionType;
import nl.overheid.aerius.wui.application.components.importer.domain.VirtualSituation;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.importer.FileContext;
import nl.overheid.aerius.wui.application.context.importer.ImportContext;
import nl.overheid.aerius.wui.application.domain.importer.ImportFilter;
import nl.overheid.aerius.wui.application.event.ImportCompleteEvent;
import nl.overheid.aerius.wui.application.event.ImportFailureEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.service.FileServiceAsync;
import nl.overheid.aerius.wui.application.util.FileDownloadUtil;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;

/**
 * Daemon process to manage uploading of files.
 */
@Singleton
public class FileContextDaemon extends BasicEventComponent implements Daemon {
  private static final FileContextDaemonEventBinder EVENT_BINDER = GWT.create(FileContextDaemonEventBinder.class);

  interface FileContextDaemonEventBinder extends EventBinder<FileContextDaemon> {}

  @Inject private FileServiceAsync service;
  @Inject private ScenarioContext scenarioContext;
  @Inject private ImportContext importContext;
  @Inject private FileContext fileContext;
  @Inject private FileValidationPoller poller;

  @EventHandler(handles = {ImportFailureEvent.class, ImportCompleteEvent.class})
  public void onImportFinishEvent(final GenericEvent e) {
    fileContext.reset();
    importContext.reset();
  }

  @EventHandler
  public void onRegisterFileCommand(final RegisterFileCommand c) {
    final FileUploadStatus file = c.getValue();
    fileContext.add(file);

    tryUpload(file);
  }

  @EventHandler
  public void onRefreshFileCommand(final RefreshFileCommand c) {
    final FileUploadStatus file = c.getValue();

    tryUpload(file);
  }

  @EventHandler
  public void onDownloadValidationReportCommand(final DownloadValidationReportCommand c) {
    final List<String> fileCodes = fileContext.getFiles().stream()
        .filter(FileUploadStatus::isValidated)
        .map(FileUploadStatus::getFileCode)
        .collect(Collectors.toList());
    service.fetchValidationReport(fileCodes);
  }

  private void tryUpload(final FileUploadStatus fileUploadStatus) {
    if ("brn".equalsIgnoreCase(FileDownloadUtil.getFileExtension(fileUploadStatus.getFile()))) {
      fileUploadStatus.setUserInput(true);

      if (fileUploadStatus.getSubstance() == null) {
        return;
      }
    }

    fileUploadStatus.reset();
    service.uploadFile(fileUploadStatus,
        AppAsyncCallback.create(fileCode -> success(fileUploadStatus, fileCode), error -> failure(fileUploadStatus, error)));
  }

  private void success(final FileUploadStatus file, final String fileCode) {
    file.setFileCode(fileCode);
    registerStatusCheck(file);
  }

  private static void failure(final FileUploadStatus file, final Throwable error) {
    file.setFailed(true);
    final AeriusJsExceptionMessage aeriusJsExceptionMessage = new AeriusJsExceptionMessage();
    aeriusJsExceptionMessage.reason = AeriusExceptionReason.IMPORT_FAILURE.getErrorCode();
    aeriusJsExceptionMessage.message = M.getErrorMessage(error);
    file.getErrors().add(aeriusJsExceptionMessage);
  }

  @EventHandler
  public void onUnregisterFileCommand(final UnregisterFileCommand c) {
    final FileUploadStatus fus = c.getValue();

    deleteFile(fus);
    // If the file deleted was an archive file deselect all results from being imported.
    if (fus.getSituationStats().isArchive()) {
      eventBus.fireEvent(new ImportFilterGroupSelectionCommand(fus, ImportFilter.RESULTS, false));
    }
  }

  @EventHandler
  public void onUnregisterFilesCommand(final UnregisterFilesCommand c) {
    c.getValue().forEach(v -> deleteFile(v));
  }

  private void deleteFile(final FileUploadStatus file) {
    if (file == null) {
      return;
    }

    // If the file upload was not successful, remove locally
    if (!file.isFailed() && !file.isComplete() || file.isFailed()) {
      purge(file);
    } else {
      // Otherwise remove gracefully
      file.setPending(true);
      service.removeFile(file, b -> purge(file));
    }
  }

  private void purge(final FileUploadStatus file) {
    cleanExistingAction(file);
    fileContext.remove(file);
  }

  private void registerStatusCheck(final FileUploadStatus file) {
    file.setPending(true);
    poller.start();
  }

  @EventHandler
  public void onImportActionSelectCommand(final ImportActionSelectCommand c) {
    final ImportAction action = c.getValue();
    final FileUploadStatus file = c.getFile();

    cleanExistingAction(file);

    if (action.getType() == ActionType.ADD_NEW) {
      final VirtualSituation situation = new VirtualSituation();
      initVirtualSituationProperties(situation, file);

      final VirtualSituation internedSituation = importContext.addVirtualSituation(situation);
      final ImportAction newAction = ImportAction.mergeIntoVirtualSituation(internedSituation);
      file.setImportAction(newAction);
    } else {
      if (file.getSituationStats() != null && file.getSituationStats().isArchive()) {
        // If Archive and add archive contribution enable import of all results. Else (which is IGNORE) disable import of all results.
        eventBus.fireEvent(
            new ImportFilterGroupSelectionCommand(c.getFile(), ImportFilter.RESULTS, action.getType() == ActionType.ADD_ARCHIVE_CONTRIBUTION));
      }
      file.setImportAction(action);
    }

    cleanVirtualSituations();
  }

  private void cleanExistingAction(final FileUploadStatus file) {
    final ImportAction action = file.getImportAction();
    if (action == null) {
      return;
    }

    final VirtualSituation virtualSituation = action.getVirtualSituation();
    if (action.getType() == ActionType.ADD_TO_VIRTUAL_SITUATION
        && virtualSituation.getFileHandle().equals(file)) {
      fileContext.getFiles().stream()
          .filter(v -> v.getImportAction() != null
              && v.getImportAction().getType() == ActionType.ADD_TO_VIRTUAL_SITUATION
              && v.getImportAction().getVirtualSituation().equals(virtualSituation)
              && !VirtualSituation.isSameFileHandle(virtualSituation, v))
          .findFirst()
          .ifPresent(f -> initVirtualSituationProperties(virtualSituation, f));
    }
  }

  private void initVirtualSituationProperties(final VirtualSituation virtualSituation, final FileUploadStatus file) {
    final String name = ImportContext.formatNextVirtualSituationName(file, scenarioContext, importContext);
    virtualSituation.setFileHandle(file);
    virtualSituation.setName(name);
    virtualSituation.setSituationType(file.getSituationType());
  }

  /**
   * Prune virtual situations to make sure none exist that aren't selected through an action.
   */
  private void cleanVirtualSituations() {
    final Set<VirtualSituation> usedVirtualSituations = fileContext.getFiles().stream()
        .map(v -> v.getImportAction())
        .filter(v -> v != null
            && v.getType() == ActionType.ADD_TO_VIRTUAL_SITUATION)
        .map(v -> v.getVirtualSituation())
        .distinct()
        .collect(Collectors.toSet());

    importContext.prune(usedVirtualSituations);
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER, poller);
  }
}
