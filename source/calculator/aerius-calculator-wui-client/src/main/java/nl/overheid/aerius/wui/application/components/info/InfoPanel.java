/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.info;

import java.util.Arrays;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;
import nl.overheid.aerius.wui.application.command.geo.ClearAssessmentAreaCommand;
import nl.overheid.aerius.wui.application.command.geo.SelectAssessmentAreaCommand;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.InfoMarkerContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.domain.info.Natura2000Info;
import nl.overheid.aerius.wui.application.domain.info.StaticReceptorInfo;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    VerticalCollapse.class,
    VerticalCollapseGroup.class,
    BasicInfoRowComponent.class,
    CollapsiblePanel.class,
    BackgroundInfoComponent.class,
    EmissionResultComponent.class,
    HabitatTypeComponent.class,
    AreaInfoComponent.class,
})
public class InfoPanel extends BasicVueComponent implements HasCreated {
  @Prop EventBus eventBus;

  @Inject @Data ApplicationContext applicationContext;
  @Inject @Data CalculationContext calculationContext;
  @Inject @Data InfoMarkerContext context;

  @Data ReceptorUtil recUtil;

  @Override
  public void created() {
    recUtil = new ReceptorUtil(applicationContext.getConfiguration().getReceptorGridSettings());
  }

  @JsMethod
  public String getTitle(final Natura2000Info info) {
    return info.getTitle(applicationContext.isAppFlagEnabled(AppFlag.SHOW_ASSESSMENT_AREA_DIRECTIVES));
  }

  @JsMethod
  public void onOpenNatureArea(final Natura2000Info info) {
    eventBus.fireEvent(new SelectAssessmentAreaCommand(info.getNatura2000AreaId()));
  }

  @JsMethod
  public void onCloseNatureArea(final Natura2000Info info) {
    eventBus.fireEvent(new ClearAssessmentAreaCommand());
  }

  @Computed
  public String getCoordinate() {
    final Point point = recUtil.getPointFromReceptorId(context.getReceptor());
    return "x:" + Math.round(point.getX()) + " y:" + Math.round(point.getY());
  }

  @Computed
  public StaticReceptorInfo getStaticInfo() {
    return context.getStaticReceptorInfo();
  }

  @JsMethod
  public String isHexagonType(final SummaryHexagonType type) {
    return Arrays.asList(getStaticInfo().getHexagonTypes()).contains(type) ? M.messages().booleanYes() : M.messages().booleanNo();
  }

  @Computed("hasNaturaInfo")
  public boolean hasNaturaInfo() {
    return context.getStaticReceptorInfo().getNaturaInfo() != null;
  }

  @Computed("hasAreas")
  public boolean hasAreas() {
    return context.getStaticReceptorInfo().getNaturaInfo() != null && context.getStaticReceptorInfo().getNaturaInfo().length > 0;
  }
}
