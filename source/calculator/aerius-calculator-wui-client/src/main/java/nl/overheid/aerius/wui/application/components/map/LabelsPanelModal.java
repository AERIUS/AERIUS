/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.map;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasDestroyed;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import elemental2.dom.HTMLElement;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.wui.application.command.geo.ChangeLabelDisplayModeCommand;
import nl.overheid.aerius.wui.application.command.geo.LabelsPopupHiddenCommand;
import nl.overheid.aerius.wui.application.command.geo.LabelsPopupResetCommand;
import nl.overheid.aerius.wui.application.command.geo.LabelsPopupVisibleCommand;
import nl.overheid.aerius.wui.application.components.modal.PanelizedModalComponent;
import nl.overheid.aerius.wui.application.components.toggle.ToggleButtons;
import nl.overheid.aerius.wui.application.context.LabelsPanelContext;
import nl.overheid.aerius.wui.application.context.MapConfigurationContext;
import nl.overheid.aerius.wui.application.domain.geo.LabelDisplayMode;
import nl.overheid.aerius.wui.application.util.ScreenUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    ToggleButtons.class,
    PanelizedModalComponent.class
})
public class LabelsPanelModal extends BasicVueComponent implements IsVueComponent, HasCreated, HasDestroyed {
  private static final LabelsPanelModalEventBinder EVENT_BINDER = GWT.create(LabelsPanelModalEventBinder.class);

  interface LabelsPanelModalEventBinder extends EventBinder<LabelsPanelModal> {}

  @Prop EventBus eventBus;

  @Data @Inject LabelsPanelContext context;
  @Data @Inject MapConfigurationContext mapConfigContext;

  @Ref PanelizedModalComponent modal;

  private HandlerRegistration handlers;

  @Data HTMLElement dock;

  @EventHandler
  public void onLabelsPanelDockRemoveCommand(final LabelsPanelDockRemoveCommand c) {
    if (dock == c.getValue()) {
      dock = null;
    }
  }

  @EventHandler
  public void onLabelsPanelDockChangeCommand(final LabelsPanelDockChangeCommand c) {
    dock = c.getValue();
    dock.parentNode.insertBefore(modal.vue().$el(), dock.nextSibling);
    SchedulerUtil.delay(() -> {
      ScreenUtil.attachElementLeft(dock, modal.vue().$el(), ScreenUtil.OFFSET_LEFT);
    });
  }

  @JsMethod
  public void close() {
    eventBus.fireEvent(new LabelsPopupHiddenCommand());
  }

  @EventHandler
  public void onLabelsPopupResetCommand(final LabelsPopupResetCommand c) {
    modal.reset();
    eventBus.fireEvent(new LabelsPopupVisibleCommand());
  }

  @Override
  public void created() {
    handlers = EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Override
  public void destroyed() {
    if (handlers != null) {
      handlers.removeHandler();
    }
  }

  @Computed("labelDisplayMode")
  public LabelDisplayMode getLabelDisplayMode() {
    return mapConfigContext.getLabelDisplayMode();
  }

  @Computed("availableLabelDisplayModes")
  public List<LabelDisplayMode> getAvailableLabelDisplayModes() {
    return Arrays.asList(LabelDisplayMode.values());
  }

  @JsMethod
  public void selectLabelDisplayMode(final LabelDisplayMode labelsMode) {
    eventBus.fireEvent(new ChangeLabelDisplayModeCommand(labelsMode));
  }
}
