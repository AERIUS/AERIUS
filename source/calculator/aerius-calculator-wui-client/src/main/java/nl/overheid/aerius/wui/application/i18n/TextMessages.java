/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.i18n;

import com.google.gwt.i18n.client.Messages.PluralCount;
import com.google.gwt.i18n.client.Messages.Select;

import nl.overheid.aerius.shared.domain.ProductProfile;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.scenario.SituationType;

public interface TextMessages {
  String dropdownPlaceholder();

  String importTitle();
  String importGeneralDescription();
  String importGeneralTitle();

  String importCreateNewScenarioTitle();

  String importCreateNewScenarioText();

  String importTextDrag();
  String importDragImpededText();
  String importNoFilesAdded();

  String importTextDragging();
  String importFileTab();
  String importFileInputLabel();
  String importErrorTab(String amount);
  String importWarningTab(String amount);
  String importFileDelete();
  String importMayCloseConfirm();
  String importRemoveAll();
  String importFileImportAction();
  String importFileSituationName();
  String importFileContentArchiveContributionHeader();
  String importFileContentCalculationPointsHeader();
  String importFileContentOtherHeader();

  String importOpenFileDialog();
  String importErrorOverflow();
  String importWarningOverflow();
  String importErrorOpenLabel();
  String importWarningOpenLabel();
  String importDownloadLog();
  String importYearAutoSnapText(int oldYear, int newYear);

  String importFileName();
  String importFileUsage();
  String importFileScenarioType();
  String importActionAddAsNew();
  String importActionGroupAddToExisting();
  String importActionGroupAddToVirtual();
  String importActionGroupMisc();
  String importAddToExisting(String name);
  String importAddToVirtual(String name);
  String importVirtualSituationEmptyName(int num);
  String importAddCalculationPoints();
  String importAddArchive();
  String importIgnore();
  String importNoticeCalculationTaskNotCreated(String name);
  String importNoticeMetaDataNotImported();

  String situationViewCalculationJob(@PluralCount int num);
  String situationViewSources(@PluralCount int num);
  String situationViewCalculationPoints(@PluralCount int num);
  String situationViewBuildings(@PluralCount int num);
  String situationViewTimeVaryingProfiles(@PluralCount int num);
  String situationViewCalculationResults(@PluralCount int num);

  String scenarioPreferenceLabelYear();
  String scenarioPreferenceLabelSRM2();
  String scenarioExportFileDownload(String fileUrl);

  String scenarioExportFileDownloadWithJobKey(String fileUrl, String jobKey);
  String scenarioImportComplete(@PluralCount int files);
  String scenarioExportError(final String errorMessage);

  String buttonCancel();

  String notificationButtonLabel();
  String notificationNoMessages();
  String notificationNumberOfMessages(final int number);
  String notificationRemoveAllMessages();

  String notificationCompactDateTimeFormat();
  String notificationDateFormat();
  String notificationDateTimeFormat();
  String notificationExportStarted();
  String notificationCalculationStarted(String name);

  String notificationCalculationNotStartedNoObjects(final String situationName);
  String notificationCalculationAlreadyExists(String name);
  String notificationCalculationMaximumAllowed(int max);
  String notificationCalculationError(String name, String message);
  String notificationCalculationCompleted(String name);
  String notificationCalculationStartError(String name, String message);
  String notificationCalculationCancelledError(String name, String message);
  String notificationSourcesUpdatedBeforeEmissionsCalculated();
  String notificationSourceUpdatedBeforeEmissionsCalculated(String sourceLabel);
  String notificationExceptionReference(String reference);

  String notificationExportCancelledMessage(String name);

  String sourceShipWaterwayNotDetermined();
  String sourceShipWaterwayInconclusiveRoute(String bestFitWaterway, String otherWaterways);

  String flagActivated(@Select String key);

  String situationNameLabel();
  String situationTypeLabel();
  String situationYearLabel();
  String situationNettingFactorLabel();
  String situationDeleteTitle();
  String situationType(@Select SituationType situation);
  String situationTypeInCombinationBase(@Select SituationType situation);

  String clButtonNewSituation();
  String clButtonNewSource();
  String clButtonNewBuilding();
  String clButtonNewCalculationPoint();
  String clButtonAutomaticallyPlacePoints();
  String clButtonNewDiurnalProfile();
  String clButtonNewMonthlyProfile();
  String clButtonImportFile();
  String clPageTitle();

  String clPageInfoText(@Select Theme theme, @Select ProductProfile productProfile);

  String criticalLevel(@Select EmissionResultKey emissionResultKey);

  String emptySign();
}
