/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.building;

import java.util.function.DoubleSupplier;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.Feature;

import nl.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.wui.application.command.building.TemporaryBuildingAddCommand;
import nl.overheid.aerius.wui.application.command.building.TemporaryBuildingClearCommand;
import nl.overheid.aerius.wui.application.context.MapConfigurationContext;
import nl.overheid.aerius.wui.application.domain.ItemWithStringIdUtil;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.event.building.BuildingAddedEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingDeletedEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingListChangeEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingRegisterModifyGeometryEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingUpdatedEvent;
import nl.overheid.aerius.wui.application.event.situation.SituationSwitchEvent;
import nl.overheid.aerius.wui.application.geo.layers.LabelStyle;
import nl.overheid.aerius.wui.application.geo.layers.base.BaseClusteredMarkerLayer;
import nl.overheid.aerius.wui.application.geo.layers.base.ClusteredLabelStyleCreator;
import nl.overheid.aerius.wui.application.geo.layers.base.VectorFeaturesWrapperMap;
import nl.overheid.aerius.wui.application.i18n.M;

/**
 * Layer to show the markers of {@link BuildingFeature} objects on the map.
 */
public class BuildingMarkerLayer extends BaseClusteredMarkerLayer<BuildingFeature> {
  interface BuildingMarkerLayerEventBinder extends EventBinder<BuildingMarkerLayer> {}

  private static final BuildingMarkerLayerEventBinder EVENT_BINDER = GWT.create(BuildingMarkerLayerEventBinder.class);

  private final VectorFeaturesWrapperMap situationFeatures = new VectorFeaturesWrapperMap();

  @Inject
  public BuildingMarkerLayer(@Assisted final LayerInfo info, final EventBus eventBus, @Assisted final int zIndex,
      final MapConfigurationContext mapConfigurationContext) {
    super(info, zIndex, new BuildingLabelStyleCreator(mapConfigurationContext::getIconScale));
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  public void onTemporaryBuildingAddCommand(final TemporaryBuildingAddCommand c) {
    getLabelStyleProvider().setEditableFeatureId(c.getValue());
    refreshLayer();
  }

  @EventHandler
  public void onTemporaryBuildingClearCommand(final TemporaryBuildingClearCommand c) {
    getLabelStyleProvider().setEditableFeatureId(null);
    refreshLayer();
  }

  @EventHandler
  public void onBuildingAddedEvent(final BuildingAddedEvent e) {
    situationFeatures.addFeature(e.getSituation().getId(), e.getValue());
  }

  @EventHandler
  public void onBuildingDeletedEvent(final BuildingDeletedEvent e) {
    situationFeatures.removeFeature(e.getSituation().getId(), e.getValue());
  }

  @EventHandler
  public void onBuildingUpdatedEvent(final BuildingUpdatedEvent e) {
    situationFeatures.updateFeature(e.getSituation().getId(), e.getValue());
  }

  @EventHandler
  public void onSituationSwitch(final SituationSwitchEvent e) {
    setLayerVector(situationFeatures.getLayerVector(e.getValue()));
  }

  @EventHandler
  public void onBuildingListChangeEvent(final BuildingListChangeEvent e) {
    setFeatures(situationFeatures.getVectorFeaturesWrapper(e.getSituationId()), e.getValue());
  }

  @EventHandler
  public void onBuildingModifyGeometry(final BuildingRegisterModifyGeometryEvent e) {
    refreshLayer();
  }

  /**
   * Creator to create {@link BuildingFeature} clustered markers to show on the map.
   */
  private static class BuildingLabelStyleCreator extends ClusteredLabelStyleCreator<BuildingFeature> {

    public BuildingLabelStyleCreator(final DoubleSupplier iconScale) {
      super(iconScale, false, LabelStyle.BUILDING);
    }

    @Override
    protected String clusterLabel(final int count, final LabelStyle labelStyle) {
      return M.messages().buildingMarkerClusterLabel(count);
    }

    @Override
    public Feature createLabelFeature(final Feature feature) {
      if (feature == null || ItemWithStringIdUtil.isTemporaryFeature(feature)) {
        return null;
      }
      return super.createLabelFeature(feature);
    }
  }
}
