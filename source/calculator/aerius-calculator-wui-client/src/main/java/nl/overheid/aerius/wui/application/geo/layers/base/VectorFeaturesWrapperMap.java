/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.base;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import ol.Feature;
import ol.source.Vector;

import nl.overheid.aerius.wui.application.context.SituationContext;

/**
 * A map for {@link VectorFeaturesWrapper} objects that can be obtained by an id.
 * Usefull for maintaining {@link VectorFeaturesWrapper} objects  per situation.
 */
public class VectorFeaturesWrapperMap {

  private final Map<String, VectorFeaturesWrapper> vectorFeatures = new HashMap<>();

  private Function<Feature, Feature> mapFunction;

  /**
   * Function to map Feature objects.
   *
   * @see VectorFeaturesWrapper#setMapFunction(Function)
   * @param mapFunction the function
   */
  public void setMapFunction(final Function<Feature, Feature> mapFunction) {
    this.mapFunction = mapFunction;
  }

  /**
   * Adds a feature in the map under the given id.
   *
   * @param id id to reference the wrapper object to add the feature
   * @param feature feature to add
   */
  public void addFeature(final String id, final Feature feature) {
    getVectorFeaturesWrapper(id).addFeature(feature);
  }

  /**
   * Returns the Vector that is on the layer using the id of the situation context.
   *
   * @param situationContext situation context to get the id from
   * @return the Vector that is the layer Vector
   */
  public Vector getLayerVector(final SituationContext situationContext) {
    return situationContext == null ? null : getLayerVector(situationContext.getId());
  }

  private Vector getLayerVector(final String id) {
    final VectorFeaturesWrapper object = vectorFeatures.get(id);

    return object == null ? null : object.getLayerVector();
  }

  /**
   * Returns the Vector containing the Feature objects under the given id.
   *
   * @param id id to reference the wrapper object
   * @return Vector containing the features
   */
  public VectorFeaturesWrapper getVectorFeaturesWrapper(final String id) {
    return vectorFeatures.computeIfAbsent(id, this::newVectorObject);
  }

  private VectorFeaturesWrapper newVectorObject(final String id) {
    final VectorFeaturesWrapper vectorObject = new VectorFeaturesWrapper();

    if (mapFunction != null) {
      vectorObject.setMapFunction(mapFunction);
    }
    return vectorObject;
  }

  /**
   * Remove the feature under the given id.
   *
   * @param id id to reference the wrapper object
   * @param feature to removed
   */
  public void removeFeature(final String id, final Feature feature) {
    if (feature != null) {
      removeFeatureById(id, feature.getId());
    }
  }

  public void removeFeatureById(final String id, final String featureId) {
    final VectorFeaturesWrapper wrapper = vectorFeatures.get(id);

    if (wrapper != null) {
      wrapper.removeFeatureById(featureId);
    }
  }

  /**
   * Updates the feature  under the given id.
   *
   * @param id id to reference the wrapper object
   * @param feature to update
   */
  public void updateFeature(final String id, final Feature feature) {
    if (vectorFeatures.get(id) != null) {
      removeFeature(id, feature);
      addFeature(id, feature);
    }
  }
}
