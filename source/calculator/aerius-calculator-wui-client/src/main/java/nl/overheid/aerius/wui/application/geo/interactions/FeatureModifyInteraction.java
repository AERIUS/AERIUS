/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.interactions;

import ol.Collection;
import ol.Feature;
import ol.interaction.Interaction;
import ol.interaction.Modify;
import ol.interaction.ModifyOptions;
import ol.style.Style;

import nl.aerius.geo.domain.IsInteraction;

/**
 * OL3 interaction to modify the geometry of an emission source
 */
public class FeatureModifyInteraction implements IsInteraction<Interaction> {

  final Interaction interaction;

  /**
   * Creates OL3 interaction to modify the geometry of an emission source
   *
   * @param feature feature to modify the geometry of
   */
  public FeatureModifyInteraction(final Feature feature) {
    final Collection<Feature> featureCollection = new Collection<>();
    featureCollection.push(feature);

    final ModifyOptions modifyOptions = new ModifyOptions();
    modifyOptions.setFeatures(featureCollection);
    modifyOptions.setStyle((a, b) -> new Style[] { FeatureDrawStyle.DRAW_STYLE });

    interaction = new Modify(modifyOptions);
  }

  @Override
  public Interaction asInteraction() {
    return interaction;
  }

}
