/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.main;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasDestroyed;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import elemental2.dom.HTMLElement;

import nl.aerius.search.wui.component.MapSearchComponent;
import nl.aerius.wui.util.SchedulerUtil;
import nl.aerius.wui.vue.transition.HorizontalCollapse;
import nl.overheid.aerius.wui.application.components.map.SearchPanelDockChangeCommand;
import nl.overheid.aerius.wui.application.components.map.SearchPanelDockRemoveCommand;
import nl.overheid.aerius.wui.application.context.SearchPanelContext;
import nl.overheid.aerius.wui.application.util.ScreenUtil;

@Component(components = {
    MapSearchComponent.class,
    HorizontalCollapse.class,
})
public class SearchPanelModal implements IsVueComponent, HasCreated, HasDestroyed {
  private static final SearchPanelModalEventBinder EVENT_BINDER = GWT.create(SearchPanelModalEventBinder.class);

  interface SearchPanelModalEventBinder extends EventBinder<SearchPanelModal> {}

  @Prop EventBus eventBus;

  @Data @Inject SearchPanelContext searchContext;

  @Ref MapSearchComponent modal;

  private HandlerRegistration handlers;

  @Data HTMLElement dock;

  @EventHandler
  public void onSearchPanelDockRemoveCommand(final SearchPanelDockRemoveCommand c) {
    if (dock == c.getValue()) {
      dock = null;
    }
  }

  @EventHandler
  public void onSearchPanelDockChangeCommand(final SearchPanelDockChangeCommand c) {
    dock = c.getValue();
    SchedulerUtil.delay(() -> {
      ScreenUtil.attachElementLeft(dock, modal.vue().$el(), ScreenUtil.OFFSET_LEFT);
    });
  }

  @Computed("isShowing")
  public boolean isShowing() {
    return searchContext.isShowing();
  }

  @Watch("isShowing()")
  public void onShowingChange(final boolean neww) {
    if (neww) {
      modal.focus();
    }
  }

  @Override
  public void created() {
    handlers = EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Override
  public void destroyed() {
    handlers.removeHandler();
  }
}
