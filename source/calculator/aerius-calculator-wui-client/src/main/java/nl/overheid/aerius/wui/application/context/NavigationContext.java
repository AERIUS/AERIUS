/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.wui.application.components.nav.NavigationItem;

@Singleton
public class NavigationContext {
  private final @JsProperty List<NavigationItem> topItems = new ArrayList<>();
  private final @JsProperty List<NavigationItem> bottomItems = new ArrayList<>();

  private String activePopout = null;
  private String activePopoutContentRef = null;

  private boolean activeManual = false;

  public void addTopItem(final NavigationItem navigationItem) {
    topItems.add(navigationItem);
  }

  public List<NavigationItem> getTopItems() {
    return topItems;
  }

  public void addBottomItem(final NavigationItem buttonItem) {
    bottomItems.add(buttonItem);
  }

  public List<NavigationItem> getBottomItems() {
    return bottomItems;
  }

  public boolean isPopoutComponentActive() {
    return activePopout != null;
  }

  public boolean isActivePopout(final String popout, final String contentRef) {
    return popout.equals(activePopout)
        && (contentRef == null || contentRef.equals(activePopoutContentRef));
  }

  public String getActivePopout() {
    return activePopout;
  }

  public void togglePopout(final String popout) {
    if (activePopout == null
        || !activePopout.equals(popout)) {
      activatePopout(popout, null);
    } else {
      deactivatePopout();
    }
  }

  public void activatePopout(final String popout, final String contentRef) {
    this.activePopout = popout;
    this.activePopoutContentRef = contentRef;
  }

  public void deactivatePopout() {
    this.activePopout = null;
    this.activePopoutContentRef = null;
  }

  public boolean isActiveManual() {
    return activeManual;
  }

  public void toggleManual() {
    activeManual = !activeManual;
  }

  public void activateManual() {
    this.activeManual = true;
  }

  public void deactivateManual() {
    this.activeManual = false;
  }

  public void reset() {
    activePopout = null;
    activeManual = false;
    topItems.clear();
    bottomItems.clear();
  }
}
