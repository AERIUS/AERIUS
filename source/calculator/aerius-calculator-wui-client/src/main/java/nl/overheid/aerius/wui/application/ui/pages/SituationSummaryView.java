/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.command.situation.SituationsZoomCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.notification.NotificationButtonComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    LabeledInputComponent.class,
    CollapsiblePanel.class,
    NotificationButtonComponent.class,
    ButtonIcon.class
})
public class SituationSummaryView extends BasicVueComponent {
  @Prop EventBus eventBus;

  @Prop SituationComposition situationComposition;

  @Prop boolean showZoomButton = false;

  @Inject @Data ScenarioContext scenarioContext;
  @Inject @Data ApplicationContext applicationContext;

  @Computed
  public List<SituationContext> getSituations() {
    return scenarioContext.getSituations();
  }

  @JsMethod
  public boolean hasSources(final SituationContext situation) {
    return situation.getSourcesSize() > 0;
  }

  @JsMethod
  public boolean isActiveSituation(final SituationContext situation) {
    return Optional.ofNullable(situationComposition)
        .map(v -> v.isActiveSituation(situation))
        .orElse(false);
  }

  @Computed
  protected List<Substance> getSubstances() {
    return applicationContext.getConfiguration().getSubstances();
  }

  @JsMethod
  public boolean isOffSiteReduction(final SituationContext situation) {
    return SituationType.OFF_SITE_REDUCTION.equals(situation.getType());
  }

  @JsMethod
  public void zoomToSituation(final SituationContext situation) {
    final List<SituationContext> situations = new ArrayList<>();
    situations.add(situation);

    eventBus.fireEvent(new SituationsZoomCommand(situations));
  }

}
