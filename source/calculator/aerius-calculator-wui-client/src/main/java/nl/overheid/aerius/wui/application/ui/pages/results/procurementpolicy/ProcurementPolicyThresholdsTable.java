/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.results.procurementpolicy;

import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.gwt.i18n.client.LocaleInfo;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.summary.ProcurementPolicy;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsAreaSummary;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsSummary;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.application.ui.pages.results.GeneralizedEmissionResultType;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component
public class ProcurementPolicyThresholdsTable extends BasicVueComponent {
  private static final double HUNDRED_PERCENT = 100D;

  @Prop ProcurementPolicy procurementPolicy;
  @Prop SituationResultsSummary situationResultsSummary;
  @Prop EmissionResultType emissionResultType;

  @Data @Inject ApplicationContext applicationContext;

  @Computed("showAreas")
  public boolean showAreas() {
    return procurementPolicy == ProcurementPolicy.WNB_LBV;
  }

  @Computed("showOverall")
  public boolean showOverall() {
    return procurementPolicy == ProcurementPolicy.WNB_LBV_PLUS;
  }

  @Computed
  public GeneralizedEmissionResultType getGeneralizedEmissionResultType() {
    return GeneralizedEmissionResultType.fromEmissionResultType(emissionResultType);
  }

  @Computed
  public EmissionResultValueDisplaySettings.DepositionValueDisplayType getDisplayType() {
    return applicationContext.getConfiguration().getEmissionResultValueDisplaySettings().getDisplayType();
  }

  @JsMethod
  public String getAreaTitle(final SituationResultsAreaSummary area) {
    return area.getAssessmentArea()
        .getTitle(applicationContext.isAppFlagEnabled(AppFlag.SHOW_ASSESSMENT_AREA_DIRECTIVES));
  }

  @JsMethod
  public String getAreaThreshold(final SituationResultsAreaSummary area) {
    return formatDepositionSum(area.getStatistics().getValue(ResultStatisticType.PROCUREMENT_POLICY_THRESHOLD_VALUE));
  }

  @JsMethod
  public String getAreaSum(final SituationResultsAreaSummary area) {
    return formatDepositionSum(area.getStatistics().getValue(ResultStatisticType.SUM_CONTRIBUTION));
  }

  @JsMethod
  public Optional<String> getAreaPercentage(final SituationResultsAreaSummary area) {
    return Optional.ofNullable(area.getStatistics().getValue(ResultStatisticType.PROCUREMENT_POLICY_THRESHOLD_PERCENTAGE)).map(Object::toString);
  }

  @JsMethod
  public Optional<Boolean> isAreaOverThreshold(final SituationResultsAreaSummary area) {
    return Optional.ofNullable(area.getStatistics().getValue(ResultStatisticType.PROCUREMENT_POLICY_THRESHOLD_PERCENTAGE))
        .map(v -> v >= HUNDRED_PERCENT);
  }

  @JsMethod
  public String getOverallThreshold() {
    return formatDepositionSum(situationResultsSummary.getStatistics().getValue(ResultStatisticType.PROCUREMENT_POLICY_THRESHOLD_VALUE));
  }

  @JsMethod
  public String getOverallSum() {
    return formatDepositionSum(situationResultsSummary.getStatistics().getValue(ResultStatisticType.SUM_CONTRIBUTION));
  }

  @JsMethod
  public String getOverallPercentage() {
    return situationResultsSummary.getStatistics().getValue(ResultStatisticType.PROCUREMENT_POLICY_THRESHOLD_PERCENTAGE).toString();
  }

  @JsMethod
  public boolean isOverOverallThreshold() {
    return situationResultsSummary.getStatistics().getValue(ResultStatisticType.PROCUREMENT_POLICY_THRESHOLD_PERCENTAGE) >= HUNDRED_PERCENT;
  }

  public String formatDepositionSum(final Double value) {
    if (value == null) {
      return i18n.valueNotApplicable();
    } else {
      return MessageFormatter.formatDepositionSum(value, applicationContext.getConfiguration().getEmissionResultValueDisplaySettings());
    }
  }

  @Computed
  public String getLocale() {
    return LocaleInfo.getCurrentLocale().getLocaleName();
  }
}
