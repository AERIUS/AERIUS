/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.util;

import java.util.List;

import elemental2.core.Global;

import jsinterop.base.Js;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.SectorProperties;
import nl.overheid.aerius.shared.domain.sector.SectorPropertiesSet;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.BuoyancyType;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.EffluxType;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.SourceType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.domain.source.ADMSCharacteristics;
import nl.overheid.aerius.wui.application.domain.source.ADMSRoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.AbstractSubSource;
import nl.overheid.aerius.wui.application.domain.source.ColdStartESFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.FarmAnimalHousingESFeature;
import nl.overheid.aerius.wui.application.domain.source.FarmLodgingESFeature;
import nl.overheid.aerius.wui.application.domain.source.FarmlandESFeature;
import nl.overheid.aerius.wui.application.domain.source.GenericESFeature;
import nl.overheid.aerius.wui.application.domain.source.InlandMaritimeShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.InlandShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.ManureStorageESFeature;
import nl.overheid.aerius.wui.application.domain.source.MaritimeMaritimeShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.MooringInlandShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.MooringMaritimeShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.OPSCharacteristics;
import nl.overheid.aerius.wui.application.domain.source.OffRoadMobileESFeature;
import nl.overheid.aerius.wui.application.domain.source.SRM2RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.SourceCharacteristics;
import nl.overheid.aerius.wui.application.domain.source.combustion.CombustionPlantESFeature;
import nl.overheid.aerius.wui.application.domain.source.farm.CustomFarmAnimalHousing;
import nl.overheid.aerius.wui.application.domain.source.farm.CustomFarmLodging;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmAnimalHousing;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmAnimalHousingType;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmLodging;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmLodgingType;
import nl.overheid.aerius.wui.application.domain.source.farm.StandardFarmAnimalHousing;
import nl.overheid.aerius.wui.application.domain.source.farm.StandardFarmLodging;
import nl.overheid.aerius.wui.application.domain.source.farmland.CustomFarmlandActivity;
import nl.overheid.aerius.wui.application.domain.source.farmland.FarmlandActivity;
import nl.overheid.aerius.wui.application.domain.source.farmland.FarmlandActivityType;
import nl.overheid.aerius.wui.application.domain.source.farmland.StandardFarmlandActivity;
import nl.overheid.aerius.wui.application.domain.source.manure.CustomManureStorage;
import nl.overheid.aerius.wui.application.domain.source.manure.ManureStorage;
import nl.overheid.aerius.wui.application.domain.source.manure.ManureStorageType;
import nl.overheid.aerius.wui.application.domain.source.manure.StandardManureStorage;
import nl.overheid.aerius.wui.application.domain.source.road.ColdStartStandardVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.CustomVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.SpecificVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.StandardVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.ValuesPerColdStartVehicleTypes;
import nl.overheid.aerius.wui.application.domain.source.road.ValuesPerVehicleType;
import nl.overheid.aerius.wui.application.domain.source.road.ValuesPerVehicleTypes;
import nl.overheid.aerius.wui.application.domain.source.road.VehicleType;
import nl.overheid.aerius.wui.application.domain.source.road.Vehicles;
import nl.overheid.aerius.wui.application.domain.source.shipping.inland.CustomInlandShipping;
import nl.overheid.aerius.wui.application.domain.source.shipping.inland.CustomMooringInlandShipping;
import nl.overheid.aerius.wui.application.domain.source.shipping.inland.InlandShipping;
import nl.overheid.aerius.wui.application.domain.source.shipping.inland.InlandShippingType;
import nl.overheid.aerius.wui.application.domain.source.shipping.inland.MooringInlandShipping;
import nl.overheid.aerius.wui.application.domain.source.shipping.inland.MooringInlandShippingType;
import nl.overheid.aerius.wui.application.domain.source.shipping.inland.StandardInlandShipping;
import nl.overheid.aerius.wui.application.domain.source.shipping.inland.StandardMooringInlandShipping;
import nl.overheid.aerius.wui.application.domain.source.shipping.maritime.CustomMaritimeShipping;
import nl.overheid.aerius.wui.application.domain.source.shipping.maritime.CustomMooringMaritimeShipping;
import nl.overheid.aerius.wui.application.domain.source.shipping.maritime.MaritimeShipping;
import nl.overheid.aerius.wui.application.domain.source.shipping.maritime.MaritimeShippingType;
import nl.overheid.aerius.wui.application.domain.source.shipping.maritime.MooringMaritimeShipping;
import nl.overheid.aerius.wui.application.domain.source.shipping.maritime.MooringMaritimeShippingType;
import nl.overheid.aerius.wui.application.domain.source.shipping.maritime.StandardMaritimeShipping;
import nl.overheid.aerius.wui.application.domain.source.shipping.maritime.StandardMooringMaritimeShipping;

/*
 * Util class to convert the feature between the different sourceType
 */
public class EmissionSourceFeatureUtil {
  /**
   * Deep copies a {@link AbstractSubSource} object.
   *
   * @param <T>
   * @param org object to clone
   * @return cloned object.
   */
  public static <T extends AbstractSubSource> T clone(final T org) {
    return Js.uncheckedCast(Global.JSON.parse(Global.JSON.stringify(org)));
  }

  /**
   * Creates a new {@link EmissionSourceFeature} for the given sector and copies generic data from the given source.
   *
   * @param sector sector to create new source for
   * @param source original source to base sources on
   * @param sectorPropertiesSet set with sector meta data
   * @param characteristicsType characteristics type of the data to enter
   * @return new source
   */
  public static EmissionSourceFeature changeEmissionsSourceFeature(final Sector sector, final EmissionSourceFeature source,
      final SectorPropertiesSet sectorPropertiesSet, final CharacteristicsType characteristicsType) {
    final int sectorId = sector.getSectorId();
    final EmissionSourceFeature newSource = createEmissionSourceFeature(sectorPropertiesSet.get(sectorId), characteristicsType);

    newSource.setSectorId(sectorId);
    newSource.setId(source.getId());
    newSource.setLabel(source.getLabel());
    newSource.setGeometry(source.getGeometry());
    updateCharacteristicsFromSector(newSource, source, sector);
    return newSource;
  }

  public static EmissionSourceFeature createEmissionSourceFeature(final SectorProperties sectorProperties, final CharacteristicsType cType) {
    final EmissionSourceFeature newSource;

    switch (sectorProperties.getMethod()) {
    case FARM_ANIMAL_HOUSING:
      newSource = FarmAnimalHousingESFeature.create(cType);
      break;
    case FARM_LODGE:
      newSource = FarmLodgingESFeature.create(cType);
      break;
    case FARMLAND:
      newSource = FarmlandESFeature.create(cType);
      break;
    case MANURE_STORAGE:
      newSource = ManureStorageESFeature.create(cType);
      break;
    case MEDIUM_COMBUSTION_PLANT:
      newSource = CombustionPlantESFeature.create();
      break;
    case SRM2_ROAD:
      newSource = SRM2RoadESFeature.create();
      break;
    case ADMS_ROAD:
      newSource = ADMSRoadESFeature.create();
      break;
    case SHIPPING_MARITIME_DOCKED:
      newSource = MooringMaritimeShippingESFeature.create();
      break;
    case SHIPPING_MARITIME_INLAND:
      newSource = InlandMaritimeShippingESFeature.create();
      break;
    case SHIPPING_MARITIME_MARITIME:
      newSource = MaritimeMaritimeShippingESFeature.create();
      break;
    case SHIPPING_INLAND_DOCKED:
      newSource = MooringInlandShippingESFeature.create();
      break;
    case SHIPPING_INLAND:
      newSource = InlandShippingESFeature.create();
      break;
    case OFFROAD_MOBILE:
      newSource = OffRoadMobileESFeature.create();
      break;
    case COLD_START_PARKING_GARAGE:
      newSource = ColdStartESFeature.create(cType, false);
      break;
    case COLD_START_OTHER:
      newSource = ColdStartESFeature.create(cType, true);
      break;
    default:
      newSource = GenericESFeature.create(cType);
      break;
    }
    return newSource;
  }

  private static void updateCharacteristicsFromSector(final EmissionSourceFeature newSource, final EmissionSourceFeature oldSource,
      final Sector sector) {
    if (newSource.getCharacteristicsType() == CharacteristicsType.OPS) {
      final OPSCharacteristics characteristics = (OPSCharacteristics) newSource.getCharacteristics();
      final OPSSourceCharacteristics sectorCharacteristics = sector.getDefaultCharacteristics();
      // Only set characteristics that are retrieved in backend
      characteristics.setDiurnalVariation(sectorCharacteristics.getDiurnalVariation());
      characteristics.setHeatContent(sectorCharacteristics.getHeatContent());
      characteristics.setEmissionHeight(sectorCharacteristics.getEmissionHeight());
      characteristics.setSpread(sectorCharacteristics.getSpread());
      // PSD only needed for PM10, but better safe than sorry.
      characteristics.setParticleSizeDistribution(sectorCharacteristics.getParticleSizeDistribution());
      if (oldSource.getCharacteristicsType() == CharacteristicsType.OPS && !isVehicleBasedCharacteristics(newSource)) {
        characteristics.setBuildingGmlId(((OPSCharacteristics) oldSource.getCharacteristics()).getBuildingGmlId());
      }
    } else if (newSource.getCharacteristicsType() == CharacteristicsType.ADMS) {
      final ADMSCharacteristics characteristics = (ADMSCharacteristics) newSource.getCharacteristics();

      if (characteristics.getStandardHourlyTimeVaryingProfileCode() == null) {
        characteristics.setStandardHourlyTimeVaryingProfileCode(
            ((ADMSCharacteristics) oldSource.getCharacteristics()).getStandardHourlyTimeVaryingProfileCode());
      }

      if (oldSource.getCharacteristicsType() == CharacteristicsType.ADMS) {
        updateADMSSourceType(characteristics, oldSource.getCharacteristics().asADMS().getSourceType());
        characteristics.setSourceType(oldSource.getCharacteristics().asADMS().getSourceType());
        characteristics.setBuildingGmlId(((ADMSCharacteristics) oldSource.getCharacteristics()).getBuildingGmlId());
      }
    }

    SourceCharacteristics.initTypeDependent(newSource, newSource.getCharacteristicsType());
  }

  private static boolean isVehicleBasedCharacteristics(final EmissionSourceFeature source) {
    return source.getEmissionSourceType() == EmissionSourceType.COLD_START
        && ((ColdStartESFeature) source).isVehicleBasedCharacteristics();
  }

  public static void updateADMSSourceType(final ADMSCharacteristics chars, final SourceType type) {
    chars.setSourceType(type);

    if (type == SourceType.VOLUME) {
      chars.setBuoyancyType(BuoyancyType.AMBIENT);
      chars.setEffluxType(EffluxType.VELOCITY);
    }

    if (type == SourceType.JET) {
      chars.setEffluxType(EffluxType.VELOCITY);
    }
  }

  /**
   * Creates a FarmLodging object given the {@link FarmLodgingType}.
   *
   * @param type type to create
   * @return new FarmLodging object
   * @deprecated {@link FarmLodging} is replaced by {@link FarmAnimalHousing}
   */
  @Deprecated
  public static FarmLodging createFarmLodgingType(final FarmLodgingType type) {
    final FarmLodging farmLodging;

    if (type == FarmLodgingType.STANDARD) {
      farmLodging = StandardFarmLodging.create();
    } else {
      farmLodging = CustomFarmLodging.create();
    }
    return farmLodging;
  }

  /**
   * Creates a FarmAnimalHousing object given the {@link FarmAnimalHousingType}.
   *
   * @param type type to create
   * @return new FarmAnimalHousing object
   */
  public static FarmAnimalHousing createFarmAnimalHousingType(final FarmAnimalHousingType type) {
    final FarmAnimalHousing farmAnimalHousing;

    if (type == FarmAnimalHousingType.STANDARD) {
      farmAnimalHousing = StandardFarmAnimalHousing.create();
    } else {
      farmAnimalHousing = CustomFarmAnimalHousing.create();
    }
    return farmAnimalHousing;
  }

  /**
   * Creates a FarmlandActivity object given the {@link FarmlandActivityType} {@link FramlandCategory}.
   *
   * @param standardActivity boolean create a standard otherwise custom
   * @param farmlandCategory farmlandCategory to create
   * @return new FarmlandActivity object
   */
  public static FarmlandActivity createFarmlandActivityType(final boolean standardActivity, final String activityCode) {
    return standardActivity ? StandardFarmlandActivity.create(activityCode) : CustomFarmlandActivity.create(activityCode);
  }

  /**
   * Creates a ManureStorage object given the {@link ManureStorageType}.
   *
   * @param type type to create
   * @return new ManureStorage object
   */
  public static ManureStorage createManureStorageType(final ManureStorageType type) {
    final ManureStorage farmLodging;

    if (type == ManureStorageType.STANDARD) {
      farmLodging = StandardManureStorage.create();
    } else {
      farmLodging = CustomManureStorage.create();
    }
    return farmLodging;
  }

  /**
   * Creates a {@link Vehicles} object given the {@link VehicleType} .
   *
   * @param type type to create
   * @return new Vehicles object
   */
  public static Vehicles convertVehiclesType(final VehicleType type, final Vehicles oldVehicle, final List<Substance> substances,
      final List<String> standardVehicleCodes) {
    final Vehicles newVehicle;
    if (type == VehicleType.STANDARD) {
      newVehicle = StandardVehicles.create();
      final ValuesPerVehicleTypes standardRoadVehicleType = ValuesPerVehicleTypes.create();
      for (final String vehicleTypeCode : standardVehicleCodes) {
        standardRoadVehicleType.setValuePerVehicleType(vehicleTypeCode, ValuesPerVehicleType.create());
      }
      ((StandardVehicles) newVehicle).setValuesPerVehicleTypes(standardRoadVehicleType);
    } else if (type == VehicleType.STANDARD_CS) {
      newVehicle = ColdStartStandardVehicles.create();
      final ValuesPerColdStartVehicleTypes standardRoadVehicleType = ValuesPerColdStartVehicleTypes.create();
      for (final String vehicleTypeCode : standardVehicleCodes) {
        standardRoadVehicleType.setValuePerVehicleType(vehicleTypeCode, 0D);
      }
      ((ColdStartStandardVehicles) newVehicle).setValuesPerVehicleTypes(standardRoadVehicleType);
    } else if (type == VehicleType.SPECIFIC) {
      newVehicle = SpecificVehicles.create();
    } else {
      newVehicle = CustomVehicles.create();
      for (final Substance substance : substances) {
        ((CustomVehicles) newVehicle).setEmissionFactor(substance, 0D);
      }
      ((CustomVehicles) newVehicle).setVehicleCode("");
    }
    if (oldVehicle != null) {
      newVehicle.setTimeUnit(oldVehicle.getTimeUnit());
    }
    return newVehicle;
  }

  /**
   * Creates a MooringMaritimeShipping object given the {@link MooringMaritimeShipping}.
   *
   * @param type type to create
   * @return new MooringMaritimeShipping object
   */
  public static MooringMaritimeShipping createMooringMaritimeShippingType(final MooringMaritimeShippingType type) {
    final MooringMaritimeShipping mooringMaritimeShipping;

    if (type == MooringMaritimeShippingType.STANDARD) {
      mooringMaritimeShipping = StandardMooringMaritimeShipping.create();
    } else {
      mooringMaritimeShipping = CustomMooringMaritimeShipping.create();
    }
    return mooringMaritimeShipping;
  }

  /**
   * Creates a {@link MaritimeShipping} object given the {@link MaritimeShippingType} .
   *
   * @param type type to create
   * @return new MaritimeShipping object
   */
  public static MaritimeShipping convertMaritimeShippingType(final MaritimeShippingType type, final MaritimeShipping oldMaritimeShipping) {
    final MaritimeShipping newMaritimeShipping;
    if (type == MaritimeShippingType.STANDARD) {
      newMaritimeShipping = StandardMaritimeShipping.create();
    } else {
      newMaritimeShipping = CustomMaritimeShipping.create();
    }
    if (oldMaritimeShipping != null) {
      newMaritimeShipping.setTimeUnit(oldMaritimeShipping.getTimeUnit());
      newMaritimeShipping.setMovementsPerTimeUnit(oldMaritimeShipping.getMovementsPerTimeUnit());
    }
    return newMaritimeShipping;
  }

  /**
   * Creates a {@link InlandShipping} object given the {@link InlandShippingType} .
   *
   * @param type type to create
   * @param oldInlandShipping object to copy values from
   * @return new InlandShipping object
   */
  public static InlandShipping convertInlandShippingType(final InlandShippingType type, final InlandShipping oldInlandShipping) {
    final InlandShipping newInlandShipping;
    if (type == InlandShippingType.STANDARD) {
      newInlandShipping = StandardInlandShipping.create();
    } else {
      newInlandShipping = CustomInlandShipping.create();
    }
    return newInlandShipping;
  }

  /**
   * Creates a {@link MooringInlandShipping} object given the {@link MooringInlandShippingType} .
   *
   * @param type type to create
   * @param oldInlandShipping object to copy values from
   * @return new MooringInlandShipping object
   */
  public static MooringInlandShipping convertMooringInlandShippingType(final MooringInlandShippingType type,
      final MooringInlandShipping oldInlandShipping) {
    final MooringInlandShipping newMooringInlandShipping;
    if (type == MooringInlandShippingType.STANDARD) {
      newMooringInlandShipping = StandardMooringInlandShipping.create();
    } else {
      newMooringInlandShipping = CustomMooringInlandShipping.create();
    }
    return newMooringInlandShipping;
  }

  /**
   * Creates a {@link MooringMaritimeShipping} object given the {@link MooringMaritimeShippingType} .
   *
   * @param type type to create
   * @return new MooringMaritimeShipping object
   */
  public static MooringMaritimeShipping convertMooringMaritimeShippingType(final MooringMaritimeShippingType type,
      final MooringMaritimeShipping oldMooringMaritimeShipping) {
    final MooringMaritimeShipping newMooringMaritimeShipping;
    if (type == MooringMaritimeShippingType.STANDARD) {
      newMooringMaritimeShipping = StandardMooringMaritimeShipping.create();
    } else {
      newMooringMaritimeShipping = CustomMooringMaritimeShipping.create();
    }
    if (oldMooringMaritimeShipping != null) {
      newMooringMaritimeShipping.setDescription(oldMooringMaritimeShipping.getDescription());
      newMooringMaritimeShipping.setShipsPerTimeUnit(oldMooringMaritimeShipping.getShipsPerTimeUnit());
      newMooringMaritimeShipping.setTimeUnit(oldMooringMaritimeShipping.getTimeUnit());
      newMooringMaritimeShipping.setAverageResidenceTime(oldMooringMaritimeShipping.getAverageResidenceTime());
      newMooringMaritimeShipping.setShorePowerFactor(oldMooringMaritimeShipping.getShorePowerFactor());
    }
    return newMooringMaritimeShipping;
  }

}
