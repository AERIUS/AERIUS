/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.road.srm2;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.v2.source.road.SRM2RoadSideBarrierType;
import nl.overheid.aerius.srm2.conversion.Sigma0Calculator;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.InputWarningComponent;
import nl.overheid.aerius.wui.application.components.input.InputWithUnitComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.road.srm2.SRM2BarrierEditorValidators.SRM2BarrierEditorValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.domain.source.SRM2RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.road.SRM2RoadSideBarrier;

@Component(name = "aer-srm2-barrier-editor", customizeOptions = {
    SRM2BarrierEditorValidators.class,
}, directives = {
    ValidateDirective.class
}, components = {
    LabeledInputComponent.class,
    InputWithUnitComponent.class,
    InputErrorComponent.class,
    InputWarningComponent.class,
    ValidationBehaviour.class,
    VerticalCollapse.class,
})
public class SRM2BarrierEditorComponent extends ErrorWarningValidator implements HasCreated, HasValidators<SRM2BarrierEditorValidations> {
  private static final String EMPTY_BARRIER_VALUE = "none";

  @Prop SRM2RoadESFeature source;

  @Data String barrierLeftHeightV;
  @Data String barrierRightHeightV;
  @Data String barrierLeftDistanceV;
  @Data String barrierRightDistanceV;

  @Data String barrierLeftHeightWarningV;
  @Data String barrierRightHeightWarningV;
  @Data String barrierLeftDistanceWarningV;
  @Data String barrierRightDistanceWarningV;

  @JsProperty(name = "$v") SRM2BarrierEditorValidations validation;

  @Override
  @Computed
  public SRM2BarrierEditorValidations getV() {
    return validation;
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Computed("emptyBarrierValue")
  public String getEmptyBarrierValue() {
    return EMPTY_BARRIER_VALUE;
  }

  @Watch(value = "source", isImmediate = true)
  public void onSourceChange(final SRM2RoadESFeature neww) {
    if (neww == null) {
      return;
    }

    if (hasBarrierLeft()) {
      barrierLeftHeightV = String.valueOf(neww.getBarrierLeft().getHeight());
      barrierLeftDistanceV = String.valueOf(neww.getBarrierLeft().getDistance());
      barrierLeftHeightWarningV = String.valueOf(neww.getBarrierLeft().getHeight());
      barrierLeftDistanceWarningV = String.valueOf(neww.getBarrierLeft().getDistance());
    }

    if (hasBarrierRight()) {
      barrierRightHeightV = String.valueOf(neww.getBarrierRight().getHeight());
      barrierRightDistanceV = String.valueOf(neww.getBarrierRight().getDistance());
      barrierRightHeightWarningV = String.valueOf(neww.getBarrierRight().getHeight());
      barrierRightDistanceWarningV = String.valueOf(neww.getBarrierRight().getDistance());
    }
  }

  @Computed
  protected String getBarrierLeftType() {
    return source.getBarrierLeft() == null ? EMPTY_BARRIER_VALUE : source.getBarrierLeft().getBarrierType().name();
  }

  @Computed
  protected void setBarrierLeftType(final String barrierType) {
    if (EMPTY_BARRIER_VALUE.equals(barrierType)) {
      source.setBarrierLeft(null);
      barrierLeftHeightV = "";
      barrierLeftHeightWarningV = "";
      barrierLeftDistanceV = "";
      barrierLeftDistanceWarningV = "";
    } else {
      ensureBarrierLeftCreated();
      source.getBarrierLeft().setBarrierType(barrierType.isEmpty() ? null : SRM2RoadSideBarrierType.valueOf(barrierType));
    }
  }

  @Computed
  protected String getBarrierRightType() {
    return source.getBarrierRight() == null ? EMPTY_BARRIER_VALUE : source.getBarrierRight().getBarrierType().name();
  }

  @Computed
  protected void setBarrierRightType(final String barrierType) {
    if (EMPTY_BARRIER_VALUE.equals(barrierType)) {
      source.setBarrierRight(null);
      barrierRightHeightV = "";
      barrierRightHeightWarningV = "";
      barrierRightDistanceV = "";
      barrierRightDistanceWarningV = "";
    } else {
      ensureBarrierRightCreated();
      source.getBarrierRight().setBarrierType(barrierType.isEmpty() ? null : SRM2RoadSideBarrierType.valueOf(barrierType));
    }
  }

  @Computed
  protected String getBarrierLeftHeight() {
    return barrierLeftHeightV;
  }

  @Computed
  protected void setBarrierLeftHeight(final String height) {
    ensureBarrierLeftCreated();
    ValidationUtil.setSafeDoubleValue(v -> source.getBarrierLeft().setHeight(v), height, 0D);
    barrierLeftHeightV = height;
    barrierLeftHeightWarningV = height;
  }

  protected String barrierLeftHeightError() {
    return i18n.ivDoubleLowerlimit(i18n.sourceRoadBarrierLeftHeight(), 0D);
  }

  protected String barrierLeftHeightWarning() {
    return source.getBarrierLeft().getHeight() > Sigma0Calculator.MAX_BARRIER_HEIGHT
        ? i18n.ivRoadBarrierHeightUpperLimit(i18n.sourceRoadBarrierLeftHeight(), Sigma0Calculator.MAX_BARRIER_HEIGHT)
        : i18n.ivRoadBarrierHeightUndefined(i18n.sourceRoadBarrierLeftHeight(), Sigma0Calculator.MIN_BARRIER_HEIGHT);
  }

  @Computed
  protected String getBarrierRightHeight() {
    return barrierRightHeightV;
  }

  @Computed
  protected void setBarrierRightHeight(final String height) {
    ensureBarrierRightCreated();
    ValidationUtil.setSafeDoubleValue(v -> source.getBarrierRight().setHeight(v), height, 0D);
    barrierRightHeightV = height;
    barrierRightHeightWarningV = height;
  }

  protected String barrierRightHeightError() {
    return i18n.ivDoubleLowerlimit(i18n.sourceRoadBarrierRightHeight(), 0D);
  }

  protected String barrierRightHeightWarning() {
    return source.getBarrierRight().getHeight() > Sigma0Calculator.MAX_BARRIER_HEIGHT
        ? i18n.ivRoadBarrierHeightUpperLimit(i18n.sourceRoadBarrierRightHeight(), Sigma0Calculator.MAX_BARRIER_HEIGHT)
        : i18n.ivRoadBarrierHeightUndefined(i18n.sourceRoadBarrierRightHeight(), Sigma0Calculator.MIN_BARRIER_HEIGHT);
  }

  @Computed
  protected String getBarrierLeftDistance() {
    return barrierLeftDistanceV;
  }

  @Computed
  protected void setBarrierLeftDistance(final String distance) {
    ensureBarrierLeftCreated();
    ValidationUtil.setSafeDoubleValue(v -> source.getBarrierLeft().setDistance(v), distance, 0D);
    barrierLeftDistanceV = distance;
    barrierLeftDistanceWarningV = distance;
  }

  protected String barrierLeftDistanceError() {
    return i18n.ivDoubleLowerlimit(i18n.sourceRoadBarrierLeftDistance(), 0D);
  }

  protected String barrierLeftDistanceWarning() {
    return i18n.ivRoadBarrierDistanceUpperLimit(i18n.sourceRoadBarrierLeftDistance(), Sigma0Calculator.MAX_BARRIER_DISTANCE);
  }

  @Computed
  protected String getBarrierRightDistance() {
    return barrierRightDistanceV;
  }

  @Computed
  protected void setBarrierRightDistance(final String distance) {
    ensureBarrierRightCreated();
    ValidationUtil.setSafeDoubleValue(v -> source.getBarrierRight().setDistance(v), distance, 0D);
    barrierRightDistanceV = distance;
    barrierRightDistanceWarningV = distance;
  }

  protected String barrierRightDistanceError() {
    return i18n.ivDoubleLowerlimit(i18n.sourceRoadBarrierRightDistance(), 0D);
  }

  protected String barrierRightDistanceWarning() {
    return i18n.ivRoadBarrierDistanceUpperLimit(i18n.sourceRoadBarrierRightDistance(), Sigma0Calculator.MAX_BARRIER_DISTANCE);
  }

  private void ensureBarrierLeftCreated() {
    if (!hasBarrierLeft()) {
      final SRM2RoadSideBarrier barrier = SRM2RoadSideBarrier.create();
      source.setBarrierLeft(barrier);
      barrierLeftHeightV = String.valueOf(barrier.getHeight());
      barrierLeftHeightWarningV = String.valueOf(barrier.getHeight());
      barrierLeftDistanceV = String.valueOf(barrier.getDistance());
      barrierLeftDistanceWarningV = String.valueOf(barrier.getDistance());
    }
  }

  @Computed("hasBarrierLeft")
  public boolean hasBarrierLeft() {
    return source.getBarrierLeft() != null;
  }

  private void ensureBarrierRightCreated() {
    if (!hasBarrierRight()) {
      final SRM2RoadSideBarrier barrier = SRM2RoadSideBarrier.create();
      source.setBarrierRight(barrier);
      barrierRightHeightV = String.valueOf(barrier.getHeight());
      barrierRightHeightWarningV = String.valueOf(barrier.getHeight());
      barrierRightDistanceV = String.valueOf(barrier.getDistance());
      barrierRightDistanceWarningV = String.valueOf(barrier.getDistance());
    }
  }

  @Computed("hasBarrierRight")
  public boolean hasBarrierRight() {
    return source.getBarrierRight() != null;
  }

  @Computed("hasWarning")
  public boolean hasWarning() {
    return hasBarrierLeft()
        && (!validation.barrierLeftHeightV.invalid && validation.barrierLeftHeightWarningV.invalid || validation.barrierLeftDistanceWarningV.invalid)
        || hasBarrierRight() && (!validation.barrierRightHeightV.invalid && validation.barrierRightHeightWarningV.invalid
            || validation.barrierRightDistanceWarningV.invalid);
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return validation.barrierLeftHeightV.invalid || validation.barrierRightHeightV.invalid
        || validation.barrierLeftDistanceV.invalid || validation.barrierRightDistanceV.invalid;
  }
}
