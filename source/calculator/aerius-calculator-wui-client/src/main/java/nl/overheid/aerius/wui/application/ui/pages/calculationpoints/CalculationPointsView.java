/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.ui.pages.calculationpoints;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.wui.application.components.calculationpoint.CalculationPointEditorComponent;
import nl.overheid.aerius.wui.application.components.calculationpoint.CalculationPointPlacementComponent;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.context.CalculationPointAutomaticPlacementContext;
import nl.overheid.aerius.wui.application.context.CalculationPointEditorContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component(components = {
    CalculationPointsListComponent.class,
    VerticalCollapse.class,
    CollapsiblePanel.class,
    CalculationPointEditorComponent.class,
    CalculationPointPlacementComponent.class,
})
public class CalculationPointsView extends BasicVueView {

  @Prop EventBus eventBus;
  @Inject @Data ScenarioContext scenarioContext;

  @Inject @Data CalculationPointEditorContext editorContext;
  @Inject @Data CalculationPointAutomaticPlacementContext automaticPlacementContext;

}
