/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.offroad.detail;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.wui.application.components.divider.DividerComponent;
import nl.overheid.aerius.wui.application.components.source.emission.EmissionValueComponent;
import nl.overheid.aerius.wui.application.components.source.generic.GenericDetailComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.OffRoadMobileESFeature;
import nl.overheid.aerius.wui.application.domain.source.offroad.OffRoadMobileSource;
import nl.overheid.aerius.wui.application.domain.source.offroad.StandardOffRoadMobileSource;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "aer-offroad-detail", components = {
    GenericDetailComponent.class,
    EmissionValueComponent.class,
    VerticalCollapseGroup.class,
    VerticalCollapse.class,
    DividerComponent.class
})
public class OffroadDetailComponent extends BasicVueComponent {
  @Inject protected ApplicationContext applicationContext;

  @Prop @JsProperty protected OffRoadMobileESFeature source;
  @Prop protected String sector;
  @Prop String totalEmissionTitle;

  @Computed
  public List<Substance> getSubstances() {
    return applicationContext.getConfiguration().getSubstances();
  }

  @JsMethod
  public boolean hasAdblueUsage(final OffRoadMobileSource row) {
    return !Double.isNaN(row.getAdBlueUsage());
  }

  @JsMethod
  public String formatEmissionWithUnit(final double em) {
    return MessageFormatter.formatEmissionWithUnitSmart(em);
  }

  @JsMethod
  public String getOffRoadMobileSourceCode(final OffRoadMobileSource source) {
    if (applicationContext.getConfiguration().getSectorCategories()
        .determineOffRoadMobileSourceCategoryByCode(((StandardOffRoadMobileSource) source).getOffRoadMobileSourceCode()) == null) {
      return source.getCategory();
    }

    return applicationContext.getConfiguration().getSectorCategories()
        .determineOffRoadMobileSourceCategoryByCode(((StandardOffRoadMobileSource) source).getOffRoadMobileSourceCode())
        .getDescription();
  }

}
