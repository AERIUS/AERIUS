/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.util;

import com.google.gwt.regexp.shared.RegExp;

public final class StringUtils {
  private static final class Replacer {
    private final String target;
    private final String replacement;
    private final RegExp re;

    Replacer(final String target, final String replacement) {
      this.target = target;
      this.replacement = replacement;
      re = RegExp.compile(RegExp.quote(target), "g");
    }

    public String process(final String s) {
      if (s.contains(target)) {
        return re.replace(s, replacement);
      }
      return s;
    }
  }

  private static final Replacer amp = new Replacer("&amp;", "&");
  private static final Replacer lt = new Replacer("&lt;", "<");
  private static final Replacer gt = new Replacer("&gt;", ">");
  private static final Replacer quot = new Replacer("&quot;", "\"");
  private static final Replacer quotSingle = new Replacer("&#39;", "'");

  private StringUtils() {}

  public static String unescapeHtml(final String escaped) {
    String s = escaped;

    s = amp.process(s);
    s = lt.process(s);
    s = gt.process(s);
    s = quot.process(s);
    s = quotSingle.process(s);

    return s;
  }

  public static boolean isEmpty(final String str) {
    return str == null || str.isEmpty();
  }

  /**
   * Encodes special characters to HTML entities
   */
  public static String encodeHtmlEntities(final String text) {
    final StringBuilder output = new StringBuilder(text.length());
    final int length = text.length();
    for (int offset = 0; offset < length;) {
      final int codepoint = text.codePointAt(offset);
      final char[] chars = Character.toChars(codepoint);

      if (codepoint > 127 || "\"'<>&".indexOf(chars[0]) != -1) {
        output.append("&#").append(codepoint).append(";");
      } else {
        output.append(String.valueOf(chars));
      }
      offset += Character.charCount(codepoint);
    }
    return output.toString();
  }
}
