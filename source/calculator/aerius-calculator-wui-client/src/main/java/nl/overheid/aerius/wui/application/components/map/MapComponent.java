/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.map;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasBeforeDestroy;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import elemental2.dom.DomGlobal;

import jsinterop.annotations.JsMethod;
import jsinterop.base.Js;

import nl.aerius.geo.event.MapChangeEvent;
import nl.aerius.geo.event.MapRenderCompleteEvent;
import nl.aerius.geo.wui.OL3MapComponent;
import nl.aerius.search.wui.component.PopoutSearchComponent;
import nl.aerius.search.wui.context.SearchContext;
import nl.overheid.aerius.wui.application.command.MeasureToggleCommand;
import nl.overheid.aerius.wui.application.command.SearchPopupToggleCommand;
import nl.overheid.aerius.wui.application.command.geo.InfoPopupResetCommand;
import nl.overheid.aerius.wui.application.command.geo.InfoPopupToggleCommand;
import nl.overheid.aerius.wui.application.command.geo.LabelsPopupResetCommand;
import nl.overheid.aerius.wui.application.command.geo.LabelsPopupToggleCommand;
import nl.overheid.aerius.wui.application.command.geo.LayerPopupResetCommand;
import nl.overheid.aerius.wui.application.command.geo.LayerPopupToggleCommand;
import nl.overheid.aerius.wui.application.command.geo.LoginPopupResetCommand;
import nl.overheid.aerius.wui.application.command.geo.LoginPopupToggleCommand;
import nl.overheid.aerius.wui.application.components.button.IconComponent;
import nl.overheid.aerius.wui.application.components.notification.NotificationButtonComponent;
import nl.overheid.aerius.wui.application.components.toggle.SvgToggleButton;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.InfoPanelContext;
import nl.overheid.aerius.wui.application.context.LabelsPanelContext;
import nl.overheid.aerius.wui.application.context.LayerPanelContext;
import nl.overheid.aerius.wui.application.context.LoginPanelContext;
import nl.overheid.aerius.wui.application.context.MeasureContext;
import nl.overheid.aerius.wui.application.context.NavigationContext;
import nl.overheid.aerius.wui.application.daemon.misc.PersistablePreferencesContext;
import nl.overheid.aerius.wui.application.daemon.misc.PreferencesDaemon;
import nl.overheid.aerius.wui.application.daemon.misc.UserSettingChangeEvent;
import nl.overheid.aerius.wui.application.ui.main.ContentResizeEvent;
import nl.overheid.aerius.wui.vue.BasicVueEventComponent;
import nl.overheid.aerius.wui.vue.StyleDirective;

@Component(name = "aer-map", components = {
    NotificationButtonComponent.class,
    OL3MapComponent.class,
    PopoutSearchComponent.class,
    SvgToggleButton.class,
    IconComponent.class,
    StatefulIconButton.class,
}, directives = {
    StyleDirective.class,
})
public class MapComponent extends BasicVueEventComponent implements HasMounted, HasBeforeDestroy {
  private static final MapComponentEventBinder EVENT_BINDER = GWT.create(MapComponentEventBinder.class);

  interface MapComponentEventBinder extends EventBinder<MapComponent> {}

  @Prop EventBus eventBus;
  @Ref OL3MapComponent olMap;

  @Inject @Data SearchContext searchContext;

  @Data @Inject MeasureContext measureContext;
  @Data @Inject LabelsPanelContext labelsContext;
  @Data @Inject LayerPanelContext layerContext;
  @Data @Inject InfoPanelContext infoContext;
  @Data @Inject NavigationContext navigationContext;

  @Data @Inject PersistablePreferencesContext preferencesContext;
  @Data @Inject LoginPanelContext loginContext;

  @Ref NotificationButtonComponent notificationButton;
  @Ref StatefulIconButton infoButton;
  @Ref StatefulIconButton layerButton;
  @Ref StatefulIconButton labelsButton;
  @Ref StatefulIconButton searchButton;
  @Ref StatefulIconButton loginButton;
  private HandlerRegistration resizeHandler;
  private HandlerRegistration eventHandlers;

  @Data boolean loading;
  @Prop boolean isLeft;

  @Inject @Data ApplicationContext applicationContext;

  @PropDefault("isLeft")
  boolean isLeftDefault() {
    return false;
  }

  @EventHandler
  public void onMapChangeEvent(final MapChangeEvent e) {
    loading = true;
  }

  @EventHandler
  public void onMapRenderCompleteEvent(final MapRenderCompleteEvent e) {
    loading = false;
  }

  @EventHandler
  public void onUserSettingChangeEvent(final UserSettingChangeEvent c) {
    if (!PreferencesDaemon.ENABLE_LOGIN.equals(c.getKey())) {
      return;
    }

    updateDocks();
  }

  @EventHandler
  public void onContentResizeEvent(final ContentResizeEvent e) {
    updateDocks();
  }

  @Computed("manualPanelVisibility")
  public boolean manualPanelVisibility() {
    return navigationContext.isActiveManual();
  }

  @Watch("manualPanelVisibility()")
  public void onManualPanelVisibilityChange(final boolean old, final boolean neww) {
    updateDocks();
  }

  @Override
  public void mounted() {
    updateDocks();
    resizeHandler = Window.addResizeHandler(v -> updateDocks());
    eventHandlers = EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  private void updateDocks() {
    eventBus.fireEvent(new InfoPanelDockChangeCommand(infoButton.vue().$el()));
    eventBus.fireEvent(new LayerPanelDockChangeCommand(layerButton.vue().$el()));
    eventBus.fireEvent(new LabelsPanelDockChangeCommand(labelsButton.vue().$el()));
    eventBus.fireEvent(new SearchPanelDockChangeCommand(searchButton.vue().$el()));
    if (notificationButton != null) {
      eventBus.fireEvent(new NotificationButtonChangeCommand(notificationButton.vue().$el()));
    }
    if (isLoginPanelActive()) {
      eventBus.fireEvent(new LoginPanelDockChangeCommand(loginButton.vue().$el()));
    }
  }

  @Override
  public void beforeDestroy() {
    eventBus.fireEvent(new InfoPanelDockRemoveCommand(infoButton.vue().$el()));
    eventBus.fireEvent(new LayerPanelDockRemoveCommand(layerButton.vue().$el()));
    eventBus.fireEvent(new LabelsPanelDockRemoveCommand(labelsButton.vue().$el()));
    eventBus.fireEvent(new SearchPanelDockRemoveCommand(searchButton.vue().$el()));
    if (isLoginPanelActive()) {
      eventBus.fireEvent(new LoginPanelDockRemoveCommand(loginButton.vue().$el()));
    }
    resizeHandler.removeHandler();
    eventHandlers.removeHandler();
    olMap.detach();
  }

  @Computed("isLoginPanelActive")
  public boolean isLoginPanelActive() {
    return !isLeft && preferencesContext.isLoginEnabled() && applicationContext.getConfiguration().isDisplayLoginSettings();
  }

  public void attach() {
    olMap.attach();
    Js.asPropertyMap(DomGlobal.window).set("olMap", olMap.getInnerMap());
  }

  @JsMethod
  public void onMeasureClick() {
    eventBus.fireEvent(new MeasureToggleCommand());
  }

  @JsMethod
  public void onSearchPanelClick() {
    eventBus.fireEvent(new SearchPopupToggleCommand());
  }

  @JsMethod
  public boolean isSearchShowing() {
    return searchContext.isSearchShowing();
  }

  @JsMethod
  public void onLoginPanelClick() {
    eventBus.fireEvent(new LoginPopupToggleCommand());
  }

  @JsMethod
  public void onLoginPanelReset() {
    eventBus.fireEvent(new LoginPopupResetCommand());
  }

  @JsMethod
  public void onInfoPanelClick() {
    eventBus.fireEvent(new InfoPopupToggleCommand());
  }

  @JsMethod
  public void onInfoPanelReset() {
    eventBus.fireEvent(new InfoPopupResetCommand());
  }

  @JsMethod
  public void onLayerPanelClick() {
    eventBus.fireEvent(new LayerPopupToggleCommand());
  }

  @JsMethod
  public void onLayerPanelReset() {
    eventBus.fireEvent(new LayerPopupResetCommand());
  }

  @JsMethod
  public void onLabelsPanelClick() {
    eventBus.fireEvent(new LabelsPopupToggleCommand());
  }

  @JsMethod
  public void onLabelsPanelReset() {
    eventBus.fireEvent(new LabelsPopupResetCommand());
  }
}
