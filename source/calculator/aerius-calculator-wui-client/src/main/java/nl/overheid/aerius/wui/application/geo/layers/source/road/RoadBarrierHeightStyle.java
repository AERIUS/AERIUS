/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source.road;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import ol.OLFactory;
import ol.geom.LineString;
import ol.style.Style;

import nl.aerius.geo.domain.legend.Legend;
import nl.aerius.geo.domain.legend.LegendType;
import nl.aerius.geo.wui.util.OL3GeometryUtil;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.legend.ColorRange;
import nl.overheid.aerius.shared.domain.legend.ColorRangeType;
import nl.overheid.aerius.shared.domain.legend.ColorRangesLegend;
import nl.overheid.aerius.wui.application.domain.source.RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.road.ADMSRoadSideBarrier;
import nl.overheid.aerius.wui.application.domain.source.road.SRM2RoadSideBarrier;
import nl.overheid.aerius.wui.application.i18n.M;

public class RoadBarrierHeightStyle extends RoadBarrierStyle implements RoadStyle {

  private final Map<Double, Style> styleMap;
  private final List<ColorRange> colorRange;

  public RoadBarrierHeightStyle(final Function<ColorRangeType, List<ColorRange>> colorRangeFunction) {
    colorRange = colorRangeFunction.apply(ColorRangeType.ROAD_BARRIER_HEIGHT);
    styleMap = createStyleMap();
  }

  @Override
  public List<Style> getStyle(final RoadESFeature source, final String vehicleTypeCode, final double resolution) {
    final List<Style> styleList = new ArrayList<>();
    styleList.add(ROAD_STYLE);
    addStyle(styleList, source, BarrierSide.LEFT, resolution);
    addStyle(styleList, source, BarrierSide.RIGHT, resolution);
    return styleList;
  }

  private void addStyle(final List<Style> styleList, final RoadESFeature source, final BarrierSide barrierSide, final Double resolution) {
    final LineString geometry = (LineString) source.getGeometry();
    if (showBarrier(source, barrierSide)) {
      styleList.add(getBarrierStyle(geometry, getBarrierHeight(source, barrierSide), getBarrierDistanceOnMap(barrierSide, resolution)));
    }
  }

  private Style getBarrierStyle(final LineString geometry, final double height, final double distance) {
    final double lowerValueRange = styleMap.keySet().stream()
        .mapToDouble(x -> x)
        .filter(x -> x <= height)
        .max().orElse(0D);
    final Style roadBarrierStyle = styleMap.get(lowerValueRange).clone();
    roadBarrierStyle.setGeometry(OL3GeometryUtil.offsetFromLineString(geometry, distance));
    return roadBarrierStyle;
  }

  @Override
  public Legend getLegend(final ApplicationConfiguration appThemeConfiguration) {
    return withRoadNetworkLabel(
        new ColorRangesLegend(colorRange, M.messages().colorRangesLegendBetweenText(), LegendType.LINE, M.messages().unitSingularMeter()));
  }

  private Map<Double, Style> createStyleMap() {
    return colorRange.stream()
        .collect(Collectors.toMap(
            cr -> cr.getLowerValue(),
            cr -> OLFactory.createStyle(OLFactory.createStroke(OLFactory.createColor(cr.getColor()),
                ROAD_BARRIER_WIDTH))));
  }

  private static Double getBarrierHeight(final RoadESFeature source, final BarrierSide barrierSide) {
    return getBarrierProperty(source, barrierSide, 0D, srmBarrier -> Optional.of(srmBarrier).map(SRM2RoadSideBarrier::getHeight).orElse(0D),
        admsBarrier -> Optional.of(admsBarrier).map(ADMSRoadSideBarrier::getAverageHeight).orElse(0D));
  }
}
