/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.export;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.gwt.user.client.Window;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.HorizontalCollapse;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.button.IconComponent;
import nl.overheid.aerius.wui.application.components.progress.JobProgressBarComponent;
import nl.overheid.aerius.wui.application.components.queue.QueueCounter;
import nl.overheid.aerius.wui.application.components.tooltip.TooltipComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.export.HistoryRecord;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    ButtonIcon.class,
    IconComponent.class,
    QueueCounter.class,
    JobProgressBarComponent.class,
    HorizontalCollapse.class,
    TooltipComponent.class
})
public class HistoryItemComponent extends BasicVueComponent {
  @Prop HistoryRecord item;

  @Inject @Data ApplicationContext applicationContext;

  @Computed
  public String getHistoryItemName() {
    return M.messages().exportHistoryDisplayLabel(item.getLabel());
  }

  @Computed
  public String getDatetime() {
    return M.formatCompactDateTime(item.getDateTime());
  }

  @Computed
  public boolean isCancelable() {
    return item.getStatus() != null && !item.getStatus().isFinalState();
  }

  @Computed
  public boolean isCompletedState() {
    return item.getStatus() == JobState.COMPLETED;
  }

  @Computed
  public String getCalculationErrorMessage() {
    return item.getErrorMessage();
  }

  @Computed
  public boolean isErrorState() {
    return item.getStatus() == JobState.ERROR;
  }

  @Computed
  public int getQueuePosition() {
    return item.getQueuePosition();
  }

  @Computed
  public boolean isQueuingState() {
    return item.getStatus() == JobState.QUEUING;
  }

  @Computed
  public boolean isRunningState() {
    final JobState state = item.getStatus();

    return state != null && !state.isFinalState()
        && (state != JobState.UNDEFINED && state != JobState.QUEUING);
  }

  @JsMethod
  public void cancelExport() {
    if (Window.confirm(i18n.exportHistoryRemoveConfirm())) {
      vue().$emit("remove");
    }
  }
}
