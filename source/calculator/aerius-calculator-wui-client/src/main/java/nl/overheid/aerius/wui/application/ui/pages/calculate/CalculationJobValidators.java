/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.calculate;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.aerius.vuelidate.ValidationOptions;
import nl.aerius.vuelidate.Validations;
import nl.aerius.vuelidate.ValidatorBuilder;

public class CalculationJobValidators extends ValidationOptions<CalculationJobEditView> {

  private static final int MAX_NAME_LENGTH = 128;

  @JsType(isNative = true, namespace = JsPackage.GLOBAL)
  static class CalculationJobValidations extends Validations {
    public @JsProperty Validations labelV;
  }

  @Override
  protected void constructValidators(final Object options, final ValidatorInstaller v) {
    final CalculationJobEditView instance = Js.uncheckedCast(options);

    v.install("labelV",
        () -> instance.labelV = null,
        ValidatorBuilder.create()
            .required()
            .maxLength(MAX_NAME_LENGTH));
  }

}
