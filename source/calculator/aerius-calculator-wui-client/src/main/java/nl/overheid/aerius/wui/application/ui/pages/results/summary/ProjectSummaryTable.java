/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.results.summary;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings.DepositionValueDisplayType;
import nl.overheid.aerius.shared.domain.legend.ColorRange;
import nl.overheid.aerius.shared.domain.legend.ColorRangeType;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationHandle;
import nl.overheid.aerius.wui.application.domain.summary.JobSummaryProjectRecord;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.ui.pages.results.GeneralizedEmissionResultType;
import nl.overheid.aerius.wui.application.ui.pages.results.stats.ResultsStatisticsFormatter;
import nl.overheid.aerius.wui.application.util.NumberUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    SortIndicatorComponent.class,
})
public class ProjectSummaryTable extends BasicVueComponent implements HasCreated {
  public enum Column {
    SITE, SITUATION, POLLUTANT
  }

  @Inject @Data ApplicationContext applicationContext;
  @Inject ResultsStatisticsFormatter resultsStatisticsFormatter;

  @Prop(required = true) CalculationExecutionContext jobContext;

  @Prop @JsProperty JobSummaryProjectRecord[] projects;

  @Data Comparator<JobSummaryProjectRecord> comparator;
  @Data Object sortType;
  @Data boolean asc;

  @Computed
  public List<ResultStatisticType> getSupportedStatisticTypes() {
    return applicationContext.getConfiguration().getSupportedSummaryStatisticTypes();
  }

  @JsMethod
  public String getPollutant(final JobSummaryProjectRecord projectRecord) {
    return M.messages().calculateEmissionResultKey(projectRecord.getEmissionResultKey());
  }

  @JsMethod
  public boolean isStatisticSupported(final ResultStatisticType stat) {
    return getSupportedStatisticTypes().contains(stat);
  }

  @JsMethod
  public String getRecordUnit(final JobSummaryProjectRecord projectRecord) {
    return M.messages().emissionResultUnit(getResultType(projectRecord), getDisplayType());
  }

  @JsMethod
  public GeneralizedEmissionResultType getResultType(final JobSummaryProjectRecord projectRecord) {
    return GeneralizedEmissionResultType.fromEmissionResultType(projectRecord.getEmissionResultKey().getEmissionResultType());
  }

  @Computed
  public DepositionValueDisplayType getDisplayType() {
    return applicationContext.getConfiguration().getEmissionResultValueDisplaySettings().getDisplayType();
  }

  @JsMethod
  public String getProcessContributionPercCLColor(final JobSummaryProjectRecord projectRecord) {
    return findColorInRange(applicationContext.getConfiguration().getColorRange(ColorRangeType.PROJECT_CALCULATION_PERCENTAGE_CRITICAL_LOAD),
        getStatistic(projectRecord, ResultStatisticType.MAX_PERCENTAGE_CRITICAL_LEVEL));
  }

  @JsMethod
  public String getMaxTotalPercCLColor(final JobSummaryProjectRecord projectRecord) {
    return findColorInRange(applicationContext.getConfiguration().getColorRange(ColorRangeType.MAX_TOTAL_AS_PERCENTAGE_CRITICAL_LOAD),
        getStatistic(projectRecord, ResultStatisticType.MAX_TOTAL_PERCENTAGE_CRITICAL_LEVEL));
  }

  private String findColorInRange(final List<ColorRange> colorRange, final Double num) {
    if (num == null) {
      return null;
    }

    ColorRange prev = null;
    for (final ColorRange range : colorRange) {
      if (range != null && range.getLowerValue() > num) {
        return range.getColor();
      }

      prev = range;
    }

    return prev == null ? null : prev.getColor();
  }

  @JsMethod
  public String formatDepositionValue(final JobSummaryProjectRecord projectRecord, final ResultStatisticType statisticsType) {
    return resultsStatisticsFormatter.formatStatisticType(projectRecord.getMaxValues(), statisticsType);
  }

  @JsMethod
  public Double getStatistic(final JobSummaryProjectRecord projectRecord, final ResultStatisticType statisticsType) {
    return projectRecord == null || projectRecord.getMaxValues() == null || statisticsType == null
        ? null
        : projectRecord.getMaxValues().getValue(statisticsType);
  }

  @Override
  public void created() {
    sort(v -> getSiteName(v), Column.SITE);
  }

  @JsMethod
  public boolean isActive(final Object column) {
    return column == sortType;
  }

  @Computed
  public List<JobSummaryProjectRecord> getProjectsData() {
    if (projects == null) {
      return new ArrayList<>();
    }

    return Stream.of(projects)
        .sorted(comparator)
        .collect(Collectors.toList());
  }

  @JsMethod
  public String getSiteName(final JobSummaryProjectRecord projectRecord) {
    final String siteName = projectRecord.getAssessmentArea().getName();
    return siteName == null ? "" : siteName;
  }

  @JsMethod
  public String getSituationName(final JobSummaryProjectRecord projectRecord) {
    final SituationHandle situationHandle = getSituation(projectRecord.getCalculationReference());
    if (situationHandle == null) {
      return "";
    }

    return situationHandle.getName();
  }

  @JsMethod
  public String getSituationType(final JobSummaryProjectRecord projectRecord) {
    final SituationHandle situationHandle = getSituation(projectRecord.getCalculationReference());
    if (situationHandle == null) {
      return "";
    }

    return i18n.situationType(situationHandle.getType());
  }

  @JsMethod
  public void sort(final Function<JobSummaryProjectRecord, String> func, final Object key) {
    if (this.sortType == key) {
      this.asc = !this.asc;
    } else {
      this.sortType = key;
      this.asc = true;
    }

    comparator = (o1, o2) -> (asc ? 1 : -1) * func.apply(o1).compareTo(func.apply(o2));
  }

  @JsMethod
  public void sortNumber(final Function<JobSummaryProjectRecord, Number> func, final Object key) {
    if (this.sortType == key) {
      this.asc = !this.asc;
    }
    this.sortType = key;

    comparator = NumberUtil.sortComparator(func, asc);
  }

  private SituationHandle getSituation(final String calculationReference) {
    return CalculationExecutionContext.findHandleFromId(jobContext, calculationReference);
  }
}
