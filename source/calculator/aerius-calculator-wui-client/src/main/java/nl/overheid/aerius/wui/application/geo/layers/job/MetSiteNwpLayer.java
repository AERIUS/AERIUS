/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.job;

import javax.inject.Inject;

import com.google.inject.assistedinject.Assisted;

import ol.OLFactory;
import ol.layer.Image;
import ol.layer.Layer;
import ol.source.ImageWms;
import ol.source.ImageWmsOptions;
import ol.source.ImageWmsParams;

import nl.aerius.geo.domain.IsLayer;
import nl.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;

/**
 * Provides the NWP site layer
 */
public class MetSiteNwpLayer implements IsLayer<Layer> {

  private static final String WMS_MET_SITES_NWP_VIEW = "calculator:wms_met_sites_nwp_view";
  private static final double DEFAULT_OPACITY = 0.8;

  private final Image layer;
  private final ImageWmsParams imageWmsParams;
  private final ImageWmsOptions imageWmsOptions;
  private ImageWms imageWms;

  private final LayerInfo info;

  @Inject
  public MetSiteNwpLayer(final EnvironmentConfiguration configuration, @Assisted final int zIndex) {
    imageWmsParams = OLFactory.createOptions();

    this.info = new LayerInfo();
    this.info.setName(this.getClass().getCanonicalName());
    this.info.setTitle(M.messages().layerMetSites());

    imageWmsOptions = OLFactory.createOptions();
    imageWmsOptions.setUrl(configuration.getGeoserverBaseUrl());
    imageWmsOptions.setParams(imageWmsParams);

    layer = new Image();
    layer.setZIndex(zIndex);
    layer.setVisible(false);
    layer.setOpacity(DEFAULT_OPACITY);

    updateLayer();
  }

  private void updateLayer() {
    imageWmsParams.setLayers(WMS_MET_SITES_NWP_VIEW);

    if (imageWms == null) {
      imageWms = new ImageWms(imageWmsOptions);
      layer.setSource(imageWms);
    }

    imageWms.refresh();
  }

  @Override
  public Layer asLayer() {
    return layer;
  }

  @Override
  public LayerInfo getInfo() {
    return info;
  }

}
