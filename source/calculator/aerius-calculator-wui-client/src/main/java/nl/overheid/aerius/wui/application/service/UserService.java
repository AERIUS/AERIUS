/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.service;

import java.util.Map;

import javax.inject.Inject;

import com.google.gwt.user.client.rpc.AsyncCallback;

import nl.aerius.wui.util.RequestUtil;
import nl.overheid.aerius.shared.RequestMappings;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;

import vuegwt.shaded.com.helger.commons.annotation.Singleton;

@Singleton
public class UserService {

  @Inject EnvironmentConfiguration cfg;
  @Inject HeaderHelper hdr;

  public void isValidApiKey(final String apiKey, final AsyncCallback<String> callback) {
    final String url = RequestUtil.prepareUrl(cfg.getConnectBaseUrl(), RequestMappings.CONNECT_USER_VALIDATE);

    final Map<String, String> additionalHeaders = hdr.defaultHeaders(apiKey);
    RequestUtil.doGetWithHeaders(url, additionalHeaders, callback);
  }
}
