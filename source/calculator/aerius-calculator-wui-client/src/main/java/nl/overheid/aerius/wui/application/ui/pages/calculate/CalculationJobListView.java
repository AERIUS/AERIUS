/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.calculate;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.wui.application.command.calculation.CalculationDeleteJobCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobClearCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobDuplicateCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobEditCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobPeekCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobStartCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobToggleCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobUnpeekCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobsDeleteAllCommand;
import nl.overheid.aerius.wui.application.command.calculation.TemporaryCalculationJobAddCommand;
import nl.overheid.aerius.wui.application.command.exporter.ExportPrepareCommand;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.modify.ModifyListComponent;
import nl.overheid.aerius.wui.application.components.modify.ModifyListCustomButton;
import nl.overheid.aerius.wui.application.context.GenericValidationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationPreparationContext;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.config.ApplicationFlags;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    CollapsiblePanel.class,
    ModifyListComponent.class,
    CalculationJobRowComponent.class,
    CalculationSummaryList.class,
    VerticalCollapse.class,
    VerticalCollapseGroup.class,
})
public class CalculationJobListView extends BasicVueComponent {
  @Prop EventBus eventBus;

  @Inject @Data ScenarioContext context;
  @Inject @Data CalculationPreparationContext prepContext;
  @Inject @Data GenericValidationContext validationContext;

  @Inject @Data ApplicationFlags flags;

  @JsMethod
  public List<ModifyListCustomButton> customListButtons() {
    return Arrays.asList(
      new ModifyListCustomButton(
        i18n.esButtonCalculate(i18n.calculationJob()),
        i18n.esButtonCalculate(i18n.calculationJob()),
        "icon-menu-calculate",
        "calculateCalculationJobButton",
        true
        ) {
      @Override
      @JsMethod
      public void select() {
        calculateCalculationJob();
      }
    },
      new ModifyListCustomButton(
          i18n.esButtonExport(i18n.calculationJob()),
          i18n.esButtonExport(i18n.calculationJob()),
          "icon-menu-export",
          "exportCalculationJobButton",
          true
          ) {
        @Override
        @JsMethod
        public void select() {
          exportCalculationJob();
        }
      }
    );
  }

  @JsMethod
  public boolean isActiveJob(final CalculationJobContext job) {
    return prepContext.isActiveJob(job);
  }

  @JsMethod
  public void openJob(final CalculationJobContext job) {
    eventBus.fireEvent(new CalculationJobToggleCommand(job));
  }

  @JsMethod
  public void closeJob(final CalculationJobContext job) {
    eventBus.fireEvent(new CalculationJobClearCommand());
  }

  @Computed
  public List<CalculationJobContext> getJobs() {
    return prepContext.getJobs();
  }

  @JsMethod
  public void addCalculationJob() {
    eventBus.fireEvent(new TemporaryCalculationJobAddCommand());
  }

  @JsMethod
  public void editCalculationJob() {
    eventBus.fireEvent(new CalculationJobEditCommand());
  }

  @JsMethod
  public void duplicateCalculationJob() {
    eventBus.fireEvent(new CalculationJobDuplicateCommand());
  }

  @JsMethod
  public void deleteCalculationJob() {
    eventBus.fireEvent(new CalculationDeleteJobCommand(prepContext.getActiveJob()));
  }

  @JsMethod
  public void calculateCalculationJob() {
    eventBus.fireEvent(new CalculationJobStartCommand(prepContext.getActiveJob()));
  }

  @JsMethod
  public void exportCalculationJob() {
    eventBus.fireEvent(new ExportPrepareCommand(prepContext.getActiveJob()));
  }

  @JsMethod
  public void peekCalculationJob(final CalculationJobContext job) {
    eventBus.fireEvent(new CalculationJobPeekCommand(job));
  }

  @JsMethod
  public void unpeekCalculationJob(final CalculationJobContext job) {
    eventBus.fireEvent(new CalculationJobUnpeekCommand(job));
  }

  @JsMethod
  public void selectCalculationJob(final CalculationJobContext job) {
    eventBus.fireEvent(new CalculationJobToggleCommand(job));
  }

  @JsMethod
  public void deleteAllCalculationJobs() {
    if (Window.confirm(M.messages().calculationJobsDeleteWarning())) {
      eventBus.fireEvent(new CalculationJobsDeleteAllCommand());
    }
  }

}
