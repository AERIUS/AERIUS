/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers;

import java.util.List;

import nl.aerius.geo.domain.IsLayer;
import nl.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.constants.DrivingSide;
import nl.overheid.aerius.wui.application.geo.layers.building.BuildingMarkerLayer;
import nl.overheid.aerius.wui.application.geo.layers.building.ModifyBuildingGeometryLayer;
import nl.overheid.aerius.wui.application.geo.layers.building.SelectedBuildingMarkerLayer;
import nl.overheid.aerius.wui.application.geo.layers.building.SituationBuildingGeometryLayer;
import nl.overheid.aerius.wui.application.geo.layers.calculationpoint.CalculationPointGeometryLayer;
import nl.overheid.aerius.wui.application.geo.layers.calculationpoint.CalculationPointLayerGroup;
import nl.overheid.aerius.wui.application.geo.layers.calculationpoint.CalculationPointMarkerLayer;
import nl.overheid.aerius.wui.application.geo.layers.calculationpoint.SelectedCalculationPointMarkerLayer;
import nl.overheid.aerius.wui.application.geo.layers.criticallevel.PercentageCriticalLevelResultLayer;
import nl.overheid.aerius.wui.application.geo.layers.criticallevel.TotalPercentageCriticalLevelResultLayer;
import nl.overheid.aerius.wui.application.geo.layers.habitats.HabitatHoverLayer;
import nl.overheid.aerius.wui.application.geo.layers.habitats.HabitatSelectLayer;
import nl.overheid.aerius.wui.application.geo.layers.habitats.HabitatTypeLayerGroup;
import nl.overheid.aerius.wui.application.geo.layers.habitats.NitrogenSensitivityLayerGroup;
import nl.overheid.aerius.wui.application.geo.layers.job.JobInputGeometryLayer;
import nl.overheid.aerius.wui.application.geo.layers.job.JobInputLayerGroup;
import nl.overheid.aerius.wui.application.geo.layers.job.JobInputMarkerLayer;
import nl.overheid.aerius.wui.application.geo.layers.job.MetSiteLayerGroup;
import nl.overheid.aerius.wui.application.geo.layers.job.MetSiteMarkerLayer;
import nl.overheid.aerius.wui.application.geo.layers.job.MetSiteNwpLayer;
import nl.overheid.aerius.wui.application.geo.layers.results.ArchiveContributionOutlineLayer;
import nl.overheid.aerius.wui.application.geo.layers.results.ArchiveProjectMarkersLayer;
import nl.overheid.aerius.wui.application.geo.layers.results.CalculationMarkersLayer;
import nl.overheid.aerius.wui.application.geo.layers.results.CalculationPointResultLayer;
import nl.overheid.aerius.wui.application.geo.layers.results.CalculationResultLayer;
import nl.overheid.aerius.wui.application.geo.layers.results.CalculationResultLayerGroup;
import nl.overheid.aerius.wui.application.geo.layers.source.GenericSourceGeometryLayer;
import nl.overheid.aerius.wui.application.geo.layers.source.GeometryHighlightLayer;
import nl.overheid.aerius.wui.application.geo.layers.source.InfoMarkerLayer;
import nl.overheid.aerius.wui.application.geo.layers.source.ModifySourceGeometryLayer;
import nl.overheid.aerius.wui.application.geo.layers.source.RoadSourceGeometryLayer;
import nl.overheid.aerius.wui.application.geo.layers.source.SelectedSourceMarkerLayer;
import nl.overheid.aerius.wui.application.geo.layers.source.SourceLayerGroup;
import nl.overheid.aerius.wui.application.geo.layers.source.SourceMarkerLayer;
import nl.overheid.aerius.wui.application.geo.layers.zone.CalculationBoundaryLayer;
import nl.overheid.aerius.wui.application.geo.layers.zone.ZoneOfInfluenceLayer;

public interface LayerFactory {
  SourceLayerGroup createSourceLayerGroup(final int zIndex);

  GenericSourceGeometryLayer createGenericSourceGeometryLayer(final LayerInfo info, final int zIndex);

  SourceMarkerLayer createSourceMarkerLayer(final LayerInfo info, final int zIndex);

  SelectedSourceMarkerLayer createSelectedSourceMarkerLayer(final LayerInfo info, final int zIndex);

  ModifySourceGeometryLayer createModifySourceGeometryLayer(final LayerInfo info, final int zIndex);

  MetSiteMarkerLayer createMetSiteLayer(int zIndex);

  MetSiteNwpLayer createMetSiteNwpLayer(int zIndex);

  MetSiteLayerGroup createMetSiteLayerGroup(int zIndex);

  CalculationPointLayerGroup createCalculationPointLayerGroup(final int zIndex);

  CalculationPointMarkerLayer createCalculationPointMarkerLayer(final int zIndex);

  SelectedCalculationPointMarkerLayer createSelectedCalculationPointMarkerLayer(final int zIndex);

  CalculationPointGeometryLayer createCalculationPointGeometryLayer(final int zIndex);

  JobInputLayerGroup createJobInputLayerGroup(final int zIndex, final ApplicationConfiguration themeConfiguration);

  JobInputMarkerLayer createJobInputMarkerLayer(final LayerInfo info, final FeatureType featureType, final int zIndex);

  JobInputGeometryLayer createJobInputGeometryLayer(final LayerInfo info, final int zIndex);

  RoadSourceGeometryLayer createRoadSourceGeometryLayer(final ApplicationConfiguration applicationConfiguration,
      final ApplicationConfiguration themeConfiguration, final DrivingSide drivingSide, final int zIndex);

  SituationBuildingGeometryLayer createSituationBuildingGeometryLayer(LayerInfo info, int zIndex);

  BuildingMarkerLayer createBuildingMarkerLayer(final LayerInfo info, final int zIndex);

  SelectedBuildingMarkerLayer createSelectedBuildingMarkerLayer(LayerInfo info, int zIndex);

  ModifyBuildingGeometryLayer createModifyBuildingGeometryLayer(LayerInfo info, int zIndex);

  CalculationResultLayer createCalculationResultLayer(final ApplicationConfiguration themeConfiguration);

  CalculationResultLayerGroup createCalculationResultLayerGroup(final ApplicationConfiguration themeConfiguration);

  ArchiveContributionOutlineLayer createArchiveContributionOutlineLayer();

  ArchiveProjectMarkersLayer createArchiveProjectsMarkersLayer(final int zIndex);

  PercentageCriticalLevelResultLayer createPercentageCriticalLevelLayer(final ApplicationConfiguration themeConfiguration);

  TotalPercentageCriticalLevelResultLayer createTotalPercentageCriticalLevelLayer(final ApplicationConfiguration themeConfiguration);

  CalculationMarkersLayer createCalculationMarkersLayer();

  CalculationPointResultLayer createCalculationPointResultLayer();

  HabitatSelectLayer createHabitatSelectionLayer();

  HabitatHoverLayer createHabitatHoverLayer();

  InfoMarkerLayer createInfoMarkerLayer();

  GeometryHighlightLayer createGeometryHighlightLayer();

  HabitatAreasSensitivityLevelLayer createHabitatAreasSensitivityLevelLayer(final ApplicationConfiguration themeConfiguration, final int zIndex);

  BackgroundResultLayer createBackgroundDepositionLayer(final ApplicationConfiguration themeConfiguration);

  NatureAreasLayer createNatureAreasLayer(ApplicationConfiguration themeConfiguration);

  CalculationBoundaryLayer createCalculationBoundaryLayer(final String boundaryWkt);

  ZoneOfInfluenceLayer createZoneOfInfluenceLayer();

  HabitatTypeLayerGroup createHabitatTypesLayerGroup(List<IsLayer<?>> layers);

  NitrogenSensitivityLayerGroup createNitrogenSensitivityLayerGroup(List<IsLayer<?>> layers);

}
