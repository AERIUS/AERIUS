/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.farmanimalhousing.standard.additional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Emit;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.sector.category.FarmAdditionalHousingSystemCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalHousingCategories;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.components.tooltip.TooltipComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.farm.AdditionalHousingSystem;
import nl.overheid.aerius.wui.application.domain.source.farm.AdditionalSystemType;
import nl.overheid.aerius.wui.application.domain.source.farm.CustomAdditionalHousingSystem;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmAnimalHousingUtil;
import nl.overheid.aerius.wui.application.domain.source.farm.StandardAdditionalHousingSystem;

/**
 * Component to display the Farm Animal Housing Additional technique edit panel.
 * For standard techniques it shows the row with the name and delete button.
 * For custom techniques it shows the specific technique and the delete button row,
 * But also 2 input fields, one for reduction percentage, and one for explanation.
 */
@Component(directives = {
    ValidateDirective.class,
}, components = {
    AnimalStandardAdditionalCustomRowComponent.class,
    ButtonIcon.class,
    LabeledInputComponent.class,
    TooltipComponent.class,
    ValidationBehaviour.class,
    VerticalCollapse.class,
    InputErrorComponent.class,
})
public class AnimalStandardAdditionalRowComponent extends ErrorWarningValidator {
  @Prop(required = true) @JsProperty String farmAnimalHousingCode;
  @Prop(required = true) @JsProperty AdditionalHousingSystem system;
  @Prop(required = true) int index;

  @Inject @Data ApplicationContext applicationContext;

  @JsMethod
  @Emit(value = "delete")
  protected void deleteRow() {}

  @Computed("isCustom")
  protected boolean isCustom() {
    return AdditionalSystemType.CUSTOM.equals(system.getAdditionalSystemType());
  }

  @JsMethod
  protected FarmAnimalHousingCategories getCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getFarmAnimalHousingCategories();
  }

  @JsMethod
  protected String additionalTechniqueName() {
    if (AdditionalSystemType.STANDARD == system.getAdditionalSystemType()) {
      final FarmAdditionalHousingSystemCategory category = getCategories()
          .determineAdditionalHousingCategoryByCode(((StandardAdditionalHousingSystem) system).getAdditionalSystemCode());
      return category == null ? "" : category.getName();
    } else if (AdditionalSystemType.CUSTOM == system.getAdditionalSystemType()) {
      return ((CustomAdditionalHousingSystem) system).isAirScrubber()
          ? i18n.esFarmAnimalHousingStandardAdditionalCustomAirScrubber()
          : i18n.esFarmAnimalHousingStandardAdditionalCustomOther();
    } else {
      return "";
    }
  }

  @Computed("isIncompatible")
  public boolean isIncompatible() {
    return FarmAnimalHousingUtil.isIncompatibleCombination(system, farmAnimalHousingCode, getCategories());
  }
}
