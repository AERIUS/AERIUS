/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.info;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.geo.shared.WKTGeometry;

/**
 * Bean containing area information.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class Natura2000Info extends AssessmentArea {

  private String code;
  private int natura2000AreaId;
  private String contractor;
  private long surface;
  private String protection;
  private String status;
  private WKTGeometry geometry;

  private @JsProperty HabitatInfo[] habitats;

  public final @JsOverlay String getCode() {
    return code;
  }

  public final @JsOverlay int getNatura2000AreaId() {
    return natura2000AreaId;
  }

  public final @JsOverlay String getContractor() {
    return contractor;
  }

  public final @JsOverlay long getSurface() {
    return surface;
  }

  public final @JsOverlay String getProtection() {
    return protection;
  }

  public final @JsOverlay String getStatus() {
    return status;
  }

  public final @JsOverlay HabitatInfo[] getHabitats() {
    return habitats;
  }

  public final @JsOverlay WKTGeometry getGeometry() {
    return geometry;
  }

}
