/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.offroad;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.wui.vue.ReactivityUtil;

/**
 * Client side implementation of StandardOffRoadMobileSource props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class StandardOffRoadMobileSource extends OffRoadMobileSource {

  private String offRoadMobileSourceCode;
  private int literFuelPerYear;
  private int operatingHoursPerYear;
  private int literAdBluePerYear;

  public static final @JsOverlay StandardOffRoadMobileSource create() {
    final StandardOffRoadMobileSource props = Js.uncheckedCast(JsPropertyMap.of());
    init(props);
    return props;
  }

  public static final @JsOverlay void init(final StandardOffRoadMobileSource props) {
    OffRoadMobileSource.init(props, OffRoadType.STANDARD);
    ReactivityUtil.ensureJsPropertyAsNull(props, "offRoadMobileSourceCode", props::setOffRoadMobileSourceCode);

    ReactivityUtil.ensureJsProperty(props, "literFuelPerYear", props::setLiterFuelPerYear, 0);
    ReactivityUtil.ensureJsProperty(props, "operatingHoursPerYear", props::setOperatingHoursPerYear, 0);
    ReactivityUtil.ensureJsProperty(props, "literAdBluePerYear", props::setLiterAdBluePerYear, 0);
  }

  public final @JsOverlay String getOffRoadMobileSourceCode() {
    return offRoadMobileSourceCode;
  }

  public final @JsOverlay void setOffRoadMobileSourceCode(final String offRoadMobileSourceCode) {
    this.offRoadMobileSourceCode = offRoadMobileSourceCode;
  }

  public final @JsOverlay int getLiterFuelPerYear() {
    return literFuelPerYear;
  }

  public final @JsOverlay void setLiterFuelPerYear(final int literFuelPerYear) {
    this.literFuelPerYear = literFuelPerYear;
  }

  public final @JsOverlay int getOperatingHoursPerYear() {
    return operatingHoursPerYear;
  }

  public final @JsOverlay void setOperatingHoursPerYear(final int operatingHoursPerYear) {
    this.operatingHoursPerYear = operatingHoursPerYear;
  }

  public final @JsOverlay int getLiterAdBluePerYear() {
    return literAdBluePerYear;
  }

  public final @JsOverlay void setLiterAdBluePerYear(final int literAdBluePerYear) {
    this.literAdBluePerYear = literAdBluePerYear;
  }

}
