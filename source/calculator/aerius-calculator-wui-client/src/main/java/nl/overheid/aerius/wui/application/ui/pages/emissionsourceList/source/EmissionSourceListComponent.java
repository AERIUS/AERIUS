/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.emissionsourceList.source;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.EventBus;

import elemental2.dom.MouseEvent;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.wui.application.ApplicationLimits;
import nl.overheid.aerius.wui.application.command.source.DeleteAllEmissionSourceCommand;
import nl.overheid.aerius.wui.application.command.source.DeleteAllRoadNetworkSourcesCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceCreateNewCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceDeleteSelectedCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceDuplicateSelectedCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditSelectedCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceFeatureToggleSelectCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourcePeekFeatureCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceUnpeekFeatureCommand;
import nl.overheid.aerius.wui.application.command.source.FeatureZoomCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.modify.ModifyListComponent;
import nl.overheid.aerius.wui.application.components.source.EmissionSourceRowComponent;
import nl.overheid.aerius.wui.application.components.source.emission.EmissionValueComponent;
import nl.overheid.aerius.wui.application.context.EmissionSourceListContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.road.RoadEmissionSourceTypes;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.application.ui.pages.emissionsourceList.road.RoadNetworkRowComponent;
import nl.overheid.aerius.wui.application.util.DoubleClickUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    VerticalCollapse.class,
    VerticalCollapseGroup.class,
    EmissionSourceRowComponent.class,
    EmissionValueComponent.class,
    ModifyListComponent.class,
    ButtonIcon.class,
    RoadNetworkRowComponent.class,
})
public class EmissionSourceListComponent extends BasicVueComponent {
  @Prop EventBus eventBus;
  @Prop SituationContext situation;

  @Inject @Data ScenarioContext context;
  @Inject @Data EmissionSourceListContext listContext;

  @Data boolean roadNetworkSelected;

  private final DoubleClickUtil<EmissionSourceFeature> doubleClickUtil = new DoubleClickUtil<>();

  @JsMethod
  public void selectRoadNetwork(final boolean selected) {
    roadNetworkSelected = selected;
  }

  @Computed
  public List<EmissionSourceFeature> getAllEmissionSources() {
    return Optional.ofNullable(situation)
        .map(v -> v.getSources())
        .orElse(new ArrayList<>())
        .stream()
        .limit(ApplicationLimits.SCENARIO_INPUT_LIST_DRAW_LIMIT)
        .collect(Collectors.toList());
  }

  @Computed("hasEmissionSources")
  public boolean hasEmissionSources() {
    return !getAllEmissionSources().isEmpty();
  }

  @Computed
  public List<EmissionSourceFeature> getEmissionSources() {
    return getAllEmissionSources().stream()
        .filter(v -> !RoadEmissionSourceTypes.isRoadSource(v))
        .collect(Collectors.toList());
  }

  @JsMethod
  public void createNewSource() {
    eventBus.fireEvent(new EmissionSourceCreateNewCommand());
  }

  @JsMethod
  public void deleteAllEmissionSources() {
    if (Window.confirm(i18n.esButtonDeleteAllSourcesWarning())) {
      eventBus.fireEvent(new DeleteAllEmissionSourceCommand());
    }
  }

  @JsMethod
  public void duplicateSource() {
    eventBus.fireEvent(new EmissionSourceDuplicateSelectedCommand());
  }

  @JsMethod
  public void deleteSource() {
    if (roadNetworkSelected) {
      eventBus.fireEvent(new DeleteAllRoadNetworkSourcesCommand());
    } else {
      eventBus.fireEvent(new EmissionSourceDeleteSelectedCommand());
    }
  }

  @JsMethod
  public void editSource() {
    eventBus.fireEvent(new EmissionSourceEditSelectedCommand());
  }

  @JsMethod
  public void selectSource(final EmissionSourceFeature source) {
    eventBus.fireEvent(new EmissionSourceFeatureToggleSelectCommand(source, false));
  }

  @JsMethod
  public void multiSelectSource(final EmissionSourceFeature source) {
    eventBus.fireEvent(new EmissionSourceFeatureToggleSelectCommand(source, true));
  }

  @JsMethod
  public void peekSource(final EmissionSourceFeature source) {
    eventBus.fireEvent(new EmissionSourcePeekFeatureCommand(source));
  }

  @JsMethod
  public void unpeekSource(final EmissionSourceFeature source) {
    eventBus.fireEvent(new EmissionSourceUnpeekFeatureCommand(source));
  }

  @JsMethod
  public void focusSource(final EmissionSourceFeature source) {
    eventBus.fireEvent(new FeatureZoomCommand(source));
  }

  @JsMethod
  public void clickSource(final EmissionSourceFeature source, final MouseEvent ev) {
    if (ev.ctrlKey) {
      multiSelectSource(source);
    } else {
      doubleClickUtil.click(source, this::selectSource, this::focusSource);
    }
  }

  @Computed("totalNOXEmission")
  public String getTotalNOXEmission() {
    return MessageFormatter.formatEmissionWithUnitSmart(situation.getTotalEmission(Substance.NOX));
  }

  @Computed("totalNH3Emission")
  public String getTotalNH3Emission() {
    return MessageFormatter.formatEmissionWithUnitSmart(situation.getTotalEmission(Substance.NH3));
  }
}
