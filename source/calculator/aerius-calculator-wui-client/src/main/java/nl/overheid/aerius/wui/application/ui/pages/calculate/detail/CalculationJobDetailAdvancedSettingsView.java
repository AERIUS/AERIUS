/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.calculate.detail;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.RoadLocalFractionNO2Option;
import nl.overheid.aerius.wui.application.components.divider.DividerComponent;
import nl.overheid.aerius.wui.application.components.source.DetailStatComponent;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    DividerComponent.class,
    DetailStatComponent.class
})
public class CalculationJobDetailAdvancedSettingsView extends BasicVueComponent {
  private static final int ADVANCED_SETTINGS_DECIMALS = 2;

  @Prop CalculationSetOptions calculationSetOptions;
  @Prop Theme theme;

  @Computed("obukhovLengthValue")
  public String getObukhovLengthValue() {
    return i18n.unitM(i18n.decimalNumberFixed(calculationSetOptions.getNcaCalculationOptions().getAdmsOptions().getMinMoninObukhovLength(),
        ADVANCED_SETTINGS_DECIMALS));
  }

  @Computed("surfaceAlbedoValue")
  public String getSurfaceAlbedoValue() {
    return i18n.decimalNumberFixed(calculationSetOptions.getNcaCalculationOptions().getAdmsOptions().getSurfaceAlbedo(), ADVANCED_SETTINGS_DECIMALS);
  }

  @Computed("priestleyTaylorParameterValue")
  public String getPriestleyTaylorParameterValue() {
    return i18n.decimalNumberFixed(calculationSetOptions.getNcaCalculationOptions().getAdmsOptions().getPriestleyTaylorParameter(),
        ADVANCED_SETTINGS_DECIMALS);
  }

  @Computed("plumeDepletionNH3Label")
  public String getPlumeDepletionNH3Label() {
    return i18n.calculatePlumeDepletionLabel() + " (" + i18n.pollutant(Substance.NH3) + ")";
  }

  @Computed("plumeDepletionNH3Value")
  public String getPlumeDepletionNH3Value() {
    return i18n.booleanText(calculationSetOptions.getNcaCalculationOptions().getAdmsOptions().isPlumeDepletionNH3());
  }

  @Computed("plumeDepletionNOxLabel")
  public String getPlumeDepletionNOxLabel() {
    return i18n.calculatePlumeDepletionLabel() + " (" + i18n.pollutant(Substance.NOX) + ")";
  }

  @Computed("plumeDepletionNOxValue")
  public String getPlumeDepletionNOxValue() {
    return i18n.booleanText(calculationSetOptions.getNcaCalculationOptions().getAdmsOptions().isPlumeDepletionNOX());
  }

  @Computed("complexTerrainValue")
  public String getComplexTerrainValue() {
    return i18n.booleanText(calculationSetOptions.getNcaCalculationOptions().getAdmsOptions().isComplexTerrain());
  }

  @Computed("primaryfNO2ReceptorPointsValue")
  public String getPrimaryfNO2ReceptorPointsValue() {
    return i18n.calculatePrimaryfNO2Option(calculationSetOptions.getNcaCalculationOptions().getRoadLocalFractionNO2ReceptorsOption());
  }

  @Computed("primaryfNO2PointsValue")
  public String getPrimaryfNO2PointsValue() {
    return i18n.calculatePrimaryfNO2Option(calculationSetOptions.getNcaCalculationOptions().getRoadLocalFractionNO2PointsOption());
  }

  @Computed("roadLocalFractionNO2Visible")
  public boolean isRoadLocalFractionNO2Enabled() {
    return RoadLocalFractionNO2Option.ONE_CUSTOM_VALUE == calculationSetOptions.getNcaCalculationOptions().getRoadLocalFractionNO2ReceptorsOption()
        || RoadLocalFractionNO2Option.ONE_CUSTOM_VALUE == calculationSetOptions.getNcaCalculationOptions().getRoadLocalFractionNO2PointsOption();
  }

  @Computed("roadLocalFractionNO2")
  public String getRoadLocalFractionNO2() {
    return i18n.decimalNumberFixed(calculationSetOptions.getNcaCalculationOptions().getRoadLocalFractionNO2(), ADVANCED_SETTINGS_DECIMALS);
  }

}
