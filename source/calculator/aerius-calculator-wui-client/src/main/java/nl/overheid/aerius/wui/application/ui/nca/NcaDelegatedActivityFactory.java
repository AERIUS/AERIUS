/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.nca;

import nl.overheid.aerius.wui.application.place.BuildingPlace;
import nl.overheid.aerius.wui.application.place.CalculatePlace;
import nl.overheid.aerius.wui.application.place.CalculationPointsPlace;
import nl.overheid.aerius.wui.application.place.TimeVaryingProfilePlace;
import nl.overheid.aerius.wui.application.place.EmissionSourcePlace;
import nl.overheid.aerius.wui.application.place.ExportPlace;
import nl.overheid.aerius.wui.application.place.HomePlace;
import nl.overheid.aerius.wui.application.place.NextStepsPlace;
import nl.overheid.aerius.wui.application.place.ResultsPlace;
import nl.overheid.aerius.wui.application.place.ScenarioInputListPlace;
import nl.overheid.aerius.wui.application.ui.nca.pages.next.NextStepsActivity;
import nl.overheid.aerius.wui.application.ui.nca.pages.timevarying.TimeVaryingProfileActivity;
import nl.overheid.aerius.wui.application.ui.pages.building.BuildingActivity;
import nl.overheid.aerius.wui.application.ui.pages.calculate.CalculateActivity;
import nl.overheid.aerius.wui.application.ui.pages.calculationpoints.CalculationPointsActivity;
import nl.overheid.aerius.wui.application.ui.pages.emissionsource.EmissionSourceActivity;
import nl.overheid.aerius.wui.application.ui.pages.emissionsourceList.ScenarioInputListActivity;
import nl.overheid.aerius.wui.application.ui.pages.export.ExportActivity;
import nl.overheid.aerius.wui.application.ui.pages.launch.HomeActivity;
import nl.overheid.aerius.wui.application.ui.pages.results.ResultsActivity;

public interface NcaDelegatedActivityFactory {

  BuildingActivity create(BuildingPlace place);

  CalculateActivity create(CalculatePlace place);

  ScenarioInputListActivity create(ScenarioInputListPlace place);

  EmissionSourceActivity create(EmissionSourcePlace place);

  ResultsActivity create(ResultsPlace place);

  HomeActivity create(HomePlace place);

  ExportActivity create(ExportPlace place);

  CalculationPointsActivity create(CalculationPointsPlace place);

  TimeVaryingProfileActivity create(TimeVaryingProfilePlace place);

  NextStepsActivity create(NextStepsPlace place);
}
