/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.nca;

import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.ProductProfile;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.wui.application.components.nav.NavigationItem;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.place.NextStepsPlace;
import nl.overheid.aerius.wui.application.ui.pages.ThemeActivity;

public class NcaThemeActivity extends ThemeActivity<NcaDelegatedActivityManager> {
  @Inject
  public NcaThemeActivity(final NcaDelegatedActivityManager delegator) {
    super(delegator);
  }

  @Override
  public void initNavigation(final Theme theme, final ProductProfile productProfile) {
    super.initNavigation(theme, productProfile);

    navigationContext.addTopItem(NavigationItem.createPlaceItem(M.messages().menuNext(), "icon-menu-next",
        placeController, new NextStepsPlace(theme)));
  }

  @Override
  protected void addLanguage() {
    // Do nothing
  }
}
