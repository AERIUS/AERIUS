/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.i18n;

import com.google.gwt.i18n.client.Messages.Select;

import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfileType;
import nl.overheid.aerius.wui.application.components.source.characteristics.timevarying.TimeVaryingProfileSelectorComponent.TimeVaryingProfileOption;
import nl.overheid.aerius.wui.application.components.source.characteristics.timevarying.TimeVaryingProfileSelectorComponent.TimeVaryingProfileViewMode;
import nl.overheid.aerius.wui.application.ui.nca.pages.timevarying.TimeVaryingProfileView.InputMode;
import nl.overheid.aerius.wui.application.ui.nca.pages.timevarying.raw.TimeVaryingProfileInterpretException.Reason;

/**
 * Text specific for time-varying profile pages.
 */
public interface TimeVaryingProfileMessages {
  String esTimeVaryingProfiles();

  String timeVaryingProfileTitle(@Select CustomTimeVaryingProfileType type);
  String timeVaryingProfileTitleEdit(String type);

  String timeVaryingProfilePanelTitle(@Select CustomTimeVaryingProfileType type);

  String timeVaryingProfileRemoveConfirm(String timeVaryingProfileId, String timeVaryingProfileLabel, String srcId, String srcLabel);
  String timeVaryingProfileRemoveMultipleConfirm(String timeVaryingProfileId, String timeVaryingProfileLabel, int amount);

  String timeVaryingProfileButtonDeleteAll();
  String timeVaryingProfileButtonDeleteAllWarning();

  String timeVaryingProfileInputMode(@Select InputMode mode);

  String timeVaryingProfileAllValuesValidError();

  String timeVaryingProfileImportSummary(String total, String max);
  String timeVaryingProfileTotalSummaryError(String total, String max);
  String timeVaryingProfileTotalSummaryWarning(String total, String max);

  String timeVaryingProfileTotalWeekday();
  String timeVaryingProfileTotalSaturday();
  String timeVaryingProfileTotalSunday();
  String timeVaryingProfileAverageWeekday();
  String timeVaryingProfileAverageSaturday();
  String timeVaryingProfileAverageSunday();
  String timeVaryingProfileTotal();
  String timeVaryingProfileAverage();

  String timeVaryingProfileNameLabel();
  String timeVaryingProfileTypeLabel();
  String timeVaryingProfileTypeValue(@Select boolean systemProfile);
  String timeVaryingProfileSectorDefaultLabel(String name);
  String timeVaryingProfileSystemProfileNotice();

  String timeVaryingProfileTimeOfDay();
  String timeVaryingProfileWeekDay();
  String timeVaryingProfileSaturday();
  String timeVaryingProfileSunday();
  String timeVaryingProfileLegendTotal();

  String timeVaryingProfileRawInputPasteExplain();

  String timeVaryingProfileImportError(@Select Reason reason, String text);
  String timeVaryingProfileImportErrorOr(int one, int two);

  String timeVaryingProfileImportWellFormed();
  String timeVaryingProfileImportText();
  String timeVaryingProfileExportText();

  String timeVaryingProfileMayStop();
  String timeVaryingProfileMayCancel();

  String timeVaryingProfiles(@Select TimeVaryingProfileViewMode mode);
  String timeVaryingProfileCustomNoProfiles();

  String timeVaryingProfileStandardLabel(@Select TimeVaryingProfileOption option);
  String timeVaryingProfileStandardOption(@Select TimeVaryingProfileOption option);

  String timeVaryingProfileMissingError();
}
