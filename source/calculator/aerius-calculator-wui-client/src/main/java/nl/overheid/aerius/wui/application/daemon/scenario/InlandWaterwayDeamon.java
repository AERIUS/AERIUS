/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.scenario;

import java.util.stream.Collectors;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.geom.Geometry;

import elemental2.core.JsArray;

import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.util.NotificationUtil;
import nl.overheid.aerius.wui.application.command.DetermineWaterwayCommand;
import nl.overheid.aerius.wui.application.domain.source.shipping.inland.InlandWaterway;
import nl.overheid.aerius.wui.application.event.WaterwayResultsEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.service.AeriusRequestCallback;
import nl.overheid.aerius.wui.application.service.SourceServiceAsync;

@Singleton
public class InlandWaterwayDeamon extends BasicEventComponent implements Daemon {

  private static final InlandWaterwayDeamonEventBinder EVENT_BINDER = GWT.create(InlandWaterwayDeamonEventBinder.class);

  interface InlandWaterwayDeamonEventBinder extends EventBinder<InlandWaterwayDeamon> {
  }

  @Inject private SourceServiceAsync sourceServiceAsync;

  @EventHandler
  public void onDetermineWaterwayCommand(final DetermineWaterwayCommand c) {
    final Geometry geometry = c.getValue();
    sourceServiceAsync.suggestInlandShippingWaterway(geometry,
        AeriusRequestCallback.create(
            this::updateWaterwayResults,
            e -> NotificationUtil.broadcastError(eventBus, e)));
  }

  private void updateWaterwayResults(final JsArray<InlandWaterway> waterways) {
    if (waterways.length == 0) {
      NotificationUtil.broadcastMessage(eventBus, M.messages().sourceShipWaterwayNotDetermined());
    } else {
      final InlandWaterway bestFitWaterway = waterways.getAt(0);
      if (waterways.length > 1) {
        final String otherWaterWaycodes = waterways.asList()
            .stream()
            .skip(1)
            .map(InlandWaterway::getWaterwayCode)
            .collect(Collectors.joining(" "));
        NotificationUtil.broadcastMessage(eventBus,
            M.messages().sourceShipWaterwayInconclusiveRoute(bestFitWaterway.getWaterwayCode(), otherWaterWaycodes));
      }
      eventBus.fireEvent(new WaterwayResultsEvent(bestFitWaterway));
    }
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }

}
