/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.info;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.EventBus;

import elemental2.core.JsArray;
import elemental2.dom.ClientRect;
import elemental2.dom.HTMLElement;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.wui.application.command.geo.HabitatTypeHoverActiveCommand;
import nl.overheid.aerius.wui.application.command.geo.HabitatTypeHoverInactiveCommand;
import nl.overheid.aerius.wui.application.command.geo.HabitatTypeSelectToggleCommand;
import nl.overheid.aerius.wui.application.components.info.habitat.HabitatInfoPanel;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.result.ResultSelectionContext;
import nl.overheid.aerius.wui.application.domain.info.HabitatInfo;
import nl.overheid.aerius.wui.application.domain.info.Natura2000Info;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    BasicInfoRowComponent.class,
    HabitatInfoPanel.class
})
public class AreaInfoComponent extends BasicVueComponent {

  private static final double POPUP_FROM_BOTTOM = 270.0;

  @Prop EventBus eventBus;
  @Prop @JsProperty Natura2000Info natura2000Info;
  @Data boolean habitatTypePopupShowing;
  @Data HTMLElement selectedHabitatTypeElement;
  @Data HabitatInfo selectedHabitatInfo;

  @Inject ApplicationContext appContext;
  @Inject @Data ResultSelectionContext context;

  @Ref @JsProperty JsArray<HTMLElement> habitatTypeList;
  @Ref HabitatInfoPanel habitatTypePopup;

  @Computed
  public String getPopupTop() {
    final ClientRect rect = selectedHabitatTypeElement.getBoundingClientRect();
    selectedHabitatTypeElement.parentElement.getBoundingClientRect();
    final double clientHeight = Window.getClientHeight();
    final double top = Math.min(rect.top, clientHeight - POPUP_FROM_BOTTOM);
    return top + "px";
  }

  @Computed
  public String getPopupLeft() {
    final ClientRect rect = selectedHabitatTypeElement.getBoundingClientRect();
    return rect.left < 405 ? rect.left + 405.0 + "px" : rect.left - 355.0 + "px";
  }

  @Computed("showAllFields")
  public boolean isShowAllFields() {
    return !appContext.isAppFlagEnabled(AppFlag.REDUCED_NATURE_METADATA_FIELDS);
  }

  @JsMethod
  public boolean isSelected(final HabitatInfo info) {
    return context.isSelectedHabitatTypeCode(String.valueOf(info.getId()));
  }

  @JsMethod
  public void habitatRowToggle(final HabitatInfo row) {
    final String habitatId = String.valueOf(row.getId());
    eventBus.fireEvent(new HabitatTypeSelectToggleCommand(habitatId, false));
  }

  @JsMethod
  public void showHabitatInfoPanel(final boolean show, final int index, final HabitatInfo habitatInfo) {
    final String habitatId = String.valueOf(habitatInfo.getId());

    if (show) {
      selectedHabitatInfo = habitatInfo;
      selectedHabitatTypeElement = habitatTypeList.getAt(index);
      habitatTypePopupShowing = true;
      eventBus.fireEvent(new HabitatTypeHoverActiveCommand(habitatId, false));
    } else {
      selectedHabitatInfo = null;
      selectedHabitatTypeElement = null;
      habitatTypePopupShowing = false;
      eventBus.fireEvent(new HabitatTypeHoverInactiveCommand(habitatId));
    }
  }

}
