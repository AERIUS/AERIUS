/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source;

import java.util.HashMap;
import java.util.Map;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;

@Component
public abstract class SubSourceEditorComponent extends ErrorWarningValidator {
  @Data public Integer selectedIndex = -1;

  @Data @JsProperty Map<Integer, Boolean> subSourceErrors = new HashMap<>();
  @Data @JsProperty Map<Integer, Boolean> subSourceWarnings = new HashMap<>();

  @JsMethod
  public boolean hasErrors(final int index) {
    return subSourceErrors.containsKey(index) ? getBoolean(subSourceErrors.get(index)) : false;
  }

  public boolean hasErrors() {
    return subSourceErrors.values().stream().anyMatch(Boolean.TRUE::equals);
  }

  private boolean getBoolean(final Boolean bool) {
    return Boolean.TRUE.equals(bool);
  }

  @JsMethod
  public boolean hasWarnings(final int index) {
    return subSourceWarnings.containsKey(index) ? getBoolean(subSourceWarnings.get(index)) : false;
  }

  @JsMethod
  public boolean isSelected(final Number idx) {
    return selectedIndex.equals(idx.intValue());
  }

  @JsMethod
  public boolean selected() {
    return !selectedIndex.equals(-1);
  }

  public void editSource() {
    vue().$emit("editSource", selectedIndex);
  }

  public void resetSelected() {
    selectedIndex = -1;
    vue().$emit("resetSelected");
  }

  @JsMethod
  public void setSubSourceWarnings(final Map<Integer, Boolean> subSourceWarnings) {
    this.subSourceWarnings = subSourceWarnings;
  }

  @JsMethod
  public void setSubSourceWarning(final int idx, final boolean value) {
    this.subSourceWarnings.put(idx, value);
  }

  @JsMethod
  public void setSubSourceErrors(final Map<Integer, Boolean> subSourceErrors) {
    this.subSourceErrors = subSourceErrors;
  }

  @JsMethod
  public void setSubSourceError(final int idx, final boolean value) {
    this.subSourceErrors.put(idx, value);
  }
}
