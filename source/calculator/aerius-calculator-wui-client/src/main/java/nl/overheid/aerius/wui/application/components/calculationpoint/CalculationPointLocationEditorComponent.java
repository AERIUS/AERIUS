/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.calculationpoint;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasBeforeDestroy;
import com.google.web.bindery.event.shared.EventBus;

import ol.geom.Geometry;
import ol.geom.GeometryType;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointDrawCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.MinimalInputComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.components.tooltip.TooltipComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;
import nl.overheid.aerius.wui.application.event.calculationpoint.CalculationPointChangeEvent;
import nl.overheid.aerius.wui.application.geo.util.GeoType;
import nl.overheid.aerius.wui.application.ui.pages.emissionsource.SmartWktInputComponent;

@Component(components = {
    ButtonIcon.class,
    TooltipComponent.class,
    MinimalInputComponent.class,
    ValidationBehaviour.class,
    SmartWktInputComponent.class,
    InputErrorComponent.class,
})
public class CalculationPointLocationEditorComponent extends ErrorWarningValidator implements HasBeforeDestroy {
  @Prop CalculationPointFeature calculationPoint;
  @Prop EventBus eventBus;
  @Prop boolean open;
  @Prop boolean showErrors;

  @Data Geometry drawingGeometry = null;

  @Data @JsProperty List<GeoType> validGeometryTypes = Arrays.asList(GeoType.POINT);

  @Inject @Data ApplicationContext appContext;

  @Override
  public void beforeDestroy() {
    drawingGeometry = null;
    eventBus.fireEvent(new CalculationPointDrawCommand(null));
  }

  @Watch(value = "open", isImmediate = true)
  public void onOpenChange(final boolean open) {
    if (open) {
      eventBus.fireEvent(new CalculationPointDrawCommand(geometry -> saveGeometry(geometry)));
    } else {
      eventBus.fireEvent(new CalculationPointDrawCommand(null));
    }
  }

  @JsMethod
  public void addPoint() {
    eventBus.fireEvent(new CalculationPointDrawCommand(geometry -> saveGeometry(geometry)));
  }

  @Computed("isInvalidWktGeometry")
  public boolean getIsInvalidWktGeometry() {
    return !GeoType.POINT.is(getGeometry());
  }

  @Computed
  public Geometry getGeometry() {
    return Optional.ofNullable(drawingGeometry)
        .orElse(calculationPoint.getGeometry());
  }

  @JsMethod
  public void saveGeometry(final Geometry geometry) {
    drawingGeometry = geometry;

    calculationPoint.setGeometry(geometry);
    vue().$emit("calculationPointLocationUpdated", geometry);
    eventBus.fireEvent(new CalculationPointChangeEvent(calculationPoint));
  }

  @Computed
  public Supplier<String> getGeometryInvalidErrorMessage() {
    return () -> i18n.geometryInvalid(appContext.getTheme(), GeometryType.Point);
  }
}
