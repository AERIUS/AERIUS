/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.i18n;

import com.google.gwt.i18n.client.Messages.Select;

import nl.overheid.aerius.shared.domain.ops.DiurnalVariation;
import nl.overheid.aerius.shared.domain.ops.HeatContentType;
import nl.overheid.aerius.shared.domain.ops.OutflowDirectionType;

/**
 * Text specific for OPS characteristics.
 */
public interface SourceCharacteristicsMessages {
  String sourceCharacteristicsTitle();
  String sourceHeatContentTypeLabel();
  String sourceHeatContentLabel();
  String sourceEstablished();
  String sourceBuildingInfluenceLabel();
  String sourceBuildingEmptyLabel();
  String sourceBuildingSelectionLabel();
  String sourceBuildingSelectionPlaceholder();
  String sourceBuildingSelectionCreateNew();
  String sourceTimeVaryingProfileSelectionCreateNew();
  String sourceEmissionHeightLabel();
  String sourceEmissionTemperatureLabel();
  String sourceOutflowDiameterLabel();
  String sourceOutflowDirectionLabel();
  String sourceOutflowVelocityLabel();
  String sourceSpreadLabel();
  String sourceDiurnalVariationLabel();
  String sourceParticleSizeDistributionLabel();

  String sourceCalculationValue();

  String sourceHeatContentType();

  String heatContentType(@Select HeatContentType type);
  String sourceNoBuildingInFluence();
  String sourceOutflowDirectionType(@Select OutflowDirectionType type);
  String sourceDiurnalVariation(@Select DiurnalVariation type);
}
