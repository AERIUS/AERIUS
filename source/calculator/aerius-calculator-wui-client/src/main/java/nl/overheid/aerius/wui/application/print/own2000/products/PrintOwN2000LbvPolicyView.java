/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.print.own2000.products;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.wui.application.components.notification.NotificationPanelComponent;
import nl.overheid.aerius.wui.application.context.PrintContext;
import nl.overheid.aerius.wui.application.print.own2000.EmissionSources;
import nl.overheid.aerius.wui.application.print.own2000.Overview;
import nl.overheid.aerius.wui.application.print.own2000.ProjectCalculationCoverPage;
import nl.overheid.aerius.wui.application.print.own2000.SituationSummaries;
import nl.overheid.aerius.wui.application.print.own2000.results.ProcurementPolicyResultsComponent;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component(components = {
    ProjectCalculationCoverPage.class,
    Overview.class,
    ProcurementPolicyResultsComponent.class,
    SituationSummaries.class,
    EmissionSources.class,
    NotificationPanelComponent.class
})
public class PrintOwN2000LbvPolicyView extends BasicVueView {
  @Prop EventBus eventBus;
  @Inject @Data PrintContext context;
}
