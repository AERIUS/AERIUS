/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.input;

import java.util.function.Supplier;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import elemental2.dom.HTMLElement;

import nl.aerius.vuelidate.Validations;
import nl.aerius.wui.vue.transition.VerticalCollapse;

@Component(components = {
    VerticalCollapse.class,
    InputErrorComponent.class,
    InputWarningComponent.class,
})
public class MinimalInputComponent implements IsVueComponent {
  /**
   * Whether to include input warning|error styles (default: yes)
   */
  @Prop boolean inputStyles;
  /**
   * Override for showing warning status.
   */
  @Prop boolean showWarning;
  /**
   * Validations object bound to the input. Called to check for errors.
   */
  @Prop Validations errorValidation;
  /**
   * Override for showing error status.
   */
  @Prop boolean showError;
  /**
   * Supplier that should return a customized warning message in case of an
   * validation error.
   */
  @Prop Supplier<String> warningMessage;
  /**
   * Supplier that should return a customized error message in case of an
   * validation error.
   */
  @Prop Supplier<String> errorMessage;

  @Ref HTMLElement inputContainer;

  @PropDefault("inputStyles")
  boolean inputStylesDefault() {
    return true;
  }

  @PropDefault("showWarning")
  boolean showWarningDefault() {
    return false;
  }

  @Watch("showWarning")
  public void onShowWarningChange(final boolean showError) {
    InputUtil.reportNotice(inputContainer, showError, false);
  }

  @PropDefault("showError")
  boolean showErrorDefault() {
    return false;
  }

  @Computed("showError")
  boolean showErrorComputed() {
    return isError() || showError;
  }

  @Watch("showErrorComputed()")
  public void onShowErrorChange(final boolean showError) {
    InputUtil.reportNotice(inputContainer, showError, true);
  }

  @Computed("isError")
  boolean isError() {
    return errorValidation != null && errorValidation.error;
  }
}
