/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.calculationpoint;

import static jsinterop.annotations.JsPackage.GLOBAL;

import java.util.List;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;

/**
 * Represents the result of calculation points determined by Connect.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class CalculationPointPlacementResult {

  private @JsProperty List<CalculationPointFeature> computedCalculationPoints;

  private int assessmentAreas;

  public final @JsOverlay List<CalculationPointFeature> getComputedCalculationPoints() {
    return computedCalculationPoints;
  }

  public final @JsOverlay void setComputedCalculationPoints(final List<CalculationPointFeature> computedCalculationPoints) {
    this.computedCalculationPoints = computedCalculationPoints;
  }

  public final @JsOverlay int getAssessmentAreas() {
    return assessmentAreas;
  }

  public final @JsOverlay void setAssessmentAreas(final int assessmentAreas) {
    this.assessmentAreas = assessmentAreas;
  }
}
