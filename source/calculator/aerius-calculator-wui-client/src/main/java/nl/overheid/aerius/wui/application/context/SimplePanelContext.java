/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import jsinterop.annotations.JsMethod;

public class SimplePanelContext implements PanelContext {
  boolean showing = false;
  boolean active = false;

  @Override
  public void setPanelShowing(final boolean showing) {
    this.showing = showing;
  }

  @Override
  public void setPanelActive(final boolean active) {
    this.active = active;
  }

  public boolean togglePanelShowing(final boolean attached) {
    final boolean oldShowing = showing;
    if (showing) {
      if (active || !attached) {
        this.showing = false;
        this.active = false;
      } else {
        // If attached and showing, but inactive: activate
        this.active = true;
      }
    } else {
      this.showing = true;
      this.active = true;
    }

    return !oldShowing && showing;
  }

  @Override
  @JsMethod
  public boolean isShowing() {
    return showing;
  }

  @Override
  @JsMethod
  public boolean isActive() {
    return active;
  }
}
