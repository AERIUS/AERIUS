/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.base;

import java.util.function.DoubleSupplier;

import ol.Feature;
import ol.style.Style;
import ol.style.StyleFunction;

import nl.overheid.aerius.wui.application.domain.ImaerFeature;
import nl.overheid.aerius.wui.application.domain.ItemWithStringIdUtil;
import nl.overheid.aerius.wui.application.geo.layers.FeatureType;
import nl.overheid.aerius.wui.application.geo.layers.LabelFeature;
import nl.overheid.aerius.wui.application.geo.layers.LabelStyle;
import nl.overheid.aerius.wui.application.geo.layers.LayerStyleUtil;

/**
 * Creates the styling for a marker layer. The markers on the marker layer are features converted to LabelFeature objects.
 */
public class LabelStyleCreator<F extends ImaerFeature> {

  public static final Style[] NO_RENDERING = new Style[] {};

  private final DoubleSupplier iconScale;
  private final boolean hasOutline;
  /**
   * Label style
   */
  private final LabelStyle labelStyle;

  /**
   * Keeps track of the feature that is being edited
   */
  private String editableFeatureId;
  /**
   * If true the name will be shown on the label, otherwise the id (number).
   */
  private boolean showNameLabels;

  /**
   * Constructor.
   *
   * @param iconScale scale of the icon
   * @param hasOutline if true the labe with be shown with an outline
   * @param labelStyle style of the label
   */
  public LabelStyleCreator(final DoubleSupplier iconScale, final boolean hasOutline, final LabelStyle labelStyle) {
    this.hasOutline = hasOutline;
    this.iconScale = iconScale;
    this.labelStyle = labelStyle;
  }

  public String getEditableFeatureId() {
    return editableFeatureId;
  }

  public void setEditableFeatureId(final Feature feature) {
    this.editableFeatureId = feature == null ? null : feature.getId();
  }

  public boolean isShowNameLabels() {
    return showNameLabels;
  }

  public void setShowNameLabels(final boolean showNameLabels) {
    this.showNameLabels = showNameLabels;
  }

  protected double getIconScale() {
    return iconScale.getAsDouble();
  }

  public boolean isHasOutline() {
    return hasOutline;
  }

  public LabelStyle getLabelStyle() {
    return labelStyle;
  }

  /**
   * @see {@link StyleFunction#createStyle(Feature, double)}
   */
  public Style[] createStyle(final ImaerFeature feature, final double resolution) {
    return LayerStyleUtil.createStyle(feature, createLabelName(feature), getLabelStyle(), hasOutline, getIconScale());
  }

  protected String createLabelName(final ImaerFeature feature) {
    final String label = feature.getId().equals(editableFeatureId) || !showNameLabels ? feature.getId() : feature.getLabel();
    return ItemWithStringIdUtil.cleanStringId(label);
  }

  public Feature createLabelFeature(final Feature feature) {
    return feature == null ? null
        : LabelFeature.create(feature.getId(), feature.getGeometry(), ((ImaerFeature) feature).getLabel(),
            getLabelStyle().getLabelBackgroundColor(feature), feature, FeatureType.OTHER, null);
  }

}
