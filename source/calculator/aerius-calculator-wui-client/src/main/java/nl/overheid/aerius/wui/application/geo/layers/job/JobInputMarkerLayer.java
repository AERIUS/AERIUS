/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.job;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.Feature;

import nl.aerius.geo.domain.LayerInfo;
import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.ApplicationLimits;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobSelectCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobToggleCommand;
import nl.overheid.aerius.wui.application.command.calculation.TemporaryCalculationJobAddCommand;
import nl.overheid.aerius.wui.application.context.MapConfigurationContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationPreparationContext;
import nl.overheid.aerius.wui.application.domain.ImaerFeature;
import nl.overheid.aerius.wui.application.domain.ItemWithStringIdUtil;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.road.RoadEmissionSourceTypes;
import nl.overheid.aerius.wui.application.event.CalculationJobCompositionChangedEvent;
import nl.overheid.aerius.wui.application.geo.layers.FeatureType;
import nl.overheid.aerius.wui.application.geo.layers.LabelFeature;
import nl.overheid.aerius.wui.application.geo.layers.LabelStyle;
import nl.overheid.aerius.wui.application.geo.layers.base.BaseClusteredMarkerLayer;
import nl.overheid.aerius.wui.application.geo.layers.base.VectorFeaturesWrapper;

/**
 * Layer to show markers of all situations combined on the map.
 * The marker will show small indicators with the type of the situation.
 * The layers will cluster markers depending on zoom level.
 */
public class JobInputMarkerLayer extends BaseClusteredMarkerLayer<LabelFeature> implements JobInputLayer {
  interface JobInputMarkerLayerEventBinder extends EventBinder<JobInputMarkerLayer> {
  }

  private static final JobInputMarkerLayerEventBinder EVENT_BINDER = GWT.create(JobInputMarkerLayerEventBinder.class);

  private final VectorFeaturesWrapper vectorObject = new VectorFeaturesWrapper();

  private final FeatureType featureType;
  private final CalculationPreparationContext context;
  private Map<String, SituationType> situationTypes = new HashMap<>();

  @Inject
  public JobInputMarkerLayer(@Assisted final LayerInfo info, @Assisted final FeatureType featureType, final EventBus eventBus,
      @Assisted final int zIndex, final MapConfigurationContext mapConfigurationContext, final CalculationPreparationContext context) {
    super(info, zIndex, new JobInputLabelStyleCreator(mapConfigurationContext::getIconScale));
    this.featureType = featureType;
    this.context = context;
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @EventHandler(handles = {CalculationJobToggleCommand.class, CalculationJobSelectCommand.class, TemporaryCalculationJobAddCommand.class,
      CalculationJobCompositionChangedEvent.class})
  void onCalculationChange() {
    SchedulerUtil.delay(() -> selectJob(context.getActiveOrTemporaryJob()));
  }

  private void selectJob(final CalculationJobContext jobContext) {
    if (jobContext == null) {
      setFeatures(vectorObject, Collections.emptyList());
      return;
    }
    situationTypes = jobContext.getSituationComposition().getSituations().stream()
        .collect(Collectors.toMap(SituationContext::getId, SituationContext::getType));
    final List<LabelFeature> collectedFeatures = jobContext.getSituationComposition().getSituations().stream()
        .flatMap(this::getSituationFeatures)
        .limit(ApplicationLimits.SOURCE_LAYER_DRAW_LIMIT)
        .collect(Collectors.toList());
    setFeatures(vectorObject, collectedFeatures);
  }

  @Override
  public void onAdd(final String situationId, final Feature feature, final FeatureType featureType) {
    addLabelFeature(situationId, feature, featureType);
  }

  @Override
  public void onUpdate(final String situationId, final Feature feature, final FeatureType featureType) {
    onDelete(situationId, feature, featureType);
    onAdd(situationId, feature, featureType);
  }

  @Override
  public void onDelete(final String situationId, final Feature feature, final FeatureType featureType) {
    vectorObject.removeFeatureById(situationFeatureKey(situationId, feature, featureType));
  }

  private void addLabelFeature(final String situationId, final Feature feature, final FeatureType featureType) {
    if (ItemWithStringIdUtil.isTemporaryFeature(feature)) {
      return;
    }

    if (situationId != null && situationTypes.containsKey(situationId)) {
      vectorObject.addFeature(createLabelFeature(situationId, feature, featureType));
    }
  }

  private Stream<LabelFeature> getSituationFeatures(final SituationContext situation) {
    switch (featureType) {
    case SOURCE:
      return situation.getSources().stream()
          .map(source -> createLabelFeature(situation.getId(), source, FeatureType.SOURCE))
          .filter(Objects::nonNull);
    case BUILDING:
      return situation.getBuildings().stream()
          .map(source -> createLabelFeature(situation.getId(), source, FeatureType.BUILDING))
          .filter(Objects::nonNull);
    case OTHER:
    default:
      return Stream.empty();
    }
  }

  private LabelFeature createLabelFeature(final String situationId, final Feature feature, final FeatureType featureType) {
    if (RoadEmissionSourceTypes.isRoadSource((EmissionSourceFeature) feature)) {
      return null;
    } else {
      final LabelStyle labelStyle = featureType == FeatureType.SOURCE ? LabelStyle.EMISSION_SOURCE : LabelStyle.BUILDING;

      return LabelFeature.create(situationFeatureKey(situationId, feature, featureType), feature.getGeometry(), ((ImaerFeature) feature).getLabel(),
          labelStyle.getLabelBackgroundColor(feature), feature, featureType, situationTypes.get(situationId));
    }
  }
}
