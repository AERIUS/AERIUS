/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source.road;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import ol.OLFactory;
import ol.style.Style;

import nl.aerius.geo.domain.legend.ColorLabelsLegend;
import nl.aerius.geo.domain.legend.LegendType;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.legend.ColorRange;
import nl.overheid.aerius.shared.domain.legend.ColorRangeType;
import nl.overheid.aerius.shared.domain.legend.ColorRangesLegend;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.domain.source.ADMSRoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.SRM2RoadESFeature;
import nl.overheid.aerius.wui.application.i18n.M;

public class RoadElevationHeightStyle implements RoadStyle {

  public static final int DEFAULT_ROAD_ELEVATION_HEIGHT_WIDTH = 6;

  private final List<ColorRange> colorRange;

  public RoadElevationHeightStyle(final Function<ColorRangeType, List<ColorRange>> colorRangeFunction) {
    // Ensure each range gets an appropriate label.
    final List<ColorRange> colorRangesWithoutLabel = colorRangeFunction.apply(ColorRangeType.ROAD_ELEVATION_HEIGHT);
    colorRange = colorRangeFunction.apply(ColorRangeType.ROAD_ELEVATION_HEIGHT).stream()
        .map(r -> new ColorRange(r.getLowerValue(), r.getColor(), determineLabel(r, colorRangesWithoutLabel)))
        .collect(Collectors.toList());
  }

  private static String determineLabel(final ColorRange colorRange, final List<ColorRange> colorRanges) {
    final int index = colorRanges.indexOf(colorRange);
    final String rangePart;
    final double lowerValue = colorRange.getLowerValue();
    if (index == colorRanges.size() - 1) {
      rangePart = "≥ " + Math.round(lowerValue);
    } else {
      if (Double.isInfinite(-lowerValue)) {
        rangePart = "< " + colorRanges.get(index + 1).getLowerValue();
      } else if (Double.doubleToLongBits(lowerValue) == 0) {
        rangePart = "0";
      } else if (Math.round(lowerValue) == 0) {
        rangePart = "> 0 " + M.messages().colorRangesLegendBetweenText() + " " + Math.round(colorRanges.get(index + 1).getLowerValue());
      } else {
        // Rounding to avoid the 0.0001 that is used for the 'exactly 0' to pop up.
        rangePart = Math.round(lowerValue) + " " + M.messages().colorRangesLegendBetweenText() + " "
            + Math.round(colorRanges.get(index + 1).getLowerValue());
      }
    }
    return determineFullLabel(lowerValue, rangePart);
  }

  private static String determineFullLabel(final double lowerValue, final String rangePart) {
    final String fullLabel;
    if (Double.doubleToLongBits(lowerValue) == 0) {
      fullLabel = M.messages().layerRoadElevationHeightZero();
    } else if (lowerValue < 0) {
      fullLabel = M.messages().layerRoadElevationHeightNegative(rangePart);
    } else {
      fullLabel = M.messages().layerRoadElevationHeightPositive(rangePart);
    }
    return fullLabel;
  }

  private Map<Double, List<Style>> getStyleMap() {
    return colorRange.stream()
        .collect(Collectors.toMap(
            cr -> (double) cr.getLowerValue(),
            cr -> Collections.singletonList(OLFactory.createStyle(OLFactory.createStroke(OLFactory.createColor(cr.getColor()),
                DEFAULT_ROAD_ELEVATION_HEIGHT_WIDTH)))));
  }

  @Override
  public List<Style> getStyle(final RoadESFeature feature, final String vehicleTypeCode, final double resolution) {
    final double emission = feature != null ? determineElevationHeight(feature) : 0D;
    final Map<Double, List<Style>> styleMap = getStyleMap();
    final double lowerValueRange = styleMap.keySet().stream()
        .mapToDouble(x -> x)
        .filter(x -> x <= emission)
        .max().orElse(0D);
    return styleMap.get(lowerValueRange);
  }

  private static double determineElevationHeight(final RoadESFeature feature) {
    if (feature.getEmissionSourceType() == EmissionSourceType.SRM2_ROAD) {
      return ((SRM2RoadESFeature) feature).getElevationHeight();
    } else if (feature.getEmissionSourceType() == EmissionSourceType.ADMS_ROAD) {
      return ((ADMSRoadESFeature) feature).getElevation();
    } else {
      return 0;
    }
  }

  @Override
  public ColorLabelsLegend getLegend(final ApplicationConfiguration appThemeConfiguration) {
    return new ColorRangesLegend(
        colorRange,
        M.messages().colorRangesLegendBetweenText(),
        LegendType.LINE);
  }
}
