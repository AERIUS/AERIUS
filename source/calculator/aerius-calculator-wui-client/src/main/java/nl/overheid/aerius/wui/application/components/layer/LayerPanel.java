/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.layer;

import java.util.ArrayList;
import java.util.Collections;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.geo.daemon.LayerContext;
import nl.aerius.geo.domain.BundledLayerItem;
import nl.aerius.geo.domain.LayerItem;
import nl.aerius.geo.domain.SimpleLayerItem;
import nl.overheid.aerius.wui.application.components.layer.item.LayerItemRadioComponent;
import nl.overheid.aerius.wui.application.components.layer.item.LayerItemRadioComponentFactory;
import nl.overheid.aerius.wui.application.components.layer.item.LayerItemSimpleComponent;
import nl.overheid.aerius.wui.application.components.layer.item.LayerItemSimpleComponentFactory;

@Component(components = {
    LayerItemRadioComponent.class,
    LayerItemSimpleComponent.class
})
public class LayerPanel implements IsVueComponent {
  @Prop EventBus eventBus;

  @Inject @Data LayerContext layerContext;

  @Computed
  public ArrayList<LayerItem> getLayers() {
    final ArrayList<LayerItem> lst = new ArrayList<>(layerContext.getLayerItems());
    Collections.reverse(lst);
    return lst;
  }

  @JsMethod
  public String getComponentType(final LayerItem prop) {
    if (prop instanceof BundledLayerItem) {
      return LayerItemRadioComponentFactory.get().getComponentTagName();
    } else if (prop instanceof SimpleLayerItem) {
      return LayerItemSimpleComponentFactory.get().getComponentTagName();
    } else {
      return "";
    }
  }
}
