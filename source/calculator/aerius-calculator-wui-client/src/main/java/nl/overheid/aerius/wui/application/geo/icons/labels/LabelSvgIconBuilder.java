/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.icons.labels;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import nl.aerius.geo.icon.DynamicSvgIcon;
import nl.overheid.aerius.wui.application.util.StringUtils;

/**
 * Class to generate a map icon using SVG an label icon style
 */
public class LabelSvgIconBuilder {

  // Settings for the text in the label
  private final String TEXT_STYLE = "font-size: 19px; font-weight: bold; font-family: " + getFontFamily() + "; white-space: nowrap;";

  // Settings for the colored textBox
  private static final int TEXT_BOX_HEIGHT = 30;
  private static final int TEXT_BOX_HORIZONTAL_PADDING = 8;

  // Settings for the border surrounding and between labels
  private static final int LABEL_BORDER_WIDTH = 3;
  private static final int LABEL_BETWEEN_BORDER_WIDTH = 3;
  private static final String LABEL_COLOR = "#0b0c0c";

  // Settings for the outline if applicable (selected or moving)
  private static final double HIGHLIGHT_OUTLINE_WIDTH = 2.5;
  private static final double HIGHLIGHT_OUTLINE_OFFSET = 2;
  private static final String HIGHLIGHT_OUTLINE_COLOR = "#157cb1";

  // Constants for computing outlines in corners
  private static final double COS_45 = 0.7071;
  private static final double TAN_22_DOT_5 = 0.4142;

  // Constants for rendering indicators
  private final String INDICATOR_TEXT_STYLE = "font-size: 9px; font-family: " + getFontFamily() + "; white-space: nowrap;";
  private static final int INDICATOR_RADIUS = 6;
  private static final int INDICATOR_INITIAL_OFFSET = 6;
  private static final int INDICATOR_DISTANCE = 13;
  private static final double INDICATOR_CENTER_Y = -(TEXT_BOX_HEIGHT + LABEL_BORDER_WIDTH) / 2.0;

  private static final int BOTTOM_POINTER_EXTRA_HEIGHT = 30;

  /**
   * Generates a map icon for a set of icons
   *
   * @param labels labels to generate the icon for
   * @param hasOutline whether the icon should have an outline
   * @param labelStyle one of the predefined label icon styles
   * @return the icon along with positioning information
   */
  public DynamicSvgIcon getSvgIcon(final List<Label> labels, final boolean hasOutline, final LabelIconStyle labelStyle) {
    final StringBuilder svg = new StringBuilder();

    final List<Integer> textWidths = labels.stream().map(m ->
    // Compute the required size of the text label
    // The textBox width is at least getTextBoxMinWidth()
    Math.max(this.calculateStringWidth(m.getLabelText()) + TEXT_BOX_HORIZONTAL_PADDING * 2, labelStyle.getTextBoxMinWidth()))
        .collect(Collectors.toList());
    final int totalTextWidths = textWidths.stream().mapToInt(i -> i + LABEL_BETWEEN_BORDER_WIDTH).sum() - LABEL_BETWEEN_BORDER_WIDTH;
    final int totalTextWidthsNoOffset = totalTextWidths - 2 * labelStyle.getTextBoxRadius();

    // Compute the boundaries of the image
    final double outerOffset = HIGHLIGHT_OUTLINE_OFFSET + HIGHLIGHT_OUTLINE_WIDTH + labelStyle.getLabelOffset(LABEL_BORDER_WIDTH);
    double viewBoxMaxX = totalTextWidthsNoOffset / 2D + outerOffset;
    double viewBoxMinX = -viewBoxMaxX;
    double viewBoxMaxY = labelStyle.getTextBoxNoOffset(TEXT_BOX_HEIGHT) / 2D + outerOffset;
    final double viewBoxMinY = -viewBoxMaxY;

    if (labelStyle.getPointerLocation() == LabelPointerLocation.TOP_LEFT || labelStyle.getPointerLocation() == LabelPointerLocation.BOTTOM_LEFT) {
      // Marker is larger on the left
      viewBoxMinX -= outerOffset / TAN_22_DOT_5 + labelStyle.getTextBoxNoOffset(TEXT_BOX_HEIGHT) - outerOffset;
    }
    if (labelStyle.getPointerLocation() == LabelPointerLocation.TOP_RIGHT || labelStyle.getPointerLocation() == LabelPointerLocation.BOTTOM_RIGHT) {
      // Marker is larger on the right
      viewBoxMaxX += outerOffset / TAN_22_DOT_5 + labelStyle.getTextBoxNoOffset(TEXT_BOX_HEIGHT) - outerOffset;
    }
    if (labelStyle.getPointerLocation() == LabelPointerLocation.BOTTOM) {
      // Marker is larger at the bottom
      viewBoxMaxY += BOTTOM_POINTER_EXTRA_HEIGHT;
    }

    final double width = viewBoxMaxX - viewBoxMinX;
    final double height = viewBoxMaxY - viewBoxMinY;

    svg.append("<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"")
        .append(viewBoxMinX).append(" ").append(viewBoxMinY).append(" ")
        .append(viewBoxMaxX - viewBoxMinX).append(" ").append(viewBoxMaxY - viewBoxMinY)
        .append("\" width=\"").append(width)
        .append("\" height=\"").append(height)
        .append("\">");

    // If the source is selected or is moving, the label gets an outline with a white background
    if (hasOutline) {
      svg.append("<path d=\"");
      appendOffsetPath(svg, labelStyle, totalTextWidthsNoOffset,
          HIGHLIGHT_OUTLINE_OFFSET + HIGHLIGHT_OUTLINE_WIDTH + labelStyle.getLabelOffset(LABEL_BORDER_WIDTH));
      svg.append("\" fill=\"#ffffff\" />");

      svg.append("<path d=\"");
      appendOffsetPath(svg, labelStyle, totalTextWidthsNoOffset,
          HIGHLIGHT_OUTLINE_OFFSET + HIGHLIGHT_OUTLINE_WIDTH / 2.0 + labelStyle.getLabelOffset(LABEL_BORDER_WIDTH));
      svg.append("\" stroke=\"").append(HIGHLIGHT_OUTLINE_COLOR).append("\" fill=\"transparent\" stroke-width=\"").append(HIGHLIGHT_OUTLINE_WIDTH)
          .append("\"");

      svg.append("/>");
    }

    // Draw the label itself
    svg.append("<path d=\"");
    appendOffsetPath(svg, labelStyle, totalTextWidthsNoOffset, labelStyle.getLabelOffset(LABEL_BORDER_WIDTH));
    svg.append("\" fill=\"" + LABEL_COLOR + "\" />");

    // Draw the textBoxes and label texts
    int i = 0;
    int offsetX = -totalTextWidths / 2;
    for (final Label label : labels) {

      final boolean leftRoundedCorners = i == 0; // The first element has rounded corners on the left
      final boolean rightRoundedCorners = i == labels.size() - 1; // The last element has rounded corners on the right

      final int textWidth = textWidths.get(i);
      drawTextBox(svg, offsetX, textWidth, label.getBackgroundColor(), labelStyle, leftRoundedCorners, rightRoundedCorners);
      drawText(svg, offsetX, textWidth, label.getLabelText(), label.getTextColor());
      drawIndicators(svg, offsetX, textWidth, labelStyle, label.getIndicators());
      offsetX += textWidth + LABEL_BETWEEN_BORDER_WIDTH;

      i++;
    }

    svg.append("</svg>");

    // Determine the anchor point of the marker (which pixel points to the actual source).
    // Because the marker now can have an outline, the anchor is not always at 0,0 anymore
    double anchorPointX = (HIGHLIGHT_OUTLINE_OFFSET + HIGHLIGHT_OUTLINE_WIDTH) / TAN_22_DOT_5;
    double anchorPointY = HIGHLIGHT_OUTLINE_OFFSET + HIGHLIGHT_OUTLINE_WIDTH;

    // Bottom pointer has the anchor on the opposite side of the label
    if (labelStyle.getPointerLocation() == LabelPointerLocation.TOP_RIGHT || labelStyle.getPointerLocation() == LabelPointerLocation.BOTTOM_RIGHT) {
      anchorPointX = width - anchorPointX;
    } else if (labelStyle.getPointerLocation() == LabelPointerLocation.BOTTOM) {
      anchorPointX = width / 2;
    }
    if (labelStyle.getPointerLocation() == LabelPointerLocation.BOTTOM_LEFT
        || labelStyle.getPointerLocation() == LabelPointerLocation.BOTTOM_RIGHT
        || labelStyle.getPointerLocation() == LabelPointerLocation.BOTTOM) {
      anchorPointY = height - anchorPointY;
    }

    return new DynamicSvgIcon(svg.toString(), new double[] {anchorPointX, anchorPointY});
  }

  /*
   * Generates a path with a certain offset around the marker
   */
  private void appendOffsetPath(final StringBuilder svg, final LabelIconStyle labelStyle, final double totalWidth, final double offset) {

    final double textBoxNoOffset = labelStyle.getTextBoxNoOffset(TEXT_BOX_HEIGHT);
    final double hWidth = totalWidth / 2D;
    final double hHeight = textBoxNoOffset / 2D;

    // start at the top right, before the rounded corner
    moveTo(svg, hWidth, -hHeight - offset);
    if (labelStyle.getPointerLocation() == LabelPointerLocation.TOP_RIGHT) {

      // top right pointer
      lineTo(svg, hWidth + offset / TAN_22_DOT_5 + textBoxNoOffset, -hHeight - offset);
      lineTo(svg, hWidth + COS_45 * offset, hHeight + COS_45 * offset);
      arcTo(svg, offset, hWidth, hHeight + offset);

    } else if (labelStyle.getPointerLocation() == LabelPointerLocation.BOTTOM_RIGHT) {

      // bottom right pointer
      arcTo(svg, offset, hWidth + COS_45 * offset, -hHeight - COS_45 * offset);
      lineTo(svg, hWidth + offset / TAN_22_DOT_5 + textBoxNoOffset, hHeight + offset);
    } else if (labelStyle.getPointerLocation() == LabelPointerLocation.BOTTOM) {
      // bottom pointer
      arcTo(svg, offset, hWidth + offset, -hHeight);
      lineTo(svg, hWidth + offset, hHeight);
      arcTo(svg, offset, hWidth + COS_45 * offset, hHeight + COS_45 * offset);
      lineTo(svg, 0, hHeight + BOTTOM_POINTER_EXTRA_HEIGHT + offset);
    } else {
      arcTo(svg, offset, hWidth + offset, -hHeight);

      // bottom right corner
      lineTo(svg, hWidth + offset, hHeight);
      arcTo(svg, offset, hWidth, hHeight + offset);
    }

    if (labelStyle.getPointerLocation() != LabelPointerLocation.BOTTOM) {
      lineTo(svg, -hWidth, hHeight + offset);
    }

    if (labelStyle.getPointerLocation() == LabelPointerLocation.BOTTOM_LEFT) {

      // bottom left pointer
      lineTo(svg, -hWidth - offset / TAN_22_DOT_5 - textBoxNoOffset, hHeight + offset);
      lineTo(svg, -hWidth - COS_45 * offset, -hHeight - COS_45 * offset);
      arcTo(svg, offset, -hWidth, -hHeight - offset);

    } else if (labelStyle.getPointerLocation() == LabelPointerLocation.TOP_LEFT) {

      // top left pointer
      arcTo(svg, offset, -hWidth - COS_45 * offset, hHeight + COS_45 * offset);
      lineTo(svg, -hWidth - offset / TAN_22_DOT_5 - textBoxNoOffset, -hHeight - offset);
    } else if (labelStyle.getPointerLocation() == LabelPointerLocation.BOTTOM) {
      // bottom pointer
      lineTo(svg, -hWidth - COS_45 * offset, hHeight + COS_45 * offset);
      arcTo(svg, offset, -hWidth - offset, hHeight);
      lineTo(svg, -hWidth - offset, -hHeight);
      arcTo(svg, offset, -hWidth, -hHeight - offset);
    } else {
      arcTo(svg, offset, -hWidth - offset, hHeight);

      // top left corner
      lineTo(svg, -hWidth - offset, -hHeight);
      arcTo(svg, offset, -hWidth, -hHeight - offset);
    }

    closePath(svg);
  }

  private static void closePath(final StringBuilder svg) {
    svg.append("Z");
  }

  private static void moveTo(final StringBuilder svg, final double x, final double y) {
    svg.append("M").append(x).append(",").append(y);
  }

  private static void lineTo(final StringBuilder svg, final double x, final double y) {
    svg.append("L").append(x).append(",").append(y);
  }

  private static void arcTo(final StringBuilder svg, final double radius, final double x, final double y) {
    svg.append("A").append(radius).append(" ").append(radius).append(",0,0,1,").append(x).append(",").append(y);
  }

  /**
   * Draws one of the text boxes of the label, with optional rounded corners
   */
  private void drawTextBox(final StringBuilder svg, final double offsetX, final int textBoxWidth, final String backgroundColor,
      final LabelIconStyle labelStyle, final boolean leftRoundedCorners, final boolean rightRoundedCorners) {
    final double offsetY = -labelStyle.getTextBoxNoOffset(TEXT_BOX_HEIGHT) / 2D;
    final double textBoxRadius = labelStyle.getTextBoxRadius();

    svg.append("<path d=\"");
    moveTo(svg, offsetX, offsetY);

    // Top left corner
    if (leftRoundedCorners) {
      arcTo(svg, textBoxRadius, offsetX + textBoxRadius, offsetY - textBoxRadius);
    } else {
      lineTo(svg, offsetX, offsetY - textBoxRadius);
    }
    lineTo(svg, offsetX + textBoxWidth - textBoxRadius, offsetY - textBoxRadius);

    // Top right corner
    if (rightRoundedCorners) {
      arcTo(svg, textBoxRadius, offsetX + textBoxWidth, offsetY);
    } else {
      lineTo(svg, offsetX + textBoxWidth, offsetY - textBoxRadius);
    }
    lineTo(svg, offsetX + textBoxWidth, offsetY - textBoxRadius * 2 + TEXT_BOX_HEIGHT);

    // Bottom right corner
    if (rightRoundedCorners) {
      arcTo(svg, textBoxRadius, offsetX + textBoxWidth - textBoxRadius, offsetY - textBoxRadius + TEXT_BOX_HEIGHT);
    } else {
      lineTo(svg, offsetX + textBoxWidth, offsetY - textBoxRadius + TEXT_BOX_HEIGHT);
    }
    lineTo(svg, offsetX + textBoxRadius, offsetY - textBoxRadius + TEXT_BOX_HEIGHT);

    // Bottom left corner
    if (leftRoundedCorners) {
      arcTo(svg, textBoxRadius, offsetX, offsetY - textBoxRadius * 2 + TEXT_BOX_HEIGHT);
    } else {
      lineTo(svg, offsetX, offsetY - textBoxRadius + TEXT_BOX_HEIGHT);
    }
    closePath(svg);
    svg.append("\" fill=\"").append(backgroundColor).append("\" />");
  }

  /**
   * Draws the main text label
   */
  private void drawText(final StringBuilder svg, final int xOffset, final int textBoxWidth, final String label, final String labelColor) {
    svg.append("<text");
    svg.append(" text-anchor=\"middle\" ");
    svg.append(" dominant-baseline=\"central\" ");
    svg.append(" style=\"").append(TEXT_STYLE).append("\"");
    svg.append(" x=\"").append(xOffset + textBoxWidth / 2.0).append("\"");
    svg.append(" y=\"").append(0).append("\" ");
    svg.append(" fill=\"").append(labelColor).append("\"");
    svg.append(">").append(StringUtils.encodeHtmlEntities(label)).append("</text>");
  }

  /**
   * Draws a small round indicator with 1 character
   */
  private void drawIndicators(final StringBuilder svg, final int xOffset, final int textBoxWidth, final LabelIconStyle labelStyle,
      final Character[] indicators) {

    final boolean leftToRight = labelStyle.getPointerLocation() != LabelPointerLocation.TOP_LEFT
        && labelStyle.getPointerLocation() != LabelPointerLocation.BOTTOM_LEFT;

    // reverse indicator order if right to left
    final List<Character> indicatorList = Arrays.asList(indicators);
    if (!leftToRight) {
      Collections.reverse(indicatorList);
    }

    int indicatorCtr = 0;
    for (final Character indicator : indicatorList) {
      final int indicatorOffsetX = INDICATOR_INITIAL_OFFSET + indicatorCtr++ * INDICATOR_DISTANCE;
      int indicatorCenterX;

      // whether to align the indicators left or right
      if (leftToRight) {
        indicatorCenterX = xOffset + indicatorOffsetX;
      } else {
        indicatorCenterX = xOffset + textBoxWidth - indicatorOffsetX;
      }

      svg.append("<circle")
          .append(" cx=\"").append(indicatorCenterX).append("\"")
          .append(" cy=\"").append(INDICATOR_CENTER_Y).append("\"")
          .append(" r=\"").append(INDICATOR_RADIUS).append("\"")
          .append(" fill=\"").append(LABEL_COLOR).append("\"")
          .append("/>");

      svg.append("<text")
          .append(" text-anchor=\"middle\" ")
          .append(" dominant-baseline=\"central\" ")
          .append(" style=\"").append(INDICATOR_TEXT_STYLE).append("\"")
          .append(" x=\"").append(indicatorCenterX).append("\"")
          .append(" y=\"").append(INDICATOR_CENTER_Y).append("\"")
          .append(" fill=\"").append("white").append("\"")
          .append(">").append(indicator).append("</text>");
    }
  }

  /**
   * In order to determine the width of a certain string precisely you have to add it to the DOM.
   * Otherwise the width will remain 0.
   * Only works in the DOM context.
   *
   * @param text text to calculate width for
   * @return width in px
   */
  protected int calculateStringWidth(final String text) {
    return calculateStringWidthNative(text, TEXT_STYLE);
  }

  private static native int calculateStringWidthNative(final String text, final String textStyle) /*-{
      var svg = document.createElement('svg');
      var textElement = document.createElement("text");
      textElement.setAttribute("style", textStyle);
      textElement.innerText = text;
      svg.appendChild(textElement);
      document.body.appendChild(svg);
      var width = textElement.getBoundingClientRect().width;
      document.body.removeChild(svg);
      return width;
  }-*/;

  /**
   * Returns the font family to use for the map labels.
   * Only works in the DOM context.
   *
   * @return font family
   */
  protected String getFontFamily() {
    return getFontFamilyNative().replace('\"', '\'');
  }

  private static native String getFontFamilyNative() /*-{
      return getComputedStyle(parent.document.body).getPropertyValue("font-family")
  }-*/;

}
