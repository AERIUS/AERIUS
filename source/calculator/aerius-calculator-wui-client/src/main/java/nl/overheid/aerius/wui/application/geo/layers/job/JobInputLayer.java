/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.job;

import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.Feature;

import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.event.building.BuildingAddedEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingDeletedEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingUpdatedEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceAddedEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceDeletedEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceUpdatedEvent;
import nl.overheid.aerius.wui.application.geo.layers.FeatureType;

/**
 * Interface for the JobInput layers.
 * Uses default methods to register the event handlers to listen for changes with {@link EmissionSourceFeature} and {@link BuildingFeature} changes.
 */
interface JobInputLayer {

  String SITUATION_FEATURE_KEY_SEPARATOR = "_";

  @EventHandler
  default void onEmissionSourceAddedEvent(final EmissionSourceAddedEvent e) {
    onAdd(e.getSituation().getId(), e.getValue(), FeatureType.SOURCE);
  }

  @EventHandler
  default void onEmissionSourceUpdatedEvent(final EmissionSourceUpdatedEvent e) {
    onUpdate(e.getSituation().getId(), e.getValue(), FeatureType.SOURCE);
  }

  @EventHandler
  default void onEmissionSourceDeletedEvent(final EmissionSourceDeletedEvent e) {
    onDelete(e.getSituation().getId(), e.getValue(), FeatureType.SOURCE);
  }

  @EventHandler
  default void onBuildingAddedEvent(final BuildingAddedEvent e) {
    onAdd(e.getSituation().getId(), e.getValue(), FeatureType.BUILDING);
  }

  @EventHandler
  default void onBuildingUpdatedEvent(final BuildingUpdatedEvent e) {
    onUpdate(e.getSituation().getId(), e.getValue(), FeatureType.BUILDING);
  }

  @EventHandler
  default void onBuildingDeletedEvent(final BuildingDeletedEvent e) {
    onDelete(e.getSituation().getId(), e.getValue(), FeatureType.BUILDING);
  }

  void onAdd(final String situationId, final Feature feature, final FeatureType featureType);

  void onUpdate(final String situationId, final Feature feature, final FeatureType featureType);

  void onDelete(final String situationId, final Feature feature, final FeatureType featureType);

  default String situationFeatureKey(final String situationId, final Feature feature, final FeatureType featureType) {
    return feature == null ? null
        : (situationId + SITUATION_FEATURE_KEY_SEPARATOR + feature.getId() + SITUATION_FEATURE_KEY_SEPARATOR + featureType.name());
  }
}
