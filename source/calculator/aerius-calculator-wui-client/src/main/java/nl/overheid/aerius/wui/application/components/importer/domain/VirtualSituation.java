/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.importer.domain;

import java.util.Objects;
import java.util.Optional;

import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;
import nl.overheid.aerius.wui.application.util.UUID;

/**
 * Represents a handle to a hypothetical future situation
 */
public class VirtualSituation {
  private String id = null;
  private String name = null;

  private FileUploadStatus fileHandle = null;
  private SituationType situationType = null;

  // SituationContext that will be used during actual import
  private SituationContext newlyExistingSituation = null;

  public VirtualSituation() {
    this.id = UUID.uuidv4();
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getId() {
    return id;
  }

  /**
   * The file handle which represents the base file
   */
  public FileUploadStatus getFileHandle() {
    return fileHandle;
  }

  public void setFileHandle(final FileUploadStatus fileHandle) {
    this.fileHandle = fileHandle;
  }

  public SituationContext getNewlyExistingSituation() {
    return newlyExistingSituation;
  }

  public void setNewlyExistingSituation(final SituationContext existingSituation) {
    this.newlyExistingSituation = existingSituation;
  }

  public void setSituationType(final SituationType situationType) {
    this.situationType = situationType;
  }

  public SituationType getSituationType() {
    return situationType;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    final VirtualSituation other = (VirtualSituation) obj;
    return Objects.equals(id, other.id);
  }

  public static boolean isSameBaseFile(final VirtualSituation virtualSituation, final FileUploadStatus file) {
    return virtualSituation.getFileHandle().getFile().equals(file.getFile());
  }

  public static boolean isSameFileHandle(final VirtualSituation virtualSituation, final FileUploadStatus file) {
    return Optional.ofNullable(virtualSituation.getFileHandle())
        .map(v -> v.getFileCode())
        .orElse("").equals(file.getFileCode());
  }

  public static boolean isAlteredMetaData(final VirtualSituation virtualSituation, final FileUploadStatus file) {
    if (file == null || virtualSituation == null) {
      return false;
    }

    return file.getSituationType() != virtualSituation.getSituationType();
  }
}
