/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.base;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

/**
 * Client side implementation of CustomVehicles props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class LinearReference {

  private double fromPosition;
  private double toPosition;

  public final @JsOverlay double getFromPosition() {
    return fromPosition;
  }

  public final @JsOverlay void setFromPosition(final double fromPosition) {
    this.fromPosition = fromPosition;
  }

  public final @JsOverlay double getToPosition() {
    return toPosition;
  }

  public final @JsOverlay void setToPosition(final double toPosition) {
    this.toPosition = toPosition;
  }

}
