/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.calculate.detail;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.components.divider.DividerComponent;
import nl.overheid.aerius.wui.application.components.source.DetailStatComponent;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    DividerComponent.class,
    DetailStatComponent.class
})
public class CalculationJobDetailDevelopmentPressureSearchView extends BasicVueComponent {
  @Prop CalculationSetOptions calculationSetOptions;
  @Prop SituationComposition situationComposition;

  @Computed("selectedSources")
  public int getAmountOfSelectedSources() {
    return calculationSetOptions.getNcaCalculationOptions().getDevelopmentPressureSourceIds().size();
  }

  @Computed("totalSources")
  public int getTotalAmountOfSources() {
    final SituationContext proposedSituation = situationComposition.getSituation(SituationType.PROPOSED);
    return proposedSituation == null ? 0 : (int) proposedSituation.getSources().stream()
        .filter(src -> src.getEmission(Substance.NOX) > 0D || src.getEmission(Substance.NH3) > 0D)
        .count();
  }

}
