/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.emissionsource;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;

import nl.aerius.wui.vue.transition.HorizontalCollapse;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    HorizontalCollapse.class,
})
public class ErrorWarningIconComponent extends BasicVueComponent {
  @Prop boolean reveal;

  @Prop String title;

  @Prop boolean showError;
  @Prop boolean showWarning;

  @PropDefault("showError")
  boolean showErrorDefault() {
    return false;
  }

  @PropDefault("showWarning")
  boolean showWarningDefault() {
    return false;
  }

  @PropDefault("reveal")
  boolean revealDefault() {
    // Default to true so this prop is optional (i.e. the reveal condition might
    // aswell be ORed into the showError and showWarning conditions)
    return true;
  }

  @Computed
  public String getErrorWarningText() {
    return showError ? i18n.validationErrorLabel()
        : showWarning ? i18n.validationWarningLabel() : "";
  }

  @Computed
  public String getAriaLabel() {
    return showError ? i18n.ariaErrorIcon(title)
        : showWarning ? i18n.ariaWarningIcon(title) : "";
  }
}
