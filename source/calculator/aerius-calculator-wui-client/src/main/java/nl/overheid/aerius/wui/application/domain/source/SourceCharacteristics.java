/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.wui.vue.ReactivityUtil;
import nl.overheid.aerius.wui.vue.SkipReactivityValidations;

/**
 * Base class for model specific source characteristics.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public abstract class SourceCharacteristics {

  private String type;
  private String buildingId;

  public static final @JsOverlay void init(final SourceCharacteristics chars, final CharacteristicsType type) {
    chars.setCharacteristicsType(type);
    ReactivityUtil.ensureDefault(chars::getBuildingGmlId, chars::setBuildingGmlId);
  }

  @SkipReactivityValidations
  public static final @JsOverlay void initTypeDependent(final EmissionSourceFeature feature, final CharacteristicsType type) {
    ReactivityUtil.ensureDefaultSupplied(feature::getCharacteristics, feature::setCharacteristics,
        () -> type == CharacteristicsType.OPS ? OPSCharacteristics.create()
            : type == CharacteristicsType.ADMS ? ADMSCharacteristics.create()
                : null);
    ReactivityUtil.ensureInitialized(() -> feature.getCharacteristics(), chars -> {
      if (type == CharacteristicsType.OPS) {
        OPSCharacteristics.initOPS((OPSCharacteristics) chars);
      } else if (type == CharacteristicsType.ADMS) {
        ADMSCharacteristics.initADMS((ADMSCharacteristics) chars);
      }
    });
  }

  public final @JsOverlay CharacteristicsType getCharacteristicsType() {
    return CharacteristicsType.safeValueOf(type);
  }

  public final @JsOverlay void setCharacteristicsType(final CharacteristicsType type) {
    this.type = type == null ? null : type.name();
  }

  @SkipReactivityValidations
  public final @JsOverlay boolean isBuildingInfluence() {
    return buildingId != null;
  }

  public final @JsOverlay void setBuildingGmlId(final String buildingId) {
    this.buildingId = buildingId;
  }

  public final @JsOverlay String getBuildingGmlId() {
    return buildingId;
  }

  public final @JsOverlay ADMSCharacteristics asADMS() {
    return Js.uncheckedCast(this);
  }

  public final @JsOverlay OPSCharacteristics asOPS() {
    return Js.uncheckedCast(this);
  }
}
