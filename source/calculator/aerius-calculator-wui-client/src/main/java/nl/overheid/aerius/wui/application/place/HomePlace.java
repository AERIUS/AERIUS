/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.place;

import java.util.function.Supplier;

import nl.aerius.wui.place.PlaceTokenizer;
import nl.overheid.aerius.shared.domain.Theme;

/**
 * Start page for a theme.
 */
public class HomePlace extends MainThemePlace {

  public static class Tokenizer<T extends MainThemePlace> extends MainThemePlace.Tokenizer<T> {
    public Tokenizer(final Supplier<T> provider, final Theme theme, final String... postfix) {
      super(provider, theme, postfix);
    }
  }

  public HomePlace(final Theme theme) {
    this(createTokenizer(theme));
  }

  public <P extends MainThemePlace> HomePlace(final PlaceTokenizer<P> tokenizer) {
    super(tokenizer);
  }

  public static PlaceTokenizer<EmissionSourcePlace> createTokenizer(final Theme theme) {
    return new Tokenizer(() -> new HomePlace(theme), theme);
  }

  @Override
  public <T> T visit(final PlaceVisitor<T> visitor) {
    return visitor.apply(this);
  }

}
