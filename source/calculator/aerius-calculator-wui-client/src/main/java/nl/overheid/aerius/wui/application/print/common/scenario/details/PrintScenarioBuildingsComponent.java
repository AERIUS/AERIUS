/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.print.common.scenario.details;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.v2.building.BuildingLimits;
import nl.overheid.aerius.wui.application.components.common.StyledPointComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.geo.util.GeoType;
import nl.overheid.aerius.wui.application.geo.util.GeoUtil;
import nl.overheid.aerius.wui.application.geo.util.OrientedEnvelope;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.print.common.PrintSection;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    PrintSection.class,
    StyledPointComponent.class,
})
public class PrintScenarioBuildingsComponent extends BasicVueComponent {
  @Prop SituationContext situation;

  @Inject @Data ApplicationContext appContext;

  @JsMethod
  public BuildingLimits getBuildingLimits() {
    return appContext.getConfiguration().getBuildingLimits();
  }

  @JsMethod
  public boolean isPoint(final BuildingFeature building) {
    return GeoType.POINT.is(building.getGeometry());
  }

  @JsMethod
  public boolean isPolygon(final BuildingFeature building) {
    return GeoType.POLYGON.is(building.getGeometry());
  }

  public OrientedEnvelope getOrientedEnvelope(final BuildingFeature building) {
    return GeoUtil.determineOrientedEnvelope(building.getGeometry());
  }

  public double getUsedBuildingLength(final BuildingFeature feature) {
    return Math.min(getBuildingLimits().buildingLengthMaximum(), getOrientedEnvelope(feature).getLength());
  }

  public double getUsedBuildingWidth(final BuildingFeature feature) {
    return Math.min(getBuildingLimits().buildingWidthMaximum(), getOrientedEnvelope(feature).getWidth());
  }

  public double getUsedBuildingHeight(final BuildingFeature feature) {
    return Math.max(getBuildingLimits().buildingHeightMinimum(), Math.min(getBuildingLimits().buildingHeightMaximum(), feature.getHeight()));
  }

  @JsMethod
  public String getUsedBuildingLengthStr(final BuildingFeature feature) {
    return i18n.decimalNumberFixed(getUsedBuildingLength(feature), getBuildingLimits().buildingDigitsPrecision());
  }

  @JsMethod
  public String getUsedBuildingWidthStr(final BuildingFeature feature) {
    return i18n.decimalNumberFixed(getUsedBuildingWidth(feature), getBuildingLimits().buildingDigitsPrecision());
  }

  @JsMethod
  public String getUsedBuildingHeightStr(final BuildingFeature feature) {
    return i18n.decimalNumberFixed(getUsedBuildingHeight(feature), getBuildingLimits().buildingDigitsPrecision());
  }

  @JsMethod
  public String getBuildingDiameter(final BuildingFeature feature) {
    return feature.getDiameter() == null ? ""
        : i18n.unitM(i18n.decimalNumberFixed(feature.getDiameter(), getBuildingLimits().buildingDigitsPrecision()));
  }

  @JsMethod
  public String getUsedBuildingOrientationStr(final BuildingFeature feature) {
    return M.messages().decimalNumberFixed(getOrientedEnvelope(feature).getOrientation(), 0);
  }
}
