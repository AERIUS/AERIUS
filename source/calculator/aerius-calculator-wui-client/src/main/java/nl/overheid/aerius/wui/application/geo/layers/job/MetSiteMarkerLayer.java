/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.job;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.DoubleSupplier;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.Coordinate;
import ol.geom.Point;

import nl.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.shared.domain.backgrounddata.MetSite;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.MapConfigurationContext;
import nl.overheid.aerius.wui.application.domain.source.GenericImaerFeature;
import nl.overheid.aerius.wui.application.event.met.MetSiteListChangeEvent;
import nl.overheid.aerius.wui.application.geo.layers.LabelStyle;
import nl.overheid.aerius.wui.application.geo.layers.base.BaseClusteredMarkerLayer;
import nl.overheid.aerius.wui.application.geo.layers.base.ClusteredLabelStyleCreator;
import nl.overheid.aerius.wui.application.geo.layers.base.VectorFeaturesWrapper;
import nl.overheid.aerius.wui.application.i18n.M;

/**
 * Layer to show a list of {@link MetSite} objects as markers on the map.
 */
public class MetSiteMarkerLayer extends BaseClusteredMarkerLayer<GenericImaerFeature> {

  interface MetSiteMarkerLayerEventBinder extends EventBinder<MetSiteMarkerLayer> {}

  private final MetSiteMarkerLayerEventBinder EVENT_BINDER = GWT.create(MetSiteMarkerLayerEventBinder.class);

  private final VectorFeaturesWrapper vectorObject = new VectorFeaturesWrapper();

  @Inject
  public MetSiteMarkerLayer(final EventBus eventBus, @Assisted final int zIndex, final MapConfigurationContext mapConfigurationContext,
      final ApplicationContext appContext) {
    super(new LayerInfo(), zIndex, new MetSiteLabelStyleCreator(mapConfigurationContext::getIconScale));
    getInfo().setName(MetSiteMarkerLayer.class.getName());
    getInfo().setTitle(M.messages().layerMetSites());

    setFeatures(vectorObject, Collections.emptyList());
    asLayer().setVisible(false);
    EVENT_BINDER.bindEventHandlers(this, eventBus);

    setMetSites(appContext.getConfiguration().getMetSites());
  }

  private void setMetSites(final List<MetSite> metSites) {
    final List<GenericImaerFeature> metSitesFeatures = Optional.ofNullable(metSites).orElseGet(ArrayList::new)
        .stream()
        .filter(v -> v.getDatasets().stream().noneMatch(dataset -> dataset.getDatasetType().isNwp()))
        .map(v -> toMetSiteFeature(v))
        .collect(Collectors.toList());

    setFeatures(vectorObject, metSitesFeatures);
  }

  @EventHandler
  public void onMetSiteListChangeEvent(final MetSiteListChangeEvent e) {
    setMetSites(e.getValue());
  }

  private GenericImaerFeature toMetSiteFeature(final MetSite v) {
    final GenericImaerFeature metSiteFeature = GenericImaerFeature.create();
    metSiteFeature.setId(v.getName());
    metSiteFeature.setGeometry(new Point(new Coordinate(v.getX(), v.getY())));
    metSiteFeature.setLabel(v.getName());
    return metSiteFeature;
  }

  private static class MetSiteLabelStyleCreator extends ClusteredLabelStyleCreator<GenericImaerFeature> {
    public MetSiteLabelStyleCreator(final DoubleSupplier iconScale) {
      super(iconScale, false, LabelStyle.MET_SITE);
    }

    @Override
    protected String clusterLabel(final int count, final LabelStyle labelStyle) {
      return M.messages().metSiteMarkerClusterLabel(count);
    }
  }
}
