/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source;

import static jsinterop.annotations.JsPackage.GLOBAL;

import java.util.Locale;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadManager;
import nl.overheid.aerius.shared.domain.v2.source.road.TrafficDirection;
import nl.overheid.aerius.wui.application.domain.source.road.Vehicles;
import nl.overheid.aerius.wui.vue.ReactivityUtil;

/**
 * Abstract feature object for Road Emission Sources.
 */
@JsType(namespace = GLOBAL, name = "ol.Feature", isNative = true)
public abstract class RoadESFeature extends EmissionSubSourceFeature<Vehicles> {
  protected final static @JsOverlay <T extends AbstractSubSource> void initRoadFeature(final RoadESFeature feature,
      final CharacteristicsType characteristicsType, final EmissionSourceType type) {
    EmissionSubSourceFeature.initSubSourceFeature(feature, characteristicsType, type);
    ReactivityUtil.ensureDefault(feature::getTrafficDirection, feature::setTrafficDirection, TrafficDirection.BOTH);
    ReactivityUtil.ensureDefault(feature::getRoadAreaCode, feature::setRoadAreaCode);
    ReactivityUtil.ensureDefault(feature::getRoadTypeCode, feature::setRoadTypeCode);
    ReactivityUtil.ensureJsProperty(feature.getProperties(), "roadManager", feature::setRoadManager, RoadManager.STATE);

    // Unclear yet whether this will be used in ADMS (as is):
    ReactivityUtil.ensureDefault(feature::getTunnelFactor, feature::setTunnelFactor, 1D);
  }

  public final @JsOverlay String getRoadAreaCode() {
    return get("roadAreaCode");
  }

  public final @JsOverlay void setRoadAreaCode(final String roadAreaCode) {
    set("roadAreaCode", roadAreaCode);
  }

  public final @JsOverlay String getRoadTypeCode() {
    return get("roadTypeCode");
  }

  public final @JsOverlay void setRoadTypeCode(final String roadTypeCode) {
    set("roadTypeCode", roadTypeCode);
  }

  public final @JsOverlay RoadManager getRoadManager() {
    final String roadManager = get("roadManager");
    return roadManager == null ? null : RoadManager.valueOf(roadManager);
  }

  public final @JsOverlay void setRoadManager(final RoadManager roadManager) {
    set("roadManager", roadManager.name());
  }

  public final @JsOverlay Double getTunnelFactor() {
    return get("tunnelFactor") == null ? null : Js.asDouble(get("tunnelFactor"));
  }

  public final @JsOverlay void setTunnelFactor(final double tunnelFactor) {
    set("tunnelFactor", Double.valueOf(tunnelFactor));
  }

  public final @JsOverlay TrafficDirection getTrafficDirection() {
    final String trafficDirection = get("trafficDirection");
    return trafficDirection == null ? null : safeTrafficDirectionOf(trafficDirection);
  }

  public final @JsOverlay void setTrafficDirection(final TrafficDirection trafficDirection) {
    set("trafficDirection", trafficDirection.name());
  }

  private static final @JsOverlay TrafficDirection safeTrafficDirectionOf(final String value) {
    try {
      return value == null ? null : TrafficDirection.valueOf(value.toUpperCase(Locale.ROOT));
    } catch (final IllegalArgumentException e) {
      return null;
    }
  }

}
