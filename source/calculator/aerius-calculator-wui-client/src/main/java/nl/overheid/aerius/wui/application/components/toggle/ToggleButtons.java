/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.toggle;

import java.util.List;
import java.util.function.Function;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

/**
 * A simple component displaying toggle buttons that can switch between tabs,
 * also includes accessibility tags.
 */
@Component
public class ToggleButtons implements IsVueComponent {
  @SuppressWarnings("rawtypes") @Prop(checkType = true) @JsProperty List options;
  @Prop Object selected;

  @Prop Function<Object, String> idFunc;
  @Prop boolean buttonMargin;

  @Prop String groupId;

  @Prop boolean hasContent;

  @PropDefault("hasContent")
  boolean hasContentDefault() {
    return true;
  }

  @PropDefault("buttonMargin")
  boolean hasButtonMargin() {
    return false;
  }

  @PropDefault("idFunc")
  Function<Object, String> idFuncDefault() {
    return obj -> String.valueOf("tab-" + (groupId != null ? groupId + "-" : "") + String.valueOf(obj));
  }

  @JsMethod
  public void selectTab(final Object obj) {
    vue().$emit("select", obj);
  }
}
