/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.event.source;

import nl.aerius.wui.event.SimpleGenericEvent;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;

public abstract class EmissionSourceChangeEvent extends SimpleGenericEvent<EmissionSourceFeature> {

  public static enum Change {
    /*
    Add a new emission source to the situation
     */
    ADDED,
    /*
    Updated an emission source
     */
    UPDATED,
    /*
    Remove a emission source from the situation
     */
    DELETED,
    /*
    Start an interaction on the map to modify the geometry of the emission source
     */
    MODIFY_GEOMETRY,
  }

  private final SituationContext situation;
  private final Change change;

  public EmissionSourceChangeEvent(final EmissionSourceFeature source, final SituationContext situation, final Change change) {
    super(source);
    this.situation = situation;
    this.change = change;
  }

  public SituationContext getSituation() {
    return situation;
  }

  public Change getChange() {
    return change;
  }
}
