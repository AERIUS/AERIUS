/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.farm;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;
import nl.overheid.aerius.wui.application.domain.source.EmissionFactors;
import nl.overheid.aerius.wui.vue.ReactivityUtil;
import nl.overheid.aerius.wui.vue.SkipReactivityValidations;

/**
 * Client side implementation of customFarmAnimalHousings props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class CustomFarmAnimalHousing extends FarmAnimalHousing {

  private String description;
  private String farmEmissionFactorType;
  private EmissionFactors emissionFactors;

  public static final @JsOverlay CustomFarmAnimalHousing create() {
    final CustomFarmAnimalHousing props = Js.uncheckedCast(JsPropertyMap.of());
    init(props);
    return props;
  }

  public static final @JsOverlay void init(final CustomFarmAnimalHousing props) {
    FarmAnimalHousing.init(props, FarmAnimalHousingType.CUSTOM);

    ReactivityUtil.ensureJsProperty(props, "description", props::setDescription, "");
    ReactivityUtil.ensureJsProperty(props, "farmEmissionFactorType", props::setFarmEmissionFactorType, FarmEmissionFactorType.PER_ANIMAL_PER_YEAR);
    ReactivityUtil.ensureJsPropertySupplied(props, "emissionFactors", props::setEmissionFactors, EmissionFactors::create);
    ReactivityUtil.ensureInitialized(props::emissionFactors, v -> EmissionFactors.init(v, Substance.NH3));
  }

  public final @JsOverlay String getDescription() {
    return description;
  }

  public final @JsOverlay void setDescription(final String description) {
    this.description = description;
  }

  @SkipReactivityValidations
  public final @JsOverlay double getEmissionFactor(final Substance substance) {
    return emissionFactors.getEmissionFactor(substance);
  }

  public final @JsOverlay void setEmissionFactor(final Substance substance, final double emission) {
    emissionFactors.setEmissionFactor(substance, emission);
  }

  public final @JsOverlay void setEmissionFactors(final EmissionFactors emissionFactors) {
    this.emissionFactors = emissionFactors;
  }

  private final @JsOverlay EmissionFactors emissionFactors() {
    return emissionFactors;
  }

  public final @JsOverlay FarmEmissionFactorType getFarmEmissionFactorType() {
    return FarmEmissionFactorType.valueOf(farmEmissionFactorType);
  }

  public final @JsOverlay void setFarmEmissionFactorType(final FarmEmissionFactorType farmEmissionFactorType) {
    this.farmEmissionFactorType = farmEmissionFactorType.name();
  }
}
