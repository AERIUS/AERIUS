/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.i18n;

import com.google.gwt.i18n.client.Messages.Select;

public interface HotKeyMessages {
  String hotkeyTitle();

  String hotkeyEnabledState(@Select boolean enabled);
  String hotkeyEnabledStateToggle(@Select boolean enabled);
  String hotkeyEnabledStateToggleAria(@Select boolean enabled);

  String hotkeyToolsTitle();
  String hotkeyNotificationPopupKeys();
  String hotkeyNotificationPopupExplain();
  String hotkeyLayerPopupKeys();
  String hotkeyLayerPopupExplain();
  String hotkeyInfoPopupKeys();
  String hotkeyInfoPopupExplain();
  String hotkeySearchPopupKeys();
  String hotkeySearchPopupExplain();
  String hotkeyMeasureToolKeys();
  String hotkeyMeasureToolExplain();
  String hotkeyLeftPanelToggleKeys();
  String hotkeyLeftPanelToggleExplain();
  String hotkeyManualPanelToggleKeys();
  String hotkeyManualPanelToggleExplain();
  String hotkeyHotKeyPanelToggleKeys();
  String hotkeyHotKeyPanelToggleExplain();
  String hotkeyEscapeFocusKeys();
  String hotkeyEscapeFocusExplain();
  String hotkeyTurnOffHotKeysKeys();
  String hotkeyTurnOffHotKeysExplain();

  String hotkeyNavigationTitle();
  String hotkeyNavigateSourcesKeys();
  String hotkeyNavigateSourcesExplain();
  String hotkeyNavigateAssessmentPointsKeys();
  String hotkeyNavigateAssessmentPointsExplain();
  String hotkeyNavigateCalculateKeys();
  String hotkeyNavigateCalculateExplain();
  String hotkeyNavigateResultsKeys();
  String hotkeyNavigateResultsExplain();
  String hotkeyNavigateExportKeys();
  String hotkeyNavigateExportExplain();
  String hotkeyNavigateHomeKeys();
  String hotkeyNavigateHomeExplain();

  String hotkeyCreateCurrentKeys();
  String hotkeyCreateCurrentExplain();
  String hotkeyCreateSituationKeys();
  String hotkeyCreateSituationExplain();
  String hotkeyCreateSourceKeys();
  String hotkeyCreateSourceExplain();
  String hotkeyCreateBuildingKeys();
  String hotkeyCreateBuildingExplain();
  String hotkeyCreateAssessmentPointKeys();
  String hotkeyCreateAssessmentPointExplain();

  String hotkeySelectTitle();
  String hotkeySelectFeatureKeys();
  String hotkeySelectFeatureExplain();
  String hotkeySelectSituationKeys();
  String hotkeySelectSituationExplain();
  String hotkeySelectedZoomKeys();
  String hotkeySelectedZoomExplain();
  String hotkeySelectedRemoveKeys();
  String hotkeySelectedRemoveExplain();
  String hotkeySelectedEditKeys();
  String hotkeySelectedEditExplain();
  String hotkeySelectedDuplicateKeys();
  String hotkeySelectedDuplicateExplain();
  String hotkeyDeselectAllKeys();
  String hotkeyDeselectAllExplain();

  String hotkeyMapTitle();
  String hotkeyMapZoomInKeys();
  String hotkeyMapZoomInExplain();
  String hotkeyMapZoomOutKeys();
  String hotkeyMapZoomOutExplain();
  String hotkeyMapPanLeftKeys();
  String hotkeyMapPanLeftExplain();
  String hotkeyMapPanRightKeys();
  String hotkeyMapPanRightExplain();
  String hotkeyMapPanDownKeys();
  String hotkeyMapPanDownExplain();
  String hotkeyMapPanUpKeys();
  String hotkeyMapPanUpExplain();
  String hotkeyMapLabelDisplayModeKeys();
  String hotkeyMapLabelDisplayModeExplain();
  String hotkeyMapResetButtonKeys();
  String hotkeyMapResetButtonExplain();
  String hotkeyMapDragToZoomKeys();
  String hotkeyMapDragToZoomExplain();

  String hotkeyPanelApplicationVersion(String version);
  String hotkeyPanelDatabaseVersion(String version);
}
