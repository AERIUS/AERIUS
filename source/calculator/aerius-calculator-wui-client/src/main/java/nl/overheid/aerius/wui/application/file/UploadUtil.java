/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.file;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.user.client.rpc.AsyncCallback;

import elemental2.dom.EventListener;
import elemental2.dom.FormData;
import elemental2.dom.ProgressEvent;
import elemental2.dom.XMLHttpRequest;

import nl.overheid.aerius.shared.RequestMappings;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.wui.application.components.importer.domain.ImportAction;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;

public final class UploadUtil {

  private static final Logger LOG = Logger.getLogger(UploadUtil.class.getName());

  private UploadUtil() {}

  public static void uploadFile(final String url, final FileUploadStatus item, final Theme theme, final Map<String, String> headers,
      final AsyncCallback<String> callback) {
    // Not using RequestUtil, since we want to track progress.
    final FormData data = new FormData();
    data.append("filePart", item.getFile());
    data.append("importProperties", toImportProperties(item, theme));

    final XMLHttpRequest req = new XMLHttpRequest();
    req.upload.addEventListener("progress", evt -> {
      final ProgressEvent progress = (ProgressEvent) evt;
      if (progress.lengthComputable) {
        item.setCompleteRatio(progress.loaded / progress.total);
      }
    });

    final EventListener errorEvent = evt -> fail(item, callback, req);
    req.addEventListener("error", errorEvent);
    req.addEventListener("abort", errorEvent);
    req.addEventListener("load", evt -> {
      if (req.status == 200) {
        final String fileCode = req.responseText;

        succeed(item, fileCode, callback);
      } else {
        fail(item, callback, req);
      }
    });

    req.open("POST", url + RequestMappings.CONNECT_UI_IMPORT);
    headers.forEach((k, v) -> req.setRequestHeader(k, v));
    req.send(data);
  }

  private static String toImportProperties(final FileUploadStatus item, final Theme theme) {
    return "{"
        + ((item.getSubstance() == null ? "" : ("\"substance\": \"" + item.getSubstance().name() + "\",")))
        + "\"year\": " + "\"" + item.getImportYear() + "\"" + ","
        + "\"theme\": " + "\"" + theme.name() + "\""
        + "}";
  }

  private static void succeed(final FileUploadStatus item, final String fileCode, final AsyncCallback<String> callback) {
    item.setComplete(true);
    item.setCompleteRatio(1D);
    callback.onSuccess(fileCode);
  }

  private static void fail(final FileUploadStatus item, final AsyncCallback<String> callback, final XMLHttpRequest req) {
    item.setComplete(false);
    item.setFailed(true);
    item.setImportAction(ImportAction.ignore());
    item.setCompleteRatio(1D);
    LOG.log(Level.SEVERE,
        () -> "Failure uploading. Response URL " + req.responseURL + ". Status code: " + req.status + ". Status text: " + req.statusText);
    callback.onFailure(new AeriusException(AeriusExceptionReason.UPLOAD_ABORTED, item.getFile().name));
  }
}
