/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.input;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

@Component(name = "checkbox")
public class CheckBoxComponent implements IsVueComponent {
  @Prop String label;
  @Prop String id;
  @Prop boolean accessibleLabel;

  @Prop boolean disabled;

  @Data boolean checked;
  @Prop boolean value;

  @PropDefault("value")
  boolean valueDefault() {
    return false;
  }

  @PropDefault("label")
  String labelDefault() {
    return "";
  }

  @PropDefault("id")
  String idDefault() {
    return "";
  }

  @PropDefault("accessibleLabel")
  boolean accessibleLabelDefault() {
    return false;
  }

  @PropDefault("disabled")
  boolean disabledDefault() {
    return false;
  }

  @Watch(value = "value", isImmediate = true)
  public void onValueChange(final boolean neww, final boolean old) {
    checked = value;
  }

  @Watch("checked")
  public void onCheckedChange(final boolean neww, final boolean old) {
    vue().$emit("input", neww);
  }
}
