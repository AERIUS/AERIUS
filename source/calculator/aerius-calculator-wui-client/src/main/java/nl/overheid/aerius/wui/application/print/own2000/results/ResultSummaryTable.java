/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.print.own2000.results;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.PdfProductType;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings.DepositionValueDisplayType;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.wui.application.command.result.FetchSummaryResultsCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.PrintContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.context.result.ResultSummaryContext;
import nl.overheid.aerius.wui.application.domain.info.AssessmentArea;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsAreaSummary;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsStatistics;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsSummary;
import nl.overheid.aerius.wui.application.event.FetchSummaryResultsEvent;
import nl.overheid.aerius.wui.application.event.RestoreCalculationJobContextEvent;
import nl.overheid.aerius.wui.application.ui.pages.results.GeneralizedEmissionResultType;
import nl.overheid.aerius.wui.application.ui.pages.results.stats.ResultsStatisticsFormatter;
import nl.overheid.aerius.wui.application.util.ResultDisplaySetUtil;
import nl.overheid.aerius.wui.application.util.ResultStatisticUtil;
import nl.overheid.aerius.wui.vue.BasicVueEventComponent;

@Component(name = "result-summary-table")
public class ResultSummaryTable extends BasicVueEventComponent implements HasCreated {
  private static final ResultSummaryTableEventBinder EVENT_BINDER = GWT.create(ResultSummaryTableEventBinder.class);

  interface ResultSummaryTableEventBinder extends EventBinder<ResultSummaryTable> {}

  @Inject @Data ApplicationContext applicationContext;

  @Inject @Data PrintContext printOwN2000Context;
  @Inject ScenarioContext scenarioContext;
  @Inject CalculationContext calculationContext;
  @Inject ResultsStatisticsFormatter resultsStatisticsFormatter;

  @Prop EventBus eventBus;
  @Data SituationResultsKey key = null;
  @Data boolean fetchComplete;

  @Computed
  public boolean isRender() {
    return fetchComplete;
  }

  @Computed
  public Theme getTheme() {
    return applicationContext.getTheme();
  }

  @EventHandler
  public void onFetchSummaryResultsEvent(final FetchSummaryResultsEvent e) {
    if (e.getValue().equals(key)) {
      fetchComplete = true;
    }
  }

  @EventHandler
  public void onRestoreCalculationContextEvent(final RestoreCalculationJobContextEvent e) {
    final SituationResultsKey edgeEffectsKey = new SituationResultsKey(
        SituationContext.handleFromSituation(scenarioContext.getActiveSituation()),
        printOwN2000Context.getScenarioResultType(),
        printOwN2000Context.getEmissionResultKey(),
        printOwN2000Context.getSummaryHexagonType(),
        printOwN2000Context.getOverlappingHexagonType(),
        printOwN2000Context.getProcurementPolicy());
    key = edgeEffectsKey;
    eventBus.fireEvent(new FetchSummaryResultsCommand(edgeEffectsKey));
  }

  @Override
  public void created() {
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Computed
  public DepositionValueDisplayType getDisplayType() {
    return applicationContext.getConfiguration().getEmissionResultValueDisplaySettings().getDisplayType();
  }

  @Computed
  public GeneralizedEmissionResultType getEmissionResultType() {
    return GeneralizedEmissionResultType.fromEmissionResultType(getResultContext().getEmissionResultKey().getEmissionResultType());
  }

  @Computed
  public String getIntro() {
    return i18n.pdfResultSummaryTableIntro(scenarioContext.getActiveSituation() != null ? scenarioContext.getActiveSituation().getName() : "",
        printOwN2000Context.getPdfProductType());
  }

  @Computed
  public List<ResultStatisticType> getResultStatisticTypes() {
    return ResultStatisticUtil.getStatisticsForPdfResult(applicationContext.getTheme(), printOwN2000Context.getScenarioResultType(),
        printOwN2000Context.getSummaryHexagonType(), printOwN2000Context.getPdfProductType(), printOwN2000Context.getEmissionResultKey());
  }

  @Computed
  public SituationResultsAreaSummary[] getAreas() {
    return Optional.ofNullable(getSummary())
        .map(SituationResultsSummary::getAreaStatistics)
        .orElse(new SituationResultsAreaSummary[0]);
  }

  @JsMethod
  public SituationResultsSummary getSummary() {
    return Optional.ofNullable(getResultContext())
        .map(c -> c.getResultSummary(key))
        .orElseThrow(() -> new RuntimeException("Could not retrieve result summary."));
  }

  @Computed
  public CalculationExecutionContext getCalculationExecution() {
    return calculationContext.getActiveCalculation();
  }

  @JsMethod
  public ResultSummaryContext getResultContext() {
    return Optional.ofNullable(getCalculationExecution())
        .map(CalculationExecutionContext::getResultContext)
        .orElseThrow(() -> new RuntimeException("Could not retrieve result context."));
  }

  @Computed
  public AssessmentArea[] getOmittedAreas() {
    return Optional.ofNullable(getSummary())
        .map(SituationResultsSummary::getOmittedAreas)
        .orElse(new AssessmentArea[0]);
  }

  @JsMethod
  public String formatValueFor(final SituationResultsStatistics statistics, final ResultStatisticType type) {
    return resultsStatisticsFormatter.formatStatisticType(statistics, type);
  }

  @JsMethod
  public boolean isPdfOwN2000Permit() {
    return PdfProductType.OWN2000_PERMIT.equals(printOwN2000Context.getPdfProductType());
  }

  @JsMethod
  public boolean isDemandCheck(final SituationResultsStatistics statistics) {
    return formatValueFor(statistics, ResultStatisticType.DEVELOPMENT_SPACE_DEMAND_CHECK).contains("1");
  }

  @JsMethod
  protected String noResultsText() {
    return ResultDisplaySetUtil.noCalculationResultsText(getResultContext());
  }
}
