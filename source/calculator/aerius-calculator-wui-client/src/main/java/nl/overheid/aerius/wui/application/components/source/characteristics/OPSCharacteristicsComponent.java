/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.characteristics;

import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Watch;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.wui.application.components.divider.DividerComponent;
import nl.overheid.aerius.wui.application.components.source.DetailStatComponent;
import nl.overheid.aerius.wui.application.components.source.characteristics.ops.OPSCharacteristicsBasicComponent;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.domain.source.OPSCharacteristics;

@Component(name = "ops-characteristics", components = {
    VerticalCollapse.class,
    DetailStatComponent.class,
    MinMaxLabelComponent.class,
    DividerComponent.class
})
public class OPSCharacteristicsComponent extends OPSCharacteristicsBasicComponent {

  @Inject @Data ScenarioContext scenarioContext;

  @Override
  @Watch(value = "source", isImmediate = true)
  public void watchSource() {
    super.watchSource();
  }

  @JsMethod
  public String getBuildingName() {
    if (isCType(source.getCharacteristicsType())) {
      return Optional.ofNullable((OPSCharacteristics) source.getCharacteristics())
          .map(OPSCharacteristics::getBuildingGmlId)
          .map(v -> scenarioContext.getActiveSituation().findBuilding(v).getLabel())
          .orElse(null);
    } else {
      return null;
    }
  }

  private boolean isCType(final CharacteristicsType type) {
    return source.getCharacteristicsType() == type;
  }

}
