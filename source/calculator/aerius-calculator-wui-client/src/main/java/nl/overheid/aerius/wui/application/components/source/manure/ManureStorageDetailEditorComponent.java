/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.manure;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;

import elemental2.core.JsArray;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.sector.category.FarmSourceCategory;
import nl.overheid.aerius.wui.application.components.source.manure.custom.ManureStorageCustomEmissionComponent;
import nl.overheid.aerius.wui.application.components.source.manure.standard.ManureStorageStandardEmissionComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.components.toggle.ToggleButtons;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.ManureStorageESFeature;
import nl.overheid.aerius.wui.application.domain.source.manure.ManureStorage;
import nl.overheid.aerius.wui.application.domain.source.manure.ManureStorageType;
import nl.overheid.aerius.wui.application.domain.source.util.EmissionSourceFeatureUtil;
import nl.overheid.aerius.wui.vue.DebugDirective;

@Component(components = {
    ManureStorageStandardEmissionComponent.class,
    ManureStorageCustomEmissionComponent.class,
    ToggleButtons.class,
    ValidationBehaviour.class,
}, directives = {
    DebugDirective.class
})
public class ManureStorageDetailEditorComponent extends ErrorWarningValidator {
  @Prop ManureStorageESFeature source;
  @Prop ManureStorage subSource;

  @Data ManureStorageType toggleSelect;

  @Data @Inject ApplicationContext applicationContext;

  @Data @JsProperty List<FarmSourceCategory> farmSourceCategories = new ArrayList<>();

  @Watch(value = "subSource", isImmediate = true)
  public void onValueChange(final Integer neww, final Integer old) {
    if (subSource != null) {
      toggleSelect = subSource.getManureStorageType();
    } else {
      toggleSelect = ManureStorageType.STANDARD;
    }
    farmSourceCategories = applicationContext.getConfiguration().getSectorCategories().getFarmSourceCategories(source.getSectorId());
  }

  @Computed
  List<ManureStorageType> getManureStorageTypes() {
    return Arrays.asList(ManureStorageType.values());
  }

  @JsMethod
  public boolean isSourceAvailable() {
    return subSource != null;
  }

  @JsMethod
  public void selectTab(final ManureStorageType type) {
    toggleSelect = type;
    final JsArray<ManureStorage> subSources = source.getSubSources();
    subSources.splice(subSources.indexOf(subSource), 1, EmissionSourceFeatureUtil.createManureStorageType(type));
  }
}
