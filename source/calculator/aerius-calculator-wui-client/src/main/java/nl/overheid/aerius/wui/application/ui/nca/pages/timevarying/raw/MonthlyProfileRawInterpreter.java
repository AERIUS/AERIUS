/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.nca.pages.timevarying.raw;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.overheid.aerius.wui.application.domain.source.util.MonthlyProfileUtil;
import nl.overheid.aerius.wui.application.domain.source.util.TimeVaryingProfileUtil;
import nl.overheid.aerius.wui.application.ui.nca.pages.timevarying.raw.TimeVaryingProfileInterpretException.Reason;

/**
 * Importer for raw monthly profile tables.
 *
 * Constraints:
 *
 * <ul>
 * <li>Will separate on tab or komma</li>
 * <li>Will ignore empty lines or lines starting with !</li>
 * <li>Will import 12 or 13 lines (12 rows of data + 1 optional header)</li>
 * <li>Will import 1 or 2 columns (1 columns of data + 1 optional time column)</li>
 * <li>Will interpret headers containing: 'month,factor'</li>
 * <li>Will not care about the content of the month column</li>
 * <li>12 explicit values must ultimately be found for import to succeed</li>
 * </ul>
 */
public class MonthlyProfileRawInterpreter extends TimeVaryingProfileRawInterpreter<MonthlyProfileUtil> {

  public static final MonthlyProfileRawInterpreter INSTANCE = new MonthlyProfileRawInterpreter();

  private static final String HEADER = "month,factor";
  private static final String COLUMN_MONTH = "month";
  private static final String COLUMN_FACTOR = "factor";

  public MonthlyProfileRawInterpreter() {
    super(TimeVaryingProfileUtil.MONTHLY_PROFILE_UTIL, HEADER, 12, 1);
  }

  @Override
  protected void tryInterpretLines(final boolean columnsWithName, final Map<String, Integer> headerConfig, final List<String[]> rawData,
      final List<Double> values) throws TimeVaryingProfileInterpretException {
    final int factorIdx = columnsWithName ? 1 : 0;

    for (int i = 0; i < rawData.size(); i++) {
      final String[] line = rawData.get(i);

      interpret(values, i, line[factorIdx]);
    }
  }

  private void interpret(final List<Double> values, final int month, final String numberStr)
      throws TimeVaryingProfileInterpretException {
    try {
      final double num = Double.parseDouble(numberStr);

      util.setInput(values, month, num);
    } catch (final NumberFormatException e) {
      throw new TimeVaryingProfileInterpretException(Reason.CANNOT_INTERPRET_VALUE, numberStr);
    }
  }

  @Override
  protected void validateHeader(final List<String> header, final boolean hasRowName) throws TimeVaryingProfileInterpretException {
    if (hasRowName && !contains(header, COLUMN_MONTH)) {
      throw new TimeVaryingProfileInterpretException(Reason.MISSING_HEADER_MONTH);
    }
    if (!contains(header, COLUMN_FACTOR)) {
      throw new TimeVaryingProfileInterpretException(Reason.MISSING_HEADER_MONTH_FACTOR);
    }
  }

  @Override
  protected void putHeaderColumns(final Map<String, Integer> map, final List<String> header, final boolean hasRowName) {
    if (hasRowName) {
      map.put(COLUMN_MONTH, findFirst(header, COLUMN_MONTH));
    }
    map.put(COLUMN_FACTOR, findFirst(header, COLUMN_FACTOR));
  }

  @Override
  protected Map<String, Integer> getDefaultHeaderConfig(final boolean hasRowName) {
    final int mod = hasRowName ? 1 : 0;
    final Map<String, Integer> map = new HashMap<>();

    if (hasRowName) {
      map.put(COLUMN_MONTH, 0);
    }
    map.put(COLUMN_FACTOR, mod);
    return map;
  }

  @Override
  protected void encode(final StringBuilder bldr, final List<Double> values, final int rowNumber) {
    bldr.append(rowNumber + 1);
    bldr.append(SEPARATOR);
    bldr.append(util.getValue(values, rowNumber));
  }
}
