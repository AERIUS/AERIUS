/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.print.own2000.results;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.summary.ProcurementPolicy;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsAreaSummary;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsSummary;
import nl.overheid.aerius.wui.application.ui.pages.results.stats.ResultsStatisticsFormatter;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component
public class ProcurementPolicyOverview extends BasicVueComponent {

  @Prop @JsProperty Map<ProcurementPolicy, SituationResultsSummary> resultsSummaries;

  @Inject ResultsStatisticsFormatter resultsStatisticsFormatter;

  @Computed
  public List<ProcurementPolicy> getProcurementPolicies() {
    // Use the `ProcurementPolicy` enum to ensure the correct order
    return Arrays.stream(ProcurementPolicy.values())
        .filter(resultsSummaries::containsKey).collect(Collectors.toList());
  }

  @JsMethod
  public String isThresholdExceeded(final ProcurementPolicy procurementPolicy) {
    final boolean pass;
    if (procurementPolicy == ProcurementPolicy.WNB_LBV_PLUS) {
      pass = resultsSummaries.get(procurementPolicy).getStatistics()
          .getValue(ResultStatisticType.PROCUREMENT_POLICY_THRESHOLD_PERCENTAGE) > 100D;
    } else {
      pass = getNumberOfAreasAboveThresholdValue(procurementPolicy) > 0;
    }

    return pass ? i18n.pdfOverviewProcurementPass() : i18n.pdfOverviewProcurementFail();
  }

  @JsMethod
  public int getNumberOfAreasAboveThresholdValue(final ProcurementPolicy procurementPolicy) {
    return (int) Arrays.stream(resultsSummaries.get(procurementPolicy).getAreaStatistics())
        .map(SituationResultsAreaSummary::getStatistics)
        .filter(stats -> stats.hasValue(ResultStatisticType.PROCUREMENT_POLICY_THRESHOLD_PERCENTAGE))
        .filter(stats -> stats.getValue(ResultStatisticType.PROCUREMENT_POLICY_THRESHOLD_PERCENTAGE) >= 100D)
        .count();
  }

  @JsMethod
  public String getSumContribution(final ProcurementPolicy procurementPolicy) {
    return resultsStatisticsFormatter.formatStatisticType(resultsSummaries.get(procurementPolicy).getStatistics(),
        ResultStatisticType.SUM_CONTRIBUTION);
  }

  @JsMethod
  public String getMaxContribution(final ProcurementPolicy procurementPolicy) {
    return resultsStatisticsFormatter.formatStatisticType(resultsSummaries.get(procurementPolicy).getStatistics(),
        ResultStatisticType.MAX_CONTRIBUTION);
  }

  @JsMethod
  public String getCountReceptors(final ProcurementPolicy procurementPolicy) {
    return resultsStatisticsFormatter.formatStatisticType(resultsSummaries.get(procurementPolicy).getStatistics(),
        ResultStatisticType.COUNT_RECEPTORS);
  }
}
