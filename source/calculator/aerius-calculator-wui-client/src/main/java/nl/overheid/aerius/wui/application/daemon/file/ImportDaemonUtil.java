/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.file;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.domain.importer.ImportFilter;
import nl.overheid.aerius.wui.application.domain.importer.ImportParcel;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;

/**
 * Util class accompanying ImportDaemon
 */
final class ImportDaemonUtil {

  private static final List<CalculationJobType> DERIVABLE_CALCULATION_JOBS = Arrays.asList(CalculationJobType.PROCESS_CONTRIBUTION,
      CalculationJobType.DEPOSITION_SUM);

  private ImportDaemonUtil() {
    // Util class
  }

  static boolean skipImportCalculationJob(final ApplicationContext applicationContext, final Map<FileUploadStatus, ImportParcel> lst) {
    // Don't import if:
    // - Any of the filters disallow calculation job import
    // - The group is not a compatible calculation job import
    // - All in the group do not have calculation options
    // - Any of the options does not contain a valid calculation job type for the current theme.
    final Set<FileUploadStatus> group = lst.keySet();
    return group.stream().anyMatch(v -> v.getFilters().isDisallowed(ImportFilter.CALCULATION_JOB))
        || FileUploadStatus.isIncompatibleCalculationJobImport(group)
        || lst.values().stream().allMatch(v -> v.getCalculationSetOptions() == null)
        || group.stream().anyMatch(v -> !isValidJobType(applicationContext, v));
  }

  private static boolean isValidJobType(final ApplicationContext applicationContext, final FileUploadStatus file) {
    return applicationContext.getConfiguration().isAllowed(file.getCalculationJobType())
        || FileUploadStatus.isArchiveContribution(file);
  }

  static CalculationJobType deriveCalculationJob(final ApplicationContext applicationContext, final SituationComposition situationComposition) {
    final List<CalculationJobType> allowedJobTypes = applicationContext.getConfiguration().getCalculationJobTypes();
    for (final CalculationJobType jobType : DERIVABLE_CALCULATION_JOBS) {
      if (allowedJobTypes.contains(jobType) && isJobDerivable(jobType, situationComposition)) {
        return jobType;
      }
    }
    return null;
  }

  private static boolean isJobDerivable(final CalculationJobType jobType, final SituationComposition situationComposition) {
    for (final SituationType situationType : SituationType.values()) {
      final Set<SituationContext> situationsOfType = situationComposition.getSituationsByType(situationType);
      if (situationsOfType.isEmpty() && jobType.isRequired(situationType)) {
        return false;
      }
      if (!situationsOfType.isEmpty() && jobType.isIllegal(situationType)) {
        return false;
      }
      if (situationsOfType.size() > 1
          && (jobType.isRequired(situationType) || jobType.isOptional(situationType))
          && !jobType.isPlural(situationType)) {
        return false;
      }
    }
    return true;
  }
}
