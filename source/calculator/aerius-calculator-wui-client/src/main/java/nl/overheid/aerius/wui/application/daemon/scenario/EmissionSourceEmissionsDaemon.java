/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.scenario;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.geom.Polygon;

import elemental2.core.JsArray;

import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.util.GWTAtomicInteger;
import nl.aerius.wui.util.NotificationUtil;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.command.situation.SituationRefreshEmissionsCommand;
import nl.overheid.aerius.wui.application.context.EmissionSourceEmissionsContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.ItemWithStringIdUtil;
import nl.overheid.aerius.wui.application.domain.source.AbstractSubSource;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSubSourceFeature;
import nl.overheid.aerius.wui.application.event.situation.SituationChangedYearEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceAddedEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceChangeEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceUpdatedEvent;
import nl.overheid.aerius.wui.application.geo.util.GeoType;
import nl.overheid.aerius.wui.application.geo.util.GeoUtil;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.service.AeriusRequestCallback;
import nl.overheid.aerius.wui.application.service.SourceServiceAsync;

/**
 * Daemon in charge of keeping emissions for emission sources up to date.
 *
 * TODO: Refactor so the following design principles are met:
 * - Multiple sources can be edited simultaneously without race conditions occurring
 * - Sources can be rapidly edited without service calls rapidly firing for each edit
 * - Robust against out-of-order service calls (either they do not occur, or they are accounted for)
 */
public class EmissionSourceEmissionsDaemon extends BasicEventComponent {
  private static final EmissionSourceEmissionsDaemonEventBinder EVENT_BINDER = GWT.create(EmissionSourceEmissionsDaemonEventBinder.class);

  interface EmissionSourceEmissionsDaemonEventBinder extends EventBinder<EmissionSourceEmissionsDaemon> {}

  private static class UpdatableBundle {
    private EmissionSourceFeature source;
    private int year;

    public static UpdatableBundle of(final EmissionSourceFeature source, final int year) {
      final UpdatableBundle bundle = new UpdatableBundle();
      bundle.source = source;
      bundle.year = year;
      return bundle;
    }
  }

  private static final List<EmissionSourceType> YEAR_DEPENDANT_TYPES = Arrays.asList(
      EmissionSourceType.SRM1_ROAD, EmissionSourceType.SRM2_ROAD, EmissionSourceType.ADMS_ROAD,
      EmissionSourceType.SHIPPING_INLAND, EmissionSourceType.SHIPPING_INLAND_DOCKED,
      EmissionSourceType.SHIPPING_MARITIME_INLAND, EmissionSourceType.SHIPPING_MARITIME_MARITIME, EmissionSourceType.SHIPPING_MARITIME_DOCKED);

  @Inject private EmissionSourceEmissionsContext emissionsContext;
  @Inject private SourceServiceAsync sourceServiceAsync;

  /**
   * Because sources can be rapidly updated, these refreshes are deferred
   */
  private final Map<String, UpdatableBundle> deferredSources = new HashMap<>();

  private final GWTAtomicInteger serviceCallCounter = new GWTAtomicInteger();
  private final GWTAtomicInteger serviceResultCounter = new GWTAtomicInteger();

  @EventHandler
  public void onEmissionSourceAddedEvent(final EmissionSourceAddedEvent event) {
    handleSourceChangeEventDeferred(event);
  }

  @EventHandler
  public void onEmissionSourceUpdatedEvent(final EmissionSourceUpdatedEvent event) {
    handleSourceChangeEventDeferred(event);
  }

  @EventHandler
  public void onYearChangedEvent(final SituationChangedYearEvent event) {
    updateSituationEmissions(event.getValue());
  }

  @EventHandler
  public void onRefreshEmissionsEvent(final SituationRefreshEmissionsCommand event) {
    updateSituationEmissions(event.getValue());
  }

  private void updateSituationEmissions(final SituationContext situation) {
    final List<EmissionSourceFeature> updateableSources = situation.getSources().stream()
        .filter(EmissionSourceEmissionsDaemon::hasUpdatableYearEmissions)
        .collect(Collectors.toList());
    if (!updateableSources.isEmpty()) {
      fetchUpdatedEmissions(updateableSources, situation.getYear());
    }
  }

  private void handleSourceChangeEventDeferred(final EmissionSourceChangeEvent e) {
    final UpdatableBundle bundle = UpdatableBundle.of(e.getValue(), e.getSituation().getYear());
    deferredSources.put(bundle.source.getId(), bundle);
    tryDeferred();
  }

  private void handleSourceChangeEvent(final UpdatableBundle value) {
    if (value.source == null) {
      return;
    }

    completeDefer(value.source);

    // Skip in case the emission source is:
    // - Fully local
    // - Temporary (i.e. being edited)
    // - Definitely invalid (i.e. self-intersecting or broken polygon)
    if (isFullyLocalEmissionSource(value.source)
        || ItemWithStringIdUtil.isTemporaryFeature(value.source)
        || Optional.ofNullable(value.source)
            .map(v -> v.getGeometry())
            .filter(GeoType.POLYGON::is)
            .map(g -> (Polygon) g)
            .map(geo -> GeoUtil.isSelfIntersecting(geo)
                || GeoUtil.isBrokenClosedRing(geo))
            .orElse(false)) {
      tryDeferred();
      return;
    }

    // Fetch emission update
    fetchUpdatedEmissions(Arrays.asList(value.source), value.year);
  }

  private static boolean isFullyLocalEmissionSource(final EmissionSourceFeature source) {
    if (source.getEmissionSourceType() == EmissionSourceType.MEDIUM_COMBUSTION_PLANT) {
      // Remind through console that MEDIUM_COMBUSTION_PLANT is not being updated.
      // TODO Remove this check when removing the below exclusion, when this source type is supported on the server
      GWTProd.warn("Not updating emissions for MEDIUM_COMBUSTION_PLANT source.");
    }

    /*
     * For now update all non-generic emissions.
     *
     * For some other source types it might be possible to do the update in frontend,
     * but that should still be handled in this daemon. That way, there is one spot to update emissions.
     *
     * Even in simple cases I would keep calling the server to update emissions however:
     * If it's a simple calculation, it'll be a fast response, and it won't impact the server/user too much.
     * And if it's complex, implementing the emission calculation in a second location (frontend)
     * can cause slight differences, probably needs quite some data and needs to be maintained...
     */
    return source.getEmissionSourceType() == EmissionSourceType.GENERIC
        // Exclude MEDIUM_COMBUSTION_PLANT because these are not yet supported on the server
        || source.getEmissionSourceType() == EmissionSourceType.MEDIUM_COMBUSTION_PLANT;
  }

  private static boolean hasUpdatableYearEmissions(final EmissionSourceFeature source) {
    return YEAR_DEPENDANT_TYPES.contains(source.getEmissionSourceType());
  }

  private void fetchUpdatedEmissions(final List<EmissionSourceFeature> sources, final int year) {
    serviceCallCounter.incrementAndGet();
    emissionsContext.setLoading(true);
    sourceServiceAsync.refreshEmissions(sources, year, AeriusRequestCallback.create(
        results -> updateEmissions(sources, results),
        this::exceptionHandling));
  }

  private void updateEmissions(final List<EmissionSourceFeature> existingSources, final List<EmissionSourceFeature> updatedSources) {
    if (updatedSources.size() == existingSources.size()) {
      for (int i = 0; i < existingSources.size(); i++) {
        updateEmissionsSource(existingSources.get(i), updatedSources.get(i));
      }
    } else {
      NotificationUtil.broadcastError(eventBus, M.messages().notificationSourcesUpdatedBeforeEmissionsCalculated());
    }
    handledServiceCall();
  }

  private void updateEmissionsSource(final EmissionSourceFeature existingSource, final EmissionSourceFeature updatedSource) {
    if (existingSource == null || updatedSource == null || !existingSource.getId().equals(updatedSource.getId())) {
      return;
    }

    // For now update the emissions of the existing source, just to check if that works.
    // The entire object could be replaced I guess, but this seems just as secure.
    existingSource.setEmissions(updatedSource.getEmissions());
    // Update subsources for those sources that have those (currently all but generic)
    if (existingSource.getEmissionSourceType() != EmissionSourceType.GENERIC) {
      updateEmissionsForSubSources(
          ((EmissionSubSourceFeature<?>) existingSource).getSubSources(),
          ((EmissionSubSourceFeature<?>) updatedSource).getSubSources(),
          updatedSource.getLabel());
    }
  }

  private void updateEmissionsForSubSources(final JsArray<? extends AbstractSubSource> existingSubSources,
      final JsArray<? extends AbstractSubSource> updatedSubSources, final String sourceLabel) {
    if (existingSubSources == null || updatedSubSources == null) {
      return;
    }
    if (existingSubSources.length != updatedSubSources.length) {
      NotificationUtil.broadcastError(eventBus, M.messages().notificationSourceUpdatedBeforeEmissionsCalculated(sourceLabel));
      return;
    }
    for (int i = 0; i < existingSubSources.length; i++) {
      existingSubSources.getAt(i).setEmissions(updatedSubSources.getAt(i).getEmissions());
    }
  }

  private void exceptionHandling(final Throwable e) {
    NotificationUtil.broadcastError(eventBus, e);
    handledServiceCall();
  }

  private void completeDefer(final EmissionSourceFeature source) {
    deferredSources.remove(source.getId());
  }

  private void tryDeferred() {
    if (deferredSources.isEmpty()) {
      return;
    }

    if (emissionsContext.isLoading()) {
      return;
    }

    handleSourceChangeEvent(deferredSources.values().iterator().next());
  }

  private void handledServiceCall() {
    final int count = serviceResultCounter.incrementAndGet();
    if (count == serviceCallCounter.get()) {
      emissionsContext.setLoading(false);
    }
    tryDeferred();
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
