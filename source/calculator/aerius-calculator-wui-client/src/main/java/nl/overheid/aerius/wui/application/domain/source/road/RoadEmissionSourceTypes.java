/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.road;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;

/**
 * Util class to check on both types of road emission sources.
 */
public final class RoadEmissionSourceTypes {

  private static final Set<EmissionSourceType> ROAD_SOURCE_TYPES = new HashSet<>(Arrays.asList(
    EmissionSourceType.SRM1_ROAD, EmissionSourceType.SRM2_ROAD, EmissionSourceType.ADMS_ROAD
  ));

  /**
   * Returns whether if a feature is a road feature
   * @param feature feature to check
   * @return true if feature is a road feature
   */
  public static boolean isRoadSource(final EmissionSourceFeature feature) {
    return ROAD_SOURCE_TYPES.contains(feature.getEmissionSourceType());
  }

}
