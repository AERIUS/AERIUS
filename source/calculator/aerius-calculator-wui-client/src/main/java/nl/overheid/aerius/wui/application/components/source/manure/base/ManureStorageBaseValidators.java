/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.manure.base;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.aerius.vuelidate.ValidationOptions;
import nl.aerius.vuelidate.Validations;
import nl.aerius.vuelidate.ValidatorBuilder;
import nl.overheid.aerius.shared.ImaerConstants;

/**
 * Validators for ManureStorage editing.
 */
class ManureStorageBaseValidators extends ValidationOptions<ManureStorageBaseEmissionComponent> {

  protected static final int MIN_TONNES = 0;
  protected static final int MAX_TONNES = 1_000_000;
  protected static final int MIN_METERS_SQUARED = 0;
  protected static final int MAX_METERS_SQUARED = 1_000_000;
  protected static final int MIN_NUMBER_OF_DAYS = 0;
  protected static final int MAX_NUMBER_OF_DAYS = ImaerConstants.DAYS_PER_YEAR;

  @JsType(isNative = true, namespace = JsPackage.GLOBAL)
  static class ManureStorageBaseValidations extends Validations {
    public @JsProperty Validations tonnesV;
    public @JsProperty Validations metersSquaredV;
    public @JsProperty Validations numberOfDaysV;
  }

  @Override
  protected void constructValidators(final Object options, final ValidatorInstaller v) {
    final ManureStorageBaseEmissionComponent instance = Js.uncheckedCast(options);

    v.install("tonnesV", () -> instance.tonnesV = null, ValidatorBuilder.create()
        .required()
        .decimal()
        .minValue(MIN_TONNES)
        .maxValue(MAX_TONNES));
    v.install("metersSquaredV", () -> instance.metersSquaredV = null, ValidatorBuilder.create()
        .required()
        .decimal()
        .minValue(MIN_METERS_SQUARED)
        .maxValue(MAX_METERS_SQUARED));
    v.install("numberOfDaysV", () -> instance.numberOfDaysV = null, ValidatorBuilder.create()
        .required()
        .integer()
        .minValue(MIN_NUMBER_OF_DAYS)
        .maxValue(MAX_NUMBER_OF_DAYS));
  }
}
