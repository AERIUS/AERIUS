/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.context;

import java.util.ArrayList;

import javax.inject.Singleton;

import nl.overheid.aerius.wui.application.domain.calculationpoint.CalculationPointPlacementResult;

/**
 * Context for automatic placement of calculation points
 */
@Singleton
public class CalculationPointAutomaticPlacementContext {

  public enum PlacementStatus {
    INFO,
    WARN;
  }

  private boolean automaticPlacementOpen = false;
  private String placementMessage = "";
  private boolean isDetermining = false;
  private CalculationPointPlacementResult placementResult = createEmptyPlacementResult();
  private PlacementStatus placementStatus = null;

  public boolean isAutomaticPlacementOpen() {
    return automaticPlacementOpen;
  }

  public void setAutomaticPlacementOpen(final boolean automaticPlacementOpen) {
    this.automaticPlacementOpen = automaticPlacementOpen;
  }

  public String getPlacementMessage() {
    return placementMessage;
  }

  public void setPlacementMessage(final String placementMessage) {
    this.placementMessage = placementMessage;
  }

  public PlacementStatus getPlacementStatus() {
    return placementStatus;
  }

  public void setPlacementStatus(final PlacementStatus placementStatus) {
    this.placementStatus = placementStatus;
  }

  public boolean isDetermining() {
    return isDetermining;
  }

  public void setDetermining(final boolean determining) {
    isDetermining = determining;
  }

  public CalculationPointPlacementResult getPlacementResult() {
    return placementResult;
  }

  public void setPlacementResult(final CalculationPointPlacementResult placementResult) {
    this.placementResult = placementResult;
  }

  public static CalculationPointPlacementResult createEmptyPlacementResult() {
    final CalculationPointPlacementResult result = new CalculationPointPlacementResult();
    result.setComputedCalculationPoints(new ArrayList<>());
    return result;
  }
}
