/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source.road;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import ol.OLFactory;
import ol.geom.LineString;
import ol.style.Style;

import nl.aerius.geo.domain.legend.Legend;
import nl.aerius.geo.domain.legend.LegendType;
import nl.aerius.geo.wui.util.OL3GeometryUtil;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.constants.DrivingSide;
import nl.overheid.aerius.shared.domain.legend.ColorRange;
import nl.overheid.aerius.shared.domain.legend.ColorRangeType;
import nl.overheid.aerius.shared.domain.legend.ColorRangesLegend;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.shared.domain.v2.source.road.TrafficDirection;
import nl.overheid.aerius.wui.application.domain.source.RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.road.CustomVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.SpecificVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.StandardVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.ValuesPerVehicleType;
import nl.overheid.aerius.wui.application.domain.source.road.VehicleType;
import nl.overheid.aerius.wui.application.domain.source.road.Vehicles;
import nl.overheid.aerius.wui.application.geo.layers.source.RoadSourceGeometryLayer;
import nl.overheid.aerius.wui.application.i18n.M;

public class RoadTrafficVolumeStyle implements RoadStyle {

  private static final int ROAD_CENTERLINE_WIDTH = 1;
  private static final Style ROAD_CENTERLINE_STYLE = OLFactory
      .createStyle(OLFactory.createStroke(OLFactory.createColor("white"), ROAD_CENTERLINE_WIDTH));
  private static final int ROAD_WIDTH_INCREASE_FACTOR = 2;

  private List<ColorRange> colorRange;
  private Map<String, List<ColorRange>> colorRanges = new HashMap<>();

  private final DrivingSide drivingSide;
  private double totalVehicles;

  public RoadTrafficVolumeStyle(final Function<ColorRangeType, List<ColorRange>> colorRangeFunction,
      final ApplicationConfiguration appThemeConfiguration, final DrivingSide drivingSide) {
    colorRange = colorRangeFunction.apply(ColorRangeType.ROAD_TRAFFIC_VOLUME);
    colorRanges = appThemeConfiguration.getRoadTrafficVolumeColorRanges();
    this.drivingSide = drivingSide;
  }

  @Override
  public List<Style> getStyle(final RoadESFeature feature, final String vehicleTypeCode, final double resolution) {
    final int trafficVolume;
    if (vehicleTypeCode.isEmpty()) {
      trafficVolume = (int) feature.getSubSources().asList().stream()
          .mapToDouble(v -> this.getTotalSaveVehiclesPer(v))
          .sum();
    } else {
      trafficVolume = (int) feature.getSubSources().asList().stream()
          .filter(v -> v.getVehicleType() == VehicleType.STANDARD)
          .map(v -> ((StandardVehicles) v))
          .mapToDouble(v -> getSafeVehiclesPer(v.getValuesPerVehicleType(vehicleTypeCode), v.getTimeUnit()))
          .sum();
    }

    final ColorRange range = colorRange.stream()
        .sorted(Comparator.reverseOrder())
        .filter(v -> trafficVolume >= v.getLowerValue())
        .findFirst().orElse(colorRange.get(0));

    final int lineWidth = getRoadWidth(range);
    double lineOffset = 0;

    // Render the line one side of the line
    if (feature.getTrafficDirection() != null && feature.getTrafficDirection() != TrafficDirection.BOTH) {
      lineOffset = (lineWidth - 2 * ROAD_CENTERLINE_WIDTH) * resolution * 0.5;

      boolean drawRightSide = drivingSide == DrivingSide.RIGHT;
      if (feature.getTrafficDirection() == TrafficDirection.B_TO_A) {
        drawRightSide = !drawRightSide;
      }

      if (!drawRightSide) {
        lineOffset *= -1;
      }
    }

    final Style trafficVolumeStyle = OLFactory.createStyle(OLFactory.createStroke(OLFactory.createColor(range.getColor()), lineWidth));
    trafficVolumeStyle.setGeometry(OL3GeometryUtil.offsetFromLineString((LineString) feature.getGeometry(), lineOffset));

    return Arrays.asList(trafficVolumeStyle, ROAD_CENTERLINE_STYLE);
  }

  private double getTotalSaveVehiclesPer(final Vehicles vehicles) {
    switch (vehicles.getVehicleType()) {
    case CUSTOM:
      return ((CustomVehicles) vehicles).getVehiclesPer(TimeUnit.DAY);
    case SPECIFIC:
      return ((SpecificVehicles) vehicles).getVehiclesPer(TimeUnit.DAY);
    case STANDARD:
      totalVehicles = 0;
      ((StandardVehicles) vehicles).getValuesPerVehicleTypes().forEach(key -> {
        totalVehicles += getSafeVehiclesPer(((StandardVehicles) vehicles).getValuesPerVehicleType(key), ((StandardVehicles) vehicles).getTimeUnit());
      });
      return totalVehicles;
    default:
      throw new RuntimeException("Unknown type: " + vehicles.getVehicleType());
    }
  }

  private double getSafeVehiclesPer(final ValuesPerVehicleType valuesPerVehicleType, final TimeUnit timeUnit) {
    return valuesPerVehicleType == null ? 0D : valuesPerVehicleType.getVehiclesPer(timeUnit, TimeUnit.DAY);
  }

  private int getRoadWidth(final ColorRange range) {
    return colorRange.indexOf(range) * ROAD_WIDTH_INCREASE_FACTOR + RoadSourceGeometryLayer.DEFAULT_ROAD_WIDTH;
  }

  @Override
  public Legend getLegend(final ApplicationConfiguration appThemeConfiguration) {
    final ColorRangesLegend colorRangesLegend = new ColorRangesLegend(colorRange, M.messages().colorRangesLegendBetweenText(), LegendType.ROAD);
    colorRangesLegend.setUnit(M.messages().layerRoadVolumeUnit());
    colorRangesLegend.setIconSizes(colorRange.stream().mapToInt(this::getRoadWidth).boxed().toArray(l -> new Integer[l]));
    return colorRangesLegend;
  }

  public void updateVehicleType(final String vehicleTypeCode) {
    if (!colorRanges.isEmpty()) {
      colorRange = colorRanges.get(vehicleTypeCode.toLowerCase());
    }
  }

}
