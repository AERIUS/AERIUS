/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.misc;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Timer;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.event.NotificationEvent;
import nl.aerius.wui.util.Notification;
import nl.overheid.aerius.wui.application.command.NotificationRemoveCommand;
import nl.overheid.aerius.wui.application.command.NotificationsClearCommand;
import nl.overheid.aerius.wui.application.command.misc.CloseNotificationDisplayCommand;
import nl.overheid.aerius.wui.application.command.misc.ToggleNotificationDisplayCommand;
import nl.overheid.aerius.wui.application.context.NotificationContext;

public class NotificationDaemon extends BasicEventComponent {
  private static final NotificationDaemonEventBinder EVENT_BINDER = GWT.create(NotificationDaemonEventBinder.class);

  interface NotificationDaemonEventBinder extends EventBinder<NotificationDaemon> {}

  private static final int QUICK_DELAY = 2400;
  private static final int REMOVE_DELAY = 6000;

  @Inject private NotificationContext notificationContext;

  private final Timer tempVisibleTimer = new Timer() {
    @Override
    public void run() {
      notificationContext.setShowing(false);
    }
  };

  @EventHandler
  public void onNotificationEvent(final NotificationEvent e) {
    final Notification notification = e.getValue();
    notificationContext.addNotification(notification);

    if (!notification.isSilent()) {
      notificationContext.setShowing(true);
      tempVisibleTimer.schedule(REMOVE_DELAY);
    }
  }

  @EventHandler
  public void onNotificationsClearCommand(final NotificationsClearCommand c) {
    notificationContext.reset();
    tempVisibleTimer.schedule(QUICK_DELAY);
  }

  @EventHandler
  public void onRemoveNotificationCommand(final NotificationRemoveCommand c) {
    notificationContext.removeNotification(c.getValue());
    if (notificationContext.getNotificationCount() == 0) {
      tempVisibleTimer.schedule(QUICK_DELAY);
    }
  }

  @EventHandler
  public void onToggleNotificationDisplayCommand(final ToggleNotificationDisplayCommand c) {
    notificationContext.setShowing(!notificationContext.isShowing());

    if (notificationContext.isShowing() && notificationContext.getNotificationCount() == 0) {
      tempVisibleTimer.schedule(QUICK_DELAY);
    }
  }

  @EventHandler
  public void onCloseNotificationDisplayCommand(final CloseNotificationDisplayCommand c) {
    notificationContext.setShowing(false);
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
