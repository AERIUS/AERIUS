/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.source.farmland.custom;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.farmland.custom.FarmlandCustomEmissionValidators.FarmlandCustomEmissionValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.domain.source.farmland.CustomFarmlandActivity;
import nl.overheid.aerius.wui.vue.DebugDirective;

/**
 * Shows an emission editor with label for farmland.
 */
@Component(name = "aer-farmland-custom-emission-row", customizeOptions = FarmlandCustomEmissionValidators.class, directives = {
    ValidateDirective.class,
    DebugDirective.class
}, components = {
    ValidationBehaviour.class,
    LabeledInputComponent.class
})
public class FarmlandCustomDetailEmissionRowComponent extends ErrorWarningValidator implements IsVueComponent,
  HasCreated, HasValidators<FarmlandCustomEmissionValidations> {
  @Prop(required = true) Substance substance;
  @Prop(required = true) CustomFarmlandActivity farmlandActivity;
  @Data String emissionV;

  @JsProperty(name = "$v") FarmlandCustomEmissionValidations validation;

  @Watch(value = "farmlandActivity", isImmediate = true)
  public void onFarmlandActivityChange() {
    emissionV = String.valueOf(farmlandActivity.getEmission(substance));
  }

  @Computed
  public FarmlandCustomEmissionValidations getV() {
    return validation;
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Computed
  public String getEmission() {
    return emissionV;
  }

  @Computed
  public void setEmission(final String emission) {
    ValidationUtil.setSafeDoubleValue(em -> farmlandActivity.setEmission(substance, em), emission, 0D);
    emissionV = emission;
  }

  @JsMethod
  public String emissionConversionError() {
    return i18n.errorDecimal(emissionV);
  }
}
