/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.farmanimalhousing.standard;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import elemental2.core.JsArray;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.FarmAdditionalHousingSystemCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalHousingCategories;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalHousingCategory;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;
import nl.overheid.aerius.wui.application.components.input.InputWarningComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.MinimalInputComponent;
import nl.overheid.aerius.wui.application.components.input.suggest.SuggestListBoxComponent;
import nl.overheid.aerius.wui.application.components.input.suggest.SuggestListBoxData;
import nl.overheid.aerius.wui.application.components.source.farmanimalhousing.detail.FarmAnimalHousingDetailHeaderComponent;
import nl.overheid.aerius.wui.application.components.source.farmanimalhousing.standard.FarmAnimalHousingStandardValidators.FarmAnimalHousingStandardValidations;
import nl.overheid.aerius.wui.application.components.source.farmanimalhousing.standard.additional.AnimalStandardAdditionalRowComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.farm.AdditionalHousingSystem;
import nl.overheid.aerius.wui.application.domain.source.farm.AdditionalSystemType;
import nl.overheid.aerius.wui.application.domain.source.farm.CustomAdditionalHousingSystem;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmAnimalHousingUtil;
import nl.overheid.aerius.wui.application.domain.source.farm.StandardAdditionalHousingSystem;
import nl.overheid.aerius.wui.application.domain.source.farm.StandardFarmAnimalHousing;
import nl.overheid.aerius.wui.vue.DebugDirective;

/**
 * Component to edit the generic details of a StandardFarmAnimalHousing sub source.
 */
@Component(customizeOptions = {
    FarmAnimalHousingStandardValidators.class
}, directives = {
    ValidateDirective.class,
    DebugDirective.class
}, components = {
    AnimalStandardAdditionalRowComponent.class,
    FarmAnimalHousingDetailHeaderComponent.class,
    FarmAnimalHousingDetailStandardComponent.class,
    InputWarningComponent.class,
    LabeledInputComponent.class,
    MinimalInputComponent.class,
    SuggestListBoxComponent.class,
    ValidationBehaviour.class,
})
public class FarmAnimalHousingStandardEmissionComponent extends ErrorWarningValidator
    implements HasCreated, HasValidators<FarmAnimalHousingStandardValidations> {

  private static final String CUSTOM_AIRSCRUBBER = "custom-airscrubber";
  private static final String CUSTOM_OTHER = "custom-other";

  @Prop @JsProperty StandardFarmAnimalHousing standardFarmAnimalHousing;

  @Inject @Data ApplicationContext applicationContext;

  @Ref SuggestListBoxComponent additionalTechniqueSuggestListBox;

  @Data String farmAnimalHousingCodeV;
  @Data String numberOfAnimalsV;
  @Data String numberOfDaysV;

  @JsProperty(name = "$v") FarmAnimalHousingStandardValidations validation;

  @Data FarmAnimalHousingCategory selectedFarmAnimalHousingCategory;
  @Data @JsProperty List<SuggestListBoxData> additionalSystemsOptions = new ArrayList<>();

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Override
  @Computed
  public FarmAnimalHousingStandardValidations getV() {
    return validation;
  }

  @Watch(value = "standardFarmAnimalHousing", isImmediate = true)
  protected void onStandardFarmAnimalHousingChange(final StandardFarmAnimalHousing neww) {
    if (neww == null) {
      return;
    }

    farmAnimalHousingCodeV = neww.getAnimalHousingCode();
    updateFarmAnimalHousingSystemFromCode(neww.getAnimalHousingCode());
    numberOfAnimalsV = String.valueOf(neww.getNumberOfAnimals());
    numberOfDaysV = String.valueOf(neww.getNumberOfDays());
  }

  @Watch(value = "farmAnimalHousingCodeV", isImmediate = true)
  protected void updateAdditionalSystemsOptions(final String farmAnimalHousingCodeV) {
    additionalSystemsOptions.clear();
    additionalSystemsOptions.add(new SuggestListBoxData(CUSTOM_AIRSCRUBBER, i18n.esFarmAnimalHousingStandardAdditionalCustomAirScrubber(),
        i18n.esFarmAnimalHousingStandardAdditionalCustomAirScrubber(), false));
    additionalSystemsOptions.add(new SuggestListBoxData(CUSTOM_OTHER, i18n.esFarmAnimalHousingStandardAdditionalCustomOther(),
        i18n.esFarmAnimalHousingStandardAdditionalCustomOther(), false));
    if (farmAnimalHousingCodeV != null) {
      final Set<String> allowedCodes = getCategories().getHousingAllowedAdditionalSystems().get(farmAnimalHousingCodeV);

      if (allowedCodes != null) {
        additionalSystemsOptions.addAll(allowedCodes.stream().map(code -> getCategories().determineAdditionalHousingCategoryByCode(code))
            .sorted((o1, o2) -> o1.getName().compareTo(o2.getName()))
            .map(v -> new SuggestListBoxData(v.getCode(), v.getName(), v.getName() + " " + v.getDescription(), false))
            .collect(Collectors.toList()));
      }
    }
  }

  @Computed("standardCodeWarningEnabled")
  protected boolean isStandardCodeWarningEnabled() {
    return applicationContext.isAppFlagEnabled(AppFlag.SHOW_FARM_ANIMAL_HOUSING_STANDARD_CODE_WARNING);
  }

  @Computed
  protected List<SuggestListBoxData> getFarmAnimalHousingCodes() {
    return getCategories().getAnimalHousingCategories().stream()
        .map(v -> new SuggestListBoxData(v.getCode(), v.getName(), v.getName() + " " + v.getDescription(), false))
        .collect(Collectors.toList());
  }

  private void updateFarmAnimalHousingSystemFromCode(final String code) {
    selectedFarmAnimalHousingCategory = getCategories().determineHousingCategoryByCode(code);
    final FarmAnimalCategory animalCategory = selectedFarmAnimalHousingCategory == null ? null
        : getCategories().determineAnimalCategoryByCode(selectedFarmAnimalHousingCategory.getAnimalCategoryCode());

    standardFarmAnimalHousing.setAnimalTypeCode(animalCategory == null ? "" : animalCategory.getCode());
  }

  @Watch(value = "selectedFarmAnimalHousingCategory", isImmediate = true)
  protected void onSelectedFarmAnimalHousingCategory(final FarmAnimalHousingCategory neww) {
    if (neww == null) {
      farmAnimalHousingCodeV = null;
    } else {
      farmAnimalHousingCodeV = neww.getCode();
    }
  }

  @JsMethod
  protected FarmAnimalHousingCategories getCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getFarmAnimalHousingCategories();
  }

  @JsMethod
  protected boolean hasNumberOfDays() {
    return selectedFarmAnimalHousingCategory != null
        && selectedFarmAnimalHousingCategory.getFarmEmissionFactorType() == FarmEmissionFactorType.PER_ANIMAL_PER_DAY;
  }

  @JsMethod
  protected boolean hasAdditionalHousingSystems() {
    return standardFarmAnimalHousing.getAdditionalSystems().length > 0;
  }

  @Computed
  protected String getNumberOfAnimals() {
    return numberOfAnimalsV;
  }

  @Computed
  protected void setNumberOfAnimals(final String numberOfAnimals) {
    ValidationUtil.setSafeIntegerValue(v -> standardFarmAnimalHousing.setNumberOfAnimals(v), numberOfAnimals, 0);
    numberOfAnimalsV = numberOfAnimals;
  }

  @Computed("isNumberOfDaysEnabled")
  protected boolean isNumberOfDaysEnabled() {
    return FarmAnimalHousingUtil.hasAnimalsPerDay(selectedFarmAnimalHousingCategory);
  }

  @Computed
  protected String getNumberOfDays() {
    return numberOfDaysV;
  }

  @Computed
  protected void setNumberOfDays(final String numberOfDays) {
    ValidationUtil.setSafeIntegerValue(v -> standardFarmAnimalHousing.setNumberOfDays(v), numberOfDays, 0);
    numberOfDaysV = numberOfDays;
  }

  @JsMethod
  protected String numberOfAnimalsError() {
    return i18n.errorNumeric(numberOfAnimalsV);
  }

  @JsMethod
  protected String numberOfDaysError() {
    return numberOfDaysV == null || numberOfDaysV.isEmpty()
        ? i18n.validationRequired()
        : i18n.ivIntegerRangeBetween(numberOfDaysV, FarmAnimalHousingStandardValidators.MIN_NUMBER_OF_DAYS,
            FarmAnimalHousingStandardValidators.MAX_NUMBER_OF_DAYS);
  }

  @JsMethod
  protected void unselectFarmAnimalHousingCode() {
    standardFarmAnimalHousing.setAnimalHousingCode(null);
    selectedFarmAnimalHousingCategory = null;
  }

  @JsMethod
  protected void selectFarmAnimalHousingCode(final String code) {
    standardFarmAnimalHousing.setAnimalHousingCode(code);
    updateFarmAnimalHousingSystemFromCode(code);
  }

  @Computed
  protected List<SuggestListBoxData> getAdditionalSystemsCodes() {
    return additionalSystemsOptions;
  }

  @Computed
  protected List<AdditionalHousingSystem> getAdditionalSystems() {
    return standardFarmAnimalHousing.getAdditionalSystems().asList();
  }

  @JsMethod
  protected void selectAdditionalCode(final String code) {
    final boolean customAirScrubber = CUSTOM_AIRSCRUBBER.equals(code);
    final boolean customOther = CUSTOM_OTHER.equals(code);

    if (customAirScrubber || customOther) {
      final CustomAdditionalHousingSystem custom = CustomAdditionalHousingSystem.create();

      custom.setAirScrubber(customAirScrubber);
      standardFarmAnimalHousing.getAdditionalSystems().push(custom);
    } else {
      final FarmAdditionalHousingSystemCategory ac = getCategories().determineAdditionalHousingCategoryByCode(code);

      if (ac != null) {
        final StandardAdditionalHousingSystem standard = StandardAdditionalHousingSystem.create();
        standard.setAdditionalSystemCode(code);
        standardFarmAnimalHousing.getAdditionalSystems().push(standard);
      }
    }
    additionalTechniqueSuggestListBox.setValue(null);
  }

  @Computed("hasLingeringAdditionalValue")
  protected boolean hasLingeringAdditionalTechniqueValue() {
    final String additionalTechniqueValue = additionalTechniqueSuggestListBox == null ? null : additionalTechniqueSuggestListBox.getValue();
    return additionalTechniqueValue != null && !additionalTechniqueValue.isEmpty();
  }

  @JsMethod
  protected String lingeringAdditionalTechniqueError() {
    return i18n.esFarmAnimalHousingLingeringCodeError(additionalTechniqueSuggestListBox == null ? "" : additionalTechniqueSuggestListBox.getValue());
  }

  @Computed("isInvalid")
  protected boolean isInvalid() {
    return validation.farmAnimalHousingCodeV.invalid
        || validation.numberOfAnimalsV.invalid
        || (isNumberOfDaysEnabled() && validation.numberOfDaysV.invalid)
        || hasLingeringAdditionalTechniqueValue()
        || hasAdditionalErrors()
        || FarmAnimalHousingUtil.hasIncompatibleCombinations(standardFarmAnimalHousing, getCategories());
  }

  @JsMethod
  protected void deleteAdditional(final int index) {
    removeAtIndex(index, standardFarmAnimalHousing.getAdditionalSystems(), "additional-");
  }

  private boolean hasAdditionalErrors() {
    return standardFarmAnimalHousing.getAdditionalSystems().asList()
        .stream()
        .anyMatch(x -> !isValid(x));
  }

  private boolean isValid(final AdditionalHousingSystem system) {
    if (system.getAdditionalSystemType() == AdditionalSystemType.CUSTOM) {
      final CustomAdditionalHousingSystem customSystem = (CustomAdditionalHousingSystem) system;
      final double reductionFactor = customSystem.getEmissionReductionFactor(Substance.NH3);
      return customSystem.getDescription() != null && !customSystem.getDescription().isEmpty()
          && reductionFactor >= 0.0 && reductionFactor <= 100.0;
    } else {
      final StandardAdditionalHousingSystem standardSystem = (StandardAdditionalHousingSystem) system;
      return standardSystem.getAdditionalSystemCode() != null;
    }
  }

  private void removeAtIndex(final int index, final JsArray<?> dataSource, final String prefix) {
    if (index == dataSource.length - 1) {
      dataSource.pop();
    } else {
      dataSource.splice(index, 1);
    }

    pruneChildProblems(prefix, dataSource.length);
  }

  private void pruneChildProblems(final String prefix, final int length) {
    pruneChildErrors(prefix, length);
    pruneChildWarnings(prefix, length);
  }

  private void pruneChildErrors(final String prefix, final int length) {
    if (removeChildError(prefix + length)) {
      pruneChildErrors(prefix, length + 1);
    }
  }

  private void pruneChildWarnings(final String prefix, final int length) {
    if (removeChildWarning(prefix + length)) {
      pruneChildWarnings(prefix, length + 1);
    }
  }
}
