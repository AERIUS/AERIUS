/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.job;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.google.inject.assistedinject.Assisted;

import ol.Collection;
import ol.layer.Base;
import ol.layer.Group;
import ol.layer.LayerGroupOptions;

import nl.aerius.geo.domain.IsLayer;
import nl.aerius.geo.domain.LayerInfo;
import nl.aerius.geo.domain.legend.Legend;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.domain.legend.TextLabelsLegend;
import nl.overheid.aerius.wui.application.geo.layers.FeatureType;
import nl.overheid.aerius.wui.application.geo.layers.IsMarkerLayer;
import nl.overheid.aerius.wui.application.geo.layers.LayerFactory;
import nl.overheid.aerius.wui.application.i18n.M;

public class JobInputLayerGroup implements IsMarkerLayer<Group> {
  private final LayerInfo info;
  private final Group layer;

  private final List<IsLayer<?>> markerLayers = new ArrayList<>();

  @Inject
  public JobInputLayerGroup(final LayerFactory layerFactory, @Assisted final int zIndex,
      @Assisted final ApplicationConfiguration appThemeConfiguration) {
    this.info = new LayerInfo();
    this.info.setName(this.getClass().getCanonicalName());
    this.info.setTitle(M.messages().layerJobInput());
    this.info.setLegend(constructLegend(appThemeConfiguration));

    int layersZIndex = 0;

    final Collection<Base> layers = new Collection<>();
    layers.push(layerFactory.createJobInputGeometryLayer(info, layersZIndex++).asLayer());

    markerLayers.add(layerFactory.createJobInputMarkerLayer(info, FeatureType.SOURCE, layersZIndex++));
    markerLayers.add(layerFactory.createJobInputMarkerLayer(info, FeatureType.BUILDING, layersZIndex++));

    markerLayers.forEach(ml -> layers.push((Base) ml.asLayer()));

    final LayerGroupOptions groupOptions = new LayerGroupOptions();
    groupOptions.setLayers(layers);

    layer = new Group(groupOptions);
    layer.setVisible(false);
    layer.setZIndex(zIndex);
  }

  @Override
  public Group asLayer() {
    return layer;
  }

  @Override
  public LayerInfo getInfo() {
    return info;
  }

  @Override
  public List<IsLayer<?>> getMarkerLayers() {
    return markerLayers;
  }

  private static Legend constructLegend(final ApplicationConfiguration appThemeConfiguration) {
    final List<String> items = new ArrayList<>();
    final List<String> descriptions = new ArrayList<>();
    for (final SituationType situationType : appThemeConfiguration.getAvailableSituationTypes()) {
      items.add(M.messages().layerJobSituationTypeIndicator(situationType).substring(0, 1));
      descriptions.add(M.messages().situationType(situationType));
    }
    return new TextLabelsLegend(
        items.toArray(new String[items.size()]),
        descriptions.toArray(new String[descriptions.size()]));
  }
}
