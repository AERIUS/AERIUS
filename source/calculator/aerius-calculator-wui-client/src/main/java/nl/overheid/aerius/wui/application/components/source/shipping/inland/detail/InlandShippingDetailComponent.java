/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.shipping.inland.detail;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.components.divider.DividerComponent;
import nl.overheid.aerius.wui.application.components.source.DetailStatComponent;
import nl.overheid.aerius.wui.application.components.source.generic.GenericDetailComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.InlandShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.MooringInlandShippingESFeature;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "aer-inland-shipping-detail", components = {
    GenericDetailComponent.class,
    VerticalCollapse.class,
    VerticalCollapseGroup.class,
    DetailStatComponent.class,
    DividerComponent.class
})
public class InlandShippingDetailComponent extends BasicVueComponent {
  @Inject @Data protected ApplicationContext applicationContext;
  @Inject @Data ScenarioContext scenarioContext;

  @Prop @JsProperty protected EmissionSourceFeature source;
  @Prop String totalEmissionTitle;

  @Computed
  public List<Substance> getSubstances() {
    return applicationContext.getConfiguration().getSubstances();
  }

  @Computed
  public InlandShippingESFeature getInlandSource() {
    return (InlandShippingESFeature) source;
  }

  @Computed
  public MooringInlandShippingESFeature getMooringInlandSource() {
    return (MooringInlandShippingESFeature) source;
  }

  @JsMethod
  public String getDescriptionFromCode(final String shipCode) {
    final InlandShippingCategory shipCategoryByCode = applicationContext.getConfiguration().getSectorCategories().getInlandShippingCategories()
        .getShipCategoryByCode(shipCode);
    return shipCategoryByCode != null ? shipCategoryByCode.getDescription() : null;
  }

  @JsMethod
  public boolean isESType(final EmissionSourceType type) {
    return source.getEmissionSourceType() == type;
  }

  @JsMethod
  public String getSourceLabel(final String gmlId) {
    return Optional.of(scenarioContext.getActiveSituation().findSource(gmlId))
        .map(feature -> feature.getLabel().isEmpty() ? feature.getGmlId() : feature.getLabel())
        .orElse("");
  }

}
