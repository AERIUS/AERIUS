/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source.road;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import ol.OLFactory;
import ol.style.Style;

import nl.aerius.geo.domain.legend.ColorLabelsLegend;
import nl.aerius.geo.domain.legend.LegendType;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.legend.ColorRange;
import nl.overheid.aerius.shared.domain.legend.ColorRangeType;
import nl.overheid.aerius.shared.domain.legend.ColorRangesLegend;
import nl.overheid.aerius.wui.application.domain.source.RoadESFeature;
import nl.overheid.aerius.wui.application.i18n.M;

public class RoadTunnelFactorStyle implements RoadStyle {

  public static final int DEFAULT_ROAD_TUNNEL_FACTOR_WIDTH = 6;

  private final List<ColorRange> colorRange;

  public RoadTunnelFactorStyle(final Function<ColorRangeType, List<ColorRange>> colorRangeFunction) {
    colorRange = colorRangeFunction.apply(ColorRangeType.ROAD_TUNNELFACTOR).stream()
        .map(r -> new ColorRange(r.getLowerValue(), r.getColor(), determineLabel(r)))
        .collect(Collectors.toList());
  }

  private Map<Double, List<Style>> getStyleMap() {
    return colorRange.stream()
        .collect(Collectors.toMap(
            cr -> (double) cr.getLowerValue(),
            cr -> Collections.singletonList(OLFactory.createStyle(OLFactory.createStroke(OLFactory.createColor(cr.getColor()),
                DEFAULT_ROAD_TUNNEL_FACTOR_WIDTH)))));
  }

  private static String determineLabel(final ColorRange colorRange) {
    if (colorRange.getLowerValue() < 1) {
      return M.messages().esRoadDetailTunnelFactor() + " < 1";
    } else if (colorRange.getLowerValue() > 1) {
      return M.messages().esRoadDetailTunnelFactor() + " > 1";
    } else {
      return M.messages().esRoadDetailTunnelFactor() + " = 1";
    }
  }

  @Override
  public List<Style> getStyle(final RoadESFeature feature, final String vehicleTypeCode, final double resolution) {
    final double tunnelFactor = feature != null ? feature.getTunnelFactor() : 1D;
    final Map<Double, List<Style>> styleMap = getStyleMap();
    final double lowerValueRange = styleMap.keySet().stream()
        .mapToDouble(x -> x)
        .filter(x -> x <= tunnelFactor)
        .max().orElse(0D);
    return styleMap.get(lowerValueRange);
  }

  @Override
  public ColorLabelsLegend getLegend(final ApplicationConfiguration appThemeConfiguration) {
    return new ColorRangesLegend(
        colorRange,
        M.messages().colorRangesLegendBetweenText(),
        LegendType.LINE);
  }
}
