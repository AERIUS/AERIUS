/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.scenario;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.place.PlaceController;
import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.wui.application.command.situation.SwitchSituationCommand;
import nl.overheid.aerius.wui.application.command.source.DeleteAllEmissionSourceCommand;
import nl.overheid.aerius.wui.application.command.source.DeleteAllRoadNetworkSourcesCommand;
import nl.overheid.aerius.wui.application.command.source.DeselectAllFeaturesCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceAddCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceDeleteCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceDeleteSelectedCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceDuplicateSelectedCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditEvent;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditSelectedCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceFeatureDeselectCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceFeatureSelectCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceFeatureToggleSelectCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourcePeekFeatureCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceUnpeekFeatureCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.EmissionSourceListContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.geo.FeatureUtil;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.road.RoadEmissionSourceTypes;
import nl.overheid.aerius.wui.application.event.SelectionResetEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceDeletedEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceListChangeEvent;
import nl.overheid.aerius.wui.application.place.EmissionSourcePlace;

@Singleton
public class EmissionSourceListDaemon extends BasicEventComponent {
  private static final EmissionSourceListDaemonEventBinder EVENT_BINDER = GWT.create(EmissionSourceListDaemonEventBinder.class);

  interface EmissionSourceListDaemonEventBinder extends EventBinder<EmissionSourceListDaemon> {}

  @Inject private PlaceController placeController;

  @Inject private EmissionSourceListContext context;
  @Inject private ScenarioContext scenarioContext;
  @Inject private ApplicationContext applicationContext;

  @EventHandler
  public void onDeselectAllFeaturesCommand(final DeselectAllFeaturesCommand c) {
    resetSelection();
  }

  @EventHandler
  public void onEmissionSourceDuplicateSelectedCommand(final EmissionSourceDuplicateSelectedCommand c) {
    final List<EmissionSourceFeature> newSelection = new ArrayList<>();
    final Set<String> existingLabels = getExistingLabels();

    context.getSelections().forEach(source -> {
      final EmissionSourceFeature copy = FeatureUtil.cloneWithoutIds(source);
      copy.setLabel(FeatureUtil.smartIncrementLabel(copy.getLabel(), existingLabels));
      existingLabels.add(copy.getLabel());

      final SituationContext situation = scenarioContext.getActiveSituation();
      eventBus.fireEvent(new EmissionSourceAddCommand(copy, situation));
      newSelection.add(copy);
    });

    // Delay because of what is described in EmissionSourceEditorDaemon:120
    SchedulerUtil.delay(() -> {
      resetSelection();
      newSelection.forEach(v -> context.addSelection(v));
    });
  }

  private Set<String> getExistingLabels() {
    return scenarioContext.getActiveSituation().getSources().stream()
        .map(source -> source.getLabel())
        .collect(Collectors.toSet());
  }

  @EventHandler
  public void onEmissionSourceDeleteSelectedCommand(final EmissionSourceDeleteSelectedCommand c) {
    if (!context.hasSelection()) {
      return;
    }

    final SituationContext situation = scenarioContext.getActiveSituation();
    context.getSelections().forEach(v -> eventBus.fireEvent(new EmissionSourceDeleteCommand(v, situation)));
    resetSelection();
  }

  @EventHandler
  public void onEmissionSourceFeatureSelectCommand(final EmissionSourceFeatureSelectCommand c) {
    final EmissionSourceFeature source = c.getValue();

    if (c.isMulti()) {
      context.addSelection(source);
    } else {
      context.setSingleSelection(source);
    }
  }

  @EventHandler
  public void onEmissionSourceFeatureDeselectCommand(final EmissionSourceFeatureDeselectCommand c) {
    if (c.getValue() == null) {
      resetSelection();
    } else {
      context.removeSelection(c.getValue());
    }
  }

  @EventHandler
  public void onEmissionSourceFeatureToggleSelectCommand(final EmissionSourceFeatureToggleSelectCommand c) {
    final EmissionSourceFeature source = c.getValue();

    if (context.isSelected(source)) {
      eventBus.fireEvent(new EmissionSourceFeatureDeselectCommand(source));
    } else {
      eventBus.fireEvent(new EmissionSourceFeatureSelectCommand(source, c.isMulti()));
    }
  }

  @EventHandler
  public void onSwitchSituationCommand(final SwitchSituationCommand c) {
    resetSelection();
  }

  @EventHandler
  public void onEmissionSourceDeletedEvent(final EmissionSourceDeletedEvent e) {
    context.removeSelection(e.getValue());
  }

  @EventHandler
  public void onEmissionSourcePeekFeatureCommand(final EmissionSourcePeekFeatureCommand c) {
    context.setPeekSelection(c.getValue());
  }

  @EventHandler
  public void onEmissionSourceUnpeekFeatureCommand(final EmissionSourceUnpeekFeatureCommand c) {
    SchedulerUtil.delay(() -> context.clearPeekSelection(c.getValue()));
  }

  @EventHandler
  public void onEmissionSourceEditSelectedCommand(final EmissionSourceEditSelectedCommand c) {
    if (!context.hasSelection()) {
      return;
    }

    eventBus.fireEvent(new EmissionSourceEditCommand(context.getSingleSelection()));
  }

  @EventHandler
  public void onEmissionSourceEditEvent(final EmissionSourceEditEvent c) {
    final EmissionSourcePlace place = new EmissionSourcePlace(applicationContext.getTheme());
    place.setEmissionSourceId(c.getValue().getId());

    resetSelection();
    placeController.goTo(place);
  }

  @EventHandler
  public void onDeleteAllRoadNetworkSourcesCommand(final DeleteAllRoadNetworkSourcesCommand c) {
    Optional.ofNullable(scenarioContext.getActiveSituation())
        .map(v -> v.getSources())
        .orElse(new ArrayList<>())
        .stream()
        .filter(v -> RoadEmissionSourceTypes.isRoadSource(v))
        .forEach(source -> {
          eventBus.fireEvent(new EmissionSourceDeleteCommand(source, scenarioContext.getActiveSituation()));
        });
  }

  @EventHandler
  public void onDeleteAllEmissionSourceCommand(final DeleteAllEmissionSourceCommand c) {
    final SituationContext situation = scenarioContext.getActiveSituation();
    final List<EmissionSourceFeature> features = situation.getOriginalSources();
    features.clear();
    resetSelection();
    eventBus.fireEvent(new EmissionSourceListChangeEvent(situation.getSources(), situation.getId()));
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }

  private void resetSelection() {
    context.reset();
    eventBus.fireEvent(new SelectionResetEvent());
  }
}
