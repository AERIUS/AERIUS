/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.farmanimalhousing.detail;

import java.util.List;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalHousingCategories;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;
import nl.overheid.aerius.wui.application.components.divider.DividerComponent;
import nl.overheid.aerius.wui.application.components.source.farmanimalhousing.custom.FarmAnimalHousingDetailCustomComponent;
import nl.overheid.aerius.wui.application.components.source.farmanimalhousing.standard.FarmAnimalHousingDetailStandardComponent;
import nl.overheid.aerius.wui.application.domain.source.FarmAnimalHousingESFeature;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmAnimalHousing;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmAnimalHousingStandardUtil;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmAnimalHousingType;
import nl.overheid.aerius.wui.application.domain.source.farm.StandardFarmAnimalHousing;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

/**
 * Component to show all details of a FarmAnimalHouse object.
 */
@Component(name = "aer-farmanimalhousing-detail", components = {
    FarmAnimalHousingDetailHeaderComponent.class,
    FarmAnimalHousingDetailStandardComponent.class,
    FarmAnimalHousingDetailCustomComponent.class,
    DividerComponent.class
})
public class FarmAnimalHousingDetailComponent extends BasicVueComponent {
  @Prop FarmAnimalHousingCategories farmAnimalHousingCategories;
  @Prop @JsProperty FarmAnimalHousingESFeature source;
  @Prop @JsProperty protected List<Substance> substances;
  @Prop String totalEmissionTitle;

  @Computed("substances")
  protected List<Substance> getSubstances() {
    return substances;
  }

  @JsMethod
  protected boolean hasAdditionalHousingSystems() {
    for (int i = 0; i < source.getSubSources().length; i++) {
      final FarmAnimalHousing fah = source.getSubSources().getAt(i);

      if (fah.getAnimalHousingType() == FarmAnimalHousingType.STANDARD &&
          ((StandardFarmAnimalHousing) fah).getAdditionalSystems().length > 0) {
        return true;
      }
    }
    return false;
  }

  @JsMethod
  protected boolean hasNumberOfDays() {
    for (int i = 0; i < source.getSubSources().length; i++) {
      final FarmAnimalHousing fah = source.getSubSources().getAt(i);

      if (fah.getAnimalHousingType() == FarmAnimalHousingType.STANDARD &&
          FarmAnimalHousingStandardUtil.getEmissionFactorType(farmAnimalHousingCategories,
              (StandardFarmAnimalHousing) fah) == FarmEmissionFactorType.PER_ANIMAL_PER_DAY) {
        return true;
      }
    }
    return false;
  }

}
