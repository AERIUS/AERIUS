/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.farmlodging.detail;

import java.util.List;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategories;
import nl.overheid.aerius.wui.application.components.divider.DividerComponent;
import nl.overheid.aerius.wui.application.components.source.farmlodging.custom.FarmLodgingDetailCustomComponent;
import nl.overheid.aerius.wui.application.components.source.farmlodging.standard.FarmLodgingDetailStandardComponent;
import nl.overheid.aerius.wui.application.domain.source.FarmLodgingESFeature;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmLodgingStandardUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "aer-farmlodging-detail",
components = {
    FarmLodgingDetailHeaderComponent.class,
    FarmLodgingDetailStandardComponent.class,
    FarmLodgingDetailCustomComponent.class,
    DividerComponent.class
})
public class FarmLodgingDetailComponent extends BasicVueComponent implements IsVueComponent{
  @Prop FarmLodgingCategories farmLodgingCategories;
  @Prop @JsProperty FarmLodgingESFeature source;
  @Prop @JsProperty protected List<Substance> substances;
  @Prop String totalEmissionTitle;

  @Computed("substances")
  public List<Substance> getSubstances() {
    return substances;
  }

  @JsMethod
  public boolean hasFarmReductiveLodgingSystemCategories() {
    return FarmLodgingStandardUtil.hasFarmReductiveLodgingSystemCategories(farmLodgingCategories);
  }
}
