/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.habitats;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;

import ol.Collection;
import ol.layer.Base;
import ol.layer.Group;
import ol.layer.Image;
import ol.layer.LayerGroupOptions;

import nl.aerius.geo.domain.IsLayer;
import nl.aerius.geo.domain.LayerInfo;
import nl.aerius.geo.domain.LayerOptions;
import nl.aerius.geo.shared.LayerProps;
import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.wui.application.i18n.M;

public class HabitatTypeLayerGroup extends BasicEventComponent implements IsLayer<Group> {
  private static final String BUNDLE_NAME = "habitat_type";

  private static final String HABITAT_TYPES_NAME = "calculator:wms_habitat_types";
  private static final String EXTRA_ASSESSMENT_HEXAGONS_NAME = "calculator:wms_extra_assessment_hexagons_view";

  private static final HabitatLayerGroupEventBinder EVENT_BINDER = GWT.create(HabitatLayerGroupEventBinder.class);

  public interface HabitatLayerGroupEventBinder extends EventBinder<HabitatTypeLayerGroup> {
  }

  private final LayerInfo info;
  private final Group layer;

  @Inject
  public HabitatTypeLayerGroup(@Assisted List<IsLayer<?>> layers, final EventBus eventBus) {
    this.info = new LayerInfo();
    this.info.setName(this.getClass().getCanonicalName());
    this.info.setTitle(M.messages().layerHabitatExtraAssessmentCombined());
    this.info.setBundle(BUNDLE_NAME);

    final Optional<IsLayer<?>> habitatSensitivityLayer = layers.stream()
        .filter(v -> HABITAT_TYPES_NAME.equals(v.getInfo().getName()))
        .findFirst();
    final Optional<IsLayer<?>> extraAssessmentHexagonLayer = layers.stream()
        .filter(v -> EXTRA_ASSESSMENT_HEXAGONS_NAME.equals(v.getInfo().getName()))
        .findFirst();

    if (habitatSensitivityLayer.isPresent() && extraAssessmentHexagonLayer.isPresent()) {
      final Image habitatLayer = (Image) habitatSensitivityLayer.get().asLayer();
      final Image extraAssessmentLayer = (Image) extraAssessmentHexagonLayer.get().asLayer();

      final Collection<Base> baseLayers = new Collection<>();
      baseLayers.push(habitatLayer);
      baseLayers.push(extraAssessmentLayer);

      // Just grab the habitat layer legend and use that one
      this.info.setLegend(habitatSensitivityLayer.get().getInfo().getLegend());

      final LayerOptions[] options1 = habitatSensitivityLayer.get().getInfo().getOptions();
      final LayerOptions[] options2 = extraAssessmentHexagonLayer.get().getInfo().getOptions();

      this.info.setOptions(new LayerOptions[] {combineLayerOptions(options1[0], options2[0])});

      habitatLayer.setVisible(true);
      extraAssessmentLayer.setVisible(true);

      final LayerGroupOptions groupOptions = new LayerGroupOptions();
      groupOptions.setLayers(baseLayers);
      layer = new Group(groupOptions);
      layer.setVisible(false);
    } else {
      GWTProd.warn(habitatSensitivityLayer.isPresent() + " > " + extraAssessmentHexagonLayer.isPresent());
      throw new RuntimeException("Habitat layer could not be assembled.");
    }
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  public static final boolean isSensitiveHabitatLayer(LayerProps props) {
    return HABITAT_TYPES_NAME.equals(props.getName())
        || isExtraAssessmentHexagonLayer(props.getName());
  }

  public static final boolean isExtraAssessmentHexagonLayer(String name) {
    return EXTRA_ASSESSMENT_HEXAGONS_NAME.equals(name);
  }

  @Override
  public Group asLayer() {
    return layer;
  }

  @Override
  public LayerInfo getInfo() {
    return info;
  }

  private static LayerOptions combineLayerOptions(final LayerOptions... original) {
    final List<Object> options = new ArrayList<>();
    options.addAll(original[0].getOptions());

    final LayerOptions duplicate = new LayerOptions(
        options,
        key -> original[0].getNameOf(key),
        key -> Stream.of(original).forEach(v -> v.select(key)),
        LayerOptions.NO_OPTION_SELECTED);

    return duplicate;
  }

}
