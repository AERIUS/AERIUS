/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.farmanimalhousing.custom;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalHousingCategories;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;
import nl.overheid.aerius.wui.application.components.source.farmanimalhousing.detail.FarmAnimalHousingDetailUtil;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.farm.CustomFarmAnimalHousing;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmAnimalHousingStandardUtil;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

/**
 * Component to display the details of a AnimalHousingDetailCustom sub source.
 */
@Component(name = "aer-farmanimalhousing-detail-custom")
public class FarmAnimalHousingDetailCustomComponent extends BasicVueComponent {
  @Prop @JsProperty protected CustomFarmAnimalHousing source;
  @Prop boolean hasReductionFactorColumn;
  @Prop boolean hasNumberOfDaysColumn;

  @Data int index;

  @Inject @Data ApplicationContext applicationContext;

  @JsMethod
  protected FarmAnimalHousingCategories getCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getFarmAnimalHousingCategories();
  }

  @Computed
  protected String emission() {
    return MessageFormatter.formatEmissionWithUnitSmart(source.getEmission(Substance.NH3));
  }

  @Computed
  protected String farmAnimalHousingEmissionFactor() {
    final String emissionFactorValue = i18n.decimalNumberCapped(source.getEmissionFactor(Substance.NH3), 2);
    if (hasAdditionalHousingSystemCategories()) {
      return emissionFactorValue;
    } else {
      return emissionFactorValue + " " + i18n.esFarmEmissionFactorType(source.getFarmEmissionFactorType());
    }
  }

  @JsMethod
  protected boolean hasAdditionalHousingSystemCategories() {
    return FarmAnimalHousingStandardUtil.hasAdditionalHousingSystemCategories(getCategories());
  }

  @Computed
  protected boolean hasNumberOfDays() {
    return source.getFarmEmissionFactorType() == FarmEmissionFactorType.PER_ANIMAL_PER_DAY;
  }

  @Computed
  protected int columnsWithoutEmission() {
    return FarmAnimalHousingDetailUtil.columnsWithoutEmission(hasReductionFactorColumn, hasNumberOfDaysColumn);
  }

  @Computed
  protected String numberOfDays() {
    if (source.getFarmEmissionFactorType() == FarmEmissionFactorType.PER_ANIMAL_PER_DAY) {
      return Integer.toString(source.getNumberOfDays());
    } else {
      return "-";
    }
  }
}
