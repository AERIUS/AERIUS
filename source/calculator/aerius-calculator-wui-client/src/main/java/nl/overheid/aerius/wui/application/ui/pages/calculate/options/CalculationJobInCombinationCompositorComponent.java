/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.calculate.options;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.components.input.CheckBoxComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;

@Component(components = {
    CheckBoxComponent.class,
    LabeledInputComponent.class,
})
public class CalculationJobInCombinationCompositorComponent extends ErrorWarningValidator {

  private static final List<SituationType> IN_COMBINATION_SITUATION_TYPES = Arrays.asList(
      SituationType.COMBINATION_REFERENCE,
      SituationType.COMBINATION_PROPOSED);

  @Prop(required = true) SituationComposition situationComposition;
  @Prop(required = true) CalculationSetOptions calculationOptions;

  @Inject @Data ApplicationContext applicationContext;
  @Inject @Data ScenarioContext scenarioContext;

  @Computed
  public List<SituationType> getInCombinationTypes() {
    return IN_COMBINATION_SITUATION_TYPES.stream()
        .filter(this::hasInCombinationSituations)
        .collect(Collectors.toList());
  }

  @JsMethod
  public boolean hasAnyCombinationSituations() {
    return IN_COMBINATION_SITUATION_TYPES.stream()
        .anyMatch(this::hasInCombinationSituations);
  }

  @JsMethod
  public boolean hasInCombinationSituations(final SituationType situationType) {
    return scenarioContext.getSituations().stream()
        .anyMatch(sc -> situationType == sc.getType());
  }

  @JsMethod
  public List<SituationContext> getInCombinationSituations(final SituationType situationType) {
    return scenarioContext.getSituations().stream()
        .filter(sc -> situationType == sc.getType())
        .collect(Collectors.toList());
  }

  @JsMethod
  public void updateIncludingSituation(final boolean checked, final SituationContext situation) {
    if (checked) {
      situationComposition.addSituation(situation);
    } else {
      situationComposition.removeSituation(situation);
    }
  }

  @JsMethod
  public boolean isActiveSituation(final SituationContext situation) {
    return situationComposition.isActiveSituation(situation);
  }

  @JsMethod
  public boolean isUseArchive() {
    return calculationOptions.isUseInCombinationArchive();
  }

  @JsMethod
  public void setUseArchive(final boolean enable) {
    calculationOptions.setUseInCombinationArchive(enable);
  }
}
