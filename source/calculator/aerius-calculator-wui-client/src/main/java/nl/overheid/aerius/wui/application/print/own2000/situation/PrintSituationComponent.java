/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.print.own2000.situation;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.OPSLimits;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.sector.SectorIcon;
import nl.overheid.aerius.shared.util.ColorUtil;
import nl.overheid.aerius.wui.application.ApplicationLimits;
import nl.overheid.aerius.wui.application.components.common.StyledPointComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.road.RoadEmissionSourceTypes;
import nl.overheid.aerius.wui.application.geo.util.GeoUtil;
import nl.overheid.aerius.wui.application.geo.util.OrientedEnvelope;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.application.util.SectorColorUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    StyledPointComponent.class,
    LabeledField.class
})
public class PrintSituationComponent extends BasicVueComponent {

  @Inject @Data ApplicationContext applicationContext;

  @Prop SituationContext situation;

  @Computed("maxNumberOfSourcesExceeded")
  public boolean maxNumberOfSourcesExceeded() {
    return getEmissionSources().size() > ApplicationLimits.PRINT_EMISSION_SOURCE_LIMIT;
  }

  @Computed
  public String getMaxNumberOfSourcesExceededMessage() {
    return i18n.pdfEmissionSourcesLimitReachedMessage(String.valueOf(ApplicationLimits.PRINT_EMISSION_SOURCE_LIMIT),
        String.valueOf(ApplicationLimits.SCENARIO_INPUT_LIST_DRAW_LIMIT));
  }

  @Computed
  public List<EmissionSourceFeature> getEmissionSources() {
    return situation.getSources().stream().filter(es -> !RoadEmissionSourceTypes.isRoadSource(es)).collect(Collectors.toList());
  }

  @Computed
  public List<BuildingFeature> getBuildings() {
    return situation.getBuildings();
  }

  @JsMethod
  public SectorGroup findSectorGroupFromSectorId(final int sectorId) {
    return applicationContext.getConfiguration().getSectorCategories().findSectorGroupFromSectorId(sectorId);
  }

  @JsMethod
  public String getSectorGroup(final EmissionSourceFeature source) {
    return Optional.ofNullable(findSectorGroupFromSectorId(source.getSectorId()))
        .map(v -> i18n.sectorGroup(v))
        .orElse("-");
  }

  @JsMethod
  public Sector findSectorFromSectorId(final int sectorId) {
    return applicationContext.getConfiguration().getSectorCategories().determineSectorById(sectorId);
  }

  @JsMethod
  public String getSector(final EmissionSourceFeature source) {
    return Optional.ofNullable(findSectorFromSectorId(source.getSectorId()))
        .map(v -> v.getDescription())
        .orElse("-");
  }

  @JsMethod
  public String formatDimensions(final BuildingFeature feature) {
    final OrientedEnvelope envelope = GeoUtil.determineOrientedEnvelope(feature.getGeometry());

    String dimensions = M.messages().pdfSituationOverviewBuildingsEnvelope(
        buildingUnit(envelope.getLength()),
        buildingUnit(envelope.getWidth()),
        buildingUnit(feature.getHeight()),
        buildingOrientationUnit(envelope.getOrientation()));

    if (envelope.getLength() > OPSLimits.SCOPE_BUILDING_LENGTH_MAXIMUM ||
        envelope.getWidth() > OPSLimits.SCOPE_BUILDING_LENGTH_MAXIMUM ||
        feature.getHeight() > OPSLimits.SCOPE_BUILDING_HEIGHT_MAXIMUM) {

      dimensions += " " + M.messages().pdfSituationOverviewBuildingsEnvelopeCorrected(
          buildingUnit(Math.min(envelope.getLength(), OPSLimits.SCOPE_BUILDING_LENGTH_MAXIMUM)),
          buildingUnit(Math.min(envelope.getWidth(), OPSLimits.SCOPE_BUILDING_LENGTH_MAXIMUM)),
          buildingUnit(Math.min(feature.getHeight(), OPSLimits.SCOPE_BUILDING_HEIGHT_MAXIMUM)));
    }

    return dimensions;
  }

  public String buildingUnit(final double value) {
    return i18n.unitM(i18n.decimalNumberFixed(value, OPSLimits.SOURCE_BUILDING_DIGITS_PRECISION));
  }

  public String buildingOrientationUnit(final double value) {
    return i18n.unitOrientation(i18n.decimalNumberFixed(value, 0));
  }

  @JsMethod
  public boolean hasRoadNetwork() {
    return situation.getSources().stream().anyMatch(es -> RoadEmissionSourceTypes.isRoadSource(es));
  }

  @JsMethod
  public String getRoadNetworkEmission(final Substance substance) {
    return MessageFormatter.formatEmissionWithUnitSmart(
        situation.getSources().stream()
            .filter(es -> RoadEmissionSourceTypes.isRoadSource(es))
            .map(es -> es.getEmission(substance)).mapToDouble(Double::doubleValue)
            .sum());
  }

  @JsMethod
  public String formatEmission(final EmissionSourceFeature source, final Substance substance) {
    final double emission = source.getEmission(substance);
    return emission > 0 ? MessageFormatter.formatEmissionWithUnitSmart(emission) : "-";
  }

  @JsMethod
  public String sectorColor(final EmissionSourceFeature source) {
    final SectorIcon icon = SectorColorUtil.getSectorIcon(applicationContext.getConfiguration(), source.getSectorId());
    return ColorUtil.webColor(icon.getColor());
  }
}
