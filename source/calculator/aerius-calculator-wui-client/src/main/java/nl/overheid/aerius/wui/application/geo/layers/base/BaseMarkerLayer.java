/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.base;

import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.style.Style;

import nl.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.wui.application.command.geo.ChangeLabelDisplayModeCommand;
import nl.overheid.aerius.wui.application.domain.ImaerFeature;
import nl.overheid.aerius.wui.application.domain.geo.LabelDisplayMode;

/**
 * Base layer for showing Feature markers on a layer for specific Feature objects.
 *
 * @param <T> type of the Feature objects
 */
public abstract class BaseMarkerLayer<F extends ImaerFeature> extends BaseVectorLayer<F> {

  private final LabelStyleCreator<F> labelStyleProvider;

  public BaseMarkerLayer(final LayerInfo info, final int zIndex, final LabelStyleCreator<F> labelStyleProvider) {
    super(info, zIndex);
    this.labelStyleProvider = labelStyleProvider;
  }

  public LabelStyleCreator<F> getLabelStyleProvider() {
    return labelStyleProvider;
  }

  @EventHandler
  public void onChangeLabelDisplayModeCommand(final ChangeLabelDisplayModeCommand e) {
    labelStyleProvider.setShowNameLabels(e.getValue() == LabelDisplayMode.NAME);
    refreshLayer();
  }

  @Override
  protected Style[] createStyle(final F feature, final double resolution) {
    return labelStyleProvider.createStyle(feature, resolution);
  }
}
