/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.nav;

import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import nl.aerius.wui.place.Place;
import nl.aerius.wui.place.TokenizedPlace;
import nl.overheid.aerius.wui.application.place.ApplicationPlaceController;

public class NavigationItem {

  private final String name;
  private final String icon;
  private final Predicate<Place> active;
  private final Supplier<Boolean> enabled;
  private final Supplier<Boolean> permanentlyDisabled;
  private final Consumer<NavigationItem> action;
  private final String href;
  private boolean labelLeft;
  private boolean hasPopout;

  private NavigationItem(
      final String name,
      @Nullable final String icon,
      final Predicate<Place> active,
      final Supplier<Boolean> enabled,
      final Supplier<Boolean> permanentlyDisabled,
      final Consumer<NavigationItem> action,
      final String href) {
    if (name == null) {
      throw new NullPointerException("Null getName");
    }
    this.name = name;
    this.icon = icon;
    this.active = active;
    if (enabled == null) {
      throw new NullPointerException("Null isEnabled");
    }
    this.enabled = enabled;
    if (permanentlyDisabled == null) {
      throw new NullPointerException("Null permanentlyDisabled");
    }
    this.permanentlyDisabled = permanentlyDisabled;
    if (action == null) {
      throw new NullPointerException("Null getAction");
    }
    this.action = action;
    this.href = href;
  }

  public static NavigationItem create(final String name,
      final String icon,
      final Predicate<Place> active,
      final Supplier<Boolean> enabled,
      final Supplier<Boolean> permanentlyDisabled,
      final Consumer<NavigationItem> action,
      final String href) {
    return new NavigationItem(name, icon, active, enabled, permanentlyDisabled, action, href);
  }

  public static NavigationItem createPlaceItem(final String name,
      final String icon,
      final ApplicationPlaceController placeController,
      final TokenizedPlace place) {
    return createPlaceItem(name, icon, t -> t.getClass() == place.getClass(), placeController, place);
  }

  public static NavigationItem createPlaceItem(final String name,
      final String icon,
      final Predicate<Place> active,
      final ApplicationPlaceController placeController,
      final TokenizedPlace place) {
    final Supplier<Boolean> enabled = () -> placeController.isEnabled(place.getClass());
    final Supplier<Boolean> permanentlyDisabled = () -> placeController.isPermanentlyDisabled(place.getClass());
    final Consumer<NavigationItem> action = p -> placeController.goTo(place);
    final String href = place.getToken();
    return create(name, icon, active, enabled, permanentlyDisabled, action, href);
  }

  public static NavigationItem createSimpleItem(final String name,
      final String icon,
      final Predicate<Place> active,
      final Consumer<NavigationItem> action) {
    return createSimpleItem(name, icon, active, action, null);
  }

  public static NavigationItem createSimpleItem(final String name,
      final String icon,
      final Predicate<Place> active,
      final Consumer<NavigationItem> action,
      final String href) {
    return create(name, icon, active, () -> true, () -> false, action, href);
  }

  public String getName() {
    return name;
  }

  @Nullable
  public String getIcon() {
    return icon;
  }

  public Predicate<Place> isActive() {
    return active;
  }

  public Supplier<Boolean> isEnabled() {
    return enabled;
  }

  public Supplier<Boolean> isPermanentlyDisabled() {
    return permanentlyDisabled;
  }

  public Consumer<NavigationItem> getAction() {
    return action;
  }

  public String getHref() {
    return href;
  }

  public boolean isLabelLeft() {
    return labelLeft;
  }

  public NavigationItem withLabelLeft(final boolean labelLeft) {
    this.labelLeft = labelLeft;
    return this;
  }

  public boolean hasPopout() {
    return hasPopout;
  }

  public NavigationItem withPopout(final boolean hasPopout) {
    this.hasPopout = hasPopout;
    return this;
  }

  @Override
  public String toString() {
    return "NavigationItem{"
        + "getName=" + name + ", "
        + "getIcon=" + icon + ", "
        + "isActive=" + active + ", "
        + "isEnabled=" + enabled + ", "
        + "getAction=" + action + ", "
        + "getHref=" + href
        + "}";
  }

  @Override
  public boolean equals(final Object o) {
    if (o == this) {
      return true;
    }
    if (o != null && o.getClass() == this.getClass()) {
      final NavigationItem that = (NavigationItem) o;
      return this.name.equals(that.getName())
          && (this.icon == null ? that.getIcon() == null : this.icon.equals(that.getIcon()))
          && this.active.equals(that.isActive())
          && this.enabled.equals(that.isEnabled())
          && this.action.equals(that.getAction())
          && this.href.equals(that.getHref());
    }
    return false;
  }

  @Override
  public int hashCode() {
    int h$ = 1;
    h$ *= 1000003;
    h$ ^= name.hashCode();
    h$ *= 1000003;
    h$ ^= icon == null ? 0 : icon.hashCode();
    h$ *= 1000003;
    h$ ^= active.hashCode();
    h$ *= 1000003;
    h$ ^= enabled.hashCode();
    h$ *= 1000003;
    h$ ^= action.hashCode();
    h$ *= 1000003;
    h$ ^= href.hashCode();
    return h$;
  }
}
