/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.results;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;

import jsinterop.annotations.JsMethod;

import nl.aerius.geo.domain.legend.ColorLabelsLegend;
import nl.aerius.geo.domain.legend.LegendType;
import nl.overheid.aerius.shared.domain.legend.ColorRange;
import nl.overheid.aerius.shared.domain.legend.ColorRangeType;
import nl.overheid.aerius.shared.domain.legend.ColorRangesLegend;
import nl.overheid.aerius.wui.application.components.legend.ColorLabelsLegendComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    ColorLabelsLegendComponent.class
})
public class ProcessContributionsLegendPanel extends BasicVueComponent {
  @Inject @Data ApplicationContext applicationContext;

  @JsMethod
  public ColorLabelsLegend getLegend() {
    final List<ColorRange> colorRanges = applicationContext.getConfiguration()
        .getColorRange(ColorRangeType.PROJECT_CALCULATION_PERCENTAGE_CRITICAL_LOAD);

    return new ColorRangesLegend(
        colorRanges,
        M.messages().colorRangesLegendBetweenText(),
        LegendType.SQUARE);
  }

}
