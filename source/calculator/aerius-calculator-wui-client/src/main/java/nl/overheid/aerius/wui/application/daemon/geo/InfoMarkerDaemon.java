/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.geo;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;
import com.google.web.bindery.event.shared.binder.GenericEvent;

import ol.Coordinate;
import ol.geom.Geometry;
import ol.geom.GeometryType;

import nl.aerius.geo.command.InteractionAddedCommand;
import nl.aerius.geo.command.InteractionRemoveCommand;
import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;
import nl.overheid.aerius.wui.application.command.geo.ClearAssessmentAreaCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.InfoMarkerContext;
import nl.overheid.aerius.wui.application.context.ResultsSummaryContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationInfo;
import nl.overheid.aerius.wui.application.domain.geo.InfoMarkerOptions;
import nl.overheid.aerius.wui.application.event.ApplicationFinishedLoadingEvent;
import nl.overheid.aerius.wui.application.event.CalculationCompleteEvent;
import nl.overheid.aerius.wui.application.event.CalculationStatusEvent;
import nl.overheid.aerius.wui.application.event.InfoPopupHiddenEvent;
import nl.overheid.aerius.wui.application.event.InfoPopupVisibleEvent;
import nl.overheid.aerius.wui.application.event.SwitchActiveCalculationEvent;
import nl.overheid.aerius.wui.application.event.situation.SituationChangedYearEvent;
import nl.overheid.aerius.wui.application.geo.interactions.FeatureDrawInteraction;
import nl.overheid.aerius.wui.application.service.AeriusRequestCallback;
import nl.overheid.aerius.wui.application.service.InfoServiceAsync;

/**
 * Keeps track of information marker map interactions and service retrieval
 */
@Singleton
public class InfoMarkerDaemon extends BasicEventComponent {
  private static final InfoMarkerDaemonEventBinder EVENT_BINDER = GWT.create(InfoMarkerDaemonEventBinder.class);

  interface InfoMarkerDaemonEventBinder extends EventBinder<InfoMarkerDaemon> {}

  private static final int LOAD_DELAY = 250;

  private FeatureDrawInteraction markerPointInteraction;

  @Inject private ApplicationContext applicationContext;
  @Inject private ResultsSummaryContext resultsSummaryContext;
  @Inject private CalculationContext calculationContext;
  @Inject private ScenarioContext scenarioContext;
  @Inject private InfoMarkerContext infoContext;

  @Inject private InfoServiceAsync service;

  private ReceptorUtil receptorUtil;

  private boolean scheduleDelayedLoading;

  @EventHandler
  public void onApplicationFinishedLoadingEvent(final ApplicationFinishedLoadingEvent e) {
    receptorUtil = new ReceptorUtil(applicationContext.getConfiguration().getReceptorGridSettings());
  }

  @EventHandler
  public void onInfoPopupVisibleEvent(final InfoPopupVisibleEvent e) {
    if (markerPointInteraction != null) {
      // Warn about a bug having been implemented (and handled); a popup visible event
      // should ideally not be fired without a hidden event in between.
      GWTProd.warn("Info marker made active while an interaction still exists.");
      destroyMarkerInteraction();
    }

    markerPointInteraction = new FeatureDrawInteraction(GeometryType.Point, geom -> updateInformationMarker(geom));
    eventBus.fireEvent(new InteractionAddedCommand(markerPointInteraction));

    if (infoContext.hasSelection()) {
      // Re-fire the remembered selection
      final InfoMarkerOptions options = new InfoMarkerOptions();
      options.setReceptorId(infoContext.getReceptor());
      eventBus.fireEvent(new InfoMarkerSelectionEvent(options));

      retrieveStaticInfoSummary(infoContext.getReceptor());
    }
  }

  private void updateInformationMarker(final Geometry geom) {
    final ol.geom.Point point = (ol.geom.Point) geom;
    final Coordinate coordinate = point.getFirstCoordinate();

    final Point aeriusPoint = new Point(coordinate.getX(), coordinate.getY());

    final InfoMarkerOptions options = new InfoMarkerOptions();
    options.setReceptorId(receptorUtil.getReceptorIdFromPoint(aeriusPoint));

    eventBus.fireEvent(new InfoMarkerSelectionCommand(options));
  }

  @EventHandler
  public void onInfoMarkerSelectionCommand(final InfoMarkerSelectionCommand c) {
    final InfoMarkerOptions options = c.getValue();
    if (infoContext.isSelected(options.getReceptorId())) {
      c.silence();
      return;
    }

    infoContext.clearJobReceptorInfo();
    retrieveStaticInfoSummary(options.getReceptorId());
    retrieveJobInfo(options.getReceptorId());
  }

  @EventHandler(handles = {SwitchActiveCalculationEvent.class, SituationChangedYearEvent.class})
  public void onRefreshContentEvent(final GenericEvent e) {
    if (markerPointInteraction != null && infoContext.hasSelection()) {
      retrieveStaticInfoSummary(infoContext.getReceptor());
    }
  }

  @EventHandler(handles = {CalculationCompleteEvent.class})
  public void onCalculationCompletedEvent(final CalculationCompleteEvent e) {
    if (markerPointInteraction != null && infoContext.hasSelection()) {
      retrieveCalculationInfo(e.getValue().getCalculationInfo(), infoContext.getReceptor(), e.getValue().getJobKey());
    }
  }

  @EventHandler(handles = {CalculationStatusEvent.class})
  public void onCalculationStatusEvent(final CalculationStatusEvent e) {
    if (markerPointInteraction == null || !infoContext.hasSelection()) {
      return;
    }

    retrieveCalculationInfo(e.getValue(), infoContext.getReceptor(), e.getJobKey());
  }

  private void retrieveJobInfo(final int receptor) {
    for (final CalculationExecutionContext calculation : calculationContext.getCalculations()) {
      retrieveCalculationInfo(calculation.getCalculationInfo(), receptor, calculation.getJobKey());
    }
  }

  private void retrieveCalculationInfo(final CalculationInfo info, final int receptor, final String jobKey) {
    final int numberOfPointsCalculated = info.getJobProgress().getNumberOfPointsCalculated();
    final int numberOfPointsCalculatedPrev = resultsSummaryContext.getNumberOfPointsCalculated(jobKey, 0);
    final boolean isSuccessState = info.getJobProgress().getState().isSuccessState();

    if (numberOfPointsCalculated == numberOfPointsCalculatedPrev && !isSuccessState) {
      return;
    }

    service.retrieveJobReceptorInfo(receptor, jobKey, AeriusRequestCallback.create(v -> {
      infoContext.updateJobReceptorInfo(jobKey, v);
    }));
  }

  private void retrieveStaticInfoSummary(final Integer receptorId) {
    final int year = scenarioContext.getActiveSituation().getYear();
    infoContext.setReceptor(receptorId);

    infoContext.setStaticReceptorInfo(null);

    delayLoadingIndicator();

    service.retrieveStaticReceptorInfo(receptorId, year, AeriusRequestCallback.create(v -> {
      loadComplete();
      infoContext.setStaticReceptorInfo(v);
    }));
  }

  private void delayLoadingIndicator() {
    scheduleDelayedLoading = true;
    SchedulerUtil.delay(() -> {
      if (scheduleDelayedLoading) {
        infoContext.setLoading(true);
      }

      scheduleDelayedLoading = false;
    }, LOAD_DELAY);
  }

  private void loadComplete() {
    scheduleDelayedLoading = false;
    infoContext.setLoading(false);
  }

  @EventHandler
  public void onInfoPopupHiddenEvent(final InfoPopupHiddenEvent e) {
    destroyMarkerInteraction();

    // Also clear the assessment area selection
    eventBus.fireEvent(new ClearAssessmentAreaCommand());
  }

  private void destroyMarkerInteraction() {
    if (markerPointInteraction != null) {
      eventBus.fireEvent(new InteractionRemoveCommand(markerPointInteraction));
      markerPointInteraction = null;
    }
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
