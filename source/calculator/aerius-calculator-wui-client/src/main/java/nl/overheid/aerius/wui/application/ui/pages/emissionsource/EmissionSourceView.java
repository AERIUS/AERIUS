/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.emissionsource;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.EventBus;

import ol.geom.Geometry;

import elemental2.dom.FocusEvent;

import jsinterop.annotations.JsMethod;

import nl.aerius.geo.command.MapResizeSequenceCommand;
import nl.aerius.wui.place.Place;
import nl.aerius.wui.place.PlaceController;
import nl.aerius.wui.util.SchedulerUtil;
import nl.aerius.wui.vue.directives.VectorDirective;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.sector.SectorIcon;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.SourceType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.shared.util.ColorUtil;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditCancelCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditSaveCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceSectorChangeCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSubSourceDeselectFeatureCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSubSourceSelectFeatureCommand;
import nl.overheid.aerius.wui.application.command.source.SectorGroupSelectionCommand;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.common.FeatureHeading;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.modify.ModifyCancelSaveComponent;
import nl.overheid.aerius.wui.application.components.source.characteristics.OPSCharacteristicsEditorComponent;
import nl.overheid.aerius.wui.application.components.source.characteristics.adms.ADMSGenericCharacteristicsEditorComponent;
import nl.overheid.aerius.wui.application.components.source.characteristics.timevarying.TimeVaryingProfilesComponent;
import nl.overheid.aerius.wui.application.components.source.coldstart.ColdStartEmissionEditorComponent;
import nl.overheid.aerius.wui.application.components.source.combustion.CombustionPlantEmissionEditorComponent;
import nl.overheid.aerius.wui.application.components.source.farmanimalhousing.FarmAnimalHousingEmissionEditorComponent;
import nl.overheid.aerius.wui.application.components.source.farmland.FarmlandEmissionEditorComponent;
import nl.overheid.aerius.wui.application.components.source.farmlodging.FarmLodgingEmissionEditorComponent;
import nl.overheid.aerius.wui.application.components.source.generic.GenericDetailEditorComponent;
import nl.overheid.aerius.wui.application.components.source.manure.ManureStorageEmissionEditorComponent;
import nl.overheid.aerius.wui.application.components.source.offroad.OffRoadMobileEmissionEditorComponent;
import nl.overheid.aerius.wui.application.components.source.road.RoadEmissionEditorComponent;
import nl.overheid.aerius.wui.application.components.source.road.adms.ADMSRoadCharacteristicsEditorComponent;
import nl.overheid.aerius.wui.application.components.source.road.srm2.SRM2CharacteristicsEditorComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.inland.InlandMooringEditorComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.inland.InlandMooringShippingEmissionEditorComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.inland.InlandShippingEmissionEditorComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.maritime.MaritimeMooringEditorComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.maritime.MaritimeShippingEmissionEditorComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.maritime.mooring.MooringMaritimeShippingEmissionEditorComponent;
import nl.overheid.aerius.wui.application.components.source.validation.NoopValidationEditorComponent;
import nl.overheid.aerius.wui.application.components.warning.PrivacyWarning;
import nl.overheid.aerius.wui.application.components.warning.SectorHintWarning;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.EmissionSourceEditorContext;
import nl.overheid.aerius.wui.application.context.EmissionSourceValidationContext;
import nl.overheid.aerius.wui.application.context.GenericValidationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.source.ColdStartESFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.InlandShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.MaritimeShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.MooringInlandShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.MooringMaritimeShippingESFeature;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.place.BuildingPlace;
import nl.overheid.aerius.wui.application.place.TimeVaryingProfilePlace;
import nl.overheid.aerius.wui.application.util.AccessibilityUtil;
import nl.overheid.aerius.wui.application.util.SectorColorUtil;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component(components = {
    PrivacyWarning.class,
    SectorHintWarning.class,
    FeatureHeading.class,

    LabeledInputComponent.class,
    SimplifiedListBoxComponent.class,
    CollapsiblePanel.class,

    EmissionSourceLocationView.class,

    ADMSGenericCharacteristicsEditorComponent.class,
    ADMSRoadCharacteristicsEditorComponent.class,
    OPSCharacteristicsEditorComponent.class,
    SRM2CharacteristicsEditorComponent.class,
    NoopValidationEditorComponent.class,

    ErrorWarningIconComponent.class,

    GenericDetailEditorComponent.class,
    FarmAnimalHousingEmissionEditorComponent.class,
    FarmLodgingEmissionEditorComponent.class,
    FarmlandEmissionEditorComponent.class,
    ManureStorageEmissionEditorComponent.class,
    RoadEmissionEditorComponent.class,
    MaritimeShippingEmissionEditorComponent.class,
    OffRoadMobileEmissionEditorComponent.class,
    ColdStartEmissionEditorComponent.class,
    InlandMooringEditorComponent.class,
    MaritimeMooringEditorComponent.class,
    InlandShippingEmissionEditorComponent.class,
    InlandMooringShippingEmissionEditorComponent.class,
    MooringMaritimeShippingEmissionEditorComponent.class,
    CombustionPlantEmissionEditorComponent.class,
    TimeVaryingProfilesComponent.class,

    ModifyCancelSaveComponent.class,
    VerticalCollapse.class,
}, directives = {
    VectorDirective.class,
})
public class EmissionSourceView extends BasicVueView implements HasCreated {
  enum EmissionSourcePanel {
    LOCATION, SOURCE_CHARACTERISTICS, SHIPPING_DOCK, EMISSION_SOURCE_TYPE, TIME_VARYING_PROFILES
  }

  @Prop EmissionSourcePresenter presenter;

  @Prop EventBus eventBus;

  @Inject @Data ApplicationContext appContext;
  @Inject @Data EmissionSourceEditorContext editorContext;
  @Inject @Data EmissionSourceValidationContext validationContext;
  @Inject @Data GenericValidationContext genericValidationContext;
  @Inject @Data ScenarioContext scenarioContext;

  @Inject PlaceController placeController;

  @Data EmissionSourcePanel openPanel;

  @Data boolean nameLabelFocused = false;

  @Override
  public void created() {
    if (!editorContext.isEditing()) {
      return;
    }

    validationContext.reset();
    genericValidationContext.reset();
    openPanel = determineBestOpenPanel();
  }

  @JsMethod
  public void onLabelNameFocus() {
    nameLabelFocused = true;
  }

  @JsMethod
  public void onLabelNameFocusOut(final FocusEvent event) {
    AccessibilityUtil.doOnFocusLeave(event,
        () -> SchedulerUtil.delay(() -> nameLabelFocused = false, AccessibilityUtil.FOCUS_OUT_DELAY));
  }

  @Computed
  public EmissionSourceFeature getSource() {
    return editorContext.getSource();
  }

  @Computed
  public SituationContext getSituation() {
    return editorContext.getSituation();
  }

  @Computed
  public SectorGroup getSelectedSectorGroup() {
    return editorContext.getSelectedSectorGroup();
  }

  @Computed
  public List<SectorGroup> getSectorGroups() {
    return appContext.getConfiguration().getSectorGroups();
  }

  @Computed
  public boolean hasMultipleSectors() {
    return getAvailableSectors().size() > 1;
  }

  @Computed("hasCharacteristics")
  public boolean hasCharacteristics() {
    return isEditableSource()
        && !isColdStartVehicleBasedCharacteristics()
        && (isESType(EmissionSourceType.SRM2_ROAD)
            || isESType(EmissionSourceType.ADMS_ROAD)
            || isCType(CharacteristicsType.OPS)
            || isCType(CharacteristicsType.ADMS));
  }

  @Computed("sourceCharacteristicsType")
  public String getSourceCharacteristicsType() {
    return EmissionEditorComponentUtil.getSourceCharacteristicsComponent(hasCharacteristics() ? getSource() : null);
  }

  @Computed("emissionEditorType")
  public String getEmissionEditorType() {
    return EmissionEditorComponentUtil.getEmissionEditorComponent(getSource());
  }

  @Computed("sourceCharacteristicsDebugId")
  public String getSourceCharacteristicsDebugId() {
    String type;

    if (isESType(EmissionSourceType.SRM2_ROAD)) {
      type = "collapsibleSRM2Characteristics";
    } else if (isCType(CharacteristicsType.OPS)) {
      type = "collapsibleOPSCharacteristics";
    } else if (isCType(CharacteristicsType.ADMS)) {
      type = "collapsibleADMSCharacteristics";
    } else {
      type = null;
    }

    return type;
  }

  @JsMethod
  public void onLocationWarningChange(final boolean locationWarning) {
    validationContext.setLocationWarning(locationWarning);
  }

  @JsMethod
  public void onLocationErrorChange(final boolean locationError) {
    validationContext.setLocationError(locationError);
  }

  @JsMethod
  public void onCharacteristicsWarningChange(final boolean characteristicsWarning) {
    validationContext.setCharacteristicsWarning(characteristicsWarning);
  }

  @JsMethod
  public void onCharacteristicsErrorChange(final boolean characteristicsError) {
    validationContext.setCharacteristicsError(characteristicsError);
  }

  @JsMethod
  public void onEmissionEditorWarningChange(final boolean emissionEditorWarning) {
    validationContext.setEmissionEditorWarning(emissionEditorWarning);
  }

  @JsMethod
  public void onEmissionEditorErrorChange(final boolean emissionEditorError) {
    validationContext.setEmissionEditorError(emissionEditorError);
  }

  @JsMethod
  public void onTimeVaryingWarningChange(final boolean timeVaryingWarning) {
    validationContext.setTimeVaryingWarning(timeVaryingWarning);
  }

  @JsMethod
  public void onTimeVaryingErrorChange(final boolean timeVaryingError) {
    validationContext.setTimeVaryingError(timeVaryingError);
  }

  @JsMethod
  public String getId() {
    return getSource().getId();
  }

  @Computed
  public SectorIcon getSectorIcon() {
    return SectorColorUtil.getSectorIcon(appContext.getConfiguration(), getSource().getSectorId());
  }

  @Computed
  public String getSectorColor() {
    return ColorUtil.webColor(getSectorIcon().getColor());
  }

  @Computed
  public String getGeometryType() {
    final EmissionSourceFeature src = getSource();
    if (src.getCharacteristicsType() == CharacteristicsType.ADMS) {
      if (src.getEmissionSourceType() == EmissionSourceType.ADMS_ROAD) {
        return i18n.esADMSSourceTitle(SourceType.ROAD);
      } else {
        return Optional.ofNullable(src.getCharacteristics())
            .map(v -> v.asADMS())
            .map(v -> v.getSourceType())
            .map(v -> i18n.esADMSSourceTitle(v))
            .orElse(null);
      }
    } else {
      return Optional.ofNullable(src.getGeometry())
          .map(Geometry::getType)
          .map(v -> i18n.esGeometryType(appContext.getTheme(), v))
          .orElseGet(() -> i18n.esGeometryType(null, null));
    }
  }

  @Computed
  public String getLabel() {
    return getSource().getLabel();
  }

  @Computed
  public void setLabel(final String label) {
    getSource().setLabel(label);
  }

  @JsMethod
  public boolean isSourceAvailable() {
    return editorContext.isEditing();
  }

  @JsMethod
  public boolean isCType(final CharacteristicsType type) {
    return getSource().getCharacteristicsType() == type;
  }

  @JsMethod
  public boolean isColdStartVehicleBasedCharacteristics() {
    return getSource().getEmissionSourceType() == EmissionSourceType.COLD_START
        && ((ColdStartESFeature) getSource()).isVehicleBasedCharacteristics();
  }

  @JsMethod
  public boolean isESType(final EmissionSourceType type) {
    return getSource().getEmissionSourceType() == type;
  }

  @Computed("hasInvalidName")
  public boolean hasInvalidName() {
    return getLabel() == null || getLabel().trim().isEmpty();
  }

  @Computed("hasSector")
  public boolean hasSector() {
    return getSource().getSectorId() != 0;
  }

  @Computed("hasSectorGroup")
  public boolean hasSectorGroup() {
    return getSelectedSectorGroup() != null;
  }

  @Computed("isEditableSource")
  public boolean isEditableSource() {
    return hasSector();
  }

  @Computed
  public List<Sector> getAvailableSectors() {
    SectorGroup selectedSectorGroup = getSelectedSectorGroup();
    return appContext.getConfiguration().getSectorCategories().getSectors().stream()
        .filter(v -> v.getSectorGroup() == selectedSectorGroup)
        .collect(Collectors.toList());
  }

  @Computed("hasMooring")
  public boolean hasMooring() {
    // For now only show editable mooring part for maritime inland routes.
    // Technically it's possible for maritime maritime routes and inland routes as
    // well, but since it has no effect on emission calculation that part is left
    // out. To re-enable, add checks on EmissionSourceType.SHIPPING_INLAND or
    // EmissionSourceType.SHIPPING_MARITIME_MARITIME For now, only show the block if
    // the source already has mooring connections (for instance, due to import of
    // old GML)
    return isEditableSource() && (isESType(EmissionSourceType.SHIPPING_MARITIME_INLAND) || hasExistingMooring());
  }

  private boolean hasExistingMooring() {
    return hasExistingMooringMaritime() || hasExistingMooringInland();
  }

  private boolean hasExistingMooringMaritime() {
    if (isESType(EmissionSourceType.SHIPPING_MARITIME_MARITIME)
        || isESType(EmissionSourceType.SHIPPING_MARITIME_INLAND)) {
      final MaritimeShippingESFeature source = (MaritimeShippingESFeature) getSource();
      return source.getMooringAId() != null || source.getMooringBId() != null;
    } else {
      return false;
    }
  }

  private boolean hasExistingMooringInland() {
    if (isESType(EmissionSourceType.SHIPPING_INLAND)) {
      final InlandShippingESFeature source = (InlandShippingESFeature) getSource();
      return source.getMooringAId() != null || source.getMooringBId() != null;
    } else {
      return false;
    }
  }

  @Computed
  public List<MooringInlandShippingESFeature> getMooringInland() {
    return scenarioContext.getActiveSituation().getSources().stream()
        .filter(v -> EmissionSourceType.SHIPPING_INLAND_DOCKED == v.getEmissionSourceType())
        .map(v -> (MooringInlandShippingESFeature) v)
        .collect(Collectors.toList());
  }

  @Computed
  public List<MooringMaritimeShippingESFeature> getMooringMaritime() {
    return scenarioContext.getActiveSituation().getSources().stream()
        .filter(v -> EmissionSourceType.SHIPPING_MARITIME_DOCKED == v.getEmissionSourceType())
        .map(v -> (MooringMaritimeShippingESFeature) v)
        .collect(Collectors.toList());
  }

  @Computed("isADMS")
  public boolean isADMS() {
    return hasCharacteristics() && isCType(CharacteristicsType.ADMS);
  }

  @JsMethod
  public void selectSectorGroupType(final SectorGroup option) {
    eventBus.fireEvent(new SectorGroupSelectionCommand(option));
  }

  @JsMethod
  public void changeSector(final Sector sector) {
    eventBus.fireEvent(new EmissionSourceSectorChangeCommand(sector));
  }

  @JsMethod
  protected void openPanel(final EmissionSourcePanel panel) {
    openPanel = panel;
  }

  @JsMethod
  protected void closePanel(final EmissionSourcePanel panel) {
    openPanel = null;
  }

  @JsMethod
  public boolean isOpen(final EmissionSourcePanel esp) {
    return esp == openPanel;
  }

  @JsMethod
  public void cancel() {
    if (Window.confirm(M.messages().esMayCancel())) {
      eventBus.fireEvent(new EmissionSourceEditCancelCommand());
    }
  }

  @JsMethod
  public void closeWarning() {
    setNotificationState(false);
  }

  @JsMethod
  public void closeError() {
    setNotificationState(false);
  }

  @Computed
  public boolean displayFormProblems() {
    return genericValidationContext.isDisplayFormProblems();
  }

  @JsMethod
  public void setNotificationState(final boolean confirm) {
    genericValidationContext.setDisplayFormProblems(confirm);
  }

  @Computed("hasValidationWarnings")
  public boolean hasValidationWarnings() {
    return validationContext.hasWarnings();
  }

  @Computed("hasValidationErrors")
  public boolean hasValidationErrors() {
    return hasInvalidName()
        || !isEditableSource()
        || validationContext.hasErrors();
  }

  @Computed("displaySectorRequiredHint")
  public boolean displaySectorRequiredHint() {
    return validationContext.getSectorRequiredHintWarning();
  }

  @JsMethod
  public void trySave() {
    // Try to save and open the first panel that has errors (if any)
    if (validationContext.getCharacteristicsError()) {
      openPanel = EmissionSourcePanel.SOURCE_CHARACTERISTICS;
    } else if (validationContext.getLocationError()) {
      openPanel = EmissionSourcePanel.LOCATION;
    } else if (validationContext.getEmissionEditorError()
        || validationContext.getDetailEditorError()) {
      openPanel = EmissionSourcePanel.EMISSION_SOURCE_TYPE;
    }
  }

  @JsMethod
  public void save() {
    eventBus.fireEvent(new EmissionSourceEditSaveCommand());
  }

  @JsMethod
  public void resetSelectedSubSource() {
    eventBus.fireEvent(new MapResizeSequenceCommand());
    eventBus.fireEvent(new EmissionSubSourceDeselectFeatureCommand());
  }

  @JsMethod
  public void editSubSource(final int idx) {
    eventBus.fireEvent(new MapResizeSequenceCommand());
    eventBus.fireEvent(new EmissionSubSourceSelectFeatureCommand(idx));
  }

  private EmissionSourcePanel determineBestOpenPanel() {
    final Place prev = placeController.getPreviousPlace();
    final EmissionSourcePanel panelToOpen;

    if (prev instanceof BuildingPlace) {
      panelToOpen = EmissionSourcePanel.SOURCE_CHARACTERISTICS;
    } else if (prev instanceof TimeVaryingProfilePlace) {
      panelToOpen = EmissionSourcePanel.TIME_VARYING_PROFILES;
    } else if (getSource().getGeometry() == null) {
      panelToOpen = EmissionSourcePanel.LOCATION;
    } else {
      panelToOpen = EmissionSourcePanel.EMISSION_SOURCE_TYPE;
    }
    return panelToOpen;
  }
}
