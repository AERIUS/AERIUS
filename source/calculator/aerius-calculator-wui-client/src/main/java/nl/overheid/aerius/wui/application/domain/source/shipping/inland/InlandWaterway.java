/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.shipping.inland;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.wui.vue.ReactivityUtil;

/**
 * Client side implementation of InlandWaterway props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class InlandWaterway {

  private String waterwayCode;
  private String direction;

  public static final @JsOverlay InlandWaterway create() {
    final InlandWaterway props = Js.uncheckedCast(JsPropertyMap.of());
    init(props);
    return props;
  }

  public static final @JsOverlay void init(final InlandWaterway props) {
    ReactivityUtil.ensureDefault(props::getDirection, props::setDirection, WaterwayDirection.IRRELEVANT);
    ReactivityUtil.ensureDefault(props::getWaterwayCode, props::setWaterwayCode, "");
  }

  public final @JsOverlay String getWaterwayCode() {
    return waterwayCode;
  }

  public final @JsOverlay void setWaterwayCode(final String waterwayCode) {
    this.waterwayCode = waterwayCode;
  }

  public final @JsOverlay WaterwayDirection getDirection() {
    return WaterwayDirection.safeValueOf(direction);
  }

  public final @JsOverlay void setDirection(final WaterwayDirection direction) {
    this.direction = direction.name();
  }

}
