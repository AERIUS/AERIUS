/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.characteristics.adms;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;
import jsinterop.base.Js;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.ADMSLimits;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.BuoyancyType;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.EffluxType;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.SourceType;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceSourceTypeChangeCommand;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.source.characteristics.adms.ADMSGenericCharacteristicsEditorValidators.ADMSCharacteristicsEditorValidations;
import nl.overheid.aerius.wui.application.components.source.characteristics.building.BuildingSelectorComponent;
import nl.overheid.aerius.wui.application.components.source.characteristics.timevarying.TimeVaryingProfileSelectorComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.source.ADMSCharacteristics;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.geo.util.GeoType;
import nl.overheid.aerius.wui.application.i18n.M;

/**
 * Editor component of {@link ADMSCharacteristics}.
 */
@Component(customizeOptions = {
    ADMSGenericCharacteristicsEditorValidators.class
}, directives = {
    ValidateDirective.class
}, components = {
    CollapsiblePanel.class,
    SimplifiedListBoxComponent.class,
    LabeledInputComponent.class,
    BuildingSelectorComponent.class,
    ValidationBehaviour.class,
    VerticalCollapse.class,
    TimeVaryingProfileSelectorComponent.class,
})
public class ADMSGenericCharacteristicsEditorComponent extends ErrorWarningValidator
    implements HasCreated, HasValidators<ADMSCharacteristicsEditorValidations> {
  // Jet sources are not supported by ADMS Urban therefore disabled.
  private static final List<SourceType> POINT_TYPES = Arrays.asList(SourceType.POINT/*, SourceType.JET*/);

  // Note: We do not allow SourceType.ROAD to be specified in the generic characteristics panel
  private static final List<SourceType> LINE_TYPES = Arrays.asList(SourceType.LINE);
  private static final List<SourceType> AREA_TYPES = Arrays.asList(SourceType.AREA, SourceType.VOLUME);

  private static final List<EffluxType> SUPPORTED_EFFLUX_TYPES = Arrays.asList(EffluxType.VELOCITY, EffluxType.VOLUME);

  @Prop(required = true) EventBus eventBus;
  @Prop(required = true) SituationContext situation;
  @Prop(required = true) EmissionSourceFeature source;

  @Inject @Data ApplicationContext applicationContext;
  @Inject @Data ScenarioContext scenarioContext;

  @Data ADMSCharacteristics characteristics;

  // Validations
  @JsProperty(name = "$v") ADMSCharacteristicsEditorValidations validation;

  @Data String heightV;
  @Data String widthV;
  @Data String diameterV;
  @Data String verticalDimensionV;
  @Data String densityV;
  @Data String temperatureV;
  @Data String verticalVelocityV;
  @Data String volumetricFlowRateV;
  @Data String massFluxV;
  @Data String specificHeatCapacityV;
  @Data String elevationAngleV;
  @Data String horizontalAngleV;

  @Override
  @Computed
  public ADMSCharacteristicsEditorValidations getV() {
    return validation;
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Computed("hasGeometry")
  public boolean hasGeometry() {
    return source.getGeometry() != null;
  }

  @Computed
  public List<SourceType> getAvailableSourceTypes() {
    if (isPointSource()) {
      return POINT_TYPES;
    } else if (isLineSource()) {
      return LINE_TYPES;
    } else if (isPolygonSource()) {
      return AREA_TYPES;
    }

    return null;
  }

  @Watch(value = "characteristics", isImmediate = true)
  public void onCharacteristicsChange(final ADMSCharacteristics neww) {
    if (neww == null) {
      return;
    }

    heightV = String.valueOf(neww.getHeight());
    widthV = String.valueOf(neww.getWidth());
    diameterV = String.valueOf(neww.getDiameter());
    verticalDimensionV = String.valueOf(neww.getVerticalDimension());
    densityV = String.valueOf(neww.getDensity());
    temperatureV = String.valueOf(neww.getTemperature());
    verticalVelocityV = String.valueOf(neww.getVerticalVelocity());
    volumetricFlowRateV = String.valueOf(neww.getVolumetricFlowRate());
    massFluxV = String.valueOf(neww.getMassFlux());
    specificHeatCapacityV = String.valueOf(neww.getSpecificHeatCapacity());
    elevationAngleV = String.valueOf(neww.getElevationAngle());
    horizontalAngleV = String.valueOf(neww.getHorizontalAngle());
    displayProblems(false);
  }

  @Watch(value = "source", isImmediate = true)
  public void watchSource() {
    characteristics = source == null ? null : Js.uncheckedCast(source.getCharacteristics());
  }

  @Computed("isPointSource")
  protected boolean isPointSource() {
    return GeoType.POINT.is(source.getGeometry());
  }

  @Computed("isPolygonSource")
  protected boolean isPolygonSource() {
    return GeoType.POLYGON.is(source.getGeometry());
  }

  @Computed("isLineSource")
  protected boolean isLineSource() {
    return GeoType.LINE_STRING.is(source.getGeometry());
  }

  @Computed
  public boolean getBuildingInfluence() {
    return Optional.ofNullable(characteristics)
        .map(ADMSCharacteristics::isBuildingInfluence)
        .orElse(false);
  }

  @Computed
  public List<EffluxType> getEffluxTypes() {
    return SUPPORTED_EFFLUX_TYPES;
  }

  @JsMethod
  protected boolean isBuoyancyType(final BuoyancyType type) {
    return characteristics.getBuoyancyType() == type;
  }

  @Computed
  protected SourceType getSourceType() {
    return characteristics.getSourceType();
  }

  @Computed
  public String getSourceTypeConfirmationMessage() {
    // If building influence, ask user to confirm his choice
    return characteristics.isBuildingInfluence()
        && SourceType.POINT == characteristics.getSourceType()
            ? M.messages().sourceADMSSourceTypeConfirmBuildingLinkSever(situation.findBuilding(characteristics.getBuildingGmlId()).getLabel())
            : null;
  }

  @JsMethod
  protected void selectSourceType(final SourceType selectedSourceType) {
    eventBus.fireEvent(new EmissionSourceSourceTypeChangeCommand(source, selectedSourceType));
  }

  @Computed("isVolumeSource")
  protected boolean isVolumeSource() {
    return SourceType.VOLUME == characteristics.getSourceType();
  }

  @Computed("isJetSource")
  protected boolean isJetSource() {
    return SourceType.JET == characteristics.getSourceType();
  }

  @Computed
  protected String getHeight() {
    return heightV;
  }

  @Computed
  protected void setHeight(final String height) {
    ValidationUtil.setSafeDoubleValue(v -> characteristics.setHeight(v), height, minimumSourceHeight());
    heightV = height;
  }

  private double minimumSourceHeight() {
    return isVolumeSource() ? ADMSLimits.SOURCE_HEIGHT_VOLUME_MINIMUM : ADMSLimits.SOURCE_HEIGHT_MINIMUM;
  }

  @Computed
  protected String getHeightValidationWarning() {
    if (shouldShowHeightError() || !shouldShowHeightWarning()) {
      return null;
    }
    return i18n.sourceADMSHeightVolumeSourceNotOnGroundWarning();
  }

  @JsMethod
  protected boolean shouldShowHeightWarning() {
    return !shouldShowHeightError()
        && isVolumeSource()
        && validation.heightV.dirty && isHeightMoreThanHalfOfVerticalDimension();
  }

  private boolean isHeightMoreThanHalfOfVerticalDimension() {
    try {
      return Double.valueOf(verticalDimensionV) - Double.valueOf(heightV) < Double.valueOf(heightV);
    } catch (final NumberFormatException e) {
      return false;
    }
  }

  @Computed
  protected String getHeightValidationError() {
    if (!shouldShowHeightError()) {
      return null;
    }

    if (isVolumeSource() && isHeightLessThanHalfOfVerticalDimension()) {
      // Add second condition because height minimum error is source height, and we also want to check on minimum volume height.
      // Since validation is static class, it would require a second validation specific for volume to use validation for this
      // minimum check, which makes it all more complex.
      return validation.heightV.error || Double.valueOf(heightV) < minimumSourceHeight()
          ? i18n.sourceADMSHeightAtLeastHalfOfVerticalDimensionAndRangeBetween(ADMSLimits.SOURCE_HEIGHT_VOLUME_MINIMUM,
              ADMSLimits.SOURCE_HEIGHT_MAXIMUM)
          : i18n.sourceADMSHeightAtLeastHalfOfVerticalDimension();
    }

    return i18n.ivDoubleRangeBetween(i18n.sourceADMSHeight(), minimumSourceHeight(), ADMSLimits.SOURCE_HEIGHT_MAXIMUM);
  }

  @JsMethod
  protected boolean shouldShowHeightError() {
    return validation.heightV.error
        || isVolumeSource()
            && validation.heightV.dirty && isHeightLessThanHalfOfVerticalDimension();
  }

  private boolean isHeightLessThanHalfOfVerticalDimension() {
    try {
      return Double.valueOf(verticalDimensionV) - Double.valueOf(heightV) > Double.valueOf(heightV);
    } catch (final NumberFormatException e) {
      return false;
    }
  }

  @Computed
  protected String getWidth() {
    return widthV;
  }

  @Computed
  protected void setWidth(final String width) {
    ValidationUtil.setSafeDoubleValue(v -> characteristics.setWidth(v), width, ADMSLimits.SOURCE_WIDTH_INITIAL);
    widthV = width;
  }

  @Computed
  protected String getWidthValidationError() {
    return i18n.ivDoubleRangeBetween(i18n.sourceADMSWidth(), ADMSLimits.SOURCE_WIDTH_MINIMUM, ADMSLimits.SOURCE_WIDTH_MAXIMUM);
  }

  @Computed
  protected String getDiameter() {
    return diameterV;
  }

  @Computed
  protected void setDiameter(final String diameter) {
    ValidationUtil.setSafeDoubleValue(v -> characteristics.setDiameter(v), diameter, ADMSLimits.SOURCE_DIAMETER_MINIMUM);
    diameterV = diameter;
  }

  @Computed
  protected String getDiameterValidationError() {
    return i18n.ivDoubleRangeBetween(i18n.sourceADMSDiameter(), ADMSLimits.SOURCE_DIAMETER_MINIMUM, ADMSLimits.SOURCE_DIAMETER_MAXIMUM);
  }

  @Computed
  protected String getVerticalDimension() {
    return verticalDimensionV;
  }

  @Computed
  protected void setVerticalDimension(final String verticalDimension) {
    ValidationUtil.setSafeDoubleValue(v -> characteristics.setVerticalDimension(v), verticalDimension, ADMSLimits.SOURCE_L1_DEFAULT);
    verticalDimensionV = verticalDimension;
  }

  @Computed
  protected String getVerticalDimensionValidationError() {
    return i18n.ivDoubleRangeBetween(i18n.sourceADMSVerticalDimension(), ADMSLimits.SOURCE_L1_MINIMUM, ADMSLimits.SOURCE_L1_MAXIMUM);
  }

  @Computed
  protected String getBuoyancyType() {
    return characteristics.getBuoyancyType() == null ? null : characteristics.getBuoyancyType().name();
  }

  @Computed
  protected void setBuoyancyType(final String selectedBuoyancyType) {
    characteristics.setBuoyancyType(BuoyancyType.safeValueOf(selectedBuoyancyType));
  }

  @Computed
  protected String getTemperature() {
    return temperatureV;
  }

  @Computed
  protected void setTemperature(final String temperature) {
    ValidationUtil.setSafeDoubleValue(v -> characteristics.setTemperature(v), temperature, ADMSLimits.SOURCE_TEMPERATURE_DEFAULT);
    temperatureV = temperature;
  }

  @Computed
  protected String getTemperatureValidationError() {
    return i18n.ivDoubleRangeBetween(i18n.sourceADMSTemperature(), ADMSLimits.SOURCE_TEMPERATURE_MINIMUM, ADMSLimits.SOURCE_TEMPERATURE_MAXIMUM);
  }

  @Computed
  protected String getDensity() {
    return densityV;
  }

  @Computed
  protected void setDensity(final String density) {
    ValidationUtil.setSafeDoubleValue(v -> characteristics.setDensity(v), density, ADMSLimits.SOURCE_DENSITY_DEFAULT);
    densityV = density;
  }

  @Computed
  protected String getDensityValidationError() {
    return i18n.ivDoubleRangeBetween(i18n.sourceADMSDensity(), ADMSLimits.SOURCE_DENSITY_MINIMUM, ADMSLimits.SOURCE_DENSITY_MAXIMUM);
  }

  @Computed
  protected String getEffluxType() {
    return characteristics.getEffluxType().name();
  }

  @Computed
  protected void setEffluxType(final String selectedEffluxType) {
    characteristics.setEffluxType(EffluxType.safeValueOf(selectedEffluxType));
  }

  @JsMethod
  protected boolean isVerticalVelocity() {
    return characteristics.getEffluxType() != null && characteristics.getEffluxType().isVelocity();
  }

  @Computed
  protected String getVerticalVelocity() {
    return verticalVelocityV;
  }

  @Computed
  protected void setVerticalVelocity(final String verticalVelocity) {
    ValidationUtil.setSafeDoubleValue(v -> characteristics.setVerticalVelocity(v), verticalVelocity, ADMSLimits.SOURCE_VERTICAL_VELOCITY_DEFAULT);
    verticalVelocityV = verticalVelocity;
  }

  @Computed
  protected String getVerticalVelocityValidationError() {
    return i18n.ivDoubleRangeBetween(i18n.sourceADMSVerticalVelocity(), ADMSLimits.SOURCE_VERTICAL_VELOCITY_MINIMUM,
        ADMSLimits.SOURCE_VERTICAL_VELOCITY_MAXIMUM);
  }

  @JsMethod
  protected boolean isVolumetricFlowRate() {
    return characteristics.getEffluxType() != null && characteristics.getEffluxType().isVolumeFlux();
  }

  @Computed
  protected String getVolumetricFlowRate() {
    return volumetricFlowRateV;
  }

  @Computed
  protected void setVolumetricFlowRate(final String volumetricFlowRate) {
    ValidationUtil.setSafeDoubleValue(v -> characteristics.setVolumetricFlowRate(v), volumetricFlowRate,
        ADMSLimits.SOURCE_VOLUMETRIC_FLOW_RATE_DEFAULT);
    volumetricFlowRateV = volumetricFlowRate;
  }

  @Computed
  protected String getVolumetricFlowRateValidationError() {
    return i18n.ivDoubleRangeBetween(i18n.sourceADMSVolumetricFlowRate(), ADMSLimits.SOURCE_VOLUMETRIC_FLOW_RATE_MINIMUM,
        ADMSLimits.SOURCE_VOLUMETRIC_FLOW_RATE_MAXIMUM);
  }

  @JsMethod
  protected boolean isMassFlux() {
    return characteristics.getEffluxType() != null && characteristics.getEffluxType().isMassFlux();
  }

  @Computed
  protected String getMassFlux() {
    return massFluxV;
  }

  @Computed
  protected void setMassFlux(final String massFlux) {
    ValidationUtil.setSafeDoubleValue(m -> characteristics.setMassFlux(m), massFlux, ADMSLimits.SOURCE_MASS_FLUX_DEFAULT);
    massFluxV = massFlux;
  }

  @Computed
  protected String getMassFluxValidationError() {
    return i18n.ivDoubleRangeBetween(i18n.sourceADMSMassFlux(), ADMSLimits.SOURCE_MASS_FLUX_MINIMUM, ADMSLimits.SOURCE_MASS_FLUX_MAXIMUM);
  }

  @Computed
  protected String getSpecificHeatCapacity() {
    return specificHeatCapacityV;
  }

  @Computed
  protected void setSpecificHeatCapacity(final String specificHeatCapacity) {
    ValidationUtil.setSafeDoubleValue(v -> characteristics.setSpecificHeatCapacity(v), specificHeatCapacity, 0.0);
    specificHeatCapacityV = specificHeatCapacity;
  }

  @Computed
  protected String getSpecificHeatCapacityValidationError() {
    return i18n.ivDoubleRangeBetween(i18n.sourceADMSSpecificHeatCapacity(), ADMSLimits.SOURCE_SPECIFIC_HEAT_CAPACITY_MINIMUM,
        ADMSLimits.SOURCE_SPECIFIC_HEAT_CAPACITY_MAXIMUM);
  }

  @Computed
  protected String getElevationAngle() {
    return elevationAngleV;
  }

  @Computed
  protected void setElevationAngle(final String elevationAngle) {
    ValidationUtil.setSafeDoubleValue(v -> characteristics.setElevationAngle(v), elevationAngle, ADMSLimits.ELEVATION_ANGLE_DEFAULT);
    elevationAngleV = elevationAngle;
  }

  @Computed
  protected String getElevationAngleValidationError() {
    return i18n.ivDoubleRangeBetween(i18n.sourceADMSElevationAngle(), ADMSLimits.ELEVATION_ANGLE_MIN, ADMSLimits.ELEVATION_ANGLE_MAX);
  }

  @Computed
  protected String getHorizontalAngle() {
    return horizontalAngleV;
  }

  @Computed
  protected void setHorizontalAngle(final String horizontalAngle) {
    ValidationUtil.setSafeDoubleValue(v -> characteristics.setHorizontalAngle(v), horizontalAngle, ADMSLimits.HORIZONTAL_ANGLE_DEFAULT);
    horizontalAngleV = horizontalAngle;
  }

  @Computed
  protected String getHorizontalAngleValidationError() {
    return i18n.ivDoubleRangeBetween(i18n.sourceADMSHorizontalAngle(), ADMSLimits.HORIZONTAL_ANGLE_MIN, ADMSLimits.HORIZONTAL_ANGLE_MAX);
  }

  @Computed("hasWarnings")
  protected boolean hasWarnings() {
    return shouldShowHeightWarning();
  }

  @Computed("isInvalid")
  protected boolean isInvalid() {
    return BuoyancyType.TEMPERATURE == characteristics.getBuoyancyType() && validation.temperatureV.invalid
        || BuoyancyType.DENSITY == characteristics.getBuoyancyType() && validation.densityV.invalid
        || isLineSource() && validation.widthV.invalid
        || isVolumeSource() && (validation.verticalDimensionV.invalid || isHeightLessThanHalfOfVerticalDimension())
        || isVerticalVelocity() && validation.verticalVelocityV.invalid
        || isVolumetricFlowRate() && validation.volumetricFlowRateV.invalid
        || isMassFlux() && validation.massFluxV.invalid
        || validation.heightV.invalid
        || isPointSource() && validation.diameterV.invalid
        || validation.specificHeatCapacityV.invalid
        || isJetSource() && validation.verticalDimensionV.invalid
        || isJetSource() && validation.horizontalAngleV.invalid;
  }
}
