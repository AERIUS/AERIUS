/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.apikey;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasDestroyed;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import elemental2.dom.HTMLElement;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.wui.application.command.geo.LoginPopupHiddenCommand;
import nl.overheid.aerius.wui.application.command.geo.LoginPopupResetCommand;
import nl.overheid.aerius.wui.application.command.geo.LoginPopupVisibleCommand;
import nl.overheid.aerius.wui.application.components.map.LoginPanelDockChangeCommand;
import nl.overheid.aerius.wui.application.components.map.LoginPanelDockRemoveCommand;
import nl.overheid.aerius.wui.application.components.modal.PanelizedModalComponent;
import nl.overheid.aerius.wui.application.context.LoginPanelContext;
import nl.overheid.aerius.wui.application.util.ScreenUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    LoginPanel.class,
    PanelizedModalComponent.class
})
public class LoginPanelModal extends BasicVueComponent implements IsVueComponent, HasCreated, HasDestroyed {
  private static final LoginPanelModalEventBinder EVENT_BINDER = GWT.create(LoginPanelModalEventBinder.class);

  interface LoginPanelModalEventBinder extends EventBinder<LoginPanelModal> {}

  @Prop EventBus eventBus;

  @Data @Inject LoginPanelContext context;

  @Ref PanelizedModalComponent modal;

  private HandlerRegistration handlers;

  @Data HTMLElement dock;

  @EventHandler
  public void onLoginPanelDockRemoveCommand(final LoginPanelDockRemoveCommand c) {
    if (dock == c.getValue()) {
      dock = null;
    }
  }

  @EventHandler
  public void onLoginPanelDockChangeCommand(final LoginPanelDockChangeCommand c) {
    dock = c.getValue();
    dock.parentNode.insertBefore(modal.vue().$el(), dock.nextSibling);
    SchedulerUtil.delay(() -> {
      ScreenUtil.attachElementLeft(dock, modal.vue().$el(), ScreenUtil.OFFSET_LEFT);
    });
  }

  @JsMethod
  public void close() {
    eventBus.fireEvent(new LoginPopupHiddenCommand());
  }

  @EventHandler
  public void onLoginPopupResetCommand(final LoginPopupResetCommand c) {
    modal.reset();
    eventBus.fireEvent(new LoginPopupVisibleCommand());
  }

  @Override
  public void created() {
    handlers = EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Override
  public void destroyed() {
    if (handlers != null) {
      handlers.removeHandler();
    }
  }
}
