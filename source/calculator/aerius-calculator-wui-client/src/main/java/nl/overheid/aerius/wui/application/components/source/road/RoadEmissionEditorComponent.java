/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.source.road;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import elemental2.core.JsArray;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.shared.domain.sector.category.SimpleCategory;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.shared.domain.v2.source.road.TrafficDirection;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.modify.ModifyListComponent;
import nl.overheid.aerius.wui.application.components.source.SubSourceEditorComponent;
import nl.overheid.aerius.wui.application.components.source.SubSourceValidationBehaviour;
import nl.overheid.aerius.wui.application.components.source.road.RoadEmissionEditorValidators.RoadEmissionEditorValidations;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.SubSourceEmptyError;
import nl.overheid.aerius.wui.application.components.source.validation.SubSourceValidatedRowComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.components.toggle.ToggleButtons;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.road.StandardVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.VehicleType;
import nl.overheid.aerius.wui.application.domain.source.road.Vehicles;

@Component(name = "aer-road-emission-editor", customizeOptions = {
    RoadEmissionEditorValidators.class
}, directives = {
    ValidateDirective.class
}, components = {
    ModifyListComponent.class,
    RoadEmissionEditorRowComponent.class,
    SubSourceValidationBehaviour.class,
    SubSourceValidatedRowComponent.class,
    SubSourceEmptyError.class,
    ToggleButtons.class,
    VerticalCollapseGroup.class,
    LabeledInputComponent.class,
    ValidationBehaviour.class,
})
public class RoadEmissionEditorComponent extends SubSourceEditorComponent implements HasCreated,
    HasValidators<RoadEmissionEditorValidations> {
  @Prop RoadESFeature source;

  @Data RoadEmissionEditorActivity presenter;
  @Data TrafficDirection toggleSelect;
  @Data String roadAreaCodeV;
  @Data String roadTypeCodeV;

  @Data @Inject ApplicationContext applicationContext;

  @JsProperty(name = "$v") RoadEmissionEditorValidations validation;

  @Watch(value = "source", isImmediate = true)
  public void watchSource() {
    resetSelected();
    toggleSelect = source.getTrafficDirection() == null ? TrafficDirection.BOTH : source.getTrafficDirection();
    if (hasOnePossibleArea()) {
      source.setRoadAreaCode(getAreas().get(0).getCode());
    }
    roadAreaCodeV = source.getRoadAreaCode();
    roadTypeCodeV = source.getRoadTypeCode();
  }

  @Override
  @Computed
  public RoadEmissionEditorValidations getV() {
    return validation;
  }

  @Computed
  public List<SimpleCategory> getAreas() {
    return applicationContext.getConfiguration().getSectorCategories().getRoadEmissionCategories().getAreas();
  }

  @Computed("hasOnePossibleArea")
  public boolean hasOnePossibleArea() {
    return getAreas().size() == 1;
  }

  @Computed
  public String getArea() {
    return roadAreaCodeV;
  }

  @Computed
  public void setArea(final String areaCode) {
    source.setRoadAreaCode(areaCode);
    roadAreaCodeV = areaCode;
  }

  @Computed("roadTypes")
  public List<SimpleCategory> getRoadTypes() {
    return applicationContext.getConfiguration().getSectorCategories().getRoadEmissionCategories().getRoadTypes(roadAreaCodeV);
  }

  @Watch("getRoadTypes()")
  public void onRoadTypesChange(final List<SimpleCategory> newRoadTypes) {
    if (newRoadTypes.stream()
        .map(v -> v.getCode())
        .noneMatch(v -> v.equals(roadTypeCodeV))) {
      roadTypeCodeV = null;
      presenter.resetSelected();
    }
  }

  @Computed
  public String getRoadType() {
    return roadTypeCodeV;
  }

  @Computed
  public void setRoadType(final String roadType) {
    source.setRoadTypeCode(roadType);
    roadTypeCodeV = roadType;

    final JsArray<Vehicles> vehiclesActivity = source.getSubSources();
    for (int i = 0; i < vehiclesActivity.length; i++) {
      final Vehicles vehicle = vehiclesActivity.getAt(i);
      if (vehicle.getVehicleType() == VehicleType.STANDARD && source.getEmissionSourceType() != EmissionSourceType.ADMS_ROAD) {
        ((StandardVehicles) vehicle).setMaximumSpeed(0);
        ((StandardVehicles) vehicle).setStrictEnforcement(false);
        setSubSourceError(i, isRoadSpeedRequired());
      }
    }
  }

  @Computed
  List<TrafficDirection> getTrafficDirections() {
    return Arrays.asList(TrafficDirection.values());
  }

  @Computed("roadSource")
  protected RoadESFeature getRoadSource() {
    return source;
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
    presenter = new RoadEmissionEditorActivity();
    presenter.setView(this);
  }

  @JsMethod
  protected void selectSource(final Number index, final Vehicles selectedSource) {
    if (selectedIndex.equals(index.intValue())) {
      selectedIndex = -1;
      resetSelected();
    } else {
      selectedIndex = index.intValue();
      presenter.selectedSource(selectedSource);
      editSource();
    }
  }

  @JsMethod
  protected void setToggle(final TrafficDirection trafficDirection) {
    toggleSelect = trafficDirection == null ? TrafficDirection.BOTH : trafficDirection;
    source.setTrafficDirection(trafficDirection);
  }

  @Computed("directionIconName")
  protected String getDirectionIconName() {
    String iconName = "";
    switch (toggleSelect) {
    case BOTH:
      iconName = "icon-traffic-flow-both-directions";
      break;
    case A_TO_B:
      iconName = "icon-traffic-flow-a-to-b";
      break;
    case B_TO_A:
      iconName = "icon-traffic-flow-b-to-a";
      break;
    }
    return iconName;
  }

  @JsMethod
  protected String roadAreaRequiredError() {
    return i18n.errorRoadAreaRequired();
  }

  @JsMethod
  protected String roadTypeRequiredError() {
    return i18n.errorRoadTypeRequired();
  }

  @Computed("isInvalid")
  protected boolean isInvalid() {
    return validation.roadAreaCodeV.invalid
        || validation.roadTypeCodeV.invalid
        || source.getSubSources().length == 0
        || hasErrors();
  }

  private boolean isRoadSpeedRequired() {
    return applicationContext.getConfiguration().getSectorCategories().getRoadEmissionCategories()
        .getMaximumSpeedCategories(roadAreaCodeV, roadTypeCodeV).size() > 1;
  }
}
