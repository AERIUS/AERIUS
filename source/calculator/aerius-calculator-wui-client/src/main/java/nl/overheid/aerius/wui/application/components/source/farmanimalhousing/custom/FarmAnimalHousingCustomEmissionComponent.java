/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.farmanimalhousing.custom;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.AnimalType;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.farmanimalhousing.custom.FarmAnimalHousingCustomValidators.FarmAnimalHousingCustomValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.farm.CustomFarmAnimalHousing;
import nl.overheid.aerius.wui.vue.DebugDirective;

/**
 * Component to edit a Custom FarmAnimalHousing Custom sub source.
 */
@Component(customizeOptions = FarmAnimalHousingCustomValidators.class, directives = {
    ValidateDirective.class,
    DebugDirective.class
}, components = {
    LabeledInputComponent.class,
    ValidationBehaviour.class,
    VerticalCollapse.class
})
public class FarmAnimalHousingCustomEmissionComponent extends ErrorWarningValidator
    implements HasCreated, HasValidators<FarmAnimalHousingCustomValidations> {
  @Prop @JsProperty CustomFarmAnimalHousing customFarmAnimalHousing;

  @Data String descriptionV;
  @Data String emissionFactorV;
  @Data String numberOfAnimalsV;
  @Data String numberOfDaysV;
  @Data FarmEmissionFactorType farmEmissionFactorTypeV = FarmEmissionFactorType.PER_ANIMAL_PER_YEAR;

  @JsProperty(name = "$v") FarmAnimalHousingCustomValidations validation;

  @Inject @Data ApplicationContext applicationContext;

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Override
  @Computed
  public FarmAnimalHousingCustomValidations getV() {
    return validation;
  }

  @Watch(value = "customFarmAnimalHousing", isImmediate = true)
  protected void onCustomFarmAnimalHousingChange() {
    descriptionV = String.valueOf(customFarmAnimalHousing.getDescription());
    emissionFactorV = String.valueOf(customFarmAnimalHousing.getEmissionFactor(Substance.NH3));
    numberOfAnimalsV = String.valueOf(customFarmAnimalHousing.getNumberOfAnimals());
    numberOfDaysV = String.valueOf(customFarmAnimalHousing.getNumberOfDays());
    setFarmEmissionFactorType(customFarmAnimalHousing.getFarmEmissionFactorType() != null ? customFarmAnimalHousing.getFarmEmissionFactorType().name()
        : FarmEmissionFactorType.PER_ANIMAL_PER_YEAR.name());
    validation.$reset();
  }

  @Computed
  protected String getDescription() {
    return descriptionV;
  }

  @Computed
  protected void setDescription(final String description) {
    customFarmAnimalHousing.setDescription(description);
    descriptionV = description;
  }

  @Computed
  protected String getAnimalType() {
    return AnimalType.safeValueOf(customFarmAnimalHousing.getAnimalTypeCode()).name();
  }

  @Computed
  protected void setAnimalType(final String animalType) {
    customFarmAnimalHousing.setAnimalTypeCode(AnimalType.safeValueOf(animalType).name());
  }

  @Computed
  protected String getNumberOfAnimals() {
    return numberOfAnimalsV;
  }

  @Computed
  protected void setNumberOfAnimals(final String numberOfAnimals) {
    ValidationUtil.setSafeIntegerValue(v -> customFarmAnimalHousing.setNumberOfAnimals(v), numberOfAnimals, 0);
    this.numberOfAnimalsV = numberOfAnimals;
  }

  @Computed
  protected String getFarmEmissionFactorType() {
    return this.farmEmissionFactorTypeV.name();
  }

  @Computed
  protected void setFarmEmissionFactorType(final String farmEmissionFactorType) {
    this.farmEmissionFactorTypeV = FarmEmissionFactorType.valueOf(farmEmissionFactorType);
    this.customFarmAnimalHousing.setFarmEmissionFactorType(farmEmissionFactorTypeV);
  }

  @Computed
  protected String getEmissionFactor() {
    return emissionFactorV;
  }

  @Computed
  protected void setEmissionFactor(final String emissionFactor) {
    ValidationUtil.setSafeDoubleValue(ef -> customFarmAnimalHousing.setEmissionFactor(Substance.NH3, ef), emissionFactor, 0D);
    this.emissionFactorV = emissionFactor;
  }

  @Computed
  protected String getNumberOfDays() {
    return numberOfDaysV;
  }

  @Computed
  protected void setNumberOfDays(final String numberOfDays) {
    ValidationUtil.setSafeIntegerValue(v -> customFarmAnimalHousing.setNumberOfDays(v), numberOfDays, 0);
    this.numberOfDaysV = numberOfDays;
  }

  @Computed
  protected List<FarmEmissionFactorType> getFarmEmissionFactorTypes() {
    return applicationContext.getConfiguration().getCustomFarmAnimalHousingEmissionFactorTypes();
  }

  @JsMethod
  protected String emissionFactorConversionError() {
    return i18n.errorDecimal(emissionFactorV);
  }

  @JsMethod
  protected String numberOfAnimalsConversionError() {
    return i18n.errorNumeric(numberOfAnimalsV);
  }

  @JsMethod
  protected String numberOfDaysConversionError() {
    return i18n.errorNumeric(numberOfDaysV);
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return validation.descriptionV.invalid
        || validation.emissionFactorV.invalid
        || validation.numberOfAnimalsV.invalid
        || isInvalidNumberOfDays()
        || validation.farmEmissionFactorTypeV.invalid;
  }

  @JsMethod
  protected boolean expectsNumberOfDays() {
    return farmEmissionFactorTypeV != null && FarmEmissionFactorType.expectsNumberOfDays(farmEmissionFactorTypeV);
  }

  private boolean isInvalidNumberOfDays() {
    return expectsNumberOfDays() && validation.numberOfDaysV.invalid;
  }
}
