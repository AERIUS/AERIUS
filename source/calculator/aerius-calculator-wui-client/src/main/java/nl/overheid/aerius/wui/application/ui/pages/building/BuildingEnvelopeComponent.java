/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.building;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import ol.geom.Geometry;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.domain.v2.building.BuildingLimits;
import nl.overheid.aerius.wui.application.components.source.DetailStatComponent;
import nl.overheid.aerius.wui.application.components.source.characteristics.MinMaxLabelComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.geo.util.GeoType;
import nl.overheid.aerius.wui.application.geo.util.GeoUtil;
import nl.overheid.aerius.wui.application.geo.util.OrientedEnvelope;
import nl.overheid.aerius.wui.application.i18n.M;

@Component(components = {
    VerticalCollapse.class,
    BuildingPreviewComponent.class,
    DetailStatComponent.class,
    MinMaxLabelComponent.class,
    ValidationBehaviour.class,
})
public class BuildingEnvelopeComponent extends ErrorWarningValidator implements IsVueComponent {
  private static final double SCOPE_BUILDING_RATIO_MINIMUM = 0.15;

  @Inject ApplicationContext applicationContext;

  @Prop BuildingLimits buildingLimits;
  @Prop Geometry geometry;
  @Prop Double height;
  @Prop Double diameter;

  @Prop boolean forgiveHeightWarning;
  @Prop String backgroundColor;

  @Data OrientedEnvelope envelope;

  @PropDefault("forgiveHeightWarning")
  public boolean forgiveHeightWarningDefault() {
    return false;
  }

  @PropDefault("backgroundColor")
  public String backgroundColorDefault() {
    return "var(--back-color)";
  }

  @Computed("hasEnvelope")
  public boolean hasEnvelope() {
    return envelope != null;
  }

  @Watch(value = "geometry", isImmediate = true, isDeep = true)
  public void onGeometryChange(final Geometry neww) {
    if (neww == null) {
      envelope = null;
    } else {
      try {
        envelope = GeoUtil.determineOrientedEnvelope(geometry);
      } catch (final Exception e) {
        envelope = null;
      }
    }
  }

  @JsMethod
  public boolean isInvalidGeometry() {
    if (geometry == null) {
      return false;
    }

    try {
      GeoUtil.determineOrientedEnvelope(geometry);
      return false;
    } catch (final Exception e) {
      return true;
    }
  }

  @JsMethod
  public boolean isPoint() {
    return GeoType.POINT.is(geometry);
  }

  @JsMethod
  public boolean isPolygon() {
    return GeoType.POLYGON.is(geometry);
  }

  @Computed("isBuildingLengthCorrected")
  public boolean isBuildingLengthCorrected() {
    return envelope.getLength() > buildingLimits.buildingLengthMaximum();
  }

  @Computed
  public double getUsedBuildingLength() {
    return Math.min(buildingLimits.buildingLengthMaximum(), envelope.getLength());
  }

  @Computed
  public String getUsedBuildingLengthStr() {
    return i18n.unitM(i18n.decimalNumberFixed(getUsedBuildingLength(), buildingLimits.buildingDigitsPrecision()));
  }

  @Computed
  public String getUsedBuildingHeightStr() {
    return i18n.unitM(i18n.decimalNumberFixed(getUsedBuildingHeight(), buildingLimits.buildingDigitsPrecision()));
  }

  @Computed
  public String getBuildingHeightValidationMessage() {
    if (applicationContext.isAppFlagEnabled(AppFlag.HAS_BUILDING_MINIMUM_HEIGHT_LARGER_ZERO)
        && height < buildingLimits.buildingHeightMinimum()) {
      return i18n.buildingLocationDimensionHeightMinimumCorrectedMessage(buildingLimits.buildingHeightMinimum());
    } else {
      return i18n.buildingLocationDimensionHeightMaximumCorrectedMessage(buildingLimits.buildingHeightMaximum());
    }
  }

  @Computed
  public String getUsedBuildingWidthStr() {
    return i18n.unitM(i18n.decimalNumberFixed(getUsedBuildingWidth(), buildingLimits.buildingDigitsPrecision()));
  }

  @Computed
  public double getUsedBuildingHeight() {
    return Math.max(buildingLimits.buildingHeightMinimum(), Math.min(buildingLimits.buildingHeightMaximum(), height));
  }

  @Computed("isBuildingWidthCorrected")
  public boolean isBuildingWidthCorrected() {
    return envelope.getWidth() > buildingLimits.buildingWidthMaximum();
  }

  @Computed
  public double getUsedBuildingWidth() {
    return Math.min(buildingLimits.buildingWidthMaximum(), envelope.getWidth());
  }

  @Computed
  public String getBuildingLength() {
    return i18n.unitM(i18n.decimalNumberFixed(envelope.getLength(), buildingLimits.buildingDigitsPrecision()));
  }

  @Computed
  public String getBuildingWidth() {
    return i18n.unitM(i18n.decimalNumberFixed(envelope.getWidth(), buildingLimits.buildingDigitsPrecision()));
  }

  @Computed("isBuildingHeightCorrected")
  public boolean isBuildingHeightCorrected() {
    return (applicationContext.isAppFlagEnabled(AppFlag.HAS_BUILDING_MINIMUM_HEIGHT_LARGER_ZERO)
        && height < buildingLimits.buildingHeightMinimum())
        || height > buildingLimits.buildingHeightMaximum();
  }

  @Computed
  public String getBuildingHeight() {
    return i18n.unitM(i18n.decimalNumberFixed(height, buildingLimits.buildingDigitsPrecision()));
  }

  @Computed
  public String getBuildingDiameter() {
    return diameter == null ? "" : i18n.unitM(i18n.decimalNumberFixed(diameter, buildingLimits.buildingDigitsPrecision()));
  }

  @Computed
  public String getBuildingOrientation() {
    return M.messages().decimalNumberFixed(envelope.getOrientation(), 0) + M.messages().unitSingularDegrees();
  }

  @Computed("buildingRatio")
  public double getBuildingRatio() {
    return getUsedBuildingWidth() / getUsedBuildingLength();
  }

  @Computed("formattedBuildingRatio")
  public String getFormattedBuildingRatio() {
    return i18n.decimalNumberFixed(getBuildingRatio(), 3);
  }

  @Computed("isBuildingRatioViolated")
  public boolean isBuildingRatioViolated() {
    return envelope != null && getBuildingRatio() < SCOPE_BUILDING_RATIO_MINIMUM;
  }

  @Computed("isBuildingDimensionViolated")
  public boolean isBuildingDimensionViolated() {
    return isBuildingLengthCorrected()
        || isBuildingWidthCorrected()
        || !forgiveHeightWarning && isBuildingHeightCorrected();
  }
}
