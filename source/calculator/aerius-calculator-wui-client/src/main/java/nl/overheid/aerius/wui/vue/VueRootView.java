/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.vue;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.vue.VueComponentFactory;
import com.google.web.bindery.event.shared.EventBus;

import nl.aerius.wui.activity.Presenter;
import nl.aerius.wui.event.HasEventBus;
import nl.aerius.wui.vue.AcceptsOneComponent;
import nl.overheid.aerius.wui.application.print.nca.PrintNcaView;
import nl.overheid.aerius.wui.application.print.own2000.PrintOwN2000View;
import nl.overheid.aerius.wui.application.ui.pages.ThemeView;
import nl.overheid.aerius.wui.application.ui.unsupported.NotSupportedView;

@Component(components = {
    ThemeView.class,
    NotSupportedView.class,
    PrintOwN2000View.class,
    PrintNcaView.class,
})
public class VueRootView implements IsVueComponent, AcceptsOneComponent, HasEventBus, Presenter {
  @Data String id;
  @Data Object presenter;

  @Data EventBus eventBus;

  @Override
  public <P extends Presenter> void setComponent(final VueComponentFactory<?> factory, final P presenter) {
    id = factory.getComponentTagName();
    this.presenter = presenter;
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    this.eventBus = eventBus;
  }
}
