/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.emissionsourceList.source;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Emit;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;

import nl.aerius.geo.wui.util.OL3GeometryUtil;
import nl.aerius.wui.vue.transition.HorizontalCollapse;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.components.divider.DividerComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.DetailStatComponent;
import nl.overheid.aerius.wui.application.components.source.characteristics.OPSCharacteristicsComponent;
import nl.overheid.aerius.wui.application.components.source.characteristics.adms.ADMSCharacteristicsComponent;
import nl.overheid.aerius.wui.application.components.source.coldstart.detail.ColdStartDetailComponent;
import nl.overheid.aerius.wui.application.components.source.combustion.CombustionPlantDetailComponent;
import nl.overheid.aerius.wui.application.components.source.farmanimalhousing.detail.FarmAnimalHousingDetailComponent;
import nl.overheid.aerius.wui.application.components.source.farmland.detail.FarmlandDetailComponent;
import nl.overheid.aerius.wui.application.components.source.farmlodging.detail.FarmLodgingDetailComponent;
import nl.overheid.aerius.wui.application.components.source.generic.GenericDetailComponent;
import nl.overheid.aerius.wui.application.components.source.manure.detail.ManureStorageDetailComponent;
import nl.overheid.aerius.wui.application.components.source.offroad.detail.OffroadDetailComponent;
import nl.overheid.aerius.wui.application.components.source.road.adms.ADMSRoadCharacteristicsComponent;
import nl.overheid.aerius.wui.application.components.source.road.detail.SRM2RoadDetailComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.inland.detail.InlandShippingDetailComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.maritime.detail.MaritimeShippingDetailComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.domain.source.ColdStartESFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.OPSCharacteristics;
import nl.overheid.aerius.wui.application.domain.source.SourceCharacteristics;
import nl.overheid.aerius.wui.application.domain.source.TimeVaryingProfile;
import nl.overheid.aerius.wui.application.domain.source.util.TimeVaryingProfileUtil;
import nl.overheid.aerius.wui.application.domain.source.util.TimeVaryingProfileUtilBase;
import nl.overheid.aerius.wui.application.geo.util.GeoUtil;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.application.ui.nca.pages.emisionsourceList.timevarying.TimeVaryingProfileDetailView;
import nl.overheid.aerius.wui.application.ui.pages.emissionsourceList.building.BuildingDetailView;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    LabeledInputComponent.class,
    ADMSCharacteristicsComponent.class,
    OPSCharacteristicsComponent.class,
    GenericDetailComponent.class,
    FarmAnimalHousingDetailComponent.class,
    FarmLodgingDetailComponent.class,
    FarmlandDetailComponent.class,
    ManureStorageDetailComponent.class,
    SRM2RoadDetailComponent.class,
    ColdStartDetailComponent.class,
    ADMSRoadCharacteristicsComponent.class,
    InlandShippingDetailComponent.class,
    MaritimeShippingDetailComponent.class,
    CombustionPlantDetailComponent.class,
    OffroadDetailComponent.class,
    HorizontalCollapse.class,
    VerticalCollapse.class,
    BuildingDetailView.class,
    DetailStatComponent.class,
    TimeVaryingProfileDetailView.class,
    DividerComponent.class
})
public class EmissionSourceListDetailView extends BasicVueComponent {
  @Prop EmissionSourceFeature source;

  @Inject @Data ScenarioContext scenarioContext;
  @Inject @Data ApplicationContext applicationContext;

  @Computed
  public String getLocation() {
    return Optional.ofNullable(OL3GeometryUtil.getMiddlePointOfGeometry(source.getGeometry()))
        .map(v -> MessageFormatter.formatPoint(v))
        .orElse(null);
  }

  @Computed("isVehicleBasedCharacteristics")
  boolean isVehicleBasedCharacteristics() {
    return source.getEmissionSourceType() == EmissionSourceType.COLD_START
        && ((ColdStartESFeature) source).isVehicleBasedCharacteristics();
  }

  @Computed
  String getLocationStatisticLabel() {
    return GeoUtil.getGeometryStatisticLabel(source.getGeometry());
  }

  @Computed
  String getLocationStatisticValue() {
    return GeoUtil.getGeometryStatisticValue(source.getGeometry());
  }

  @Computed
  public boolean hasBuilding() {
    return Optional.ofNullable(source)
        .map(EmissionSourceFeature::getCharacteristics)
        .map(SourceCharacteristics::isBuildingInfluence)
        .orElse(false) && !isVehicleBasedCharacteristics();
  }

  @Computed
  public boolean showTimeVaryingProfile() {
    return applicationContext.getTheme() == Theme.NCA;
  }

  @Computed
  TimeVaryingProfile threeDayProfile() {
    return timeVaryingProfile(TimeVaryingProfileUtil.THREE_DAY_PROFILE_UTIL);

  }

  @Computed
  TimeVaryingProfile monthlyProfile() {
    return timeVaryingProfile(TimeVaryingProfileUtil.MONTHLY_PROFILE_UTIL);
  }

  private TimeVaryingProfile timeVaryingProfile(final TimeVaryingProfileUtilBase util) {
    return Optional.ofNullable(util.findTimeVaryingProfile(source, scenarioContext.getActiveSituation(),
        applicationContext.getConfiguration().getSectorCategories().getTimeVaryingProfiles())).orElse(util.getContinuousProfile());
  }

  @Computed
  public BuildingFeature getBuilding() {
    if (isCType(source.getCharacteristicsType())) {
      return Optional.ofNullable((OPSCharacteristics) source.getCharacteristics())
          .map(v -> v.getBuildingGmlId())
          .map(v -> scenarioContext.getActiveSituation().findBuilding(v))
          .orElse(null);
    } else {
      return null;
    }
  }

  @Computed
  public SectorGroup getSectorGroup() {
    return applicationContext.getConfiguration().getSectorCategories().getSectors()
        .stream().filter(v -> v.getSectorId() == source.getSectorId())
        .findFirst()
        .map(v -> v.getSectorGroup())
        .orElse(null);
  }

  @Computed("substances")
  public List<Substance> getSubstances() {
    return applicationContext.getConfiguration().getSubstances();
  }

  @Computed("substancesRoad")
  public List<Substance> getSubstancesRoad() {
    return applicationContext.getConfiguration().getEmissionSubstancesRoad();
  }

  @JsMethod
  public boolean isCType(final CharacteristicsType type) {
    return source.getCharacteristicsType() == type;
  }

  @Computed
  public Sector getSector() {
    return applicationContext.getConfiguration().getSectorCategories().determineSectorById(source.getSectorId());
  }

  @JsMethod
  public boolean isESType(final EmissionSourceType type) {
    return source.getEmissionSourceType() == type;
  }

  @JsMethod
  public boolean hasSectors(final SectorGroup sectorGroup) {
    return applicationContext.getConfiguration().getSectorCategories().getSectors().stream()
        .filter(v -> v.getSectorGroup() == sectorGroup)
        .count() > 1;
  }

  @JsMethod
  public String getTotalEmissionTitle(final Sector sector, final SectorGroup sectorGroup) {
    return hasSectors(sectorGroup) ? i18n.esTotalEmission(sector.getDescription().toLowerCase(), i18n.sectorGroup(sectorGroup).toLowerCase())
        : i18n.esTotalEmissionSimple(sector.getDescription().toLowerCase());
  }

  @JsMethod
  @Emit
  public void close() {}
}
