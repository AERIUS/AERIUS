/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.source.farmland;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Emit;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.sector.category.FarmlandCategory;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.farmland.FarmlandActivity;
import nl.overheid.aerius.wui.application.domain.source.farmland.FarmlandUtil;
import nl.overheid.aerius.wui.application.domain.source.farmland.StandardFarmlandActivity;
import nl.overheid.aerius.wui.application.resources.util.FarmIconUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "aer-farmland-emission-row-editor")
public class FarmlandEmissionEditorRowComponent extends BasicVueComponent {
  @Prop @JsProperty FarmlandActivity source;
  @Data @Inject ApplicationContext applicationContext;

  @Prop boolean selected;

  @Computed("icon")
  public String getIcon() {
    return FarmIconUtil.getFarmlandIcon(source.getActivityCode());
  }

  @Computed("description")
  public String getDescription() {
    FarmlandCategory selectedFarmLandCategory = FarmlandUtil.findFarmland(source.getActivityCode(),
        applicationContext.getConfiguration().getSectorCategories().getFarmlandCategories());
    return selectedFarmLandCategory != null ? selectedFarmLandCategory.getDescription() : "";
  }

  @JsMethod
  @Emit
  public void select() {}

}
