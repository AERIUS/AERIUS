/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.calculate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.backgrounddata.MetSite;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.sector.category.AbstractCategory;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ExportContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.domain.calculation.ADMSOptions;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.application.domain.calculation.NCACalculationOptions;
import nl.overheid.aerius.wui.application.i18n.M;

/**
 * Common validations for calculations within a UI context
 */
public final class CalculateValidations {

  private static final Map<CalculationJobType, Integer> MAXIMUM_NUMBER_OF_SITUATIONS = new HashMap<>();
  static {
    MAXIMUM_NUMBER_OF_SITUATIONS.put(CalculationJobType.PROCESS_CONTRIBUTION, 3);
    MAXIMUM_NUMBER_OF_SITUATIONS.put(CalculationJobType.DEPOSITION_SUM, 1);
    MAXIMUM_NUMBER_OF_SITUATIONS.put(CalculationJobType.SINGLE_SCENARIO, 1);
  }

  private CalculateValidations() {}

  public static Integer getMaxNumberOfSituations(final CalculationJobType calculationJobType,
      final ApplicationConfiguration appThemeConfiguration) {
    return MAXIMUM_NUMBER_OF_SITUATIONS.getOrDefault(calculationJobType, appThemeConfiguration.getMaxNumberOfSituations());
  }

  public static boolean hasMissingRequiredSituation(final SituationComposition situationComposition, final CalculationJobType calculationJobType) {
    for (final SituationType requiredType : calculationJobType.getRequiredSituationTypes()) {
      if (isRequiredTypeMissing(situationComposition, requiredType)) {
        return true;
      }
    }
    return false;
  }

  public static boolean isRequiredTypeMissing(final SituationComposition situationComposition, final SituationType requiredType) {
    return situationComposition.getSituationsByType(requiredType).isEmpty();
  }

  private static boolean expectsPermitArea(final ApplicationContext context) {
    return context.isAppFlagEnabled(AppFlag.HAS_CALCULATION_PERMIT_AREA);
  }

  private static boolean hasPermitArea(final NCACalculationOptions options) {
    return options.getPermitArea() != null;
  }

  public static boolean hasInvalidPermitArea(final ApplicationContext appContext,
      final NCACalculationOptions options) {
    return expectsPermitArea(appContext)
        && !(hasPermitArea(options)
            && isValidCategory(appContext.getConfiguration().getCalculationPermitAreas(), options.getPermitArea()));
  }

  private static boolean expectsProjectCategory(final ApplicationContext context) {
    return context.isAppFlagEnabled(AppFlag.HAS_CALCULATION_PROJECT_CATEGORY);
  }

  private static boolean hasProjectCategory(final NCACalculationOptions options) {
    return options.getProjectCategory() != null;
  }

  public static boolean hasInvalidProjectCategory(final ApplicationContext appContext,
      final NCACalculationOptions options) {
    return expectsProjectCategory(appContext)
        && !(hasProjectCategory(options)
            && isValidCategory(appContext.getConfiguration().getCalculationProjectCategories(), options.getProjectCategory()));
  }

  private static boolean isValidCategory(final List<? extends AbstractCategory> categories, final String code) {
    return AbstractCategory.determineByCode(categories, code) != null;
  }

  public static String formatRequiredSituationTypes(final SituationComposition comp, final CalculationJobType type) {
    return type.getRequiredSituationTypes().stream()
        .filter(st -> comp.getSituationsByType(st).isEmpty())
        .map(v -> M.messages().situationType(v))
        .collect(Collectors.joining(", "));
  }

  public static boolean hasTooManySituations(final SituationComposition situationComposition,
      final ApplicationConfiguration appThemeConfiguration) {
    return situationComposition.getSituations().size() > appThemeConfiguration.getMaxNumberOfSituations();
  }

  public static boolean hasTooManySituationsForType(final SituationComposition comp, final CalculationJobType calculationJobType) {
    return MAXIMUM_NUMBER_OF_SITUATIONS.containsKey(calculationJobType)
        && comp.getSituations().size() > MAXIMUM_NUMBER_OF_SITUATIONS.get(calculationJobType);
  }

  public static boolean hasInvalidCustomPointsOption(final CalculationSetOptions options, final ScenarioContext scenarioContext) {
    return options.getCalculationMethod() == CalculationMethod.CUSTOM_POINTS
        && scenarioContext.getCalculationPoints().isEmpty();
  }

  public static boolean hasNoEmissionsInAnyOfComposition(final SituationComposition situationComposition) {
    return !hasTooFewSituations(situationComposition) && situationComposition.getSituations().stream()
        .anyMatch(sit -> sit.getSources().stream()
            .noneMatch(src -> src.getEmission(Substance.NOX) > 0
                || src.getEmission(Substance.NH3) > 0));
  }

  public static boolean hasTooFewSituations(final SituationComposition comp) {
    return comp.getSituations().isEmpty();
  }

  public static boolean hasNoSelectedDevelopmentPressureSources(final NCACalculationOptions options) {
    return options.getDevelopmentPressureSourceIds().isEmpty();
  }

  public static boolean isInvalidFormalAssessmentCalculation(final SituationComposition comp) {
    return comp.getSituation(SituationType.PROPOSED) == null;
  }

  /**
   * Validates whether a met site exists and is valid (UK theme only)
   */
  public static boolean hasInvalidMetSite(final ApplicationContext appContext,
      final CalculationSetOptions calculationOptions) {
    if (expectsNoMetInformation(appContext)) {
      return false;
    }
    final int metSiteId = calculationOptions.getNcaCalculationOptions().getAdmsOptions().getMetSiteId();
    final List<MetSite> sites = appContext.getConfiguration().getMetSites();
    return sites.stream()
        .noneMatch(v -> v.getId() == metSiteId);
  }

  /**
   * Validates whether a met year exists and is valid (UK theme only)
   *
   * If there is no met site available, returns false (i.e. tacitly "valid")
   */
  public static boolean hasInvalidMetYear(final ApplicationContext appContext,
      final CalculationSetOptions calculationOptions) {
    if (expectsNoMetInformation(appContext)) {
      return false;
    }
    final ADMSOptions admsOptions = calculationOptions.getNcaCalculationOptions().getAdmsOptions();
    final int metSiteId = admsOptions.getMetSiteId();
    final List<String> metYears = admsOptions.getMetYears();
    return metYears != null && appContext.getConfiguration().getMetSites()
        .stream().filter(v -> v.getId() == metSiteId).findFirst()
        .map(v -> v.getDatasets().stream()
            .noneMatch(dataset -> metYears.contains(dataset.getYear())))
        .orElse(false);
  }

  private static boolean expectsNoMetInformation(final ApplicationContext appContext) {
    final List<MetSite> sites = appContext.getConfiguration().getMetSites();
    return sites == null || sites.isEmpty();
  }

  /**
   * Validates whether the given options are compatible with the given requested calculation method
   */
  public static boolean hasIncompatibleCalculationMethod(final CalculationSetOptions opts, final CalculationMethod calculationMethod,
      final ExportContext.ClientJobType jobType) {
    return opts.getCalculationMethod() != calculationMethod && jobType == ExportContext.ClientJobType.REPORT;
  }

  /**
   * Validates whether the given options are compatible with the given requested report type
   */
  public static boolean hasIncompatibleJobType(final CalculationSetOptions opts, final CalculationJobType reportType,
      final ExportContext.ClientJobType jobType) {
    return opts.getCalculationJobType() != reportType && jobType == ExportContext.ClientJobType.REPORT;
  }

  public static boolean isIncompleteInCombinationJob(final CalculationSetOptions opts, final SituationComposition situationComposition) {
    return opts.getCalculationJobType() == CalculationJobType.IN_COMBINATION_PROCESS_CONTRIBUTION &&
        !opts.isUseInCombinationArchive() &&
        situationComposition.getSituationsByType(SituationType.COMBINATION_PROPOSED).isEmpty();
  }
}
