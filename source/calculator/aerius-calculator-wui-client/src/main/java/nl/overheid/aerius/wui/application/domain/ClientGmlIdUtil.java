/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain;

import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import nl.overheid.aerius.wui.application.domain.source.TimeVaryingProfile;

public final class ClientGmlIdUtil {

  private ClientGmlIdUtil() {
    // Utility class
  }

  /**
   * Finds an appropriate available GML ID based on the existing list, then assigns it to the item.
   *
   * @param collection The collection of existing objects with a GML ID.
   * @param item The item to set the GML ID for.
   * @param prefix The prefix to use. Should already include a trailing separation character to add a number to.
   * @return The GML ID that is finally used for this item.
   */
  public static <T extends ImaerFeature> String setGmlId(final List<T> collection, final T item, final String prefix) {
    return setGmlId(collection, item, prefix, t -> t.getGmlId(), (t, v) -> t.setGmlId(v));
  }

  /**
   * Finds an appropriate available GML ID based on the existing list, then assigns it to the item.
   *
   * @param collection The collection of existing objects with a GML ID.
   * @param item The item to set the GML ID for.
   * @param prefix The prefix to use. Should already include a trailing separation character to add a number to.
   * @return The GML ID that is finally used for this item.
   */
  public static String setGmlId(final List<TimeVaryingProfile> collection, final TimeVaryingProfile item, final String prefix) {
    return setGmlId(collection, item, prefix, TimeVaryingProfile::getGmlId, TimeVaryingProfile::setGmlId);
  }

  private static <F> String setGmlId(final List<F> collection, final F item, final String prefix,
      final Function<F, String> currentIdRetriever, final BiConsumer<F, String> newIdSetter) {
    final Set<String> existingIds = collection.stream()
        .map(currentIdRetriever)
        .collect(Collectors.toSet());
    int guessId = collection.size() + 1;
    String targetGmlId = prefix + guessId;
    while (existingIds.contains(targetGmlId)) {
      guessId++;
      targetGmlId = prefix + guessId;
    }
    newIdSetter.accept(item, targetGmlId);
    return targetGmlId;
  }
}
