/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.domain.source;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.vue.SkipReactivityValidations;

/**
 * Feature object for Generic Emission Sources with OPS characteristics.
 */
@JsType(namespace = GLOBAL, name = "ol.Feature", isNative = true)
public class GenericESFeature extends EmissionSourceFeature {
  /**
   * @return Returns a new {@link GenericESFeature} object.
   */
  public static @JsOverlay GenericESFeature create(final CharacteristicsType type) {
    final GenericESFeature feature = new GenericESFeature();
    init(feature, type);
    return feature;
  }

  @SkipReactivityValidations
  public static @JsOverlay void init(final GenericESFeature feature) {
    init(feature, feature.getCharacteristicsType());
  }

  /**
   * @return Returns a new {@link GenericESFeature} object.
   */
  public static @JsOverlay void init(final GenericESFeature feature, final CharacteristicsType type) {
    EmissionSourceFeature.initEmissionSource(feature, type, EmissionSourceType.GENERIC);
  }

  /**
   * @return Returns model characteristics as OPS characteristics.
   */
  @SkipReactivityValidations
  public final @JsOverlay OPSCharacteristics getOPSCharacteristics() {
    return Js.uncheckedCast(getCharacteristics());
  }
}
