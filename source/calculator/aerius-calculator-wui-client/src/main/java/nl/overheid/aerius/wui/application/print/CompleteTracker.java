/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.print;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Singleton;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.wui.application.util.UUID;

/**
 * Utility class to track several tasks that need to be completed before the page has rendered
 * This is used for the PDF generation, where the PDF needs to be printed after the page is rendered fully,
 * but can be used for other tasks as well.
 */
@Singleton
public class CompleteTracker {

  private boolean complete = false;
  private final @JsProperty Set<String> tasks = new HashSet<>();

  /**
   * Creates and registers a new task. This makes sure the complete handlers
   * are not called until this task#complete is called.
   * @return the task
   */
  public Task createTask() {
    final Task task = new Task();
    tasks.add(task.id);
    return task;
  }

  public boolean isComplete() {
    return complete;
  }

  public class Task {
    private final String id;

    public Task() {
      this.id = UUID.uuidv4();
    }

    /**
     * Checks if the task is already marked as completed
     * @return whether the task is completed
     */
    public boolean isComplete() {
      return !tasks.contains(id);
    }

    /**
     * Marks the task as completed, checks if all the tasks are completed, and
     * fires onComplete if this is the case.
     */
    public void complete() {
      tasks.remove(id);
      if (tasks.isEmpty()) {
        complete = true;
      }
    }
  }

}
