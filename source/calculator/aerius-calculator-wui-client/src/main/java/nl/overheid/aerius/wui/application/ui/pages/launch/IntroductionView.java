/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.launch;

import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.ProductProfile;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.manual.ExternalContentComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.daemon.misc.PersistablePreferencesContext;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    ButtonIcon.class,
    ExternalContentComponent.class
})
public class IntroductionView extends BasicVueComponent {
  @Prop EventBus eventBus;

  @Inject @Data PersistablePreferencesContext preferencesContext;
  @Inject @Data ApplicationContext applicationContext;

  @Computed
  public Theme getActiveTheme() {
    return applicationContext.getTheme();
  }

  @Computed
  public Optional<String> getIntroductionText() {
    final String introductionText = i18n.clPageInfoText(applicationContext.getTheme(), applicationContext.getProductProfile());
    if (introductionText == null || "".equals(introductionText)) {
      return Optional.empty();
    } else {
      return Optional.of(introductionText);
    }
  }

  @Computed
  public ProductProfile getProductProfile() {
    return applicationContext.getProductProfile();
  }

  @Computed
  public String getQuickStartUrl() {
    return applicationContext.getOptionalSetting(SharedConstantsEnum.QUICK_START_URL);
  }

  @Computed
  public String getManualUrl() {
    return applicationContext.getOptionalSetting(SharedConstantsEnum.MANUAL_URL);
  }
}
