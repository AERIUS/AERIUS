/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.calculation;

import static jsinterop.annotations.JsPackage.GLOBAL;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.calculation.RoadLocalFractionNO2Option;
import nl.overheid.aerius.wui.application.util.json.JsonBuilder;
import nl.overheid.aerius.wui.vue.ReactivityUtil;

/**
 * Calculation options related to the NCA theme.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class NCACalculationOptions {

  private @JsProperty String projectCategory;

  /**
   * Regional area for which a permit calculation would apply to.
   */
  private @JsProperty String permitArea;

  /**
   * Additional ADMS options.
   */
  private @JsProperty ADMSOptions admsOptions;

  private @JsProperty String roadLocalFractionNO2ReceptorsOption;

  private @JsProperty String roadLocalFractionNO2PointsOption;

  private @JsProperty double roadLocalFractionNO2;

  private @JsProperty String[] developmentPressureSourceIds;

  public static final @JsOverlay NCACalculationOptions create() {
    final NCACalculationOptions ncaCalculationOptions = new NCACalculationOptions();
    init(ncaCalculationOptions);
    return ncaCalculationOptions;
  }

  public static final @JsOverlay void init(final NCACalculationOptions opts) {
    ReactivityUtil.ensureDefault(opts::getProjectCategory, opts::setProjectCategory);
    ReactivityUtil.ensureDefault(opts::getPermitArea, opts::setPermitArea);
    ReactivityUtil.ensureDefaultSupplied(opts::getAdmsOptions, opts::setAdmsOptions, ADMSOptions::create);
    ReactivityUtil.ensureInitialized(opts::getAdmsOptions, ADMSOptions::init);
    ReactivityUtil.ensureDefault(opts::getRoadLocalFractionNO2ReceptorsOption, opts::setRoadLocalFractionNO2ReceptorsOption,
        RoadLocalFractionNO2Option.LOCATION_BASED);
    ReactivityUtil.ensureDefault(opts::getRoadLocalFractionNO2PointsOption, opts::setRoadLocalFractionNO2PointsOption,
        RoadLocalFractionNO2Option.LOCATION_BASED);
    ReactivityUtil.ensureDefault(opts::getRoadLocalFractionNO2, opts::setRoadLocalFractionNO2, 1D);
    ReactivityUtil.ensureJsPropertySupplied(opts, "developmentPressureSourceIds", opts::setDevelopmentPressureSourceIds,
        () -> new ArrayList<>());
  }

  public final @JsOverlay String getProjectCategory() {
    return projectCategory;
  }

  public final @JsOverlay void setProjectCategory(final String projectCategory) {
    this.projectCategory = projectCategory;
  }

  public final @JsOverlay String getPermitArea() {
    return permitArea;
  }

  public final @JsOverlay void setPermitArea(final String permitArea) {
    this.permitArea = permitArea;
  }

  public final @JsOverlay ADMSOptions getAdmsOptions() {
    return admsOptions;
  }

  public final @JsOverlay void setAdmsOptions(final ADMSOptions admsOptions) {
    this.admsOptions = admsOptions;
  }

  public final @JsOverlay RoadLocalFractionNO2Option getRoadLocalFractionNO2ReceptorsOption() {
    return RoadLocalFractionNO2Option.safeValueOf(roadLocalFractionNO2ReceptorsOption);
  }

  public final @JsOverlay void setRoadLocalFractionNO2ReceptorsOption(final RoadLocalFractionNO2Option roadLocalFractionNO2ReceptorsOption) {
    this.roadLocalFractionNO2ReceptorsOption = roadLocalFractionNO2ReceptorsOption == null ? null : roadLocalFractionNO2ReceptorsOption.name();
  }

  public final @JsOverlay RoadLocalFractionNO2Option getRoadLocalFractionNO2PointsOption() {
    return RoadLocalFractionNO2Option.safeValueOf(roadLocalFractionNO2PointsOption);
  }

  public final @JsOverlay void setRoadLocalFractionNO2PointsOption(final RoadLocalFractionNO2Option roadLocalFractionNO2PointsOption) {
    this.roadLocalFractionNO2PointsOption = roadLocalFractionNO2PointsOption == null ? null : roadLocalFractionNO2PointsOption.name();
  }

  public final @JsOverlay double getRoadLocalFractionNO2() {
    return roadLocalFractionNO2;
  }

  public final @JsOverlay void setRoadLocalFractionNO2(final double roadLocalFractionNO2) {
    this.roadLocalFractionNO2 = roadLocalFractionNO2;
  }

  public final @JsOverlay List<String> getDevelopmentPressureSourceIds() {
    return developmentPressureSourceIds == null ? new ArrayList<>() : Arrays.asList(developmentPressureSourceIds);
  }

  public final @JsOverlay void setDevelopmentPressureSourceIds(final List<String> developmentPressureSourceIds) {
    this.developmentPressureSourceIds = developmentPressureSourceIds == null
        ? new String[] {}
        : developmentPressureSourceIds.toArray(new String[developmentPressureSourceIds.size()]);
  }

  public final @JsOverlay Object toJSON() {
    final JsonBuilder builder = new JsonBuilder(this)
        .include("projectCategory", "permitArea", "roadLocalFractionNO2ReceptorsOption", "roadLocalFractionNO2PointsOption",
            "roadLocalFractionNO2", "developmentPressureSourceIds");
    if (admsOptions != null) {
      builder.set("admsOptions", admsOptions.toJSON());
    }

    return builder.build();
  }
}
