/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.main;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasDestroyed;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import elemental2.dom.ClientRect;
import elemental2.dom.DomGlobal;
import elemental2.dom.Element;
import elemental2.dom.HTMLElement;
import elemental2.dom.MouseEvent;

import jsinterop.annotations.JsMethod;

import nl.aerius.geo.command.MapResizeSequenceCommand;
import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.util.SchedulerUtil;
import nl.aerius.wui.vue.transition.HorizontalCollapse;
import nl.overheid.aerius.wui.application.components.apikey.LoginPanelModal;
import nl.overheid.aerius.wui.application.components.calculationpoint.CalculationPointHabitatDetailViewContainer;
import nl.overheid.aerius.wui.application.components.dummy.SimpleDummyComponent;
import nl.overheid.aerius.wui.application.components.help.HotKeyPanelComponent;
import nl.overheid.aerius.wui.application.components.info.InfoPanelModal;
import nl.overheid.aerius.wui.application.components.layer.LayerPanelModal;
import nl.overheid.aerius.wui.application.components.manual.HelpManualComponent;
import nl.overheid.aerius.wui.application.components.map.LabelsPanelModal;
import nl.overheid.aerius.wui.application.components.map.MapComponent;
import nl.overheid.aerius.wui.application.components.notification.NotificationPanelComponent;
import nl.overheid.aerius.wui.application.context.NavigationContext;
import nl.overheid.aerius.wui.application.event.LeftPanelToggleCommand;
import nl.overheid.aerius.wui.application.event.SelectionResetEvent;
import nl.overheid.aerius.wui.application.ui.main.MainView.FlexView;
import nl.overheid.aerius.wui.application.ui.nca.pages.emisionsourceList.timevarying.TimeVaryingProfileEditorView;
import nl.overheid.aerius.wui.application.ui.nca.pages.next.EmptyView;
import nl.overheid.aerius.wui.application.ui.nca.pages.next.NextStepsView;
import nl.overheid.aerius.wui.application.ui.nca.pages.timevarying.TimeVaryingProfileView;
import nl.overheid.aerius.wui.application.ui.pages.building.BuildingView;
import nl.overheid.aerius.wui.application.ui.pages.calculate.CalculationJobView;
import nl.overheid.aerius.wui.application.ui.pages.calculate.CalculationSituationSummary;
import nl.overheid.aerius.wui.application.ui.pages.calculate.detail.CalculationJobDetailView;
import nl.overheid.aerius.wui.application.ui.pages.calculationpoints.CalculationPointsView;
import nl.overheid.aerius.wui.application.ui.pages.emissionsource.EmissionSourceDetailView;
import nl.overheid.aerius.wui.application.ui.pages.emissionsource.EmissionSourceView;
import nl.overheid.aerius.wui.application.ui.pages.emissionsourceList.ScenarioInputListDetailView;
import nl.overheid.aerius.wui.application.ui.pages.emissionsourceList.ScenarioInputListView;
import nl.overheid.aerius.wui.application.ui.pages.export.ExportSituationSummary;
import nl.overheid.aerius.wui.application.ui.pages.export.ExportView;
import nl.overheid.aerius.wui.application.ui.pages.externalcontent.ExternalContentView;
import nl.overheid.aerius.wui.application.ui.pages.language.LanguageView;
import nl.overheid.aerius.wui.application.ui.pages.launch.HomeView;
import nl.overheid.aerius.wui.application.ui.pages.launch.IntroductionView;
import nl.overheid.aerius.wui.application.ui.pages.preferences.PreferencesView;
import nl.overheid.aerius.wui.application.ui.pages.results.ResultsView;
import nl.overheid.aerius.wui.application.util.HorizontalResizeUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    ScenarioInputListView.class,
    ScenarioInputListDetailView.class,

    EmissionSourceView.class,
    EmissionSourceDetailView.class,

    SimpleDummyComponent.class,
    CalculationPointsView.class,
    ExportView.class,
    HomeView.class,
    IntroductionView.class,
    PreferencesView.class,
    ExternalContentView.class,
    MapComponent.class,
    ResultsView.class,
    BuildingView.class,
    LanguageView.class,
    TimeVaryingProfileView.class,
    TimeVaryingProfileEditorView.class,
    NextStepsView.class,
    HelpManualComponent.class,
    EmptyView.class,

    CalculationSituationSummary.class,
    ExportSituationSummary.class,
    CalculationJobView.class,
    CalculationJobDetailView.class,
    CalculationPointHabitatDetailViewContainer.class,

    NotificationPanelComponent.class,
    LayerPanelModal.class,
    LabelsPanelModal.class,
    InfoPanelModal.class,
    LoginPanelModal.class,
    SearchPanelModal.class,
    HotKeyPanelComponent.class,

    HorizontalCollapse.class,
})
public class ContentView extends BasicVueComponent implements HasCreated, HasDestroyed {
  private static final ContentViewEventBinder EVENT_BINDER = GWT.create(ContentViewEventBinder.class);

  interface ContentViewEventBinder extends EventBinder<ContentView> {}

  private static final int ANIMATION_DURATION = 300;
  private static final int SLIDER_WIDTH = 6;
  private static final double MAX_RATIO = 100D;
  private static final double MID_RATIO = 50D;
  private static final double MIN_RATIO = 0D;
  private static final List<Double> PRESET_RATIOS = Arrays.asList(30D, MID_RATIO, 70D);
  private static final String COLUMN_LEFT_KEY = "--column-left-width-user-setting";
  private static final String COLUMN_SPLIT_KEY_LEFT = "--split-ratio-user-preference-left";
  private static final String COLUMN_SPLIT_KEY_RIGHT = "--split-ratio-user-preference-right";

  private static final String COLUMN_LEFT_WIDTH_KEY = "--column-left-actual-width";
  private static final String COLUMN_MANUAL_KEY = "--column-manual-width-user-setting";

  @Prop String left;
  @Prop String middle;
  @Prop String right;
  @Prop String manual;

  @Prop boolean softMiddle;
  @Data boolean hardMiddleLingering;

  @Prop boolean hasLeft;
  @Prop boolean hasMiddle;
  @Prop boolean hasRight;
  @Prop boolean hasManual;

  @Prop(required = true) FlexView flexView;

  @Prop EventBus eventBus;

  @Ref HTMLElement contexts;
  @Ref HTMLElement leftPanel;
  @Ref HTMLElement sliderLeft;
  @Ref HTMLElement manualPanel;
  @Ref HTMLElement sliderManual;

  @Ref IsVueComponent leftComponent;
  @Ref IsVueComponent rightComponent;

  @Data @Inject NavigationContext navigationContext;

  private Double dragStartX;
  private int dragStartWidth;
  private HandlerRegistration handlers;

  @Watch("softMiddle")
  public void onSoftMiddleChange() {
    if (!softMiddle) {
      return;
    }

    hardMiddleLingering = true;
    // Delay this so certain styles can be applied so long as subcomponents are closing
    SchedulerUtil.delay(() -> {
      hardMiddleLingering = false;
      resize();
    }, ANIMATION_DURATION);
  }

  @Watch("left")
  public void onLeftChange(final String neww) {
    vue().$nextTick(() -> {
      if (leftComponent instanceof MapComponent) {
        ((MapComponent) leftComponent).attach();
      }
    });
  }

  @Watch("right")
  public void onRightChange(final String neww) {
    vue().$nextTick(() -> {
      if (rightComponent instanceof MapComponent) {
        ((MapComponent) rightComponent).attach();
      }
    });
  }

  @Computed("isRightView")
  public boolean isRightView() {
    return flexView == FlexView.RIGHT;
  }

  @Computed("isSplitView")
  public boolean isSplitView() {
    return flexView == FlexView.SPLIT;
  }

  @Watch("hasManual")
  public void onHasManualChange() {
    resize();
  }

  @Watch("flexView")
  public void onFlexViewChange() {
    resize();
  }

  private void resize() {
    eventBus.fireEvent(new MapResizeSequenceCommand());
    eventBus.fireEvent(new ContentResizeEvent());
    if (softMiddle) {
      removePropertyValue(DomGlobal.document.documentElement, COLUMN_LEFT_WIDTH_KEY);
    } else if (leftPanel != null) {
      setPropertyValue(DomGlobal.document.documentElement, COLUMN_LEFT_WIDTH_KEY, getColumnLeftWidth() + "px");
    }
  }

  @EventHandler
  public void on(final LeftPanelToggleCommand e) {
    switch (flexView) {
    case RIGHT:
      final int columnLeftWidthPx = getColumnLeftWidth();
      if (columnLeftWidthPx < 10) {
        removePropertyValue(DomGlobal.document.documentElement, COLUMN_LEFT_KEY);
      } else {
        adjustLeftSize(0);
      }
      break;
    case SPLIT:
      final ClientRect sliderRect = sliderLeft.getBoundingClientRect();
      final ClientRect contextsRect = contexts.getBoundingClientRect();
      final double width = contextsRect.width;
      final double sliderLeftPos = sliderRect.left - contextsRect.left;
      final double currentRatio = sliderLeftPos / width * 100;
      // Tldr; find whichever ratio in the list is closest to the current ratio, and get whatever
      // is the next ratio in the list, then set it
      final int nextIdx = PRESET_RATIOS.indexOf(PRESET_RATIOS.stream()
          .sorted((o1, o2) -> (int) ((Math.abs(o1 - currentRatio) - Math.abs(o2 - currentRatio)) * 100))
          .findFirst().orElse(MID_RATIO)) + 1;
      setSplitRatio(PRESET_RATIOS.get(nextIdx % PRESET_RATIOS.size()));
      break;
    default:
      GWTProd.warn("No resize strategy for current view mode.");
      break;
    }

    resize();
  }

  @EventHandler
  public void onSelectionReset(final SelectionResetEvent e) {
    SchedulerUtil.delay(this::resize, ANIMATION_DURATION);
  }

  @Override
  public void created() {
    handlers = EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Override
  public void destroyed() {
    handlers.removeHandler();
  }

  private int getColumnLeftWidth() {
    return leftPanel.clientWidth;
  }

  private int getColumnManualWidth() {
    return manualPanel.clientWidth;
  }

  @JsMethod
  public void startResizeDragLeft(final MouseEvent ev) {
    final int columnLeftWidthPx = getColumnLeftWidth();
    final ClientRect sliderRect = sliderLeft.getBoundingClientRect();
    final ClientRect contextsRect = contexts.getBoundingClientRect();
    final double sliderLeftPos = sliderRect.left - contextsRect.left;
    HorizontalResizeUtil.startResizeDrag(sliderLeft, ev, v -> applyResizeDelta(columnLeftWidthPx, contextsRect.width, sliderLeftPos, v));
  }

  @JsMethod
  public void startResizeDragManual(final MouseEvent ev) {
    final int columnManualWidthPx = getColumnManualWidth();
    HorizontalResizeUtil.startResizeDrag(sliderManual, ev, delta -> adjustManualSize(columnManualWidthPx, delta));
  }

  private void applyResizeDelta(final int initialLeftWidth, final double contextWidth, final double initialSliderLeft, final int delta) {
    switch (flexView) {
    case RIGHT:
      adjustLeftSize(initialLeftWidth + delta);
      break;
    case SPLIT:
      final double ratio = (initialSliderLeft + delta) / contextWidth * 100;
      setSplitRatio(ratio);
      break;
    default:
      GWTProd.warn("No resize strategy for current view mode.");
      break;
    }

    resize();
  }

  private void setSplitRatio(final double ratio) {
    setPropertyValue(DomGlobal.document.documentElement, COLUMN_SPLIT_KEY_LEFT,
        "minmax(var(--slider-width, 6px), " + clamp(MIN_RATIO, ratio, MAX_RATIO) + "fr)");
    setPropertyValue(DomGlobal.document.documentElement, COLUMN_SPLIT_KEY_RIGHT,
        "minmax(var(--slider-width, 6px), " + clamp(MIN_RATIO, 100 - ratio, MAX_RATIO) + "fr)");
  }

  private double clamp(final double min, final double value, final double max) {
    return Math.max(min, Math.min(value, max));
  }

  @JsMethod
  public void applyLeftDelta(final int delta) {
    adjustLeftSize(getColumnLeftWidth() + delta);
  }

  public void adjustLeftSize(final int val) {
    setPropertyValue(DomGlobal.document.documentElement, COLUMN_LEFT_KEY, Math.max(SLIDER_WIDTH, val) + "px");
  }

  private void adjustManualSize(final int width, final int val) {
    setPropertyValue(DomGlobal.document.documentElement, COLUMN_MANUAL_KEY, Math.max(SLIDER_WIDTH, width - val) + "px");
    resize();
  }

  private static native void removePropertyValue(final Element el, final String prop) /*-{
    el.style.removeProperty(prop);
  }-*/;

  private static native void setPropertyValue(final Element el, final String prop, final String val) /*-{
    el.style.setProperty(prop, val);
  }-*/;

  public static native String getPropertyValue(Element el, String prop) /*-{
    return getComputedStyle(el).getPropertyValue(prop);
  }-*/;
}
