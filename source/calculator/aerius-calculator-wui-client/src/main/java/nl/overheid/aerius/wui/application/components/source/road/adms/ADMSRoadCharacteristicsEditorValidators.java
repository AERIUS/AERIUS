/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.road.adms;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.aerius.vuelidate.ValidationOptions;
import nl.aerius.vuelidate.Validations;
import nl.aerius.vuelidate.ValidatorBuilder;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.ADMSLimits;

/**
 * Validators for {@link ADMSRoadCharacteristicsEditorComponent}.
 */
public class ADMSRoadCharacteristicsEditorValidators extends ValidationOptions<ADMSRoadCharacteristicsEditorComponent> {
  @JsType(isNative = true, namespace = JsPackage.GLOBAL)
  static class ADMSRoadCharacteristicsEditorValidations extends Validations {
    public @JsProperty Validations widthV;
    public @JsProperty Validations elevationV;
    public @JsProperty Validations gradientV;
    public @JsProperty Validations coverageV;
  }

  @Override
  protected void constructValidators(final Object options, final ValidatorInstaller v) {
    final ADMSRoadCharacteristicsEditorComponent instance = Js.uncheckedCast(options);

    v.install("widthV",
        () -> instance.widthV = null,
        ValidatorBuilder.create()
            .required()
            .decimal()
            .minValue(ADMSLimits.ROAD_WIDTH_MIN)
            .maxValue(ADMSLimits.ROAD_WIDTH_MAX));
    v.install("elevationV",
        () -> instance.elevationV = null,
        ValidatorBuilder.create()
            .required()
            .decimal()
            .minValue(ADMSLimits.ROAD_ELEVATION_MIN)
            .maxValue(ADMSLimits.ROAD_ELEVATION_MAX));
    v.install("gradientV",
        () -> instance.gradientV = null,
        ValidatorBuilder.create()
            .required()
            .decimal()
            .minValue(ADMSLimits.ROAD_GRADIENT_MIN)
            .maxValue(ADMSLimits.ROAD_GRADIENT_MAX));
    v.install("coverageV",
        () -> instance.coverageV = null,
        ValidatorBuilder.create()
            .required()
            .decimal()
            .minValue(ADMSLimits.ROAD_COVERAGE_MIN)
            .maxValue(ADMSLimits.ROAD_COVERAGE_MAX));
  }
}
