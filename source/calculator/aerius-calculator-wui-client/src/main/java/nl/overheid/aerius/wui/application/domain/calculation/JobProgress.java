/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.calculation;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.calculation.JobState;

@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class JobProgress {

  private String state;
  private int queuePosition;

  private boolean protect;
  private int numberOfPoints;
  private int numberOfPointsCalculated;
  private int estimatedRemainingCalculationTime;
  private String resultUrl;
  private String errorMessage;

  public final @JsOverlay JobState getState() {
    return state == null ? JobState.UNDEFINED : JobState.safeValueOf(state);
  }

  public final @JsOverlay int getQueuePosition() {
    return queuePosition;
  }

  public final @JsOverlay void setState(final JobState state) {
    this.state = state == null ? null : state.name();
  }

  public final @JsOverlay int getNumberOfPoints() {
    return numberOfPoints;
  }

  public final @JsOverlay int getNumberOfPointsCalculated() {
    return numberOfPointsCalculated;
  }

  public final @JsOverlay int getEstimatedRemainingCalculationTime() {
    return estimatedRemainingCalculationTime;
  }

  public final @JsOverlay String getResultUrl() {
    return resultUrl;
  }

  public final @JsOverlay String getErrorMessage() {
    return errorMessage;
  }

  public final @JsOverlay void setErrorMessage(final String errorMessage) {
    this.errorMessage = errorMessage;
  }

  public final @JsOverlay boolean isProtect() {
    return protect;
  }

  public final @JsOverlay void setProtect(final boolean protect) {
    this.protect = protect;
  }

}
