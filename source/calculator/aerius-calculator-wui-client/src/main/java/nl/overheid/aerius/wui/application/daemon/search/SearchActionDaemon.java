/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.search;

import java.util.Optional;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.Coordinate;
import ol.format.Wkt;
import ol.geom.Geometry;
import ol.geom.Point;

import nl.aerius.geo.command.MapCenterChangeCommand;
import nl.aerius.search.wui.context.SearchContext;
import nl.aerius.search.wui.domain.SearchSuggestion;
import nl.aerius.search.wui.event.SearchSuggestionSelectionEvent;
import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.wui.application.daemon.geo.HighlightOutlineCommand;
import nl.overheid.aerius.wui.application.daemon.geo.SonarCommand;
import nl.overheid.aerius.wui.application.geo.util.GeoType;
import nl.overheid.aerius.wui.application.geo.util.GeoUtil;

public class SearchActionDaemon extends BasicEventComponent {
  private static final int ZOOM_LEVEL = 10;

  private static final SearchActionDaemonEventBinder EVENT_BINDER = GWT.create(SearchActionDaemonEventBinder.class);

  interface SearchActionDaemonEventBinder extends EventBinder<SearchActionDaemon> {}

  private static final Wkt WKT = new Wkt();

  @Inject private SearchContext context;

  @EventHandler
  public void onSearchSuggestionSelectionEvent(final SearchSuggestionSelectionEvent e) {
    if (e.getValue().centroid != null) {
      focus(e.getValue());
    }

    context.setSearchShowing(false);
  }

  private void focus(final SearchSuggestion searchSuggestion) {
    final Geometry centroid = Optional.ofNullable(searchSuggestion.centroid)
        .map(v -> WKT.readGeometry(v))
        .orElse(null);
    final Geometry bbox = Optional.ofNullable(searchSuggestion.bbox)
        .map(v -> WKT.readGeometry(v))
        .orElse(null);
    final Geometry geometry = Optional.ofNullable(searchSuggestion.geometry)
        .map(v -> WKT.readGeometry(v))
        .orElse(null);

    pingSonar(centroid);
    highlightGeometry(geometry);
    moveMap(Optional.ofNullable(geometry)
        .orElse(Optional.ofNullable(bbox)
            .orElse(Optional.ofNullable(centroid)
                .orElse(null))));
  }

  private void highlightGeometry(final Geometry geometry) {
    eventBus.fireEvent(new HighlightOutlineCommand(geometry));
  }

  private void pingSonar(final Geometry centroid) {
    if (GeoType.POINT.is(centroid)) {
      eventBus.fireEvent(new SonarCommand((Point) centroid));
    } else {
      // Warn because this is not something we should be expecting to happen
      GWTProd.warn("Got centroid that is not a point: ", centroid);
    }
  }

  private void moveMap(final Geometry geometry) {
    if (geometry == null) {
      // This may be normal behaviour, but warn anyway because generally we expect a
      // search suggestion to have a geometry (whether a centroid or outline)
      GWTProd.warn("No geometry to move to.");
      return;
    }

    // If the geometry is a point, move to the point with a set zoom level
    if (GeoType.POINT.is(geometry)) {
      final Point point = (Point) geometry;
      final Coordinate coord = point.getFirstCoordinate();
      eventBus.fireEvent(new MapCenterChangeCommand(coord.getX(), coord.getY(), ZOOM_LEVEL));
    } else { // Else move to the extent of the geometry
      eventBus.fireEvent(GeoUtil.createMapSetExtendCommand(geometry));
    }

  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
