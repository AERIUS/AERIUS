/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source.road;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import ol.OLFactory;
import ol.geom.LineString;
import ol.style.Stroke;
import ol.style.Style;

import nl.aerius.geo.domain.legend.ColorLabelsLegend;
import nl.aerius.geo.domain.legend.Legend;
import nl.aerius.geo.domain.legend.LegendType;
import nl.aerius.geo.wui.util.OL3GeometryUtil;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.legend.ColorItem;
import nl.overheid.aerius.shared.domain.legend.ColorItemType;
import nl.overheid.aerius.shared.domain.v2.source.road.ADMSRoadSideBarrierType;
import nl.overheid.aerius.shared.domain.v2.source.road.SRM2RoadSideBarrierType;
import nl.overheid.aerius.wui.application.domain.source.RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.road.ADMSRoadSideBarrier;
import nl.overheid.aerius.wui.application.domain.source.road.SRM2RoadSideBarrier;
import nl.overheid.aerius.wui.application.i18n.M;

public class RoadBarrierTypeStyle extends RoadBarrierStyle implements RoadStyle {

  private final List<ColorItem> colorItems;

  public RoadBarrierTypeStyle(final Function<ColorItemType, List<ColorItem>> colorRangeFunction) {
    colorItems = colorRangeFunction.apply(ColorItemType.ROAD_BARRIER_TYPE);
  }

  private Stroke getStroke(final ColorItem cr) {
    return OLFactory.createStroke(OLFactory.createColor(cr.getColor()), ROAD_BARRIER_WIDTH);
  }

  @Override
  public List<Style> getStyle(final RoadESFeature source, final String vehicleTypeCode, final double resolution) {
    final List<Style> styleList = new ArrayList<>();
    styleList.add(ROAD_STYLE);
    addStyle(styleList, source, BarrierSide.LEFT, resolution);
    addStyle(styleList, source, BarrierSide.RIGHT, resolution);
    return styleList;
  }

  private void addStyle(final List<Style> styleList, final RoadESFeature source, final BarrierSide barrierSide, final Double resolution) {
    final LineString geometry = (LineString) source.getGeometry();
    if (showBarrier(source, barrierSide)) {
      styleList.add(getBarrierStyle(geometry, getBarrierTypeAsString(source, barrierSide), getBarrierDistanceOnMap(barrierSide, resolution)));
    }
  }

  private Style getBarrierStyle(final LineString geometry, final String barrierTypeString, final double distance) {
    final ColorItem color = colorItems.stream()
        .filter(colorItem -> colorItem.getItemValue().equalsIgnoreCase(barrierTypeString))
        .findAny()
        .orElse(colorItems.get(colorItems.size() - 1));
    final Style roadWallStyle = OLFactory.createStyle(getStroke(color));
    roadWallStyle.setGeometry(OL3GeometryUtil.offsetFromLineString(geometry, distance));
    return roadWallStyle;
  }

  @Override
  public Legend getLegend(final ApplicationConfiguration appThemeConfiguration) {
    final String[] labels = colorItems.stream()
        .map(ColorItem::getItemValue)
        .map(itemValue -> getLabel(appThemeConfiguration, itemValue))
        .toArray(String[]::new);

    final String[] colors = colorItems.stream()
        .map(ColorItem::getColor)
        .toArray(String[]::new);

    return withRoadNetworkLabel(new ColorLabelsLegend(labels, colors, LegendType.LINE));
  }

  private static String getLabel(final ApplicationConfiguration appThemeConfiguration, final String value) {
    if (appThemeConfiguration.getTheme() == Theme.NCA) {
      return M.messages().sourceAdmsRoadsideBarrierType(ADMSRoadSideBarrierType.valueOf(value));
    } else {
      return M.messages().sourceRoadsideBarrierType(SRM2RoadSideBarrierType.valueOf(value));
    }
  }

  private static String getBarrierTypeAsString(final RoadESFeature source, final BarrierSide barrierSide) {
    return getBarrierProperty(source, barrierSide,
        null, srmBarrier -> Optional.of(srmBarrier)
            .map(SRM2RoadSideBarrier::getBarrierType)
            .map(SRM2RoadSideBarrierType::name)
            .orElse(null),
        admsBarrier -> Optional.of(admsBarrier)
            .map(ADMSRoadSideBarrier::getBarrierType)
            .map(ADMSRoadSideBarrierType::name)
            .orElse(null));
  }
}
