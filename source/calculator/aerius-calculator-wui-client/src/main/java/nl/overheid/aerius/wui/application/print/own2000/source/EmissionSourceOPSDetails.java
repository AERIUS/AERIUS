/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.print.own2000.source;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;

import jsinterop.annotations.JsMethod;

import nl.aerius.geo.wui.util.OL3GeometryUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.sector.SectorIcon;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.shared.util.ColorUtil;
import nl.overheid.aerius.wui.application.components.common.StyledPointComponent;
import nl.overheid.aerius.wui.application.components.source.characteristics.ops.OPSCharacteristicsBasicComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.MooringType;
import nl.overheid.aerius.wui.application.components.source.shipping.inland.inland.InlandMovementComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.source.ColdStartESFeature;
import nl.overheid.aerius.wui.application.domain.source.InlandMaritimeShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.InlandShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.OPSCharacteristics;
import nl.overheid.aerius.wui.application.geo.util.GeoType;
import nl.overheid.aerius.wui.application.geo.util.GeoUtil;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.application.print.own2000.source.coldstart.ColdStartDetailPrintComponent;
import nl.overheid.aerius.wui.application.print.own2000.source.coldstart.ColdStartDetailPrintComponentFactory;
import nl.overheid.aerius.wui.application.print.own2000.source.farmanimalhousing.FarmAnimalHousingPrintComponent;
import nl.overheid.aerius.wui.application.print.own2000.source.farmanimalhousing.FarmAnimalHousingPrintComponentFactory;
import nl.overheid.aerius.wui.application.print.own2000.source.farmland.FarmlandDetailPrintComponent;
import nl.overheid.aerius.wui.application.print.own2000.source.farmland.FarmlandDetailPrintComponentFactory;
import nl.overheid.aerius.wui.application.print.own2000.source.farmlodging.FarmLodgingSource;
import nl.overheid.aerius.wui.application.print.own2000.source.farmlodging.FarmLodgingSourceFactory;
import nl.overheid.aerius.wui.application.print.own2000.source.offroad.OffroadDetailPrintComponent;
import nl.overheid.aerius.wui.application.print.own2000.source.offroad.OffroadDetailPrintComponentFactory;
import nl.overheid.aerius.wui.application.print.own2000.source.road.SRM2RoadDetailPrintComponent;
import nl.overheid.aerius.wui.application.print.own2000.source.road.SRM2RoadDetailPrintComponentFactory;
import nl.overheid.aerius.wui.application.print.own2000.source.shipping.InlandShippingDetailPrintComponent;
import nl.overheid.aerius.wui.application.print.own2000.source.shipping.InlandShippingDetailPrintComponentFactory;
import nl.overheid.aerius.wui.application.print.own2000.source.shipping.MaritimeShippingDetailPrintComponent;
import nl.overheid.aerius.wui.application.print.own2000.source.shipping.MaritimeShippingDetailPrintComponentFactory;
import nl.overheid.aerius.wui.application.print.own2000.source.shipping.MaritimeShippingDockedDetailPrintComponent;
import nl.overheid.aerius.wui.application.print.own2000.source.shipping.MaritimeShippingDockedDetailPrintComponentFactory;
import nl.overheid.aerius.wui.application.util.SectorColorUtil;

@Component(name = "emission-source-ops-details", components = {
    FarmLodgingSource.class,
    FarmAnimalHousingPrintComponent.class,
    FarmlandDetailPrintComponent.class,
    OffroadDetailPrintComponent.class,
    InlandShippingDetailPrintComponent.class,
    MaritimeShippingDetailPrintComponent.class,
    MaritimeShippingDockedDetailPrintComponent.class,
    SRM2RoadDetailPrintComponent.class,
    ColdStartDetailPrintComponent.class,
    StyledPointComponent.class,
    MinMaxLabelPrintComponent.class
})
public class EmissionSourceOPSDetails extends OPSCharacteristicsBasicComponent {
  private static final String DEFAULT_CLASS = "default";
  private static final OPSCharacteristics DEFAULT_CHARACTERISTICS = OPSCharacteristics.create();

  protected @Prop SituationContext situation;

  @Inject @Data ApplicationContext appContext;

  @Override
  @Watch(value = "source", isImmediate = true)
  public void watchSource() {
    super.watchSource();
  }

  @Computed
  public String getLocation() {
    return Optional.ofNullable(OL3GeometryUtil.getMiddlePointOfGeometry(source.getGeometry()))
        .map(v -> MessageFormatter.formatPoint(v))
        .orElse(null);
  }

  @Computed
  public String getLocationStatisticLabel() {
    return GeoUtil.getGeometryStatisticLabel(source.getGeometry());
  }

  @Computed
  public String getLocationStatisticValue() {
    return GeoUtil.getGeometryStatisticValue(source.getGeometry());
  }

  @Computed
  public boolean getHasLocationStatistic() {
    return !GeoType.POINT.is(source.getGeometry());
  }

  @Computed
  public String getDetailComponentName() {
    switch (source.getEmissionSourceType()) {
    case FARM_LODGE:
      return FarmLodgingSourceFactory.get().getComponentTagName();
    case FARM_ANIMAL_HOUSING:
      return FarmAnimalHousingPrintComponentFactory.get().getComponentTagName();
    case FARMLAND:
      return FarmlandDetailPrintComponentFactory.get().getComponentTagName();
    case OFFROAD_MOBILE:
      return OffroadDetailPrintComponentFactory.get().getComponentTagName();
    case SRM2_ROAD:
      return SRM2RoadDetailPrintComponentFactory.get().getComponentTagName();
    case COLD_START:
      return ColdStartDetailPrintComponentFactory.get().getComponentTagName();
    case SHIPPING_INLAND:
    case SHIPPING_INLAND_DOCKED:
      return InlandShippingDetailPrintComponentFactory.get().getComponentTagName();
    case SHIPPING_MARITIME_INLAND:
    case SHIPPING_MARITIME_MARITIME:
      return MaritimeShippingDetailPrintComponentFactory.get().getComponentTagName();
    case SHIPPING_MARITIME_DOCKED:
      return MaritimeShippingDockedDetailPrintComponentFactory.get().getComponentTagName();
    default:
      return null;
    }
  }

  @Computed
  public Map<String, String> getAdditionalProperties() {
    final Map<String, String> additionalProperties = new HashMap<>();
    addInlandShippingDetails(additionalProperties);
    addMaritimeShippingDetails(additionalProperties);
    return additionalProperties;
  }

  @Computed
  public String getDiurnalVariation() {
    return i18n.sourceDiurnalVariation(characteristics.getDiurnalVariation());
  }

  @Computed
  public String getDiurnalVariationClass() {
    return DEFAULT_CHARACTERISTICS.getDiurnalVariation() == characteristics.getDiurnalVariation() ? DEFAULT_CLASS : "";
  }

  @Computed
  public String getBuilding() {
    return characteristics.getBuildingGmlId() != null ? situation.findBuilding(characteristics.getBuildingGmlId()).getLabel() : null;
  }

  @Computed
  public String getEmissionHeight() {
    return MessageFormatter.formatEmissionHeight(characteristics.getEmissionHeight());
  }

  @Computed("isNotSRM2RoadSource")
  public Boolean isNotSRM2RoadSource() {
    return EmissionSourceType.SRM2_ROAD != source.getEmissionSourceType();
  }

  @JsMethod
  public String getEmission(final Substance substance) {
    return MessageFormatter.formatEmissionWithUnitSmart(source.getEmission(substance));
  }

  @JsMethod
  public boolean hasEmission(final Substance substance) {
    return source.getEmission(substance) > 0.0;
  }

  @JsMethod
  public List<Substance> getSubstances() {
    return applicationContext.getConfiguration().getImportSubstances();
  }

  @JsMethod
  public boolean isBuildingInfluence() {
    return hasCharacteristics() && characteristics.isBuildingInfluence();
  }

  @Computed
  public SectorIcon getSectorIcon() {
    return SectorColorUtil.getSectorIcon(appContext.getConfiguration(), source.getSectorId());
  }

  @Computed
  public String getSectorColor() {
    return ColorUtil.webColor(getSectorIcon().getColor());
  }

  @Computed
  public String getSectorGroup() {
    return Optional.ofNullable(findSectorGroupFromSectorId(source.getSectorId()))
        .map(v -> i18n.sectorGroup(v))
        .orElse("-");
  }

  @JsMethod
  public SectorGroup findSectorGroupFromSectorId(final int sectorId) {
    return applicationContext.getConfiguration().getSectorCategories().findSectorGroupFromSectorId(sectorId);
  }

  @JsMethod
  public String getSectorDescription() {
    return Optional.ofNullable(findSectorFromSectorId(source.getSectorId()))
        .map(v -> v.getDescription())
        .orElse("-");
  }

  @JsMethod
  public Sector findSectorFromSectorId(final int sectorId) {
    return applicationContext.getConfiguration().getSectorCategories().determineSectorById(sectorId);
  }

  @Computed("hasCharacteristics")
  public boolean hasCharacteristics() {
    return characteristics != null && !isColdStartVehicleBasedCharacteristics();
  }

  @JsMethod
  public boolean isColdStartVehicleBasedCharacteristics() {
    return source.getEmissionSourceType() == EmissionSourceType.COLD_START
        && ((ColdStartESFeature) source).isVehicleBasedCharacteristics();
  }

  private void addMaritimeShippingDetails(final Map<String, String> additionalProperties) {
    if (source.getEmissionSourceType() == EmissionSourceType.SHIPPING_MARITIME_INLAND) {
      final InlandMaritimeShippingESFeature maritimeShippingESFeature = (InlandMaritimeShippingESFeature) this.source;
      final String mooringAId = maritimeShippingESFeature.getMooringAId();
      if (mooringAId != null && !mooringAId.isEmpty()) {
        situation.getSources().stream()
            .filter(es -> es.getGmlId().equals(mooringAId))
            .findAny()
            .ifPresent(es -> additionalProperties.put(i18n.mooringType(MooringType.A), es.getLabel()));
      }

      final String mooringBId = maritimeShippingESFeature.getMooringBId();
      if (mooringBId != null && !mooringBId.isEmpty()) {
        situation.getSources().stream()
            .filter(es -> es.getGmlId().equals(mooringBId))
            .findAny()
            .ifPresent(es -> additionalProperties.put(i18n.mooringType(MooringType.B), es.getLabel()));
      }
    }
  }

  private void addInlandShippingDetails(final Map<String, String> additionalProperties) {
    if (source.getEmissionSourceType() == EmissionSourceType.SHIPPING_INLAND) {
      final InlandShippingESFeature shippingESFeature = (InlandShippingESFeature) this.source;
      additionalProperties.put(i18n.esWaterwayCategory(), shippingESFeature.getWaterway().getWaterwayCode());
      additionalProperties.put(i18n.esMovement(InlandMovementComponent.InlandMovementType.A_TO_B),
          i18n.esWaterwayDirectionType(shippingESFeature.getWaterway().getDirection()));
    }
  }
}
