/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.input.suggest;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;

import elemental2.dom.HTMLElement;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.wui.application.util.AccessibilityUtil;
import nl.overheid.aerius.wui.application.util.AccessibilityUtil.Register;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component(name = "suggest-listbox", components = {
    SuggestDropdownModal.class
})
public class SuggestListBoxComponent extends BasicVueView implements IsVueComponent, HasMounted {
  @Prop boolean enabled;
  @Prop String placeholder;
  @Prop @JsProperty List<SuggestListBoxData> options;

  @Prop String selectedValue;
  @Prop SuggestListBoxData selected;

  @Prop String labelledBy;
  @Prop String debugId;

  @Data SuggestListBoxData selectedData;
  @Data String valueData = "";

  @Prop int modalOffset;
  @Prop int modalWidth;
  @Data boolean showing = false;

  @Ref HTMLElement elem;
  @Data HTMLElement ref;

  private Register modalFocusTrap;

  @PropDefault("enabled")
  public boolean enabledDefault() {
    return true;
  }

  @Watch(value = "showing")
  public void onShowingChange() {
    if (showing) {
      modalFocusTrap = AccessibilityUtil.trapFocus(elem);
    } else {
      if (modalFocusTrap != null) {
        modalFocusTrap.removeHandle();
      }
    }
  }

  @Watch(value = "selectedValue", isImmediate = true)
  public void onSelectedValueChange(final String neww, final String old) {
    if (neww == null || "".equals(neww)) {
      selectedData = null;
      return;
    }

    final Optional<SuggestListBoxData> match = options.stream().filter(v -> v.getCode().equals(neww))
        .findFirst();

    if (match.isPresent()) {
      final SuggestListBoxData result = match.get();
      selectedData = result;
    } else {
      selectedData = null;
    }
  }

  @Watch(value = "selected")
  public void onSelectedChange(final SuggestListBoxData neww, final SuggestListBoxData old) {
    selectedData = neww;
  }

  @Watch(value = "selectedData", isImmediate = true)
  public void onSelectedDataChange(final SuggestListBoxData neww, final SuggestListBoxData old) {
    if (neww == null) {
      valueData = "";
    } else {
      valueData = selectedData.getName();
    }
  }

  /**
   * Cleverly match filter options:
   *
   * <pre>
   * Secondarily match for each part of the string (split on nbsp)
   * Then primarily match on nbsp-less trim
   * </pre>
   *
   * Result will be a prioritized list of comprehensive matches.
   */
  @Computed
  public List<SuggestListBoxData> getFilterOptions() {
    if (valueData == null || valueData.isEmpty()) {
      return options;
    }

    final String query = valueData.toLowerCase();

    final List<String> phrases = Stream.of(query.split(" "))
        .collect(Collectors.toCollection(ArrayList::new));

    final Set<SuggestListBoxData> secondaries = options.stream()
        .filter(v -> phrases.stream().allMatch(ph -> v.getDescription().toLowerCase().contains(ph)))
        .collect(Collectors.toCollection(LinkedHashSet::new));

    final String trim = Optional.of(query)
        .map(v -> v.replace(" ", ""))
        .orElse("");

    final HashSet<SuggestListBoxData> primaries = options.stream()
        .filter(v -> v.getDescription().replace(" ", "").toLowerCase().contains(trim))
        .collect(Collectors.toCollection(LinkedHashSet::new));

    primaries.addAll(secondaries);

    return new ArrayList<>(primaries);
  }

  @Computed
  public void setValue(final String input) {
    this.valueData = input;
    if (!showing) {
      showing = true;
    }
  }

  @Computed
  public String getValue() {
    return valueData;
  }

  @JsMethod
  public void toggle() {
    showing = !showing;
  }

  @JsMethod
  public void emptySelect() {
    vue().$emit("empty-select");
  }

  @JsMethod
  public void select(final SuggestListBoxData selection) {
    valueData = selection.getCode();
    vue().$emit("select", selection);
    vue().$emit("select-value", selection.getCode());
    showing = false;
  }

  @JsMethod
  public boolean isWarn() {
    return selectedData != null && selectedData.isWarn();
  }

  @Override
  public void mounted() {
    ref = elem;
  }
}
