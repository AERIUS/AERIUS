/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.farmland.standard;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.sector.category.FarmSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmlandCategory;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.suggest.SuggestListBoxComponent;
import nl.overheid.aerius.wui.application.components.input.suggest.SuggestListBoxData;
import nl.overheid.aerius.wui.application.components.source.farmland.standard.FarmlandStandardValidators.FarmlandStandardValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.farmland.FarmlandStandardUtil;
import nl.overheid.aerius.wui.application.domain.source.farmland.FarmlandUtil;
import nl.overheid.aerius.wui.application.domain.source.farmland.StandardFarmlandActivity;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.vue.DebugDirective;

@Component(name = "aer-farmland-standard-emission", customizeOptions = FarmlandStandardValidators.class, directives = {
    ValidateDirective.class,
    DebugDirective.class
}, components = {
    LabeledInputComponent.class,
    ValidationBehaviour.class,
    SuggestListBoxComponent.class,
    VerticalCollapse.class
})
public class FarmlandStandardEmissionComponent extends ErrorWarningValidator implements HasCreated, HasValidators<FarmlandStandardValidations> {
  @Prop @JsProperty StandardFarmlandActivity standardFarmlandActivity;
  @Prop @JsProperty List<FarmSourceCategory> farmSourceCategories;

  @Data @Inject ApplicationContext applicationContext;

  @Data String farmlandCategoryCodeV;
  @Data String numberOfAnimalsV;
  @Data String numberOfDaysV;
  @Data String tonnesV;
  @Data String metersCubedV;
  @Data String numberOfApplicationsV;

  @JsProperty(name = "$v") FarmlandStandardValidations validation;

  @Override
  @Computed
  public FarmlandStandardValidations getV() {
    return validation;
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Watch(value = "standardFarmlandActivity", isImmediate = true)
  public void onStandardFarmLodgingChange(final StandardFarmlandActivity neww) {
    if (neww == null) {
      return;
    }

    farmlandCategoryCodeV = neww.getFarmSourceCategoryCode();
    final FarmSourceCategory currentCategory = getCurrentCategory();
    final FarmEmissionFactorType emissionFactorType = currentCategory == null ? null : currentCategory.getFarmEmissionFactorType();
    updateValues(neww, emissionFactorType);
  }

  private void updateValues(final StandardFarmlandActivity neww, final FarmEmissionFactorType emissionFactorType) {
    if (FarmEmissionFactorType.expectsNumberOfAnimals(emissionFactorType)) {
      numberOfAnimalsV = neww.getNumberOfAnimals() == null ? "" : String.valueOf(neww.getNumberOfAnimals());
    } else {
      numberOfAnimalsV = "";
      neww.setNumberOfAnimals(null);
    }
    if (FarmEmissionFactorType.expectsNumberOfDays(emissionFactorType)) {
      numberOfDaysV = neww.getNumberOfDays() == null ? "" : String.valueOf(neww.getNumberOfDays());
    } else {
      numberOfDaysV = "";
      neww.setNumberOfDays(null);
    }
    if (FarmEmissionFactorType.expectsTonnes(emissionFactorType)) {
      tonnesV = neww.getTonnes() == null ? "" : String.valueOf(neww.getTonnes());
    } else {
      tonnesV = "";
      neww.setTonnes(null);
    }
    if (FarmEmissionFactorType.expectsMetersCubed(emissionFactorType)) {
      metersCubedV = neww.getMetersCubed() == null ? "" : String.valueOf(neww.getMetersCubed());
    } else {
      metersCubedV = "";
      neww.setMetersCubed(null);
    }
    if (FarmEmissionFactorType.expectsNumberOfApplications(emissionFactorType)) {
      numberOfApplicationsV = neww.getNumberOfApplications() == null ? "" : String.valueOf(neww.getNumberOfApplications());
    } else {
      numberOfApplicationsV = "";
      neww.setNumberOfApplications(null);
    }
  }

  @Computed
  public String getCategoryLabel() {
    return M.messages().esFarmlandStandardType(standardFarmlandActivity == null ? null : standardFarmlandActivity.getActivityCode());
  }

  @Computed
  public List<SuggestListBoxData> getFarmlandCategoryCodes() {
    return farmSourceCategories.stream()
        .map(v -> new SuggestListBoxData(v.getCode(), v.getDescription(), v.getDescription(), false))
        .collect(Collectors.toList());
  }

  @JsMethod
  public void unselectFarmlandCategoryCode() {
    standardFarmlandActivity.setFarmSourceCategoryCode(null);
    farmlandCategoryCodeV = null;
  }

  @JsMethod
  void selectFarmlandCategoryCode(final String code) {
    standardFarmlandActivity.setFarmSourceCategoryCode(code);
    farmlandCategoryCodeV = code;
  }

  @Computed
  protected String getFarmlandCategoryCode() {
    return farmlandCategoryCodeV;
  }

  @Computed
  protected String getNumberOfAnimals() {
    return numberOfAnimalsV;
  }

  @Computed
  protected void setNumberOfAnimals(final String numberOfAnimals) {
    ValidationUtil.setSafeDoubleOptionalValue(v -> standardFarmlandActivity.setNumberOfAnimals(v), numberOfAnimals);
    this.numberOfAnimalsV = numberOfAnimals;
  }

  @Computed
  protected String getNumberOfDays() {
    return numberOfDaysV;
  }

  @Computed
  protected void setNumberOfDays(final String numberOfDays) {
    ValidationUtil.setSafeDoubleOptionalValue(v -> standardFarmlandActivity.setNumberOfDays(v), numberOfDays);
    this.numberOfDaysV = numberOfDays;
  }

  @Computed
  protected String getTonnes() {
    return tonnesV;
  }

  @Computed
  protected void setTonnes(final String tonnes) {
    ValidationUtil.setSafeDoubleOptionalValue(v -> standardFarmlandActivity.setTonnes(v), tonnes);
    this.tonnesV = tonnes;
  }

  @Computed
  protected String getMetersCubed() {
    return metersCubedV;
  }

  @Computed
  protected void setMetersCubed(final String metersCubed) {
    ValidationUtil.setSafeDoubleOptionalValue(v -> standardFarmlandActivity.setMetersCubed(v), metersCubed);
    this.metersCubedV = metersCubed;
  }

  @Computed
  protected String getNumberOfApplications() {
    return numberOfApplicationsV;
  }

  @Computed
  protected void setNumberOfApplications(final String numberOfApplications) {
    ValidationUtil.setSafeDoubleOptionalValue(v -> standardFarmlandActivity.setNumberOfApplications(v), numberOfApplications);
    this.numberOfApplicationsV = numberOfApplications;
  }

  @Computed
  protected FarmSourceCategory getCurrentCategory() {
    return FarmlandStandardUtil.findFarmSourceCategory(farmlandCategoryCodeV, farmSourceCategories);
  }

  @JsMethod
  protected String farmlandAcitivityCode() {
    final FarmlandCategory selectedFarmlandActivity = FarmlandUtil.findFarmland(standardFarmlandActivity.getActivityCode(),
        applicationContext.getConfiguration().getSectorCategories().getFarmlandCategories());
    return selectedFarmlandActivity != null ? selectedFarmlandActivity.getDescription() : "";
  }

  @JsMethod
  protected String numberOfAnimalsConversionError() {
    return errorIntMessage(numberOfAnimalsV,
        FarmlandStandardValidators.MIN_NUMBER_OF_ANIMALS,
        FarmlandStandardValidators.MAX_NUMBER_OF_ANIMALS);
  }

  @JsMethod
  protected String numberOfDaysConversionError() {
    return errorIntMessage(numberOfDaysV,
        FarmlandStandardValidators.MIN_NUMBER_OF_DAYS,
        FarmlandStandardValidators.MAX_NUMBER_OF_DAYS);
  }

  @JsMethod
  protected String tonnesConversionError() {
    return errorDoubleMessage(tonnesV,
        FarmlandStandardValidators.MIN_TONNES,
        FarmlandStandardValidators.MAX_TONNES);
  }

  @JsMethod
  protected String metersCubedConversionError() {
    return errorDoubleMessage(metersCubedV,
        FarmlandStandardValidators.MIN_METERS_CUBED,
        FarmlandStandardValidators.MAX_METERS_CUBED);
  }

  @JsMethod
  protected String numberOfApplicationsConversionError() {
    return errorIntMessage(numberOfApplicationsV,
        FarmlandStandardValidators.MIN_NUMBER_OF_APPLICATIONS,
        FarmlandStandardValidators.MAX_NUMBER_OF_APPLICATIONS);
  }

  private String errorIntMessage(final String field, final int min, final int max) {
    return field == null || field.isEmpty()
        ? i18n.validationRequired()
        : i18n.ivIntegerRangeBetween(field, min, max);
  }

  private String errorDoubleMessage(final String field, final int min, final int max) {
    return field == null || field.isEmpty()
        ? i18n.validationRequired()
        : i18n.ivDoubleRangeBetween(field, min, max);
  }

  @JsMethod
  protected boolean expectsNumberOfAnimals() {
    final FarmSourceCategory category = getCurrentCategory();
    return category != null && FarmEmissionFactorType.expectsNumberOfAnimals(category.getFarmEmissionFactorType());
  }

  @JsMethod
  protected boolean expectsNumberOfDays() {
    final FarmSourceCategory category = getCurrentCategory();
    return category != null && FarmEmissionFactorType.expectsNumberOfDays(category.getFarmEmissionFactorType());
  }

  @JsMethod
  protected boolean expectsTonnes() {
    final FarmSourceCategory category = getCurrentCategory();
    return category != null && FarmEmissionFactorType.expectsTonnes(category.getFarmEmissionFactorType());
  }

  @JsMethod
  protected boolean expectsMetersCubed() {
    final FarmSourceCategory category = getCurrentCategory();
    return category != null && FarmEmissionFactorType.expectsMetersCubed(category.getFarmEmissionFactorType());
  }

  @JsMethod
  protected boolean expectsNumberOfApplications() {
    final FarmSourceCategory category = getCurrentCategory();
    return category != null && FarmEmissionFactorType.expectsNumberOfApplications(category.getFarmEmissionFactorType());
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return isInvalidFarmlandCategoryCode()
        || isInvalidNumberOfAnimals()
        || isInvalidNumberOfDays()
        || isInvalidTonnes()
        || isInvalidMetersCubed()
        || isInvalidNumberOfApplications();
  }

  private boolean isInvalidFarmlandCategoryCode() {
    return validation.farmlandCategoryCodeV.invalid;
  }

  private boolean isInvalidNumberOfAnimals() {
    return expectsNumberOfAnimals() && validation.numberOfAnimalsV.invalid;
  }

  private boolean isInvalidNumberOfDays() {
    return expectsNumberOfDays() && validation.numberOfDaysV.invalid;
  }

  private boolean isInvalidTonnes() {
    return expectsTonnes() && validation.tonnesV.invalid;
  }

  private boolean isInvalidMetersCubed() {
    return expectsMetersCubed() && validation.metersCubedV.invalid;
  }

  private boolean isInvalidNumberOfApplications() {
    return expectsNumberOfApplications() && validation.numberOfApplicationsV.invalid;
  }

}
