/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.command.result;

import com.google.web.bindery.event.shared.binder.GenericEvent;

import elemental2.dom.HTMLElement;

import nl.aerius.wui.command.SimpleStatelessCommand;
import nl.overheid.aerius.wui.application.event.LegendPopupHiddenEvent;
import nl.overheid.aerius.wui.application.event.LegendPopupVisibleEvent;

public class ProcessContributionsLegendPopupToggleCommand extends SimpleStatelessCommand<HTMLElement> {
  public ProcessContributionsLegendPopupToggleCommand(final HTMLElement value) {
    super(value);
  }

  private boolean showing;

  @Override
  public GenericEvent getEvent() {
    return showing ? new LegendPopupVisibleEvent() : new LegendPopupHiddenEvent();
  }

  @Override
  public boolean isSilent() {
    return false;
  }

  public void resolve(final boolean showing) {
    this.showing = showing;
  }
}
