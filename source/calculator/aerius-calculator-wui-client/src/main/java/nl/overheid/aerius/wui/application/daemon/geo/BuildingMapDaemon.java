/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.geo;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.EventsKey;
import ol.Observable;

import nl.aerius.geo.command.InteractionAddedCommand;
import nl.aerius.geo.command.InteractionRemoveCommand;
import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.event.PlaceChangeEvent;
import nl.overheid.aerius.wui.application.command.building.BuildingDrawGeometryCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingModifyGeometryCommand;
import nl.overheid.aerius.wui.application.event.building.BuildingDrawGeometryEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingEndModifyGeometryEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingStartDrawGeometryEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingStartModifyGeometryEvent;
import nl.overheid.aerius.wui.application.geo.interactions.FeatureDrawInteraction;
import nl.overheid.aerius.wui.application.geo.interactions.FeatureModifyInteraction;
import nl.overheid.aerius.wui.application.place.BuildingPlace;

/**
 * Manages interactions with buildings on the map
 */
public class BuildingMapDaemon extends BasicEventComponent implements Daemon {
  private static final BuildingMapDaemonEventBinder EVENT_BINDER = GWT.create(BuildingMapDaemonEventBinder.class);

  interface BuildingMapDaemonEventBinder extends EventBinder<BuildingMapDaemon> {}

  private FeatureDrawInteraction drawInteraction;
  private FeatureModifyInteraction modifyInteraction;

  private EventsKey start;
  private EventsKey end;

  /**
   * Adds or removes interaction to draw the geometry of a building
   */
  @EventHandler
  public void onBuildingDrawGeometryCommand(final BuildingDrawGeometryCommand c) {
    if (drawInteraction != null) {
      eventBus.fireEvent(new InteractionRemoveCommand(drawInteraction));
    }

    if (c.getValue() != null) {
      drawInteraction = new FeatureDrawInteraction(c.getValue(),
          v -> eventBus.fireEvent(new BuildingStartDrawGeometryEvent(v)),
          v -> eventBus.fireEvent(new BuildingDrawGeometryEvent(v)));

      eventBus.fireEvent(new InteractionAddedCommand(drawInteraction));
    }
  }

  /**
   * Adds or removes interaction to modify the geometry of an emission source
   *
   * @param c
   */
  @EventHandler
  public void onBuildingModifyGeometryCommand(final BuildingModifyGeometryCommand c) {
    if (modifyInteraction != null) {
      eventBus.fireEvent(new InteractionRemoveCommand(modifyInteraction));
      Observable.unByKey(start);
      Observable.unByKey(end);
    }

    if (c.getValue() != null) {
      modifyInteraction = new FeatureModifyInteraction(c.getValue());
      eventBus.fireEvent(new InteractionAddedCommand(modifyInteraction));

      start = modifyInteraction.asInteraction().on("modifystart", e -> {
        eventBus.fireEvent(new BuildingStartModifyGeometryEvent());
      });
      end = modifyInteraction.asInteraction().on("modifyend", e -> {
        eventBus.fireEvent(new BuildingEndModifyGeometryEvent());

        // Explicitly persist the building on interaction end (primarily to persist
        // radius)
        eventBus.fireEvent(new BuildingDrawGeometryEvent(c.getValue().getGeometry()));
      });
    }
  }

  @EventHandler
  public void onPlaceChange(final PlaceChangeEvent e) {
    if (!(e.getValue() instanceof BuildingPlace)) {
      if (drawInteraction != null) {
        eventBus.fireEvent(new BuildingDrawGeometryCommand(null));
      }
      if (modifyInteraction != null) {
        eventBus.fireEvent(new BuildingModifyGeometryCommand(null));
      }
    }
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
