/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.results;

import nl.overheid.aerius.shared.domain.result.EmissionResultType;

/**
 * A client-side only enum that generalized all formal EmissionResultType permutations into a more simple form used for display purposes only.
 */
public enum GeneralizedEmissionResultType {
  DEPOSITION, CONCENTRATION;

  public static GeneralizedEmissionResultType fromEmissionResultType(final EmissionResultType type) {
    switch (type) {
    case DIRECT_CONCENTRATION:
    case CONCENTRATION:
      return CONCENTRATION;
    case DEPOSITION:
    case DRY_DEPOSITION:
    case WET_DEPOSITION:
      return DEPOSITION;
    default:
    case EXCEEDANCE_DAYS:
    case EXCEEDANCE_HOURS:
      // Behaviour is undefined (as of now), so just return null
      return null;
    }
  }
}
