/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.misc;

import java.util.Optional;

import javax.inject.Singleton;

import com.google.gwt.storage.client.Storage;

@Singleton
public class HotkeyContext {
  private static final String HOTKEY_PREFERENCE_KEY = "hotkeysEnabled";

  private boolean hotkeysEnabled = true;

  public HotkeyContext() {
    final Storage storage = Storage.getLocalStorageIfSupported();
    hotkeysEnabled = Optional.ofNullable(storage)
        .map(v -> v.getItem(HOTKEY_PREFERENCE_KEY))
        .map(v -> false).orElse(true);
  }

  public void toggle() {
    setHotkeysEnabled(!hotkeysEnabled);
  }

  private void setHotkeysEnabled(final boolean hotkeysEnabled) {
    this.hotkeysEnabled = hotkeysEnabled;

    final Storage storage = Storage.getLocalStorageIfSupported();
    if (storage != null) {
      if (hotkeysEnabled) {
        storage.removeItem(HOTKEY_PREFERENCE_KEY);
      } else {
        storage.setItem(HOTKEY_PREFERENCE_KEY, String.valueOf(hotkeysEnabled));
      }
    }
  }

  public boolean isEnabled() {
    return hotkeysEnabled;
  }
}
