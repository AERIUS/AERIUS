/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.info;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationHandle;
import nl.overheid.aerius.wui.application.domain.info.JobReceptorInfo;
import nl.overheid.aerius.wui.application.domain.result.EmissionResults;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    VerticalCollapse.class,
    BasicInfoRowComponent.class,
})
public class EmissionResultComponent extends BasicVueComponent {
  private static final String EMPTY = "-";

  @Inject @Data ApplicationContext appContext;
  @Inject @Data ScenarioContext scenarioContext;

  @Prop CalculationExecutionContext calculation;
  @Prop JobReceptorInfo jobReceptorInfo;
  @Prop boolean isComplete;

  @Computed("isCalculationSourceDiscrepancy")
  public boolean isCalculationSourceDiscrepancy() {
    return ScenarioContext.isCalculationStale(scenarioContext, calculation.getJobContext(), calculation);
  }

  @Computed
  public List<EmissionResultKey> getEmissionResultKeys() {
    return appContext.getConfiguration().getEmissionResultKeys();
  }

  @Computed
  public List<ScenarioResultType> getScenarioResultTypes() {
    final CalculationJobType jobType = calculation.getJobContext().getCalculationOptions().getCalculationJobType();
    return ScenarioResultType.getResultTypesForJobType(jobType).stream()
        .filter(v -> v != ScenarioResultType.SITUATION_RESULT)
        .collect(Collectors.toList());
  }

  @Computed
  public ScenarioResultType getScenarioTotalResultType() {
    final CalculationJobType jobType = calculation.getJobContext().getCalculationOptions().getCalculationJobType();
    switch (jobType) {
    case PROCESS_CONTRIBUTION:
      return ScenarioResultType.PROJECT_CALCULATION;
    case IN_COMBINATION_PROCESS_CONTRIBUTION:
      return ScenarioResultType.IN_COMBINATION;
    case MAX_TEMPORARY_EFFECT:
      return ScenarioResultType.MAX_TEMPORARY_EFFECT;
    default:
      return null;
    }
  }

  @Computed
  public String getScenarioTotalResultHeader() {
    final String totalLabel = M.messages().resultsTotalName(appContext.getTheme());
    final String scenarioResultTypeLabel = M.messages().resultsDropdown(getScenarioTotalResultType());
    return M.messages().infoPanelTotalResults(totalLabel, scenarioResultTypeLabel);
  }

  @Computed
  public List<SituationHandle> getScenarios() {
    return Optional.ofNullable(calculation)
        .map(v -> v.getSituations())
        .orElseGet(() -> new ArrayList<>());
  }

  @Computed("hasEmissionResults")
  public boolean hasEmissionResults() {
    if (jobReceptorInfo == null) {
      return false;
    }

    return getScenarios().stream()
        .anyMatch(situation -> jobReceptorInfo.hasSituationEmissionResults(situation.getId()));
  }

  @JsMethod
  public String getSituationResult(final String situationId, final EmissionResultKey key) {
    return getEmissionResult(
        () -> jobReceptorInfo.hasSituationEmissionResults(situationId),
        () -> jobReceptorInfo.getSituationEmissionResults(situationId),
        key);
  }

  @JsMethod
  public String getScenarioResult(final ScenarioResultType type, final EmissionResultKey key) {
    return getEmissionResult(
        () -> jobReceptorInfo.hasScenarioEmissionResults(type),
        () -> jobReceptorInfo.getScenarioEmissionResults(type).getResults(),
        key);
  }

  @JsMethod
  public String getScenarioTotalResult(final ScenarioResultType type, final EmissionResultKey key) {
    return getEmissionResult(
        () -> jobReceptorInfo.hasScenarioEmissionResults(type),
        () -> jobReceptorInfo.getScenarioEmissionResults(type).getTotalResults(),
        key);
  }

  private String getEmissionResult(final Supplier<Boolean> hasResults, final Supplier<EmissionResults> getResults, final EmissionResultKey key) {
    if (jobReceptorInfo == null || !hasResults.get() || getResults.get() == null) {
      return EMPTY;
    }

    final EmissionResults result = getResults.get();
    return MessageFormatter.formatResultWithUnit(
        result.getEmissionResultValue(key),
        key,
        appContext.getConfiguration().getEmissionResultValueDisplaySettings());
  }

}
