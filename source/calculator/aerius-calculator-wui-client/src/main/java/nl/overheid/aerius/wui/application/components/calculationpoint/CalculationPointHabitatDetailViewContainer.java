/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.calculationpoint;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.HorizontalCollapse;
import nl.overheid.aerius.wui.application.context.CalculationPointEditorContext;
import nl.overheid.aerius.wui.application.domain.calculationpoint.CalculationPointEntityReference;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component(components = {
    CalculationPointHabitatDetailView.class,
    HorizontalCollapse.class
})
public class CalculationPointHabitatDetailViewContainer extends BasicVueView  {
  @Inject @Data CalculationPointEditorContext context;
  @Prop EventBus eventBus;

  @Computed
  public CalculationPointEntityReference getHabitat() {
    if (isHabitatAvailable()) {
      return context.getSelectedHabitat();
    }

    return null;
  }

  @JsMethod
  public boolean isHabitatAvailable() {
    if (context == null) {
      return false;
    }

    return context.getSelectedHabitat() != null;
  }

}
