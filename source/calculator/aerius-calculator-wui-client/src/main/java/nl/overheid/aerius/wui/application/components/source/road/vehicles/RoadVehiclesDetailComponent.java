/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.road.vehicles;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.shared.ImaerConstants;
import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.AbstractCategory;
import nl.overheid.aerius.shared.domain.sector.category.ColdStartCategories;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategories;
import nl.overheid.aerius.shared.domain.sector.category.SimpleCategory;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.components.divider.DividerComponent;
import nl.overheid.aerius.wui.application.components.source.DetailStatComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.ColdStartESFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSubSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.road.ColdStartStandardVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.CustomVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.RoadTypeMode;
import nl.overheid.aerius.wui.application.domain.source.road.SpecificVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.StandardVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.ValuesPerVehicleType;
import nl.overheid.aerius.wui.application.domain.source.road.VehicleType;
import nl.overheid.aerius.wui.application.domain.source.road.Vehicles;
import nl.overheid.aerius.wui.vue.BasicVueComponent;
import nl.overheid.aerius.wui.vue.DebugDirective;

@Component(components = {
    DetailStatComponent.class,
    VerticalCollapse.class,
    VerticalCollapseGroup.class,
    DividerComponent.class
}, directives = {
    DebugDirective.class,
})
public class RoadVehiclesDetailComponent extends BasicVueComponent {
  @SuppressWarnings("rawtypes") @Prop EmissionSubSourceFeature source;
  @Prop(required = true) protected RoadTypeMode mode;
  @Prop boolean isVehicleBasedCharacteristics;
  @Prop @JsProperty protected List<Substance> substances;

  @Prop(required = true) protected boolean stagnationEnabled;

  @Data @Inject protected ApplicationContext applicationContext;

  public RoadEmissionCategories getRoadEmissionCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getRoadEmissionCategories();
  }

  public ColdStartCategories getColdStartEmissionCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getColdStartCategories();
  }

  @Computed("includeVehicleType")
  public boolean includeVehicleType() {
    return source.getEmissionSourceType() == EmissionSourceType.COLD_START
        && ((ColdStartESFeature) source).isVehicleBasedCharacteristics();
  }

  @JsMethod
  public String getColdStartVehicleType(final CustomVehicles customVehicle) {
    return Optional.ofNullable(AbstractCategory.determineByCode(getVehicleTypes(), customVehicle.getVehicleCode()))
        .map(v -> v.getDescription())
        .orElse("-");
  }

  @Computed("vehicleTypes")
  protected List<SimpleCategory> getVehicleTypes() {
    return getRoadEmissionCategories().getVehicleTypes();
  }

  @Computed("substances")
  public List<Substance> getSubstances() {
    return substances;
  }

  @Computed
  public String getUnit() {
    return mode == RoadTypeMode.COLD_START ? i18n.unitSingularGramPerColdStart()
        : applicationContext.isAppFlagEnabled(AppFlag.USE_ROAD_CUSTOM_UNITS_G_KM_S)
            ? i18n.unitSingularGramPerKilometersPerSecond()
            : i18n.unitSingularGramPerKilometers();
  }

  @JsMethod
  public String getEmissionFactorWithUnit(final CustomVehicles customVehicle, final Substance substance) {
    return i18n.decimalNumberFixed(customVehicle.getEmissionFactor(substance), 1) + " " + getUnit();
  }

  @JsMethod
  public boolean hasVehicleType(final VehicleType vehicleType) {
    return !getVehiclesByType(vehicleType).isEmpty();
  }

  @JsMethod
  public List<Vehicles> getVehiclesByType(final VehicleType vehicleType) {
    final List<Vehicles> list = new ArrayList<>();
    for (final Vehicles vehicle : getVehicleSubSources(source)) {
      if (vehicle.getVehicleType() == vehicleType) {
        list.add(vehicle);
      }
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  private List<Vehicles> getVehicleSubSources(final EmissionSubSourceFeature<?> source) {
    return ((EmissionSubSourceFeature<Vehicles>) source).getSubSources().asList();
  }

  @JsMethod
  public String getColdStartStandardVehiclesPerTimeUnit(final ColdStartStandardVehicles standardVehicle, final AbstractCategory vehicleType) {
    final double vehicles = standardVehicle.getValuesPerVehicleType(vehicleType.getCode());
    return i18n.decimalNumberFixed(vehicles, 1);
  }

  @JsMethod
  public String getStandardVehiclesPerTimeUnit(final StandardVehicles standardVehicle, final AbstractCategory vehicleType) {
    final ValuesPerVehicleType values = standardVehicle.getValuesPerVehicleType(vehicleType.getCode());
    final double vehicles = values == null ? 0 : values.getVehiclesPerTimeUnit();
    return i18n.decimalNumberFixed(vehicles, 1);
  }

  @JsMethod
  public String getStandardStagnationPerTimeUnit(final StandardVehicles standardVehicle, final AbstractCategory vehicleType) {
    final ValuesPerVehicleType values = standardVehicle.getValuesPerVehicleType(vehicleType.getCode());
    final double stagnationFraction = values == null ? 0 : values.getStagnationFraction();
    return i18n.decimalNumberFixed(stagnationFraction * ImaerConstants.PERCENTAGE_TO_FRACTION, 1) + " " + i18n.unitSingularPercentage();
  }

  @JsMethod
  public String getDetailStatDivision() {
    if (stagnationEnabled) {
      return "15fr 8fr"; // 2 columns
    }
    return "1fr"; // 1 column
  }

  @JsMethod
  public String getSpecificVehicleEuroClass(final SpecificVehicles specificVehicle) {
    if (applicationContext.getConfiguration().getSectorCategories()
        .determineOnRoadMobileSourceCategoryByCode(specificVehicle.getVehicleCode()) == null) {
      return specificVehicle.getVehicleCode();
    }

    return applicationContext.getConfiguration().getSectorCategories()
        .determineOnRoadMobileSourceCategoryByCode(specificVehicle.getVehicleCode()).getDescription();
  }
}
