/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.wui.application.command.api.ApiKeyLoginCommand;
import nl.overheid.aerius.wui.application.command.api.ApiKeyLogoutCommand;
import nl.overheid.aerius.wui.application.context.UserContext;
import nl.overheid.aerius.wui.application.daemon.flags.LocalStorageUtil;
import nl.overheid.aerius.wui.application.event.ApiKeyLoginFailedEvent;
import nl.overheid.aerius.wui.application.service.AeriusRequestCallback;
import nl.overheid.aerius.wui.application.service.UserService;

public class UserDaemon extends BasicEventComponent implements Daemon {
  interface UserDaemonEventBinder extends EventBinder<UserDaemon> {}

  private static final UserDaemonEventBinder EVENT_BINDER = GWT.create(UserDaemonEventBinder.class);

  private static final String AERIUS_API_KEY = "apiKey";

  @Inject private UserContext userContext;
  @Inject private UserService userService;

  @Override
  public void init() {
    userContext.setApiKey(LocalStorageUtil.tryGet(AERIUS_API_KEY)
        .orElse(null));
  }

  @EventHandler
  public void onUserApiLoginCommand(final ApiKeyLoginCommand c) {
    c.silence();

    userService.isValidApiKey(c.getValue(), AeriusRequestCallback.create(s -> {
      if (c.isPersistent()) {
        LocalStorageUtil.trySet(AERIUS_API_KEY, c.getValue());
      } else {
        LocalStorageUtil.tryRemove(AERIUS_API_KEY);
      }
      userContext.setApiKey(c.getValue());
      userContext.setAuthenticationError(false);
      eventBus.fireEvent(c.getEvent());
    }, s -> {
      userContext.setAuthenticationError(true);
      eventBus.fireEvent(new ApiKeyLoginFailedEvent(c.getValue()));
    }));
  }

  @EventHandler
  public void onUserApiLogoutCommand(final ApiKeyLogoutCommand c) {
    LocalStorageUtil.tryRemove(AERIUS_API_KEY);
    userContext.setApiKey(null);
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
