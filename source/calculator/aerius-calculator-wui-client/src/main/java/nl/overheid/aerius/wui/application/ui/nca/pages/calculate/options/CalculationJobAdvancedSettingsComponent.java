/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.nca.pages.calculate.options;

import static nl.overheid.aerius.shared.domain.calculation.RoadLocalFractionNO2Option.INDIVIDUAL_CUSTOM_VALUES;
import static nl.overheid.aerius.shared.domain.calculation.RoadLocalFractionNO2Option.LOCATION_BASED;
import static nl.overheid.aerius.shared.domain.calculation.RoadLocalFractionNO2Option.ONE_CUSTOM_VALUE;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.backgrounddata.MetSite;
import nl.overheid.aerius.shared.domain.calculation.RoadLocalFractionNO2Option;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.ADMSLimits;
import nl.overheid.aerius.wui.application.components.divider.DividerComponent;
import nl.overheid.aerius.wui.application.components.input.CheckBoxComponent;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.InputWithUnitComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.components.toggle.ToggleButtons;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.calculation.ADMSOptions;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.application.domain.calculation.NCACalculationOptions;
import nl.overheid.aerius.wui.application.ui.nca.pages.calculate.options.CalculationOptionsValidators.CalculationOptionsValidations;

@Component(customizeOptions = {
    CalculationOptionsValidators.class,
}, directives = {
    ValidateDirective.class
}, components = {
    ValidationBehaviour.class,
    ToggleButtons.class,
    LabeledInputComponent.class,
    SimplifiedListBoxComponent.class,
    InputWithUnitComponent.class,
    CalculationJobMeteorologicalSettingsComponent.class,
    VerticalCollapse.class,
    CheckBoxComponent.class,
    InputErrorComponent.class,
    DividerComponent.class,
})
public class CalculationJobAdvancedSettingsComponent extends ErrorWarningValidator
    implements HasCreated, HasValidators<CalculationOptionsValidations> {
  private static final Collection<RoadLocalFractionNO2Option> ROAD_LOCAL_FRACTION_NO2_RECEPTOR_OPTIONS = Arrays.asList(LOCATION_BASED,
      ONE_CUSTOM_VALUE);
  private static final Collection<RoadLocalFractionNO2Option> ROAD_LOCAL_FRACTION_NO2_POINTS_OPTIONS = Arrays.asList(LOCATION_BASED, ONE_CUSTOM_VALUE,
      INDIVIDUAL_CUSTOM_VALUES);

  @Inject @Data ApplicationContext applicationContext;

  @Prop(required = true) CalculationSetOptions options;

  @Data String minMoninObukhovLengthV;
  @Data String surfaceAlbedoV;
  @Data String priestleyTaylorParameterV;
  @Data String roadLocalFractionNO2V;

  @JsProperty(name = "$v") CalculationOptionsValidations validation;

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Override
  @Computed
  public CalculationOptionsValidations getV() {
    return validation;
  }

  @Watch(value = "options", isImmediate = true)
  public void onOptionsChange(final CalculationSetOptions neww) {
    final ADMSOptions admsOptions = getAdmsOptions();
    if (admsOptions != null) {
      minMoninObukhovLengthV = String.valueOf(admsOptions.getMinMoninObukhovLength());
      surfaceAlbedoV = String.valueOf(admsOptions.getSurfaceAlbedo());
      priestleyTaylorParameterV = String.valueOf(admsOptions.getPriestleyTaylorParameter());
      roadLocalFractionNO2V = String.valueOf(getNCAOptions().getRoadLocalFractionNO2());
    }
  }

  @JsMethod
  protected String obukhovLengthError() {
    return i18n.ivDoubleRangeBetween(i18n.calculateObukhovLengthLabel(), ADMSLimits.MIN_MONIN_OBUKHOV_LENGTH_MIN,
        ADMSLimits.MIN_MONIN_OBUKHOV_LENGTH_MAX);
  }

  @JsMethod
  protected String surfaceAlbedoError() {
    return i18n.ivDoubleRangeBetween(i18n.calculateSurfaceAlbedoLabel(), ADMSLimits.SURFACE_ALBEDO_MIN,
        ADMSLimits.SURFACE_ALBEDO_MAX);
  }

  @JsMethod
  protected String priestleyTaylorError() {
    return i18n.ivDoubleRangeBetween(i18n.calculatePriestleyTaylorLabel(), ADMSLimits.PRIESTLEY_TAYLOR_PARAMETER_MIN,
        ADMSLimits.PRIESTLEY_TAYLOR_PARAMETER_MAX);
  }

  @Computed
  List<CalculationOptionMode> getCalculationOptionMode() {
    return Arrays.asList(CalculationOptionMode.values());
  }

  /** BOILER PLATE */

  @Computed
  public void setMinMoninObukhovLength(final String val) {
    minMoninObukhovLengthV = val;
    ValidationUtil.setSafeDoubleValue(v -> getAdmsOptions().setMinMoninObukhovLength(v), val, 0D);
  }

  @Computed
  public String getMinMoninObukhovLength() {
    return minMoninObukhovLengthV;
  }

  @Computed
  public void setSurfaceAlbedo(final String val) {
    surfaceAlbedoV = val;
    ValidationUtil.setSafeDoubleValue(v -> getAdmsOptions().setSurfaceAlbedo(v), val, 0D);
  }

  @Computed
  public String getSurfaceAlbedo() {
    return surfaceAlbedoV;
  }

  @Computed
  public void setPriestleyTaylorParameter(final String val) {
    priestleyTaylorParameterV = val;
    ValidationUtil.setSafeDoubleValue(v -> getAdmsOptions().setPriestleyTaylorParameter(v), val, 0D);
  }

  @Computed
  public String getPriestleyTaylorParameter() {
    return priestleyTaylorParameterV;
  }

  @Computed
  public boolean getPlumeDepletionNH3() {
    return getAdmsOptions().isPlumeDepletionNH3();
  }

  @Computed
  public void setPlumeDepletionNH3(final boolean plumeDepletion) {
    getAdmsOptions().setPlumeDepletionNH3(plumeDepletion);
  }

  @Computed
  public boolean getPlumeDepletionNOX() {
    return getAdmsOptions().isPlumeDepletionNOX();
  }

  @Computed
  public void setPlumeDepletionNOX(final boolean plumeDepletion) {
    getAdmsOptions().setPlumeDepletionNOX(plumeDepletion);
  }

  @Computed
  public boolean getComplexTerrain() {
    return getAdmsOptions().isComplexTerrain();
  }

  @Computed
  public void setComplexTerrain(final boolean terrain) {
    getAdmsOptions().setComplexTerrain(terrain);
  }

  @JsMethod
  public String getPlumeDepletionLabel(final Substance substance) {
    return i18n.calculatePlumeDepletionLabel() + " (" + i18n.pollutant(substance) + ")";
  }

  private ADMSOptions getAdmsOptions() {
    return getNCAOptions().getAdmsOptions();
  }

  private NCACalculationOptions getNCAOptions() {
    return options.getNcaCalculationOptions();
  }

  @Computed("nameCalculatePrimaryfNO2Option")
  public Function<Object, String> nameCalculatePrimaryfNO2Option(final Object obj) {
    return name -> i18n.calculatePrimaryfNO2Option((RoadLocalFractionNO2Option) name);
  }

  @JsMethod
  public Collection<RoadLocalFractionNO2Option> roadLocalFractionNO2ReceptorsOptions() {
    return ROAD_LOCAL_FRACTION_NO2_RECEPTOR_OPTIONS;
  }

  @JsMethod
  public void selectRoadLocalFractionNO2ReceptorsOption(final String roadLocalFractionNO2OptionKey) {
    getNCAOptions().setRoadLocalFractionNO2ReceptorsOption(RoadLocalFractionNO2Option.safeValueOf(roadLocalFractionNO2OptionKey));
  }

  @Computed
  public String getRoadLocalFractionNO2ReceptorsOption() {
    return getNCAOptions().getRoadLocalFractionNO2ReceptorsOption().name();
  }

  @JsMethod
  public void selectRoadLocalFractionNO2PointsOption(final String roadLocalFractionNO2OptionKey) {
    getNCAOptions().setRoadLocalFractionNO2PointsOption(RoadLocalFractionNO2Option.safeValueOf(roadLocalFractionNO2OptionKey));
  }

  @JsMethod
  public Collection<RoadLocalFractionNO2Option> roadLocalFractionNO2PointsOptions() {
    return ROAD_LOCAL_FRACTION_NO2_POINTS_OPTIONS;
  }

  @Computed
  public String getRoadLocalFractionNO2PointsOption() {
    return getNCAOptions().getRoadLocalFractionNO2PointsOption().name();
  }

  @Computed("roadLocalFractionNO2Visible")
  public boolean isRoadLocalFractionNO2Enabled() {
    return RoadLocalFractionNO2Option.ONE_CUSTOM_VALUE == getNCAOptions().getRoadLocalFractionNO2ReceptorsOption()
        || RoadLocalFractionNO2Option.ONE_CUSTOM_VALUE == getNCAOptions().getRoadLocalFractionNO2PointsOption();
  }

  @Computed
  public void setRoadLocalFractionNO2(final String val) {
    roadLocalFractionNO2V = val;
    ValidationUtil.setSafeDoubleValue(v -> getNCAOptions().setRoadLocalFractionNO2(v), val, 0D);
  }

  @Computed
  public String getRoadLocalFractionNO2() {
    return roadLocalFractionNO2V;
  }

  @JsMethod
  protected String roadLocalFractionNO2Error() {
    return i18n.ivDoubleRangeBetween(i18n.calculatePrimaryfNO2PointsLabel(), 0.0, 1.0);
  }

  public List<MetSite> getSiteLocations() {
    return applicationContext.getConfiguration().getMetSites();
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return validation.minMoninObukhovLength.invalid
        || validation.surfaceAlbedo.invalid
        || validation.priestleyTaylorParameter.invalid
        || isRoadLocalFractionNO2Enabled() && validation.roadLocalFractionNO2.invalid;
  }
}
