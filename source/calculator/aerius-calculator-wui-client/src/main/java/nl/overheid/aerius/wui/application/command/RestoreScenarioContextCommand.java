/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.command;

import nl.aerius.wui.command.SimpleGenericCommand;
import nl.overheid.aerius.wui.application.event.RestoreScenarioContextEvent;

/**
 * Command to restore a scenario from the server.
 *
 * Print jobs get the situation id in the URL. Reloading a job in the UI should leave the situation id empty.
 * Reloading jobs expects the situation id to be empty in order to work correctly.
 */
public class RestoreScenarioContextCommand extends SimpleGenericCommand<String, RestoreScenarioContextEvent> {
  private String situationId;

  /**
   * Constructor for reloading a job.
   *
   * @param jobKey the job key for the scenario to retrieve from the server
   */
  public RestoreScenarioContextCommand(final String jobKey) {
    this(jobKey, "");
  }

  /**
   * Constructor for printing a page.
   *
   * @param jobKey the job key for the scenario to retrieve from the server
   * @param situationId The situation that should be activated
   */
  public RestoreScenarioContextCommand(final String jobKey, final String situationId) {
    super(jobKey);
    this.situationId = situationId;
  }

  public void setSituationId(final String situationId) {
    this.situationId = situationId;
  }

  public String getSituationId() {
    return situationId;
  }

  @Override
  protected RestoreScenarioContextEvent createEvent(final String jobKey) {
    return new RestoreScenarioContextEvent(jobKey, situationId);
  }
}
