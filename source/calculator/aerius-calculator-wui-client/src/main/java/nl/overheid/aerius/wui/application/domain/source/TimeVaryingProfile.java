/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import elemental2.core.Global;

import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfileType;
import nl.overheid.aerius.wui.application.domain.ItemWithStringId;
import nl.overheid.aerius.wui.application.util.json.JsonBuilder;
import nl.overheid.aerius.wui.application.util.json.JsonSerializable;
import nl.overheid.aerius.wui.vue.ReactivityUtil;

@JsType
public class TimeVaryingProfile implements ItemWithStringId, JsonSerializable {
  private @JsProperty String id = null;
  private @JsProperty String gmlId = null;
  private @JsProperty String label = null;
  private @JsProperty String type = null;

  private @JsProperty boolean systemProfile = false;
  private @JsProperty String description = null;

  private @JsProperty List<Double> values = new ArrayList<>();;

  public static TimeVaryingProfile create(final CustomTimeVaryingProfileType type) {
    final TimeVaryingProfile profile = new TimeVaryingProfile();
    init(profile, type);
    return profile;
  }

  public static void init(final TimeVaryingProfile profile, final CustomTimeVaryingProfileType type) {
    ReactivityUtil.ensureJsPropertyAsNull(profile, "id", profile::setId);
    ReactivityUtil.ensureJsPropertyAsNull(profile, "gmlId", profile::setGmlId);
    ReactivityUtil.ensureJsPropertyAsNull(profile, "label", profile::setLabel);
    profile.setType(type);

    ReactivityUtil.ensureJsProperty(profile, "systemProfile", profile::setSystemProfile, false);
    ReactivityUtil.ensureJsPropertyAsNull(profile, "description", profile::setDescription);

    ReactivityUtil.ensureJsPropertySupplied(profile, "values", profile::setValues, () -> new ArrayList<>());
  }

  public CustomTimeVaryingProfileType getType() {
    return CustomTimeVaryingProfileType.safeValueOf(type);
  }

  public void setType(final CustomTimeVaryingProfileType type) {
    this.type = type == null ? null : type.name();
  }

  public void setId(final String id) {
    this.id = id;
  }

  /**
   * The ID is for internal use/accounting, the GML will in practice be largely the same with a prefix
   */
  public String getId() {
    return id;
  }

  public void setGmlId(final String gmlId) {
    this.gmlId = gmlId;
  }

  public String getGmlId() {
    return gmlId;
  }

  public void setLabel(final String label) {
    this.label = label;
  }

  public String getLabel() {
    return label;
  }

  public List<Double> getValues() {
    return values;
  }

  public void setValues(final List<Double> values) {
    this.values = values;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  public boolean isSystemProfile() {
    return systemProfile;
  }

  public void setSystemProfile(final boolean systemProfile) {
    this.systemProfile = systemProfile;
  }

  public TimeVaryingProfile copy() {
    final TimeVaryingProfile copy = new TimeVaryingProfile();
    copyInto(copy);
    return copy;
  }

  public void copyInto(final TimeVaryingProfile target) {
    target.setId(target.getId());
    target.setGmlId(gmlId);
    target.setLabel(label);
    target.setType(getType());
    target.setValues(new ArrayList<>(values));
    target.setSystemProfile(systemProfile);
    target.setDescription(description);
  }

  public static TimeVaryingProfile fromJSObject(final JsPropertyMap<?> jsObject) {
    final TimeVaryingProfile profile = new TimeVaryingProfile();
    profile.setGmlId((String) jsObject.get("gmlId"));
    profile.setLabel((String) jsObject.get("label"));
    profile.setType(CustomTimeVaryingProfileType.safeValueOf((String) jsObject.get("type")));
    profile.setValues(new ArrayList<>());
    final double[] values = Js.uncheckedCast(jsObject.get("values"));
    profile.setValues(new ArrayList<>(Arrays.stream(values).boxed().collect(Collectors.toList())));
    return profile;
  }

  @Override
  public int hashCode() {
    return Objects.hash(label, systemProfile, type, values);
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final TimeVaryingProfile other = (TimeVaryingProfile) obj;
    return Objects.equals(label, other.label) && systemProfile == other.systemProfile && Objects.equals(type, other.type)
        && Objects.equals(values, other.values);
  }

  @Override
  public Object toJSON() {
    return new JsonBuilder(this)
        .include("gmlId", "label", "type")
        // Can't find another way to properly get rid of the GWT typing stuff, so open to suggestions here, but this is safe
        .set("values", Global.JSON.parse("["
            + (values == null ? "" : values.stream()
                .map(String::valueOf)
                .collect(Collectors.joining(",")))
            + "]"))
        .build();
  }
}
