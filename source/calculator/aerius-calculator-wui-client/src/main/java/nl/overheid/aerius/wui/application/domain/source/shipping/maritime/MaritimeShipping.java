/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.shipping.maritime;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.wui.application.domain.source.shipping.base.AbstractShipping;
import nl.overheid.aerius.wui.vue.ReactivityUtil;
import nl.overheid.aerius.wui.vue.SkipReactivityValidations;

/**
 * Client side implementation of Maritime Shipping.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public abstract class MaritimeShipping extends AbstractShipping {

  private String maritimeShippingType;
  private int movementsPerTimeUnit;
  private String timeUnit;

  public static final @JsOverlay void init(final MaritimeShipping props, final MaritimeShippingType type) {
    AbstractShipping.initAbstractShipping(props);
    props.setMaritimeShippingType(type);

    ReactivityUtil.ensureJsProperty(props, "movementsPerTimeUnit", props::setMovementsPerTimeUnit, 0);
    ReactivityUtil.ensureJsProperty(props, "timeUnit", props::setTimeUnit, TimeUnit.YEAR);
  }

  @SkipReactivityValidations
  public static final @JsOverlay void initTypeDependent(final MaritimeShipping props, final MaritimeShippingType type) {
    switch (type) {
    case CUSTOM:
      CustomMaritimeShipping.init(Js.uncheckedCast(props));
      break;
    case STANDARD:
      StandardMaritimeShipping.init(Js.uncheckedCast(props));
      break;
    default:
      throw new RuntimeException("Unknown type: " + type);
    }
  }

  public final @JsOverlay MaritimeShippingType getMaritimeShippingType() {
    return MaritimeShippingType.safeValueOf(maritimeShippingType);
  }

  public final @JsOverlay void setMaritimeShippingType(final MaritimeShippingType type) {
    this.maritimeShippingType = type.name();
  }

  public final @JsOverlay int getMovementsPerTimeUnit() {
    return movementsPerTimeUnit;
  }

  public final @JsOverlay void setMovementsPerTimeUnit(final int movementsPerTimeUnit) {
    this.movementsPerTimeUnit = movementsPerTimeUnit;
  }

  public final @JsOverlay TimeUnit getTimeUnit() {
    return TimeUnit.valueOf(timeUnit);
  }

  public final @JsOverlay void setTimeUnit(final TimeUnit timeUnit) {
    this.timeUnit = timeUnit.name();
  }

}
