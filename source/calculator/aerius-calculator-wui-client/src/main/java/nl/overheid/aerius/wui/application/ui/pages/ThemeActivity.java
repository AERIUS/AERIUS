/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.ResettableEventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import elemental2.dom.DomGlobal;
import elemental2.dom.EventListener;
import elemental2.dom.MouseEvent;

import jsinterop.base.Js;

import nl.aerius.wui.activity.ActivityContext;
import nl.aerius.wui.activity.DelegableActivity;
import nl.aerius.wui.command.PlaceChangeCommand;
import nl.aerius.wui.command.PlaceChangeRequestCommand;
import nl.aerius.wui.place.Place;
import nl.aerius.wui.util.NotificationUtil;
import nl.aerius.wui.vue.activity.AbstractVueActivity;
import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.ProductProfile;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.wui.application.activity.DelegatedActivityManager;
import nl.overheid.aerius.wui.application.command.RestoreScenarioContextCommand;
import nl.overheid.aerius.wui.application.command.misc.NavigateExternalContentViewCommand;
import nl.overheid.aerius.wui.application.command.misc.ToggleHotkeysCommand;
import nl.overheid.aerius.wui.application.components.nav.NavigationItem;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ExternalContentContext;
import nl.overheid.aerius.wui.application.context.LayerPanelContext;
import nl.overheid.aerius.wui.application.context.NavigationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.event.ApplicationFinishedLoadingEvent;
import nl.overheid.aerius.wui.application.event.RestoreCalculationJobContextEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.place.ApplicationPlaceController;
import nl.overheid.aerius.wui.application.place.BuildingPlace;
import nl.overheid.aerius.wui.application.place.CalculatePlace;
import nl.overheid.aerius.wui.application.place.CalculationPointsPlace;
import nl.overheid.aerius.wui.application.place.EmissionSourcePlace;
import nl.overheid.aerius.wui.application.place.ExportPlace;
import nl.overheid.aerius.wui.application.place.HomePlace;
import nl.overheid.aerius.wui.application.place.MainThemePlace;
import nl.overheid.aerius.wui.application.place.ResultsPlace;
import nl.overheid.aerius.wui.application.place.ScenarioInputListPlace;
import nl.overheid.aerius.wui.application.place.TimeVaryingProfilePlace;
import nl.overheid.aerius.wui.application.ui.pages.externalcontent.ExternalContentViewFactory;
import nl.overheid.aerius.wui.application.ui.pages.language.LanguageViewFactory;
import nl.overheid.aerius.wui.application.ui.pages.preferences.PreferencesViewFactory;
import nl.overheid.aerius.wui.config.ApplicationFlags;

/**
 * Base class for Delegable Theme activity classes. Override methods if specific implementations are needed.
 */
public class ThemeActivity<T extends DelegatedActivityManager<ThemeView>> extends AbstractVueActivity<ThemePresenter, ThemeView, ThemeViewFactory>
    implements ThemePresenter, DelegableActivity {

  @SuppressWarnings("rawtypes")
  interface ThemeActivityEventBinder extends EventBinder<ThemeActivity> {}

  private static final String DRAG_ENTER = "dragenter";

  private final ThemeActivityEventBinder EVENT_BINDER = GWT.create(ThemeActivityEventBinder.class);

  @Inject protected ApplicationPlaceController placeController;
  @Inject ActivityContext activityContext;
  @Inject ApplicationFlags flags;

  @Inject LayerPanelContext layerPanelContext;
  @Inject ApplicationContext applicationContext;
  @Inject CalculationContext calculationContext;
  @Inject ScenarioContext scenarioContext;
  @Inject protected NavigationContext navigationContext;

  private final T delegator;

  private final EventListener dragEnterListener = e -> onDragStart(Js.uncheckedCast(e));

  public ThemeActivity(final T delegator) {
    super(ThemeViewFactory.get());

    this.delegator = delegator;
  }

  @Override
  public void onStart() {
    DomGlobal.document.addEventListener(DRAG_ENTER, dragEnterListener, true);
  }

  private void onDragStart(final MouseEvent e) {
    if (e.relatedTarget == null) {
      tryMoveHome();
    }
  }

  private void tryMoveHome() {
    // Don't change place if the current activity will be stubborn about stopping
    if (activityContext.getActivity().mayStop() != null) {
      NotificationUtil.broadcastWarning(eventBus, M.messages().importDragImpededText());
    } else {
      eventBus.fireEvent(new PlaceChangeRequestCommand(new HomePlace(applicationContext.getTheme())));
    }
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    setEventBus(eventBus, this, EVENT_BINDER);
  }

  @Override
  public String mayStop() {
    return delegator.mayStop();
  }

  @Override
  public void onStop() {
    DomGlobal.document.removeEventListener(DRAG_ENTER, dragEnterListener);
    delegator.onStop();
  }

  @Override
  public ThemePresenter getPresenter() {
    return this;
  }

  @Override
  public boolean delegate(final ResettableEventBus eventBus, final PlaceChangeCommand c) {
    return delegator.delegate(eventBus, c.getValue(), c::setRedirect);
  }

  @Override
  public void setView(final ThemeView view) {
    delegator.setView(view);
  }

  @EventHandler
  public void onApplicationFinishedLoadingEvent(final ApplicationFinishedLoadingEvent e) {
    // TODO: Investigate whether this is needed; the navigation is also initialized in the constructor of the activity
    initNavigation(applicationContext.getTheme(), applicationContext.getProductProfile());

    final String jobKey = flags.getJobKey();
    if (jobKey != null) {
      eventBus.fireEvent(new RestoreScenarioContextCommand(jobKey));
    }
  }

  @EventHandler
  public void onRestoreCalculationJobContextEvent(final RestoreCalculationJobContextEvent e) {
    placeController.goTo(new ResultsPlace(applicationContext.getTheme()));
  }

  public void initNavigation(final Theme theme, final ProductProfile productProfile) {
    navigationContext.reset();

    navigationContext.addTopItem(NavigationItem.createPlaceItem(M.messages().menuHome(), "icon-menu-home",
        p -> p instanceof HomePlace,
        placeController, new HomePlace(theme)));

    navigationContext.addTopItem(NavigationItem.createPlaceItem(M.messages().menuSources(), "icon-menu-input",
        p -> p instanceof BuildingPlace
            || p instanceof EmissionSourcePlace
            || p instanceof TimeVaryingProfilePlace
            || p instanceof ScenarioInputListPlace,
        placeController, new ScenarioInputListPlace(theme)));

    navigationContext.addTopItem(NavigationItem.createPlaceItem(M.messages().menuCalculationPoints(), "icon-menu-assessment-points",
        placeController, new CalculationPointsPlace(theme)));

    navigationContext.addTopItem(NavigationItem.createPlaceItem(M.messages().menuCalculate(), "icon-menu-calculate",
        placeController, new CalculatePlace(theme)));

    navigationContext.addTopItem(NavigationItem.createPlaceItem(M.messages().menuResults(), "icon-menu-results",
        placeController, new ResultsPlace(theme)));

    navigationContext.addTopItem(NavigationItem.createPlaceItem(M.messages().menuExport(), "icon-menu-export",
        placeController, new ExportPlace(theme)));

    addBottomItems();
  }

  private void addBottomItems() {
    addManual();

    addContact();

    addPreferences();

    addHotkeys();

    addAbout();

    addManualDownload();

    addLanguage();
  }

  private void addManual() {
    if (applicationContext.getConfiguration().hasManual()) {
      navigationContext.addBottomItem(NavigationItem.createSimpleItem(M.messages().menuManual(), "icon-menu-manual1",
          p -> navigationContext.isActiveManual(), t -> navigationContext.toggleManual()));
    }
  }

  private void addContact() {
    if (applicationContext.isAppFlagEnabled(AppFlag.SHOW_CONTACT_MENU)) {
      navigationContext.addBottomItem(NavigationItem.createSimpleItem(M.messages().menuContact(), "icon-menu-contact-us",
          p -> isExternalContentActive(ExternalContentContext.CONTACT),
          t -> toggleExternalContent(ExternalContentContext.CONTACT))
          .withPopout(true));
    }
  }

  private void addPreferences() {
    navigationContext.addBottomItem(NavigationItem.createSimpleItem(M.messages().menuPreferences(), "icon-menu-settings",
        p -> navigationContext.isActivePopout(PreferencesViewFactory.get().getComponentTagName(), null),
        t -> navigationContext.togglePopout(PreferencesViewFactory.get().getComponentTagName()))
        .withPopout(true));
  }

  private void addHotkeys() {
    navigationContext.addBottomItem(NavigationItem.createSimpleItem(M.messages().menuHotkeys(), "icon-menu-hotkeys",
        p -> false,
        t -> openHotkeys()));
  }

  private void addAbout() {
    if (applicationContext.isAppFlagEnabled(AppFlag.SHOW_ABOUT_MENU)) {
      navigationContext.addBottomItem(NavigationItem.createSimpleItem(M.messages().menuAbout(), "icon-menu-about",
          p -> isExternalContentActive(ExternalContentContext.ABOUT_THE_APPLICATION),
          t -> toggleExternalContent(ExternalContentContext.ABOUT_THE_APPLICATION))
          .withPopout(true));
    }
  }

  private void addManualDownload() {
    if (applicationContext.hasSetting(SharedConstantsEnum.MANUAL_URL)) {
      final String url = applicationContext.getConfiguration().getSettings().getSetting(SharedConstantsEnum.MANUAL_URL);
      navigationContext.addBottomItem(NavigationItem.createSimpleItem(M.messages().menuManualDownload(), "icon-menu-manual",
          p -> false,
          t -> Window.open(url, "_blank", null), url));
    }
  }

  protected void addLanguage() {
    navigationContext.addBottomItem(NavigationItem.createSimpleItem(M.messages().menuLanguage(), "icon-menu-language",
        p -> navigationContext.isActivePopout(LanguageViewFactory.get().getComponentTagName(), null),
        t -> navigationContext.togglePopout(LanguageViewFactory.get().getComponentTagName()))
        .withPopout(true));
  }

  private void openHotkeys() {
    eventBus.fireEvent(new ToggleHotkeysCommand());
  }

  private boolean isExternalContentActive(final String contentRef) {
    return navigationContext.isActivePopout(ExternalContentViewFactory.get().getComponentTagName(), contentRef);
  }

  private void toggleExternalContent(final String contentRef) {
    if (isExternalContentActive(contentRef)) {
      navigationContext.deactivatePopout();
    } else {
      eventBus.fireEvent(new NavigateExternalContentViewCommand(contentRef));
    }
  }

  @Override
  public boolean isDelegable(final Place place) {
    return place instanceof MainThemePlace;
  }
}
