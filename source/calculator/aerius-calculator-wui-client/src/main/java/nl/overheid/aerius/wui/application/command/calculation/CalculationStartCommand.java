/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.command.calculation;

import nl.aerius.wui.command.SimpleGenericCommand;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.event.CalculationStartEvent;

public class CalculationStartCommand extends SimpleGenericCommand<CalculationExecutionContext, CalculationStartEvent> {
  private final CalculationJobContext jobContext;

  public CalculationStartCommand(final CalculationExecutionContext value, final CalculationJobContext jobContext) {
    super(value);
    this.jobContext = jobContext;
  }

  @Override
  public CalculationStartEvent createEvent(final CalculationExecutionContext value) {
    return new CalculationStartEvent(value);
  }

  public CalculationJobContext getJobContext() {
    return jobContext;
  }
}
