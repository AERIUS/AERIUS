/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.characteristics.ops;

import java.util.Date;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.gwt.i18n.shared.DateTimeFormat;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;
import jsinterop.base.Js;

import nl.overheid.aerius.shared.domain.ops.HeatContentType;
import nl.overheid.aerius.shared.domain.ops.OPSLimits;
import nl.overheid.aerius.shared.domain.ops.OutflowDirectionType;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.OPSCharacteristics;
import nl.overheid.aerius.wui.application.geo.util.GeoType;
import nl.overheid.aerius.wui.application.util.NumberUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

/**
 * Abstract base class for displaying ops characteristics.
 */
@Component
public abstract class OPSCharacteristicsBasicComponent extends BasicVueComponent {
  private static final String SERVER_DATE_FORMAT = "yyyy-MM-dd";

  protected @Prop @JsProperty EmissionSourceFeature source;
  protected @Data @JsProperty OPSCharacteristics characteristics = null;
  protected @Data boolean particleSizeDistributionVisible = false;

  protected @Inject @Data ScenarioContext context;
  protected @Inject @Data ApplicationContext applicationContext;

  protected void watchSource() {
    characteristics = source == null ? null : Js.uncheckedCast(source.getCharacteristics());
  }

  @Computed
  public Sector getSector() {
    return applicationContext.getConfiguration().getSectorCategories().getSectorById(source.getSectorId());
  }

  @Computed("isFarmAnimalHousing")
  public boolean isFarmAnimalHousing() {
    return EmissionSourceType.FARM_ANIMAL_HOUSING == source.getEmissionSourceType()
        || EmissionSourceType.FARM_LODGE == source.getEmissionSourceType();
  }

  @Computed
  public String getEstablished() {
    if (!source.getEstablished().isEmpty()) {
      final DateTimeFormat formatterOutput = DateTimeFormat.getFormat(i18n.dateFormat());
      final DateTimeFormat formatterInput = DateTimeFormat.getFormat(SERVER_DATE_FORMAT);
      final Date establishedDate = formatterInput.parse(source.getEstablished());
      return formatterOutput.format(establishedDate);
    } else {
      return "-";
    }
  }

  @JsMethod
  public boolean spreadVisible() {
    return GeoType.POLYGON.is(source.getGeometry());
  }

  @Computed
  public String getInputOutflowDiameter() {
    return i18n.unitM(i18n.decimalNumberFixed(characteristics.getOutflowDiameter(), OPSLimits.SOURCE_OUTFLOW_DIAMETER_DIGITS_PRECISION));
  }

  @Computed
  public String getUsedOutflowDiameter() {
    return i18n.unitM(i18n.decimalNumberFixed(characteristics.isBuildingInfluence()
        ? determineMinMax(characteristics.getOutflowDiameter(), OPSLimits.SCOPE_BUILDING_OUTFLOW_DIAMETER_MINIMUM,
            OPSLimits.SCOPE_BUILDING_OUTFLOW_DIAMETER_MAXIMUM)
        : characteristics.getOutflowDiameter(),
        OPSLimits.SOURCE_OUTFLOW_DIAMETER_DIGITS_PRECISION));
  }

  @Computed
  public String getInputOutflowVelocity() {
    return i18n.unitMPerS(i18n.decimalNumberFixed(characteristics.getOutflowVelocity(), OPSLimits.SOURCE_OUTFLOW_VELOCITY_DIGITS_PRECISION));
  }

  @Computed
  public String getUsedOutflowVelocity() {
    return i18n.unitMPerS(i18n.decimalNumberFixed(characteristics.isBuildingInfluence()
        ? determineMinMax(characteristics.getOutflowVelocity(), OPSLimits.SCOPE_BUILDING_OUTFLOW_VELOCITY_MINIMUM,
            OPSLimits.SCOPE_BUILDING_OUTFLOW_VELOCITY_MAXIMUM)
        : characteristics.getOutflowVelocity(),
        OPSLimits.SOURCE_OUTFLOW_VELOCITY_DIGITS_PRECISION));
  }

  @Computed
  public String getInputEmissionHeight() {
    return i18n.unitM(i18n.decimalNumberFixed(characteristics.getEmissionHeight(), OPSLimits.SOURCE_OUTFLOW_VELOCITY_DIGITS_PRECISION));
  }

  @Computed
  public String getUsedEmissionHeight() {
    return i18n.unitM(i18n.decimalNumberFixed(characteristics.isBuildingInfluence()
        ? determineMinMax(characteristics.getEmissionHeight(), OPSLimits.SCOPE_BUILDING_OUTFLOW_HEIGHT_MINIMUM,
            OPSLimits.SCOPE_BUILDING_OUTFLOW_HEIGHT_MAXIMUM)
        : characteristics.getEmissionHeight(),
        OPSLimits.SOURCE_BUILDING_DIGITS_PRECISION));
  }

  @Computed
  public String getInputHeatContent() {
    return i18n.unitMW(i18n.decimalNumberFixed(characteristics.getHeatContent(), OPSLimits.SOURCE_HEAT_CONTENT_DIGITS_PRECISION));
  }

  @Computed
  public String getUsedHeatContent() {
    return i18n.unitMW(i18n.decimalNumberFixed(characteristics.isBuildingInfluence()
        ? determineMinMax(characteristics.getHeatContent(), 0D, 0D)
        : characteristics.getHeatContent(),
        OPSLimits.SOURCE_HEAT_CONTENT_DIGITS_PRECISION));
  }

  @Computed
  public String getParticleSizeDistribution() {
    return String.valueOf(characteristics.getParticleSizeDistribution());
  }

  @JsMethod
  public boolean isUserdefinedHeatContent() {
    return characteristics.getHeatContentType() == HeatContentType.NOT_FORCED;
  }

  @JsMethod
  public boolean isEmissionTemperatureDefault() {
    return NumberUtil.equalEnough(OPSLimits.AVERAGE_SURROUNDING_TEMPERATURE, characteristics.getEmissionTemperature());
  }

  @JsMethod
  public String getDefaultValueLabel(final boolean defaultValue) {
    return defaultValue ? i18n.esDefaultValues() : null;
  }

  @JsMethod
  public boolean isEmissionHeightDefault() {
    return NumberUtil.equalEnough(characteristics.getEmissionHeight(), getSector().getDefaultCharacteristics().getEmissionHeight());
  }

  @JsMethod
  public boolean isHeatContentDefault() {
    return NumberUtil.equalEnough(characteristics.getHeatContent(), getSector().getDefaultCharacteristics().getHeatContent());
  }

  /**
   * Unused because this isn't usually editable (but implemented nonetheless)
   */
  @JsMethod
  public boolean isDiurnalVariationDefault() {
    return characteristics.getDiurnalVariation() == getSector().getDefaultCharacteristics().getDiurnalVariation();
  }

  @JsMethod
  public boolean isSpreadDefault() {
    return NumberUtil.equalEnough(characteristics.getSpread(), getSector().getDefaultCharacteristics().getSpread());
  }

  @JsMethod
  public boolean isOutflowDirectionDefault() {
    return OutflowDirectionType.VERTICAL == characteristics.getOutflowDirection();
  }

  @JsMethod
  public boolean isOutflowVelocityDefault() {
    return NumberUtil.equalEnough(characteristics.getOutflowVelocity(), 0D);
  }

  @JsMethod
  public boolean isOutflowDiameterDefault() {
    return NumberUtil.equalEnough(characteristics.getOutflowDiameter(), OPSLimits.SCOPE_NO_BUILDING_OUTFLOW_DIAMETER_MINIMUM);
  }

  @JsMethod
  public boolean isParticleSizeDistributionDefault() {
    return characteristics.getParticleSizeDistribution() == getSector().getDefaultCharacteristics().getParticleSizeDistribution();
  }

  @JsMethod
  public double determineMinMax(final double value, final double min, final double max) {
    return Math.max(min, Math.min(max, value));
  }
}
