/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.characteristics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsProperty;
import jsinterop.base.Js;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.ops.DiurnalVariation;
import nl.overheid.aerius.shared.domain.ops.HeatContentType;
import nl.overheid.aerius.shared.domain.ops.OPSLimits;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.characteristics.OPSCharacteristicsEditorValidators.OPSCharacteristicsEditorValidations;
import nl.overheid.aerius.wui.application.components.source.characteristics.outflow.OutflowEditorComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.OPSCharacteristics;
import nl.overheid.aerius.wui.application.domain.source.SourceCharacteristics;
import nl.overheid.aerius.wui.application.geo.util.GeoType;

@Component(customizeOptions = {
    OPSCharacteristicsEditorValidators.class
}, directives = {
    ValidateDirective.class
}, components = {
    CollapsiblePanel.class,
    LabeledInputComponent.class,
    OutflowEditorComponent.class,
    ValidationBehaviour.class,
    VerticalCollapse.class,
})
public class OPSCharacteristicsEditorComponent extends ErrorWarningValidator
    implements HasCreated, HasValidators<OPSCharacteristicsEditorValidations> {
  @Prop EventBus eventBus;
  @Prop EmissionSourceFeature source;

  // Validations
  @Data String spreadV;
  @JsProperty(name = "$v") OPSCharacteristicsEditorValidations validation;

  @Data OPSCharacteristics characteristics;

  @Inject @Data ApplicationContext applicationContext;

  @Override
  @Computed
  public OPSCharacteristicsEditorValidations getV() {
    return validation;
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Watch(value = "characteristics", isImmediate = true)
  public void onCharacteristicsChange(final OPSCharacteristics neww) {
    if (neww == null) {
      return;
    }

    // If HeatContentType is null we set it to a defined value.
    if (neww.getHeatContentType() == null) {
      neww.setHeatContentType(HeatContentType.FORCED);
    }

    spreadV = String.valueOf(neww.getSpread());
  }

  @Watch(value = "source", isImmediate = true)
  public void watchSource() {
    characteristics = source == null ? null : Js.uncheckedCast(source.getCharacteristics());
  }

  @Computed
  public boolean getBuildingInfluence() {
    return Optional.ofNullable(characteristics)
        .map(SourceCharacteristics::isBuildingInfluence)
        .orElse(false);
  }

  @Computed
  protected String getEstablished() {
    return source.getEstablished();
  }

  @Computed
  void setEstablished(final String established) {
    source.setEstablished(established);
  }

  @Computed
  protected String getSpread() {
    return spreadV;
  }

  @Computed
  protected void setSpread(final String spread) {
    ValidationUtil.setSafeDoubleValue(v -> characteristics.setSpread(v), spread, 0.0);
    spreadV = spread;
  }

  @Computed
  protected String getSpreadValidationError() {
    return i18n.ivDoubleRangeBetween(i18n.sourceSpreadLabel(), OPSLimits.SOURCE_SPREAD_MINIMUM, OPSLimits.SOURCE_SPREAD_MAXIMUM);
  }

  @Computed("isPolygon")
  protected boolean isPolygon() {
    return GeoType.POLYGON.is(source.getGeometry());
  }

  @Computed("isFarmAnimalHousing")
  protected boolean isFarmAnimalHousing() {
    return EmissionSourceType.FARM_ANIMAL_HOUSING == source.getEmissionSourceType()
        || EmissionSourceType.FARM_LODGE == source.getEmissionSourceType();
  }

  @Computed("isEditDiurnalVariation")
  protected boolean isEditDiurnalVariation() {
    final SectorGroup sectorGroup = applicationContext.getConfiguration().getSectorCategories().findSectorGroupFromSectorId(source.getSectorId());
    return sectorGroup == SectorGroup.OTHER;
  }

  @Computed("diurnalVariations")
  protected List<DiurnalVariation> getDiurnalVariations() {
    final List<DiurnalVariation> diurnalVariations = new ArrayList<>(Arrays.asList(DiurnalVariation.values()));
    diurnalVariations.remove(DiurnalVariation.UNKNOWN);
    return diurnalVariations;
  }

  @Computed
  protected String getDiurnalVariationType() {
    return characteristics.getDiurnalVariation().name();
  }

  @Computed
  protected void setDiurnalVariationType(final String selectedDiurnalVariationType) {
    characteristics.setDiurnalVariation(DiurnalVariation.safeValueOf(selectedDiurnalVariationType));
  }
}
