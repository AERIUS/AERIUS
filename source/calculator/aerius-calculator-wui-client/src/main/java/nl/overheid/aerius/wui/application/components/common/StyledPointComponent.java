/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.common;

import java.util.Optional;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;

import ol.Feature;

import jsinterop.base.Js;

import nl.overheid.aerius.wui.application.domain.ItemWithStringIdUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component
public class StyledPointComponent extends BasicVueComponent {
  @Prop Feature feature;
  @Prop String displayId;
  @Prop String backColor;
  @Prop String foreColor;
  @Prop String borderRadius;
  @Prop String borderWidth;
  @Prop String height;
  @Prop String width;

  @Computed
  public String getId() {
    return Optional.ofNullable(displayId).orElse(Optional.ofNullable(feature)
        .map(v -> ItemWithStringIdUtil.getStringId(Js.uncheckedCast(v)))
        .orElse("-"));
  }
}
