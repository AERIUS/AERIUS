/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.source.manure;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.wui.application.components.modify.ModifyListComponent;
import nl.overheid.aerius.wui.application.components.source.SubSourceEditorComponent;
import nl.overheid.aerius.wui.application.components.source.SubSourceValidationBehaviour;
import nl.overheid.aerius.wui.application.components.source.validation.SubSourceEmptyError;
import nl.overheid.aerius.wui.application.components.source.validation.SubSourceValidatedRowComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.ManureStorageESFeature;
import nl.overheid.aerius.wui.application.domain.source.manure.ManureStorage;

@Component(components = {
    ModifyListComponent.class,
    SubSourceValidationBehaviour.class,
    ManureStorageEmissionEditorRowComponent.class,
    SubSourceValidatedRowComponent.class,
    SubSourceEmptyError.class,
    VerticalCollapseGroup.class,
})
public class ManureStorageEmissionEditorComponent extends SubSourceEditorComponent implements HasCreated {
  @Prop ManureStorageESFeature source;
  @Data ManureStorageEmissionEditorActivity presenter;

  @Inject @Data ApplicationContext applicationContext;

  @Computed("manureStorageSource")
  public ManureStorageESFeature getManureStorageSource() {
    return source;
  }

  @JsMethod
  public void selectSource(final Number index, final ManureStorage selectedSource) {
    if (selectedIndex.equals(index.intValue())) {
      selectedIndex = -1;
      resetSelected();
    } else {
      selectedIndex = index.intValue();
      presenter.selectedSource(selectedSource);
      editSource();
    }
  }

  @Override
  public void created() {
    presenter = new ManureStorageEmissionEditorActivity();
    presenter.setView(this);
  }
}
