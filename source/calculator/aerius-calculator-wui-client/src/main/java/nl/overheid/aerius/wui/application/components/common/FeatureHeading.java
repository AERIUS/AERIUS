/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.common;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import ol.Feature;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.wui.application.command.source.FeatureZoomCommand;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    StyledPointComponent.class
})
public class FeatureHeading extends BasicVueComponent {
  @Prop Feature feature;
  @Prop String backColor;
  @Prop String foreColor;
  @Prop String borderRadius;
  @Prop String width;

  @Prop EventBus eventBus;
  @Prop String title;

  @JsMethod
  public void onClick() {
    if (feature == null) {
      return;
    }

    eventBus.fireEvent(new FeatureZoomCommand(feature));
  }
}
