/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.scenario;

import java.util.List;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.place.PlaceController;
import nl.overheid.aerius.shared.domain.context.InputTypeViewMode;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfileType;
import nl.overheid.aerius.shared.util.ConsecutiveIdUtil;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileAddCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileCreateNewCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileEditCancelCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileEditCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileEditSaveCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileSelectCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.EmissionSourceEditorContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.TimeVaryingProfileEditorContext;
import nl.overheid.aerius.wui.application.domain.ClientConsecutiveIdUtil;
import nl.overheid.aerius.wui.application.domain.source.ADMSCharacteristics;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.TimeVaryingProfile;
import nl.overheid.aerius.wui.application.domain.source.util.TimeVaryingProfileUtil;
import nl.overheid.aerius.wui.application.event.timevarying.TimeVaryingProfileAddedEvent;
import nl.overheid.aerius.wui.application.event.timevarying.TimeVaryingProfileCreateNewEvent;
import nl.overheid.aerius.wui.application.event.timevarying.TimeVaryingProfileEditCancelEvent;
import nl.overheid.aerius.wui.application.event.timevarying.TimeVaryingProfileEditEvent;
import nl.overheid.aerius.wui.application.event.timevarying.TimeVaryingProfileEditSaveEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.place.EmissionSourcePlace;
import nl.overheid.aerius.wui.application.place.ScenarioInputListPlace;
import nl.overheid.aerius.wui.application.place.TimeVaryingProfilePlace;

@Singleton
public class TimeVaryingProfileEditorDaemon extends BasicEventComponent {
  private static final TimeVaryingProfileEditorDaemonEventBinder EVENT_BINDER = GWT.create(TimeVaryingProfileEditorDaemonEventBinder.class);

  interface TimeVaryingProfileEditorDaemonEventBinder extends EventBinder<TimeVaryingProfileEditorDaemon> {}

  @Inject private PlaceController placeController;

  @Inject private EmissionSourceEditorContext sourceEditorContext;
  @Inject private TimeVaryingProfileEditorContext context;
  @Inject private ScenarioContext scenarioContext;
  @Inject private ApplicationContext applicationContext;

  private TimeVaryingProfile originalProfile;

  @EventHandler
  public void onTimeVaryingProfileCreateNewCommand(final TimeVaryingProfileCreateNewCommand c) {
    final TimeVaryingProfile profile = TimeVaryingProfileUtil.create(c.getValue());
    final List<TimeVaryingProfile> profiles = scenarioContext.getActiveSituation().getTimeVaryingProfiles();
    final String nextLabel = ClientConsecutiveIdUtil.getNextLabel(profiles);
    profile.setLabel(M.messages().timeVaryingProfilePanelTitle(profile.getType()) + " " + nextLabel);

    final ConsecutiveIdUtil.IndexIdPair newId = ClientConsecutiveIdUtil.getConsecutiveIdUtil(profiles).generate();
    profile.setId(newId.getId());

    beginTimeVaryingProfileEdit(profile);
  }

  @EventHandler
  public void onTimeVaryingProfileEditCommand(final TimeVaryingProfileEditCommand c) {
    final TimeVaryingProfile clone = c.getValue().copy();

    beginTimeVaryingProfileEdit(clone, c.getValue());
  }

  @EventHandler
  public void onTimeVaryingProfileEditEvent(final TimeVaryingProfileEditEvent e) {
    final TimeVaryingProfile profile = e.getValue();
    final TimeVaryingProfilePlace place = new TimeVaryingProfilePlace(applicationContext.getTheme(),
        profile == null ? null : profile.getType());
    place.setProfileId(profile == null ? null : profile.getId());
    placeController.goTo(place);
  }

  @EventHandler
  public void onTimeVaryingProfileCreateNewEvent(final TimeVaryingProfileCreateNewEvent e) {
    placeController.goTo(new TimeVaryingProfilePlace(applicationContext.getTheme(), e.getValue()));
  }

  @EventHandler
  public void onTimeVaryingProfileEditSaveCommand(final TimeVaryingProfileEditSaveCommand c) {
    if (originalProfile == null) {
      eventBus.fireEvent(new TimeVaryingProfileAddCommand(context.getTimeVaryingProfile()));
      // silence the event because for new profiles the TimeVaryingProfileAddCommand will set the id of the profile.
      c.silence();
    } else {
      context.getTimeVaryingProfile().copyInto(originalProfile);
      eventBus.fireEvent(new TimeVaryingProfileSelectCommand(context.getTimeVaryingProfile(), false));
      originalProfile = null;
    }
    finishTimeVaryingProfileEdit();
  }

  @EventHandler
  public void onTimeVaryingProfileAddedEvent(final TimeVaryingProfileAddedEvent c) {
    eventBus.fireEvent(new TimeVaryingProfileSelectCommand(c.getValue(), false));
    setTimeVaryingProfileIdAndExit(c.getValue());
  }

  @EventHandler
  public void onTimeVaryingProfileEditSaveEvent(final TimeVaryingProfileEditSaveEvent e) {
    setTimeVaryingProfileIdAndExit(e.getValue());
  }

  private void setTimeVaryingProfileIdAndExit(final TimeVaryingProfile profile) {
    if (sourceEditorContext.isEditing()) {
      final EmissionSourceFeature source = sourceEditorContext.getSource();

      if (source.getCharacteristicsType() == CharacteristicsType.ADMS) {
        if (profile.getType() == CustomTimeVaryingProfileType.MONTHLY) {
          ((ADMSCharacteristics) source.getCharacteristics()).setCustomMonthlyTimeVaryingProfileId(profile.getGmlId());
        } else {
          ((ADMSCharacteristics) source.getCharacteristics()).setCustomHourlyTimeVaryingProfileId(profile.getGmlId());
        }
      }
    }
    exitTimeVaryingProfilePlace();
  }

  @EventHandler
  public void onTimeVaryingProfileEditCancelCommand(final TimeVaryingProfileEditCancelCommand c) {
    finishTimeVaryingProfileEdit();
  }

  @EventHandler
  public void onTimeVaryingProfileEditCancelEvent(final TimeVaryingProfileEditCancelEvent e) {
    exitTimeVaryingProfilePlace();
  }

  private void exitTimeVaryingProfilePlace() {
    // If a source is being edited, go there
    if (sourceEditorContext.isEditing()) {
      final EmissionSourceFeature source = sourceEditorContext.getSource();
      final EmissionSourcePlace place = new EmissionSourcePlace(applicationContext.getTheme());
      place.setEmissionSourceId(source.getId());
      placeController.goTo(place);
    } else {
      // Else go to the emission source overview
      placeController.goTo(new ScenarioInputListPlace(applicationContext.getTheme(), InputTypeViewMode.TIME_VARYING_PROFILES));
    }
  }

  /**
   * Begin editing a time-varying profile.
   */
  private void beginTimeVaryingProfileEdit(final TimeVaryingProfile profile) {
    beginTimeVaryingProfileEdit(profile, null);
  }

  private void beginTimeVaryingProfileEdit(final TimeVaryingProfile profile, final TimeVaryingProfile original) {
    if (context.isEditing()) {
      // Ideally we shouldn't be cleaning up (although it can be legitimate), so warn
      // about this occurring
      GWTProd.warn("Clearing temporary time-varying profile at a point where ideally we shouldn't be doing that.");
      finishTimeVaryingProfileEdit();
    }

    context.setTimeVaryingProfile(profile);
    profile.setId("-" + profile.getId());
    context.setSituationId(scenarioContext.getActiveSituationId());

    if (original != null) {
      originalProfile = original;
    } else {
      originalProfile = null;
    }
  }

  private void finishTimeVaryingProfileEdit() {
    if (context.isEditing()) {
      context.reset();
    }
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
