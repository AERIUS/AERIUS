/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.i18n;

import com.google.gwt.i18n.client.Messages.PluralCount;
import com.google.gwt.i18n.client.Messages.Select;

import nl.overheid.aerius.shared.domain.ServerUsage;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.JobState;

/**
 * Text specific for results pages.
 */
public interface CalculationSourceMessages {
  String calculationStateDescription(@Select JobState state);
  String calculationStateDescriptionCalculating(int remainingTime);
  String calculationStateDescriptionCalculatingAlmost();
  String calculationStateDescriptionCalculatingEstimating();
  String calculationStateDescriptionCalculatingLong(int remainingTimeHours, int remainingTimeMinutes);
  String calculationStateLastUpdate(int seconds);

  String calculationStateDescriptionQueuing(@PluralCount int queuePosition);
  String calculationStateDescriptionPostProcessing(@Select Theme theme);

  String calculationWarning(@Select ServerUsage usageState);

  String calculationStop();
  String calculationReset();
}
