/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.shipping.maritime;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.maritime.MaritimeMovementValidators.MaritimeMovementValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.domain.source.shipping.maritime.MaritimeShipping;
import nl.overheid.aerius.wui.vue.DebugDirective;

@Component(name = "aer-maritime-movement",
customizeOptions = {
    MaritimeMovementValidators.class
}, directives = {
    ValidateDirective.class,
    DebugDirective.class
}, components = {
    LabeledInputComponent.class,
    ValidationBehaviour.class
})
public class MaritimeMovementComponent extends ErrorWarningValidator implements HasCreated,
    HasValidators<MaritimeMovementValidations> {
  @Prop @JsProperty MaritimeShipping ship;
  @Data String maritimeMovementV;

  @JsProperty(name = "$v") MaritimeMovementValidations validation;

  @Watch(value = "ship", isImmediate = true)
  public void onShipChange(MaritimeShipping ship) {
    maritimeMovementV = String.valueOf(ship.getMovementsPerTimeUnit());
  }

  @Computed
  public MaritimeMovementValidations getV() {
    return validation;
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Computed
  protected void setTimeUnit(final String timeUnit) {
    ship.setTimeUnit(TimeUnit.valueOf(timeUnit));
  }

  @Computed
  protected String getTimeUnit() {
    return ship.getTimeUnit().name();
  }

  @Computed
  protected Integer getMaritimeMovements() {
    return ship.getMovementsPerTimeUnit();
  }

  @Computed
  protected void setMaritimeMovements(final String maritimeMovement) {
    ValidationUtil.setSafeIntegerValue(ship::setMovementsPerTimeUnit, maritimeMovement, 0);
    this.maritimeMovementV = maritimeMovement;
  }

  @Computed("isInvalid")
  protected boolean isInvalid() {
    return validation.maritimeMovementV.invalid;
  }

  @JsMethod
  protected String numberOfMaritimeMovementConversionError() {
    return i18n.errorNotValidEntry(maritimeMovementV);
  }
}
