/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.source.farmlodging.systems;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.Vue;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import elemental2.core.JsArray;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategories;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.source.farmlodging.systems.types.FarmAdditionalLodgingSystemsRowComponent;
import nl.overheid.aerius.wui.application.components.source.farmlodging.systems.types.FarmFodderMeasureRowComponent;
import nl.overheid.aerius.wui.application.components.source.farmlodging.systems.types.FarmReductiveLodgingSystemsRowComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.farm.AdditionalLodgingSystem;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmLodgingUtil;
import nl.overheid.aerius.wui.application.domain.source.farm.LodgingFodderMeasure;
import nl.overheid.aerius.wui.application.domain.source.farm.ReductiveLodgingSystem;
import nl.overheid.aerius.wui.application.domain.source.farm.StandardFarmLodging;

@Component(name = "aer-farmlodging-measure-editor", components = {
    SimplifiedListBoxComponent.class,
    FarmAdditionalLodgingSystemsRowComponent.class,
    FarmReductiveLodgingSystemsRowComponent.class,
    FarmFodderMeasureRowComponent.class,
    ValidationBehaviour.class,
    VerticalCollapse.class,
    VerticalCollapseGroup.class
})
public class FarmLodgingMeasureComponent extends ErrorWarningValidator implements IsVueComponent {
  @Prop @JsProperty StandardFarmLodging standardFarmLodging;
  @Prop @JsProperty FarmLodgingCategory farmLodgingCategory;

  @Data @Inject ApplicationContext applicationContext;

  @Data LodgingStackType selectedAdditionalType;

  @Computed("suggestionWarning")
  public boolean getSuggestionWarning() {
    if (farmLodgingCategory == null) {
      return false;
    }

    return FarmLodgingUtil.hasIncompatibleCombinations(standardFarmLodging, farmLodgingCategory, getCategories());
  }

  @JsMethod
  public FarmLodgingCategories getCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getFarmLodgingCategories();
  }

  @Computed
  public List<LodgingStackType> getAvailableStackTypes() {
    if (farmLodgingCategory == null) {
      return new ArrayList<>();
    }

    final FarmLodgingCategories cats = getCategories();
    final List<LodgingStackType> availableTypes = new ArrayList<>();
    if (!cats.getFarmAdditionalLodgingSystemCategories().isEmpty()) {
      availableTypes.add(LodgingStackType.ADDITIONAL);
    }
    if (!cats.getFarmReductiveLodgingSystemCategories().isEmpty()) {
      availableTypes.add(LodgingStackType.REDUCTIVE);
    }
    if (!cats.getFarmLodgingFodderMeasureCategories().isEmpty()) {
      availableTypes.add(LodgingStackType.FODDER);
    }
    return availableTypes;
  }

  @Computed
  public List<AdditionalLodgingSystem> getAdditionalLodgingSystems() {
    return standardFarmLodging.getAdditionalLodgingSystems().asList();
  }

  @Computed
  public List<ReductiveLodgingSystem> getReductiveLodgingSystems() {
    return standardFarmLodging.getReductiveLodgingSystems().asList();
  }

  @Computed
  public List<LodgingFodderMeasure> getFodderMeasures() {
    return standardFarmLodging.getFodderMeasures().asList();
  }

  @JsMethod
  public void selectLodgingSystem(final LodgingStackType option) {
    selectedAdditionalType = option;
    final LodgingStackType selectedValue = option;
    switch (selectedValue) {
    case FODDER:
      standardFarmLodging.getFodderMeasures().push(LodgingFodderMeasure.create());
      break;
    case REDUCTIVE:
      standardFarmLodging.getReductiveLodgingSystems().push(ReductiveLodgingSystem.create());
      break;
    case ADDITIONAL:
      final JsArray<AdditionalLodgingSystem> additionalLodgingSystems = standardFarmLodging.getAdditionalLodgingSystems();
      final AdditionalLodgingSystem additionalLodging = AdditionalLodgingSystem.create();
      additionalLodging.setNumberOfAnimals(standardFarmLodging.getNumberOfAnimals());
      additionalLodgingSystems.push(additionalLodging);
      break;
    default:
      break;
    }

    // Reset the selection next tick
    Vue.nextTick(() -> {
      selectedAdditionalType = null;
    });
  }

  @JsMethod
  public void deleteAdditional(final int index) {
    removeAtIndex(index, standardFarmLodging.getAdditionalLodgingSystems(), "additional-");
  }

  @JsMethod
  public void deleteReductive(final int index) {
    removeAtIndex(index, standardFarmLodging.getReductiveLodgingSystems(), "reductive-");
  }

  @JsMethod
  public void deleteFodder(final int index) {
    removeAtIndex(index, standardFarmLodging.getFodderMeasures(), "fodder-");
  }

  private void removeAtIndex(final Number index, final JsArray<?> dataSource, final String prefix) {
    if (index.intValue() == dataSource.length - 1) {
      dataSource.pop();
    } else {
      dataSource.splice(index.intValue(), 1);
    }

    pruneChildProblems(prefix, dataSource.length);
  }

  private void pruneChildProblems(final String prefix, final int length) {
    pruneChildErrors(prefix, length);
    pruneChildWarnings(prefix, length);
  }

  private void pruneChildErrors(final String prefix, final int length) {
    if (removeChildError(prefix + length)) {
      pruneChildErrors(prefix, length + 1);
    }
  }

  private void pruneChildWarnings(final String prefix, final int length) {
    if (removeChildWarning(prefix + length)) {
      pruneChildWarnings(prefix, length + 1);
    }
  }
}
