/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.ui.pages.calculationpoints;

import javax.inject.Inject;

import com.google.web.bindery.event.shared.binder.EventHandler;
import com.google.web.bindery.event.shared.binder.GenericEvent;

import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointEditCancelCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointSaveCommand;
import nl.overheid.aerius.wui.application.components.calculationpoint.CalculationPointHabitatDetailViewContainerFactory;
import nl.overheid.aerius.wui.application.components.map.MapComponentFactory;
import nl.overheid.aerius.wui.application.context.CalculationPointEditorContext;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.ui.main.MainView.FlexView;
import nl.overheid.aerius.wui.application.ui.pages.ThemeDelegatedActivity;
import nl.overheid.aerius.wui.application.ui.pages.ThemeView;

public class CalculationPointsActivity extends BasicEventComponent implements ThemeDelegatedActivity, CalculationPointsPresenter {
  private boolean userCancelled;

  @Inject CalculationPointEditorContext context;

  @Override
  public void onStart(final ThemeView view) {
    view.setDelegatedPresenter(this);
    view.setLeftView(CalculationPointsViewFactory.get(), FlexView.RIGHT);
    view.setSoftMiddle(false);
    view.setMiddleView(CalculationPointHabitatDetailViewContainerFactory.get());
    view.setRightView(MapComponentFactory.get());
  }

  @EventHandler(handles = {CalculationPointSaveCommand.class, CalculationPointEditCancelCommand.class})
  public void onEmissionSourceEnd(final GenericEvent c) {
    userCancelled = true;
  }

  @Override
  public String mayStop() {
    final boolean cancelled = userCancelled;
    userCancelled = false;
    return cancelled || !context.isEditing() ? null : M.messages().calculationPointsMayStop();
  }

  @Override
  public void onStop() {
    if (!context.isEditing()) {
      return;
    }

    final CalculationPointEditCancelCommand cmd = new CalculationPointEditCancelCommand();
    cmd.silence();
    eventBus.fireEvent(cmd);
  }

}
