/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.print.own2000.source.farmanimalhousing;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.AnimalType;
import nl.overheid.aerius.wui.application.components.source.farmanimalhousing.custom.FarmAnimalHousingDetailCustomComponent;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmAnimalHousing;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.application.resources.util.FarmIconUtil;

@Component(
  name = "farm-animal-housing-details-custom-print"
)
public class FarmAnimalHousingDetailsCustomPrintComponent extends FarmAnimalHousingDetailCustomComponent {

  @Computed
  public String getIcon() {
    return FarmIconUtil.getFarmAnimalHousingIcon(getCategories(), source);
  }

  @Computed
  public AnimalType getAnimalType() {
    return FarmIconUtil.getFarmAnimalType(getCategories(), source);
  }

  @JsMethod
  public String getFormattedEmission(final FarmAnimalHousing animalHousing, final Substance substance) {
    return MessageFormatter.formatEmissionWithUnitSmart(animalHousing.getEmission(substance));
  }
}
