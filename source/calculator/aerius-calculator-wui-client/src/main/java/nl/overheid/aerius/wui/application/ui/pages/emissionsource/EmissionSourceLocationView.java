/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.emissionsource;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.google.web.bindery.event.shared.EventBus;

import ol.geom.Geometry;
import ol.geom.GeometryType;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceFinishDrawGeometryCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourcePersistGeometryCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceUpdateSourceTypeCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.InputWarningComponent;
import nl.overheid.aerius.wui.application.components.input.MinimalInputComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.components.tooltip.TooltipComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.daemon.misc.PersistablePreferencesContext;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceDrawGeometryCommand;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceModifyGeometryCommand;
import nl.overheid.aerius.wui.application.geo.util.GeoType;
import nl.overheid.aerius.wui.application.geo.util.GeoUtil;
import nl.overheid.aerius.wui.application.util.WKTUtil;
import nl.overheid.aerius.wui.application.util.WKTUtil.GeometryResult;

/**
 * Component to handle editing the location of a emission source.
 * <p>
 * This component manages the interaction on the map through events.
 * <p>
 * TODO Refactor entirely and model after BuildingLocationEditorComponent instead.
 */
@Component(components = {
    ButtonIcon.class,
    MinimalInputComponent.class,
    TooltipComponent.class,
    VerticalCollapse.class,
    ValidationBehaviour.class,
    InputWarningComponent.class,
    SmartWktInputComponent.class,
    InputErrorComponent.class,
})
public class EmissionSourceLocationView extends ErrorWarningValidator {
  @Inject @Data ApplicationContext appContext;
  @Inject @Data PersistablePreferencesContext prefContext;

  @Prop EventBus eventBus;

  @Prop EmissionSourceFeature source;
  @Prop boolean open;

  @Data GeometryType geometryType = GeometryType.Point;

  @Data String locationString;

  @Data Geometry drawingGeometry;

  @PropDefault("open")
  boolean openDefault() {
    return true;
  }

  @Computed
  public Theme getTheme() {
    return appContext.getTheme();
  }

  @Computed("isValidateConvexGeometries")
  public boolean isValidateConvexGeometries() {
    return appContext.isAppFlagEnabled(AppFlag.VALIDATE_CONVEX_GEOMETRIES);
  }

  @JsMethod
  public String getGeometryTypeText() {
    return Optional.ofNullable(geometryType)
        .map(GeometryType::name)
        .map(v -> i18n.esGeometryType(appContext.getTheme(), v))
        .orElse("");
  }

  @Watch(value = "source", isImmediate = true)
  void onSourceChange(final EmissionSourceFeature neww) {
    observeGeometry(source.getGeometry());
  }

  @Watch(value = "open", isImmediate = true)
  public void onOpenChange(final boolean open) {
    if (open) {
      eventBus.fireEvent(new EmissionSourceModifyGeometryCommand(source, this::observeGeometry));
      eventBus.fireEvent(new EmissionSourceDrawGeometryCommand(geometryType, this::startDraw, this::finishDraw));
    } else {
      eventBus.fireEvent(new EmissionSourceDrawGeometryCommand());
      eventBus.fireEvent(new EmissionSourceModifyGeometryCommand());
    }
  }

  private void startDraw(final Geometry v) {
    drawingGeometry = v;
  }

  private void finishDraw(final Geometry v) {
    drawingGeometry = null;
    // Initialize the modify interaction
    eventBus.fireEvent(new EmissionSourceFinishDrawGeometryCommand());
    eventBus.fireEvent(new EmissionSourceModifyGeometryCommand(source, this::observeGeometry));
    eventBus.fireEvent(new EmissionSourceUpdateSourceTypeCommand());
  }

  @Computed
  public List<GeoType> getValidGeometryTypes() {
    return requiresLineGeometry(source.getEmissionSourceType()) ? Arrays.asList(GeoType.LINE_STRING) : Arrays.asList(GeoType.values());
  }

  @Computed("isIllegalGeometryType")
  public boolean isIllegalGeometryType() {
    return source != null && requiresLineGeometry(source.getEmissionSourceType()) && geometryType != GeometryType.LineString;
  }

  private boolean requiresLineGeometry(final EmissionSourceType est) {
    return est == EmissionSourceType.SRM2_ROAD
        || est == EmissionSourceType.ADMS_ROAD
        || est == EmissionSourceType.SHIPPING_MARITIME_MARITIME
        || est == EmissionSourceType.SHIPPING_INLAND
        || est == EmissionSourceType.SHIPPING_MARITIME_INLAND;
  }

  private boolean requiresAreaGeometry(final EmissionSourceType est) {
    return est == EmissionSourceType.COLD_START;
  }

  @JsMethod
  public void drawSource(final GeometryType type) {
    geometryType = type;
    eventBus.fireEvent(new EmissionSourceDrawGeometryCommand(type, this::startDraw, this::finishDraw));
  }

  @JsMethod
  public boolean isSourceType(final GeometryType type) {
    return geometryType == type;
  }

  @Computed
  public Geometry getGeometry() {
    return Optional.ofNullable(drawingGeometry)
        .orElse(source.getGeometry());
  }

  @Computed
  public String getGeometryStatistic() {
    return Optional.ofNullable(locationString)
        .map(v -> WKTUtil.tryReadGeometry(v, appContext.getConfiguration().getConvertibleGeometrySystems(),
            prefContext.getSelectedProjection(), appContext.getTheme(), geometryType))
        .map(GeometryResult::getGeometry)
        .map(GeoUtil::getGeometryStatistic)
        .orElse("");
  }

  @JsMethod
  public void updateWkt(final String wkt) {
    locationString = wkt;
  }

  @JsMethod
  public void saveGeometry(final Geometry geometry) {
    geometryType = GeometryType.valueOf(geometry.getType());
    eventBus.fireEvent(new EmissionSourcePersistGeometryCommand(geometry, source));
  }

  private void observeGeometry(final Geometry geometry) {
    drawSource(Optional.ofNullable(geometry)
        .map(v -> GeometryType.valueOf(geometry.getType()))
        .orElseGet(() -> findAppropriateGeometryType(source.getEmissionSourceType())));
    eventBus.fireEvent(new EmissionSourceUpdateSourceTypeCommand());
  }

  private GeometryType findAppropriateGeometryType(final EmissionSourceType type) {
    if (requiresLineGeometry(type)) {
      return GeometryType.LineString;
    } else if (requiresAreaGeometry(type)) {
      return GeometryType.Polygon;
    } else {
      return GeometryType.Point;
    }
  }
}
