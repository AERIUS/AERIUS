/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import javax.inject.Inject;

import com.google.inject.Singleton;

import nl.aerius.search.wui.context.SearchContext;

@Singleton
public class SearchPanelContext extends SimplePanelContext {
  @Inject SearchContext searchContext;

  @Override
  public void setPanelShowing(final boolean showing) {
    super.setPanelShowing(showing);
    searchContext.setSearchShowing(isShowing());
  }

  @Override
  public boolean togglePanelShowing(final boolean attached) {
    final boolean ret = super.togglePanelShowing(attached);
    searchContext.setSearchShowing(isShowing());
    return ret;
  }

}
