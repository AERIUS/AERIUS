/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers;

import ol.Feature;

import nl.overheid.aerius.shared.util.ColorUtil;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.geo.icons.labels.LabelIconStyle;
import nl.overheid.aerius.wui.application.util.SectorColorUtilDeprecated;

/**
 * Contains the styles for the different labels on the map (emission sources and buildings)
 */
public enum LabelStyle {
  BUILDING(LabelStyle.BUILDING_LABEL_TEXT_COLOR, LabelStyle.BUILDING_LABEL_BACKGROUND_COLOR, LabelIconStyle.BUILDING),

  EMISSION_SOURCE(LabelStyle.EMISSION_SOURCE_TEXT_COLOR, LabelStyle.NO_SECTOR_SELECTED_COLOR, LabelIconStyle.EMISSION_SOURCE) {
    @Override
    public String getLabelBackgroundColor(final Feature feature) {
      return ((EmissionSourceFeature) feature).getSectorId() == 0
          ? super.getLabelBackgroundColor(feature)
          : ColorUtil.webColor(SectorColorUtilDeprecated.getSectorIcon(((EmissionSourceFeature) feature).getSectorId()).getColor());
    }
  },
  CALCULATION_POINT(LabelStyle.MARKER_TEXT_COLOR, LabelStyle.CALCULATION_POINT_BACKGROUND_COLOR, LabelIconStyle.CALCULATION_POINT),

  MET_SITE(LabelStyle.MET_SITE_LABEL_TEXT_COLOR, LabelStyle.MET_SITE_LABEL_BACKGROUND_COLOR, LabelIconStyle.MET_SITE),

  ARCHIVE_PROJECT(LabelStyle.ARCHIVE_PROJECT_TEXT_COLOR, LabelStyle.ARCHIVE_PROJECT_BACKGROUND_COLOR, LabelIconStyle.ARCHIVE_PROJECT);

  private static final String BUILDING_LABEL_BACKGROUND_COLOR = "#FFFFFF";
  private static final String BUILDING_LABEL_TEXT_COLOR = "#222222";
  private static final String MET_SITE_LABEL_BACKGROUND_COLOR = "#555";
  private static final String MET_SITE_LABEL_TEXT_COLOR = "#ddd";
  private static final String NO_SECTOR_SELECTED_COLOR = "#699DCD";
  private static final String EMISSION_SOURCE_TEXT_COLOR = "#FFFFFF";
  private static final String CALCULATION_POINT_BACKGROUND_COLOR = "#ffdd00";
  private static final String ARCHIVE_PROJECT_BACKGROUND_COLOR = "#f15b29";
  private static final String ARCHIVE_PROJECT_TEXT_COLOR = "#ffffff";
  private static final String MARKER_TEXT_COLOR = "black";

  private final String labelTextColor;
  private final String labelBackgroundColor;
  private final LabelIconStyle labelIconStyle;

  private LabelStyle(final String labelTextColor, final String labelBackgroundColor, final LabelIconStyle labelIconStyle) {
    this.labelTextColor = labelTextColor;
    this.labelBackgroundColor = labelBackgroundColor;
    this.labelIconStyle = labelIconStyle;
  }

  /**
   * @return Color of the text
   */
  public String getLabelTextColor() {
    return labelTextColor;
  }

  /**
   * Returns background color to show. If a color is in the LabelFeature passed use that color.
   *
   * @param feature feature to get the background color from
   * @return background color
   */
  public String getLabelBackgroundColor(final Feature feature) {
    // Note: because javascript has no typing we can safely cast to LabelFeature here,
    // the implementation of LabelFeature will just check a value from a map, a map which is present in all feature
    // objects.
    final String color = ((LabelFeature) feature).getColor();

    return color.isEmpty() ? labelBackgroundColor : color;
  }

  public LabelIconStyle getLabelIconStyle() {
    return labelIconStyle;
  }
}
