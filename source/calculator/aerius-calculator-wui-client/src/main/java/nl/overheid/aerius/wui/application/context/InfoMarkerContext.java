/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Singleton;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.wui.application.domain.info.JobReceptorInfo;
import nl.overheid.aerius.wui.application.domain.info.StaticReceptorInfo;

@Singleton
public class InfoMarkerContext {
  private boolean loading = false;
  private Integer receptor = null;
  private StaticReceptorInfo staticReceptorInfo = null;
  private @JsProperty Map<String, JobReceptorInfo> jobReceptorInfos = new HashMap<>();

  public boolean isLoading() {
    return loading;
  }

  public void setLoading(final boolean loading) {
    this.loading = loading;
  }

  public boolean hasSelection() {
    return receptor != null;
  }

  public int getReceptor() {
    return receptor;
  }

  public void setReceptor(final Integer id) {
    this.receptor = id;
  }

  public boolean isSelected(final Integer value) {
    return receptor != null && receptor.equals(value);
  }

  public boolean hasStaticReceptorInfo() {
    return staticReceptorInfo != null;
  }

  public StaticReceptorInfo getStaticReceptorInfo() {
    return staticReceptorInfo;
  }

  public void setStaticReceptorInfo(final StaticReceptorInfo receptorInfo) {
    this.staticReceptorInfo = receptorInfo;
  }

  public boolean hasJobReceptorInfo(String jobKey) {
    return jobReceptorInfos.containsKey(jobKey);
  }

  public JobReceptorInfo getJobReceptorInfo(String jobKey) {
    return jobReceptorInfos.get(jobKey);
  }

  public void updateJobReceptorInfo(String jobKey, JobReceptorInfo jobReceptorInfo) {
    this.jobReceptorInfos.put(jobKey, jobReceptorInfo);
  }

  public Map<String, JobReceptorInfo> getJobReceptorInfos() {
    return jobReceptorInfos;
  }

  public void clearJobReceptorInfo() {
    this.jobReceptorInfos.clear();
  }
}
