/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.results.critical;

import static elemental2.core.Global.Infinity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.web.bindery.event.shared.EventBus;

import elemental2.core.Global;
import elemental2.dom.CustomEvent;

import jsinterop.annotations.JsMethod;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.shared.ImaerConstants;
import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.domain.legend.ColorRange;
import nl.overheid.aerius.shared.domain.legend.ColorRangeType;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.wui.application.command.geo.ClearAssessmentAreaCommand;
import nl.overheid.aerius.wui.application.command.geo.SelectAssessmentAreaCommand;
import nl.overheid.aerius.wui.application.command.result.DeselectResultGraphRangeCommand;
import nl.overheid.aerius.wui.application.command.result.SelectResultGraphRangeCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.result.ResultSelectionContext;
import nl.overheid.aerius.wui.application.context.result.ResultSummaryContext;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsAreaSummary;
import nl.overheid.aerius.wui.application.geo.util.GeoUtil;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.ui.pages.results.GeneralizedSurfaceType;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    CollapsiblePanel.class,
    ButtonIcon.class,
    VerticalCollapse.class,
    VerticalCollapseGroup.class
})
public class ResultPercentageCriticalLoadView extends BasicVueComponent {
  private static final Map<ScenarioResultType, ColorRangeType> SUPPORTED_RESULT_TYPES = new HashMap<>();
  static {
    SUPPORTED_RESULT_TYPES.put(ScenarioResultType.PROJECT_CALCULATION, ColorRangeType.PROJECT_CALCULATION_PERCENTAGE_CRITICAL_LOAD);
    SUPPORTED_RESULT_TYPES.put(ScenarioResultType.IN_COMBINATION, ColorRangeType.IN_COMBINATION_PERCENTAGE_CRITICAL_LOAD);
    SUPPORTED_RESULT_TYPES.put(ScenarioResultType.ARCHIVE_CONTRIBUTION, ColorRangeType.ARCHIVE_CONTRIBUTION_PERCENTAGE_CRITICAL_LOAD);
  }

  @Prop EventBus eventBus;
  @Prop(required = true) ResultSummaryContext context;

  @Data @Inject ResultSelectionContext selectionContext;
  @Data @Inject ApplicationContext applicationContext;

  @Computed
  public GeneralizedSurfaceType getSurfaceType() {
    return context.getGeneralizedSurfaceType();
  }

  @JsMethod
  public String getResultsPercentageCLTitle() {
    if (context.getEmissionResultKey().getEmissionResultType() == EmissionResultType.DEPOSITION) {
      return M.messages().resultsPercentageCriticalLoadTitle();
    } else {
      return M.messages().resultsPercentageCriticalLevelTitle();
    }
  }

  @JsMethod
  public void openPanel(final SituationResultsAreaSummary summary) {
    final int id = summary.getAssessmentArea().getId();
    eventBus.fireEvent(new SelectAssessmentAreaCommand(id));
  }

  @Computed
  public String getLocale() {
    return LocaleInfo.getCurrentLocale().getLocaleName();
  }

  @Computed("isPercentageCriticalLoadSupported")
  public boolean isPercentageCriticalLoadSupported() {
    return SUPPORTED_RESULT_TYPES.containsKey(context.getResultType());
  }

  @Computed(value = "yAxisFactor")
  public int getYAxisFactor() {
    return getSurfaceType() == GeneralizedSurfaceType.SURFACE ? (int) Math.round(ImaerConstants.M2_TO_HA) : 1;
  }

  @JsMethod
  public void closePanel() {
    eventBus.fireEvent(new ClearAssessmentAreaCommand());
  }

  @JsMethod
  public boolean isOpen(final int id) {
    return id == selectionContext.getSelectedAssessmentArea();
  }

  @JsMethod
  public String getTitle(final SituationResultsAreaSummary d) {
    return d.getAssessmentArea().getTitle(applicationContext.isAppFlagEnabled(AppFlag.SHOW_ASSESSMENT_AREA_DIRECTIVES));
  }

  @JsMethod
  public boolean hasDistributionData(final SituationResultsAreaSummary d) {
    return d.getPercentageCriticalLoadResults().length > 0;
  }

  @JsMethod
  public String getDistributionData(final SituationResultsAreaSummary d) {
    return Global.JSON.stringify(d.getPercentageCriticalLoadResults());
  }

  @JsMethod
  public String getColorRangeDefinition() {
    final List<ColorRange> colorRange = applicationContext
        .getConfiguration()
        .getColorRange(SUPPORTED_RESULT_TYPES.get(context.getResultType()));
    final Object[] jsonified = colorRange.stream()
        .map(x -> JsPropertyMap.of("lowerBound", x.getLowerValue(), "color", x.getColor()))
        .toArray();
    return Global.JSON.stringify(jsonified, (key, value) -> {
      if (value.equals(-Infinity)) {
        return "-Infinity";
      }
      return value.equals(Infinity) ? "Infinity" : value;
    });
  }

  @JsMethod
  public void selectBar(final CustomEvent input) {
    eventBus.fireEvent(new SelectResultGraphRangeCommand(Js.uncheckedCast(input.detail)));
  }

  @JsMethod
  public void deselectBar(final CustomEvent input) {
    eventBus.fireEvent(new DeselectResultGraphRangeCommand(Js.uncheckedCast(input.detail)));
  }

  @JsMethod
  public void zoomToArea(final SituationResultsAreaSummary areaSummary) {
    eventBus.fireEvent(GeoUtil.createMapSetExtendCommand(areaSummary.getAssessmentArea().getBounds()));
  }

}
