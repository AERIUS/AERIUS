/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source.road;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import ol.OLFactory;
import ol.style.Style;

import nl.aerius.geo.domain.legend.ColorLabelsLegend;
import nl.aerius.geo.domain.legend.LegendType;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.legend.ColorItem;
import nl.overheid.aerius.shared.domain.legend.ColorItemType;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.sector.category.SimpleCategory;
import nl.overheid.aerius.wui.application.domain.source.RoadESFeature;
import nl.overheid.aerius.wui.application.geo.layers.source.RoadSourceGeometryLayer;
import nl.overheid.aerius.wui.application.i18n.M;

public class RoadTypeStyle implements RoadStyle {

  private static final String UNKNOWN_COLOR = "#707070";
  private static final List<Style> UNKNOWN_STYLE = Collections.singletonList(
      OLFactory.createStyle(OLFactory.createStroke(OLFactory.createColor(UNKNOWN_COLOR), RoadSourceGeometryLayer.DEFAULT_ROAD_WIDTH)));
  private final Map<String, List<Style>> styleMap;
  private final ColorLabelsLegend legend;

  public RoadTypeStyle(final Function<ColorItemType, List<ColorItem>> colorRangeFunction, final SectorCategories sectorCategories) {
    final List<ColorItem> colorItems = colorRangeFunction.apply(ColorItemType.ROAD_TYPE);
    this.styleMap = colorItems.stream()
        .collect(Collectors.toMap(
            ColorItem::getItemValue,
            cr -> Collections.singletonList(
                OLFactory.createStyle(OLFactory.createStroke(OLFactory.createColor(cr.getColor()), RoadSourceGeometryLayer.DEFAULT_ROAD_WIDTH)))));

    final List<String> labels = colorItems.stream()
        .map(ColorItem::getItemValue)
        .map(itemValue -> {
          final SimpleCategory category = sectorCategories.getRoadEmissionCategories().getRoadType(itemValue);
          if (category == null) {
            return M.messages().esRoadCategoryUnknown();
          } else {
            return category.getName();
          }
        }).collect(Collectors.toList());
    labels.add(M.messages().esRoadCategoryUnknown());

    final List<String> colors = colorItems.stream()
        .map(cr -> cr.getColor())
        .collect(Collectors.toList());
    colors.add(UNKNOWN_COLOR);

    this.legend = new ColorLabelsLegend(labels.toArray(new String[labels.size()]), colors.toArray(new String[colors.size()]), LegendType.LINE);
  }

  @Override
  public List<Style> getStyle(final RoadESFeature feature, final String vehicleTypeCode, final double resolution) {
    final String roadType = feature.getRoadTypeCode();
    if (roadType == null || !styleMap.containsKey(roadType)) {
      return UNKNOWN_STYLE;
    }

    return styleMap.get(roadType);
  }

  @Override
  public ColorLabelsLegend getLegend(final ApplicationConfiguration appThemeConfiguration) {
    return legend;
  }

}
