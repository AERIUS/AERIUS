/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.building;

import java.util.Collections;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.wui.application.command.building.TemporaryBuildingAddCommand;
import nl.overheid.aerius.wui.application.command.building.TemporaryBuildingClearCommand;
import nl.overheid.aerius.wui.application.context.MapConfigurationContext;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.event.situation.SituationSwitchEvent;
import nl.overheid.aerius.wui.application.geo.layers.LabelStyle;
import nl.overheid.aerius.wui.application.geo.layers.base.BaseMarkerLayer;
import nl.overheid.aerius.wui.application.geo.layers.base.LabelStyleCreator;
import nl.overheid.aerius.wui.application.geo.layers.base.VectorFeaturesWrapper;

/**
 * Layer to show the marker of the {@link BuildingFeature} on the map that is being selected.
 */
public class SelectedBuildingMarkerLayer extends BaseMarkerLayer<BuildingFeature> {

  interface SelectedBuildingMarkerLayerEventBinder extends EventBinder<SelectedBuildingMarkerLayer> {}

  private static final SelectedBuildingMarkerLayerEventBinder EVENT_BINDER = GWT.create(SelectedBuildingMarkerLayerEventBinder.class);

  private final VectorFeaturesWrapper vectorObject = new VectorFeaturesWrapper();

  @Inject
  public SelectedBuildingMarkerLayer(@Assisted final LayerInfo info, final EventBus eventBus, @Assisted final int zIndex,
      final MapConfigurationContext mapConfigurationContext) {
    super(info, zIndex, new LabelStyleCreator<BuildingFeature>(mapConfigurationContext::getIconScale, true, LabelStyle.BUILDING));
    setFeatures(vectorObject, Collections.emptyList());
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  public void onTemporaryBuildingAddCommand(final TemporaryBuildingAddCommand c) {
    vectorObject.addFeature(c.getValue());
  }

  @EventHandler
  public void onTemporaryBuildingClearCommand(final TemporaryBuildingClearCommand c) {
    vectorObject.removeFeature(c.getValue());
  }

  @EventHandler
  public void onSituationSwitch(final SituationSwitchEvent e) {
    vectorObject.clear();
    refreshLayer();
  }
}
