/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.util;

import elemental2.dom.DomGlobal;
import elemental2.dom.File;

public final class FileDownloadUtil {

  private FileDownloadUtil() {
    // Util class
  }

  /**
   * Open url in new window.
   *
   * @param url url to open
   */
  public static void wget(final String url) {
    DomGlobal.window.open(url, "_blank");
  }

  /**
   * Returns the extension of the file or an empty string.
   *
   * @param file file to get extension of
   * @return extension of file or empty string
   */
  public static String getFileExtension(final File file) {
    if (file == null || file.name == null) {
      return "";
    }
    final int lastIndexOfDot = file.name.lastIndexOf(".");

    return lastIndexOfDot < 0 ? "" : file.name.substring(lastIndexOfDot + 1, file.name.length());
  }
}
