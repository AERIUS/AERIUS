/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.farmanimalhousing.standard;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.FarmAdditionalHousingSystemCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalHousingCategories;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalHousingCategory;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;
import nl.overheid.aerius.wui.application.components.source.farmanimalhousing.detail.FarmAnimalHousingDetailUtil;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.farm.AdditionalHousingSystem;
import nl.overheid.aerius.wui.application.domain.source.farm.AdditionalSystemType;
import nl.overheid.aerius.wui.application.domain.source.farm.CustomAdditionalHousingSystem;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmAnimalHousingStandardUtil;
import nl.overheid.aerius.wui.application.domain.source.farm.StandardAdditionalHousingSystem;
import nl.overheid.aerius.wui.application.domain.source.farm.StandardFarmAnimalHousing;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

/**
 * Component to show the details of a StandardFarmAnimalHousing sub source.
 */
@Component(name = "aer-farmanimalhousing-detail-standard")
public class FarmAnimalHousingDetailStandardComponent extends BasicVueComponent {

  private static final int EMISSION_FACTOR_PRECISION = 3;

  @Prop @JsProperty protected StandardFarmAnimalHousing source;
  @Prop boolean hasReductionFactorColumn;
  @Prop boolean hasNumberOfDaysColumn;
  @Data int index;

  @Inject @Data ApplicationContext applicationContext;

  @JsMethod
  protected FarmAnimalHousingCategories getCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getFarmAnimalHousingCategories();
  }

  @Computed
  protected String getFarmAnimalHousingEmissionFactor() {
    final double emissionFactor = FarmAnimalHousingStandardUtil.getEmissionFactor(getCategories(), source, Substance.NH3);
    final FarmEmissionFactorType emissionFactorType = FarmAnimalHousingStandardUtil.getEmissionFactorType(getCategories(), source);
    final String emissionFactorValue = MessageFormatter.toCapped(emissionFactor, EMISSION_FACTOR_PRECISION);

    if (emissionFactor > 0) {
      return emissionFactorValue + (hasAdditionalHousingSystemCategories() ? "" : (" " + i18n.esFarmEmissionFactorType(emissionFactorType)));
    } else {
      return "-";
    }
  }

  @Computed
  protected String emission() {
    return emission2String(FarmAnimalHousingStandardUtil.getFlatEmission(getCategories(), source, Substance.NH3));
  }

  @Computed("constrained")
  protected boolean isConstrained() {
    return FarmAnimalHousingStandardUtil.isContrained(getCategories(), source);
  }

  @Computed
  protected String getFarmAnimalHousingUnconstrainedEmissionFactor() {
    return MessageFormatter.toCapped(FarmAnimalHousingStandardUtil.getUnconstrainedEmissionFactor(getCategories(), source, Substance.NH3),
        EMISSION_FACTOR_PRECISION);
  }

  @Computed
  protected String unconstrainedEmission() {
    return emission2String(FarmAnimalHousingStandardUtil.getUnconstrainedFlatEmission(getCategories(), source, Substance.NH3));
  }

  @Computed
  protected String getFarmAnimalHousingNumberOfDays() {
    final FarmEmissionFactorType emissionFactorType = FarmAnimalHousingStandardUtil.getEmissionFactorType(getCategories(), source);
    return emissionFactorType == FarmEmissionFactorType.PER_ANIMAL_PER_DAY ? Integer.toString(source.getNumberOfDays()) : "-";
  }

  @JsMethod
  protected boolean hasNumberOfDays() {
    return FarmAnimalHousingStandardUtil.getEmissionFactorType(getCategories(), source) == FarmEmissionFactorType.PER_ANIMAL_PER_DAY;
  }

  @JsMethod
  protected boolean hasAdditionalHousingSystemCategories() {
    return FarmAnimalHousingStandardUtil.hasAdditionalHousingSystemCategories(getCategories());
  }

  @Computed
  protected int columnsWithoutEmission() {
    return FarmAnimalHousingDetailUtil.columnsWithoutEmission(hasReductionFactorColumn, hasNumberOfDaysColumn);
  }

  @JsMethod
  protected String additionalName(final AdditionalHousingSystem additionalHousingSystem) {
    return AdditionalSystemType.STANDARD == additionalHousingSystem.getAdditionalSystemType()
        ? ((StandardAdditionalHousingSystem) additionalHousingSystem).getAdditionalSystemCode()
        : i18n.esFarmAnimalHousingCustom();
  }

  @JsMethod
  protected String additionalNameExtended(final AdditionalHousingSystem additionalHousingSystem) {
    final AdditionalSystemType type = additionalHousingSystem.getAdditionalSystemType();
    if (AdditionalSystemType.STANDARD == type) {
      final FarmAdditionalHousingSystemCategory category = getCategories()
          .determineAdditionalHousingCategoryByCode(((StandardAdditionalHousingSystem) additionalHousingSystem).getAdditionalSystemCode());

      return category.getName() + " - " + category.getDescription();
    } else if (AdditionalSystemType.CUSTOM == type) {
      final CustomAdditionalHousingSystem custom = (CustomAdditionalHousingSystem) additionalHousingSystem;
      final String prefix = custom.isAirScrubber()
          ? i18n.esFarmAnimalHousingStandardAdditionalCustomAirScrubber()
          : i18n.esFarmAnimalHousingStandardAdditionalCustomOther();

      return prefix + " - " + custom.getDescription();
    } else {
      return "";
    }
  }

  @JsMethod
  protected String additionalReductionPercentage(final AdditionalHousingSystem additionalHousingSystem) {
    final double reductionFactor = FarmAnimalHousingStandardUtil.getAdditionalHousingFactor(getCategories(), source, Substance.NH3,
        additionalHousingSystem);

    return MessageFormatter.formatPercentageWithUnit(reductionFactor * 100, 0);
  }

  @JsMethod
  protected String additionalEmission(final AdditionalHousingSystem additionalHousingSystem) {
    return emission2String(FarmAnimalHousingStandardUtil.getEmissionsAdditional(getCategories(), source, Substance.NH3, additionalHousingSystem));
  }

  @Computed
  protected String detail() {
    final FarmAnimalHousingCategory category = getCategories().determineHousingCategoryByCode(source.getAnimalHousingCode());

    return category == null ? "" : category.getDescription();
  }

  private String emission2String(final double emission) {
    return MessageFormatter.formatEmissionWithUnitSmart(emission);
  }
}
