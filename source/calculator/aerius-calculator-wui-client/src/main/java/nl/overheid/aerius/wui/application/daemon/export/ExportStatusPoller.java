/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.export;

import javax.inject.Inject;

import com.google.web.bindery.event.shared.EventBus;

import nl.aerius.wui.util.NotificationUtil;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.wui.application.daemon.calculation.AbstractStatusPoller;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationInfo;
import nl.overheid.aerius.wui.application.domain.calculation.JobProgress;
import nl.overheid.aerius.wui.application.domain.export.HistoryRecord;
import nl.overheid.aerius.wui.application.event.ExportCalculationCompleteEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.service.AeriusRequestCallback;
import nl.overheid.aerius.wui.application.service.ExportServiceAsync;

public class ExportStatusPoller extends AbstractStatusPoller<HistoryRecord> {
  private static final int INTERVAL = 10_000;
  private static final int RETRY_AMOUNT = 6 * 15; // 15 minutes

  @Inject private ExportServiceAsync service;
  @Inject private EventBus eventBus;

  public ExportStatusPoller() {
    super(INTERVAL, RETRY_AMOUNT);
  }

  @Override
  public void start(final HistoryRecord item) {
    item.setJobProgress(null);
    super.start(item);
  }

  @Override
  protected void doFetchStatus(final HistoryRecord item) {
    service.retrieveExportStatus(item.getJobKey(), AeriusRequestCallback.create(v -> doStatusSuccess(v, item), e -> fail(item, e)));
  }

  private void doStatusSuccess(final CalculationInfo v, final HistoryRecord item) {
    succeedCall(item);
    final JobProgress jobProgress = v.getJobProgress();

    item.setJobProgress(jobProgress);
    if (jobProgress.getState().isFinalState()) {
      complete(v, item);
    }

    tryFetchDelayed();
  }

  private void complete(final CalculationInfo info, final HistoryRecord item) {
    item.setJobProgress(info.getJobProgress());

    eventBus.fireEvent(new ExportCalculationCompleteEvent(item.getJobKey(), info));
    finish(item);
  }

  @Override
  protected void finalFailure(final HistoryRecord item, final Throwable e) {
    if (item.getJobProgress() == null) {
      item.setJobProgress(new JobProgress());
    }
    item.getJobProgress().setState(JobState.ERROR);
    item.setErrorMessage(M.getErrorMessage(e));
    NotificationUtil.broadcastError(eventBus, e);
    finish(item);
  }
}
