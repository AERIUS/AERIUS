/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import java.util.HashMap;
import java.util.Map;

import com.google.inject.Singleton;

import jsinterop.annotations.JsProperty;

@Singleton
public class ManualContext {
  private String activeUrl = null;

  private final @JsProperty Map<String, String> content = new HashMap<>();

  private Throwable exception = null;

  public void addContentAndActivate(final String url, final String html) {
    addContent(url, html);
    setActiveUrl(url);
  }

  public void addContent(final String url, final String html) {
    content.put(url, html);
  }

  public String getContentHtml(final String url) {
    return content.get(url);
  }

  public String getActiveContentHtml() {
    return getContentHtml(activeUrl);
  }

  public String getActiveUrl() {
    return activeUrl;
  }

  public void setActiveUrl(final String activeUrl) {
    this.activeUrl = activeUrl;
    this.exception = null;
  }

  public Throwable getException() {
    return exception;
  }

  public void setException(final Throwable exception) {
    this.exception = exception;
  }

  public boolean hasError() {
    return getException() != null;
  }
}
