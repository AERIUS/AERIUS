/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.print.own2000;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.ApplicationLimits;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.RoadESFeature;
import nl.overheid.aerius.wui.application.print.own2000.source.EmissionSourceOPSDetails;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(
    components = EmissionSourceOPSDetails.class
)
public class EmissionSources extends BasicVueComponent {
  public static final String ID = "emission-sources";

  @Inject @Data ScenarioContext scenarioContext;

  @Computed
  public List<SituationContext> getSituations() {
    return scenarioContext.getSituations();
  }

  @Computed
  public boolean isRender() {
    return getSituations().stream().anyMatch(situationContext -> !getEmissionSources(situationContext).isEmpty());
  }

  @JsMethod
  public boolean render(final SituationContext situationContext) {
    return !getEmissionSources(situationContext).isEmpty();
  }

  @JsMethod
  public boolean maxNumberOfSourcesExceeded(final SituationContext situationContext) {
    return situationContext.getSources().size() > ApplicationLimits.PRINT_EMISSION_SOURCE_LIMIT;
  }

  @Computed
  public String getMaxNumberOfSourcesExceededMessage() {
    return i18n.pdfEmissionSourcesLimitReachedMessage(String.valueOf(ApplicationLimits.PRINT_EMISSION_SOURCE_LIMIT),
        String.valueOf(ApplicationLimits.SCENARIO_INPUT_LIST_DRAW_LIMIT));
  }

  @JsMethod
  public List<EmissionSourceFeature> getEmissionSources(final SituationContext situationContext) {
    return situationContext.getSources()
        .stream().filter(this::isNotSRM1RoadSource)
        .collect(Collectors.toList());
  }

  @JsMethod
  public boolean maxNumberOfSRM2SourcesExceeded(final SituationContext situationContext) {
    final List<EmissionSourceFeature> srm2roadSourcesList = situationContext.getSources()
        .stream().filter(this::isSRM2RoadSource)
        .collect(Collectors.toList());
    return srm2roadSourcesList.size() > ApplicationLimits.PRINT_SRM2_ROAD_EMISSION_SOURCE_LIMIT;
  }

  @Computed
  public String getMaxNumberOfSRM2SourcesExceededMessage() {
    return i18n.pdfEmissionSourcesSRM2RoadLimitReachedMessage(String.valueOf(ApplicationLimits.PRINT_SRM2_ROAD_EMISSION_SOURCE_LIMIT),
        String.valueOf(ApplicationLimits.SCENARIO_INPUT_LIST_DRAW_LIMIT));
  }

  @JsMethod
  public boolean isNotSRM1RoadSource(final EmissionSourceFeature emissionSourceFeature) {
    return emissionSourceFeature.getEmissionSourceType() != EmissionSourceType.SRM1_ROAD;
  }

  @JsMethod
  public boolean isSRM2RoadSource(final EmissionSourceFeature emissionSourceFeature) {
    return emissionSourceFeature.getEmissionSourceType() == EmissionSourceType.SRM2_ROAD;
  }

}
