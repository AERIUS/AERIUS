/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.activity;

import javax.inject.Inject;

import nl.aerius.wui.activity.Activity;
import nl.aerius.wui.activity.ActivityMapper;
import nl.aerius.wui.place.Place;
import nl.aerius.wui.vue.AcceptsOneComponent;
import nl.overheid.aerius.shared.domain.PdfProductType;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.place.CalculatorDefaultPlace;
import nl.overheid.aerius.wui.application.place.HomePlace;
import nl.overheid.aerius.wui.application.place.MainThemePlace;
import nl.overheid.aerius.wui.application.place.PrintPlace;

public class ApplicationActivityMapper implements ActivityMapper<AcceptsOneComponent> {
  @Inject private ApplicationActivityFactory factory;
  @Inject private ApplicationContext appContext;

  @Override
  public Activity<?, AcceptsOneComponent> getActivity(final Place place) {
    final Activity<?, AcceptsOneComponent> presenter;

    if (place instanceof MainThemePlace) {
      presenter = tryGetThemeActivity((MainThemePlace) place);
    } else if (place instanceof PrintPlace) {
      presenter = tryGetPrintActivity((PrintPlace) place);
    } else {
      presenter = tryGetActivity(place);
    }

    if (presenter == null) {
      throw new RuntimeException("Presenter is null: Place ends up nowhere. " + place);
    }

    return presenter;
  }

  /**
   * @param place
   * @return
   */
  private Activity<?, AcceptsOneComponent> tryGetThemeActivity(final MainThemePlace place) {
    if (place.getTheme() == null) {
      return null;
    }
    return tryGetThemeActivity(place.getTheme(), place);
  }

  private Activity<?, AcceptsOneComponent> tryGetPrintActivity(final PrintPlace place) {
    if (place.getPdfProductType() == PdfProductType.NCA_CALCULATOR) {
      return factory.createPrintNcaActivity(place);
    } else {
      return factory.createPrintOwN2000Activity(place);
    }
  }

  private Activity<?, AcceptsOneComponent> tryGetActivity(final Place place) {
    if (place instanceof CalculatorDefaultPlace) {
      final Theme theme = appContext.getTheme();

      return tryGetThemeActivity(theme, new HomePlace(theme));
    } else {
      return null;
    }
  }

  private Activity<?, AcceptsOneComponent> tryGetThemeActivity(final Theme theme, final MainThemePlace place) {
    switch (theme) {
    case NCA:
      return factory.createNcaActivity(place);
    case OWN2000:
      return factory.createOwN2000Activity(place);
    default:
      return null;
    }
  }
}
