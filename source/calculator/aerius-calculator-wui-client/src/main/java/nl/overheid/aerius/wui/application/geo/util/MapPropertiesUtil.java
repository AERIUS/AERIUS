/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.util;

import nl.aerius.geo.shared.MapProperties;
import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.geo.EPSG;

/**
 * Util class to get the {@link MapProperties} for a SRID.
 */
public final class MapPropertiesUtil {

  // RDNEW map properties
  private static final String RDNEW_EPSG_CODE = EPSG.RDNEW.getEpsgCode();
  private static final BBox RDNEW_BOUNDS = new BBox(-285401.920, 22598.080, 595401.920, 903401.920);
  private static final double RDNEW_CENTER_X = 155000.0;
  private static final double RDNEW_CENTER_Y = 463000.0;
  private static final int RDNEW_MAX_ZOOMLEVEL = 16;
  private static final String RDNEW_TEXT_COORDINATES_PREFIX = "";

  private static final MapProperties RDNEW_MA_PPROP = new MapProperties(RDNEW_EPSG_CODE, RDNEW_BOUNDS, RDNEW_CENTER_X, RDNEW_CENTER_Y, RDNEW_MAX_ZOOMLEVEL, RDNEW_TEXT_COORDINATES_PREFIX);

  // BNG map properties
  private static final String BNG_EPSG_CODE = EPSG.BNG.getEpsgCode();
  private static final BBox BNG_BOUNDS = new BBox(-4000, 4000, 660000, 1222000);
  private static final double BNG_CENTER_X = 308188.48;
  private static final double BNG_CENTER_Y = 608846.16;
  private static final int BNG_MAX_ZOOMLEVEL = 15;
  private static final String BNG_TEXT_COORDINATES_PREFIX = "BNG: ";

  private static final MapProperties BNG_MAP_PROP = new MapProperties(BNG_EPSG_CODE, BNG_BOUNDS, BNG_CENTER_X, BNG_CENTER_Y, BNG_MAX_ZOOMLEVEL, BNG_TEXT_COORDINATES_PREFIX);

  /**
   * Returns the {@link MapProperties} for the given SRID or throws an exception in case sird not supported.
   *
   * @param epsg EPSG to get MapProps
   * @return MapProps for EPSG.
   */
  public static MapProperties getMapProps(final EPSG epsg) {
    switch (epsg) {
    case RDNEW:
      return RDNEW_MA_PPROP;
    case BNG:
      return BNG_MAP_PROP;
    default:
      throw new IllegalArgumentException("Unsupported epsg: " + epsg);
    }
  }
}
