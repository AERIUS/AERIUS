/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.results;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.OLFactory;
import ol.layer.Image;
import ol.layer.Layer;
import ol.source.ImageWms;
import ol.source.ImageWmsOptions;
import ol.source.ImageWmsParams;

import nl.aerius.geo.domain.IsLayer;
import nl.aerius.geo.domain.LayerInfo;
import nl.aerius.geo.domain.legend.LegendType;
import nl.overheid.aerius.js.calculation.ResultsGraphRange;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.legend.ColorRangeType;
import nl.overheid.aerius.shared.domain.legend.ColorRangesLegend;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.summary.OverlappingHexagonType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.application.event.CalculationStatusEvent;
import nl.overheid.aerius.wui.application.event.ResultsSelectedEvent;
import nl.overheid.aerius.wui.application.event.SituationResultsSelectedEvent;
import nl.overheid.aerius.wui.application.event.SwitchActiveCalculationEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.application.util.ResultDisplaySetUtil;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;

/**
 * Provides the deposition result layer
 */
public class CalculationResultLayer implements IsLayer<Layer> {
  private static final String WMS_BASE_LAYER_NAME = "calculator:wms_";

  private static final String SITUATION_RESULT = "situation_result";
  private static final String PROJECT_CALCULATION = "project_calculation";
  private static final String TEMPORARY_EFFECT = "temporary_effect";
  private static final String IN_COMBINATION = "in_combination";
  private static final String ARCHIVE_CONTRIBUTION = "archive_contribution";

  private static final String DEPOSITION_LAYER = "_deposition";
  private static final String SUB_RESULTS_LAYER = "_sub_results";
  private static final String RESULTS_LAYER = "_results";

  private static final String CQL_FILTER = "CQL_FILTER";
  private static final String DEPOSITION = "deposition";
  private static final String RESULT = "result";

  private static final CalculationResultLayerEventBinder EVENT_BINDER = GWT.create(CalculationResultLayerEventBinder.class);

  interface CalculationResultLayerEventBinder extends EventBinder<CalculationResultLayer> {}

  private final LayerInfo info;
  private final Image layer;

  private final ImageWmsParams imageWmsParams;
  private final ImageWmsOptions imageWmsOptions;
  private ImageWms imageWms;

  private List<ResultsGraphRange> selectedBounds = new ArrayList<>();

  private final CalculationContext calculationContext;
  private final ApplicationConfiguration themeConfiguration;

  @Inject
  public CalculationResultLayer(@Assisted final ApplicationConfiguration themeConfiguration,
      final EventBus eventBus, final EnvironmentConfiguration configuration, final CalculationContext calculationContext) {

    this.calculationContext = calculationContext;
    this.themeConfiguration = themeConfiguration;

    this.info = new LayerInfo();
    this.info.setName(this.getClass().getCanonicalName());
    this.info.setTitle(M.messages().layerResults());

    imageWmsParams = OLFactory.createOptions();

    imageWmsOptions = OLFactory.createOptions();
    imageWmsOptions.setUrl(configuration.getGeoserverBaseUrl());
    imageWmsOptions.setParams(imageWmsParams);

    layer = new Image();
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  public void onCalculationStatusEvent(final CalculationStatusEvent e) {
    final CalculationExecutionContext calculation = calculationContext.getInitializedCalculation(e.getJobKey());
    if (calculationContext.isActiveCalculation(calculation)) {
      updateLayer();
    }
  }

  @EventHandler
  public void onSwitchActiveCalculationEvent(final SwitchActiveCalculationEvent e) {
    updateLayer();
  }

  @EventHandler
  public void onSituationResultsSelectedEvent(final SituationResultsSelectedEvent e) {
    updateLayer();
  }

  @EventHandler
  public void onResultsSelectedEvent(final ResultsSelectedEvent e) {
    selectedBounds = e.getValue().getSelectedBounds();
    updateLayer();
  }

  private SituationResultsKey getSituationResultsKey() {
    return Optional.ofNullable(calculationContext.getActiveCalculation())
        .map(v -> v.getResultContext())
        .map(v -> v.getSituationResultsKey())
        .orElse(null);
  }

  private String buildViewParams(final SituationResultsKey situationResultsKey) {
    final String situationId = situationResultsKey.getSituationHandle().getId();
    final SummaryHexagonType hexagonType = situationResultsKey.getHexagonType();
    final OverlappingHexagonType overlappingHexagonType = situationResultsKey.getOverlappingHexagonType();
    final EmissionResultKey emissionResultKey = situationResultsKey.getEmissionResultKey();

    String viewParams = "";
    viewParams += "job_key:" + calculationContext.getActiveJobId();
    viewParams += ";situation_reference:" + situationId;
    viewParams += ";hexagon_type:" + hexagonType.name().toLowerCase(Locale.ROOT);
    viewParams += ";overlapping_hexagon_type:" + overlappingHexagonType.name().toLowerCase(Locale.ROOT);
    viewParams += ";emission_result_type:" + emissionResultKey.getEmissionResultType().name().toLowerCase(Locale.ROOT);
    viewParams += ";substance_id:" + emissionResultKey.getSubstance().getId();
    return viewParams;
  }

  private String buildCqlFilter(final SituationResultsKey situationResultsKey) {
    final EmissionResultKey emissionResultKey = situationResultsKey.getEmissionResultKey();
    final String cqlFilterOn = getEmissionResultTypeKey(emissionResultKey.getEmissionResultType());
    return selectedBounds.stream().map(bound -> {
      if (Double.isInfinite(bound.getLowerBound())) {
        return cqlFilterOn + " < " + bound.getUpperBound();
      } else if (Double.isInfinite(bound.getUpperBound())) {
        return cqlFilterOn + " >= " + bound.getLowerBound();
      } else {
        return cqlFilterOn + " >= " + bound.getLowerBound() + " AND " + cqlFilterOn + " < " + bound.getUpperBound();
      }
    }).map(bound -> "(" + bound + ")").collect(Collectors.joining(" OR "));
  }

  private String getEmissionResultTypeKey(final EmissionResultType emissionResultType) {
    switch (emissionResultType) {
    case DEPOSITION:
      return DEPOSITION;
    case CONCENTRATION:
      // Concentration is supplied by the result key currently
      return RESULT;
    default:
      return RESULT;
    }
  }

  private void updateLayer() {
    final SituationResultsKey situationResultsKey = getSituationResultsKey();

    updateLegend(situationResultsKey);
    if (situationResultsKey == null
        || situationResultsKey.getSituationHandle() == null
        || situationResultsKey.getHexagonType() == SummaryHexagonType.CUSTOM_CALCULATION_POINTS
        || !ResultDisplaySetUtil.couldHaveResults(situationResultsKey)) {
      layer.setSource(null);
      return;
    }
    imageWmsParams.setViewParams(buildViewParams(situationResultsKey));
    imageWmsParams.setLayers(buildLayerName(situationResultsKey));
    final int numberOfPointsCalculated = Optional.ofNullable(calculationContext.getActiveCalculation())
        .map(v -> v.getCalculationInfo())
        .map(v -> v.getJobProgress())
        .map(v -> v.getNumberOfPointsCalculated())
        .orElse(0);
    imageWmsParams.set("hc", String.valueOf(numberOfPointsCalculated));

    final String cqlFilter = buildCqlFilter(situationResultsKey);
    if (cqlFilter.isEmpty()) {
      imageWmsParams.set(CQL_FILTER, null);
    } else {
      imageWmsParams.set(CQL_FILTER, cqlFilter);
    }

    if (imageWms == null) {
      imageWms = new ImageWms(imageWmsOptions);
    }
    layer.setSource(imageWms);

    imageWms.refresh();
  }

  private void updateLegend(final SituationResultsKey situationResultsKey) {
    if (situationResultsKey == null) {
      this.info.setLegend(null);
      return;
    }
    final ScenarioResultType resultType = situationResultsKey.getResultType();
    final EmissionResultKey emissionResultKey = situationResultsKey.getEmissionResultKey();
    final ColorRangeType colorRangeType = themeConfiguration.getResultTypeColorRangeTypes()
        .determineColorRangeType(resultType, emissionResultKey);

    this.info.setLegend(new ColorRangesLegend(
        themeConfiguration.getColorRange(colorRangeType),
        v -> MessageFormatter.formatDeposition(v, themeConfiguration.getEmissionResultValueDisplaySettings()),
        M.messages().colorRangesLegendBetweenText(),
        LegendType.HEXAGON,
        emissionResultKey.getEmissionResultType() == EmissionResultType.CONCENTRATION
            ? M.messages().layerConcentrationResultsUnit()
            : M.messages().layerDepositionResultsUnit(themeConfiguration.getEmissionResultValueDisplaySettings().getDisplayType())));
  }

  private String buildLayerName(final SituationResultsKey situationResultsKey) {
    String url = WMS_BASE_LAYER_NAME;

    final ScenarioResultType resultType = situationResultsKey.getResultType();
    switch (resultType) {
    case SITUATION_RESULT:
      url += SITUATION_RESULT;
      break;
    case PROJECT_CALCULATION:
      url += PROJECT_CALCULATION;
      break;
    case MAX_TEMPORARY_EFFECT:
      url += TEMPORARY_EFFECT;
      break;
    case IN_COMBINATION:
      url += IN_COMBINATION;
      break;
    case ARCHIVE_CONTRIBUTION:
      url += ARCHIVE_CONTRIBUTION;
      break;
    default:
      break;
    }

    final EmissionResultKey emissionResultKey = situationResultsKey.getEmissionResultKey();
    final EmissionResultType emissionResultType = emissionResultKey.getEmissionResultType();

    switch (emissionResultType) {
    case DEPOSITION:
      url += DEPOSITION_LAYER;
      break;
    case CONCENTRATION:
      // Concentration on sub points (UK) is supplied by the _sub_results layer
      // NL doesn't use concentration result layers, so should be OK to use the concentration = sub points approach here.
      url += SUB_RESULTS_LAYER;
      break;
    default:
      url += RESULTS_LAYER;
      break;
    }

    return url;
  }

  @Override
  public Layer asLayer() {
    return layer;
  }

  @Override
  public Optional<LayerInfo> getInfoOptional() {
    return Optional.of(info);
  }

  @Override
  public LayerInfo getInfo() {
    return info;
  }

}
