/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.calculationpoint;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.google.web.bindery.event.shared.EventBus;

import elemental2.core.JsArray;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.application.command.CalculationPointDetailEditorErrorChangeCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.modify.ModifyListComponent;
import nl.overheid.aerius.wui.application.context.CalculationPointEditorContext;
import nl.overheid.aerius.wui.application.context.CalculationPointValidationContext;
import nl.overheid.aerius.wui.application.context.HabitatListContext;
import nl.overheid.aerius.wui.application.domain.calculationpoint.CalculationPointEntityReference;
import nl.overheid.aerius.wui.application.domain.calculationpoint.CalculationPointEntityType;
import nl.overheid.aerius.wui.application.domain.source.NcaCustomCalculationPointFeature;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.vue.BasicVueEventComponent;

@Component(components = {
    ButtonIcon.class,
    CalculationPointHabitatRowComponent.class,
    LabeledInputComponent.class,
    ModifyListComponent.class,
    SimplifiedListBoxComponent.class,
    VerticalCollapseGroup.class,
    VerticalCollapse.class
})
public class CalculationPointHabitatsEditorComponent extends BasicVueEventComponent implements HasCreated {
  protected static final Double CRITICAL_LEVEL_NOX_DEFAULT = 30.0;
  protected static final Double CRITICAL_LEVEL_NH3_DEFAULT = 3.0;
  protected static final Double CRITICAL_LOAD_DEFAULT = 25.0;

  @Inject @Data HabitatListContext listContext;
  @Inject @Data CalculationPointEditorContext context;
  @Inject @Data CalculationPointValidationContext validationContext;

  @Prop EventBus eventBus;
  @Prop @Data NcaCustomCalculationPointFeature calculationPoint;
  @Data @JsProperty CalculationPointEntityReference selectedHabitat = null;

  @Computed
  public JsArray<CalculationPointEntityReference> getHabitats() {
    return calculationPoint.getHabitats();
  }

  @Computed
  public boolean hasHabitatTypes() {
    return getHabitats().length > 0;
  }

  @JsMethod
  public boolean isSelected(final CalculationPointEntityReference habitat) {
    return listContext.getSingleSelection() != null && listContext.getSingleSelection().equals(habitat);
  }

  @JsMethod
  public void createNewHabitatType() {
    if (!(this.context.getCalculationPoint() instanceof NcaCustomCalculationPointFeature)) {
      return;
    }

    final NcaCustomCalculationPointFeature customCalculationPoint = (NcaCustomCalculationPointFeature) this.context.getCalculationPoint();
    final CalculationPointEntityReference newHabitat = new CalculationPointEntityReference();

    newHabitat.setEntityType(CalculationPointEntityType.CRITICAL_LEVEL_ENTITY); // For now the only supported entity type
    newHabitat.setDescription(M.messages().calculationPointHabitatDefaultLabel());
    newHabitat.setCriticalLevel(EmissionResultKey.NOX_CONCENTRATION, CRITICAL_LEVEL_NOX_DEFAULT);
    newHabitat.setCriticalLevel(EmissionResultKey.NH3_CONCENTRATION, CRITICAL_LEVEL_NH3_DEFAULT);
    newHabitat.setCriticalLevel(EmissionResultKey.NOXNH3_DEPOSITION, CRITICAL_LOAD_DEFAULT);
    customCalculationPoint.getHabitats().push(newHabitat);
    selectHabitat(newHabitat);
  }

  @JsMethod
  public void selectHabitat(final CalculationPointEntityReference habitat) {
    if (habitat == null || habitat.equals(context.getSelectedHabitat())) {
      context.selectHabitat(null);
      listContext.removeSelection(habitat);
    } else {
      context.selectHabitat(habitat);
      listContext.setSingleSelection(habitat);
    }
  }

  @JsMethod
  public void duplicateHabitatType() {
    final CalculationPointEntityReference duplicatedHabitat = createDuplicatedHabitat();
    final NcaCustomCalculationPointFeature customCalculationPoint = (NcaCustomCalculationPointFeature) this.context.getCalculationPoint();

    this.selectHabitat(duplicatedHabitat);
    customCalculationPoint.getHabitats().push(duplicatedHabitat);
  }

  private CalculationPointEntityReference createDuplicatedHabitat() {
    final CalculationPointEntityReference duplicatedHabitat = new CalculationPointEntityReference();
    duplicatedHabitat.setDescription(context.getSelectedHabitat().getDescription());
    duplicatedHabitat.setEntityType(context.getSelectedHabitat().getEntityType());
    duplicatedHabitat.setCriticalLevel(EmissionResultKey.NOX_CONCENTRATION, context.getSelectedHabitat().getCriticalLevel(EmissionResultKey.NOX_CONCENTRATION));
    duplicatedHabitat.setCriticalLevel(EmissionResultKey.NH3_CONCENTRATION, context.getSelectedHabitat().getCriticalLevel(EmissionResultKey.NH3_CONCENTRATION));
    duplicatedHabitat.setCriticalLevel(EmissionResultKey.NOXNH3_DEPOSITION, context.getSelectedHabitat().getCriticalLevel(EmissionResultKey.NOXNH3_DEPOSITION));
    return duplicatedHabitat;
  }

  @JsMethod
  public void deleteHabitatType() {
    final JsArray<CalculationPointEntityReference> currentHabitats = this.getHabitats();
    final JsArray<CalculationPointEntityReference> updatedHabitats = new JsArray<>();
    for (int i = 0; i < currentHabitats.length; i++) {
      if (!currentHabitats.getAt(i).equals(context.getSelectedHabitat())) {
        updatedHabitats.push(currentHabitats.getAt(i));
      }
    }
    calculationPoint.setHabitats(updatedHabitats);
    this.selectHabitat(null);

    if (updatedHabitats.length == 0) {
      eventBus.fireEvent(new CalculationPointDetailEditorErrorChangeCommand(false));
    }
  }

}
