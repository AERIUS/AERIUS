/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.farmland;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

/**
 * Client side implementation of customFarmland props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class CustomFarmlandActivity extends FarmlandActivity {

  public static final @JsOverlay CustomFarmlandActivity create(final String activityCode) {
    final CustomFarmlandActivity props = Js.uncheckedCast(JsPropertyMap.of());
    init(props, activityCode);
    return props;
  }

  public static final @JsOverlay void init(final CustomFarmlandActivity props, final String acitvityCode) {
    FarmlandActivity.initFarmlandActivity(props, acitvityCode, FarmlandActivityType.CUSTOM);
  }

}
