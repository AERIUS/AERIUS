/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.shipping.maritime;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.wui.vue.ReactivityUtil;

/**
 * Client side implementation of CustomMaritimeShipping props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class CustomMaritimeShipping extends MaritimeShipping {

  private CustomMaritimeShippingEmissionProperties emissionProperties;
  private int grossTonnage;

  public static final @JsOverlay CustomMaritimeShipping create() {
    final CustomMaritimeShipping props = Js.uncheckedCast(JsPropertyMap.of());
    init(props);
    return props;
  }

  public static final @JsOverlay void init(final CustomMaritimeShipping props) {
    MaritimeShipping.init(props, MaritimeShippingType.CUSTOM);
    ReactivityUtil.ensureJsPropertySupplied(props, "emissionProperties", props::setEmissionProperties, CustomMaritimeShippingEmissionProperties::create);
    ReactivityUtil.ensureInitialized(props::getEmissionProperties, CustomMaritimeShippingEmissionProperties::init);
    ReactivityUtil.ensureJsProperty(props, "grossTonnage", props::setGrossTonnage, 0);
  }

  public final @JsOverlay CustomMaritimeShippingEmissionProperties getEmissionProperties() {
    return emissionProperties;
  }

  public final @JsOverlay void setEmissionProperties(final CustomMaritimeShippingEmissionProperties emissionProperties) {
    this.emissionProperties = emissionProperties;
  }

  public final @JsOverlay int getGrossTonnage() {
    return grossTonnage;
  }

  public final @JsOverlay void setGrossTonnage(final int grossTonnage) {
    this.grossTonnage = grossTonnage;
  }

}
