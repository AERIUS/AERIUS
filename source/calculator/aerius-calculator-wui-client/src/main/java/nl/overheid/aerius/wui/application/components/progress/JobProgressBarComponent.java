/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.progress;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;

import nl.aerius.wui.vue.transition.HorizontalCollapse;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.tooltip.TooltipComponent;
import nl.overheid.aerius.wui.application.domain.calculation.JobProgress;
import nl.overheid.aerius.wui.application.ui.main.FadeTransition;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

/**
 * Component that shows the progress when calculating.
 */
@Component(components = {
    FadeTransition.class,
    HorizontalCollapse.class,
    TooltipComponent.class,
    ButtonIcon.class,
})
public class JobProgressBarComponent extends BasicVueComponent {
  @Prop(required = true) JobProgress jobProgress;
  @Prop boolean selected;

  @Computed("color")
  protected String getColor() {
    return selected ? "var(--progress-bar-color)" : "var(--progress-bar-color-light)";
  }

  @Computed("isCalculating")
  protected boolean isCalculating() {
    return jobProgress != null && jobProgress.getState() == JobState.CALCULATING;
  }

  @Computed
  protected String completePercentage() {
    final double completeRatio = jobProgress == null || jobProgress.getNumberOfPointsCalculated() == -1 || jobProgress.getNumberOfPoints() == 0
        ? 0.0 : (jobProgress.getNumberOfPointsCalculated() / (double) jobProgress.getNumberOfPoints());

    return Math.round(completeRatio * 100) + "%";
  }
}
