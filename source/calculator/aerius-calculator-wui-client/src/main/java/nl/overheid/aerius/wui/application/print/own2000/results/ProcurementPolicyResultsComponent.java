/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.print.own2000.results;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.summary.ProcurementPolicy;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.context.result.ResultSummaryContext;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsSummary;
import nl.overheid.aerius.wui.application.ui.pages.results.procurementpolicy.ProcurementPolicyThresholdsTable;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    ProcurementPolicyThresholdsTable.class
})
public class ProcurementPolicyResultsComponent extends BasicVueComponent {

  public static final String ID = "procurement-policy-results";

  @Prop EventBus eventBus;

  @Inject @Data CalculationContext calculationContext;

  @JsMethod
  public SituationResultsSummary getResultsSummary(final ProcurementPolicy policy) {
    return getSituationResultSummaries().entrySet().stream()
        .filter(e -> e.getKey().getProcurementPolicy() == policy)
        .map(Map.Entry::getValue)
        .findFirst()
        .orElse(null);
  }

  private Map<SituationResultsKey, SituationResultsSummary> getSituationResultSummaries() {
    return Optional.ofNullable(calculationContext)
        .map(CalculationContext::getActiveCalculation)
        .map(CalculationExecutionContext::getResultContext)
        .map(ResultSummaryContext::getAvailableResults)
        .orElse(new HashMap<>());
  }
}
