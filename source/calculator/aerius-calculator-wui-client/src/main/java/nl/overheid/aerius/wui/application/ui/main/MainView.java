/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.main;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasDestroyed;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.geo.command.MapResizeSequenceCommand;
import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.wui.application.components.cookie.CookieNoticeComponent;
import nl.overheid.aerius.wui.application.components.development.DeveloperPanel;
import nl.overheid.aerius.wui.application.components.manual.HelpManualComponentFactory;
import nl.overheid.aerius.wui.application.components.modal.ModalComponentShowingChangeEvent;
import nl.overheid.aerius.wui.application.components.nav.NavigationComponent;
import nl.overheid.aerius.wui.application.components.systeminfo.SystemInfoPanel;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.NavigationContext;
import nl.overheid.aerius.wui.application.context.SystemInfoContext;
import nl.overheid.aerius.wui.application.daemon.misc.PlaceContextDaemon;
import nl.overheid.aerius.wui.config.ApplicationFlags;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    SystemInfoPanel.class,
    NavigationComponent.class,

    ContentView.class,

    DeveloperPanel.class,
    CookieNoticeComponent.class,
})
public class MainView extends BasicVueComponent implements IsVueComponent, HasCreated, HasDestroyed {
  private static final MainViewEventBinder EVENT_BINDER = GWT.create(MainViewEventBinder.class);

  interface MainViewEventBinder extends EventBinder<MainView> {}

  public enum FlexView {
    /**
     * Sets the left and right pane to split down the middle.
     */
    SPLIT,
    /**
     * Sets the right pane to fill the remaining space on the page.
     */
    RIGHT;
  }

  @Prop String left;
  @Prop String middle;
  @Prop String right;

  @Prop boolean hasLeft;
  @Prop boolean hasMiddle;
  @Prop boolean hasRight;

  @Prop boolean softMiddle;

  @Data boolean closedLeft;
  @Data boolean closedRight;
  @Data boolean ariaHidden;

  @Prop FlexView flexView;

  @Data @Inject ApplicationContext appContext;
  @Data @Inject NavigationContext navigationContext;

  @Prop EventBus eventBus;

  @Inject @Data ApplicationFlags flags;

  @Inject PlaceContextDaemon placeDaemon;
  @Inject @Data SystemInfoContext systemInfoContext;

  private HandlerRegistration handlers;

  @PropDefault("softMiddle")
  boolean softMiddleDefault() {
    return false;
  }

  @Computed
  boolean hasSystemInfo() {
    return systemInfoContext.hasSystemInfoMessage();
  }

  @Watch("left")
  public void onLeftSLotChange(final Boolean old, final Boolean neww) {

  }

  @Watch("middle")
  public void onMiddleSlotChange(final Boolean old, final Boolean neww) {
    resize();
  }

  @Watch("right")
  public void onRightSlotChange(final Boolean old, final Boolean neww) {
    resize();
  }

  @Watch("hasMiddle")
  public void onMiddleChange(final Boolean old, final Boolean neww) {
    resize();
  }

  @Watch("softMiddle")
  public void onSoftMiddleChange(final Boolean old, final Boolean neww) {
    resize();
  }

  @Watch("closedRight")
  public void onClosedRightChange(final Boolean old, final Boolean neww) {
    resize();
  }

  @Computed
  public String getContentManual() {
    return navigationContext.isActiveManual()
        ? HelpManualComponentFactory.get().getComponentTagName()
        : null;
  }

  @Computed
  public boolean hasManual() {
    return navigationContext.isActiveManual();
  }

  @Computed
  public boolean isCookiePopupEnabled() {
    return appContext.isAppFlagEnabled(AppFlag.ENABLE_COOKIE_POLICY_POPUP);
  }

  public void resize() {
    eventBus.fireEvent(new MapResizeSequenceCommand());
  }

  @EventHandler
  public void onModalComponentShowingChangeEvent(final ModalComponentShowingChangeEvent e) {
    this.ariaHidden = e.getValue();
  }

  @Computed("withAuxPanel")
  public boolean withAuxPanel() {
    return flags.hasDeveloperPanel() || systemInfoContext.hasSystemInfoMessage();
  }

  @Override
  public void created() {
    placeDaemon.register(() -> closedLeft = !closedLeft, b -> closedLeft = !b);
    handlers = EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Override
  public void destroyed() {
    handlers.removeHandler();
  }
}
