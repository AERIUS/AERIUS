/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain;

import ol.Feature;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.wui.application.domain.source.TimeVaryingProfile;

/**
 * JsInterop does not support interfaces when the methods are a JsOverlay, so we need to write these
 * kinds of boilerplate "dual trampoline" default methods to achieve what is in effect a simple java interface.
 *
 * These things existing is fundamentally tragic, but what can you do..
 */
@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "*")
public interface ItemWithStringId {
  /**
   * Returns the id.
   *
   * @return id
   */
  @JsOverlay
  default String retrieveId() {
    if (this instanceof Feature) {
      return ((Feature) this).getId();
    } else if (this instanceof TimeVaryingProfile) {
      return ((TimeVaryingProfile) this).getId();
    } else {
      return null;
    }
  }

  /**
   * Set the id.
   *
   * @param id id
   */
  @JsOverlay
  default void persistId(final String id) {
    if (this instanceof Feature) {
      ((Feature) this).setId(id);
    } else if (this instanceof TimeVaryingProfile) {
      ((TimeVaryingProfile) this).setId(id);
    }
  }
}
