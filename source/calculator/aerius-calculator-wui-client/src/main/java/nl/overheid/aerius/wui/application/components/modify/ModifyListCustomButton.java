/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.modify;

/**
 * Class to add additional custom buttons to the ModifyList bar.
 */
public abstract class ModifyListCustomButton {

  private final String title;
  private final String ariaLabel;
  private final String iconClass;
  private final String debugId;
  private final boolean canBeDisabled;

  protected ModifyListCustomButton(final String title, final String ariaLabel, final String iconClass, final String debugId) {
    this(title, ariaLabel, iconClass, debugId, false);
  }

  protected ModifyListCustomButton(final String title, final String ariaLabel, final String iconClass, final String debugId,
      final boolean canBeDisabled) {
    this.title = title;
    this.ariaLabel = ariaLabel;
    this.iconClass = iconClass;
    this.debugId = debugId;
    this.canBeDisabled = canBeDisabled;
  }

  /**
   * Called when the button is selected.
   */
  public abstract void select();

  public String title() {
    return title;
  }

  public String ariaLabel() {
    return ariaLabel;
  }

  public String iconClass() {
    return iconClass;
  }

  public String debugId() {
    return debugId;
  }

  public boolean canBeDisabled() {
    return canBeDisabled;
  }
}
