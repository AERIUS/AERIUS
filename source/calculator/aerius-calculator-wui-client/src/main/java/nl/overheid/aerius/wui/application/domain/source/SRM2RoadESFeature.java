/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadElevation;
import nl.overheid.aerius.wui.application.domain.source.road.SRM2RoadSideBarrier;
import nl.overheid.aerius.wui.application.domain.source.road.Vehicles;
import nl.overheid.aerius.wui.vue.ReactivityUtil;

/**
 * Feature object for SRM2 Road Emission Sources.
 */
@JsType(namespace = GLOBAL, name = "ol.Feature", isNative = true)
public class SRM2RoadESFeature extends RoadESFeature {
  /**
   * @param sectorId
   * @return Returns Feature {@link SRM2RoadESFeature} object.
   */
  public static @JsOverlay SRM2RoadESFeature create() {
    final SRM2RoadESFeature feature = new SRM2RoadESFeature();
    init(feature);
    return feature;
  }

  public static @JsOverlay void init(final SRM2RoadESFeature feature) {
    RoadESFeature.initRoadFeature(feature, null, EmissionSourceType.SRM2_ROAD);
    ReactivityUtil.ensureDefault(feature::getElevation, feature::setElevation, RoadElevation.NORMAL);
    ReactivityUtil.ensureDefault(feature::getElevationHeight, feature::setElevationHeight, 0);
    ReactivityUtil.ensureDefault(feature::getPorosity, feature::setPorosity, 0D);
    ReactivityUtil.ensureDefault(feature::getBarrierLeft, feature::setBarrierLeft);
    ReactivityUtil.ensureInitialized(feature::getBarrierLeft, SRM2RoadSideBarrier::init);
    ReactivityUtil.ensureDefault(feature::getBarrierRight, feature::setBarrierRight);
    ReactivityUtil.ensureInitialized(feature::getBarrierRight, SRM2RoadSideBarrier::init);
    EmissionSubSourceFeature.initSubSources(feature, v -> Vehicles.initTypeDependent(v, v.getVehicleType()));
  }

  public final @JsOverlay void setPorosity(final Double porosity) {
    set("porosity", porosity);
  }

  public final @JsOverlay Double getPorosity() {
    return get("porosity") == null ? null : Js.coerceToDouble(get("porosity"));
  }

  public final @JsOverlay void setElevation(final RoadElevation elevation) {
    set("elevation", elevation.name());
  }

  public final @JsOverlay RoadElevation getElevation() {
    return get("elevation") == null ? null : RoadElevation.valueOf(get("elevation"));
  }

  public final @JsOverlay Integer getElevationHeight() {
    return get("elevationHeight") == null ? null : Js.coerceToInt(get("elevationHeight"));
  }

  public final @JsOverlay void setElevationHeight(final int elevationHeight) {
    set("elevationHeight", Double.valueOf(elevationHeight));
  }

  public final @JsOverlay SRM2RoadSideBarrier getBarrierLeft() {
    return Js.uncheckedCast(get("barrierLeft"));
  }

  public final @JsOverlay void setBarrierLeft(final SRM2RoadSideBarrier barrierLeft) {
    set("barrierLeft", barrierLeft);
  }

  public final @JsOverlay SRM2RoadSideBarrier getBarrierRight() {
    return Js.uncheckedCast(get("barrierRight"));
  }

  public final @JsOverlay void setBarrierRight(final SRM2RoadSideBarrier barrierRight) {
    set("barrierRight", barrierRight);
  }
}
