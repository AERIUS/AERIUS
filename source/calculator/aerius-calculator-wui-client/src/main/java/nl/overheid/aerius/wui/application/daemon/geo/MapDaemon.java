/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.geo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Singleton;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Document;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.Extent;
import ol.Feature;
import ol.OLFactory;
import ol.Overlay;
import ol.OverlayOptions;
import ol.Pixel;
import ol.format.GeoJson;
import ol.format.GeoJsonOptions;
import ol.format.Wkt;
import ol.geom.Geometry;
import ol.geom.Polygon;

import nl.aerius.geo.command.InteractionAddedCommand;
import nl.aerius.geo.command.InteractionRemoveCommand;
import nl.aerius.geo.command.LayerAddedCommand;
import nl.aerius.geo.command.LayerRemovedCommand;
import nl.aerius.geo.command.MapSetExtentCommand;
import nl.aerius.geo.command.OverlayAddedCommand;
import nl.aerius.geo.command.OverlayRemoveCommand;
import nl.aerius.geo.domain.IsLayer;
import nl.aerius.geo.domain.IsOverlay;
import nl.aerius.geo.event.MapSetExtentEvent;
import nl.aerius.geo.wui.MapLayoutPanel;
import nl.aerius.geo.wui.util.OL3MapUtil;
import nl.aerius.wui.command.SimpleGenericCommand;
import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.util.NotificationUtil;
import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.wui.application.command.MeasureToggleCommand;
import nl.overheid.aerius.wui.application.command.geo.ChangeLabelDisplayModeCommand;
import nl.overheid.aerius.wui.application.command.situation.SituationResultsZoomCommand;
import nl.overheid.aerius.wui.application.command.situation.SituationsZoomCommand;
import nl.overheid.aerius.wui.application.command.source.FeatureZoomCommand;
import nl.overheid.aerius.wui.application.command.source.GeometryZoomCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.MapConfigurationContext;
import nl.overheid.aerius.wui.application.context.MeasureContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.event.ApplicationFinishedLoadingEvent;
import nl.overheid.aerius.wui.application.event.LayersLoadedEvent;
import nl.overheid.aerius.wui.application.geo.MeasureTool;
import nl.overheid.aerius.wui.application.geo.interactions.FeatureClickInteraction;
import nl.overheid.aerius.wui.application.geo.layers.BackgroundResultLayer;
import nl.overheid.aerius.wui.application.geo.layers.HabitatAreasSensitivityLevelLayer;
import nl.overheid.aerius.wui.application.geo.layers.LayerFactory;
import nl.overheid.aerius.wui.application.geo.layers.NatureAreasLayer;
import nl.overheid.aerius.wui.application.geo.layers.calculationpoint.CalculationPointLayerGroup;
import nl.overheid.aerius.wui.application.geo.layers.criticallevel.PercentageCriticalLevelResultLayer;
import nl.overheid.aerius.wui.application.geo.layers.criticallevel.TotalPercentageCriticalLevelResultLayer;
import nl.overheid.aerius.wui.application.geo.layers.habitats.HabitatHoverLayer;
import nl.overheid.aerius.wui.application.geo.layers.habitats.HabitatSelectLayer;
import nl.overheid.aerius.wui.application.geo.layers.habitats.HabitatTypeLayerGroup;
import nl.overheid.aerius.wui.application.geo.layers.habitats.NitrogenSensitivityLayerGroup;
import nl.overheid.aerius.wui.application.geo.layers.job.JobInputLayerGroup;
import nl.overheid.aerius.wui.application.geo.layers.job.MetSiteLayerGroup;
import nl.overheid.aerius.wui.application.geo.layers.results.ArchiveProjectMarkersLayer;
import nl.overheid.aerius.wui.application.geo.layers.results.CalculationMarkersLayer;
import nl.overheid.aerius.wui.application.geo.layers.results.CalculationResultLayerGroup;
import nl.overheid.aerius.wui.application.geo.layers.source.RoadSourceGeometryLayer;
import nl.overheid.aerius.wui.application.geo.layers.source.SourceLayerGroup;
import nl.overheid.aerius.wui.application.geo.layers.zone.ZoneOfInfluenceLayer;
import nl.overheid.aerius.wui.application.geo.util.GeoUtil;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.print.own2000.results.PrintViewZoomCommand;

/**
 * Daemon to manage map changes.
 */
@Singleton
public class MapDaemon extends BasicEventComponent implements Daemon {
  private static final double BOUNDING_BOX_SCALE = 1.3;
  public static final int SONAR_REMOVE_TIME = 11_000;

  interface MapDaemonEventBinder extends EventBinder<MapDaemon> {}

  private final MapDaemonEventBinder EVENT_BINDER = GWT.create(MapDaemonEventBinder.class);

  @Inject private ScenarioContext scenarioContext;
  @Inject private CalculationContext calculationContext;

  @Inject private ApplicationContext applicationContext;
  @Inject private MapLayoutPanel map;
  @Inject private LayerFactory layerFactory;

  @Inject private ScenarioLayerContext scenarioLayerContext;
  @Inject private ResultLayerContext resultLayerContext;
  @Inject private MeasureContext measureContext;
  @Inject private MapConfigurationContext mapConfigurationContext;

  private EventBus eventBus;

  private final MeasureTool measureTool;

  // Keep record of sent situation results zoom commands by their extent
  // used for timing the result zoom event properly
  // (i.e. only after handling the set extent command and the set extent event has fired).
  private final Map<String, SimpleGenericCommand<SituationResultsKey, ?>> zoomCommandsIssued = new HashMap<>();

  public MapDaemon() {
    measureTool = new MeasureTool(overlay -> {
      eventBus.fireEvent(new OverlayAddedCommand(overlay));
    });
  }

  @EventHandler
  public void onFeatureZoomCommand(final FeatureZoomCommand c) {
    final Geometry geom = c.getValue().getGeometry();
    if (geom == null) {
      GWTProd.warn("Feature that does not have a geometry was focused.");
      return;
    }

    eventBus.fireEvent(new GeometryZoomCommand(geom));
  }

  @EventHandler
  public void onGeometryZoomCommand(final GeometryZoomCommand c) {
    eventBus.fireEvent(GeoUtil.createMapSetExtendCommand(c.getValue()));
  }

  @EventHandler
  public void onSonarCommand(final SonarCommand c) {
    final DivElement innerSonar = Document.get().createDivElement();
    innerSonar.setClassName("aer-inner-sonar");

    final DivElement sonar = Document.get().createDivElement();
    sonar.setClassName("aer-sonar");
    sonar.appendChild(innerSonar);

    final DivElement animationSonar = Document.get().createDivElement();
    animationSonar.setClassName("aer-animation-sonar");

    final DivElement outerSonar = Document.get().createDivElement();
    outerSonar.setClassName("aer-outer-sonar");
    outerSonar.appendChild(animationSonar);
    outerSonar.appendChild(sonar);

    final OverlayOptions options = OLFactory.createOptions();
    options.setElement(outerSonar);
    options.setInsertFirst(false);
    options.setStopEvent(false);
    options.setPositioning("top-left");
    options.setOffset(new Pixel(0, 0));
    final Overlay tooltip = new Overlay(options);

    tooltip.setPosition(c.getValue().getFirstCoordinate());

    final IsOverlay<Overlay> overlay = () -> tooltip;

    eventBus.fireEvent(new OverlayAddedCommand(overlay));
    SchedulerUtil.delay(() -> {
      eventBus.fireEvent(new OverlayRemoveCommand(overlay));
    }, SONAR_REMOVE_TIME);
  }

  @EventHandler
  public void onMeasureToggleCommand(final MeasureToggleCommand c) {
    if (measureContext.isActive()) {
      eventBus.fireEvent(new InteractionRemoveCommand(measureTool.getInteraction()));
      eventBus.fireEvent(new LayerRemovedCommand(measureTool.getLayer()));
      measureTool.getOverlays().forEach(overlay -> {
        eventBus.fireEvent(new OverlayRemoveCommand(overlay));
      });
      measureTool.clear();
    } else {
      eventBus.fireEvent(new InteractionAddedCommand(measureTool.getInteraction()));
      eventBus.fireEvent(new LayerAddedCommand(measureTool.getLayer()));
    }

    measureContext.setActive(!measureContext.isActive());
  }

  @EventHandler
  public void onApplicationFinishedLoadingEvent(final ApplicationFinishedLoadingEvent e) {
    initContext();
  }

  private void initContext() {
    resultLayerContext.clearResultLayers();
    scenarioLayerContext.clear();

    final ApplicationConfiguration appConfig = applicationContext.getConfiguration();

    // Add baselayers to map
    OL3MapUtil.createLayers(appConfig.getBaseLayers(), this::handleLayerError)
        .forEach(baseLayer -> eventBus.fireEvent(new LayerAddedCommand(baseLayer)));

    // Add calculation boundary layer
    final String calculatorBoundary = appConfig.getSettings().getSetting(SharedConstantsEnum.CALCULATOR_BOUNDARY);
    if (calculatorBoundary != null && !calculatorBoundary.isEmpty()) {
      eventBus.fireEvent(new LayerAddedCommand(layerFactory.createCalculationBoundaryLayer(calculatorBoundary)));
    }
    if (appConfig.isAppFlagEnabled(AppFlag.ZONE_OF_INFLUENCE_MAP_LAYER)) {
      final ZoneOfInfluenceLayer zoneOfInfluenceLayer = layerFactory.createZoneOfInfluenceLayer();
      eventBus.fireEvent(new LayerAddedCommand(zoneOfInfluenceLayer));
      scenarioLayerContext.addResultsLayer(zoneOfInfluenceLayer);
    }
    final NatureAreasLayer natureAreasLayer = layerFactory.createNatureAreasLayer(appConfig);
    eventBus.fireEvent(new LayerAddedCommand(natureAreasLayer));

    // Add layers to map
    final List<IsLayer<?>> layers = OL3MapUtil.createLayers(appConfig.getLayers(), this::handleLayerError);
    layers.forEach(layer -> eventBus.fireEvent(new LayerAddedCommand(layer)));

    final BackgroundResultLayer backgroundDepositionLayer = layerFactory.createBackgroundDepositionLayer(appConfig);
    eventBus.fireEvent(new LayerAddedCommand(backgroundDepositionLayer));

    // Add source layers
    int zIndex = 0;
    final SourceLayerGroup sourceLayerGroup = layerFactory.createSourceLayerGroup(zIndex++);
    scenarioLayerContext.setSourceLayerGroup(sourceLayerGroup);
    eventBus.fireEvent(new LayerAddedCommand(sourceLayerGroup));

    final JobInputLayerGroup jobInputLayerGroup = layerFactory.createJobInputLayerGroup(zIndex++, appConfig);
    scenarioLayerContext.setJobInputLayerGroup(jobInputLayerGroup);
    eventBus.fireEvent(new LayerAddedCommand(jobInputLayerGroup));

    if (appConfig.isAppFlagEnabled(AppFlag.ARCHIVE_PROJECTS_MAP_LAYER)) {
      final ArchiveProjectMarkersLayer archiveProjectsMarkerLayer = layerFactory.createArchiveProjectsMarkersLayer(zIndex++);
      eventBus.fireEvent(new LayerAddedCommand(archiveProjectsMarkerLayer));
      scenarioLayerContext.setArchiveProjectMarkerLayer(archiveProjectsMarkerLayer);
    }

    if (applicationContext.isAppFlagEnabled(AppFlag.ENABLE_MET_SITES_LAYER)) {
      final MetSiteLayerGroup metSiteLayer = layerFactory.createMetSiteLayerGroup(zIndex++);
      scenarioLayerContext.setMetSiteLayer(metSiteLayer);
      eventBus.fireEvent(new LayerAddedCommand(metSiteLayer));
    }

    final RoadSourceGeometryLayer roadSourceGeometryLayer = layerFactory.createRoadSourceGeometryLayer(appConfig, appConfig,
        appConfig.getSettings().getSetting(SharedConstantsEnum.DRIVING_SIDE), zIndex++);
    scenarioLayerContext.setRoadInputLayer(roadSourceGeometryLayer);
    eventBus.fireEvent(new LayerAddedCommand(roadSourceGeometryLayer));

    // Add calculation point marker layer
    final CalculationPointLayerGroup calculationPointLayerGroup = layerFactory.createCalculationPointLayerGroup(zIndex++);
    scenarioLayerContext.setCalculationPointLayerGroup(calculationPointLayerGroup);
    eventBus.fireEvent(new LayerAddedCommand(calculationPointLayerGroup));

    // Create habitat selection and hover layers
    final HabitatHoverLayer habitatHoverLayer = layerFactory.createHabitatHoverLayer();
    final HabitatSelectLayer habitatSelectionLayer = layerFactory.createHabitatSelectionLayer();
    eventBus.fireEvent(new LayerAddedCommand(habitatHoverLayer));
    eventBus.fireEvent(new LayerAddedCommand(habitatSelectionLayer));

    final Theme theme = appConfig.getTheme();

    // Create the combined sensitive habitat layer
    if (theme == Theme.OWN2000) {
      final List<IsLayer<?>> habitatLayers = OL3MapUtil.createLayers(appConfig.getLayers().stream()
          .filter(v -> NitrogenSensitivityLayerGroup.isSensitiveHabitatLayer(v) || HabitatTypeLayerGroup.isSensitiveHabitatLayer(v))
          .collect(Collectors.toList()), this::handleLayerError);

      final HabitatTypeLayerGroup sensitiveHabitatLayerGroup = layerFactory.createHabitatTypesLayerGroup(habitatLayers);
      eventBus.fireEvent(new LayerAddedCommand(sensitiveHabitatLayerGroup));
      final NitrogenSensitivityLayerGroup sensitiveHabitatsLayerGroup = layerFactory.createNitrogenSensitivityLayerGroup(habitatLayers);
      eventBus.fireEvent(new LayerAddedCommand(sensitiveHabitatsLayerGroup));

      final Optional<IsLayer<?>> habitatSensitivityLayer = layers.stream()
          .filter(v -> NitrogenSensitivityLayerGroup.HABITAT_AREAS_SENSITIVITY_NAME.equals(v.getInfo().getName()))
          .findFirst();
      final Optional<IsLayer<?>> extraAssessmentHexagonLayer = layers.stream()
          .filter(v -> NitrogenSensitivityLayerGroup.EXTRA_ASSESSMENT_HEXAGONS_SENSITIVITY_NAME.equals(v.getInfo().getName()))
          .findFirst();

      if (habitatSensitivityLayer.isPresent() && extraAssessmentHexagonLayer.isPresent()) {
        resultLayerContext.setHabitatLayer(habitatSensitivityLayer.get());
        resultLayerContext.setExtraAssessmentHexagonsLayer(extraAssessmentHexagonLayer.get());
      }
    }

    // Add result layers
    final CalculationResultLayerGroup resultLayerGroup = layerFactory.createCalculationResultLayerGroup(appConfig);
    final CalculationMarkersLayer markersLayer = layerFactory.createCalculationMarkersLayer();
    markersLayer.asLayer().setVisible(false);
    resultLayerContext.setResultLayerGroup(resultLayerGroup);
    resultLayerContext.setMarkersLayer(markersLayer);
    scenarioLayerContext.setResultLayerGroup(resultLayerGroup);
    scenarioLayerContext.addResultsLayer(markersLayer);
    eventBus.fireEvent(new LayerAddedCommand(resultLayerGroup));
    eventBus.fireEvent(new LayerAddedCommand(markersLayer));

    // Only add these layers for the NCA theme
    if (theme == Theme.NCA) {
      final PercentageCriticalLevelResultLayer percentageCriticalLoadLayer = layerFactory.createPercentageCriticalLevelLayer(appConfig);
      final TotalPercentageCriticalLevelResultLayer totalPercentageCriticalLoadLayer = layerFactory
          .createTotalPercentageCriticalLevelLayer(appConfig);
      final HabitatAreasSensitivityLevelLayer habitatAreasSensitivityLevelLayer = layerFactory
          .createHabitatAreasSensitivityLevelLayer(appConfig, zIndex++);
      percentageCriticalLoadLayer.asLayer().setVisible(false);
      totalPercentageCriticalLoadLayer.asLayer().setVisible(false);
      resultLayerContext.setPercentageCriticalLoadLayer(percentageCriticalLoadLayer);
      resultLayerContext.setTotalPercentageCriticalLoadLayer(totalPercentageCriticalLoadLayer);
      resultLayerContext.setHabitatLayer(habitatAreasSensitivityLevelLayer);
      eventBus.fireEvent(new LayerAddedCommand(habitatAreasSensitivityLevelLayer));
      eventBus.fireEvent(new LayerAddedCommand(percentageCriticalLoadLayer));
      eventBus.fireEvent(new LayerAddedCommand(totalPercentageCriticalLoadLayer));
    }

    eventBus.fireEvent(new LayerAddedCommand(layerFactory.createInfoMarkerLayer()));
    eventBus.fireEvent(new LayerAddedCommand(layerFactory.createGeometryHighlightLayer()));

    // Add a feature click interaction layer
    eventBus.fireEvent(new InteractionAddedCommand(new FeatureClickInteraction(eventBus)));

    eventBus.fireEvent(new LayersLoadedEvent());
  }

  private void handleLayerError(final String layerTitle) {
    NotificationUtil.broadcastError(eventBus, M.messages().errorLayerCouldNotBeLoaded(layerTitle));
  }

  @EventHandler
  public void onPrintViewZoomCommand(final PrintViewZoomCommand c) {
    c.silence();

    final List<EmissionSourceFeature> emissionSourceFeatures = scenarioContext.getSituations().stream()
        .flatMap(s -> s.getSources().stream())
        .collect(Collectors.toList());

    final List<BuildingFeature> buildingFeatures = scenarioContext.getSituations().stream()
        .flatMap(s -> s.getBuildings().stream())
        .collect(Collectors.toList());

    setMapExtentForFeatures(emissionSourceFeatures, buildingFeatures, c);
  }

  @EventHandler
  public void onSituationsZoomCommand(final SituationsZoomCommand c) {
    final List<SituationContext> situations = c.getValue();

    final List<EmissionSourceFeature> emissionSourceFeatures = situations.stream()
        .flatMap(s -> s.getSources().stream())
        .collect(Collectors.toList());

    final List<BuildingFeature> buildingFeatures = situations.stream()
        .flatMap(s -> s.getBuildings().stream())
        .collect(Collectors.toList());

    // List all geometries that should be included in the map view
    final List<Geometry> geometries = new ArrayList<>();
    emissionSourceFeatures.forEach(esf -> geometries.add(esf.getGeometry()));
    buildingFeatures.forEach(bf -> geometries.add(bf.getGeometry()));

    if (geometries.isEmpty()) {
      return;
    }

    // Find the extent spanning all geometries
    final Extent mapExtent = getMapExtentFromGeometries(geometries);

    final Polygon polygon = Polygon.fromExtent(mapExtent);
    polygon.scale(BOUNDING_BOX_SCALE, BOUNDING_BOX_SCALE);
    final MapSetExtentCommand mapSetExtendCommand = GeoUtil.createMapSetExtendCommand(polygon);

    eventBus.fireEvent(mapSetExtendCommand);
  }

  @EventHandler
  public void onSituationZoomCommand(final SituationResultsZoomCommand c) {
    c.silence();
    final SituationResultsKey situationResultsKey = c.getValue();

    final List<EmissionSourceFeature> emissionSourceFeatures = Optional
        .ofNullable(scenarioContext.getSituation(situationResultsKey.getSituationHandle().getId()))
        .map(SituationContext::getSources)
        .orElse(Arrays.asList());

    final List<BuildingFeature> buildingFeatures = Optional
        .ofNullable(scenarioContext.getSituation(situationResultsKey.getSituationHandle().getId()))
        .map(SituationContext::getBuildings)
        .orElse(Arrays.asList());

    setMapExtentForFeatures(emissionSourceFeatures, buildingFeatures, c);
  }

  private void setMapExtentForFeatures(final List<EmissionSourceFeature> emissionSourceFeatures, final List<BuildingFeature> buildingFeatures,
      final SimpleGenericCommand<SituationResultsKey, ?> command) {

    final List<Feature> markerFeatures = Optional.ofNullable(calculationContext.getActiveCalculation())
        .map(CalculationExecutionContext::getResultContext)
        .map(rc -> rc.getResultSummary(command.getValue()))
        .map(rs -> rs.getMarkers().asList().stream().map(rm -> {
          final Feature markerFeature = new Feature();
          markerFeature.setGeometry(new GeoJson().readGeometry(rm.getPoint(), new GeoJsonOptions()));
          return markerFeature;
        }).collect(Collectors.toList()))
        .orElse(Arrays.asList());

    // List all geometries that should be included in the map view
    final List<Geometry> geometries = new ArrayList<>();
    emissionSourceFeatures.forEach(esf -> geometries.add(esf.getGeometry()));
    buildingFeatures.forEach(bf -> geometries.add(bf.getGeometry()));
    markerFeatures.forEach(mf -> geometries.add(mf.getGeometry()));

    // Find the extent spanning all geometries
    // If there are no geometries on the map, use the boundary
    final Extent mapExtent;
    if (geometries.isEmpty()) {
      mapExtent = new Wkt()
          .readGeometry(applicationContext.getConfiguration().getSettings().getSetting(SharedConstantsEnum.CALCULATOR_BOUNDARY))
          .getExtent();
    } else {
      mapExtent = getMapExtentFromGeometries(geometries);
    }

    final Polygon polygon = Polygon.fromExtent(mapExtent);
    polygon.scale(BOUNDING_BOX_SCALE, BOUNDING_BOX_SCALE);
    final MapSetExtentCommand mapSetExtendCommand = GeoUtil.createMapSetExtendCommand(polygon);

    // Don't animate zoom as long as SituationResultsZoomCommand is only fired from print OwN2000 context.
    mapSetExtendCommand.setAnimated(false);
    eventBus.fireEvent(mapSetExtendCommand);
    zoomCommandsIssued.put(mapSetExtendCommand.getValue(), command);
  }

  private Extent getMapExtentFromGeometries(final List<Geometry> geometries) {
    final Extent mapExtent = Extent.createEmpty();

    geometries.stream()
        .map(Geometry::getExtent)
        .forEach(extent -> mapExtent.extend(extent));

    return mapExtent;
  }

  @EventHandler
  public void onMapSetExtentEvent(final MapSetExtentEvent e) {
    Optional.ofNullable(zoomCommandsIssued.get(e.getValue()))
        .filter(v -> v.getValue() != null)
        .ifPresent(command -> {
          eventBus.fireEvent(command.getEvent());
          zoomCommandsIssued.remove(e.getValue());
        });
  }

  @EventHandler
  public void onChangeLabelDisplayModeCommand(final ChangeLabelDisplayModeCommand command) {
    mapConfigurationContext.setLabelDisplayMode(command.getValue());
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    this.eventBus = eventBus;
    EVENT_BINDER.bindEventHandlers(this, eventBus);
    map.setEventBus(eventBus);
  }
}
