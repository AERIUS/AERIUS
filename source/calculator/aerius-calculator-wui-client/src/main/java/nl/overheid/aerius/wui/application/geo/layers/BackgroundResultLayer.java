/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.OLFactory;
import ol.layer.Image;
import ol.layer.Layer;
import ol.source.ImageWms;
import ol.source.ImageWmsOptions;
import ol.source.ImageWmsParams;

import nl.aerius.geo.domain.IsLayer;
import nl.aerius.geo.domain.LayerInfo;
import nl.aerius.geo.domain.LayerOptions;
import nl.aerius.geo.domain.legend.LegendType;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.legend.ColorRangeType;
import nl.overheid.aerius.shared.domain.legend.ColorRangesLegend;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.event.situation.SituationChangedYearEvent;
import nl.overheid.aerius.wui.application.event.situation.SituationSwitchEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;

/**
 * Provides the background result layer
 */
public class BackgroundResultLayer implements IsLayer<Layer> {

  private static final BackgroundResultLayerEventBinder EVENT_BINDER = GWT.create(BackgroundResultLayerEventBinder.class);

  interface BackgroundResultLayerEventBinder extends EventBinder<BackgroundResultLayer> {}

  private static final String WMS_BACKGROUND_RESULTS_VIEW = "calculator:wms_background_results_view";

  private final Image layer;
  private final ImageWmsParams imageWmsParams;
  private final ImageWmsOptions imageWmsOptions;
  private ImageWms imageWms;
  private final ApplicationConfiguration themeConfiguration;

  private EmissionResultKey emissionResultKey = EmissionResultKey.NOXNH3_DEPOSITION;
  private ColorRangeType colorRangeType = ColorRangeType.BACKGROUND_DEPOSITION;

  private final LayerInfo info;

  @Inject ScenarioContext scenarioContext;

  @Inject
  public BackgroundResultLayer(final EventBus eventBus, @Assisted final ApplicationConfiguration themeConfiguration,
      final EnvironmentConfiguration configuration) {
    imageWmsParams = OLFactory.createOptions();
    this.themeConfiguration = themeConfiguration;

    this.info = new LayerInfo();
    this.info.setName(this.getClass().getCanonicalName());
    this.info.setTitle(M.messages().layerBackgroundResults());
    this.info.setOptions(constructLayerOptions());

    updateLegend();

    imageWmsOptions = OLFactory.createOptions();
    imageWmsOptions.setUrl(configuration.getGeoserverBaseUrl());
    imageWmsOptions.setParams(imageWmsParams);

    layer = new Image();
    layer.setVisible(false);
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  public void onUpdateLayer(final SituationChangedYearEvent e) {
    updateLayer();
  }

  @EventHandler
  public void onSituationSwitchEvent(final SituationSwitchEvent e) {
    if (e.getValue() == null) {
      layer.setSource(null);
    } else {
      layer.setSource(imageWms);
      updateLayer();
    }
  }

  private LayerOptions[] constructLayerOptions() {
    final List<LayerOptions> options = new ArrayList<>();
    final List<EmissionResultKey> emissionResultKeys = themeConfiguration.getEmissionResultKeys();
    if (emissionResultKeys.size() > 1) {
      options.add(LayerOptions.createFromEnum(emissionResultKeys.toArray(new EmissionResultKey[0]),
          ls -> M.messages().calculateEmissionResultKey(ls),
          v -> this.setEmissionResultKey(v), emissionResultKey));
    } else if (emissionResultKeys.size() == 1) {
      // Don't show the dropdown, just set the ERK.
      emissionResultKey = emissionResultKeys.get(0);
    } else {
      emissionResultKey = EmissionResultKey.NOXNH3_DEPOSITION;
    }

    return options.toArray(new LayerOptions[0]);
  }

  private void setEmissionResultKey(final EmissionResultKey emissionResultKey) {
    this.emissionResultKey = emissionResultKey;
    updateColorRangeType();
    updateLayer();
    asLayer().getSource().changed();
  }

  private void updateColorRangeType() {
    switch (emissionResultKey) {
    case NOX_CONCENTRATION:
      colorRangeType = ColorRangeType.BACKGROUND_CONCENTRATION_NOX;
      break;
    case NH3_CONCENTRATION:
      colorRangeType = ColorRangeType.BACKGROUND_CONCENTRATION_NH3;
      break;
    default:
      colorRangeType = ColorRangeType.BACKGROUND_DEPOSITION;
      break;
    }
  }

  private String buildViewParams() {
    final SituationContext activeSituation = this.scenarioContext.getActiveSituation();

    String viewParams = "";
    viewParams += "year:" + (activeSituation == null ? "0" : activeSituation.getYear());
    viewParams += ";color_range_type:" + this.colorRangeType.name().toLowerCase(Locale.ROOT);
    viewParams += ";emission_result_type:" + emissionResultKey.getEmissionResultType().name().toLowerCase(Locale.ROOT);
    viewParams += ";substance_id:" + emissionResultKey.getSubstance().getId();
    return viewParams;
  }

  private void updateLayer() {
    imageWmsParams.setViewParams(buildViewParams());
    imageWmsParams.setLayers(WMS_BACKGROUND_RESULTS_VIEW);

    if (imageWms == null) {
      imageWms = new ImageWms(imageWmsOptions);
      layer.setSource(imageWms);
    }

    imageWms.refresh();

    updateLegend();
  }

  private void updateLegend() {
    this.info.setLegend(new ColorRangesLegend(
        themeConfiguration.getColorRange(colorRangeType),
        v -> MessageFormatter.formatDeposition(v, themeConfiguration.getEmissionResultValueDisplaySettings()),
        M.messages().colorRangesLegendBetweenText(),
        LegendType.HEXAGON,
        emissionResultKey.getEmissionResultType() == EmissionResultType.CONCENTRATION
            ? M.messages().layerConcentrationResultsUnit()
            : M.messages().layerDepositionResultsUnit(themeConfiguration.getEmissionResultValueDisplaySettings().getDisplayType())));
  }

  @Override
  public Layer asLayer() {
    return layer;
  }

  @Override
  public LayerInfo getInfo() {
    return info;
  }

}
