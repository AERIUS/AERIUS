/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.util;

import java.util.function.Consumer;

import nl.aerius.wui.util.SchedulerUtil;

/**
 * Tragically as it turns out, the dblclick event also fires a click event, so
 * we must fabricate that it doesn't.
 */
public class DoubleClickUtil<T> {

  private T clickedEarlier = null;
  private static final int DBL_CLICK_TIME = 500;

  /**
   * Execute when an element is clicked on
   * @param element element that is clicked on
   * @param onSingleClick ran when it is a single click
   * @param onDoubleClick ran when it is a double click
   */
  public void click(final T element, final Consumer<T> onSingleClick, final Consumer<T> onDoubleClick) {
    if (clickedEarlier == element) {
      onDoubleClick.accept(element);
    } else {
      clickedEarlier = element;
      onSingleClick.accept(element);
      SchedulerUtil.delay(() -> {
        clickedEarlier = null;
      }, DBL_CLICK_TIME);
    }
  }

}
