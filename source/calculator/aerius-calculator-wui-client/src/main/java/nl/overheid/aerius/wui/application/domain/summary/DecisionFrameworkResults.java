/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.summary;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.JsPropertyMap;

@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class DecisionFrameworkResults {

  //Overall decision making threshold (DMT) results
  private JsPropertyMap<ThresholdResult> decisionMakingThresholdResults;

  private String developmentPressureClassification;

  // Site relevant threshold (SRT) results
  private AssessmentAreaThresholdResults[] assessmentAreaThresholdResults;

  public final @JsOverlay JsPropertyMap<ThresholdResult> getDecisionMakingThresholdResults() {
    return decisionMakingThresholdResults;
  }

  public final @JsOverlay String getDevelopmentPressureClassification() {
    return developmentPressureClassification;
  }

  public final @JsOverlay AssessmentAreaThresholdResults[] getAssessmentAreaThresholdResults() {
    return assessmentAreaThresholdResults;
  }

}
