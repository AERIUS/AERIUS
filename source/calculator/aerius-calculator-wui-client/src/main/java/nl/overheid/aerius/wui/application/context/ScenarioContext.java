/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.inject.Singleton;

import ol.Feature;
import ol.format.GeoJson;
import ol.format.GeoJsonFeatureOptions;

import elemental2.core.Global;
import elemental2.core.JsArray;

import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.util.ConsecutiveIdUtil;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.application.domain.geo.CalculationPointUtil;
import nl.overheid.aerius.wui.application.domain.importer.ScenarioMetaData;
import nl.overheid.aerius.wui.application.domain.source.ADMSCharacteristics;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.util.json.JsonBuilder;

/**
 * Object to represent a full scenario that can be calculated. A scenario can
 * consist of multiple situations.
 *
 * @see nl.overheid.aerius.shared.domain.v2.scenario.Scenario
 */
@JsType
@Singleton
public class ScenarioContext {
  private final @JsProperty List<SituationContext> situations = new ArrayList<>();
  private String scenarioCode = null;
  private String activeSituationId = null;
  private final @JsProperty List<CalculationPointFeature> calculationPoints = new ArrayList<>();

  public boolean hasScenario() {
    return scenarioCode != null;
  }

  public String getScenarioCode() {
    return scenarioCode;
  }

  public void setScenarioCode(final String scenarioCode) {
    this.scenarioCode = scenarioCode;
  }

  public SituationContext getActiveSituation() {
    return getSituation(getActiveSituationId());
  }

  public SituationContext getSituation(final String situationId) {
    for (final SituationContext situation : situations) {
      if (situation.getId().equals(situationId)) {
        return situation;
      }
    }

    return null;
  }

  public boolean deleteSituation(final SituationContext situation) {
    // Also set active situation code to null if that is the one being removed
    if (activeSituationId != null && activeSituationId.equals(situation.getId())) {
      activeSituationId = null;
    }

    return situations.remove(situation);
  }

  public boolean hasSituations() {
    return !situations.isEmpty();
  }

  public boolean hasActiveSituation() {
    return activeSituationId != null;
  }

  public String getActiveSituationId() {
    return activeSituationId;
  }

  /**
   * Sets a new active situation id. It returns true if the new id was
   * different as the current id or if the current id was null.
   *
   * @param newSituationId situation id to set.
   * @return true if activation code set is different from the current
   */
  public boolean setActiveSituationId(final String newSituationId) {
    final boolean changed = this.activeSituationId == null || !this.activeSituationId.equals(newSituationId);
    this.activeSituationId = newSituationId;
    return changed;
  }

  public List<SituationContext> getSituations() {
    return situations;
  }

  public List<CalculationPointFeature> getCalculationPoints() {
    return calculationPoints;
  }

  /**
   * Add all calculation points to this scenario context ignoring points that
   * overlap. Returns the list of added calculation points.
   */
  public List<CalculationPointFeature> addCalculationPointsIgnoreOverlapping(final List<CalculationPointFeature> calculationPoints) {
    final List<CalculationPointFeature> addedCalculationPoints = new ArrayList<>();
    for (final CalculationPointFeature feature : calculationPoints) {
      this.addCalculationPointIgnoreOverlapping(feature).ifPresent(addedCalculationPoints::add);
    }
    return addedCalculationPoints;
  }

  private Optional<CalculationPointFeature> addCalculationPointIgnoreOverlapping(final CalculationPointFeature newFeature) {
    if (newFeature == null || newFeature.getGeometry() == null) {
      return Optional.empty();
    }

    for (final CalculationPointFeature feature : this.calculationPoints) {
      if (feature.getGeometry().getExtent().containsExtent(newFeature.getGeometry().getExtent())) {
        return Optional.empty();
      }
    }

    CalculationPointUtil.addAndSetId(this.calculationPoints, newFeature);
    return Optional.of(newFeature);
  }

  public String toJSONString(final ScenarioMetaData scenarioMetaData, final SituationComposition composition,
      final CalculationSetOptions calculationOptions) {
    return Global.JSON.stringify(toJSON(scenarioMetaData, composition, calculationOptions));
  }

  private Object toJSON(final ScenarioMetaData scenarioMetaData, final SituationComposition composition,
      final CalculationSetOptions calculationOptions) {
    final JsonBuilder jsonBuilder = new JsonBuilder(this);
    jsonBuilder.set("situations", asJsArray(filterSituations(this, composition)));
    jsonBuilder.set("customPoints",
        new GeoJson().writeFeaturesObject(this.calculationPoints.toArray(new Feature[calculationPoints.size()]), new GeoJsonFeatureOptions()));

    if (scenarioMetaData != null) {
      jsonBuilder.set("metaData", scenarioMetaData.toJSON());
    }

    if (calculationOptions != null) {
      jsonBuilder.set("options", calculationOptions.toJSON());
    }

    return jsonBuilder.build();
  }

  private JsArray<SituationContext> asJsArray(final List<SituationContext> situations) {
    final JsArray<SituationContext> arr = new JsArray<>();
    situations.forEach(arr::push);
    return arr;
  }

  public boolean isActiveSituation(final SituationContext value) {
    // If either the value given or the active situation is null, return false
    if (value == null || activeSituationId == null) {
      return false;
    }

    // Just throw here because this should be impossible.
    if (value.getId() == null) {
      throw new IllegalStateException("SituationContext is in an illegal state: no id when presented for id comparison");
    }

    return value.getId().equals(activeSituationId);
  }

  /**
   * Note: This should in ordinary circumstances /only/ be called from
   * SituationDaemon, which does sanity checks.
   */
  public void addSituation(final int idx, final SituationContext situationContext) {
    situations.add(idx, situationContext);
  }

  public ConsecutiveIdUtil createConsecutiveSituationIdUtil() {
    return new ConsecutiveIdUtil(
        i -> Integer.toString(i + 1),
        i -> situations.get(i).getSituationNumber(),
        situations::size);
  }

  public static BuildingFeature findBuilding(final SituationContext context, final String id) {
    return context.getBuildings().stream()
        .filter(v -> id.equals(v.getId()))
        .findFirst().orElse(null);
  }

  /**
   * Static search method for emission sources linked to a building
   */
  public static List<EmissionSourceFeature> findBuildingLinkedSources(final ScenarioContext context, final String situationId,
      final String buildingGmlId) {
    return context.getSituations().stream()
        .filter(v -> situationId.equals(v.getId()))
        .flatMap(v -> v.getSources().stream())
        .filter(v -> v.getCharacteristics() != null)
        .filter(v -> v.getCharacteristics().isBuildingInfluence())
        .filter(v -> buildingGmlId.equals(v.getCharacteristics().getBuildingGmlId()))
        .collect(Collectors.toList());
  }

  /**
   * Static search method for emission sources that have ADMS Characteristics.
   */
  public static Stream<EmissionSourceFeature> findADMSCharacteristicsSources(final ScenarioContext context, final String situationId) {
    return context.getSituations().stream()
        .filter(v -> situationId.equals(v.getId()))
        .flatMap(v -> v.getSources().stream())
        .filter(v -> v.getCharacteristics() != null && v.getCharacteristicsType() == CharacteristicsType.ADMS);
  }

  /**
   * Static method to remove the given time-varying profile from all sources that have that profile.
   */
  public static void cleanTimeVaryingProfileLinkedSources(final ScenarioContext context, final String situationId,
      final String timeVaryingProfileId) {
    findADMSCharacteristicsSources(context, situationId)
        .forEach(v -> conditionalClean((ADMSCharacteristics) v.getCharacteristics(), timeVaryingProfileId));
  }

  private static void conditionalClean(final ADMSCharacteristics characteristics, final String timeVaryingProfileId) {
    if (timeVaryingProfileId.equals(characteristics.getCustomHourlyTimeVaryingProfileId())) {
      characteristics.setCustomHourlyTimeVaryingProfileId(null);
    }
    if (timeVaryingProfileId.equals(characteristics.getCustomMonthlyTimeVaryingProfileId())) {
      characteristics.setCustomMonthlyTimeVaryingProfileId(timeVaryingProfileId);
    }
  }

  // This is a computed util method and as such it is made a public static
  public static String calculateExecutionInputHashFromJob(final ScenarioContext scenarioContext, final CalculationJobContext jobContext) {
    return calculateExecutionInputHash(scenarioContext, jobContext.getSituationComposition(), jobContext.getCalculationOptions());
  }

  // This is a computed util method and as such it is made a public static
  public static String calculateExecutionInputHash(final ScenarioContext scenarioContext, final SituationComposition composition,
      final CalculationSetOptions calculationOptions) {
    // This is quite the poor man's hash function, but it'll do.
    return String.valueOf(scenarioContext.toJSONString(null, composition, calculationOptions).hashCode());
  }

  public static List<SituationContext> filterSituations(final ScenarioContext scenarioContext, final SituationComposition composition) {
    return scenarioContext.getSituations().stream()
        .filter(composition::isActiveSituation)
        .collect(Collectors.toList());
  }

  public static boolean hasAnyContent(final ScenarioContext scenarioContext) {
    return scenarioContext.getSituations().stream()
        .anyMatch(SituationContext::hasAnyContent);
  }

  public static boolean hasAnySource(final ScenarioContext scenarioContext) {
    return scenarioContext.getSituations().stream()
        .anyMatch(sit -> !sit.getSources().isEmpty());
  }

  public static boolean isCalculationStale(final ScenarioContext scenarioContext, final CalculationJobContext activeJob,
      final CalculationExecutionContext calculation) {
    final String scenarioInputHash = ScenarioContext.calculateExecutionInputHashFromJob(scenarioContext, activeJob);
    return calculation != null && !scenarioInputHash.equals(calculation.getExecutionInputHash());
  }
}
