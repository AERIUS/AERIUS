/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.util;

import java.util.stream.Stream;

import ol.Coordinate;
import ol.geom.Polygon;

import nl.aerius.wui.dev.GWTProd;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;

/**
 * The poor man's unautomated unit test.
 *
 * GWT is hard to unit test, so this is the easiest solution we have available
 * right now.
 *
 * Add GeoUtilTest.doTest() in ApplicationInitializer or something and it should
 * report whether this code (still) works.
 */
public class GeoUtilTest {
  private static final double DELTA = 0.01D;

  public static final void doTest() {
    provideTestCases().forEach(args -> doTest(args));
  }

  private static void doTest(final Arguments args) {
    final double[][] coordinates = args.getAt(4);
    final Coordinate[] olCoords = Stream.of(coordinates)
        .map(v -> new Coordinate(v[0], v[1]))
        .toArray(len -> new Coordinate[len]);
    final Polygon polygon = new Polygon(new Coordinate[][] { olCoords });

    final OrientedEnvelope envelope = GeoUtil.determineOrientedEnvelope(polygon);

    assertEquals("Envelope width", args.getAt(1), envelope.getWidth(), DELTA);
    assertEquals("Envelope length", args.getAt(2), envelope.getLength(), DELTA);
    assertEquals("Envelope orientation", args.getAt(3), envelope.getOrientation(), DELTA);
    GWTProd.log("---");
  }

  private static void assertEquals(final String string, final Double expected, final Double actual, final double acceptableDelta) {
    final double delta = Math.abs(expected - actual);
    if (delta >= 0 && delta <= acceptableDelta) {
      GWTProd.log("PASSED ::: " + string, actual);
    } else {
      GWTProd.error("FAILED ::: " + string + " - value does not match. [expected:actual]", expected, actual);
    }
  }

  /**
   * Copied straight from nl.overheid.aerius.util.ConstructPolygonTest
   */
  private static Stream<Arguments> provideTestCases() {
    return Stream.of(
        testCase(2.5, 5, 5, 10, 90,
            new double[][] { { 0.0, 0.0 }, { 5.0, 0.0 }, { 0.0, 10.0 }, { 5.0, 10.0 }, { 0.0, 0.0 } }),
        testCase(5, 2.5, 5, 10, 0,
            new double[][] { { 0.0, 0.0 }, { 10.0, 0.0 }, { 0.0, 5.0 }, { 10.0, 5.0 }, { 0.0, 0.0 } }),
        testCase(5, 5.5, 5, 10, 53.130,
            new double[][] { { 0.0, 3.0 }, { 4.0, 0.0 }, { 10.0, 8.0 }, { 6.0, 11.0 }, { 0.0, 3.0 } }),
        testCase(5.5, 5, 5, 10, 143.130,
            new double[][] { { 0.0, 6.0 }, { 8.0, 0.0 }, { 11.0, 4.0 }, { 3.0, 10.0 }, { 0.0, 6.0 } }));
  }

  private static Arguments testCase(final double centerX, final double centerY, final double width, final double length, final double orientation,
      final double[][] expectedCoordinates) {
    // Using a client side point rather than a server side point, which should be
    // more or less the same
    final Point point = new Point(centerX, centerY);
    return Arguments.of(point, width, length, orientation, expectedCoordinates);
  }

  private static class Arguments {
    private final Object[] objects;

    public Arguments(final Object... objects) {
      this.objects = objects;
    }

    public static Arguments of(final Object... objects) {
      return new Arguments(objects);
    }

    @SuppressWarnings({ "unchecked" })
    public <T> T getAt(final int idx) {
      return (T) objects[idx];
    }
  }
}
