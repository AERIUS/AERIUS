/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.vue;

import java.util.function.Function;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.google.gwt.safehtml.shared.ExtraSimpleHtmlSanitizer;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.wui.application.i18n.ApplicationMessages;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.resources.ImageResources;
import nl.overheid.aerius.wui.application.resources.R;
import nl.overheid.aerius.wui.application.util.StringUtils;

@Component
public abstract class BasicVueComponent implements IsVueComponent {
  @Data public ApplicationMessages i18n = M.messages();
  @Data public ImageResources img = R.images();

  public void destroy() {
    vue().$destroy();
    vue().$el().parentNode.removeChild(vue().$el());
  }

  @JsMethod
  @SuppressWarnings({"rawtypes", "unchecked"})
  public Function func(final Function func) {
    return t -> func.apply(t);
  }

  @JsMethod
  public String textify(final String text) {
    return StringUtils.unescapeHtml(text);
  }

  @JsMethod
  public String sanitize(final String html) {
    return ExtraSimpleHtmlSanitizer.sanitizeHtml(html).asString();
  }

  /**
   * Dummy method that does nothing. VueGWT doesn't support empty expressions,
   * using this dummy makes that easier.
   */
  @JsMethod
  public void dummy() {}
}
