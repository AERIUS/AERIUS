/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.input.suggest;

import java.util.List;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Emit;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.google.gwt.user.client.Window;

import elemental2.dom.ClientRect;
import elemental2.dom.HTMLElement;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    VerticalCollapse.class,
    VerticalCollapseGroup.class
})
public class SuggestDropdownModal extends BasicVueComponent {
  @Prop boolean showing;

  @Prop @JsProperty List<SuggestListBoxData> options;
  @Prop SuggestListBoxData selected;
  @Prop int modalOffset;
  @Prop int modalWidth;

  @Prop HTMLElement host;

  @Data String minWidth = "";
  @Data String maxWidth = "";
  @Data String top = "50px";
  @Data String left = "50px";

  @Data String height = null;

  @Data String search = "";

  @Watch("showing")
  public void onShowingChange(final boolean neww) {
    if (neww) {
      final ClientRect boundingClientRect = host.getBoundingClientRect();
      minWidth = modalWidth + "px";
      maxWidth = Window.getClientWidth() - boundingClientRect.left - 10 + "px";
      top = boundingClientRect.top + "px";
      left = boundingClientRect.left + modalOffset + "px";
      height = boundingClientRect.height + "px";
    }
  }

  @JsMethod
  public void select(final SuggestListBoxData sel) {
    vue().$emit("select", sel);
  }

  @JsMethod
  @Emit("exit")
  public void autoClose() {}
}
