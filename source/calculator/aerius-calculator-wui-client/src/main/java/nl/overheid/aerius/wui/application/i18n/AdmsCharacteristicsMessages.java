/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.i18n;

import com.google.gwt.i18n.client.Messages.Select;

import nl.overheid.aerius.shared.domain.v2.characteristics.adms.BuoyancyType;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.EffluxType;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.SourceType;
import nl.overheid.aerius.shared.domain.v2.source.road.ADMSRoadSideBarrierType;

/**
 * Text specific for ADMS characteristics.
 */
public interface AdmsCharacteristicsMessages {
  String sourceADMSBuoyancyTypeLabels();
  String sourceADMSBuoyancyTypeDetailLabel();
  String sourceADMSBuoyancyType(@Select BuoyancyType bt);
  String sourceADMSDensity();
  String sourceADMSDiameter();
  String sourceADMSEffluxTypeLabels();
  String sourceADMSEffluxType(@Select EffluxType et);
  String sourceADMSHeight();
  String sourceADMSHeightAtLeastHalfOfVerticalDimension();
  String sourceADMSHeightAtLeastHalfOfVerticalDimensionAndRangeBetween(double min, int max);
  String sourceADMSHeightVolumeSourceNotOnGroundWarning();
  String sourceADMSWidth();
  String sourceADMSMassFlux();
  String sourceADMSSourceTypeLabels();
  String sourceADMSSourceType(@Select SourceType st);
  String sourceADMSSourceTypeConfirmBuildingLinkSever(String buildingName);
  String sourceADMSSpecificHeatCapacity();
  String sourceADMSTemperature();
  String sourceADMSVerticalDimension();
  String sourceADMSVerticalVelocity();
  String sourceADMSVolumetricFlowRate();
  String sourceADMSElevationAngle();
  String sourceADMSHorizontalAngle();

  String sourceRoadWidthLabel();
  String sourceRoadElevationLabel();
  String sourceRoadGradientLabel();
  String sourceRoadTunnelLabel();
  String sourceRoadCoverageLabel();
  String sourceRoadBarrierWidthLabel();
  String sourceRoadBarrierMaximumHeightLabel();
  String sourceRoadBarrierAverageHeightLabel();
  String sourceRoadBarrierMinimumHeightLabel();
  String sourceRoadBarrierPorosityLabel();
  String sourceRoadBarrierLeftWidth();
  String sourceRoadBarrierLeftMaximumHeight();
  String sourceRoadBarrierLeftAverageHeight();
  String sourceRoadBarrierLeftMinimumHeight();
  String sourceRoadBarrierLeftPorosity();
  String sourceRoadBarrierRightWidth();
  String sourceRoadBarrierRightMaximumHeight();
  String sourceRoadBarrierRightAverageHeight();
  String sourceRoadBarrierRightMinimumHeight();
  String sourceRoadBarrierRightPorosity();
  String sourceAdmsRoadsideBarrierType(@Select ADMSRoadSideBarrierType type);
}
