/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.manual;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.google.web.bindery.event.shared.EventBus;

import elemental2.dom.DomGlobal;
import elemental2.dom.Element;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.directives.VectorDirective;
import nl.overheid.aerius.wui.application.command.misc.RequestExternalContentCommand;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ExternalContentContext;
import nl.overheid.aerius.wui.application.util.ManualUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    HTMLOutputComponent.class,
    InputErrorComponent.class,
}, directives = {
    VectorDirective.class,
})
public class ExternalContentComponent extends BasicVueComponent {
  @Inject @Data ExternalContentContext externalContent;

  @Prop String href;

  @Data String activePath;

  @Prop EventBus eventBus;

  @Inject @Data ApplicationContext appContext;

  @Watch(value = "href", isImmediate = true)
  public void onContentRefChange(final String newValue) {
    if (href != null) {
      navigate(newValue);
    }
  }

  @Computed
  public String getBase() {
    return ManualUtil.getManualUrl(appContext);
  }

  @Computed
  public String getContent() {
    return externalContent.getContent(activePath);
  }

  @Computed
  public boolean hasError() {
    return externalContent.getError(activePath) != null;
  }

  @JsMethod
  public void onScrollInternal(final String link) {
    final Element elem = DomGlobal.document.getElementById(link.replace("#", ""));
    if (elem != null) {
      elem.scrollIntoView();
      elem.focus();
    }
  }

  @JsMethod
  public void onNavigateRelative(final String link) {
    final String activeUrl = activePath;
    final String activePath = activeUrl.substring(0, activeUrl.lastIndexOf("/"));

    final String targetUrl = link;
    // Remove leading . if any
    final String derelativizedUrl = targetUrl.startsWith("./") ? targetUrl.substring(1) : targetUrl;

    final String ultimateUrl = activePath + derelativizedUrl;

    navigate(ultimateUrl);
  }

  private void navigate(final String href) {
    activePath = href;
    eventBus.fireEvent(new RequestExternalContentCommand(href));
  }
}
