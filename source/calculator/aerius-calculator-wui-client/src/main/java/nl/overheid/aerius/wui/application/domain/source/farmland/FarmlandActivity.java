/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.farmland;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.overheid.aerius.wui.application.domain.source.AbstractSubSource;
import nl.overheid.aerius.wui.vue.ReactivityUtil;
import nl.overheid.aerius.wui.vue.SkipReactivityValidations;

/**
 * Client side implementation of Farm land activity
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public abstract class FarmlandActivity extends AbstractSubSource {
  private String activityType;
  private String activityCode;

  public static final @JsOverlay void initFarmlandActivity(final FarmlandActivity props, final String activityCode, final FarmlandActivityType type) {
    AbstractSubSource.initAbstractSubSource(props);
    props.setActivityType(type);
    ReactivityUtil.ensureJsProperty(props, "activityCode", props::setActivityCode, activityCode);
  }

  @SkipReactivityValidations
  public static final @JsOverlay void initTypeDependent(final FarmlandActivity props, final FarmlandActivityType type) {
    switch (type) {
    case CUSTOM:
      CustomFarmlandActivity.init(Js.uncheckedCast(props), props.getActivityCode());
      break;
    case STANDARD:
      StandardFarmlandActivity.init(Js.uncheckedCast(props), props.getActivityCode());
      break;
    default:
      throw new RuntimeException("Unknown type: " + type);
    }
  }

  public static final @JsOverlay void copy(final FarmlandActivity from, final FarmlandActivity to) {
    to.setActivityCode(from.getActivityCode());
  }

  public final @JsOverlay FarmlandActivityType getActivityType() {
    return FarmlandActivityType.safeValueOf(activityType);
  }

  public final @JsOverlay void setActivityType(final FarmlandActivityType type) {
    this.activityType = type.name();
  }

  public final @JsOverlay String getActivityCode() {
    return activityCode;
  }

  public final @JsOverlay void setActivityCode(final String activityCode) {
    this.activityCode = activityCode;
  }

}
