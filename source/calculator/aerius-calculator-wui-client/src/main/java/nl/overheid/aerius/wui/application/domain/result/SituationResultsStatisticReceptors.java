/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.domain.result;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;

@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class SituationResultsStatisticReceptors implements JsPropertyMap<SituationResultsStatisticReceptor> {

  public final @JsOverlay SituationResultsStatisticReceptor getValue(final ResultStatisticType key) {
    return get(key.getJsonKey());
  }
}
