/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.nca.pages.timevarying.raw;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import nl.overheid.aerius.wui.application.domain.source.util.ThreeDayProfileUtil;
import nl.overheid.aerius.wui.application.domain.source.util.TimeVaryingProfileUtil;
import nl.overheid.aerius.wui.application.ui.nca.pages.timevarying.raw.TimeVaryingProfileInterpretException.Reason;

/**
 * Importer for raw diurnal profile tables.
 *
 * Constraints:
 *
 * <ul>
 * <li>Will separate on tab or komma</li>
 * <li>Will ignore empty lines or lines starting with !</li>
 * <li>Will import 24 or 25 lines (24 rows of data + 1 optional header)</li>
 * <li>Will import 3 or 4 columns (3 columns of data + 1 optional time column)</li>
 * <li>Will interpret headers containing: 'time|hour,weekday|weekdays,saturday|saturdays,sunday|sundays'</li>
 * <li>Will consider up to the first 2 characters of the time column</li>
 * <li>If there is no 0 in the time column, will import 1-24 as though it is 0-23</li>
 * <li>72 explicit values must ultimately be found for import to succeed</li>
 * </ul>
 */
public class DiurnalProfileRawInterpreter extends TimeVaryingProfileRawInterpreter<ThreeDayProfileUtil> {

  public static final DiurnalProfileRawInterpreter INSTANCE = new DiurnalProfileRawInterpreter();

  private static final String HEADER = "hour,weekdays,saturdays,sundays";
  private static final String COLUMN_TIME_OF_DAY = "hour";
  private static final String COLUMN_TIME_OF_DAY_VAR1 = "time";
  private static final String COLUMN_TIME_OF_DAY_VAR2 = "hour";
  private static final String COLUMN_SATURDAY = "saturday";
  private static final String COLUMN_SATURDAY_VAR1 = "saturday";
  private static final String COLUMN_SATURDAY_VAR2 = "saturdays";
  private static final String COLUMN_SUNDAY = "sunday";
  private static final String COLUMN_SUNDAY_VAR1 = "sunday";
  private static final String COLUMN_SUNDAY_VAR2 = "sundays";
  private static final String COLUMN_WEEKDAY = "weekday";
  private static final String COLUMN_WEEKDAY_VAR1 = "weekday";
  private static final String COLUMN_WEEKDAY_VAR2 = "weekdays";

  public DiurnalProfileRawInterpreter() {
    super(TimeVaryingProfileUtil.THREE_DAY_PROFILE_UTIL, HEADER, 24, 3);
  }

  @Override
  protected void tryInterpretLines(final boolean columnsWithName, final Map<String, Integer> headerConfig, final List<String[]> rawData,
      final List<Double> values) throws TimeVaryingProfileInterpretException {
    final int timeOfDayIdx = columnsWithName ? headerConfig.get(COLUMN_TIME_OF_DAY) : -1;
    final int saturdayIdx = headerConfig.get(COLUMN_SATURDAY);
    final int sundayIdx = headerConfig.get(COLUMN_SUNDAY);
    final int weekdayIdx = headerConfig.get(COLUMN_WEEKDAY);

    boolean zeroIndex = false;
    for (int i = 0; i < rawData.size(); i++) {
      final String[] line = rawData.get(i);
      final int hour = columnsWithName ? interpretHour(line[timeOfDayIdx]) : i;
      if (hour == 0) {
        zeroIndex = true;
        break;
      }
    }
    final Set<Integer> hours = IntStream.range(0, rawData.size()).boxed().collect(Collectors.toSet());

    for (int i = 0; i < rawData.size(); i++) {
      final String[] line = rawData.get(i);
      final int hour = (columnsWithName ? interpretHour(line[timeOfDayIdx]) : i)
          - (zeroIndex ? 0 : 1);
      hours.remove(hour);
      if (hour < 0 || hour >= ThreeDayProfileUtil.HOURS_IN_DAY) {
        throw new TimeVaryingProfileInterpretException(Reason.INCORRECT_HOUR_VALUE);
      }

      interpret(values, hour, ThreeDayProfileUtil.WEEKDAY, line[weekdayIdx]);
      interpret(values, hour, ThreeDayProfileUtil.SATURDAY, line[saturdayIdx]);
      interpret(values, hour, ThreeDayProfileUtil.SUNDAY, line[sundayIdx]);
    }
    if (columnsWithName && !hours.isEmpty()) {
      throw new TimeVaryingProfileInterpretException(Reason.NOT_ALL_TIMESLOTS_FOUND,
          hours.stream().map(String::valueOf).collect(Collectors.joining(", ")));
    }
  }

  private void interpret(final List<Double> values, final int hour, final int day, final String numberStr)
      throws TimeVaryingProfileInterpretException {
    try {
      final double num = Double.parseDouble(numberStr);

      util.setInput(values, day, hour, num);
    } catch (final NumberFormatException e) {
      throw new TimeVaryingProfileInterpretException(Reason.CANNOT_INTERPRET_VALUE, numberStr);
    }
  }

  private static int interpretHour(final String timeOfDay) throws TimeVaryingProfileInterpretException {
    try {
      return Integer.parseInt(timeOfDay.substring(0, Math.min(2, timeOfDay.length())));
    } catch (final NumberFormatException e) {
      throw new TimeVaryingProfileInterpretException(Reason.CANNOT_INTERPRET_TIME_OF_DAY);
    }
  }

  @Override
  protected void validateHeader(final List<String> header, final boolean hasRowName) throws TimeVaryingProfileInterpretException {
    if (hasRowName && !contains(header, COLUMN_TIME_OF_DAY_VAR1, COLUMN_TIME_OF_DAY_VAR2)) {
      throw new TimeVaryingProfileInterpretException(Reason.MISSING_HEADER_TIME_OF_DAY);
    }
    if (!contains(header, COLUMN_WEEKDAY_VAR1, COLUMN_WEEKDAY_VAR2)) {
      throw new TimeVaryingProfileInterpretException(Reason.MISSING_HEADER_WEEKDAY);
    }
    if (!contains(header, COLUMN_SATURDAY_VAR1, COLUMN_SATURDAY_VAR2)) {
      throw new TimeVaryingProfileInterpretException(Reason.MISSING_HEADER_SATURDAY);
    }
    if (!contains(header, COLUMN_SUNDAY_VAR1, COLUMN_SUNDAY_VAR2)) {
      throw new TimeVaryingProfileInterpretException(Reason.MISSING_HEADER_SUNDAY);
    }
  }

  @Override
  protected void putHeaderColumns(final Map<String, Integer> map, final List<String> header, final boolean hasRowName) {
    if (hasRowName) {
      map.put(COLUMN_TIME_OF_DAY, findFirst(header, COLUMN_TIME_OF_DAY_VAR1, COLUMN_TIME_OF_DAY_VAR2));
    }
    map.put(COLUMN_WEEKDAY, findFirst(header, COLUMN_WEEKDAY_VAR1, COLUMN_WEEKDAY_VAR2));
    map.put(COLUMN_SATURDAY, findFirst(header, COLUMN_SATURDAY_VAR1, COLUMN_SATURDAY_VAR2));
    map.put(COLUMN_SUNDAY, findFirst(header, COLUMN_SUNDAY_VAR1, COLUMN_SUNDAY_VAR2));
  }

  @Override
  protected Map<String, Integer> getDefaultHeaderConfig(final boolean hasRowName) {
    final int mod = hasRowName ? 1 : 0;
    final Map<String, Integer> map = new HashMap<>();

    if (hasRowName) {
      map.put(COLUMN_TIME_OF_DAY, 0);
    }
    map.put(COLUMN_WEEKDAY, mod);
    map.put(COLUMN_SATURDAY, 1 + mod);
    map.put(COLUMN_SUNDAY, 2 + mod);
    return map;
  }

  @Override
  protected void encode(final StringBuilder bldr, final List<Double> values, final int rowNumber) {
    bldr.append(rowNumber);
    bldr.append(SEPARATOR);
    bldr.append(util.getValue(values, ThreeDayProfileUtil.WEEKDAY, rowNumber));
    bldr.append(SEPARATOR);
    bldr.append(util.getValue(values, ThreeDayProfileUtil.SATURDAY, rowNumber));
    bldr.append(SEPARATOR);
    bldr.append(util.getValue(values, ThreeDayProfileUtil.SUNDAY, rowNumber));
  }
}
