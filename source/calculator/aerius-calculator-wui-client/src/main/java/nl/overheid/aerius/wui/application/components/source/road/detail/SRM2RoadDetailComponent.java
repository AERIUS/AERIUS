/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.road.detail;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategories;
import nl.overheid.aerius.shared.domain.sector.category.SimpleCategory;
import nl.overheid.aerius.srm2.conversion.Sigma0Calculator;
import nl.overheid.aerius.wui.application.components.divider.DividerComponent;
import nl.overheid.aerius.wui.application.components.source.DetailStatComponent;
import nl.overheid.aerius.wui.application.components.source.characteristics.MinMaxLabelComponent;
import nl.overheid.aerius.wui.application.components.source.road.vehicles.RoadVehiclesDetailComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.SRM2RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.road.SRM2RoadSideBarrier;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "aer-srm2road-detail", components = {
    VerticalCollapse.class,
    RoadVehiclesDetailComponent.class,
    MinMaxLabelComponent.class,
    DetailStatComponent.class,
    DividerComponent.class
})
public class SRM2RoadDetailComponent extends BasicVueComponent {
  @Data @Inject ApplicationContext applicationContext;

  @Prop @JsProperty protected SRM2RoadESFeature source;
  @Prop String totalEmissionTitle;

  public RoadEmissionCategories getRoadEmissionCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getRoadEmissionCategories();
  }

  @Computed
  public boolean showArea() {
    return getRoadEmissionCategories().getAreas().size() > 1;
  }

  @Computed("substances")
  public List<Substance> getSubstances() {
    return applicationContext.getConfiguration().getEmissionSubstancesRoad();
  }

  @Computed
  public String getRoadAreaDescription() {
    final SimpleCategory areaCategory = getRoadEmissionCategories().getArea(source.getRoadAreaCode());
    return areaCategory == null ? "" : areaCategory.getDescription();
  }

  @Computed
  public String getRoadTypeDescription() {
    final SimpleCategory typeCategory = getRoadEmissionCategories().getRoadType(source.getRoadTypeCode());
    return typeCategory == null ? "" : typeCategory.getDescription();
  }

  @JsMethod
  protected String barrierType(final SRM2RoadSideBarrier barrier) {
    return barrier == null ? "-" : i18n.esRoadBarrierType(barrier.getBarrierType());
  }

  @JsMethod
  public String barrierHeight(final SRM2RoadSideBarrier barrier) {
    return barrier == null ? "-" : i18n.unitM(i18n.decimalNumberFixed(barrier.getHeight(), 1));
  }

  @JsMethod
  public String barrierHeightCorrected(final SRM2RoadSideBarrier barrier) {
    if (barrier == null) {
      return "-";
    }

    final double usedValue = barrier.getHeight() < Sigma0Calculator.MIN_BARRIER_HEIGHT
        ? 0D
        : Math.min(Sigma0Calculator.MAX_BARRIER_HEIGHT, barrier.getHeight());
    return i18n.unitM(i18n.decimalNumberFixed(usedValue, 1));
  }

  @JsMethod
  public String barrierDistance(final SRM2RoadSideBarrier barrier) {
    return barrier == null ? "-" : i18n.unitM(i18n.decimalNumberFixed(barrier.getDistance(), 1));
  }

  @JsMethod
  public String barrierDistanceCorrected(final SRM2RoadSideBarrier barrier) {
    return barrier == null || barrier.getDistance() > Sigma0Calculator.MAX_BARRIER_DISTANCE
        ? "-"
        : i18n.unitM(i18n.decimalNumberFixed(barrier.getDistance(), 1));
  }

  @JsMethod
  public String elevationHeightCorrected(final int elevationHeight) {
    final double usedValue = elevationHeight < Sigma0Calculator.MIN_ROAD_HEIGHT
        ? Sigma0Calculator.MIN_ROAD_HEIGHT
        : Math.min(Sigma0Calculator.MAX_ROAD_HEIGHT, elevationHeight);
    return i18n.unitM(i18n.decimalNumberFixed(usedValue, 0));
  }
}
