/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.event.source;

import com.google.gwt.user.client.rpc.AsyncCallback;

import ol.geom.Geometry;

import nl.aerius.wui.event.SimpleGenericEvent;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;

public class EmissionSourceModifyGeometryCommand extends SimpleGenericEvent<EmissionSourceFeature> {
  private final AsyncCallback<Geometry> changeCallback;

  public EmissionSourceModifyGeometryCommand() {
    this(null, null);
  }

  public EmissionSourceModifyGeometryCommand(final EmissionSourceFeature source, final AsyncCallback<Geometry> callback) {
    super(source);
    changeCallback = callback;
  }

  public AsyncCallback<Geometry> getGeometryCallback() {
    return changeCallback;
  }
}
