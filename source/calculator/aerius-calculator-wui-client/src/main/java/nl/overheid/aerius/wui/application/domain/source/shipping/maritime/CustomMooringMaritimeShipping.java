/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.shipping.maritime;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.wui.vue.ReactivityUtil;

/**
 * Client side implementation of CustomMooringMaritimeShipping props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class CustomMooringMaritimeShipping extends MooringMaritimeShipping {

  private CustomMaritimeShippingEmissionProperties emissionProperties;

  public static final @JsOverlay CustomMooringMaritimeShipping create() {
    final CustomMooringMaritimeShipping props = Js.uncheckedCast(JsPropertyMap.of());
    init(props);
    return props;
  }

  public static final @JsOverlay void init(final CustomMooringMaritimeShipping props) {
    MooringMaritimeShipping.init(props, MooringMaritimeShippingType.CUSTOM);
    ReactivityUtil.ensureJsPropertySupplied(props, "emissionProperties", props::setEmissionProperties, CustomMaritimeShippingEmissionProperties::create);
    ReactivityUtil.ensureInitialized(props::getEmissionProperties, CustomMaritimeShippingEmissionProperties::init);
  }

  public final @JsOverlay CustomMaritimeShippingEmissionProperties getEmissionProperties() {
    return emissionProperties;
  }

  public final @JsOverlay void setEmissionProperties(final CustomMaritimeShippingEmissionProperties emissionProperties) {
    this.emissionProperties = emissionProperties;
  }

}
