/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.shipping.inland.inland;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.inland.inland.InlandStandardValidators.InlandStandardValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.shipping.inland.StandardInlandShipping;
import nl.overheid.aerius.wui.vue.DebugDirective;

@Component(name = "aer-inland-standard", customizeOptions = {
    InlandStandardValidators.class
}, directives = {
    ValidateDirective.class,
    DebugDirective.class
}, components = {
    LabeledInputComponent.class,
    InlandMovementComponent.class,
    ValidationBehaviour.class
})
public class InlandStandardEmissionComponent extends ErrorWarningValidator implements HasCreated,
    HasValidators<InlandStandardValidations> {
  @Prop @JsProperty StandardInlandShipping standardInland;
  @Prop @JsProperty String waterwayCode;
  @Data String descriptionV;
  @Data String shipCodeV;

  @Data @Inject ApplicationContext applicationContext;

  @JsProperty(name = "$v") InlandStandardValidations validation;

  @Watch(value = "standardInland", isImmediate = true)
  public void onStandardInlandChange() {
    descriptionV = standardInland.getDescription();
    shipCodeV = standardInland.getShipCode();
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Computed
  public InlandStandardValidations getV() {
    return validation;
  }

  @Computed
  protected String getDescription() {
    return standardInland.getDescription();
  }

  @Computed
  protected void setDescription(final String description) {
    standardInland.setDescription(description);
    this.descriptionV = description;
  }

  @Computed
  public List<InlandShippingCategory> getShipCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getInlandShippingCategories().getShipCategories();
  }

  @Computed
  public String getInlandShippingCategory() {
    return shipCodeV;
  }

  @Computed
  public void setInlandShippingCategory(final String shipCode) {
    standardInland.setShipCode(shipCode);
    shipCodeV = shipCode;
  }

  @Computed
  protected boolean getInvalidShipWaterwayCombo() {
    return shipCodeV != null && !isValidShipWaterwayCombo();
  }

  @Computed("isInvalid")
  protected boolean isInvalid() {
    return validation.descriptionV.invalid || validation.shipCodeV.invalid || !isValidShipWaterwayCombo();
  }

  @JsMethod
  protected String descriptionError() {
    return i18n.errorDescriptionRequired();
  }

  @JsMethod
  protected String shipCategoryError() {
    return isValidShipWaterwayCombo()
        ? i18n.errorShipCategoryRequired()
        : i18n.errorShipTypeWaterwayCombination();
  }

  private boolean isValidShipWaterwayCombo() {
    return applicationContext.getConfiguration().getSectorCategories().getInlandShippingCategories()
        .isValidCombinationByCodes(shipCodeV, waterwayCode);
  }
}
