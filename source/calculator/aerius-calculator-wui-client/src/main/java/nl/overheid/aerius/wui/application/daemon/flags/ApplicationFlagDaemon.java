/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.flags;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;

import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.util.Notification;
import nl.aerius.wui.util.NotificationUtil;
import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.config.ApplicationFlags;

@Singleton
public class ApplicationFlagDaemon extends BasicEventComponent implements Daemon {
  private @Inject ApplicationFlags flags;
  private @Inject ApplicationContext appContext;

  public void initFlags() {
    final Map<String, List<String>> queryMap = Window.Location.getParameterMap().entrySet().stream()
        .collect(Collectors.toMap(e -> e.getKey().toLowerCase(Locale.ROOT), e -> e.getValue()));

    initFlagBoolean(queryMap, "internal", AppFlag.ENABLE_INTERNAL, v -> flags.setInternal(v),
        Notification.Type.MESSAGE);
    initFlagBoolean(queryMap, "ops", AppFlag.ENABLE_OPS_EXPORT, v -> flags.setOpsExport(v),
        Notification.Type.MESSAGE);
    initFlagBoolean(queryMap, "adms", AppFlag.ENABLE_ADMS_EXPORT, v -> flags.setAdmsExport(v),
        Notification.Type.MESSAGE);
    initFlagBoolean(queryMap, "demo", AppFlag.ENABLE_DEMO_CALCULATION, v -> flags.setDemoMode(v),
        Notification.Type.PERSISTENT_MESSAGE);
    initFlag(queryMap, "jobkey", v -> flags.setJobKey(v));
    initDeveloperPanel(queryMap);
  }

  private void initFlagBoolean(final Map<String, List<String>> queryMap, final String key, final AppFlag appThemeFlag,
      final Consumer<Boolean> consumer, final Notification.Type type) {
    final boolean contains = queryMap.containsKey(key);
    if (appContext.isAppFlagEnabled(appThemeFlag)) {
      if (contains) {
        NotificationUtil.broadcast(eventBus, new Notification(M.messages().flagActivated(key), type));
      }
      consumer.accept(contains);
    }
  }

  private static void initFlag(final Map<String, List<String>> queryMap, final String key, final Consumer<String> consumer) {
    final List<String> value = queryMap.get(key);

    if (value != null && !value.isEmpty()) {
      consumer.accept(value.get(0));
    }
  }

  private void initDeveloperPanel(final Map<String, List<String>> queryMap) {
    final Date tomorrowDate = new Date();
    final long nowLong = tomorrowDate.getTime();

    flags.setDeveloperPanel((queryMap.containsKey("dev")
        || LocalStorageUtil.tryGet("dev")
            .map(v -> Long.parseLong(v))
            .map(v -> v > nowLong)
            .orElse(false))
        && GWT.getHostPageBaseURL().contains("localhost"));

    if (flags.hasDeveloperPanel()) {
      final long tomorrowLong = nowLong + 1000L * 60 * 60 * 24; // one day
      LocalStorageUtil.trySet("dev", tomorrowLong);
    }
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus);
  }
}
