/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.building;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasDestroyed;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.format.Wkt;
import ol.geom.Circle;
import ol.geom.Geometry;
import ol.geom.GeometryType;
import ol.geom.Point;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.geo.wui.util.OL3GeometryUtil;
import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.domain.v2.building.BuildingLimits;
import nl.overheid.aerius.wui.application.command.building.BuildingDrawGeometryCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingModifyGeometryCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.components.tooltip.TooltipComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.event.building.BuildingDrawGeometryEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingEndModifyGeometryEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingStartDrawGeometryEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingStartModifyGeometryEvent;
import nl.overheid.aerius.wui.application.geo.util.GeoType;
import nl.overheid.aerius.wui.application.geo.util.GeoUtil;
import nl.overheid.aerius.wui.application.geo.util.OrientedEnvelope;
import nl.overheid.aerius.wui.application.ui.pages.building.BuildingLocationEditorValidators.BuildingLocationEditorValidations;
import nl.overheid.aerius.wui.application.ui.pages.emissionsource.SmartWktInputComponent;
import nl.overheid.aerius.wui.application.util.NumberUtil;

@Component(customizeOptions = {
    BuildingLocationEditorValidators.class
}, components = {
    BuildingEnvelopeComponent.class,
    TooltipComponent.class,
    LabeledInputComponent.class,
    ButtonIcon.class,
    VerticalCollapse.class,
    ValidationBehaviour.class,
    SmartWktInputComponent.class
}, directives = {
    ValidateDirective.class
})
public class BuildingLocationEditorComponent extends ErrorWarningValidator implements IsVueComponent, HasCreated, HasDestroyed {
  private static final BuildingLocationEditorComponentEventBinder EVENT_BINDER = GWT.create(BuildingLocationEditorComponentEventBinder.class);

  interface BuildingLocationEditorComponentEventBinder extends EventBinder<BuildingLocationEditorComponent> {}

  private static final Wkt WKT = new Wkt();

  @Inject ApplicationContext applicationContext;

  @Prop BuildingLimits buildingLimits;
  @Prop BuildingFeature building;
  @Prop EventBus eventBus;

  @Data String bagSearchQuery;

  @Data String wktGeometryV;

  @Data GeometryType geometryType;

  @Data String heightV;
  @Data String diameterV;
  @JsProperty(name = "$v") BuildingLocationEditorValidations validation;

  private boolean embargo;
  private HandlerRegistration registration;

  @Data Geometry drawGeometry;

  @Computed
  public BuildingLocationEditorValidations getV() {
    return validation;
  }

  @Computed("isCircularBuildingSupported")
  public boolean isCircularBuildingSupported() {
    return buildingLimits.isCircularBuildingSupported();
  }

  @Computed("buildingDiameterPrecision")
  public double buildingDiameterPrecision() {
    return Math.pow(10, -buildingLimits.buildingDigitsPrecision());
  }

  @Computed("buildingHeightPrecision")
  public double buildingHeightPrecision() {
    return Math.pow(10, -buildingLimits.buildingDigitsPrecision());
  }

  @Computed
  public List<GeoType> getValidGeometryTypes() {
    if (isCircularBuildingSupported()) {
      return Arrays.asList(GeoType.POINT, GeoType.POLYGON);
    }
    return Arrays.asList(GeoType.POLYGON);
  }

  @Computed
  public GeometryType getWktInputGeometryType() {
    if (GeometryType.Circle == geometryType) {
      return GeometryType.Point;
    }
    return geometryType;
  }

  @Computed
  public Geometry getWktInputGeometry() {
    final Geometry geometry = building.getGeometry();
    if (GeoType.CIRCLE.is(geometry)) {
      return new Point(((Circle) geometry).getCenter());
    }
    return building.getGeometry();
  }

  @JsMethod
  public void setWktInputGeometry(Geometry geometry) {
    drawGeometry = geometry;
    if (GeoType.POINT.is(geometry)) {
      geometry = new Circle(((Point) geometry).getCoordinates(), Optional.ofNullable(building.getDiameter()).map(d -> d / 2.0).orElse(0.0));
    }

    if (embargo) {
      updateValidity();
      return;
    }

    building.setGeometry(geometry);
    eventBus.fireEvent(new BuildingDrawGeometryEvent(geometry));
  }

  @EventHandler
  public void onBuildingStartModifyGeometryEvent(final BuildingStartModifyGeometryEvent e) {
    embargo = true;
  }

  @EventHandler
  public void onBuildingEndModifyGeometryEvent(final BuildingEndModifyGeometryEvent e) {
    embargo = false;
  }

  @EventHandler
  public void onBuildingStartDrawGeometryEvent(final BuildingStartDrawGeometryEvent e) {
    drawGeometry = e.getValue();
  }

  @Watch(value = "building", isImmediate = true, isDeep = true)
  public void onBuildingChangeDeep(final BuildingFeature neww) {
    updateBuilding(neww);
  }

  @Watch(value = "building", isImmediate = true)
  public void onBuildingChange(final BuildingFeature neww) {
    if (updateBuilding(neww)) {
      eventBus.fireEvent(new BuildingModifyGeometryCommand(neww));
      eventBus.fireEvent(new BuildingDrawGeometryCommand(geometryType));
    }
  }

  private boolean updateBuilding(final BuildingFeature neww) {
    if (neww == null) {
      return false;
    }
    if (geometryType == null) {
      geometryType = isCircularBuildingSupported() && neww.getGeometry() != null && GeometryType.Point.name().equals(neww.getGeometry().getType())
          ? GeometryType.Circle
          : GeometryType.Polygon;
    }
    heightV = String.valueOf(neww.getHeight());
    diameterV = neww.getDiameter() == null ? "" : String.valueOf(neww.getDiameter());
    updateGeometry(neww.getGeometry());
    return true;
  }

  private void updateGeometry(final Geometry geometry) {
    if (geometry == null) {
      wktGeometryV = null;
      drawGeometry = null;
    } else {
      wktGeometryV = OL3GeometryUtil.toWktString(geometry);
      drawGeometry = WKT.readGeometry(wktGeometryV);
    }

    updateValidity();
  }

  @Computed
  protected String getDiameter() {
    return diameterV;
  }

  @Computed
  protected void setDiameter(final String diameter) {
    ValidationUtil.setSafeDoubleValue(v -> {
      // Only set diameter if the geometry is actually a circle (this covers the case
      // where the user selects the circle tool, but has not yet created a geometry)
      if (GeoType.POINT.is(building.getGeometry())) {
        building.setDiameter(v);
        eventBus.fireEvent(new BuildingDrawGeometryEvent(new Circle(((Point) building.getGeometry()).getFirstCoordinate(),
            building.getDiameter() / 2.0)));
      }
    }, diameter, 0D);
    diameterV = diameter;
  }

  @Computed
  protected String getBuildingHeight() {
    return heightV;
  }

  @Computed
  protected void setBuildingHeight(final String height) {
    ValidationUtil.setSafeDoubleValue(v -> building.setHeight(v), height, 0D);
    heightV = height;
  }

  @JsMethod
  public boolean isValidWktGeometry() {
    try {
      final Geometry geometry = getWktInputGeometry();

      return isCircularBuilding() && GeoType.POINT.is(geometry)
          || isPolygonBuilding() && GeoType.POLYGON.is(geometry);
    } catch (final Exception e) {
      return false;
    }
  }

  @JsMethod
  public boolean isValidGeometry() {
    try {
      final OrientedEnvelope envelope = GeoUtil.determineOrientedEnvelope(building.getGeometry());

      // A null envelope is not valid
      return envelope != null;
    } catch (final Exception e) {
      return false;
    }
  }

  private void updateValidity() {
    final boolean valid = isValidWktGeometry()
        && isValidGeometry()
        && !validation.invalid;

    vue().$emit("validity-change", valid);
  }

  @Computed
  public String getGeometryInvalidErrorMessage() {
    return i18n.geometryInvalid(applicationContext.getTheme(), geometryType == null ? null : geometryType);
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
    registration = EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Override
  public void destroyed() {
    registration.removeHandler();
  }

  @JsMethod
  public void addCircleBuilding() {
    // Do nothing if already drawing circle
    if (geometryType == GeometryType.Circle) {
      return;
    }

    geometryType = GeometryType.Circle;
    resetBuilding();
  }

  @JsMethod
  public void addPolygonBuilding() {
    // Do nothing if already drawing polygon
    if (geometryType == GeometryType.Polygon) {
      return;
    }

    geometryType = GeometryType.Polygon;
    resetBuilding();
  }

  private void resetBuilding() {
    diameterV = "";
    building.setDiameter(null);
    building.setGeometry(null);
    eventBus.fireEvent(new BuildingDrawGeometryCommand(geometryType));
    eventBus.fireEvent(new BuildingModifyGeometryCommand(building));
  }

  @JsMethod
  protected String getHeightValidationWarning() {
    return i18n.buildingWarningHeight(heightV);
  }

  @Computed("hasHeightWarning")
  protected boolean hasHeightWarning() {
    return !isHeightInvalid()
        // If HAS_BUILDING_MINIMUM_HEIGHT_LARGER_ZERO is not present, then we present an error instead
        && !applicationContext.isAppFlagEnabled(AppFlag.HAS_BUILDING_MINIMUM_HEIGHT_LARGER_ZERO)
        && NumberUtil.equalExactly(building.getHeight(), buildingLimits.buildingHeightMinimum());
  }

  @Computed("isHeightInvalid")
  protected boolean isHeightInvalid() {
    return validation.heightV.invalid ||
        (applicationContext.isAppFlagEnabled(AppFlag.HAS_BUILDING_MINIMUM_HEIGHT_LARGER_ZERO)
            ? (building.getHeight() <= buildingLimits.buildingHeightMinimum() || building.getHeight() > buildingLimits.buildingHeightMaximum())
            : (building.getHeight() < buildingLimits.buildingHeightMinimum()));
  }

  @JsMethod
  protected String getHeightValidationError() {
    return applicationContext.isAppFlagEnabled(AppFlag.HAS_BUILDING_MINIMUM_HEIGHT_LARGER_ZERO)
        ? i18n.ivDoubleRangeBetween(i18n.buildingLocationHeight(), buildingLimits.buildingHeightMinimum(), buildingLimits.buildingHeightMaximum())
        : i18n.ivDoubleLowerlimit(i18n.buildingLocationHeight(), buildingLimits.buildingHeightMinimum());
  }

  @Computed("isDiameterInvalid")
  protected boolean isDiameterInvalid() {
    return isCircularBuilding()
        && (building.getDiameter() == null
            || building.getDiameter() < buildingLimits.buildingDiameterMinimum()
            || building.getDiameter() > buildingLimits.buildingDiameterMaximum());
  }

  @JsMethod
  protected String getDiameterValidationError() {
    return i18n.ivDoubleRangeBetween(i18n.buildingLocationDiameter(), buildingLimits.buildingDiameterMinimum(),
        buildingLimits.buildingDiameterMaximum());
  }

  @Computed("showGeometryError")
  public boolean showGeometryError() {
    return wktGeometryV != null && !wktGeometryV.isEmpty() && !isValidWktGeometry();
  }

  @Computed("isCircularBuilding")
  public boolean isCircularBuilding() {
    return geometryType == GeometryType.Circle;
  }

  @Computed("isPolygonBuilding")
  public boolean isPolygonBuilding() {
    return geometryType == GeometryType.Polygon;
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return validation.invalid
        || !isValidWktGeometry()
        || !isValidGeometry()
        || isHeightInvalid()
        || isDiameterInvalid();
  }
}
