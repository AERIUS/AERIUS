/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.command.source;

import nl.aerius.wui.command.SimpleGenericCommand;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceAddedEvent;

/**
 * Command to add a new {@link EmissionSourceFeature} object.
 */
public class EmissionSourceAddCommand extends SimpleGenericCommand<EmissionSourceFeature, EmissionSourceAddedEvent> {
  private SituationContext situation;

  public EmissionSourceAddCommand(final EmissionSourceFeature emissionSource, final SituationContext situation) {
    super(emissionSource);
    this.situation = situation;
  }

  public SituationContext getSituation() {
    return situation;
  }

  public void setSituation(final SituationContext situationContext) {
    this.situation = situationContext;
  }

  @Override
  protected EmissionSourceAddedEvent createEvent(final EmissionSourceFeature value) {
    return new EmissionSourceAddedEvent(value, situation);
  }

}
