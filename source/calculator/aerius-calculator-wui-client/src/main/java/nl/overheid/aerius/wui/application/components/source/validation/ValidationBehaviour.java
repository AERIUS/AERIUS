/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.validation;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasRender;
import com.axellience.vuegwt.core.client.vnode.VNode;
import com.axellience.vuegwt.core.client.vnode.builder.VNodeBuilder;

import nl.aerius.wui.dev.GWTProd;
import nl.overheid.aerius.wui.application.context.GenericValidationContext;

/**
 * A simple component to help in determining and propagating warning/error
 * states
 *
 * Part of this component is a hack to circumvent a rare but otherwise
 * irresolvable problem where validators are not yet initialized when the
 * validators are computed or watched. This happens because immediate watchers
 * are called before created(), and the validators are only initialized in
 * created() and made reactive thereafter.
 *
 * There is no hook (that I'm aware of) that is triggered before the immediate
 * watchers are called, which might otherwise be used to initialize the
 * validators.
 *
 * This component circumvents the problem described above, because the props in
 * this component are set after the parent component's created() method has been
 * passed, thus the computed conditions are determined when the validators have
 * been initialized, and all code executes as one might intuit.
 *
 * Intended usage:
 *
 * <pre>
 *   &lt;validation-behaviour
 *     :warnings="warningCondition1 || warningCondition2"
 *     &#64;warnings="emitWarnings((boolean) $event)"
 *     :errors="errorCondition1 || errorCondition2"
 *     &#64;errors="emitErrors((boolean) $event)" /&gt;
 * </pre>
 */
@Component(hasTemplate = false)
public class ValidationBehaviour extends ErrorWarningValidator implements HasRender, IsVueComponent {
  @Prop boolean errors;
  @Prop boolean warnings;

  @Prop boolean debug = false;

  @Inject @Data GenericValidationContext validationContext;

  @PropDefault("debug")
  boolean debugDefault() {
    return false;
  }

  @Computed
  public boolean isDisplayFormProblems() {
    return validationContext.isDisplayFormProblems();
  }

  @Watch(value = "isDisplayFormProblems()", isImmediate = true)
  public void onDisplayFormProblemsChange(final boolean neww) {
    if (debug) {
      GWTProd.log("displaying problems: " + neww);
    }

    vue().$emit("display-problems", neww);
  }

  @PropDefault("errors")
  boolean errorsDefault() {
    return false;
  }

  @PropDefault("warnings")
  boolean warningsDefault() {
    return false;
  }

  @Watch(value = "errors", isImmediate = true)
  public void onErrorsChange(final boolean neww) {
    if (debug) {
      GWTProd.log("onErrorsChange: " + neww + " (emitting)");
    }

    emitErrors(neww);
  }

  @Watch(value = "warnings", isImmediate = true)
  public void onWarningsChange(final boolean neww) {
    emitWarnings(neww);
  }

  @Override
  public VNode render(final VNodeBuilder builder) {
    return builder.el();
  }
}
