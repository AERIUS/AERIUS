/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.road.srm2;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadElevation;
import nl.overheid.aerius.srm2.conversion.Sigma0Calculator;
import nl.overheid.aerius.wui.application.components.input.CheckBoxComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.road.srm2.SRM2CharacteristicsEditorValidators.SRM2CharacteristicsValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.domain.source.SRM2RoadESFeature;

@Component(customizeOptions = {
    SRM2CharacteristicsEditorValidators.class,
}, directives = {
    ValidateDirective.class,
}, components = {
    LabeledInputComponent.class,
    CheckBoxComponent.class,
    SRM2BarrierEditorComponent.class,
    ValidationBehaviour.class,
    VerticalCollapse.class,
})
public class SRM2CharacteristicsEditorComponent extends ErrorWarningValidator implements HasCreated, HasValidators<SRM2CharacteristicsValidations> {
  @Prop SRM2RoadESFeature source;
  @Data boolean tunnelFactorApplied;

  @Data String tunnelFactorV;
  @Data String porosityV;
  @Data String elevationHeightWarningV;
  @Data String elevationHeightV;

  @JsProperty(name = "$v") SRM2CharacteristicsValidations validation;

  @Override
  @Computed
  public SRM2CharacteristicsValidations getV() {
    return validation;
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Computed("porosityEnabled")
  public boolean isPorosityEnabled() {
    // TODO Determine enabling condition (probably theme-based)
    return false;
  }

  @Watch(value = "source", isImmediate = true)
  public void onSourceValueChange(final boolean neww, final boolean old) {
    tunnelFactorApplied = Double.compare(source.getTunnelFactor(), 1d) != 0;
    tunnelFactorV = String.valueOf(source.getTunnelFactor());
    porosityV = String.valueOf(source.getPorosity());
    elevationHeightWarningV = String.valueOf(source.getElevationHeight());
    elevationHeightV = String.valueOf(source.getElevationHeight());
  }

  @Watch(value = "tunnelFactorApplied", isImmediate = true)
  public void onTunnelFactorAppliedValueChange(final boolean neww, final boolean old) {
    if (!neww) {
      setTunnelFactor("1");
    }
  }

  @Computed
  protected String getTunnelFactor() {
    return tunnelFactorV;
  }

  @Computed
  protected void setTunnelFactor(final String tunnelFactor) {
    ValidationUtil.setSafeDoubleValue(source::setTunnelFactor, tunnelFactor, 0.0);
    tunnelFactorV = tunnelFactor;
  }

  @Computed
  protected String getPorosity() {
    return porosityV;
  }

  @Computed
  protected void setPorosity(final String porosity) {
    ValidationUtil.setSafeDoubleValue(v -> source.setPorosity(v), porosity, 0.0);
    porosityV = porosity;
  }

  protected String tunnelFactorError() {
    return i18n.ivDoubleRangeBetween(i18n.sourceRoadTunnelFactorLabel(),
        SRM2CharacteristicsEditorValidators.TUNNEL_FACTOR_MINIMUM,
        SRM2CharacteristicsEditorValidators.TUNNEL_FACTOR_MAXIMUM);
  }

  protected boolean isRoadElevationDefault() {
    return RoadElevation.NORMAL.equals(source.getElevation());
  }

  @Computed
  protected String getRoadElevationType() {
    return source.getElevation().name();
  }

  @Computed
  protected void setRoadElevationType(final String elevation) {
    source.setElevation(RoadElevation.valueOf(elevation));
    if (isRoadElevationDefault()) {
      setElevationHeight("0");
    }
  }

  @Computed
  protected String getElevationHeight() {
    return elevationHeightV;
  }

  @Computed
  protected void setElevationHeight(final String elevationHeight) {
    ValidationUtil.setSafeIntegerValue(v -> source.setElevationHeight(v), elevationHeight, 0);
    elevationHeightV = elevationHeight;
    elevationHeightWarningV = elevationHeight;
  }

  @JsMethod
  protected String getElevationHeightWarning() {
    return i18n.warningElevationHeight(Sigma0Calculator.MIN_ROAD_HEIGHT, Sigma0Calculator.MAX_ROAD_HEIGHT);
  }

  protected String porosityError() {
    return i18n.ivDoubleLowerlimit(i18n.sourceRoadPorositeit(), 0d);
  }

  protected String elevationHeightError() {
    return i18n.ivInteger(i18n.esRoadDetailElevationHeight());
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return tunnelFactorApplied && validation.tunnelFactorV.invalid
        || validation.porosityV.invalid || validation.elevationHeightV.invalid;
  }

  @Computed("isElevationHeightWarning")
  public boolean isElevationHeightWarning() {
    return !isElevationHeightError() && validation.elevationHeightWarningV.invalid;
  }

  @Computed("isElevationHeightError")
  public boolean isElevationHeightError() {
    return validation.elevationHeightV.invalid;
  }

  @Computed("isElevationHeightEnabled")
  public boolean isElevationHeightEnabled() {
    return isRoadElevationDefault();
  }
}
