/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.shipping.base;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.wui.application.domain.source.AbstractSubSource;
import nl.overheid.aerius.wui.vue.ReactivityUtil;

/**
 * Client side implementation of AbstractShipping props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public abstract class AbstractShipping extends AbstractSubSource {
  private String description;

  public static final @JsOverlay void initAbstractShipping(final AbstractShipping shipping) {
    AbstractSubSource.initAbstractSubSource(shipping);
    ReactivityUtil.ensureJsPropertyAsNull(shipping, "description", shipping::setDescription);
  }

  public final @JsOverlay String getDescription() {
    return description;
  }

  public final @JsOverlay void setDescription(final String description) {
    this.description = description;
  }

}
