/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.progress;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.wui.application.domain.calculation.JobProgress;
import nl.overheid.aerius.wui.application.i18n.ApplicationMessages;
import nl.overheid.aerius.wui.application.i18n.M;

/**
 * Util class to get the progress text message.
 */
public final class JobProgressTextUtil {
  private static final ApplicationMessages i18n = M.messages();

  private static final double SECONDS_PER_MINUTE = 60;

  private static final double SECONDS_PER_HOUR = SECONDS_PER_MINUTE * 60;

  private JobProgressTextUtil() {
    // Util class
  }

  /**
   * Text of the job progress, including queuing, calculation progress and other steps.
   * @param jobProgress
   * @param theme The theme of this application
   * @return
   */
  public static String getJobProgressText(final JobProgress jobProgress, final Theme theme) {
    final String text;

    if (jobProgress == null) {
      text = i18n.calculationStateDescription(JobState.UNDEFINED);
    } else if (jobProgress.getState() == JobState.CALCULATING) {
      text = remaining(jobProgress.getEstimatedRemainingCalculationTime());
    } else if (jobProgress.getState() == JobState.QUEUING) {
      text = i18n.calculationStateDescriptionQueuing(jobProgress.getQueuePosition());
    } else if (jobProgress.getState() == JobState.POST_PROCESSING) {
      text = i18n.calculationStateDescriptionPostProcessing(theme);
    } else {
      text = i18n.calculationStateDescription(jobProgress.getState());
    }
    return text;
  }

  /**
   * Text displaying the remaining calculation time.
   *
   * @param remainingTime
   * @return
   */
  private static String remaining(final int remainingTime) {
    final String text;

    if (remainingTime < 0) {
      text = i18n.calculationStateDescriptionCalculatingEstimating();
    } else if (remainingTime <= SECONDS_PER_MINUTE) {
      text = i18n.calculationStateDescriptionCalculatingAlmost();
    } else if (remainingTime <= SECONDS_PER_HOUR) {
      text = i18n.calculationStateDescriptionCalculating((int) Math.ceil(remainingTime / SECONDS_PER_MINUTE));
    } else {
      final int hours = (int) Math.floor(remainingTime / SECONDS_PER_HOUR);
      final int minutes = (int) Math.ceil( (remainingTime - hours * SECONDS_PER_HOUR) / SECONDS_PER_MINUTE);
      text = i18n.calculationStateDescriptionCalculatingLong(hours, minutes);
    }
    return text;
  }
}
