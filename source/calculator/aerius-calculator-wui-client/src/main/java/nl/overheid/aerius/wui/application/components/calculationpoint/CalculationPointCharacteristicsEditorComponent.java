/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.calculationpoint;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.NcaPointHeightSupplier;
import nl.overheid.aerius.shared.domain.v2.point.AssessmentCategory;
import nl.overheid.aerius.wui.application.components.calculationpoint.CalculationPointCharacteristicsEditorValidators.CalculationPointCharacteristicsEditorValidations;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.NcaCustomCalculationPointFeature;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.util.NumberUtil;

@Component(customizeOptions = {
    CalculationPointCharacteristicsEditorValidators.class
}, directives = {
    ValidateDirective.class
}, components = {
    LabeledInputComponent.class,
    SimplifiedListBoxComponent.class,
    ValidationBehaviour.class,
    InputErrorComponent.class,
})
public class CalculationPointCharacteristicsEditorComponent extends ErrorWarningValidator
    implements HasCreated, HasValidators<CalculationPointCharacteristicsEditorValidations> {
  @Inject @Data ApplicationContext applicationContext;

  @Prop @Data NcaCustomCalculationPointFeature calculationPoint;
  @Prop boolean showErrors;

  // Validations
  @Data String heightV;
  @Data String roadLocalFractionNO2V;
  @JsProperty(name = "$v") CalculationPointCharacteristicsEditorValidations validation;

  @Watch(value = "calculationPoint", isImmediate = true)
  public void onCalculationPointChange(final NcaCustomCalculationPointFeature neww) {
    if (neww == null) {
      return;
    }

    heightV = computeHeightV(neww);
    roadLocalFractionNO2V = neww.getRoadLocalFractionNO2() == null ? null : String.valueOf(neww.getRoadLocalFractionNO2());
  }

  private String computeHeightV(final NcaCustomCalculationPointFeature neww) {
    final double height;
    if (neww.getHeight() == null) {
      final AssessmentCategory assessmentCategory = getAssessmentCategory();
      height = assessmentCategory == null ? NcaPointHeightSupplier.DEFAULT_CUSTOM_POINT_HEIGHT : assessmentCategory.getDefaultHeight();
    } else {
      height = NumberUtil.round(neww.getHeight(), 2);
    }
    return Double.toString(height);
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Override
  @Computed
  public CalculationPointCharacteristicsEditorValidations getV() {
    return validation;
  }

  @Computed
  public List<AssessmentCategory> getAssessmentCategories() {
    return applicationContext.getConfiguration().getAssessmentCategories();
  }

  @Computed
  public AssessmentCategory getAssessmentCategory() {
    return calculationPoint.getAssessmentCategory();
  }

  @Computed
  public double getAssessmentPointHeightDefault() {
    return applicationContext.getConfiguration().getAssessmentPointHeightDefault();
  }

  @JsMethod
  public void selectAssessmentCategory(final AssessmentCategory category) {
    calculationPoint.setAssessmentCategory(category);
    setHeight(String.valueOf(category == null ? getAssessmentPointHeightDefault() : category.getDefaultHeight()));
  }

  @Computed
  protected String getHeight() {
    return heightV;
  }

  @Computed
  protected void setHeight(final String height) {
    ValidationUtil.setSafeDoubleOptionalValue(v -> calculationPoint.setHeight(v), height);
    heightV = height;
  }

  @Computed
  protected String getHeightValidationError() {
    return i18n.ivDoubleRangeBetween(M.messages().calculationPointsCharacteristicsHeight(), 0, 3000);
  }

  @Computed
  protected String getRoadLocalFractionNO2() {
    return roadLocalFractionNO2V;
  }

  @Computed
  protected void setRoadLocalFractionNO2(final String roadLocalFractionNO2) {
    ValidationUtil.setSafeDoubleOptionalValue(v -> calculationPoint.setRoadLocalFractionNO2(v), roadLocalFractionNO2);
    roadLocalFractionNO2V = roadLocalFractionNO2;
  }

  @Computed
  protected String getRoadLocalFractionNO2ValidationError() {
    return i18n.ivDoubleRangeBetween(M.messages().calculationPointsCharacteristicsFractionNO2(), 0, 1);
  }

}
