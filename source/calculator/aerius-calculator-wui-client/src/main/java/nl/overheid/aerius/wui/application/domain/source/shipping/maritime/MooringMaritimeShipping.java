/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.shipping.maritime;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.wui.application.domain.source.shipping.base.AbstractShipping;
import nl.overheid.aerius.wui.vue.ReactivityUtil;
import nl.overheid.aerius.wui.vue.SkipReactivityValidations;

/**
 * Client side implementation of MooringMaritimeShipping props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public abstract class MooringMaritimeShipping extends AbstractShipping {

  private String mooringMaritimeShippingType;
  private int shipsPerTimeUnit;
  private String timeUnit;
  private int averageResidenceTime;
  private double shorePowerFactor;

  public static final @JsOverlay void init(final MooringMaritimeShipping props, final MooringMaritimeShippingType type) {
    AbstractShipping.initAbstractShipping(props);
    props.setMooringMaritimeShippingType(type);

    ReactivityUtil.ensureJsProperty(props, "shipsPerTimeUnit", props::setShipsPerTimeUnit, 0);
    ReactivityUtil.ensureJsProperty(props, "timeUnit", props::setTimeUnit, TimeUnit.YEAR);
    ReactivityUtil.ensureJsProperty(props, "averageResidenceTime", props::setAverageResidenceTime, 0);
    ReactivityUtil.ensureJsProperty(props, "shorePowerFactor", props::setShorePowerFactor, 0D);
  }

  @SkipReactivityValidations
  public static final @JsOverlay void initTypeDependent(final MooringMaritimeShipping props, final MooringMaritimeShippingType type) {
    switch (type) {
    case CUSTOM:
      CustomMooringMaritimeShipping.init(Js.uncheckedCast(props));
      break;
    case STANDARD:
      StandardMooringMaritimeShipping.init(Js.uncheckedCast(props));
      break;
    default:
      throw new RuntimeException("Unknown type: " + type);
    }
  }

  public final @JsOverlay MooringMaritimeShippingType getMooringMaritimeShippingType() {
    return MooringMaritimeShippingType.safeValueOf(mooringMaritimeShippingType);
  }

  public final @JsOverlay void setMooringMaritimeShippingType(final MooringMaritimeShippingType type) {
    this.mooringMaritimeShippingType = type.name();
  }

  public final @JsOverlay int getAverageResidenceTime() {
    return averageResidenceTime;
  }

  public final @JsOverlay void setAverageResidenceTime(final int averageResidenceTime) {
    this.averageResidenceTime = averageResidenceTime;
  }

  public final @JsOverlay double getShorePowerFactor() {
    return shorePowerFactor;
  }

  public final @JsOverlay void setShorePowerFactor(final double shorePowerFactor) {
    this.shorePowerFactor = shorePowerFactor;
  }

  public final @JsOverlay int getShipsPerTimeUnit() {
    return shipsPerTimeUnit;
  }

  public final @JsOverlay void setShipsPerTimeUnit(final int shipsPerTimeUnit) {
    this.shipsPerTimeUnit = shipsPerTimeUnit;
  }

  public final @JsOverlay TimeUnit getTimeUnit() {
    return TimeUnit.valueOf(timeUnit);
  }

  public final @JsOverlay void setTimeUnit(final TimeUnit timeUnit) {
    this.timeUnit = timeUnit.name();
  }

}
