/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.calculate.options;

import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import nl.aerius.wui.vue.transition.HorizontalCollapse;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.button.IconComponent;
import nl.overheid.aerius.wui.application.components.queue.QueueCounter;
import nl.overheid.aerius.wui.application.components.tooltip.TooltipComponent;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationPreparationContext;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    ButtonIcon.class,
    HorizontalCollapse.class,
    IconComponent.class,
    QueueCounter.class,
    TooltipComponent.class,
})
public class CalculationJobStatusComponent extends BasicVueComponent {
  @Prop CalculationJobContext jobContext;

  @Prop boolean highlight;

  @Inject @Data ScenarioContext scenarioContext;
  @Inject @Data CalculationPreparationContext prepContext;

  @Computed
  public CalculationExecutionContext getCalculation() {
    return prepContext.getActiveCalculation(jobContext);
  }

  @Computed("hasCalculation")
  public boolean hasCalculation() {
    return prepContext.getActiveCalculation(jobContext) != null;
  }

  @Computed
  public JobState getJobState() {
    return Optional.ofNullable(getCalculation())
        .map(v -> v.getJobState())
        .orElse(JobState.UNDEFINED);
  }

  @Computed
  public String getCalculationStatus() {
    return Optional.ofNullable(getCalculation())
        .map(v -> i18n.calculationStatus(v.getJobState()))
        .orElse("");
  }

  @Computed
  public boolean isStaleState() {
    return ScenarioContext.isCalculationStale(scenarioContext, jobContext, getCalculation());
  }

  @Computed
  public boolean isCancelledState() {
    return getCalculation().isCancelled();
  }

  @Computed
  public boolean isCompletedState() {
    return getCalculation().isCalculationCompleted();
  }

  @Computed
  public String getCalculationErrorMessage() {
    return getCalculation().getJobErrorMessage();
  }

  @Computed
  public boolean isErrorState() {
    return getCalculation().isError();
  }

  @Computed
  public int getQueuePosition() {
    return getCalculation().getCalculationInfo().getJobProgress().getQueuePosition();
  }

  @Computed
  public boolean isQueuingState() {
    return getCalculation().getJobState() == JobState.QUEUING;
  }

  @Computed
  public boolean isRunningState() {
    final JobState state = getCalculation().getJobState();

    return state != null && !state.isFinalState()
        && (state != JobState.UNDEFINED && state != JobState.QUEUING);
  }
}
