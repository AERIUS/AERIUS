/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.farmlodging;

import java.util.Arrays;
import java.util.List;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;

import elemental2.core.JsArray;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.wui.application.components.source.farmlodging.custom.FarmLodgingCustomEmissionComponent;
import nl.overheid.aerius.wui.application.components.source.farmlodging.standard.FarmLodgingDetailStandardComponent;
import nl.overheid.aerius.wui.application.components.source.farmlodging.standard.FarmLodgingStandardEmissionComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.components.toggle.ToggleButtons;
import nl.overheid.aerius.wui.application.domain.source.FarmLodgingESFeature;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmLodging;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmLodgingType;
import nl.overheid.aerius.wui.application.domain.source.util.EmissionSourceFeatureUtil;
import nl.overheid.aerius.wui.vue.DebugDirective;

@Component(components = {
    FarmLodgingStandardEmissionComponent.class,
    FarmLodgingDetailStandardComponent.class,
    FarmLodgingCustomEmissionComponent.class,
    ToggleButtons.class,
    ValidationBehaviour.class,
}, directives = {
    DebugDirective.class
})
public class FarmLodgingDetailEditorComponent extends ErrorWarningValidator {
  @Prop FarmLodgingESFeature source;
  @Prop FarmLodging subSource;

  @Data FarmLodgingType toggleSelect;

  @Watch(value = "subSource", isImmediate = true)
  public void onValueChange(final Integer neww, final Integer old) {
    if (subSource != null) {
      toggleSelect = subSource.getFarmLodgingType();
    }
  }

  @Computed
  List<FarmLodgingType> getFarmLodgingTypes() {
    return Arrays.asList(FarmLodgingType.values());
  }

  @JsMethod
  public boolean isSourceAvailable() {
    return subSource != null;
  }

  @JsMethod
  public void selectTab(final FarmLodgingType type) {
    toggleSelect = type;
    final JsArray<FarmLodging> farmLodgings = source.getSubSources();
    farmLodgings.splice(farmLodgings.indexOf(subSource), 1, EmissionSourceFeatureUtil.createFarmLodgingType(type));
  }
}
