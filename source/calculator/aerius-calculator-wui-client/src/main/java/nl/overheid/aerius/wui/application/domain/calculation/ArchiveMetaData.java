/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.calculation;

import static jsinterop.annotations.JsPackage.GLOBAL;

import java.util.Arrays;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class ArchiveMetaData {

  // In backend this is a ZoneDateTime field.
  // GWT doesn't emulate that class however, so just using the string representation (for now)
  private String retrievalDateTime;
  private @JsProperty ArchiveProject[] archiveProjects;

  public final @JsOverlay String getRetrievalDateTime() {
    return retrievalDateTime;
  }

  public final @JsOverlay void setRetrievalDateTime(final String retrievalDateTime) {
    this.retrievalDateTime = retrievalDateTime;
  }

  public final @JsOverlay ArchiveProject[] getArchiveProjects() {
    return archiveProjects;
  }

  public final @JsOverlay void setArchiveProjects(final ArchiveProject[] archiveProjects) {
    this.archiveProjects = archiveProjects;
  }

  public final @JsOverlay String determineDisplayId(final ArchiveProject archiveProject) {
    return archiveProjects == null ? "0" : "" + (Arrays.asList(this.archiveProjects).indexOf(archiveProject) + 1);
  }

}
