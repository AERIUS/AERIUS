/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.manual;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import elemental2.dom.DomGlobal;
import elemental2.dom.Element;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.directives.VectorDirective;
import nl.overheid.aerius.wui.application.command.misc.ManualNavigateRelativePageCommand;
import nl.overheid.aerius.wui.application.command.misc.ManualNavigateTableOfContentsCommand;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ManualContext;
import nl.overheid.aerius.wui.application.context.NavigationContext;
import nl.overheid.aerius.wui.application.util.ManualUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    HTMLOutputComponent.class,
    InputErrorComponent.class,
}, directives = {
    VectorDirective.class,
})
public class HelpManualComponent extends BasicVueComponent {
  @Inject @Data ManualContext manualContext;
  @Inject @Data NavigationContext navContext;
  @Inject @Data ApplicationContext appContext;

  @Prop EventBus eventBus;

  @Computed("activeContent")
  public String getActiveContent() {
    return manualContext.getActiveContentHtml();
  }

  @Computed
  public String getActivePath() {
    final String activeUrl = manualContext.getActiveUrl();
    return activeUrl == null
        ? null
        : activeUrl.substring(0, activeUrl.lastIndexOf("/"));
  }

  @Computed
  public String getBase() {
    return ManualUtil.getManualUrl(appContext);
  }

  @Computed
  public String getCurrentPage() {
    return manualContext.getActiveUrl();
  }

  @JsMethod
  public void goToTableOfContents() {
    eventBus.fireEvent(new ManualNavigateTableOfContentsCommand());
  }

  @JsMethod
  public void onNavigateRelative(final String link) {
    eventBus.fireEvent(new ManualNavigateRelativePageCommand(link));
  }

  @JsMethod
  public void onScrollInternal(final String link) {
    final Element elem = DomGlobal.document.getElementById(link.replace("#", ""));
    if (elem != null) {
      elem.scrollIntoView();
      elem.focus();
    }
  }

  @JsMethod
  public void closeManual() {
    navContext.deactivateManual();
  }
}
