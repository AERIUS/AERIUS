/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.results;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.google.gwt.i18n.client.LocaleInfo;

import jsinterop.annotations.JsMethod;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.summary.ThresholdResult;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.application.util.JsObjectsUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component
public class DecisionThresholdsTable extends BasicVueComponent {
  private static final double HUNDRED_PERCENT = 100D;
  private static final int VALUE_PRECISION = 2;

  @Prop(required = true) JsPropertyMap<ThresholdResult> results;

  @Prop(required = true) String thresholdHeader;

  @Prop String debugId;

  @Data @Inject ApplicationContext applicationContext;

  @PropDefault("debugId")
  String debugIdDefault() {
    return "1";
  }

  @Computed
  public List<EmissionResultKey> getEmissionResultKeys() {
    return applicationContext.getConfiguration().getEmissionResultKeys();
  }

  @Computed
  public GeneralizedEmissionResultType getGeneralizedEmissionResultType(final EmissionResultKey key) {
    return GeneralizedEmissionResultType.fromEmissionResultType(key.getEmissionResultType());
  }

  @Computed
  public EmissionResultValueDisplaySettings.DepositionValueDisplayType getDisplayType() {
    return applicationContext.getConfiguration().getEmissionResultValueDisplaySettings().getDisplayType();
  }

  @JsMethod
  public Optional<String> getPollutantPercentage(final EmissionResultKey key) {
    return getThresholdResult(key).map(x -> x.getFraction() * HUNDRED_PERCENT).map(Object::toString);
  }

  @JsMethod
  public String getPollutantContribution(final EmissionResultKey key) {
    return formatResult(getThresholdResult(key).map(x -> x.getValue()), key);
  }

  @JsMethod
  public String getPollutantThreshold(final EmissionResultKey key) {
    return formatResult(getThresholdResult(key).map(x -> x.getThreshold()), key);
  }

  @JsMethod
  public Optional<Boolean> isPollutantAboveThreshold(final EmissionResultKey key) {
    return getThresholdResult(key)
        .map(v -> v.getFraction() >= 1.0);
  }

  private Optional<ThresholdResult> getThresholdResult(final EmissionResultKey key) {
    return Optional.ofNullable(results).map(x -> JsObjectsUtil.getValueForKey(x, key));
  }

  private String formatResult(final Optional<Double> value, final EmissionResultKey key) {
    if (value.isEmpty()) {
      return i18n.valueNotApplicable();
    } else {
      return MessageFormatter.toPrecision(value.get(), VALUE_PRECISION);
    }
  }

  @JsMethod
  public String getPollutantUnit(final EmissionResultKey key) {
    final GeneralizedEmissionResultType resultType = getGeneralizedEmissionResultType(key);
    return i18n.unitSingularResult(resultType, getDisplayType());
  }

  @Computed
  public String getLocale() {
    return LocaleInfo.getCurrentLocale().getLocaleName();
  }
}
