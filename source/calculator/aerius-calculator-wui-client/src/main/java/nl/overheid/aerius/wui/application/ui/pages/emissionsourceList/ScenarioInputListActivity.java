/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.emissionsourceList;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.place.PlaceController;
import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.shared.domain.context.InputTypeViewMode;
import nl.overheid.aerius.wui.application.command.building.BuildingFeatureDeselectCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingFeatureSelectCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceFeatureDeselectCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceFeatureSelectCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileDeselectCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileSelectCommand;
import nl.overheid.aerius.wui.application.components.map.MapComponentFactory;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.BuildingListContext;
import nl.overheid.aerius.wui.application.context.EmissionSourceListContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.TimeVaryingProfileListContext;
import nl.overheid.aerius.wui.application.place.ScenarioInputListPlace;
import nl.overheid.aerius.wui.application.ui.main.MainView.FlexView;
import nl.overheid.aerius.wui.application.ui.pages.ThemeDelegatedActivity;
import nl.overheid.aerius.wui.application.ui.pages.ThemeView;

public class ScenarioInputListActivity extends BasicEventComponent implements ThemeDelegatedActivity, ScenarioInputsPresenter {
  private static final ScenarioInputListActivityEventBinder EVENT_BINDER = GWT.create(ScenarioInputListActivityEventBinder.class);

  interface ScenarioInputListActivityEventBinder extends EventBinder<ScenarioInputListActivity> {}

  @Inject PlaceController placeController;
  @Inject ApplicationContext applicationContext;
  @Inject ScenarioContext scenarioContext;
  @Inject EmissionSourceListContext sourceListContext;
  @Inject BuildingListContext buildingListContext;
  @Inject TimeVaryingProfileListContext tvpListContext;
  @Inject ScenarioInputListContext inputListContext;

  private ThemeView themeView;

  private HandlerRegistration handlers;
  private final InputTypeViewMode inputTypeViewMode;

  @Inject
  public ScenarioInputListActivity(@Assisted final ScenarioInputListPlace place) {
    inputTypeViewMode = place.getMode();
  }

  @Override
  public void onStart(final ThemeView view) {
    this.themeView = view;

    view.setDelegatedPresenter(this);
    view.setLeftView(ScenarioInputListViewFactory.get(), FlexView.RIGHT);
    view.setMiddleView(ScenarioInputListDetailViewFactory.get());
    view.setRightView(MapComponentFactory.get());
    updateSoftMiddle();

    if (inputTypeViewMode != null) {
      inputListContext.setViewMode(inputTypeViewMode);
    }
  }

  @Override
  public void onStop() {
    handlers.removeHandler();
    sourceListContext.reset();
    buildingListContext.reset();
    tvpListContext.reset();
  }

  @EventHandler(handles = {EmissionSourceFeatureSelectCommand.class, BuildingFeatureSelectCommand.class, TimeVaryingProfileSelectCommand.class,
      EmissionSourceFeatureDeselectCommand.class, BuildingFeatureDeselectCommand.class, TimeVaryingProfileDeselectCommand.class})
  public void onSelectOrPeekCommand() {
    updateSoftMiddle();
  }

  private void updateSoftMiddle() {
    SchedulerUtil.delay(() -> themeView.setSoftMiddle(!(sourceListContext.hasSingleSelection()
        || buildingListContext.hasSingleSelection()
        || tvpListContext.hasSingleSelection())));
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    handlers = super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
