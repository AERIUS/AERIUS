/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.results;

import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.google.web.bindery.event.shared.EventBus;

import elemental2.dom.HTMLElement;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.directives.VectorDirective;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.wui.application.command.result.ProcessContributionsLegendPopupToggleCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationInfo;
import nl.overheid.aerius.wui.application.domain.calculation.JobProgress;
import nl.overheid.aerius.wui.application.domain.summary.JobSummary;
import nl.overheid.aerius.wui.application.domain.summary.JobSummaryProjectRecord;
import nl.overheid.aerius.wui.application.domain.summary.JobSummarySituationRecord;
import nl.overheid.aerius.wui.application.ui.pages.results.summary.ProjectSummaryTable;
import nl.overheid.aerius.wui.application.ui.pages.results.summary.SituationSummaryTable;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    ResultsWrapperComponent.class,
    VerticalCollapse.class,

    ProjectSummaryTable.class,
    SituationSummaryTable.class,
    ProcessContributionsLegendPanelModal.class,
}, directives = {
    VectorDirective.class,
})
public class SummaryResultsView extends BasicVueComponent {
  @Prop EventBus eventBus;

  @Inject @Data CalculationContext calculationContext;
  @Inject @Data ApplicationContext applicationContext;

  @Ref HTMLElement toggleLegendPanelButton;

  @Computed
  public JobSummarySituationRecord[] getSituations() {
    return Optional.ofNullable(getJobSummary())
        .map(JobSummary::getSituationRecords)
        .orElse(null);
  }

  @Computed
  public CalculationExecutionContext getCalculationJob() {
    return calculationContext.getActiveCalculation();
  }

  @Computed
  public CalculationInfo getCalculationInfo() {
    return Optional.ofNullable(getCalculationJob())
        .map(CalculationExecutionContext::getCalculationInfo)
        .orElseThrow(() -> new RuntimeException("Could not retrieve calculation info."));
  }

  @Computed
  public JobProgress getJobProgress() {
    return Optional.ofNullable(getCalculationInfo())
        .map(CalculationInfo::getJobProgress)
        .orElseGet(JobProgress::new);
  }

  @Computed
  public JobSummaryProjectRecord[] getProjectRecords() {
    return Optional.ofNullable(getJobSummary())
        .map(v -> v.getProjectRecords())
        .orElse(null);
  }

  @JsMethod
  public JobSummary getJobSummary() {
    return getCalculationJob().getJobSummary();
  }

  @JsMethod
  public void toggleLegendPanel() {
    eventBus.fireEvent(new ProcessContributionsLegendPopupToggleCommand(toggleLegendPanelButton));
  }

}
