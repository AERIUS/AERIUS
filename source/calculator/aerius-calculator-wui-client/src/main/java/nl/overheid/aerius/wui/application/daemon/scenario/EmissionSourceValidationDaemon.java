/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.scenario;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.wui.application.command.DetailEditorErrorChangeCommand;
import nl.overheid.aerius.wui.application.command.DetailEditorWarningChangeCommand;
import nl.overheid.aerius.wui.application.context.EmissionSourceValidationContext;

@Singleton
public class EmissionSourceValidationDaemon extends BasicEventComponent {
  private static final EmissionSourceValidationDaemonEventBinder EVENT_BINDER = GWT.create(EmissionSourceValidationDaemonEventBinder.class);

  interface EmissionSourceValidationDaemonEventBinder extends EventBinder<EmissionSourceValidationDaemon> {}

  @Inject private EmissionSourceValidationContext context;

  @EventHandler
  public void onDetailEditorErrorChangeCommand(final DetailEditorErrorChangeCommand c) {
    context.setDetailEditorError(c.getValue());
  }

  @EventHandler
  public void onDetailEditorWarningChangeCommand(final DetailEditorWarningChangeCommand c) {
    context.setDetailEditorWarning(c.getValue());
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
