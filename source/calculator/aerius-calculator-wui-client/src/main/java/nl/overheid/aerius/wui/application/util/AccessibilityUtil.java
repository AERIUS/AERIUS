/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.util;

import java.util.Optional;
import java.util.function.Consumer;

import com.axellience.vuegwt.core.client.Vue;

import elemental2.dom.Element;
import elemental2.dom.Event;
import elemental2.dom.EventListener;
import elemental2.dom.FocusEvent;
import elemental2.dom.HTMLElement;
import elemental2.dom.KeyboardEvent;
import elemental2.dom.Node;
import elemental2.dom.NodeList;

import jsinterop.base.Js;

public class AccessibilityUtil {
  public static interface Register {
    void removeHandle();
  }

  public static final int FOCUS_OUT_DELAY = 500;

  /**
   * Recursively figure out of the given element has another element as its parent
   */
  public static boolean hasAsParent(final Node elem, final Node possibleParent) {
    if (elem == null) {
      return false;
    }

    if (elem == possibleParent) {
      return true;
    }

    Node up = elem;
    while (up.parentNode != null) {
      if (up.parentNode == possibleParent) {
        return true;
      }

      up = up.parentNode;
    }

    return false;
  }

  /**
   * Find and return the first focusable element within the given element.
   */
  public static Element findFirstFocusable(final Element me) {
    final NodeList<Element> lst = findFocusables(me);
    for (int i = 0; i < lst.length; i++) {
      final HTMLElement elem = (HTMLElement) lst.getAt(i);
      if (elem.offsetParent != null) {
        return elem;
      }
    }

    return null;
  }

  /**
   * Find and return the last focusable element within the given element.
   */
  public static Element findLastFocusable(final Element me) {
    final NodeList<Element> lst = findFocusables(me);
    for (int i = lst.length - 1; i > 0; i--) {
      final HTMLElement elem = (HTMLElement) lst.getAt(i);
      if (elem.offsetParent != null) {
        return elem;
      }
    }

    return null;
  }

  /**
   * Find and return the first focusable element within the given element.
   */
  public static NodeList<Element> findFocusables(final Element me) {
    return me.querySelectorAll("button, [href], input, select, textarea, [tabindex]:not([tabindex=\"-1\"]), input:not(:disabled)");
  }

  /**
   * Given the passed element, add relevant handlers that will figure out if the
   * element loses inner focus, and if so cycle to the first/last focusable
   * element, thereby trapping focus within this element.
   *
   * Useful for modal components.
   *
   * Usage: call this method on focusout and pass the event.
   *
   * <pre>
   * public void mounted() {
   *   register = AccessibilityUtil.trapFocus(main);
   * }
   * </pre>
   */
  public static Register trapFocus(final HTMLElement elem) {
    // Store shift key in semaphore for forward/backward cycling
    final Semaphore<Boolean> shiftKey = new Semaphore<>(false);
    final EventListener keyDownEvent = e -> trackShift(e, down -> shiftKey.setValue(down));

    // Track focusout, if leaving focus find the element to cycle and give focus to
    final EventListener focusOutEvent = e -> doOnFocusLeave((FocusEvent) e,
        () -> Optional.ofNullable(shiftKey.getValue()
            ? AccessibilityUtil.findLastFocusable(elem)
            : AccessibilityUtil.findFirstFocusable(elem))
            .ifPresent(focusable -> Vue.nextTick(() -> {
              e.preventDefault();
              focusable.focus();
            })));

    // Register event listeners
    elem.addEventListener("keydown", keyDownEvent);
    elem.addEventListener("focusout", focusOutEvent);

    // Return a register to remove event listeners
    return () -> {
      elem.removeEventListener("keydown", keyDownEvent);
      elem.removeEventListener("focusout", focusOutEvent);
    };
  }

  private static Object trackShift(final Event e, final Consumer<Boolean> object) {
    if (e instanceof KeyboardEvent ) {
      object.accept(((KeyboardEvent) e).shiftKey);
    }
    return null;
  }

  /**
   * Given the passed focus event, figure out if the related target (the element
   * which has its focus blurred) does not have the current target (the element
   * which listens to the onfocusout event) as its parent, and if so run the given
   * runner to do whatever.
   *
   * This allows some clever custom action when focus leaves an element and all of
   * its children.
   */
  public static Object doOnFocusLeave(final FocusEvent event, final Runnable runner) {
    final HTMLElement me = (HTMLElement) event.currentTarget;
    final HTMLElement target = Js.cast(event.relatedTarget);
    if (!AccessibilityUtil.hasAsParent(target, me)) {
      runner.run();
    }
    return null;
  }

  public static NodeList<Element> findFirstInput(final HTMLElement inputContainer) {
    return inputContainer.querySelectorAll("input, textarea, select");
  }
}
