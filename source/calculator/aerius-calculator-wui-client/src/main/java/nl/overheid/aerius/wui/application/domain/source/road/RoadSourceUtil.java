/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.road;

import java.util.List;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.ColdStartSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.OnRoadMobileSourceCategory;
import nl.overheid.aerius.wui.application.domain.source.EmissionFactors;

/*
 * Util class for road source
 */
public class RoadSourceUtil {

  /**
   * Update the emissionFactors when year of onRoadMobileSourceCategory changes
   *
   * @param roadMobilesourceCategories
   * @param onRoadMobileSourceCategory
   * @param substances
   * @param roadType
   * @param year
   */
  public static EmissionFactors updateEmissionFactors(final List<OnRoadMobileSourceCategory> roadMobilesourceCategories,
      final String onRoadMobileSourceCategory, final List<Substance> substances, final String roadTypeCode, final int year) {
    final EmissionFactors emissionFactors = EmissionFactors.create();
    for (final OnRoadMobileSourceCategory category : roadMobilesourceCategories) {
      if (category.getCode().equals(onRoadMobileSourceCategory)) {
        for (final Substance substance : substances) {
          try {
            final int yearInt = year;
            emissionFactors.setEmissionFactor(substance, category.getEmissionFactor(substance, roadTypeCode, yearInt));
          } catch (final NumberFormatException e) {
            throw new RuntimeException("Could not parse year into number: " + year);
          }
        }
        break;
      }
    }
    return emissionFactors;
  }

  public static EmissionFactors updateEmissionFactors(final List<ColdStartSourceCategory> categories, final String vehicleCode,
      final List<Substance> substances,
      final int year) {
    final EmissionFactors emissionFactors = EmissionFactors.create();
    for (final ColdStartSourceCategory category : categories) {
      if (category.getCode().equals(vehicleCode)) {
        for (final Substance substance : substances) {
          try {
            final int yearInt = year;
            emissionFactors.setEmissionFactor(substance, category.getEmissionFactor(substance, yearInt));
          } catch (final NumberFormatException e) {
            throw new RuntimeException("Could not parse year into number: " + year);
          }
        }
        break;
      }
    }
    return emissionFactors;
  }

}
