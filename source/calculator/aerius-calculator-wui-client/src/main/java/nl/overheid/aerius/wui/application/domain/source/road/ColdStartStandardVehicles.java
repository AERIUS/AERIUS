/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.road;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.wui.vue.ReactivityUtil;
import nl.overheid.aerius.wui.vue.SkipReactivityValidations;

@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class ColdStartStandardVehicles extends Vehicles {
  private ValuesPerColdStartVehicleTypes valuesPerVehicleTypes;

  public static final @JsOverlay ColdStartStandardVehicles create() {
    final ColdStartStandardVehicles props = Js.uncheckedCast(JsPropertyMap.of());
    init(props);
    return props;
  }

  public static final @JsOverlay void init(final ColdStartStandardVehicles props) {
    Vehicles.initVehicles(props, VehicleType.STANDARD_CS);

    ReactivityUtil.ensureJsPropertySupplied(props, "valuesPerVehicleTypes", props::setValuesPerVehicleTypes, ValuesPerColdStartVehicleTypes::create);
    ReactivityUtil.ensureInitialized(props::getValuesPerVehicleTypes, ValuesPerColdStartVehicleTypes::init);
  }

  @SkipReactivityValidations
  public final @JsOverlay Double getValuesPerVehicleType(final String vehicleTypeCode) {
    return valuesPerVehicleTypes.getValuePerVehicleType(vehicleTypeCode);
  }

  public final @JsOverlay boolean hasValuesPerVehicleType(final String vehicleTypeCode) {
    return this.valuesPerVehicleTypes.hasValuePerVehicleType(vehicleTypeCode);
  }

  public final @JsOverlay void setValuesPerVehicleType(final String vehicleTypeCode, final Double valuesPerVehicleType) {
    this.valuesPerVehicleTypes.setValuePerVehicleType(vehicleTypeCode, valuesPerVehicleType);
  }

  public final @JsOverlay ValuesPerColdStartVehicleTypes getValuesPerVehicleTypes() {
    return valuesPerVehicleTypes;
  }

  public final @JsOverlay void setValuesPerVehicleTypes(final ValuesPerColdStartVehicleTypes valuesVehicleType) {
    this.valuesPerVehicleTypes = valuesVehicleType;
  }
}
