/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.development;

/**
 * Rinky dink number generator
 */
public class HashNumber {
  private int seed;
  private int i;

  private final int[] primes = new int[] { 2047483673, 1047483649, 1047484007, 2047483721, 3, 5, 23, 43 };

  public HashNumber(final Object seedObj) {
    seed = 31
        * 251
        * 2047483673
        * 6073
        * seedObj.hashCode(); // Bunch of primes multiplied by seed hash
    nextInt(seedObj.hashCode());
  }

  public int nextInt(final int n) {
    final int ret = (seed = seed * primes[i++ % primes.length] & 0xFFFFFFFF) % n;
    seed += n;
    return Math.abs(ret);
  }

  public static HashNumber create(final String txt) {
    return new HashNumber(txt);
  }
}
