/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.domain.source.road.Vehicles;
import nl.overheid.aerius.wui.vue.SkipReactivityValidations;

/**
 * Feature object for Cold Start Emission Sources with OPS characteristics.
 */
@JsType(namespace = GLOBAL, name = "ol.Feature", isNative = true)
public class ColdStartESFeature extends EmissionSubSourceFeature<Vehicles> {
  /**
   * @return Returns Feature {@link ColdStartESFeature} object.
   */
  public static @JsOverlay ColdStartESFeature create(final CharacteristicsType characteristicsType, final boolean vehicleBasedCharacteristics) {
    final ColdStartESFeature feature = new ColdStartESFeature();
    init(feature, characteristicsType, vehicleBasedCharacteristics);
    return feature;
  }

  public static @JsOverlay void init(final ColdStartESFeature feature, final CharacteristicsType type, final boolean vehicleBasedCharacteristics) {
    feature.setVehicleBasedCharacteristics(vehicleBasedCharacteristics);

    EmissionSubSourceFeature.initSubSourceFeature(feature, type, EmissionSourceType.COLD_START);
    EmissionSubSourceFeature.initSubSources(feature, v -> Vehicles.initColdStartVehiclesTypeDependent(v, v.getVehicleType()));
  }

  public final @JsOverlay void setVehicleBasedCharacteristics(final boolean vehicleBasedCharacteristics) {
    set("vehicleBasedCharacteristics", vehicleBasedCharacteristics);
  }

  public final @JsOverlay boolean isVehicleBasedCharacteristics() {
    return Js.isTruthy(get("vehicleBasedCharacteristics"));
  }

  /**
   * @return Returns model characteristics as OPS characteristics.
   */
  @SkipReactivityValidations
  public final @JsOverlay OPSCharacteristics getOPSCharacteristics() {
    return Js.uncheckedCast(getCharacteristics());
  }
}
