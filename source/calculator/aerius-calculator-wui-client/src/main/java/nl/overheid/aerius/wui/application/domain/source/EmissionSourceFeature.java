/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source;

import static jsinterop.annotations.JsPackage.GLOBAL;

import ol.Feature;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.domain.ImaerFeature;
import nl.overheid.aerius.wui.vue.ReactivityUtil;
import nl.overheid.aerius.wui.vue.SkipReactivityValidations;

/**
 * Base class Emission Source Feature. This class extends openlayers {@link Feature} class.
 *
 * For subclasses to get access to the geojson properties use the get and set methods.
 *
 * This class also contains the EmissionSourceType that is used to specify the properties subclass because instanceof doesn't work for objects
 * obtained from the server.
 *
 * NOTE: This class can't be implemented with java fields as the variables should be in the Feature properties
 */
@JsType(namespace = GLOBAL, name = "ol.Feature", isNative = true)
public abstract class EmissionSourceFeature extends ImaerFeature {
  protected static final @JsOverlay void initEmissionSource(final EmissionSourceFeature feature,
      final CharacteristicsType characteristicsType, final EmissionSourceType emissionSourceType) {
    ImaerFeature.initImaer(feature);
    SourceCharacteristics.initTypeDependent(feature, characteristicsType);
    feature.setEmissionSourceType(emissionSourceType);
    ReactivityUtil.ensureDefault(feature::getDescription, feature::setDescription);
    ReactivityUtil.ensureDefault(feature::getJurisdictionId, feature::setJurisdictionId);
    ReactivityUtil.ensureDefault(feature::getSectorId, feature::setSectorId);
    ReactivityUtil.ensureDefault(feature::getEstablished, feature::setEstablished);
    ReactivityUtil.ensureDefault(feature::getEmissions, feature::setEmissions, JsPropertyMap.of());
  }

  /**
   * Returns true if the feature is an {@link EmissionSourceFeature} object.
   */
  public static final @JsOverlay boolean isEmissionSourceFeature(final Feature feature) {
    return feature.get("emissionSourceType") != null;
  }

  /**
   * Returns true if the feature has subsources and is an
   * {@link EmissionSubSourceFeature} object.
   */
  public static final @JsOverlay boolean hasSubSources(final Feature feature) {
    return feature.get("subsources") != null;
  }

  @SkipReactivityValidations
  public final @JsOverlay CharacteristicsType getCharacteristicsType() {
    final SourceCharacteristics characteristics = getCharacteristics();

    return characteristics == null ? null : characteristics.getCharacteristicsType();
  }

  @SkipReactivityValidations
  public final @JsOverlay SourceCharacteristics getCharacteristics() {
    return Js.uncheckedCast(get("characteristics"));
  }

  public final @JsOverlay void setCharacteristics(final SourceCharacteristics sourceCharacteristics) {
    set("characteristics", sourceCharacteristics);
  }

  public final @JsOverlay String getDescription() {
    return get("description") == null ? null : Js.asString(get("description"));
  }

  public final @JsOverlay void setDescription(final String description) {
    set("description", description);
  }

  public final @JsOverlay int getJurisdictionId() {
    return get("JurisdictionId") == null ? 0 : Js.coerceToInt(get("JurisdictionId"));
  }

  public final @JsOverlay void setJurisdictionId(final int jurisdictionId) {
    if (jurisdictionId > 0) {
      set("jurisdictionId", Double.valueOf(jurisdictionId));
    }
  }

  public final @JsOverlay int getSectorId() {
    return get("sectorId") == null ? 0 : Js.coerceToInt(get("sectorId"));
  }

  public final @JsOverlay void setSectorId(final int sectorId) {
    set("sectorId", Double.valueOf(sectorId));
  }

  public final @JsOverlay EmissionSourceType getEmissionSourceType() {
    return EmissionSourceType.safeValueOf(get("emissionSourceType"));
  }

  public final @JsOverlay void setEmissionSourceType(final EmissionSourceType emissionSourceType) {
    set("emissionSourceType", emissionSourceType.getName());
  }

  @SkipReactivityValidations
  public final @JsOverlay double getEmission(final Substance substance) {
    return emissions().has(substance.name()) ? Js.coerceToDouble(emissions().get(substance.name())) : 0.0;
  }

  public final @JsOverlay void setEmission(final Substance substance, final double emission) {
    emissions().set(substance.name(), emission);
  }

  public final @JsOverlay String getEstablished() {
    return get("established") == null ? "" : Js.uncheckedCast(get("established"));
  }

  public final @JsOverlay void setEstablished(final String established) {
    set("established", established);
  }

  public final @JsOverlay Object getEmissions() {
    return get("emissions");
  }

  public final @JsOverlay void setEmissions(final Object emissions) {
    set("emissions", emissions);
  }

  private final @JsOverlay JsPropertyMap<Object> emissions() {
    return Js.asPropertyMap(get("emissions"));
  }
}
