/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import elemental2.dom.File;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.js.exception.AeriusJsExceptionMessage;
import nl.overheid.aerius.js.file.SituationStats;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.components.importer.domain.ImportAction;
import nl.overheid.aerius.wui.application.components.importer.domain.ImportAction.ActionType;
import nl.overheid.aerius.wui.application.components.importer.domain.VirtualSituation;
import nl.overheid.aerius.wui.application.domain.importer.ImportFilters;

public class FileUploadStatus {
  private final String key = UUID.uuidv4();

  private File file = null;
  private String fileCode = null;

  private double percentage = 0;

  /**
   * This file has been validated
   */
  private boolean validated = false;
  private boolean complete = false;
  private boolean failed = false;
  /**
   * This file is pending processing
   */
  private boolean pending = false;
  /**
   * This file is waiting for user input
   */
  private boolean userInput = false;

  private final @JsProperty List<AeriusJsExceptionMessage> exceptions = new ArrayList<>();
  private final @JsProperty List<AeriusJsExceptionMessage> warnings = new ArrayList<>();

  private @JsProperty final ImportFilters filters = ImportFilters.all();

  private String situationName = null;
  private SituationStats situationStats = null;

  private ImportAction importAction = null;

  private SituationType situationType = null;
  private CalculationJobType calculationJobType = null;
  private Substance substance = null;
  private int importYear = 0;
  private String createdSituationId = null;

  public File getFile() {
    return file;
  }

  public void setFile(final File file) {
    this.file = file;
  }

  public String getKey() {
    return key;
  }

  public String getFileCode() {
    return fileCode;
  }

  public void setFileCode(final String fileCode) {
    this.fileCode = fileCode;
  }

  public double getPercentage() {
    return percentage;
  }

  public void setCompleteRatio(final double percentage) {
    this.percentage = percentage;
  }

  public boolean isComplete() {
    return complete;
  }

  public void setComplete(final boolean complete) {
    this.complete = complete;
  }

  public boolean isFailed() {
    return failed;
  }

  public void setFailed(final boolean failed) {
    this.failed = failed;
  }

  public boolean isPending() {
    return pending;
  }

  public void setPending(final boolean pending) {
    this.pending = pending;
  }

  public void setUserInput(final boolean userInput) {
    this.userInput = userInput;
  }

  public boolean isUserInput() {
    return userInput;
  }

  public boolean hasErrors() {
    return !exceptions.isEmpty();
  }

  public List<AeriusJsExceptionMessage> getErrors() {
    return exceptions;
  }

  public boolean hasWarnings() {
    return !warnings.isEmpty();
  }

  public List<AeriusJsExceptionMessage> getWarnings() {
    return warnings;
  }

  public SituationStats getSituationStats() {
    return situationStats;
  }

  public void setSituationStats(final SituationStats situationStats) {
    this.situationStats = situationStats;
  }

  public void clear() {
    warnings.clear();
    exceptions.clear();
  }

  @Override
  public String toString() {
    return "FileUploadStatus [file=" + file.name + "]";
  }

  public boolean isValidated() {
    return validated;
  }

  public void setValidated(final boolean validated) {
    this.validated = validated;
  }

  public boolean isCalculationPointStatus() {
    return situationStats != null && situationStats.getCalculationPoints() > 0;
  }

  public Substance getSubstance() {
    return substance;
  }

  public void setSubstance(final Substance substance) {
    this.substance = substance;
  }

  public int getImportYear() {
    return importYear;
  }

  public void setImportYear(final int importYear) {
    this.importYear = importYear;
  }

  public SituationType getSituationType() {
    return situationType;
  }

  public void setSituationType(final SituationType situationType) {
    this.situationType = situationType;
  }

  public CalculationJobType getCalculationJobType() {
    return calculationJobType;
  }

  public void setCalculationJobType(final CalculationJobType calculationJobType) {
    this.calculationJobType = calculationJobType;
  }

  public ImportAction getImportAction() {
    return importAction;
  }

  public void setImportAction(final ImportAction importAction) {
    this.importAction = importAction;
  }

  public void setSituationName(final String situationName) {
    this.situationName = situationName;
  }

  public String getSituationName() {
    return situationName;
  }

  public ImportFilters getFilters() {
    return filters;
  }

  public String getCreatedSituationId() {
    return createdSituationId;
  }

  public void setCreatedSituationId(final String createdSituationId) {
    this.createdSituationId = createdSituationId;
  }

  public void reset() {
    fileCode = null;
    percentage = 0;
    complete = false;
    validated = false;
    pending = false;
    situationName = null;
    situationStats = null;
  }

  public static boolean isNotAddToVirtualOrAddCalculationPoints(final FileUploadStatus status) {
    final ActionType actionType = Optional.ofNullable(status.getImportAction())
        .map(v -> v.getType())
        .orElse(null);
    return !(actionType == ActionType.ADD_TO_VIRTUAL_SITUATION
        || actionType == ActionType.ADD_CALCULATION_POINTS
        || actionType == ActionType.ADD_ARCHIVE_CONTRIBUTION);
  }

  public static boolean isAddToVirtual(final FileUploadStatus status) {
    return Optional.ofNullable(status.getImportAction())
        .map(v -> v.getType())
        .orElse(null) == ActionType.ADD_TO_VIRTUAL_SITUATION;
  }

  public static boolean isArchiveContribution(final FileUploadStatus status) {
    return Optional.ofNullable(status.getImportAction())
        .map(v -> v.getType())
        .orElse(null) == ActionType.ADD_ARCHIVE_CONTRIBUTION;
  }

  public static boolean canHaveCalculationJobImport(final FileUploadStatus file, final List<FileUploadStatus> files,
      final Predicate<CalculationJobType> isAllowedJobTypeFunction) {
    final boolean archiveContribution = isArchiveContribution(file);
    if (!(isAddToVirtual(file) || archiveContribution)) {
      return false;
    }

    final List<FileUploadStatus> group = files.stream()
        .filter(v -> v.getFile().equals(file.getFile()))
        .collect(Collectors.toList());
    return !FileUploadStatus.isIncompatibleCalculationJobImport(group)
        && (isAllowedJobTypeFunction.test(file.getCalculationJobType())
            || archiveContribution);
  }

  /**
   * Determine if this group of FileUploadStatus is a valid import
   * - Any of the import actions aren't to add a new situation or to add calculation points or to import archive contribution
   * - Any of the newly created situations do not reference their own basefile
   * - None of the import actions are to add a new situation
   * - None of the virtual situations to be created stray from the original metadata
   */
  public static boolean isIncompatibleCalculationJobImport(final Collection<FileUploadStatus> group) {
    return group.stream()
        .anyMatch(v -> FileUploadStatus.isNotAddToVirtualOrAddCalculationPoints(v))
        || group.stream()
            .filter(v -> FileUploadStatus.isAddToVirtual(v))
            .anyMatch(v -> !VirtualSituation.isSameFileHandle(v.getImportAction().getVirtualSituation(), v))
        || group.stream()
            .noneMatch(v -> FileUploadStatus.isAddToVirtual(v))
        || group.stream()
            .anyMatch(v -> VirtualSituation.isAlteredMetaData(v.getImportAction().getVirtualSituation(), v));
  }

  public static String getIconForFile(final FileUploadStatus file) {
    switch (FileDownloadUtil.getFileExtension(file.getFile())) {
    case "gml":
      return "icon-doc-gml";
    case "pdf":
      return "icon-doc-pdf";
    case "zip":
      return "icon-doc-zip";
    case "rcp":
      return "icon-doc-rcp";
    case "brn":
      return "icon-doc-brn";
    case "csv":
      return "icon-doc-csv";
    default:
      return "icon-doc-text";
    }
  }
}
