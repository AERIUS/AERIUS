/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.results;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.Collection;
import ol.Feature;
import ol.OLFactory;
import ol.format.GeoJson;
import ol.format.GeoJsonOptions;
import ol.layer.Layer;
import ol.layer.Vector;
import ol.source.VectorOptions;
import ol.style.Icon;
import ol.style.IconOptions;
import ol.style.Style;
import ol.style.StyleOptions;

import nl.aerius.geo.domain.IsLayer;
import nl.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.result.CalculationMarker;
import nl.overheid.aerius.shared.domain.result.ResultView;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.MapConfigurationContext;
import nl.overheid.aerius.wui.application.context.PrintContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.context.result.MarkerSortOrder;
import nl.overheid.aerius.wui.application.context.result.ResultSummaryContext;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsMarker;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsSummary;
import nl.overheid.aerius.wui.application.event.CalculationStartEvent;
import nl.overheid.aerius.wui.application.event.SituationResultsSelectedEvent;
import nl.overheid.aerius.wui.application.geo.layers.LegendPair;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.resources.images.markers.MarkerResource;
import nl.overheid.aerius.wui.application.util.ResultStatisticUtil;

public class CalculationMarkersLayer implements IsLayer<Layer> {
  private final Vector layer;
  private final Collection<Feature> featureCollection;

  private final CalculationMarkersLayer.CalculationMarkersLayerEventBinder EVENT_BINDER = GWT.create(
      CalculationMarkersLayer.CalculationMarkersLayerEventBinder.class);

  interface CalculationMarkersLayerEventBinder extends EventBinder<CalculationMarkersLayer> {}

  private final MapConfigurationContext mapConfigurationContext;
  private final CalculationContext calculationContext;
  private final LayerInfo info;
  private final ApplicationContext appContext;
  private final PrintContext printContext;

  @Inject
  public CalculationMarkersLayer(final EventBus eventBus, final MapConfigurationContext mapConfigurationContext,
      final CalculationContext calculationContext, final ApplicationContext appContext, final PrintContext printContext) {
    this.mapConfigurationContext = mapConfigurationContext;
    this.appContext = appContext;
    this.printContext = printContext;
    this.featureCollection = new Collection<>();
    this.layer = new Vector();

    this.info = new LayerInfo();
    this.info.setName(this.getClass().getCanonicalName());
    this.info.setTitle(M.messages().layerMarkers());

    this.calculationContext = calculationContext;

    updateLegend();

    final VectorOptions vectorSourceOptions = OLFactory.createOptions();
    vectorSourceOptions.setFeatures(featureCollection);
    final ol.source.Vector vectorSource = new ol.source.Vector(vectorSourceOptions);

    layer.setSource(vectorSource);
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  public void onSituationResultsSelectedEvent(final SituationResultsSelectedEvent e) {
    updateMarkers();
    updateLegend();
  }

  private void updateMarkers() {
    featureCollection.clear();

    final Optional<ResultSummaryContext> summary = Optional.ofNullable(calculationContext.getActiveCalculation())
        .map(v -> v.getResultContext());
    if (!summary.isPresent()) {
      return;
    }

    final ResultSummaryContext summaryContext = summary.get();

    if (!summaryContext.hasActiveResultSummary()) {
      return;
    }
    final SituationResultsKey key = summaryContext.getSituationResultsKey();
    final List<ResultView> resultViews = appContext.getConfiguration().getResultViews();
    final SituationResultsSummary resultsSummary = summaryContext.getActiveResultSummary();
    for (final SituationResultsMarker resultsMarker : resultsSummary.getMarkers().asList()) {
      final Set<CalculationMarker> calculationMarkers = CalculationMarker.getCalculationMarkers(resultViews, key.getResultType(),
          resultsMarker.getStatisticsTypes(), printContext.getPdfProductType());

      if (calculationMarkers.isEmpty()) {
        continue;
      }

      final MarkerResource markerImage = CalculationMarkers.getCombinableMarkerImage(calculationMarkers, key.getResultType(),
          appContext.getTheme());

      final Feature markerFeature = createFeature(markerImage, resultsMarker.getPoint());
      featureCollection.push(markerFeature);
    }
  }

  private Feature createFeature(final MarkerResource markerResource, final Object point) {
    final IconOptions iconOptions = new IconOptions();
    iconOptions.setSrc(markerResource.getDataResource().getSafeUri().asString());
    iconOptions.setAnchor(markerResource.getAnchor());
    iconOptions.setScale(mapConfigurationContext.getIconScale());
    final Icon icon = new Icon(iconOptions);

    final StyleOptions styleOptions = new StyleOptions();
    styleOptions.setImage(icon);
    final Style style = new Style(styleOptions);

    final Feature markerFeature = new Feature();
    markerFeature.setGeometry(new GeoJson().readGeometry(point, new GeoJsonOptions()));
    markerFeature.setStyle(style);
    return markerFeature;
  }

  @EventHandler
  public void onCalculationStartEvent(final CalculationStartEvent e) {
    layer.setVisible(false);
    featureCollection.clear();
  }

  private void updateLegend() {
    final SimplePairedLegend legend = new SimplePairedLegend();

    final Optional<ResultSummaryContext> summary = Optional.ofNullable(calculationContext.getActiveCalculation())
        .map(CalculationExecutionContext::getResultContext);
    if (summary.isEmpty() || !summary.get().hasActiveResultSummary()) {
      legend.setPairs(new ArrayList<>());
      this.info.setLegend(legend);
      return;
    }

    final ResultSummaryContext summaryContext = summary.get();
    final List<LegendPair> pairs = Stream.of(summaryContext.getResultType())
        .flatMap(v -> {
          final List<ResultStatisticType> stats = ResultStatisticUtil.getStatisticsForHabitat(v);
          return CalculationMarker.getCalculationMarkers(appContext.getConfiguration().getResultViews(),
              v, stats, null)
              .stream()
              .map(mrk -> {
                final Theme activeTheme = appContext.getTheme();
                return new LegendPair(CalculationMarkers.getSingleMarkerImage(mrk, v, activeTheme).getDataResource(),
                    M.messages().resultsCalculationMarker(v, mrk, M.messages().resultsTotalName(activeTheme)), new MarkerSortOrder(mrk, v));
              });
        })
        .sorted()
        .collect(Collectors.toList());

    legend.setPairs(pairs);
    this.info.setLegend(legend);
  }

  @Override
  public Layer asLayer() {
    return layer;
  }

  @Override
  public LayerInfo getInfo() {
    return info;
  }

}
