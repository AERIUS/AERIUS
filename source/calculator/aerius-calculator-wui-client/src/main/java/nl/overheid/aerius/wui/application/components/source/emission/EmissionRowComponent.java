/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.source.emission;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.emission.EmissionValidators.EmissionValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;

/**
 * Shows an emission editor with label for generic emission source.
 */
@Component(customizeOptions = {
    EmissionValidators.class,
}, directives = {
    ValidateDirective.class,
}, components = {
    LabeledInputComponent.class,
    ValidationBehaviour.class,
})
public class EmissionRowComponent extends ErrorWarningValidator implements HasCreated, HasValidators<EmissionValidations> {
  @Prop Substance substance;
  @Prop EmissionSourceFeature source;
  @Data String emissionV;

  @JsProperty(name = "$v") EmissionValidations validations;

  @Watch(value = "source", isImmediate = true)
  public void watchSource(final EmissionSourceFeature neww) {
    emissionV = String.valueOf(neww.getEmission(substance));
  }

  @Override
  @Computed
  public EmissionValidations getV() {
    return validations;
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Computed
  public String getEmission() {
    return emissionV;
  }

  @Computed
  public void setEmission(final String emission) {
    ValidationUtil.setSafeDoubleValue(v -> source.setEmission(substance, v), emission, 0D);
    emissionV = emission;
  }

  @JsMethod
  public String emissionConversionError() {
    return i18n.errorDecimal(emissionV);
  }

}
