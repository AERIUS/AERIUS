/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.button;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Emit;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;

import elemental2.dom.Event;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.wui.vue.BasicVueComponent;

/**
 * Button styled as an icon. The iconClass refers to a class name containing the
 * image to display as font character.
 *
 * @click events should not be attached to this component because it captures those itself, use @select
 */
@Component
public class ButtonIcon extends BasicVueComponent {
  /**
   * Classname of the class containing the image to display as font character.
   */
  @Prop String iconClass;

  @Prop boolean disabled;

  /**
   * Whether this button is inline with other buttons, and thus whether a border
   * radius should be shown
   */
  @Prop boolean inline;

  /**
   * Whether this button should have a contrast-compliant hard border to better
   * indicate it is a button
   */
  @Prop boolean hardBorder;

  /**
   * An accessible label for this button
   */
  @Prop String titleText;

  /**
   * Whether to force the active style
   */
  @Prop boolean pressed;

  @Prop String fontSize;

  @PropDefault("fontSize")
  String fontSizeDefault() {
    return "20px";
  }

  @PropDefault("titleText")
  String titleTextDefault() {
    return "";
  }

  @PropDefault("hardBorder")
  boolean hardBorderDefault() {
    return false;
  }

  @PropDefault("disabled")
  boolean disabledDefault() {
    return false;
  }

  @PropDefault("inline")
  boolean inlineDefault() {
    return false;
  }

  @JsMethod
  public void prevent(final Event ev) {
    ev.preventDefault();
  }

  @PropDefault("pressed")
  boolean pressedDefault() {
    return false;
  }

  @JsMethod
  @Emit
  public void select() {}
}
