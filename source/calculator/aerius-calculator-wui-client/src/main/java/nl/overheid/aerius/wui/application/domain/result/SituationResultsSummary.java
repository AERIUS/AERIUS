/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.result;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.wui.application.domain.info.AssessmentArea;

@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class SituationResultsSummary {

  private SituationResultsStatistics statistics;
  private SituationResultsAreaSummary[] areaStatistics;
  private SituationResultsStatisticReceptors statisticReceptors;
  private SituationResultsMarkers markers;
  private CustomCalculationPointResult[] customPointResults;

  // Areas that have depositions in the individual situation calculation, but not in the overall project calculation.
  // Note: this field is only set for PROJECT calculations.
  private AssessmentArea[] omittedAreas;

  public final @JsOverlay SituationResultsStatistics getStatistics() {
    return statistics;
  }

  public final @JsOverlay SituationResultsAreaSummary[] getAreaStatistics() {
    return areaStatistics;
  }

  public final @JsOverlay SituationResultsStatisticReceptors getStatisticReceptors() {
    return statisticReceptors;
  }

  public final @JsOverlay SituationResultsMarkers getMarkers() {
    return markers;
  }

  public final @JsOverlay CustomCalculationPointResult[] getCustomPointResults() {
    return customPointResults;
  }

  public final @JsOverlay AssessmentArea[] getOmittedAreas() {
    return omittedAreas;
  }
}
