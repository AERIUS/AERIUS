/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.info;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.info.StaticReceptorInfo;
import nl.overheid.aerius.wui.application.domain.result.EmissionResults;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    BasicInfoRowComponent.class,
})
public class BackgroundInfoComponent extends BasicVueComponent {
  @Inject @Data ApplicationContext appContext;

  @Prop @JsProperty StaticReceptorInfo receptorInfo;

  @Computed
  public List<EmissionResultKey> getEmissionResultKeys() {
    return appContext.getConfiguration().getEmissionResultKeys();
  }

  @JsMethod
  public String getBackgroundValueString(final EmissionResultKey emissionResultKey) {
    return MessageFormatter.formatResultWithUnit(getBackgroundValue(emissionResultKey), emissionResultKey,
        appContext.getConfiguration().getEmissionResultValueDisplaySettings());
  }

  @Computed
  public EmissionResults getBackground() {
    return receptorInfo.getBackgroundEmissionResults();
  }

  private Double getBackgroundValue(final EmissionResultKey emissionResultKey) {
    final EmissionResults background = getBackground();
    return background != null && background.hasKey(emissionResultKey)
        ? background.getEmissionResultValue(emissionResultKey)
        : null;
  }

}
