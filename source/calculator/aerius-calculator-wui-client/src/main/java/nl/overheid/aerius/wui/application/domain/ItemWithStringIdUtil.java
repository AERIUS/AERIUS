/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain;

import ol.Feature;

import jsinterop.base.Js;

public final class ItemWithStringIdUtil {
  private ItemWithStringIdUtil() {}

  public static final boolean isTemporaryItem(final ItemWithStringId feature) {
    return isTemporaryLabel(feature.retrieveId());
  }

  public static final boolean isTemporaryFeature(final Feature feature) {
    return isTemporaryItem(Js.uncheckedCast(feature));
  }

  public static final String getStringId(final ItemWithStringId feature) {
    return cleanStringId(feature.retrieveId());
  }

  public static final boolean isTemporaryLabel(final String label) {
    return label.startsWith("-");
  }

  public static final String cleanStringId(final String label) {
    return isTemporaryLabel(label) ? label.substring(1) : label;
  }
}
