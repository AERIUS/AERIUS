/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.interactions;

import ol.OLFactory;
import ol.style.Circle;
import ol.style.CircleOptions;
import ol.style.Fill;
import ol.style.Stroke;
import ol.style.Style;
import ol.style.StyleOptions;

public final class FeatureDrawStyle {

  private static final Stroke EDIT_STYLE_STROKE = OLFactory.createStroke(OLFactory.createColor(21, 124, 177, 1), 2);
  private static final Fill EDIT_STYLE_FILL = OLFactory.createFill(OLFactory.createColor(208, 229, 239, 0.4));
  private static final int EDIT_VERTEX_RADIUS = 6;
  public static final Circle EDIT_POINTER;
  public static final Style EDIT_STYLE;

  private static final Stroke DRAW_POINTER_STROKE = OLFactory.createStroke(OLFactory.createColor(59, 123, 172, 1), 2);
  private static final int DRAW_POINTER_RADIUS = 6;
  public static final Style DRAW_STYLE;

  static {
    final CircleOptions editPointerCircleOptions = new CircleOptions();
    editPointerCircleOptions.setFill(EDIT_STYLE_FILL);
    editPointerCircleOptions.setStroke(EDIT_STYLE_STROKE);
    editPointerCircleOptions.setRadius(EDIT_VERTEX_RADIUS);
    EDIT_POINTER = new Circle(editPointerCircleOptions);

    final StyleOptions editStyleOptions = new StyleOptions();
    editStyleOptions.setStroke(EDIT_STYLE_STROKE);
    editStyleOptions.setFill(EDIT_STYLE_FILL);
    EDIT_STYLE = new Style(editStyleOptions);

    final CircleOptions drawPointerCircleOptions = new CircleOptions();
    drawPointerCircleOptions.setStroke(DRAW_POINTER_STROKE);
    drawPointerCircleOptions.setRadius(DRAW_POINTER_RADIUS);
    final Circle drawPointer = new Circle(drawPointerCircleOptions);

    final StyleOptions drawStyleOptions = new StyleOptions();
    drawStyleOptions.setStroke(EDIT_STYLE_STROKE);
    drawStyleOptions.setFill(EDIT_STYLE_FILL);
    drawStyleOptions.setImage(drawPointer);
    DRAW_STYLE = new Style(drawStyleOptions);
  }
}
