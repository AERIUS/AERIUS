/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.ui.pages.calculate;

import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.wui.application.command.calculation.CalculationCancelCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobStartCommand;
import nl.overheid.aerius.wui.application.components.common.StyledPointComponent;
import nl.overheid.aerius.wui.application.components.progress.JobProgressBarComponent;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationPreparationContext;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationInfo;
import nl.overheid.aerius.wui.application.domain.calculation.JobProgress;
import nl.overheid.aerius.wui.application.ui.pages.calculate.options.CalculationJobStatusComponent;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    StyledPointComponent.class,
    JobProgressBarComponent.class,
    CalculationJobStatusComponent.class
})
public class CalculationJobRowComponent extends BasicVueComponent {
  @Prop CalculationJobContext job;
  @Prop int index;
  @Prop EventBus eventBus;
  @Prop boolean selected;

  @Inject @Data CalculationPreparationContext prepContext;
  @Inject @Data ScenarioContext scenarioContext;

  @Computed
  protected JobProgress getJobProgress() {
    final CalculationExecutionContext calculation = prepContext.getActiveCalculation(job);
    final CalculationInfo calculationInfo = calculation == null ? null : calculation.getCalculationInfo();

    return calculationInfo == null ? null : calculationInfo.getJobProgress();
  }

  @JsMethod
  public boolean isInitialized() {
    return calculationHasStarted();
  }

  @JsMethod
  public boolean isNotFinished() {
    return getCalculationContextForJob() != null && !getCalculationContextForJob().getJobState().isFinalState();
  }

  @Computed
  public boolean isStaleState() {
    return ScenarioContext.isCalculationStale(scenarioContext, job, getCalculationContextForJob());
  }

  @JsMethod
  protected void cancelCalculation() {
    // Silently cancel and delete the existing calculation, if any
    Optional.ofNullable(getCalculationContextForJob()).ifPresent(v -> eventBus.fireEvent(new CalculationCancelCommand(v)));
  }

  @JsMethod
  protected void startCalculation() {
    eventBus.fireEvent(new CalculationJobStartCommand(job));
  }

  private boolean calculationHasStarted() {
    return getCalculationContextForJob() != null && getCalculationContextForJob().getJobKey() != null;
  }

  private CalculationExecutionContext getCalculationContextForJob() {
    return prepContext.getActiveCalculation(job);
  }
}
