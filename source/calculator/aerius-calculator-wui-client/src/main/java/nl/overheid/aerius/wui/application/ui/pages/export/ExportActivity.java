/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.ui.pages.export;

import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.wui.application.command.ResetEdgeEffectReportExportSettingsCommand;
import nl.overheid.aerius.wui.application.command.calculation.VerifyCalculationJobDefaultsCommand;
import nl.overheid.aerius.wui.application.command.exporter.VerifyExportDefaultsCommand;
import nl.overheid.aerius.wui.application.ui.main.MainView;
import nl.overheid.aerius.wui.application.ui.pages.ThemeDelegatedActivity;
import nl.overheid.aerius.wui.application.ui.pages.ThemeView;

public class ExportActivity extends BasicEventComponent implements ThemeDelegatedActivity {
  @Override
  public void onStart(final ThemeView view) {
    view.setDelegatedPresenter(this);
    view.setRightView(ExportSituationSummaryFactory.get());
    view.setLeftView(ExportViewFactory.get(), MainView.FlexView.RIGHT);
    view.setNoMiddleView();

    eventBus.fireEvent(new VerifyExportDefaultsCommand());
    eventBus.fireEvent(new VerifyCalculationJobDefaultsCommand());
    eventBus.fireEvent(new ResetEdgeEffectReportExportSettingsCommand());
  }
}
