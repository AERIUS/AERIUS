/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.manual;

import java.util.function.Consumer;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.Vue;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;

import elemental2.dom.Element;
import elemental2.dom.HTMLAnchorElement;
import elemental2.dom.HTMLDivElement;
import elemental2.dom.HTMLElement;
import elemental2.dom.HTMLImageElement;
import elemental2.dom.Node;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.vue.directives.VectorDirective;
import nl.overheid.aerius.wui.application.util.NumberUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;
import nl.overheid.aerius.wui.vue.StyleUtil;

@Component(name = "html-output", directives = {VectorDirective.class})
public class HTMLOutputComponent extends BasicVueComponent implements HasMounted {
  @Prop String html;
  @Prop String path;

  @Prop String base;
  @Prop boolean showScrollToTop;

  @Ref HTMLDivElement contentScrollContainer;
  @Ref HTMLDivElement contentTarget;

  @Data boolean noScrollBar;

  @Watch(value = "html", isImmediate = true)
  public void onHTMLChange(final String neww, final String old) {
    Vue.nextTick(() -> changeHTML(neww));
  }

  @PropDefault("showScrollToTop")
  public boolean showScrollToTopDefault() {
    return false;
  }

  @JsMethod
  public void scrollToTop() {
    contentScrollContainer.scrollTop = 0D;
  }

  private void changeHTML(final String neww) {
    // It's possible for contentScrollContainer to be null during various moments of the component lifecycle,
    // warn when this occurs so it doesn't quietly not work
    if (contentScrollContainer == null) {
      GWTProd.log("contentScrollContainer does not exist; content could not be rendered");
      return;
    }

    contentTarget.innerHTML = neww;
    contentScrollContainer.scrollTop = 0D;
    updateNoScrollBar();
    postProcess(contentTarget);
  }

  @Override
  public void mounted() {
    contentScrollContainer.addEventListener("scroll", e -> updateNoScrollBar());
  }

  @JsMethod
  public void updateNoScrollBar() {
    noScrollBar = NumberUtil.equalExactly(contentScrollContainer.scrollTop, 0D);
  }

  private void postProcess(final HTMLDivElement target) {
    // Copy data attributes so the parent component can used its scoped style to mark things up
    StyleUtil.propagateDataAttributes(target);

    // Traverse the child nodes and intercept anything that contains an url
    traverse(target, elem -> {
      if (elem instanceof HTMLAnchorElement) {
        final HTMLAnchorElement anchor = (HTMLAnchorElement) elem;
        interceptAnchor(anchor);
        if (elem.classList.contains("header-anchor")) {
          anchor.tabIndex = -1;
        }
      }
      if (elem instanceof HTMLImageElement) {
        interceptImg((HTMLImageElement) elem);
      }
    });
  }

  private void interceptImg(final HTMLImageElement img) {
    final String srcUrl = img.getAttribute("src");

    final String finalUrl;
    if (srcUrl.startsWith("./")) {
      // Note: Due to the way vite processes relative images, this won't work (images should be put in /public instead)
      // However, because it should work in principle when vite doesn't do its asset processing, it's left in
      // We will warn that this is happening, though
      final String derelativizedUrl = srcUrl.startsWith("./") ? srcUrl.substring(1) : srcUrl;
      GWTProd.warn("Processing relative image url: " + srcUrl + " > " + base + path + derelativizedUrl);
      finalUrl = base + path + derelativizedUrl;
    } else if (srcUrl.startsWith("/")) {
      finalUrl = base + srcUrl;
    } else {
      finalUrl = srcUrl;
    }

    img.src = finalUrl;
  }

  private void interceptAnchor(final HTMLAnchorElement anchor) {
    // We can't use .href because that is contaminated with browser "intelligence"
    final String href = anchor.getAttribute("href");

    if (isRelative(href)) {
      anchor.onclick = e -> {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        vue().$emit("navigate", href);
        return null;
      };
    } else if (isInternal(href)) {
      anchor.onclick = e -> {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        vue().$emit("scroll", href);
        return null;
      };
    }
  }

  private boolean isInternal(final String href) {
    return href.startsWith("#");
  }

  private static boolean isRelative(final String href) {
    return href.startsWith("./");
  }

  private void traverse(final HTMLElement target, final Consumer<Element> runner) {
    target.childNodes.asList().stream()
        .filter(node -> node.nodeType == Node.ELEMENT_NODE)
        .forEach(node -> {
          if (node instanceof Element) {
            final HTMLElement elem = (HTMLElement) node;
            runner.accept(elem);
            traverse(elem, runner);
          }
        });
  }
}
