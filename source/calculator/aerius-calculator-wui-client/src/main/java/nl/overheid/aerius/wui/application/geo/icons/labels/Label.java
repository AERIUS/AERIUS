/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.icons.labels;

/**
 * Represents a label in a map icon
 */
public class Label {

  private final String labelText;
  private final String backgroundColor;
  private final String textColor;
  private final Character[] indicators;

  public Label(final String labelText, final String backgroundColor, final String textColor) {
    this.labelText = labelText;
    this.backgroundColor = backgroundColor;
    this.textColor = textColor;
    this.indicators = new Character[0];
  }

  public Label(final String labelText, final String backgroundColor, final String textColor, final Character[] indicators) {
    this.labelText = labelText;
    this.backgroundColor = backgroundColor;
    this.textColor = textColor;
    this.indicators = indicators;
  }

  public String getLabelText() {
    return labelText;
  }

  public String getBackgroundColor() {
    return backgroundColor;
  }

  public String getTextColor() {
    return textColor;
  }

  public Character[] getIndicators() {
    return indicators;
  }

  public void appendCacheKey(final StringBuilder cacheKey) {
    cacheKey.append(labelText).append(".")
        .append(backgroundColor).append(".")
        .append(textColor).append(".");
    for (final Character indicator : indicators) {
      cacheKey.append(indicator);
    }
  }
}
