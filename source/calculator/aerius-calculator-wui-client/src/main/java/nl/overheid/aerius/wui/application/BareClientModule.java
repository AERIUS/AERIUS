/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application;

import com.google.gwt.inject.client.AbstractGinModule;
import com.google.gwt.inject.client.assistedinject.GinFactoryModuleBuilder;
import com.google.gwt.place.shared.PlaceHistoryHandler.Historian;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;

import nl.aerius.geo.proj4.OL3Proj4Initializer;
import nl.aerius.geo.proj4.Proj4Initializer;
import nl.aerius.geo.wui.MapLayoutPanel;
import nl.aerius.search.wui.config.SearchConfiguration;
import nl.aerius.wui.activity.ActivityManager;
import nl.aerius.wui.activity.ActivityMapper;
import nl.aerius.wui.daemon.DaemonBootstrapper;
import nl.aerius.wui.dev.DevelopmentObserver;
import nl.aerius.wui.history.HTML5Historian;
import nl.aerius.wui.history.PlaceHistoryMapper;
import nl.aerius.wui.init.Initializer;
import nl.aerius.wui.place.ApplicationPlace;
import nl.aerius.wui.place.DefaultPlace;
import nl.aerius.wui.vue.AcceptsOneComponent;
import nl.aerius.wui.vue.activity.VueActivityManager;
import nl.overheid.aerius.wui.application.activity.ApplicationActivityFactory;
import nl.overheid.aerius.wui.application.activity.ApplicationActivityMapper;
import nl.overheid.aerius.wui.application.activity.ApplicationPlaceHistoryMapper;
import nl.overheid.aerius.wui.application.config.EnvironmentConfigurationImpl;
import nl.overheid.aerius.wui.application.daemon.ApplicationDaemonBootstrapper;
import nl.overheid.aerius.wui.application.dev.ApplicationDevelopmentObserver;
import nl.overheid.aerius.wui.application.factories.ExportStatusPollerFactory;
import nl.overheid.aerius.wui.application.geo.layers.LayerFactory;
import nl.overheid.aerius.wui.application.init.ApplicationInitializer;
import nl.overheid.aerius.wui.application.place.CalculatorDefaultPlace;
import nl.overheid.aerius.wui.application.ui.nca.NcaDelegatedActivityFactory;
import nl.overheid.aerius.wui.application.ui.own2000.OwN2000DelegatedActivityFactory;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;

class BareClientModule extends AbstractGinModule {
  @Override
  protected void configure() {
    bind(ApplicationPlace.class).annotatedWith(DefaultPlace.class).to(CalculatorDefaultPlace.class);
    bind(Historian.class).to(HTML5Historian.class);

    bind(new TypeLiteral<ActivityManager<AcceptsOneComponent>>() {}).to(VueActivityManager.class);
    bind(new TypeLiteral<ActivityMapper<AcceptsOneComponent>>() {}).to(ApplicationActivityMapper.class);
    bind(Initializer.class).to(ApplicationInitializer.class);
    bind(DaemonBootstrapper.class).to(ApplicationDaemonBootstrapper.class);
    bind(PlaceHistoryMapper.class).to(ApplicationPlaceHistoryMapper.class);
    bind(DevelopmentObserver.class).to(ApplicationDevelopmentObserver.class);

    bind(SearchConfiguration.class).to(EnvironmentConfiguration.class);
    bind(EnvironmentConfiguration.class).to(EnvironmentConfigurationImpl.class);
    bind(MapLayoutPanel.class).in(Singleton.class);
    bind(Proj4Initializer.class).to(OL3Proj4Initializer.class);

    // Bind factories
    install(new GinFactoryModuleBuilder().build(ApplicationActivityFactory.class));
    install(new GinFactoryModuleBuilder().build(NcaDelegatedActivityFactory.class));
    install(new GinFactoryModuleBuilder().build(OwN2000DelegatedActivityFactory.class));

    install(new GinFactoryModuleBuilder().build(ExportStatusPollerFactory.class));
    install(new GinFactoryModuleBuilder().build(LayerFactory.class));
  }
}
