/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.nca.pages.calculate.options;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.backgrounddata.MetSite;
import nl.overheid.aerius.wui.application.components.input.InputWarningComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.domain.calculation.ADMSOptions;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;

@Component(components = {
    ValidationBehaviour.class,
    InputWarningComponent.class,
    LabeledInputComponent.class,
    SimplifiedListBoxComponent.class,
})
public class MetSiteSuggestComponent extends ErrorWarningValidator {
  @Prop(required = true) CalculationSetOptions options;
  @Prop(required = true) @JsProperty List<MetSite> siteLocations;
  @Prop(required = true) SituationComposition composition;

  @Inject @Data ScenarioContext scenarioContext;

  @Computed
  public List<MetSiteWithDistance> getSuggestSiteLocations() {
    return TemporaryGeometryUtil.getSuggestedSiteLocations(siteLocations, options.getCalculationJobType(), composition);
  }

  @JsMethod
  public String formatDistance(final double dist) {
    return MessageFormatter.formatDistanceKmWithUnit(dist);
  }

  @Computed
  public String getMeteoSiteLocation() {
    return String.valueOf(getAdmsOptions().getMetSiteId());
  }

  @JsMethod
  public void toggleSiteLocation(final MetSite siteLocation) {
    if (getAdmsOptions().getMetSiteId() == siteLocation.getId()) {
      getAdmsOptions().setMetSiteId(0);
    } else {
      getAdmsOptions().setMetSiteId(siteLocation.getId());
    }
  }

  private ADMSOptions getAdmsOptions() {
    return options.getNcaCalculationOptions().getAdmsOptions();
  }

  @Computed("isMetSiteInvalid")
  public boolean isMetSiteInvalid() {
    return getAdmsOptions().getMetSiteId() == 0
        || !siteLocations.stream()
            .anyMatch(v -> v.getId() == getAdmsOptions().getMetSiteId());
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return isMetSiteInvalid();
  }
}
