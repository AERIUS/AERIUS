/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.scenario;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import javax.inject.Inject;

import com.google.web.bindery.event.shared.EventBus;

import nl.aerius.wui.future.AppAsyncCallback;
import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.wui.application.components.importer.domain.ImportAction.ActionType;
import nl.overheid.aerius.wui.application.domain.importer.ImportFilter;
import nl.overheid.aerius.wui.application.domain.importer.ImportParcel;
import nl.overheid.aerius.wui.application.event.ImportFailureEvent;
import nl.overheid.aerius.wui.application.service.ScenarioServiceAsync;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;

/**
 * Poller to retrieve imported situations from the server.
 *
 * Situations are identified by the file code as returned when the file(s)
 * containing the situation information was uploaded.
 */
public class FileImportPoller {
  private static final int INTERVAL = 1000;
  @Inject private ScenarioServiceAsync service;
  @Inject private EventBus eventBus;

  private List<FileUploadStatus> files;
  private final Map<FileUploadStatus, ImportParcel> parcels = new LinkedHashMap<>();

  private boolean running;

  private Consumer<Map<FileUploadStatus, ImportParcel>> complete;

  public void start(final List<FileUploadStatus> files, final Consumer<Map<FileUploadStatus, ImportParcel>> complete) {
    if (running) {
      return;
    }

    this.files = files;
    this.complete = complete;

    parcels.clear();
    running = true;

    SchedulerUtil.delay(() -> this.doFetch());
  }

  public boolean isRunning() {
    return running;
  }

  private void doFetch() {
    if (files.isEmpty()) {
      finish();
      return;
    }

    final FileUploadStatus file = files.get(parcels.size());
    final boolean keepFile = shouldKeepFile(file);
    service.importSituation(file.getFileCode(), keepFile, AppAsyncCallback.create(parcel -> {
      tryProcess(file, parcel);
    }, e -> {
      running = false;
      eventBus.fireEvent(new ImportFailureEvent(e));
    }));
  }

  private boolean shouldKeepFile(final FileUploadStatus file) {
    // Always keep archive contribution for later: these can contain no results, in which case it should still be imported.
    if (file.getImportAction().getType() == ActionType.ADD_ARCHIVE_CONTRIBUTION) {
      return true;
    } else {
      // Otherwise figure out if any file in the group is expected to be imported with results,
      // in which case all files in that group should be kept.
      return files.stream()
          .filter(v -> v.getFile().equals(file.getFile()))
          .anyMatch(x -> x.getSituationStats().getCalculationResults() > 0 && file.getFilters().isAllowed(ImportFilter.RESULTS));
    }
  }

  private void tryProcess(final FileUploadStatus file, final ImportParcel parcel) {
    if (parcel == null) {
      scheduleFetch();
    } else {
      process(file, parcel);
    }
  }

  private void process(final FileUploadStatus file, final ImportParcel parcel) {
    try {
      parcels.put(file, parcel);
      tryFinishImport();
    } catch (final RuntimeException e) {
      running = false;
      eventBus.fireEvent(new ImportFailureEvent(e));
      parcels.remove(file);
    }
  }

  private void tryFinishImport() {
    if (files.size() == parcels.size()) {
      finish();
    } else {
      doFetch();
    }
  }

  private void finish() {
    running = false;
    complete.accept(parcels);
  }

  private void scheduleFetch() {
    SchedulerUtil.delay(() -> doFetch(), INTERVAL);
  }
}
