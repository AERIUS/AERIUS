/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.misc;

import javax.inject.Singleton;

import nl.overheid.aerius.shared.domain.v2.point.AssessmentCategory;
import nl.overheid.aerius.wui.application.ui.pages.preferences.PreferencesView.ImportMode;

/**
 * A set of persistable user settings.
 *
 * All of these values should be considered optional, no default values other than null/false/0 should be set.
 * Components using these settings should take care of their own defaults if any.
 */
@Singleton
public class PersistablePreferencesContext {
  private String selectedProjection = "";
  private ImportMode importMode = null;
  private boolean hidePrivacyWarning = false;
  private boolean hideSectorWarning = false;
  private boolean layerAutoSwitch = false;
  private boolean loginEnabled = false;
  private boolean manualAutoSwitch = true;
  private AssessmentCategory assessmentCategory = null;
  private boolean collapsedMenu = false;

  public String getSelectedProjection() {
    return selectedProjection;
  }

  public void setSelectedProjection(final String selectedProjection) {
    this.selectedProjection = selectedProjection;
  }

  public ImportMode getImportMode() {
    return importMode;
  }

  public void setImportMode(final ImportMode importMode) {
    this.importMode = importMode;
  }

  public void setHidePrivacyWarning(final boolean hidePrivacyWarning) {
    this.hidePrivacyWarning = hidePrivacyWarning;
  }

  public boolean isHidePrivacyWarning() {
    return hidePrivacyWarning;
  }

  public void setHideSectorWarning(final boolean hideSectorWarning) {
    this.hideSectorWarning = hideSectorWarning;
  }

  public boolean isHideSectorWarning() {
    return hideSectorWarning;
  }

  public void setLayerAutoSwitch(final boolean layerAutoSwitch) {
    this.layerAutoSwitch = layerAutoSwitch;
  }

  public boolean isLayerAutoSwitch() {
    return layerAutoSwitch;
  }

  public boolean isManualAutoSwitch() {
    return manualAutoSwitch;
  }

  public void setManualAutoSwitch(final boolean manualAutoSwitch) {
    this.manualAutoSwitch = manualAutoSwitch;
  }

  public boolean isLoginEnabled() {
    return loginEnabled;
  }

  public void setLoginEnabledSwitch(final boolean loginEnabled) {
    this.loginEnabled = loginEnabled;
  }

  public AssessmentCategory getAssessmentCategory() {
    return assessmentCategory;
  }

  public void setAssessmentCategory(final AssessmentCategory assessmentCategory) {
    this.assessmentCategory = assessmentCategory;
  }

  public boolean isCollapsedMenu() {
    return collapsedMenu;
  }

  public void setCollapsedMenu(final boolean collapsedMenu) {
    this.collapsedMenu = collapsedMenu;
  }

}
