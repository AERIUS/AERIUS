/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.farm;

import java.util.Map;

import elemental2.core.JsArray;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.FarmAdditionalHousingSystemCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalHousingCategories;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalHousingCategory;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;

/**
 * Class for standard Farm Animal Housing emissions, based on a category.
 */
public class FarmAnimalHousingStandardUtil {

  /**
   * Fraction that main system emission reduction is constrained by if there are any scrubbers.
   */
  private static final double SCRUBBER_CONSTRAIN_FRACTION = 0.3;

  private FarmAnimalHousingStandardUtil() {
    // Util class
  }

  /**
   * Get emission of the base type without additional systems.
   */
  public static double getFlatEmission(final FarmAnimalHousingCategories categories, final StandardFarmAnimalHousing sfah,
      final Substance substance) {
    return sfah.getNumberOfAnimals() * periodFactor(categories, sfah) * getEmissionFactor(categories, sfah, substance);
  }

  /**
   * Get emission of the base type, unconstrained.
   */
  public static double getUnconstrainedFlatEmission(final FarmAnimalHousingCategories categories, final StandardFarmAnimalHousing sfah,
      final Substance substance) {
    return sfah.getNumberOfAnimals() * periodFactor(categories, sfah) * getUnconstrainedEmissionFactor(categories, sfah, substance);
  }

  /**
   * Calculate emissions for lodging with additional systems.
   * Can be used to calculate emissions up to a specific additional system.
   * If the supplied upTo system is null, all additional systems will be included.
   */
  public static double getEmissionsAdditional(final FarmAnimalHousingCategories categories, final StandardFarmAnimalHousing sfah,
      final Substance substance, final AdditionalHousingSystem upTo) {
    // First get the (possibly constrained) emissions of the base lodging.
    double emission = getFlatEmission(categories, sfah, substance);

    // Add emissions of additional types
    final JsArray<AdditionalHousingSystem> alss = sfah.getAdditionalSystems();

    for (int i = 0; i < alss.length; i++) {
      final AdditionalHousingSystem als = alss.getAt(i);

      emission *= (1.0 - getAdditionalHousingFactor(categories, sfah, substance, als));
      if (als == upTo) {
        break;
      }
    }
    return emission;
  }

  private static int periodFactor(final FarmAnimalHousingCategories categories, final StandardFarmAnimalHousing sfah) {
    final FarmAnimalHousingCategory category = categories.determineHousingCategoryByCode(sfah.getAnimalHousingCode());

    return category != null && category.getFarmEmissionFactorType() == FarmEmissionFactorType.PER_ANIMAL_PER_DAY ? sfah.getNumberOfDays() : 1;
  }

  /**
   *
   * @param categories
   * @param sfah
   * @param substance
   * @param additional
   * @return
   */
  public static double getAdditionalHousingFactor(final FarmAnimalHousingCategories categories, final StandardFarmAnimalHousing sfah,
      final Substance substance, final AdditionalHousingSystem additional) {
    if (sfah == null || additional == null) {
      return 0;
    } else if (AdditionalSystemType.STANDARD == additional.getAdditionalSystemType()) {
      final FarmAdditionalHousingSystemCategory category = categories
          .determineAdditionalHousingCategoryByCode(((StandardAdditionalHousingSystem) additional).getAdditionalSystemCode());
      final Map<Substance, Double> factors = category == null ? null : category.getReductionFractions().get(sfah.getAnimalHousingCode());

      return factors == null ? 0.0 : factors.get(substance);
    } else if (AdditionalSystemType.CUSTOM == additional.getAdditionalSystemType()) {
      return ((CustomAdditionalHousingSystem) additional).getEmissionReductionFactor(substance);
    } else {
      return 0;
    }
  }

  /**
   * Calculate total emissions.
   *
   * @return total emission
   */
  public static double getEmissions(final FarmAnimalHousingCategories categories, final StandardFarmAnimalHousing sfah, final Substance substance) {
    return getEmissionsAdditional(categories, sfah, substance, null);
  }

  /**
   * Get unconstrained emission factor of the base type.
   */
  public static double getUnconstrainedEmissionFactor(final FarmAnimalHousingCategories categories, final StandardFarmAnimalHousing sfah,
      final Substance substance) {
    final FarmAnimalHousingCategory category = categories.determineHousingCategoryByCode(sfah.getAnimalHousingCode());

    return category != null && Substance.NH3 == substance ? category.getEmissionFactors().get(Substance.NH3) : 0;
  }

  /**
   * Get emission factor of the base type (could be constrained).
   */
  public static double getEmissionFactor(final FarmAnimalHousingCategories categories, final StandardFarmAnimalHousing sfah,
      final Substance substance) {
    final FarmAnimalHousingCategory category = categories.determineHousingCategoryByCode(sfah.getAnimalHousingCode());

    if (category != null && Substance.NH3 == substance) {
      final double systemEmissionFactor = category.getEmissionFactors().get(substance).doubleValue();

      if (isContrained(categories, sfah)) {
        final double reducedBasicHousingEmissionFactor = reducedBasicHousingEmissionFactor(categories, sfah);

        if (reducedBasicHousingEmissionFactor > 0) {
          return Math.max(systemEmissionFactor, reducedBasicHousingEmissionFactor);
        }
      }
      return systemEmissionFactor;
    }
    return 0.0;
  }

  public static FarmEmissionFactorType getEmissionFactorType(final FarmAnimalHousingCategories categories, final StandardFarmAnimalHousing sfah) {
    final FarmEmissionFactorType emissionFactorType;
    final FarmAnimalHousingCategory category = categories.determineHousingCategoryByCode(sfah.getAnimalHousingCode());

    if (category == null) {
      emissionFactorType = FarmEmissionFactorType.PER_ANIMAL_PER_YEAR;
    } else {
      emissionFactorType = category.getFarmEmissionFactorType();
    }
    return emissionFactorType;
  }

  /**
   * Determine if the base animal housing emission is constrained by any of the additional systems.
   */
  public static boolean isContrained(final FarmAnimalHousingCategories categories, final StandardFarmAnimalHousing sfah) {
    final JsArray<AdditionalHousingSystem> alsArray = sfah.getAdditionalSystems();

    for (int i = 0; i < alsArray.length; i++) {
      final AdditionalHousingSystem additional = alsArray.getAt(i);

      if ((isStandardAirscrubber(categories, additional) || isCustomAirscrubber(additional)) && isContrainedByBasic(categories, sfah)) {
        return true;
      }
    }
    return false;
  }

  private static boolean isStandardAirscrubber(final FarmAnimalHousingCategories categories, final AdditionalHousingSystem additional) {
    if (AdditionalSystemType.STANDARD == additional.getAdditionalSystemType()) {
      final FarmAdditionalHousingSystemCategory systemCategory = categories
          .determineAdditionalHousingCategoryByCode(((StandardAdditionalHousingSystem) additional).getAdditionalSystemCode());

      return systemCategory != null && systemCategory.isAirScrubber();
    }
    return false;
  }

  private static boolean isCustomAirscrubber(final AdditionalHousingSystem additional) {
    return AdditionalSystemType.CUSTOM == additional.getAdditionalSystemType() && ((CustomAdditionalHousingSystem) additional).isAirScrubber();
  }

  private static boolean isContrainedByBasic(final FarmAnimalHousingCategories categories, final StandardFarmAnimalHousing sfah) {
    final double reducedBasicHousingEmissionFactor = reducedBasicHousingEmissionFactor(categories, sfah);

    if (reducedBasicHousingEmissionFactor > 0) {
      final FarmAnimalHousingCategory category = categories.determineHousingCategoryByCode(sfah.getAnimalHousingCode());
      if (category != null) {
        final double systemEmissionFactor = category.getEmissionFactors().get(Substance.NH3).doubleValue();

        return systemEmissionFactor < reducedBasicHousingEmissionFactor;
      }
    }
    return false;
  }

  public static boolean hasAdditionalHousingSystemCategories(final FarmAnimalHousingCategories farmAnimalHousingCategories) {
    return !farmAnimalHousingCategories.getAdditionalHousingSystemCategories().isEmpty();
  }

  private static double reducedBasicHousingEmissionFactor(final FarmAnimalHousingCategories categories, final StandardFarmAnimalHousing sfah) {
    final String basicHousingCode = categories.getAnimalBasicHousingCategoryCodes().get(sfah.getAnimalHousingCode());

    if (basicHousingCode != null && !basicHousingCode.equals(sfah.getAnimalHousingCode())) {
      final FarmAnimalHousingCategory basicHoudingCategory = categories.determineHousingCategoryByCode(basicHousingCode);

      return basicHoudingCategory.getEmissionFactors().get(Substance.NH3).doubleValue() * SCRUBBER_CONSTRAIN_FRACTION;
    }
    return -1.0;
  }
}
