/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.print;

import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.vue.VueComponentFactory;
import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import elemental2.dom.DomGlobal;
import elemental2.dom.HTMLElement;

import jsinterop.base.Js;

import nl.aerius.wui.activity.Presenter;
import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.util.SchedulerUtil;
import nl.aerius.wui.vue.activity.AbstractVueActivity;
import nl.overheid.aerius.wui.application.command.RestoreScenarioContextCommand;
import nl.overheid.aerius.wui.application.context.PrintContext;
import nl.overheid.aerius.wui.application.event.ApplicationFinishedLoadingEvent;
import nl.overheid.aerius.wui.application.place.PrintPlace;

public class PrintActivity<P extends Presenter, V extends IsVueComponent, F extends VueComponentFactory<V>>
    extends AbstractVueActivity<P, V, F> implements PrintPresenter {

  @SuppressWarnings("rawtypes")
  interface PrintActivityEventBinder extends EventBinder<PrintActivity> {}

  // 3 minutes
  private static final int RENDER_FINISH_FALLBACK_PERIOD_MILLISECONDS = 3 * 60 * 1_000;

  private static final PrintActivityEventBinder EVENT_BINDER = GWT.create(PrintActivityEventBinder.class);

  private final PrintPlace place;

  private @Inject PrintContext context;

  public PrintActivity(final F viewFactory, final PrintPlace place) {
    super(viewFactory);
    this.place = place;
  }

  @EventHandler
  void onApplicationFinishedLoadingEvent(final ApplicationFinishedLoadingEvent e) {
    SchedulerUtil.delay(() -> restoreScenario());

    // Set the font size here for debugging purposes. (the print css also sets it)
    if (GWTProd.isDev()) {
      final HTMLElement documentElement = Js.uncheckedCast(DomGlobal.document.documentElement);
      documentElement.style.setProperty("--font-size", "8pt");
      GWTProd.warn("NOTE: Font size forced to 8pt to match print style");
    }
  }

  /**
   * Restore scenario after whole application finished loading.
   */
  void restoreScenario() {
    GWTProd.log("Job Key: ", place.getJobKey());
    context.setJobKey(place.getJobKey());

    GWTProd.log("Reference: ", place.getReference());
    context.setReference(place.getReference());

    GWTProd.log("Pdf product type: ", place.getPdfProductType());
    context.setPdfProductType(place.getPdfProductType());

    GWTProd.log("Proposed Situation Id: ", place.getProposedSituationId());
    eventBus.fireEvent(new RestoreScenarioContextCommand(place.getJobKey(), place.getProposedSituationId()));

    SchedulerUtil.delay(() -> context.fail(), RENDER_FINISH_FALLBACK_PERIOD_MILLISECONDS);
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    setEventBus(eventBus, this, EVENT_BINDER);
  }

  @SuppressWarnings("unchecked")
  @Override
  public P getPresenter() {
    return (P) this;
  }
}
