/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.nca.pages.emisionsourceList.timevarying;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.google.gwt.i18n.client.LocaleInfo;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfileType;
import nl.overheid.aerius.wui.application.components.divider.DividerComponent;
import nl.overheid.aerius.wui.application.components.source.DetailStatComponent;
import nl.overheid.aerius.wui.application.domain.source.TimeVaryingProfile;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    DividerComponent.class,
    DetailStatComponent.class,
    VerticalCollapse.class,
    MonthlyProfileStatsView.class,
    ThreeDayProfileStatsView.class,
})
public class TimeVaryingProfileDetailView extends BasicVueComponent {
  @Prop TimeVaryingProfile timeVaryingProfile;
  @Prop int headingLevel;
  @Prop boolean stats;

  @PropDefault("stats")
  boolean statsDefault() {
    return true;
  }

  @PropDefault("headingLevel")
  public int headingLevelDefault() {
    return 2;
  }

  @Computed
  public String getLocale() {
    return LocaleInfo.getCurrentLocale().getLocaleName();
  }

  @Computed
  public Double[] getTimeVaryingProfileData() {
    return timeVaryingProfile.getValues().toArray(new Double[timeVaryingProfile.getValues().size()]);
  }

  @Computed
  public String getTimeVaryingProfileType() {
    return timeVaryingProfile.getType().name();
  }

  @JsMethod
  public boolean isTimeVaryingProfielTitleCustom() {
    return !timeVaryingProfile.getLabel().equals(i18n.timeVaryingProfilePanelTitle(timeVaryingProfile.getType()));
  }

  @JsMethod
  public String getDebugId(final String debugId) {
    return debugId + '-' + getTimeVaryingProfileType();
  }

  @Computed boolean isThreeDayProfile() {
    return timeVaryingProfile.getType() == CustomTimeVaryingProfileType.THREE_DAY;
  }

  @Computed boolean isMonthlyProfile() {
    return timeVaryingProfile.getType() == CustomTimeVaryingProfileType.MONTHLY;
  }
}
