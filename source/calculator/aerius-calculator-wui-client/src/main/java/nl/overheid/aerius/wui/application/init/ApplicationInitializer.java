/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.init;

import com.axellience.vuegwt.core.client.Vue;
import com.google.inject.Inject;

import elemental2.core.JsArray;
import elemental2.core.JsString;

import nl.aerius.geo.proj4.Proj4Initializer;
import nl.aerius.geo.wui.MapLayoutPanel;
import nl.aerius.wui.future.AppAsyncCallback;
import nl.aerius.wui.init.AbstractInitializer;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.ProductProfile;
import nl.overheid.aerius.shared.service.v2.LegacyContextServiceAsync;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.daemon.flags.ApplicationFlagDaemon;
import nl.overheid.aerius.wui.application.daemon.misc.ColorThemeDaemon;
import nl.overheid.aerius.wui.application.geo.layers.results.CalculationMarkers;
import nl.overheid.aerius.wui.application.geo.util.MapPropertiesUtil;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.util.SectorColorUtilDeprecated;
import nl.overheid.aerius.wui.application.util.WKTUtil;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;
import nl.overheid.aerius.wui.util.ES6Util;

public class ApplicationInitializer extends AbstractInitializer {
  private static final int INIT_TARGET = 1;

  private final LegacyContextServiceAsync contextService = LegacyContextServiceAsync.Util.getInstance();

  private @Inject EnvironmentConfiguration cfg;
  private @Inject ApplicationContext applicationContext;
  private @Inject ApplicationFlagDaemon applicationFlagDaemon;
  private @Inject MapLayoutPanel map;
  private @Inject UrlPlaceholderResolver urlPlaceholderResolver;
  private @Inject Proj4Initializer proj4;
  private @Inject ColorThemeDaemon colorThemeDaemon;

  public ApplicationInitializer() {
    super(INIT_TARGET);
  }

  @Override
  public void init() {
    ES6Util.register("DepositionGraphElement", "/components", "deposition-graph-component", cfg.getApplicationVersion());
    ES6Util.register("TimeVaryingProfileGraphComponent", "/components", "time-varying-profile-graph-component", cfg.getApplicationVersion());
    ES6Util.register("ThresholdIndicatorComponent", "/components", "threshold-indicator-component", cfg.getApplicationVersion());

    /**
     * Initialize marker combinations
     */
    CalculationMarkers.init();

    // Set the complete array because there is a bug in addIgnoredElement that won't
    // allow us to add a String
    final JsArray<JsString> arr = new JsArray<>();
    arr.push(new JsString("deposition-graph-component"));
    arr.push(new JsString("time-varying-profile-graph-component"));
    arr.push(new JsString("threshold-indicator-component"));
    Vue.getConfig().setIgnoredElements(arr);
    applicationContext.setProductProfile(ProductProfile.safeValueOf(MetaReader.readMetaData("aerius:productProfile")));
    contextService.getContext(applicationContext.getProductProfile(), AppAsyncCallback.create(
        this::initAppContext,
        e -> fail(M.messages().errorCouldNotLoadApplicationConfiguration(), e)));
  }

  private void initAppContext(final ApplicationConfiguration configuration) {
    try {
      urlPlaceholderResolver.replaceGeoserverPlaceholders(configuration);
      applicationContext.setConfiguration(configuration);
      applicationFlagDaemon.initFlags();
      colorThemeDaemon.initColorTheme(applicationContext.getTheme(), applicationContext.getProductProfile());
      SectorColorUtilDeprecated.init(applicationContext.getConfiguration());
      WKTUtil.initGridProjection(configuration.getReceptorGridSettings().getEPSG().getEpsgCode());
      initProjections(configuration);
      map.init(MapPropertiesUtil.getMapProps(configuration.getReceptorGridSettings().getEPSG()));
      complete();
    } catch (final RuntimeException e) {
      fail(M.messages().errorInternalFatal(), e);
    }
  }

  private void initProjections(final ApplicationConfiguration configuration) {
    configuration.getProjections().forEach((k, v) -> proj4.defs(k, v));
    proj4.register();
  }
}
