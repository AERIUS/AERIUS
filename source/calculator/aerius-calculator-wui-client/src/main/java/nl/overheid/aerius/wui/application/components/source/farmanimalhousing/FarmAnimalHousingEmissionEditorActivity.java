/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.farmanimalhousing;

import elemental2.core.JsArray;

import nl.overheid.aerius.wui.application.components.modify.ModifyListActions;
import nl.overheid.aerius.wui.application.components.source.SubSourceEmissionEditorPresenter;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmAnimalHousing;
import nl.overheid.aerius.wui.application.domain.source.farm.StandardFarmAnimalHousing;

/**
 * Activity related to edit FarmAnimalHousing objects.
 */
class FarmAnimalHousingEmissionEditorActivity extends SubSourceEmissionEditorPresenter<FarmAnimalHousingEmissionEditorComponent, FarmAnimalHousing>
    implements ModifyListActions {
  @Override
  protected FarmAnimalHousing createSubSource() {
    return StandardFarmAnimalHousing.create();
  }

  @Override
  protected JsArray<FarmAnimalHousing> getSubSources() {
    return view.source.getSubSources();
  }
}
