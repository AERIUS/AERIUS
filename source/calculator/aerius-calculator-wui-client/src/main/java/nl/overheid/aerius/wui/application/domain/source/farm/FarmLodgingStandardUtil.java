/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.farm;

import elemental2.core.JsArray;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.FarmAdditionalLodgingSystemCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategories;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingFodderMeasureCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmReductiveLodgingSystemCategory;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;

/**
 * Class for standard Farm Lodging emissions, based on a category.
 */
public class FarmLodgingStandardUtil {

  /**
   * Get emission of the base type (without additional or reductive lodging
   * systems or fodder measures, possibly constrained).
   */
  public static double getFlatEmission(final FarmLodgingCategories categories, final StandardFarmLodging sfl, final Substance substance) {
    final FarmLodgingCategory category = categories.determineFarmLodgingCategoryByCode(sfl.getFarmLodgingCode());
    final int dayFactor = category != null && category.getFarmEmissionFactorType() == FarmEmissionFactorType.PER_ANIMAL_PER_DAY
        ? sfl.getNumberOfDays()
        : 1;
    return sfl.getNumberOfAnimals() * dayFactor * getEmissionFactor(categories, sfl, substance);
  }

  /**
   * Determine if the base lodging emission is constrained by any of the additional systems.
   */
  public static boolean isConstrained(final FarmLodgingCategories categories, final StandardFarmLodging standardLodging) {
    final FarmLodgingCategory category = categories.determineFarmLodgingCategoryByCode(standardLodging.getFarmLodgingCode());
    return category != null ? isEmissionFactorConstrained(categories, standardLodging, category) : false;
  }

  /**
   * Get emission of the base type, unconstrained.
   */
  public static double getUnconstrainedFlatEmission(final FarmLodgingCategories categories, final StandardFarmLodging sfl,
      final Substance substance) {
    return sfl.getNumberOfAnimals() * getUnconstrainedEmissionFactor(categories, sfl, substance);
  }

  /**
   * Calculate emissions for lodging with additional systems.
   * Can be used to calculate emissions up to a specific additional system.
   * If the supplied upTo system is null, all additional systems will be included.
   */
  public static double getEmissionsAdditional(final FarmLodgingCategories categories, final StandardFarmLodging sfl, final Substance substance,
      final AdditionalLodgingSystem upTo) {
    // First get the (possibly constrained) emissions of the base lodging.
    double emission = getFlatEmission(categories, sfl, substance);

    // Add emissions of additional types
    final JsArray<AdditionalLodgingSystem> alss = sfl.getAdditionalLodgingSystems();

    for (int i = 0; i < alss.length; i++) {
      final AdditionalLodgingSystem als = alss.getAt(i);
      final FarmAdditionalLodgingSystemCategory category = categories.determineAdditionalLodgingSystemByCode(als.getLodgingSystemCode());
      if (category != null) {
        emission += als.getNumberOfAnimals() * category.getEmissionFactor();
      }
      if (als == upTo) {
        break;
      }
    }

    return emission;
  }

  /**
   * Calculate emissions for lodging with additional systems and reductive systems.
   * Can be used to calculate emissions up to a specific reductive system.
   * If the supplied upTo system is null, all reductive systems will be included.
   */
  public static double getEmissionsReductive(final FarmLodgingCategories categories, final StandardFarmLodging sfl, final Substance substance,
      final ReductiveLodgingSystem upTo) {
    // First get base + additional emissions.
    double emission = getEmissionsAdditional(categories, sfl, substance, null);

    // Reduce with reduction factors
    final JsArray<ReductiveLodgingSystem> rlss = sfl.getReductiveLodgingSystems();

    for (int i = 0; i < rlss.length; i++) {
      final ReductiveLodgingSystem rls = rlss.getAt(i);
      final FarmReductiveLodgingSystemCategory category = categories.determineReductiveLodgingSystemByCode(rls.getLodgingSystemCode());
      if (category != null) {
        emission *= 1 - category.getReductionFactor();
      }
      if (rls == upTo) {
        break;
      }
    }

    return emission;
  }

  /**
   * Calculate emissions for lodging with additional systems, reductive systems and fodder measures.
   * Can be used to calculate emissions up to a specific fodder measure.
   * If the supplied upTo measure is null, all fodder measures will be included.
   */
  public static double getEmissionsFodder(final FarmLodgingCategories categories, final StandardFarmLodging sfl, final Substance substance,
      final LodgingFodderMeasure upTo) {
    // First get base + additional emissions.
    double emission = getEmissionsReductive(categories, sfl, substance, null);

    // Reduce with fodder measure factors
    final JsArray<LodgingFodderMeasure> fm = sfl.getFodderMeasures();

    for (int i = 0; i < fm.length; i++) {
      final LodgingFodderMeasure lfm = fm.getAt(i);
      final FarmLodgingFodderMeasureCategory category = categories.determineLodgingFodderMeasureByCode(lfm.getFodderMeasureCode());
      if (category != null) {
        emission *= 1 - category.getReductionFactorTotal();
      }
      if (upTo == lfm) {
        break;
      }
    }

    return emission;
  }

  /**
   * Calculate total emissions.
   *
   * @return total emission
   */
  public static double getEmissions(final FarmLodgingCategories categories, final StandardFarmLodging sfl, final Substance substance) {
    return getEmissionsFodder(categories, sfl, substance, null);
  }

  /**
   * Get unconstrained emission factor of the base type.
   */
  public static double getUnconstrainedEmissionFactor(final FarmLodgingCategories categories, final StandardFarmLodging sfl,
      final Substance substance) {
    final FarmLodgingCategory category = categories.determineFarmLodgingCategoryByCode(sfl.getFarmLodgingCode());
    return category != null && Substance.NH3 == substance ? category.getEmissionFactor() : 0;
  }

  /**
   * Get emission factor of the base type (could be constrained).
   */
  public static double getEmissionFactor(final FarmLodgingCategories categories, final StandardFarmLodging sfl, final Substance substance) {
    final double emissionFactor;
    final FarmLodgingCategory category = categories.determineFarmLodgingCategoryByCode(sfl.getFarmLodgingCode());

    if (category != null && Substance.NH3 == substance) {
      emissionFactor = determineEmissionFactorsLodging(categories, sfl, category);
    } else {
      emissionFactor = 0;
    }
    return emissionFactor;
  }

  public static FarmEmissionFactorType getEmissionFactorType(final FarmLodgingCategories categories, final StandardFarmLodging sfl) {
    final FarmEmissionFactorType emissionFactorType;
    final FarmLodgingCategory category = categories.determineFarmLodgingCategoryByCode(sfl.getFarmLodgingCode());

    if (category != null) {
      emissionFactorType = category.getFarmEmissionFactorType();
    } else {
      emissionFactorType = FarmEmissionFactorType.PER_ANIMAL_PER_DAY;
    }
    return emissionFactorType;
  }

  private static double determineEmissionFactorsLodging(final FarmLodgingCategories categories, final StandardFarmLodging standardLodging,
      final FarmLodgingCategory category) {
    if (category.canLodgingEmissionFactorsBeConstrained() && isAnyLodgingSystemScrubber(categories, standardLodging)) {
      return category.getConstrainedEmissionFactor();
    } else {
      return category.getEmissionFactor();
    }
  }

  private static boolean isEmissionFactorConstrained(final FarmLodgingCategories categories, final StandardFarmLodging standardLodging,
      final FarmLodgingCategory category) {
    return category.canLodgingEmissionFactorsBeConstrained() && isAnyLodgingSystemScrubber(categories, standardLodging);
  }

  private static boolean isAnyLodgingSystemScrubber(final FarmLodgingCategories categories, final StandardFarmLodging standardLodging) {
    final JsArray<AdditionalLodgingSystem> alsArray = standardLodging.getAdditionalLodgingSystems();

    for (int i = 0; i < alsArray.length; i++) {
      final FarmAdditionalLodgingSystemCategory category =
          categories.determineAdditionalLodgingSystemByCode(alsArray.getAt(i).getLodgingSystemCode());
      if (category != null && category.isScrubber()) {
        return true;
      }
    }
    final JsArray<ReductiveLodgingSystem> rlsArray = standardLodging.getReductiveLodgingSystems();

    for (int i = 0; i < rlsArray.length; i++) {
      final FarmReductiveLodgingSystemCategory category = categories.determineReductiveLodgingSystemByCode(rlsArray.getAt(i).getLodgingSystemCode());
      if (category != null && category.isScrubber()) {
        return true;
      }
    }
    return false;
  }

  public static boolean hasFarmReductiveLodgingSystemCategories(FarmLodgingCategories farmLodgingCategories) {
    return  !farmLodgingCategories.getFarmReductiveLodgingSystemCategories().isEmpty();
  }
}
