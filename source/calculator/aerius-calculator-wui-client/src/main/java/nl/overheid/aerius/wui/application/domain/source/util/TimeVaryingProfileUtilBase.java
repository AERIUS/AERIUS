/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.util;

import java.util.List;
import java.util.Optional;

import com.google.gwt.i18n.client.NumberFormat;

import elemental2.core.JsNumber;

import jsinterop.base.Js;

import nl.aerius.wui.dev.GWTProd;
import nl.overheid.aerius.shared.domain.characteristics.StandardTimeVaryingProfile;
import nl.overheid.aerius.shared.domain.characteristics.TimeVaryingProfiles;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfileType;
import nl.overheid.aerius.wui.application.components.source.characteristics.timevarying.TimeVaryingProfileSelectorComponent.TimeVaryingProfileOption;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.source.ADMSCharacteristics;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.TimeVaryingProfile;
import nl.overheid.aerius.wui.application.i18n.M;

/**
 * Base class for different time-varying profile implementations.
 */
public abstract class TimeVaryingProfileUtilBase {
  public static final int PRECISION = 4;
  public static final double SMALLEST = 1D / Math.pow(10, PRECISION);
  private static final NumberFormat FORMAT = NumberFormat.getFormat("0");
  private static final NumberFormat FORMAT_PRECISE = NumberFormat.getFormat("0.0000");
  private final CustomTimeVaryingProfileType type;
  private final TimeVaryingProfile continuousProfile;
  private final TimeVaryingProfile sectorDefaultProfile;

  protected TimeVaryingProfileUtilBase(final CustomTimeVaryingProfileType type) {
    this.type = type;
    continuousProfile = createStandardProfile(type, TimeVaryingProfileOption.CONTINUOUS);
    sectorDefaultProfile = createStandardProfile(type, TimeVaryingProfileOption.SECTOR_DEFAULT);
  }

  public boolean isType(final CustomTimeVaryingProfileType type) {
    return this.type == type;
  }

  public CustomTimeVaryingProfileType getType() {
    return type;
  }

  public TimeVaryingProfile createStandardProfile(final CustomTimeVaryingProfileType type, final TimeVaryingProfileOption name) {
    final TimeVaryingProfile profile = TimeVaryingProfileUtil.create(type);
    profile.setSystemProfile(true);
    profile.setLabel(M.messages().timeVaryingProfileStandardLabel(name));
    return profile;
  }

  public TimeVaryingProfile getContinuousProfile() {
    return continuousProfile;
  }

  public TimeVaryingProfile getSectorDefaultProfile() {
    return sectorDefaultProfile;
  }

  /**
   * Return the time-varying profile for the given source, or null for the continuous profile.
   *
   * Note, null will be returned to indicate a continuous profile. See {@link #createContinuousProfile} to create this.
   *
   * The time-varying profile can be, in order of preference:
   *
   * <ul>
   *  <li>Custom time-varying profile
   *  <li>Explicitly selected continuous profile (null)
   *  <li>Explicitly selected standard profile (although note: there is no UI path to support this currently)
   *  <li>Sector default standard profile
   *  <li>Continuous profile (null)
   * </ul>
   */
  public TimeVaryingProfile findTimeVaryingProfile(final EmissionSourceFeature source, final SituationContext situation,
      final TimeVaryingProfiles variations) {
    // Currently, only ADMS is supported
    if (source.getCharacteristicsType() != CharacteristicsType.ADMS) {
      return null;
    }

    final ADMSCharacteristics chars = (ADMSCharacteristics) source.getCharacteristics();
    final TimeVaryingProfile customProfile = Optional.ofNullable(getCustomTimeVaryingProfileId(chars)).map(v -> situation.findTimeVaryingProfile(v))
        .orElse(null);

    if (customProfile == null) {
      final String code = getStandardTimeVaryingProfileCode(chars);
      return TimeVaryingProfiles.CONTINUOUS.equals(code) ? null : sectorDefaultProfile(source, situation, variations, code);
    } else {
      return customProfile;
    }
  }

  /**
   * Determine the sector default profile. Look for the code in the available sector default profiles.
   * If code is null (implying sector default) or could not be found the standard sector default profile is returned.
   *
   * @param source source to get sector id from
   * @param situation situation to get year from
   * @param variations available time varying profiles
   * @param code code to find sector default for (can be null)
   * @return
   */
  private TimeVaryingProfile sectorDefaultProfile(final EmissionSourceFeature source, final SituationContext situation,
      final TimeVaryingProfiles variations, final String code) {
    StandardTimeVaryingProfile standardProfile = Optional.ofNullable(code)
        .map(v -> variations.determineStandardTimeVaryingProfile(getType(), v)).orElse(null);

    if (standardProfile == null) {
      standardProfile = Optional.ofNullable(variations.defaultProfileForSectorAndYear(getType(), source.getSectorId(), situation.getYear()))
          .map(v -> v).orElse(null);
    }
    if (standardProfile == null) {
      return getSectorDefaultProfile();
    } else {
      return TimeVaryingProfileUtil.fromStandardTimeVaryingProfile(standardProfile, standardProfile.getCode());
    }

  }

  public abstract String getCustomTimeVaryingProfileId(ADMSCharacteristics chars);

  public abstract void setCustomTimeVaryingProfileId(ADMSCharacteristics chars, String id);

  public abstract String getStandardTimeVaryingProfileCode(ADMSCharacteristics chars);

  public abstract void setStandardTimeVaryingProfileCode(ADMSCharacteristics chars, String code);

  public double getValuesTotal(final List<Double> values) {
    return getValuesTotalRaw(values);
  }

  public double getValuesAverage(final List<Double> values) {
    return getValuesAverageRaw(values);
  }

  public String getValuesTotalText(final List<Double> values) {
    return FORMAT.format(getValuesTotal(values));
  }

  public String getValuesAverageText(final List<Double> values) {
    return FORMAT.format(getValuesAverage(values));
  }

  public String getValuesTotalPreciseText(final List<Double> values) {
    return FORMAT_PRECISE.format(getValuesTotal(values));
  }

  public String getValuesAveragePreciseText(final List<Double> values) {
    return FORMAT_PRECISE.format(getValuesAverage(values));
  }

  protected String formattedPreciseValue(final double value) {
    return FORMAT_PRECISE.format(value);
  }

  public String getValuesMaxText() {
    return FORMAT.format(getValuesMax());
  }

  public int getValuesMax() {
    return type.getExpectedTotalNumberOfValues();
  }

  protected void setInputAtIndex(final List<Double> values, final int idx, final double value) {
    // Cut off to the spec's precision and minimum value
    final double finalValue = Math.max(0, Double.parseDouble(Js.<JsNumber> cast(value).toFixed(PRECISION)));

    if (idx >= values.size() || idx < 0) {
      throw new IllegalArgumentException("Illegal index supplied");
    }
    values.set(idx, finalValue);
  }

  /**
   * Returns the unrounded total of all values.
   *
   * @param values values to sum.
   * @return
   */
  protected abstract double getValuesTotalRaw(final List<Double> values);

  /**
   * Returns the unrounded average of all values.
   *
   * @param values values to average
   * @return
   */
  protected abstract double getValuesAverageRaw(final List<Double> values);

  /**
   * Checks if the number of values is the same as the expected number of values for the profile.
   * Returns true if it matches the size. Also log a warn console message about the size not matching.
   *
   * @param values values to check
   * @return true if size matches expected profile size
   */
  protected boolean checkAndwarnInvalidSize(final List<Double> values) {
    if (values.size() != type.getExpectedNumberOfValues()) {
      GWTProd.warn("Time-varying profile values not of expected size (" + type.getExpectedNumberOfValues() + "): ", values.size());
      return false;
    }
    return true;
  }
}
