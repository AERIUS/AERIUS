/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.i18n;

import ol.geom.Point;

import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;

/**
 * Util class for formatting numbers.
 */
public class MessageFormatter {
  private static final double MINIMUM_THRESHOLD_GRAM = 0.001;
  private static final double MAXIMUM_THRESHOLD_GRAM = 0.1;
  private static final int MINIMUM_THRESHOLD_TON = 10_000;

  private static final int GRAM_IN_KILO = 1_000;
  private static final int KILO_IN_TON = 1_000;
  private static final int M_IN_HA = 10_000;

  private static final int EMISSION_VALUE_ONE_PRECISE = 1;
  private static final int SURFACE_PRECISION = 2;
  private static final int DISTANCE_M_PRECISION = 2;
  private static final int DISTANCE_KM_PRECISION = 1;
  private static final int CL_PRECISION = 0;

  private MessageFormatter() {
  }

  public static String formatEmissionWithUnitSmart(final double em) {
    final String withUnit;
    if (showAsGram(em)) {
      withUnit = M.messages().unitGramPerY(formatEmissionToGram(em));
    } else if (showAsTon(em)) {
      withUnit = M.messages().unitTonnesPerY(formatEmissionToTon(em));
    } else {
      withUnit = M.messages().unitKgPerY(formatEmissionToKg(em));
    }
    return withUnit;
  }

  public static String getEmissionUnitSmart(final double em) {
    if (showAsGram(em)) {
      return M.messages().unitSingularGramPerY();
    } else if (showAsTon(em)) {
      return M.messages().unitSingularTonPerY();
    } else {
      return M.messages().unitSingularKgPerY();
    }
  }

  private static boolean showAsGram(final double em) {
    // Show as gram if between minimum threshold (1 gram) and maximum threshold (100 gram)
    return em >= MINIMUM_THRESHOLD_GRAM && em < MAXIMUM_THRESHOLD_GRAM;
  }

  private static boolean showAsTon(final double em) {
    // Show as ton if rounded value is equal to or above the threshold
    final double scale = Math.pow(10, EMISSION_VALUE_ONE_PRECISE);
    return Math.abs(Math.round(em * scale)) > MINIMUM_THRESHOLD_TON * scale;
  }

  public static String toFixed(final double value, final int length) {
    return M.messages().decimalNumberFixed(value, length);
  }

  public static String toCapped(final double value, final int length) {
    return M.messages().decimalNumberCapped(value, length);
  }

  /**
   * Formats with precision as significant number of digits.
   * As an example: value of 1.23456 with precision 2 displays 1.2 instead of 0.12.
   * And if value is 0.00123455 with precision 2, it'll display 0.0012 instead of 0.0.
   * Warning: Does not (yet) use default locale, as it's only used in DMT which is UK only.
   */
  public static String toPrecision(final Number value, final int precision) {
    return value == null ? "-" : MessageFormatter.toPrecisionNative(value, precision);
  }

  private static native String toPrecisionNative(final Number value, final int precision) /*-{
    return value.toPrecision(precision);
  }-*/;

  public static String formatEmission(final double em, final int precision) {
    return toFixed(em, precision);
  }

  public static String formatEmissionOnePrecise(final double em) {
    return formatEmission(em, EMISSION_VALUE_ONE_PRECISE);
  }

  public static String formatPoint(final Point point) {
    return M.messages().point(point.getCoordinates().getX(), point.getCoordinates().getY());
  }

  public static String formatDistanceKm(final double distance) {
    return M.messages().decimalNumberFixed(distance / 1000, DISTANCE_KM_PRECISION);
  }

  public static String formatDistanceKmWithUnit(final double distance) {
    return M.messages().unitKm(formatDistanceKm(distance));
  }

  public static String formatDistance(final double distance) {
    return M.messages().decimalNumberFixed(distance, DISTANCE_M_PRECISION);
  }

  public static String formatDistanceWithUnit(final double distance) {
    return M.messages().unitM(formatDistance(distance));
  }

  public static String formatEmissionHeight(final double emissionHeight) {
    return M.messages().unitM(toFixed(emissionHeight, 1));
  }

  public static String formatHeatContent(final double heatContent) {
    return M.messages().unitMW(toFixed(heatContent, 3));
  }

  private static String formatEmissionToTon(final double em) {
    return formatEmissionOnePrecise(em / KILO_IN_TON);
  }

  private static String formatEmissionToKg(final double em) {
    return formatEmissionOnePrecise(em);
  }

  private static String formatEmissionToGram(final double em) {
    return formatEmissionOnePrecise(em * GRAM_IN_KILO);
  }

  /* Result formatting. */

  public static String formatResult(final Double resultValue, final EmissionResultKey emissionResultKey,
      final EmissionResultValueDisplaySettings settings) {
    return emissionResultKey.getEmissionResultType() == EmissionResultType.CONCENTRATION
        ? formatConcentration(resultValue, settings)
        : formatDeposition(resultValue, settings);
  }

  public static String formatResultWithUnit(final Double resultValue, final EmissionResultKey emissionResultKey,
      final EmissionResultValueDisplaySettings settings) {
    return emissionResultKey.getEmissionResultType() == EmissionResultType.CONCENTRATION
        ? formatConcentrationWithUnit(resultValue, settings)
        : formatDepositionWithUnit(resultValue, settings);
  }

  /* Critical level/load formatting, using 0 precision */

  public static String formatCL(final Double resultValue, final EmissionResultKey emissionResultKey,
      final EmissionResultValueDisplaySettings settings) {
    return emissionResultKey.getEmissionResultType() == EmissionResultType.CONCENTRATION
        ? formatConcentration(resultValue, CL_PRECISION)
        : formatDeposition(resultValue, settings, CL_PRECISION);
  }

  public static String formatCLWithUnit(final Double resultValue, final EmissionResultKey emissionResultKey,
      final EmissionResultValueDisplaySettings settings) {
    return emissionResultKey.getEmissionResultType() == EmissionResultType.CONCENTRATION
        ? formatConcentrationWithUnit(resultValue, CL_PRECISION)
        : formatDepositionWithUnit(resultValue, settings, CL_PRECISION);
  }

  /* Deposition formatting. */

  /**
   * Returns a formatted deposition value with the given unit and precision.
   *
   * @param deposition
   * @param settings
   * @param precision
   * @return
   */
  public static String formatDeposition(final Double deposition, final EmissionResultValueDisplaySettings settings, final int precision) {
    return deposition == null ? "-" : toFixed(convertDepositionValue(deposition, settings), precision);
  }

  /**
   * TODO Note: This method is commonly used to format values other than deposition. See also the TODOs in the various callers of this method.
   * When a utility has been written to handle the use case of formatting any ResultStatisticType, all such callers must be refactored to
   * use that utility instead. This note should remain here until that utility has been completed.
   */
  public static String formatDeposition(final Number deposition, final EmissionResultValueDisplaySettings settings) {
    return deposition == null ? "-" : MessageFormatter.formatDeposition(deposition.doubleValue(), settings);
  }

  public static String formatDeposition(final double deposition, final EmissionResultValueDisplaySettings settings) {
    return toFixed(convertDepositionValue(deposition, settings), settings.getDepositionValueRoundingLength());
  }

  public static String formatDepositionWithUnit(final Double deposition, final EmissionResultValueDisplaySettings settings) {
    return formatDepositionWithUnit(deposition, settings, settings.getDepositionValueRoundingLength());
  }

  public static String formatDepositionWithUnit(final Double deposition, final EmissionResultValueDisplaySettings settings, final int precision) {
    return deposition == null ? "-" : formatDepositionValueDisplayUnit(formatDeposition(deposition, settings, precision), settings);
  }

  public static String formatDepositionSum(final Number deposition, final EmissionResultValueDisplaySettings settings) {
    return deposition == null ? "-" : MessageFormatter.formatDepositionSum(deposition.doubleValue(), settings);
  }

  public static String formatDepositionSum(final double deposition, final EmissionResultValueDisplaySettings settings) {
    return toFixed(convertDepositionValue(deposition, settings), settings.getDepositionValueSumRoundingLength());
  }

  public static String formatDepositionSumWithUnit(final Double deposition, final EmissionResultValueDisplaySettings settings) {
    return formatDepositionWithUnit(deposition, settings, settings.getDepositionValueSumRoundingLength());
  }

  private static double convertDepositionValue(final double depositionValue, final EmissionResultValueDisplaySettings settings) {
    return depositionValue * settings.getConversionFactor();
  }

  private static String formatDepositionValueDisplayUnit(final String depositionValue, final EmissionResultValueDisplaySettings settings) {
    return M.messages().unitDeposition(settings.getDisplayType(), depositionValue);
  }

  /* Concentration formatting */

  public static String formatConcentration(final Double concentration, final EmissionResultValueDisplaySettings settings) {
    // For now, use same rounding as deposition.
    return concentration == null ? "-" : toFixed(concentration, settings.getDepositionValueRoundingLength());
  }

  public static String formatConcentration(final Double concentration, final int precision) {
    return concentration == null ? "-" : toFixed(concentration, precision);
  }

  public static String formatConcentrationWithUnit(final Double concentration, final EmissionResultValueDisplaySettings settings) {
    // For now, use same rounding as deposition.
    return formatConcentrationWithUnit(concentration, settings.getDepositionValueRoundingLength());
  }

  public static String formatConcentrationWithUnit(final Double concentration, final int precision) {
    return concentration == null ? "-" : formatConcentrationValueDisplayUnit(formatConcentration(concentration, precision));
  }

  private static String formatConcentrationValueDisplayUnit(final String concentrationValue) {
    return M.messages().unitConcentration(concentrationValue);
  }

  /* Surface formatting. */

  public static String formatSurfaceWithUnit(final double surfaceInHectare) {
    return M.messages().unitHa(toFixed(surfaceInHectare, SURFACE_PRECISION));
  }

  public static String formatSurfaceToHectare(final Double surfaceInMeters) {
    return surfaceInMeters == null ? "-" : formatSurfaceToHectareInternal(surfaceInMeters.doubleValue());
  }

  public static String formatSurfaceToHectareInternal(final double surfaceInMeters) {
    return toFixed(surfaceInMeters / M_IN_HA, SURFACE_PRECISION);
  }

  public static String formatSurfaceToHectareWithUnit(final Double surfaceInMeters) {
    return M.messages().unitHa(formatSurfaceToHectare(surfaceInMeters));
  }

  public static String formatPercentageWithUnit(final double value, final int length) {
    return M.messages().unitPercentage(toFixed(value, length));
  }
}
