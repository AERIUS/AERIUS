/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.legend;

import static com.axellience.vuegwt.core.client.tools.JsUtils.e;
import static com.axellience.vuegwt.core.client.tools.JsUtils.map;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;
import jsinterop.base.JsPropertyMap;

import nl.aerius.geo.domain.legend.ColorLabelsLegend;
import nl.aerius.geo.domain.legend.LegendType;
import nl.aerius.wui.vue.directives.VectorDirective;
import nl.overheid.aerius.wui.application.event.LegendFilterEvent;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(directives = {
    VectorDirective.class
})
public class ColorLabelsLegendComponent extends BasicVueComponent {

  @Inject EventBus eventBus;

  @Prop @JsProperty protected ColorLabelsLegend legend;

  private final Set<Integer> filteredOut = new HashSet<>();

  @Computed
  public String getIconClass() {
    return legend.getIcon().name().toLowerCase(Locale.ROOT);
  }

  @Computed
  public boolean getUseFontIcon() {
    return LegendType.HEXAGON == legend.getIcon() || LegendType.LINEDASH == legend.getIcon()
        || LegendType.CIRCLE == legend.getIcon() || LegendType.CIRCLE_OUTLINE == legend.getIcon();
  }

  @Computed("isFilterable")
  public boolean isFilterable() {
    return legend.getFilterables() != null;
  }

  @JsMethod
  public boolean getFilteredOut(final int index) {
    return filteredOut.contains(index);
  }

  @JsMethod
  public void toggleFilterSelection(final int index) {
    if (filteredOut.contains(index)) {
      filteredOut.remove(index);
      fireFilteredOutUpdate(legend.getFilterables()[index], true);
    } else {
      filteredOut.add(index);
      fireFilteredOutUpdate(legend.getFilterables()[index], false);
    }
  }

  @JsMethod
  public String getFontIconLegend() {
    switch (legend.getIcon()) {
    case HEXAGON:
      return "icon-hexagon-legend";
    case LINEDASH:
      return "icon-noise-barrier-legend";
    case CIRCLE_OUTLINE:
    case CIRCLE:
      return "icon-nature-areas-legend";
    case ROAD:
    case LINE:
    case SQUARE:
    case TEXT:
    default:
      return "";
    }
  }

  @JsMethod
  public JsPropertyMap<String> getIconStyle(final int index) {
    final JsPropertyMap<String> style = map(e("backgroundColor", legend.getColors()[index]));
    if (legend.getIconSizes()[index] != null) {
      style.set("height", legend.getIconSizes()[index] + "px");
    }
    return style;
  }

  @JsMethod
  public String getColor(final int index) {
    return legend.getIcon() == LegendType.CIRCLE_OUTLINE ? legend.getOutlineColor() : legend.getColors()[index];
  }

  @JsMethod
  public String getOutlineColor(final int index) {
    return legend.getIcon() == LegendType.CIRCLE_OUTLINE ? legend.getColors()[index] : legend.getOutlineColor();
  }

  @JsMethod
  public List<String> getTranslatedLabels() {
    final List<String> labels = new ArrayList<>();
    for (final String dbLabel : legend.getLabels()) {
      final String i18nLabel = i18n.layerItemLabel(cleanString(dbLabel));
      if ("?".equals(i18nLabel)) {
        labels.add(dbLabel);
      } else {
        labels.add(i18nLabel);
      }
    }

    return labels;
  }

  private void fireFilteredOutUpdate(final String filterable, final boolean visible) {
    eventBus.fireEvent(new LegendFilterEvent(filterable, visible));
  }

  private String cleanString(final String input) {
    return input.replace(", ", "_").replace(",", "_").replace(" ", "_");
  }
}
