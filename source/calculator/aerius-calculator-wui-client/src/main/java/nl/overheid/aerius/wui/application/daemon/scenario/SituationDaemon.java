/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.scenario;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.place.PlaceController;
import nl.aerius.wui.util.NotificationUtil;
import nl.overheid.aerius.shared.util.ConsecutiveIdUtil;
import nl.overheid.aerius.wui.application.command.situation.CreateEmptySituationCommand;
import nl.overheid.aerius.wui.application.command.situation.DuplicateSituationCommand;
import nl.overheid.aerius.wui.application.command.situation.GoToSituationCommand;
import nl.overheid.aerius.wui.application.command.situation.SwitchSituationCommand;
import nl.overheid.aerius.wui.application.command.source.SituationCreateNewCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.geo.FeatureUtil;
import nl.overheid.aerius.wui.application.event.situation.SituationDeletedEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.place.HomePlace;
import nl.overheid.aerius.wui.application.place.ScenarioInputListPlace;

@Singleton
public class SituationDaemon extends BasicEventComponent {
  private static final SituationDaemonEventBinder EVENT_BINDER = GWT.create(SituationDaemonEventBinder.class);

  interface SituationDaemonEventBinder extends EventBinder<SituationDaemon> {}

  @Inject private ApplicationContext applicationContext;
  @Inject private ScenarioContext scenarioContext;

  @Inject private PlaceController placeController;

  @EventHandler
  public void onSituationCreateNewCommand(final SituationCreateNewCommand c) {
    final SituationContext situation = createAndAddSituation(v -> {});

    if (situation != null) {
      eventBus.fireEvent(new SwitchSituationCommand(situation));
      placeController.goTo(new ScenarioInputListPlace(applicationContext.getTheme()));
    }
  }

  @EventHandler
  public void onGoToSituationCommand(final GoToSituationCommand c) {
    placeController.goTo(new ScenarioInputListPlace(applicationContext.getTheme()));
    eventBus.fireEvent(new SwitchSituationCommand(c.getValue()));
  }

  @EventHandler
  public void onDuplicateSituationCommand(final DuplicateSituationCommand c) {
    final SituationContext situation = c.getValue();
    final Set<String> existingLabels = getExistingLabels(scenarioContext.getSituations());

    // Duplicate situation
    final SituationContext newSit = createAndAddSituation(copy -> {
      copy.copyFrom(situation);

      copy.setName(FeatureUtil.smartIncrementLabel(situation.getName(), existingLabels));
    });
    eventBus.fireEvent(new SwitchSituationCommand(newSit));
  }

  private Set<String> getExistingLabels(final List<SituationContext> list) {
    return list.stream()
        .map(source -> source.getName())
        .collect(Collectors.toSet());
  }

  @EventHandler
  public void onCreateEmptySituationCommand(final CreateEmptySituationCommand c) {
    Optional.ofNullable(createAndAddSituation(sc -> {}))
        .ifPresent(sit -> {
          if (c.isAlsoSwitch()) {
            eventBus.fireEvent(new SwitchSituationCommand(sit));
          }
        });
  }

  /**
   * Create a new situation and add to the context
   *
   * @param situationBuilder function to change situation properties before the
   *                         situation is added
   */
  public SituationContext createAndAddSituation(final Consumer<SituationContext> situationBuilder) {
    final ConsecutiveIdUtil.IndexIdPair newId = scenarioContext.createConsecutiveSituationIdUtil().generate();

    final String situationNumber = newId.getId();
    final SituationContext situationContext = new SituationContext(situationNumber);

    // Set default values
    situationContext.setName(M.messages().navigationSituationShortLabel(situationNumber));
    situationContext.setYear(applicationContext.getConfiguration().getCalculationYearDefault());
    SituationContext.safeSetSituationType(situationContext, applicationContext.getConfiguration().getDefaultSituationType());
    situationBuilder.accept(situationContext);

    // Guard the year against empty or 0-value by resetting to the default year
    // (ideally the situationBuilder doesn't set these values in the first place,
    // but we shouldn't assume that they don't at this stage)
    if (situationContext.getYear() == 0) {
      // Warn to console because the situation builder ideally shouldn't do this
      GWTProd.warn("SituationBuilder set the calculation year to an invalid value (" + situationContext.getYear() + ") and has been corrected");
      situationContext.setYear(applicationContext.getConfiguration().getCalculationYearDefault());
    }

    snapYear(situationContext, applicationContext.getConfiguration().getCalculationYears(),
        applicationContext.getConfiguration().getCalculationYearDefault(), eventBus);

    scenarioContext.addSituation(newId.getIndex(), situationContext);

    return situationContext;
  }

  @EventHandler
  public void onSituationDeleteEvent(final SituationDeletedEvent e) {
    if (scenarioContext.hasSituations()) {
      // Switch to another situation if there are any
      final SituationContext situationContext = scenarioContext.getSituations().get(0);
      eventBus.fireEvent(new SwitchSituationCommand(situationContext));
    } else {
      // If no situations are left, go back to start
      placeController.goTo(new HomePlace(applicationContext.getTheme()));
    }
  }

  /**
   * Given a situation, snap its calculation year to the closest of the given
   * years. When it is not provided (0) set it to calcualtionYearDefault.
   *
   * @param situationContext the situation to snap the year of
   * @param calculationYears the available years
   * @param string the default year
   */
  private static void snapYear(final SituationContext situation, final List<Integer> calculationYears, final int calculationYearDefault,
      final EventBus eventBus) {
    final int intYear = situation.getYear();

    calculationYears.stream()
        .min((o1, o2) -> Math.abs(o1 - intYear) - Math.abs(o2 - intYear))
        .filter(v -> v != intYear)
        .ifPresent(newYear -> {
          newYear = intYear == 0 ? calculationYearDefault : newYear;
          situation.setYear(newYear);
          NotificationUtil.broadcastMessage(eventBus, M.messages().importYearAutoSnapText(intYear, newYear));
        });
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
