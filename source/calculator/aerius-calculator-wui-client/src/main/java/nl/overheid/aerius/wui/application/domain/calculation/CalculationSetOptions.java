/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.calculation;

import static jsinterop.annotations.JsPackage.GLOBAL;

import java.util.Objects;

import elemental2.core.Global;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.wui.application.util.json.JsonBuilder;
import nl.overheid.aerius.wui.vue.ReactivityUtil;

/**
 * Calculation options to pass to the server
 *
 * @see nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class CalculationSetOptions {
  private @JsProperty String calculationJobType;
  private @JsProperty String calculationMethod;
  private @JsProperty double calculateMaximumRange;
  private @JsProperty Boolean useInCombinationArchive;
  private @JsProperty NCACalculationOptions ncaCalculationOptions;

  public static final @JsOverlay CalculationSetOptions create() {
    final CalculationSetOptions opts = new CalculationSetOptions();
    init(opts);
    return opts;
  }

  public final static @JsOverlay void init(final CalculationSetOptions opts) {
    ReactivityUtil.ensureDefault(opts::getCalculationJobType, opts::setCalculationJobType, CalculationJobType.PROCESS_CONTRIBUTION);
    ReactivityUtil.ensureDefault(opts::getCalculationMethod, opts::setCalculationMethod, CalculationMethod.FORMAL_ASSESSMENT);
    ReactivityUtil.ensureJsProperty(opts, "calculateMaximumRange", opts::setCalculateMaximumRange, 0D);
    ReactivityUtil.ensureDefault(opts::isUseInCombinationArchive, opts::setUseInCombinationArchive, false);
    ReactivityUtil.ensureDefaultSupplied(opts::getNcaCalculationOptions, opts::setNcaCalculationOptions, NCACalculationOptions::create);
    ReactivityUtil.ensureInitialized(opts::getNcaCalculationOptions, NCACalculationOptions::init);
  }

  public final @JsOverlay CalculationJobType getCalculationJobType() {
    return CalculationJobType.safeValueOf(this.calculationJobType);
  }

  public final @JsOverlay void setCalculationJobType(final CalculationJobType calculationJobType) {
    this.calculationJobType = calculationJobType.name();
  }

  public final @JsOverlay CalculationMethod getCalculationMethod() {
    return CalculationMethod.safeValueOf(calculationMethod);
  }

  public final @JsOverlay void setCalculationMethod(final CalculationMethod calculationMethod) {
    this.calculationMethod = calculationMethod.name();
  }

  public final @JsOverlay double getCalculateMaximumRange() {
    return calculateMaximumRange;
  }

  public final @JsOverlay void setCalculateMaximumRange(final double calculateMaximumRange) {
    this.calculateMaximumRange = calculateMaximumRange;
  }

  public final @JsOverlay Boolean isUseInCombinationArchive() {
    return useInCombinationArchive;
  }

  public final @JsOverlay void setUseInCombinationArchive(final boolean useInCombinationArchive) {
    this.useInCombinationArchive = useInCombinationArchive;
  }

  public final @JsOverlay NCACalculationOptions getNcaCalculationOptions() {
    return ncaCalculationOptions;
  }

  public final @JsOverlay void setNcaCalculationOptions(final NCACalculationOptions ncaCalculationOptions) {
    this.ncaCalculationOptions = ncaCalculationOptions;
  }

  @Override
  public final @JsOverlay int hashCode() {
    return Objects.hash(toJSONString());
  }

  @Override
  public final @JsOverlay boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    final CalculationSetOptions other = (CalculationSetOptions) obj;
    return toJSONString().equals(other.toJSONString());
  }

  public final @JsOverlay CalculationSetOptions copy() {
    return Js.uncheckedCast(Global.JSON.parse(toJSONString()));
  }

  public final @JsOverlay String toJSONString() {
    return Global.JSON.stringify(toJSON());
  }

  public final @JsOverlay Object toJSON() {
    final JsonBuilder builder = new JsonBuilder(this)
        .include("calculationMethod", "calculateMaximumRange", "calculationJobType", "useInCombinationArchive");

    if (ncaCalculationOptions != null) {
      builder.set("ncaCalculationOptions", ncaCalculationOptions.toJSON());
    }

    return builder.build();
  }
}
