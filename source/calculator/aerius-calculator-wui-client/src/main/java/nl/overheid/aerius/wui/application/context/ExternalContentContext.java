/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import java.util.HashMap;
import java.util.Map;

import com.google.inject.Singleton;

import jsinterop.annotations.JsProperty;

@Singleton
public class ExternalContentContext {
  public static final String ABOUT_THE_APPLICATION = "/content/about-the-application.html";
  public static final String CONTACT = "/content/contact.html";
  public static final String INTRODUCTION = "/content/introduction.html";
  public static final String NEXT_STEPS = "/content/next-steps.html";
  public static final String PRIVACY_STATEMENT = "/content/4-privacy-statement.html";
  public static final String COOKIE_POLICY = "/content/5-cookie-policy.html";

  private final @JsProperty Map<String, Throwable> errors = new HashMap<>();

  public void setContent(final String href, final String content) {
    this.content.put(href, content);
  }

  private String activeExternalContent = ExternalContentContext.ABOUT_THE_APPLICATION;

  private final @JsProperty Map<String, String> content = new HashMap<>();

  public String getContent(final String href) {
    return content.get(href);
  }

  public void setError(final String href, final Throwable error) {
    this.errors.put(href, error);
  }

  public Throwable getError(final String href) {
    return errors.get(href);
  }

  public String getActiveExternalContent() {
    return activeExternalContent;
  }

  public void setActiveExternalContent(final String activeExternalContent) {
    this.activeExternalContent = activeExternalContent;
  }
}
