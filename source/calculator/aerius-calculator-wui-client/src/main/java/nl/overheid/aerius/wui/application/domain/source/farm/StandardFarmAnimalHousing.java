/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.farm;

import static jsinterop.annotations.JsPackage.GLOBAL;

import elemental2.core.JsArray;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.wui.vue.ReactivityUtil;

/**
 * Client side implementation of StandardFarmAnimalHousing props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class StandardFarmAnimalHousing extends FarmAnimalHousing {
  private String animalHousingCode;
  private JsArray<AdditionalHousingSystem> additionalSystems;

  public static final @JsOverlay StandardFarmAnimalHousing create() {
    final StandardFarmAnimalHousing props = Js.uncheckedCast(JsPropertyMap.of());
    init(props);
    return props;
  }

  public static final @JsOverlay void init(final StandardFarmAnimalHousing props) {
    FarmAnimalHousing.init(props, FarmAnimalHousingType.STANDARD);
    ReactivityUtil.ensureJsProperty(props, "animalHousingCode", props::setAnimalHousingCode, "");
    ReactivityUtil.ensureJsPropertySupplied(props, "additionalSystems", props::setAdditionalSystems, () -> new JsArray<>());
  }

  public final @JsOverlay String getAnimalHousingCode() {
    return animalHousingCode;
  }

  public final @JsOverlay void setAnimalHousingCode(final String animalHousingCode) {
    this.animalHousingCode = animalHousingCode;
  }

  public final @JsOverlay JsArray<AdditionalHousingSystem> getAdditionalSystems() {
    return additionalSystems;
  }

  public final @JsOverlay void setAdditionalSystems(final JsArray<AdditionalHousingSystem> additionalSystems) {
    this.additionalSystems = additionalSystems;
  }
}
