/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.util;

import ol.geom.Geometry;
import ol.geom.GeometryType;

/**
 * Enum class to check if geometry is a specific geometry.
 */
public enum GeoType {
  CIRCLE(GeometryType.Circle),
  LINE_STRING(GeometryType.LineString),
  POINT(GeometryType.Point),
  POLYGON(GeometryType.Polygon);

  private final String type;

  GeoType(final GeometryType geometryType) {
    this.type = geometryType.name().toString();
  }

  /**
   * @return Returns true if the given geometry is this geometry type.
   */
  public boolean is(final Geometry geometry) {
    return geometry != null && type.equals(geometry.getType());
  }
}
