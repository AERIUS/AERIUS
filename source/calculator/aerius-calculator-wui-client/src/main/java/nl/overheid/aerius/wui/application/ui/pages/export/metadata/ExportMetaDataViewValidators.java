/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.ui.pages.export.metadata;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.aerius.vuelidate.ValidationOptions;
import nl.aerius.vuelidate.Validations;
import nl.aerius.vuelidate.ValidatorBuilder;

public class ExportMetaDataViewValidators extends ValidationOptions<ExportMetaDataComponent> {

  @Override
  protected void constructValidators(final Object instance, final ValidatorInstaller installer) {
    final ExportMetaDataComponent exportMetaDataView = Js.uncheckedCast(instance);
    installer.install("corporationV", () -> exportMetaDataView.corporationV = null, ValidatorBuilder.create().required());
    installer.install("projectNameV", () -> exportMetaDataView.projectNameV = null, ValidatorBuilder.create().required());
    installer.install("descriptionV", () -> exportMetaDataView.descriptionV = null, ValidatorBuilder.create().required());
    installer.install("streetAddressV", () -> exportMetaDataView.streetAddressV = null, ValidatorBuilder.create().required());
    installer.install("postcodeV", () -> exportMetaDataView.postcodeV = null, ValidatorBuilder.create().required());
    installer.install("cityV", () -> exportMetaDataView.cityV = null, ValidatorBuilder.create().required());
  }

  @JsType(isNative = true, namespace = JsPackage.GLOBAL)
  static class ExportMetaDataValidations extends Validations {
    public @JsProperty Validations corporationV;
    public @JsProperty Validations projectNameV;
    public @JsProperty Validations descriptionV;
    public @JsProperty Validations streetAddressV;
    public @JsProperty Validations postcodeV;
    public @JsProperty Validations cityV;
  }
}
