/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.calculate.detail;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.sector.category.AbstractCategory;
import nl.overheid.aerius.shared.domain.sector.category.SimpleCategory;
import nl.overheid.aerius.wui.application.components.source.DetailStatComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    DetailStatComponent.class
})
public class CalculationJobDetailHeaderView extends BasicVueComponent {
  @Prop CalculationSetOptions calculationSetOptions;
  @Prop Theme theme;

  @Inject @Data ApplicationContext applicationContext;

  @JsMethod
  protected boolean hasPermitArea() {
    return applicationContext.isAppFlagEnabled(AppFlag.HAS_CALCULATION_PERMIT_AREA);
  }

  @JsMethod
  protected boolean hasProjectCategory() {
    return applicationContext.isAppFlagEnabled(AppFlag.HAS_CALCULATION_PROJECT_CATEGORY);
  }

  @Computed("hasZoneOfInfluenceOption")
  protected boolean hasZoneOfInfluenceOption() {
    return calculationSetOptions != null
        && (calculationSetOptions.getCalculationMethod() == CalculationMethod.NATURE_AREA
            || calculationSetOptions.getCalculationMethod() == CalculationMethod.QUICK_RUN);
  }

  @Computed
  protected String getPermitArea() {
    return getName(AbstractCategory.determineByCode(applicationContext.getConfiguration().getCalculationPermitAreas(),
        calculationSetOptions.getNcaCalculationOptions().getPermitArea()));
  }

  @Computed
  protected String getProjectCategory() {
    return getName(AbstractCategory.determineByCode(applicationContext.getConfiguration().getCalculationProjectCategories(),
        calculationSetOptions.getNcaCalculationOptions().getProjectCategory()));
  }

  private static String getName(final SimpleCategory category) {
    return category == null ? "-" : category.getName();
  }

}
