/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.ui.pages.emissionsource;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasDestroyed;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import jsinterop.annotations.JsMethod;
import jsinterop.base.Js;

import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.vue.transition.HorizontalCollapse;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.wui.application.command.DetailEditorErrorChangeCommand;
import nl.overheid.aerius.wui.application.command.DetailEditorWarningChangeCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSubSourceDeselectFeatureCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.source.coldstart.ColdStartDetailEditorComponent;
import nl.overheid.aerius.wui.application.components.source.combustion.CombustionPlantDetailEditorComponent;
import nl.overheid.aerius.wui.application.components.source.farmanimalhousing.FarmAnimalHousingDetailEditorComponent;
import nl.overheid.aerius.wui.application.components.source.farmland.FarmlandDetailEditorComponent;
import nl.overheid.aerius.wui.application.components.source.farmlodging.FarmLodgingDetailEditorComponent;
import nl.overheid.aerius.wui.application.components.source.manure.ManureStorageDetailEditorComponent;
import nl.overheid.aerius.wui.application.components.source.offroad.OffRoadMobileDetailEditorComponent;
import nl.overheid.aerius.wui.application.components.source.road.ADMSRoadDetailEditorComponent;
import nl.overheid.aerius.wui.application.components.source.road.SRM2RoadDetailEditorComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.inland.inland.InlandShippingDetailEditorComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.inland.mooring.InlandMooringShippingDetailEditorComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.maritime.MaritimeShippingDetailEditorComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.maritime.mooring.MooringMaritimeShippingDetailEditorComponent;
import nl.overheid.aerius.wui.application.context.EmissionSourceEditorContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSubSourceFeature;
import nl.overheid.aerius.wui.application.ui.main.LeftPanelCloseEvent;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component(components = {
    FarmAnimalHousingDetailEditorComponent.class,
    FarmLodgingDetailEditorComponent.class,
    FarmlandDetailEditorComponent.class,
    ManureStorageDetailEditorComponent.class,
    ADMSRoadDetailEditorComponent.class,
    SRM2RoadDetailEditorComponent.class,
    MaritimeShippingDetailEditorComponent.class,
    OffRoadMobileDetailEditorComponent.class,
    ColdStartDetailEditorComponent.class,
    InlandShippingDetailEditorComponent.class,
    InlandMooringShippingDetailEditorComponent.class,
    MooringMaritimeShippingDetailEditorComponent.class,
    CombustionPlantDetailEditorComponent.class,
    HorizontalCollapse.class,
    VerticalCollapse.class,
    ButtonIcon.class
})
public class EmissionSourceDetailView extends BasicVueView implements HasCreated, HasDestroyed {
  private static final EmissionSourceDetailViewEventBinder EVENT_BINDER = GWT.create(EmissionSourceDetailViewEventBinder.class);

  interface EmissionSourceDetailViewEventBinder extends EventBinder<EmissionSourceDetailView> {}

  @Prop EventBus eventBus;

  @Inject @Data EmissionSourceEditorContext context;
  @Inject @Data ScenarioContext scenarioContext;

  private HandlerRegistration handlers;

  @JsMethod
  public void onDetailEditorErrorChange(final boolean error) {
    eventBus.fireEvent(new DetailEditorErrorChangeCommand(error));
  }

  @JsMethod
  public void onDetailEditorWarningChange(final boolean error) {
    eventBus.fireEvent(new DetailEditorWarningChangeCommand(error));
  }

  @Override
  public void created() {
    handlers = EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Override
  public void destroyed() {
    handlers.removeHandler();
  }

  @Computed
  public String getEmissionDetailEditorType() {
    return EmissionEditorComponentUtil.getEmissionEditorDetailComponent(getSource());
  }

  @EventHandler
  public void onLeftPanelCloseEvent(final LeftPanelCloseEvent e) {
    if (e.getValue()) {
      eventBus.fireEvent(new EmissionSubSourceDeselectFeatureCommand());
    }
  }

  @Computed
  public int getSelectedIndex() {
    return context.getSubSourceSelection();
  }

  @Computed
  EmissionSourceFeature getSource() {
    return context.getSource();
  }

  @Computed("hasSubSourceSelection")
  boolean hasSubSourceSelection() {
    return context.hasSubSourceSelection();
  }

  @Computed
  public Object getSubSource() {
    if (hasSubSourceSelection()) {
      final EmissionSubSourceFeature<?> feat = Js.uncheckedCast(context.getSource());

      // Add guard, this indicates a bug (and is quick to regress so should remain)
      if (getSelectedIndex() >= feat.getSubSources().length) {
        GWTProd.warn("Attempting to retrieve sub source that does not exist: ", getSelectedIndex());
        return null;
      }

      return feat.getSubSources().getAt(getSelectedIndex());
    }

    return null;
  }

  @Computed("isSubSourceAvailable")
  public boolean isSubSourceAvailable() {
    return getSubSource() != null;
  }

  @JsMethod
  public void close() {
    eventBus.fireEvent(new EmissionSubSourceDeselectFeatureCommand());
  }
}
