/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.input.drop;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.Vue;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.google.gwt.user.client.Window;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;
import jsinterop.base.Js;

import nl.overheid.aerius.wui.vue.BasicVueView;

@Component(name = "simple-listbox")
public class SimplifiedListBoxComponent extends BasicVueView implements IsVueComponent {
  @Prop boolean enabled;
  @Prop String placeholder;
  @Prop String id;

  @Prop boolean strict;

  @Prop int size;

  @Prop Function<Object, Object> keyFunction;
  @Prop Function<Object, String> nameFunction;

  @Prop String confirmation;

  @SuppressWarnings("rawtypes") @Prop(checkType = true) @JsProperty Collection options;
  @Prop @JsProperty Object[] optionsArr;

  @Prop String selectedKey;
  @Prop Object selected;

  @Prop boolean allowDeselection;

  @Data @JsProperty Map<String, Object> optionsData = new HashMap<>();
  @Data String selectedData = "";
  @Data boolean ignoreNextChange = false;

  @PropDefault("strict")
  public boolean strictDefault() {
    return true;
  }

  @PropDefault("enabled")
  public boolean enabledDefault() {
    return true;
  }

  @PropDefault("size")
  public int sizeDefault() {
    return 1;
  }

  @SuppressWarnings("rawtypes")
  @PropDefault("options")
  public List optionsDefault() {
    return new ArrayList<>();
  }

  @PropDefault("keyFunction")
  public Function<Object, String> keyFunctionDefault() {
    return String::valueOf;
  }

  @PropDefault("nameFunction")
  public Function<Object, String> nameFunctionDefault() {
    return String::valueOf;
  }

  @Watch(value = "optionsArr", isImmediate = true)
  public void onOptionsArrChange(final Object[] neww) {
    if (neww == null) {
      return;
    }

    // Invoke on the next tick, this is a parallel of :options and can therefore not
    // be executed in the same tick
    Vue.nextTick(() -> {
      indexOptions(Stream.of(neww).collect(Collectors.toList()));

      // Trigger selected prop watcher manually here (because it probably updated
      // based on old options data)
      onSelectedChange(selected);
    });
  }

  @Watch(value = "options", isImmediate = true)
  public void onOptionsChange(final List<Object> neww) {
    indexOptions(neww);
  }

  public void indexOptions(final List<Object> options) {
    optionsData.clear();
    if (options == null) {
      return;
    }

    optionsData.putAll(options.stream()
        .collect(Collectors.toMap(v -> formatKey(Js.uncheckedCast(v)), Js::uncheckedCast)));
  }

  @Watch(value = "selected", isImmediate = true)
  public void onSelectedChange(final Object neww) {
    if (neww == null && strict) {
      selectedData = "";
    } else {
      setSelection(formatKey(neww));
    }
  }

  @Watch(value = "selectedKey", isImmediate = true)
  public void onSelectedKeyChange(final String neww) {
    if (neww == null) {
      // Do nothing
    } else {
      setSelection(neww);
    }
  }

  private void setSelection(final String selection) {
    if (optionsData.containsKey(selection) || !strict) {
      selectedData = selection;
    } else {
      selectedData = "";
    }
  }

  @JsMethod
  public boolean isSelected(final Object obj) {
    return selectedData != null && selectedData.equals(formatKey(obj));
  }

  @JsMethod
  public String formatKey(final Object obj) {
    // Do an additional string conversion so the key function can return things like
    // integers aswell
    return String.valueOf(keyFunction.apply(obj));
  }

  @JsMethod
  public String formatName(final Object obj) {
    return nameFunction.apply(obj);
  }

  @Watch("selectedData")
  public void onSelectedDataChange(final String newVal, final String oldVal) {
    if (ignoreNextChange) {
      selectedData = oldVal;
      ignoreNextChange = false;
    }
  }

  @JsMethod
  public void onUserChange() {
    if (allowDeselection) {
      return; // Use `toggle()` instead
    }

    // If a confirmation message is necessary, ask for confirmation and set a reset on the change if cancelled
    // (unfortunately, we can't prevent the event and stop the listbox changing its value, so we must
    // reset it manually, which we do via the watcher that is fired after this change event concludes)
    if (confirmation != null
        && !Window.confirm(confirmation)) {
      ignoreNextChange = true;
      return;
    }

    if (!optionsData.containsKey(selectedData)) {
      if (strict) {
        throw new IllegalStateException("Selected data not known to be an option: " + selectedData);
      } else {
        vue().$emit("select-unknown", selectedData);
        return;
      }
    }

    vue().$emit("select", optionsData.get(selectedData));
    vue().$emit("select-key", selectedData);
  }

  @JsMethod
  public void toggle() {
    if (allowDeselection) {
      vue().$emit("select", optionsData.get(selectedData));
    }
  }
}
