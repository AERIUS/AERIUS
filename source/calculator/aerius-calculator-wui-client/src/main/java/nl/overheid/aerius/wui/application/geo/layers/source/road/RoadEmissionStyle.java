/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source.road;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import ol.OLFactory;
import ol.geom.Geometry;
import ol.geom.LineString;
import ol.style.Style;

import nl.aerius.geo.domain.legend.ColorLabelsLegend;
import nl.aerius.geo.domain.legend.LegendType;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.legend.ColorRange;
import nl.overheid.aerius.shared.domain.legend.ColorRangeType;
import nl.overheid.aerius.shared.domain.legend.ColorRangesLegend;
import nl.overheid.aerius.wui.application.domain.source.RoadESFeature;
import nl.overheid.aerius.wui.application.geo.util.GeoType;
import nl.overheid.aerius.wui.application.i18n.M;

/**
 * Style to display road emissions on the map.
 */
public class RoadEmissionStyle implements RoadStyle {

  public static final int DEFAULT_ROAD_EMISSION_WIDTH = 6;

  private final double conversionFactor;
  private List<ColorRange> colorRange;
  private final List<ColorRange> colorRangeNox;
  private final List<ColorRange> colorRangeNH3;
  private Substance selectedSubstance = Substance.NOX;

  public RoadEmissionStyle(final Function<ColorRangeType, List<ColorRange>> colorRangeFunction, final double conversionFactor) {
    colorRangeNox = colorRangeFunction.apply(ColorRangeType.ROAD_EMISSION_NOX);
    colorRangeNH3 = colorRangeFunction.apply(ColorRangeType.ROAD_EMISSION_NH3);
    colorRange = colorRangeNox;
    this.conversionFactor = conversionFactor;
  }

  private Map<Double, List<Style>> getStyleMap() {
    return colorRange.stream()
        .collect(Collectors.toMap(
            cr -> (double) cr.getLowerValue(),
            cr -> Collections.singletonList(OLFactory.createStyle(OLFactory.createStroke(OLFactory.createColor(cr.getColor()),
                DEFAULT_ROAD_EMISSION_WIDTH)))));
  }

  private void updateRoadStyle(final Substance substance) {
    selectedSubstance = substance;
    colorRange = Substance.NOX == substance ? colorRangeNox : colorRangeNH3;
  }

  @Override
  public List<Style> getStyle(final RoadESFeature feature, final String vehicleTypeCode, final double resolution) {
    final double emission = feature != null ? calculateEmissionPerKm(feature) : 0D;
    final Map<Double, List<Style>> styleMap = getStyleMap();
    final double lowerValueRange = styleMap.keySet().stream()
        .mapToDouble(x -> x)
        .filter(x -> x <= emission)
        .max().orElse(0D);
    return styleMap.get(lowerValueRange);
  }

  private double calculateEmissionPerKm(final RoadESFeature feature) {
    final Geometry geometry = feature.getGeometry();

    if (GeoType.LINE_STRING.is(geometry)) {
      final double length = ((LineString) geometry).getLength();
      return length > 0 ? (feature.getEmission(selectedSubstance) * conversionFactor / length) : 0;
    } else {
      return feature.getEmission(selectedSubstance);
    }
  }

  @Override
  public ColorLabelsLegend getLegend(final ApplicationConfiguration appThemeConfiguration) {
    return new ColorRangesLegend(
        colorRange,
        String::valueOf,
        M.messages().colorRangesLegendBetweenText(),
        LegendType.LINE,
        M.messages().layerRoadEmissionUnit(appThemeConfiguration.getTheme()));
  }

  public void setSelectedSubstance(final Substance selectedSubstance) {
    updateRoadStyle(selectedSubstance);
  }
}
