/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.print.own2000.results;

import nl.aerius.wui.command.SimpleGenericCommand;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;

/**
 * Issuing this command sets the extent of the map to include features from all situations, and the markers associated with the {@link SituationResultsKey}.
 */
public class PrintViewZoomCommand extends SimpleGenericCommand<SituationResultsKey, PrintViewZoomEvent> {

  public PrintViewZoomCommand(final SituationResultsKey value) {
    super(value);
  }

  @Override
  protected PrintViewZoomEvent createEvent(final SituationResultsKey situationResultsKey) {
    return new PrintViewZoomEvent(situationResultsKey);
  }
}
