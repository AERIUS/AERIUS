/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.calculation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

import javax.inject.Singleton;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import jsinterop.annotations.JsProperty;

import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.future.AppAsyncCallback;
import nl.aerius.wui.util.GWTAtomicInteger;
import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.summary.OverlappingHexagonType;
import nl.overheid.aerius.shared.domain.summary.ProcurementPolicy;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.command.result.ChangeSituationResultKeyCommand;
import nl.overheid.aerius.wui.application.command.result.ClearCustomCalculationPointResultCommand;
import nl.overheid.aerius.wui.application.command.result.ClearResultValueBoundsCommand;
import nl.overheid.aerius.wui.application.command.result.FetchSummaryResultsCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ResultsSummaryContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationHandle;
import nl.overheid.aerius.wui.application.context.result.ResultSelectionContext;
import nl.overheid.aerius.wui.application.context.result.ResultSummaryContext;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.application.event.CalculationCompleteEvent;
import nl.overheid.aerius.wui.application.event.CalculationStartEvent;
import nl.overheid.aerius.wui.application.event.CalculationStatusEvent;
import nl.overheid.aerius.wui.application.event.SituationResultsSelectedEvent;
import nl.overheid.aerius.wui.application.event.SwitchActiveCalculationEvent;
import nl.overheid.aerius.wui.application.service.CalculationServiceAsync;
import nl.overheid.aerius.wui.application.ui.pages.results.habitat.SelectResultHabitatStatisticTypeCommand;
import nl.overheid.aerius.wui.application.util.ResultStatisticUtil;

/**
 * This daemon manages retrieval, persistence, and caching of result summaries via resultkeys
 */
@Singleton
public class ResultsSummaryDaemon extends BasicEventComponent {
  private static final ResultsDaemonEventBinder EVENT_BINDER = GWT.create(ResultsDaemonEventBinder.class);

  interface ResultsDaemonEventBinder extends EventBinder<ResultsSummaryDaemon> {}

  private final @JsProperty Map<SituationResultsKey, Integer> resultAges = new HashMap<>();

  @Inject private CalculationServiceAsync calculationService;
  @Inject private ApplicationContext applicationContext;
  @Inject private CalculationContext calculationContext;
  @Inject private ResultSelectionContext selectionContext;
  @Inject private ResultsSummaryContext resultsSummaryContext;

  private final Map<SituationResultsKey, GWTAtomicInteger> calculationAges = new HashMap<>();
  private final Map<SituationResultsKey, GWTAtomicInteger> serviceCallCounters = new HashMap<>();

  @EventHandler
  public void onChangeSituationResultKeyCommand(final ChangeSituationResultKeyCommand c) {
    getResultContext().ifPresent(context -> {
      final SituationResultsKey key = c.getValue();
      updateSituationHandle(context, key.getSituationHandle());
      updateResultType(context, key.getResultType());
      updateEmissionResultKey(context, key.getEmissionResultKey());
      updateHexagonType(context, key.getHexagonType());
      updateOverlappingHexagonType(context, key.getOverlappingHexagonType());
      updateProcurementPolicy(context, key.getProcurementPolicy());

      // Clear result value bounds
      eventBus.fireEvent(new ClearResultValueBoundsCommand());
      eventBus.fireEvent(new ClearCustomCalculationPointResultCommand());

      renewCalculationResult(c.getValue(), context, v -> eventBus.fireEvent(new SituationResultsSelectedEvent(v)));
    });
  }

  private static void updateSituationHandle(final ResultSummaryContext context, final SituationHandle handle) {
    context.setSituationHandle(handle);
  }

  private void updateResultType(final ResultSummaryContext context, final ScenarioResultType type) {
    final ScenarioResultType old = context.getResultType();
    if (old == type) {
      return;
    }

    context.setResultType(type);

    // Update habitat result type statistics to be compatible with the scenario (if it isn't)
    final List<ResultStatisticType> types = ResultStatisticUtil.getStatisticsForResult(applicationContext.getTheme(),
        context.getResultType(),
        context.getHexagonType(), context.getEmissionResultKey());
    if (!types.contains(selectionContext.getHabitatStatisticType())) {
      final ResultStatisticType defaultStatisticType = types.get(0);
      eventBus.fireEvent(new SelectResultHabitatStatisticTypeCommand(defaultStatisticType));
    }
  }

  private static void updateEmissionResultKey(final ResultSummaryContext context, final EmissionResultKey emissionResultKey) {
    context.setEmissionResultKey(emissionResultKey);
  }

  private static void updateHexagonType(final ResultSummaryContext context, final SummaryHexagonType type) {
    context.setHexagonType(type);
  }

  private static void updateOverlappingHexagonType(final ResultSummaryContext context, final OverlappingHexagonType type) {
    context.setOverlappingHexagonType(type);
  }

  private static void updateProcurementPolicy(final ResultSummaryContext context, final ProcurementPolicy procurementPolicy) {
    context.setProcurementPolicy(procurementPolicy);
  }

  @EventHandler
  public void onSwitchActiveCalculationEvent(final SwitchActiveCalculationEvent e) {
    updateActiveSituationResultsSummary();
  }

  @EventHandler
  public void onFetchSummaryResultsCommand(final FetchSummaryResultsCommand c) {
    // Event is fired after request completed.
    c.silence();

    getResultContext()
        .ifPresent(context -> renewCalculationResult(c.getValue(), context, key -> eventBus.fireEvent(c.getEvent())));
  }

  private void renewCalculationResult(final SituationResultsKey resultsKey, final ResultSummaryContext context,
      final Consumer<SituationResultsKey> callback) {
    if (actualizeResultKey(resultsKey)) {
      eventBus.fireEvent(new SituationResultsSelectedEvent(resultsKey));
      return;
    }

    final CalculationExecutionContext calculation = calculationContext.getActiveCalculation();
    if (calculation == null || calculation.getJobKey() == null) {
      // Log a warning to point out a regression has occurred (and handled)
      GWTProd.warn("Not expecting calculation to be empty at this point. (1)");
      return;
    }

    if (!(calculation.isCalculating() || calculation.isCalculationFinalState())) {
      // If not yet calculating or finished, there's no point in retrieving results.
      return;
    }

    final int count = serviceCallCounters.computeIfAbsent(resultsKey, key -> new GWTAtomicInteger()).incrementAndGet();
    context.setLoading(true);
    calculationService.getSituationResultsSummary(calculation.getJobKey(), resultsKey, AppAsyncCallback.create(summary -> {
      // Belated bail if the counter has updated -- will fetch next time
      if (count != serviceCallCounters.get(resultsKey).get()) {
        return;
      }

      context.setLoading(false);
      context.addResultSummary(resultsKey, summary);
      callback.accept(resultsKey);
    }, e -> GWTProd.error("Failure while fetching calculation scenario result summary for job \"" + getJobName(calculation) + "\". Message: ",
        e.getMessage(), e)));

    fetchExecutionSummary(calculation);
  }

  private static String getJobName(final CalculationExecutionContext calculation) {
    return calculation.getJobContext().getName();
  }

  @EventHandler
  public void onCalculationCompleteEvent(final CalculationCompleteEvent e) {
    fetchExecutionSummary(e.getValue());
  }

  private void fetchExecutionSummary(final CalculationExecutionContext calculation) {
    if (calculation == null || calculation.getJobKey() == null) {
      // Log a warning to point out a regression has occurred (and handled)
      GWTProd.warn("Not expecting calculation to be empty at this point. (2)");
      return;
    }

    calculationService.getCalculationSummary(calculation.getJobKey(), AppAsyncCallback.create(
        calculation::setJobSummary,
        e -> GWTProd.error("Failure while fetching calculation summary status for job \"" + getJobName(calculation) + "\". Message: ",
            e.getMessage(), e)));
    if (shouldDoDecisionFrameworkRequest(calculation)) {
      calculationService.getDecisionFrameworkResults(calculation.getJobKey(), AppAsyncCallback.create(
          calculation::setDecisionFrameworkResults,
          e -> GWTProd.error("Failure while fetching decision framework results for job \"" + getJobName(calculation) + "\". Message: ",
              e.getMessage(), e)));
    }
  }

  private boolean shouldDoDecisionFrameworkRequest(final CalculationExecutionContext calculation) {
    final CalculationJobType calculationJobType = calculation.getJobContext().getCalculationOptions().getCalculationJobType();
    return applicationContext.isAppFlagEnabled(AppFlag.HAS_DEVELOPMENT_PRESSURE_SEARCH)
        && (calculationJobType == CalculationJobType.PROCESS_CONTRIBUTION
            || calculationJobType == CalculationJobType.IN_COMBINATION_PROCESS_CONTRIBUTION);
  }

  /**
   * Return true if local result key is actual, false if not
   */
  private boolean actualizeResultKey(final SituationResultsKey resultsKey) {
    final int localAge = getCalculationAge(resultsKey).get();
    // If the result age is equal to the calculation age, bail out (this indicates a
    // result is not potentially newer and can be retrieved out of the cache)
    final int resultAge = getResultAge(resultsKey);
    if (localAge != 0 && resultAge == localAge) {
      return true;
    }

    actualizeResultAge(resultsKey, localAge);
    return false;
  }

  @EventHandler
  public void calculationStartEvent(final CalculationStartEvent e) {
    getCalculationAge(e.getValue().getResultContext().getSituationResultsKey()).set(0);
  }

  @EventHandler
  public void calculationStatusEvent(final CalculationStatusEvent e) {
    // Only renew if the status event is for the active calculation
    if (!Optional.ofNullable(calculationContext.getActiveCalculation())
        .map(CalculationExecutionContext::getJobKey)
        .orElse("").equals(e.getJobKey())) {
      return;
    }

    getCalculationAge(getResultContext().get().getSituationResultsKey()).incrementAndGet();

    final int numberOfPointsCalculatedPrev = resultsSummaryContext.getNumberOfPointsCalculated(e.getJobKey(), 0);
    final int numberOfPointsCalculated = e.getValue().getJobProgress().getNumberOfPointsCalculated();
    // Only update when number of points calculated counts are different, or when the success state has been attained
    if (numberOfPointsCalculatedPrev != numberOfPointsCalculated
        || e.getValue().getJobProgress().getState().isSuccessState()) {
      updateActiveSituationResultsSummary();
    }

    resultsSummaryContext.setNumberOfPointsCalculated(e.getJobKey(), numberOfPointsCalculated);
  }

  private void updateActiveSituationResultsSummary() {
    getResultContext().ifPresent(context -> {
      final SituationResultsKey resultsKey = context.getSituationResultsKey();
      renewCalculationResult(resultsKey, context, key -> eventBus.fireEvent(new SituationResultsSelectedEvent(key)));
    });
  }

  public Integer getResultAge(final SituationResultsKey resultsKey) {
    return resultAges.computeIfAbsent(resultsKey, key -> 0);
  }

  public GWTAtomicInteger getCalculationAge(final SituationResultsKey resultsKey) {
    return calculationAges.computeIfAbsent(resultsKey, key -> new GWTAtomicInteger());
  }

  public void actualizeResultAge(final SituationResultsKey resultsKey, final int calculationAge) {
    resultAges.put(resultsKey, calculationAge);
  }

  private Optional<ResultSummaryContext> getResultContext() {
    return Optional.ofNullable(calculationContext.getActiveCalculation())
        .map(v -> v.getResultContext());
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
