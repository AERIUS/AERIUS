/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source;

import static jsinterop.annotations.JsPackage.GLOBAL;

import java.util.function.Consumer;

import elemental2.core.JsArray;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.vue.ReactivityUtil;

/**
 * Feature object for Emission Sources with a list of sub sources.
 */
@JsType(namespace = GLOBAL, name = "ol.Feature", isNative = true)
public abstract class EmissionSubSourceFeature<T extends AbstractSubSource> extends EmissionSourceFeature {

  protected static final @JsOverlay <T extends AbstractSubSource> void initSubSourceFeature(final EmissionSubSourceFeature<T> feature,
      final CharacteristicsType characteristicsType, final EmissionSourceType emissionSourceType) {
    EmissionSourceFeature.initEmissionSource(feature, characteristicsType, emissionSourceType);
    ReactivityUtil.ensureDefault(feature::getSubSources, feature::setSubSources, new JsArray<>());
  }

  /**
   * Read a simple array of sub sources and return it as an @JsArray.
   *
   * @return Returns sub sources
   */
  public final @JsOverlay JsArray<T> getSubSources() {
    return Js.uncheckedCast(get("subSources"));
  }

  /**
   * Supply @JsArray of T and write it.
   *
   * @param T sub sources
   */
  public final @JsOverlay void setSubSources(final JsArray<T> subSources) {
    set("subSources", subSources);
  }

  public static final @JsOverlay <T extends AbstractSubSource> void initSubSources(final EmissionSubSourceFeature<T> feat, final Consumer<T> object) {
    feat.getSubSources().forEach((v, idx, lst) -> {
      object.accept(v);
      return null;
    });
  }
}
