/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.criticallevel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.OLFactory;
import ol.layer.Image;
import ol.layer.Layer;
import ol.source.ImageWms;
import ol.source.ImageWmsOptions;
import ol.source.ImageWmsParams;

import nl.aerius.geo.domain.IsLayer;
import nl.aerius.geo.domain.LayerInfo;
import nl.aerius.geo.domain.legend.Legend;
import nl.overheid.aerius.js.calculation.ResultsGraphRange;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.event.CalculationCancelEvent;
import nl.overheid.aerius.wui.application.event.CalculationCompleteEvent;
import nl.overheid.aerius.wui.application.event.CalculationStartEvent;
import nl.overheid.aerius.wui.application.event.CalculationStatusEvent;
import nl.overheid.aerius.wui.application.event.ResultsSelectedEvent;
import nl.overheid.aerius.wui.application.event.SituationResultsSelectedEvent;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;

public abstract class BasePCLResultLayer implements IsLayer<Layer> {

  private static final String WMS_BASE_LAYER_NAME = "calculator:wms_";

  private static final String PROJECT_CALCULATION = "project_calculation";
  private static final String IN_COMBINATION = "in_combination";
  private static final String ARCHIVE_CONTRIBUTION = "archive_contribution";

  private static final String PERCENTAGE_CL_NAME = "percentage_cl";

  private static final String HEXAGON = "hexagon";
  private static final String RECEPTOR = "receptor";
  private static final String SUB_POINT = "sub_point";

  private static final String CQL_FILTER = "CQL_FILTER";

  private final String name;

  protected final LayerInfo info;
  private final Image layer;

  private final ImageWmsParams imageWmsParams;
  private final ImageWmsOptions imageWmsOptions;
  private ImageWms imageWms;

  private String situationId = null;
  protected ScenarioResultType resultType = ScenarioResultType.SITUATION_RESULT;
  private SummaryHexagonType hexagonType = SummaryHexagonType.RELEVANT_HEXAGONS;
  protected EmissionResultKey emissionResultKey = EmissionResultKey.NOXNH3_DEPOSITION;
  protected List<ResultsGraphRange> selectedBounds = new ArrayList<>();

  protected final CalculationContext calculationContext;
  protected final ApplicationConfiguration themeConfiguration;

  @Inject
  public BasePCLResultLayer(final String name, final String title, final ApplicationConfiguration themeConfiguration, final EventBus eventBus,
      final EnvironmentConfiguration configuration, final CalculationContext calculationContext) {
    this.name = name;
    this.calculationContext = calculationContext;
    this.themeConfiguration = themeConfiguration;

    this.info = new LayerInfo();
    this.info.setName(this.getClass().getSimpleName());
    this.info.setTitle(title);
    updateLegend();

    imageWmsParams = OLFactory.createOptions();

    imageWmsOptions = OLFactory.createOptions();
    imageWmsOptions.setUrl(configuration.getGeoserverBaseUrl());
    imageWmsOptions.setParams(imageWmsParams);

    layer = new Image();
  }

  @EventHandler
  public void onCalculationStartEvent(final CalculationStartEvent e) {
    this.situationId = null;
    updateLayer();
  }

  @EventHandler
  public void onCalculationCancelEvent(final CalculationCancelEvent e) {
    this.situationId = null;
    updateLayer();
  }

  @EventHandler
  public void onCalculationStatusEvent(final CalculationStatusEvent e) {
    updateLayer();
  }

  @EventHandler
  public void onCalculationCompleteEvent(final CalculationCompleteEvent e) {
    updateLayer();
  }

  @EventHandler
  public void onSituationResultsSelectedEvent(final SituationResultsSelectedEvent e) {
    situationId = e.getValue().getSituationHandle().getId();
    resultType = e.getValue().getResultType();
    hexagonType = e.getValue().getHexagonType();
    emissionResultKey = e.getValue().getEmissionResultKey();
    updateLayer();
  }

  @EventHandler
  public void onResultsSelectedEvent(final ResultsSelectedEvent e) {
    selectedBounds = e.getValue().getSelectedBounds();
    updateLayer();
  }

  private String buildLayerName() {
    String url = WMS_BASE_LAYER_NAME;

    switch (resultType) {
    case PROJECT_CALCULATION:
      url += PROJECT_CALCULATION;
      break;
    case IN_COMBINATION:
      url += IN_COMBINATION;
      break;
    case ARCHIVE_CONTRIBUTION:
      url += ARCHIVE_CONTRIBUTION;
      break;
    }

    url += name;

    final EmissionResultType emissionResultType = emissionResultKey.getEmissionResultType();

    switch (emissionResultType) {
    case DEPOSITION:
      url += HEXAGON;
      break;
    case CONCENTRATION:
      url += SUB_POINT;
      break;
    default:
      url += RECEPTOR;
      break;
    }

    return url;
  }

  private String buildViewParams() {
    String viewParams = "";
    viewParams += "job_key:" + calculationContext.getActiveJobId();
    viewParams += ";situation_reference:" + situationId;
    viewParams += ";hexagon_type:" + hexagonType.name().toLowerCase(Locale.ROOT);
    viewParams += ";emission_result_type:" + emissionResultKey.getEmissionResultType().name().toLowerCase(Locale.ROOT);
    viewParams += ";substance_id:" + emissionResultKey.getSubstance().getId();
    return viewParams;
  }

  protected String buildCqlFilter() {
    final String cqlFilterOn = PERCENTAGE_CL_NAME;
    return selectedBounds.stream().map(bound -> {
      if (Double.isInfinite(bound.getLowerBound())) {
        return cqlFilterOn + " < " + bound.getUpperBound();
      } else if (Double.isInfinite(bound.getUpperBound())) {
        return cqlFilterOn + " >= " + bound.getLowerBound();
      } else {
        return cqlFilterOn + " >= " + bound.getLowerBound() + " AND " + cqlFilterOn + " < " + bound.getUpperBound();
      }
    }).map(bound -> "(" + bound + ")").collect(Collectors.joining(" OR "));
  }

  private void updateLegend() {
    final Legend legend = createLegend();
    info.setLegend(legend);
  }

  protected abstract Legend createLegend();

  protected void updateLayer() {
    imageWmsParams.setViewParams(buildViewParams());
    imageWmsParams.setLayers(buildLayerName());

    final String cqlFilter = buildCqlFilter();
    if (cqlFilter.isEmpty()) {
      imageWmsParams.set(CQL_FILTER, null);
    } else {
      imageWmsParams.set(CQL_FILTER, cqlFilter);
    }

    if (imageWms == null) {
      imageWms = new ImageWms(imageWmsOptions);
      layer.setSource(imageWms);
    }

    imageWms.refresh();

    updateLegend();
  }

  @Override
  public Layer asLayer() {
    return layer;
  }

  @Override
  public Optional<LayerInfo> getInfoOptional() {
    return Optional.of(info);
  }

  @Override
  public LayerInfo getInfo() {
    return info;
  }

}
