/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.interactions;

import com.google.web.bindery.event.shared.EventBus;

import ol.AtPixelOptions;
import ol.Feature;
import ol.MapBrowserEvent;
import ol.interaction.Interaction;
import ol.interaction.Pointer;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.aerius.geo.domain.IsInteraction;
import nl.overheid.aerius.wui.application.command.result.SelectCustomCalculationPointResultCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceFeatureSelectCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceToggleHighlightCommand;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.road.RoadEmissionSourceTypes;
import nl.overheid.aerius.wui.application.geo.layers.LabelFeature;

/**
 * Provides the interaction between the source geometry/labels on the map and the emission source list
 */
@JsType
public class FeatureClickInteraction extends Pointer implements IsInteraction<Interaction> {

  /**
   * Border around the features to be able to select line sources by clicking on the line
   */
  private static final int HIT_TOLERANCE = 3;

  private final EventBus eventBus;

  public FeatureClickInteraction(final EventBus eventBus) {
    this.eventBus = eventBus;
  }

  private static boolean isClusteredFeature(final Feature feature) {
    return feature.getProperties().has("features");
  }

  private static boolean isLabelFeature(final Feature feature) {
    return feature.getProperties().has("referenceFeature");
  }

  private static Feature[] getFeaturesInCluster(final Feature feature) {
    return Js.uncheckedCast(feature.getProperties().get("features"));
  }

  private static boolean isCustomPointResult(final Feature feature) {
    return feature.getProperties().has("customPointResult");
  }

  @JsMethod
  public boolean handleDownEvent(final MapBrowserEvent event) {
    final AtPixelOptions atPixelOptions = new AtPixelOptions();
    atPixelOptions.setHitTolerance(HIT_TOLERANCE);

    event.getMap().forEachFeatureAtPixel(event.getPixel(), (feature, layer) -> {

      if (isClusteredFeature(feature)) {
        if (getFeaturesInCluster(feature).length > 1) {
          // Clicked clustered source marker, ignore
          return true;
        } else {
          feature = getFeaturesInCluster(feature)[0];
        }
      }

      if (isLabelFeature(feature)) {
        feature = ((LabelFeature) feature).getReferenceFeature();
      }

      if (isCustomPointResult(feature)) {
        eventBus.fireEvent(new SelectCustomCalculationPointResultCommand(feature.get("customPointResult"),
            SelectCustomCalculationPointResultCommand.SelectionSource.MAP));
      }

      final EmissionSourceFeature source = (EmissionSourceFeature) feature;
      if (source.getEmissionSourceType() == null) {
        return true;
      }

      if (RoadEmissionSourceTypes.isRoadSource(source)) {
        eventBus.fireEvent(new EmissionSourceToggleHighlightCommand(source));
      } else {
        // A single feature was selected, select that feature
        eventBus.fireEvent(new EmissionSourceFeatureSelectCommand(source, false));
      }

      return true;
    }, atPixelOptions);

    return false;
  }

  @Override
  public Interaction asInteraction() {
    return this;
  }
}
