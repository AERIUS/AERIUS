/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.job;

import javax.inject.Inject;

import com.google.inject.assistedinject.Assisted;

import ol.Collection;
import ol.layer.Base;
import ol.layer.Group;
import ol.layer.LayerGroupOptions;

import nl.aerius.geo.domain.IsLayer;
import nl.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.wui.application.geo.layers.LayerFactory;
import nl.overheid.aerius.wui.application.i18n.M;

public class MetSiteLayerGroup implements IsLayer<Group> {

  private final Group layer;
  private final LayerInfo info;

  private final MetSiteMarkerLayer markerLayer;
  private final MetSiteNwpLayer nwpSiteLayer;

  @Inject
  public MetSiteLayerGroup(final LayerFactory layerFactory, @Assisted final int zIndex) {
    this.info = new LayerInfo();
    this.info.setName(this.getClass().getCanonicalName());
    this.info.setTitle(M.messages().layerMetSites());

    final Collection<Base> baseLayers = new Collection<>();
    int layersZIndex = 0;

    nwpSiteLayer = layerFactory.createMetSiteNwpLayer(layersZIndex++);
    nwpSiteLayer.asLayer().setVisible(true);
    baseLayers.push(nwpSiteLayer.asLayer());

    markerLayer = layerFactory.createMetSiteLayer(layersZIndex++);
    markerLayer.asLayer().setVisible(true);
    baseLayers.push(markerLayer.asLayer());

    final LayerGroupOptions groupOptions = new LayerGroupOptions();
    groupOptions.setLayers(baseLayers);

    layer = new Group(groupOptions);
    layer.setZIndex(zIndex);
  }

  @Override
  public Group asLayer() {
    return layer;
  }

  @Override
  public LayerInfo getInfo() {
    return info;
  }
}
