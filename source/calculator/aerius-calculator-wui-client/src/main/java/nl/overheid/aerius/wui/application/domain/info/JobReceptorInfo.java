/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.info;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.wui.application.domain.result.EmissionResults;
import nl.overheid.aerius.wui.application.domain.result.ScenarioEmissionResults;

@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class JobReceptorInfo {

  private @JsProperty JsPropertyMap<Object> situationsEmissionResults;
  private @JsProperty JsPropertyMap<Object> scenarioEmissionResults;

  public final @JsOverlay EmissionResults getSituationEmissionResults(final String situationId) {
    return situationsEmissionResults.has(situationId)
        ? (EmissionResults) Js.asAny(situationsEmissionResults.get(situationId))
        : null;
  }

  public final @JsOverlay boolean hasSituationsEmissionResults() {
    return situationsEmissionResults != null;
  }

  public final @JsOverlay boolean hasSituationEmissionResults(final String situationId) {
    return hasSituationsEmissionResults() && situationsEmissionResults.has(situationId);
  }

  public final @JsOverlay ScenarioEmissionResults getScenarioEmissionResults(final ScenarioResultType scenarioResultType) {
    return scenarioEmissionResults.has(scenarioResultType.name())
        ? (ScenarioEmissionResults) Js.asAny(scenarioEmissionResults.get(scenarioResultType.name()))
        : null;
  }

  public final @JsOverlay boolean hasScenarioEmissionResults() {
    return scenarioEmissionResults != null;
  }

  public final @JsOverlay boolean hasScenarioEmissionResults(final ScenarioResultType scenarioResultType) {
    return hasScenarioEmissionResults() && scenarioEmissionResults.has(scenarioResultType.name());
  }

}
