/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.input.errors;

import java.util.function.Supplier;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;

import nl.aerius.vuelidate.Validations;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

/**
 * <p>
 * An error notice component that automatically displays the most appropriate error given a
 * {@link Validations} object.
 *
 * <p>
 * Supports automatic error texts for:
 *
 * <ul>
 * <li>required
 * <li>integer
 * <li>decimal
 * <li>email
 * <li>maxLength
 * <li>minValue
 * <li>maxValue
 * <li>minValue and maxValue (i.e. between)
 * <li>other invalid (fallback)
 * <ul>
 */
@Component(components = {
    InputErrorComponent.class,
})
public class SmartNoticeComponent extends BasicVueComponent {
  @Prop String name;
  @Prop Validations validation;
  @Prop(required = true) String label;

  /**
   * Whether this notice is attached to an input field (on the top right).
   */
  @Prop boolean attached;
  /**
   * Whether to fully round this component.
   */
  @Prop boolean round;
  /**
   * Whether to round the bottom of this component.
   */
  @Prop boolean roundBottom;

  @PropDefault("attached")
  boolean attachedDefault() {
    return false;
  }

  @PropDefault("round")
  boolean roundDefault() {
    return false;
  }

  @PropDefault("roundBottom")
  boolean roundBottomDefault() {
    return false;
  }

  @Computed("isDisplayNotice")
  public boolean isDisplayNotice() {
    return validation.invalid;
  }

  @Computed
  public Supplier<String> getNoticeMessage() {
    return () -> {
      final String msg;
      if (validation.params.required != null
          && !validation.required) {
        msg = M.messages().validationRequired();
      } else if (validation.params.integer != null
          && !validation.integer) {
        msg = M.messages().errorNumeric(validation.model);
      } else if (validation.params.decimal != null
          && !validation.decimal) {
        msg = M.messages().errorDecimal(validation.model);
      } else if (validation.params.email != null
          && !validation.email) {
        msg = M.messages().errorEmail(validation.model);
      } else if (validation.params.maxLength != null
          && !validation.maxLength) {
        msg = M.messages().validationMaxLength((int) validation.params.maxLength.max);
      } else if (validation.params.minValue != null
          && !validation.minValue && validation.params.maxValue == null) { // Lower limit and no upper limit
        msg = M.messages().ivDoubleLowerlimit(label, validation.params.minValue.min);
      } else if (validation.params.maxValue != null
          && !validation.maxValue && validation.params.minValue == null) { // Upper limit and no lower limit
        msg = M.messages().ivDoubleUpperlimit(label, validation.params.maxValue.max);
      } else if (validation.params.minValue != null
          && validation.params.maxValue != null
          && (!validation.minValue || !validation.maxValue)) { // Between
        msg = M.messages().ivDoubleRangeBetween(label, validation.params.minValue.min, validation.params.maxValue.max);
      } else if (validation.invalid) { // Generic error label
        msg = M.messages().validationErrorLabel();
      } else {
        msg = null;
      }

      return msg;
    };
  }
}
