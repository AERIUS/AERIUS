/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.domain.source;

import static jsinterop.annotations.JsPackage.GLOBAL;

import elemental2.core.JsArray;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.overheid.aerius.shared.domain.v2.point.AssessmentCategory;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointType;
import nl.overheid.aerius.wui.application.domain.calculationpoint.CalculationPointEntityReference;
import nl.overheid.aerius.wui.vue.ReactivityUtil;

@JsType(namespace = GLOBAL, name = "ol.Feature", isNative = true)
public class NcaCustomCalculationPointFeature extends CalculationPointFeature {
  public static final @JsOverlay NcaCustomCalculationPointFeature createNCACalculationPoint(final AssessmentCategory assessmentCategory) {
    final NcaCustomCalculationPointFeature feature = new NcaCustomCalculationPointFeature();
    init(feature, assessmentCategory);
    return feature;
  }

  public static final @JsOverlay void init(final NcaCustomCalculationPointFeature feature, final AssessmentCategory assessmentCategory) {
    CalculationPointFeature.initCalculationPointFeature(feature, CalculationPointType.NCA_CUSTOM_CALCULATION_POINT);
    feature.setAssessmentCategory(assessmentCategory);

    ReactivityUtil.ensureDefault(feature::getHeight, feature::setHeight, assessmentCategory == null ? null : assessmentCategory.getDefaultHeight());
    ReactivityUtil.ensureDefault(feature::getRoadLocalFractionNO2, feature::setRoadLocalFractionNO2, 0D);
    ReactivityUtil.ensureDefault(feature::getHabitats, feature::setHabitats, new JsArray<>());
  }

  public final @JsOverlay Double getHeight() {
    final Object height = get("height");
    return height == null ? null : Js.coerceToDouble(height);
  }

  public final @JsOverlay void setHeight(final Double height) {
    set("height", height == null ? null : String.valueOf(height));
  }

  public final @JsOverlay Double getRoadLocalFractionNO2() {
    final Object roadLocalFractionNO2 = get("roadLocalFractionNO2");
    return roadLocalFractionNO2 == null ? null : Js.coerceToDouble(roadLocalFractionNO2);
  }

  public final @JsOverlay void setRoadLocalFractionNO2(final Double roadLocalFractionNO2) {
    set("roadLocalFractionNO2", roadLocalFractionNO2);
  }

  public final @JsOverlay AssessmentCategory getAssessmentCategory() {
    return AssessmentCategory.safeValueOf(get("assessmentCategory"));
  }

  public final @JsOverlay void setAssessmentCategory(final AssessmentCategory category) {
    set("assessmentCategory", category == null ? null : category.name());
  }

  public final @JsOverlay JsArray<CalculationPointEntityReference> getHabitats() {
    return Js.uncheckedCast(get("entityReferences"));
  }

  public final @JsOverlay void setHabitats(final JsArray<CalculationPointEntityReference> habitats) {
    set("entityReferences", habitats);
  }

}
