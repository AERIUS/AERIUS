/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.util;

import elemental2.dom.ClientRect;
import elemental2.dom.DomGlobal;
import elemental2.dom.HTMLElement;

public final class ScreenUtil {
  public static final double OFFSET_LEFT = -10;

  public ScreenUtil() {}

  public static final native double getDevicePixelRatio() /*-{
    return window.devicePixelRatio;
  }-*/;

  public static void attachElementLeft(final HTMLElement dock, final HTMLElement target, final double offsetX) {
    if (dock == null) {
      return;
    }

    final ClientRect rect = dock.getBoundingClientRect();

    final double targetTop = rect.top;

    final double absoluteLeft = DomGlobal.window.innerWidth - rect.left;
    final double targetRight = absoluteLeft - offsetX;

    target.style.setProperty("--modal-top", targetTop + "px");
    target.style.setProperty("--modal-right", targetRight + "px");
  }
}
