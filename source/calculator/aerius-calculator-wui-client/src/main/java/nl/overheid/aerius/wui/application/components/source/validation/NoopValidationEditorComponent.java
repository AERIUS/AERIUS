/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.validation;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.wui.application.components.source.validation.NoopValidationEditorValidators.NoopValidationEditorValidations;

@Component(customizeOptions = {
    NoopValidationEditorValidators.class
}, components = {
    ValidationBehaviour.class,
})
public class NoopValidationEditorComponent extends ErrorWarningValidator
    implements HasCreated, HasValidators<NoopValidationEditorValidations> {

  @JsProperty(name = "$v") NoopValidationEditorValidations validation;

  @Override
  public NoopValidationEditorValidations getV() {
    return validation;
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

}
