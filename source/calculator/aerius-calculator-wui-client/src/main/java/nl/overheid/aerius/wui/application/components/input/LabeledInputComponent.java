/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.input;

import java.util.function.Supplier;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import elemental2.dom.HTMLElement;

import nl.aerius.vuelidate.Validations;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.wui.application.components.input.errors.SmartNoticeComponent;

/**
 * Wraps an input element and displays a label next to the input element. In
 * case of input errors it shows a border around in the input element and
 * displays the error text below the label/input row.
 */
@Component(components = {
    VerticalCollapse.class,
    SmartNoticeComponent.class,
    InputNoticeComponent.class,
    InputWarningComponent.class,
    InputErrorComponent.class,
    InputWithUnitComponent.class,
})
public class LabeledInputComponent implements IsVueComponent {
  /**
   * Label to show
   */
  @Prop String label;
  /**
   * Form name of the input element.
   */
  @Prop String name;
  /**
   * Whether to include input warning|error styles (default: yes)
   */
  @Prop boolean inputStyles;
  /**
   * Validations object bound to the input. Called to generically display errors.
   */
  @Prop Validations errorNotices;
  /**
   * Validations object bound to the input. Called to check for errors.
   */
  @Prop Validations errorValidation;
  /**
   * Override for showing notice status.
   */
  @Prop boolean showNotice;
  /**
   * Supplier that should return a customized notice message.
   */
  @Prop Supplier<String> noticeMessage;
  /**
   * Override for showing warning status.
   */
  @Prop boolean showWarning;
  /**
   * Supplier that should return a customized warning message in case of an
   * validation error.
   */
  @Prop Supplier<String> warningMessage;
  /**
   * Override for showing error status.
   */
  @Prop boolean showError;
  /**
   * Supplier that should return a customized error message in case of an
   * validation error.
   */
  @Prop Supplier<String> errorMessage;
  /**
   * Whether to show a gap between warnings/errors. Defaults to false.
   */
  @Prop boolean errorGaps;
  /**
   * Optional unit specifier for the input value.
   */
  @Prop String unit;
  /**
   * Optional description for the unit. Should be present if the unit is present.
   */
  @Prop String unitDescription;

  /**
   * Optional division, in fractions
   */
  @Prop String division;

  /**
   * Optional value to specify whether the background should be excluded.
   */
  @Prop boolean transparent;

  /**
   * Optional value to specify whether a gap should appear in the middle
   */
  @Prop boolean gap;

  /**
   * Optional value to make the label align at the top. This isn't enabled by default because it makes assumptions about the content/row height.
   */
  @Prop boolean alignTop;

  /**
   * Optional value to make the label align at the right.
   */
  @Prop boolean labelRight;

  @Ref HTMLElement contentContainer;

  @PropDefault("errorGaps")
  boolean errorGapsDefault() {
    return false;
  }

  @PropDefault("inputStyles")
  boolean inputStylesDefault() {
    return true;
  }

  @PropDefault("showNotice")
  boolean showNoticeDefault() {
    return false;
  }

  @PropDefault("showError")
  boolean showErrorDefault() {
    return false;
  }

  @PropDefault("showWarning")
  boolean showWarningDefault() {
    return false;
  }

  @PropDefault("division")
  String divisionDefault() {
    return "1fr 1fr";
  }

  @PropDefault("alignTop")
  boolean alignTopDefault() {
    return false;
  }

  @PropDefault("labelRight")
  boolean labelRightDefault() {
    return false;
  }

  @PropDefault("gap")
  boolean gapDefault() {
    return false;
  }

  @PropDefault("transparent")
  boolean transparentDefault() {
    return false;
  }

  @Watch("showWarning")
  public void onShowWarningChange(final boolean showWarning) {
    InputUtil.reportNotice(contentContainer, showWarning, false, name, "_warning");
  }

  /**
   * For errors, passing the `errorValidation` or `errorNotices` prop is also supported, so we have a separate
   * computed method to account for those.
   */
  @Computed("showErrorComputed")
  boolean showErrorComputed() {
    return isError() || showError;
  }

  @Watch("showErrorComputed()")
  public void onShowErrorChange(final boolean showError) {
    InputUtil.reportNotice(contentContainer, showError, true, name, "_error");
  }

  @Computed("isError")
  boolean isError() {
    return errorValidation != null && errorValidation.error
        || errorNotices != null && errorNotices.error;
  }
}
