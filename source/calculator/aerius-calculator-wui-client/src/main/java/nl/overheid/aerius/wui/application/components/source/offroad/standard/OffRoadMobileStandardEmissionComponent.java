/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.offroad.standard;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMobileSourceCategory;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.offroad.standard.OffRoadMobileStandardValidators.OffRoadMobileStandardValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.offroad.StandardOffRoadMobileSource;
import nl.overheid.aerius.wui.vue.DebugDirective;

@Component(customizeOptions = {
    OffRoadMobileStandardValidators.class
}, directives = {
    ValidateDirective.class,
    DebugDirective.class
}, components = {
    LabeledInputComponent.class,
    ValidationBehaviour.class,
})
public class OffRoadMobileStandardEmissionComponent extends ErrorWarningValidator
    implements HasCreated, HasValidators<OffRoadMobileStandardValidations> {
  @Prop @JsProperty StandardOffRoadMobileSource standardSubSource;
  @Data String descriptionV;
  @Data String literFuelV;
  @Data String operatingHoursV;
  @Data String literAdBlueV;
  @Data OffRoadMobileSourceCategory selectedCategory;

  @Data @Inject ApplicationContext applicationContext;

  @JsProperty(name = "$v") OffRoadMobileStandardValidations validation;

  @Watch(value = "standardSubSource", isImmediate = true)
  public void onStandardSubSourceChange() {
    descriptionV = String.valueOf(standardSubSource.getDescription());
    literFuelV = String.valueOf(standardSubSource.getLiterFuelPerYear());
    operatingHoursV = String.valueOf(standardSubSource.getOperatingHoursPerYear());
    literAdBlueV = String.valueOf(standardSubSource.getLiterAdBluePerYear());

    updateSelectedCategory();
    displayProblems(false);
  }

  @Computed
  public List<OffRoadMobileSourceCategory> getCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getOffRoadMobileSourceCategories();
  }

  @Override
  @Computed
  public OffRoadMobileStandardValidations getV() {
    return validation;
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Computed
  protected String getDescription() {
    return standardSubSource.getDescription();
  }

  @Computed
  protected void setDescription(final String description) {
    standardSubSource.setDescription(description);
    this.descriptionV = description;
  }

  @JsMethod
  protected String descriptionError() {
    return i18n.errorDescriptionRequired();
  }

  @JsMethod
  protected String literFuelError() {
    return i18n.ivIntegerLowerlimit(i18n.esOffRoadMobileStandardLiterFuel(), 1);
  }

  @JsMethod
  protected String operatingHoursError() {
    return i18n.ivIntegerLowerlimit(i18n.esOffRoadMobileStandardOperatingHours(), 1);
  }

  @JsMethod
  protected String literAdBlueError() {
    return i18n.ivIntegerLowerlimit(i18n.esOffRoadMobileStandardLiterAdBlue(), 0);
  }

  @Computed("offRoadMobileSourceCode")
  protected String getOffRoadMobileSourceCode() {
    return standardSubSource.getOffRoadMobileSourceCode();
  }

  @Computed("offRoadMobileSourceCode")
  protected void setOffRoadMobileSourceCode(final String offRoadMobileSourceCode) {
    standardSubSource.setOffRoadMobileSourceCode(offRoadMobileSourceCode);
    updateSelectedCategory();
  }

  @Computed
  protected String getLiterFuel() {
    return literFuelV;
  }

  @Computed
  protected void setLiterFuel(final String literFuel) {
    ValidationUtil.setSafeIntegerValue(value -> standardSubSource.setLiterFuelPerYear(value), literFuel, 0);
    literFuelV = literFuel;
  }

  @Computed
  protected String getOperatingHours() {
    return operatingHoursV;
  }

  @Computed
  protected void setOperatingHours(final String operatingHours) {
    ValidationUtil.setSafeIntegerValue(value -> standardSubSource.setOperatingHoursPerYear(value), operatingHours, 0);
    operatingHoursV = operatingHours;
  }

  @Computed
  protected String getLiterAdBlue() {
    return literAdBlueV;
  }

  @Computed
  protected void setLiterAdBlue(final String literAdBlue) {
    ValidationUtil.setSafeIntegerValue(value -> standardSubSource.setLiterAdBluePerYear(value), literAdBlue, 0);
    literAdBlueV = literAdBlue;
  }

  @Computed
  protected boolean needsLiterFuel() {
    return selectedCategory != null && selectedCategory.expectsLiterFuel();
  }

  @Computed
  protected boolean needsOperatingHours() {
    return selectedCategory != null && selectedCategory.expectsOperatingHours();
  }

  @Computed
  protected boolean needsLiterAdBlue() {
    return selectedCategory != null && selectedCategory.expectsLiterAdBlue();
  }

  @Computed("hasFuelAdBlueRatioWarning")
  protected boolean hasFuelAdBlueRatioWarning() {
    return selectedCategory != null && needsLiterFuel() && needsLiterAdBlue()
        && isInvalidFuelAdBlueRatio(selectedCategory.getMaxAdBlueFuelRatio());
  }

  private boolean isInvalidFuelAdBlueRatio(final Double maxRatio) {
    return maxRatio != null && maxRatio * standardSubSource.getLiterFuelPerYear() < standardSubSource.getLiterAdBluePerYear();
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return validation.descriptionV.invalid
        || validation.selectedCategoryV.invalid
        || needsLiterFuel() && validation.literFuelV.invalid
        || needsOperatingHours() && validation.operatingHoursV.invalid
        || needsLiterAdBlue() && validation.literAdBlueV.invalid;
  }

  private void updateSelectedCategory() {
    if (standardSubSource.getOffRoadMobileSourceCode() != null) {
      selectedCategory = applicationContext.getConfiguration().getSectorCategories()
          .determineOffRoadMobileSourceCategoryByCode(standardSubSource.getOffRoadMobileSourceCode());
      setUnusedInputValuesToZero();
    } else {
      selectedCategory = null;
    }
  }

  private void setUnusedInputValuesToZero() {
    if (!this.needsLiterFuel()) {
      setLiterFuel("0");
    }
    if (!this.needsOperatingHours()) {
      setOperatingHours("0");
    }
    if (!this.needsLiterAdBlue()) {
      setLiterAdBlue("0");
    }
  }
}
