/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.results.tab;

import java.util.List;
import java.util.function.Function;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component
public class TabViewComponent extends BasicVueComponent {
  @SuppressWarnings("rawtypes") @Prop(checkType = true) @JsProperty List options;
  @Prop Object selected;

  @Prop Function<Object, String> idFunc;

  @PropDefault("idFunc")
  Function<Object, String> idFuncDefault() {
    return obj -> String.valueOf("tab-" + String.valueOf(obj));
  }

  @JsMethod
  public void selectTab(final Object obj) {
    vue().$emit("select", obj);
  }
}
