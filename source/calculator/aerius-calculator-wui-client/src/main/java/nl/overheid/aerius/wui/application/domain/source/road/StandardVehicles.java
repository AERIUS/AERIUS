/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.road;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.wui.vue.ReactivityUtil;
import nl.overheid.aerius.wui.vue.SkipReactivityValidations;

/**
 * Client side implementation of StandardVehicles props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class StandardVehicles extends Vehicles {

  private int maximumSpeed;
  private Boolean strictEnforcement;
  private ValuesPerVehicleTypes valuesPerVehicleTypes;

  public static final @JsOverlay StandardVehicles create() {
    final StandardVehicles props = Js.uncheckedCast(JsPropertyMap.of());
    init(props);
    return props;
  }

  public static final @JsOverlay void init(final StandardVehicles props) {
    Vehicles.initVehicles(props, VehicleType.STANDARD);

    ReactivityUtil.ensureJsProperty(props, "strictEnforcement", props::setStrictEnforcement, false);
    ReactivityUtil.ensureJsProperty(props, "maximumSpeed", props::setMaximumSpeed, 0);
    ReactivityUtil.ensureJsPropertySupplied(props, "valuesPerVehicleTypes", props::setValuesPerVehicleTypes, ValuesPerVehicleTypes::create);
    ReactivityUtil.ensureInitialized(props::getValuesPerVehicleTypes, ValuesPerVehicleTypes::init);
  }

  public final @JsOverlay int getMaximumSpeed() {
    // Cast this native int because the server side of this object uses an Integer object and there on import the maximumSpeed might not exist.
    return Js.coerceToInt(maximumSpeed);
  }

  public final @JsOverlay void setMaximumSpeed(final int maximumSpeed) {
    this.maximumSpeed = maximumSpeed;
  }

  public final @JsOverlay boolean isStrictEnforcement() {
    return Boolean.TRUE.equals(strictEnforcement);
  }

  public final @JsOverlay void setStrictEnforcement(final Boolean strictEnforcement) {
    this.strictEnforcement = strictEnforcement;
  }

  @SkipReactivityValidations
  public final @JsOverlay ValuesPerVehicleType getValuesPerVehicleType(final String vehicleTypeCode) {
    return valuesPerVehicleTypes.getValuePerVehicleType(vehicleTypeCode);
  }

  public final @JsOverlay void setValuesPerVehicleType(final String vehicleTypeCode,
      final ValuesPerVehicleType valuesPerVehicleType) {
    this.valuesPerVehicleTypes.setValuePerVehicleType(vehicleTypeCode, valuesPerVehicleType);
  }

  public final @JsOverlay ValuesPerVehicleTypes getValuesPerVehicleTypes() {
    return valuesPerVehicleTypes;
  }

  public final @JsOverlay void setValuesPerVehicleTypes(final ValuesPerVehicleTypes valuesVehicleType) {
    this.valuesPerVehicleTypes = valuesVehicleType;
  }

}
