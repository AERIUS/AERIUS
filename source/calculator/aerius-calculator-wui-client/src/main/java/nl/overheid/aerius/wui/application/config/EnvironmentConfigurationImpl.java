/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.config;

import java.util.HashSet;
import java.util.Set;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import nl.aerius.search.domain.SearchCapability;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.config.ApplicationFlags;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;

@Singleton
public class EnvironmentConfigurationImpl implements EnvironmentConfiguration {
  private final EnvironmentConfigurationWrapper wrapper = new EnvironmentConfigurationWrapper();

  @Inject ApplicationFlags flags;
  @Inject ApplicationContext applicationContext;

  @Override
  public String getConnectBaseUrl() {
    return flags.isInternal() ? getConfigurationSetting(SharedConstantsEnum.CONNECT_INTERNAL_URL) : wrapper.connectBaseUrl;
  }

  @Override
  public String getGeoserverBaseUrl() {
    return flags.isInternal() ? getConfigurationSetting(SharedConstantsEnum.GEOSERVER_INTERNAL_URL) : wrapper.geoserverBaseUrl;
  }

  @Override
  public String getApplicationVersion() {
    return wrapper.applicationVersion;
  }

  @Override
  public boolean isProduction() {
    return wrapper.production;
  }

  @Override
  public String getSearchEndpoint() {
    return getConfigurationSetting(SharedConstantsEnum.SEARCH_ENDPOINT_URL);
  }

  @Override
  public Set<SearchCapability> getSearchCapabilities() {
    return new HashSet<>(applicationContext.getConfiguration().getSearchCapabilities());
  }

  @Override
  public String getSearchRegion() {
    return applicationContext.getConfiguration().getSearchRegion().name();
  }

  private String getConfigurationSetting(final SharedConstantsEnum sharedConstant) {
    return applicationContext.getConfiguration().getSettings().getSetting(sharedConstant);
  }

  private String getConfigurationSetting(final SharedConstantsEnum sharedConstant, final boolean soft) {
    return applicationContext.getConfiguration().getSettings().getSetting(sharedConstant, soft);
  }
}
