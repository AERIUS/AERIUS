/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.building;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.EventBus;

import elemental2.dom.FocusEvent;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.util.SchedulerUtil;
import nl.aerius.wui.vue.directives.VectorDirective;
import nl.overheid.aerius.wui.application.command.building.BuildingEditCancelCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingEditSaveCommand;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.common.FeatureHeading;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.modify.ModifyCancelSaveComponent;
import nl.overheid.aerius.wui.application.components.warning.PrivacyWarning;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.BuildingEditorContext;
import nl.overheid.aerius.wui.application.context.GenericValidationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.domain.ItemWithStringIdUtil;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.util.AccessibilityUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    FeatureHeading.class,
    LabeledInputComponent.class,
    CollapsiblePanel.class,
    ModifyCancelSaveComponent.class,
    BuildingLocationEditorComponent.class,
    PrivacyWarning.class,
}, directives = {
    VectorDirective.class,
})
public class BuildingView extends BasicVueComponent {
  @Prop EventBus eventBus;

  @Inject @Data ApplicationContext appContext;
  @Inject @Data ScenarioContext scenarioContext;
  @Inject @Data BuildingEditorContext context;
  @Inject @Data GenericValidationContext genericValidationContext;

  @Data boolean buildingErrors;
  @Data boolean buildingWarnings;

  @Data boolean confirmState;

  @Data boolean nameLabelFocused = false;

  @Computed
  public String getBuildingTitle() {
    final BuildingFeature building = getBuilding();
    final String id = ItemWithStringIdUtil.getStringId(building);

    final BuildingFeature match = scenarioContext.getActiveSituation().findBuildingById(id);

    return match == null ? i18n.buildingTitleNew() : i18n.buildingTitleEdit();
  }

  @JsMethod
  public void onLabelNameFocus() {
    nameLabelFocused = true;
  }

  @JsMethod
  public void onLabelNameFocusOut(final FocusEvent event) {
    AccessibilityUtil.doOnFocusLeave(event, () -> SchedulerUtil.delay(() -> nameLabelFocused = false, AccessibilityUtil.FOCUS_OUT_DELAY));
  }

  @Computed
  public BuildingFeature getBuilding() {
    return context.getBuilding();
  }

  @Computed("isBuildingAvailable")
  public boolean isBuildingAvailable() {
    return context.isEditing();
  }

  @Computed
  public String getLabel() {
    return getBuilding().getLabel();
  }

  @Computed
  public void setLabel(final String label) {
    getBuilding().setLabel(label);
  }

  @JsMethod
  public void cancel() {
    if (Window.confirm(M.messages().buildingMayCancel())) {
      eventBus.fireEvent(new BuildingEditCancelCommand());
    }
  }

  @JsMethod
  public void save() {
    eventBus.fireEvent(new BuildingEditSaveCommand(context.getBuilding().getGmlId()));
  }

  @JsMethod
  public void closeWarning() {
    setNotificationState(false);
  }

  @JsMethod
  public void closeError() {
    setNotificationState(false);
  }

  @JsMethod
  public void setNotificationState(final boolean confirmState) {
    this.confirmState = confirmState;
    genericValidationContext.setDisplayFormProblems(confirmState);
  }

  @Computed("hasInvalidName")
  public boolean hasInvalidName() {
    return getLabel() == null || getLabel().trim().isEmpty();
  }

  @Computed("hasValidationErrors")
  public boolean hasValidationErrors() {
    return buildingErrors
        || hasInvalidName();
  }

  @JsMethod
  public void onBuildingErrorsChange(final boolean buildingErrors) {
    this.buildingErrors = buildingErrors;
  }

  @JsMethod
  public void onBuildingWarningsChange(final boolean buildingWarnings) {
    this.buildingWarnings = buildingWarnings;
  }

  @Computed("hasValidationWarnings")
  public boolean hasValidationWarnings() {
    return buildingWarnings;
  }

}
