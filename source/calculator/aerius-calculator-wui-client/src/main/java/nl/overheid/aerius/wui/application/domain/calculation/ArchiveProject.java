/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.calculation;

import static jsinterop.annotations.JsPackage.GLOBAL;

import java.util.Locale;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.shared.domain.Substance;

@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class ArchiveProject {

  private String id;
  private String name;
  private String type;
  private String permitReference;
  private String planningReference;
  private @JsProperty JsPropertyMap<Double> netEmissions;
  private String aeriusVersion;
  private Object centroid;

  public final @JsOverlay String getId() {
    return id;
  }

  public final @JsOverlay String getName() {
    return name;
  }

  public final @JsOverlay String getType() {
    return type;
  }

  public final @JsOverlay String getPermitReference() {
    return permitReference;
  }

  public final @JsOverlay String getPlanningReference() {
    return planningReference;
  }

  public final @JsOverlay Double getNetEmissions(final Substance substance) {
    final String key = substance.getName().toUpperCase(Locale.ROOT);
    if (netEmissions == null || !netEmissions.has(key)) {
      return 0D;
    }
    return netEmissions.get(key);
  }

  public final @JsOverlay String getAeriusVersion() {
    return aeriusVersion;
  }

  public final @JsOverlay Object getCentroid() {
    return centroid;
  }
}
