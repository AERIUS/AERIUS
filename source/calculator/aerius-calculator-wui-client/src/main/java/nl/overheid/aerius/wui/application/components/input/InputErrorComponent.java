/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.input;

import java.util.Optional;
import java.util.function.Supplier;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;

import nl.aerius.vuelidate.Validations;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.wui.application.components.notice.NoticeRule;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    VerticalCollapse.class
})
public class InputErrorComponent extends BasicVueComponent {
  /**
   * Describe the behaviour of this component using a canonical notice rule.
   */
  @Prop NoticeRule noticeRule;
  /**
   * Validations object bound to the input. Called to check for errors.
   */
  @Prop Validations errorValidation;
  /**
   * Override whether to show errors
   */
  @Prop boolean showError;
  /**
   * Whether this notice is attached to an input field (on the top right).
   */
  @Prop boolean attached;
  /**
   * Whether to fully round this component.
   */
  @Prop boolean round;
  /**
   * Whether to round the bottom of this component.
   */
  @Prop boolean roundBottom;
  /**
   * Supplier that should return a customized error message in case of an
   * validation error.
   */
  @Prop Supplier<String> errorMessage;

  @PropDefault("showError")
  boolean showErrorDefault() {
    return false;
  }

  @PropDefault("attached")
  boolean attachedDefault() {
    return false;
  }

  @PropDefault("round")
  boolean roundDefault() {
    return false;
  }

  @PropDefault("roundBottom")
  boolean roundBottomDefault() {
    return false;
  }

  @Computed
  public NoticeRule getRule() {
    return NoticeRule.create(
        () -> shouldShowError(),
        () -> getErrorMessage());
  }

  private String getErrorMessage() {
    return Optional.ofNullable(noticeRule == null
        ? errorMessage
        : noticeRule.getNotice())
        .map(v -> v.get())
        .orElse(null);
  }

  /**
   * Rolls the 3 different ways of how an error can be shown (the errorValidation object, the showError/errorMessage mechanic, and the noticeRule
   * property) for this component into one
   */
  private boolean shouldShowError() {
    return noticeRule == null ? showError
        || errorValidation != null && errorValidation.error
        : noticeRule.getShowNotice().get();
  }
}
