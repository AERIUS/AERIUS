/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.print.own2000.results;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.geom.Point;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.command.result.FetchSummaryResultsCommand;
import nl.overheid.aerius.wui.application.context.PrintContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.result.CustomCalculationPointResult;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.application.event.FetchSummaryResultsEvent;
import nl.overheid.aerius.wui.application.event.RestoreCalculationJobContextEvent;
import nl.overheid.aerius.wui.application.event.RestoreScenarioContextEvent;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.application.ui.pages.results.custompoints.CustomPointsView;

@Component
public class ResultSummaryCalculationPointsTable extends CustomPointsView {
  protected static final ResultSummaryCalculationPointsTableEventBinder EVENT_BINDER = GWT.create(
      ResultSummaryCalculationPointsTableEventBinder.class);

  interface ResultSummaryCalculationPointsTableEventBinder extends EventBinder<ResultSummaryCalculationPointsTable> {}

  @Inject @Data PrintContext printOwN2000Context;

  @Data SituationResultsKey key = null;
  @Data boolean fetchComplete;

  @Override
  public void created() {
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Override
  protected CustomCalculationPointResult[] getCustomPointResults() {
    if (key == null || !fetchComplete) {
      return new CustomCalculationPointResult[0];
    }
    return getResultContext().getResultSummary(key).getCustomPointResults();
  }

  @Computed
  public boolean isRender() {
    return fetchComplete && getCustomPointResults().length > 0;
  }

  @JsMethod
  public String getCustomPointLocation(final int id) {
    return scenarioContext.getCalculationPoints().stream()
        .filter(p -> p.getCustomPointId() == id)
        .findAny()
        .map(cp -> MessageFormatter.formatPoint((Point) cp.getGeometry()))
        .orElse("");
  }

  @EventHandler
  public void onFetchSummaryResultsEvent(final FetchSummaryResultsEvent e) {
    if (e.getValue().equals(key)) {
      fetchComplete = true;
    }
  }

  @EventHandler
  public void onRestoreCalculationContextEvent(final RestoreCalculationJobContextEvent e) {
    // Fetch custom assessment points situation results for OwN2000 project calculation PDF
    final SituationResultsKey customAssessmentPointsResultsKey = new SituationResultsKey(
        SituationContext.handleFromSituation(scenarioContext.getActiveSituation()),
        printOwN2000Context.getScenarioResultType(),
        printOwN2000Context.getEmissionResultKey(),
        SummaryHexagonType.CUSTOM_CALCULATION_POINTS,
        printOwN2000Context.getOverlappingHexagonType(),
        printOwN2000Context.getProcurementPolicy());
    key = customAssessmentPointsResultsKey;
    eventBus.fireEvent(new FetchSummaryResultsCommand(customAssessmentPointsResultsKey));
  }

  @EventHandler
  public void onRestoreScenarioContextCompletedEvent(final RestoreScenarioContextEvent e) {
    updateCustomPointLabels();
  }
}
