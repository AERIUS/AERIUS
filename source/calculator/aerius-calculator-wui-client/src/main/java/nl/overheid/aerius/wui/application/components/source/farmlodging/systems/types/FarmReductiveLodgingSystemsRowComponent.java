/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.source.farmlodging.systems.types;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Emit;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategories;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingSystemDefinition;
import nl.overheid.aerius.shared.domain.sector.category.FarmReductiveLodgingSystemCategory;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.MinimalInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.input.suggest.SuggestListBoxComponent;
import nl.overheid.aerius.wui.application.components.input.suggest.SuggestListBoxData;
import nl.overheid.aerius.wui.application.components.input.suggest.SuggestUtil;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.components.tooltip.TooltipComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.farm.ReductiveLodgingSystem;

@Component(components = {
    MinimalInputComponent.class,
    ButtonIcon.class,
    SuggestListBoxComponent.class,
    SimplifiedListBoxComponent.class,
    TooltipComponent.class,
    ValidationBehaviour.class,
    InputErrorComponent.class,
})
public class FarmReductiveLodgingSystemsRowComponent extends ErrorWarningValidator implements IsVueComponent {
  @Prop Integer index;
  @Prop @JsProperty ReductiveLodgingSystem lodgingSystem;
  @Prop @JsProperty FarmLodgingCategory farmLodgingCategory;

  @Prop int idx;

  @Data @Inject ApplicationContext applicationContext;

  @Data FarmReductiveLodgingSystemCategory farmReductiveLodgingSystemCategory;

  @Data @JsProperty List<SuggestListBoxData> lodgingSystemOptions = new ArrayList<>();
  @Data @JsProperty List<FarmLodgingSystemDefinition> systemDefinitionOptions = new ArrayList<>();

  @PropDefault("idx")
  int idxDefault() {
    return 0;
  }

  @JsMethod
  public FarmLodgingCategories getCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getFarmLodgingCategories();
  }

  @Watch(value = "lodgingSystem", isImmediate = true)
  public void onLodgingSystemChange(final ReductiveLodgingSystem neww) {
    updateFarmLodgingSystemFromCode(neww.getLodgingSystemCode());
  }

  private void updateFarmLodgingSystemFromCode(final String code) {
    farmReductiveLodgingSystemCategory = getCategories().getFarmReductiveLodgingSystemCategories().stream()
        .filter(v -> v.getCode().equals(code))
        .findFirst().orElse(null);
  }

  @Watch(value = "farmLodgingCategory", isImmediate = true)
  public void onCategoryChange(final FarmLodgingCategory neww) {
    if (neww == null) {
      lodgingSystemOptions = new ArrayList<>();
    } else {
      lodgingSystemOptions = getCategories().getFarmReductiveLodgingSystemCategories().stream()
          .map(v -> new SuggestListBoxData(v.getCode(), v.getName(), v.getName() + " " + v.getDescription(),
              !neww.canStackReductiveLodgingSystemCategory(v)))
          .sorted(SuggestUtil.WARN)
          .collect(Collectors.toList());
    }
  }

  @Watch(value = "farmReductiveLodgingSystemCategory", isImmediate = true)
  void onFarmReductiveLodgingSystemCategoryChange(final FarmReductiveLodgingSystemCategory neww) {
    if (neww == null) {
      systemDefinitionOptions = new ArrayList<>();
    } else {
      systemDefinitionOptions = neww.getFarmLodgingSystemDefinitions();
    }

    // If there is only one option in the new set of system definitions, select it
    if (systemDefinitionOptions.size() == 1) {
      selectSystemDefinitionCode(systemDefinitionOptions.get(0));
    } else if (!systemDefinitionOptions.stream()
        .anyMatch(v -> v.getCode().equals(lodgingSystem.getSystemDefinitionCode()))) {
      // If the selected system definition code does not exist in the new list,
      // deselect it
      selectSystemDefinitionCode(null);
    }
  }

  @JsMethod
  @Emit(value = "delete")
  public void deleteRow() {}

  @JsMethod
  public void unselectLodgingSystem() {
    lodgingSystem.setLodgingSystemCode("");
    farmReductiveLodgingSystemCategory = null;
  }

  @JsMethod
  public void selectLodgingSystemCode(final String code) {
    lodgingSystem.setLodgingSystemCode(code);
    updateFarmLodgingSystemFromCode(code);
  }

  @Computed("farmReductiveLodgingSystemCategoryInvalid")
  public boolean farmReductiveLodgingSystemCategoryInvalid() {
    return lodgingSystem.getLodgingSystemCode() == null
        || lodgingSystem.getLodgingSystemCode().isEmpty();
  }

  @JsMethod
  public void selectSystemDefinitionCode(final FarmLodgingSystemDefinition value) {
    lodgingSystem.setSystemDefinitionCode(value == null ? null : value.getCode());
  }

  @Computed("systemDefinitionCodeError")
  public boolean systemDefinitionCodeError() {
    return displayProblems && systemDefinitionCodeInvalid();
  }

  @Computed("systemDefinitionCodeInvalid")
  public boolean systemDefinitionCodeInvalid() {
    // If there are no options, system definition is not required.
    return !systemDefinitionOptions.isEmpty() && (lodgingSystem.getSystemDefinitionCode() == null
        || lodgingSystem.getSystemDefinitionCode().isEmpty());
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return farmReductiveLodgingSystemCategoryInvalid()
        || systemDefinitionCodeInvalid();
  }
}
