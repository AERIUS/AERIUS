/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.i18n;

/**
 * Text specific for emission source pages.
 */
public interface BuildingMessages {
  String buildingTitleNew();
  String buildingTitleEdit();

  String buildingInfo();

  String buildingLabelName();
  String buildingLabelDefaultPrefix(String label);

  String buildingLocationPanelTitle();

  String buildingButtonEdit();
  String buildingButtonDeleteAll();
  String buildingButtonDeleteAllWarning();

  String buildingRemoveConfirm(String buildingId, String buildingLabel, String srcId, String srcLabel);
  String buildingRemoveMultipleConfirm(String buildingId, String buildingLabel, int amount);

  String buildingMarkerClusterLabel(int numberOfSources);

  String buildingLocationDrawExplain();
  String buildingLocationToolLabel();
  String buildingLocationPolygonLabel();
  String buildingLocationCircleLabel();
  String buildingLocationGeometryBAGLabel();
  String buildingLocationGeometryBAGPlaceholder();
  String buildingLocationGeometryWKTLabel();
  String buildingLocationGeometryCenterLabel();
  String buildingLocationEnvelopeInvalid();
  String buildingLocationDimensionLengthCorrectedMessage();
  String buildingLocationDimensionWidthCorrectedMessage();
  String buildingLocationDimensionHeightMinimumCorrectedMessage(double height);
  String buildingLocationDimensionHeightMaximumCorrectedMessage(double height);
  String buildingLocationRatioViolationWarning(String ratio);
  String buildingLocationDimensionsExceededWarning();
  String buildingLocationHeight();
  String buildingLocationDiameter();
  String buildingLocationDimensionsLength();
  String buildingLocationDimensionsWidth();
  String buildingLocationDimensionsHeight();
  String buildingLocationDimensionsOrientation();

  String buildingMayStop();
  String buildingMayCancel();

  String buildingWarningHeight(String height);

  String buildingSituationLimit(int limit);
}
