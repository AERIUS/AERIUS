/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.source.Source;

import nl.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceFeatureDeselectCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceFeatureSelectCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceFeatureToggleSelectCommand;
import nl.overheid.aerius.wui.application.command.source.TemporaryEmissionSourceAddCommand;
import nl.overheid.aerius.wui.application.command.source.TemporaryEmissionSourceClearCommand;
import nl.overheid.aerius.wui.application.context.EmissionSourceEditorContext;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.event.situation.SituationSwitchEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceAddedEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceDeletedEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceHighlightToggledEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceListChangeEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceModifyGeometryCommand;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceUpdatedEvent;
import nl.overheid.aerius.wui.application.geo.layers.base.BaseGeometryLayer;
import nl.overheid.aerius.wui.application.geo.layers.base.VectorFeaturesWrapperMap;

/**
 * Abstract layer to show the geometry of {@link EmissionSourceFeature} objects on the map.
 */
public abstract class SourceGeometryLayer extends BaseGeometryLayer<EmissionSourceFeature> {
  interface SourceGeometryLayerEventBinder extends EventBinder<SourceGeometryLayer> {}

  private final SourceGeometryLayerEventBinder EVENT_BINDER = GWT.create(SourceGeometryLayerEventBinder.class);

  @Inject private EmissionSourceEditorContext emissionSourceEditorContext;

  private final VectorFeaturesWrapperMap situationFeatures = new VectorFeaturesWrapperMap();

  public SourceGeometryLayer(final LayerInfo info, final EventBus eventBus, final int zIndex) {
    super(info, zIndex);
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  public void onTemporaryEmissionSourceAddCommand(final TemporaryEmissionSourceAddCommand c) {
    situationFeatures.addFeature(c.getSituationId(), c.getValue());
  }

  @EventHandler
  public void onTemporaryEmissionSourceClearCommand(final TemporaryEmissionSourceClearCommand c) {
    situationFeatures.removeFeature(c.getSituationId(), c.getValue());
  }

  @EventHandler
  public void onEmissionSourceAddedEvent(final EmissionSourceAddedEvent e) {
    situationFeatures.addFeature(e.getSituation().getId(), e.getValue());
  }

  @EventHandler
  public void onEmissionSourceUpdatedEvent(final EmissionSourceUpdatedEvent e) {
    situationFeatures.updateFeature(e.getSituation().getId(), e.getValue());
  }

  @EventHandler
  public void onEmissionSourceDeletedEvent(final EmissionSourceDeletedEvent e) {
    situationFeatures.removeFeature(e.getSituation().getId(), e.getValue());
  }

  @EventHandler
  public void onEmissionSourceListChangeEvent(final EmissionSourceListChangeEvent e) {
    setFeatures(situationFeatures.getVectorFeaturesWrapper(e.getSituationId()), e.getValue());
  }

  @EventHandler
  public void onEmissionSourceModifyGeometry(final EmissionSourceModifyGeometryCommand e) {
    setGeometryEditingFeatureId(e.getValue());
  }

  @Override
  @EventHandler(handles = {EmissionSourceFeatureSelectCommand.class, EmissionSourceFeatureDeselectCommand.class,
      EmissionSourceHighlightToggledEvent.class, EmissionSourceFeatureToggleSelectCommand.class, EmissionSourceUpdatedEvent.class})
  public Source refreshLayer() {
    return super.refreshLayer();
  }

  @EventHandler
  public void onSituationSwitch(final SituationSwitchEvent e) {
    setLayerVector(situationFeatures.getLayerVector(e.getValue()));
  }

  @Override
  protected EmissionSourceFeature getEditingFeatureFromContext() {
    return emissionSourceEditorContext.getSource();
  }
}
