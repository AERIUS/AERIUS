/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.base;

import java.util.List;
import java.util.function.Function;

import ol.Collection;
import ol.Feature;
import ol.OLFactory;
import ol.source.Vector;
import ol.source.VectorOptions;

import nl.overheid.aerius.wui.application.ApplicationLimits;
import nl.overheid.aerius.wui.application.geo.layers.OptimizedCollection;

/**
 * Wraps a Vector object and provides methods to manage the features on the vector.
 */
public class VectorFeaturesWrapper {

  private Vector vector;
  private Vector layerVector;
  private Function<Feature, Feature> mapFunction = f -> f;

  /**
   * Optional set a function that takes the feature and converts it to another feature that will be added to the Vector.
   *
   * @param mapFunction the function
   */
  public void setMapFunction(final Function<Feature, Feature> mapFunction) {
    this.mapFunction = mapFunction;
  }

  /**
   * Returns the Vector that is on the layer. Usefull when Vector containing the features is different than the Vector on the layer.
   * This is the case with a Cluster vector.
   *
   * @return The Vector that is on the layers
   */
  public Vector getLayerVector() {
    return layerVector;
  }

  /**
   * Set the Vector that is on the layer. Usefull when Vector on the layer is different from Vector containing the features.
   * This is the case with a Cluster vector.
   *
   * @param layerVector The Vector on the layer
   * @return the parameter passed
   */
  public Vector setLayerVector(final Vector layerVector) {
    this.layerVector = layerVector;
    return this.layerVector;
  }

  /**
   * Clears the features on the Vector.
   */
  public void clear() {
    vector.clear(true);
  }

  /**
   * Returns the Vector containing the features. Not necessarily the vector on the layer.
   * Use {@link #getLayerVector()} to the the Vector on the layer.
   *
   * @return The Vector containing the features
   */
  public Vector getVector() {
    return vector;
  }

  /**
   * Add a Feature to the Vector. Applies the mapping function before adding and only adds if the number of features doesn't exceed the
   * max allowed features.
   *
   * @param feature feature to add
   */
  public void addFeature(final Feature feature) {
    if (feature == null || vector == null || vector.getFeatures().length > ApplicationLimits.SOURCE_LAYER_DRAW_LIMIT) {
      return;
    }
    final Feature mappedFeature = mapFunction.apply(feature);

    if (mappedFeature != null && !vector.getFeaturesCollection().contains(mappedFeature)) {
      vector.addFeature(mappedFeature);
    }
  }

  /**
   * Add a Feature to the Vector. Applies the mapping function before adding and only adds up to the number of features allowed
   *
   * @param feature feature to add
   */
  public void addFeatures(final List<? extends Feature> features) {
    if (features == null || vector == null) {
      return;
    }

    final Feature[] mappedFeatures = features.stream()
        .map(v -> mapFunction.apply(v))
        .limit((long) ApplicationLimits.SOURCE_LAYER_DRAW_LIMIT - vector.getFeatures().length)
        .toArray(size -> new Feature[size]);

    if (mappedFeatures != null) {
      vector.addFeatures(mappedFeatures);
    }
  }

  /**
   * Sets the features on the map. This method initializes the Vector containing the features. This method should always be called before
   * the Vector in the class can be used.
   *
   * @param features Initial features to set on the map.
   */
  public <F extends Feature> void setFeatures(final List<F> features) {
    final Collection<Feature> olFeatures = new OptimizedCollection<>(
        features.stream()
            .map(mapFunction)
            .filter(f -> f != null)
            // Clustering is quite efficient, so if sources are clustered there is no real
            // reason to apply a limit, however when sources are unclustered there's a
            // noticeable slowdown starting from a couple thousand features.
            // Therefor just always apply a limit.
            .limit(ApplicationLimits.SOURCE_LAYER_DRAW_LIMIT)
            .toArray(size -> new Feature[size]));
    final VectorOptions vectorSourceOptions = OLFactory.createOptions();

    vectorSourceOptions.setFeatures(olFeatures);
    vector = new Vector(vectorSourceOptions);
    layerVector = vector;
  }

  /**
   * Removes the feature from the Vector.
   *
   * @param feature feature to remove
   */
  public void removeFeature(final Feature feature) {
    if (feature != null) {
      removeFeatureById(feature.getId());
    }
  }

  /**
   * Removes the feature by id from the Vector.
   *
   * @param id id of the feature to remove
   */
  public void removeFeatureById(final String id) {
    final Feature feature = vector == null ? null : vector.getFeatureById(id);

    if (feature != null) {
      vector.removeFeature(feature);
    }
  }

  /**
   * Updates the feature by removing the feature and readding it to the Vector.
   *
   * @param feature feature to update
   */
  public void updateFeature(final Feature feature) {
    removeFeature(feature);
    addFeature(feature);
  }
}
