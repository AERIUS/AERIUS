/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.geo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.search.wui.event.SearchSuggestionSelectionEvent;
import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.wui.application.command.SearchPopupToggleCommand;
import nl.overheid.aerius.wui.application.command.geo.InfoPopupHiddenCommand;
import nl.overheid.aerius.wui.application.command.geo.InfoPopupToggleCommand;
import nl.overheid.aerius.wui.application.command.geo.InfoPopupVisibleCommand;
import nl.overheid.aerius.wui.application.command.geo.LabelsPopupHiddenCommand;
import nl.overheid.aerius.wui.application.command.geo.LabelsPopupToggleCommand;
import nl.overheid.aerius.wui.application.command.geo.LabelsPopupVisibleCommand;
import nl.overheid.aerius.wui.application.command.geo.LayerPopupHiddenCommand;
import nl.overheid.aerius.wui.application.command.geo.LayerPopupToggleCommand;
import nl.overheid.aerius.wui.application.command.geo.LayerPopupVisibleCommand;
import nl.overheid.aerius.wui.application.command.geo.LoginPopupHiddenCommand;
import nl.overheid.aerius.wui.application.command.geo.LoginPopupToggleCommand;
import nl.overheid.aerius.wui.application.command.geo.LoginPopupVisibleCommand;
import nl.overheid.aerius.wui.application.command.result.ProcessContributionsLegendPopupHiddenCommand;
import nl.overheid.aerius.wui.application.command.result.ProcessContributionsLegendPopupToggleCommand;
import nl.overheid.aerius.wui.application.command.result.ProcessContributionsLegendPopupVisibleCommand;
import nl.overheid.aerius.wui.application.context.InfoPanelContext;
import nl.overheid.aerius.wui.application.context.LabelsPanelContext;
import nl.overheid.aerius.wui.application.context.LayerPanelContext;
import nl.overheid.aerius.wui.application.context.LoginPanelContext;
import nl.overheid.aerius.wui.application.context.PanelContext;
import nl.overheid.aerius.wui.application.context.ProcessContributionsLegendPanelContext;
import nl.overheid.aerius.wui.application.context.SearchPanelContext;
import nl.overheid.aerius.wui.application.context.SimplePanelContext;

/*
 * Daemon for the InfoPanel and LayerPanel
 */
public class PanelDaemon extends BasicEventComponent implements Daemon {
  private static final PanelDaemonEventBinder EVENT_BINDER = GWT.create(PanelDaemonEventBinder.class);

  interface PanelDaemonEventBinder extends EventBinder<PanelDaemon> {}

  @Inject private InfoPanelContext infoContext;
  @Inject private LoginPanelContext loginContext;
  @Inject private LabelsPanelContext labelsContext;
  @Inject private LayerPanelContext layerContext;
  @Inject private ProcessContributionsLegendPanelContext processContributionsLegendContext;
  @Inject private SearchPanelContext searchContext;

  private PanelContext activePanelContext;

  private final List<PanelContext> panelContexts = new ArrayList<>();

  /**
   * A collection of contexts not to consider in the activation logic
   */
  private final Set<PanelContext> blacklistedContexts = new HashSet<>();

  @Override
  public void init() {
    panelContexts.add(infoContext);
    panelContexts.add(loginContext);
    panelContexts.add(labelsContext);
    panelContexts.add(layerContext);
    panelContexts.add(processContributionsLegendContext);
    panelContexts.add(searchContext);
  }

  @EventHandler
  public void onPanelDock(final PanelDockCommand c) {
    blacklistedContexts.remove(c.getValue());
    setActive(c.getValue());
  }

  @EventHandler
  public void onPanelUndock(final PanelUndockCommand c) {
    blacklistedContexts.add(c.getValue());
    activateRemainingVisible();
  }

  @EventHandler
  public void onPanelActive(final PanelActivateCommand c) {
    if (blacklistedContexts.contains(c.getValue())) {
      return;
    }

    setActive(c.getValue());
  }

  @EventHandler
  public void onLoginPopupVisibleCommand(final LoginPopupVisibleCommand c) {
    setActiveVisible(loginContext);
  }

  @EventHandler
  public void onLoginPopupHiddenCommand(final LoginPopupHiddenCommand c) {
    setInactiveInvisible(loginContext);
  }

  @EventHandler
  public void onLoginPopupToggleCommand(final LoginPopupToggleCommand c) {
    c.resolve(toggleActiveVisible(loginContext));
  }

  @EventHandler
  public void onInfoPopupVisibleCommand(final InfoPopupVisibleCommand c) {
    setActiveVisible(infoContext);
  }

  @EventHandler
  public void onInfoPopupHiddenCommand(final InfoPopupHiddenCommand c) {
    setInactiveInvisible(infoContext);
  }

  @EventHandler
  public void onInfoPopupToggleCommand(final InfoPopupToggleCommand c) {
    c.resolve(toggleActiveVisible(infoContext));
  }

  @EventHandler
  public void onLabelsPopupVisibleCommand(final LabelsPopupVisibleCommand c) {
    setActiveVisible(labelsContext);
  }

  @EventHandler
  public void onLabelsPopupHiddenCommand(final LabelsPopupHiddenCommand c) {
    setInactiveInvisible(labelsContext);
  }

  @EventHandler
  public void onLayerPopupVisibleCommand(final LayerPopupVisibleCommand c) {
    setActiveVisible(layerContext);
  }

  @EventHandler
  public void onLayerPopupHiddenCommand(final LayerPopupHiddenCommand c) {
    setInactiveInvisible(layerContext);
  }

  @EventHandler
  public void onProcessContributionsLegendPopupVisibleCommand(final ProcessContributionsLegendPopupVisibleCommand c) {
    setActiveVisible(processContributionsLegendContext);
  }

  @EventHandler
  public void onProcessContributionsLegendPopupHiddenCommand(final ProcessContributionsLegendPopupHiddenCommand c) {
    setInactiveInvisible(processContributionsLegendContext);
  }

  @EventHandler
  public void onSearchPopupToggleCommand(final SearchPopupToggleCommand c) {
    c.resolve(toggleActiveVisible(searchContext));
  }

  @EventHandler
  public void onSearchSuggestionSelectionEvent(final SearchSuggestionSelectionEvent e) {
    setInactiveInvisible(searchContext);
  }

  @EventHandler
  public void onLayerPopupToggleCommand(final LayerPopupToggleCommand c) {
    c.resolve(toggleActiveVisible(layerContext));
  }

  @EventHandler
  public void onProcessContributionsLegendPopupToggleCommand(final ProcessContributionsLegendPopupToggleCommand c) {
    c.resolve(toggleActiveVisible(processContributionsLegendContext));
  }

  @EventHandler
  public void onLabelsPopupToggleCommand(final LabelsPopupToggleCommand c) {
    c.resolve(toggleActiveVisible(labelsContext));
  }

  private boolean toggleActiveVisible(final SimplePanelContext context) {
    final boolean attached = !blacklistedContexts.contains(context);
    final boolean result = context.togglePanelShowing(attached);
    if (attached) {
      if (context.isActive()) {
        setActive(context);
      } else {
        activateRemainingVisible();
      }
    }

    return result;
  }

  private void setActiveVisible(final PanelContext context) {
    context.setPanelShowing(true);
    setActive(context);
  }

  private void setActive(final PanelContext context) {
    deactivateExcept(context);

    activePanelContext = context;
    activePanelContext.setPanelActive(true);
  }

  private void deactivateExcept(final PanelContext context) {
    panelContexts.stream()
        .filter(v -> v != context)
        .filter(PanelContext::isActive)
        .filter(v -> !blacklistedContexts.contains(v))
        .forEach(v -> v.setPanelActive(false));
  }

  private void setInactiveInvisible(final PanelContext context) {
    context.setPanelShowing(false);
    deactivateExcept(context);
    activateRemainingVisible();
  }

  private void activateRemainingVisible() {
    panelContexts.stream()
        .filter(PanelContext::isShowing)
        .filter(v -> !blacklistedContexts.contains(v))
        .findFirst().ifPresent(this::setActive);
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
