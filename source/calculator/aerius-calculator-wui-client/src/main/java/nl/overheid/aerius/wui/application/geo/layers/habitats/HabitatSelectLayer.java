/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.habitats;

import java.util.stream.Collectors;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;
import com.google.web.bindery.event.shared.binder.GenericEvent;

import ol.layer.Layer;

import nl.aerius.geo.domain.IsLayer;
import nl.overheid.aerius.wui.application.event.ClearAssessmentAreaEvent;
import nl.overheid.aerius.wui.application.event.HabitatTypeSelectToggleEvent;
import nl.overheid.aerius.wui.application.event.ResultsViewSelectionEvent;
import nl.overheid.aerius.wui.application.event.SelectAssessmentAreaEvent;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;

/**
 * Provides the habitat selection layer
 */
public class HabitatSelectLayer extends HabitatHighlightLayer implements IsLayer<Layer> {
  private static final HabitatHoverLayerEventBinder EVENT_BINDER = GWT.create(HabitatHoverLayerEventBinder.class);

  interface HabitatHoverLayerEventBinder extends EventBinder<HabitatSelectLayer> {}

  @Inject
  public HabitatSelectLayer(final EventBus eventBus, final EnvironmentConfiguration configuration) {
    super(HabitatHighlightLayer.STYLE_SELECT, configuration);

    setEventBus(eventBus);
  }

  @EventHandler(handles = {ClearAssessmentAreaEvent.class, SelectAssessmentAreaEvent.class, HabitatTypeSelectToggleEvent.class,
      ResultsViewSelectionEvent.class})
  public void onUpdateLayer(final GenericEvent e) {
    updateLayer();
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }

  @Override
  protected String getHabitatTypes() {
    return selectionContext.getSelectedHabitatTypes().stream().collect(Collectors.joining("\\,"));
  }
}
