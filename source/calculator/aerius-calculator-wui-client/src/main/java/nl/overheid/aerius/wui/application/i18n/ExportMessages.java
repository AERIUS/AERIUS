/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.i18n;

import com.google.gwt.i18n.client.Messages.Select;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.export.ExportAppendix;
import nl.overheid.aerius.wui.application.context.ExportContext.ClientJobType;

/**
 * Text specific for export page.
 */
public interface ExportMessages {
  String exportAsADMS();
  String exportAsOPS();
  String exportButton();
  String exportEmailLabel();
  String exportErrorNoEmissions();
  String exportGMLWithMetaData();

  String exportPDFAdditionalInfoLabel();
  String exportPDFAppendicesLabel();
  String exportPDFAddAppendixName(@Select ExportAppendix appendix);
  String exportPDFProjectCalculationEditJob();
  String exportTitle();
  String exportToggleType(@Select Theme theme, @Select ClientJobType type);
  String exportAlreadyRunningWarning();
  String exportPrivacyStatementText();
  String exportProcessContributionsLegend();
  String exportProcessContributionsLegendTitle();
  String exportPrivacyStatementLabel();
  String exportTypeCalculation();
  String exportTypeDescriptionSources();
  String exportTypeDescriptionCalculation();
  String exportTypeDescriptionReport(@Select Theme theme);
  String exportJobSelectionName(String name, String jobType);
  String exportCalculationJobSelectionRequired();
  String exportCalculationJobsEmpty();
  String exportCalculationJobsCreateNew();
  String exportCalculationJobsEditJob();
  String exportMetSiteSelectionInvalid();
  String exportMetYearSelectionInvalid();
  String exportCalculatePermitAreaInvalid();
  String exportCalculateProjectCategoryInvalid();
  String exportReportType(@Select CalculationJobType jobType);
  String exportSituationsMissing(String types);
  String exportTooFewSituationsSelected();
  String exportCalculationJobIncompatibleMethod(String reportType, String desiredMethod, String existingMethod);
  String exportCalculationJobIncompatibleWithType(String reportType);
  String exportHistoryName(@Select ClientJobType clientType, String name);
  String exportHistorySourcesName(int num);
  String exportHistoryRemoveConfirm();
  String exportHistoryRemoveItem();
  String exportHistoryDownloadItem();
  String exportHistoryDisplayLabel(String name);

  String scenarioMetaDataCity();
  String scenarioMetaDataCorporation();
  String scenarioMetaDataDescription();
  String scenarioMetaDataLocation();
  String scenarioMetaDataPostcode();
  String scenarioMetaDataProjectName();
  String scenarioMetaDataStreetAddress();

  String exportAgreeMetaDataInclusionText();
  String exportAgreeMetaDataInclusionWarning();
  String exportSituationSelection();

  String exportOwnExportLabel();
}
