/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui;

import com.google.inject.Inject;

public class ApplicationImpl extends Application {
  @Inject private ApplicationRoot appRoot;

  @Override
  public void create(final Runnable finish) {
    ApplicationGinjector.INSTANCE.inject(this);

    onFinishedLoading(finish);
  }

  /**
   * Initializes the application activity managers, user interface, and starts the application by handling the current history token.
   * @param finish
   */
  private void onFinishedLoading(final Runnable finish) {
    try {
      appRoot.startUp(finish);
    } catch (final Exception e) {
      appRoot.hideDisplayOnError(e, finish);
    }
  }
}
