/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.print.own2000.results;

import static com.axellience.vuegwt.core.client.tools.JsUtils.e;
import static com.axellience.vuegwt.core.client.tools.JsUtils.map;

import java.util.ArrayList;
import java.util.List;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.wui.application.components.legend.ColorLabelsLegendComponent;
import nl.overheid.aerius.wui.application.geo.layers.LegendPair;

@Component
public class ColorLabelsLegendPrintComponent extends ColorLabelsLegendComponent {

  @Prop @JsProperty List<LegendPair> markerLegend;

  @PropDefault("markerLegend")
  public List<LegendPair> markerLegendDefault() {
    return new ArrayList<>();
  }

  @JsMethod
  public JsPropertyMap<String> getBackgroundColorStyle(final int index) {
    return map(e("background-color", legend.getColors()[index]));
  }
}
