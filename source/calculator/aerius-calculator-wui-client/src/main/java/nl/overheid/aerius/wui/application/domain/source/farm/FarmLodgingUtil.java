/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.farm;

import java.util.List;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.FarmAdditionalLodgingSystemCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategories;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingFodderMeasureCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmReductiveLodgingSystemCategory;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;

/**
 * Util class for calculating emissions of Farm Lodging objects.
 */
public final class FarmLodgingUtil {

  private FarmLodgingUtil() {
    // Util class
  }

  /**
   * Gets the emission of a {@link FarmLodging} object and returns the calculated emission
   *
   * @param farmLodging object to get emission for.
   */
  public static double getEmission(final FarmLodgingCategories categories, final FarmLodging farmLodging) {
    if (farmLodging.getFarmLodgingType() == FarmLodgingType.STANDARD) {
      return FarmLodgingStandardUtil.getEmissions(categories, (StandardFarmLodging) farmLodging, Substance.NH3);
    } else {
      final double emission = getEmission((CustomFarmLodging) farmLodging);

      farmLodging.setEmission(Substance.NH3, emission);
      return emission;
    }
  }

  /**
   * Calculates the emission of a {@link CustomFarmLodging} object.
   *
   * @param customFarmLodging object to calculate emission for
   * @return emission
   */
  private static double getEmission(final CustomFarmLodging customFarmLodging) {
    final int numberOfDaysFactor = customFarmLodging.getFarmEmissionFactorType() == FarmEmissionFactorType.PER_ANIMAL_PER_DAY ?
        customFarmLodging.getNumberOfDays() : 1;
    return customFarmLodging.getEmissionFactor(Substance.NH3) * customFarmLodging.getNumberOfAnimals() * numberOfDaysFactor;
  }

  public static boolean hasStacking(final StandardFarmLodging standardFarmLodging) {
    return standardFarmLodging.getAdditionalLodgingSystems().length > 0
        || standardFarmLodging.getReductiveLodgingSystems().length > 0
        || standardFarmLodging.getFodderMeasures().length > 0;
  }

  public static boolean hasIncompatibleCombinations(final StandardFarmLodging standardFarmLodging, final FarmLodgingCategory farmLodgingCategory,
      final FarmLodgingCategories farmLodgingCategories) {
    for(final AdditionalLodgingSystem row : standardFarmLodging.getAdditionalLodgingSystems().asList()) {
      final List<FarmAdditionalLodgingSystemCategory> listAdditinal = farmLodgingCategories.getFarmAdditionalLodgingSystemCategories();
      final FarmAdditionalLodgingSystemCategory foundAdditional = listAdditinal.stream()
          .filter(additional -> additional.getCode().equals(row.getLodgingSystemCode())).findAny().orElse(null);

      if (!farmLodgingCategory.canStackAdditionalLodgingSystemCategory(foundAdditional)) {
        return true;
      }
    }
    for(final ReductiveLodgingSystem row : standardFarmLodging.getReductiveLodgingSystems().asList()) {
      final List<FarmReductiveLodgingSystemCategory> listReductive = farmLodgingCategories.getFarmReductiveLodgingSystemCategories();
      final FarmReductiveLodgingSystemCategory foundreductive = listReductive.stream()
          .filter(reductive -> reductive.getCode().equals(row.getLodgingSystemCode())).findAny().orElse(null);
      if (foundreductive != null && !farmLodgingCategory.canStackReductiveLodgingSystemCategory(foundreductive)) {
        return true;
      }
    }
    for(final LodgingFodderMeasure row : standardFarmLodging.getFodderMeasures().asList()) {
      final List<FarmLodgingFodderMeasureCategory> listFodder = farmLodgingCategories.getFarmLodgingFodderMeasureCategories();
      final FarmLodgingFodderMeasureCategory foundFodder = listFodder.stream()
          .filter(fodder -> fodder.getCode().equals(row.getFodderMeasureCode()))
          .findAny().orElse(null);
      if (foundFodder != null && !farmLodgingCategory.canStackFodderMeasureCategory(foundFodder)) {
        return true;
      }
    }
    return false;
  }
}
