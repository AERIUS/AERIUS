/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.util;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Least-recently-used cache with a given maximum size.
 * This cache is not thread-safe, and should without modifications only be used in the web UI (which is, ignoring web workers, single threaded).
 *
 * @param <K> key type
 * @param <V> value type
 */
public class LruCache<K, V> extends LinkedHashMap<K, V> {

  /**
   * Defines the fraction between the initial size and the maximum size of the internal HashMap
   * A lower value makes the storage more efficient if it is used to the maximum size anyway,
   * a higher value makes sense if the cache has a high maxSize and is not always maximally used.
   */
  private static final int INITIAL_SIZE_FACTOR = 4;

  /**
   * Default load factor from the JVM HashMap implementation
   */
  private static final float DEFAULT_LOAD_FACTOR = .75f;

  /**
   * Should be set to true to use the access order instead of the insertion order for eviction
   */
  private static final boolean USE_ACCESS_ORDER = true;

  /**
   * Maximum size of the cache after which it will start evicting old entries
   */
  private final int maxSize;

  public LruCache(final int maxSize) {
    super(maxSize / INITIAL_SIZE_FACTOR, DEFAULT_LOAD_FACTOR, USE_ACCESS_ORDER);
    this.maxSize = maxSize;
  }

  @Override
  protected boolean removeEldestEntry(final Map.Entry<K, V> eldest) {
    return size() > maxSize;
  }
}
