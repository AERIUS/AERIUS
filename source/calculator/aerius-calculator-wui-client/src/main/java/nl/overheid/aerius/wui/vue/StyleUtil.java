/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.vue;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import elemental2.dom.Element;
import elemental2.dom.Node;

import jsinterop.base.Js;

public final class StyleUtil {
  private StyleUtil() {}

  public static void propagateDataAttributes(final Element el) {
    final List<Node> dataAttributes = Stream.of(el.getAttributeNames().asArray(new String[0]))
        .filter(v -> v.startsWith("data-"))
        .map(v -> (Node) Js.cast(el.attributes.get(v)))
        .collect(Collectors.toList());
    el.childNodes.asList().stream()
        .filter(node -> node.nodeType == (int) Node.ELEMENT_NODE)
        .forEach(node -> {
          dataAttributes.forEach(attr -> node.attributes.setNamedItem(attr.cloneNode(true)));
          StyleUtil.propagateDataAttributes(node);
        });
  }

  private static void propagateDataAttributes(final Node node) {
    if (node instanceof Element) {
      propagateDataAttributes((Element) node);
    }
  }
}
