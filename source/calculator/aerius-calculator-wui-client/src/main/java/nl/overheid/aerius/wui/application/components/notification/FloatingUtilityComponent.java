/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.notification;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.client.component.hooks.HasBeforeDestroy;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.wui.application.command.geo.LoginPopupResetCommand;
import nl.overheid.aerius.wui.application.command.geo.LoginPopupToggleCommand;
import nl.overheid.aerius.wui.application.components.map.LoginPanelDockChangeCommand;
import nl.overheid.aerius.wui.application.components.map.LoginPanelDockRemoveCommand;
import nl.overheid.aerius.wui.application.components.map.NotificationButtonChangeCommand;
import nl.overheid.aerius.wui.application.components.map.StatefulIconButton;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.LoginPanelContext;
import nl.overheid.aerius.wui.application.daemon.misc.PersistablePreferencesContext;
import nl.overheid.aerius.wui.application.daemon.misc.PreferencesDaemon;
import nl.overheid.aerius.wui.application.daemon.misc.UserSettingChangeEvent;
import nl.overheid.aerius.wui.application.ui.main.ContentResizeEvent;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    NotificationButtonComponent.class,
    StatefulIconButton.class,
})
public class FloatingUtilityComponent extends BasicVueComponent implements HasMounted, HasBeforeDestroy {
  private static final FloatingUtilityComponentEventBinder EVENT_BINDER = GWT.create(FloatingUtilityComponentEventBinder.class);

  interface FloatingUtilityComponentEventBinder extends EventBinder<FloatingUtilityComponent> {}

  @Prop EventBus eventBus;

  @Inject @Data ApplicationContext appContext;
  @Inject @Data PersistablePreferencesContext preferencesContext;
  @Inject @Data LoginPanelContext loginContext;

  @Ref NotificationButtonComponent notificationButton;
  @Ref StatefulIconButton loginButton;

  private HandlerRegistration resizeHandler;
  private HandlerRegistration eventHandlers;

  @Prop boolean floating;

  @Computed("isLoginEnabled")
  public boolean isLoginEnabled() {
    return preferencesContext.isLoginEnabled() && appContext.getConfiguration().isDisplayLoginSettings();
  }

  @EventHandler
  public void onUserSettingChangeEvent(final UserSettingChangeEvent c) {
    if (!PreferencesDaemon.ENABLE_LOGIN.equals(c.getKey())) {
      return;
    }

    updateDock();
  }

  @EventHandler
  public void onContentResizeEvent(final ContentResizeEvent e) {
    updateDock();
  }

  @Override
  public void mounted() {
    updateDock();
    resizeHandler = Window.addResizeHandler(v -> updateDock());
    eventHandlers = EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  private void updateDock() {
    eventBus.fireEvent(new LoginPanelDockChangeCommand(loginButton.vue().$el()));
    if (notificationButton != null) {
      eventBus.fireEvent(new NotificationButtonChangeCommand(notificationButton.vue().$el()));
    }
  }

  @Override
  public void beforeDestroy() {
    eventBus.fireEvent(new LoginPanelDockRemoveCommand(loginButton.vue().$el()));
    resizeHandler.removeHandler();
    eventHandlers.removeHandler();
  }

  @PropDefault("floating")
  boolean floatingDefault() {
    return true;
  }

  @JsMethod
  public void onLoginPanelClick() {
    eventBus.fireEvent(new LoginPopupToggleCommand());
  }

  @JsMethod
  public void onLoginPanelReset() {
    eventBus.fireEvent(new LoginPopupResetCommand());
  }
}
