/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.DoubleSupplier;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.Feature;
import ol.style.Style;

import nl.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.wui.application.command.geo.ChangeLabelDisplayModeCommand;
import nl.overheid.aerius.wui.application.command.source.SourceMarkersHighlightClearCommand;
import nl.overheid.aerius.wui.application.command.source.SourceMarkersHighlightCommand;
import nl.overheid.aerius.wui.application.context.MapConfigurationContext;
import nl.overheid.aerius.wui.application.domain.ItemWithStringIdUtil;
import nl.overheid.aerius.wui.application.domain.geo.LabelDisplayMode;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.event.situation.SituationSwitchEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceAddedEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceDeletedEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceListChangeEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceModifyGeometryCommand;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceUpdatedEvent;
import nl.overheid.aerius.wui.application.geo.layers.LabelFeature;
import nl.overheid.aerius.wui.application.geo.layers.LabelStyle;
import nl.overheid.aerius.wui.application.geo.layers.base.BaseClusteredMarkerLayer;
import nl.overheid.aerius.wui.application.geo.layers.base.ClusteredLabelStyleCreator;
import nl.overheid.aerius.wui.application.geo.layers.base.VectorFeaturesWrapperMap;
import nl.overheid.aerius.wui.application.i18n.M;

/**
 * Layer to show the markers of {@link EmissionSourceFeature} objects on the map.
 */
public class SourceMarkerLayer extends BaseClusteredMarkerLayer<EmissionSourceFeature> {

  interface SourceMarkerLayerEventBinder extends EventBinder<SourceMarkerLayer> {
  }

  private final SourceMarkerLayerEventBinder EVENT_BINDER = GWT.create(SourceMarkerLayerEventBinder.class);

  private final VectorFeaturesWrapperMap situationFeatures = new VectorFeaturesWrapperMap();

  private final MapConfigurationContext context;
  private final SourceLabelStyleCreator outlineStyleCreator;
  private final Set<String> highlightedSourceIds = new HashSet<>();

  private boolean highlight;

  @Inject
  public SourceMarkerLayer(@Assisted final LayerInfo info, final EventBus eventBus, @Assisted final int zIndex,
      final MapConfigurationContext mapConfigurationContext) {
    super(info, zIndex, new SourceLabelStyleCreator(mapConfigurationContext::getIconScale, false));
    this.context = mapConfigurationContext;
    this.outlineStyleCreator = new SourceLabelStyleCreator(mapConfigurationContext::getIconScale, true);
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  public void onEmissionSourceAddedEvent(final EmissionSourceAddedEvent e) {
    if (context.isMarked(e.getValue())) {
      situationFeatures.addFeature(e.getSituation().getId(), e.getValue());
    }
  }

  @EventHandler
  public void onEmissionSourceDeletedEvent(final EmissionSourceDeletedEvent e) {
    situationFeatures.removeFeature(e.getSituation().getId(), e.getValue());
  }

  @EventHandler
  public void onEmissionSourceUpdatedEvent(final EmissionSourceUpdatedEvent e) {
    final String situationId = e.getSituation().getId();
    if (context.isMarked(e.getValue())) {
      situationFeatures.updateFeature(situationId, e.getValue());
    } else {
      situationFeatures.removeFeature(situationId, e.getValue());
    }
  }

  @EventHandler
  public void onEmissionSourceListChangeEvent(final EmissionSourceListChangeEvent e) {
    final List<EmissionSourceFeature> emissionSources = Optional.ofNullable(e.getValue()).orElseGet(ArrayList::new).stream()
        .filter(context::isMarked)
        .collect(Collectors.toList());
    setFeatures(situationFeatures.getVectorFeaturesWrapper(e.getSituationId()), emissionSources);
  }

  @EventHandler
  public void onEmissionSourceModifyGeometry(final EmissionSourceModifyGeometryCommand e) {
    getLabelStyleProvider().setEditableFeatureId(e.getValue());
    refreshLayer();
  }

  @EventHandler
  public void onSituationSwitch(final SituationSwitchEvent e) {
    setLayerVector(situationFeatures.getLayerVector(e.getValue()));
  }

  @Override
  @EventHandler
  public void onChangeLabelDisplayModeCommand(final ChangeLabelDisplayModeCommand e) {
    outlineStyleCreator.setShowNameLabels(e.getValue() == LabelDisplayMode.NAME);
    super.onChangeLabelDisplayModeCommand(e);
  }

  @EventHandler
  public void onSourceMarkersHighlightCommand(final SourceMarkersHighlightCommand e) {
    highlight = true;
    highlightedSourceIds.clear();
    highlightedSourceIds.addAll(e.getValue());
    refreshLayer();
  }

  @EventHandler
  public void onSourceMarkersHighlightClearCommand(final SourceMarkersHighlightClearCommand e) {
    highlight = false;
    highlightedSourceIds.clear();
    refreshLayer();
  }

  @Override
  protected Style[] createStyle(final EmissionSourceFeature feature, final double resolution) {
    boolean useOutline = false;
    if (highlight && feature.get("features") != null) {
      final LabelFeature[] features = feature.get("features");
      for (final LabelFeature f : features) {
        if (highlightedSourceIds.contains(((EmissionSourceFeature) f.getReferenceFeature()).getGmlId())) {
          useOutline = true;
          break;
        }
      }
    }
    return useOutline ? outlineStyleCreator.createStyle(feature, resolution) : super.createStyle(feature, resolution);
  }

  /**
   * Creator to create {@link EmissionSourceFeature} clustered markers to show on the map.
   */
  private static class SourceLabelStyleCreator extends ClusteredLabelStyleCreator<EmissionSourceFeature> {

    public SourceLabelStyleCreator(final DoubleSupplier iconScale, final boolean outline) {
      super(iconScale, outline, LabelStyle.EMISSION_SOURCE);
    }

    @Override
    protected String clusterLabel(final int count, final LabelStyle labelStyle) {
      return M.messages().esMarkerClusterLabel(count);
    }

    @Override
    public Feature createLabelFeature(final Feature feature) {
      if (feature == null || ItemWithStringIdUtil.isTemporaryFeature(feature)) {
        return null;
      }
      return super.createLabelFeature(feature);
    }
  }
}
