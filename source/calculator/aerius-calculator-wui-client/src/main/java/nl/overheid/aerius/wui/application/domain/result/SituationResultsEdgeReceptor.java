/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.result;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

/**
 * Receptor where an edge effect occurs
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class SituationResultsEdgeReceptor {

  private int receptorId;
  private double result;
  private double referenceResult;
  private double offSiteReductionResult;
  /**
   * Contains the proposed result of a project calculation,
   * or a temporary contribution in case of a temporary effect calculation
   */
  private double proposedResult;

  public final @JsOverlay int getReceptorId() {
    return receptorId;
  }

  public final @JsOverlay double getResult() {
    return result;
  }

  public final @JsOverlay double getReferenceResult() {
    return referenceResult;
  }
  public final @JsOverlay double getOffSiteReductionResult() {
    return offSiteReductionResult;
  }

  public final @JsOverlay double getProposedResult() {
    return proposedResult;
  }
}
