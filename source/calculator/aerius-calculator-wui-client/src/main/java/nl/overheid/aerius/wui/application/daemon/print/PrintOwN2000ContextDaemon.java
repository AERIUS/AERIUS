/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.print;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.shared.domain.PdfProductType;
import nl.overheid.aerius.shared.domain.summary.ProcurementPolicy;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.command.result.FetchSummaryResultsCommand;
import nl.overheid.aerius.wui.application.context.PrintContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.application.event.RestoreCalculationJobContextEvent;

/**
 * This daemon is responsible for handling changes in the PrintOwN2000Context.
 */
public class PrintOwN2000ContextDaemon extends BasicEventComponent {

  private static final PrintOwN2000ContextDaemonEventBinder EVENT_BINDER = GWT.create(PrintOwN2000ContextDaemonEventBinder.class);

  interface PrintOwN2000ContextDaemonEventBinder extends EventBinder<PrintOwN2000ContextDaemon> {}

  @Inject PrintContext context;
  @Inject ScenarioContext scenarioContext;

  @EventHandler
  public void onRestoreCalculationJobContextEvent(final RestoreCalculationJobContextEvent e) {

    if (context.getPdfProductType() == PdfProductType.OWN2000_LBV_POLICY) {

      context.addProcurementPolicyResultKey(new SituationResultsKey(
          SituationContext.handleFromSituation(scenarioContext.getActiveSituation()),
          ScenarioResultType.DEPOSITION_SUM,
          context.getEmissionResultKey(),
          context.getSummaryHexagonType(),
          context.getOverlappingHexagonType(),
          ProcurementPolicy.WNB_LBV));

      context.addProcurementPolicyResultKey(new SituationResultsKey(
          SituationContext.handleFromSituation(scenarioContext.getActiveSituation()),
          ScenarioResultType.DEPOSITION_SUM,
          context.getEmissionResultKey(),
          SummaryHexagonType.ABOVE_CL_HEXAGONS,
          context.getOverlappingHexagonType(),
          ProcurementPolicy.WNB_LBV_PLUS));

      context.getProcurementPolicyResultKeys().forEach(key -> eventBus.fireEvent(new FetchSummaryResultsCommand(key)));
    }
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
