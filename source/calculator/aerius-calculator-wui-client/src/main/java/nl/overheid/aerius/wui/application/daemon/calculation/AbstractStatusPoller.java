/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.calculation;

import java.util.ArrayList;
import java.util.List;

import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.wui.application.domain.export.HistoryRecord;

public abstract class AbstractStatusPoller<T> {
  private final int interval;
  private final int retryAmount;

  private int retriesLeft;

  private final List<T> items = new ArrayList<>();

  private boolean running;
  private int robin = 0;

  public AbstractStatusPoller(final int interval, final int retryAmount) {
    this.interval = interval;
    this.retryAmount = retryAmount;
    this.retriesLeft = retryAmount;
  }

  public void start(final T item) {
    items.add(item);
    if (running) {
      return;
    }

    running = true;
    SchedulerUtil.delay(this::doFetchStatus);
  }

  public void remove(final HistoryRecord historyRecord) {
    items.remove(historyRecord);
  }

  protected boolean isRunning() {
    return running;
  }

  protected boolean isStopped() {
    return !isRunning();
  }

  protected void succeedCall(final T item) {
    // Reset retry amount on success
    retriesLeft = retryAmount;
  }

  /**
   * Cycle to the next robin (or back to 0)
   */
  private int getNextRobin() {
    return robin = (robin + 1) % items.size();
  }

  protected void doFetchStatus() {
    // Check if there are any items left and stop cycling through them if not
    if (items.isEmpty()) {
      running = false;
      return;
    }

    doFetchStatus(items.get(getNextRobin()));
  }

  protected abstract void doFetchStatus(final T item);

  protected void fail(final T item, final Throwable e) {
    // Retry a couple times before erroring out
    if (retriesLeft == 0) {
      running = false;
      finalFailure(item, e);
      return;
    }

    GWTProd.warn("Error while fetching status, retrying " + retriesLeft + " times.");
    retriesLeft -= 1;
    tryFetchDelayed();
  }

  protected abstract void finalFailure(T item, Throwable e);

  protected void tryFetchDelayed() {
    SchedulerUtil.delay(() -> tryFetch(), interval);
  }

  protected void tryFetch() {
    if (!running) {
      return;
    }

    doFetchStatus();
  }

  protected void finish(final T item) {
    items.remove(item);
  }
}
