/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.calculate;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;
import com.google.web.bindery.event.shared.binder.GenericEvent;

import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.place.PlaceController;
import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobDeselectCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobEditCancelCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobEditSaveCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobSelectCommand;
import nl.overheid.aerius.wui.application.command.calculation.VerifyCalculationJobDefaultsCommand;
import nl.overheid.aerius.wui.application.context.calculation.CalculationPreparationContext;
import nl.overheid.aerius.wui.application.event.SwitchActiveCalculationEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.ui.main.MainView.FlexView;
import nl.overheid.aerius.wui.application.ui.pages.ThemeDelegatedActivity;
import nl.overheid.aerius.wui.application.ui.pages.ThemeView;
import nl.overheid.aerius.wui.application.ui.pages.calculate.detail.CalculationJobDetailViewFactory;

/**
 * Activity for editing a single building.
 */
public class CalculateActivity extends BasicEventComponent implements ThemeDelegatedActivity, CalculatePresenter {
  private static final CalculateActivityEventBinder EVENT_BINDER = GWT.create(CalculateActivityEventBinder.class);

  interface CalculateActivityEventBinder extends EventBinder<CalculateActivity> {
  }

  @Inject PlaceController placeController;
  @Inject CalculationPreparationContext prepContext;

  private HandlerRegistration handlers;
  private ThemeView themeView;

  private boolean userCancelled;

  @Override
  public void onStart(final ThemeView view) {
    this.themeView = view;

    view.setDelegatedPresenter(this);
    view.setLeftView(CalculationJobViewFactory.get(), FlexView.RIGHT);
    view.setMiddleView(CalculationJobDetailViewFactory.get());

    view.setRightView(CalculationSituationSummaryFactory.get());
    updateSoftMiddle();

    eventBus.fireEvent(new VerifyCalculationJobDefaultsCommand());
  }

  @Override
  public String mayStop() {
    final boolean cancelled = userCancelled;
    userCancelled = false;
    return prepContext.getTemporaryJob() == null || cancelled ? null : M.messages().calculationJobMayStop();
  }

  @Override
  public void onStop() {
    final CalculationJobEditCancelCommand cmd = new CalculationJobEditCancelCommand();
    cmd.silence();
    eventBus.fireEvent(cmd);
    handlers.removeHandler();
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    handlers = super.setEventBus(eventBus, this, EVENT_BINDER);
  }

  @EventHandler(handles = {CalculationJobEditCancelCommand.class, CalculationJobEditSaveCommand.class})
  public void onCancelOrSaveCommand(final GenericEvent c) {
    userCancelled = true;
  }

  @EventHandler(handles = {SwitchActiveCalculationEvent.class,
      CalculationJobSelectCommand.class, CalculationJobDeselectCommand.class})
  public void onSelectOrPeekCommand() {
    updateSoftMiddle();
  }

  private void updateSoftMiddle() {
    SchedulerUtil.delay(() -> themeView.setSoftMiddle(prepContext.getActiveJob() == null));
  }

}
