/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import com.google.inject.Singleton;

import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.ProductProfile;
import nl.overheid.aerius.shared.domain.Theme;

@Singleton
public class ApplicationContext {
  private ProductProfile productProfile = null;
  private ApplicationConfiguration configuration = null;

  public Theme getTheme() {
    return configuration.getTheme();
  }

  public ProductProfile getProductProfile() {
    return productProfile;
  }

  public void setProductProfile(final ProductProfile productProfile) {
    this.productProfile = productProfile;
  }

  public boolean hasSetting(final Enum<?> key) {
    return configuration.getSettings().hasSetting(key);
  }

  /**
   * Get a setting that is required to be present.
   * An exception will be thrown if setting is not present.
   */
  public <T extends Object> T getSetting(final Enum<?> key) {
    return getSetting(key, false);
  }

  /**
   * Get a setting that is optionally present.
   */
  public <T extends Object> T getOptionalSetting(final Enum<?> key) {
    return getSetting(key, true);
  }

  private <T extends Object> T getSetting(final Enum<?> key, final boolean soft) {
    return configuration.getSettings().getSetting(key, soft);
  }

  public ApplicationConfiguration getConfiguration() {
    return configuration;
  }

  public void setConfiguration(final ApplicationConfiguration configuration) {
    this.configuration = configuration;
  }

  public boolean isAppFlagEnabled(final AppFlag flag) {
    return configuration != null && configuration.isAppFlagEnabled(flag);
  }
}
