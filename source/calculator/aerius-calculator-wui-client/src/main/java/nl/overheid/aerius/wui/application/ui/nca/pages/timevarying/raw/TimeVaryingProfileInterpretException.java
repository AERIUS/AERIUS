/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.nca.pages.timevarying.raw;

public class TimeVaryingProfileInterpretException extends Exception {
  private static final long serialVersionUID = 1L;

  private final Reason reason;
  private String textCause;

  public enum Reason {
    INCORRECT_NUMBER_OF_ROWS,

    INCORRECT_NUMBER_OF_COLUMNS,

    INCORRECT_HOUR_VALUE,

    MISSING_HEADER_TIME_OF_DAY,

    MISSING_HEADER_SATURDAY,

    MISSING_HEADER_SUNDAY,

    MISSING_HEADER_WEEKDAY,

    MISSING_HEADER_MONTH,

    MISSING_HEADER_MONTH_FACTOR,

    CANNOT_INTERPRET_TIME_OF_DAY,

    CANNOT_INTERPRET_VALUE,

    NOT_ALL_TIMESLOTS_FOUND;
  }

  public TimeVaryingProfileInterpretException(final Reason reason) {
    this(reason, "");
  }

  public TimeVaryingProfileInterpretException(final Reason reason, final String textCause) {
    this.reason = reason;
    this.textCause = textCause;
  }

  public Reason getReason() {
    return reason;
  }

  public String getTextCause() {
    return textCause;
  }

  public void setTextCause(final String textCause) {
    this.textCause = textCause;
  }
}
