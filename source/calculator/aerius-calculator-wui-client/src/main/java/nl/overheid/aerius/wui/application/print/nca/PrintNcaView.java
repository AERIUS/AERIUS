/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.print.nca;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;
import com.google.web.bindery.event.shared.EventBus;

import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.wui.application.context.PrintContext;
import nl.overheid.aerius.wui.application.print.CompleteTracker;
import nl.overheid.aerius.wui.application.print.common.PrintView;
import nl.overheid.aerius.wui.application.print.nca.meteo.NcaMeteoInfoComponent;
import nl.overheid.aerius.wui.application.print.nca.overview.NcaScenarioOverviewComponent;
import nl.overheid.aerius.wui.application.print.nca.results.NcaScenarioResultsComponent;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component(components = {
    PrintView.class,
    NcaProjectCalculationCoverPage.class,
    NcaTableOfContentsPage.class,
    NcaScenarioOverviewComponent.class,
    NcaMeteoInfoComponent.class,
    NcaScenarioResultsComponent.class,
})
public class PrintNcaView extends BasicVueView implements HasMounted {
  @Prop EventBus eventBus;

  @Prop PrintNcaActivity presenter;
  @Inject @Data PrintContext context;

  @Inject CompleteTracker completeTracker;

  @Override
  public void mounted() {
    SchedulerUtil.delay(() -> {
      completeTracker.createTask().complete();
    }, 500);
  }

}
