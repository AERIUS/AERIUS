/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.command.timevarying;

import nl.aerius.wui.command.SimpleGenericCommand;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.source.TimeVaryingProfile;
import nl.overheid.aerius.wui.application.event.timevarying.TimeVaryingProfileDeletedEvent;

/**
 * Command to add a new {@link TimeVaryingProfile} object.
 */
public class TimeVaryingProfileDeleteCommand extends SimpleGenericCommand<TimeVaryingProfile, TimeVaryingProfileDeletedEvent> {
  private SituationContext situation;

  public TimeVaryingProfileDeleteCommand(final TimeVaryingProfile profile) {
    this(profile, null);
  }

  public TimeVaryingProfileDeleteCommand(final TimeVaryingProfile profile, final SituationContext situation) {
    super(profile);
    this.situation = situation;
  }

  public SituationContext getSituation() {
    return situation;
  }

  public void setSituation(final SituationContext situationId) {
    this.situation = situationId;
  }

  @Override
  protected TimeVaryingProfileDeletedEvent createEvent(final TimeVaryingProfile value) {
    return new TimeVaryingProfileDeletedEvent(value, situation);
  }

}
