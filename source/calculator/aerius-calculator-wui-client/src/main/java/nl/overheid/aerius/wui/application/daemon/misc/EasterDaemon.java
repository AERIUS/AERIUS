/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.misc;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.geo.wui.MapLayoutPanel;
import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.wui.application.command.EasterActivationCommand;
import nl.overheid.aerius.wui.application.command.EasterDeactivationCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;

public class EasterDaemon extends BasicEventComponent implements Daemon {
  private static final EasterDaemonEventBinder EVENT_BINDER = GWT.create(EasterDaemonEventBinder.class);

  interface EasterDaemonEventBinder extends EventBinder<EasterDaemon> {
  }

  @Inject private MapLayoutPanel map;
  @Inject ApplicationContext appContext;

  private boolean isEasterActive = false;

  private static native boolean isMapGameAvailable() /*-{
    return $wnd.__MAP_GAME__ && $wnd.__MAP_GAME__.available;
  }-*/;

  private static native void activateMapGame(Object olMap, String region, Runnable onExit) /*-{
    if ($wnd.__MAP_GAME__ && $wnd.__MAP_GAME__.available) {
      $wnd.__MAP_GAME__.instance.activate(olMap, region, function() {
        onExit.@java.lang.Runnable::run()();
      });
    }
  }-*/;

  private static native void deactivateMapGame() /*-{
    if ($wnd.__MAP_GAME__ && $wnd.__MAP_GAME__.available) {
      $wnd.__MAP_GAME__.instance.deactivate();
    }
  }-*/;

  @EventHandler
  public void onEasterActivationCommand(final EasterActivationCommand c) {
    if (isEasterActive) {
      c.silence();
      return;
    }

    if (isMapGameAvailable() && map.getInnerMap().getTarget() != null) {
      isEasterActive = true;
      final String region = appContext.getConfiguration().getSearchRegion().name();
      activateMapGame(map.getInnerMap(), region, () -> eventBus.fireEvent(new EasterDeactivationCommand()));
    } else {
      GWTProd.warn("Map is not available");
      c.silence();
    }
  }

  @EventHandler
  public void onEasterDeactivationCommand(final EasterDeactivationCommand event) {
    if (!isEasterActive) {
      event.silence();
      return;
    }

    isEasterActive = false;
    deactivateMapGame();
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
