/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.results;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.vue.VueComponentFactory;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.command.PlaceChangeRequestCommand;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.result.MainTab;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobSelectCommand;
import nl.overheid.aerius.wui.application.command.result.SelectResultMainTabCommand;
import nl.overheid.aerius.wui.application.components.button.IconComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.notification.FloatingUtilityComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationPreparationContext;
import nl.overheid.aerius.wui.application.context.result.ResultViewContext;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationInfo;
import nl.overheid.aerius.wui.application.domain.calculation.JobProgress;
import nl.overheid.aerius.wui.application.domain.summary.JobSummary;
import nl.overheid.aerius.wui.application.place.CalculatePlace;
import nl.overheid.aerius.wui.application.ui.pages.results.progress.CalculationProgressView;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    CalculatedInfoView.class,
    ScenarioResultsView.class,
    SummaryResultsView.class,
    DecisionFrameworkView.class,
    CalculationProgressView.class,
    SimplifiedListBoxComponent.class,
    VerticalCollapse.class,
    VerticalCollapseGroup.class,
    FloatingUtilityComponent.class,
    IconComponent.class,
})
public class ResultsView extends BasicVueComponent {
  @Prop EventBus eventBus;

  @Data @Inject ScenarioContext scenarioContext;
  @Data @Inject CalculationContext calculationContext;
  @Data @Inject ApplicationContext appContext;
  @Data @Inject CalculationPreparationContext prepContext;
  @Data @Inject ResultViewContext resultContext;

  @Computed("isCalculationSourceDiscrepancy")
  public boolean isCalculationSourceDiscrepancy() {
    final CalculationJobContext activeJob = prepContext.getActiveJob();
    final CalculationExecutionContext activeCalculation = getCalculationExecution();
    return activeJob != null && ScenarioContext.isCalculationStale(scenarioContext, activeJob, activeCalculation);
  }

  @Computed("hasCalculationJobSelected")
  public boolean hasCalculationJobSelected() {
    return calculationContext.getActiveJobId() != null;
  }

  @Computed("hasCalculationSelected")
  public boolean hasCalculationSelected() {
    return getCalculationExecution() != null;
  }

  @Computed
  public List<CalculationJobContext> getJobs() {
    return calculationContext.getCalculations()
        .stream()
        .map(CalculationExecutionContext::getJobContext)
        .collect(Collectors.toList());
  }

  @Computed("jobToKey")
  public Function<Object, Object> jobToKey() {
    return v -> ((CalculationJobContext) v).getId();
  }

  @Computed
  public CalculationExecutionContext getCalculationJob() {
    return calculationContext.getActiveCalculation();
  }

  @Computed
  public CalculationInfo getCalculationInfo() {
    return Optional.ofNullable(getCalculationJob())
        .map(v -> v.getCalculationInfo())
        .orElseThrow(() -> new RuntimeException("Could not retrieve calculation info."));
  }

  @Computed
  public JobProgress getJobProgress() {
    return Optional.ofNullable(getCalculationInfo())
        .map(v -> v.getJobProgress())
        .orElseGet(() -> new JobProgress());
  }

  @Computed
  public CalculationExecutionContext getCalculationExecution() {
    return calculationContext.getActiveCalculation();
  }

  @Computed("isQuickRun")
  public boolean isQuickRun() {
    return getCalculationExecution() != null
        && getCalculationExecution().getJobContext().getCalculationOptions().getCalculationMethod() == CalculationMethod.QUICK_RUN;
  }

  @Computed
  public List<MainTab> getAvailableTabs() {
    final List<MainTab> availableTabs = new ArrayList<>();
    for (final MainTab tab : appContext.getConfiguration().getAvailableResultTabs()) {
      if (tab == MainTab.DECISION_FRAMEWORK) {
        final CalculationJobType jobType = getCalculationJob() == null ? null
            : getCalculationJob().getJobContext().getCalculationOptions().getCalculationJobType();
        if (jobType == CalculationJobType.PROCESS_CONTRIBUTION || jobType == CalculationJobType.IN_COMBINATION_PROCESS_CONTRIBUTION) {
          availableTabs.add(tab);
        }
      } else {
        availableTabs.add(tab);
      }
    }
    return availableTabs;
  }

  @JsMethod
  public void selectActiveJob(final CalculationJobContext job) {
    eventBus.fireEvent(new CalculationJobSelectCommand(job));
  }

  @Computed
  public String getSelectedView() {
    VueComponentFactory<?> tabContent;

    switch (resultContext.getSelectedMainTab()) {
    case CALCULATED_INFO:
      tabContent = CalculatedInfoViewFactory.get();
      break;
    case SUMMARY:
      tabContent = SummaryResultsViewFactory.get();
      break;
    case DECISION_FRAMEWORK:
      tabContent = DecisionFrameworkViewFactory.get();
      break;
    case SCENARIO_RESULTS:
      tabContent = ScenarioResultsViewFactory.get();
      break;
    default:
      return null;
    }

    return tabContent.getComponentTagName();
  }

  @Computed
  public MainTab getSelectedTab() {
    return resultContext.getSelectedMainTab();
  }

  @JsMethod
  public void calculateJob() {
    eventBus.fireEvent(new PlaceChangeRequestCommand(new CalculatePlace(appContext.getTheme())));
  }

  @JsMethod
  public boolean isSelected(final MainTab tab) {
    return resultContext.getSelectedMainTab() == tab;
  }

  @JsMethod
  public void selectMainTab(final MainTab tab) {
    eventBus.fireEvent(new SelectResultMainTabCommand(tab));
  }

  @JsMethod
  public JobSummary getJobSummary() {
    return getCalculationJob().getJobSummary();
  }

  @JsMethod
  public boolean hasResults() {
    return getJobSummary() != null;
  }
}
