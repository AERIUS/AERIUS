/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.service;

import java.util.List;

import javax.inject.Inject;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Singleton;

import elemental2.dom.DomGlobal;

import nl.aerius.wui.util.RequestUtil;
import nl.overheid.aerius.js.file.FileValidationStatus;
import nl.overheid.aerius.shared.RequestMappings;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.file.UploadUtil;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;

@Singleton
public class FileServiceAsyncImpl implements FileServiceAsync {

  @Inject EnvironmentConfiguration cfg;
  @Inject ApplicationContext ctx;
  @Inject HeaderHelper hdr;

  @Override
  public void uploadFile(final FileUploadStatus item, final AsyncCallback<String> callback) {
    UploadUtil.uploadFile(cfg.getConnectBaseUrl(), item, ctx.getTheme(), hdr.defaultHeaders(), callback);
  }

  @Override
  public <T> void fetchJson(final String fileCode, final boolean keepFile, final AsyncCallback<T> callback) {
    final String url = RequestUtil.prepareUrl(cfg.getConnectBaseUrl(),
        keepFile ? RequestMappings.CONNECT_UI_RETRIEVE_JSON_KEEP_FILE : RequestMappings.CONNECT_UI_RETRIEVE_JSON,
        RequestMappings.FILE_CODE, fileCode);

    RequestUtil.doGetWithHeaders(url, hdr.defaultHeaders(), AeriusRequestCallback.createAsync(callback));
  }

  @Override
  public void fetchValidationResult(final String fileCode, final AsyncCallback<FileValidationStatus> callback) {
    final String url = RequestUtil.prepareUrl(cfg.getConnectBaseUrl(), RequestMappings.CONNECT_UI_RETRIEVE_VALIDATION_RESULT,
        RequestMappings.FILE_CODE, fileCode);

    RequestUtil.doGetWithHeaders(url, hdr.defaultHeaders(), AeriusRequestCallback.createAsync(callback));
  }

  @Override
  public void removeFile(final FileUploadStatus file, final AsyncCallback<Boolean> callback) {
    final String url = RequestUtil.prepareUrl(cfg.getConnectBaseUrl(), RequestMappings.CONNECT_UI_DELETE_FILE,
        RequestMappings.FILE_CODE, file.getFileCode());

    RequestUtil.doDelete(url, hdr.defaultHeaders(), AeriusRequestCallback.createRawAsync(callback, s -> Boolean.valueOf(s)));
  }

  @Override
  public void fetchValidationReport(final List<String> fileCodes) {
    final String url = RequestUtil.prepareUrl(cfg.getConnectBaseUrl(), RequestMappings.CONNECT_UI_RETRIEVE_VALIDATION_REPORT,
        RequestMappings.FILE_CODE, String.join(",", fileCodes));

    // seems counterintuitive to navigate to the url you want to download in the background
    // however, browsers don't navigate away until they've received the headers
    // and the Content-Disposition header indicates that this is not a webpage, but a file to download
    DomGlobal.window.open(url, "_blank");
  }
}
