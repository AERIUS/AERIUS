/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.nca.pages.timevarying;

import java.sql.Date;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.i18n.shared.DateTimeFormat;

import elemental2.dom.Event;
import elemental2.dom.HTMLInputElement;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.domain.source.TimeVaryingProfile;
import nl.overheid.aerius.wui.application.domain.source.util.TimeVaryingProfileUtil;

@Component(components = {
    ValidationBehaviour.class,
})
public class MonthlyTableInputComponent extends ErrorWarningValidator {
  private static final NumberFormat HOUR_FORMAT = NumberFormat.getFormat("00");
  private static final DateTimeFormat MONTH_FORMAT = DateTimeFormat.getFormat("MMMM");
  @Prop(required = true) TimeVaryingProfile profile;

  @JsMethod
  public String formatLegendMonth(final Object monthStr) {
    final int month = Integer.parseInt(String.valueOf(monthStr));
    final String start = HOUR_FORMAT.format(month);
    final String end = HOUR_FORMAT.format(month + 1D);
    return start + "-" + end;
  }

  @JsMethod
  public String getMonthInput(final int month) {
    final int monthInt = Integer.parseInt(String.valueOf(month));

    return TimeVaryingProfileUtil.MONTHLY_PROFILE_UTIL.getValue(profile.getValues(), monthInt);
  }

  @JsMethod
  public String getMonthTotal() {
    return TimeVaryingProfileUtil.MONTHLY_PROFILE_UTIL.getValuesTotalPreciseText(profile.getValues());
  }

  @JsMethod
  public String month(final int month) {
    return MONTH_FORMAT.format(new Date(2024, month, 1));
  }

  @JsMethod
  public String getDebugId(final int month) {
    return "monthlyTimeVaryingTableInput-" + month;
  }

  @JsMethod
  public void setMonthInput(final int month, final Event ev) {
    final int monthInt = Integer.parseInt(String.valueOf(month));

    double value;
    try {
      final HTMLInputElement input = (HTMLInputElement) ev.target;
      value = Double.parseDouble(input.value);
    } catch (final NumberFormatException e) {
      value = 0;
    }

    TimeVaryingProfileUtil.MONTHLY_PROFILE_UTIL.setInput(profile.getValues(), monthInt, value);
  }
}
