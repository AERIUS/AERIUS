/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.place;

import nl.aerius.wui.place.ApplicationPlace.Tokenizer;
import nl.aerius.wui.place.PlaceTokenizer;

public class ApplicationTokenizers {
  private static final String THEME_SELECTION_KEY = "theme-selection";

  public static final PlaceTokenizer<CalculatorDefaultPlace> DEFAULT_PLACE = Tokenizer.create(() -> new CalculatorDefaultPlace(),
      THEME_SELECTION_KEY);

}
