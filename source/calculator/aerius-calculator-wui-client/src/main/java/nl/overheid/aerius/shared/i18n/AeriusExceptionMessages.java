/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.i18n;

import com.google.gwt.i18n.client.ConstantsWithLookup;

/**
 * Interface for error codes representing errors occurred on the server and
 * passed to the client.
 */
public interface AeriusExceptionMessages extends ConstantsWithLookup {

  /**
   * Internal error, try later again. Recoverable error. Possible cause
   * temporary unavailability of a service.
   */
  String e666();

  /**
   * Internal error, contact help desk. When this message is shown an error
   * occurred on the server that was very likely caused by a bug.
   */
  String e667();

  /**
   * Internal error, contact help desk. When this message is shown an error
   * occurred on the server we are missing a required configuration in the database for this product.
   */
  String e668();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CALCULATION_NO_SOURCES AeriusExceptionReason.CALCULATION_NO_SOURCES
   */
  String e1001();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#SOURCE_VALIDATION_FAILED ImaerExceptionReason.SOURCE_VALIDATION_FAILED
   */
  String e1002();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#LIMIT_SOURCES_EXCEEDED ImaerExceptionReason.LIMIT_SOURCES_EXCEEDED
   */
  String e1003();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#LIMIT_LINE_LENGTH_EXCEEDED ImaerExceptionReason.LIMIT_LINE_LENGTH_EXCEEDED
   */
  String e1004();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#LIMIT_POLYGON_SURFACE_EXCEEDED ImaerExceptionReason.LIMIT_POLYGON_SURFACE_EXCEEDED
   */
  String e1005();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#INLAND_SHIPPING_SHIP_TYPE_NOT_ALLOWED ImaerExceptionReason.INLAND_SHIPPING_SHIP_TYPE_NOT_ALLOWED
   */
  String e1006();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#SHIPPING_ROUTE_GEOMETRY_NOT_ALLOWED ImaerExceptionReason.SHIPPING_ROUTE_GEOMETRY_NOT_ALLOWED
   */
  String e1007();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#GEOCODER_ERROR AeriusExceptionReason.GEOCODER_ERROR
   */
  String e1008();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CALCULATION_TO_COMPLEX AeriusExceptionReason#CALCULATION_TO_COMPLEX
   */
  String e1009();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#ROAD_GEOMETRY_NOT_ALLOWED AeriusExceptionReason.ROAD_GEOMETRY_NOT_ALLOWED
   */
  String e1010();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CALCULATION_PAA_PROPOSED_SITUATION_MISSING AeriusExceptionReason.CALCULATION_PAA_PROPOSED_SITUATION_MISSING
   */
  String e1011();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#LIMIT_LINE_LENGTH_ZERO ImaerExceptionReason.LIMIT_LINE_LENGTH_ZERO
   */
  String e1012();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#LIMIT_POLYGON_SURFACE_ZERO ImaerExceptionReason.LIMIT_POLYGON_SURFACE_ZERO
   */
  String e1013();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#INLAND_SHIPPING_WATERWAY_INCONCLUSIVE ImaerExceptionReason.INLAND_SHIPPING_WATERWAY_INCONCLUSIVE
   */
  String e1014();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#INLAND_SHIPPING_WATERWAY_NO_DIRECTION ImaerExceptionReason.INLAND_SHIPPING_WATERWAY_NO_DIRECTION
   */
  String e1015();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#SHIPPING_INVALID_SECTOR ImaerExceptionReason.SHIPPING_INVALID_SECTOR
   */
  String e1016();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CALCULATION_DUPLICATE_POINT_IDS AeriusExceptionReason.CALCULATION_DUPLICATE_POINT_IDS
   */
  String e1017();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#HOURS_EXCEEDING_HOURS_IN_YEAR ImaerExceptionReason.HOURS_EXCEEDING_HOURS_IN_YEAR
   */
  String e1020();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#CUSTOM_TIME_VARYING_PROFILE_TYPE_UNKNOWN ImaerExceptionReason.CUSTOM_TIME_VARYING_PROFILE_TYPE_UNKNOWN
   */
  String e1023();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#CUSTOM_TIME_VARYING_PROFILE_INVALID_COUNT ImaerExceptionReason.CUSTOM_TIME_VARYING_PROFILE_INVALID_COUNT
   */
  String e1024();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#CUSTOM_TIME_VARYING_PROFILE_INVALID_SUM ImaerExceptionReason.CUSTOM_TIME_VARYING_PROFILE_INVALID_SUM
   */
  String e1025();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#MOBILE_SOURCE_MISSING_LITER_FUEL ImaerExceptionReason.MOBILE_SOURCE_MISSING_LITER_FUEL
   */
  String e1026();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#MOBILE_SOURCE_MISSING_OPERATING_HOURS ImaerExceptionReason.MOBILE_SOURCE_MISSING_OPERATING_HOURS
   */
  String e1027();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#MOBILE_SOURCE_MISSING_LITER_ADBLUE ImaerExceptionReason.MOBILE_SOURCE_MISSING_LITER_ADBLUE
   */
  String e1028();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#MOBILE_SOURCE_HIGH_ADBLUE_FUEL_RATIO ImaerExceptionReason.MOBILE_SOURCE_HIGH_ADBLUE_FUEL_RATIO
   */
  String e1029();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#MISSING_NUMBER_OF_ANIMALS ImaerExceptionReason.MISSING_NUMBER_OF_ANIMALS
   */
  String e1031();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#MISSING_NUMBER_OF_DAYS ImaerExceptionReason.MISSING_NUMBER_OF_DAYS
   */
  String e1032();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#MISSING_NUMBER_OF_APPLICATIONS ImaerExceptionReason.MISSING_NUMBER_OF_APPLICATIONS
   */
  String e1033();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#MISSING_METERS_CUBED ImaerExceptionReason.MISSING_METERS_CUBED
   */
  String e1034();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#MISSING_TONNES ImaerExceptionReason.MISSING_TONNES
   */
  String e1035();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#MISSING_METERS_SQUARED ImaerExceptionReason.MISSING_METERS_SQUARED
   */
  String e1036();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#MISSING_HEAT_CONTENT ImaerExceptionReason.MISSING_HEAT_CONTENT
   */
  String e1037();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#HEAT_CONTENT_OUT_OF_RANGE ImaerExceptionReason.HEAT_CONTENT_OUT_OF_RANGE
   */
  String e1038();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#HEAT_CAPACITY_OUT_OF_RANGE ImaerExceptionReason.HEAT_CAPACITY_OUT_OF_RANGE
   */
  String e1039();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#SOURCE_VOLUME_FLOATING ImaerExceptionReason.SOURCE_VOLUME_FLOATING
   */
  String e1040();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CALCULATION_PAA_PERMIT_CALCULATION_RESULTS_MISSING AeriusExceptionReason.CALCULATION_PAA_PERMIT_CALCULATION_RESULTS_MISSING
   */
  String e1053();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CALCULATION_PAA_PERMIT_CALCULATION_RESULTS_INCORRECT_SITUATIONS_COUNT AeriusExceptionReason.CALCULATION_PAA_PERMIT_CALCULATION_RESULTS_INCORRECT_SITUATIONS_COUNT
   */
  String e1055();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CALCULATION_JOB_TYPE_MISSING_REQUIRED_SITUATION AeriusExceptionReason.CALCULATION_JOB_TYPE_MISSING_REQUIRED_SITUATION
   */
  String e1056();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CALCULATION_JOB_TYPE_INVALID_SITUATION_TYPE AeriusExceptionReason.CALCULATION_JOB_TYPE_INVALID_SITUATION_TYPE
   */
  String e1057();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CALCULATION_JOB_TYPE_TOO_MANY_SITUATIONS AeriusExceptionReason.CALCULATION_JOB_TYPE_TOO_MANY_SITUATIONS
   */
  String e1058();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORT_FILE_NOT_SUPPLIED AeriusExceptionReason.IMPORT_FILE_NOT_SUPPLIED
   */
  String e5001();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORT_FILE_COULD_NOT_BE_READ AeriusExceptionReason.IMPORT_FILE_COULD_NOT_BE_READ
   */
  String e5002();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#FAULTY_REQUEST AeriusExceptionReason.FAULTY_REQUEST
   */
  String e5003();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORT_FILE_UNSUPPORTED AeriusExceptionReason.IMPORT_FILE_UNSUPPORTED
   */
  String e5004();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORT_FILE_TYPE_NOT_ALLOWED AeriusExceptionReason.IMPORT_FILE_TYPE_NOT_ALLOWED
   */
  String e5005();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORT_REQUIRED_ID_MISSING AeriusExceptionReason.IMPORT_REQUIRED_ID_MISSING
   */
  String e5006();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORT_DUPLICATE_ENTRY AeriusExceptionReason.IMPORT_DUPLICATE_ENTRY
   */
  String e5007();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORT_NO_SOURCES_PRESENT AeriusExceptionReason.IMPORT_NO_SOURCES_PRESENT
   */
  String e5008();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORT_NO_RESULTS_PRESENT AeriusExceptionReason.IMPORT_NO_RESULTS_PRESENT
   */
  String e5009();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORT_NO_CALCULATION_POINTS_PRESENT AeriusExceptionReason.IMPORT_NO_CALCULATION_POINTS_PRESENT
   */
  String e5010();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORT_CALCULATION_POINTS_PRESENT AeriusExceptionReason.IMPORT_CALCULATION_POINTS_PRESENT
   */
  String e5013();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORT_NO_VALID_RECEIVED_DATE AeriusExceptionReason.IMPORT_NO_VALID_RECEIVED_DATE
   */
  String e5011();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORTED_FILE_NOT_FOUND AeriusExceptionReason.IMPORTED_FILE_NOT_FOUND
   */
  String e5012();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORTED_PDF_NOT_AERIUS_PAA AeriusExceptionReason.IMPORTED_PDF_NOT_AERIUS_PAA
   */
  String e5014();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORTED_FILE_DUPLICATES AeriusExceptionReason.IMPORTED_FILE_DUPLICATES
   */
  String e5015();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#UPLOAD_ABORTED AeriusExceptionReason.UPLOAD_ABORTED
   */
  String e5016();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORT_FAILURE AeriusExceptionReason.IMPORT_FAILURE
   */
  String e5017();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORTED_CALCULATION_RESULTS_DOES_NOT_MATCH_SOURCE_SITUATION IMPORTED_CALCULATION_RESULTS_DOES_NOT_MATCH_SOURCE_SITUATION
   */
  String e5040();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORTED_CALCULATION_RESULTS_VERSION_NOT_ALLOWED AeriusExceptionReason.IMPORTED_CALCULATION_RESULTS_VERSION_NOT_ALLOWED
   */
  String e5041();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#IO_EXCEPTION_UNKNOWN AeriusExceptionReason.IO_EXCEPTION_UNKNOWN
   */
  String e5050();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#IO_EXCEPTION_NUMBER_FORMAT AeriusExceptionReason.IO_EXCEPTION_NUMBER_FORMAT
   */
  String e5051();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#IO_EXCEPTION_NOT_ENOUGH_FIELDS AeriusExceptionReason.IO_EXCEPTION_NOT_ENOUGH_FIELDS
   */
  String e5052();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#BRN_WITHOUT_SUBSTANCE AeriusExceptionReason.BRN_WITHOUT_SUBSTANCE
   */
  String e5101();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#BRN_SUBSTANCE_NOT_SUPPORTED AeriusExceptionReason.BRN_SUBSTANCE_NOT_SUPPORTED
   */
  String e5102();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#BRN_DIAMETER_NOT_IMPORTED AeriusExceptionReason.BRN_DIAMETER_NOT_IMPORTED
   */
  String e5103();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_VALIDATION_FAILED ImaerExceptionReason.GML_VALIDATION_FAILED
   */
  String e5201();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_GEOMETRY_INVALID ImaerExceptionReason.GML_GEOMETRY_INVALID
   */
  String e5202();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_ENCODING_INCORRECT ImaerExceptionReason.GML_ENCODING_INCORRECT
   */
  String e5203();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_GEOMETRY_INTERSECTS ImaerExceptionReason.GML_GEOMETRY_INTERSECTS
   */
  String e5204();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_GEOMETRY_NOT_PERMITTED ImaerExceptionReason.GML_GEOMETRY_NOT_PERMITTED
   */
  String e5205();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_GEOMETRY_UNKNOWN ImaerExceptionReason.GML_GEOMETRY_UNKNOWN
   */
  String e5206();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_UNKNOWN_RAV_CODE ImaerExceptionReason.GML_UNKNOWN_RAV_CODE
   */
  String e5207();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_UNKNOWN_MOBILE_SOURCE_CODE ImaerExceptionReason.GML_UNKNOWN_MOBILE_SOURCE_CODE
   */
  String e5208();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_UNKNOWN_SHIP_CODE ImaerExceptionReason.GML_UNKNOWN_SHIP_CODE
   */
  String e5209();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_UNKNOWN_PLAN_CODE ImaerExceptionReason.GML_UNKNOWN_PLAN_CODE
   */
  String e5210();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_GENERIC_PARSE_ERROR ImaerExceptionReason.GML_GENERIC_PARSE_ERROR
   */
  String e5211();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_PARSE_ERROR ImaerExceptionReason.GML_PARSE_ERROR
   */
  String e5212();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_VERSION_NOT_SUPPORTED ImaerExceptionReason.GML_VERSION_NOT_SUPPORTED
   */
  String e5213();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_CREATION_FAILED ImaerExceptionReason.GML_CREATION_FAILED
   */
  String e5214();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GEOMETRY_INVALID ImaerExceptionReason.GEOMETRY_INVALID
   */
  String e5215();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_UNKNOWN_PAS_MEASURE_CODE ImaerExceptionReason.GML_UNKNOWN_PAS_MEASURE_CODE
   */
  String e5216();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_INVALID_PAS_MEASURE_CATEGORY ImaerExceptionReason.GML_INVALID_PAS_MEASURE_CATEGORY
   */
  String e5217();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_INVALID_CATEGORY_MATCH ImaerExceptionReason.GML_INVALID_CATEGORY_MATCH
   */
  String e5218();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_INVALID_ROAD_CATEGORY_MATCH ImaerExceptionReason.GML_INVALID_ROAD_CATEGORY_MATCH
   */
  String e5219();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_ID_NOT_UNIQUE ImaerExceptionReason.GML_ID_NOT_UNIQUE
   */
  String e5220();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_UNKNOWN_ROAD_CATEGORY ImaerExceptionReason.GML_UNKNOWN_ROAD_CATEGORY
   */
  String e5221();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_METADATA_EMPTY ImaerExceptionReason.GML_METADATA_EMPTY
   */
  String e5222();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_VERSION_NOT_LATEST ImaerExceptionReason.GML_VERSION_NOT_LATEST
   */
  String e5223();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_SOURCE_NO_EMISSION ImaerExceptionReason.GML_SOURCE_NO_EMISSION
   */
  String e5224();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_SRS_NAME_UNSUPPORTED ImaerExceptionReason.GML_SRS_NAME_UNSUPPORTED
   */
  String e5225();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#SRM2_SOURCE_NO_VEHICLES ImaerExceptionReason.SRM2_SOURCE_NO_VEHICLES
   */
  String e5226();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_UNKNOWN_WATERWAY_CODE ImaerExceptionReason.GML_UNKNOWN_WATERWAY_CODE
   */
  String e5227();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_INVALID_SHIP_FOR_WATERWAY ImaerExceptionReason.GML_INVALID_SHIP_FOR_WATERWAY
   */
  String e5228();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#INLAND_WATERWAY_NOT_SET ImaerExceptionReason.INLAND_WATERWAY_NOT_SET
   */
  String e5229();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#SRM2_SOURCE_NEGATIVE_VEHICLES ImaerExceptionReason.SRM2_SOURCE_NEGATIVE_VEHICLES
   */
  String e5230();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_ROAD_SEGMENT_POSITION_NOT_FRACTION ImaerExceptionReason.GML_ROAD_SEGMENT_POSITION_NOT_FRACTION
   */
  String e5231();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_UNKNOWN_FARMLAND_ACTIVITY_CODE ImaerExceptionReason.GML_UNKNOWN_FARMLAND_ACTIVITY_CODE
   */
  String e5232();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_MOBILE_SOURCE_IDLE_NOT_SET ImaerExceptionReason.GML_MOBILE_SOURCE_IDLE_NOT_SET
   */
  String e5233();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_OFF_ROAD_CATEGORY_CONVERTED ImaerExceptionReason.GML_OFF_ROAD_CATEGORY_CONVERTED
   */
  String e5234();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_MISSING_NETTING_FACTOR ImaerExceptionReason.GML_MISSING_NETTING_FACTOR
   */
  String e5235();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_UNKNOWN_FARM_EMISSION_FACTOR_TYPE ImaerExceptionReason.GML_UNKNOWN_FARM_EMISSION_FACTOR_TYPE
   */
  String e5236();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_UNKNOWN_MANURE_STORAGE_CODE ImaerExceptionReason.GML_UNKNOWN_MANURE_STORAGE_CODE
   */
  String e5237();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#SRM1_SOURCE_WITH_STRICT_ENFORCEMENT ImaerExceptionReason.SRM1_SOURCE_WITH_STRICT_ENFORCEMENT
   */
  String e5238();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_UNKNOWN_COLD_START_CATEGORY ImaerExceptionReason.GML_UNKNOWN_COLD_START_CATEGORY
   */
  String e5239();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORTED_YEAR_OUTSIDE_RANGE AeriusExceptionReason.IMPORTED_YEAR_OUTSIDE_RANGE
   */
  String e5240();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#BUILDING_HEIGHT_ZERO ImaerExceptionReason.BUILDING_HEIGHT_ZERO
   */
  String e5241();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#BUILDING_HEIGHT_TOO_LOW ImaerExceptionReason.BUILDING_HEIGHT_TOO_LOW
   */
  String e5242();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#CIRCULAR_BUILDING_INCORRECT_DIAMETER ImaerExceptionReason.CIRCULAR_BUILDING_INCORRECT_DIAMETER
   */
  String e5243();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#BUILDING_HEIGHT_TOO_HIGH ImaerExceptionReason.BUILDING_HEIGHT_TOO_HIGH
   */
  String e5244();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#TOO_MANY_BUILDINGS_IN_SITUATION ImaerExceptionReason.TOO_MANY_BUILDINGS_IN_SITUATION
   */
  String e5245();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#UNEXPECTED_NEGATIVE_VALUE ImaerExceptionReason.UNEXPECTED_NEGATIVE_VALUE
   */
  String e5251();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#UNEXPECTED_FRACTION_VALUE ImaerExceptionReason.UNEXPECTED_FRACTION_VALUE
   */
  String e5252();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#SUB_SOURCE_NO_EMISSION ImaerExceptionReason.SUB_SOURCE_NO_EMISSION
   */
  String e5253();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_UNKNOWN_ANIMAL_HOUSING_CODE ImaerExceptionReason.GML_UNKNOWN_ANIMAL_HOUSING_CODE
   */
  String e5260();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_UNSUPPORTED_ANIMAL_HOUSING_COMBINATION ImaerExceptionReason.GML_UNSUPPORTED_ANIMAL_HOUSING_COMBINATION
   */
  String e5261();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_INCORRECT_CUSTOM_FACTORS ImaerExceptionReason.GML_INCORRECT_CUSTOM_FACTORS
   */
  String e5262();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_CONVERTED_LODGING ImaerExceptionReason.GML_CONVERTED_LODGING
   */
  String e5263();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_CONVERTED_LODGING_WITH_SYSTEMS ImaerExceptionReason.GML_CONVERTED_LODGING_WITH_SYSTEMS
   */
  String e5264();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_CONVERTED_LODGING_TO_CUSTOM ImaerExceptionReason.GML_CONVERTED_LODGING_TO_CUSTOM
   */
  String e5265();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#GEOMETRY_TOO_MANY_VERTICES ImaerExceptionReason.GEOMETRY_TOO_MANY_VERTICES
   */
  String e5271();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#PAA_VALIDATION_FAILED AeriusExceptionReason.PAA_VALIDATION_FAILED
   */
  String e5301();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#ZIP_WITHOUT_USABLE_FILES AeriusExceptionReason.ZIP_WITHOUT_USABLE_FILES
   */
  String e5401();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#ZIP_TOO_MANY_USABLE_FILES_ERROR AeriusExceptionReason.ZIP_TOO_MANY_USABLE_FILES_ERROR
   */
  String e5402();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#ZIP_TOO_MANY_USABLE_FILES_WARNING AeriusExceptionReason.ZIP_TOO_MANY_USABLE_FILES_WARNING
   */
  String e5403();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#COHESION_DUPLICATE_SOURCE_IDS ImaerExceptionReason.COHESION_DUPLICATE_SOURCE_IDS
   */
  String e5501();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#COHESION_DUPLICATE_POINT_IDS ImaerExceptionReason.COHESION_DUPLICATE_POINT_IDS
   */
  String e5502();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#COHESION_DUPLICATE_MEASURE_IDS ImaerExceptionReason.COHESION_DUPLICATE_MEASURE_IDS
   */
  String e5503();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#COHESION_DUPLICATE_DISPERSION_LINES ImaerExceptionReason.COHESION_DUPLICATE_DISPERSION_LINES
   */
  String e5504();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#COHESION_REFERENCE_DISPERSION_LINE_MISSING_ROAD ImaerExceptionReason.COHESION_REFERENCE_DISPERSION_LINE_MISSING_ROAD
   */
  String e5511();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#COHESION_REFERENCE_DISPERSION_LINE_MISSING_POINT ImaerExceptionReason.COHESION_REFERENCE_DISPERSION_LINE_MISSING_POINT
   */
  String e5512();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#COHESION_REFERENCE_CORRECTION_MISSING_POINT ImaerExceptionReason.COHESION_REFERENCE_CORRECTION_MISSING_POINT
   */
  String e5513();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#COHESION_DISPERSION_LINE_NOT_PERPENDICULAR ImaerExceptionReason.COHESION_DISPERSION_LINE_NOT_PERPENDICULAR
   */
  String e5514();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#COHESION_ROAD_MISSING_DISPERSION_LINE ImaerExceptionReason.COHESION_ROAD_MISSING_DISPERSION_LINE
   */
  String e5515();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#COHESION_REFERENCE_MISSING_BUILDING ImaerExceptionReason.COHESION_REFERENCE_MISSING_BUILDING
   */
  String e5521();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#OPS_INTERNAL_EXCEPTION AeriusExceptionReason.OPS_INTERNAL_EXCEPTION
   */
  String e6101();

  /**
   *
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#OPS_INPUT_VALIDATION AeriusExceptionReason.OPS_INPUT_VALIDATION
   */
  String e6102();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#SRM2IO_EXCEPTION_NO_ROAD_PROPERTIES AeriusExceptionReason.SRM2IO_EXCEPTION_NO_ROAD_PROPERTIES
   */
  String e6201();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#SRM2_TO_MANY_ROAD_SEGMENTS AeriusExceptionReason.SRM2_TO_MANY_ROAD_SEGMENTS
   */
  String e6202();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#SRM2_NO_PRESRM_DATA_FOR_YEAR AeriusExceptionReason.SRM2_NO_PRESRM_DATA_FOR_YEAR
   */
  String e6203();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#SRM2_MISSING_COLUMN_HEADER AeriusExceptionReason.SRM2_MISSING_COLUMN_HEADER
   */
  String e6204();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#SRM2_INCORRECT_EXPECTED_VALUE AeriusExceptionReason.SRM2_INCORRECT_EXPECTED_VALUE
   */
  String e6205();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#SRM2_INCORRECT_WKT_VALUE AeriusExceptionReason.SRM2_INCORRECT_WKT_VALUE
   */
  String e6206();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#NSL_LEGACY_FILESUPPORT_WILL_BE_REMOVED AeriusExceptionReason.NSL_LEGACY_FILESUPPORT_WILL_BE_REMOVED
   */
  String e6207();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#SRM2_SOURCE_TUNNEL_FACTOR_ZERO ImaerExceptionReason.SRM2_SOURCE_TUNNEL_FACTOR_ZERO
   */
  String e6209();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#SRM_LEGACY_INVALID_ROAD_TYPE ImaerExceptionReason.SRM_LEGACY_INVALID_ROAD_TYPE
   */
  String e6211();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#SRM_LEGACY_INVALID_SPEED_TYPE ImaerExceptionReason.SRM_LEGACY_INVALID_SPEED_TYPE
   */
  String e6212();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#SRM_LEGACY_INVALID_TREE_FACTOR ImaerExceptionReason.SRM_LEGACY_INVALID_TREE_FACTOR
   */
  String e6213();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#SRM_NO_MONITOR_SRM2_DATA_FOR_YEAR ImaerExceptionReason.SRM_NO_MONITOR_SRM2_DATA_FOR_YEAR
   */
  String e6214();

  /**
   * @see nl.overheid.aerius.shared.exception.ImaerExceptionReason#SRM_MEASURE_RECORDS_DID_NOT_MATCH ImaerExceptionReason.SRM_MEASURE_RECORDS_DID_NOT_MATCH
   */
  String e6215();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CSV_INCORRECT_ENUM_VALUE AeriusExceptionReason.CSV_INCORRECT_ENUM_VALUE
   */
  String e6216();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CSV_ID_ADJUSTED AeriusExceptionReason.CSV_ID_ADJUSTED
   */
  String e6217();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#SRM_UNSUPPORTED_METEO AeriusExceptionReason.SRM_UNSUPPORTED_METEO
   */
  String e6218();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#JSON_IO_EXCEPTION_ERROR AeriusExceptionReason.JSON_IO_EXCEPTION_ERROR
   */
  String e6219();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#ADMS_INTERNAL_EXCEPTION AeriusExceptionReason.ADMS_INTERNAL_EXCEPTION
   */
  String e6301();

  /**
   *
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#ADMS_INPUT_VALIDATION AeriusExceptionReason.ADMS_INPUT_VALIDATION
   */
  String e6302();

  /**
   *
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#ADMS_NO_ROAD_FOR_BARRIER AeriusExceptionReason.ADMS_NO_ROAD_FOR_BARRIER
   */
  String e6303();

  /**
   *
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#ADMS_CALCULATION_OPTIONS_MET_SITE_ID_MISSING AeriusExceptionReason.ADMS_CALCULATION_OPTIONS_MET_SITE_ID_MISSING
   */
  String e6304();

  /**
   *
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#ADMS_GEO_DATA_OUTSIDE_EXTENT AeriusExceptionReason.ADMS_GEO_DATA_OUTSIDE_EXTENT
   */
  String e6305();

  /**
   *
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#ADMS_DEFAULT_EPSG AeriusExceptionReason.ADMS_DEFAULT_EPSG
   */
  String e6306();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#USER_ALREADY_EXISTS AeriusExceptionReason.USER_ALREADY_EXISTS
   */
  String e40002();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#USER_EMAIL_ADDRESS_ALREADY_EXISTS AeriusExceptionReason.USER_EMAIL_ADDRESS_ALREADY_EXISTS
   */
  String e40005();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#USER_API_KEY_ALREADY_EXISTS AeriusExceptionReason.USER_API_KEY_ALREADY_EXISTS
   */
  String e40006();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#USER_INVALID_API_KEY AeriusExceptionReason.USER_INVALID_API_KEY
   */
  String e40007();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#USER_API_KEY_GENERATION_DISABLED AeriusExceptionReason.USER_API_KEY_GENERATION_DISABLED
   */
  String e40008();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#USER_MAX_CONCURRENT_JOB_LIMIT_REACHED AeriusExceptionReason.USER_MAX_CONCURRENT_JOB_LIMIT_REACHED
   */
  String e40009();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#USER_ACCOUNT_DISABLED AeriusExceptionReason.USER_ACCOUNT_DISABLED
   */
  String e40010();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#USER_PERIOD_JOB_RATE_LIMIT_REACHED AeriusExceptionReason.USER_PERIOD_JOB_RATE_LIMIT_REACHED
   */
  String e40012();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_NO_VALID_EMAIL_SUPPLIED AeriusExceptionReason.CONNECT_NO_VALID_EMAIL_SUPPLIED
   */
  String e50001();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_INCORRECT_CALCULATIONYEAR AeriusExceptionReason.CONNECT_INCORRECT_CALCULATIONYEAR
   */
  String e50002();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_NO_CALCULATION_METHOD_SUPPLIED AeriusExceptionReason.CONNECT_NO_CALCULATION_METHOD_SUPPLIED
   */
  String e50003();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_NO_SUBSTANCE_SUPPLIED AeriusExceptionReason.CONNECT_NO_SUBSTANCE_SUPPLIED
   */
  String e50004();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_INVALID_CALCULATION_RANGE AeriusExceptionReason.CONNECT_INVALID_CALCULATION_RANGE
   */
  String e50005();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_INVALID_TEMPPROJECT_RANGE AeriusExceptionReason.CONNECT_INVALID_TEMPPROJECT_RANGE
   */
  String e50006();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_NO_SOURCES AeriusExceptionReason.CONNECT_NO_SOURCES
   */
  String e50007();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_SITUATION_NO_PROPOSED AeriusExceptionReason.CONNECT_SITUATION_NO_PROPOSED
   */
  String e50008();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_CALCULATION_METHOD_SUPPLIED_NOT_SUPPORTED AeriusExceptionReason.CONNECT_CALCULATION_METHOD_SUPPLIED_NOT_SUPPORTED
   */
  String e50009();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_REPORT_PERMIT_DEMAND_COMPARISON_NOT_SUPPORTED AeriusExceptionReason.CONNECT_REPORT_PERMIT_DEMAND_COMPARISON_NOT_SUPPORTED
   */
  String e50010();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_UNKNOWN_SUBSTANCE_SUPPLIED AeriusExceptionReason.CONNECT_UNKNOWN_SUBSTANCE_SUPPLIED
   */
  String e50011();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_USER_JOBKEY_DOES_NOT_EXIST AeriusExceptionReason.CONNECT_USER_JOBKEY_DOES_NOT_EXIST
   */
  String e50012();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_JOB_CANCELLED AeriusExceptionReason.CONNECT_JOB_CANCELLED
   */
  String e50013();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_NO_RECEPTORS_IN_PARAMETERS AeriusExceptionReason.CONNECT_NO_RECEPTORS_IN_PARAMETERS
   */
  String e50014();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_USER_CALCULATION_POINT_SET_ALREADY_EXISTS AeriusExceptionReason.CONNECT_USER_CALCULATION_POINT_SET_ALREADY_EXISTS
   */
  String e50015();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_USER_CALCULATION_POINT_SET_DOES_NOT_EXIST AeriusExceptionReason.CONNECT_USER_CALCULATION_POINT_SET_DOES_NOT_EXIST
   */
  String e50016();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_UNSUPPORTED_DATATYPE_IN_OPERATION AeriusExceptionReason.CONNECT_UNSUPPORTED_DATATYPE_IN_OPERATION
   */
  String e50017();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_UNSUPPORTED_PAS_OPTIONS AeriusExceptionReason.CONNECT_UNSUPPORTED_PAS_OPTIONS
   */
  String e50018();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_VALIDATION_SKIPPED_WARNING AeriusExceptionReason.CONNECT_VALIDATION_SKIPPED_WARNING
   */
  String e50019();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_INVALID_OUTPUTTYPE AeriusExceptionReason.CONNECT_INVALID_OUTPUTTYPE
   */
  String e50020();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_INVALID_METEO AeriusExceptionReason.CONNECT_INVALID_METEO
   */
  String e50021();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_MISSING_RECEPTOR_HEIGHT AeriusExceptionReason.CONNECT_MISSING_RECEPTOR_HEIGHT
   */
  String e50022();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_UNEXPECTED_RECEPTOR_HEIGHT AeriusExceptionReason.CONNECT_UNEXPECTED_RECEPTOR_HEIGHT
   */
  String e50023();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_NO_CUSTOM_POINTS AeriusExceptionReason.CONNECT_NO_CUSTOM_POINTS
   */
  String e50024();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_NO_SITUATION_TYPE_DETECTED AeriusExceptionReason.CONNECT_NO_SITUATION_TYPE_DETECTED
   */
  String e50025();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_INVALID_ZOOM_LEVEL AeriusExceptionReason.CONNECT_INVALID_ZOOM_LEVEL
   */
  String e50026();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_OPTION_COMBINATION_NOT_ALLOWED AeriusExceptionReason.CONNECT_OPTION_COMBINATION_NOT_ALLOWED
   */
  String e50027();

  /**
   * @see nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_USER_CALCULATION_POINT_SET_EMPTY_NAME AeriusExceptionReason.CONNECT_USER_CALCULATION_POINT_SET_EMPTY_NAME
   */
  String e50028();

}
