/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.shared.domain.v2.source.shipping.maritime.ShippingMovementType;
import nl.overheid.aerius.wui.application.domain.source.shipping.maritime.MaritimeShipping;

/**
 * Feature object for Maritime Maritime Shipping Emission Sources.
 */
@JsType(namespace = GLOBAL, name = "ol.Feature", isNative = true)
public class MaritimeMaritimeShippingESFeature extends MaritimeShippingESFeature {
  /**
   * @return Returns Feature {@link InlandMaritimeShippingESFeature} object.
   */
  public static @JsOverlay MaritimeMaritimeShippingESFeature create() {
    final MaritimeMaritimeShippingESFeature feature = new MaritimeMaritimeShippingESFeature();
    init(feature);
    return feature;
  }

  public static @JsOverlay void init(final MaritimeMaritimeShippingESFeature feature) {
    MaritimeShippingESFeature.initMaritime(feature, EmissionSourceType.SHIPPING_MARITIME_MARITIME, ShippingMovementType.MARITIME);
    EmissionSubSourceFeature.initSubSources(feature, v -> MaritimeShipping.initTypeDependent(v, v.getMaritimeShippingType()));
  }
}
