/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.development;

import ol.Coordinate;
import ol.geom.Geometry;
import ol.geom.LineString;
import ol.geom.Point;
import ol.geom.Polygon;

import nl.overheid.aerius.shared.domain.v2.geojson.GeometryType;

public class DeterministicGeometry {

  private DeterministicGeometry() {}

  public static Geometry get(final HashNumber num) {
    return get(num, GeometryType.POINT);
  }

  public static Geometry get(final HashNumber num, final GeometryType geometryType) {
    switch (geometryType) {
    case POLYGON:
      final double xP = getX(num);
      final double yP = getY(num);

      final double length = getLength(num) * 2;
      final double width = getLength(num);

      final double fuzz = getLength(num) - 30;

      return new Polygon(new Coordinate[][] {
          { rCd(xP, yP, 0, 0), rCd(xP, yP, length + fuzz, fuzz), rCd(xP, yP, length, width + fuzz), rCd(xP, yP, fuzz, width), rCd(xP, yP, 0, 0) } });
    case LINESTRING:
      final double xL = getX(num);
      final double yL = getY(num);
      return new LineString(new Coordinate[] { rCd(xL, yL, 0, 0), rCd(xL, yL, 250, 150), rCd(xL, yL, 420, 60) });
    default:
      return new Point(rCd(getX(num), getY(num), 0, 0));
    }
  }

  private static double getLength(final HashNumber num) {
    return 10D + num.nextInt(50);
  }

  private static double getX(final HashNumber num) {
    return 100000D + num.nextInt(200000);
  }

  private static double getY(final HashNumber num) {
    return 400000D + num.nextInt(200000);
  }

  private static Coordinate rCd(final double x, final double y, final double oX, final double oY) {
    return new Coordinate(x + oX, y + oY);
  }
}
