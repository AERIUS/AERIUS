/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import jsinterop.annotations.JsProperty;

public abstract class SimpleListContext<T> {
  protected final @JsProperty List<T> selections = new ArrayList<>();
  protected T peekItem = null;

  public T getLooseSelection() {
    return Optional.ofNullable(getSingleSelection())
        .orElse(peekItem);
  }

  public boolean hasSingleSelection() {
    return selections.size() == 1;
  }

  public boolean hasMultiSelection() {
    return selections.size() > 1;
  }

  public boolean hasSelection() {
    return !selections.isEmpty();
  }

  public T getSingleSelection() {
    return selections.isEmpty() ? null : selections.get(0);
  }

  public List<T> getSelections() {
    return selections;
  }

  public void addSelection(final T selection) {
    selections.add(selection);
  }

  public void removeSelection(final T selection) {
    selections.remove(selection);
    if (peekItem == selection) {
      peekItem = null;
    }
  }

  public void setSingleSelection(final T selection) {
    this.selections.clear();
    addSelection(selection);
  }

  public boolean hasLooseSelection() {
    return getLooseSelection() != null;
  }

  public void reset() {
    // Note: If expanding with more resets, make sure assumptions are still met
    selections.clear();
    peekItem = null;
  }

  public abstract boolean isSelected(final T item);

  public void setPeekSelection(final T peekSelection) {
    this.peekItem = peekSelection;
  }

  public void clearPeekSelection(final T peekSelection) {
    if (this.peekItem != null && this.peekItem.equals(peekSelection)) {
      this.peekItem = null;
    }
  }
}
