/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.print.own2000.source.farmland;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.FarmlandCategory;
import nl.overheid.aerius.wui.application.components.source.farmland.detail.FarmlandDetailComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.farmland.FarmlandActivity;
import nl.overheid.aerius.wui.application.domain.source.farmland.FarmlandUtil;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.application.resources.util.FarmIconUtil;

@Component(name = "farmland-detail-print")
public class FarmlandDetailPrintComponent extends FarmlandDetailComponent {

  @Inject ApplicationContext applicationContext;

  @JsMethod
  public String formatEmission(final double emission) {
    return MessageFormatter.formatEmissionWithUnitSmart(emission);
  }

  @JsMethod
  public String getIcon(final FarmlandActivity activity) {
    return FarmIconUtil.getFarmlandIcon(activity.getActivityCode());
  }

  @JsMethod
  public String getDescription(final FarmlandActivity activity) {
    final FarmlandCategory selectedFarmlandCategory = FarmlandUtil.findFarmland(activity.getActivityCode(),
        applicationContext.getConfiguration().getSectorCategories().getFarmlandCategories());
    return selectedFarmlandCategory != null ? selectedFarmlandCategory.getDescription() : "";
  }

  @Override
  @Computed
  public List<Substance> getSubstances() {
    return applicationContext.getConfiguration().getSubstances();
  }
}
