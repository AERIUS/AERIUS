/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.print.own2000.source.offroad;

import com.axellience.vuegwt.core.annotations.component.Component;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.wui.application.components.source.offroad.detail.OffroadDetailComponent;
import nl.overheid.aerius.wui.application.domain.source.offroad.OffRoadMobileSource;
import nl.overheid.aerius.wui.application.domain.source.offroad.OffRoadType;
import nl.overheid.aerius.wui.application.domain.source.offroad.StandardOffRoadMobileSource;

@Component
public class OffroadDetailPrintComponent extends OffroadDetailComponent {

  @JsMethod
  public boolean hasFuel(final OffRoadMobileSource subSource) {
    return applicationContext.getConfiguration().getSectorCategories()
        .determineOffRoadMobileSourceCategoryByCode(((StandardOffRoadMobileSource) subSource).getOffRoadMobileSourceCode())
        .expectsLiterFuel();
  }

  @JsMethod
  public boolean hasOperatingHours(final OffRoadMobileSource subSource) {
    return applicationContext.getConfiguration().getSectorCategories()
        .determineOffRoadMobileSourceCategoryByCode(((StandardOffRoadMobileSource) subSource).getOffRoadMobileSourceCode())
        .expectsOperatingHours();
  }

  @JsMethod
  public boolean hasAdBlue(final OffRoadMobileSource subSource) {
    return applicationContext.getConfiguration().getSectorCategories()
        .determineOffRoadMobileSourceCategoryByCode(((StandardOffRoadMobileSource) subSource).getOffRoadMobileSourceCode())
        .expectsLiterAdBlue();
  }

  @JsMethod
  public String getCorrectedAdBlueTotal(final OffRoadMobileSource subSource) {
    final StandardOffRoadMobileSource standardOffRoadMobileSource = (StandardOffRoadMobileSource) subSource;
    final int literAdBluePerYear = standardOffRoadMobileSource.getLiterAdBluePerYear();
    final double correctedAdBlueUsage = getMaxAdBlueFuelRatio(standardOffRoadMobileSource) * standardOffRoadMobileSource.getLiterFuelPerYear();

    if (literAdBluePerYear > correctedAdBlueUsage) {
      return " (" + i18n.decimalNumberCapped(correctedAdBlueUsage, 0) + ")";
    }
    return "";
  }

  @JsMethod
  public boolean isStandard(final OffRoadMobileSource source) {
    return source.getOffroadType() == OffRoadType.STANDARD;
  }

  @JsMethod
  public String getOffRoadMobileSourceCode(final OffRoadMobileSource source) {
    return applicationContext.getConfiguration().getSectorCategories()
        .determineOffRoadMobileSourceCategoryByCode(((StandardOffRoadMobileSource) source).getOffRoadMobileSourceCode())
        .getDescription();
  }

  @JsMethod
  public String getFuel(final OffRoadMobileSource source) {
    return String.valueOf(((StandardOffRoadMobileSource) source).getLiterFuelPerYear());
  }

  @JsMethod
  public String getAdBlue(final OffRoadMobileSource source) {
    return String.valueOf(((StandardOffRoadMobileSource) source).getLiterAdBluePerYear());
  }

  @JsMethod
  public String getOperatingHours(final OffRoadMobileSource source) {
    return String.valueOf(((StandardOffRoadMobileSource) source).getOperatingHoursPerYear());
  }

  private double getMaxAdBlueFuelRatio(final StandardOffRoadMobileSource source) {
    return applicationContext.getConfiguration().getSectorCategories()
        .determineOffRoadMobileSourceCategoryByCode(source.getOffRoadMobileSourceCode())
        .getMaxAdBlueFuelRatio();
  }
}
