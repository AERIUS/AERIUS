/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.place;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import nl.aerius.wui.place.ApplicationPlace;
import nl.aerius.wui.place.PlaceTokenizer;
import nl.overheid.aerius.shared.domain.PdfProductType;

public final class PrintPlace extends ApplicationPlace {

  private static final String PREFIX = "print";

  public static class Tokenizer extends ApplicationPlace.Tokenizer<PrintPlace> {
    public Tokenizer(final Supplier<PrintPlace> supplier) {
      super(supplier, prepend(PREFIX, new String[0]));
    }

    @Override
    protected void updatePlace(final Map<String, String> tokens, final PrintPlace place) {
      place.tokens.putAll(tokens);
    }

    @Override
    protected void setTokenMap(final PrintPlace place, final Map<String, String> tokens) {
      tokens.putAll(place.tokens);
    }
  }

  private final Map<String, String> tokens = new HashMap<>();

  private PrintPlace() {
    super(createTokenizer());
  }

  public static PlaceTokenizer<PrintPlace> createTokenizer() {
    return new Tokenizer(() -> new PrintPlace());
  }

  public String getLocale() {
    return tokens.get("locale");
  }

  public String getJobKey() {
    return tokens.get("jobKey");
  }

  public String getProposedSituationId() {
    return tokens.get("proposedSituationId");
  }

  public String getReference() {
    return tokens.get("reference");
  }

  public PdfProductType getPdfProductType() {
    return PdfProductType.safeValueOf(tokens.get("pdfProductType"));
  }

  public Map<String, String> getTokens() {
    return tokens;
  }
}
