/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.domain.source.road.ADMSRoadSideBarrier;
import nl.overheid.aerius.wui.application.domain.source.road.Vehicles;
import nl.overheid.aerius.wui.vue.ReactivityUtil;

/**
 * Feature object for ADMS Road Emission Sources.
 */
@JsType(namespace = GLOBAL, name = "ol.Feature", isNative = true)
public class ADMSRoadESFeature extends RoadESFeature {
  /**
   * @return Returns Feature {@link ADMSRoadESFeature} object.
   */
  public static @JsOverlay ADMSRoadESFeature create() {
    final ADMSRoadESFeature feature = new ADMSRoadESFeature();
    init(feature);
    return feature;
  }

  public static @JsOverlay void init(final ADMSRoadESFeature feature) {
    RoadESFeature.initRoadFeature(feature, CharacteristicsType.ADMS, EmissionSourceType.ADMS_ROAD);
    ReactivityUtil.ensureDefault(feature::getWidth, feature::setWidth, 2D);
    ReactivityUtil.ensureDefault(feature::getElevation, feature::setElevation, 0D);
    ReactivityUtil.ensureDefault(feature::getGradient, feature::setGradient, 0D);
    ReactivityUtil.ensureDefault(feature::getCoverage, feature::setCoverage, 0D);
    ReactivityUtil.ensureDefault(feature::getBarrierLeft, feature::setBarrierLeft);
    ReactivityUtil.ensureInitialized(feature::getBarrierLeft, ADMSRoadSideBarrier::init);
    ReactivityUtil.ensureDefault(feature::getBarrierRight, feature::setBarrierRight);
    ReactivityUtil.ensureInitialized(feature::getBarrierRight, ADMSRoadSideBarrier::init);

    EmissionSubSourceFeature.initSubSources(feature, v -> Vehicles.initTypeDependent(v, v.getVehicleType()));
  }

  public final @JsOverlay Double getWidth() {
    return get("width") == null ? 0 : Js.coerceToDouble(get("width"));
  }

  public final @JsOverlay void setWidth(final Double width) {
    set("width", width);
  }

  public final @JsOverlay Double getElevation() {
    return get("elevation") == null ? 0 : Js.coerceToDouble(get("elevation"));
  }

  public final @JsOverlay void setElevation(final Double elevation) {
    set("elevation", elevation);
  }

  public final @JsOverlay Double getGradient() {
    return get("gradient") == null ? 0 : Js.coerceToDouble(get("gradient"));
  }

  public final @JsOverlay void setGradient(final Double gradient) {
    set("gradient", gradient);
  }

  public final @JsOverlay Double getCoverage() {
    return get("coverage") == null ? 0 : Js.coerceToDouble(get("coverage"));
  }

  public final @JsOverlay void setCoverage(final Double coverage) {
    set("coverage", coverage);
  }

  public final @JsOverlay ADMSRoadSideBarrier getBarrierLeft() {
    return Js.uncheckedCast(get("barrierLeft"));
  }

  public final @JsOverlay void setBarrierLeft(final ADMSRoadSideBarrier barrierLeft) {
    set("barrierLeft", barrierLeft);
  }

  public final @JsOverlay ADMSRoadSideBarrier getBarrierRight() {
    return Js.uncheckedCast(get("barrierRight"));
  }

  public final @JsOverlay void setBarrierRight(final ADMSRoadSideBarrier barrierRight) {
    set("barrierRight", barrierRight);
  }

}
