/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.offroad;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;

import elemental2.core.JsArray;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.wui.application.components.input.MinimalInputComponent;
import nl.overheid.aerius.wui.application.components.source.offroad.standard.OffRoadMobileStandardEmissionComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.domain.source.OffRoadMobileESFeature;
import nl.overheid.aerius.wui.application.domain.source.offroad.CustomOffRoadMobileSource;
import nl.overheid.aerius.wui.application.domain.source.offroad.OffRoadMobileSource;
import nl.overheid.aerius.wui.application.domain.source.offroad.OffRoadType;
import nl.overheid.aerius.wui.application.domain.source.offroad.StandardOffRoadMobileSource;

@Component(components = {
    MinimalInputComponent.class,
    OffRoadMobileStandardEmissionComponent.class,
    ValidationBehaviour.class,
})
public class OffRoadMobileDetailEditorComponent extends ErrorWarningValidator {
  @Prop OffRoadMobileESFeature source;
  @Prop OffRoadMobileSource subSource;

  @Data OffRoadType toggleSelect;

  @Watch(value = "subSource", isImmediate = true)
  public void onValueChange(final Object neww, final Object old) {
    if (subSource != null) {
      toggleSelect = subSource.getOffroadType();
    }
  }

  @JsMethod
  public boolean isSourceAvailable() {
    return subSource != null;
  }

  @JsMethod
  public void setToggle(final OffRoadType type) {
    if (toggleSelect != type) {
      toggleSelect = type;
      final JsArray<OffRoadMobileSource> subSources = source.getSubSources();
      subSources.splice(subSources.indexOf(subSource), 1, createSubSource(type));
    }
  }

  private OffRoadMobileSource createSubSource(final OffRoadType type) {
    final OffRoadMobileSource newSubSource;

    if (type == OffRoadType.STANDARD) {
      newSubSource = StandardOffRoadMobileSource.create();
    } else {
      newSubSource = CustomOffRoadMobileSource.create();
    }
    return newSubSource;
  }
}
