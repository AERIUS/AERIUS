/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import javax.inject.Singleton;

import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;

@Singleton
public class BuildingEditorContext {
  private SituationContext situation = null;
  private BuildingFeature building = null;

  public BuildingFeature getBuilding() {
    return building;
  }

  public void setBuilding(final BuildingFeature building) {
    this.building = building;
  }

  public boolean isEditing() {
    return building != null;
  }

  public SituationContext getSituation() {
    return situation;
  }

  public void setSituation(final SituationContext situation) {
    this.situation = situation;
  }

  public void reset() {
    building = null;
    situation = null;
  }
}
