/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages;

import nl.aerius.wui.place.Place;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.wui.application.activity.DelegatedActivity;
import nl.overheid.aerius.wui.application.activity.DelegatedActivityManager;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.place.HomePlace;
import nl.overheid.aerius.wui.application.place.MainThemePlace;
import nl.overheid.aerius.wui.application.place.PlaceVisitor;

/**
 * Base class Delegation manager for Theme activity places.
 */
public class DelegatedThemeActivityManager extends DelegatedActivityManager<ThemeView> {

  private final PlaceVisitor<ThemeDelegatedActivity> placeVisitor;
  private final ScenarioContext context;
  private final Theme theme;

  public DelegatedThemeActivityManager(final PlaceVisitor<ThemeDelegatedActivity> placeVisitor, final ApplicationContext appContext,
      final ScenarioContext context, final Theme theme) {
    this.placeVisitor = placeVisitor;
    this.context = context;
    this.theme = theme;
  }

  @Override
  public DelegatedActivity<ThemeView> getActivity(final Place place) {
    return place instanceof MainThemePlace && ((MainThemePlace) place).getTheme() == theme
        ? ((MainThemePlace) place).visit(placeVisitor)
        : null;
  }

  @Override
  protected Place getRedirect(final Place place) {
    final boolean redirectOnNoSituations = place instanceof MainThemePlace
        && !(place instanceof HomePlace)
        && context.getSituations().isEmpty()
        && context.getCalculationPoints().isEmpty();

    return redirectOnNoSituations ? new HomePlace(theme) : null;
  }
}
