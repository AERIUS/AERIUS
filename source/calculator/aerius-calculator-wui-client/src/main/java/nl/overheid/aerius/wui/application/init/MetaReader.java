/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.init;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NodeList;

/**
 * Util class to read a specific HTML meta tag value.
 */
final class MetaReader {

  private MetaReader() {
    // Util class.
  }

  /**
   * reads the content attribute of the HTML meta element with the name matching the give metaName.
   *
   * @param metaName name of the HTML meta element to read
   * @return returns the content attribute or null if not found
   */
  public static String readMetaData(final String metaName) {
    final NodeList<Element> node = Document.get().getElementsByTagName("meta");
    for (int i = 0; i < node.getLength(); i++) {
      final Element meta = node.getItem(i);
      final String name = meta.getAttribute("name");

      if (name != null && name.equals(metaName)) {
        return meta.getAttribute("content");
      }
    }
    return null;
  }
}
