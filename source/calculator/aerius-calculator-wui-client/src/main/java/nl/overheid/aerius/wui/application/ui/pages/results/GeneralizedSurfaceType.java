/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.results;

import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;

/**
 * A client-side only enum that can be used to decide what statistic/label to use when surface statistics are asked for.
 * In most cases this is (cartographic) surface, but for some it'll be number of receptors/hexagons or number of calculation points.
 */
public enum GeneralizedSurfaceType {
  POINTS(ResultStatisticType.COUNT_CALCULATION_POINTS),
  RECEPTORS(ResultStatisticType.COUNT_RECEPTORS),
  SURFACE(ResultStatisticType.SUM_CARTOGRAPHIC_SURFACE);

  private final ResultStatisticType statisticType;

  GeneralizedSurfaceType(final ResultStatisticType statisticType) {
    this.statisticType = statisticType;
  }

  public ResultStatisticType getStatisticType() {
    return statisticType;
  }

  public static GeneralizedSurfaceType from(final EmissionResultType emissionType, final SummaryHexagonType hexagonType) {
    return from(GeneralizedEmissionResultType.fromEmissionResultType(emissionType), hexagonType);
  }

  public static GeneralizedSurfaceType from(final GeneralizedEmissionResultType emissionType, final SummaryHexagonType hexagonType) {
    final boolean showPoints = emissionType == GeneralizedEmissionResultType.CONCENTRATION;
    if (showPoints) {
      return POINTS;
    }
    final boolean showHexagons = hexagonType == SummaryHexagonType.EXTRA_ASSESSMENT_HEXAGONS;
    return showHexagons
        ? RECEPTORS
        : SURFACE;
  }
}
