/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.service;

import java.util.List;
import java.util.Optional;

import ol.format.GeoJson;
import ol.format.GeoJsonFeatureOptions;

import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.util.json.JsonBuilder;
import nl.overheid.aerius.wui.application.util.json.JsonSerializable;

/**
 * Support object for the request of determining calculation points
 */
@JsType
public class CalculationPointDetermineRequest implements JsonSerializable {

  private final @JsProperty List<EmissionSourceFeature> sources;
  private final @JsProperty boolean pointsAbroad;
  private final @JsProperty Integer radiusInland;

  public CalculationPointDetermineRequest(final List<EmissionSourceFeature> sources, final boolean pointsAbroad, final Integer radiusInland) {
    this.sources = sources;
    this.pointsAbroad = pointsAbroad;
    this.radiusInland = radiusInland;
  }

  public List<EmissionSourceFeature> getSources() {
    return sources;
  }

  public boolean isPointsAbroad() {
    return pointsAbroad;
  }

  public Integer getRadiusInland() {
    return radiusInland;
  }

  @Override
  public Object toJSON() {
    final JsonBuilder jsonBuilder = new JsonBuilder(this)
        .set("sources", new GeoJson().writeFeaturesObject(sources.toArray(new EmissionSourceFeature[sources.size()]), new GeoJsonFeatureOptions()))
        .set("radiusInland", Optional.ofNullable(radiusInland).map(Integer::doubleValue).orElse(null)) // use Double to prevent serialized Integer boxed type
        .include("pointsAbroad");

    return jsonBuilder.build();
  }
}
