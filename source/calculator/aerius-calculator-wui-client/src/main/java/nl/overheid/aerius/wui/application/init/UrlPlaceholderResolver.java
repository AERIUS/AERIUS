/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.init;

import java.util.stream.Stream;

import com.google.inject.Inject;

import nl.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.wui.application.config.EnvironmentConfigurationWrapper;
import nl.overheid.aerius.wui.config.ApplicationFlags;

public class UrlPlaceholderResolver {
  private static final String AERIUS_GEOSERVER_HOST_PLACEHOLDER_TOKEN = "[AERIUS_GEOSERVER_HOST]";
  private final EnvironmentConfigurationWrapper environmentConfiguration = new EnvironmentConfigurationWrapper();

  @Inject ApplicationFlags flags;

  public void replaceGeoserverPlaceholders(final ApplicationConfiguration configuration) {
    final String internalGeoserverUrl = configuration.getSettings().getSetting(SharedConstantsEnum.GEOSERVER_INTERNAL_URL);

    Stream.concat(
        configuration.getLayers().stream(),
        configuration.getBaseLayers().stream())
        .forEach(props -> replaceGeoserverPlaceholders(props, internalGeoserverUrl));
  }

  private void replaceGeoserverPlaceholders(final LayerProps props, final String internalGeoserverUrl) {
    if (AERIUS_GEOSERVER_HOST_PLACEHOLDER_TOKEN.equals(props.getUrl())) {
      props.setUrl(flags.isInternal() ? internalGeoserverUrl : environmentConfiguration.geoserverBaseUrl);
    }
  }
}
