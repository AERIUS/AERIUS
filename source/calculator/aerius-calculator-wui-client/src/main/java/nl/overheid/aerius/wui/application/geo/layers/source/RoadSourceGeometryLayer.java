/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;

import ol.OLFactory;
import ol.geom.LineString;
import ol.geom.Polygon;
import ol.style.Fill;
import ol.style.Stroke;
import ol.style.Style;

import nl.aerius.geo.domain.LayerInfo;
import nl.aerius.geo.domain.LayerOptions;
import nl.aerius.geo.domain.legend.Legend;
import nl.aerius.geo.wui.util.OL3GeometryUtil;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.constants.DrivingSide;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.SimpleCategory;
import nl.overheid.aerius.shared.domain.source.road.RoadLayerStyle;
import nl.overheid.aerius.wui.application.context.EmissionSourceListContext;
import nl.overheid.aerius.wui.application.context.RoadNetworkContext;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.road.RoadEmissionSourceTypes;
import nl.overheid.aerius.wui.application.geo.layers.source.road.RoadBarrierHeightStyle;
import nl.overheid.aerius.wui.application.geo.layers.source.road.RoadBarrierPorosityStyle;
import nl.overheid.aerius.wui.application.geo.layers.source.road.RoadBarrierTypeStyle;
import nl.overheid.aerius.wui.application.geo.layers.source.road.RoadElevationHeightStyle;
import nl.overheid.aerius.wui.application.geo.layers.source.road.RoadElevationTypeStyle;
import nl.overheid.aerius.wui.application.geo.layers.source.road.RoadEmissionStyle;
import nl.overheid.aerius.wui.application.geo.layers.source.road.RoadMaxSpeedStyle;
import nl.overheid.aerius.wui.application.geo.layers.source.road.RoadStagnationStyle;
import nl.overheid.aerius.wui.application.geo.layers.source.road.RoadStyle;
import nl.overheid.aerius.wui.application.geo.layers.source.road.RoadTrafficVolumeStyle;
import nl.overheid.aerius.wui.application.geo.layers.source.road.RoadTunnelFactorStyle;
import nl.overheid.aerius.wui.application.geo.layers.source.road.RoadTypeStyle;
import nl.overheid.aerius.wui.application.i18n.M;

/**
 * Geometry layer to display road sources on the map styling depending on what attributes of a road are selected to be shown.
 */
public class RoadSourceGeometryLayer extends SourceGeometryLayer {

  public static final int DEFAULT_ROAD_WIDTH = 4;

  private static final Stroke GRAY_STROKE = OLFactory.createStroke(OLFactory.createColor(128, 128, 128, 1), 2);
  private static final Stroke THEME_STROKE = OLFactory.createStroke(OLFactory.createColor(24, 124, 178, 1), 2);

  private static final Fill WHITE_FILL = OLFactory.createFill(OLFactory.createColor(255, 255, 255, 1));

  private static final Style ROAD_SELECTED_STYLE = OLFactory.createStyle(WHITE_FILL, THEME_STROKE);
  private static final Style ROAD_HIGHLIGHTED_STYLE = OLFactory.createStyle(WHITE_FILL, GRAY_STROKE);
  private static final double ROAD_SELECTION_OFFSET_DISTANCE = 8.0;
  private static final List<Substance> ROAD_EMISSION_SUBSTANCES = Arrays.asList(Substance.NOX, Substance.NH3);

  private final Map<RoadLayerStyle, RoadStyle> roadStyles = new HashMap<>();

  private @Inject EmissionSourceListContext selectionContext;
  private @Inject RoadNetworkContext roadNetworkContext;

  private final ApplicationConfiguration applicationConfiguration;
  private final RoadTrafficVolumeStyle roadTrafficVolumeStyle;
  private final RoadEmissionStyle roadEmissionStyle;

  private RoadLayerStyle roadLayerType;
  private String vehicleTypeCode;

  @Inject
  public RoadSourceGeometryLayer(final EventBus eventBus, @Assisted final ApplicationConfiguration applicationConfiguration,
      @Assisted final DrivingSide drivingSide, @Assisted final int zIndex) {
    super(new LayerInfo(), eventBus, zIndex);
    this.applicationConfiguration = applicationConfiguration;

    addRoadStyleBasedOnTheme(RoadLayerStyle.ROAD_TYPE,
        () -> new RoadTypeStyle(applicationConfiguration::getColorItems, applicationConfiguration.getSectorCategories()), applicationConfiguration);
    addRoadStyleBasedOnTheme(RoadLayerStyle.MAX_SPEED, () -> new RoadMaxSpeedStyle(applicationConfiguration::getColorRange),
        applicationConfiguration);
    addRoadStyleBasedOnTheme(RoadLayerStyle.STAGNATION, () -> new RoadStagnationStyle(), applicationConfiguration);
    roadTrafficVolumeStyle = addRoadStyleBasedOnTheme(RoadLayerStyle.TRAFFIC_VOLUME,
        () -> new RoadTrafficVolumeStyle(applicationConfiguration::getColorRange, applicationConfiguration, drivingSide), applicationConfiguration);
    addRoadStyleBasedOnTheme(RoadLayerStyle.ROAD_BARRIER_TYPE,
        () -> new RoadBarrierTypeStyle(applicationConfiguration::getColorItems), applicationConfiguration);
    addRoadStyleBasedOnTheme(RoadLayerStyle.ROAD_BARRIER_HEIGHT,
        () -> new RoadBarrierHeightStyle(applicationConfiguration::getColorRange), applicationConfiguration);
    addRoadStyleBasedOnTheme(RoadLayerStyle.ROAD_BARRIER_POROSITY,
        () -> new RoadBarrierPorosityStyle(applicationConfiguration::getColorRange), applicationConfiguration);
    roadEmissionStyle = addRoadStyleBasedOnTheme(RoadLayerStyle.ROAD_EMISSION,
        () -> new RoadEmissionStyle(applicationConfiguration::getColorRange, applicationConfiguration.getRoadEmissionDisplayConversionFactor()),
        applicationConfiguration);
    addRoadStyleBasedOnTheme(RoadLayerStyle.ELEVATION_TYPE, () -> new RoadElevationTypeStyle(applicationConfiguration::getColorItems),
        applicationConfiguration);
    addRoadStyleBasedOnTheme(RoadLayerStyle.ELEVATION_HEIGHT, () -> new RoadElevationHeightStyle(applicationConfiguration::getColorRange),
        applicationConfiguration);
    addRoadStyleBasedOnTheme(RoadLayerStyle.TUNNEL_FACTOR, () -> new RoadTunnelFactorStyle(applicationConfiguration::getColorRange),
        applicationConfiguration);

    roadLayerType = applicationConfiguration.getRoadLayerStyle().isEmpty() ? RoadLayerStyle.ROAD_TYPE
        : applicationConfiguration.getRoadLayerStyle().get(0);
    vehicleTypeCode = applicationConfiguration.getSectorCategories().getRoadEmissionCategories().getVehicleTypes().isEmpty()
        ? ""
        : applicationConfiguration.getSectorCategories().getRoadEmissionCategories().getVehicleTypes().get(0).getCode();

    final LayerInfo info = getInfo();

    info.setName(getClass().getCanonicalName());
    info.setTitle(M.messages().esRoadNetwork());
    info.setOptions(getLayerOptions());
    updateLegend();
  }

  private <R extends RoadStyle> R addRoadStyleBasedOnTheme(final RoadLayerStyle roadType, final Supplier<R> roadTypeStyle,
      final ApplicationConfiguration applicationConfiguration) {
    if (applicationConfiguration.getRoadLayerStyle().contains(roadType)) {
      final R roadStyle = roadTypeStyle.get();

      roadStyles.put(roadType, roadStyle);
      return roadStyle;
    }
    return null;
  }

  private void setRoadLayerType(final RoadLayerStyle roadLayerType) {
    this.roadLayerType = roadLayerType;
    getInfo().setOptions(getLayerOptions());
    if (roadLayerType.dependsOnVehicleType()) {
      setVehicleType(defaultVehicleType().getCode());
    } else {
      updateLegend();
      asLayer().getSource().changed();
    }
  }

  public void setVehicleType(final String vehicleTypeCode) {
    this.vehicleTypeCode = vehicleTypeCode;
    if (roadTrafficVolumeStyle != null) {
      roadTrafficVolumeStyle.updateVehicleType(vehicleTypeCode);
    }
    updateLegend();
    asLayer().getSource().changed();
  }

  private void updateLegend() {
    getInfo().setLegend(getLegend());
  }

  public Legend getLegend() {
    return roadStyles.get(roadLayerType).getLegend(applicationConfiguration);
  }

  private LayerOptions[] getLayerOptions() {
    final List<LayerOptions> options = new ArrayList<>();

    addLayerOptions(options);
    addSubLayerOptions(options);
    addVehicleTypeOptions(options);
    addSubstanceOptions(options);

    return options.toArray(new LayerOptions[0]);
  }

  private void addLayerOptions(final List<LayerOptions> options) {
    final List<RoadLayerStyle> topLevelStyles = applicationConfiguration.getRoadLayerStyle()
        .stream().filter(rls -> !rls.isSubStyle()).collect(Collectors.toList());
    final RoadLayerStyle selectedTopLevelStyle = roadLayerType.isSubStyle() ? roadLayerType.getParentStyle() : roadLayerType;

    options.add(new LayerOptions(topLevelStyles,
        v -> M.messages().layerRoad((RoadLayerStyle) v),
        v -> {
          if (((RoadLayerStyle) v).hasSubStyles()) {
            this.setRoadLayerType(((RoadLayerStyle) v).getSubStyles().get(0));
          } else {
            this.setRoadLayerType((RoadLayerStyle) v);
          }
        },
        selectedTopLevelStyle));
  }

  private void addSubLayerOptions(final List<LayerOptions> options) {
    if (roadLayerType != null && roadLayerType.isSubStyle()) {
      final List<RoadLayerStyle> subStyles = applicationConfiguration.getRoadLayerStyle().stream()
          .filter(rls -> roadLayerType.getParentStyle().getSubStyles().contains(rls)).collect(Collectors.toList());

      options.add(new LayerOptions(subStyles,
          v -> M.messages().layerRoad((RoadLayerStyle) v),
          v -> this.setRoadLayerType((RoadLayerStyle) v),
          roadLayerType));
    }
  }

  private void addVehicleTypeOptions(final List<LayerOptions> options) {
    if (roadLayerType != null && roadLayerType.dependsOnVehicleType()) {
      options.add(new LayerOptions(correctVehicleTypes(applicationConfiguration.getSectorCategories()
          .getRoadEmissionCategories().getVehicleTypes()),
          v -> this.nameFunction(v),
          v -> this.setVehicleType(((SimpleCategory) v).getCode()),
          defaultVehicleType()));
    }
  }

  private SimpleCategory defaultVehicleType() {
    if (roadLayerType == RoadLayerStyle.STAGNATION) {
      return applicationConfiguration.getSectorCategories().getRoadEmissionCategories().getVehicleTypes().get(0);
    } else {
      return getTotalVehicle();
    }
  }

  private List<SimpleCategory> correctVehicleTypes(final List<SimpleCategory> vehicleTypes) {
    if (roadLayerType != null && roadLayerType.dependsOnVehicleType() && roadLayerType != RoadLayerStyle.STAGNATION) {
      final List<SimpleCategory> modifiedVehicleTypes = new ArrayList<>(vehicleTypes);
      modifiedVehicleTypes.add(0, getTotalVehicle());
      return modifiedVehicleTypes;
    }
    return vehicleTypes;
  }

  private SimpleCategory getTotalVehicle() {
    return new SimpleCategory(0, "", "", M.messages().esRoadVehicleTypeLegendaTotal());
  }

  private void addSubstanceOptions(final List<LayerOptions> options) {
    if (roadLayerType != null && roadLayerType == RoadLayerStyle.ROAD_EMISSION) {
      options.add(new LayerOptions(ROAD_EMISSION_SUBSTANCES,
          v -> this.nameFunction(v),
          v -> this.setSubstance((Substance) v),
          Substance.NOX));
    }
  }

  private String nameFunction(final Object v) {
    if (v instanceof SimpleCategory) {
      return ((SimpleCategory) v).getDescription();
    } else if (v instanceof Substance) {
      return ((Substance) v).getName();
    } else if (v instanceof RoadLayerStyle) {
      return M.messages().layerRoad((RoadLayerStyle) v);
    } else {
      return v.toString();
    }
  }

  private void setSubstance(final Substance roadEmissionSubstance) {
    if (roadEmissionStyle != null) {
      roadEmissionStyle.setSelectedSubstance(roadEmissionSubstance);
    }
    updateLegend();
    asLayer().getSource().changed();
  }

  @Override
  protected Style[] createStyle(final EmissionSourceFeature feature, final double resolution) {
    if (feature.getId().equals(getGeometryEditingFeatureId())) {
      return NO_RENDERING;
    }

    if (!RoadEmissionSourceTypes.isRoadSource(feature)) {
      return NO_RENDERING;
    }

    final List<Style> baseRoadStyle = new ArrayList<>(roadStyles.get(roadLayerType).getStyle((RoadESFeature) feature, vehicleTypeCode, resolution));

    Optional.ofNullable(getRoadStyle(feature))
        .ifPresent(v -> {
          final Polygon lineString = OL3GeometryUtil.outlinePolygonOfLineString((LineString) feature.getGeometry(),
              ROAD_SELECTION_OFFSET_DISTANCE * resolution);
          v.setGeometry(lineString);
          baseRoadStyle.add(v);
        });

    return baseRoadStyle.toArray(new Style[0]);
  }

  private Style getRoadStyle(final EmissionSourceFeature source) {
    return selectionContext.isSelected(source)
        ? ROAD_SELECTED_STYLE.clone()
        : roadNetworkContext.isHighlighted(source)
            ? ROAD_HIGHLIGHTED_STYLE.clone()
            : null;
  }

}
