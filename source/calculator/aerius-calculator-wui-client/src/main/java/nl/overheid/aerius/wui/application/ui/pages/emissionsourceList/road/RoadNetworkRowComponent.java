/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.emissionsourceList.road;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.google.web.bindery.event.shared.EventBus;

import elemental2.dom.MouseEvent;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.wui.application.ApplicationLimits;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceFeatureDeselectCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceFeatureToggleSelectCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourcePeekFeatureCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceUnpeekFeatureCommand;
import nl.overheid.aerius.wui.application.command.source.FeatureZoomCommand;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsibleButton;
import nl.overheid.aerius.wui.application.components.source.EmissionSourceRowComponent;
import nl.overheid.aerius.wui.application.context.EmissionSourceListContext;
import nl.overheid.aerius.wui.application.context.RoadNetworkContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.road.RoadEmissionSourceTypes;
import nl.overheid.aerius.wui.application.util.DoubleClickUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "aer-road-network-row", components = {
    EmissionSourceRowComponent.class,
    VerticalCollapseGroup.class,
    CollapsibleButton.class
})
public class RoadNetworkRowComponent extends BasicVueComponent {
  private static final int DEFAULT_LIMIT = 100;

  @Prop SituationContext situation;

  @Data int limit = DEFAULT_LIMIT;
  @Data int limitIncrement = 100;

  @Inject @Data ScenarioContext context;
  @Inject @Data EmissionSourceListContext listContext;
  @Inject @Data RoadNetworkContext roadNetworkContext;
  @Inject EventBus eventBus;

  @Data boolean roadGroupOpen = false;

  private final DoubleClickUtil<EmissionSourceFeature> doubleClickUtil = new DoubleClickUtil<>();

  public Stream<EmissionSourceFeature> getAllVisibleRoadSources() {
    return Optional.ofNullable(situation)
        .map(v -> v.getSources())
        .orElse(new ArrayList<>())
        .stream()
        .filter(v -> RoadEmissionSourceTypes.isRoadSource(v))
        .filter(v -> roadGroupOpen || roadNetworkContext.isHighlighted(v));
  }

  @Computed("areSourcesLimited")
  public boolean areSourcesLimited() {
    return getAllVisibleRoadSources().count() > limit;
  }

  @Computed
  public List<EmissionSourceFeature> getEmissionSources() {
    return getAllVisibleRoadSources()
        .sorted((o1, o2) -> -Boolean.compare(roadNetworkContext.isHighlighted(o1), roadNetworkContext.isHighlighted(o2)))
        .limit(calculateLimit())
        .collect(Collectors.toList());
  }

  private long calculateLimit() {
    return Math.max(roadNetworkContext.highlightedSize(), limit);
  }

  @Computed("hasSelectedRoadEmissionSources")
  public boolean hasSelectedRoadEmissionSources() {
    return !getEmissionSources().isEmpty();
  }

  @JsMethod
  public void increaseLimit() {
    limit = Math.max(limit, roadNetworkContext.highlightedSize()) + limitIncrement;
  }

  @Computed("hasRoadNetwork")
  public boolean hasRoadNetwork() {
    return !getRoadNetworkSources().isEmpty();
  }

  @Computed
  public List<EmissionSourceFeature> getAllEmissionSources() {
    return Optional.ofNullable(situation)
        .map(v -> v.getSources())
        .orElse(new ArrayList<EmissionSourceFeature>())
        .stream()
        .limit(ApplicationLimits.SCENARIO_INPUT_LIST_DRAW_LIMIT)
        .collect(Collectors.toList());
  }

  @JsMethod
  public List<EmissionSourceFeature> getRoadNetworkSources() {
    return getAllEmissionSources().stream()
        .filter(v -> RoadEmissionSourceTypes.isRoadSource(v))
        .collect(Collectors.toList());
  }

  /**
   * Returns true if
   *   - the road group is open
   *   - there are road sources
   *   - no sources (of any type) are selected
   */
  @Computed("isRoadNetworkGroupSelected")
  public boolean isRoadNetworkGroupSelected() {
    return roadGroupOpen
        && context.getActiveSituation()
            .getSources().stream()
            .filter(v -> RoadEmissionSourceTypes.isRoadSource(v))
            .count() > 0
        && listContext.getSelections().stream()
            .count() == 0;
  }

  @Watch("isRoadNetworkGroupSelected()")
  public void onIsRoadNetworkGroupSelectedChange(final boolean neww) {
    vue().$emit("select", neww);
  }

  @JsMethod
  public void toggleNetwork() {
    roadGroupOpen = !roadGroupOpen;
    limit = Math.max(DEFAULT_LIMIT, roadNetworkContext.highlightedSize() + limitIncrement);

    // If closing the network, deselect feature if it is a road source
    if (!roadGroupOpen) {
      listContext.getSelections().stream()
          .filter(v -> RoadEmissionSourceTypes.isRoadSource(v))
          .forEach(selected -> eventBus.fireEvent(new EmissionSourceFeatureDeselectCommand(selected)));
    }
  }

  @JsMethod
  public void peekSource(final EmissionSourceFeature source) {
    eventBus.fireEvent(new EmissionSourcePeekFeatureCommand(source));
  }

  @JsMethod
  public void unpeekSource(final EmissionSourceFeature source) {
    eventBus.fireEvent(new EmissionSourceUnpeekFeatureCommand(source));
  }

  @JsMethod
  public void selectSource(final EmissionSourceFeature source) {
    eventBus.fireEvent(new EmissionSourceFeatureToggleSelectCommand(source, false));
  }

  @JsMethod
  public void multiSelectSource(final EmissionSourceFeature source) {
    eventBus.fireEvent(new EmissionSourceFeatureToggleSelectCommand(source, true));
  }

  @JsMethod
  public void focusSource(final EmissionSourceFeature source) {
    eventBus.fireEvent(new FeatureZoomCommand(source));
  }

  @JsMethod
  public void clickSource(final EmissionSourceFeature source, final MouseEvent ev) {
    if (ev.ctrlKey) {
      multiSelectSource(source);
    } else {
      doubleClickUtil.click(source, this::selectSource, this::focusSource);
    }
  }

}
