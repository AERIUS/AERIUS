/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.command.building;

import nl.aerius.wui.command.SimpleGenericCommand;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.event.building.BuildingAddedEvent;

/**
 * Command to add a new {@link BuildingFeature} object.
 */
public class BuildingAddCommand extends SimpleGenericCommand<BuildingFeature, BuildingAddedEvent> {
  private SituationContext situation;

  public BuildingAddCommand(final BuildingFeature building) {
    this(building, null);
  }

  public BuildingAddCommand(final BuildingFeature building, final SituationContext situation) {
    super(building);
    this.situation = situation;
  }

  public SituationContext getSituation() {
    return situation;
  }

  public void setSituation(final SituationContext situation) {
    this.situation = situation;
  }

  @Override
  protected BuildingAddedEvent createEvent(final BuildingFeature value) {
    return new BuildingAddedEvent(value, situation);
  }

}
