/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.farmlodging.standard;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.aerius.vuelidate.ValidationOptions;
import nl.aerius.vuelidate.Validations;
import nl.aerius.vuelidate.ValidatorBuilder;
import nl.overheid.aerius.shared.ImaerConstants;

/**
 * Validators for OffRoadMobileStandard editing.
 */
class FarmLodgingStandardValidators extends ValidationOptions<FarmLodgingStandardEmissionComponent> {

  protected static final int MIN_NUMBER_OF_DAYS = 0;
  protected static final int MAX_NUMBER_OF_DAYS = ImaerConstants.DAYS_PER_YEAR;

  @JsType(isNative = true, namespace = JsPackage.GLOBAL)
  static class FarmLodgingStandardValidations extends Validations {
    public @JsProperty Validations farmLodgingCodeV;
    public @JsProperty Validations numberOfAnimalsV;
    public @JsProperty Validations numberOfDaysV;
    public @JsProperty Validations systemDefinitionCodeV;
  }

  @Override
  protected void constructValidators(final Object options, final ValidatorInstaller v) {
    final FarmLodgingStandardEmissionComponent instance = Js.uncheckedCast(options);
    v.install("farmLodgingCodeV", () -> instance.farmLodgingCodeV = null,
        ValidatorBuilder.create()
            .required());
    v.install("numberOfAnimalsV", () -> instance.numberOfAnimalsV = null,
        ValidatorBuilder.create()
            .required()
            .integer()
            .minValue(0));
    v.install("numberOfDaysV", () -> instance.numberOfDaysV = null,
        ValidatorBuilder.create()
            .minValue(MIN_NUMBER_OF_DAYS)
            .maxValue(MAX_NUMBER_OF_DAYS));
    v.install("systemDefinitionCodeV", () -> instance.systemDefinitionCodeV = null,
        ValidatorBuilder.create()
            .required());
  }
}
