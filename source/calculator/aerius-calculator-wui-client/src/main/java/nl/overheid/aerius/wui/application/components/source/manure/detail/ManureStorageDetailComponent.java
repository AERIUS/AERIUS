/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.manure.detail;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.wui.application.components.divider.DividerComponent;
import nl.overheid.aerius.wui.application.components.source.manure.custom.ManureStorageCustomDetailComponent;
import nl.overheid.aerius.wui.application.components.source.manure.standard.ManureStorageStandardDetailComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.ManureStorageESFeature;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "aer-manure-storage-detail", components = {
    ManureStorageStandardDetailComponent.class,
    ManureStorageCustomDetailComponent.class,
    DividerComponent.class
})
public class ManureStorageDetailComponent extends BasicVueComponent {
  @Prop @JsProperty protected ManureStorageESFeature source;
  @Prop @JsProperty protected List<Substance> substances;
  @Prop String totalEmissionTitle;
  @Data @Inject ApplicationContext applicationContext;
}
