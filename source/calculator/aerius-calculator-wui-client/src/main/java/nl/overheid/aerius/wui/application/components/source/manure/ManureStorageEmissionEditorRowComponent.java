/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.source.manure;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Emit;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.sector.SectorIcon;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.manure.CustomManureStorage;
import nl.overheid.aerius.wui.application.domain.source.manure.ManureStorage;
import nl.overheid.aerius.wui.application.domain.source.manure.ManureStorageType;
import nl.overheid.aerius.wui.application.domain.source.manure.StandardManureStorage;
import nl.overheid.aerius.wui.application.resources.util.FontIconUtil;
import nl.overheid.aerius.wui.application.util.SectorColorUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component
public class ManureStorageEmissionEditorRowComponent extends BasicVueComponent {
  @Prop @JsProperty ManureStorage source;

  @Prop int sectorId;
  @Prop String id;

  @Prop boolean selected;

  @Inject @Data ApplicationContext appContext;

  @JsMethod
  @Emit
  public void select() {}

  @Computed
  public SectorIcon getSectorIcon() {
    return SectorColorUtil.getSectorIcon(appContext.getConfiguration(), sectorId);
  }

  @JsMethod
  protected String sectorFontClass() {
    return FontIconUtil.fontClassNamePlain(getSectorIcon().getSectorName());
  }

  @Computed
  protected String getDescription() {
    if (source.getManureStorageType() == ManureStorageType.STANDARD) {
      return ((StandardManureStorage) source).getManureStorageCode();
    } else {
      return ((CustomManureStorage) source).getDescription();
    }
  }
}
