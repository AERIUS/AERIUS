/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.event.situation;

import nl.aerius.wui.event.SimpleGenericEvent;
import nl.overheid.aerius.wui.application.context.SituationContext;

public abstract class SituationChangeEvent extends SimpleGenericEvent<SituationContext> {

  public static enum Change {
    /*
    Add a situation
     */
    ADDED,
    /*
    Remove a situation
     */
    DELETED,
    /*
    Rename a situation
     */
    RENAMED,
    /*
    Change the situation type
     */
    CHANGED_TYPE,
    /*
    Change the situation year
     */
    CHANGED_YEAR,
    /*
    Change the situation netting factor
     */
    CHANGED_NETTING_FACTOR;
  }

  private final Change change;

  public SituationChangeEvent(final SituationContext situation, final Change change) {
    super(situation);
    this.change = change;
  }

  public Change getChange() {
    return change;
  }
}
