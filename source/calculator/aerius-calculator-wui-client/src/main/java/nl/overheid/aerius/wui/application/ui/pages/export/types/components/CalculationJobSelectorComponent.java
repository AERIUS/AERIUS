/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.export.types.components;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.command.PlaceChangeRequestCommand;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobSelectCommand;
import nl.overheid.aerius.wui.application.command.exporter.ExportPrepareCommand;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.InputNoticeComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.notice.NoticeRule;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ExportContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationPreparationContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.application.place.CalculatePlace;
import nl.overheid.aerius.wui.application.ui.pages.calculate.CalculateValidations;

@Component(components = {
    LabeledInputComponent.class,
    SimplifiedListBoxComponent.class,
    ValidationBehaviour.class,
    InputErrorComponent.class,
    InputNoticeComponent.class,
    VerticalCollapse.class,
})
public class CalculationJobSelectorComponent extends ErrorWarningValidator {
  public @Data String NO_JOB_SELECTED = "[null]";

  @Data @Inject CalculationPreparationContext prepContext;
  @Data @Inject ApplicationContext appContext;
  @Data @Inject ExportContext exportContext;

  @Prop(required = true) EventBus eventBus;

  @Computed("jobs")
  public List<CalculationJobContext> getJobs() {
    return prepContext.getJobs();
  }

  @JsMethod
  public String formatJobName(final CalculationJobContext job) {
    return i18n.exportJobSelectionName(job.getName(), i18n.calculationJobType(job.getCalculationOptions().getCalculationJobType()));
  }

  @JsMethod
  public void selectJob(final CalculationJobContext job) {
    eventBus.fireEvent(new ExportPrepareCommand(job, false));
  }

  @JsMethod
  public String getJobKey(final CalculationJobContext job) {
    return job == null ? NO_JOB_SELECTED : job.getId();
  }

  @Computed
  public String getActiveKey() {
    return Optional.ofNullable(exportContext.getCalculationJob())
        .map(v -> v.getId())
        .orElse(NO_JOB_SELECTED);
  }

  @Computed
  public Stream<NoticeRule> getCalculationJobErrors() {
    if (exportContext.getCalculationOptions() == null) {
      return Stream.of();
    }

    final SituationComposition comp = exportContext.getSituationComposition();
    final CalculationSetOptions opts = exportContext.getCalculationOptions();

    return Stream.of(
        NoticeRule.create(
            () -> CalculateValidations.hasIncompatibleJobType(opts, exportContext.getReportType(), exportContext.getJobType()),
            () -> i18n.exportCalculationJobIncompatibleWithType(i18n.exportReportType(exportContext.getReportType()))),
        NoticeRule.create(
            () -> !CalculateValidations.hasIncompatibleJobType(opts, exportContext.getReportType(), exportContext.getJobType())
                && CalculateValidations.hasIncompatibleCalculationMethod(opts, CalculationMethod.FORMAL_ASSESSMENT, exportContext.getJobType()),
            () -> i18n.exportCalculationJobIncompatibleMethod(i18n.exportReportType(exportContext.getReportType()),
                i18n.calculationMethod(appContext.getTheme(), CalculationMethod.FORMAL_ASSESSMENT),
                i18n.calculationMethod(appContext.getTheme(), opts.getCalculationMethod()))),
        NoticeRule.create(
            () -> !CalculateValidations.hasIncompatibleJobType(opts, exportContext.getReportType(), exportContext.getJobType())
                && !CalculateValidations.hasIncompatibleCalculationMethod(opts, CalculationMethod.FORMAL_ASSESSMENT, exportContext.getJobType())
                && isMissingRequiredSituation(comp, opts),
            () -> i18n.exportSituationsMissing(CalculateValidations.formatRequiredSituationTypes(comp, opts.getCalculationJobType()))),
        NoticeRule.create(
            () -> hasTooFewSituations(comp, opts),
            () -> i18n.exportTooFewSituationsSelected()),
        NoticeRule.create(
            () -> CalculateValidations.hasInvalidMetSite(appContext, opts),
            () -> i18n.exportMetSiteSelectionInvalid()),
        NoticeRule.create(
            () -> CalculateValidations.hasInvalidMetYear(appContext, opts),
            () -> i18n.exportMetYearSelectionInvalid()),
        NoticeRule.create(
            () -> CalculateValidations.hasInvalidPermitArea(appContext, opts.getNcaCalculationOptions()),
            () -> i18n.exportCalculatePermitAreaInvalid()),
        NoticeRule.create(
            () -> CalculateValidations.hasInvalidProjectCategory(appContext, opts.getNcaCalculationOptions()),
            () -> i18n.exportCalculateProjectCategoryInvalid()));
  }

  private static boolean isMissingRequiredSituation(final SituationComposition comp, final CalculationSetOptions opts) {
    return CalculateValidations.hasMissingRequiredSituation(comp, opts.getCalculationJobType());
  }

  private static boolean hasTooFewSituations(final SituationComposition comp, final CalculationSetOptions opts) {
    // Don't trigger too few situations if we're already notifying about missing required situation.
    return !isMissingRequiredSituation(comp, opts) && CalculateValidations.hasTooFewSituations(comp);
  }

  @Computed
  public List<NoticeRule> getCalculationJobErrorsShowing() {
    return getCalculationJobErrors()
        .filter(v -> v.showing())
        .collect(Collectors.toList());
  }

  @JsMethod
  public void createNewJob() {
    eventBus.fireEvent(new PlaceChangeRequestCommand(new CalculatePlace(appContext.getTheme())));
  }

  @JsMethod
  public void editJob(final CalculationJobContext job) {
    eventBus.fireEvent(new CalculationJobSelectCommand(job));
    eventBus.fireEvent(new PlaceChangeRequestCommand(new CalculatePlace(appContext.getTheme())));
  }

  @Computed("isInvalidCalculationJob")
  public boolean isInvalidCalculationJob() {
    return exportContext.getCalculationJob() == null;
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return isInvalidCalculationJob()
        || exportContext.getCalculationOptions() == null
        || exportContext.getSituationComposition() == null
        || !getCalculationJobErrorsShowing().isEmpty();
  }
}
