/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.farmanimalhousing;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Emit;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalHousingCategories;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.farm.CustomFarmAnimalHousing;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmAnimalHousing;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmAnimalHousingType;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmAnimalHousingUtil;
import nl.overheid.aerius.wui.application.domain.source.farm.StandardFarmAnimalHousing;
import nl.overheid.aerius.wui.application.resources.util.FarmIconUtil;
import nl.overheid.aerius.wui.application.resources.util.FarmIconUtil.AdditionalIcon;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

/**
 * Component to show a single FarmEmissionHouse sub source in the table of sub sources.
 */
@Component
class FarmAnimalHousingEmissionEditorRowComponent extends BasicVueComponent {
  @Prop @JsProperty FarmAnimalHousing source;
  @Prop boolean selected;

  @Inject @Data ApplicationContext applicationContext;

  @JsMethod
  @Emit
  protected void select() {}

  @Computed
  protected String getIcon() {
    return FarmIconUtil.getFarmAnimalHousingIcon(getCategories(), source);
  }

  @Computed("additionalIcon")
  protected AdditionalIcon additionalIcon() {
    return FarmIconUtil.getFarmAnimalHousingAdditionalIcon(source, getCategories());
  }

  @JsMethod
  protected FarmAnimalHousingCategories getCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getFarmAnimalHousingCategories();
  }

  @Computed
  protected int getNumberOfAnimals() {
    return source.getNumberOfAnimals();
  }

  @Computed
  protected String getDescription() {
    if (hasStandardEmissions()) {
      return ((StandardFarmAnimalHousing) source).getAnimalHousingCode();
    } else {
      return ((CustomFarmAnimalHousing) source).getDescription();
    }
  }

  @Computed
  protected double getEmission() {
    return FarmAnimalHousingUtil.getEmission(getCategories(), source);
  }

  private boolean hasStandardEmissions() {
    return source.getAnimalHousingType() == FarmAnimalHousingType.STANDARD;
  }
}
