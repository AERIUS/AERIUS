/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.calculationpoint;

import java.util.Collections;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.color.Color;
import ol.style.Style;

import nl.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;
import nl.overheid.aerius.wui.application.event.DeleteAllCalculationPointsEvent;
import nl.overheid.aerius.wui.application.event.calculationpoint.CalculationPointAddedEvent;
import nl.overheid.aerius.wui.application.event.calculationpoint.CalculationPointDeleteEvent;
import nl.overheid.aerius.wui.application.event.calculationpoint.CalculationPointSaveEvent;
import nl.overheid.aerius.wui.application.event.calculationpoint.CalculationPointsAddedEvent;
import nl.overheid.aerius.wui.application.geo.layers.LabelStyle;
import nl.overheid.aerius.wui.application.geo.layers.base.BaseGeometryLayer;
import nl.overheid.aerius.wui.application.geo.layers.base.VectorFeaturesWrapper;

public class CalculationPointGeometryLayer extends BaseGeometryLayer<CalculationPointFeature> {
  interface CalculationPointGeometryLayerEventBinder extends EventBinder<CalculationPointGeometryLayer> {}

  private final CalculationPointGeometryLayerEventBinder EVENT_BINDER = GWT.create(CalculationPointGeometryLayerEventBinder.class);

  private final VectorFeaturesWrapper vectorObject = new VectorFeaturesWrapper();

  @Inject
  public CalculationPointGeometryLayer(final EventBus eventBus, @Assisted final int zIndex) {
    super(new LayerInfo(), zIndex);
    getInfo().setName(CalculationPointGeometryLayer.class.getName());

    setFeatures(vectorObject, Collections.emptyList());
    asLayer().setVisible(true);
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  public void onCalculationPointSaveEvent(final CalculationPointSaveEvent e) {
    vectorObject.updateFeature(e.getValue());
  }

  @EventHandler
  public void onDeleteAllCalculationPointsEvent(final DeleteAllCalculationPointsEvent e) {
    vectorObject.clear();
    clear();
  }

  @EventHandler
  public void onCalculationPointsAddedEvent(final CalculationPointsAddedEvent e) {
    vectorObject.addFeatures(e.getValue());
  }

  @EventHandler
  public void onCalculationPointAddedEvent(final CalculationPointAddedEvent e) {
    vectorObject.addFeature(e.getValue());
  }

  @EventHandler
  public void onRemoveCalculationPointCommand(final CalculationPointDeleteEvent e) {
    vectorObject.removeFeature(e.getValue());
  }

  @Override
  protected Style[] createStyle(final CalculationPointFeature feature, final double resolution) {
    return getPointStyle(Color.getColorFromString(LabelStyle.CALCULATION_POINT.getLabelBackgroundColor(feature)));
  }
}
