/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.road.srm2;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.aerius.vuelidate.ValidationOptions;
import nl.aerius.vuelidate.Validations;
import nl.aerius.vuelidate.ValidatorBuilder;
import nl.overheid.aerius.srm2.conversion.Sigma0Calculator;

/**
 * Validators for roadside barrier.
 */
class SRM2BarrierEditorValidators extends ValidationOptions<SRM2BarrierEditorComponent> {
  @JsType(isNative = true, namespace = JsPackage.GLOBAL)
  static class SRM2BarrierEditorValidations extends Validations {
    public @JsProperty Validations barrierLeftHeightV;
    public @JsProperty Validations barrierRightHeightV;
    public @JsProperty Validations barrierLeftDistanceV;
    public @JsProperty Validations barrierRightDistanceV;

    public @JsProperty Validations barrierLeftHeightWarningV;
    public @JsProperty Validations barrierRightHeightWarningV;
    public @JsProperty Validations barrierLeftDistanceWarningV;
    public @JsProperty Validations barrierRightDistanceWarningV;
  }

  @Override
  protected void constructValidators(final Object options, final ValidatorInstaller v) {
    final SRM2BarrierEditorComponent instance = Js.uncheckedCast(options);
    v.install("barrierLeftHeightV", () -> instance.barrierLeftHeightV = null, ValidatorBuilder.create()
        .decimal()
        .minValue(0D));
    v.install("barrierRightHeightV", () -> instance.barrierRightHeightV = null, ValidatorBuilder.create()
        .decimal()
        .minValue(0D));
    v.install("barrierLeftDistanceV", () -> instance.barrierLeftDistanceV = null, ValidatorBuilder.create()
        .decimal()
        .minValue(0D));
    v.install("barrierRightDistanceV", () -> instance.barrierRightDistanceV = null, ValidatorBuilder.create()
        .decimal()
        .minValue(0D));

    v.install("barrierLeftHeightWarningV", () -> instance.barrierLeftHeightWarningV = null, ValidatorBuilder.create()
        .minValue(Sigma0Calculator.MIN_BARRIER_HEIGHT)
        .maxValue(Sigma0Calculator.MAX_BARRIER_HEIGHT));
    v.install("barrierRightHeightWarningV", () -> instance.barrierRightHeightWarningV = null, ValidatorBuilder.create()
        .minValue(Sigma0Calculator.MIN_BARRIER_HEIGHT)
        .maxValue(Sigma0Calculator.MAX_BARRIER_HEIGHT));
    v.install("barrierLeftDistanceWarningV", () -> instance.barrierLeftDistanceWarningV = null, ValidatorBuilder.create()
        .maxValue(Sigma0Calculator.MAX_BARRIER_DISTANCE));
    v.install("barrierRightDistanceWarningV", () -> instance.barrierRightDistanceWarningV = null, ValidatorBuilder.create()
        .maxValue(Sigma0Calculator.MAX_BARRIER_DISTANCE));
  }
}
