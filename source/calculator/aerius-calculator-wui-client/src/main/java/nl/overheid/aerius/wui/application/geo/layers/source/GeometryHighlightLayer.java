/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Timer;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.Feature;
import ol.OLFactory;
import ol.layer.LayerGroupOptions;
import ol.layer.VectorLayerOptions;
import ol.style.Fill;
import ol.style.Stroke;
import ol.style.Style;
import ol.style.StyleOptions;

import nl.aerius.geo.domain.IsLayer;
import nl.aerius.geo.domain.LayerInfo;
import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.wui.application.daemon.geo.HighlightOutlineCommand;
import nl.overheid.aerius.wui.application.daemon.geo.MapDaemon;

public class GeometryHighlightLayer extends BasicEventComponent implements IsLayer<ol.layer.Group> {
  private static final InfoMarkerLayerEventBinder EVENT_BINDER = GWT.create(InfoMarkerLayerEventBinder.class);

  interface InfoMarkerLayerEventBinder extends EventBinder<GeometryHighlightLayer> {}

  private static final Stroke HIGHLIGHT_STYLE_STROKE = OLFactory.createStroke(OLFactory.createColor(21, 124, 177, 1), 2);
  private static final Fill HIGHLIGHT_STYLE_FILL = OLFactory.createFill(OLFactory.createColor(208, 229, 239, 0.4));

  public static final Style HIGHLIGHT_VECTOR_STYLE;
  static {
    final StyleOptions vectorStyleOptions = new StyleOptions();
    vectorStyleOptions.setStroke(HIGHLIGHT_STYLE_STROKE);
    vectorStyleOptions.setFill(HIGHLIGHT_STYLE_FILL);
    HIGHLIGHT_VECTOR_STYLE = new Style(vectorStyleOptions);
  }

  private final ol.source.Vector vectorSource;
  private final ol.layer.Group groupLayer;

  private final Timer timer = new Timer() {
    @Override
    public void run() {
      removeHighlight();
    }
  };

  @Inject
  public GeometryHighlightLayer(final EventBus eventBus) {
    // TODO Refactor eventbus injection, the MapLayoutPanel should probably set
    // these (for HasEventBus interfaces)
    setEventBus(eventBus);

    vectorSource = new ol.source.Vector();
    final VectorLayerOptions vectorLayerOptions = OLFactory.createOptions();
    vectorLayerOptions.setStyle((feature, v) -> getVectorStyle(feature, v));
    final ol.layer.Vector vectorLayer = new ol.layer.Vector(vectorLayerOptions);
    vectorLayer.setSource(vectorSource);

    final ol.Collection<ol.layer.Base> layers = new ol.Collection<>();
    layers.push(vectorLayer);

    final LayerGroupOptions groupOptions = new LayerGroupOptions();
    groupOptions.setLayers(layers);

    groupLayer = new ol.layer.Group(groupOptions);
  }

  private void removeHighlight() {
    vectorSource.clear(true);
  }

  private Style[] getVectorStyle(final Feature feature, final double resolution) {
    return new Style[] { HIGHLIGHT_VECTOR_STYLE };
  }

  @EventHandler
  public void onHighlightOutlineCommand(final HighlightOutlineCommand c) {
    timer.cancel();

    final Feature polyFeature = new ol.Feature(c.getValue());
    vectorSource.clear(true);
    vectorSource.addFeature(polyFeature);

    timer.schedule(MapDaemon.SONAR_REMOVE_TIME);
  }

  @Override
  public ol.layer.Group asLayer() {
    return groupLayer;
  }

  @Override
  public LayerInfo getInfo() {
    return null;
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
