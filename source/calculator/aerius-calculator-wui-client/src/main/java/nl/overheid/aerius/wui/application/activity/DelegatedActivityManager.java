/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.activity;

import java.util.function.Consumer;

import com.google.web.bindery.event.shared.EventBus;

import nl.aerius.wui.event.HasEventBus;
import nl.aerius.wui.place.Place;

/**
 * Base class for activity managers that delegate the actual work to another activity.
 */
public abstract class DelegatedActivityManager<V> {
  private EventBus eventBus;

  private V view;
  private Place place;
  private Consumer<Place> redirector;

  private boolean delegate;

  private DelegatedActivity<V> act;

  public boolean delegate(final EventBus eventBus, final Place place, final Consumer<Place> redirector) {
    this.eventBus = eventBus;
    this.place = place;
    this.redirector = redirector;

    return tryCanDelegate();
  }

  public void setView(final V view) {
    this.view = view;
    doDelegate();
  }

  private void doDelegate() {
    if (!delegate) {
      return;
    }

    onStop();
    delegate = false;
    act = getActivity(place);

    if (act == null) {
      return;
    }

    if (act instanceof HasEventBus) {
      ((HasEventBus) act).setEventBus(eventBus);
    }
    act.onStart(view);
  }

  public String mayStop() {
    if (act != null) {
      return act.mayStop();
    }

    return null;
  }

  public void onStop() {
    if (act != null) {
      act.onStop();
      act = null;
    }
  }

  private DelegatedActivity<V> redirect(final Place place) {
    redirector.accept(place);
    return null;
  }

  private boolean tryCanDelegate() {
    final Place redirect = getRedirect(place);

    if (redirect == null) {
      scheduleDelegate();
    } else {
      redirect(redirect);
    }

    return redirect == null;
  }

  private void scheduleDelegate() {
    delegate = true;
    if (view != null) {
      doDelegate();
    }
  }

  protected abstract Place getRedirect(Place place);

  protected abstract DelegatedActivity<V> getActivity(Place place);
}
