/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.importer;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;

/**
 * Data object to transfer a import parcel to the client.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class ImportParcel {

  private ScenarioMetaData importedMetaData;
  private ImportParcelSituation situation;
  private Object calculationPoints;
  private CalculationSetOptions calculationSetOptions;

  public final @JsOverlay ScenarioMetaData getImportedMetaData() {
    return importedMetaData;
  }

  public final @JsOverlay ImportParcelSituation getSituation() {
    return situation;
  }

  /**
   * @return The FeatureCollection of calculation points
   */
  public final @JsOverlay Object getCalculationPoints() {
    return calculationPoints;
  }

  public final @JsOverlay CalculationSetOptions getCalculationSetOptions() {
    return calculationSetOptions;
  }
}
