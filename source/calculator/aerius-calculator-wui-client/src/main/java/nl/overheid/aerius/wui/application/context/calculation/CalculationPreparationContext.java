/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context.calculation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.inject.Singleton;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;

@Singleton
public class CalculationPreparationContext {
  private CalculationJobContext activeJob = null;
  private CalculationJobContext peekJob = null;

  @JsProperty private final List<CalculationJobContext> jobs = new ArrayList<>();
  @JsProperty private CalculationJobContext temporaryJob = null;
  @JsProperty private String temporaryJobId = null;
  @JsProperty private final Map<String, CalculationExecutionContext> latestCalculations = new HashMap<>();

  public List<CalculationJobContext> getJobs() {
    return jobs;
  }

  public void addJob(final CalculationJobContext job) {
    jobs.add(job);
  }

  public void replaceJob(final CalculationJobContext sourceJob, final CalculationJobContext targetJob) {
    final SituationComposition situationComposition = sourceJob.getSituationComposition().copy();
    final CalculationSetOptions calculationSetOptions = sourceJob.getCalculationOptions().copy();

    targetJob.setName(sourceJob.getName());
    targetJob.setSituationComposition(situationComposition);
    targetJob.setCalculationOptions(calculationSetOptions);
  }

  public CalculationExecutionContext removeJob(final CalculationJobContext job) {
    jobs.remove(job);
    if (job.equals(activeJob)) {
      clearActiveJob();
    }
    return latestCalculations.remove(job.getId());
  }

  public void removeAllJobs() {
    jobs.clear();
    clearActiveJob();
    latestCalculations.clear();
  }

  public CalculationJobContext getTemporaryJob() {
    return temporaryJob;
  }

  public void setTemporaryJob(final CalculationJobContext job) {
    this.temporaryJob = job;
  }

  public void clearTemporaryJob() {
    this.temporaryJob = null;
    this.temporaryJobId = null;
  }

  public String getTemporaryJobId() {
    return temporaryJobId;
  }

  public void setTemporaryJobId(final String id) {
    this.temporaryJobId = id;
  }

  public void removeCalculation(final CalculationExecutionContext calculation) {
    // Remove calculation in this roundabout way (removing from .values() is not reactive)
    latestCalculations.values().stream()
        .filter(v -> v.equals(calculation))
        .findFirst()
        .map(v -> v.getJobContext())
        .ifPresent(v -> latestCalculations.remove(v.getId()));
  }

  /**
   * Note: active job and active calculation (likely) need to be kept in sync, so this method should be called through a daemon managing this
   */
  public void setActiveJob(final CalculationJobContext job) {
    this.activeJob = job;
  }

  public void clearActiveJob() {
    this.activeJob = null;
  }

  public boolean hasActiveJob() {
    return activeJob != null;
  }

  public void setLatestCalculation(final CalculationJobContext job, final CalculationExecutionContext calculation) {
    latestCalculations.put(job.getId(), calculation);
  }

  public CalculationExecutionContext getActiveCalculation() {
    return getActiveCalculation(getActiveJob());
  }

  public CalculationExecutionContext getActiveCalculation(final CalculationJobContext job) {
    return latestCalculations.get(Optional.ofNullable(job)
        .map(v -> v.getId())
        .orElse(null));
  }

  /**
   * Return the active/highlighted job, or null if there is none.
   */
  public CalculationJobContext getActiveJob() {
    return activeJob;
  }

  public void setPeekJob(final CalculationJobContext job) {
    this.peekJob = job;
  }

  public void clearPeekJob(final CalculationJobContext job) {
    if (this.peekJob != null && this.peekJob.equals(job)) {
      this.peekJob = null;
    }
  }

  public CalculationJobContext getLooseSelection() {
    return Optional.ofNullable(activeJob)
        .orElse(peekJob);
  }

  public boolean isActiveJob(final CalculationJobContext job) {
    return job.equals(getActiveJob());
  }

  /**
   * Return the active/temporary job, or null if there is none.
   */
  public CalculationJobContext getActiveOrTemporaryJob() {
    return Optional.ofNullable(activeJob)
        .orElse(temporaryJob);
  }

  public void reset() {
    activeJob = null;
    temporaryJob = null;
    temporaryJobId = null;
    peekJob = null;
    jobs.clear();
  }
}
