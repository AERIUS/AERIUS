/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import ol.Feature;
import ol.OLFactory;
import ol.geom.LineString;
import ol.geom.Point;
import ol.style.Icon;
import ol.style.IconOptions;
import ol.style.Style;

import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.resources.R;

/**
 * Provides a style with A and B icons at the start and the end of the line
 * Only is applied to emission sources that are registered
 */
public final class LineDirectionStyle {

  private static final Style LINE_DIRECTION_A_STYLE = createStyleFromIconSrc(R.images().iconLineDirectionA().getSafeUri().asString());
  private static final Style LINE_DIRECTION_B_STYLE = createStyleFromIconSrc(R.images().iconLineDirectionB().getSafeUri().asString());

  private static final Set<EmissionSourceType> SOURCE_TYPES_WITH_DIRECTION = new HashSet<>(
      Arrays.asList(
          EmissionSourceType.SRM1_ROAD, EmissionSourceType.SRM2_ROAD, EmissionSourceType.ADMS_ROAD,
          EmissionSourceType.SHIPPING_INLAND, EmissionSourceType.SHIPPING_MARITIME_INLAND,
          EmissionSourceType.SHIPPING_MARITIME_MARITIME
      )
  );

  private LineDirectionStyle() {
  }

  private static Style createStyleFromIconSrc(final String src) {
    final IconOptions iconOptions = new IconOptions();
    iconOptions.setSrc(src);
    return OLFactory.createStyle(new Icon(iconOptions));
  }

  public static boolean isStyleApplicable(final EmissionSourceFeature source) {
    return source.getGeometry() instanceof LineString && SOURCE_TYPES_WITH_DIRECTION.contains(source.getEmissionSourceType());
  }

  public static Style[] renderStyle(final Feature source) {
    final LineString lineStringGeometry = (LineString) source.getGeometry();
    final Style roadDirectionAStyle = LINE_DIRECTION_A_STYLE.clone();
    roadDirectionAStyle.setGeometry(new Point(lineStringGeometry.getFirstCoordinate()));

    final Style roadDirectionBStyle = LINE_DIRECTION_B_STYLE.clone();
    roadDirectionBStyle.setGeometry(new Point(lineStringGeometry.getLastCoordinate()));
    return new Style[] {roadDirectionAStyle, roadDirectionBStyle};
  }
}
