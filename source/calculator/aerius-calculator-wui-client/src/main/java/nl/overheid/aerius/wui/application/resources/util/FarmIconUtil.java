/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.resources.util;

import nl.overheid.aerius.shared.domain.sector.category.AnimalType;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalHousingCategories;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalHousingCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategories;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.wui.application.domain.source.farm.CustomFarmLodging;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmAnimalHousing;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmAnimalHousingType;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmAnimalHousingUtil;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmLodging;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmLodgingType;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmLodgingUtil;
import nl.overheid.aerius.wui.application.domain.source.farm.StandardFarmAnimalHousing;
import nl.overheid.aerius.wui.application.domain.source.farm.StandardFarmLodging;

/**
 * Util class for Farm related data.
 */
public final class FarmIconUtil {

  public enum AdditionalIcon {
    WARNING, ADJUSTED;

    public boolean is(final AdditionalIcon icon) {
      return this == icon;
    }
  }

  private static final String FARMLAND_PREFIX = "farmland";
  private static final String ANIMAL_PREFIX = "animal";

  private FarmIconUtil() {}

  public static String getFarmAnimalHousingIcon(final FarmAnimalHousingCategories categories, final FarmAnimalHousing farmAnimalHousing) {
    final AnimalType animalType = getFarmAnimalType(categories, farmAnimalHousing);

    return FontIconUtil.fontClassNamePlain(ANIMAL_PREFIX, animalType.name());
  }

  public static AnimalType getFarmAnimalType(final FarmAnimalHousingCategories categories, final FarmAnimalHousing farmAnimalHousing) {
    final FarmAnimalCategory category = categories.determineAnimalCategoryByCode(farmAnimalHousing.getAnimalTypeCode());

    // Fallback for cases where animal type is based on the actual enum (CustomFarmAnimalHousing)
    return category == null ? AnimalType.safeValueOf(farmAnimalHousing.getAnimalTypeCode()) : category.getAnimalType();
  }

  @Deprecated
  public static String getFarmLodgingIcon(final FarmLodging farmLodging) {
    final String code;
    if (farmLodging.getFarmLodgingType() == FarmLodgingType.STANDARD) {
      code = ((StandardFarmLodging) farmLodging).getFarmLodgingCode();
    } else {
      code = ((CustomFarmLodging) farmLodging).getAnimalCode();
    }
    return FontIconUtil.fontClassNamePlain(ANIMAL_PREFIX, AnimalType.getByCode(code).name());
  }

  /**
   * Returns an indicator if the given standard farm lodging type is adjusted or contains incompatible combinations.
   *
   * @param source farm animal housing source to check
   * @param farmAnimalHousingCategories farm animal housing categories
   * @return indicator enum or null if nothing to indicate
   */
  public static AdditionalIcon getFarmAnimalHousingAdditionalIcon(final FarmAnimalHousing source,
      final FarmAnimalHousingCategories farmAnimalHousingCategories) {
    AdditionalIcon additionalIcon = null;

    if (source.getAnimalHousingType() == FarmAnimalHousingType.STANDARD) {
      final StandardFarmAnimalHousing standardFarmAnimalHousing = (StandardFarmAnimalHousing) source;
      final String category = standardFarmAnimalHousing.getAnimalHousingCode();

      if (category != null) {
        final FarmAnimalHousingCategory farmAnimalHousingCategory = farmAnimalHousingCategories.determineHousingCategoryByCode(category);

        if (farmAnimalHousingCategory != null &&
            FarmAnimalHousingUtil.hasIncompatibleCombinations(standardFarmAnimalHousing, farmAnimalHousingCategories)) {
          additionalIcon = AdditionalIcon.WARNING;
        } else if (FarmAnimalHousingUtil.hasStacking(standardFarmAnimalHousing)) {
          additionalIcon = AdditionalIcon.ADJUSTED;
        }
      }
    }
    return additionalIcon;
  }

  /**
   * Returns an indicator if the given standard farm lodging type is adjusted or contains incompatible combinations.
   *
   * @param source farmlodging source to check
   * @param farmLodgingCategories farmloding categories
   * @return indicator enum or null if nothing to indicate
   */
  @Deprecated
  public static AdditionalIcon getFarmLodgingAdditionalIcon(final FarmLodging source, final FarmLodgingCategories farmLodgingCategories) {
    AdditionalIcon additionalIcon = null;

    if (source.getFarmLodgingType() == FarmLodgingType.STANDARD) {
      final StandardFarmLodging standardFarmLodging = (StandardFarmLodging) source;
      final String category = standardFarmLodging.getFarmLodgingCode();

      if (category != null) {
        final FarmLodgingCategory farmLodgingCategory = farmLodgingCategories.determineFarmLodgingCategoryByCode(category);

        if (farmLodgingCategory != null &&
            FarmLodgingUtil.hasIncompatibleCombinations(standardFarmLodging, farmLodgingCategory, farmLodgingCategories)) {
          additionalIcon = AdditionalIcon.WARNING;
        } else if (FarmLodgingUtil.hasStacking(standardFarmLodging)) {
          additionalIcon = AdditionalIcon.ADJUSTED;
        }
      }
    }
    return additionalIcon;
  }

  /**
   * Return icon for supplied farmlandCategoryCode.
   *
   * @param farmlandCategoryCode
   * @return string for icon
   */
  public static String getFarmlandIcon(final String farmlandCategoryCode) {
    return farmlandCategoryCode == null ? FontIconUtil.fontClassNamePlain(FARMLAND_PREFIX)
        : FontIconUtil.fontClassNamePlain(FARMLAND_PREFIX, farmlandCategoryCode);
  }
}
