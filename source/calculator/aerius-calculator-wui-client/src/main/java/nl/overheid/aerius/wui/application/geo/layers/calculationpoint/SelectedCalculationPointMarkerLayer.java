/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.calculationpoint;

import java.util.Collections;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.wui.application.command.calculationpoint.TemporaryCalculationPointAddCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.TemporaryCalculationPointClearCommand;
import nl.overheid.aerius.wui.application.context.MapConfigurationContext;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;
import nl.overheid.aerius.wui.application.event.calculationpoint.CalculationPointChangeEvent;
import nl.overheid.aerius.wui.application.geo.layers.LabelStyle;
import nl.overheid.aerius.wui.application.geo.layers.base.BaseMarkerLayer;
import nl.overheid.aerius.wui.application.geo.layers.base.LabelStyleCreator;
import nl.overheid.aerius.wui.application.geo.layers.base.VectorFeaturesWrapper;

/**
 * Layer to show the calculation point that is being edited on the map.
 */
public class SelectedCalculationPointMarkerLayer extends BaseMarkerLayer<CalculationPointFeature> {
  interface SelectedCalculationPointMarkerLayerEventBinder extends EventBinder<SelectedCalculationPointMarkerLayer> {}

  private final SelectedCalculationPointMarkerLayerEventBinder EVENT_BINDER = GWT.create(SelectedCalculationPointMarkerLayerEventBinder.class);

  private final VectorFeaturesWrapper vectorObject = new VectorFeaturesWrapper();

  @Inject
  public SelectedCalculationPointMarkerLayer(final EventBus eventBus, @Assisted final int zIndex,
      final MapConfigurationContext mapConfigurationContext) {
    super(null, zIndex, new LabelStyleCreator<CalculationPointFeature>(mapConfigurationContext::getIconScale, true,
        LabelStyle.CALCULATION_POINT));
    vectorObject.setMapFunction(getLabelStyleProvider()::createLabelFeature);
    setFeatures(vectorObject, Collections.emptyList());
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  public void onTemporaryCalculationPointAddCommand(final TemporaryCalculationPointAddCommand c) {
    vectorObject.addFeature(c.getValue());
  }

  @EventHandler
  public void onTemporaryEmissionSourceClearCommand(final TemporaryCalculationPointClearCommand c) {
    vectorObject.removeFeature(c.getValue());
  }

  @EventHandler
  public void onCalculationPointChangeEvent(final CalculationPointChangeEvent e) {
    vectorObject.updateFeature(e.getValue());
  }
}
