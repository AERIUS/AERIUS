/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.axellience.vuegwt.core.client.Vue;
import com.google.gwt.dom.client.Document;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import elemental2.core.JsError;

import jsinterop.base.Js;

import nl.aerius.wui.activity.ActivityManager;
import nl.aerius.wui.daemon.DaemonBootstrapper;
import nl.aerius.wui.dev.DevelopmentObserver;
import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.history.HistoryManager;
import nl.aerius.wui.init.Initializer;
import nl.aerius.wui.util.ExceptionHelper;
import nl.aerius.wui.util.NotificationUtil;
import nl.aerius.wui.util.SchedulerUtil;
import nl.aerius.wui.util.WebUtil;
import nl.aerius.wui.vue.AcceptsOneComponent;
import nl.overheid.aerius.wui.application.event.ApplicationFinishedLoadingEvent;
import nl.overheid.aerius.wui.bootstrap.Bootstrap;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;
import nl.overheid.aerius.wui.vue.VueRootView;
import nl.overheid.aerius.wui.vue.VueRootViewFactory;

/**
 * Root of the application logic.
 */
public class ApplicationRoot {

  private static final Logger LOG = Logger.getLogger(ApplicationRoot.class.getName());

  @SuppressWarnings("unused") @Inject private DevelopmentObserver development;

  @Inject private HistoryManager historyManager;

  @Inject private EventBus eventBus;

  @Inject EnvironmentConfiguration cfg;

  @Inject DaemonBootstrapper daemonBootstrapper;

  @Inject ActivityManager<AcceptsOneComponent> activityManager;

  @Inject VueRootViewFactory rootViewFactory;

  @Inject Initializer initializer;

  private VueRootView rootView;

  /**
   * Starts the application.
   *
   * @param finish
   */
  public void startUp(final Runnable finish) {
    final String root = Document.get().getElementsByTagName("base").getItem(0).getAttribute("href");
    WebUtil.setAbsoluteRoot(root);

    daemonBootstrapper.setEventBus(eventBus);

    rootView = rootViewFactory.create();
    activityManager.setPanel(rootView);

    initializer.init(() -> onFinishStartup(finish), e -> hideDisplayOnError(e, finish));
  }

  private void onFinishStartup(final Runnable finish) {
    finish.run();
    Vue.getConfig().setWarnHandler((msg, vue, info) -> SchedulerUtil.delay(() -> {
      GWTProd.warn("Warning within vue context: " + info);
      GWTProd.warn(msg);

      /**
       * Suppress Vue.js warning: Vue expects JS Object ({}), but receives JS primitives (e.g., Java String) causing type mismatch.
       */
      if (msg.startsWith("Invalid prop: type check failed for prop") && msg.contains("Expected Object")) {
        return;
      }

      NotificationUtil.broadcastWarning(eventBus, msg);
    }));
    Vue.getConfig().setErrorHandler((err, vue, info) -> SchedulerUtil.delay(() -> {
      GWTProd.error("Uncaught exception within vue context: " + info);
      GWTProd.error(err);
      final JsError ex = Js.uncheckedCast(err);
      LOG.log(Level.SEVERE, () -> "Uncaught exception within vue context: " + info
          + ", message:" + ex.message + (GWTProd.isDev() ? (", stacktrace: " + ex.stack) : ""));
      NotificationUtil.broadcastError(eventBus, ex.message);
    }));
    ExceptionHelper.setUncaughtExceptionHandler(eventBus, cfg.isProduction());

    // Indicate that the application has finished loading so specific css rules can be triggered
    Document.get().getBody().addClassName("aerius-loaded");

    rootView.vue().$mount("#base");
    historyManager.handleCurrentHistory();
    SchedulerUtil.delay(() -> eventBus.fireEvent(new ApplicationFinishedLoadingEvent()));
  }

  /**
   * Hides the main application display, if attached.
   */
  public void hideDisplayOnError(final Throwable e, final Runnable finish) {
    finish.run();
    Bootstrap.displayBootError(e);
  }
}
