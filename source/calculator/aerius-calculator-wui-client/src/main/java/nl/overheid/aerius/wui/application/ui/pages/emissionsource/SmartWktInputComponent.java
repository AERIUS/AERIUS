/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.emissionsource;

import static nl.aerius.geo.wui.util.OL3GeometryUtil.WKT_DECIMALS;

import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import ol.Coordinate;
import ol.EventsKey;
import ol.Observable;
import ol.geom.Geometry;
import ol.geom.GeometryType;
import ol.geom.LineString;
import ol.geom.Polygon;

import elemental2.dom.HTMLInputElement;
import elemental2.dom.InputEvent;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.geo.wui.util.OL3GeometryUtil;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.InputWarningComponent;
import nl.overheid.aerius.wui.application.components.input.MinimalInputComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.daemon.misc.PersistablePreferencesContext;
import nl.overheid.aerius.wui.application.geo.util.GeoType;
import nl.overheid.aerius.wui.application.geo.util.GeoUtil;
import nl.overheid.aerius.wui.application.geo.util.hull.ConvexHullGrahamScan;
import nl.overheid.aerius.wui.application.geo.util.hull.Point;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.application.util.NumberUtil;
import nl.overheid.aerius.wui.application.util.WKTUtil;
import nl.overheid.aerius.wui.application.util.WKTUtil.GeometryResult;

@Component(components = {
    MinimalInputComponent.class,
    ValidationBehaviour.class,
    VerticalCollapse.class,
    InputErrorComponent.class,
    InputWarningComponent.class,
})
public class SmartWktInputComponent extends ErrorWarningValidator implements IsVueComponent {
  private static final int M_IN_HA = 10_000;

  @Prop Geometry geometry;
  @Prop GeometryType geometryType;

  @Prop(required = true) @JsProperty List<GeoType> validGeometryTypes;

  @Prop boolean showErrors;
  @Prop boolean validateConvex;

  @Prop String id;

  @Data String locationString;
  @Data GeometryResult conversionResult;
  @Data boolean selfIntersecting;

  @Inject @Data ApplicationContext appContext;
  @Inject @Data PersistablePreferencesContext prefContext;

  private EventsKey on;

  @PropDefault("validateConvex")
  public boolean validateConvex() {
    return false;
  }

  @Computed("isGeometryConcave")
  public boolean isGeometryConcave() {
    if (!validateConvex) {
      return false;
    }

    final GeometryResult result = parseWkt();
    if (result.getGeometry() == null) {
      return false;
    }

    final Geometry geometry = result.getGeometry();
    if (!GeoType.POLYGON.is(geometry)) {
      return false;
    }

    final Polygon polygon = (Polygon) geometry.clone();
    final ConvexHullGrahamScan hullBuilder = new ConvexHullGrahamScan();
    final Coordinate[][] coordinates = polygon.getCoordinates();
    Stream.of(coordinates[0]).forEach(coordinate -> hullBuilder.addPoint(coordinate.getX(), coordinate.getY()));
    final Point[] hull = hullBuilder.getHull();
    final Coordinate[] olCoords = new Coordinate[hull.length];
    for (int i = 0; i < hull.length; i++) {
      olCoords[i] = new Coordinate(hull[i].x, hull[i].y);
    }
    final Polygon hullPolygon = new Polygon(new Coordinate[][] {olCoords});
    if (NumberUtil.equalEnough(polygon.getArea(), hullPolygon.getArea(), 0D)) {
      return false;
    }

    // If here, the only remaining option is the geometry is concave
    return true;
  }

  @Computed("geometryConcaveWarning")
  public Supplier<String> geometryConcaveWarning() {
    return () -> i18n.esLocationGeometryConcaveWarning();
  }

  @Computed("isGeometryTooLarge")
  public boolean isGeometryTooLarge() {
    final GeometryResult result = parseWkt();
    if (result.getGeometry() == null) {
      return false;
    }

    final Geometry parsedGeometry = result.getGeometry();
    if (GeoType.LINE_STRING.is(parsedGeometry)) {
      return ((LineString) parsedGeometry).getLength() > (int) appContext.getSetting(SharedConstantsEnum.CALCULATOR_LIMITS_MAX_LINE_LENGTH);
    } else if (GeoType.POLYGON.is(parsedGeometry)) {
      return ((Polygon) parsedGeometry).getArea() > M_IN_HA * (int) appContext.getSetting(SharedConstantsEnum.CALCULATOR_LIMITS_MAX_POLYGON_SURFACE);
    } else {
      return false;
    }
  }

  @Computed("geometryToLargeWarning")
  public Supplier<String> geometryToLargeWarning() {
    return () -> {
      final GeometryResult result = parseWkt();
      if (result.getGeometry() == null) {
        return "";
      }
      final Geometry parsedGeometry = result.getGeometry();
      final String limit;

      if (GeoType.LINE_STRING.is(parsedGeometry)) {
        limit = MessageFormatter.formatDistanceWithUnit((int) appContext.getSetting(SharedConstantsEnum.CALCULATOR_LIMITS_MAX_LINE_LENGTH));
      } else if (GeoType.POLYGON.is(parsedGeometry)) {
        limit = MessageFormatter.formatSurfaceWithUnit((int) appContext.getSetting(SharedConstantsEnum.CALCULATOR_LIMITS_MAX_POLYGON_SURFACE));
      } else {
        limit = "";
      }
      return i18n.esLocationGeometryTooLargeWarning(result.getGeometry().getType(), limit);
    };
  }

  @Watch(value = "showErrors", isImmediate = true)
  public void onShowErrorsChange(final boolean neww) {
    displayProblems(neww);
  }

  @Watch(value = "geometry", isImmediate = true)
  public void onGeometryChange(final Geometry geom) {
    updateGeometry(geom);
    if (on != null) {
      Observable.unByKey(on);
      on = null;
    }
    if (geom != null) {
      on = geom.on("change", v -> {
        updateGeometry(geom);
      });
    }
  }

  private void updateGeometry(final Geometry geom) {
    locationString = Optional.ofNullable(geom)
        .map(v -> OL3GeometryUtil.toWktString(geom))
        .orElse("");

    selfIntersecting = Optional.ofNullable(geom)
        .filter(GeoType.POLYGON::is)
        .map(g -> (Polygon) g)
        .map(GeoUtil::isSelfIntersecting)
        .orElse(false);

    if (conversionResult != null) {
      if (conversionResult.getGeometry() != geom) {
        conversionResult = null;
      }
    }
  }

  @Computed
  public String getLocationDisplayString() {
    return conversionResult != null
        && conversionResult.getGeometry() != null
        && conversionResult.getGeometry() == geometry
            ? conversionResult.getWktInput()
            : locationString;
  }

  @Computed("isConvertedGeometry")
  public boolean isConvertedGeometry() {
    return conversionResult != null
        && conversionResult.getGeometry() != null
        && !conversionResult.getEpsgInput().equals(conversionResult.getEpsgOutput());
  }

  @Computed
  public String getConversionNotice() {
    return i18n.esLocationConversion(conversionResult.getEpsgInput(), conversionResult.getEpsgOutput());
  }

  @Computed("isConversionError")
  public boolean isConversionError() {
    return conversionResult != null
        && conversionResult.getErrorMessage() != null;
  }

  @Computed
  public Supplier<String> getConversionErrorNotice() {
    return () -> conversionResult.getErrorMessage();
  }

  @JsMethod
  public void insertConvertedCoordinates() {
    // "insert" it by "forgetting" the conversion
    conversionResult = null;
  }

  @Computed
  public String getConversionResultWkt() {
    return conversionResult.getWktOutput();
  }

  @Watch(value = "locationString", isImmediate = true)
  public void onLocationStringChange() {
    vue().$emit("wkt-change", locationString);
  }

  @JsMethod
  public String getPlaceholderText() {
    return i18n.esLocationDirectPlaceholder(Optional.ofNullable(geometryType)
        .orElse(GeometryType.Point).name());
  }

  @JsMethod
  public void updateFromDirectInput(final InputEvent event) {
    final String wktInput = ((HTMLInputElement) event.target).value;

    locationString = wktInput;
    conversionResult = parseWkt();
    if (conversionResult.getGeometry() == null) {
      return;
    }

    vue().$emit("geometry-change", conversionResult.getGeometry());
  }

  private GeometryResult parseWkt() {
    final List<String> convertibleSystems = appContext.getConfiguration().getConvertibleGeometrySystems();
    return WKTUtil.tryReadGeometry(locationString, convertibleSystems, prefContext.getSelectedProjection(), appContext.getTheme(),
        geometryType);
  }

  @Computed("isValidGeometryType")
  public boolean isValidGeometryType() {
    final Geometry geom = parseWkt().getGeometry();
    return geom != null && validGeometryTypes.stream().anyMatch(geoType -> geoType.is(geom));
  }

  @Computed("isGeometryValid")
  public boolean isGeometryValid() {
    return parseWkt().getGeometry() != null;
  }

  @Computed("isGeometryComplete")
  public boolean isGeometryComplete() {
    final Geometry geometry = parseWkt().getGeometry();
    if (geometry == null) {
      return false;
    }
    if (geometry.getType().equals(GeometryType.Polygon.name())) {
      final Polygon polygon = (Polygon) geometry;
      return polygon.getFirstCoordinate().toStringXY(WKT_DECIMALS).equals(polygon.getLastCoordinate().toStringXY(WKT_DECIMALS));
    }
    return true;
  }

  @Computed
  public Supplier<String> getGeometryValidErrorMessage() {
    return () -> {
      if (!isGeometryValid()) {
        return i18n.geometryInvalid(appContext.getTheme(), geometryType == null ? null : geometryType);
      }
      if (!isGeometryComplete()) {
        return i18n.geometryPolygonIncomplete();
      }
      if (!isValidGeometryType()) {
        return i18n.esGeometryTypeIllegal();
      }
      return "";
    };
  }
}
