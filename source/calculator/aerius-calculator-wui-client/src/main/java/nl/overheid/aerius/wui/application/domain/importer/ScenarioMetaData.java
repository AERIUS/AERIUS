/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.domain.importer;

import static jsinterop.annotations.JsPackage.GLOBAL;

import java.util.Objects;

import elemental2.core.Global;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.wui.application.util.json.JsonBuilder;
import nl.overheid.aerius.wui.vue.ReactivityUtil;

@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class ScenarioMetaData {
  private String corporation;
  private String projectName;
  private String description;
  private String streetAddress;
  private String postcode;
  private String city;

  public static final @JsOverlay ScenarioMetaData create() {
    final ScenarioMetaData scenarioMetaData = new ScenarioMetaData();
    init(scenarioMetaData);
    return scenarioMetaData;
  }

  public static final @JsOverlay void init(final ScenarioMetaData scenarioMetaData) {
    ReactivityUtil.ensureJsProperty(scenarioMetaData, "corporation", scenarioMetaData::setCorporation, "");
    ReactivityUtil.ensureJsProperty(scenarioMetaData, "projectName", scenarioMetaData::setProjectName, "");
    ReactivityUtil.ensureJsProperty(scenarioMetaData, "description", scenarioMetaData::setDescription, "");
    ReactivityUtil.ensureJsProperty(scenarioMetaData, "streetAddress", scenarioMetaData::setStreetAddress, "");
    ReactivityUtil.ensureJsProperty(scenarioMetaData, "postcode", scenarioMetaData::setPostcode, "");
    ReactivityUtil.ensureJsProperty(scenarioMetaData, "city", scenarioMetaData::setCity, "");
  }

  public final @JsOverlay String getCity() {
    return city;
  }

  public final @JsOverlay void setCity(final String city) {
    this.city = city;
  }

  public final @JsOverlay String getPostcode() {
    return postcode;
  }

  public final @JsOverlay void setPostcode(final String postcode) {
    this.postcode = postcode;
  }

  public final @JsOverlay String getStreetAddress() {
    return streetAddress;
  }

  public final @JsOverlay void setStreetAddress(final String streetAddress) {
    this.streetAddress = streetAddress;
  }

  public final @JsOverlay String getDescription() {
    return description;
  }

  public final @JsOverlay void setDescription(final String description) {
    this.description = description;
  }

  public final @JsOverlay String getProjectName() {
    return projectName;
  }

  public final @JsOverlay void setProjectName(final String projectName) {
    this.projectName = projectName;
  }

  public final @JsOverlay String getCorporation() {
    return corporation;
  }

  public final @JsOverlay void setCorporation(final String corporation) {
    this.corporation = corporation;
  }

  @Override
  public final @JsOverlay int hashCode() {
    return Objects.hash(city, corporation, description, postcode, projectName, streetAddress);
  }

  @Override
  public final @JsOverlay boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    final ScenarioMetaData other = (ScenarioMetaData) obj;
    return Objects.equals(city, other.city)
        && Objects.equals(corporation, other.corporation)
        && Objects.equals(description, other.description)
        && Objects.equals(postcode, other.postcode)
        && Objects.equals(projectName, other.projectName)
        && Objects.equals(streetAddress, other.streetAddress);
  }

  public final @JsOverlay Object toJSON() {
    return new JsonBuilder(this)
        .include("city", "corporation", "description", "postcode", "projectName", "streetAddress")
        .build();
  }

  public final @JsOverlay String toJSONString() {
    return Global.JSON.stringify(toJSON());
  }
}
