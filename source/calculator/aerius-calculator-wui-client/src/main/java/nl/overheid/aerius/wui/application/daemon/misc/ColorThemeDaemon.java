/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.misc;

import javax.inject.Singleton;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import elemental2.dom.DomGlobal;
import elemental2.dom.HTMLElement;

import jsinterop.base.Js;

import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.shared.domain.ProductProfile;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.wui.application.command.misc.ColorTheme;
import nl.overheid.aerius.wui.application.command.misc.ColorThemeChangeCommand;

@Singleton
public class ColorThemeDaemon extends BasicEventComponent {
  private static final ColorThemeDaemonEventBinder EVENT_BINDER = GWT.create(ColorThemeDaemonEventBinder.class);

  interface ColorThemeDaemonEventBinder extends EventBinder<ColorThemeDaemon> {}

  @EventHandler
  public void onColorThemeChangeCommand(final ColorThemeChangeCommand c) {
    changeTheme(c.getValue());
  }

  public void initColorTheme(final Theme theme, final ProductProfile productProfile) {
    final ColorTheme colorTheme = productProfile == ProductProfile.LBV_POLICY
        ? ColorTheme.ORANGE
        : theme == Theme.NCA
            ? ColorTheme.UK_BLUE
            : ColorTheme.DEFAULT_BLUE;

    eventBus.fireEvent(new ColorThemeChangeCommand(colorTheme));
  }

  private void changeTheme(final ColorTheme theme) {
    final HTMLElement documentElement = Js.uncheckedCast(DomGlobal.document.documentElement);
    documentElement.style.setProperty("--theme-color", theme.getThemeColor());
    documentElement.style.setProperty("--theme-color-light", theme.getThemeColorLight());
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
