/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.service;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.ImplementedBy;

import nl.overheid.aerius.js.file.FileValidationStatus;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;

@ImplementedBy(FileServiceAsyncImpl.class)
public interface FileServiceAsync {
  void uploadFile(FileUploadStatus file, AsyncCallback<String> callback);

  void removeFile(FileUploadStatus file, AsyncCallback<Boolean> callback);

  <T> void fetchJson(String fileCode, boolean keepFile, AsyncCallback<T> callback);

  void fetchValidationResult(String fileCode, AsyncCallback<FileValidationStatus> callback);

  void fetchValidationReport(List<String> fileCodes);
}
