/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Document;

import ol.Collection;
import ol.Coordinate;
import ol.Feature;
import ol.OLFactory;
import ol.Overlay;
import ol.OverlayOptions;
import ol.Pixel;
import ol.geom.Geometry;
import ol.geom.GeometryType;
import ol.geom.LineString;
import ol.interaction.Draw;
import ol.interaction.DrawOptions;
import ol.layer.Vector;
import ol.source.VectorOptions;

import jsinterop.base.Js;

import nl.aerius.geo.domain.IsInteraction;
import nl.aerius.geo.domain.IsLayer;
import nl.aerius.geo.domain.IsOverlay;
import nl.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.wui.application.geo.interactions.FeatureDrawStyle;
import nl.overheid.aerius.wui.application.i18n.M;

public class MeasureTool {
  private final IsLayer<ol.layer.Vector> layer;
  private final IsInteraction<Draw> interaction;
  private final List<IsOverlay<Overlay>> overlays = new ArrayList<>();

  private final ol.source.Vector vectorSource;
  private final Consumer<IsOverlay<Overlay>> onOverlay;
  private DivElement activeElem;
  private Overlay activeMeasureTooltip;

  public MeasureTool(final Consumer<IsOverlay<Overlay>> onOverlay) {
    this.onOverlay = onOverlay;

    final Collection<Feature> featureCollection = new Collection<>();
    final VectorOptions vectorSourceOptions = OLFactory.createOptions();
    vectorSourceOptions.setFeatures(featureCollection);
    vectorSource = new ol.source.Vector();

    interaction = createInteraction();
    layer = createVector();

    interaction.asInteraction().on("drawstart", evt -> {
      publishOverlay();

      final Draw.Event drawEvent = Js.cast(evt);
      final Geometry geom = drawEvent.getFeature().getGeometry();
      updateTooltip(geom);
      geom.on("change", change -> {
        updateTooltip(geom);
      });
    });
  }

  private void updateTooltip(final Geometry geom) {
    final LineString line = Js.cast(geom);
    final double length = line.getLength();

    final Coordinate lastCoordinate = line.getLastCoordinate();
    activeMeasureTooltip.setPosition(lastCoordinate);

    final String lengthText = formatLength(length);

    activeElem.setInnerHTML(lengthText);
  }

  private String formatLength(final double length) {
    return M.messages().unitM(M.messages().decimalNumberFixed(length, 2));
  }

  private void publishOverlay() {
    activeElem = Document.get().createDivElement();
    activeElem.addClassName("ol-tooltip");
    activeElem.addClassName("ol-tooltip-measure");

    final OverlayOptions options = OLFactory.createOptions();
    options.setElement(activeElem);
    options.setInsertFirst(false);
    options.setStopEvent(false);
    options.setPositioning("bottom-center");
    options.setOffset(new Pixel(0, -16));
    final Overlay tooltip = new Overlay(options);
    activeMeasureTooltip = tooltip;

    final IsOverlay<Overlay> overlay = () -> tooltip;
    overlays.add(overlay);
    onOverlay.accept(overlay);
  }

  private IsLayer<Vector> createVector() {
    final ol.layer.VectorLayerOptions vectorLayerOptions = OLFactory.createOptions();
    vectorLayerOptions.setSource(vectorSource);
    final ol.layer.Vector vector = new ol.layer.Vector(vectorLayerOptions);
    return new IsLayer<ol.layer.Vector>() {
      @Override
      public ol.layer.Vector asLayer() {
        return vector;
      }

      @Override
      public LayerInfo getInfo() {
        return null;
      }
    };
  }

  private IsInteraction<Draw> createInteraction() {
    final DrawOptions drawOptions = new DrawOptions();
    drawOptions.setType(GeometryType.LineString.name());
    drawOptions.setSource(vectorSource);
    drawOptions.setStyle(FeatureDrawStyle.DRAW_STYLE);

    final Draw draw = new Draw(drawOptions);

    return () -> draw;
  }

  public IsInteraction<?> getInteraction() {
    return interaction;
  }

  public IsLayer<ol.layer.Vector> getLayer() {
    return layer;
  }

  public void clear() {
    vectorSource.clear(false);
    overlays.clear();
  }

  public List<IsOverlay<Overlay>> getOverlays() {
    return overlays;
  }
}
