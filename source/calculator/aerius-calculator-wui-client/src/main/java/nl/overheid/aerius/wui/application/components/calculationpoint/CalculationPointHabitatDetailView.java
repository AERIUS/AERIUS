/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.calculationpoint;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasDestroyed;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.application.command.CalculationPointDetailEditorErrorChangeCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.calculationpoint.CalculationPointHabitatValidators.CalculationPointHabitatValidations;
import nl.overheid.aerius.wui.application.components.input.CheckBoxComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.CalculationPointEditorContext;
import nl.overheid.aerius.wui.application.domain.calculationpoint.CalculationPointEntityReference;
import nl.overheid.aerius.wui.application.event.calculationpoint.CalculationPointSaveEvent;
import nl.overheid.aerius.wui.application.ui.main.LeftPanelCloseEvent;

@Component(customizeOptions = {
    CalculationPointHabitatValidators.class,
}, directives = {
    ValidateDirective.class,
}, components = {
    VerticalCollapse.class,
    LabeledInputComponent.class,
    CheckBoxComponent.class,
    ValidationBehaviour.class,
    ButtonIcon.class
})
public class CalculationPointHabitatDetailView extends ErrorWarningValidator
    implements HasCreated, HasDestroyed, HasValidators<CalculationPointHabitatValidations> {
  private static final CalculationPointHabitatDetailViewEventBinder EVENT_BINDER = GWT.create(CalculationPointHabitatDetailViewEventBinder.class);

  interface CalculationPointHabitatDetailViewEventBinder extends EventBinder<CalculationPointHabitatDetailView> {}

  @Prop EventBus eventBus;
  @Prop(required = true) CalculationPointEntityReference habitat;

  @Data String nameV;
  @Data String criticalLevelNOxV;
  @Data String criticalLevelNH3V;
  @Data String criticalLoadV;
  @Data boolean criticalLevelNOxEnabled;
  @Data boolean criticalLevelNH3Enabled;
  @Data boolean criticalLoadEnabled;

  @JsProperty(name = "$v") CalculationPointHabitatValidations validation;

  @Inject @Data CalculationPointEditorContext context;

  private HandlerRegistration handlers;

  @Override
  @Computed
  public CalculationPointHabitatValidations getV() {
    return validation;
  }

  @Override
  public void created() {
    handlers = EVENT_BINDER.bindEventHandlers(this, eventBus);
    VuelidateUtil.proxy(this);
  }

  @Watch(value = "habitat", isImmediate = true)
  public void onHabitatChange(final CalculationPointEntityReference neww) {
    if (neww == null) {
      return;
    }

    nameV = habitat.getDescription();
    criticalLevelNOxEnabled = habitat.getCriticalLevel(EmissionResultKey.NOX_CONCENTRATION) != 0;
    criticalLevelNH3Enabled = habitat.getCriticalLevel(EmissionResultKey.NH3_CONCENTRATION) != 0;
    criticalLoadEnabled = habitat.getCriticalLevel(EmissionResultKey.NOXNH3_DEPOSITION) != 0;
    criticalLevelNOxV = String.valueOf(habitat.getCriticalLevel(EmissionResultKey.NOX_CONCENTRATION));
    criticalLevelNH3V = String.valueOf(habitat.getCriticalLevel(EmissionResultKey.NH3_CONCENTRATION));
    criticalLoadV = String.valueOf(habitat.getCriticalLevel(EmissionResultKey.NOXNH3_DEPOSITION));
  }

  @Override
  public void destroyed() {
    handlers.removeHandler();
  }

  @Computed
  protected String getName() {
    return nameV;
  }

  @Computed
  protected void setName(final String name) {
    habitat.setDescription(name);
    nameV = name;
  }

  @Computed
  protected String getCriticalLevelNOx() {
    return criticalLevelNOxV;
  }

  @Computed
  protected void setCriticalLevelNOx(final String value) {
    if (!criticalLevelNOxEnabled) {
      return;
    }

    criticalLevelNOxV = value;
    setCriticalValue(EmissionResultKey.NOX_CONCENTRATION, value);
  }

  @Computed
  protected String getCriticalLevelNH3() {
    return criticalLevelNH3V;
  }

  @Computed
  protected void setCriticalLevelNH3(final String value) {
    if (!criticalLevelNH3Enabled) {
      return;
    }

    criticalLevelNH3V = value;
    setCriticalValue(EmissionResultKey.NH3_CONCENTRATION, value);
  }

  @Computed
  protected String getCriticalLoad() {
    return criticalLoadV;
  }

  @Computed
  protected void setCriticalLoad(final String value) {
    if (!criticalLoadEnabled) {
      return;
    }

    criticalLoadV = value;
    setCriticalValue(EmissionResultKey.NOXNH3_DEPOSITION, value);
  }

  @Computed
  public String getCriticalLevelInputError(final String label) {
    return i18n.ivDoubleLowerlimit(label, 0);
  }

  @Computed
  public String getCriticalLoadInputError(final String label) {
    return i18n.ivDoubleLowerlimit(label, 0);
  }

  @Watch("criticalLevelNOxEnabled")
  public void onCriticalLevelNOxEnabledChange(final boolean neww) {
    if (neww) {
      criticalLevelNOxEnabled = true;
      setCriticalValue(EmissionResultKey.NOX_CONCENTRATION, criticalLevelNOxV);
    } else {
      criticalLevelNOxEnabled = false;
      resetCriticalValue(EmissionResultKey.NOX_CONCENTRATION);
    }
  }

  @Watch("criticalLevelNH3Enabled")
  public void onCriticalLevelNH3EnabledChange(final boolean neww) {
    if (neww) {
      criticalLevelNH3Enabled = true;
      setCriticalValue(EmissionResultKey.NH3_CONCENTRATION, criticalLevelNH3V);
    } else {
      criticalLevelNH3Enabled = false;
      resetCriticalValue(EmissionResultKey.NH3_CONCENTRATION);
    }
  }

  @Watch("criticalLoadEnabled")
  public void onCriticalLoadEnabledChange(final boolean neww) {
    if (neww) {
      criticalLoadEnabled = true;
      setCriticalValue(EmissionResultKey.NOXNH3_DEPOSITION, criticalLoadV);
    } else {
      criticalLoadEnabled = false;
      resetCriticalValue(EmissionResultKey.NOXNH3_DEPOSITION);
    }
  }

  private void setCriticalValue(final EmissionResultKey key, final String value) {
    ValidationUtil.setSafeDoubleValue(v -> habitat.setCriticalLevel(key, v), value, 0D);
  }

  private void resetCriticalValue(final EmissionResultKey key) {
    habitat.setCriticalLevel(key, null);
  }

  @JsMethod
  public void onDetailEditorErrorChange(final boolean error) {
    eventBus.fireEvent(new CalculationPointDetailEditorErrorChangeCommand(error));
  }

  @EventHandler
  public void onLeftPanelCloseEvent(final LeftPanelCloseEvent e) {
    close();
  }

  @EventHandler
  public void onCalculationPointSaveEvent(final CalculationPointSaveEvent e) {
    close();
  }

  @JsMethod
  public void close() {
    context.selectHabitat(null);
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return validation.nameV.invalid
        || (criticalLevelNOxEnabled && validation.criticalLevelNOxV.invalid)
        || (criticalLevelNH3Enabled && validation.criticalLevelNH3V.invalid)
        || (criticalLoadEnabled && validation.criticalLoadV.invalid);
  }
}
