/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.road.standard;

import nl.overheid.aerius.wui.application.domain.source.road.StandardVehicles;
import nl.overheid.aerius.wui.application.i18n.ApplicationMessages;

/**
 * Util to display speed values for different speed categories of standard road vehicles.
 */
public final class StandardRoadCategoryUtil {

  private StandardRoadCategoryUtil() {
    // Util class
  }

  public static String roadCategoryDisplayText(final ApplicationMessages i18n, final StandardVehicles standardVehicles) {
    return roadCategoryDisplayText(i18n, standardVehicles.getMaximumSpeed(), standardVehicles.isStrictEnforcement());
  }

  public static String roadCategoryDisplayText(final ApplicationMessages i18n, final int maximumSpeed, final boolean strict) {
    final String text;

    if (hasMaximumSpeed(maximumSpeed)) {
      text = strict ? i18n.esRoadCategoryStrict(maximumSpeed) : i18n.esRoadCategory(maximumSpeed);
    } else {
      text = i18n.esRoadSelectType(nl.overheid.aerius.wui.application.domain.source.road.VehicleType.STANDARD);
    }
    return text;
  }

  public static boolean hasMaximumSpeed(final int maximumSpeed) {
    return maximumSpeed > 0;
  }
}
