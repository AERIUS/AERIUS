/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.info;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.ImaerConstants;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.application.command.geo.HabitatTypeHoverActiveCommand;
import nl.overheid.aerius.wui.application.command.geo.HabitatTypeHoverInactiveCommand;
import nl.overheid.aerius.wui.application.command.geo.HabitatTypeSelectToggleCommand;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.result.ResultSelectionContext;
import nl.overheid.aerius.wui.application.domain.info.HabitatInfo;
import nl.overheid.aerius.wui.application.domain.result.CriticalLevels;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.application.ui.pages.results.GeneralizedEmissionResultType;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    VerticalCollapse.class,
    BasicInfoRowComponent.class,
    SimplifiedListBoxComponent.class
})
public class HabitatTypeComponent extends BasicVueComponent {
  @Prop EventBus eventBus;
  @Prop @JsProperty List<HabitatInfo> habitatTypeInfo;

  @Data EmissionResultKey selectedCriticalLevelKey = EmissionResultKey.NOXNH3_DEPOSITION;

  @Inject @Data ResultSelectionContext context;
  @Inject @Data ApplicationContext applicationContext;

  @Computed
  public List<EmissionResultKey> getCriticalLevelKeys() {
    return applicationContext.getConfiguration().getEmissionResultKeys();
  }

  @Computed("showCriticalLevelSelector")
  public boolean showCriticalLevelSelector() {
    return getCriticalLevelKeys().size() > 1;
  }

  @JsMethod
  public void selectCriticalLevelKey(final EmissionResultKey option) {
    selectedCriticalLevelKey = option;
  }

  @Computed
  public String getCriticalLevelHeader() {
    final GeneralizedEmissionResultType resultType = GeneralizedEmissionResultType
        .fromEmissionResultType(selectedCriticalLevelKey.getEmissionResultType());
    return i18n.infoPanelHabitatCriticalLevel(i18n.unitSingularResult(resultType,
        applicationContext.getConfiguration().getEmissionResultValueDisplaySettings().getDisplayType()));
  }

  @JsMethod
  public String getCriticalLevel(final HabitatInfo habitatType) {
    final CriticalLevels criticalLevels = habitatType.getCriticalLevels();
    if (criticalLevels.hasKey(selectedCriticalLevelKey)) {
      final double criticalLevel = criticalLevels.getEmissionResultValue(selectedCriticalLevelKey);
      return MessageFormatter.formatCL(criticalLevel, selectedCriticalLevelKey,
          applicationContext.getConfiguration().getEmissionResultValueDisplaySettings());
    } else {
      return i18n.valueNotApplicable();
    }
  }

  @JsMethod
  public boolean isSelected(final HabitatInfo info) {
    return context.getSelectedHabitatTypes().contains(String.valueOf(info.getId()));
  }

  @JsMethod
  public double getOverlap(final HabitatInfo habitatInfo) {
    return habitatInfo.getCartographicSurface() / ImaerConstants.M2_TO_HA;
  }

  @JsMethod
  public void rowToggle(final HabitatInfo row) {
    final String habitatId = String.valueOf(row.getId());
    eventBus.fireEvent(new HabitatTypeSelectToggleCommand(habitatId, false));
  }

  @JsMethod
  public void showHabitatInfoPanel(final boolean show, final HabitatInfo habitatInfo) {
    if (show) {
      eventBus.fireEvent(new HabitatTypeHoverActiveCommand(String.valueOf(habitatInfo.getId()), false));
    } else {
      eventBus.fireEvent(new HabitatTypeHoverInactiveCommand(String.valueOf(habitatInfo.getId())));
    }
  }

}
