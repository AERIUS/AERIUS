/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.farmland.standard;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.FarmSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmlandCategory;
import nl.overheid.aerius.wui.application.components.source.DetailStatComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.farmland.FarmlandStandardUtil;
import nl.overheid.aerius.wui.application.domain.source.farmland.FarmlandUtil;
import nl.overheid.aerius.wui.application.domain.source.farmland.StandardFarmlandActivity;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "aer-farmland-standard-detail", components = {
    DetailStatComponent.class,
})
public class FarmlandStandardDetailComponent extends BasicVueComponent {

  @Prop @JsProperty protected StandardFarmlandActivity source;
  @Prop @JsProperty protected int idx;

  @Data @Inject ApplicationContext applicationContext;

  @Computed
  public String getCategoryLabel() {
    return M.messages().esFarmlandStandardType(source == null ? null : source.getActivityCode());
  }

  @Computed
  protected String description() {
    final FarmSourceCategory selectedFarmSourceCategory = getFarmSourceCategory(source.getFarmSourceCategoryCode());
    return selectedFarmSourceCategory != null ? selectedFarmSourceCategory.getDescription() : "";
  }

  @Computed("substances")
  public List<Substance> getSubstances() {
    return applicationContext.getConfiguration().getSubstances();
  }

  @JsMethod
  protected String emission(final Substance substance) {
    return emission2String(source.getEmission(substance));
  }

  @Computed
  protected String metersCubed() {
    return M.messages().unitMetersCubed(String.valueOf(source.getMetersCubed()));
  }

  protected String emission2String(final double emission) {
    return MessageFormatter.formatEmissionWithUnitSmart(emission);
  }

  protected FarmlandCategory getFarmlandCategory(final String category) {
    return FarmlandUtil.findFarmland(category, applicationContext.getConfiguration().getSectorCategories().getFarmlandCategories());
  }

  protected FarmSourceCategory getFarmSourceCategory(final String categoryCode) {
    return FarmlandStandardUtil.findFarmSourceCategory(categoryCode,
        applicationContext.getConfiguration().getSectorCategories().getFarmSourceCategories());
  }

}
