/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.place;

import java.util.Map;
import java.util.function.Supplier;

import nl.aerius.wui.place.PlaceTokenizer;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfileType;

/**
 * Place to display a single theme source.
 */
public class TimeVaryingProfilePlace extends MainThemePlace {
  private static final String PLACE_KEY_SOURCE = "timevarying";

  public static class Tokenizer extends MainThemePlace.Tokenizer<TimeVaryingProfilePlace> {
    private CustomTimeVaryingProfileType type;

    public Tokenizer(final Supplier<TimeVaryingProfilePlace> provider, final Theme theme, final String... postfix) {
      super(provider, theme, postfix);
      if (postfix.length == 2) {
        type = CustomTimeVaryingProfileType.safeValueOf(postfix[1]);
      }
    }

    public CustomTimeVaryingProfileType getType() {
      return type;
    }

    @Override
    protected void updatePlace(final Map<String, String> tokens, final TimeVaryingProfilePlace place) {
      if (!tokens.isEmpty()) {
        place.setProfileId(tokens.entrySet().iterator().next().getKey());
      }
    }

    @Override
    protected void setTokenMap(final TimeVaryingProfilePlace place, final Map<String, String> tokens) {
      final String id = place.getProfileId();

      if (id != null) {
        tokens.put(String.valueOf(id), null);
      }
    }
  }

  private CustomTimeVaryingProfileType type;
  private String profileId;

  public <P extends MainThemePlace> TimeVaryingProfilePlace(final Theme theme, final CustomTimeVaryingProfileType type) {
    this(createTokenizer(theme, type));
    this.type = type;
  }

  public <P extends MainThemePlace> TimeVaryingProfilePlace(final PlaceTokenizer<P> tokenizer) {
    super(tokenizer);
  }

  public static PlaceTokenizer<TimeVaryingProfilePlace> createTokenizer(final Theme theme, final CustomTimeVaryingProfileType type) {
    return new Tokenizer(() -> new TimeVaryingProfilePlace(theme, type), theme, PLACE_KEY_SOURCE, type.name().toLowerCase());
  }

  public CustomTimeVaryingProfileType getType() {
    return type;
  }

  public String getProfileId() {
    return profileId;
  }

  public void setProfileId(final String profileId) {
    this.profileId = profileId;
  }

  @Override
  public <T> T visit(final PlaceVisitor<T> visitor) {
    return visitor.apply(this);
  }
}
