/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.input.suggest;

import jsinterop.annotations.JsType;

@JsType
public class SuggestListBoxData {
  private String code;
  private String name;
  private String description;
  private boolean warn;

  /**
   * Constructor.
   *
   * @param code Key of the data. Should be unique over all entries in the list
   * @param name Short text shown as the selected name in the list box
   * @param description Detailed text shown in the opened list and on hover of the entry in the list box
   * @param warn Show the entry in the list with the warning color as background color
   */
  public SuggestListBoxData(final String code, final String name, final String description, final boolean warn) {
    this.code = code;
    this.name = name;
    this.description = description;
    this.warn = warn;
  }

  public String getCode() {
    return code;
  }

  public void setCode(final String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  public boolean isWarn() {
    return warn;
  }

  public void setWarn(final boolean warn) {
    this.warn = warn;
  }
}
