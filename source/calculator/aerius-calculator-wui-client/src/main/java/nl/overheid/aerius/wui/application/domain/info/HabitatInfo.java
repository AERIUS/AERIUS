/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.info;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.shared.domain.info.HabitatGoal;
import nl.overheid.aerius.shared.domain.info.HabitatSurfaceType;
import nl.overheid.aerius.wui.application.domain.result.CriticalLevels;

/**
 * Basic bean containing all habitat type info for a specific area (could be a receptor, a habitat area etc).
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class HabitatInfo {

  private int id;
  private String code;
  private String name;

  private @JsProperty CriticalLevels criticalLevels;
  private double cartographicSurface;
  private @JsProperty JsPropertyMap<Object> additionalSurfaces;

  private String qualityGoal;
  private String extentGoal;

  public final @JsOverlay int getId() {
    return id;
  }

  public final @JsOverlay String getCode() {
    return code;
  }

  public final @JsOverlay String getName() {
    return name;
  }

  public final @JsOverlay boolean isMapped() {
    return cartographicSurface > 0;
  }

  public final @JsOverlay CriticalLevels getCriticalLevels() {
    return criticalLevels;
  }

  public final @JsOverlay double getCartographicSurface() {
    return cartographicSurface; // Always in m2
  }

  public final @JsOverlay Double getAdditionalSurface(final HabitatSurfaceType habitatSurfaceType) {
    return additionalSurfaces.has(habitatSurfaceType.name()) ? Js.asDouble(additionalSurfaces.get(habitatSurfaceType.name())) : 0.0;
  }

  public final @JsOverlay JsPropertyMap<Object> getAdditionalSurfaces() {
    return additionalSurfaces;
  }

  public final @JsOverlay HabitatGoal getQualityGoal() {
    return qualityGoal == null ? HabitatGoal.NONE : HabitatGoal.valueOf(qualityGoal);
  }

  public final @JsOverlay HabitatGoal getExtentGoal() {
    return extentGoal == null ? HabitatGoal.NONE : HabitatGoal.valueOf(extentGoal);
  }

}
