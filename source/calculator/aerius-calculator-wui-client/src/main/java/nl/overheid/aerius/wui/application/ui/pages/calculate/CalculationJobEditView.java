/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.calculate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasDestroyed;
import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.aerius.wui.vue.directives.VectorDirective;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.command.calculation.CalculationCancelCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationDeleteJobCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobEditCancelCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobEditSaveCommand;
import nl.overheid.aerius.wui.application.command.situation.SituationsZoomCommand;
import nl.overheid.aerius.wui.application.command.situation.SwitchSituationCommand;
import nl.overheid.aerius.wui.application.command.source.SourceMarkersHighlightClearCommand;
import nl.overheid.aerius.wui.application.command.source.SourceMarkersHighlightCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.InputWarningComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.modify.ModifyCancelSaveComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.CalculationJobValidationContext;
import nl.overheid.aerius.wui.application.context.GenericValidationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationPreparationContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.domain.DevelopmentPressureUtil;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.application.event.CalculationJobPanelEvent;
import nl.overheid.aerius.wui.application.event.met.MetSiteListChangeEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.ui.nca.pages.calculate.options.CalculationJobAdvancedSettingsComponent;
import nl.overheid.aerius.wui.application.ui.nca.pages.calculate.options.CalculationJobDevelopmentPressureSearchComponent;
import nl.overheid.aerius.wui.application.ui.nca.pages.calculate.options.CalculationJobMeteorologicalSettingsComponent;
import nl.overheid.aerius.wui.application.ui.pages.calculate.CalculationJobValidators.CalculationJobValidations;
import nl.overheid.aerius.wui.application.ui.pages.calculate.options.CalculationJobSettingsComponent;
import nl.overheid.aerius.wui.application.ui.pages.emissionsource.ErrorWarningIconComponent;

@Component(components = {
    CalculationJobSettingsComponent.class,
    CalculationJobDevelopmentPressureSearchComponent.class,
    CalculationJobMeteorologicalSettingsComponent.class,
    CalculationJobAdvancedSettingsComponent.class,

    CollapsiblePanel.class,
    ErrorWarningIconComponent.class,
    LabeledInputComponent.class,
    ValidationBehaviour.class,
    InputErrorComponent.class,
    InputWarningComponent.class,
    ModifyCancelSaveComponent.class,
    ButtonIcon.class,
    VerticalCollapse.class,
}, directives = {
    VectorDirective.class,
    ValidateDirective.class,
}, customizeOptions = CalculationJobValidators.class)
public class CalculationJobEditView extends ErrorWarningValidator implements HasCreated, HasDestroyed {
  @Prop EventBus eventBus;
  @Prop CalculationJobContext temporaryJob;
  @Prop String temporaryJobId;

  @Data String labelV;
  @Data CalculationJobEditPanel openPanel = CalculationJobEditPanel.CALCULATION_JOB_SETTINGS;

  @Inject @Data ApplicationContext appContext;
  @Inject @Data ScenarioContext scenarioContext;
  @Inject @Data CalculationJobValidationContext validationContext;
  @Inject @Data GenericValidationContext genericValidationContext;
  @Inject @Data CalculationPreparationContext prepContext;

  @JsProperty(name = "$v") CalculationJobValidations validation;

  @Watch(value = "temporaryJob", isImmediate = true)
  protected void onJobContextChange(final CalculationJobContext neww) {
    if (neww == null) {
      return;
    }

    eventBus.fireEvent(new MetSiteListChangeEvent(appContext.getConfiguration().getMetSites()));

    labelV = String.valueOf(neww.getName());
    displayProblems(false);
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);

    validationContext.reset();
    genericValidationContext.reset();
  }

  @Override
  public void destroyed() {
    clearHighlightedMarkers();
  }

  @Computed
  protected CalculationJobValidations getV() {
    return validation;
  }

  @JsMethod
  public boolean isOpen(final CalculationJobEditPanel panel) {
    return openPanel == panel;
  }

  @JsMethod
  public void selectPanel(final CalculationJobEditPanel panel) {
    openPanel = panel;
    if (openPanel == CalculationJobEditPanel.DEVELOPMENT_PRESSURE_SEARCH) {
      highlightDevelopmentPressureSourceMarkers();
    } else {
      clearHighlightedMarkers();
    }
    eventBus.fireEvent(new CalculationJobPanelEvent(panel));
  }

  @Computed("isCalculationSourceDiscrepancy")
  protected boolean isCalculationSourceDiscrepancy() {
    final CalculationExecutionContext calculation = prepContext.getActiveCalculation(temporaryJob);
    if (calculation == null) {
      return false;
    }

    return ScenarioContext.isCalculationStale(scenarioContext, temporaryJob, calculation);
  }

  @Computed
  protected String getLabel() {
    return temporaryJob.getName();
  }

  @Computed
  protected void setLabel(final String value) {
    temporaryJob.setName(value);
    labelV = value;
  }

  @Computed
  protected CalculationJobType getCalculationJobType() {
    return temporaryJob.getCalculationOptions().getCalculationJobType();
  }

  @JsMethod
  protected SituationComposition getComposition() {
    return temporaryJob.getSituationComposition();
  }

  @JsMethod
  protected CalculationSetOptions getCalculationOptions() {
    return temporaryJob.getCalculationOptions();
  }

  @JsMethod
  protected SituationContext getProposedScenario() {
    return getComposition().getSituation(SituationType.PROPOSED);
  }

  @Computed("isShowDevelopmentPressureSearch")
  protected boolean isShowDevelopmentPressureSearch() {
    return DevelopmentPressureUtil.expectsDevelopmentPressureSearch(appContext, getCalculationJobType());
  }

  @Computed("isNCA")
  protected boolean isNCA() {
    return appContext.getTheme() == Theme.NCA;
  }

  @Computed()
  protected CalculationMethod getCalculationMethod() {
    return temporaryJob.getCalculationOptions().getCalculationMethod();
  }

  @JsMethod
  protected void cancel() {
    if (Window.confirm(M.messages().calculationJobMayCancel())) {
      eventBus.fireEvent(new CalculationJobEditCancelCommand());
    }
  }

  @JsMethod
  public void trySave() {
    // Try to save and open the first panel that has errors (if any)
    if (validationContext.hasCalculationJobSettingsError()) {
      selectPanel(CalculationJobEditPanel.CALCULATION_JOB_SETTINGS);
    } else if (isShowDevelopmentPressureSearch() && validationContext.hasDevelopmentPressureSearchError()) {
      selectPanel(CalculationJobEditPanel.DEVELOPMENT_PRESSURE_SEARCH);
    } else if (validationContext.hasMeteorologicalSettingsError()) {
      selectPanel(CalculationJobEditPanel.METEOROLOGICAL_SETTINGS);
    } else if (validationContext.hasAdvancedSettingsError()) {
      selectPanel(CalculationJobEditPanel.ADVANCED_SETTINGS);
    }
  }

  @JsMethod
  protected void save() {
    eventBus.fireEvent(new CalculationJobEditSaveCommand());
    eventBus.fireEvent(new CalculationJobPanelEvent(null));
  }

  @Computed
  public boolean displayFormProblems() {
    return genericValidationContext.isDisplayFormProblems();
  }

  @JsMethod
  public void setNotificationState(final boolean confirm) {
    genericValidationContext.setDisplayFormProblems(confirm);
  }

  @JsMethod
  protected void removeCalculationJob() {
    eventBus.fireEvent(new CalculationDeleteJobCommand(temporaryJob));
  }

  @JsMethod
  protected void cancelCalculation() {
    eventBus.fireEvent(new CalculationCancelCommand(prepContext.getActiveCalculation(temporaryJob)));
  }

  @JsMethod
  protected List<String> getSimilars() {
    final String jobHash = CalculationJobContext.calculateJobHash(temporaryJob);
    return prepContext.getJobs().stream()
        .filter(v -> !v.getId().equals(temporaryJob.getId()))
        .filter(v -> CalculationJobContext.calculateJobHash(v).equals(jobHash))
        .map(v -> v.getName())
        .collect(Collectors.toList());
  }

  @Computed("hasJobDuplicates")
  protected boolean hasJobDuplicates() {
    final String jobHash = CalculationJobContext.calculateJobHash(temporaryJob);
    return prepContext.getJobs().stream()
        .filter(v -> !v.getId().equals(temporaryJob.getId()))
        .anyMatch(v -> CalculationJobContext.calculateJobHash(v).equals(jobHash));
  }

  @JsMethod
  public void onCalculationJobSettingsWarningChange(final boolean calculationJobWarning) {
    validationContext.setCalculationJobSettingsWarning(calculationJobWarning);
  }

  @JsMethod
  public void onCalculationJobSettingsErrorChange(final boolean calculationJobError) {
    validationContext.setCalculationJobSettingsError(calculationJobError);
  }

  @JsMethod
  public void onDevelopmentPressureSearchWarningChange(final boolean developmentPressureSearchWarning) {
    validationContext.setDevelopmentPressureSearchWarning(developmentPressureSearchWarning);
  }

  @JsMethod
  public void onDevelopmentPressureSearchErrorChange(final boolean developmentPressureSearchError) {
    validationContext.setDevelopmentPressureSearchError(developmentPressureSearchError);
  }

  @JsMethod
  public void onMeteorologicalSettingsWarningChange(final boolean meteorologicalSettingsWarning) {
    validationContext.setMeteorologicalSettingsWarning(meteorologicalSettingsWarning);
  }

  @JsMethod
  public void onMeteorologicalSettingsErrorChange(final boolean meteorologicalSettingsError) {
    validationContext.setMeteorologicalSettingsError(meteorologicalSettingsError);
  }

  @JsMethod
  public void onAdvancedSettingsWarningChange(final boolean advancedSettingsWarning) {
    validationContext.setAdvancedSettingsWarning(advancedSettingsWarning);
  }

  @JsMethod
  public void onAdvancedSettingsErrorChange(final boolean advancedSettingsError) {
    validationContext.setAdvancedSettingsError(advancedSettingsError);
  }

  @Computed("isInvalid")
  protected boolean isInvalid() {
    return validation.labelV.invalid
        || hasChildErrors();
  }

  @JsMethod
  protected void closeWarning() {
    setNotificationState(false);
  }

  @JsMethod
  protected void closeError() {
    setNotificationState(false);
  }

  @Computed("hasValidationWarnings")
  public boolean hasValidationWarnings() {
    return validationContext.hasWarnings();
  }

  @Computed("hasValidationErrors")
  public boolean hasValidationErrors() {
    return validation.labelV.invalid
        || validationContext.hasErrors(isShowDevelopmentPressureSearch());
  }

  @JsMethod
  protected void developmentPressureSearchSelectionModified() {
    highlightDevelopmentPressureSourceMarkers();
  }

  private void highlightDevelopmentPressureSourceMarkers() {
    eventBus.fireEvent(new SwitchSituationCommand(getProposedScenario()));
    eventBus.fireEvent(new SourceMarkersHighlightCommand(
        temporaryJob.getCalculationOptions().getNcaCalculationOptions().getDevelopmentPressureSourceIds()));
  }

  private void clearHighlightedMarkers() {
    eventBus.fireEvent(new SourceMarkersHighlightClearCommand());
  }

  @JsMethod
  public void zoomToCalculationJob() {
    final List<SituationContext> situationsList = new ArrayList<>(temporaryJob.getSituationComposition().getSituations());

    eventBus.fireEvent(new SituationsZoomCommand(situationsList));
  }

}
