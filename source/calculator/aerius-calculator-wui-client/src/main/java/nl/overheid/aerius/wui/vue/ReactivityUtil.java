/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.vue;

import java.util.function.Consumer;
import java.util.function.Supplier;

import elemental2.core.Global;

import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.aerius.wui.dev.GWTProd;
import nl.overheid.aerius.wui.application.util.NumberUtil;

/**
 * A simple utility to ease ensuring default values for objects that are made
 * reactive through vue.js
 */
public final class ReactivityUtil {
  private ReactivityUtil() {}

  /**
   * Ensure an empty value is initialized using the given getter and setter
   *
   * <pre>
   * ReactivityUtil.ensureDefault(obj::getFoo, obj::setFoo);
   * </pre>
   *
   * @param <T> The type of the object being initialized
   * @param sup Supplier of the given object (the getter), be aware this method
   *          must be null-safe
   * @param con Consumer of the given object (the setter)
   */
  public static <T> void ensureDefault(final Supplier<T> sup, final Consumer<T> con) {
    ensureDefault(sup, con, null);
  }

  /**
   * Ensure an empty value is initialized with the given default value using the
   * given getter and setter.
   *
   * <pre>
   * ReactivityUtil.ensureDefault(obj::getFoo, obj::setFoo, 5);
   * </pre>
   *
   * @param <T> The type of the object being initialized
   * @param sup Supplier of the given object (the getter), be aware this method
   *          must be null-safe
   * @param con Consumer of the given object (the setter)
   * @param defaultt instead of `null`, initialize to the given default object
   */
  public static <T> void ensureDefault(final Supplier<T> sup, final Consumer<T> con, final T defaultt) {
    if (sup.get() == null) {
      con.accept(defaultt);
    }
  }

  /**
   * Ensure an empty double is initialized with the given default value using
   * the given getter and setter.
   *
   * Note that this checks for both null aswell as `0D` equality. For
   * primitives, `ensureJsProperty` is preferred.
   *
   * <pre>
   * ReactivityUtil.ensureDefault(obj::getFoo, obj::setFoo, 5);
   * </pre>
   *
   * @param <T> The type of the object being initialized
   * @param sup Supplier of the given object (the getter), be aware this method
   *          must be null-safe
   * @param con Consumer of the given object (the setter)
   * @param defaultt instead of `null`, initialize to the given default object
   */
  public static void ensureDefaultDouble(final Supplier<Double> sup, final Consumer<Double> con, final Double defaultt) {
    final Double dbl = sup.get();
    if (dbl == null || NumberUtil.equalEnough(dbl, 0D)) {
      con.accept(defaultt);
    }
  }

  /**
   * Ensure an empty integer is initialized with the given default value using
   * the given getter and setter.
   *
   * Note that this checks for both null aswell as `0` equality. For
   * primitives, `ensureJsProperty` is preferred.
   *
   * <pre>
   * ReactivityUtil.ensureDefault(obj::getFoo, obj::setFoo, 5);
   * </pre>
   *
   * @param <T> The type of the object being initialized
   * @param sup Supplier of the given object (the getter), be aware this method
   *          must be null-safe
   * @param con Consumer of the given object (the setter)
   * @param defaultt instead of `null`, initialize to the given default object
   */
  public static void ensureDefaultInt(final Supplier<Integer> sup, final Consumer<Integer> con, final Integer defaultt) {
    final Integer integer = sup.get();
    if (integer == null || integer == 0) {
      con.accept(defaultt);
    }
  }

  /**
   * Ensure an empty value is initialized with a supplied default value using
   * the given
   * getter and setter.
   *
   * <pre>
   * ReactivityUtil.ensureDefault(obj::getFoo, obj::setFoo, () -> Foo.create());
   * </pre>
   *
   * @param <T> The type of the object being initialized
   * @param sup Supplier of the given object (the getter), be aware this method
   *          must be null-safe
   * @param con Consumer of the given object (the setter)
   * @param defaultt instead of `null`, initialize to the object supplied
   *          through the given consumer
   */
  public static <T> void ensureDefaultSupplied(final Supplier<T> sup, final Consumer<T> con, final Supplier<T> defaultt) {
    if (sup.get() == null) {
      con.accept(defaultt.get());
    }
  }

  /**
   * Ensure a non-empty value is initialized with the given initializer.
   *
   * This is useful when complex objects need to be internally initialized
   * further.
   *
   * <pre>
   * ReactivityUtil.ensureInitialized(obj::getBar, Bar::init);
   * </pre>
   *
   * @param <T> The type of the object being initialized
   * @param sup Supplier of the given object (the getter), be aware this method
   *          must be null-safe
   * @param initializer consumer initializing the value supplied in the getter
   */
  public static <T> void ensureInitialized(final Supplier<T> sup, final Consumer<T> initializer) {
    final T val = sup.get();
    if (val != null) {
      initializer.accept(val);
    }
  }

  /**
   * Ensure a @JsProperty field exists in a given object, and if not initialize
   * it to null
   *
   * @param obj Object to check field existence for
   * @param fieldName field name to check
   * @param setter A consumer of the default value
   */
  public static <T> void ensureJsPropertyAsNull(final Object obj, final String fieldName, final Consumer<T> setter) {
    ensureJsProperty(obj, fieldName, () -> setter.accept(null));
  }

  /**
   * Ensure a @JsProperty field exists in a given object, and if not initialize
   * it to the given value
   *
   * @param obj Object to check field existence for
   * @param fieldName field name to check
   * @param setter A consumer of the default value
   * @param defaultt The default value to use
   */
  public static <T> void ensureJsProperty(final Object obj, final String fieldName, final Consumer<T> setter, final T defaultt) {
    ensureJsProperty(obj, fieldName, () -> setter.accept(defaultt));
  }

  /**
   * Ensure a @JsProperty field exists in a given object, and if not initialize
   * it to the given Supplier's value
   *
   * @param obj Object to check field existence for
   * @param fieldName field name to check
   * @param setter A consumer of the default value
   * @param defaultt A {@link Supplier} supplying the default value to use
   */
  public static <T> void ensureJsPropertySupplied(final Object obj, final String fieldName, final Consumer<T> setter, final Supplier<T> defaultt) {
    ensureJsProperty(obj, fieldName, () -> setter.accept(defaultt.get()));
  }

  /**
   * Ensure a @JsProperty field exists in a given object, and if not run the
   * given runnable. This approach is advised for primitives, which cannot be
   * null-checked like Objects can.
   *
   * @param obj Object to check field existence for
   * @param fieldName field name to check
   * @param ifNotExistsRunnable a runnable that will be executed if the field
   *          does not exist
   */
  public static void ensureJsProperty(final Object obj, final String fieldName, final Runnable ifNotExistsRunnable) {
    final JsPropertyMap<?> map = Js.uncheckedCast(obj);
    if (!map.has(fieldName)) {
      final String before = Global.JSON.stringify(obj);

      GWTProd.warn("Initializing field because it does not exist: " + fieldName, obj);
      ifNotExistsRunnable.run();

      // Here, we perform a runtime sanity check to make sure the runnable has been implemented correctly
      // We only log it to console as error but don't crash.
      if (!map.has(fieldName)) {
        GWTProd.error("Field does not exist after initializer has run: " + fieldName);
        GWTProd.error("Before: " + before);
        GWTProd.error("After: " + Global.JSON.stringify(obj));
      }
    }
  }
}
