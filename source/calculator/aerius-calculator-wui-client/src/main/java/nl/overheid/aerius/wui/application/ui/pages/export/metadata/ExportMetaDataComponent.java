/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.ui.pages.export.metadata;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.Validations;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.wui.application.components.input.CheckBoxComponent;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.MinimalInputComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ExportContext;
import nl.overheid.aerius.wui.application.domain.importer.ScenarioMetaData;
import nl.overheid.aerius.wui.application.ui.pages.export.metadata.ExportMetaDataViewValidators.ExportMetaDataValidations;

@Component(customizeOptions = {
    ExportMetaDataViewValidators.class,
}, directives = {
    ValidateDirective.class
}, components = {
    LabeledInputComponent.class,
    MinimalInputComponent.class,
    ValidationBehaviour.class,
    InputErrorComponent.class,
    CheckBoxComponent.class,
})
public class ExportMetaDataComponent extends ErrorWarningValidator implements HasCreated, HasValidators<ExportMetaDataValidations> {
  @Inject @Data ExportContext exportContext;

  @Prop boolean validationRequired;

  @Data String corporationV;
  @Data String projectNameV;
  @Data String descriptionV;
  @Data String streetAddressV;
  @Data String postcodeV;
  @Data String cityV;

  @Data boolean agreed;

  @JsProperty(name = "$v") ExportMetaDataValidations validation;

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
    final ScenarioMetaData metaData = exportContext.getScenarioMetaData();
    corporationV = metaData.getCorporation();
    projectNameV = metaData.getProjectName();
    descriptionV = metaData.getDescription();
    streetAddressV = metaData.getStreetAddress();
    postcodeV = metaData.getPostcode();
    cityV = metaData.getCity();
    agreed = exportContext.isIncludeScenarioMetaData();
  }

  @Override
  @Computed
  public ExportMetaDataValidations getV() {
    return validation;
  }

  @Computed
  public String getCorporation() {
    return corporationV;
  }

  @Computed
  public void setCorporation(final String corporation) {
    this.corporationV = corporation;
    exportContext.getScenarioMetaData().setCorporation(corporation);
  }

  @Computed
  public String getProjectName() {
    return projectNameV;
  }

  @Computed
  public void setProjectName(final String projectName) {
    this.projectNameV = projectName;
    exportContext.getScenarioMetaData().setProjectName(projectName);
  }

  @Computed
  public String getDescription() {
    return descriptionV;
  }

  @Computed
  public void setDescription(final String description) {
    descriptionV = description;
    exportContext.getScenarioMetaData().setDescription(description);
  }

  @Computed
  public String getStreetAddress() {
    return streetAddressV;
  }

  @Computed
  public void setStreetAddress(final String streetAddress) {
    streetAddressV = streetAddress;
    exportContext.getScenarioMetaData().setStreetAddress(streetAddress);
  }

  @Computed
  public String getPostcode() {
    return postcodeV;
  }

  @Computed
  public void setPostcode(final String postcode) {
    postcodeV = postcode;
    exportContext.getScenarioMetaData().setPostcode(postcode);
  }

  @Computed
  public String getCity() {
    return cityV;
  }

  @Computed
  public void setCity(final String city) {
    cityV = city;
    exportContext.getScenarioMetaData().setCity(city);
  }

  @Computed
  public boolean isEmpty() {
    return safeIsEmpty(getProjectName())
        && safeIsEmpty(getCorporation())
        && safeIsEmpty(getStreetAddress())
        && safeIsEmpty(getPostcode())
        && safeIsEmpty(getCity())
        && safeIsEmpty(getDescription());
  }

  private static boolean safeIsEmpty(final String value) {
    return value == null || value.isEmpty();
  }

  @JsMethod
  public boolean isShowError(final Validations validations) {
    return (validationRequired || agreed) && validations.error;
  }

  @Computed("agreeDataInclusion")
  public boolean isAgreeDataInclusion() {
    return exportContext.isIncludeScenarioMetaData();
  }

  @JsMethod()
  public void toggleIncludeScenarioMetaData(final boolean enabled) {
    agreed = enabled;
    exportContext.setIncludeScenarioMetaData(enabled);
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return validation.corporationV.invalid
        || validation.projectNameV.invalid
        || validation.descriptionV.invalid
        || validation.streetAddressV.invalid
        || validation.postcodeV.invalid
        || validation.cityV.invalid
        || !agreed;
  }

  @Computed("hasWarnings")
  public boolean hasWarnings() {
    return !isEmpty() && !agreed && !validationRequired;
  }
}
