/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.results.receptors;

import java.util.Arrays;
import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.command.geo.ClearAssessmentAreaCommand;
import nl.overheid.aerius.wui.application.command.geo.SelectAssessmentAreaCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.context.result.ResultSelectionContext;
import nl.overheid.aerius.wui.application.context.result.ResultSummaryContext;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsAreaSummary;
import nl.overheid.aerius.wui.application.geo.util.GeoUtil;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.application.ui.pages.results.habitat.ResultsHabitatListView;
import nl.overheid.aerius.wui.vue.BasicVueEventComponent;

@Component(components = {
    CollapsiblePanel.class,
    SimplifiedListBoxComponent.class,
    ButtonIcon.class,
    ResultsHabitatListView.class,
    VerticalCollapseGroup.class
})
public class EdgeEffectReceptorsView extends BasicVueEventComponent {

  @Prop protected EventBus eventBus;
  @Prop(required = true) ResultSummaryContext context;

  @Data @Inject ResultSelectionContext selectionContext;
  @Data @Inject ApplicationContext applicationContext;
  @Data @Inject CalculationContext calculationContext;
  @Data @Inject protected ScenarioContext scenarioContext;

  @JsMethod
  public void openPanel(final SituationResultsAreaSummary summary) {
    final int id = summary.getAssessmentArea().getId();
    eventBus.fireEvent(new SelectAssessmentAreaCommand(id));
  }

  @JsMethod
  public void closePanel() {
    eventBus.fireEvent(new ClearAssessmentAreaCommand());
  }

  @JsMethod
  public boolean isOpen(final int id) {
    return id == selectionContext.getSelectedAssessmentArea();
  }

  @JsMethod
  public void zoomToArea(final SituationResultsAreaSummary areaSummary) {
    eventBus.fireEvent(GeoUtil.createMapSetExtendCommand(areaSummary.getAssessmentArea().getBounds()));
  }

  @JsMethod
  public String formatValueFor(final double value) {
    return MessageFormatter.formatDeposition(value, applicationContext.getConfiguration().getEmissionResultValueDisplaySettings());
  }

  @Computed
  public EmissionResultValueDisplaySettings.DepositionValueDisplayType getDepositionType() {
    return applicationContext.getConfiguration().getEmissionResultValueDisplaySettings().getDisplayType();
  }

  @JsMethod
  public ResultSummaryContext getResultContext() {
    return Optional.ofNullable(getCalculationJob())
        .map(CalculationExecutionContext::getResultContext)
        .orElseThrow(() -> new RuntimeException("Could not retrieve result context."));
  }

  @Computed
  public CalculationExecutionContext getCalculationJob() {
    return calculationContext.getActiveCalculation();
  }

  @Computed("showOffSiteReductionResultsColumn")
  public boolean showOffSiteReductionResultsColumn() {
    return Arrays.stream(getCalculationJob().getCalculationInfo().getSituationCalculations())
        .anyMatch(situationCalculation -> situationCalculation.getSituationType() == SituationType.OFF_SITE_REDUCTION);
  }
}
