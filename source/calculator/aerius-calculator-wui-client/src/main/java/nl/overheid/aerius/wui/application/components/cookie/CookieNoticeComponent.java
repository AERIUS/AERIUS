/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.cookie;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.wui.application.components.manual.ExternalContentComponent;
import nl.overheid.aerius.wui.application.components.modal.PanelizedModalComponent;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(
    components = {
        PanelizedModalComponent.class,
        ExternalContentComponent.class,
    }
)
public class CookieNoticeComponent extends BasicVueComponent implements HasMounted {

  @Prop EventBus eventBus;

  @Inject @Data CookiePanelContext context;

  @JsMethod
  public void toggleCookieNotice() {
    if (context.isShowing()) {
      CookieControl.open();
    } else {
      CookieControl.hide();
    }
    context.setPanelShowing(!context.isShowing());
    context.setPanelActive(!context.isActive());
  }

  public native void exportToggleCookieNotice() /*-{
      var that = this;
      $wnd.toggleCookieNotice = $entry(function () {
          that.@nl.overheid.aerius.wui.application.components.cookie.CookieNoticeComponent::toggleCookieNotice()();
      });
  }-*/;

  @Override
  public void mounted() {
    exportToggleCookieNotice();
    // If the cookie policy link was clicked before initialization of the app was completed,
    // this makes sure that the policy window is opened on startup.
    final AerCookiePanel aerCookiePanel = new AerCookiePanel();
    if (Boolean.TRUE.equals(aerCookiePanel.showCookieNotice.call())) {
      toggleCookieNotice();
    }
  }
}
