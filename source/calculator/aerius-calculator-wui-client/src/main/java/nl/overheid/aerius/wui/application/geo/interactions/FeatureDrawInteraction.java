/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.interactions;

import java.util.Optional;
import java.util.function.Consumer;

import ol.geom.Geometry;
import ol.geom.GeometryType;
import ol.interaction.Draw;
import ol.interaction.DrawEventType;
import ol.interaction.DrawOptions;
import ol.interaction.Interaction;

import jsinterop.base.Js;

import nl.aerius.geo.domain.IsInteraction;

/**
 * OL3 interaction to draw geometry on the map
 */
public class FeatureDrawInteraction implements IsInteraction<Interaction> {

  final Interaction interaction;

  /**
   * Starts a OL3 interaction to draw geometry on the map. The interaction removes
   * itself after the geometry is complete and returns the drawn geometry
   *
   * @param geometryType   type of the geometry to draw
   * @param finishCallback callback for drawn geometry
   */
  public FeatureDrawInteraction(final GeometryType geometryType, final Consumer<Geometry> finishCallback) {
    this(geometryType, geom -> {}, finishCallback);
  }

  /**
   * Starts a OL3 interaction to draw geometry on the map. The interaction removes
   * itself after the geometry is complete and returns the drawn geometry
   *
   * @param geometryType     type of the geometry to draw
   * @param startCallback    callback for drawn geometry
   * @param finishedCallback callback when finished drawing geometry (i.e. user
   *                         double clicks at the end of a line).
   */
  public FeatureDrawInteraction(final GeometryType geometryType, final Consumer<Geometry> startCallback,
      final Consumer<Geometry> finishedCallback) {
    final DrawOptions drawOptions = new DrawOptions();
    drawOptions.setType(geometryType.name());
    drawOptions.setStyle(FeatureDrawStyle.DRAW_STYLE);

    interaction = new Draw(drawOptions);
    interaction.on(DrawEventType.drawstart.name(), evt -> {
      final Draw.Event drawEvent = Js.cast(evt);
      final Geometry geom = drawEvent.getFeature().getGeometry();
      Optional.ofNullable(startCallback)
          .ifPresent(v -> v.accept(geom));
    });
    interaction.on(DrawEventType.drawend.name(), evt -> {
      final Draw.Event drawEvent = Js.cast(evt);
      final Geometry geom = drawEvent.getFeature().getGeometry();
      Optional.ofNullable(finishedCallback)
          .ifPresent(v -> v.accept(geom));
    });
  }

  @Override
  public Interaction asInteraction() {
    return interaction;
  }

}
