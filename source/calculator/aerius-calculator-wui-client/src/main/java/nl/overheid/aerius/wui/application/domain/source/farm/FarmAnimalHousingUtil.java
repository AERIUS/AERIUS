/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.farm;

import java.util.Set;

import elemental2.core.JsArray;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalHousingCategories;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalHousingCategory;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;

/**
 * Util class for calculating emissions of Farm Lodging objects.
 */
public final class FarmAnimalHousingUtil {

  private FarmAnimalHousingUtil() {
    // Util class
  }

  /**
   * Gets the emission of a {@link FarmAnimalHousing} object and returns the calculated emission
   *
   * @param farmAnimalHousing object to get emission for.
   */
  public static double getEmission(final FarmAnimalHousingCategories categories, final FarmAnimalHousing farmAnimalHousing) {
    if (farmAnimalHousing.getAnimalHousingType() == FarmAnimalHousingType.STANDARD) {
      return FarmAnimalHousingStandardUtil.getEmissions(categories, (StandardFarmAnimalHousing) farmAnimalHousing, Substance.NH3);
    } else {
      final double emission = getEmission((CustomFarmAnimalHousing) farmAnimalHousing);

      farmAnimalHousing.setEmission(Substance.NH3, emission);
      return emission;
    }
  }

  public static boolean hasAnimalsPerDay(final FarmAnimalHousingCategory category) {
    return category != null && category.getFarmEmissionFactorType() == FarmEmissionFactorType.PER_ANIMAL_PER_DAY;

  }

  /**
   * Calculates the emission of a {@link CustomFarmAnimalHousing} object.
   *
   * @param customFarmAnimalHousing object to calculate emission for
   * @return emission
   */
  private static double getEmission(final CustomFarmAnimalHousing customFarmAnimalHousing) {
    final int numberOfDaysFactor = customFarmAnimalHousing.getFarmEmissionFactorType() == FarmEmissionFactorType.PER_ANIMAL_PER_DAY
        ? customFarmAnimalHousing.getNumberOfDays()
        : 1;
    return customFarmAnimalHousing.getEmissionFactor(Substance.NH3) * customFarmAnimalHousing.getNumberOfAnimals() * numberOfDaysFactor;
  }

  public static boolean hasStacking(final StandardFarmAnimalHousing standardFarmAnimalHousing) {
    return standardFarmAnimalHousing.getAdditionalSystems().length > 0;
  }

  /**
   * Check if standardFarmAnimalHousing has additional house systems that are not compatible.
   * Returns true if any additional system is not compatible.
   *
   * @param standardFarmAnimalHousing
   * @param farmAnimalHousingCategories
   * @return true if has none compatible additional housing systems
   */
  public static boolean hasIncompatibleCombinations(final StandardFarmAnimalHousing standardFarmAnimalHousing,
      final FarmAnimalHousingCategories categories) {
    final JsArray<AdditionalHousingSystem> additionalSystems = standardFarmAnimalHousing.getAdditionalSystems();

    for (int i = 0; i < additionalSystems.length; i++) {
      if (isIncompatibleCombination(additionalSystems.getAt(i), standardFarmAnimalHousing.getAnimalHousingCode(), categories)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Check if the additional house system is compatible with given farmAnimalHousingCode.
   * Returns true if not compatible.
   *
   * @param system
   * @param farmAnimalHousingCode
   * @param categories
   * @return true if is none compatible additional housing system.
   */
  public static boolean isIncompatibleCombination(final AdditionalHousingSystem system, final String farmAnimalHousingCode,
      final FarmAnimalHousingCategories categories) {
    if (farmAnimalHousingCode == null || system == null || AdditionalSystemType.CUSTOM == system.getAdditionalSystemType()) {
      return false;
    }
    final Set<String> allowedAdditionals = categories.getHousingAllowedAdditionalSystems().get(farmAnimalHousingCode);
    return allowedAdditionals == null || !allowedAdditionals.contains(((StandardAdditionalHousingSystem) system).getAdditionalSystemCode());
  }
}
