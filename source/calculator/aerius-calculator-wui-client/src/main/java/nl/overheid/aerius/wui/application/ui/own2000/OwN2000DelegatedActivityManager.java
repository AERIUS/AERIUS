/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.own2000;

import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.ui.pages.DelegatedThemeActivityManager;

/**
 * Delegation manager for OwN2000 Theme activity places.
 */
public class OwN2000DelegatedActivityManager extends DelegatedThemeActivityManager {

  @Inject
  public OwN2000DelegatedActivityManager(final OwN2000ThemeActivityHelperFactory themeActivityHelperFactory, final ApplicationContext appContext,
      final ScenarioContext context) {
    super(themeActivityHelperFactory, appContext, context, Theme.OWN2000);
  }
}
