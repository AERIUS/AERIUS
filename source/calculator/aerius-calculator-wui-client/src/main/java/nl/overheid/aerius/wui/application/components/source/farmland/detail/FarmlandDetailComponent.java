/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.farmland.detail;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.FarmlandCategory;
import nl.overheid.aerius.wui.application.components.divider.DividerComponent;
import nl.overheid.aerius.wui.application.components.source.farmland.custom.FarmlandCustomDetailComponent;
import nl.overheid.aerius.wui.application.components.source.farmland.standard.FarmlandStandardDetailComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.FarmlandESFeature;
import nl.overheid.aerius.wui.application.domain.source.farmland.FarmlandUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "aer-farmland-detail", components = {
    FarmlandStandardDetailComponent.class,
    FarmlandCustomDetailComponent.class,
    DividerComponent.class
})
public class FarmlandDetailComponent extends BasicVueComponent {
  @Data @Inject ApplicationContext applicationContext;

  @Prop @JsProperty protected FarmlandESFeature source;
  @Prop @JsProperty protected List<Substance> substances;
  @Prop String totalEmissionTitle;

  @Computed("substances")
  public List<Substance> getSubstances() {
    return substances;
  }

  @JsMethod
  public String description(final String category) {
    final FarmlandCategory selectedFarmlandCategory = FarmlandUtil.findFarmland(category,
        applicationContext.getConfiguration().getSectorCategories().getFarmlandCategories());
    return selectedFarmlandCategory != null ? selectedFarmlandCategory.getDescription() : "";
  }
}
