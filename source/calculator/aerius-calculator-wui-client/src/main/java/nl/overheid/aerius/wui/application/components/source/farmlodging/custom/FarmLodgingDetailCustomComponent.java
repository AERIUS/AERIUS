/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.farmlodging.custom;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsProperty;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;
import nl.overheid.aerius.wui.application.domain.source.farm.CustomFarmLodging;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "aer-farmlodging-detail-custom")
public class FarmLodgingDetailCustomComponent extends BasicVueComponent {
  @Prop @JsProperty protected CustomFarmLodging source;
  @Prop protected boolean hasFarmReductiveLodgingSystemCategories;
  @Data int index;

  @Computed
  String emission() {
    return MessageFormatter.formatEmissionWithUnitSmart(source.getEmission(Substance.NH3));
  }

  @Computed
  String farmLodgingEmissionFactor() {
    final String emissionFactorValue = i18n.decimalNumberCapped(source.getEmissionFactor(Substance.NH3), 2);
    if (hasFarmReductiveLodgingSystemCategories) {
      return emissionFactorValue;
    } else {
      return emissionFactorValue + " " + i18n.esFarmEmissionFactorType(source.getFarmEmissionFactorType());
    }
  }

  @Computed
  String numberOfDays() {
    if (source.getFarmEmissionFactorType() == FarmEmissionFactorType.PER_ANIMAL_PER_DAY) {
      return Integer.toString(source.getNumberOfDays());
    } else {
      return "-";
    }
  }
}
