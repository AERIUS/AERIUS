/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.scenario;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.wui.application.command.CalculationPointDetailEditorErrorChangeCommand;
import nl.overheid.aerius.wui.application.context.CalculationPointValidationContext;

@Singleton
public class CalculationPointValidationDaemon extends BasicEventComponent {
  private static final CalculationPointValidationDaemonEventBinder EVENT_BINDER = GWT.create(CalculationPointValidationDaemonEventBinder.class);

  interface CalculationPointValidationDaemonEventBinder extends EventBinder<CalculationPointValidationDaemon> {}

  @Inject private CalculationPointValidationContext context;

  @EventHandler
  public void onCalculationPointDetailEditorErrorChangeCommand(final CalculationPointDetailEditorErrorChangeCommand c) {
    context.setDetailError(c.getValue());
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
