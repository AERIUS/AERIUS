/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.export;

import java.util.List;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.wui.application.command.exporter.ExportCancelCommand;
import nl.overheid.aerius.wui.application.domain.export.HistoryRecord;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    HistoryItemComponent.class,
})
public class ExportHistoryComponent extends BasicVueComponent {
  @Prop EventBus eventBus;
  @Prop @JsProperty List<HistoryRecord> history;

  @JsMethod
  public void removeItem(final HistoryRecord item) {
    eventBus.fireEvent(new ExportCancelCommand(item));
  }
}
