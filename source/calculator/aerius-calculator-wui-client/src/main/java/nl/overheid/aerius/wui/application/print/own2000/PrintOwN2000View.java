/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.print.own2000;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.wui.application.context.PrintContext;
import nl.overheid.aerius.wui.application.print.common.PrintView;
import nl.overheid.aerius.wui.application.print.own2000.misc.ApplicationVersionComponent;
import nl.overheid.aerius.wui.application.print.own2000.products.PrintOwN2000CalculatorView;
import nl.overheid.aerius.wui.application.print.own2000.products.PrintOwN2000EdgeEffectAppendixView;
import nl.overheid.aerius.wui.application.print.own2000.products.PrintOwN2000ExtraAssessmentAppendixView;
import nl.overheid.aerius.wui.application.print.own2000.products.PrintOwN2000LbvPolicyView;
import nl.overheid.aerius.wui.application.print.own2000.products.PrintOwN2000SingleScenarioView;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component(components = {
    PrintView.class,
    PrintOwN2000CalculatorView.class,
    PrintOwN2000LbvPolicyView.class,
    PrintOwN2000EdgeEffectAppendixView.class,
    PrintOwN2000ExtraAssessmentAppendixView.class,
    PrintOwN2000SingleScenarioView.class,
    ApplicationVersionComponent.class,
})
public class PrintOwN2000View extends BasicVueView {
  @Prop EventBus eventBus;

  @Inject @Data PrintContext context;

}
