/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.calculationpoint;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import ol.geom.Geometry;

import elemental2.dom.FocusEvent;

import jsinterop.annotations.JsMethod;

import nl.aerius.geo.wui.util.OL3GeometryUtil;
import nl.aerius.wui.util.SchedulerUtil;
import nl.aerius.wui.vue.directives.VectorDirective;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointType;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointEditCancelCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointSaveCommand;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.common.FeatureHeading;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.modify.ModifyCancelSaveComponent;
import nl.overheid.aerius.wui.application.components.warning.PrivacyWarning;
import nl.overheid.aerius.wui.application.context.CalculationPointEditorContext;
import nl.overheid.aerius.wui.application.context.CalculationPointValidationContext;
import nl.overheid.aerius.wui.application.context.GenericValidationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;
import nl.overheid.aerius.wui.application.ui.pages.emissionsource.ErrorWarningIconComponent;
import nl.overheid.aerius.wui.application.util.AccessibilityUtil;
import nl.overheid.aerius.wui.vue.BasicVueEventComponent;

@Component(components = {
    PrivacyWarning.class,
    LabeledInputComponent.class,
    ModifyCancelSaveComponent.class,
    CollapsiblePanel.class,
    CalculationPointLocationEditorComponent.class,
    CalculationPointCharacteristicsEditorComponent.class,
    CalculationPointHabitatsEditorComponent.class,
    ErrorWarningIconComponent.class,
    FeatureHeading.class,
    InputErrorComponent.class,
}, directives = {
    VectorDirective.class,
})
public class CalculationPointEditorComponent extends BasicVueEventComponent {

  enum CalculationPointPanel {
    LOCATION, ROAD_FRACTION_NO2, ASSIGN_HABITAT
  }

  @Prop EventBus eventBus;
  @Prop CalculationPointFeature calculationPoint;

  @Data CalculationPointPanel openPanel = CalculationPointPanel.LOCATION;
  @Data boolean nameLabelFocused = false;

  @Inject @Data CalculationPointEditorContext editorContext;
  @Inject @Data GenericValidationContext genericValidationContext;
  @Inject @Data ScenarioContext scenarioContext;
  @Inject @Data CalculationPointValidationContext validationContext;

  @Computed
  public String getCalculationPointTitle() {
    return scenarioContext.getCalculationPoints().contains(editorContext.getCalculationPoint())
        ? i18n.calculationPointsEditTitle()
        : i18n.calculationPointsNewTitle();
  }

  @Computed
  public String getLabel() {
    return calculationPoint.getLabel();
  }

  @Computed
  public void setLabel(final String label) {
    calculationPoint.setLabel(label);
  }

  @JsMethod
  public void onLabelNameFocus() {
    nameLabelFocused = true;
  }

  @JsMethod
  public void onLabelNameFocusOut(final FocusEvent event) {
    AccessibilityUtil.doOnFocusLeave(event, () -> SchedulerUtil.delay(() -> nameLabelFocused = false, AccessibilityUtil.FOCUS_OUT_DELAY));
  }

  @JsMethod
  public void cancel() {
    eventBus.fireEvent(new CalculationPointEditCancelCommand());
  }

  @JsMethod
  public void onLocationErrorsChange(final boolean locationError) {
    validationContext.setLocationError(locationError);
  }

  @JsMethod
  public void onPointCharacteristicsErrorsChange(final boolean pointCharacteristicsError) {
    validationContext.setCharacteristicsError(pointCharacteristicsError);
  }

  @Computed
  public boolean getNameError() {
    final String label = getLabel();
    return label == null || "".equals(label);
  }

  @JsMethod
  public void trySave() {
    // Try to save and open the first panel that has errors (if any)
    if (validationContext.getCharacteristicsError()) {
      openPanel = CalculationPointPanel.ROAD_FRACTION_NO2;
    } else if (validationContext.getLocationError()) {
      openPanel = CalculationPointPanel.LOCATION;
    } else if (validationContext.getDetailError()) {
      openPanel = CalculationPointPanel.ASSIGN_HABITAT;
    }
  }

  @JsMethod
  public void save() {
    eventBus.fireEvent(new CalculationPointSaveCommand(calculationPoint));
  }

  @JsMethod
  public void closeError() {
    setNotificationState(false);
  }

  @Computed
  public boolean displayFormProblems() {
    return genericValidationContext.isDisplayFormProblems();
  }

  @JsMethod
  public void setNotificationState(final boolean confirm) {
    genericValidationContext.setDisplayFormProblems(confirm);
  }

  @JsMethod
  public void updateLocationNotUnique() {
    final Geometry wktGeometry = calculationPoint.getGeometry();
    final String wktString = wktGeometry == null ? null : OL3GeometryUtil.toWktString(wktGeometry);
    final boolean locationNotUnique = wktGeometry != null
        && scenarioContext.getCalculationPoints().stream()
            .filter(v -> v != null && !v.getId().equals(calculationPoint.getId()))
            .map(v -> OL3GeometryUtil.toWktString(v.getGeometry()))
            .anyMatch(v -> v.equals(wktString));

    validationContext.setLocationNotUnique(locationNotUnique);
  }

  @JsMethod
  public String getLocationNotUniqueErrorMessage() {
    return i18n.esLocationAssessmentPointNotUnique();
  }

  @JsMethod
  protected void openPanel(final CalculationPointPanel panel) {
    openPanel = panel;
  }

  @JsMethod
  protected void closePanel() {
    openPanel = null;
  }

  @JsMethod
  public boolean isOpen(final CalculationPointPanel esp) {
    return esp == openPanel;
  }

  @Computed("isNcaPoint")
  public boolean isNcaPoint() {
    return calculationPoint.getCalculationPointType() == CalculationPointType.NCA_CUSTOM_CALCULATION_POINT;
  }

}
