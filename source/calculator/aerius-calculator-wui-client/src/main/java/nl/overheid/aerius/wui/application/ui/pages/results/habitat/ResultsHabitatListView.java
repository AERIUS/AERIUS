/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.results.habitat;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings.DepositionValueDisplayType;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.result.ResultSelectionContext;
import nl.overheid.aerius.wui.application.context.result.ResultSummaryContext;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsHabitatSummary;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.application.ui.pages.results.GeneralizedEmissionResultType;
import nl.overheid.aerius.wui.application.ui.pages.results.stats.ResultsStatisticsFormatter;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component
public class ResultsHabitatListView extends BasicVueView {
  @Prop(required = true) ResultSummaryContext summaryContext;
  @Prop @JsProperty SituationResultsHabitatSummary[] data;
  @Prop ResultStatisticType statisticsType = ResultStatisticType.MAX_CONTRIBUTION;
  @Prop(required = true) ResultSummaryContext context;

  @Inject @Data ApplicationContext applicationContext;
  @Inject @Data ResultSelectionContext selectionContext;
  @Inject ResultsStatisticsFormatter resultsStatisticsFormatter;

  @JsMethod
  public void rowSelectToggle(final SituationResultsHabitatSummary row) {
    vue().$emit("toggle-habitat-selection", String.valueOf(row.getHabitatType().getId()));
  }

  @JsMethod
  public void rowHighlightActive(final SituationResultsHabitatSummary row) {
    vue().$emit("habitat-highlight-active", String.valueOf(row.getHabitatType().getId()));
  }

  @JsMethod
  public void rowHighlightInactive(final SituationResultsHabitatSummary row) {
    vue().$emit("habitat-highlight-inactive", String.valueOf(row.getHabitatType().getId()));
  }

  @JsMethod
  public boolean isSelected(final SituationResultsHabitatSummary summary) {
    return selectionContext.isSelectedHabitatTypeCode(String.valueOf(summary.getHabitatType().getId()));
  }

  @Computed
  public ResultStatisticType getSurfaceStatisticType() {
    return context.getSurfaceStatisticType();
  }

  @JsMethod
  public String formatSurfaceOrPointsValue(final SituationResultsHabitatSummary row) {
    final ResultStatisticType surfaceStatisticType = getSurfaceStatisticType();
    return resultsStatisticsFormatter.formatStatisticType(row.getStatistics(), surfaceStatisticType);
  }

  @Computed
  public GeneralizedEmissionResultType getEmissionResultType() {
    return GeneralizedEmissionResultType.fromEmissionResultType(context.getEmissionResultKey().getEmissionResultType());
  }

  @Computed
  public DepositionValueDisplayType getUnitDisplayType() {
    return applicationContext.getConfiguration().getEmissionResultValueDisplaySettings().getDisplayType();
  }

  @Computed
  public Theme getTheme() {
    return applicationContext.getTheme();
  }

  @JsMethod
  public String formatMinimumCriticalLevel(final SituationResultsHabitatSummary row) {
    final Double statistic = row.getMinimumCriticalLevel();
    return MessageFormatter.formatCL(statistic, context.getEmissionResultKey(),
        applicationContext.getConfiguration().getEmissionResultValueDisplaySettings());
  }

  @JsMethod
  public String formatDepositionValue(final SituationResultsHabitatSummary row) {
    return resultsStatisticsFormatter.formatStatisticType(row.getStatistics(), statisticsType);
  }

}
