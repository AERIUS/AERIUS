/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.nca.pages.timevarying;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.gwt.i18n.client.NumberFormat;

import elemental2.dom.Event;
import elemental2.dom.HTMLInputElement;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.domain.source.TimeVaryingProfile;
import nl.overheid.aerius.wui.application.domain.source.util.TimeVaryingProfileUtil;

@Component(components = {
    ValidationBehaviour.class,
})
public class ThreeDayTableInputComponent extends ErrorWarningValidator {
  private static final NumberFormat HOUR_FORMAT = NumberFormat.getFormat("00");

  @Prop(required = true) TimeVaryingProfile profile;

  @Computed
  public List<Integer> days() {
    return IntStream.range(0, 3)
        .boxed()
        .collect(Collectors.toList());
  }

  @Computed
  public List<Integer> hours() {
    return IntStream.range(0, 24)
        .boxed()
        .collect(Collectors.toList());
  }

  @JsMethod
  public String formatLegendHour(final Object hourStr) {
    final int hour = Integer.parseInt(String.valueOf(hourStr));
    final String start = HOUR_FORMAT.format(hour);
    final String end = HOUR_FORMAT.format(hour + 1D);
    return start + "-" + end;
  }

  @JsMethod
  public String getHourInput(final int day, final int hour) {
    final int dayInt = Integer.parseInt(String.valueOf(day));
    final int hourInt = Integer.parseInt(String.valueOf(hour));

    return TimeVaryingProfileUtil.THREE_DAY_PROFILE_UTIL.getValue(profile.getValues(), dayInt, hourInt);
  }

  @JsMethod
  public String getHourTotal(final int day) {
    final int dayInt = Integer.parseInt(String.valueOf(day));
    return TimeVaryingProfileUtil.THREE_DAY_PROFILE_UTIL.getTotal(profile.getValues(), dayInt);
  }

  @JsMethod
  public String getDebugId(final int day, final int hour) {
    return "diurnalTimeVaryingTableInput-" + day + "-" + hour;
  }

  @JsMethod
  public void setHourInput(final int day, final int hour, final Event ev) {
    final int dayInt = Integer.parseInt(String.valueOf(day));
    final int hourInt = Integer.parseInt(String.valueOf(hour));

    double value;
    try {
      final HTMLInputElement input = (HTMLInputElement) ev.target;
      value = Double.parseDouble(input.value);
    } catch (final NumberFormatException e) {
      value = 0;
    }

    TimeVaryingProfileUtil.THREE_DAY_PROFILE_UTIL.setInput(profile.getValues(), dayInt, hourInt, value);
  }
}
