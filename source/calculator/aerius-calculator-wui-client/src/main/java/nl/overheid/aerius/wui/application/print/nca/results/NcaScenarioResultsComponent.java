/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.print.nca.results;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.application.print.common.PrintSection;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    PrintSection.class,
    NcaPrintAreaResultsComponent.class
})
public class NcaScenarioResultsComponent extends BasicVueComponent {
  @Computed
  public List<EmissionResultKey> getResultKeys() {
    return Arrays.asList(EmissionResultKey.NH3_CONCENTRATION, EmissionResultKey.NOX_CONCENTRATION);
  }

  @JsMethod
  public List<String> getLongDummyContent(final int paragraphs) {
    final String baseParagraph = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n" +
        "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. \n" +
        "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \n" +
        "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \n" +
        "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
    final List<String> lst = new ArrayList<>();
    for (int i = 0; i < paragraphs; i++) {
      lst.add(baseParagraph);
    }
    return lst;
  }

  @JsMethod
  public List<String> getShortDummyContent(final int paragraphs) {
    final String baseParagraph = "Lorem ipsum";
    final List<String> lst = new ArrayList<>();
    for (int i = 0; i < paragraphs; i++) {
      lst.add(baseParagraph);
    }
    return lst;
  }
}
