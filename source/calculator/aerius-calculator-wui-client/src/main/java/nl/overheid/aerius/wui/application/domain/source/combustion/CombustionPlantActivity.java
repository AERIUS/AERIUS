/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.combustion;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.wui.application.domain.source.AbstractSubSource;
import nl.overheid.aerius.wui.vue.ReactivityUtil;

@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class CombustionPlantActivity extends AbstractSubSource {

  private String description;
  private String plantType;
  private String fuelType;

  private String startDate;
  private int operatingHours;
  private double thermalInput;
  private int applicableELV;

  public static @JsOverlay CombustionPlantActivity create() {
    final CombustionPlantActivity props = Js.uncheckedCast(JsPropertyMap.of());
    init(props);
    return props;
  }

  public static @JsOverlay void init(final CombustionPlantActivity props) {
    AbstractSubSource.initAbstractSubSource(props);
    if (!props.hasEmission(Substance.NOX)) {
      props.setEmission(Substance.NOX, 0D);
    }
    if (!props.hasEmission(Substance.NH3)) {
      props.setEmission(Substance.NH3, 0D);
    }

    ReactivityUtil.ensureJsPropertyAsNull(props, "description", props::setDescription);
    ReactivityUtil.ensureJsPropertyAsNull(props, "plantType", props::setPlantType);
    ReactivityUtil.ensureJsPropertyAsNull(props, "fuelType", props::setFuelType);

    ReactivityUtil.ensureJsPropertyAsNull(props, "startDate", props::setStartDate);
    ReactivityUtil.ensureJsProperty(props, "operatingHours", props::setOperatingHours, 0);
    ReactivityUtil.ensureJsProperty(props, "thermalInput", props::setThermalInput, 0D);
    ReactivityUtil.ensureJsProperty(props, "applicableELV", props::setApplicableELV, 0);
  }

  public final @JsOverlay String getDescription() {
    return description;
  }

  public final @JsOverlay void setDescription(final String description) {
    this.description = description;
  }

  public final @JsOverlay String getPlantType() {
    return plantType;
  }

  public final @JsOverlay void setPlantType(final String plantType) {
    this.plantType = plantType;
  }

  public final @JsOverlay String getFuelType() {
    return fuelType;
  }

  public final @JsOverlay void setFuelType(final String fuelType) {
    this.fuelType = fuelType;
  }

  public final @JsOverlay String getStartDate() {
    return startDate;
  }

  public final @JsOverlay void setStartDate(final String startDate) {
    this.startDate = startDate;
  }

  public final @JsOverlay int getOperatingHours() {
    return operatingHours;
  }

  public final @JsOverlay void setOperatingHours(final int operatingHours) {
    this.operatingHours = operatingHours;
  }

  public final @JsOverlay double getThermalInput() {
    return thermalInput;
  }

  public final @JsOverlay void setThermalInput(final double thermalInput) {
    this.thermalInput = thermalInput;
  }

  public final @JsOverlay int getApplicableELV() {
    return applicableELV;
  }

  public final @JsOverlay void setApplicableELV(final int applicableELV) {
    this.applicableELV = applicableELV;
  }

}
