/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.geo;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.axellience.vuegwt.core.client.Vue;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.geo.command.LayerHiddenCommand;
import nl.aerius.geo.command.LayerVisibleCommand;
import nl.aerius.geo.domain.IsLayer;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.event.PlaceChangeEvent;
import nl.aerius.wui.place.Place;
import nl.aerius.wui.place.PlaceController;
import nl.overheid.aerius.wui.application.command.geo.ChangeLabelDisplayModeCommand;
import nl.overheid.aerius.wui.application.context.MapConfigurationContext;
import nl.overheid.aerius.wui.application.daemon.calculation.RefreshResultsViewCommand;
import nl.overheid.aerius.wui.application.daemon.calculation.ResultLayerDaemon;
import nl.overheid.aerius.wui.application.daemon.misc.PersistablePreferencesContext;
import nl.overheid.aerius.wui.application.domain.geo.LabelDisplayMode;
import nl.overheid.aerius.wui.application.event.CalculationJobPanelEvent;
import nl.overheid.aerius.wui.application.event.LayersLoadedEvent;
import nl.overheid.aerius.wui.application.place.BuildingPlace;
import nl.overheid.aerius.wui.application.place.CalculatePlace;
import nl.overheid.aerius.wui.application.place.CalculationPointsPlace;
import nl.overheid.aerius.wui.application.place.EmissionSourcePlace;
import nl.overheid.aerius.wui.application.place.PrintPlace;
import nl.overheid.aerius.wui.application.place.ResultsPlace;
import nl.overheid.aerius.wui.application.place.ScenarioInputListPlace;
import nl.overheid.aerius.wui.application.place.TimeVaryingProfilePlace;
import nl.overheid.aerius.wui.application.ui.pages.calculate.CalculationJobEditPanel;

/**
 * This daemon is responsible for managing visibility of layers in conjunction with application place.
 *
 * See {@link ResultLayerDaemon} for a similar setup within the result page.
 */
@Singleton
public class ApplicationLayerDaemon extends BasicEventComponent {
  private static final ApplicationLayerDaemonEventBinder EVENT_BINDER = GWT.create(ApplicationLayerDaemonEventBinder.class);

  interface ApplicationLayerDaemonEventBinder extends EventBinder<ApplicationLayerDaemon> {
  }

  @Inject private PersistablePreferencesContext preferencesContext;
  @Inject private ScenarioLayerContext context;
  @Inject private MapConfigurationContext mapConfigContext;

  @Inject private PlaceController placeController;

  @EventHandler
  public void onPlaceChange(final PlaceChangeEvent e) {
    if (!preferencesContext.isLayerAutoSwitch()) {
      return;
    }

    final Place place = e.getValue();
    final Place previousPlace = placeController.getPreviousPlace();
    handleLayerVisibility(place, previousPlace);
  }

  @EventHandler
  public void onLayersLoadedEvent(final LayersLoadedEvent e) {
    final Place place = placeController.getPlace();
    final Place previousPlace = placeController.getPreviousPlace();
    handleLayerVisibility(place, previousPlace);
  }

  @EventHandler
  public void onCalculationJobPanelEvent(final CalculationJobPanelEvent e) {
    showCalculateLayers(e.getValue());
  }

  private void handleLayerVisibility(final Place place, final Place previousPlace) {
    if (shouldSwitch(place, previousPlace, ApplicationLayerDaemon::isInstanceOfInputPlace)) {
      showInputLayers();
    } else if (shouldSwitch(place, previousPlace, ApplicationLayerDaemon::isInstanceOfCalculationPointPlace)) {
      showCalculationPointLayers();
    } else if (shouldSwitch(place, previousPlace, ApplicationLayerDaemon::isInstanceOfCalculatePlace)) {
      showCalculateLayers(null);
    } else if (shouldSwitch(place, previousPlace, ApplicationLayerDaemon::isInstanceOfResultsPlace)) {
      showResultsLayers();
    } else if (shouldSwitch(place, previousPlace, ApplicationLayerDaemon::isInstanceOfPrintPlace)) {
      showPrintLayers();
    }

    // Change label visibility back to the selected mode
    switchLabelDisplayMode(mapConfigContext.getLabelDisplayMode());
  }

  private void showInputLayers() {
    switchToVisible(context.getInputLayers(), context.getSourceLayerGroup(), context.getRoadInputLayer());
    switchToVisible(context.getResultLayers());
    switchToVisible(context.getCalculateLayers());
    hideArchiveProjects();
  }

  private void showCalculationPointLayers() {
    switchToVisible(context.getInputLayers(), context.getCalculationPointLayerGroup());
    switchToVisible(context.getResultLayers());
    switchToVisible(context.getCalculateLayers());
    hideArchiveProjects();
  }

  private void showCalculateLayers(final CalculationJobEditPanel panel) {
    if (panel == CalculationJobEditPanel.DEVELOPMENT_PRESSURE_SEARCH) {
      switchToVisible(context.getInputLayers(), context.getSourceLayerGroup(), context.getRoadInputLayer());
    } else {
      switchToVisible(context.getInputLayers(), context.getJobInputLayerGroup());
    }

    switchToVisible(context.getResultLayers());

    if (panel == CalculationJobEditPanel.METEOROLOGICAL_SETTINGS) {
      switchToVisible(context.getCalculateLayers(), context.getMetSiteLayer());
    } else {
      switchToVisible(context.getCalculateLayers());
    }

    hideArchiveProjects();
  }

  private void showResultsLayers() {
    switchToVisible(context.getInputLayers(), context.getJobInputLayerGroup());
    switchToVisible(context.getCalculateLayers());
    showArchiveProjects();

    eventBus.fireEvent(new LayerVisibleCommand(context.getResultLayerGroup()));

    // Signal that whatever is happening in the result page should be refreshed
    eventBus.fireEvent(new RefreshResultsViewCommand());
  }

  private void showPrintLayers() {
    switchToVisible(context.getInputLayers(), context.getJobInputLayerGroup(), context.getCalculationPointLayerGroup());
    switchToVisible(context.getCalculateLayers());
  }

  @EventHandler
  public void onChangeLabelDisplayModeCommand(final ChangeLabelDisplayModeCommand c) {
    switchLabelDisplayMode(c.getValue());
  }

  private void switchLabelDisplayMode(final LabelDisplayMode labelDisplayMode) {
    final boolean hidden = labelDisplayMode == LabelDisplayMode.HIDDEN;
    Optional.ofNullable(context.getSourceLayerGroup())
        .map(v -> v.getMarkerLayers())
        .ifPresent(layers -> layers.forEach(l -> toggle(hidden, l)));
    Optional.ofNullable(context.getJobInputLayerGroup())
        .map(v -> v.getMarkerLayers())
        .ifPresent(layers -> layers.forEach(l -> toggle(hidden, l)));
    Optional.ofNullable(context.getCalculationPointLayerGroup())
        .map(v -> v.getMarkerLayers())
        .ifPresent(layers -> layers.forEach(l -> toggle(hidden, l)));
  }

  private void toggle(final boolean hide, final IsLayer<?> layer) {
    eventBus.fireEvent(hide ? new LayerHiddenCommand(layer) : new LayerVisibleCommand(layer));
  }

  private static boolean shouldSwitch(final Place place, final Place previousPlace, final Predicate<Place> func) {
    return func.test(place) && !func.test(previousPlace);
  }

  private static boolean isInstanceOfPrintPlace(final Place place) {
    return place instanceof PrintPlace;
  }

  private static boolean isInstanceOfResultsPlace(final Place place) {
    return place instanceof ResultsPlace;
  }

  private static boolean isInstanceOfCalculatePlace(final Place place) {
    return place instanceof CalculatePlace;
  }

  private static boolean isInstanceOfCalculationPointPlace(final Place place) {
    return place instanceof CalculationPointsPlace;
  }

  private static boolean isInstanceOfInputPlace(final Place place) {
    return place instanceof BuildingPlace
        || place instanceof EmissionSourcePlace
        || place instanceof TimeVaryingProfilePlace
        || place instanceof ScenarioInputListPlace;
  }

  /**
   * Updates the visibility of layers. The layers specified in `layersToShow` will be made visible,
   * and all other layers in `allLayers` will be made invisible.
   *
   * @param allLayers the list of all layers to consider
   * @param layersToShow the layers to make visible
   */
  private void switchToVisible(final List<IsLayer<?>> allLayers, final IsLayer<?>... layersToShow) {
    final List<IsLayer<?>> visibles = Stream.of(layersToShow).collect(Collectors.toList());

    // Doubly fire these due to unresolved timing issues.
    allLayers.forEach(v -> eventBus.fireEvent(visibles.contains(v) ? new LayerVisibleCommand(v) : new LayerHiddenCommand(v)));
    Vue.nextTick(() -> {
      allLayers.forEach(v -> eventBus.fireEvent(visibles.contains(v) ? new LayerVisibleCommand(v) : new LayerHiddenCommand(v)));
    });
  }

  private void showArchiveProjects() {
    if (context.getArchiveProjectMarkerLayer() != null) {
      switchToVisible(Arrays.asList(context.getArchiveProjectMarkerLayer()), context.getArchiveProjectMarkerLayer());
    }
  }

  private void hideArchiveProjects() {
    if (context.getArchiveProjectMarkerLayer() != null) {
      switchToVisible(Arrays.asList(context.getArchiveProjectMarkerLayer()));
    }
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
