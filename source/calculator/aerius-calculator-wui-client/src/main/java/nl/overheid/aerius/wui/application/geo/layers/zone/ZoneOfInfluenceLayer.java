/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.zone;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.Feature;
import ol.OLFactory;
import ol.format.GeoJson;
import ol.format.GeoJsonFeatureOptions;
import ol.layer.Layer;
import ol.source.Vector;
import ol.style.Fill;
import ol.style.Stroke;
import ol.style.Style;

import nl.aerius.geo.command.LayerHiddenCommand;
import nl.aerius.geo.command.LayerVisibleCommand;
import nl.aerius.geo.domain.IsLayer;
import nl.aerius.geo.domain.LayerInfo;
import nl.aerius.geo.domain.legend.TextLegend;
import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationPreparationContext;
import nl.overheid.aerius.wui.application.event.CalculationStatusEvent;
import nl.overheid.aerius.wui.application.event.SituationResultsSelectedEvent;
import nl.overheid.aerius.wui.application.event.SwitchActiveCalculationEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceAddedEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceDeletedEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceListChangeEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceUpdatedEvent;
import nl.overheid.aerius.wui.application.i18n.M;

/**
 * Renders a zone of influence on the map, comprising of a buffer around the sources of all situations in the currently active calculaton job,
 * and union of the resulting geometries
 */
public class ZoneOfInfluenceLayer extends BasicEventComponent implements IsLayer<Layer> {
  private static final ZoneOfInfluenceLayerEventBinder EVENT_BINDER = GWT.create(ZoneOfInfluenceLayerEventBinder.class);

  interface ZoneOfInfluenceLayerEventBinder extends EventBinder<ZoneOfInfluenceLayer> {
  }

  private static final Stroke STYLE_STROKE = OLFactory.createStroke(OLFactory.createColor(0, 0, 0, 1), 2);
  private static final int[] STYLE_STROKE_LINEDASH = new int[] {1, 4};
  private static final Fill STYLE_FILL = OLFactory.createFill(OLFactory.createColor(255, 255, 255, 0.2));
  private static final Style STYLE = OLFactory.createStyle(STYLE_FILL, STYLE_STROKE);
  private static final int MIN_RESOLUTION = 10;

  protected final LayerInfo info;
  private final ol.layer.Vector layer;

  private final Vector source;

  private final CalculationPreparationContext calculationContext;

  static {
    STYLE_STROKE.setLineDash(STYLE_STROKE_LINEDASH);
  }

  @Inject
  public ZoneOfInfluenceLayer(final EventBus eventBus, final CalculationPreparationContext calculationContext) {
    this.calculationContext = calculationContext;
    this.info = new LayerInfo();
    this.info.setName(this.getClass().getCanonicalName());
    this.info.setTitle(M.messages().layerZoneOfInfluence());

    source = new ol.source.Vector();
    layer = new ol.layer.Vector();
    layer.setSource(source);
    layer.setStyle(STYLE);

    setEventBus(eventBus);
  }

  @EventHandler
  public void handle(final SituationResultsSelectedEvent e) {
    if (e.getValue().getHexagonType() == SummaryHexagonType.CUSTOM_CALCULATION_POINTS) {
      eventBus.fireEvent(new LayerHiddenCommand(this));
    } else {
      eventBus.fireEvent(new LayerVisibleCommand(this));
    }
  }

  @EventHandler(handles = {EmissionSourceAddedEvent.class, EmissionSourceDeletedEvent.class, EmissionSourceUpdatedEvent.class,
      EmissionSourceListChangeEvent.class, SwitchActiveCalculationEvent.class, CalculationStatusEvent.class})
  protected void onEvent() {
    updateZoneOfInfluence();
  }

  private int maximumRange() {
    final CalculationJobContext context = calculationContext.getActiveOrTemporaryJob();
    return (int) (context == null || context.getCalculationOptions() == null
        ? 0
        : context.getCalculationOptions().getCalculateMaximumRange());
  }

  private void updateZoneOfInfluence() {
    source.clear(true);

    final CalculationJobContext context = calculationContext.getActiveOrTemporaryJob();
    if (context == null || context.getSituationComposition().getSituations().isEmpty()) {
      return;
    }

    final List<Object> points = context.getSituationComposition().getSituations().stream()
        .flatMap(x -> x.getSources().stream())
        .filter(v -> v.getGeometry() != null)
        .map(v -> new GeoJson().writeFeatureObject(v, new GeoJsonFeatureOptions()))
        .collect(Collectors.toList());
    final int maximumRange = maximumRange();

    this.info.setLegend(new TextLegend(M.messages().layerZoneOfInfluenceLegendText(maximumRange / 1000D)));
    final String geometryGeoJson = zoneOfInfluence(points, maximumRange);
    final Feature[] features = new GeoJson().readFeatures(geometryGeoJson, new GeoJsonFeatureOptions());
    source.addFeatures(features);
  }

  private native String zoneOfInfluence(List<Object> geoJsonLst, int bufferDistance) /*-{
    var reader = new $wnd.jsts.io.GeoJSONReader();
    var writer = new $wnd.jsts.io.GeoJSONWriter();

    // Higher accuracy for buffer by setting the number of quadrants (default is 8)
    var bufferParams = { quadrantSegments: 32 };

    var geometries = geoJsonLst.@java.util.List::toArray()().map(function(feature) {
        return reader.read(feature).geometry.buffer(bufferDistance, bufferParams.quadrantSegments);
    });

    var unionGeometry = geometries.reduce(function(prev, curr) {
        return prev.union(curr);
    });

    return JSON.stringify(writer.write(unionGeometry));
  }-*/;

  @Override
  public Layer asLayer() {
    return layer;
  }

  @Override
  public Optional<LayerInfo> getInfoOptional() {
    return Optional.of(info);
  }

  @Override
  public LayerInfo getInfo() {
    return info;
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
