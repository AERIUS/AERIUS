/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.input.drop;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.Vue;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;
import jsinterop.base.Js;

import nl.overheid.aerius.wui.vue.BasicVueView;

/**
 * A simple complex version of the {@link SimplifiedListBoxComponent}, the main difference is this component supports a
 * groupFunction which creates {@link GroupItem}, which allows callers to group options in an optgroup element.
 *
 * Ideally, this feature can be added to the simple listbox aswell, but then there's probably some future bugs or regressions
 * in there especially with regards to optionsArr, so we'll 90% duplicate this and hopefully merge them together at some point.
 */
@Component(name = "complex-listbox")
public class ComplexifiedListBoxComponent extends BasicVueView implements IsVueComponent {
  public static class GroupItem {
    public String group;
    public Map<String, Object> values;

    public GroupItem(final String group) {
      this(group, new LinkedHashMap<>());
    }

    public GroupItem(final String group, final Map<String, Object> values) {
      this.group = group;
      this.values = values;
    }

    public boolean hasGroup() {
      return group != null && !"".equals(group);
    }

    public String getLabel() {
      return group;
    }

    public Map<String, Object> getValues() {
      return values;
    }

    public static GroupItem asItem(final Entry<String, List<Entry<String, Object>>> lst) {
      return new GroupItem(lst.getKey(), lst.getValue().stream()
          .collect(Collectors.toMap(v -> v.getKey(),
              v -> v.getValue(),
              (e1, e2) -> e1, () -> new LinkedHashMap<>())));
    }
  }

  @Prop boolean enabled;
  @Prop String placeholder;
  @Prop String id;

  @Prop boolean strict;

  @Prop Function<Object, Object> keyFunction;
  @Prop Function<Object, String> nameFunction;

  @Prop Function<Object, String> groupFunction;

  @SuppressWarnings("rawtypes") @Prop(checkType = true) @JsProperty Collection options;
  @Prop @JsProperty Object[] optionsArr;

  @Prop String selectedKey;
  @Prop Object selected;

  @Data @JsProperty Map<String, Object> optionsData = new HashMap<>();
  @Data String selectedData = "";

  @PropDefault("strict")
  public boolean strictDefault() {
    return true;
  }

  @PropDefault("enabled")
  public boolean enabledDefault() {
    return true;
  }

  @Computed
  public List<GroupItem> getGroupItems() {
    GroupItem lastGroup = null;
    final List<GroupItem> groups = new ArrayList<>();
    for (final Entry<String, Object> entry : optionsData.entrySet()) {
      final Object value = entry.getValue();
      final String group = Optional.ofNullable(groupFunction.apply(value)).orElse("");
      if (lastGroup == null || !group.equals(lastGroup.group)) {
        lastGroup = new GroupItem(group);
        groups.add(lastGroup);
      }

      lastGroup.values.put(entry.getKey(), value);
    }
    return groups;
  }

  @PropDefault("groupFunction")
  public Function<Object, String> groupFunctionDefault() {
    return null;
  }

  @SuppressWarnings("rawtypes")
  @PropDefault("options")
  public List optionsDefault() {
    return new ArrayList<>();
  }

  @PropDefault("keyFunction")
  public Function<Object, String> keyFunctionDefault() {
    return String::valueOf;
  }

  @PropDefault("nameFunction")
  public Function<Object, String> nameFunctionDefault() {
    return String::valueOf;
  }

  @Computed("hasGroups")
  public boolean hasGroups() {
    return groupFunction != null;
  }

  @Watch(value = "optionsArr", isImmediate = true)
  public void onOptionsArrChange(final Object[] neww) {
    if (neww == null) {
      return;
    }

    // Invoke on the next tick, this is a parallel of :options and can therefore not
    // be executed in the same tick
    Vue.nextTick(() -> {
      indexOptions(Stream.of(neww).collect(Collectors.toList()));

      // Trigger selected prop watcher manually here (because it probably updated
      // based on old options data)
      onSelectedChange(selected);
    });
  }

  @Watch(value = "options", isImmediate = true)
  public void onOptionsChange(final List<Object> neww) {
    indexOptions(neww);
  }

  public void indexOptions(final List<Object> options) {
    optionsData.clear();
    if (options == null) {
      return;
    }

    optionsData.putAll(options.stream()
        .collect(Collectors.toMap(v -> formatKey(Js.uncheckedCast(v)), Js::uncheckedCast)));
  }

  @Watch(value = "selected", isImmediate = true)
  public void onSelectedChange(final Object neww) {
    if (neww == null && strict) {
      selectedData = "";
    } else {
      setSelection(formatKey(neww));
    }
  }

  @Watch(value = "selectedKey", isImmediate = true)
  public void onSelectedKeyChange(final String neww) {
    if (neww == null) {
      // Do nothing
    } else {
      setSelection(neww);
    }
  }

  private void setSelection(final String selection) {
    if (optionsData.containsKey(selection) || !strict) {
      selectedData = selection;
    } else {
      selectedData = "";
    }
  }

  @JsMethod
  public boolean isSelected(final Object obj) {
    return selectedData != null && selectedData.equals(formatKey(obj));
  }

  @JsMethod
  public String formatKey(final Object obj) {
    // Do an additional string conversion so the key function can return things like
    // integers aswell
    return String.valueOf(keyFunction.apply(obj));
  }

  @JsMethod
  public String formatName(final Object obj) {
    return nameFunction.apply(obj);
  }

  @JsMethod
  public void onUserChange() {
    if (!optionsData.containsKey(selectedData)) {
      if (strict) {
        throw new IllegalStateException("Selected data not known to be an option: " + selectedData);
      } else {
        vue().$emit("select-unknown", selectedData);
        return;
      }
    }

    vue().$emit("select", optionsData.get(selectedData));
    vue().$emit("select-key", selectedData);
  }
}
