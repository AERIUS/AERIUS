/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.calculation;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.axellience.vuegwt.core.client.Vue;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.format.GeoJson;
import ol.format.GeoJsonOptions;

import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.js.calculation.ResultsGraphRange;
import nl.overheid.aerius.js.calculation.ResultsSelected;
import nl.overheid.aerius.shared.domain.result.ResultView;
import nl.overheid.aerius.wui.application.command.geo.ClearAssessmentAreaCommand;
import nl.overheid.aerius.wui.application.command.geo.HabitatTypeHoverActiveCommand;
import nl.overheid.aerius.wui.application.command.geo.HabitatTypeHoverInactiveCommand;
import nl.overheid.aerius.wui.application.command.geo.HabitatTypeSelectToggleCommand;
import nl.overheid.aerius.wui.application.command.geo.SelectAssessmentAreaCommand;
import nl.overheid.aerius.wui.application.command.result.ClearCustomCalculationPointResultCommand;
import nl.overheid.aerius.wui.application.command.result.ClearResultValueBoundsCommand;
import nl.overheid.aerius.wui.application.command.result.DeselectResultGraphRangeCommand;
import nl.overheid.aerius.wui.application.command.result.SelectCustomCalculationPointResultCommand;
import nl.overheid.aerius.wui.application.command.result.SelectResultGraphRangeCommand;
import nl.overheid.aerius.wui.application.command.source.GeometryZoomCommand;
import nl.overheid.aerius.wui.application.context.result.ResultSelectionContext;
import nl.overheid.aerius.wui.application.event.ResultsSelectedEvent;
import nl.overheid.aerius.wui.application.event.ResultsViewSelectionCommand;
import nl.overheid.aerius.wui.application.ui.pages.results.habitat.SelectResultHabitatStatisticTypeCommand;

/**
 * This daemon is responsible for capturing and managing result statistic view
 * selections
 */
@Singleton
public class ResultSelectionDaemon extends BasicEventComponent {
  private static final ResultSelectionDaemonEventBinder EVENT_BINDER = GWT.create(ResultSelectionDaemonEventBinder.class);

  interface ResultSelectionDaemonEventBinder extends EventBinder<ResultSelectionDaemon> {
  }

  @Inject private ResultSelectionContext context;

  @EventHandler
  protected void onSelectResultHabitatStatisticTypeCommand(final SelectResultHabitatStatisticTypeCommand c) {
    context.setHabitatStatistic(c.getValue());
  }

  @EventHandler
  protected void onResultsViewSelectionCommand(final ResultsViewSelectionCommand c) {
    context.setExtraAssessmentHabitats(c.getValue() == ResultView.EXTRA_ASSESSMENT_HABITAT_TYPES);
  }

  @EventHandler
  protected void onHabitatTypeHoverActiveCommand(final HabitatTypeHoverActiveCommand c) {
    final boolean result = context.setHighlightedHabitatType(c.getValue());
    context.setExtraAssessmentHabitats(c.isExtraAssessmentHabitats());
    if (!result) {
      c.silence();
    }
  }

  @EventHandler
  protected void onHabitatTypeHoverInactiveCommand(final HabitatTypeHoverInactiveCommand c) {
    // Run at the end of the event loop because this is ordinarily paired with an
    // activation command.
    final boolean result = context.removeHighlightedHabitatType(c.getValue());
    if (!result) {
      c.silence();
    }
  }

  @EventHandler
  protected void onHabitatTypeToggleCommand(final HabitatTypeSelectToggleCommand c) {
    context.setExtraAssessmentHabitats(c.isExtraAssessmentHabitats());
    if (context.isSelectedHabitatTypeCode(c.getValue())) {
      context.removeSelectedHabitatType(c.getValue());
    } else {
      context.addSelectedHabitatTypeCode(c.getValue());
    }
  }

  @EventHandler
  protected void onSelectAssessmentAreaCommand(final SelectAssessmentAreaCommand c) {
    context.setSelectedAssessmentArea(c.getValue());
    fireResultGraphUpdate();
  }

  @EventHandler
  protected void onClearAssessmentAreaCommand(final ClearAssessmentAreaCommand c) {
    context.reset();
    fireResultGraphUpdate();
  }

  @EventHandler
  protected void onClearResultValueBoundsCommand(final ClearResultValueBoundsCommand c) {
    context.clearSelectedBounds();
    fireResultGraphUpdate();
  }

  @EventHandler
  protected void onSelectResultGraphRangeCommand(final SelectResultGraphRangeCommand c) {
    context.addSelectedBound(c.getValue());
    fireResultGraphUpdate();
  }

  @EventHandler
  protected void onDeselectResultGraphRangeCommand(final DeselectResultGraphRangeCommand c) {
    context.removeSelectedBound(c.getValue());
    fireResultGraphUpdate();
  }

  private void fireResultGraphUpdate() {
    // Not sure why this nexttick delay is in here of if it's still necessary.
    Vue.nextTick(() -> {
      fireResultGraphUpdated(context.getSelectedAssessmentArea(), context.getSelectedBounds());
    });
  }

  private void fireResultGraphUpdated(final int assessmentAreaId, final List<ResultsGraphRange> selectedLowerBoundValues) {
    final ResultsSelected resultsSelected = new ResultsSelected(assessmentAreaId, selectedLowerBoundValues);
    eventBus.fireEvent(new ResultsSelectedEvent(resultsSelected));
  }

  @EventHandler
  protected void onSelectCustomCalculationPointResultCommand(final SelectCustomCalculationPointResultCommand command) {
    context.setSelectedCustomPointResult(command.getValue());
    if (command.isSelectedFromList()) {
      eventBus.fireEvent(new GeometryZoomCommand(new GeoJson().readGeometry(command.getValue().getPoint(), new GeoJsonOptions())));
    }
  }

  @EventHandler
  protected void onClearCustomCalculationPointResultCommand(final ClearCustomCalculationPointResultCommand c) {
    context.setSelectedCustomPointResult(null);
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
