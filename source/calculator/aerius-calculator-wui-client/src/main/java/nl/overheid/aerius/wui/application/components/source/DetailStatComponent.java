/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import nl.aerius.wui.vue.transition.VerticalCollapse;

@Component(components = {
    VerticalCollapse.class,
})
public class DetailStatComponent implements IsVueComponent {
  @Prop String label;
  @Prop String value;
  @Prop String debugId;

  @Prop boolean show;

  @Prop String division;
  @Prop String maxWidth;
  @Prop boolean alignRight;

  @PropDefault("show")
  boolean showDefault() {
    return true;
  }

  @PropDefault("alignRight")
  boolean alignRightDefault() {
    return false;
  }

  @PropDefault("maxWidth")
  String maxWidthDefault() {
    return "100%";
  }

  @PropDefault("division")
  String divisionDefault() {
    return "15fr 23fr";
  }

  @PropDefault("debugId")
  String debugIdDefault() {
    return "label";
  }
}
