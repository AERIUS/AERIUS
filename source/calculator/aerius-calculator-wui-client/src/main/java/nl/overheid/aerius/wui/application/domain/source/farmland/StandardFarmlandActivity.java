/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.farmland;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.wui.vue.ReactivityUtil;

/**
 * Client side implementation of StandardFarmland props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class StandardFarmlandActivity extends FarmlandActivity {
  private String farmSourceCategoryCode;
  private Double numberOfAnimals;
  private Double numberOfDays;
  private Double tonnes;
  private Double metersCubed;
  private Double numberOfApplications;

  public static final @JsOverlay StandardFarmlandActivity create(final String activityCode) {
    final StandardFarmlandActivity props = Js.uncheckedCast(JsPropertyMap.of());

    init(props, activityCode);
    return props;
  }

  public static final @JsOverlay void init(final StandardFarmlandActivity props, final String activityCode) {
    FarmlandActivity.initFarmlandActivity(props, activityCode, FarmlandActivityType.STANDARD);
    ReactivityUtil.ensureJsProperty(props, "farmSourceCategoryCode", props::setFarmSourceCategoryCode, null);
    ReactivityUtil.ensureJsPropertyAsNull(props, "numberOfAnimals", props::setNumberOfAnimals);

    ReactivityUtil.ensureJsPropertyAsNull(props, "numberOfDays", props::setNumberOfDays);
    ReactivityUtil.ensureJsPropertyAsNull(props, "tonnes", props::setTonnes);
    ReactivityUtil.ensureJsPropertyAsNull(props, "metersCubed", props::setMetersCubed);
    ReactivityUtil.ensureJsPropertyAsNull(props, "numberOfApplications", props::setNumberOfApplications);
  }

  public final @JsOverlay String getFarmSourceCategoryCode() {
    return farmSourceCategoryCode;
  }

  public final @JsOverlay void setFarmSourceCategoryCode(final String farmSourceCategoryCode) {
    this.farmSourceCategoryCode = farmSourceCategoryCode;
  }

  public final @JsOverlay Double getNumberOfAnimals() {
    return numberOfAnimals;
  }

  public final @JsOverlay void setNumberOfAnimals(final Double numberOfAnimals) {
    this.numberOfAnimals = numberOfAnimals;
  }

  public final @JsOverlay Double getNumberOfDays() {
    return numberOfDays;
  }

  public final @JsOverlay void setNumberOfDays(final Double numberOfDays) {
    this.numberOfDays = numberOfDays;
  }

  public final @JsOverlay Double getTonnes() {
    return tonnes;
  }

  public final @JsOverlay void setTonnes(final Double tonnes) {
    this.tonnes = tonnes;
  }

  public final @JsOverlay Double getMetersCubed() {
    return metersCubed;
  }

  public final @JsOverlay void setMetersCubed(final Double metersCubed) {
    this.metersCubed = metersCubed;
  }

  public final @JsOverlay Double getNumberOfApplications() {
    return numberOfApplications;
  }

  public final @JsOverlay void setNumberOfApplications(final Double numberOfApplications) {
    this.numberOfApplications = numberOfApplications;
  }

}
