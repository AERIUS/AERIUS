/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.flags;

import java.util.Optional;

import com.google.gwt.storage.client.Storage;

public class LocalStorageUtil {
  public static Optional<String> tryGet(final String key) {
    return Optional.ofNullable(Storage.getLocalStorageIfSupported())
        .map(v -> v.getItem(key));
  }

  public static void trySet(final String key, final Object value) {
    Optional.ofNullable(Storage.getLocalStorageIfSupported())
        .ifPresent(storage -> storage.setItem(key, String.valueOf(value)));
  }

  public static void tryRemove(final String key) {
    Optional.ofNullable(Storage.getLocalStorageIfSupported())
        .ifPresent(storage -> storage.removeItem(key));
  }
}
