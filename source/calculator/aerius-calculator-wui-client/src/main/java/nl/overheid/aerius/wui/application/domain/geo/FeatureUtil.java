/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.domain.geo;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import ol.Coordinate;
import ol.Feature;
import ol.geom.Geometry;
import ol.geom.LineString;
import ol.geom.Point;
import ol.geom.Polygon;

import elemental2.core.Global;

import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.wui.application.geo.util.GeoType;

public class FeatureUtil {
  /**
   * Refer to the AERIUS Calculator Handbook 2023, page 207, for details on coordinate accuracy.
   * See: https://nexus.aerius.nl/repository/website-resources/calculator/handboek_aerius_calculator_2023.pdf
   */
  private static final int MAX_DECIMAL_PLACES = 3;
  private static final double DECIMAL_FACTOR = Math.pow(10, MAX_DECIMAL_PLACES);

  private FeatureUtil() {}

  public static String smartIncrementLabel(final String label, final Set<String> existingLabels) {
    final String next = smartIncrementLabel(label);
    return existingLabels.contains(next)
        ? smartIncrementLabel(next, existingLabels)
        : next;
  }

  /**
   * Perform a file-style increment of the given string.
   *
   * If the label matches "(n)" at the end of the string where n is a natural number,
   * increment the number and return it. If not, add ` (1)` to the string and
   * return it.
   */
  public static String smartIncrementLabel(final String label) {
    if (label.matches(".+\\((\\d+)\\)$")) {
      final int beginBracket = label.lastIndexOf("(");
      final int endBracket = label.lastIndexOf(")");
      final String numPortion = label.substring(beginBracket + 1, endBracket);

      try {
        final int num = Integer.parseInt(numPortion);
        final String labelBare = label.substring(0, beginBracket);
        return labelBare + "(" + (num + 1) + ")";
      } catch (final NumberFormatException e) {
        return label + " (1)";
      }
    } else {
      return label + " (1)";
    }
  }

  /**
   * Deep copies a {@link Feature}.
   * Includes copying ID.
   *
   * @param originalFeature The original feature object.
   * @return new feature
   */
  public static <F extends Feature> F clone(final F originalFeature) {
    final Feature feature = originalFeature.clone();
    // ol.Feature.clone does not create a deep copy. Hence copy properties.
    copyProperties(feature, feature);
    // ol.Feature.clone does not copy the id, so be sure to set it in this case.
    feature.setId(originalFeature.getId());
    return Js.cast(feature);
  }

  /**
   * Deep copies a {@link Feature}.
   * Feature will be copied without id (and gmlId).
   *
   * @param originalFeature The original feature object.
   * @return new feature
   */
  public static <F extends Feature> F cloneWithoutIds(final F originalFeature) {
    final Feature feature = originalFeature.clone();
    // ol.Feature.clone does not create a deep copy. Hence copy properties.
    copy(feature, feature);
    // ol.Feature.clone does not copy the id, which is fine in this case.
    // However, we also want to clear GML ID (perhaps not all features have this, but shouldn't hurt in that case).
    feature.set("gmlId", null);
    return Js.cast(feature);
  }

  /**
   * Deep copies the source feature to the target feature.
   *
   * @param sourceFeature The feature to copy
   * @param targetFeature The feature to set the data of the source feature
   */
  public static <F extends Feature> void copy(final F sourceFeature, final F targetFeature) {
    copyProperties(sourceFeature, targetFeature);

    targetFeature.setGeometry(roundGeometry(sourceFeature.getGeometry()));
  }

  /**
   * Copies properties of a feature without the geometry data.
   *
   * @param <F>
   * @param sourceFeature
   * @param targetFeature
   */
  private static <F extends Feature> void copyProperties(final F sourceFeature, final Feature targetFeature) {
    // Because clone makes a shallow copy of the properties we need to set the
    // properties by re-reading it as json.
    final JsPropertyMap<Object> properties = Js.cast(Global.JSON.parse(Global.JSON.stringify(sourceFeature.getProperties())));
    // Because properties contains everything, including geometry which can't be set
    // as property it must be deleted and added via the method.
    properties.delete("geometry");
    targetFeature.setProperties(properties);
  }

  private static Geometry roundGeometry(final Geometry geometry) {
    final Geometry ret;
    if (GeoType.POINT.is(geometry)) {
      ret = new Point(roundCoordinate(((Point) geometry).getCoordinates()));
    } else if (GeoType.LINE_STRING.is(geometry)) {
      ret = new LineString(roundCoordinates(((LineString) geometry).getCoordinates()));
    } else if (GeoType.POLYGON.is(geometry)) {
      ret = new Polygon(roundCoordinates(((Polygon) geometry).getCoordinates()));
    } else {
      ret = geometry;
    }

    return ret;
  }

  private static Coordinate[][] roundCoordinates(final Coordinate[][] coordinates) {
    return Stream.of(coordinates)
        .map(v -> roundCoordinates(v))
        .collect(Collectors.toList())
        .toArray(new Coordinate[][] {});
  }

  private static Coordinate[] roundCoordinates(final Coordinate[] coordinates) {
    return Stream.of(coordinates)
        .map(v -> roundCoordinate(v))
        .collect(Collectors.toList())
        .toArray(new Coordinate[] {});
  }

  private static Coordinate roundCoordinate(final Coordinate coordinate) {
    return new Coordinate(roundCoordinate(coordinate.getX()), roundCoordinate(coordinate.getY()));
  }

  /**
   * Round the given value to the given number of decimal places.
   */
  private static double roundCoordinate(final double value) {
    return value;
    // TODO AER-2361: enable actual rounding when powers that be decide it is time.
    //return Math.round(value * DECIMAL_FACTOR) / DECIMAL_FACTOR;
  }
}
