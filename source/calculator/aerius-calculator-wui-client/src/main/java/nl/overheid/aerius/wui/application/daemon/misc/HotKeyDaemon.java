/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.misc;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import elemental2.dom.DomGlobal;
import elemental2.dom.Event;
import elemental2.dom.EventTarget;
import elemental2.dom.KeyboardEvent;

import jsinterop.base.Js;

import nl.aerius.geo.command.MapPanDownCommand;
import nl.aerius.geo.command.MapPanLeftCommand;
import nl.aerius.geo.command.MapPanRightCommand;
import nl.aerius.geo.command.MapPanUpCommand;
import nl.aerius.geo.command.MapZoomInCommand;
import nl.aerius.geo.command.MapZoomOutCommand;
import nl.aerius.wui.command.SimpleStatelessCommand;
import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.place.Place;
import nl.overheid.aerius.shared.domain.context.InputTypeViewMode;
import nl.overheid.aerius.wui.application.command.MeasureToggleCommand;
import nl.overheid.aerius.wui.application.command.SearchPopupToggleCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingCreateNewCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingDeleteSelectedCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingDuplicateSelectedCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingEditSelectedCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingFeatureToggleSelectCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointCreateNewCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointDeleteSelectedCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointEditSelectedCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointToggleSelectCommand;
import nl.overheid.aerius.wui.application.command.geo.ChangeLabelDisplayModeCommand;
import nl.overheid.aerius.wui.application.command.geo.InfoPopupToggleCommand;
import nl.overheid.aerius.wui.application.command.geo.LayerPopupToggleCommand;
import nl.overheid.aerius.wui.application.command.misc.HeaderVisibilityToggleCommand;
import nl.overheid.aerius.wui.application.command.misc.ManualPanelToggleCommand;
import nl.overheid.aerius.wui.application.command.misc.ToggleHotkeysCommand;
import nl.overheid.aerius.wui.application.command.misc.ToggleHotkeysEnabledCommand;
import nl.overheid.aerius.wui.application.command.misc.ToggleNotificationDisplayCommand;
import nl.overheid.aerius.wui.application.command.situation.CreateEmptySituationCommand;
import nl.overheid.aerius.wui.application.command.situation.SwitchSituationCommand;
import nl.overheid.aerius.wui.application.command.source.DeselectAllFeaturesCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceCreateNewCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceDeleteSelectedCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceDuplicateSelectedCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditSelectedCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceFeatureToggleSelectCommand;
import nl.overheid.aerius.wui.application.command.source.FeatureZoomCommand;
import nl.overheid.aerius.wui.application.command.source.SituationCreateNewCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.BuildingListContext;
import nl.overheid.aerius.wui.application.context.CalculationPointEditorContext;
import nl.overheid.aerius.wui.application.context.CalculationPointListContext;
import nl.overheid.aerius.wui.application.context.EmissionSourceListContext;
import nl.overheid.aerius.wui.application.context.MapConfigurationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SimpleListContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.ImaerFeature;
import nl.overheid.aerius.wui.application.domain.geo.LabelDisplayMode;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.event.LeftPanelToggleCommand;
import nl.overheid.aerius.wui.application.place.ApplicationPlaceController;
import nl.overheid.aerius.wui.application.place.CalculatePlace;
import nl.overheid.aerius.wui.application.place.CalculationPointsPlace;
import nl.overheid.aerius.wui.application.place.ExportPlace;
import nl.overheid.aerius.wui.application.place.HomePlace;
import nl.overheid.aerius.wui.application.place.ResultsPlace;
import nl.overheid.aerius.wui.application.place.ScenarioInputListPlace;
import nl.overheid.aerius.wui.application.ui.pages.emissionsourceList.ScenarioInputListContext;

public class HotKeyDaemon extends BasicEventComponent implements Daemon {
  private static final HotKeyDaemonEventBinder EVENT_BINDER = GWT.create(HotKeyDaemonEventBinder.class);

  interface HotKeyDaemonEventBinder extends EventBinder<HotKeyDaemon> {}

  private static final String CREATION_MODE_KEY = "KeyC";
  private static final String SITUATION_MODE_KEY = "KeyQ";

  @Inject private ApplicationContext appContext;
  @Inject private ScenarioContext scenarioContext;
  @Inject private ApplicationPlaceController placeController;

  @Inject private ScenarioInputListContext inputListContext;
  @Inject private EmissionSourceListContext emissionSourceListContext;
  @Inject private BuildingListContext buildingListContext;
  @Inject private CalculationPointListContext calculationPointListContext;
  @Inject private MapConfigurationContext mapConfigContext;

  @Inject private HotkeyContext hotkeyContext;

  @Inject private CalculationPointEditorContext calculationPointsEditorContext;

  private boolean situationMode;
  private boolean creationMode;

  @Override
  public void init() {
    DomGlobal.document.addEventListener("keydown", this::handleKeyDown);
    DomGlobal.document.addEventListener("keyup", this::handleKeyUp);
  }

  @EventHandler
  public void onToggleHotkeysEnabledCommand(final ToggleHotkeysEnabledCommand c) {
    hotkeyContext.toggle();
  }

  private void handleKeyDown(final Event e) {
    if (e instanceof KeyboardEvent) {
      handleKeyDown((KeyboardEvent) e);
    }
  }

  private void handleKeyUp(final Event e) {
    if (e instanceof KeyboardEvent) {
      handleKeyUp((KeyboardEvent) e);
    }
  }

  private void handleKeyDown(final KeyboardEvent e) {
    if (hotKeysDisabled()) {
      return;
    }

    // Check ctrl+* shortcuts before input
    final boolean ctrl = e.ctrlKey;
    switch (e.code) {
    case "KeyK": // Also allow ctrl+K for search (undocumented)
      if (ctrl) {
        eventBus.fireEvent(new SearchPopupToggleCommand());
        e.preventDefault(); // Prevent default browser shortcut (also search)
      }
      break;
    }

    // Hotkeys that should always work (regardless of input)
    handlePriorityKeys(e);

    // Cancel if input element has focus
    final boolean isInput = isInput(e.target);
    if (isInput) {
      return;
    }

    switch (e.key) {
    case "+":
      // Zoom in if ctrl is not pressed
      if (!ctrl) {
        eventBus.fireEvent(new MapZoomInCommand());
      }
      break;
    case "-":
      // Zoom out if ctrl is not pressed
      if (!ctrl) {
        eventBus.fireEvent(new MapZoomOutCommand());
      }
      break;
    case "ArrowRight":
      eventBus.fireEvent(new MapPanRightCommand());
      break;
    case "ArrowLeft":
      eventBus.fireEvent(new MapPanLeftCommand());
      break;
    case "ArrowUp":
      eventBus.fireEvent(new MapPanUpCommand());
      break;
    case "ArrowDown":
      eventBus.fireEvent(new MapPanDownCommand());
      break;
    default:
      break;
    }
  }

  private boolean hotKeysDisabled() {
    return !hotkeyContext.isEnabled();
  }

  private void handleKeyUp(final KeyboardEvent e) {
    // Hotkey enable/disable overrides everything
    if ("?".equals(e.key) && e.ctrlKey) {
      hotkeyContext.toggle();
      return;
    }

    if (hotKeysDisabled()) {
      return;
    }

    final boolean isInput = isInput(e.target);

    if (isInput) {
      return;
    }

    // Hotkeys that should not work if within an input paradigm
    handleSecondaryKeys(e);

    if (creationMode && !CREATION_MODE_KEY.equals(e.code)) {
      creationMode = false;
    }
    if (situationMode && !SITUATION_MODE_KEY.equals(e.code)) {
      situationMode = false;
    }
  }

  private void handleSecondaryKeys(final KeyboardEvent e) {
    final boolean ctrl = e.ctrlKey;
    final boolean shift = e.shiftKey;

    switch (e.code) {
    case "KeyN":
      eventBus.fireEvent(new ToggleNotificationDisplayCommand());
      break;
    case "KeyL":
      eventBus.fireEvent(new LayerPopupToggleCommand());
      break;
    case "KeyW":
      eventBus.fireEvent(new DeselectAllFeaturesCommand());
      break;
    case "KeyI":
      eventBus.fireEvent(new InfoPopupToggleCommand());
      break;
    case "KeyS":
      if (creationMode) {
        eventBus.fireEvent(new CreateEmptySituationCommand(true));
      } else {
        eventBus.fireEvent(new SearchPopupToggleCommand());
      }
      break;
    case "KeyM":
      eventBus.fireEvent(new MeasureToggleCommand());
      break;
    case "KeyF":
      if (ctrl && shift) {
        eventBus.fireEvent(new HeaderVisibilityToggleCommand());
      } else if (shift) {
        eventBus.fireEvent(new ManualPanelToggleCommand());
      } else {
        eventBus.fireEvent(new LeftPanelToggleCommand());
      }
      break;
    case "KeyT":
      switch (mapConfigContext.getLabelDisplayMode()) {
      case ID:
        eventBus.fireEvent(new ChangeLabelDisplayModeCommand(LabelDisplayMode.NAME));
        break;
      case NAME:
        eventBus.fireEvent(new ChangeLabelDisplayModeCommand(LabelDisplayMode.HIDDEN));
        break;
      case HIDDEN:
        eventBus.fireEvent(new ChangeLabelDisplayModeCommand(LabelDisplayMode.ID));
        break;
      }
      break;
    case SITUATION_MODE_KEY:
      situationMode = true;
      break;
    case CREATION_MODE_KEY:
      if (creationMode) {
        doInCurrentInput(
            () -> eventBus.fireEvent(new EmissionSourceCreateNewCommand()),
            () -> eventBus.fireEvent(new BuildingCreateNewCommand()),
            () -> eventBus.fireEvent(new CalculationPointCreateNewCommand()));
        creationMode = false;
      } else {
        creationMode = true;
      }
      break;
    case "KeyD":
      doWithSelectedFeature(
          v -> eventBus.fireEvent(new EmissionSourceDeleteSelectedCommand()),
          v -> eventBus.fireEvent(new BuildingDeleteSelectedCommand()),
          v -> eventBus.fireEvent(new CalculationPointDeleteSelectedCommand()));
      break;
    case "KeyV":
      doWithSelectedFeature(
          v -> eventBus.fireEvent(new EmissionSourceDuplicateSelectedCommand()),
          v -> eventBus.fireEvent(new BuildingDuplicateSelectedCommand()),
          v -> {
          });
      break;
    case "KeyE":
      if (creationMode) {
        if (scenarioContext.getSituations().isEmpty()) {
          eventBus.fireEvent(new SituationCreateNewCommand());
        }
        eventBus.fireEvent(new EmissionSourceCreateNewCommand());
      } else {
        doWithSelectedFeature(
            v -> eventBus.fireEvent(new EmissionSourceEditSelectedCommand()),
            v -> eventBus.fireEvent(new BuildingEditSelectedCommand()),
            v -> eventBus.fireEvent(new CalculationPointEditSelectedCommand()));
      }
      break;
    case "KeyZ":
      doWithSelectedFeature(
          v -> eventBus.fireEvent(new FeatureZoomCommand(v)),
          v -> eventBus.fireEvent(new FeatureZoomCommand(v)),
          v -> eventBus.fireEvent(new FeatureZoomCommand(v)));
      break;
    case "KeyG": // Also allow G (undocumented)
    case "KeyB":
      if (creationMode) {
        eventBus.fireEvent(new BuildingCreateNewCommand());
      }
      break;
    case "KeyR": // Also allow R (undocumented)
    case "KeyA":
      if (creationMode) {
        eventBus.fireEvent(new CalculationPointCreateNewCommand());
      }
      break;
    case "KeyH":
      if (shift) {
        placeController.goTo(new HomePlace(appContext.getTheme()));
      }
      break;
    case "Digit1":
      if (shift) {
        placeController.goTo(new ScenarioInputListPlace(appContext.getTheme()));
      }
      break;
    case "Digit2":
      if (shift) {
        placeController.goTo(new CalculationPointsPlace(appContext.getTheme()));
      }
      break;
    case "Digit3":
      if (shift) {
        placeController.goTo(new CalculatePlace(appContext.getTheme()));
      }
      break;
    case "Digit4":
      if (shift) {
        placeController.goTo(new ResultsPlace(appContext.getTheme()));
      }
      break;
    case "Digit5":
      if (shift) {
        placeController.goTo(new ExportPlace(appContext.getTheme()));
      }
      break;
    }

    switch (e.key) {
    case "?":
      eventBus.fireEvent(new ToggleHotkeysCommand());
      break;
    case "1":
    case "2":
    case "3":
    case "4":
    case "5":
    case "6":
    case "7":
    case "8":
    case "9":
      final int num = Integer.parseInt(e.key);
      if (situationMode) {
        selectSituation(num);
      } else {
        selectFeatureNumber(num);
      }
      break;
    }
  }

  private void selectSituation(final int num) {
    final List<SituationContext> situations = scenarioContext.getSituations();
    if (situations.size() >= num) {
      final SituationContext sit = situations.get(num - 1);
      eventBus.fireEvent(new SwitchSituationCommand(sit));
    }
  }

  private void doInCurrentInput(final Runnable emissionSourceRunnable, final Runnable buildingRunnable, final Runnable calculationPointRunnable) {
    final Place currentPlace = placeController.getPlace();
    final Optional<Runnable> runnableToRun;
    if (currentPlace instanceof ScenarioInputListPlace && inputListContext.getViewMode() == InputTypeViewMode.BUILDING) {
      runnableToRun = Optional.ofNullable(buildingRunnable);
    } else if (currentPlace instanceof ScenarioInputListPlace && inputListContext.getViewMode() == InputTypeViewMode.EMISSION_SOURCES) {
      runnableToRun = Optional.ofNullable(emissionSourceRunnable);
    } else if (currentPlace instanceof CalculationPointsPlace && !calculationPointsEditorContext.isEditing()) {
      runnableToRun = Optional.ofNullable(calculationPointRunnable);
    } else {
      runnableToRun = Optional.empty();
    }
    runnableToRun.ifPresent(Runnable::run);
  }

  private void selectFeatureNumber(final int num) {
    final Place currentPlace = placeController.getPlace();
    if (currentPlace instanceof ScenarioInputListPlace && inputListContext.getViewMode() == InputTypeViewMode.BUILDING) {
      selectFeatureNumber(scenarioContext.getActiveSituation().getBuildings(), num,
          feature -> new BuildingFeatureToggleSelectCommand(feature, false));
    } else if (currentPlace instanceof ScenarioInputListPlace && inputListContext.getViewMode() == InputTypeViewMode.EMISSION_SOURCES) {
      selectFeatureNumber(scenarioContext.getActiveSituation().getSources(), num,
          feature -> new EmissionSourceFeatureToggleSelectCommand(feature, false));
    } else if (currentPlace instanceof CalculationPointsPlace && !calculationPointsEditorContext.isEditing()) {
      selectFeatureNumber(scenarioContext.getCalculationPoints(), num,
          feature -> new CalculationPointToggleSelectCommand(feature, false));
    } else {
      // Ignore other combinations
    }
  }

  private <T extends ImaerFeature> void selectFeatureNumber(final List<T> features, final int num,
      final Function<T, SimpleStatelessCommand<T>> eventCreator) {
    if (features.size() >= num) {
      final T feature = features.get(num - 1);
      eventBus.fireEvent(eventCreator.apply(feature));
    }
  }

  private void doWithSelectedFeature(final Consumer<EmissionSourceFeature> emissionSourceConsumer,
      final Consumer<BuildingFeature> buildingConsumer,
      final Consumer<CalculationPointFeature> assessmentPointConsumer) {
    final Place currentPlace = placeController.getPlace();
    if (currentPlace instanceof ScenarioInputListPlace && inputListContext.getViewMode() == InputTypeViewMode.BUILDING) {
      doWithSelectedFeature(buildingListContext, buildingConsumer);
    } else if (currentPlace instanceof ScenarioInputListPlace && inputListContext.getViewMode() == InputTypeViewMode.EMISSION_SOURCES) {
      doWithSelectedFeature(emissionSourceListContext, emissionSourceConsumer);
    } else if (currentPlace instanceof CalculationPointsPlace && !calculationPointsEditorContext.isEditing()) {
      doWithSelectedFeature(calculationPointListContext, assessmentPointConsumer);
    } else {
      // Ignore other combinations
    }
  }

  private static <T extends ImaerFeature> void doWithSelectedFeature(final SimpleListContext<T> context, final Consumer<T> consumer) {
    Optional.ofNullable(context.getLooseSelection())
        .ifPresent(consumer);
  }

  private static void handlePriorityKeys(final KeyboardEvent e) {
    final boolean shiftKey = e.shiftKey;

    switch (e.code) {
    case "Escape":
      if (shiftKey) {
        e.preventDefault();
        DomGlobal.document.querySelector("main").focus();
      }
      break;
    }
  }

  private static boolean isInput(final EventTarget target) {
    final Element elem = (Element) Js.cast(target);
    final String tag = elem.getTagName().toLowerCase(Locale.ROOT);
    return "input".equals(tag)
        || "textarea".equals(tag)
        || "select".equals(tag);
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
