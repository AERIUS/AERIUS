/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.zone;

import java.util.Optional;

import javax.inject.Inject;

import com.google.inject.assistedinject.Assisted;

import ol.Feature;
import ol.OLFactory;
import ol.format.Wkt;
import ol.geom.Geometry;
import ol.layer.Layer;
import ol.style.Fill;
import ol.style.Stroke;
import ol.style.Style;

import nl.aerius.geo.domain.IsLayer;
import nl.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.wui.application.i18n.M;

/**
 * Renders the calculation boundary WKT on the map
 */
public class CalculationBoundaryLayer implements IsLayer<Layer> {

  private static final Stroke STYLE_STROKE = OLFactory.createStroke(OLFactory.createColor(0, 0, 0, 1), 1);
  private static final int[] STYLE_STROKE_LINEDASH = new int[] {1, 4};
  private static final Fill STYLE_FILL = OLFactory.createFill(OLFactory.createColor(255, 255, 255, 0.5));
  private static final Style STYLE = OLFactory.createStyle(STYLE_FILL, STYLE_STROKE);
  private static final int MIN_RESOLUTION = 10;

  protected final LayerInfo info;
  private final ol.layer.Vector layer;

  static {
    STYLE_STROKE.setLineDash(STYLE_STROKE_LINEDASH);
  }

  @Inject
  public CalculationBoundaryLayer(@Assisted final String boundaryWkt) {
    this.info = new LayerInfo();
    this.info.setName(this.getClass().getCanonicalName());
    this.info.setTitle(M.messages().layerCalculationBoundary());

    final ol.source.Vector source = new ol.source.Vector();
    layer = new ol.layer.Vector();
    layer.setSource(source);
    layer.setStyle(STYLE);
    layer.setMinResolution(MIN_RESOLUTION);

    final Geometry boundaryGeometry = new Wkt().readGeometry(boundaryWkt);
    final Feature boundaryFeature = new Feature();
    boundaryFeature.setGeometry(boundaryGeometry);
    source.addFeature(boundaryFeature);
  }

  @Override
  public Layer asLayer() {
    return layer;
  }

  @Override
  public Optional<LayerInfo> getInfoOptional() {
    return Optional.of(info);
  }

  @Override
  public LayerInfo getInfo() {
    return info;
  }

}
