/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.i18n;

import java.util.MissingResourceException;

import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.shared.ExtraSimpleHtmlSanitizer;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;

import nl.aerius.wui.dev.GWTProd;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.i18n.AeriusExceptionMessages;
import nl.overheid.aerius.wui.application.error.ConnectServerException;

public class BaseM {
  private static final AeriusExceptionMessages AERIUS_EXCEPTIONS = GWT.create(AeriusExceptionMessages.class);

  private static BaseMessages baseMessages;

  /**
   * Returns a user friendly readable message of the exception.
   *
   * @param e Exception to format
   * @return user friendly version of the message
   */
  public static String getErrorMessage(final Throwable e) {
    String message = "";
    if (e == null) {
      // do nothing, just return empty string.
    } else if (e instanceof AeriusException) {
      message = getServerExceptionMessage((AeriusException) e);
    } else if (e instanceof ConnectServerException) {
      message = e.getMessage();
    } else {
      if (GWTProd.isDev()) {
        message = e.getMessage();
      } else {
        message = baseMessages.errorInternalFatal();
      }
    }

    return message == null ? "" : ExtraSimpleHtmlSanitizer.sanitizeHtml(message).asString();
  }

  static void init(final BaseMessages baseMessages) {
    BaseM.baseMessages = baseMessages;
  }

  /**
   * Returns the formatted message based on the error code and optional additional arguments in the ServerExeption.
   *
   * @param se ServerException server exception to decode
   * @return Human readable error message based on the error code
   */
  private static String getServerExceptionMessage(final AeriusException se) {
    String messageToDisplay = null;

    try {
      final String format = AERIUS_EXCEPTIONS.getString(se.getReason().getErrorCodeKey());
      final String[] args = se.getArgs() == null ? null : se.getArgs();

      if (args == null) {
        messageToDisplay = format;

      } else {
        messageToDisplay = format(format, args);
      }
    } catch (final MissingResourceException e) {
      final String format = AERIUS_EXCEPTIONS.getString(se.getReason().getErrorCodeKey());
      GWTProd.error("M", "Missing resource for error code: " + format + " > " + se.getReason());
      messageToDisplay = baseMessages.errorInternalFatal();
    }
    return messageToDisplay;
  }

  /**
   * Constructs a String based on a format with arguments. The arguments are code as <code>{0}</code>, where <code>0</code> indicates the index in the
   * variable list of arguments.
   */
  private static String format(final String format, final String... args) {
    final StringBuilder sb = new StringBuilder();
    final int len = format.length();
    int cur = 0;

    while (cur < len) {
      final int fi = format.indexOf('{', cur);

      if (fi == -1) {
        sb.append(format, cur, len);
        break;
      } else {
        sb.append(format, cur, fi);
        final int si = format.indexOf('}', fi);

        if (si == -1) {
          sb.append(format.substring(fi));
          break;
        } else {
          final String nStr = format.substring(fi + 1, si);
          final int i = Integer.parseInt(nStr);

          sb.append(args[i] == null ? "" : SafeHtmlUtils.htmlEscapeAllowEntities(args[i]));
          cur = si + 1;
        }
      }
    }
    return sb.toString();
  }
}
