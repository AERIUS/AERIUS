/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import java.util.Optional;
import java.util.function.Supplier;

import nl.overheid.aerius.wui.application.context.ExportContext.ClientJobType;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;

public class ExportOptionsFacade {
  private ClientJobType jobType = ClientJobType.SOURCES;
  private CalculationJobContext job = null;
  private SituationComposition situationComposition = new SituationComposition();
  private CalculationSetOptions calculationOptions = CalculationSetOptions.create();

  public ClientJobType getJobType() {
    return jobType;
  }

  public void setJobType(final ClientJobType jobType) {
    this.jobType = jobType;
  }

  public CalculationJobContext getJob() {
    return job;
  }

  public void setJob(final CalculationJobContext job) {
    this.job = job;
  }

  public SituationComposition getSituationComposition() {
    return doForSourceTypeOrElse(
        () -> situationComposition,
        () -> Optional.ofNullable(job)
            .map(v -> v.getSituationComposition())
            .orElse(null));
  }

  public void setSituationComposition(final SituationComposition situationComposition) {
    this.situationComposition = situationComposition;
  }

  public CalculationSetOptions getCalculationOptions() {
    return doForSourceTypeOrElse(
        () -> calculationOptions,
        () -> Optional.ofNullable(job)
            .map(v -> v.getCalculationOptions())
            .orElse(null));
  }

  public void setCalculationOptions(final CalculationSetOptions calculationOptions) {
    this.calculationOptions = calculationOptions;
  }

  private <T> T doForSourceTypeOrElse(final Supplier<T> sources, final Supplier<T> orElse) {
    return jobType == ClientJobType.SOURCES ? sources.get() : orElse.get();
  }

  public void removeSourceInputSituation(final SituationContext value) {
    situationComposition.removeSituation(value);
  }
}
