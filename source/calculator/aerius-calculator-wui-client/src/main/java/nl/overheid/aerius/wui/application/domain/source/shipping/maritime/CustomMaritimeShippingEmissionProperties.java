/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.shipping.maritime;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.wui.application.domain.source.EmissionFactors;
import nl.overheid.aerius.wui.vue.ReactivityUtil;
import nl.overheid.aerius.wui.vue.SkipReactivityValidations;

/**
 * Client side implementation of CustomMaritimeShipping props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class CustomMaritimeShippingEmissionProperties {

  private double heatContent;
  private double emissionHeight;
  private EmissionFactors emissionFactors;

  public static final @JsOverlay CustomMaritimeShippingEmissionProperties create() {
    final CustomMaritimeShippingEmissionProperties props = Js.uncheckedCast(JsPropertyMap.of());
    init(props);
    return props;
  }

  public static final @JsOverlay void init(final CustomMaritimeShippingEmissionProperties props) {
    ReactivityUtil.ensureJsPropertyAsNull(props, "heatContent", props::setHeatContent);
    ReactivityUtil.ensureJsPropertyAsNull(props, "emissionHeight", props::setEmissionHeight);
    ReactivityUtil.ensureJsPropertySupplied(props, "emissionFactors", props::setEmissionFactors, EmissionFactors::create);
    ReactivityUtil.ensureInitialized(props::getEmissionFactors, EmissionFactors::init);
  }

  public final @JsOverlay double getHeatContent() {
    return heatContent;
  }

  public final @JsOverlay void setHeatContent(final double heatContent) {
    this.heatContent = heatContent;
  }

  public final @JsOverlay double getEmissionHeight() {
    return emissionHeight;
  }

  public final @JsOverlay void setEmissionHeight(final double emissionHeight) {
    this.emissionHeight = emissionHeight;
  }

  @SkipReactivityValidations
  public final @JsOverlay double getEmissionFactor(final Substance substance) {
    return emissionFactors.getEmissionFactor(substance);
  }

  @SkipReactivityValidations
  public final @JsOverlay void setEmissionFactor(final Substance substance, final double emission) {
    emissionFactors.setEmissionFactor(substance, emission);
  }

  public final @JsOverlay void setEmissionFactors(final EmissionFactors emissionFactors) {
    this.emissionFactors = emissionFactors;
  }

  public final @JsOverlay EmissionFactors getEmissionFactors() {
    return emissionFactors;
  }

}
