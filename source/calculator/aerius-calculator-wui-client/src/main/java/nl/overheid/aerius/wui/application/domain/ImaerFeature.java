/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain;

import static jsinterop.annotations.JsPackage.GLOBAL;

import ol.Feature;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.overheid.aerius.wui.vue.ReactivityUtil;

/**
 * Class for all Imaer based Feature objects.
 */
@JsType(namespace = GLOBAL, name = "ol.Feature", isNative = true)
public abstract class ImaerFeature extends Feature implements ItemWithStringId {
  public static final @JsOverlay void initImaer(final ImaerFeature feature) {
    ReactivityUtil.ensureDefault(feature::getGmlId, feature::setGmlId);
    ReactivityUtil.ensureDefault(feature::getLabel, feature::setLabel);

    // Geometry is held within Feature, but we initialize it here
    ReactivityUtil.ensureDefault(feature::getGeometry, feature::setGeometry);
  }

  public final @JsOverlay String getGmlId() {
    return get("gmlId") == null ? "" : Js.asString(get("gmlId"));
  }

  public final @JsOverlay void setGmlId(final String gmlId) {
    set("gmlId", gmlId);
  }

  public final @JsOverlay String getLabel() {
    return get("label") == null ? "" : Js.asString(get("label"));
  }

  public final @JsOverlay void setLabel(final String label) {
    set("label", label);
  }
}
