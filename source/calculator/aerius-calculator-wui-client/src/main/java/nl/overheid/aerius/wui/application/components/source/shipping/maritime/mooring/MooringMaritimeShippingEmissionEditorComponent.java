/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.source.shipping.maritime.mooring;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.wui.application.components.modify.ModifyListComponent;
import nl.overheid.aerius.wui.application.components.source.SubSourceEditorComponent;
import nl.overheid.aerius.wui.application.components.source.SubSourceValidationBehaviour;
import nl.overheid.aerius.wui.application.components.source.shipping.ShippingEmissionEditorRowComponent;
import nl.overheid.aerius.wui.application.components.source.validation.SubSourceEmptyError;
import nl.overheid.aerius.wui.application.components.source.validation.SubSourceValidatedRowComponent;
import nl.overheid.aerius.wui.application.domain.source.MooringMaritimeShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.shipping.maritime.MooringMaritimeShipping;

@Component(name = "aer-mooring-maritime-shipping-emission-editor", components = {
    ModifyListComponent.class,
    ShippingEmissionEditorRowComponent.class,
    SubSourceValidationBehaviour.class,
    SubSourceValidatedRowComponent.class,
    VerticalCollapseGroup.class,
    SubSourceEmptyError.class,
})
public class MooringMaritimeShippingEmissionEditorComponent extends SubSourceEditorComponent implements HasCreated {
  @Prop MooringMaritimeShippingESFeature source;
  @Data MooringMaritimeShippingEmissionEditorPresenter presenter;

  @Computed("shippingSource")
  protected MooringMaritimeShippingESFeature getShippingSource() {
    return source;
  }

  @JsMethod
  protected void selectSource(final Number index, final MooringMaritimeShipping selectedSource) {
    if (selectedIndex.equals(index.intValue())) {
      selectedIndex = -1;
      resetSelected();
    } else {
      selectedIndex = index.intValue();
      presenter.selectedSource(selectedSource);
      editSource();
    }
  }

  @Override
  public void created() {
    presenter = new MooringMaritimeShippingEmissionEditorPresenter();
    presenter.setView(this);
  }
}
