/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.calculation;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.geo.command.LayerHiddenCommand;
import nl.aerius.geo.command.LayerVisibleCommand;
import nl.aerius.geo.domain.IsLayer;
import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.place.PlaceController;
import nl.overheid.aerius.shared.domain.summary.OverlappingHexagonType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.command.result.SelectResultMainTabCommand;
import nl.overheid.aerius.wui.application.context.result.ResultViewContext;
import nl.overheid.aerius.wui.application.daemon.geo.ResultLayerContext;
import nl.overheid.aerius.wui.application.daemon.misc.PersistablePreferencesContext;
import nl.overheid.aerius.wui.application.event.CalculationStartEvent;
import nl.overheid.aerius.wui.application.event.ResultsViewSelectionCommand;
import nl.overheid.aerius.wui.application.event.ResultsViewSelectionEvent;
import nl.overheid.aerius.wui.application.event.SituationResultsSelectedEvent;
import nl.overheid.aerius.wui.application.place.PrintPlace;
import nl.overheid.aerius.wui.application.place.ResultsPlace;

/**
 * This daemon is responsible for managing visibility of layers in conjunction with selections made within the result component. The point of this is
 * to centralize this kind of logic in a way where it can be made configurable, or is easily hard-coded in one spot.
 */
@Singleton
public class ResultLayerDaemon extends BasicEventComponent {
  private static final ResultLayerDaemonEventBinder EVENT_BINDER = GWT.create(ResultLayerDaemonEventBinder.class);

  interface ResultLayerDaemonEventBinder extends EventBinder<ResultLayerDaemon> {
  }

  private static final Set<ScenarioResultType> PCL_SUPPORTED_RESULT_TYPES = new HashSet<>(Arrays.asList(
      ScenarioResultType.PROJECT_CALCULATION, ScenarioResultType.IN_COMBINATION, ScenarioResultType.ARCHIVE_CONTRIBUTION));

  @Inject private PersistablePreferencesContext preferencesContext;
  @Inject private ResultViewContext viewContext;
  @Inject private ResultLayerContext layerContext;
  @Inject private PlaceController placeController;

  private SummaryHexagonType hexagonType = SummaryHexagonType.EXCEEDING_HEXAGONS;
  private ScenarioResultType resultType = ScenarioResultType.SITUATION_RESULT;
  private OverlappingHexagonType overlappingHexagonType = OverlappingHexagonType.ALL_HEXAGONS;

  @EventHandler
  public void onSelectResultMainTabCommand(SelectResultMainTabCommand c) {
    viewContext.setSelectedMainTab(c.getValue());
  }

  @EventHandler
  public void onCalculationStartEvent(final CalculationStartEvent e) {
    updateResultLayers();
  }

  @EventHandler
  public void onResultsViewTabSelectionCommand(final ResultsViewSelectionCommand c) {
    viewContext.setResultViewTab(c.getValue());
  }

  @EventHandler
  public void onRefreshResultsViewCommand(final RefreshResultsViewCommand c) {
    updateResultLayers();
  }

  @EventHandler
  public void onResultsViewTabSelectionEvent(final ResultsViewSelectionEvent e) {
    updateResultLayers();
  }

  @EventHandler
  public void onSituationResultsSelectedEvent(final SituationResultsSelectedEvent e) {
    hexagonType = e.getValue().getHexagonType();
    resultType = e.getValue().getResultType();
    overlappingHexagonType = e.getValue().getOverlappingHexagonType();
    updateResultLayers();
  }

  private void updateResultLayers() {
    if (!preferencesContext.isLayerAutoSwitch()) {
      return;
    }

    if (!(placeController.getPlace() instanceof ResultsPlace
        || placeController.getPlace() instanceof PrintPlace)) {
      return;
    }

    if (hexagonType == SummaryHexagonType.CUSTOM_CALCULATION_POINTS
        || overlappingHexagonType == OverlappingHexagonType.NON_OVERLAPPING_HEXAGONS_ONLY) {
      switchToVisible(layerContext.getResultLayerGroup());
      return;
    }

    switch (viewContext.getResultViewTab()) {
    case PERCENTAGE_CRITICAL_LEVELS:
      if (PCL_SUPPORTED_RESULT_TYPES.contains(resultType)) {
        switchToVisible(
            layerContext.getPercentageCriticalLoadLayer(),
            layerContext.getTotalPercentageCriticalLoadLayer());
      } else {
        switchToVisible();
      }
      break;
    case DEPOSITION_DISTRIBUTION:
      switchToVisible(layerContext.getResultLayerGroup());
      break;
    case MARKERS:
    case PROCUREMENT_POLICY_THRESHOLD:
      switchToVisible(layerContext.getMarkersLayer());
      break;
    case HABITAT_TYPES:
      switchToVisible(layerContext.getHabitatLayer());
      break;
    case EXTRA_ASSESSMENT_HABITAT_TYPES:
      switchToVisible(layerContext.getExtraAssessmentHexagonsLayer());
      break;
    default:
      GWTProd.error("Unknown result view type handled as default: " + viewContext.getResultViewTab());
      switchToVisible(layerContext.getResultLayerGroup());
      break;
    }
  }

  /**
   * For each result layer, make the ones in the argument visible, all others invisible
   */
  private void switchToVisible(final IsLayer<?>... layers) {
    final List<IsLayer<?>> visibles = Stream.of(layers).collect(Collectors.toList());

    layerContext.getResultLayers().stream().forEach(v -> {
      eventBus.fireEvent(visibles.contains(v) ? new LayerVisibleCommand(v) : new LayerHiddenCommand(v));
    });
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
