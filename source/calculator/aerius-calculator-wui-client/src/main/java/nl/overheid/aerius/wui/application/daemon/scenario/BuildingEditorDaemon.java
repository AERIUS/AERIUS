/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.scenario;

import java.util.List;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.geom.Circle;
import ol.geom.Geometry;
import ol.geom.Point;

import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.place.PlaceController;
import nl.overheid.aerius.shared.domain.context.InputTypeViewMode;
import nl.overheid.aerius.shared.util.ConsecutiveIdUtil;
import nl.overheid.aerius.wui.application.command.building.BuildingAddCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingCreateNewCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingEditCancelCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingEditCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingEditSaveCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingFeatureSelectCommand;
import nl.overheid.aerius.wui.application.command.building.TemporaryBuildingAddCommand;
import nl.overheid.aerius.wui.application.command.building.TemporaryBuildingClearCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.BuildingEditorContext;
import nl.overheid.aerius.wui.application.context.EmissionSourceEditorContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.domain.ClientConsecutiveIdUtil;
import nl.overheid.aerius.wui.application.domain.geo.FeatureUtil;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.event.building.BuildingAddedEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingCreateNewEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingDrawGeometryEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingEditCancelEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingEditEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingEditSaveEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingUpdatedEvent;
import nl.overheid.aerius.wui.application.geo.util.GeoType;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.place.BuildingPlace;
import nl.overheid.aerius.wui.application.place.EmissionSourcePlace;
import nl.overheid.aerius.wui.application.place.ScenarioInputListPlace;
import nl.overheid.aerius.wui.application.util.NumberUtil;

public class BuildingEditorDaemon extends BasicEventComponent {
  private static final BuildingEditorDaemonEventBinder EVENT_BINDER = GWT.create(BuildingEditorDaemonEventBinder.class);

  interface BuildingEditorDaemonEventBinder extends EventBinder<BuildingEditorDaemon> {}

  @Inject private PlaceController placeController;

  @Inject private EmissionSourceEditorContext sourceEditorContext;
  @Inject private BuildingEditorContext context;
  @Inject private ScenarioContext scenarioContext;
  @Inject private ApplicationContext applicationContext;

  private BuildingFeature originalBuilding;

  @EventHandler
  public void onBuildingCreateNewCommand(final BuildingCreateNewCommand c) {
    final BuildingFeature building = BuildingFeature
        .create(applicationContext.getConfiguration().getBuildingLimits().buildingHeightDefault());

    final List<BuildingFeature> buildings = scenarioContext.getActiveSituation().getBuildings();
    final String nextLabel = ClientConsecutiveIdUtil.getNextLabel(buildings);
    building.setLabel(M.messages().buildingLabelDefaultPrefix(nextLabel));

    final ConsecutiveIdUtil.IndexIdPair newId = ClientConsecutiveIdUtil.getConsecutiveIdUtil(buildings).generate();
    building.setId(newId.getId());

    beginBuildingEdit(building);
  }

  @EventHandler
  public void onBuildingEditCommand(final BuildingEditCommand c) {
    final BuildingFeature clone = FeatureUtil.clone(c.getValue());

    beginBuildingEdit(clone, c.getValue());
  }

  @EventHandler
  public void onBuildingEditEvent(final BuildingEditEvent e) {
    final BuildingPlace place = new BuildingPlace(applicationContext.getTheme());
    place.setBuildingId(e.getValue().getGmlId());
    placeController.goTo(place);
  }

  @EventHandler
  public void onBuildingCreateNewEvent(final BuildingCreateNewEvent e) {
    placeController.goTo(new BuildingPlace(applicationContext.getTheme()));
  }

  @EventHandler
  public void onBuildingDrawGeometryEvent(final BuildingDrawGeometryEvent e) {
    if (!context.isEditing()) {
      return;
    }

    final BuildingFeature building = context.getBuilding();
    final Geometry geometry = e.getValue();
    if (GeoType.CIRCLE.is(geometry)) {
      building.setGeometry(new Point(((Circle) geometry).getCenter()));
      building.setDiameter(NumberUtil.round(((Circle) geometry).getRadius() * 2.0,
          applicationContext.getConfiguration().getBuildingLimits().buildingDigitsPrecision()));
    } else {
      building.setGeometry(geometry);
    }
    eventBus.fireEvent(new BuildingUpdatedEvent(building, scenarioContext.getActiveSituation()));
  }

  @EventHandler
  public void onBuildingEditSaveCommand(final BuildingEditSaveCommand c) {
    if (originalBuilding == null) {
      eventBus.fireEvent(new BuildingAddCommand(context.getBuilding()));
      // silence the event because for new buildings the BuildingAddCommand will set the gmlId of the building.
      c.silence();
    } else {
      FeatureUtil.copy(context.getBuilding(), originalBuilding);
      eventBus.fireEvent(new BuildingFeatureSelectCommand(originalBuilding, false));
      eventBus.fireEvent(new BuildingUpdatedEvent(originalBuilding, scenarioContext.getActiveSituation()));
      originalBuilding = null;
    }
    finishBuildingEdit();
  }

  @EventHandler
  public void onBuildingAddedEvent(final BuildingAddedEvent c) {
    eventBus.fireEvent(new BuildingFeatureSelectCommand(c.getValue(), false));
    setBuildingIdAndExit(c.getValue().getGmlId());
  }

  @EventHandler
  public void onBuildingEditSaveEvent(final BuildingEditSaveEvent e) {
    setBuildingIdAndExit(e.getValue());
  }

  private void setBuildingIdAndExit(final String buildingGmlId) {
    if (sourceEditorContext.isEditing()) {
      final EmissionSourceFeature source = sourceEditorContext.getSource();

      if (source.getCharacteristicsType() != null) {
        source.getCharacteristics().setBuildingGmlId(buildingGmlId);
      }
    }
    exitBuildingPlace();
  }

  @EventHandler
  public void onBuildingEditCancelCommand(final BuildingEditCancelCommand c) {
    finishBuildingEdit();
  }

  @EventHandler
  public void onBuildingEditCancelEvent(final BuildingEditCancelEvent e) {
    exitBuildingPlace();
  }

  private void exitBuildingPlace() {
    // If a source is being edited, go there
    if (sourceEditorContext.isEditing()) {
      final EmissionSourceFeature source = sourceEditorContext.getSource();
      final EmissionSourcePlace place = new EmissionSourcePlace(applicationContext.getTheme());
      place.setEmissionSourceId(source.getId());
      placeController.goTo(place);
    } else {
      // Else go to the emission source overview
      placeController.goTo(new ScenarioInputListPlace(applicationContext.getTheme(), InputTypeViewMode.BUILDING));
    }
  }

  /**
   * Begin editing a building.
   */
  private void beginBuildingEdit(final BuildingFeature building) {
    beginBuildingEdit(building, null);
  }

  private void beginBuildingEdit(final BuildingFeature building, final BuildingFeature original) {
    if (context.isEditing()) {
      // Ideally we shouldn't be cleaning up (although it can be legitimate), so warn
      // about this occurring
      GWTProd.warn("Clearing temporary building at a point where ideally we shouldn't be doing that.");
      finishBuildingEdit();
    }

    context.setBuilding(building);
    building.setId("-" + building.getId());
    context.setSituation(scenarioContext.getActiveSituation());
    eventBus.fireEvent(new TemporaryBuildingAddCommand(building, context.getSituation()));

    if (original != null) {
      originalBuilding = original;
    } else {
      originalBuilding = null;
    }
  }

  private void finishBuildingEdit() {
    if (context.isEditing()) {
      eventBus.fireEvent(new TemporaryBuildingClearCommand(context.getBuilding(), context.getSituation()));
      context.reset();
    }
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
