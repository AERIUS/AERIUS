/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.building;

import javax.inject.Inject;

import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;

import ol.style.Style;

import nl.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.geo.layers.FeatureStyle;

/**
 * Layer to show the {@link BuildingFeature} geometries from a single situation on the map.
 */
public class SituationBuildingGeometryLayer extends BuildingGeometryLayer {

  @Inject
  public SituationBuildingGeometryLayer(@Assisted final LayerInfo info, final EventBus eventBus, @Assisted final int zIndex) {
    super(info, eventBus, zIndex);
  }

  @Override
  protected Style[] createStyle(final BuildingFeature feature, final double resolution) {
    if (feature.getId().equals(getEditableFeatureId())) {
      return NO_RENDERING;
    }

    return FeatureStyle.DEFAULT_STYLING;
  }
}
