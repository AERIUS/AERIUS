/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.characteristics.adms;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.aerius.vuelidate.ValidationOptions;
import nl.aerius.vuelidate.Validations;
import nl.aerius.vuelidate.ValidatorBuilder;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.ADMSLimits;

/**
 * Validators for {@link ADMSGenericCharacteristicsEditorComponent}.
 */
public class ADMSGenericCharacteristicsEditorValidators extends ValidationOptions<ADMSGenericCharacteristicsEditorComponent> {
  @JsType(isNative = true, namespace = JsPackage.GLOBAL)
  static class ADMSCharacteristicsEditorValidations extends Validations {
    public @JsProperty Validations heightV;
    public @JsProperty Validations widthV;
    public @JsProperty Validations diameterV;
    public @JsProperty Validations verticalDimensionV;
    public @JsProperty Validations densityV;
    public @JsProperty Validations massFluxV;
    public @JsProperty Validations specificHeatCapacityV;
    public @JsProperty Validations temperatureV;
    public @JsProperty Validations verticalVelocityV;
    public @JsProperty Validations volumetricFlowRateV;
    public @JsProperty Validations elevationAngleV;
    public @JsProperty Validations horizontalAngleV;
  }

  @Override
  protected void constructValidators(final Object options, final ValidatorInstaller v) {
    final ADMSGenericCharacteristicsEditorComponent instance = Js.uncheckedCast(options);

    v.install("heightV",
        () -> instance.heightV = null,
        ValidatorBuilder.create()
            .required()
            .decimal()
            .minValue(ADMSLimits.SOURCE_HEIGHT_MINIMUM)
            .maxValue(ADMSLimits.SOURCE_HEIGHT_MAXIMUM));
    v.install("widthV",
        () -> instance.widthV = null,
        ValidatorBuilder.create()
            .required()
            .decimal()
            .minValue(ADMSLimits.SOURCE_WIDTH_MINIMUM)
            .maxValue(ADMSLimits.SOURCE_WIDTH_MAXIMUM));
    v.install("diameterV",
        () -> instance.diameterV = null,
        ValidatorBuilder.create()
            .required()
            .decimal()
            .minValue(ADMSLimits.SOURCE_DIAMETER_MINIMUM)
            .maxValue(ADMSLimits.SOURCE_DIAMETER_MAXIMUM));
    v.install("verticalDimensionV",
        () -> instance.verticalDimensionV = null,
        ValidatorBuilder.create()
            .required()
            .decimal()
            .minValue(ADMSLimits.SOURCE_L1_MINIMUM)
            .maxValue(ADMSLimits.SOURCE_L1_MAXIMUM));
    v.install("temperatureV",
        () -> instance.temperatureV = null,
        ValidatorBuilder.create()
            .required()
            .decimal()
            .minValue(ADMSLimits.SOURCE_TEMPERATURE_MINIMUM)
            .maxValue(ADMSLimits.SOURCE_TEMPERATURE_MAXIMUM));
    v.install("densityV",
        () -> instance.densityV = null,
        ValidatorBuilder.create()
            .required()
            .decimal()
            .minValue(ADMSLimits.SOURCE_DENSITY_MINIMUM)
            .maxValue(ADMSLimits.SOURCE_DENSITY_MAXIMUM));
    v.install("verticalVelocityV",
        () -> instance.verticalVelocityV = null,
        ValidatorBuilder.create()
            .required()
            .decimal()
            .minValue(ADMSLimits.SOURCE_VERTICAL_VELOCITY_MINIMUM)
            .maxValue(ADMSLimits.SOURCE_VERTICAL_VELOCITY_MAXIMUM));
    v.install("volumetricFlowRateV",
        () -> instance.volumetricFlowRateV = null,
        ValidatorBuilder.create()
            .required()
            .decimal()
            .minValue(ADMSLimits.SOURCE_VOLUMETRIC_FLOW_RATE_MINIMUM)
            .maxValue(ADMSLimits.SOURCE_VOLUMETRIC_FLOW_RATE_MAXIMUM));
    v.install("massFluxV",
        () -> instance.massFluxV = null,
        ValidatorBuilder.create()
            .required()
            .decimal()
            .minValue(ADMSLimits.SOURCE_MASS_FLUX_MINIMUM)
            .maxValue(ADMSLimits.SOURCE_MASS_FLUX_MAXIMUM));
    v.install("specificHeatCapacityV",
        () -> instance.specificHeatCapacityV = null,
        ValidatorBuilder.create()
            .required()
            .decimal()
            .minValue(ADMSLimits.SOURCE_SPECIFIC_HEAT_CAPACITY_MINIMUM)
            .maxValue(ADMSLimits.SOURCE_SPECIFIC_HEAT_CAPACITY_MAXIMUM));
    v.install("elevationAngleV",
        () -> instance.elevationAngleV = null,
        ValidatorBuilder.create()
            .required()
            .decimal()
            .minValue(ADMSLimits.ELEVATION_ANGLE_MIN)
            .maxValue(ADMSLimits.ELEVATION_ANGLE_MAX));
    v.install("horizontalAngleV",
        () -> instance.horizontalAngleV = null,
        ValidatorBuilder.create()
            .required()
            .decimal()
            .minValue(ADMSLimits.HORIZONTAL_ANGLE_MIN)
            .maxValue(ADMSLimits.HORIZONTAL_ANGLE_MAX));
  }

}
