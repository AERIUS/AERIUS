/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers;

import com.google.gwt.resources.client.DataResource;

@SuppressWarnings({"rawtypes", "unchecked"})
public class LegendPair implements Comparable<LegendPair> {
  private final DataResource marker;
  private final String text;

  private final Comparable<Comparable<Comparable>> meta;

  public LegendPair(final DataResource marker, final String text, final Comparable meta) {
    this.marker = marker;
    this.text = text;
    this.meta = meta;
  }

  public DataResource getMarker() {
    return marker;
  }

  public String getText() {
    return text;
  }

  public Comparable getMeta() {
    return meta;
  }

  @Override
  public int compareTo(final LegendPair o) {
    return meta.compareTo(o.getMeta());
  }
}
