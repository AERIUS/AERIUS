/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.result;

import java.util.Objects;

import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.summary.OverlappingHexagonType;
import nl.overheid.aerius.shared.domain.summary.ProcurementPolicy;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.context.calculation.SituationHandle;

public class SituationResultsKey {
  private SituationHandle situation = null;
  private ScenarioResultType resultType = null;
  private EmissionResultKey emissionResultKey = null;
  private SummaryHexagonType hexagonType = null;
  private OverlappingHexagonType overlappingHexagonType = null;
  private ProcurementPolicy procurementPolicy = null;

  public SituationResultsKey(final SituationHandle situation, final ScenarioResultType resultType, final EmissionResultKey emissionResultKey,
      final SummaryHexagonType hexagonType, final OverlappingHexagonType overlappingHexagonType, final ProcurementPolicy procurementPolicy) {
    this.situation = situation;
    this.resultType = resultType;
    this.emissionResultKey = emissionResultKey;
    this.hexagonType = hexagonType;
    this.overlappingHexagonType = overlappingHexagonType;
    this.procurementPolicy = procurementPolicy;
  }

  public SituationHandle getSituationHandle() {
    return situation;
  }

  public ScenarioResultType getResultType() {
    return resultType;
  }

  public EmissionResultKey getEmissionResultKey() {
    return emissionResultKey;
  }

  public SummaryHexagonType getHexagonType() {
    return hexagonType;
  }

  public OverlappingHexagonType getOverlappingHexagonType() {
    return overlappingHexagonType;
  }

  public ProcurementPolicy getProcurementPolicy() {
    return procurementPolicy;
  }

  public void setSituationHandle(final SituationHandle situation) {
    this.situation = situation;
  }

  public void setResultType(final ScenarioResultType resultType) {
    this.resultType = resultType;
  }

  public void setEmissionResultKey(final EmissionResultKey emissionResultKey) {
    this.emissionResultKey = emissionResultKey;
  }

  public void setHexagonType(final SummaryHexagonType hexagonType) {
    this.hexagonType = hexagonType;
  }

  public void setOverlappingHexagonType(final OverlappingHexagonType overlappingHexagonType) {
    this.overlappingHexagonType = overlappingHexagonType;
  }

  public void setProcurementPolicy(final ProcurementPolicy procurementPolicy) {
    this.procurementPolicy = procurementPolicy;
  }

  @Override
  public int hashCode() {
    return Objects.hash(situation, resultType, hexagonType, emissionResultKey, overlappingHexagonType, procurementPolicy);
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final SituationResultsKey other = (SituationResultsKey) obj;
    if (hexagonType != other.hexagonType) {
      return false;
    }
    if (overlappingHexagonType != other.overlappingHexagonType) {
      return false;
    }
    if (resultType != other.resultType) {
      return false;
    }
    if (emissionResultKey != other.emissionResultKey) {
      return false;
    }
    if (procurementPolicy != other.procurementPolicy) {
      return false;
    }
    if (situation == null) {
      if (other.situation != null) {
        return false;
      }
    } else if (!situation.equals(other.situation)) {
      return false;
    }
    return true;
  }
}
