/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.misc;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.command.PlaceChangeCommand;
import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.event.PlaceChangeEvent;
import nl.aerius.wui.place.Place;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.place.CalculatorDefaultPlace;
import nl.overheid.aerius.wui.application.place.HomePlace;

@Singleton
public class PlaceContextDaemon extends BasicEventComponent implements Daemon {
  private final PlaceContextDaemonEventBinder EVENT_BINDER = GWT.create(PlaceContextDaemonEventBinder.class);

  interface PlaceContextDaemonEventBinder extends EventBinder<PlaceContextDaemon> {}

  private final List<Runnable> togglers = new ArrayList<>();
  private final List<Consumer<Boolean>> consumers = new ArrayList<>();

  private @Inject ApplicationContext appContext;

  private Place currentPlace;

  /**
   * If there is an active theme, redirect to its launch page
   *
   * @param place change command
   */
  @EventHandler
  public void onPlaceChangeCommand(final PlaceChangeCommand c) {
    final Theme activeTheme = appContext.getTheme();

    if (c.getValue() instanceof CalculatorDefaultPlace && activeTheme != null) {
      c.setRedirect(new HomePlace(activeTheme));
    }
  }

  @EventHandler
  public void onPlaceChangeEvent(final PlaceChangeEvent e) {
    final Place place = e.getValue();

    if (currentPlace != null && !place.getClass().equals(currentPlace.getClass())) {
      //togglers.forEach(Runnable::run);
    } else {
      consumers.forEach(v -> v.accept(true));
    }

    currentPlace = place;
  }

  public void register(final Runnable toggler, final Consumer<Boolean> consumer) {
    togglers.add(toggler);
    consumers.add(consumer);
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
