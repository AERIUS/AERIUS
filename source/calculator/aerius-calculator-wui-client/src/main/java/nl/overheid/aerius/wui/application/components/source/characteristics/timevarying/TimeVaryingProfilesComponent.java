/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.characteristics.timevarying;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.wui.application.components.divider.DividerComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;

/**
 * Shows the 2 time-varying profiles the user can select for an emission source.
 */
@Component(components = {
    DividerComponent.class,
    TimeVaryingProfileSelectorComponent.class
})
public class TimeVaryingProfilesComponent extends ErrorWarningValidator {
  @Prop EventBus eventBus;
  @Prop(required = true) SituationContext situation;
  @Prop(required = true) EmissionSourceFeature source;
}
