/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.results.summary;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings.DepositionValueDisplayType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationHandle;
import nl.overheid.aerius.wui.application.domain.summary.JobSummarySituationRecord;
import nl.overheid.aerius.wui.application.util.NumberUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    VerticalCollapseGroup.class,
    SortIndicatorComponent.class,
})
public class SituationSummaryTable extends BasicVueComponent implements HasCreated {
  public enum Column {
    SITE, SITUATION, TYPE;
  }

  @Inject @Data ApplicationContext applicationContext;
  @Inject @Data ScenarioContext scenarioContext;

  @Prop(required = true) CalculationExecutionContext jobContext;
  @Prop @JsProperty JobSummarySituationRecord[] situations;

  @Data Comparator<JobSummarySituationRecord> comparator;
  @Data Object sortType;
  @Data boolean asc;

  @Override
  public void created() {
    sort(v -> getSiteName(v), Column.SITE);
  }

  @Computed
  public int getResultColumnSize() {
    return getSupportedSummaryTypes().size();
  }

  @Computed
  public List<EmissionResultKey> getSupportedSummaryTypes() {
    return applicationContext.getConfiguration().getSupportedSummaryResultTypes();
  }

  @Computed
  public DepositionValueDisplayType getDisplayType() {
    return applicationContext.getConfiguration().getEmissionResultValueDisplaySettings().getDisplayType();
  }

  @JsMethod
  public String getSiteName(final JobSummarySituationRecord situationRecord) {
    final String siteName = situationRecord.getAssessmentArea().getName();
    return siteName == null ? "" : siteName;
  }

  @JsMethod
  public boolean isActive(final Object column) {
    return column == sortType;
  }

  @Computed
  public List<JobSummarySituationRecord> getSituationsData() {
    if (situations == null) {
      return new ArrayList<>();
    }

    return Stream.of(situations)
        .sorted(comparator)
        .collect(Collectors.toList());
  }

  @JsMethod
  public String getSituationName(final JobSummarySituationRecord situationRecord) {
    final SituationHandle situationHandle = getSituation(situationRecord.getCalculationReference());
    if (situationHandle == null) {
      return "";
    }

    return situationHandle.getName();
  }

  @JsMethod
  public String getSituationType(final JobSummarySituationRecord situationRecord) {
    final SituationHandle situationHandle = getSituation(situationRecord.getCalculationReference());
    if (situationHandle == null) {
      return "";
    }

    return i18n.situationType(situationHandle.getType());
  }

  @JsMethod
  public void sort(final Function<JobSummarySituationRecord, String> func, final Object key) {
    if (this.sortType == key) {
      this.asc = !this.asc;
    } else {
      this.sortType = key;
      this.asc = true;
    }

    comparator = (o1, o2) -> (asc ? 1 : -1) * func.apply(o1).compareTo(func.apply(o2));
  }

  @JsMethod
  public void sortNumber(final Function<JobSummarySituationRecord, Number> func, final Object key) {
    if (this.sortType == key) {
      this.asc = !this.asc;
    }
    this.sortType = key;

    comparator = NumberUtil.sortComparator(func, asc);
  }

  private SituationHandle getSituation(final String calculationReference) {
    return CalculationExecutionContext.findHandleFromId(jobContext, calculationReference);
  }

  @JsMethod
  public double getResultNum(final JobSummarySituationRecord situationRecord, final EmissionResultKey substance) {
    return situationRecord.getMaxContributions(substance);
  }

  @JsMethod
  public String getResult(final JobSummarySituationRecord situationRecord, final EmissionResultKey substance) {
    return i18n.decimalNumberCapped(getResultNum(situationRecord, substance), 2);
  }
}
