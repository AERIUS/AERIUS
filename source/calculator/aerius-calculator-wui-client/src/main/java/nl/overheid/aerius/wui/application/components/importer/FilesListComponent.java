/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.importer;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Emit;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.google.web.bindery.event.shared.EventBus;

import elemental2.dom.File;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.wui.application.components.importer.domain.ImportAction.ActionType;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;

@Component(components = {
    ValidationBehaviour.class,
    SimpleFileComponent.class,
    AdvancedFileComponent.class
})
public class FilesListComponent extends ErrorWarningValidator {
  // Note: see MultipleFileContentsComponent for a dependence on this sort order
  private static final Comparator<FileUploadStatus> deprioritizingComparator = (o1, o2) -> {
    return Boolean.compare(isAddCalculationPoints(o1), isAddCalculationPoints(o2));
  };

  private static boolean isAddCalculationPoints(final FileUploadStatus fus) {
    return Optional.ofNullable(fus.getImportAction())
        .map(v -> v.getType())
        .orElse(null) == ActionType.ADD_CALCULATION_POINTS;
  }

  @Prop EventBus eventBus;
  @Prop @JsProperty List<FileUploadStatus> files;

  @Prop boolean advanced;

  @Watch("files")
  public void onFilesChange() {
    scrubErrors(files.stream()
        .map(v -> v.getKey())
        .collect(Collectors.toList()));
  }

  @Computed
  public List<FileUploadStatus> getSortedFiles() {
    return files.stream()
        .sorted(deprioritizingComparator)
        .collect(Collectors.toList());
  }

  @Computed
  public Set<Entry<File, List<FileUploadStatus>>> getFileGroups() {
    final Set<Entry<File, List<FileUploadStatus>>> fileGroups = files.stream()
        .collect(Collectors.groupingBy(v -> v.getFile(), () -> new LinkedHashMap<>(), Collectors.toList()))
        .entrySet();

    fileGroups.forEach(e -> e.getValue().sort(deprioritizingComparator));

    return fileGroups;
  }

  @Emit
  @JsMethod
  public void displayErrors() {}

  @Emit
  @JsMethod
  public void displayWarnings() {}

  @JsMethod
  public void trackChildErrors(final List<FileUploadStatus> files, final boolean flag) {
    files.forEach(v -> trackChildError(v.getKey(), flag));
  }

  @JsMethod
  public void trackChildWarnings(final List<FileUploadStatus> files, final boolean flag) {
    files.forEach(v -> trackChildWarning(v.getKey(), flag));
  }

  @JsMethod
  public void removeFile(final FileUploadStatus file) {
    vue().$emit("remove-file", file);
  }
}
