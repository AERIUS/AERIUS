/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.source.farmlodging;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.base.Js;

import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategories;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.wui.application.components.modify.ModifyListComponent;
import nl.overheid.aerius.wui.application.components.source.SubSourceEditorComponent;
import nl.overheid.aerius.wui.application.components.source.SubSourceValidationBehaviour;
import nl.overheid.aerius.wui.application.components.source.validation.SubSourceEmptyError;
import nl.overheid.aerius.wui.application.components.source.validation.SubSourceValidatedRowComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.FarmLodgingESFeature;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmLodging;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmLodgingType;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmLodgingUtil;
import nl.overheid.aerius.wui.application.domain.source.farm.StandardFarmLodging;

@Component(components = {
    ModifyListComponent.class,
    SubSourceValidationBehaviour.class,
    FarmLodgingEmissionEditorRowComponent.class,
    SubSourceValidatedRowComponent.class,
    SubSourceEmptyError.class,
    VerticalCollapseGroup.class,
})
public class FarmLodgingEmissionEditorComponent extends SubSourceEditorComponent implements HasCreated {
  @Prop FarmLodgingESFeature source;
  @Data FarmLodgingEmissionEditorActivity presenter;

  @Inject @Data ApplicationContext applicationContext;

  @Computed("farmSource")
  public FarmLodgingESFeature getFarmSource() {
    return source;
  }

  @JsMethod
  public boolean hasAnyIncompatibleCombinations() {
    return source.getSubSources().asList().stream()
        .anyMatch(v -> hasIncompatibleCombinations(v));
  }

  @JsMethod
  public boolean hasIncompatibleCombinations(final FarmLodging source) {
    if (FarmLodgingType.STANDARD != source.getFarmLodgingType()) {
      return false;
    }

    final StandardFarmLodging standardFarmLodging = Js.uncheckedCast(source);
    final FarmLodgingCategory farmLodgingCategory = getCategories().determineFarmLodgingCategoryByCode(standardFarmLodging.getFarmLodgingCode());
    if (farmLodgingCategory == null) {
      // This is a null-check more than anything else - if there is no selected
      // lodging category, return false (i.e. the source is invalid, and we can't
      // determine if there are incompatible combinations, but we don't want a warning
      // sign to show up to reflect that, and will assume an error sign will show up
      // elsewhere)
      return false;
    } else {
      return FarmLodgingUtil.hasIncompatibleCombinations(standardFarmLodging, farmLodgingCategory,
          applicationContext.getConfiguration().getSectorCategories().getFarmLodgingCategories());
    }
  }

  @JsMethod
  public FarmLodgingCategories getCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getFarmLodgingCategories();
  }

  public boolean hasWarnings(final FarmLodging source, final int index) {
    return hasIncompatibleCombinations(source)
        || super.hasWarnings(index);
  }

  @JsMethod
  public void selectSource(final Number index, final FarmLodging selectedSource) {
    if (selectedIndex.equals(index.intValue())) {
      selectedIndex = -1;
      resetSelected();
    } else {
      selectedIndex = index.intValue();
      presenter.selectedSource(selectedSource);
      editSource();
    }
  }

  @Override
  public void created() {
    presenter = new FarmLodgingEmissionEditorActivity();
    presenter.setView(this);
  }
}
