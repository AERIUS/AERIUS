/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.summary;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.application.domain.info.AssessmentArea;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsStatistics;

@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class JobSummaryProjectRecord {
  private AssessmentArea assessmentArea;
  private String calculationReference;
  private String emissionResultKey;
  private SituationResultsStatistics maxValues;

  public final @JsOverlay AssessmentArea getAssessmentArea() {
    return assessmentArea;
  }

  public final @JsOverlay String getCalculationReference() {
    return calculationReference;
  }

  public final @JsOverlay EmissionResultKey getEmissionResultKey() {
    return EmissionResultKey.valueOf(emissionResultKey);
  }

  public final @JsOverlay SituationResultsStatistics getMaxValues() {
    return maxValues;
  }
}
