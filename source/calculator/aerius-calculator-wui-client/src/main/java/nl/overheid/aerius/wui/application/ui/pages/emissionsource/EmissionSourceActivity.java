/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.ui.pages.emissionsource;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;
import com.google.web.bindery.event.shared.binder.GenericEvent;

import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.wui.application.command.building.BuildingCreateNewCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingEditCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceCreateNewCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditCancelCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditSaveCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileCreateNewCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileEditCommand;
import nl.overheid.aerius.wui.application.components.map.MapComponentFactory;
import nl.overheid.aerius.wui.application.context.EmissionSourceEditorContext;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.ui.main.MainView.FlexView;
import nl.overheid.aerius.wui.application.ui.pages.ThemeDelegatedActivity;
import nl.overheid.aerius.wui.application.ui.pages.ThemeView;

/**
 * Activity for editing a single emission source object.
 */
public class EmissionSourceActivity extends BasicEventComponent implements ThemeDelegatedActivity, EmissionSourcePresenter {
  private final EmissionSourceActivityEventBinder EVENT_BINDER = GWT.create(EmissionSourceActivityEventBinder.class);

  interface EmissionSourceActivityEventBinder extends EventBinder<EmissionSourceActivity> {}

  @Inject EmissionSourceEditorContext editorContext;

  private boolean userCancelled;
  private boolean cancelEdit;

  private HandlerRegistration handlers;

  @Override
  public void onStart(final ThemeView view) {
    if (!editorContext.isEditing()) {
      final EmissionSourceCreateNewCommand cmd = new EmissionSourceCreateNewCommand();
      cmd.silence();
      eventBus.fireEvent(cmd);
    }

    view.setDelegatedPresenter(this);
    view.setLeftView(EmissionSourceViewFactory.get(), FlexView.RIGHT);
    view.setSoftMiddle(false);
    view.setMiddleView(EmissionSourceDetailViewFactory.get());
    view.setRightView(MapComponentFactory.get());
  }

  @EventHandler(handles = {EmissionSourceEditSaveCommand.class, EmissionSourceEditCancelCommand.class})
  public void onEmissionSourceEnd(final GenericEvent c) {
    userCancelled = true;
    cancelEdit = true;
  }

  @EventHandler(handles = {BuildingEditCommand.class, BuildingCreateNewCommand.class})
  public void onBuildingExit(final GenericEvent c) {
    userCancelled = true;
    cancelEdit = false;
  }

  @EventHandler(handles = {TimeVaryingProfileEditCommand.class, TimeVaryingProfileCreateNewCommand.class})
  public void onTimeVaryingProfileExit(final GenericEvent c) {
    userCancelled = true;
    cancelEdit = false;
  }

  @Override
  public String mayStop() {
    final boolean cancelled = userCancelled;
    userCancelled = false;
    return cancelled ? null : M.messages().esMayStop();
  }

  @Override
  public void onStop() {
    if (cancelEdit) {
      final EmissionSourceEditCancelCommand cmd = new EmissionSourceEditCancelCommand();
      cmd.silence();
      eventBus.fireEvent(cmd);
    }

    handlers.removeHandler();
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    handlers = super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
