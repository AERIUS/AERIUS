/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.farmland;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.google.web.bindery.event.shared.EventBus;

import elemental2.core.JsArray;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.FarmSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmlandCategory;
import nl.overheid.aerius.wui.application.components.input.MinimalInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.source.farmland.FarmlandDetailValidators.FarmlandDetailValidations;
import nl.overheid.aerius.wui.application.components.source.farmland.custom.FarmlandCustomEmissionComponent;
import nl.overheid.aerius.wui.application.components.source.farmland.standard.FarmlandStandardEmissionComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.FarmlandESFeature;
import nl.overheid.aerius.wui.application.domain.source.farmland.FarmlandActivity;
import nl.overheid.aerius.wui.application.domain.source.farmland.FarmlandActivityType;
import nl.overheid.aerius.wui.application.domain.source.farmland.FarmlandUtil;
import nl.overheid.aerius.wui.application.domain.source.util.EmissionSourceFeatureUtil;
import nl.overheid.aerius.wui.vue.DebugDirective;

@Component(customizeOptions = FarmlandDetailValidators.class, components = {
    MinimalInputComponent.class,
    FarmlandCustomEmissionComponent.class,
    FarmlandStandardEmissionComponent.class,
    SimplifiedListBoxComponent.class,
    ValidationBehaviour.class,
}, directives = {
    DebugDirective.class
})
public class FarmlandDetailEditorComponent extends ErrorWarningValidator implements HasValidators<FarmlandDetailValidations>, HasCreated {
  @Prop FarmlandESFeature source;
  @Prop(required = true) FarmlandActivity subSource;

  @Prop EventBus eventBus;

  @Data @Inject ApplicationContext applicationContext;

  @JsProperty(name = "$v") FarmlandDetailValidations validations;

  @Data FarmlandCategory selectedFarmlandCategory;

  @Data @JsProperty List<FarmSourceCategory> farmSourceCategories = new ArrayList<FarmSourceCategory>();

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Watch(value = "subSource", isImmediate = true)
  public void onSubSourceChange(final FarmlandActivity neww) {
    if (neww == null) {
      return;
    }
    selectedFarmlandCategory = FarmlandUtil.findFarmland(neww.getActivityCode(),
        applicationContext.getConfiguration().getSectorCategories().getFarmlandCategories());
    if (selectedFarmlandCategory != null) {
      farmSourceCategories = getCategories(selectedFarmlandCategory.getSectorCode());
    }
  }

  @Override
  @Computed
  public FarmlandDetailValidations getV() {
    return validations;
  }

  @Computed("substances")
  public List<Substance> getSubstances() {
    return applicationContext.getConfiguration().getSubstances();
  }

  @Computed("farmlandCategories")
  public Object[] getFarmlandCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getFarmlandCategories().toArray();
  }

  @JsMethod
  public boolean isSelected(final FarmlandActivityType farmlandActivityType) {
    return selectedFarmlandCategory != null && subSource.getActivityType() == farmlandActivityType;
  }

  @JsMethod
  public void selectFarmland(final FarmlandCategory option) {
    selectedFarmlandCategory = option;
    farmSourceCategories = getCategories(option.getSectorCode());

    final JsArray<FarmlandActivity> farmlandActivity = source.getSubSources();
    farmlandActivity.splice(farmlandActivity.indexOf(subSource), 1,
        EmissionSourceFeatureUtil.createFarmlandActivityType(!farmSourceCategories.isEmpty(), option.getCode()));
  }

  @JsMethod
  public boolean hasActivity() {
    return subSource != null;
  }

  @Computed("entryKey")
  public Function<Object, Object> entryKey(final Object obj) {
    return v -> v instanceof FarmlandCategory ? ((FarmlandCategory) v).getCode() : v;
  }

  private List<FarmSourceCategory> getCategories(final int sectorCode) {
    return applicationContext.getConfiguration().getSectorCategories().getFarmSourceCategories(sectorCode);
  }

}
