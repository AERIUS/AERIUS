/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.development;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.Coordinate;

import elemental2.dom.DomGlobal;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.place.PlaceController;
import nl.aerius.wui.util.NotificationUtil;
import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.shared.domain.PdfProductType;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.backgrounddata.MetSite;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.context.InputTypeViewMode;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfileType;
import nl.overheid.aerius.wui.application.command.building.BuildingAddCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobAddCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobStartCommand;
import nl.overheid.aerius.wui.application.command.exporter.ExportPrepareCommand;
import nl.overheid.aerius.wui.application.command.exporter.ExportStartCommand;
import nl.overheid.aerius.wui.application.command.situation.CreateEmptySituationCommand;
import nl.overheid.aerius.wui.application.command.situation.SituationChangeTypeCommand;
import nl.overheid.aerius.wui.application.command.source.DeleteAllEmissionSourceCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceAddCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileAddCommand;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ExportContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationPreparationContext;
import nl.overheid.aerius.wui.application.daemon.flags.LocalStorageUtil;
import nl.overheid.aerius.wui.application.domain.calculation.ADMSOptions;
import nl.overheid.aerius.wui.application.domain.export.ExportOptions;
import nl.overheid.aerius.wui.application.domain.export.HistoryRecord;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.TimeVaryingProfile;
import nl.overheid.aerius.wui.application.domain.source.util.TimeVaryingProfileUtil;
import nl.overheid.aerius.wui.application.event.ApplicationFinishedLoadingEvent;
import nl.overheid.aerius.wui.application.event.ExportCalculationCompleteEvent;
import nl.overheid.aerius.wui.application.event.ExportStartEvent;
import nl.overheid.aerius.wui.application.place.CalculatePlace;
import nl.overheid.aerius.wui.application.place.ResultsPlace;
import nl.overheid.aerius.wui.application.place.ScenarioInputListPlace;
import nl.overheid.aerius.wui.config.ApplicationFlags;

@Component(components = {
    SimplifiedListBoxComponent.class,
})
public class DeveloperPanel implements IsVueComponent, HasCreated {
  private static final DeveloperPanelEventBinder EVENT_BINDER = GWT.create(DeveloperPanelEventBinder.class);

  private static final String DEV_PANEL_SECTOR_KEY = "dev-panel-sector";
  private static final String DEV_PANEL_EMAIL_KEY = "dev-panel-email";

  interface DeveloperPanelEventBinder extends EventBinder<DeveloperPanel> {}

  @Inject @Data ApplicationFlags flags;

  @Inject @Data ExportContext exportContext;
  @Inject @Data ScenarioContext scenarioContext;
  @Inject @Data ApplicationContext applicationContext;
  @Inject @Data CalculationContext calculationContext;
  @Inject @Data CalculationPreparationContext preparationContext;

  @Inject PlaceController placeController;

  @Prop EventBus eventBus;

  @Data Sector sector;

  @Data String email;

  private boolean printRedirect = false;

  @Override
  public void created() {
    EVENT_BINDER.bindEventHandlers(this, eventBus);

    LocalStorageUtil.tryGet(DEV_PANEL_EMAIL_KEY).ifPresent(v -> email = v);
  }

  @Watch("email")
  public void onEmailChange() {
    LocalStorageUtil.trySet(DEV_PANEL_EMAIL_KEY, email);
  }

  @EventHandler
  public void onApplicationFinishedLoadingEvent(final ApplicationFinishedLoadingEvent e) {
    DummyEmissionSources.init(applicationContext);
    updateFromStorage();
  }

  private void updateFromStorage() {
    sector = LocalStorageUtil.tryGet(DEV_PANEL_SECTOR_KEY)
        .map(v -> Integer.parseInt(v))
        .map(v -> applicationContext.getConfiguration().getSectorCategories().getSectors().stream()
            .filter(sector -> sector.getSectorId() == v)
            .findFirst().orElse(null))
        .orElse(applicationContext.getConfiguration().getSectorCategories().getSectors().stream()
            .filter(v -> v.getSectorGroup() == SectorGroup.AGRICULTURE)
            .findFirst().orElse(null));
  }

  @JsMethod
  public void createMockSituation() {
    eventBus.fireEvent(new CreateEmptySituationCommand(true));
    placeController.goTo(new ScenarioInputListPlace(applicationContext.getTheme()));
  }

  @JsMethod
  public void goToFarmEditor() {
    if (!scenarioContext.hasActiveSituation()) {
      eventBus.fireEvent(new CreateEmptySituationCommand(true));
    }

    final SituationContext presentSit = scenarioContext.getActiveSituation();

    final EmissionSourceFeature src = DummyEmissionSources.createAgriSource(scenarioContext.getActiveSituation().getSourcesSize(),
        sector, getCharacteristicsType());
    eventBus.fireEvent(new EmissionSourceAddCommand(src, presentSit));
    eventBus.fireEvent(new EmissionSourceEditCommand(src));
  }

  @JsMethod
  public void goToColdStartEditor() {
    if (!scenarioContext.hasActiveSituation()) {
      eventBus.fireEvent(new CreateEmptySituationCommand(true));
    }

    final SituationContext presentSit = scenarioContext.getActiveSituation();
    final EmissionSourceFeature src = DummyEmissionSources.createColdStartSource(scenarioContext.getActiveSituation().getSourcesSize(),
        applicationContext.getConfiguration().getSectorCategories().getSectors().stream().map(v -> v.getSectorId()).collect(Collectors.toList()),
        getCharacteristicsType());
    eventBus.fireEvent(new EmissionSourceAddCommand(src, presentSit));
    eventBus.fireEvent(new EmissionSourceEditCommand(src));
  }

  @Computed
  public List<Sector> getFarmSectors() {
    return applicationContext.getConfiguration().getSectorCategories().getSectors().stream()
        .filter(v -> v.getSectorGroup() == SectorGroup.AGRICULTURE)
        .collect(Collectors.toList());
  }

  @JsMethod
  public void selectSector(final Sector sector) {
    this.sector = sector;

    LocalStorageUtil.trySet(DEV_PANEL_SECTOR_KEY, sector.getSectorId());
  }

  @JsMethod
  public void createTimeVaryingProfile() {
    addTimeVaryingProfiles();
    placeController.goTo(new ScenarioInputListPlace(applicationContext.getTheme(), InputTypeViewMode.TIME_VARYING_PROFILES));
  }

  private void addTimeVaryingProfiles() {
    if (!scenarioContext.hasActiveSituation()) {
      eventBus.fireEvent(new CreateEmptySituationCommand(true));
    }

    final SituationContext presentSit = scenarioContext.getActiveSituation();

    final TimeVaryingProfile profile1 = TimeVaryingProfileUtil.create(CustomTimeVaryingProfileType.THREE_DAY);
    final int i = scenarioContext.getActiveSituation().getTimeVaryingProfilesSize() + 1;
    profile1.setLabel("Profile " + i);
    profile1.setValues(Arrays.asList(0.124, 0.0784, 0.0654, 0.0776, 0.135, 0.3823, 0.9563, 1.7778, 1.9821, 1.5232, 1.4346, 1.4858, 1.5392, 1.5518,
        1.6628, 1.8865, 2.0769, 2.1003, 1.6218, 1.1176, 0.7701, 0.5689, 0.4111, 0.2442, 0.213, 0.133, 0.096, 0.089, 0.105, 0.195, 0.35, 0.6051,
        0.9811, 1.3442, 1.6272, 1.7772, 1.8002, 1.7142, 1.6042, 1.5192, 1.5012, 1.4392, 1.2252, 0.9441, 0.6771, 0.5271, 0.4571, 0.357, 0.249, 0.152,
        0.103, 0.086, 0.086, 0.13, 0.22, 0.353, 0.5461, 0.9481, 1.3502, 1.5772, 1.6812, 1.6192, 1.5532, 1.5272, 1.4982, 1.3352, 1.1412, 0.9291,
        0.7161, 0.5061, 0.337, 0.205));

    final TimeVaryingProfile profile2 = TimeVaryingProfileUtil.create(CustomTimeVaryingProfileType.THREE_DAY);
    profile2.setLabel("Profile " + (i + 1));
    eventBus.fireEvent(new TimeVaryingProfileAddCommand(profile1, presentSit));
    eventBus.fireEvent(new TimeVaryingProfileAddCommand(profile2, presentSit));
  }

  @JsMethod
  public void createSimpleSource() {
    addSimpleSource();
    placeController.goTo(new ScenarioInputListPlace(applicationContext.getTheme(), InputTypeViewMode.EMISSION_SOURCES));
  }

  @JsMethod
  public void createSimpleBuilding() {
    addSimpleBuilding();
    placeController.goTo(new ScenarioInputListPlace(applicationContext.getTheme(), InputTypeViewMode.EMISSION_SOURCES));
  }

  @JsMethod
  public void createComplexSource() {
    addComplexSource();
    placeController.goTo(new ScenarioInputListPlace(applicationContext.getTheme(), InputTypeViewMode.EMISSION_SOURCES));
  }

  @JsMethod
  public void createSimpleRoad() {
    addSimpleRoad();
    placeController.goTo(new ScenarioInputListPlace(applicationContext.getTheme(), InputTypeViewMode.EMISSION_SOURCES));
  }

  public void addSimpleBuilding() {
    if (!scenarioContext.hasActiveSituation()) {
      eventBus.fireEvent(new CreateEmptySituationCommand(true));
    }

    final SituationContext presentSit = scenarioContext.getActiveSituation();

    final BuildingFeature build = DummyEmissionSources.createBuilding(scenarioContext.getActiveSituation().getBuildingsSize());
    eventBus.fireEvent(new BuildingAddCommand(build, presentSit));
  }

  public void addSimpleSource() {
    if (!scenarioContext.hasActiveSituation()) {
      eventBus.fireEvent(new CreateEmptySituationCommand(true));
    }

    final SituationContext presentSit = scenarioContext.getActiveSituation();

    final EmissionSourceFeature src = isNCA()
        ? DummyEmissionSources.createAdmsFarmEnergySource(scenarioContext.getActiveSituation().getSourcesSize())
        : DummyEmissionSources.createOpsFarmEnergySource(scenarioContext.getActiveSituation().getSourcesSize());
    eventBus.fireEvent(new EmissionSourceAddCommand(src, presentSit));
  }

  public void addComplexSource() {
    if (!scenarioContext.hasActiveSituation()) {
      eventBus.fireEvent(new CreateEmptySituationCommand(true));
    }

    final SituationContext presentSit = scenarioContext.getActiveSituation();

    final EmissionSourceFeature src = isNCA()
        ? DummyEmissionSources.createAdmsFarmEnergySource(scenarioContext.getActiveSituation().getSourcesSize())
        : DummyEmissionSources.createOpsFarmEnergySource(scenarioContext.getActiveSituation().getSourcesSize());

    src.setGeometry(new ol.geom.Point(new Coordinate(207651, 536159)));
    eventBus.fireEvent(new EmissionSourceAddCommand(src, presentSit));
  }

  public void addSimpleRoad() {
    if (!scenarioContext.hasActiveSituation()) {
      eventBus.fireEvent(new CreateEmptySituationCommand(true));
    }

    final SituationContext presentSit = scenarioContext.getActiveSituation();
    final EmissionSourceFeature src = isNCA() ? DummyEmissionSources.createAdmsRoadSource(scenarioContext.getActiveSituation().getSourcesSize())
        : DummyEmissionSources.createOpsRoadSource(scenarioContext.getActiveSituation().getSourcesSize());
    eventBus.fireEvent(new EmissionSourceAddCommand(src, presentSit));
  }

  public CharacteristicsType getCharacteristicsType() {
    return isNCA() ? CharacteristicsType.ADMS : CharacteristicsType.OPS;
  }

  @JsMethod
  public void doSimpleCalculation() {
    if (scenarioContext.hasActiveSituation()) {
      eventBus.fireEvent(new DeleteAllEmissionSourceCommand());
    }

    addSimpleSource();
    startCalc();
  }

  @JsMethod
  public void doComplexCalculation() {
    if (scenarioContext.hasActiveSituation()) {
      eventBus.fireEvent(new DeleteAllEmissionSourceCommand());
    }

    addComplexSource();
    startCalc();
  }

  @JsMethod
  public void printMock() {
    if (scenarioContext.hasActiveSituation()) {
      eventBus.fireEvent(new DeleteAllEmissionSourceCommand());
    }

    addSimpleSource();
    addComplexSource();
    addSimpleRoad();

    eventBus.fireEvent(new CreateEmptySituationCommand(true));

    scenarioContext.getActiveSituation().setType(SituationType.REFERENCE);

    addSimpleSource();
    addComplexSource();
    addSimpleRoad();
    addSimpleBuilding();
    addTimeVaryingProfiles();
    addSimpleSource();
    addComplexSource();
    addSimpleRoad();
    addSimpleBuilding();
    addTimeVaryingProfiles();

    scenarioContext.getActiveSituation().getBuildings().stream().skip(1).findFirst()
        .ifPresent(building -> scenarioContext.getActiveSituation().getSources().stream()
            .filter(v -> v.getCharacteristics() != null)
            .forEach(v -> v.getCharacteristics().setBuildingGmlId(building.getGmlId())));

    printRedirect = true;

    startExport();
  }

  @EventHandler
  public void onExportStartEvent(final ExportStartEvent e) {
    final HistoryRecord lastItem = exportContext.getHistory().get(0);

    final String exportUrl = getExportUrl(lastItem.getJobKey(),
        exportContext.getSituationComposition().getSituationsByType(SituationType.PROPOSED).iterator().next().getId());

    NotificationUtil.broadcastMessage(eventBus,
        "This page will redirect to the print page when it completes. Or go <a href=\"" + exportUrl + "\" target=\"_blank\">here</a>");
  }

  @EventHandler
  public void onExportCompleteEvent(final ExportCalculationCompleteEvent e) {
    if (printRedirect) {
      printRedirect = false;
    } else {
      return;
    }

    if (e.getValue().getJobProgress().getState().isFailureState()) {
      GWTProd.error("Calculation failed.");
      return;
    }

    SchedulerUtil.delay(() -> {
      final String exportUrl = getExportUrl(e.getJobKey(), e.getValue().getSituationCalculations()[0].getSituationId());
      DomGlobal.location.assign(exportUrl);
    }, 1500);
  }

  private String getExportUrl(final String jobKey, final String proposedSituationId) {
    final StringBuilder bldr = new StringBuilder("/print");
    bldr.append("?locale=en");
    bldr.append("&pdfProductType=");
    bldr.append(PdfProductType.NCA_CALCULATOR.name());

    bldr.append("&proposedSituationId=");
    bldr.append(proposedSituationId);
    bldr.append("&reference=123");
    bldr.append("&jobKey=");
    bldr.append(jobKey);

    return bldr.toString();
  }

  private void prepareCalc(final Runnable after) {
    preparationContext.reset();
    eventBus.fireEvent(new CalculationJobAddCommand());
    SchedulerUtil.delay(() -> {
      final CalculationJobContext activeJob = preparationContext.getActiveJob();
      if (isNCA()) {
        activeJob.getCalculationOptions().setCalculationMethod(CalculationMethod.FORMAL_ASSESSMENT);
        activeJob.getCalculationOptions().setCalculateMaximumRange(5000);
        final ADMSOptions admsOptions = activeJob.getCalculationOptions().getNcaCalculationOptions().getAdmsOptions();
        final MetSite metSite = applicationContext.getConfiguration().getMetSites()
            .iterator().next();

        admsOptions.setMetSiteId(metSite.getId());
        admsOptions.setMetYears(Arrays.asList(metSite.getDatasets().stream()
            .filter(v -> v.getDatasetType() == admsOptions.getMetDatasetType())
            .findFirst()
            .map(v -> v.getYear())
            .get()));
      }

      after.run();
    }, 1);
  }

  private void startCalc() {
    prepareCalc(() -> {
      eventBus.fireEvent(new CalculationJobStartCommand(preparationContext.getActiveJob()));

      placeController.goTo(new ResultsPlace(applicationContext.getTheme()));
    });
  }

  private void startExport() {
    prepareCalc(() -> {
      final SituationContext refContext = scenarioContext.getActiveSituation();
      preparationContext.getActiveJob().getSituationComposition().setSituation(SituationType.REFERENCE, refContext);

      eventBus.fireEvent(new ExportPrepareCommand(preparationContext.getActiveJob()));

      SchedulerUtil.delay(() -> {
        final ExportOptions exportOptions = new ExportOptions();
        exportOptions.setExportType(ExportType.PDF_PAA);
        exportOptions.setProtectJob(true);
        exportOptions.setJobType(JobType.REPORT);
        exportOptions.setDemoMode(false);
        exportOptions.setName("export for print");
        exportOptions.setEmail(email);

        eventBus.fireEvent(new ExportStartCommand(exportOptions));
      });
    });
  }

  @JsMethod
  public void createStackedScenario() {
    scenarioContext.getSituations().clear();
    createMockSituation();
    createSimpleSource();
    eventBus.fireEvent(new SituationChangeTypeCommand(scenarioContext.getActiveSituation(), SituationType.REFERENCE));

    createMockSituation();
    createSimpleSource();
    createSimpleSource();
    eventBus.fireEvent(new SituationChangeTypeCommand(scenarioContext.getActiveSituation(), SituationType.PROPOSED));

    createMockSituation();
    createSimpleSource();
    createSimpleSource();
    createSimpleSource();
    eventBus.fireEvent(new SituationChangeTypeCommand(scenarioContext.getActiveSituation(), SituationType.PROPOSED));

    createMockSituation();
    createComplexSource();
    eventBus.fireEvent(new SituationChangeTypeCommand(scenarioContext.getActiveSituation(), SituationType.OFF_SITE_REDUCTION));
    scenarioContext.getActiveSituation().setNettingFactor(0.5);

    createMockSituation();
    createComplexSource();
    eventBus.fireEvent(new SituationChangeTypeCommand(scenarioContext.getActiveSituation(), SituationType.TEMPORARY));

    placeController.goTo(new CalculatePlace(applicationContext.getTheme()));
  }

  private boolean isNCA() {
    return applicationContext.getTheme() == Theme.NCA;
  }
}
