/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import com.google.inject.Singleton;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.PdfProductType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.summary.OverlappingHexagonType;
import nl.overheid.aerius.shared.domain.summary.ProcurementPolicy;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.application.print.CompleteTracker;

@Singleton
public class PrintContext {

  // Ordering in this list matters, it is used to order situations in the Overview
  // component.
  public static final @JsProperty List<SituationType> PROJECT_CALCULATION_SITUATION_TYPES = Arrays.asList(
      SituationType.REFERENCE,
      SituationType.PROPOSED,
      SituationType.OFF_SITE_REDUCTION);

  @Inject CompleteTracker completeTracker;

  private boolean failure = false;

  private String jobKey;
  private String reference;
  private final EmissionResultKey emissionResultKey = EmissionResultKey.NOXNH3_DEPOSITION;
  private final ProcurementPolicy procurementPolicy = null;
  private PdfProductType pdfProductType = null;

  @JsProperty private final Set<SituationResultsKey> procurementPolicyResultKeys = new HashSet<>();

  public void addProcurementPolicyResultKey(final SituationResultsKey situationResultsKey) {
    this.procurementPolicyResultKeys.add(situationResultsKey);
  }

  public Set<SituationResultsKey> getProcurementPolicyResultKeys() {
    return procurementPolicyResultKeys;
  }

  public void fail() {
    failure = true;
  }

  public boolean isFailure() {
    return failure;
  }

  public boolean isComplete() {
    return completeTracker.isComplete();
  }

  public String getJobKey() {
    return jobKey;
  }

  public void setJobKey(final String jobKey) {
    this.jobKey = jobKey;
  }

  public String getReference() {
    return reference;
  }

  public void setReference(final String reference) {
    this.reference = reference;
  }

  public ScenarioResultType getScenarioResultType() {
    if (pdfProductType == PdfProductType.OWN2000_SINGLE_SCENARIO) {
      return ScenarioResultType.SITUATION_RESULT;
    }
    return ScenarioResultType.PROJECT_CALCULATION;
  }

  public EmissionResultKey getEmissionResultKey() {
    return emissionResultKey;
  }

  public SummaryHexagonType getSummaryHexagonType() {
    if (pdfProductType == PdfProductType.OWN2000_EXTRA_ASSESSMENT_APPENDIX) {
      return SummaryHexagonType.EXTRA_ASSESSMENT_HEXAGONS;
    }
    return SummaryHexagonType.EXCEEDING_HEXAGONS;
  }

  public OverlappingHexagonType getOverlappingHexagonType() {
    if (pdfProductType == PdfProductType.OWN2000_EDGE_EFFECT_APPENDIX) {
      return OverlappingHexagonType.OVERLAPPING_HEXAGONS_ONLY;
    }
    return OverlappingHexagonType.ALL_HEXAGONS;
  }

  public PdfProductType getPdfProductType() {
    return pdfProductType;
  }

  public void setPdfProductType(final PdfProductType pdfProductType) {
    this.pdfProductType = pdfProductType;
  }

  public ProcurementPolicy getProcurementPolicy() {
    return procurementPolicy;
  }
}
