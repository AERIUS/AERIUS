/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.misc;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.event.PlaceChangeEvent;
import nl.aerius.wui.future.AppAsyncCallback;
import nl.aerius.wui.place.Place;
import nl.aerius.wui.place.PlaceController;
import nl.aerius.wui.util.RequestUtil;
import nl.overheid.aerius.wui.application.command.misc.ManualNavigatePageAbsoluteCommand;
import nl.overheid.aerius.wui.application.command.misc.ManualNavigatePageCommand;
import nl.overheid.aerius.wui.application.command.misc.ManualNavigateRelativePageCommand;
import nl.overheid.aerius.wui.application.command.misc.ManualNavigateTableOfContentsCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ManualContext;
import nl.overheid.aerius.wui.application.event.ResultsViewSelectionEvent;
import nl.overheid.aerius.wui.application.event.result.SelectResultMainTabEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceSectorChangeEvent;
import nl.overheid.aerius.wui.application.event.source.SectorGroupSelectionEvent;
import nl.overheid.aerius.wui.application.util.ManualUtil;

@Singleton
public class ManualDaemon extends BasicEventComponent {
  private static final ManualDaemonEventBinder EVENT_BINDER = GWT.create(ManualDaemonEventBinder.class);

  interface ManualDaemonEventBinder extends EventBinder<ManualDaemon> {}

  private @Inject ManualContext manualContext;
  private @Inject ApplicationContext appContext;
  private @Inject PersistablePreferencesContext prefContext;

  private @Inject PlaceController placeController;

  // Simple pages which map a place to documentation
  private final Map<Class<?>, String> manualPages = new HashMap<>();
  // More complex custom pages, which contain custom logic to obtain documentation
  private final Map<Class<?>, Function<Place, String>> customPages = new HashMap<>();

  @Inject
  public ManualDaemon(ManualHelperMinion subDaemon) {
    subDaemon.initManualPages(manualPages);
    subDaemon.initCustomPages(customPages);
  }

  @EventHandler
  public void on(final ManualNavigateTableOfContentsCommand c) {
    eventBus.fireEvent(new ManualNavigatePageCommand(ManualHelperMinion.MANUAL_TABLE_OF_CONTENTS));
  }

  @EventHandler
  public void on(final ManualNavigateRelativePageCommand c) {
    final String activeUrl = manualContext.getActiveUrl();
    final String activePath = activeUrl.substring(0, activeUrl.lastIndexOf("/"));

    final String targetUrl = c.getValue();
    // Remove leading . if any
    final String derelativizedUrl = targetUrl.startsWith("./") ? targetUrl.substring(1) : targetUrl;

    final String rawUrl = derelativizedUrl.replace(".html", ".raw.html");

    eventBus.fireEvent(new ManualNavigatePageAbsoluteCommand(activePath + rawUrl));
  }

  @EventHandler
  public void on(final ManualNavigatePageCommand c) {
    final String url = c.getValue();
    final String ultimateUrl = RequestUtil.prepareUrl(ManualUtil.getManualUrl(appContext),
        url.replace(".html", ".raw.html"));

    eventBus.fireEvent(new ManualNavigatePageAbsoluteCommand(ultimateUrl));
  }

  @EventHandler
  public void on(final ManualNavigatePageAbsoluteCommand c) {
    final String url = c.getValue();

    // Sanity bailouts
    if (url == null
        || url.equals(manualContext.getActiveUrl())) {
      return;
    }

    RequestUtil.doGet(url, AppAsyncCallback.create(
        v -> manualContext.addContentAndActivate(url, v),
        e -> manualContext.setException(e)));
  }

  @EventHandler
  public void on(final PlaceChangeEvent e) {
    // Don't switch if the switch setting is turned off, AND there is an active url
    if (!prefContext.isManualAutoSwitch()
        && manualContext.getActiveUrl() != null) {
      return;
    }

    doFindManualPage(e.getValue());
  }

  private void doFindManualPage(Place place) {
    String placePage = null;
    if (manualPages.containsKey(place.getClass())) {
      placePage = manualPages.get(place.getClass());
    } else if (customPages.containsKey(place.getClass())) {
      placePage = customPages.get(place.getClass()).apply(place);
    }

    if (placePage == null) {
      return;
    }

    if (appContext.getConfiguration().hasManual()) {
      eventBus.fireEvent(new ManualNavigatePageCommand(placePage));
    }
  }

  @EventHandler
  public void onSectorGroupSelectionCommand(SectorGroupSelectionEvent e) {
    // Emission source sector group selection
    doFindManualPage(placeController.getPlace());
  }

  @EventHandler
  public void onEmissionSourceSectorChangeEvent(EmissionSourceSectorChangeEvent e) {
    // Emission source specific sector selection
    doFindManualPage(placeController.getPlace());
  }

  @EventHandler
  public void onSelectResultMainTabEvent(SelectResultMainTabEvent e) {
    // Result page main view selection
    doFindManualPage(placeController.getPlace());
  }

  @EventHandler
  public void onResultsViewSelectionEvent(ResultsViewSelectionEvent e) {
    // Result page sub tab selection
    doFindManualPage(placeController.getPlace());
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
