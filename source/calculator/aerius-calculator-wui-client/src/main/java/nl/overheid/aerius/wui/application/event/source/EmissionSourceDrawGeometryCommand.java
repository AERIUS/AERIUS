/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.event.source;

import java.util.function.Consumer;

import ol.geom.Geometry;
import ol.geom.GeometryType;

import nl.aerius.wui.command.SimpleStatelessCommand;

public class EmissionSourceDrawGeometryCommand extends SimpleStatelessCommand<GeometryType> {
  private final Consumer<Geometry> startCallback;
  private final Consumer<Geometry> finishCallback;

  public EmissionSourceDrawGeometryCommand() {
    this(null, null, null);
  }

  /**
   * @param callback what to call when the geometry changes
   */
  public EmissionSourceDrawGeometryCommand(final GeometryType geometryType, final Consumer<Geometry> startCallback,
      final Consumer<Geometry> finishCallback) {
    super(geometryType);
    this.startCallback = startCallback;
    this.finishCallback = finishCallback;
  }

  public Consumer<Geometry> getStartCallback() {
    return startCallback;
  }

  public Consumer<Geometry> getFinishCallback() {
    return finishCallback;
  }
}
