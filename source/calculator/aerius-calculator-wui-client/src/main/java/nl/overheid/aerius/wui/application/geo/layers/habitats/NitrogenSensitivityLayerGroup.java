/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.habitats;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;

import ol.Collection;
import ol.layer.Base;
import ol.layer.Group;
import ol.layer.Layer;
import ol.layer.LayerGroupOptions;

import nl.aerius.geo.domain.IsLayer;
import nl.aerius.geo.domain.LayerInfo;
import nl.aerius.geo.shared.LayerProps;
import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.wui.application.i18n.M;

public class NitrogenSensitivityLayerGroup extends BasicEventComponent implements IsLayer<Group> {
  private static final String BUNDLE_NAME = "nitrogen_sensitivity";

  public static final String HABITAT_AREAS_SENSITIVITY_NAME = "calculator:wms_habitat_areas_sensitivity_view";
  public static final String EXTRA_ASSESSMENT_HEXAGONS_SENSITIVITY_NAME = "calculator:wms_extra_assessment_hexagons_sensitivity_view";

  private static final SensitiveHabitatLayerGroupEventBinder EVENT_BINDER = GWT.create(SensitiveHabitatLayerGroupEventBinder.class);

  public interface SensitiveHabitatLayerGroupEventBinder extends EventBinder<NitrogenSensitivityLayerGroup> {
  }

  private final LayerInfo info;
  private final Group layer;

  private Layer habitatLayer;
  private Layer extraAssessmentLayer;

  @Inject
  public NitrogenSensitivityLayerGroup(@Assisted List<IsLayer<?>> layers, final EventBus eventBus) {
    this.info = new LayerInfo();
    this.info.setName(this.getClass().getCanonicalName());
    this.info.setTitle(M.messages().layerNitrogenSensitivityExtraAssessmentCombined());
    this.info.setBundle(BUNDLE_NAME);

    final Optional<IsLayer<?>> habitatSensitivityLayer = layers.stream()
        .filter(v -> HABITAT_AREAS_SENSITIVITY_NAME.equals(v.getInfo().getName()))
        .findFirst();
    final Optional<IsLayer<?>> extraAssessmentHexagonLayer = layers.stream()
        .filter(v -> EXTRA_ASSESSMENT_HEXAGONS_SENSITIVITY_NAME.equals(v.getInfo().getName()))
        .findFirst();

    if (habitatSensitivityLayer.isPresent() && extraAssessmentHexagonLayer.isPresent()) {
      habitatLayer = (Layer) habitatSensitivityLayer.get().asLayer();
      extraAssessmentLayer = (Layer) extraAssessmentHexagonLayer.get().asLayer();

      final Collection<Base> baseLayers = new Collection<>();
      baseLayers.push((Layer) habitatLayer);
      baseLayers.push((Layer) extraAssessmentLayer);

      // Just grab the habitat layer legend and use that one
      this.info.setLegend(habitatSensitivityLayer.get().getInfo().getLegend());

      habitatLayer.setVisible(true);
      extraAssessmentLayer.setVisible(true);

      final LayerGroupOptions groupOptions = new LayerGroupOptions();
      groupOptions.setLayers(baseLayers);
      layer = new Group(groupOptions);
      layer.setVisible(false);
    } else {
      throw new RuntimeException("Habitat sensitivity layer could not be assembled.");
    }
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  public static final boolean isSensitiveHabitatLayer(LayerProps props) {
    return HABITAT_AREAS_SENSITIVITY_NAME.equals(props.getName())
        || isExtraAssessmentHexagonLayer(props.getName());
  }

  public static final boolean isExtraAssessmentHexagonLayer(String name) {
    return EXTRA_ASSESSMENT_HEXAGONS_SENSITIVITY_NAME.equals(name);
  }

  @Override
  public Group asLayer() {
    return layer;
  }

  @Override
  public LayerInfo getInfo() {
    return info;
  }
}
