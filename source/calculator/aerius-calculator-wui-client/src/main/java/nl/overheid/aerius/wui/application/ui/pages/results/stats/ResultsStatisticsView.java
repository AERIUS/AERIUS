/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.results.stats;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.HorizontalCollapseGroup;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings.DepositionValueDisplayType;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.result.ResultSummaryContext;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsStatistics;
import nl.overheid.aerius.wui.application.ui.pages.results.GeneralizedEmissionResultType;
import nl.overheid.aerius.wui.application.util.ResultStatisticUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    HorizontalCollapseGroup.class
})
public class ResultsStatisticsView extends BasicVueComponent {
  @Prop SituationResultsStatistics statistics;
  @Prop(required = true) ResultSummaryContext context;

  @Inject ResultsStatisticsFormatter resultsStatisticsFormatter;

  @Data @Inject ApplicationContext applicationContext;

  @Computed
  public GeneralizedEmissionResultType getEmissionResultType() {
    return GeneralizedEmissionResultType.fromEmissionResultType(context.getEmissionResultKey().getEmissionResultType());
  }

  @Computed
  public ScenarioResultType getResultType() {
    return context.getResultType();
  }

  @Computed
  public SummaryHexagonType getHexagonType() {
    return context.getHexagonType();
  }

  @Computed
  public DepositionValueDisplayType getDisplayType() {
    return applicationContext.getConfiguration().getEmissionResultValueDisplaySettings().getDisplayType();
  }

  @Computed
  public List<ResultStatisticType> getScenarioResultTypes() {
    return ResultStatisticUtil.getStatisticsForSummary(applicationContext.getTheme(), getResultType(), getHexagonType(),
        context.getEmissionResultKey());
  }

  @JsMethod
  public String formatValueFor(final ResultStatisticType resultStatisticType) {
    return resultsStatisticsFormatter.formatStatisticType(statistics, resultStatisticType);
  }

}
