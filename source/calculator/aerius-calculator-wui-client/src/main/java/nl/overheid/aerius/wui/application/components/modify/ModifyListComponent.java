/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.modify;

import java.util.ArrayList;
import java.util.List;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Emit;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.tooltip.TooltipComponent;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    ButtonIcon.class,
    TooltipComponent.class
})
public class ModifyListComponent extends BasicVueComponent {
  @Prop EventBus eventBus;

  // TODO Refactor out and remove, use emits instead
  @Deprecated @Prop ModifyListActions presenter;

  @Prop String typeText;
  @Prop String newIconClass;

  @Prop String debugId;

  @Prop boolean selected;
  @Prop boolean singleSelected;
  @Prop boolean multiSelected;

  @Prop boolean enableAutomaticPlacement;
  @Prop boolean enableEdit;

  @Prop boolean enableDelete;
  @Prop boolean disableDelete;
  @Prop boolean enableCopy;

  @Prop @JsProperty List<ModifyListCustomButton> customButtonsRight;
  @Prop @JsProperty List<ModifyListCustomButton> customButtonsLeft;

  @Data String iconFontSize = "26px";

  @PropDefault("newIconClass")
  String newIconClassDefault() {
    return "icon-btn-new-source";
  }

  @PropDefault("debugId")
  String debugIdDefault() {
    return "";
  }

  @PropDefault("enableDelete")
  boolean enableDeleteDefault() {
    return false;
  }

  @PropDefault("enableAutomaticPlacement")
  boolean enableAutomaticPlacement() {
    return false;
  }

  @PropDefault("disableDelete")
  boolean disableDeleteDefault() {
    return false;
  }

  @PropDefault("singleSelected")
  boolean singleSelectedDefault() {
    return false;
  }

  @PropDefault("multiSelected")
  boolean multiSelectedDefault() {
    return false;
  }

  @PropDefault("enableCopy")
  boolean enableCopyDefault() {
    return true;
  }

  @PropDefault("customButtonsLeft")
  List<ModifyListCustomButton> customButtonsLeftDefaults() {
    return new ArrayList<>();
  }

  @PropDefault("customButtonsRight")
  List<ModifyListCustomButton> customButtonsRightDefaults() {
    return new ArrayList<>();
  }

  @JsMethod
  @Emit
  public void newEntity() {
    if (presenter != null) {
      presenter.newEntity();
    }
  }

  @JsMethod
  @Emit
  public void importFile() {
    if (presenter != null) {
      presenter.importFile();
    }
  }

  @JsMethod
  @Emit
  public void duplicateEntity() {
    if (presenter != null) {
      presenter.copy();
    }
  }

  @JsMethod
  @Emit
  public void deleteEntity() {
    if (presenter != null) {
      presenter.delete();
    }
  }

  @JsMethod
  @Emit
  public void editEntity() {
    if (presenter != null) {
      presenter.edit();
    }
  }

  @JsMethod
  @Emit
  public void automaticPlacement() {
    if (presenter != null) {
      presenter.automaticPlacement();
    }
  }
}
