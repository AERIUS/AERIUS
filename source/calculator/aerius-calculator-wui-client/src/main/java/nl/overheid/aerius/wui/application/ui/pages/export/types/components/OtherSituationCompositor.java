/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.export.types.components;

import java.util.List;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.wui.application.components.input.CheckBoxComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;

@Component(components = {
    CheckBoxComponent.class,
    LabeledInputComponent.class,
})
public class OtherSituationCompositor extends ErrorWarningValidator {
  @Prop SituationComposition situationComposition;
  @Prop @JsProperty List<SituationContext> situations;

  @JsMethod
  public boolean isIncludingResults(final SituationContext situation) {
    return situationComposition.isActiveSituation(situation);
  }

  @JsMethod
  public void updateIncludingSituation(final boolean checked, final SituationContext situation) {
    if (checked) {
      situationComposition.addSituation(situation);
    } else {
      situationComposition.removeSituation(situation);
    }
  }

  @JsMethod
  public boolean isActiveSituation(final SituationContext situation) {
    return situationComposition.isActiveSituation(situation);
  }
}
