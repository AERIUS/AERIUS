/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.manure.custom;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.wui.application.components.source.DetailStatComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.manure.CustomManureStorage;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "aer-manure-storage-custom-detail", components = {
    DetailStatComponent.class,
})
public class ManureStorageCustomDetailComponent extends BasicVueComponent {

  @Prop @JsProperty protected CustomManureStorage source;
  @Prop @JsProperty protected int idx;

  @Data @Inject ApplicationContext applicationContext;

  @Computed
  protected String emissionFactor() {
    return MessageFormatter.formatEmissionWithUnitSmart(source.getEmissionFactor(Substance.NH3));
  }

  @Computed("substances")
  public List<Substance> getSubstances() {
    return applicationContext.getConfiguration().getSubstances();
  }

  @JsMethod
  protected String emission(final Substance substance) {
    return MessageFormatter.formatEmissionWithUnitSmart(source.getEmission(substance));
  }

}
