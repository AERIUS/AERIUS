/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.print.own2000.results;

import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.summary.OverlappingHexagonType;
import nl.overheid.aerius.wui.application.command.result.FetchSummaryResultsCommand;
import nl.overheid.aerius.wui.application.context.PrintContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsSummary;
import nl.overheid.aerius.wui.application.event.FetchSummaryResultsEvent;
import nl.overheid.aerius.wui.application.event.RestoreCalculationJobContextEvent;
import nl.overheid.aerius.wui.application.ui.pages.results.receptors.EdgeEffectReceptorsView;
import nl.overheid.aerius.wui.application.util.ResultDisplaySetUtil;

@Component
public class ResultSummaryEdgeEffectReceptors extends EdgeEffectReceptorsView {

  public static final String ID = "results-edge-effects-receptors";

  protected static final ResultSummaryEdgeEffectReceptorsEventBinder EVENT_BINDER = GWT.create(
      ResultSummaryEdgeEffectReceptorsEventBinder.class);

  interface ResultSummaryEdgeEffectReceptorsEventBinder extends EventBinder<ResultSummaryEdgeEffectReceptors> {}

  @Inject @Data PrintContext printOwN2000Context;
  @Data SituationResultsKey key = null;
  @Data boolean fetchComplete;

  @Computed
  public boolean isRender() {
    return fetchComplete;
  }

  @EventHandler
  public void onFetchSummaryResultsEvent(final FetchSummaryResultsEvent e) {
    if (e.getValue().equals(key)) {
      fetchComplete = true;
    }
  }

  @JsMethod
  public SituationResultsSummary getSummary() {
    return Optional.ofNullable(getResultContext())
        .map(c -> c.getResultSummary(key))
        .orElseThrow(() -> new RuntimeException("Could not retrieve result summary."));
  }

  @Computed
  public String getIntro() {
    return i18n.pdfResultSummaryEdgeEffectTableTitle(scenarioContext.getActiveSituation() != null ? scenarioContext.getActiveSituation().getName() : "");
  }

  @EventHandler
  public void onRestoreCalculationContextEvent(final RestoreCalculationJobContextEvent e) {
    // Fetch edge effect receptors
    final SituationResultsKey edgeEffectsKey = new SituationResultsKey(
        SituationContext.handleFromSituation(scenarioContext.getActiveSituation()),
        printOwN2000Context.getScenarioResultType(),
        printOwN2000Context.getEmissionResultKey(),
        printOwN2000Context.getSummaryHexagonType(),
        OverlappingHexagonType.NON_OVERLAPPING_HEXAGONS_ONLY,
        printOwN2000Context.getProcurementPolicy());
    key = edgeEffectsKey;
    eventBus.fireEvent(new FetchSummaryResultsCommand(edgeEffectsKey));
  }

  @Override
  public void created() {
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @JsMethod
  protected String noResultsText() {
    return ResultDisplaySetUtil.noCalculationResultsText(getResultContext());
  }
}
