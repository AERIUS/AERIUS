/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.building;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.Feature;
import ol.geom.Geometry;
import ol.geom.Point;
import ol.source.Source;

import nl.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.wui.application.command.building.BuildingFeatureSelectCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingModifyGeometryCommand;
import nl.overheid.aerius.wui.application.command.building.TemporaryBuildingAddCommand;
import nl.overheid.aerius.wui.application.command.building.TemporaryBuildingClearCommand;
import nl.overheid.aerius.wui.application.context.BuildingEditorContext;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.event.building.BuildingAddedEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingDeletedEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingListChangeEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingUpdatedEvent;
import nl.overheid.aerius.wui.application.event.situation.SituationSwitchEvent;
import nl.overheid.aerius.wui.application.geo.layers.base.BaseGeometryLayer;
import nl.overheid.aerius.wui.application.geo.layers.base.VectorFeaturesWrapperMap;
import nl.overheid.aerius.wui.application.geo.util.GeoUtil;

/**
 * Abstract base class for showing the {@link BuildingFeature} geometry on the map.
 */
abstract class BuildingGeometryLayer extends BaseGeometryLayer<BuildingFeature> {
  interface BuildingGeometryLayerEventBinder extends EventBinder<BuildingGeometryLayer> {}

  private static final BuildingGeometryLayerEventBinder EVENT_BINDER = GWT.create(BuildingGeometryLayerEventBinder.class);

  @Inject
  private BuildingEditorContext editorContext;

  private final VectorFeaturesWrapperMap situationFeatures = new VectorFeaturesWrapperMap();

  public BuildingGeometryLayer(final LayerInfo info, final EventBus eventBus, final int zIndex) {
    super(info, zIndex);
    EVENT_BINDER.bindEventHandlers(this, eventBus);
    situationFeatures.setMapFunction(this::converFeature);
  }

  @EventHandler
  public void onTemporaryBuildingAddCommand(final TemporaryBuildingAddCommand c) {
    situationFeatures.addFeature(c.getSituation().getId(), c.getValue());
  }

  @EventHandler
  public void onTemporaryBuildingClearCommand(final TemporaryBuildingClearCommand c) {
    situationFeatures.removeFeature(c.getSituation().getId(), c.getValue());
  }

  @EventHandler
  public void onBuildingUpdatedEvent(final BuildingUpdatedEvent e) {
    situationFeatures.updateFeature(e.getSituation().getId(), e.getValue());
  }

  @EventHandler
  public void onBuildingAddedEvent(final BuildingAddedEvent e) {
    situationFeatures.addFeature(e.getSituation().getId(), e.getValue());
  }

  @EventHandler
  public void onBuildingDeletedEvent(final BuildingDeletedEvent e) {
    situationFeatures.removeFeature(e.getSituation().getId(), e.getValue());
  }

  @EventHandler
  public void onBuildingListChangeEvent(final BuildingListChangeEvent e) {
    setFeatures(situationFeatures.getVectorFeaturesWrapper(e.getSituationId()), e.getValue());
  }

  @EventHandler
  public void onBuildingModifyGeometryCommand(final BuildingModifyGeometryCommand e) {
    setGeometryEditingFeatureId(e.getValue());
  }

  @EventHandler
  public Source onBuildingFeatureSelectCommand(final BuildingFeatureSelectCommand c) {
    return super.refreshLayer();
  }

  @EventHandler
  public void onSituationSwitch(final SituationSwitchEvent e) {
    setLayerVector(situationFeatures.getLayerVector(e.getValue()));
  }

  /**
   * If a building is a circle return a Feature with the circle geometry. Because the feature itself only contains a point.
   *
   * @param feature feature to convert
   * @return original feature or new circle geomtry feature
   */
  private Feature converFeature(final Feature feature) {
    final Geometry geometry = feature.getGeometry();

    if (geometry instanceof Point && ((BuildingFeature) feature).getDiameter() != null) {
      // If building a point and radius not null it's a circle
      final Feature clone = feature.clone();

      clone.setId(feature.getId());
      clone.setGeometry(GeoUtil.toCircleGeometry((Point) geometry, ((BuildingFeature) feature).getDiameter()));
      return clone;
    } else {
      return feature;
    }
  }

  @Override
  protected BuildingFeature getEditingFeatureFromContext() {
    return editorContext.getBuilding();
  }
}
