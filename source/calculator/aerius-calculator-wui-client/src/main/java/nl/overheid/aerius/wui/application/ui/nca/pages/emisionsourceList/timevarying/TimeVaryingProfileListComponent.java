/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.nca.pages.emisionsourceList.timevarying;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.EventBus;

import elemental2.dom.MouseEvent;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.shared.domain.characteristics.StandardTimeVaryingProfile;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfileType;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileCreateNewCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileDeleteAllCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileDeleteSelectedCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileDuplicateSelectedCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileEditSelectedCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfilePeekFeatureCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileToggleSelectCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileUnpeekFeatureCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.modify.ModifyListComponent;
import nl.overheid.aerius.wui.application.components.modify.ModifyListCustomButton;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.TimeVaryingProfileListContext;
import nl.overheid.aerius.wui.application.domain.source.TimeVaryingProfile;
import nl.overheid.aerius.wui.application.domain.source.util.TimeVaryingProfileUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    VerticalCollapse.class,
    VerticalCollapseGroup.class,
    ModifyListComponent.class,
    TimeVaryingProfileRowComponent.class,
    ButtonIcon.class,
})
public class TimeVaryingProfileListComponent extends BasicVueComponent {
  @Prop EventBus eventBus;

  @Prop SituationContext situation;

  @Inject @Data ApplicationContext applicationContext;
  @Inject @Data ScenarioContext scenarioContext;
  @Inject @Data TimeVaryingProfileListContext listContext;

  @JsMethod
  public List<ModifyListCustomButton> customButtons() {
    return Arrays.asList(new ModifyListCustomButton(
        i18n.esButtonNewSource(i18n.timeVaryingProfileTitle(CustomTimeVaryingProfileType.MONTHLY)),
        i18n.esButtonNewSource(i18n.timeVaryingProfileTitle(CustomTimeVaryingProfileType.MONTHLY)),
        "icon-btn-new-monthly-profile",
        "newMonthlyProfileButton") {
      @Override
      @JsMethod
      public void select() {
        createNewMonthlyProfile();
      }
    });
  }

  @Computed("hasTimeVaryingProfiles")
  public boolean hasTimeVaryingProfiles() {
    return !getTimeVaryingProfiles().isEmpty();
  }

  @JsMethod
  public SectorCategories getCategories() {
    return applicationContext.getConfiguration().getSectorCategories();
  }

  @Computed("isSystemProfileSelected")
  public boolean isSystemProfileSelected() {
    return listContext.getSelections().stream()
        .anyMatch(v -> v.isSystemProfile());
  }

  @Computed
  public List<TimeVaryingProfile> getTimeVaryingProfiles() {
    return Optional.ofNullable(situation)
        .map(v -> v.getTimeVaryingProfiles())
        .map(lst -> lst.stream()
            .filter(v -> v.getGmlId() != null)
            .collect(Collectors.toList()))
        .orElse(new ArrayList<>());
  }

  @Computed
  public List<TimeVaryingProfile> getStandardTimeVaryingProfiles() {
    return Stream.concat(standardTimeVaryingProfiles(CustomTimeVaryingProfileType.THREE_DAY),
        standardTimeVaryingProfiles(CustomTimeVaryingProfileType.MONTHLY))
        .map(v -> coerceToTimeVaryingProfile(v))
        .collect(Collectors.toList());
  }

  private Stream<StandardTimeVaryingProfile> standardTimeVaryingProfiles(final CustomTimeVaryingProfileType type) {
    return getCategories().getTimeVaryingProfiles().getStandardTimeVaryingProfiles(type).stream();
  }

  private TimeVaryingProfile coerceToTimeVaryingProfile(final StandardTimeVaryingProfile v) {
    return TimeVaryingProfileUtil.fromStandardTimeVaryingProfile(v, v.getCode());
  }

  @JsMethod
  public void selectTimeVaryingProfile(final TimeVaryingProfile profile) {
    eventBus.fireEvent(new TimeVaryingProfileToggleSelectCommand(profile, false));
  }

  @JsMethod
  public void multiSelectTimeVaryingProfile(final TimeVaryingProfile profile) {
    eventBus.fireEvent(new TimeVaryingProfileToggleSelectCommand(profile, true));
  }

  @JsMethod
  public void selectStandardTimeVaryingProfile(final TimeVaryingProfile profile) {
    eventBus.fireEvent(new TimeVaryingProfileToggleSelectCommand(profile, false));
  }

  @JsMethod
  public void peekTimeVaryingProfile(final TimeVaryingProfile profile) {
    eventBus.fireEvent(new TimeVaryingProfilePeekFeatureCommand(profile));
  }

  @JsMethod
  public void unpeekTimeVaryingProfile(final TimeVaryingProfile profile) {
    eventBus.fireEvent(new TimeVaryingProfileUnpeekFeatureCommand(profile));
  }

  @JsMethod
  public void peekStandardTimeVaryingProfile(final TimeVaryingProfile profile) {
    eventBus.fireEvent(new TimeVaryingProfilePeekFeatureCommand(profile));
  }

  @JsMethod
  public void unpeekStandardTimeVaryingProfile(final TimeVaryingProfile profile) {
    eventBus.fireEvent(new TimeVaryingProfileUnpeekFeatureCommand(profile));
  }

  @JsMethod
  public void createNewDiurnalProfile() {
    eventBus.fireEvent(new TimeVaryingProfileCreateNewCommand(CustomTimeVaryingProfileType.THREE_DAY));
  }

  @JsMethod
  public void createNewMonthlyProfile() {
    eventBus.fireEvent(new TimeVaryingProfileCreateNewCommand(CustomTimeVaryingProfileType.MONTHLY));
  }

  @JsMethod
  public void deleteAllTimeVaryingProfiles() {
    if (Window.confirm(i18n.timeVaryingProfileButtonDeleteAllWarning())) {
      eventBus.fireEvent(new TimeVaryingProfileDeleteAllCommand());
    }
  }

  @JsMethod
  public void clickTimeVaryingProfile(final TimeVaryingProfile profile, final MouseEvent ev) {
    if (ev.ctrlKey) {
      multiSelectTimeVaryingProfile(profile);
    } else {
      selectTimeVaryingProfile(profile);
    }
  }

  @JsMethod
  public void editTimeVaryingProfile() {
    eventBus.fireEvent(new TimeVaryingProfileEditSelectedCommand());
  }

  @JsMethod
  public void duplicateTimeVaryingProfile() {
    eventBus.fireEvent(new TimeVaryingProfileDuplicateSelectedCommand());
  }

  @JsMethod
  public void deleteTimeVaryingProfile() {
    eventBus.fireEvent(new TimeVaryingProfileDeleteSelectedCommand());
  }
}
