/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.inject.Singleton;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.export.ExportAppendix;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.application.domain.export.HistoryRecord;
import nl.overheid.aerius.wui.application.domain.importer.ScenarioMetaData;

@Singleton
public class ExportContext {
  public enum ClientJobType {
    SOURCES, CALCULATION, REPORT;

    public JobType toJobType() {
      switch (this) {
      default:
      case SOURCES:
      case CALCULATION:
        return JobType.CALCULATION;
      case REPORT:
        return JobType.REPORT;
      }
    }
  }

  private boolean initialized = false;

  private final ExportOptionsFacade exportOptionsFacade = new ExportOptionsFacade();

  private ScenarioMetaData scenarioMetaData = ScenarioMetaData.create();

  private ExportType exportType = ExportType.GML_SOURCES_ONLY;
  private CalculationJobType reportType = CalculationJobType.PROCESS_CONTRIBUTION;
  private final @JsProperty Set<ExportAppendix> appendices = new HashSet<>();

  private String email = "";
  private boolean includeScenarioMetaData = false;

  private final @JsProperty List<HistoryRecord> history = new ArrayList<>();

  public ClientJobType getJobType() {
    return exportOptionsFacade.getJobType();
  }

  public void setJobType(final ClientJobType jobType) {
    exportOptionsFacade.setJobType(jobType);
  }

  public CalculationJobType getReportType() {
    return reportType;
  }

  public void setReportType(final CalculationJobType reportType) {
    this.reportType = reportType;
  }

  public ExportType getExportType() {
    return exportType;
  }

  public void setExportType(final ExportType exportType) {
    this.exportType = exportType;
  }

  public Set<ExportAppendix> getAppendices() {
    return appendices;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(final String email) {
    this.email = email;
  }

  public boolean isIncludeScenarioMetaData() {
    return includeScenarioMetaData;
  }

  public void setIncludeScenarioMetaData(final boolean includeScenarioMetaData) {
    this.includeScenarioMetaData = includeScenarioMetaData;
  }

  public SituationComposition getSituationComposition() {
    return exportOptionsFacade.getSituationComposition();
  }

  public void setSituationComposition(final SituationComposition situationComposition) {
    exportOptionsFacade.setSituationComposition(situationComposition);
  }

  public CalculationSetOptions getCalculationOptions() {
    return exportOptionsFacade.getCalculationOptions();
  }

  public void setCalculationOptions(final CalculationSetOptions calculationOptions) {
    exportOptionsFacade.setCalculationOptions(calculationOptions);
  }

  public boolean isInitialized() {
    return initialized;
  }

  public void setInitialized(final boolean initialized) {
    this.initialized = initialized;
  }

  public ScenarioMetaData getScenarioMetaData() {
    return scenarioMetaData;
  }

  public void setScenarioMetaData(final ScenarioMetaData scenarioMetaData) {
    this.scenarioMetaData = scenarioMetaData;
  }

  public List<HistoryRecord> getHistory() {
    return history;
  }

  public void addHistoryItem(final HistoryRecord item) {
    history.add(0, item);
  }

  public void removeHistoryItem(final HistoryRecord historyRecord) {
    history.remove(historyRecord);
  }

  public CalculationJobContext getCalculationJob() {
    return exportOptionsFacade.getJob();
  }

  public void setCalculationJob(final CalculationJobContext job) {
    exportOptionsFacade.setJob(job);
  }

  public void removeSourceInputSituation(final SituationContext value) {
    exportOptionsFacade.removeSourceInputSituation(value);
  }
}
