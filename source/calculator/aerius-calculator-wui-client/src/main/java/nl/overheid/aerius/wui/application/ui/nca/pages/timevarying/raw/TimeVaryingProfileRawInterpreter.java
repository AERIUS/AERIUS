/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.nca.pages.timevarying.raw;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import nl.overheid.aerius.wui.application.domain.source.util.TimeVaryingProfileUtilBase;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.ui.nca.pages.timevarying.raw.TimeVaryingProfileInterpretException.Reason;

/**
 * Abstract importer interpreter for raw time-varying profile tables.
 */
public abstract class TimeVaryingProfileRawInterpreter<T extends TimeVaryingProfileUtilBase> {

  protected static final String NEW_LINE = "\n";
  protected static final String SEPARATOR = ",";

  protected final T util;
  private final String header;
  private final int numberOfRows;
  private final int numberOfRowsAndHeader;
  private final int numberOfColumns;
  private final int numberOfColumnsAndHeader;

  public TimeVaryingProfileRawInterpreter(final T util, final String header, final int numberOfRows, final int numberOfColumns) {
    this.util = util;
    this.header = header;
    this.numberOfRows = numberOfRows;
    this.numberOfRowsAndHeader = numberOfRows + 1;
    this.numberOfColumns = numberOfColumns;
    this.numberOfColumnsAndHeader = numberOfColumns + 1;
  }

  /**
   * Try to interpret the raw time-varying profile data.
   *
   * @param input raw input
   * @return list of read values
   * @throws TimeVaryingProfileInterpretException
   */
  public List<Double> tryInterpret(final String input) throws TimeVaryingProfileInterpretException {
    final List<String> lines = Stream.of(input.split(NEW_LINE))
        .filter(v -> !v.trim().isEmpty())
        .filter(v -> !v.startsWith("!"))
        .collect(Collectors.toList());

    if (!(lines.size() == numberOfRows || lines.size() == numberOfRowsAndHeader)) {
      throw new TimeVaryingProfileInterpretException(Reason.INCORRECT_NUMBER_OF_ROWS,
          M.messages().timeVaryingProfileImportErrorOr(numberOfRows, numberOfRowsAndHeader));
    }
    final List<String[]> separatedParts = lines.stream()
        // Split on komma or tab
        .map(v -> v.split("[,\\t]"))
        .collect(Collectors.toList());

    final boolean columns = separatedParts.stream().allMatch(v -> v.length == numberOfColumns);
    final boolean columnsWithHeader = separatedParts.stream().allMatch(v -> v.length == numberOfColumnsAndHeader);
    if (!(columns || columnsWithHeader)) {
      throw new TimeVaryingProfileInterpretException(Reason.INCORRECT_NUMBER_OF_COLUMNS,
          M.messages().timeVaryingProfileImportErrorOr(numberOfColumns, numberOfColumnsAndHeader));
    }

    final boolean hasHeader = lines.size() == numberOfRowsAndHeader;
    final Map<String, Integer> headerConfig = hasHeader
        ? findHeaderConfig(separatedParts.get(0), columnsWithHeader)
        : getDefaultHeaderConfig(columnsWithHeader);

    final List<Double> values = new ArrayList<>(IntStream.range(0, util.getType().getExpectedNumberOfValues())
        .mapToDouble(v -> -1D)
        .boxed()
        .collect(Collectors.toList()));

    final List<String[]> rawData = separatedParts.stream()
        .skip(hasHeader ? 1 : 0)
        .collect(Collectors.toList());

    tryInterpretLines(columnsWithHeader, headerConfig, rawData, values);
    return values;
  }

  /**
   * Try to interpret the specific data lines. This is profile specific and must be implemented on a profile base.
   *
   * @param columnsWithName true if the first column contains the name of the row
   * @param headerConfig header information
   * @param rawData raw data of the rows
   * @param values object to store the interpreted values in
   * @throws TimeVaryingProfileInterpretException
   */
  protected abstract void tryInterpretLines(final boolean columnsWithName, final Map<String, Integer> headerConfig, final List<String[]> rawData,
      final List<Double> values) throws TimeVaryingProfileInterpretException;

  protected Map<String, Integer> findHeaderConfig(final String[] parts, final boolean hasRowName) throws TimeVaryingProfileInterpretException {
    final List<String> header = Stream.of(parts)
        .map(v -> simplify(v))
        .collect(Collectors.toList());
    validateHeader(header, hasRowName);

    final Map<String, Integer> map = new HashMap<>();
    putHeaderColumns(map, header, hasRowName);
    return map;
  }

  /**
   * Validate the header.
   *
   * @param header Raw headers
   * @param hasRowName true if the first column should contain the row name header name
   * @throws TimeVaryingProfileInterpretException
   */
  protected abstract void validateHeader(List<String> header, final boolean hasRowName) throws TimeVaryingProfileInterpretException;

  /**
   * Store the names of the headers in the map with an index number to the column index of the row name.
   *
   * @param map map to store column names
   * @param header list of raw headers
   * @param hasRowName true if the first column should contain the row name header name
   */
  protected abstract void putHeaderColumns(Map<String, Integer> map, List<String> header, boolean hasRowName);

  /**
   * If no header present return the default header row indexes.
   *
   * @param hasRowName true if the first column should contain the row name header name
   * @return default row header indexes
   */
  protected abstract Map<String, Integer> getDefaultHeaderConfig(boolean hasRowName);

  protected static boolean contains(final List<String> header, final String... matches) {
    return findFirst(header, matches) != -1;
  }

  protected static int findFirst(final List<String> header, final String... matches) {
    for (int i = 0; i < matches.length; i++) {
      final int idx = header.indexOf(matches[i]);
      if (idx == -1) {
        continue;
      }
      return idx;
    }
    return -1;
  }

  private static String simplify(final String v) {
    // Take the first word in lowercase
    return v.toLowerCase().split(" ")[0];
  }

  /**
   * Puts the profiles values into a single text.
   *
   * @param values values to encode
   * @return String
   */
  public String encode(final List<Double> values) {
    final StringBuilder bldr = new StringBuilder();
    bldr.append(header);
    bldr.append(NEW_LINE);

    for (int i = 0; i < numberOfRows; i++) {
      encode(bldr, values, i);
      bldr.append(NEW_LINE);
    }

    return bldr.toString();
  }


  /**
   * Append profile specific values to the string builder.
   *
   * @param bldr string builder to append values
   * @param values data to get values to append from
   * @param rowNumber Number of the profile row to append the data for
   */
  protected abstract void encode(StringBuilder bldr, List<Double> values, int rowNumber);
}
