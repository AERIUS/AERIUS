/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.base;

import java.util.List;

import ol.Feature;
import ol.OLFactory;
import ol.layer.Layer;
import ol.layer.VectorLayerOptions;
import ol.source.Source;
import ol.source.Vector;
import ol.style.Style;
import ol.style.StyleFunction;

import nl.aerius.geo.domain.IsLayer;
import nl.aerius.geo.domain.LayerInfo;

/**
 * Abstract base {@link Vector} based layer to show features on the map.
 */
public abstract class BaseVectorLayer<F extends Feature> implements IsLayer<Layer> {
  private final LayerInfo info;
  private final ol.layer.Vector layer;

  public BaseVectorLayer(final LayerInfo info, final int zIndex) {
    this.info = info;
    final VectorLayerOptions vectorLayerOptions = OLFactory.createOptions();

    setAdditionalVectorLayerOptions(vectorLayerOptions);
    vectorLayerOptions.setStyle((feature, v) -> createStyle((F) feature, v));
    layer = new ol.layer.Vector(vectorLayerOptions);
    layer.setZIndex(zIndex);
  }

  /**
   * Override to add additional settings to the Vector layer options.
   * @param vectorLayerOptions options to add settings on.
   */
  protected void setAdditionalVectorLayerOptions(final VectorLayerOptions vectorLayerOptions) {
    // Default no-op
  }

  /**
   * @see {@link StyleFunction#createStyle(Feature, double)}
   */
  protected abstract Style[] createStyle(F feature, double resolution);

  /**
   * Initializes the vector layer with the given list of features.
   *
   * @param vectorObject
   * @param features
   */
  protected void setFeatures(final VectorFeaturesWrapper vectorObject, final List<F> features) {
    vectorObject.setFeatures(features);
    setLayerVector(vectorObject.getLayerVector());
  }

  /**
   * Sets the Vector on the layer.
   *
   * @param vector vector to set on the layer
   */
  protected void setLayerVector(final Vector vector) {
    layer.setSource(vector);
  }

  /**
   * Clears all features from the layer.
   */
  protected void clear() {
    final Vector source = layer.getSource();

    if (source != null) {
      source.clear(true);
    }
  }

  /**
   * Triggers a refresh of the layer.
   *
   * @return returns the layer object
   */
  public Source refreshLayer() {
    final Source source = layer.getSource();

    if (source != null) {
      source.changed();
    }
    return source;
  }

  @Override
  public Layer asLayer() {
    return layer;
  }

  @Override
  public LayerInfo getInfo() {
    return info;
  }
}
