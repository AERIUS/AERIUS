/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.nca.pages.calculate.options;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.ApplicationLimits;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsibleButton;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.InputWarningComponent;
import nl.overheid.aerius.wui.application.components.notice.NoticeRule;
import nl.overheid.aerius.wui.application.components.source.EmissionSourceRowComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.GenericValidationContext;
import nl.overheid.aerius.wui.application.context.RoadNetworkContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.domain.calculation.NCACalculationOptions;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.road.RoadEmissionSourceTypes;
import nl.overheid.aerius.wui.application.ui.pages.calculate.CalculateValidations;
import nl.overheid.aerius.wui.application.util.NumberUtil;

@Component(components = {
    ValidationBehaviour.class,
    VerticalCollapse.class,
    VerticalCollapseGroup.class,
    InputWarningComponent.class,
    InputErrorComponent.class,
    EmissionSourceRowComponent.class,
    CollapsibleButton.class,
})
public class CalculationJobDevelopmentPressureSearchComponent extends ErrorWarningValidator {
  private static final int DEFAULT_LIMIT = 100;

  @Prop(required = true) CalculationJobContext jobContext;

  @Inject @Data RoadNetworkContext roadNetworkContext;
  @Inject @Data GenericValidationContext validationContext;

  @Data boolean roadGroupOpen = false;
  @Data int limit = DEFAULT_LIMIT;
  @Data int limitIncrement = 100;

  @Computed
  public List<String> getSelectedEmissionSourceIds() {
    return new ArrayList<>(jobContext.getCalculationOptions().getNcaCalculationOptions().getDevelopmentPressureSourceIds());
  }

  @Computed
  public SituationContext getProposedScenario() {
    return jobContext.getSituationComposition().getSituation(SituationType.PROPOSED);
  }

  @JsMethod
  public boolean hasEmissionSourcesWithoutEmissions() {
    if (getAllEmissionSources().isEmpty()) {
      return false;
    }

    return getProposedScenario().getSources()
        .stream()
        .anyMatch(v -> NumberUtil.equalExactly(v.getEmission(Substance.NOX), 0D) && NumberUtil.equalExactly(v.getEmission(Substance.NH3), 0D));
  }

  @JsMethod
  public List<EmissionSourceFeature> getAllEmissionSources() {
    return Optional.ofNullable(getProposedScenario())
        .map(v -> v.getSources())
        .orElse(new ArrayList<>())
        .stream()
        .filter(src -> src.getEmission(Substance.NOX) > 0D || src.getEmission(Substance.NH3) > 0D)
        .limit(ApplicationLimits.SCENARIO_INPUT_LIST_DRAW_LIMIT)
        .collect(Collectors.toList());
  }

  private Stream<EmissionSourceFeature> getAllRoadSources() {
    return Optional.ofNullable(getProposedScenario())
        .map(v -> v.getSources())
        .orElse(new ArrayList<>())
        .stream()
        .filter(src -> RoadEmissionSourceTypes.isRoadSource(src) && (src.getEmission(Substance.NOX) > 0D || src.getEmission(Substance.NH3) > 0D));
  }

  private Stream<EmissionSourceFeature> getAllVisibleRoadSources() {
    return getAllRoadSources().filter(v -> roadGroupOpen);
  }

  @Computed
  public List<EmissionSourceFeature> getEmissionSources() {
    return getAllEmissionSources().stream()
        .filter(v -> !RoadEmissionSourceTypes.isRoadSource(v))
        .collect(Collectors.toList());
  }

  @Computed
  public List<EmissionSourceFeature> getRoadEmissionSources() {
    return getAllVisibleRoadSources()
        .sorted((o1, o2) -> -Boolean.compare(roadNetworkContext.isHighlighted(o1), roadNetworkContext.isHighlighted(o2)))
        .limit(calculateLimit())
        .collect(Collectors.toList());
  }

  @Computed("hasRoadEmissionSources")
  public boolean hasRoadEmissionSources() {
    return getAllRoadSources().count() > 0;
  }

  @JsMethod
  public void toggleRoadNetwork() {
    roadGroupOpen = !roadGroupOpen;
    limit = Math.max(DEFAULT_LIMIT, roadNetworkContext.highlightedSize() + limitIncrement);
  }

  private long calculateLimit() {
    return Math.max(roadNetworkContext.highlightedSize(), limit);
  }

  @JsMethod
  public void increaseLimit() {
    limit = Math.max(limit, roadNetworkContext.highlightedSize()) + limitIncrement;
  }

  @Computed("areSourcesLimited")
  public boolean areSourcesLimited() {
    return getAllVisibleRoadSources().count() > limit;
  }

  @JsMethod
  public void toggleEmissionSource(final EmissionSourceFeature emissionSource) {
    final String emissionSourceId = emissionSource.getGmlId();
    final List<String> selectedEmissionSourceIds = getSelectedEmissionSourceIds();

    if (selectedEmissionSourceIds.contains(emissionSourceId)) {
      selectedEmissionSourceIds.remove(emissionSourceId);
    } else {
      selectedEmissionSourceIds.add(emissionSourceId);
    }

    getCalculationOptions().setDevelopmentPressureSourceIds(selectedEmissionSourceIds);
    vue().$emit("selectionModified");
  }

  @JsMethod
  public boolean isSelected(final EmissionSourceFeature source) {
    return getSelectedEmissionSourceIds().contains(source.getGmlId());
  }

  private Stream<NoticeRule> getAlwaysVisibleErrors() {
    final SituationComposition comp = jobContext.getSituationComposition();

    return Stream.of(
        NoticeRule.create(
            () -> CalculateValidations.isRequiredTypeMissing(comp, SituationType.PROPOSED),
            () -> i18n.calculateDevelopmentPressureSearchMissingProposed()),
        NoticeRule.create(
            () -> CalculateValidations.hasNoEmissionsInAnyOfComposition(comp),
            () -> i18n.calculateCompositionHasNoEmissions()));
  }

  private Stream<NoticeRule> getSaveOnlyErrors() {
    final SituationComposition comp = jobContext.getSituationComposition();

    return Stream.of(
        NoticeRule.create(
            () -> !CalculateValidations.hasNoEmissionsInAnyOfComposition(comp)
                && !CalculateValidations.hasTooFewSituations(comp)
                && getSelectedEmissionSourceIds().isEmpty(),
            () -> i18n.errorNoSourcesSelectedDevelopmentPressureSearch()));
  }

  @JsMethod
  protected boolean areAlwaysVisibleErrorsShowing() {
    return !getAlwaysVisibleErrorsShowing().isEmpty();
  }

  @JsMethod
  protected boolean areSaveOnlyErrorsShowing() {
    return !getSaveOnlyErrorsShowing().isEmpty() && validationContext.isDisplayFormProblems();
  }

  @JsMethod
  protected List<NoticeRule> getAlwaysVisibleErrorsShowing() {
    return getAlwaysVisibleErrors()
        .filter(v -> v.showing())
        .collect(Collectors.toList());
  }

  @JsMethod
  protected List<NoticeRule> getSaveOnlyErrorsShowing() {
    return getSaveOnlyErrors()
        .filter(v -> v.showing())
        .collect(Collectors.toList());
  }

  private Stream<NoticeRule> getDevelopmentPressureSearchErrors() {
    return Stream.concat(getAlwaysVisibleErrors(), getSaveOnlyErrors());
  }

  @Computed("isInvalid")
  protected boolean isInvalid() {
    return getDevelopmentPressureSearchErrors().anyMatch(v -> v.hasNotice()) || hasChildErrors();
  }

  @Computed("hasWarnings")
  protected boolean hasWarnings() {
    return hasEmissionSourcesWithoutEmissions() || hasChildWarnings();
  }

  private NCACalculationOptions getCalculationOptions() {
    return jobContext.getCalculationOptions().getNcaCalculationOptions();
  }

}
