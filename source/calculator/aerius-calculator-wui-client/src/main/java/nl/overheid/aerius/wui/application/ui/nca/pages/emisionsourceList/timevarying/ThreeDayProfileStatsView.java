/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.nca.pages.emisionsourceList.timevarying;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.wui.application.components.source.DetailStatComponent;
import nl.overheid.aerius.wui.application.domain.source.TimeVaryingProfile;
import nl.overheid.aerius.wui.application.domain.source.util.TimeVaryingProfileUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    DetailStatComponent.class
})
public class ThreeDayProfileStatsView extends BasicVueComponent {
  @Prop TimeVaryingProfile timeVaryingProfile;

  @JsMethod
  public String calculateAverage(final int day) {
    return "" + TimeVaryingProfileUtil.THREE_DAY_PROFILE_UTIL.getThreeDayAverage(timeVaryingProfile.getValues(), day);
  }

  @JsMethod
  public String calculateTotal(final int day) {
    return "" + TimeVaryingProfileUtil.THREE_DAY_PROFILE_UTIL.getTotal(timeVaryingProfile.getValues(), day);
  }

  @Computed
  public String getTotal() {
    return TimeVaryingProfileUtil.THREE_DAY_PROFILE_UTIL.getValuesTotalPreciseText(timeVaryingProfile.getValues());
  }

  @Computed
  public String getAverage() {
    return TimeVaryingProfileUtil.THREE_DAY_PROFILE_UTIL.getValuesAveragePreciseText(timeVaryingProfile.getValues());
  }
}
