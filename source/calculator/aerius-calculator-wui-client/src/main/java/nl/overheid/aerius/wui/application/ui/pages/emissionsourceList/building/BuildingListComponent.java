/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.emissionsourceList.building;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.EventBus;

import elemental2.dom.MouseEvent;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.wui.application.command.building.BuildingCreateNewCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingDeleteAllCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingDeleteSelectedCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingDuplicateSelectedCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingEditSelectedCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingFeatureToggleSelectCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingPeekFeatureCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingUnpeekFeatureCommand;
import nl.overheid.aerius.wui.application.command.source.FeatureZoomCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.modify.ModifyListComponent;
import nl.overheid.aerius.wui.application.context.BuildingListContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.util.DoubleClickUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    VerticalCollapse.class,
    VerticalCollapseGroup.class,
    ModifyListComponent.class,
    BuildingRowComponent.class,
    ButtonIcon.class,
})
public class BuildingListComponent extends BasicVueComponent {
  @Prop EventBus eventBus;

  @Prop SituationContext situation;

  @Data @Inject ScenarioContext scenarioContext;
  @Inject @Data BuildingListContext listContext;

  private final DoubleClickUtil<BuildingFeature> doubleClickUtil = new DoubleClickUtil<>();

  @Computed("hasBuildings")
  public boolean hasBuildings() {
    return !getBuildings().isEmpty();
  }

  @Computed
  public List<BuildingFeature> getBuildings() {
    return Optional.ofNullable(situation)
        .map(v -> v.getBuildings())
        .map(lst -> lst.stream()
            .filter(v -> v.getId() != null)
            .collect(Collectors.toList()))
        .orElse(new ArrayList<>());
  }

  @JsMethod
  public void selectBuilding(final BuildingFeature building) {
    eventBus.fireEvent(new BuildingFeatureToggleSelectCommand(building, false));
  }

  @JsMethod
  public void multiSelectBuilding(final BuildingFeature building) {
    eventBus.fireEvent(new BuildingFeatureToggleSelectCommand(building, true));
  }

  @JsMethod
  public void peekBuilding(final BuildingFeature building) {
    eventBus.fireEvent(new BuildingPeekFeatureCommand(building));
  }

  @JsMethod
  public void unpeekBuilding(final BuildingFeature building) {
    eventBus.fireEvent(new BuildingUnpeekFeatureCommand(building));
  }

  @JsMethod
  public void focusBuilding(final BuildingFeature building) {
    eventBus.fireEvent(new FeatureZoomCommand(building));
  }

  @JsMethod
  public void createNewBuilding() {
    eventBus.fireEvent(new BuildingCreateNewCommand());
  }

  @JsMethod
  public void deleteAllBuildings() {
    if (Window.confirm(i18n.buildingButtonDeleteAllWarning())) {
      eventBus.fireEvent(new BuildingDeleteAllCommand());
    }
  }

  /**
   * Tragically as it turns out, the dblclick event also fires a click event, so
   * we must fabricate that it doesn't.
   */
  @JsMethod
  public void clickBuilding(final BuildingFeature building, final MouseEvent ev) {
    if (ev.ctrlKey) {
      multiSelectBuilding(building);
    } else {
      doubleClickUtil.click(building, this::selectBuilding, this::focusBuilding);
    }
  }

  @JsMethod
  public void editBuilding() {
    eventBus.fireEvent(new BuildingEditSelectedCommand());
  }

  @JsMethod
  public void duplicateBuilding() {
    eventBus.fireEvent(new BuildingDuplicateSelectedCommand());
  }

  @JsMethod
  public void deleteBuilding() {
    eventBus.fireEvent(new BuildingDeleteSelectedCommand());
  }
}
