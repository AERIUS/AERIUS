/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.ImplementedBy;

import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.wui.application.domain.importer.ImportParcel;

@ImplementedBy(ScenarioServiceAsyncImpl.class)
public interface ScenarioServiceAsync {

  /**
   * Import validated situations from the server.
   *
   * @param fileCode file to import
   * @param keepFile indication if file can be removed or should be kept for followup actions.
   * @param callback callback returns if import successful started.
   */
  void importSituation(String fileCode, boolean keepFile, AsyncCallback<ImportParcel> callback);

  /**
   * import an un-validated scenario from the server.
   * @param jobKey job to import
   * @param keepFile when true file will not be deleted from server on import
   * @param callback callback on the scenarioContext retrieved from fileservice.
   */
  void importScenario(String jobKey, boolean keepFile, AsyncCallback<JsPropertyMap<?>> callback);
}
