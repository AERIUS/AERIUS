/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.results;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.Collection;
import ol.layer.Base;
import ol.layer.Group;
import ol.layer.LayerGroupOptions;

import nl.aerius.geo.command.LayerHiddenCommand;
import nl.aerius.geo.command.LayerVisibleCommand;
import nl.aerius.geo.domain.IsLayer;
import nl.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.event.SituationResultsSelectedEvent;
import nl.overheid.aerius.wui.application.geo.layers.LayerFactory;
import nl.overheid.aerius.wui.application.i18n.M;

public class CalculationResultLayerGroup implements IsLayer<Group> {

  private static final CalculationResultLayerGroupEventBinder EVENT_BINDER = GWT.create(CalculationResultLayerGroupEventBinder.class);

  public interface CalculationResultLayerGroupEventBinder extends EventBinder<CalculationResultLayerGroup> {}

  private final Group layer;
  private final LayerInfo info;

  private final CalculationPointResultLayer calculationPointResultLayer;
  private final CalculationResultLayer resultLayer;
  private final EventBus eventBus;

  @Inject
  public CalculationResultLayerGroup(final EventBus eventBus, final LayerFactory layerFactory,
      @Assisted final ApplicationConfiguration appThemeConfig) {

    final Collection<Base> baseLayers = new Collection<>();

    calculationPointResultLayer = layerFactory.createCalculationPointResultLayer();
    baseLayers.push(calculationPointResultLayer.asLayer());

    resultLayer = layerFactory.createCalculationResultLayer(appThemeConfig);
    baseLayers.push(resultLayer.asLayer());

    final LayerGroupOptions groupOptions = new LayerGroupOptions();
    groupOptions.setLayers(baseLayers);

    layer = new Group(groupOptions);

    this.info = new LayerInfo();
    this.info.setName(this.getClass().getCanonicalName());
    this.info.setTitle(M.messages().layerResults());

    this.eventBus = eventBus;
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  public void handle(final SituationResultsSelectedEvent e) {
    if (e.getValue().getHexagonType() == SummaryHexagonType.CUSTOM_CALCULATION_POINTS) {
      info.setLegend(calculationPointResultLayer.getInfo().getLegend());
      eventBus.fireEvent(new LayerHiddenCommand(resultLayer));
      eventBus.fireEvent(new LayerVisibleCommand(calculationPointResultLayer));
    } else {
      info.setLegend(resultLayer.getInfo().getLegend());
      eventBus.fireEvent(new LayerHiddenCommand(calculationPointResultLayer));
      eventBus.fireEvent(new LayerVisibleCommand(resultLayer));
    }
  }

  @Override
  public Group asLayer() {
    return layer;
  }

  @Override
  public LayerInfo getInfo() {
    return info;
  }
}
