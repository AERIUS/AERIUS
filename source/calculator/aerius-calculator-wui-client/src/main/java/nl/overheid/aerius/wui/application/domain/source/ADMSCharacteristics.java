/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.ADMSLimits;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.BuoyancyType;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.EffluxType;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.ReleaseTemperatureAndPressure;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.SourceType;
import nl.overheid.aerius.wui.vue.ReactivityUtil;
import nl.overheid.aerius.wui.vue.SkipReactivityValidations;

/**
 * Client side implementation of ADMS props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class ADMSCharacteristics extends SourceCharacteristics {
  private String sourceType;
  private String buoyancyType;
  private String effluxType;
  private String releaseTemperatureAndPressure;
  private double height;
  private double width;
  private double diameter;
  private double temperature;
  private double density;
  private double verticalVelocity;
  private double volumetricFlowRate;
  private double specificHeatCapacity;
  private double percentNOxAsNO2;
  private double momentumFlux;
  private double buoyancyFlux;
  private double massFlux;
  private double verticalDimension;
  private double elevationAngle; // Angle 1
  private double horizontalAngle; // Angle 2

  private String customHourlyTimeVaryingProfileId;
  private String standardHourlyTimeVaryingProfileCode;

  private String customMonthlyTimeVaryingProfileId;
  private String standardMonthlyTimeVaryingProfileCode;

  public static final @JsOverlay ADMSCharacteristics create() {
    final ADMSCharacteristics props = Js.uncheckedCast(JsPropertyMap.of());
    initADMS(props);
    return props;
  }

  public static final @JsOverlay void initADMS(final ADMSCharacteristics props) {
    SourceCharacteristics.init(props, CharacteristicsType.ADMS);
    ReactivityUtil.ensureDefault(props::getSourceType, props::setSourceType, SourceType.POINT);
    ReactivityUtil.ensureDefault(props::getBuoyancyType, props::setBuoyancyType, BuoyancyType.TEMPERATURE);
    ReactivityUtil.ensureDefault(props::getEffluxType, props::setEffluxType, EffluxType.VELOCITY);
    ReactivityUtil.ensureDefault(props::getReleaseTemperatureAndPressure, props::setReleaseTemperatureAndPressure, ReleaseTemperatureAndPressure.NTP);
    ReactivityUtil.ensureJsProperty(props, "height", props::setHeight, ADMSLimits.SOURCE_HEIGHT_DEFAULT);
    ReactivityUtil.ensureJsProperty(props, "width", props::setWidth, (double) ADMSLimits.SOURCE_WIDTH_INITIAL);
    ReactivityUtil.ensureJsProperty(props, "diameter", props::setDiameter, ADMSLimits.SOURCE_HEIGHT_MINIMUM);
    ReactivityUtil.ensureJsProperty(props, "temperature", props::setTemperature, (double) ADMSLimits.SOURCE_TEMPERATURE_DEFAULT);
    ReactivityUtil.ensureJsProperty(props, "density", props::setDensity, ADMSLimits.SOURCE_DENSITY_DEFAULT);
    ReactivityUtil.ensureJsProperty(props, "verticalVelocity", props::setVerticalVelocity, ADMSLimits.SOURCE_VERTICAL_VELOCITY_DEFAULT);
    ReactivityUtil.ensureJsProperty(props, "volumetricFlowRate", props::setVolumetricFlowRate, ADMSLimits.SOURCE_VOLUMETRIC_FLOW_RATE_DEFAULT);
    ReactivityUtil.ensureJsProperty(props, "specificHeatCapacity", props::setSpecificHeatCapacity, ADMSLimits.SOURCE_SPECIFIC_HEAT_CAPACITY_DEFAULT);
    ReactivityUtil.ensureJsProperty(props, "percentNOxAsNO2", props::setPercentNOxAsNO2, ADMSLimits.SOURCE_PERCENT_NOX_AS_NO2_DEFAULT);
    ReactivityUtil.ensureJsProperty(props, "momentumFlux", props::setMomentumFlux, ADMSLimits.SOURCE_MOMENTUM_FLUX_DEFAULT);
    ReactivityUtil.ensureJsProperty(props, "buoyancyFlux", props::setBuoyancyFlux, ADMSLimits.SOURCE_BUOYANCY_FLUX_DEFAULT);
    ReactivityUtil.ensureJsProperty(props, "massFlux", props::setMassFlux, ADMSLimits.SOURCE_MASS_FLUX_DEFAULT);
    ReactivityUtil.ensureJsProperty(props, "verticalDimension", props::setVerticalDimension, (double) ADMSLimits.SOURCE_L1_DEFAULT);
    ReactivityUtil.ensureJsProperty(props, "elevationAngle", props::setElevationAngle, ADMSLimits.ELEVATION_ANGLE_DEFAULT);
    ReactivityUtil.ensureJsProperty(props, "horizontalAngle", props::setHorizontalAngle, ADMSLimits.HORIZONTAL_ANGLE_DEFAULT);

    ReactivityUtil.ensureDefault(props::getCustomHourlyTimeVaryingProfileId, props::setCustomHourlyTimeVaryingProfileId);
    ReactivityUtil.ensureDefault(props::getStandardHourlyTimeVaryingProfileCode, props::setStandardHourlyTimeVaryingProfileCode);
    ReactivityUtil.ensureDefault(props::getCustomMonthlyTimeVaryingProfileId, props::setCustomMonthlyTimeVaryingProfileId);
    ReactivityUtil.ensureDefault(props::getStandardMonthlyTimeVaryingProfileCode, props::setStandardMonthlyTimeVaryingProfileCode);
  }

  public final @JsOverlay SourceType getSourceType() {
    return SourceType.safeValueOf(sourceType);
  }

  public final @JsOverlay void setSourceType(final SourceType sourceType) {
    this.sourceType = sourceType == null ? null : sourceType.name();
  }

  public final @JsOverlay void setSourceType(final String sourceType) {
    this.sourceType = sourceType;
  }

  public final @JsOverlay BuoyancyType getBuoyancyType() {
    return BuoyancyType.safeValueOf(buoyancyType);
  }

  public final @JsOverlay void setBuoyancyType(final BuoyancyType buoyancyType) {
    this.buoyancyType = buoyancyType == null ? null : buoyancyType.name();
  }

  public final @JsOverlay void setBuoyancyType(final String buoyancyType) {
    this.buoyancyType = buoyancyType;
  }

  public final @JsOverlay EffluxType getEffluxType() {
    return EffluxType.safeValueOf(effluxType);
  }

  public final @JsOverlay void setEffluxType(final EffluxType effluxType) {
    this.effluxType = effluxType == null ? null : effluxType.name();
  }

  public final @JsOverlay ReleaseTemperatureAndPressure getReleaseTemperatureAndPressure() {
    return ReleaseTemperatureAndPressure.safeValueOf(releaseTemperatureAndPressure);
  }

  public final @JsOverlay void setReleaseTemperatureAndPressure(final ReleaseTemperatureAndPressure releaseTemperatureAndPressure) {
    this.releaseTemperatureAndPressure = releaseTemperatureAndPressure == null ? null : releaseTemperatureAndPressure.name();
  }

  public final @JsOverlay double getHeight() {
    return height;
  }

  public final @JsOverlay void setHeight(final double height) {
    this.height = height;
  }

  public final @JsOverlay double getDiameter() {
    return diameter;
  }

  public final @JsOverlay void setDiameter(final double diameter) {
    this.diameter = diameter;
  }

  public final @JsOverlay double getTemperature() {
    return temperature;
  }

  public final @JsOverlay void setTemperature(final double temperature) {
    this.temperature = temperature;
  }

  public final @JsOverlay double getDensity() {
    return density;
  }

  public final @JsOverlay void setDensity(final double density) {
    this.density = density;
  }

  public final @JsOverlay double getVerticalVelocity() {
    return verticalVelocity;
  }

  public final @JsOverlay void setVerticalVelocity(final double verticalVelocity) {
    this.verticalVelocity = verticalVelocity;
  }

  public final @JsOverlay double getVolumetricFlowRate() {
    return volumetricFlowRate;
  }

  public final @JsOverlay void setVolumetricFlowRate(final double volumetricFlowRate) {
    this.volumetricFlowRate = volumetricFlowRate;
  }

  public final @JsOverlay double getSpecificHeatCapacity() {
    return specificHeatCapacity;
  }

  public final @JsOverlay void setSpecificHeatCapacity(final double specificHeatCapacity) {
    this.specificHeatCapacity = specificHeatCapacity;
  }

  public final @JsOverlay double getPercentNOxAsNO2() {
    return percentNOxAsNO2;
  }

  public final @JsOverlay void setPercentNOxAsNO2(final double percentNOxAsNO2) {
    this.percentNOxAsNO2 = percentNOxAsNO2;
  }

  public final @JsOverlay double getMomentumFlux() {
    return momentumFlux;
  }

  public final @JsOverlay void setMomentumFlux(final double momentumFlux) {
    this.momentumFlux = momentumFlux;
  }

  public final @JsOverlay double getBuoyancyFlux() {
    return buoyancyFlux;
  }

  public final @JsOverlay void setBuoyancyFlux(final double buoyancyFlux) {
    this.buoyancyFlux = buoyancyFlux;
  }

  public final @JsOverlay double getMassFlux() {
    return massFlux;
  }

  public final @JsOverlay void setMassFlux(final double massFlux) {
    this.massFlux = massFlux;
  }

  public final @JsOverlay double getVerticalDimension() {
    return verticalDimension;
  }

  public final @JsOverlay void setVerticalDimension(final double verticalDimension) {
    this.verticalDimension = verticalDimension;
  }

  public final @JsOverlay double getHorizontalAngle() {
    return horizontalAngle;
  }

  public final @JsOverlay void setHorizontalAngle(final double horizontalAngle) {
    this.horizontalAngle = horizontalAngle;
  }

  public final @JsOverlay double getElevationAngle() {
    return elevationAngle;
  }

  public final @JsOverlay void setElevationAngle(final double elevationAngle) {
    this.elevationAngle = elevationAngle;
  }

  public final @JsOverlay double getWidth() {
    return width;
  }

  public final @JsOverlay void setWidth(final double width) {
    this.width = width;
  }

  @SkipReactivityValidations
  public final @JsOverlay boolean isHourlyTimeVaryingProfileAttached() {
    return customHourlyTimeVaryingProfileId != null;
  }

  public final @JsOverlay String getCustomHourlyTimeVaryingProfileId() {
    return customHourlyTimeVaryingProfileId;
  }

  public final @JsOverlay void setCustomHourlyTimeVaryingProfileId(final String id) {
    customHourlyTimeVaryingProfileId = id;
  }

  public final @JsOverlay String getStandardHourlyTimeVaryingProfileCode() {
    return standardHourlyTimeVaryingProfileCode;
  }

  public final @JsOverlay void setStandardHourlyTimeVaryingProfileCode(final String standardHourlyTimeVaryingProfileCode) {
    this.standardHourlyTimeVaryingProfileCode = standardHourlyTimeVaryingProfileCode;
  }

  @SkipReactivityValidations
  public final @JsOverlay boolean isMonthlyTimeVaryingProfileAttached() {
    return customMonthlyTimeVaryingProfileId != null;
  }

  public final @JsOverlay String getCustomMonthlyTimeVaryingProfileId() {
    return customMonthlyTimeVaryingProfileId;
  }

  public final @JsOverlay void setCustomMonthlyTimeVaryingProfileId(final String id) {
    customMonthlyTimeVaryingProfileId = id;
  }

  public final @JsOverlay String getStandardMonthlyTimeVaryingProfileCode() {
    return standardMonthlyTimeVaryingProfileCode;
  }

  public final @JsOverlay void setStandardMonthlyTimeVaryingProfileCode(final String standardMonthlyTimeVaryingProfileCode) {
    this.standardMonthlyTimeVaryingProfileCode = standardMonthlyTimeVaryingProfileCode;
  }
}
