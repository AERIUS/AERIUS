/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.manure.custom;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.AnimalType;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.manure.base.ManureStorageBaseEmissionComponent;
import nl.overheid.aerius.wui.application.components.source.manure.custom.ManureStorageCustomValidators.ManureStorageCustomValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.manure.CustomManureStorage;
import nl.overheid.aerius.wui.vue.DebugDirective;

@Component(customizeOptions = ManureStorageCustomValidators.class, directives = {
    ValidateDirective.class,
    DebugDirective.class
}, components = {
    LabeledInputComponent.class,
    ValidationBehaviour.class,
    VerticalCollapse.class,
    ManureStorageBaseEmissionComponent.class
})
public class ManureStorageCustomEmissionComponent extends ErrorWarningValidator implements HasCreated, HasValidators<ManureStorageCustomValidations> {
  @Prop @JsProperty CustomManureStorage customManureStorage;

  @Data String descriptionV;
  @Data String emissionFactorV;
  @Data @JsProperty FarmEmissionFactorType farmEmissionFactorTypeV = FarmEmissionFactorType.PER_TONNES_PER_YEAR;

  @JsProperty(name = "$v") ManureStorageCustomValidations validation;

  @Inject @Data ApplicationContext applicationContext;

  @Override
  @Computed
  public ManureStorageCustomValidations getV() {
    return validation;
  }

  @Watch(value = "customManureStorage", isImmediate = true)
  public void onCustomManureStorageChange() {
    descriptionV = String.valueOf(customManureStorage.getDescription());
    emissionFactorV = String.valueOf(customManureStorage.getEmissionFactor(Substance.NH3));
    setFarmEmissionFactorType(customManureStorage.getFarmEmissionFactorType() != null
        ? customManureStorage.getFarmEmissionFactorType().name()
        : FarmEmissionFactorType.PER_TONNES_PER_YEAR.name());
    validation.$reset();
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Computed
  protected String getDescription() {
    return descriptionV;
  }

  @Computed
  protected void setDescription(final String description) {
    customManureStorage.setDescription(description);
    descriptionV = description;
  }

  @Computed
  protected String getAnimalType() {
    return AnimalType.getByCode(customManureStorage.getAnimalCode()).getAnimalCode();
  }

  @Computed
  protected void setAnimalType(final String animalType) {
    customManureStorage.setAnimalCode(AnimalType.getByCode(animalType).getAnimalCode());
  }

  @Computed
  protected String getFarmEmissionFactorType() {
    return this.farmEmissionFactorTypeV.name();
  }

  @Computed
  protected void setFarmEmissionFactorType(final String farmEmissionFactorType) {
    this.farmEmissionFactorTypeV = FarmEmissionFactorType.valueOf(farmEmissionFactorType);
    this.customManureStorage.setFarmEmissionFactorType(farmEmissionFactorTypeV);
  }

  @Computed
  protected String getEmissionFactor() {
    return emissionFactorV;
  }

  @Computed
  protected void setEmissionFactor(final String emissionFactor) {
    ValidationUtil.setSafeDoubleValue(ef -> customManureStorage.setEmissionFactor(Substance.NH3, ef), emissionFactor, 0D);
    this.emissionFactorV = emissionFactor;
  }

  @Computed
  protected List<FarmEmissionFactorType> getFarmEmissionFactorTypes() {
    return applicationContext.getConfiguration().getCustomManureStorageEmissionFactorTypes();
  }

  @JsMethod
  protected String emissionFactorConversionError() {
    return i18n.errorDecimal(emissionFactorV);
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return validation.descriptionV.invalid
        || validation.emissionFactorV.invalid
        || validation.farmEmissionFactorTypeV.invalid;
  }
}
