/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.offroad.standard;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.aerius.vuelidate.ValidationOptions;
import nl.aerius.vuelidate.Validations;
import nl.aerius.vuelidate.ValidatorBuilder;

/**
 * Validators for OffRoadMobileStandard editing.
 */
class OffRoadMobileStandardValidators extends ValidationOptions<OffRoadMobileStandardEmissionComponent> {

  @JsType(isNative = true, namespace = JsPackage.GLOBAL)
  static class OffRoadMobileStandardValidations extends Validations {
    public @JsProperty Validations descriptionV;
    public @JsProperty Validations selectedCategoryV;
    public @JsProperty Validations literFuelV;
    public @JsProperty Validations operatingHoursV;
    public @JsProperty Validations literAdBlueV;
  }

  @Override
  protected void constructValidators(final Object options, final ValidatorInstaller v) {
    final OffRoadMobileStandardEmissionComponent instance = Js.uncheckedCast(options);
    v.install("descriptionV", () -> instance.descriptionV = null, ValidatorBuilder.create().required());
    v.install("selectedCategoryV", () -> instance.selectedCategory = null, ValidatorBuilder.create().required());
    v.install("literFuelV", () -> instance.literFuelV = null, ValidatorBuilder.create()
        .integer()
        .minValue(1)
        .required());
    v.install("operatingHoursV", () -> instance.operatingHoursV = null, ValidatorBuilder.create()
        .integer()
        .minValue(1)
        .required());
    v.install("literAdBlueV", () -> instance.literAdBlueV = null, ValidatorBuilder.create()
        .integer()
        .minValue(0));
  }
}
