/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.source.farmlodging.systems.types;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Emit;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.sector.category.FarmAdditionalLodgingSystemCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategories;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingSystemDefinition;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.MinimalInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.input.suggest.SuggestListBoxComponent;
import nl.overheid.aerius.wui.application.components.input.suggest.SuggestListBoxData;
import nl.overheid.aerius.wui.application.components.input.suggest.SuggestUtil;
import nl.overheid.aerius.wui.application.components.source.farmlodging.systems.types.FarmAdditionalLodgingValidators.FarmAdditionalLodgingValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.components.tooltip.TooltipComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.farm.AdditionalLodgingSystem;

@Component(customizeOptions = {
    FarmAdditionalLodgingValidators.class
}, directives = {
    ValidateDirective.class,
}, components = {
    ButtonIcon.class,
    MinimalInputComponent.class,
    SuggestListBoxComponent.class,
    SimplifiedListBoxComponent.class,
    TooltipComponent.class,
    ValidationBehaviour.class,
    InputErrorComponent.class,
})
public class FarmAdditionalLodgingSystemsRowComponent extends ErrorWarningValidator
    implements HasCreated, HasValidators<FarmAdditionalLodgingValidations> {
  @Prop(required = true) @JsProperty FarmLodgingCategory farmLodgingCategory;
  @Prop(required = true) @JsProperty AdditionalLodgingSystem lodgingSystem;

  @Prop int idx;

  @Data @JsProperty List<SuggestListBoxData> lodgingSystemOptions = new ArrayList<>();
  @Data @JsProperty List<FarmLodgingSystemDefinition> systemDefinitionOptions = new ArrayList<>();

  @Data @Inject ApplicationContext applicationContext;

  @Data String farmLodgingCodeV;
  @Data String numberOfAnimalsV;
  @Data String systemDefinitionCodeV;

  @JsProperty(name = "$v") FarmAdditionalLodgingValidations validation;

  @Data FarmAdditionalLodgingSystemCategory farmAdditionalLodgingSystemCategory;

  @Override
  @Computed
  public FarmAdditionalLodgingValidations getV() {
    return validation;
  }

  @PropDefault("idx")
  int idxDefault() {
    return 0;
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @JsMethod
  public FarmLodgingCategories getCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getFarmLodgingCategories();
  }

  @Watch(value = "lodgingSystem", isImmediate = true)
  public void onSourceChange(final AdditionalLodgingSystem neww) {
    if (neww == null) {
      farmAdditionalLodgingSystemCategory = null;
      farmLodgingCodeV = null;
      numberOfAnimalsV = null;
      systemDefinitionCodeV = null;
      return;
    }

    systemDefinitionCodeV = neww.getSystemDefinitionCode();
    farmLodgingCodeV = neww.getLodgingSystemCode();
    numberOfAnimalsV = String.valueOf(neww.getNumberOfAnimals());
    updateFarmLodgingSystemFromCode(neww.getLodgingSystemCode());
  }

  private void updateFarmLodgingSystemFromCode(final String code) {
    farmAdditionalLodgingSystemCategory = getCategories().getFarmAdditionalLodgingSystemCategories().stream()
        .filter(v -> v.getCode().equals(code))
        .findFirst().orElse(null);
  }

  @Watch(value = "farmLodgingCategory", isImmediate = true)
  public void onFarmLodgingCategoryChange(final FarmLodgingCategory neww) {
    if (neww == null) {
      lodgingSystemOptions.clear();
      return;
    }

    lodgingSystemOptions = getCategories().getFarmAdditionalLodgingSystemCategories().stream()
        .map(v -> new SuggestListBoxData(v.getCode(), v.getName(), v.getName() + " " + v.getDescription(),
            neww.canStackAdditionalLodgingSystemCategory(v)))
        .sorted(SuggestUtil.WARN)
        .collect(Collectors.toList());
  }

  @Watch(value = "farmAdditionalLodgingSystemCategory", isImmediate = true)
  void onFarmAdditionalLodgingSystemCategory(final FarmAdditionalLodgingSystemCategory neww) {
    if (neww == null) {
      systemDefinitionOptions = new ArrayList<>();
      farmLodgingCodeV = null;
    } else {
      systemDefinitionOptions = neww.getFarmLodgingSystemDefinitions();
      farmLodgingCodeV = neww.getCode();
    }

    // If there is only one option in the new set of system definitions, select it
    if (systemDefinitionOptions.size() == 1) {
      selectSystemDefinitionCode(systemDefinitionOptions.get(0));
    } else if (!systemDefinitionOptions.stream()
        .anyMatch(v -> v.getCode().equals(lodgingSystem.getSystemDefinitionCode()))) {
      // If the selected system definition code does not exist in the new list,
      // deselect it
      selectSystemDefinitionCode(null);
    }
  }

  @Computed
  public void setNumberOfAnimals(final String numberOfAnimals) {
    ValidationUtil.setSafeIntegerValue(v -> lodgingSystem.setNumberOfAnimals(v), numberOfAnimals, 0);
    numberOfAnimalsV = numberOfAnimals;
  }

  @Computed
  public String getNumberOfAnimals() {
    return numberOfAnimalsV;
  }

  @JsMethod
  public String numberOfAnimalsError() {
    return i18n.errorNumeric(numberOfAnimalsV);
  }

  @JsMethod
  @Emit(value = "delete")
  public void deleteRow() {}

  @JsMethod
  public void unselectLodgingSystemCode() {
    lodgingSystem.setLodgingSystemCode(null);
    updateFarmLodgingSystemFromCode(null);
  }

  @JsMethod
  public void selectLodgingSystemCode(final String code) {
    lodgingSystem.setLodgingSystemCode(code);
    updateFarmLodgingSystemFromCode(code);
  }

  @JsMethod
  public void selectSystemDefinitionCode(final FarmLodgingSystemDefinition value) {
    if (value == null) {
      lodgingSystem.setSystemDefinitionCode(null);
      systemDefinitionCodeV = null;
    } else {
      lodgingSystem.setSystemDefinitionCode(value.getCode());
      systemDefinitionCodeV = value.getCode();
    }
  }

  @Computed("systemDefinitionCodeError")
  public boolean systemDefinitionCodeError() {
    return displayProblems && systemDefinitionCodeInvalid();
  }

  @Computed("systemDefinitionCodeInvalid")
  public boolean systemDefinitionCodeInvalid() {
    // If there are no options, system definition is not required.
    return !systemDefinitionOptions.isEmpty() && validation.systemDefinitionCodeV.invalid;
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return validation.farmLodgingCodeV.invalid
        || validation.numberOfAnimalsV.invalid
        || systemDefinitionCodeInvalid();
  }
}
