/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.context;

import javax.inject.Singleton;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.wui.application.domain.calculationpoint.CalculationPointEntityReference;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;

@Singleton
public class CalculationPointEditorContext {
  private CalculationPointFeature originalCalculationPoint = null;
  private CalculationPointFeature calculationPoint = null;
  private @JsProperty CalculationPointEntityReference selectedHabitat = null;

  public CalculationPointFeature getOriginalCalculationPoint() {
    return originalCalculationPoint;
  }

  public void setOriginalCalculationPoint(final CalculationPointFeature originalCalculationPoint) {
    this.originalCalculationPoint = originalCalculationPoint;
  }

  public CalculationPointFeature getCalculationPoint() {
    return calculationPoint;
  }

  public void setCalculationPoint(final CalculationPointFeature calculationPoint) {
    this.calculationPoint = calculationPoint;
  }

  public CalculationPointEntityReference getSelectedHabitat() {
    return selectedHabitat;
  }

  public void selectHabitat(final CalculationPointEntityReference selectedHabitat) {
    this.selectedHabitat = selectedHabitat;
  }

  public void createNewCalculationPoint(final CalculationPointFeature calculationPoint) {
    setCalculationPoint(calculationPoint);
    setOriginalCalculationPoint(null);
  }

  public void editCalculationPoint(final CalculationPointFeature calculationPoint, final CalculationPointFeature original) {
    setCalculationPoint(calculationPoint);
    setOriginalCalculationPoint(original);
  }

  public boolean isEditing() {
    return calculationPoint != null;
  }

  public boolean hasOriginal() {
    return originalCalculationPoint != null;
  }

  public void reset() {
    calculationPoint = null;
    originalCalculationPoint = null;
  }

}
