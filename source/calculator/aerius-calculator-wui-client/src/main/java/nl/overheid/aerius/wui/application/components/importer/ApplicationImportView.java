/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.importer;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.Vue;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasDestroyed;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;
import com.google.web.bindery.event.shared.binder.GenericEvent;

import elemental2.dom.DomGlobal;
import elemental2.dom.Element;
import elemental2.dom.Event;
import elemental2.dom.File;
import elemental2.dom.HTMLInputElement;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.directives.VectorDirective;
import nl.aerius.wui.vue.transition.HorizontalCollapse;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.RequestMappings;
import nl.overheid.aerius.wui.application.command.importer.DownloadValidationReportCommand;
import nl.overheid.aerius.wui.application.command.importer.ImportFilesCommand;
import nl.overheid.aerius.wui.application.command.importer.RegisterFileCommand;
import nl.overheid.aerius.wui.application.command.importer.UnregisterFileCommand;
import nl.overheid.aerius.wui.application.command.importer.UnregisterFilesCommand;
import nl.overheid.aerius.wui.application.command.source.SituationCreateNewCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.importer.errors.FilesErrors;
import nl.overheid.aerius.wui.application.components.importer.errors.FilesWarnings;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.modify.ModifyCancelSaveComponent;
import nl.overheid.aerius.wui.application.components.notification.FloatingUtilityComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.GenericValidationContext;
import nl.overheid.aerius.wui.application.context.importer.FileContext;
import nl.overheid.aerius.wui.application.context.importer.ImportContext;
import nl.overheid.aerius.wui.application.daemon.misc.PersistablePreferencesContext;
import nl.overheid.aerius.wui.application.event.ImportCompleteEvent;
import nl.overheid.aerius.wui.application.event.ImportFailureEvent;
import nl.overheid.aerius.wui.application.ui.main.FadeTransition;
import nl.overheid.aerius.wui.application.ui.pages.preferences.PreferencesView.ImportMode;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;

@Component(components = {
    FilesListComponent.class,
    VerticalCollapse.class,
    HorizontalCollapse.class,
    FadeTransition.class,
    FilesWarnings.class,
    FilesErrors.class,
    ButtonIcon.class,
    InputErrorComponent.class,
    ModifyCancelSaveComponent.class,
    FloatingUtilityComponent.class,
}, directives = {
    VectorDirective.class,
})
public class ApplicationImportView extends ErrorWarningValidator implements HasCreated, HasDestroyed {
  private static final ApplicationImportViewEventBinder EVENT_BINDER = GWT.create(ApplicationImportViewEventBinder.class);

  interface ApplicationImportViewEventBinder extends EventBinder<ApplicationImportView> {}

  public enum Tab {
    FILES, WARNINGS, ERRORS,
  }

  @Inject @Data ApplicationContext appContext;
  @Inject @Data ImportContext importContext;
  @Inject @Data FileContext fileContext;
  @Inject @Data GenericValidationContext validationContext;
  @Inject @Data PersistablePreferencesContext prefContext;

  @Inject EnvironmentConfiguration cfg;

  @Prop EventBus eventBus;

  @Data Tab tab = Tab.FILES;
  @Data boolean isDragging;
  @Data boolean isImporting;
  @Data boolean focusFileInput;
  private HandlerRegistration handlers;

  @Computed
  public String getAcceptedFileTypes() {
    return appContext.getConfiguration().getAcceptedImportFileTypes().stream()
        .collect(Collectors.joining(","));
  }

  @Computed("isAdvanced")
  public boolean isAdvanced() {
    return prefContext.getImportMode() == ImportMode.ADVANCED;
  }

  @JsMethod
  public void onFocus() {
    focusFileInput = true;
  }

  @JsMethod
  public void onBlur() {
    focusFileInput = false;
  }

  @JsMethod
  public void onCreateNewSituationClick() {
    eventBus.fireEvent(new SituationCreateNewCommand());
  }

  @JsMethod
  public void onOpenFileInputClick() {
    tab = Tab.FILES;
    Vue.nextTick(() -> click(DomGlobal.document.getElementById("fileInput")));
  }

  private static native void click(final Element element) /*-{
      return element.click();
  }-*/;

  @Computed("isEmpty")
  public boolean isEmpty() {
    return fileContext.getFiles().isEmpty();
  }

  @Computed("hasErrors")
  public boolean hasErrors() {
    return getErrorCount() > 0;
  }

  @Computed
  public int getErrorCount() {
    return (int) Stream.concat(fileContext.getFiles().stream().flatMap(v -> v.getErrors().stream()), fileContext.getErrors().stream())
        .count();
  }

  @Computed
  public String getErrorCountText() {
    final int count = getErrorCount();
    return count >= RequestMappings.MAX_LIST_SIZE_SANITY
        ? ">" + RequestMappings.MAX_LIST_SIZE_SANITY
        : String.valueOf(count);
  }

  @Computed("hasWarnings")
  public boolean hasWarnings() {
    return getWarningCount() > 0;
  }

  @Computed
  public int getWarningCount() {
    return (int) Stream.concat(fileContext.getFiles().stream().flatMap(v -> v.getWarnings().stream()), fileContext.getWarnings().stream())
        .count();
  }

  @Computed
  public String getWarningCountText() {
    final int count = getWarningCount();
    return count >= RequestMappings.MAX_LIST_SIZE_SANITY
        ? ">" + RequestMappings.MAX_LIST_SIZE_SANITY
        : String.valueOf(count);
  }

  @JsMethod
  public void removeFile(final FileUploadStatus item) {
    eventBus.fireEvent(new UnregisterFileCommand(item));
  }

  @Computed("hasFiles")
  public boolean hasFiles() {
    return !isEmpty();
  }

  @Computed("isValidating")
  public boolean isValidating() {
    return fileContext.getFiles().stream()
        .allMatch(v -> !v.isUserInput() && !v.isValidated());
  }

  @Computed("isUploading")
  public boolean isUploading() {
    return fileContext.getFiles().stream()
        .anyMatch(v -> !v.isComplete() && !v.isFailed() && !v.isUserInput());
  }

  @JsMethod
  public void submit() {
    isImporting = true;
    eventBus.fireEvent(new ImportFilesCommand());
  }

  @EventHandler(handles = {ImportFailureEvent.class, ImportCompleteEvent.class})
  public void onImportFinishEvent(final GenericEvent e) {
    isImporting = false;
  }

  @JsMethod
  public void removeAll() {
    if (Window.confirm(i18n.importMayCloseConfirm())) {
      eventBus.fireEvent(new UnregisterFilesCommand(fileContext.getFiles()));
      setNotificationState(false);
      fileContext.reset();
      importContext.reset();
      tab = Tab.FILES;
    }
  }

  @JsMethod
  public void setNotificationState(final boolean value) {
    displayProblems(value);
    validationContext.setDisplayFormProblems(value);
  }

  @JsMethod
  public void closeError() {
    setNotificationState(false);
  }

  @JsMethod
  public void filesChange(final Event e) {
    final HTMLInputElement input = (HTMLInputElement) e.target;
    for (int i = 0; i < input.files.length; i++) {
      handleFileUpload(input, i);
    }

    // Clear the input value. The browser performs extra validation which may bother
    // us the next time the input field is used.
    input.value = "";
    isDragging = false;
    tab = Tab.FILES;
  }

  private void handleFileUpload(final HTMLInputElement input, final int index) {
    final File file = input.files.item(index);

    final FileUploadStatus fileStatus = asFileUploadStatus(file);
    addFile(fileStatus);
  }

  private FileUploadStatus asFileUploadStatus(final File file) {
    final FileUploadStatus fileUploadStatus = new FileUploadStatus();
    fileUploadStatus.setFile(file);
    return fileUploadStatus;
  }

  private void addFile(final FileUploadStatus fileUploadStatus) {
    eventBus.fireEvent(new RegisterFileCommand(fileUploadStatus));
  }

  @JsMethod
  public void downloadLog() {
    eventBus.fireEvent(new DownloadValidationReportCommand());
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return fileContext.getFiles().isEmpty()
        || hasChildErrors();
  }

  @Override
  public void created() {
    handlers = EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Override
  public void destroyed() {
    if (handlers != null) {
      handlers.removeHandler();
    }
  }
}
