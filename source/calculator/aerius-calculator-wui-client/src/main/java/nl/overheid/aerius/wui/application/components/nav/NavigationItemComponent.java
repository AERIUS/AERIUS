/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.nav;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.place.Place;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.tooltip.TooltipComponent;
import nl.overheid.aerius.wui.application.daemon.misc.PersistablePreferencesContext;
import nl.overheid.aerius.wui.vue.DebugDirective;

@Component(components = {
    ButtonIcon.class,
    TooltipComponent.class
}, directives = {
    DebugDirective.class,
})
public class NavigationItemComponent implements IsVueComponent {
  @Prop EventBus eventBus;

  @Prop NavigationItem item;

  @Prop Place place;

  @Inject @Data PersistablePreferencesContext preferencesContext;

  @Computed("isEnabled")
  public boolean isEnabled() {
    return !item.isPermanentlyDisabled().get() && item.isEnabled().get();
  }

  @Computed("isActive")
  public boolean isActive() {
    return item.isActive().test(place);
  }

  @Computed("isCollapsed")
  public boolean isCollapsed() {
    return preferencesContext.isCollapsedMenu();
  }

  @Computed("isLeftLabel")
  public boolean isLeftLabel() {
    return item.isLabelLeft();
  }

  @Computed
  public String getAppropriateElement() {
    return item.isEnabled().get() ? "a" : "div";
  }

  @JsMethod
  public String toMachineReadable() {
    return item.getIcon().toUpperCase();
  }

  @JsMethod
  public void click() {
    if (!isEnabled()) {
      return;
    }

    item.getAction().accept(item);
  }
}
