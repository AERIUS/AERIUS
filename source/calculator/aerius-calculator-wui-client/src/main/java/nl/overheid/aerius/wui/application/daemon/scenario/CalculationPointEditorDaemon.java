/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.daemon.scenario;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.place.PlaceController;
import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.v2.point.AssessmentCategory;
import nl.overheid.aerius.shared.util.ConsecutiveIdUtil;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointAddCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointCreateNewCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointDeleteCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointEditCancelCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointEditCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointResetCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointSaveCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.TemporaryCalculationPointAddCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.TemporaryCalculationPointClearCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.CalculationPointEditorContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.daemon.misc.PersistablePreferencesContext;
import nl.overheid.aerius.wui.application.domain.ClientConsecutiveIdUtil;
import nl.overheid.aerius.wui.application.domain.geo.FeatureUtil;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;
import nl.overheid.aerius.wui.application.domain.source.NcaCustomCalculationPointFeature;
import nl.overheid.aerius.wui.application.event.calculationpoint.CalculationPointChangeEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.place.CalculationPointsPlace;

@Singleton
public class CalculationPointEditorDaemon extends BasicEventComponent implements Daemon {
  private static final CalculationPointEditorDaemonEventBinder EVENT_BINDER = GWT.create(CalculationPointEditorDaemonEventBinder.class);

  interface CalculationPointEditorDaemonEventBinder extends EventBinder<CalculationPointEditorDaemon> {}

  @Inject private PlaceController placeController;
  @Inject private CalculationPointEditorContext editorContext;
  @Inject private ScenarioContext scenarioContext;
  @Inject private ApplicationContext appContext;
  @Inject PersistablePreferencesContext prefContext;

  @EventHandler
  public void onCalculationPointCreateNewCommand(final CalculationPointCreateNewCommand c) {
    if (editorContext.isEditing()) {
      eventBus.fireEvent(new CalculationPointSaveCommand(editorContext.getCalculationPoint()));
      SchedulerUtil.delay(() -> createNewCalculationPoint());
    } else {
      createNewCalculationPoint();
    }
  }

  private void createNewCalculationPoint() {
    final CalculationPointFeature calculationPoint;
    if (appContext.getTheme() == Theme.NCA) {
      final AssessmentCategory assessmentCategory = prefContext.getAssessmentCategory();
      calculationPoint = NcaCustomCalculationPointFeature.createNCACalculationPoint(assessmentCategory);
    } else {
      calculationPoint = CalculationPointFeature.create();
    }

    final List<CalculationPointFeature> calculationPoints = scenarioContext.getCalculationPoints();
    final String nextId = ClientConsecutiveIdUtil.getNextLabel(calculationPoints);
    calculationPoint.setLabel(M.messages().calculationPointsDefaultLabel(nextId));

    final ConsecutiveIdUtil.IndexIdPair newId = ClientConsecutiveIdUtil.getConsecutiveIdUtil(calculationPoints).generate();
    calculationPoint.setId(newId.getId());

    eventBus.fireEvent(new TemporaryCalculationPointAddCommand(calculationPoint));
    editorContext.createNewCalculationPoint(calculationPoint);

    placeController.goTo(new CalculationPointsPlace(appContext.getTheme()));
  }

  @EventHandler
  public void onCalculationPointEditCommand(final CalculationPointEditCommand c) {
    final CalculationPointFeature original = c.getValue();
    final CalculationPointFeature clone = FeatureUtil.clone(original);
    clone.setGeometry(original.getGeometry().clone());
    eventBus.fireEvent(new TemporaryCalculationPointAddCommand(clone));
    editorContext.editCalculationPoint(clone, original);
  }

  @EventHandler
  public void onCalculationPointResetCommand(final CalculationPointResetCommand c) {
    if (editorContext.isEditing()) {
      eventBus.fireEvent(new TemporaryCalculationPointClearCommand(editorContext.getCalculationPoint()));
      editorContext.reset();
    }
  }

  @EventHandler
  public void onCalculationPointRemoveCommand(final CalculationPointDeleteCommand c) {
    scenarioContext.getCalculationPoints().remove(c.getValue());
  }

  @EventHandler
  public void onCalculationPointEditCancelCommand(final CalculationPointEditCancelCommand c) {
    if (editorContext.hasOriginal()) {
      final CalculationPointFeature original = editorContext.getOriginalCalculationPoint();
      eventBus.fireEvent(new CalculationPointChangeEvent(original));
    }

    clean();
  }

  @EventHandler
  public void onCalculationPointEditSaveCommand(final CalculationPointSaveCommand c) {
    trySave();
    clean();
  }

  private void trySave() {
    if (editorContext.hasOriginal()) {
      final CalculationPointFeature original = editorContext.getOriginalCalculationPoint();
      FeatureUtil.copy(editorContext.getCalculationPoint(), original);
      if (isValid(original)) {
        eventBus.fireEvent(new CalculationPointChangeEvent(original));
      }
    } else {
      final CalculationPointFeature point = editorContext.getCalculationPoint();
      if (isValid(point)) {
        eventBus.fireEvent(new CalculationPointAddCommand(point));
      }
    }
  }

  private boolean isValid(final CalculationPointFeature point) {
    return point.getGeometry() != null;
  }

  private void clean() {
    eventBus.fireEvent(new TemporaryCalculationPointClearCommand(editorContext.getCalculationPoint()));
    editorContext.reset();
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }

}
