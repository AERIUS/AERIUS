/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context.calculation;

import elemental2.core.Global;
import elemental2.core.JsArray;

import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.util.UUID;
import nl.overheid.aerius.wui.application.util.json.JsonBuilder;

public class CalculationJobContext {
  private String id = null;
  private String name = null;

  private SituationComposition situationComposition = new SituationComposition();
  private CalculationSetOptions calculationOptions = CalculationSetOptions.create();

  public CalculationJobContext() {
    this.id = UUID.uuidv4();
  }

  public boolean isActiveSituation(final SituationContext situation) {
    return situationComposition.isActiveSituation(situation);
  }

  public CalculationSetOptions getCalculationOptions() {
    return calculationOptions;
  }

  public void setCalculationOptions(final CalculationSetOptions calculationOptions) {
    this.calculationOptions = calculationOptions;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getId() {
    return id;
  }

  public void setId(final String id) {
    this.id = id;
  }

  public SituationComposition getSituationComposition() {
    return situationComposition;
  }

  public void setSituationComposition(final SituationComposition composition) {
    this.situationComposition = composition;
  }

  public CalculationJobContext copy() {
    final CalculationJobContext copy = new CalculationJobContext();
    copyInto(copy);
    return copy;
  }

  public void copyInto(final CalculationJobContext target) {
    target.setId(id);
    target.setName(name);
    target.setCalculationOptions(calculationOptions.copy());
    target.setSituationComposition(situationComposition.copy());
  }
  
  public void copyIntoDuplicate(final CalculationJobContext target) {
    target.setName(M.messages().copyOf(name));
    target.setCalculationOptions(calculationOptions.copy());
    target.setSituationComposition(situationComposition.copy());
  }

  public static String calculateJobHash(final CalculationJobContext jobContext) {
    final JsonBuilder jsonBuilder = new JsonBuilder(jobContext);
    jsonBuilder.set("situations", computeSituationsList(jobContext));
    jsonBuilder.set("options", jobContext.calculationOptions.toJSON());

    final String json = Global.JSON.stringify(jsonBuilder.build());

    return String.valueOf(json.hashCode());
  }

  public static JsArray<String> computeSituationsList(final CalculationJobContext jobContext) {
    final JsArray<String> arr = new JsArray<>();
    jobContext.getSituationComposition().getSituations().stream()
        .map(v -> v == null ? null : v.getId())
        .sorted()
        .forEach(arr::push);
    return arr;
  }
}
