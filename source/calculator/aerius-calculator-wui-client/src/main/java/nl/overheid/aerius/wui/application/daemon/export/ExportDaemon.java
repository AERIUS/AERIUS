/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.export;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.future.AppAsyncCallback;
import nl.aerius.wui.place.PlaceController;
import nl.aerius.wui.util.NotificationUtil;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.export.ExportAppendix;
import nl.overheid.aerius.wui.application.command.ResetEdgeEffectReportExportSettingsCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationDeleteJobCommand;
import nl.overheid.aerius.wui.application.command.exporter.ExportCancelCommand;
import nl.overheid.aerius.wui.application.command.exporter.ExportCancelEvent;
import nl.overheid.aerius.wui.application.command.exporter.ExportDeleteCommand;
import nl.overheid.aerius.wui.application.command.exporter.ExportDeleteEvent;
import nl.overheid.aerius.wui.application.command.exporter.ExportPrepareCommand;
import nl.overheid.aerius.wui.application.command.exporter.ExportStartCommand;
import nl.overheid.aerius.wui.application.command.exporter.VerifyExportDefaultsCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ExportContext;
import nl.overheid.aerius.wui.application.context.ExportContext.ClientJobType;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationPreparationContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.application.domain.calculation.JobProgress;
import nl.overheid.aerius.wui.application.domain.export.ExportOptions;
import nl.overheid.aerius.wui.application.domain.export.HistoryRecord;
import nl.overheid.aerius.wui.application.domain.importer.ScenarioMetaData;
import nl.overheid.aerius.wui.application.event.ApplicationFinishedLoadingEvent;
import nl.overheid.aerius.wui.application.event.ExportCalculationCompleteEvent;
import nl.overheid.aerius.wui.application.event.ExportStartEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.place.ExportPlace;
import nl.overheid.aerius.wui.application.service.AeriusRequestCallback;
import nl.overheid.aerius.wui.application.service.CalculationServiceAsync;
import nl.overheid.aerius.wui.application.service.CalculationServiceAsyncImpl;

@Singleton
public class ExportDaemon extends BasicEventComponent implements Daemon {
  private static final ExportDaemonEventBinder EVENT_BINDER = GWT.create(ExportDaemonEventBinder.class);

  interface ExportDaemonEventBinder extends EventBinder<ExportDaemon> {}

  @Inject private PlaceController placeController;
  @Inject private ApplicationContext applicationContext;
  @Inject private ScenarioContext scenarioContext;
  @Inject private ExportContext exportContext;
  @Inject private CalculationPreparationContext prepContext;
  @Inject private CalculationServiceAsync calculationServiceAsync;

  @Inject private ExportStatusPoller exportStatusPoller;

  @EventHandler
  public void onChangeProductProfileEvent(final ApplicationFinishedLoadingEvent e) {
    applicationContext.getConfiguration().getAvailableExportReportOptions().stream().findFirst().ifPresent(exportContext::setReportType);
  }

  @EventHandler
  public void onVerifyExportDefaultsCommand(final VerifyExportDefaultsCommand c) {
    if (exportContext.isInitialized()) {
      return;
    }

    // Add all situations to the situation composition as default
    final SituationComposition situationComposition = new SituationComposition();
    scenarioContext.getSituations().forEach(situationComposition::addSituation);
    exportContext.setSituationComposition(situationComposition);

    exportContext.setInitialized(true);
  }

  @EventHandler
  public void onExportPrepareCommand(final ExportPrepareCommand c) {
    final CalculationJobContext job = c.getValue();

    exportContext.setInitialized(true);
    exportContext.setCalculationJob(job);

    if (c.isAlsoSwitch()) {
      final JobType jobType = applicationContext.getConfiguration()
          .getDefaultExportTypeForJobType(job.getCalculationOptions().getCalculationJobType());

      if (jobType == JobType.REPORT) {
        exportContext.setJobType(ClientJobType.REPORT);
      } else {
        exportContext.setJobType(ClientJobType.CALCULATION);
      }
      placeController.goTo(new ExportPlace(applicationContext.getTheme()));
    }
  }

  @EventHandler
  public void onCalculationDeleteJobCommand(final CalculationDeleteJobCommand c) {
    if (exportContext.getCalculationJob() == c.getValue()) {
      exportContext.setCalculationJob(null);
    }
  }

  @EventHandler
  public void onResetPdfExportSettingsCommand(final ResetEdgeEffectReportExportSettingsCommand c) {
    resetEdgeEffectReportExportSettings();
  }

  @EventHandler
  public void onCalculationExportStartCommand(final ExportStartCommand c) {
    c.silence();

    final ExportOptions exportOptions = c.getValue();

    // If job key present we have a calculation.
    final CalculationExecutionContext calc = prepContext.getActiveCalculation(exportContext.getCalculationJob());
    final String optionalJobKey = getValidJobKey(calc, exportContext.getCalculationJob());

    final SituationComposition situationComposition = exportContext.getSituationComposition();
    final CalculationSetOptions correctedCalculationOptions = CalculationServiceAsyncImpl
        .getValidCalculationOptionsFromContext(exportContext.getCalculationOptions(), exportOptions);

    final JobType jobType = exportOptions.getJobType();
    // Always include metadata when the job type is report
    final ScenarioMetaData scenarioMetaData = exportContext.isIncludeScenarioMetaData()
        || jobType == JobType.REPORT
            ? exportContext.getScenarioMetaData()
            : null;

    final List<String> problems = findExportStartProblems(scenarioMetaData, situationComposition, correctedCalculationOptions, exportOptions);

    if (!problems.isEmpty()) {
      // Broadcast an error for each problem
      problems.stream()
          .forEach(problem -> NotificationUtil.broadcastError(eventBus, problem));
      return;
    }

    final String exportHash = getExportHash(scenarioMetaData, situationComposition, correctedCalculationOptions, exportOptions);

    final HistoryRecord item = new HistoryRecord();
    exportContext.addHistoryItem(item);
    item.setDatetime(new Date());
    item.setExportHash(exportHash);
    item.setLabel(formatHistoryItemName(Optional.ofNullable(exportContext.getCalculationJob())
        .map(v -> v.getName())
        .orElse(""), situationComposition, exportOptions));

    calculationServiceAsync.startCalculation(situationComposition, correctedCalculationOptions, c.getValue(), scenarioContext, scenarioMetaData,
        applicationContext.getTheme(), optionalJobKey,
        AeriusRequestCallback.create(jobKey -> {
          item.setJobKey(jobKey);
          exportStatusPoller.start(item);
          eventBus.fireEvent(c.createEvent());
        }, e -> {
          if (item.getJobProgress() == null) {
            item.setJobProgress(new JobProgress());
          }
          item.getJobProgress().setState(JobState.ERROR);
          item.setErrorMessage(M.getErrorMessage(e));
          throw new RuntimeException(e);
        }));
  }

  /**
   *  If no job id or job was with imported results, or job is stale return null to start a new calculation.
   */
  private String getValidJobKey(final CalculationExecutionContext calculation, CalculationJobContext jobContext) {
    return calculation == null || calculation.isImportedJobWithResults()
        || ScenarioContext.isCalculationStale(scenarioContext, jobContext, calculation) ? null : calculation.getJobKey();
  }

  private String formatHistoryItemName(final String jobName, final SituationComposition comp, final ExportOptions exportOptions) {
    switch (exportOptions.getJobType()) {
    case CALCULATION:
      if (exportOptions.getExportType().isSourceExport()) {
        final String scenarioNames = M.messages().exportHistorySourcesName(comp.getSituations().size());
        return M.messages().exportHistoryName(ClientJobType.SOURCES, scenarioNames);
      } else {
        return M.messages().exportHistoryName(ClientJobType.CALCULATION, jobName);
      }
    case REPORT:
      return M.messages().exportHistoryName(ClientJobType.REPORT, jobName);
    }
    return null;
  }

  @EventHandler
  public void onExportCancelCommand(final ExportCancelCommand c) {
    final HistoryRecord historyRecord = c.getValue();
    if (historyRecord.getJobKey() == null) {
      eventBus.fireEvent(new ExportDeleteEvent(historyRecord));
      return;
    }

    c.silence();
    exportStatusPoller.remove(historyRecord);
    calculationServiceAsync.cancelCalculation(historyRecord.getJobKey(),
        AppAsyncCallback.create(res -> eventBus.fireEvent(c.getEvent()), e -> {
          eventBus.fireEvent(c.getEvent());
          NotificationUtil.broadcastError(eventBus, M.messages().notificationCalculationCancelledError(historyRecord.getLabel(), e.getMessage()));
        }));
  }

  @EventHandler
  public void onExportCancelEvent(final ExportCancelEvent e) {
    final HistoryRecord historyRecord = e.getValue();
    eventBus.fireEvent(new ExportDeleteCommand(historyRecord));
    NotificationUtil.broadcastMessage(eventBus, M.messages().notificationExportCancelledMessage(historyRecord.getLabel()));
  }

  @EventHandler
  public void onExportDeleteCommand(final ExportDeleteCommand c) {
    final HistoryRecord historyRecord = c.getValue();
    if (historyRecord.getJobKey() != null) {
      c.silence();
      calculationServiceAsync.deleteCalculation(historyRecord.getJobKey(),
          AppAsyncCallback.create(res -> eventBus.fireEvent(c.getEvent()), e -> eventBus.fireEvent(c.getEvent())));
    }
  }

  @EventHandler
  public void onExportDeleteEvent(final ExportDeleteEvent e) {
    final HistoryRecord historyRecord = e.getValue();
    exportStatusPoller.remove(historyRecord);
    exportContext.removeHistoryItem(historyRecord);
  }

  private void resetEdgeEffectReportExportSettings() {
    final ExportAppendix appendix = ExportAppendix.EDGE_EFFECT_REPORT;
    if (exportContext.getAppendices().contains(appendix)) {
      exportContext.getAppendices().remove(appendix);
    }
  }

  private List<String> findExportStartProblems(final ScenarioMetaData scenarioMetaData, final SituationComposition situationComposition,
      final CalculationSetOptions calculationOptions, final ExportOptions exportOptions) {
    final List<String> problems = new ArrayList<>();

    // Bail if there's already an export with this exact input running
    final String exportHash = getExportHash(scenarioMetaData, situationComposition, calculationOptions, exportOptions);
    if (exportContext.getHistory().stream()
        .filter(v -> !v.getStatus().isFinalState())
        .map(v -> v.getExportHash())
        .anyMatch(v -> v.equals(exportHash))) {
      problems.add(M.messages().exportAlreadyRunningWarning());
    }

    return problems;
  }

  private String getExportHash(final ScenarioMetaData scenarioMetaData, final SituationComposition situationComposition,
      final CalculationSetOptions calculationOptions, final ExportOptions exportOptions) {
    final String scenarioHash = String.valueOf(scenarioContext.toJSONString(scenarioMetaData, situationComposition, calculationOptions).hashCode());
    final String optionsHash = String.valueOf(exportOptions.toJSONString().hashCode());

    return scenarioHash + "-" + optionsHash;
  }

  @EventHandler
  public void onExportStartEvent(final ExportStartEvent e) {
    resetEdgeEffectReportExportSettings();
    NotificationUtil.broadcastMessage(eventBus, M.messages().notificationExportStarted());
  }

  @EventHandler
  public void onExportCalculationCompleteEvent(final ExportCalculationCompleteEvent c) {
    if (JobState.COMPLETED.equals(c.getValue().getJobProgress().getState())) {
      final String fileUrl = c.getValue().getJobProgress().getResultUrl();

      if (c.getValue().getJobProgress().isProtect()) {
        NotificationUtil.broadcastMessage(eventBus, M.messages().scenarioExportFileDownloadWithJobKey(fileUrl, c.getJobKey()));
      } else {
        NotificationUtil.broadcastMessage(eventBus, M.messages().scenarioExportFileDownload(fileUrl));
      }
    } else {
      NotificationUtil.broadcastError(eventBus, M.messages().scenarioExportError(c.getValue().getJobProgress().getErrorMessage()));
    }
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
