/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers;

import static jsinterop.annotations.JsPackage.GLOBAL;

import ol.Feature;
import ol.geom.Geometry;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.aerius.geo.wui.util.OL3GeometryUtil;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.domain.ImaerFeature;
import nl.overheid.aerius.wui.vue.SkipReactivityValidations;

/**
 * Represents a label for an emission source or building
 */
@JsType(namespace = GLOBAL, name = "ol.Feature", isNative = true)
@SkipReactivityValidations
public class LabelFeature extends ImaerFeature {

  public static @JsOverlay LabelFeature create(final String id, final Geometry geometry, final String label, final String color,
      final Feature referenceFeature) {
    return create(id, geometry, label, color, referenceFeature, FeatureType.OTHER, null);
  }

  public static @JsOverlay LabelFeature create(final String id, final Geometry geometry, final String label, final String color,
      final Feature referenceFeature, final FeatureType featureType, final SituationType sitationType) {
    final LabelFeature labelFeature = new LabelFeature();
    labelFeature.setId(id);
    labelFeature.setLabel(label);
    labelFeature.setGeometry(OL3GeometryUtil.getMiddlePointOfGeometry(geometry));
    labelFeature.setColor(color);
    labelFeature.setReferenceFeature(referenceFeature);
    labelFeature.setFeatureType(featureType);
    labelFeature.setSitationType(sitationType);
    return labelFeature;
  }

  public final @JsOverlay String getColor() {
    return get("color") == null ? "" : Js.asString(get("color"));
  }

  public final @JsOverlay void setColor(final String color) {
    set("color", color);
  }

  public final @JsOverlay Feature getReferenceFeature() {
    return get("referenceFeature");
  }

  public final @JsOverlay void setReferenceFeature(final Feature feature) {
    set("referenceFeature", feature);
  }

  public final @JsOverlay FeatureType getFeatureType() {
    return get("featureType");
  }

  public final @JsOverlay void setFeatureType(final FeatureType featureType) {
    set("featureType", featureType);
  }

  public final @JsOverlay SituationType getSituationType() {
    return get("situationType");
  }

  public final @JsOverlay void setSitationType(final SituationType sitationType) {
    set("situationType", sitationType);
  }
}
