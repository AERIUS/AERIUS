/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.navigation;

import javax.inject.Inject;

import elemental2.dom.DomGlobal;
import elemental2.dom.Event;
import elemental2.dom.EventInit;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.service.CalculationServiceAsync;
import nl.overheid.aerius.wui.config.ApplicationFlags;

public class ApplicationExitDaemon extends BasicEventComponent {
  private @Inject ScenarioContext context;
  private @Inject ApplicationFlags applicationFlags;
  private @Inject CalculationContext calculationContext;
  private @Inject CalculationServiceAsync calculationService;

  @JsType(isNative = true, namespace = JsPackage.GLOBAL)
  private static class BeforeUnloadEvent extends Event {
    public BeforeUnloadEvent(final String type, final EventInit eventInitDict) {
      super(type, eventInitDict);
    }

    public BeforeUnloadEvent(final String type) {
      super(type);
    }

    public @JsProperty String returnValue;
  }

  public ApplicationExitDaemon() {
    DomGlobal.window.addEventListener("beforeunload", e -> {
      if (!canExit()) {
        // Different browsers need different conditions to be true in order to trigger
        // the confirmation popup. See:
        // https://developer.mozilla.org/en-US/docs/Web/API/WindowEventHandlers/onbeforeunload
        final BeforeUnloadEvent evt = Js.uncheckedCast(e);

        e.preventDefault(); // Firefox
        evt.returnValue = ""; // Chromium-based
      }
    });
    DomGlobal.window.addEventListener("unload", e -> {
      if (!applicationFlags.isInternal()) {
        calculationContext.getCalculations().stream()
            .map(v -> v.getJobKey())
            .filter(v -> v != null)
            .forEach(jobKey -> purgeCalculation(jobKey));
      }
    });
  }

  private void purgeCalculation(final String jobKey) {
    calculationService.deleteCalculationViaPost(jobKey);
  }

  private boolean canExit() {
    // If dev mode, allow quick exit
    final boolean isDevelopment = GWTProd.isDev();

    // If there is any content, don't allow quick exit
    final boolean hasNoContent = !ScenarioContext.hasAnyContent(context);

    return isDevelopment
        || hasNoContent;
  }
}
