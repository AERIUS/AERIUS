/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.legend;

import nl.aerius.geo.domain.legend.Legend;

/**
 * Legend intended for use with a item - label based approach, where the items are text based.
 */
public class TextLabelsLegend implements Legend {

  private static final long serialVersionUID = 1L;

  private String[] items;
  private String[] labels;

  public TextLabelsLegend() {
    // For GWT
  }

  public TextLabelsLegend(final String[] items, final String[] labels) {
    this.items = items;
    this.labels = labels;
  }

  public String[] getItems() {
    return items;
  }

  public void setItems(final String[] items) {
    this.items = items;
  }

  public String[] getLabels() {
    return labels;
  }

  public void setLabels(final String[] labels) {
    this.labels = labels;
  }

}
