/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.event;

import nl.aerius.wui.event.SimpleGenericEvent;

/**
 * Event fired when related command is finished.
 */
public class RestoreScenarioContextEvent extends SimpleGenericEvent<String> {

  private final String situationId;

  /**
   * Constructor
   *
   * @param jobKey the job key for the scenario to retrieve from the server
   * @param situationId id of situation that is activated
   */
  public RestoreScenarioContextEvent(final String jobKey, final String situationId) {
    super(jobKey);
    this.situationId = situationId;
  }

  public String getSituationId() {
    return situationId;
  }
}
