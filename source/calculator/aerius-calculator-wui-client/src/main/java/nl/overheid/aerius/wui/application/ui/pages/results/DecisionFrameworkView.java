/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.results;

import java.util.Optional;
import java.util.function.Supplier;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;
import jsinterop.base.JsPropertyMap;

import nl.aerius.wui.vue.directives.VectorDirective;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.application.command.exporter.ExportPrepareCommand;
import nl.overheid.aerius.wui.application.components.input.InputNoticeComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationPreparationContext;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationInfo;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.application.domain.calculation.JobProgress;
import nl.overheid.aerius.wui.application.domain.summary.AssessmentAreaThresholdResults;
import nl.overheid.aerius.wui.application.domain.summary.DecisionFrameworkResults;
import nl.overheid.aerius.wui.application.domain.summary.ThresholdResult;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.util.JsObjectsUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    VerticalCollapse.class,
    InputNoticeComponent.class,
    SiteRelevantThresholdsTable.class,
    DecisionThresholdsTable.class,
    VerticalCollapseGroup.class,
}, directives = {
    VectorDirective.class,
})
public class DecisionFrameworkView extends BasicVueComponent {

  private static final String NORTHERN_IRELAND = "NORTHERN_IRELAND";

  @Inject @Data CalculationPreparationContext prepContext;

  @Prop EventBus eventBus;

  @Inject @Data CalculationContext calculationContext;
  @Inject @Data ApplicationContext applicationContext;

  @Computed
  public CalculationExecutionContext getCalculationJob() {
    return calculationContext.getActiveCalculation();
  }

  @Computed
  public CalculationInfo getCalculationInfo() {
    return Optional.ofNullable(getCalculationJob())
        .map(CalculationExecutionContext::getCalculationInfo)
        .orElseThrow(() -> new RuntimeException("Could not retrieve calculation info."));
  }

  @Computed
  public JobProgress getJobProgress() {
    return Optional.ofNullable(getCalculationInfo())
        .map(CalculationInfo::getJobProgress)
        .orElseGet(JobProgress::new);
  }

  @Computed("isNorthernIreland")
  public boolean isNorthernIreland() {
    return Optional.ofNullable(getCalculationJob())
        .map(CalculationExecutionContext::getJobContext)
        .map(CalculationJobContext::getCalculationOptions)
        .map(CalculationSetOptions::getNcaCalculationOptions)
        .map(x -> NORTHERN_IRELAND.equals(x.getPermitArea()))
        .orElse(false);
  }

  @Computed
  public DecisionFrameworkResults getResults() {
    return Optional.ofNullable(getCalculationJob())
        .map(CalculationExecutionContext::getDecisionFrameworkResults)
        .orElse(null);
  }

  @Computed("hasAnyResults")
  public boolean hasAnyResults() {
    final DecisionFrameworkResults results = getResults();
    if (results != null) {
      for (final EmissionResultKey key : applicationContext.getConfiguration().getEmissionResultKeys()) {
        if (JsObjectsUtil.hasValueForKey(results.getDecisionMakingThresholdResults(), key)) {
          return true;
        }
      }
    }
    return false;
  }

  @Computed("aboveDecisionMakingThreshold")
  public boolean isAboveDecisionMakingThreshold() {
    final DecisionFrameworkResults results = getResults();
    return results != null && overAnyThreshold(results.getDecisionMakingThresholdResults());
  }

  @Computed
  public Supplier<String> getDecisionMakingThresholdMessage() {
    return () -> hasAnyResults() ? getDecisionMakingThresholdMessageWithResults() : M.messages().decisionFrameworkNoResults();
  }

  private String getDecisionMakingThresholdMessageWithResults() {
    return isAboveDecisionMakingThreshold() ? M.messages().decisionFrameworkAboveDMT() : M.messages().decisionFrameworkNotAboveDMT();
  }

  @Computed("aboveSiteRelevantThresholds")
  public boolean isAboveSiteRelevantThresholds() {
    final DecisionFrameworkResults results = getResults();
    if (results != null) {
      for (final AssessmentAreaThresholdResults areaResults : results.getAssessmentAreaThresholdResults()) {
        if (overAnyThreshold(areaResults.getResults())) {
          return true;
        }
      }
    }
    return false;
  }

  @Computed
  public Supplier<String> getSiteRelevantThresholdsMessage() {
    return () -> isAboveSiteRelevantThresholds() ? M.messages().decisionFrameworkAboveSRT() : M.messages().decisionFrameworkNotAboveSRT();
  }

  private boolean overAnyThreshold(final JsPropertyMap<ThresholdResult> results) {
    for (final EmissionResultKey key : applicationContext.getConfiguration().getEmissionResultKeys()) {
      if (JsObjectsUtil.hasValueForKey(results, key)
          && JsObjectsUtil.getValueForKey(results, key).getFraction() > 1.0) {
        return true;
      }
    }
    return false;
  }

  @JsMethod
  public void onExportResultsClick() {
    eventBus.fireEvent(new ExportPrepareCommand(prepContext.getActiveJob()));
  }

}
