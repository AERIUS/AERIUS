/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.results.habitat;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings.DepositionValueDisplayType;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.command.geo.ClearAssessmentAreaCommand;
import nl.overheid.aerius.wui.application.command.geo.HabitatTypeHoverActiveCommand;
import nl.overheid.aerius.wui.application.command.geo.HabitatTypeHoverInactiveCommand;
import nl.overheid.aerius.wui.application.command.geo.HabitatTypeSelectToggleCommand;
import nl.overheid.aerius.wui.application.command.geo.SelectAssessmentAreaCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.context.result.ResultSelectionContext;
import nl.overheid.aerius.wui.application.context.result.ResultSummaryContext;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsAreaSummary;
import nl.overheid.aerius.wui.application.geo.util.GeoUtil;
import nl.overheid.aerius.wui.application.ui.pages.results.GeneralizedEmissionResultType;
import nl.overheid.aerius.wui.application.util.ResultStatisticUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    CollapsiblePanel.class,
    SimplifiedListBoxComponent.class,
    ButtonIcon.class,
    ResultsHabitatListView.class,
    VerticalCollapseGroup.class
})
public class ResultHabitatView extends BasicVueComponent {
  @Prop EventBus eventBus;
  @Prop(required = true) ResultSummaryContext context;

  @Data @Inject CalculationContext calculationContext;
  @Data @Inject ApplicationContext applicationContext;
  @Data @Inject ResultSelectionContext selectionContext;

  @Computed
  public DepositionValueDisplayType getUnitDisplayType() {
    return applicationContext.getConfiguration().getEmissionResultValueDisplaySettings().getDisplayType();
  }

  @Computed
  public Theme getTheme() {
    return applicationContext.getTheme();
  }

  @Computed
  public GeneralizedEmissionResultType getEmissionResultType() {
    return GeneralizedEmissionResultType.fromEmissionResultType(context.getEmissionResultKey().getEmissionResultType());
  }

  @JsMethod
  public String getTitle(final SituationResultsAreaSummary d) {
    return d.getAssessmentArea().getTitle(applicationContext.isAppFlagEnabled(AppFlag.SHOW_ASSESSMENT_AREA_DIRECTIVES));
  }

  @JsMethod
  public void toggleHabitatSelection(final String habitatTypeCode) {
    eventBus.fireEvent(new HabitatTypeSelectToggleCommand(habitatTypeCode, isExtraAssessmentHabitats()));
  }

  @JsMethod
  public void setHabitatHighlightActive(final String habitatTypeCode) {
    eventBus.fireEvent(new HabitatTypeHoverActiveCommand(habitatTypeCode, isExtraAssessmentHabitats()));
  }

  private boolean isExtraAssessmentHabitats() {
    return context.getHexagonType() == SummaryHexagonType.EXTRA_ASSESSMENT_HEXAGONS;
  }

  @JsMethod
  public void setHabitatHighlightInactive(final String habitatTypeCode) {
    eventBus.fireEvent(new HabitatTypeHoverInactiveCommand(habitatTypeCode));
  }

  @Computed
  public List<ResultStatisticType> getHabitatStatisticTypes() {
    return ResultStatisticUtil.getStatisticsForHabitat(context.getResultType());
  }

  @Computed
  public ResultStatisticType getSurfaceStatisticType() {
    return context.getSurfaceStatisticType();
  }

  @Watch(value = "getHabitatStatisticTypes()", isImmediate = true)
  public void onEmissionResultTypeChange(final List<ResultStatisticType> neww) {
    if (!neww.isEmpty() && !neww.contains(selectionContext.getHabitatStatisticType())) {
      selectHabitatStatisticsType(neww.get(0));
    }
  }

  @Computed
  public CalculationExecutionContext getCalculationJob() {
    return calculationContext.getActiveCalculation();
  }

  @JsMethod
  public void openPanel(final SituationResultsAreaSummary summary) {
    final int id = summary.getAssessmentArea().getId();
    eventBus.fireEvent(new SelectAssessmentAreaCommand(id));
  }

  @JsMethod
  public void closePanel() {
    eventBus.fireEvent(new ClearAssessmentAreaCommand());
  }

  @JsMethod
  public boolean isOpen(final int id) {
    return id == selectionContext.getSelectedAssessmentArea();
  }

  @JsMethod
  public void selectHabitatStatisticsType(final ResultStatisticType type) {
    eventBus.fireEvent(new SelectResultHabitatStatisticTypeCommand(type));
  }

  @JsMethod
  public void zoomToArea(final SituationResultsAreaSummary areaSummary) {
    eventBus.fireEvent(GeoUtil.createMapSetExtendCommand(areaSummary.getAssessmentArea().getBounds()));
  }
}
