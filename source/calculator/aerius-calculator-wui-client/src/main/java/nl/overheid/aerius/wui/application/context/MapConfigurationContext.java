/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.context;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.inject.Singleton;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.domain.geo.LabelDisplayMode;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;

@Singleton
public class MapConfigurationContext {

  private double iconScale = 1.0;
  private LabelDisplayMode labelDisplayMode = LabelDisplayMode.ID;
  private @JsProperty Set<EmissionSourceType> unmarkedESTypes = new HashSet<>(Arrays.asList(
      EmissionSourceType.SRM1_ROAD,
      EmissionSourceType.SRM2_ROAD,
      EmissionSourceType.ADMS_ROAD
  ));

  public double getIconScale() {
    return iconScale;
  }

  public void setIconScale(final double iconScale) {
    this.iconScale = iconScale;
  }

  public LabelDisplayMode getLabelDisplayMode() {
    return labelDisplayMode;
  }

  public void setLabelDisplayMode(final LabelDisplayMode labelDisplayMode) {
    this.labelDisplayMode = labelDisplayMode;
  }

  public Set<EmissionSourceType> getUnmarkedESTypes() {
    return unmarkedESTypes;
  }

  public void setUnmarkedESTypes(final Set<EmissionSourceType> unmarkedESTypes) {
    this.unmarkedESTypes = unmarkedESTypes;
  }

  public boolean isMarked(final EmissionSourceFeature feature) {
    return !this.unmarkedESTypes.contains(feature.getEmissionSourceType());
  }
}
