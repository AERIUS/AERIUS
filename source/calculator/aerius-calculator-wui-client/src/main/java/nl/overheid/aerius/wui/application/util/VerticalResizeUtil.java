/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.util;

import elemental2.dom.CSSProperties.MaxHeightUnionType;
import elemental2.dom.DomGlobal;
import elemental2.dom.Event;
import elemental2.dom.HTMLElement;
import elemental2.dom.MouseEvent;

import nl.aerius.wui.util.ComputedStyleUtil;

public final class VerticalResizeUtil {
  private VerticalResizeUtil() {}

  public static class ResizeHandler {
    private final HTMLElement panel;

    private double startY;
    private int startHeight;

    public ResizeHandler(final HTMLElement panel) {
      this.panel = panel;
    }

    private Object onMouseDown(final Event downEvent) {
      final MouseEvent mouseEvent = (MouseEvent) downEvent;
      mouseEvent.preventDefault();

      startY = mouseEvent.clientY;
      startHeight = panel.clientHeight;

      DomGlobal.document.documentElement.onmouseup = upEvent -> onMouseUp(upEvent);
      DomGlobal.document.documentElement.onmousemove = moveEvent -> onMouseMove(moveEvent);

      return null;
    }

    private Object onMouseMove(final Event moveEvent) {
      final MouseEvent mouseEvent = (MouseEvent) moveEvent;
      mouseEvent.preventDefault();

      final double dragY = mouseEvent.clientY;
      final double deltaY = dragY - startY;
      final double targetHeight = startHeight + deltaY;

      // Determine the panel's natural height using a forced render and re-render
      // This shouldn't lead to any visual artifacts because it happens within the
      // same message in the JS event loop queue
      final MaxHeightUnionType panelMaxHeight = panel.style.maxHeight;
      panel.style.maxHeight = null;
      ComputedStyleUtil.forceStyleRender(panel);
      final double naturalHeight = panel.scrollHeight;
      panel.style.maxHeight = panelMaxHeight;
      ComputedStyleUtil.forceStyleRender(panel);

      if (targetHeight > naturalHeight) {
        panel.style.maxHeight = null;
      } else {
        panel.style.maxHeight = MaxHeightUnionType.of(targetHeight + "px");
      }

      return null;
    }

    private Object onMouseUp(final Event e) {
      DomGlobal.document.documentElement.onmouseup = null;
      DomGlobal.document.documentElement.onmousemove = null;
      return null;
    }
  }

  public static ResizeHandler resizable(final HTMLElement handle, final HTMLElement panel) {
    final ResizeHandler handler = new ResizeHandler(panel);

    handle.onmousedown = e -> {
      final Object ret = handler.onMouseDown(e);
      return ret;
    };

    return handler;
  }

  public static void reset(final HTMLElement elem) {
    elem.style.maxHeight = null;
  }
}
