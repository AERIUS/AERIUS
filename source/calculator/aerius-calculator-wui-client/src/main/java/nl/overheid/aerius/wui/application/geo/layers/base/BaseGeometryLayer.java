/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.base;

import ol.Feature;
import ol.OLFactory;
import ol.color.Color;
import ol.layer.VectorLayerOptions;
import ol.style.CircleOptions;
import ol.style.Fill;
import ol.style.Style;

import nl.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.wui.application.geo.layers.FeatureStyle;

/**
 * Abstract base layer for showing feature geometries on the map.
 */
public abstract class BaseGeometryLayer<F extends Feature> extends BaseVectorLayer<F> {
  protected static final Style[] NO_RENDERING = LabelStyleCreator.NO_RENDERING;

  private static final double GEOMETRY_MAX_RESOLUTION = 100.0;

  private String geometryEditingFeatureId = null;

  public BaseGeometryLayer(final LayerInfo info, final int zIndex) {
    super(info, zIndex);
  }

  @Override
  protected void setAdditionalVectorLayerOptions(final VectorLayerOptions vectorLayerOptions) {
    vectorLayerOptions.setMaxResolution(GEOMETRY_MAX_RESOLUTION);
  }

  /**
   * Set the feature of which the geometry is being edited, or null if no feature is being edited.
   *
   * @param feature feature of which the geometry is being edited or null if no feature being edited
   */
  protected void setGeometryEditingFeatureId(final Feature feature) {
    if (feature == null) {
      geometryEditingFeatureId = null;
    } else {
      geometryEditingFeatureId = feature.getId();
    }
    refreshLayer();
  }

  protected String getGeometryEditingFeatureId() {
    return geometryEditingFeatureId;
  }

  /**
   * @return Returns the feature being edited or null if no feature being edited
   */
  protected String getEditableFeatureId() {
    final F feature = getEditingFeatureFromContext();

    return feature == null ? null : feature.getId();
  }

  /**
   * Implementations should retrieve the edit status from the feature edit context.
   * Default implementation returns null.
   *
   * @return the feature being edited as specified in the context.
   */
  protected F getEditingFeatureFromContext() {
    return null;
  }

  /**
   * Returns a style for rendering a point feature
   *
   * @param color the color of the point
   * @return an array of styles for rendering a point feature
   */
  protected static Style[] getPointStyle(final Color color) {
    final Fill fill = OLFactory.createFill(color);
    final CircleOptions circleOptions = new CircleOptions();
    circleOptions.setFill(fill);
    circleOptions.setRadius(FeatureStyle.POINT_COLOR_RADIUS);

    return new Style[] {
        FeatureStyle.POINT_STYLE,
        OLFactory.createStyle(OLFactory.createCircleStyle(circleOptions))
    };
  }
}
