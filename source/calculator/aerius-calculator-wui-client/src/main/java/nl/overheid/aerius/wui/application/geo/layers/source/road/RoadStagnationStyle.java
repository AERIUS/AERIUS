/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source.road;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import ol.OLFactory;
import ol.style.Style;

import nl.aerius.geo.domain.legend.Legend;
import nl.aerius.geo.domain.legend.LegendType;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.legend.ColorRange;
import nl.overheid.aerius.shared.domain.legend.ColorRangesLegend;
import nl.overheid.aerius.wui.application.domain.source.RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.road.StandardVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.ValuesPerVehicleType;
import nl.overheid.aerius.wui.application.domain.source.road.VehicleType;
import nl.overheid.aerius.wui.application.geo.layers.source.RoadSourceGeometryLayer;
import nl.overheid.aerius.wui.application.i18n.M;

public class RoadStagnationStyle implements RoadStyle {

  private static final List<ColorRange> legend = new ArrayList<>();
  private static final Map<String, List<Style>> styleMap;

  static {
    legend.add(new ColorRange(0d, "#fee1c1", "0%"));
    legend.add(new ColorRange(Double.MIN_VALUE, "#fdc27f", "< 10%"));
    legend.add(new ColorRange(.1d, "#fb8824", "10 " + M.messages().colorRangesLegendBetweenText() + " 25%"));
    legend.add(new ColorRange(.25d, "#bf6418", "25 " + M.messages().colorRangesLegendBetweenText() + " 50%"));
    legend.add(new ColorRange(.5d, "#80430e", "≥ 50%"));

    styleMap = legend.stream()
        .map(ColorRange::getColor)
        .collect(Collectors.toMap(
            Function.identity(),
            color -> Collections.singletonList(
                OLFactory.createStyle(OLFactory.createStroke(OLFactory.createColor(color), RoadSourceGeometryLayer.DEFAULT_ROAD_WIDTH)))));
  }

  @Override
  public List<Style> getStyle(final RoadESFeature feature, final String vehicleTypeCode, final double resolution) {

    final double stagnationPercentage = feature.getSubSources().asList().stream()
        .filter((v) -> v.getVehicleType() == VehicleType.STANDARD)
        .map(v -> ((StandardVehicles) v))
        .map(v -> getSafeStagnationFraction(v.getValuesPerVehicleType(vehicleTypeCode)))
        .findFirst().orElse(0d);

    final String color = legend.stream()
        .sorted(Comparator.reverseOrder())
        .filter(v -> stagnationPercentage >= v.getLowerValue())
        .findFirst().orElse(legend.get(0)).getColor();

    return styleMap.get(color);
  }

  private double getSafeStagnationFraction(final ValuesPerVehicleType valuesPerVehicleType) {
    return valuesPerVehicleType == null ? 0D : valuesPerVehicleType.getStagnationFraction();
  }

  @Override
  public Legend getLegend(final ApplicationConfiguration appThemeConfiguration) {
    return new ColorRangesLegend(legend, M.messages().colorRangesLegendBetweenText(), LegendType.LINE);
  }

}
