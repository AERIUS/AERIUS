/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.emissionsourceList.situation;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.Vue;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.EventBus;

import elemental2.dom.FocusEvent;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.util.SchedulerUtil;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.command.situation.SituationChangeNettingFactorCommand;
import nl.overheid.aerius.wui.application.command.situation.SituationChangeTypeCommand;
import nl.overheid.aerius.wui.application.command.situation.SituationChangeYearCommand;
import nl.overheid.aerius.wui.application.command.situation.SituationDeleteCommand;
import nl.overheid.aerius.wui.application.command.situation.SituationRenameCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.tooltip.TooltipComponent;
import nl.overheid.aerius.wui.application.components.warning.PrivacyWarning;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.ui.pages.emissionsourceList.situation.SituationValidators.SituationValidations;
import nl.overheid.aerius.wui.application.util.AccessibilityUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    ButtonIcon.class,
    TooltipComponent.class,
    PrivacyWarning.class,
    SimplifiedListBoxComponent.class,
    LabeledInputComponent.class,
    VerticalCollapse.class
}, customizeOptions = SituationValidators.class, directives = ValidateDirective.class)
public class SituationEditorView extends BasicVueComponent implements HasCreated {
  @Prop EventBus eventBus;
  @Prop SituationContext situation;

  @Data boolean nameLabelFocused = false;

  @Data String situationName;
  @Data SituationType situationType;
  @Data int situationYear;
  @Data Double situationNettingFactor;

  @Inject @Data ApplicationContext applicationContext;
  @Inject @Data ScenarioContext scenarioContext;

  private boolean embargo = false;

  @Data boolean editMode = false;

  @JsProperty(name = "$v") SituationValidations validation;

  @Computed
  public SituationValidations getV() {
    return validation;
  }

  @Watch("situationNettingFactor")
  void onSituationNettingFactorChange(final Double neww) {
    startEdit();
  }

  @Watch("situationName")
  void onsituationNameChange(final Double neww) {
    startEdit();
  }

  @Watch(value = "situation", isImmediate = true)
  public void onSituationChange(final SituationContext neww) {
    if (neww != null && neww.getType() == null) {
      GWTProd.error("Invalid situation: ", neww);
    }

    cancelEdit();
    if (neww != null) {
      situationType = neww.getType();
      situationYear = neww.getYear();
    }
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  private void update() {
    if (situation == null) {
      return;
    }

    if (!editMode) {
      resetModel();
    }
  }

  private void resetModel() {
    embargo = true;

    situationName = situation.getName();
    situationType = situation.getType();
    situationYear = situation.getYear();
    situationNettingFactor = situation.getNettingFactor();
    validation.$reset();

    Vue.nextTick(() -> embargo = false);
  }

  @JsMethod
  public void onSituationNameFocus() {
    nameLabelFocused = true;
  }

  @JsMethod
  public void onSituationNameFocusOut(final FocusEvent event) {
    AccessibilityUtil.doOnFocusLeave(event, () -> SchedulerUtil.delay(() -> nameLabelFocused = false, AccessibilityUtil.FOCUS_OUT_DELAY));
  }

  @JsMethod
  public void selectSituationType(final SituationType option) {
    startEdit();
    this.situationType = option;
    if (SituationType.OFF_SITE_REDUCTION == this.situationType) {
      situationNettingFactor = applicationContext.getSetting(SharedConstantsEnum.DEFAULT_NETTING_FACTOR);
    }
  }

  @JsMethod
  public void selectSituationYear(final Integer year) {
    startEdit();
    this.situationYear = year;
  }

  @JsMethod
  public void deleteSituation() {
    if (Window.confirm(i18n.navigationSituationDeleteConfirm(situation.getName()))) {
      eventBus.fireEvent(new SituationDeleteCommand(situation));
    }
  }

  @Computed("offSiteReductionSituation")
  public boolean isOffSiteReductionSituation() {
    return situationType != null && situationType == SituationType.OFF_SITE_REDUCTION;
  }

  @JsMethod
  protected String nettingFactorError() {
    return i18n.ivDoubleRangeBetween(i18n.situationNettingFactorLabel(), 0, 1);
  }

  @Computed
  protected Double getNettingFactor() {
    return situationNettingFactor;
  }

  public boolean isInvalid() {
    return validation.situationName.invalid
        || situationType == SituationType.OFF_SITE_REDUCTION && validation.situationNettingFactor.invalid;
  }

  @JsMethod
  public void startEdit() {
    if (embargo) {
      return;
    }

    editMode = true;
  }

  @JsMethod
  public void cancelEdit() {
    editMode = false;
    update();
  }

  @JsMethod
  public void applyEdit() {
    if (isInvalid()) {
      return;
    }

    if (!situation.getName().equals(situationName)) {
      eventBus.fireEvent(new SituationRenameCommand(situation, situationName));
    }

    if (situation.getYear() != situationYear) {
      eventBus.fireEvent(new SituationChangeYearCommand(situation, situationYear));
    }

    if (!situation.getType().equals(situationType)) {
      eventBus.fireEvent(new SituationChangeTypeCommand(situation, situationType));
    }

    if (situationType == SituationType.OFF_SITE_REDUCTION) {
      eventBus.fireEvent(new SituationChangeNettingFactorCommand(situation, situationNettingFactor));
    }

    cancelEdit();
  }
}
