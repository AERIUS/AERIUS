/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.calculation;

import javax.inject.Inject;

import com.google.web.bindery.event.shared.EventBus;

import nl.aerius.wui.util.NotificationUtil;
import nl.overheid.aerius.wui.application.command.calculation.CalculationResetCommand;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationInfo;
import nl.overheid.aerius.wui.application.event.CalculationStatusEvent;
import nl.overheid.aerius.wui.application.service.AeriusRequestCallback;
import nl.overheid.aerius.wui.application.service.CalculationServiceAsync;

public class CalculationStatusPoller extends AbstractStatusPoller<CalculationExecutionContext> {
  private static final int INTERVAL = 15_000;
  private static final int RETRY_AMOUNT = 12 * 5; // 15 minutes

  @Inject private CalculationServiceAsync service;
  @Inject private EventBus eventBus;

  public CalculationStatusPoller() {
    super(INTERVAL, RETRY_AMOUNT);
  }

  @Override
  protected void doFetchStatus(final CalculationExecutionContext job) {
    service.getCalculationStatus(job.getJobKey(), AeriusRequestCallback.create(v -> doStatusSuccess(job, v), e -> fail(job, e)));
  }

  private void doStatusSuccess(final CalculationExecutionContext job, final CalculationInfo resultInfo) {
    succeedCall(job);
    if (isStopped()) {
      return;
    }

    // Notify status update of the given calculation
    eventBus.fireEvent(new CalculationStatusEvent(job.getJobKey(), resultInfo));

    // Remove from pool when final is reached
    if (resultInfo.getJobProgress().getState().isFinalState()) {
      finish(job);
    }

    tryFetchDelayed();
  }

  @Override
  protected void finalFailure(final CalculationExecutionContext job, final Throwable e) {
    NotificationUtil.broadcastError(eventBus, e);
    eventBus.fireEvent(new CalculationResetCommand(job));
  }

  public void remove(final CalculationExecutionContext executionContext) {
    finish(executionContext);
  }
}
