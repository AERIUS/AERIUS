/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.print.own2000;

import java.util.ArrayList;
import java.util.List;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.gwt.resources.client.DataResource;

import nl.overheid.aerius.shared.domain.PdfProductType;
import nl.overheid.aerius.wui.application.context.PrintContext;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.print.own2000.results.ProcurementPolicyResultsComponent;
import nl.overheid.aerius.wui.application.print.own2000.results.ResultSummaryEdgeEffectReceptors;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component
public class ProjectCalculationCoverPage extends BasicVueComponent {
  @Prop
  PrintContext printOwN2000Context;

  @Computed
  public List<Chapter> chapters() {
    final List<Chapter> chapters = new ArrayList<>();
    final PdfProductType pdfProductType = printOwN2000Context.getPdfProductType();

    if (pdfProductType == null) {
      return chapters;
    }
    switch (pdfProductType) {
    case OWN2000_EDGE_EFFECT_APPENDIX:
      chapters.add(new Chapter(Overview.ID, M.messages().pdfProjectCalculationCoverPageTocOverview(), ""));
      chapters.add(new Chapter(Results.ID, M.messages().pdfProjectCalculationCoverPageTocResultsPerAssessmentArea(),
          M.messages().pdfProjectCalculationCoverPageTocResultsWithoutEdgeEffect()));
      chapters.add(new Chapter(ResultSummaryEdgeEffectReceptors.ID, M.messages().pdfProjectCalculationCoverPageTocResultsWithEdgeEffect(), ""));
      break;
    case OWN2000_EXTRA_ASSESSMENT_APPENDIX:
      chapters.add(new Chapter(Overview.ID, M.messages().pdfProjectCalculationCoverPageTocOverview(), ""));
      chapters.add(new Chapter(Results.ID, M.messages().pdfProjectCalculationCoverPageTocResults(), ""));
      break;
    case OWN2000_CALCULATOR:
    case OWN2000_PERMIT:
      chapters.add(new Chapter(Overview.ID, M.messages().pdfProjectCalculationCoverPageTocOverview(), ""));
      chapters.add(new Chapter(SituationSummaries.ID, M.messages().pdfProjectCalculationCoverPageTocSituationsSummary(), ""));
      chapters.add(new Chapter(Results.ID, M.messages().pdfProjectCalculationCoverPageTocResults(), ""));
      chapters.add(new Chapter(EmissionSources.ID, M.messages().pdfProjectCalculationCoverPageTocEmissionSourceDetails(), ""));
      break;
    case OWN2000_SINGLE_SCENARIO:
      chapters.add(new Chapter(Overview.ID, M.messages().pdfProjectCalculationCoverPageTocOverview(), ""));
      chapters.add(new Chapter(SituationSummaries.ID, M.messages().pdfProjectCalculationCoverPageTocSituationSummary(), ""));
      chapters.add(new Chapter(Results.ID, M.messages().pdfProjectCalculationCoverPageTocResults(), ""));
      chapters.add(new Chapter(EmissionSources.ID, M.messages().pdfProjectCalculationCoverPageTocEmissionSourceDetails(), ""));
      break;
    case OWN2000_LBV_POLICY:
      chapters.add(new Chapter(Overview.ID, M.messages().pdfProjectCalculationCoverPageTocOverview(), ""));
      chapters.add(new Chapter(SituationSummaries.ID, M.messages().pdfProjectCalculationCoverPageTocSituationSummary(), ""));
      chapters.add(new Chapter(ProcurementPolicyResultsComponent.ID, M.messages().pdfProjectCalculationCoverPageTocResults(), ""));
      chapters.add(new Chapter(EmissionSources.ID, M.messages().pdfProjectCalculationCoverPageTocEmissionSourceDetails(), ""));
      break;
    default:
      break;
    }
    return chapters;
  }

  @Computed
  public String getCoverIllustration() {
    final DataResource cover;

    switch (printOwN2000Context.getPdfProductType()) {
    case OWN2000_CALCULATOR:
    case OWN2000_SINGLE_SCENARIO:
      cover = img.printCoverIllustrationOwN2000();
      break;
    case OWN2000_PERMIT:
      cover = img.printCoverIllustrationOwN2000Register();
      break;
    case OWN2000_EDGE_EFFECT_APPENDIX:
    case OWN2000_EXTRA_ASSESSMENT_APPENDIX:
      cover = img.printCoverIllustrationOwN2000Appendix();
      break;
    case OWN2000_LBV_POLICY:
      cover = img.printCoverIllustrationOwN2000LbvPolicy();
      break;
    default:
      cover = null;
      break;
    }
    return cover == null ? "" : cover.getSafeUri().asString();
  }

  protected static class Chapter {
    private final String id;
    private final String name;
    private final String namePostfix;

    public Chapter(final String id, final String name, final String namePostfix) {
      this.id = id;
      this.name = name;
      this.namePostfix = namePostfix;
    }

    public String getId() {
      return id;
    }

    public String getName() {
      return name;
    }

    public String getNamePostfix() {
      return namePostfix;
    }
  }
}
