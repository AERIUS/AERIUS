/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.warning;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import elemental2.dom.Event;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.directives.VectorDirective;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.input.InputWarningComponent;
import nl.overheid.aerius.wui.application.daemon.misc.PersistablePreferencesContext;
import nl.overheid.aerius.wui.application.daemon.misc.PreferencesDaemon;
import nl.overheid.aerius.wui.application.daemon.misc.UserSettingChangeCommand;
import nl.overheid.aerius.wui.vue.BasicVueView;
import nl.overheid.aerius.wui.vue.DebugDirective;

@Component(components = {
    ButtonIcon.class,
    InputWarningComponent.class,
}, directives = {
    VectorDirective.class,
    DebugDirective.class,
})
public class PrivacyWarning extends BasicVueView {
  @Prop(required = true) EventBus eventBus;

  @Inject @Data PersistablePreferencesContext prefs;

  @JsMethod
  public void dontShowWarningAgain(final Event event) {
    event.preventDefault();
    event.stopPropagation();

    eventBus.fireEvent(new UserSettingChangeCommand(PreferencesDaemon.HIDE_PRIVACY_MESSAGE, true));
  }
}
