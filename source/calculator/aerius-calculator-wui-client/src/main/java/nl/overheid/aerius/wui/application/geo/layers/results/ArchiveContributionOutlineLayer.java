/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.results;

import java.util.Locale;
import java.util.Optional;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.OLFactory;
import ol.layer.Image;
import ol.layer.Layer;
import ol.source.ImageWms;
import ol.source.ImageWmsOptions;
import ol.source.ImageWmsParams;

import nl.aerius.geo.domain.IsLayer;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.application.event.CalculationStatusEvent;
import nl.overheid.aerius.wui.application.event.SwitchActiveCalculationEvent;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;

/**
 * Provides the deposition result layer
 */
public class ArchiveContributionOutlineLayer implements IsLayer<Layer> {
  private static final String WMS_LAYER_NAME = "calculator:wms_archive_contribution_outline";

  private static final ArchiveContributionOutlineEventBinder EVENT_BINDER = GWT.create(ArchiveContributionOutlineEventBinder.class);

  interface ArchiveContributionOutlineEventBinder extends EventBinder<ArchiveContributionOutlineLayer> {
  }

  private final Image layer;

  private final ImageWmsParams imageWmsParams;
  private final ImageWmsOptions imageWmsOptions;
  private ImageWms imageWms;

  private final CalculationContext calculationContext;

  @Inject
  public ArchiveContributionOutlineLayer(final EventBus eventBus, final EnvironmentConfiguration configuration, final CalculationContext calculationContext) {

    this.calculationContext = calculationContext;

    imageWmsParams = OLFactory.createOptions();

    imageWmsOptions = OLFactory.createOptions();
    imageWmsOptions.setUrl(configuration.getGeoserverBaseUrl());
    imageWmsOptions.setParams(imageWmsParams);

    layer = new Image();
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  public void onCalculationStatusEvent(final CalculationStatusEvent e) {
    final CalculationExecutionContext calculation = calculationContext.getInitializedCalculation(e.getJobKey());
    if (calculationContext.isActiveCalculation(calculation)) {
      updateLayer();
    }
  }

  @EventHandler
  public void onSwitchActiveCalculationEvent(final SwitchActiveCalculationEvent e) {
    updateLayer();
  }

  private SituationResultsKey getSituationResultsKey() {
    return Optional.ofNullable(calculationContext.getActiveCalculation())
        .map(v -> v.getResultContext())
        .map(v -> v.getSituationResultsKey())
        .orElse(null);
  }

  private String buildViewParams(final SituationResultsKey situationResultsKey) {
    final String situationId = situationResultsKey.getSituationHandle().getId();
    final SummaryHexagonType hexagonType = situationResultsKey.getHexagonType();

    String viewParams = "";
    viewParams += "job_key:" + calculationContext.getActiveJobId();
    viewParams += ";situation_reference:" + situationId;
    viewParams += ";hexagon_type:" + hexagonType.name().toLowerCase(Locale.ROOT);
    return viewParams;
  }

  private void updateLayer() {
    final SituationResultsKey situationResultsKey = getSituationResultsKey();
    if (situationResultsKey == null) {
      return;
    }

    if (situationResultsKey.getSituationHandle() == null) {
      return;
    }

    if (situationResultsKey.getHexagonType() == SummaryHexagonType.CUSTOM_CALCULATION_POINTS) {
      return; // handled by the CalculationPointResultLayer
    }

    imageWmsParams.setViewParams(buildViewParams(situationResultsKey));
    imageWmsParams.setLayers(WMS_LAYER_NAME);
    final int numberOfPointsCalculated = Optional.ofNullable(calculationContext.getActiveCalculation())
        .map(v -> v.getCalculationInfo())
        .map(v -> v.getJobProgress())
        .map(v -> v.getNumberOfPointsCalculated())
        .orElse(0);
    imageWmsParams.set("hc", String.valueOf(numberOfPointsCalculated));

    if (imageWms == null) {
      imageWms = new ImageWms(imageWmsOptions);
      layer.setSource(imageWms);
    }

    imageWms.refresh();

  }

  @Override
  public Layer asLayer() {
    return layer;
  }

}
