/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.road;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.wui.vue.ReactivityUtil;
import nl.overheid.aerius.wui.vue.SkipReactivityValidations;

/**
 * Client side implementation of SpecificVehicles props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class SpecificVehicles extends Vehicles {

  private String vehicleCode;
  private double vehiclesPerTimeUnit;

  public static final @JsOverlay SpecificVehicles create() {
    final SpecificVehicles props = Js.uncheckedCast(JsPropertyMap.of());
    init(props);
    return props;
  }

  public static final @JsOverlay void init(final SpecificVehicles props) {
    Vehicles.initVehicles(props, VehicleType.SPECIFIC);
    ReactivityUtil.ensureJsProperty(props, "vehicleCode", props::setVehicleCode, "");
    ReactivityUtil.ensureJsProperty(props, "vehiclesPerTimeUnit", props::setVehiclesPerTimeUnit, 0D);
  }

  public final @JsOverlay String getVehicleCode() {
    return vehicleCode;
  }

  public final @JsOverlay void setVehicleCode(final String vehicleCode) {
    this.vehicleCode = vehicleCode;
  }

  public final @JsOverlay double getVehiclesPerTimeUnit() {
    return vehiclesPerTimeUnit;
  }

  public final @JsOverlay void setVehiclesPerTimeUnit(final double vehiclesPerTimeUnit) {
    this.vehiclesPerTimeUnit = vehiclesPerTimeUnit;
  }

  @SkipReactivityValidations
  public final @JsOverlay double getVehiclesPer(final TimeUnit targetTimeUnit) {
    return this.getTimeUnit() == targetTimeUnit ? vehiclesPerTimeUnit : getTimeUnit().toUnit(vehiclesPerTimeUnit, targetTimeUnit);
  }
}
