/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.nav;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.wui.application.command.EasterActivationCommand;
import nl.overheid.aerius.wui.application.command.EasterDeactivationCommand;
import nl.overheid.aerius.wui.application.event.EasterActivationEvent;
import nl.overheid.aerius.wui.application.event.EasterDeactivationEvent;
import nl.overheid.aerius.wui.vue.BasicVueEventComponent;

/**
 * Easter egg activation component.
 *
 * - Activates after 3 rapid clicks (within 1 second between clicks)
 * - Deactivates when clicking again after at least 1 second has passed
 * - Click counter resets if more than 1 second passes between clicks
 */
@Component
public class EasterActivatorComponent extends BasicVueEventComponent {
  interface EasterActivatorComponentEventBinder extends EventBinder<EasterActivatorComponent> {
  }

  private final EasterActivatorComponentEventBinder EVENT_BINDER = GWT.create(EasterActivatorComponentEventBinder.class);

  @Prop EventBus eventBus;

  @Data int counter = 0;
  @Data boolean easterActive = false;

  private int resetCounter = 0;
  private long activationTime = 0;

  @JsMethod
  public void activateEaster() {
    if (easterActive) {
      final long currentTime = System.currentTimeMillis();
      if (currentTime - activationTime >= 1000) {
        eventBus.fireEvent(new EasterDeactivationCommand());
        counter = 0;
        return;
      }
      return;
    }

    counter++;

    if (counter >= 3) {
      counter = 0;
      eventBus.fireEvent(new EasterActivationCommand());
    } else {
      final int currentResetCounter = ++resetCounter;
      SchedulerUtil.delay(() -> {
        if (currentResetCounter == resetCounter) {
          resetCounter();
        }
      }, 1000);
    }
  }

  @JsMethod
  public boolean isEasterAvailable() {
    return isMapGameAvailable();
  }

  @EventHandler
  public void onEasterActivationEvent(EasterActivationEvent event) {
    easterActive = true;
    activationTime = System.currentTimeMillis();
    counter = 0;
  }

  @EventHandler
  public void onEasterDeactivationEvent(EasterDeactivationEvent event) {
    easterActive = false;
  }

  @JsMethod
  public void resetCounter() {
    counter = 0;
  }

  @JsMethod
  public void created() {
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  private native boolean isMapGameAvailable() /*-{
    return $wnd.__MAP_GAME__ && $wnd.__MAP_GAME__.available;
  }-*/;
}
