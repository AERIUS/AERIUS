/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Singleton;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;

@Singleton
public class RoadNetworkContext {

  private final @JsProperty Set<EmissionSourceFeature> highlightedSources = new HashSet<>();

  public Set<EmissionSourceFeature> getHighlightedSources() {
    return highlightedSources;
  }

  public void toggleHighlight(final EmissionSourceFeature feature) {
    if (highlightedSources.contains(feature)) {
      highlightedSources.remove(feature);
    } else {
      highlightedSources.add(feature);
    }
  }

  public void reset() {
    highlightedSources.clear();
  }

  public boolean isHighlighted(final EmissionSourceFeature feature) {
    return highlightedSources.contains(feature);
  }

  public int highlightedSize() {
    return highlightedSources.size();
  }
}
