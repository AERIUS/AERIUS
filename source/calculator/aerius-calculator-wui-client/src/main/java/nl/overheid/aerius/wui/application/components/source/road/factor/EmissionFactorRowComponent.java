/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.source.road.factor;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.wui.application.components.input.InputWithUnitComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.road.factor.EmissionFactorValidators.EmissionFactorValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.EmissionFactors;
import nl.overheid.aerius.wui.application.domain.source.road.RoadTypeMode;
import nl.overheid.aerius.wui.vue.DebugDirective;

/**
 * Shows an emission factor editor with label.
 */
@Component(customizeOptions = {
    EmissionFactorValidators.class
}, directives = {
    ValidateDirective.class,
    DebugDirective.class,
}, components = {
    LabeledInputComponent.class,
    InputWithUnitComponent.class,
    ValidationBehaviour.class
})
public class EmissionFactorRowComponent extends ErrorWarningValidator implements HasCreated,
    HasValidators<EmissionFactorValidations> {
  @Prop Substance substance;
  @Prop RoadTypeMode mode;
  @Prop @JsProperty EmissionFactors emissionFactors;
  @Data String emissionFactorV;

  @Data @Inject ApplicationContext applicationContext;

  @JsProperty(name = "$v") EmissionFactorValidations validations;

  @PropDefault("mode")
  RoadTypeMode modeDefault() {
    return RoadTypeMode.ROAD;
  }

  @Watch(value = "emissionFactors", isImmediate = true)
  public void onEmissionFactorsChange() {
    emissionFactorV = String.valueOf(emissionFactors.getEmissionFactor(substance));
  }

  @Override
  @Computed
  public EmissionFactorValidations getV() {
    return validations;
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Computed
  protected String getEmissionFactor() {
    return emissionFactorV;
  }

  @Computed
  protected void setEmissionFactor(final String emissionFactor) {
    try {
      final double ef = Double.valueOf(emissionFactor);
      if (!Double.isNaN(ef)) {
        emissionFactors.setEmissionFactor(substance, ef);
      }
    } catch (final NumberFormatException e) {
    }
    emissionFactorV = emissionFactor;
    valueModified();
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return validations.emissionFactorV.invalid;
  }

  @Computed
  public String getUnit() {
    return mode == RoadTypeMode.COLD_START ? i18n.unitSingularGramPerColdStart()
        : applicationContext.isAppFlagEnabled(AppFlag.USE_ROAD_CUSTOM_UNITS_G_KM_S)
            ? i18n.unitSingularGramPerKilometersPerSecond()
            : i18n.unitSingularGramPerKilometers();
  }

  @Computed
  public String getUnitDescription() {
    return mode == RoadTypeMode.COLD_START ? i18n.unitSingularGramPerColdStartDescription()
        : applicationContext.isAppFlagEnabled(AppFlag.USE_ROAD_CUSTOM_UNITS_G_KM_S)
            ? i18n.unitSingularGramPerKilometersPerSecondDescription()
            : i18n.unitSingularGramPerKilometersDescription();
  }

  @JsMethod
  protected String emissionFactorConversionError() {
    return i18n.errorNotValidEntry(emissionFactorV);
  }

  protected void valueModified() {
    vue().$emit("valueModified");
  }
}
