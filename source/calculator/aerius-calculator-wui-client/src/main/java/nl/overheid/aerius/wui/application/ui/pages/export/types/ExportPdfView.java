/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.ui.pages.export.types;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.place.PlaceController;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.export.ExportAppendix;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.command.ResetEdgeEffectReportExportSettingsCommand;
import nl.overheid.aerius.wui.application.command.exporter.ExportPrepareCommand;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.input.CheckBoxComponent;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.InputNoticeComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.notice.NoticeRule;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ExportContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationPreparationContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.place.CalculatePlace;
import nl.overheid.aerius.wui.application.ui.pages.emissionsource.ErrorWarningIconComponent;
import nl.overheid.aerius.wui.application.ui.pages.export.metadata.ExportMetaDataComponent;
import nl.overheid.aerius.wui.application.ui.pages.export.types.components.CalculationJobSelectorComponent;
import nl.overheid.aerius.wui.application.ui.pages.export.types.options.ExportCalculationOptionsComponent;

@Component(components = {
    SimplifiedListBoxComponent.class,
    LabeledInputComponent.class,
    CheckBoxComponent.class,
    ExportMetaDataComponent.class,
    InputNoticeComponent.class,
    InputErrorComponent.class,
    ErrorWarningIconComponent.class,
    CollapsiblePanel.class,
    ValidationBehaviour.class,
    ExportCalculationOptionsComponent.class,
    CalculationJobSelectorComponent.class,
    VerticalCollapse.class,
})
public class ExportPdfView extends ErrorWarningValidator implements HasMounted {
  enum Panel {
    META_DATA, APPENDICES
  }

  @Prop boolean metaDataEnabled;
  @Prop EventBus eventBus;

  @Inject @Data CalculationPreparationContext preparationContext;

  @Inject @Data ScenarioContext scenarioContext;
  @Inject @Data ApplicationContext applicationContext;
  @Inject @Data ExportContext exportContext;
  @Inject @Data PlaceController placeController;

  @Data Panel openPanel = null;

  @Override
  public void mounted() {
    final CalculationJobContext activeJob = preparationContext.getActiveJob();
    if (preparationContext.getJobs().size() == 1
        && activeJob != null
        && exportContext.getReportType() == activeJob.getCalculationOptions().getCalculationJobType()) {
      eventBus.fireEvent(new ExportPrepareCommand(activeJob, false));
    }
  }

  @Computed
  public List<CalculationJobType> getExportReportOptions() {
    return applicationContext.getConfiguration().getAvailableExportReportOptions();
  }

  @Computed
  public SituationComposition getSituationComposition() {
    return exportContext.getSituationComposition();
  }

  @JsMethod
  public boolean isOpen(final Panel panel) {
    return openPanel == panel;
  }

  @JsMethod
  public void openPanel(final Panel panel) {
    openPanel = panel;
  }

  @JsMethod
  public void closePanel() {
    openPanel = null;
  }

  @JsMethod
  public void openErrorPanel() {
    if (displayProblems) {
      if (hasInvalidMetaData()) {
        openPanel = Panel.META_DATA;
      }
    }

    displayProblems(displayProblems);
  }

  @JsMethod
  public void selectExportReportType(final CalculationJobType type) {
    exportContext.setReportType(type);
    eventBus.fireEvent(new ResetEdgeEffectReportExportSettingsCommand());
  }

  @JsMethod
  public boolean hasInvalidMetaData() {
    return metaDataEnabled
        && (exportContext.isIncludeScenarioMetaData() || isMetaDataRequired())
        && hasChildError("metadata");
  }

  @Computed("areOptionsVisible")
  public boolean areOptionsVisible() {
    return areOptionsEnabled()
        || exportContext.getCalculationOptions().getCalculationMethod() != CalculationMethod.FORMAL_ASSESSMENT;
  }

  @Computed("areOptionsEnabled")
  public boolean areOptionsEnabled() {
    return applicationContext.getTheme() == Theme.NCA;
  }

  @Computed("areAppendicesEnabled")
  public boolean areAppendicesEnabled() {
    return applicationContext.isAppFlagEnabled(AppFlag.SHOW_REPORT_APPENDICES);
  }

  @Computed("isEdgeEffectEnabled")
  public boolean isEdgeEffectEnabled() {
    if (!applicationContext.isAppFlagEnabled(AppFlag.SHOWS_OVERLAPPING_EFFECTS)) {
      return false;
    }

    final boolean isProjectCalculation = exportContext.getReportType() == CalculationJobType.PROCESS_CONTRIBUTION;
    final boolean hasReference = Optional.ofNullable(getSituationComposition())
        .map(sc -> sc.getSituation(SituationType.REFERENCE) != null)
        .orElse(false);

    return isProjectCalculation && hasReference;
  }

  @JsMethod
  public boolean getAppendixEnabled(final ExportAppendix appendix) {
    return exportContext.getAppendices().contains(appendix);
  }

  @JsMethod
  public void setAppendixEnabled(final ExportAppendix appendix, final boolean enabled) {
    if (enabled) {
      exportContext.getAppendices().add(appendix);
    } else {
      exportContext.getAppendices().remove(appendix);
    }
  }

  @Computed("isMetaDataRequired")
  public boolean isMetaDataRequired() {
    return exportContext.getReportType() != CalculationJobType.SINGLE_SCENARIO;
  }

  @Computed("metaDataPanelTitle")
  public String metaDataPanelTitle() {
    if (isMetaDataRequired()) {
      return i18n.exportPDFAdditionalInfoLabel();
    } else {
      return i18n.exportGMLWithMetaData();
    }
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return hasInvalidMetaData()
        || hasInvalidCalculationJob()
        || isReportInvalid();
  }

  private boolean hasInvalidCalculationJob() {
    return hasChildError("job-selection");
  }

  @Computed("isReportInvalid")
  public boolean isReportInvalid() {
    return !getShowingReportErrors().isEmpty();
  }

  @Computed("showingReportErrors")
  public List<NoticeRule> getShowingReportErrors() {
    return getReportErrors()
        .filter(NoticeRule::showing)
        .collect(Collectors.toList());
  }

  @Computed
  public Stream<NoticeRule> getReportErrors() {
    // Return an alternative set of possible errors when there is no composition
    if (exportContext.getSituationComposition() == null) {
      return Stream.of(
          NoticeRule.create(
              () -> exportContext.getSituationComposition() == null,
              null));
    }

    // TODO: Validate for the criteria as defined by the CalculationJobType (i.e. required, optional, multiple)
    return Stream.of();
  }

  @JsMethod
  public void editJob() {
    placeController.goTo(new CalculatePlace(applicationContext.getTheme()));
  }

  @JsMethod
  public void refreshProblems(final boolean value) {
    displayProblems(value);
    openErrorPanel();
  }
}
