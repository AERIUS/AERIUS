/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.source.farmlodging.standard;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.config.AppFlag;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategories;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingSystemDefinition;
import nl.overheid.aerius.shared.emissions.FarmEmissionFactorType;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.InputWarningComponent;
import nl.overheid.aerius.wui.application.components.input.MinimalInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.input.suggest.SuggestListBoxComponent;
import nl.overheid.aerius.wui.application.components.input.suggest.SuggestListBoxData;
import nl.overheid.aerius.wui.application.components.source.farmlodging.detail.FarmLodgingDetailHeaderComponent;
import nl.overheid.aerius.wui.application.components.source.farmlodging.standard.FarmLodgingStandardValidators.FarmLodgingStandardValidations;
import nl.overheid.aerius.wui.application.components.source.farmlodging.systems.FarmLodgingMeasureComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmLodgingStandardUtil;
import nl.overheid.aerius.wui.application.domain.source.farm.StandardFarmLodging;

/**
 * TODO When SuggestListBox is modeled after SimplifiedListBox it will be
 * possible to remove selectedFarmLodgingCategory, but until that time the
 * roundabout way of selecting and maintaining a duplicate local model of that
 * object will be necessary.
 */
@Component(customizeOptions = {
    FarmLodgingStandardValidators.class
}, directives = {
    ValidateDirective.class,
}, components = {
    MinimalInputComponent.class,
    SuggestListBoxComponent.class,
    SimplifiedListBoxComponent.class,
    FarmLodgingMeasureComponent.class,
    FarmLodgingDetailHeaderComponent.class,
    FarmLodgingDetailStandardComponent.class,
    ValidationBehaviour.class,
    InputErrorComponent.class,
    InputWarningComponent.class,
})
public class FarmLodgingStandardEmissionComponent extends ErrorWarningValidator implements HasCreated, HasValidators<FarmLodgingStandardValidations> {
  @Prop @JsProperty StandardFarmLodging standardFarmLodging;

  @Data @Inject ApplicationContext applicationContext;

  @Data String farmLodgingCodeV;
  @Data String numberOfAnimalsV;
  @Data String numberOfDaysV;
  @Data String systemDefinitionCodeV;

  @JsProperty(name = "$v") FarmLodgingStandardValidations validation;

  @Data @JsProperty List<FarmLodgingSystemDefinition> farmLodgingSystemDefinitions = new ArrayList<>();
  @Data FarmLodgingCategory selectedFarmLodgingCategory;

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Override
  @Computed
  public FarmLodgingStandardValidations getV() {
    return validation;
  }

  @Watch(value = "standardFarmLodging", isImmediate = true)
  public void onStandardFarmLodgingChange(final StandardFarmLodging neww) {
    if (neww == null) {
      return;
    }

    farmLodgingCodeV = neww.getFarmLodgingCode();
    updateFarmLodgingSystemFromCode(neww.getFarmLodgingCode());
    numberOfAnimalsV = String.valueOf(neww.getNumberOfAnimals());
    numberOfDaysV = String.valueOf(neww.getNumberOfDays());
    systemDefinitionCodeV = neww.getSystemDefinitionCode();
  }

  @Computed("standardLodgingCodeWarningEnabled")
  public boolean isStandardLodgingCodeWarningEnabled() {
    return applicationContext.isAppFlagEnabled(AppFlag.SHOW_FARM_ANIMAL_HOUSING_STANDARD_CODE_WARNING);
  }

  @Computed
  public List<SuggestListBoxData> getLodgingCodes() {
    return getCategories().getFarmLodgingSystemCategories().stream()
        .map(v -> new SuggestListBoxData(v.getCode(), v.getName(), v.getName() + " " + v.getDescription(), false))
        .collect(Collectors.toList());
  }

  private void updateFarmLodgingSystemFromCode(final String code) {
    selectedFarmLodgingCategory = getCategories().getFarmLodgingSystemCategories().stream()
        .filter(v -> v.getCode().equals(code))
        .findFirst()
        .orElse(null);
  }

  @Watch(value = "selectedFarmLodgingCategory", isImmediate = true)
  public void onSelectedFarmLodgingCategory(final FarmLodgingCategory neww) {
    if (neww == null) {
      farmLodgingSystemDefinitions = new ArrayList<>();
      farmLodgingCodeV = null;
    } else {
      farmLodgingSystemDefinitions = neww.getFarmLodgingSystemDefinitions();
      farmLodgingCodeV = neww.getCode();
    }

    // If there is only 1 option, select it
    if (farmLodgingSystemDefinitions.size() == 1) {
      selectLodgingSystemDefinition(farmLodgingSystemDefinitions.get(0));
    } else if (!farmLodgingSystemDefinitions.stream()
        .anyMatch(v -> v.getCode().equals(standardFarmLodging.getSystemDefinitionCode()))) {
      // If the selected system definition code does not exist in the new list,
      // deselect it
      selectLodgingSystemDefinition(null);
    }
  }

  @JsMethod
  public FarmLodgingCategories getCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getFarmLodgingCategories();
  }

  @JsMethod
  public boolean hasFarmReductiveLodgingSystemCategories() {
    return FarmLodgingStandardUtil.hasFarmReductiveLodgingSystemCategories(getCategories());
  }

  @Computed
  public String getNumberOfAnimals() {
    return numberOfAnimalsV;
  }

  @Computed
  public void setNumberOfAnimals(final String numberOfAnimals) {
    ValidationUtil.setSafeIntegerValue(v -> standardFarmLodging.setNumberOfAnimals(v), numberOfAnimals, 0);
    numberOfAnimalsV = numberOfAnimals;
  }

  @Computed("isNumberOfDaysEnabled")
  public boolean isNumberOfDaysEnabled() {
    return selectedFarmLodgingCategory != null
        && selectedFarmLodgingCategory.getFarmEmissionFactorType() == FarmEmissionFactorType.PER_ANIMAL_PER_DAY;
  }

  @Computed("isInvalidNumberOfDays")
  public boolean isInvalidNumberOfDays() {
    return isNumberOfDaysEnabled() && validation.numberOfDaysV.error;
  }

  @Computed
  public String getNumberOfDays() {
    return numberOfDaysV;
  }

  @Computed
  public void setNumberOfDays(final String numberOfDays) {
    ValidationUtil.setSafeIntegerValue(v -> standardFarmLodging.setNumberOfDays(v), numberOfDays, 0);
    numberOfDaysV = numberOfDays;
  }

  @JsMethod
  public String numberOfAnimalsError() {
    return i18n.errorNumeric(numberOfAnimalsV);
  }

  @JsMethod
  public String numberOfDaysError() {
    return numberOfDaysV == null || numberOfDaysV.isEmpty()
        ? i18n.validationRequired()
        : i18n.ivIntegerRangeBetween(numberOfDaysV, FarmLodgingStandardValidators.MIN_NUMBER_OF_DAYS,
            FarmLodgingStandardValidators.MAX_NUMBER_OF_DAYS);
  }

  @JsMethod
  public void unselectFarmLodgingCode() {
    standardFarmLodging.setFarmLodgingCode(null);
    selectedFarmLodgingCategory = null;
  }

  @JsMethod
  void selectFarmLodgingCode(final String code) {
    standardFarmLodging.setFarmLodgingCode(code);
    updateFarmLodgingSystemFromCode(code);
  }

  @Computed
  public String getLodgingSystemDefinitionCode() {
    return systemDefinitionCodeV;
  }

  @JsMethod
  public void selectLodgingSystemDefinition(final FarmLodgingSystemDefinition definition) {
    standardFarmLodging.setSystemDefinitionCode(definition == null ? null : definition.getCode());
    systemDefinitionCodeV = definition == null ? null : definition.getCode();
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return validation.farmLodgingCodeV.invalid
        || validation.numberOfAnimalsV.invalid
        || (isNumberOfDaysEnabled() && validation.numberOfDaysV.invalid)
        // If there are no options, system definition is not required.
        || (!farmLodgingSystemDefinitions.isEmpty() && validation.systemDefinitionCodeV.invalid);
  }
}
