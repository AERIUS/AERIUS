/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.emissionsourceList;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasDestroyed;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.HorizontalCollapse;
import nl.overheid.aerius.shared.domain.context.InputTypeViewMode;
import nl.overheid.aerius.wui.application.command.building.BuildingFeatureDeselectCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceFeatureDeselectCommand;
import nl.overheid.aerius.wui.application.command.timevarying.TimeVaryingProfileDeselectCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.BuildingListContext;
import nl.overheid.aerius.wui.application.context.EmissionSourceListContext;
import nl.overheid.aerius.wui.application.context.TimeVaryingProfileListContext;
import nl.overheid.aerius.wui.application.ui.main.LeftPanelCloseEvent;
import nl.overheid.aerius.wui.application.ui.nca.pages.emisionsourceList.timevarying.TimeVaryingProfileDetailView;
import nl.overheid.aerius.wui.application.ui.pages.emissionsourceList.building.BuildingDetailView;
import nl.overheid.aerius.wui.application.ui.pages.emissionsourceList.source.EmissionSourceListDetailView;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component(components = {
    EmissionSourceListDetailView.class,
    BuildingDetailView.class,
    TimeVaryingProfileDetailView.class,
    HorizontalCollapse.class,
    ButtonIcon.class,
})
public class ScenarioInputListDetailView extends BasicVueView implements HasCreated, HasDestroyed {
  private static final ScenarioInputListDetailViewEventBinder EVENT_BINDER = GWT.create(ScenarioInputListDetailViewEventBinder.class);

  interface ScenarioInputListDetailViewEventBinder extends EventBinder<ScenarioInputListDetailView> {}

  @Prop EventBus eventBus;

  @Inject @Data ApplicationContext applicationContext;
  @Inject @Data EmissionSourceListContext emissionSourceListContext;
  @Inject @Data ScenarioInputListContext inputListContext;
  @Inject @Data BuildingListContext buildingListContext;
  @Inject @Data TimeVaryingProfileListContext timeVaryingProfileListContext;

  private HandlerRegistration handlers;

  @Computed("isBuildings")
  public boolean isBuildings() {
    return inputListContext.getViewMode() == InputTypeViewMode.BUILDING
        && buildingListContext.hasLooseSelection();
  }

  @Computed("isTimeVaryingProfiles")
  public boolean isTimeVaryingProfiles() {
    return inputListContext.getViewMode() == InputTypeViewMode.TIME_VARYING_PROFILES
        && timeVaryingProfileListContext.hasLooseSelection();
  }

  @Computed("isEmissionSources")
  public boolean isEmissionSources() {
    return inputListContext.getViewMode() == InputTypeViewMode.EMISSION_SOURCES
        && emissionSourceListContext.hasLooseSelection();
  }

  @Override
  public void created() {
    handlers = EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Override
  public void destroyed() {
    handlers.removeHandler();
  }

  @JsMethod
  public void close() {
    if (isEmissionSources()) {
      eventBus.fireEvent(new EmissionSourceFeatureDeselectCommand());
    } else if (isBuildings()) {
      eventBus.fireEvent(new BuildingFeatureDeselectCommand(buildingListContext.getLooseSelection()));
    } else if (isTimeVaryingProfiles()) {
      eventBus.fireEvent(new TimeVaryingProfileDeselectCommand(timeVaryingProfileListContext.getLooseSelection()));
    } else {
      // Do nothing
    }
  }

  @EventHandler
  public void onLeftPanelCloseEvent(final LeftPanelCloseEvent e) {
    close();
  }
}
