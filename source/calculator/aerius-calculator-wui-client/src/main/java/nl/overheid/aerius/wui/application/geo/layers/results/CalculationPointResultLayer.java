/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.results;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.Collection;
import ol.Feature;
import ol.OLFactory;
import ol.color.Color;
import ol.format.GeoJson;
import ol.format.GeoJsonOptions;
import ol.layer.Layer;
import ol.source.Vector;
import ol.source.VectorOptions;
import ol.style.Circle;
import ol.style.CircleOptions;
import ol.style.Fill;
import ol.style.Stroke;
import ol.style.Style;
import ol.style.StyleOptions;

import nl.aerius.geo.domain.IsLayer;
import nl.aerius.geo.domain.LayerInfo;
import nl.aerius.geo.domain.legend.Legend;
import nl.aerius.geo.domain.legend.LegendType;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.legend.ColorRangesLegend;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.command.result.ClearCustomCalculationPointResultCommand;
import nl.overheid.aerius.wui.application.command.result.SelectCustomCalculationPointResultCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.context.result.ResultSelectionContext;
import nl.overheid.aerius.wui.application.context.result.ResultSummaryContext;
import nl.overheid.aerius.wui.application.domain.result.CustomCalculationPointResult;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsSummary;
import nl.overheid.aerius.wui.application.event.SituationResultsSelectedEvent;
import nl.overheid.aerius.wui.application.i18n.ApplicationMessages;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.service.ColorRangeService;

public class CalculationPointResultLayer implements IsLayer<Layer> {

  private static final ApplicationMessages i18n = M.messages();
  private static final CalculationPointResultLayerEventBinder EVENT_BINDER = GWT.create(CalculationPointResultLayerEventBinder.class);

  public interface CalculationPointResultLayerEventBinder extends EventBinder<CalculationPointResultLayer> {}

  private static final Color CIRCLE_STROKE_COLOR = Color.getColorFromString("#FFFFFF");
  private static final Stroke CIRCLE_STROKE = OLFactory.createStroke(CIRCLE_STROKE_COLOR, 1);
  private static final int CIRCLE_RADIUS = 7;
  private static final Stroke CIRCLE_HIGHLIGHTED_STROKE = OLFactory.createStroke(Color.getColorFromString("#157cb1"), 5);
  private static final int CIRCLE_HIGHLIGHTED_RADIUS = 11;
  private static final Style POINT_SELECTION_STYLE = createSelectionStyle();

  private static final String FEATURE_PROPERTY = "customPointResult";

  private final LayerInfo info;
  private final ol.layer.Vector layer;
  private final Collection<Feature> featureCollection;
  private final Map<String, Style> styleMap = new HashMap<>();

  private final ApplicationContext applicationContext;
  private final CalculationContext calculationContext;
  private final ColorRangeService colorRangeService;
  private final ResultSelectionContext resultSelectionContext;

  @Inject
  public CalculationPointResultLayer(final EventBus eventBus, final ApplicationContext applicationContext,
      final CalculationContext calculationContext, final ColorRangeService colorRangeService, final ResultSelectionContext resultSelectionContext) {
    this.applicationContext = applicationContext;
    this.calculationContext = calculationContext;
    this.colorRangeService = colorRangeService;
    this.resultSelectionContext = resultSelectionContext;

    this.info = new LayerInfo();
    this.info.setName(CalculationPointResultLayer.class.getName());
    this.info.setTitle(i18n.layerCalculationPointsResults());

    featureCollection = new Collection<>();
    final VectorOptions vectorSourceOptions = OLFactory.createOptions();
    vectorSourceOptions.setFeatures(featureCollection);
    final Vector vectorSource = new Vector(vectorSourceOptions);

    this.layer = new ol.layer.Vector();
    this.layer.setSource(vectorSource);
    this.layer.setStyleFunction(this::renderStyle);

    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  public void handle(final SituationResultsSelectedEvent event) {
    if (event.getValue().getHexagonType() == SummaryHexagonType.CUSTOM_CALCULATION_POINTS) {
      drawCalculationPointResults();
    }
  }

  @EventHandler(handles = {SelectCustomCalculationPointResultCommand.class, ClearCustomCalculationPointResultCommand.class})
  public void handle() {
    layer.getSource().changed();
  }

  private void drawCalculationPointResults() {
    featureCollection.clear();

    final Optional<ResultSummaryContext> resultSummaryContextOptional = Optional.ofNullable(calculationContext.getActiveCalculation())
        .map(CalculationExecutionContext::getResultContext);
    if (!resultSummaryContextOptional.isPresent()) {
      return;
    }

    final ResultSummaryContext resultSummaryContext = resultSummaryContextOptional.get();
    if (!resultSummaryContext.hasActiveResultSummary()) {
      return;
    }

    final SituationResultsSummary resultSummary = resultSummaryContext.getActiveResultSummary();
    Arrays.stream(resultSummary.getCustomPointResults())
        .filter(CustomCalculationPointResult::hasResult)
        .map(this::createFeature)
        .forEach(featureCollection::push);

    this.info.setLegend(createLegend());
  }

  private Feature createFeature(final CustomCalculationPointResult result) {
    final Feature feature = new Feature();
    feature.set(FEATURE_PROPERTY, result);
    feature.setGeometry(new GeoJson().readGeometry(result.getPoint(), new GeoJsonOptions()));
    return feature;
  }

  private Legend createLegend() {
    return new ColorRangesLegend(
        colorRangeService.getColorRanges(),
        String::valueOf,
        i18n.colorRangesLegendBetweenText(),
        LegendType.CIRCLE,
        getUnit());
  }

  private Style[] renderStyle(final Feature feature, final double resolution) {
    final CustomCalculationPointResult customPointResult = feature.get(FEATURE_PROPERTY);
    final String color = colorRangeService.getColorForResult(customPointResult.getResult());

    if (resultSelectionContext.getSelectedCustomPointResult() == customPointResult) {
      return new Style[] {POINT_SELECTION_STYLE, getStyle(color)};
    } else {
      return new Style[] {getStyle(color)};
    }
  }

  private Style getStyle(final String color) {
    if (styleMap.containsKey(color)) {
      return styleMap.get(color);
    } else {
      final Fill fill = new Fill();
      fill.setColor(Color.getColorFromString(color));

      final CircleOptions circleOptions = new CircleOptions();
      circleOptions.setFill(fill);
      circleOptions.setStroke(CIRCLE_STROKE);
      circleOptions.setRadius(CIRCLE_RADIUS);
      final Circle circle = new Circle(circleOptions);

      final StyleOptions styleOptions = new StyleOptions();
      styleOptions.setImage(circle);
      final Style style = new Style(styleOptions);

      styleMap.put(color, style);
      return style;
    }
  }

  private static Style createSelectionStyle() {
    final Fill fill = new Fill();
    fill.setColor(CIRCLE_STROKE_COLOR);

    final CircleOptions circleOptions = new CircleOptions();
    circleOptions.setFill(fill);
    circleOptions.setStroke(CIRCLE_HIGHLIGHTED_STROKE);
    circleOptions.setRadius(CIRCLE_HIGHLIGHTED_RADIUS);
    final Circle circle = new Circle(circleOptions);

    final StyleOptions styleOptions = new StyleOptions();
    styleOptions.setImage(circle);
    return new Style(styleOptions);
  }

  private String getUnit() {
    final EmissionResultKey emissionResultKey = calculationContext.getActiveCalculation().getResultContext().getEmissionResultKey();
    final ApplicationConfiguration themeConfiguration = applicationContext.getConfiguration();

    return emissionResultKey.getEmissionResultType() == EmissionResultType.CONCENTRATION
        ? i18n.layerConcentrationResultsUnit()
        : i18n.layerDepositionResultsUnit(themeConfiguration.getEmissionResultValueDisplaySettings().getDisplayType());
  }

  @Override
  public Layer asLayer() {
    return layer;
  }

  @Override
  public LayerInfo getInfo() {
    return info;
  }
}
