/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.print.common.scenario.details;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.sector.SectorIcon;
import nl.overheid.aerius.shared.util.ColorUtil;
import nl.overheid.aerius.wui.application.components.common.StyledPointComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.print.common.PrintSection;
import nl.overheid.aerius.wui.application.print.common.scenario.details.source.PrintSourceBasics;
import nl.overheid.aerius.wui.application.print.common.scenario.details.source.PrintSourceCharacteristics;
import nl.overheid.aerius.wui.application.print.common.scenario.details.source.PrintSourceDetails;
import nl.overheid.aerius.wui.application.print.common.scenario.details.source.PrintSourceEmissions;
import nl.overheid.aerius.wui.application.print.common.scenario.details.source.PrintSourceProfiles;
import nl.overheid.aerius.wui.application.util.SectorColorUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    PrintSection.class,
    PrintSourceBasics.class,
    PrintSourceCharacteristics.class,
    PrintSourceDetails.class,
    PrintSourceEmissions.class,
    PrintSourceProfiles.class,
    StyledPointComponent.class,
})
public class PrintScenarioSourcesComponent extends BasicVueComponent {
  @Prop SituationContext situation;

  @Inject @Data ApplicationContext appContext;

  @JsMethod
  public SectorIcon getSectorIcon(final EmissionSourceFeature source) {
    return SectorColorUtil.getSectorIcon(appContext.getConfiguration(), source.getSectorId());
  }

  @JsMethod
  public String getSectorColor(final EmissionSourceFeature source) {
    return ColorUtil.webColor(getSectorIcon(source).getColor());
  }
}
