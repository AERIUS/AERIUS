/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.road.custom;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.sector.category.OnRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.SimpleCategory;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.source.road.RoadNumberOfVehiclesComponent;
import nl.overheid.aerius.wui.application.components.source.road.custom.RoadCustomValidators.RoadCustomValidations;
import nl.overheid.aerius.wui.application.components.source.road.factor.EmissionFactorRowComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.EmissionFactors;
import nl.overheid.aerius.wui.application.domain.source.road.CustomVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.RoadTypeMode;
import nl.overheid.aerius.wui.vue.DebugDirective;

@Component(customizeOptions = {
    RoadCustomValidators.class
}, directives = {
    ValidateDirective.class,
    DebugDirective.class
}, components = {
    LabeledInputComponent.class,
    RoadNumberOfVehiclesComponent.class,
    EmissionFactorRowComponent.class,
    ValidationBehaviour.class,
    SimplifiedListBoxComponent.class
})
public class RoadCustomEmissionComponent extends ErrorWarningValidator implements HasCreated,
    HasValidators<RoadCustomValidations> {
  @Prop @JsProperty CustomVehicles customVehicles;
  @Prop RoadTypeMode mode;
  @Prop boolean includeVehicleType;

  @Data String descriptionV;
  @Data String vehicleTypeCodeV;

  @Data @Inject ApplicationContext applicationContext;

  @JsProperty(name = "$v") RoadCustomValidations validations;

  @PropDefault("includeVehicleType")
  boolean includeVehicleTypeDefault() {
    return false;
  }

  @Watch(value = "customVehicles", isImmediate = true)
  public void onCustomVehiclesChange() {
    descriptionV = customVehicles.getDescription();
    vehicleTypeCodeV = customVehicles.getVehicleCode();
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Override
  @Computed
  public RoadCustomValidations getV() {
    return validations;
  }

  @Computed
  public List<OnRoadMobileSourceCategory> getCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getOnRoadMobileSourceCategories();
  }

  @Computed("vehicleTypes")
  protected List<SimpleCategory> getVehicleTypes() {
    return applicationContext.getConfiguration().getSectorCategories().getRoadEmissionCategories().getVehicleTypes();
  }

  @Computed
  protected String getVehicleTypeCode() {
    return vehicleTypeCodeV;
  }

  @Computed
  protected void setVehicleTypeCode(final String vehicleTypeCode) {
    customVehicles.setVehicleCode(vehicleTypeCode);
    vehicleTypeCodeV = vehicleTypeCode;
  }

  @Computed
  protected String getDescription() {
    return customVehicles.getDescription();
  }

  @Computed
  protected void setDescription(final String description) {
    customVehicles.setDescription(description);
    descriptionV = description;
  }

  @Computed("specificVehicleCode")
  protected String getSpecificVehicleCode() {
    return "";
  }

  @Computed("specificVehicleCode")
  protected void setSpecificVehicleCode(final String vehicleCode) {
    if (!vehicleCode.isEmpty()) {
      vehicleCodeToSpecficChanged(vehicleCode);
    }
  }

  @Computed("emissionFactors")
  protected EmissionFactors getEmissionFactors() {
    return customVehicles.getEmissionFactors();
  }

  @Computed("substances")
  protected List<Substance> getSubstances() {
    return mode == RoadTypeMode.COLD_START
        ? applicationContext.getConfiguration().getSubstances()
        : applicationContext.getConfiguration().getEmissionSubstancesRoad();
  }

  @JsMethod
  protected String descriptionError() {
    return i18n.errorDescriptionRequired();
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return validations.descriptionV.invalid
        || includeVehicleType && validations.vehicleTypeCodeV.invalid;
  }

  @Computed("isShowEuroclass")
  protected boolean isShowEuroclass() {
    return applicationContext.getTheme() != Theme.NCA;
  }

  @JsMethod
  protected void vehicleCodeToSpecficChanged(final String vehicleCode) {
    vue().$emit("switchVehicleType", vehicleCode);
  }

}
