/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.print.own2000.source.road;

import java.util.Optional;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;

import nl.aerius.geo.wui.util.OL3GeometryUtil;
import nl.overheid.aerius.shared.domain.sector.category.SimpleCategory;
import nl.overheid.aerius.wui.application.components.source.road.detail.SRM2RoadDetailComponent;
import nl.overheid.aerius.wui.application.geo.util.GeoUtil;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.application.print.own2000.source.MinMaxLabelPrintComponent;

@Component(name = "aer-srm2road-detail-print", components = {
    MinMaxLabelPrintComponent.class,
    RoadVehiclesPrintComponent.class,
})
public class SRM2RoadDetailPrintComponent extends SRM2RoadDetailComponent {

  @Computed
  public String getLocation() {
    return Optional.ofNullable(OL3GeometryUtil.getMiddlePointOfGeometry(source.getGeometry()))
      .map(v -> MessageFormatter.formatPoint(v))
      .orElse(null);
  }

  @Computed
  public String getLocationStatisticLabel() {
    return GeoUtil.getGeometryStatisticLabel(source.getGeometry());
  }

  @Computed
  public String getLocationStatisticValue() {
    return GeoUtil.getGeometryStatisticValue(source.getGeometry());
  }

  @Computed
  public String roadTypeDescription() {
    final SimpleCategory typeCategory = getRoadEmissionCategories().getRoadType(source.getRoadTypeCode());
    return typeCategory == null ? "" : typeCategory.getDescription();
  }
}
