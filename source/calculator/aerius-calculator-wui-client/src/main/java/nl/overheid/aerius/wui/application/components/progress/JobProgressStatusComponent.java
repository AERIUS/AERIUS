/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.progress;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.ServerUsage;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.calculation.JobProgress;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

/**
 * Component that shows a box wiht the calculation status.
 */
@Component
public class JobProgressStatusComponent extends BasicVueComponent {
  @Prop JobProgress jobProgress;

  @Inject @Data ApplicationContext applicationContext;

  private ServerUsage serverUsage;

  @Computed
  protected String getText() {
    return JobProgressTextUtil.getJobProgressText(jobProgress, applicationContext.getTheme());
  }

  @JsMethod
  protected String getCssServerUsage() {
    final JobState jobState = jobProgress == null ? JobState.UNDEFINED : jobProgress.getState();

    if (!jobState.isFinalState()) {
      if (serverUsage == ServerUsage.ABOVE_AVERAGE) {
        return "warning-usage-high";
      } else if (serverUsage == ServerUsage.VERY_HIGH) {
        return "warning-usage-very-high";
      }
    }
    return ""; // No warning
  }
}
