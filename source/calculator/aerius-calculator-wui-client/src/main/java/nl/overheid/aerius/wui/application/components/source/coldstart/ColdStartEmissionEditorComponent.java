/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.source.coldstart;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.wui.application.components.modify.ModifyListComponent;
import nl.overheid.aerius.wui.application.components.source.SubSourceEditorComponent;
import nl.overheid.aerius.wui.application.components.source.SubSourceValidationBehaviour;
import nl.overheid.aerius.wui.application.components.source.validation.SubSourceEmptyError;
import nl.overheid.aerius.wui.application.components.source.validation.SubSourceValidatedRowComponent;
import nl.overheid.aerius.wui.application.domain.source.ColdStartESFeature;
import nl.overheid.aerius.wui.application.domain.source.road.Vehicles;

@Component(components = {
    ModifyListComponent.class,
    ColdStartEmissionEditorRowComponent.class,
    SubSourceValidatedRowComponent.class,
    SubSourceValidationBehaviour.class,
    SubSourceEmptyError.class,
    VerticalCollapseGroup.class,
})
public class ColdStartEmissionEditorComponent extends SubSourceEditorComponent implements HasCreated {
  @Prop ColdStartESFeature source;

  @Inject @Data ColdStartEmissionEditorActivity presenter;

  @Override
  public void created() {
    presenter.setView(this);
  }

  @JsMethod
  public void selectSource(final int index, final Vehicles selectedSource) {
    if (selectedIndex == index) {
      selectedIndex = -1;
      resetSelected();
    } else {
      selectedIndex = index;
      presenter.selectedSource(selectedSource);
      editSource();
    }
  }
}
