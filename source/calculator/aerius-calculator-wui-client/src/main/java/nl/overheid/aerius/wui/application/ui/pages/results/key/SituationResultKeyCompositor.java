/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.results.key;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.summary.ResultDisplaySet;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationHandle;
import nl.overheid.aerius.wui.application.context.result.ResultSummaryContext;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.application.util.ResultDisplaySetUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    SimplifiedListBoxComponent.class
})
public class SituationResultKeyCompositor extends BasicVueComponent {
  @Prop ResultSummaryContext resultContext;
  @Prop CalculationExecutionContext calculationJob;

  @Data @Inject CalculationContext calculationContext;
  @Data @Inject ScenarioContext scenarioContext;
  @Data @Inject ApplicationContext applicationContext;

  // Until the result type is manually changed, automatically change the result type according to the job type and situation type
  private boolean autoSwitchingResultType = true;

  @Computed
  public List<ScenarioResultType> getResultTypes() {
    final Optional<CalculationJobContext> jobContext = Optional.ofNullable(calculationJob.getJobContext());
    return ScenarioResultType.getResultTypesForSituationType(resultContext.getSituationHandle().getType(),
        jobContext
            .map(CalculationJobContext::getCalculationOptions)
            .map(CalculationSetOptions::getCalculationJobType).orElse(null),
        jobContext
            .map(CalculationJobContext::getCalculationOptions)
            .map(CalculationSetOptions::isUseInCombinationArchive).orElse(false));
  }

  @Watch(value = "getResultTypes()", isImmediate = true)
  public void onResultTypeChange(final List<ScenarioResultType> types) {
    // If the list of available types changes, set an appropriate result type
    if (!types.contains(resultContext.getResultType())) {
      types.stream().findFirst().ifPresent(this::selectResultType);
      autoSwitchingResultType = true;
    }
  }

  @JsMethod
  public List<EmissionResultKey> getEmissionResultKeys() {
    return applicationContext.getConfiguration().getEmissionResultKeys();
  }

  @JsMethod
  public List<ResultDisplaySet> getAvailableResultDisplaySets() {
    return ResultDisplaySetUtil.getAvailableResultDisplaySets(applicationContext, resultContext.getSituationResultsKey().getResultType(),
        getHexagonTypes(), calculationContext.getActiveCalculation().getSituations());
  }

  @Computed("resultDisplaySetToKey")
  public Function<Object, Object> resultDisplaySetToKey() {
    return v -> {
      final ResultDisplaySet displaySet = (ResultDisplaySet) v;
      return displaySet.getSummaryHexagonType() + "_" + displaySet.getOverlappingHexagonType() + "_" + displaySet.getPolicy();
    };
  }

  @JsMethod
  public String getResultDisplaySetName(final ResultDisplaySet displaySet) {
    if (displaySet.getPolicy() != null) {
      return i18n.resultsProcurementPolicy(displaySet.getPolicy());
    } else {
      return i18n.resultsHexagonType(applicationContext.getTheme(), displaySet.getSummaryHexagonType()) +
          i18n.resultsOverlappingHexagonType(displaySet.getOverlappingHexagonType());
    }
  }

  private List<SummaryHexagonType> getHexagonTypes() {
    return resultContext.getAvailableSummaryHexagonTypes();
  }

  @Watch(value = "getAvailableResultDisplaySets()", isImmediate = true)
  public void onAvailableResultDisplaySetsChange(final List<ResultDisplaySet> resultDisplaySets) {
    // If the list of available subsets changes, set an appropriate type
    if (!resultDisplaySets.contains(getSelectedResultDisplaySet()) && !resultDisplaySets.isEmpty()) {
      final ResultDisplaySet newSelection = resultDisplaySets.stream()
          .filter(f -> f.getPolicy() == getSelectedResultDisplaySet().getPolicy()
              && f.getSummaryHexagonType() == getSelectedResultDisplaySet().getSummaryHexagonType())
          .findFirst().orElse(resultDisplaySets.get(0));
      selectResultDisplaySet(newSelection);
    }
  }

  @JsMethod
  public void selectSituation(final SituationHandle option) {
    final SituationResultsKey copy = resultContext.getSituationResultsKey();
    copy.setSituationHandle(option);
    if (autoSwitchingResultType) {
      final CalculationJobType jobType = calculationJob.getJobContext().getCalculationOptions().getCalculationJobType();
      copy.setResultType(ScenarioResultType.getDefault(jobType, option.getType()));
    }
    selectKey(copy);
  }

  @JsMethod
  public void selectResultType(final ScenarioResultType option) {
    autoSwitchingResultType = false;
    final SituationResultsKey copy = resultContext.getSituationResultsKey();
    copy.setResultType(option);
    selectKey(copy);
  }

  @JsMethod
  public void selectEmissionResultKey(final EmissionResultKey option) {
    final SituationResultsKey copy = resultContext.getSituationResultsKey();
    copy.setEmissionResultKey(option);
    selectKey(copy);
  }

  @JsMethod
  public ResultDisplaySet getSelectedResultDisplaySet() {
    return new ResultDisplaySet(resultContext.getHexagonType(), resultContext.getOverlappingHexagonType(), resultContext.getProcurementPolicy());
  }

  @JsMethod
  public void selectResultDisplaySet(final ResultDisplaySet subset) {
    final SituationResultsKey copy = resultContext.getSituationResultsKey();
    copy.setHexagonType(subset.getSummaryHexagonType());
    copy.setOverlappingHexagonType(subset.getOverlappingHexagonType());
    copy.setProcurementPolicy(subset.getPolicy());
    selectKey(copy);
  }

  private void selectKey(final SituationResultsKey key) {
    vue().$emit("select", key);
  }
}
