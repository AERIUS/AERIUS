/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.misc;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import elemental2.core.Global;

import jsinterop.base.Js;

import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.util.NotificationUtil;
import nl.aerius.wui.util.RequestUtil;
import nl.overheid.aerius.shared.RequestMappings;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.SystemInfoContext;
import nl.overheid.aerius.wui.application.event.ApplicationFinishedLoadingEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;

/**
 * Daemon to monitor for system info messages at a regular interval.
 */
@Singleton
public class SystemInfoDaemon extends BasicEventComponent implements Daemon {
  private final SystemInfoDaemonEventBinder EVENT_BINDER = GWT.create(SystemInfoDaemonEventBinder.class);

  interface SystemInfoDaemonEventBinder extends EventBinder<SystemInfoDaemon> {}

  // Check every 15 minutes.
  private static final int DEFAULT_SYSTEM_INFO_REFRESH_MS = 15 * 60 * 1_000;

  private static final Object DEVELOPMENT_VERSION = "DEV";

  private String latestVersion = "";

  private @Inject ApplicationContext applicationContext;
  private @Inject SystemInfoContext systemInfoContext;
  private @Inject EnvironmentConfiguration cfg;

  private final AsyncCallback<String> callback = new AsyncCallback<String>() {
    @Override
    public void onSuccess(final String result) {
      if (!result.contains("<html") && result != null) {
        final SystemInfoResponse response = Js.uncheckedCast(Global.JSON.parse(result));
        systemInfoContext.setSystemInfo(response);
        checkSystemVersions(response);
      }
    }

    @Override
    public void onFailure(final Throwable caught) {
      // For the systeminfo we ignore failures.
    }

  };

  private void checkSystemVersions(final SystemInfoResponse response) {
    final String remoteApplicationVersion = response.getApplicationVersion();
    final String localApplicationVersion = cfg.getApplicationVersion();

    // Bail if this is a development version
    if (DEVELOPMENT_VERSION.equals(localApplicationVersion)) {
      return;
    }

    // If the remote version is not the same as the local version, and has not
    // previously been reviewed
    if (!remoteApplicationVersion.equals(localApplicationVersion)
        && !remoteApplicationVersion.equals(latestVersion)) {
      NotificationUtil.broadcastMessage(eventBus, M.messages().applicationVersionOutdated(localApplicationVersion, remoteApplicationVersion));
    }
    latestVersion = remoteApplicationVersion;
  }

  @EventHandler
  public void onApplicationFinishedLoadingEvent(final ApplicationFinishedLoadingEvent e) {
    final Object setting = applicationContext.getConfiguration().getSettings().getSetting(SharedConstantsEnum.SYSTEM_INFO_POLLING_TIME);
    final int systemInforRefreshTime = setting != null && (int) setting > 0 ? (int) setting : DEFAULT_SYSTEM_INFO_REFRESH_MS;

    Scheduler.get().scheduleFixedDelay(this::checkForSystemInfo, systemInforRefreshTime);
    checkForSystemInfo();
  }

  private boolean checkForSystemInfo() {
    RequestUtil.doGet(RequestMappings.SYSTEM_INFO, callback);
    return true;
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
