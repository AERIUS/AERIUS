/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.road.adms;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.aerius.vuelidate.ValidationOptions;
import nl.aerius.vuelidate.Validations;
import nl.aerius.vuelidate.ValidatorBuilder;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.ADMSLimits;

/**
 * Validators for roadside barrier.
 */
class ADMSBarrierEditorValidators extends ValidationOptions<ADMSBarrierEditorComponent> {

  @JsType(isNative = true, namespace = JsPackage.GLOBAL)
  static class ADMSBarrierEditorValidations extends Validations {
    public @JsProperty Validations barrierLeftWidthV;
    public @JsProperty Validations barrierLeftMaximumHeightV;
    public @JsProperty Validations barrierLeftAverageHeightV;
    public @JsProperty Validations barrierLeftMinimumHeightV;
    public @JsProperty Validations barrierLeftPorosityV;

    public @JsProperty Validations barrierRightWidthV;
    public @JsProperty Validations barrierRightMaximumHeightV;
    public @JsProperty Validations barrierRightAverageHeightV;
    public @JsProperty Validations barrierRightMinimumHeightV;
    public @JsProperty Validations barrierRightPorosityV;

    public @JsProperty Validations barrierPercentageCoveredV;
  }

  @Override
  protected void constructValidators(final Object options, final ValidatorInstaller v) {
    final ADMSBarrierEditorComponent instance = Js.uncheckedCast(options);
    v.install("barrierLeftWidthV", () -> instance.barrierLeftWidthV = null, ValidatorBuilder.create()
        .decimal()
        .minValue(ADMSLimits.ROAD_BARRIER_WIDTH_MIN)
        .required());
    v.install("barrierLeftMaximumHeightV", () -> instance.barrierLeftMaximumHeightV = null, ValidatorBuilder.create()
        .decimal()
        .minValue(ADMSLimits.ROAD_BARRIER_MAXIMUM_HEIGHT_MIN)
        .required());
    v.install("barrierLeftAverageHeightV", () -> instance.barrierLeftAverageHeightV = null, ValidatorBuilder.create()
        .decimal()
        .minValue(ADMSLimits.ROAD_BARRIER_AVERAGE_HEIGHT_MIN)
        .required());
    v.install("barrierLeftMinimumHeightV", () -> instance.barrierLeftMinimumHeightV = null, ValidatorBuilder.create()
        .decimal()
        .minValue(ADMSLimits.ROAD_BARRIER_MINIMUM_HEIGHT_MIN)
        .required());
    v.install("barrierLeftPorosityV", () -> instance.barrierLeftPorosityV = null, ValidatorBuilder.create()
        .decimal()
        .minValue(ADMSLimits.ROAD_BARRIER_POROSITY_MIN)
        .maxValue(ADMSLimits.ROAD_BARRIER_POROSITY_MAX)
        .required());

    v.install("barrierRightWidthV", () -> instance.barrierRightWidthV = null, ValidatorBuilder.create()
        .decimal()
        .minValue(ADMSLimits.ROAD_BARRIER_WIDTH_MIN)
        .required());
    v.install("barrierRightMaximumHeightV", () -> instance.barrierRightMaximumHeightV = null, ValidatorBuilder.create()
        .decimal()
        .minValue(ADMSLimits.ROAD_BARRIER_MAXIMUM_HEIGHT_MIN)
        .required());
    v.install("barrierRightAverageHeightV", () -> instance.barrierRightAverageHeightV = null, ValidatorBuilder.create()
        .decimal()
        .minValue(ADMSLimits.ROAD_BARRIER_AVERAGE_HEIGHT_MIN)
        .required());
    v.install("barrierRightMinimumHeightV", () -> instance.barrierRightMinimumHeightV = null, ValidatorBuilder.create()
        .decimal()
        .minValue(ADMSLimits.ROAD_BARRIER_MINIMUM_HEIGHT_MIN)
        .required());
    v.install("barrierRightPorosityV", () -> instance.barrierRightPorosityV = null, ValidatorBuilder.create()
        .decimal()
        .minValue(ADMSLimits.ROAD_BARRIER_POROSITY_MIN)
        .maxValue(ADMSLimits.ROAD_BARRIER_POROSITY_MAX)
        .required());
  }
}
