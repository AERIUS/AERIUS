/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source;

import elemental2.core.JsArray;

import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.wui.application.components.modify.ModifyListActions;
import nl.overheid.aerius.wui.application.domain.source.AbstractSubSource;
import nl.overheid.aerius.wui.application.domain.source.util.EmissionSourceFeatureUtil;

public abstract class SubSourceEmissionEditorPresenter<C extends SubSourceEditorComponent, S extends AbstractSubSource>
    extends BasicEventComponent implements ModifyListActions {
  protected C view;
  private S selectedSource;

  @Override
  public void newEntity() {
    final S newSubSource = createSubSource();
    final JsArray<S> subSources = getSubSources();
    subSources.push(newSubSource);
    selectedSource = newSubSource;
    view.selectedIndex = subSources.length - 1;
    edit();
  }

  protected abstract S createSubSource();

  protected abstract JsArray<S> getSubSources();

  @Override
  public void copy() {
    final JsArray<S> subSources = getSubSources();
    final S clonedSubSource = EmissionSourceFeatureUtil.clone(selectedSource);
    subSources.push(clonedSubSource);
    resetSelected();
    selectedSource = clonedSubSource;
    view.selectedIndex = subSources.length - 1;
    edit();
  }

  @Override
  public void delete() {
    final JsArray<S> subSources = getSubSources();
    final Integer id = view.selectedIndex;
    if (id.equals(subSources.length)) {
      subSources.pop();
    } else {
      subSources.splice(id, 1);
    }
    resetSelected();
  }

  @Override
  public void edit() {
    view.editSource();
  }

  public void setView(final C view) {
    this.view = view;
  }

  public void resetSelected() {
    view.resetSelected();
  }

  public void selectedSource(final S selectedSource) {
    this.selectedSource = selectedSource;
  }
}
