/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.characteristics.building;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;
import jsinterop.base.Js;

import nl.aerius.wui.vue.transition.HorizontalCollapse;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.wui.application.command.building.BuildingCreateNewCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingEditCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.input.CheckBoxComponent;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.InputWithUnitComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.components.tooltip.TooltipComponent;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.SourceCharacteristics;

@Component(components = {
    CheckBoxComponent.class,
    InputWithUnitComponent.class,
    LabeledInputComponent.class,
    HorizontalCollapse.class,
    VerticalCollapse.class,
    SimplifiedListBoxComponent.class,
    TooltipComponent.class,
    ButtonIcon.class,
    ValidationBehaviour.class,
    InputErrorComponent.class,
})
public class BuildingSelectorComponent extends ErrorWarningValidator {
  @Prop EventBus eventBus;
  @Prop EmissionSourceFeature source;
  /**
   * Use show to display the building selector. If show is false the building invalid check will be ignored.
   * This is needed because the checkbox is not stored anywhere and if checked before hiding the building selector
   * it will cause the invalid check to fail.
   * Also not to be used with v-if because that won't render this component, while the component is still used in validation.
   */
  @Prop boolean show = true;

  @Data SourceCharacteristics characteristics;

  @Data boolean buildingInfluence;

  @Inject @Data ScenarioContext context;

  @Data String defaultBuildingId;

  @Computed("hasBuildings")
  public boolean hasBuildings() {
    return !context.getActiveSituation().getBuildings().isEmpty();
  }

  @Computed("hasBuildingSelected")
  public boolean hasBuildingSelected() {
    final String buildingId = getSelectedBuildingId();
    return buildingId != null && !buildingId.isEmpty();
  }

  @Computed
  public String getSelectedBuildingId() {
    return characteristics.getBuildingGmlId();
  }

  @Computed
  public List<BuildingFeature> getBuildings() {
    return context.getActiveSituation().getBuildings();
  }

  @JsMethod
  public void selectBuilding(final BuildingFeature building) {
    characteristics.setBuildingGmlId(building.getGmlId());
  }

  @JsMethod
  public void createNew() {
    eventBus.fireEvent(new BuildingCreateNewCommand());
  }

  @JsMethod
  public void editSelectedBuilding() {
    final BuildingFeature selectedBuilding = findSelectedBuilding();

    if (selectedBuilding != null) {
      eventBus.fireEvent(new BuildingEditCommand(selectedBuilding));
    }
  }

  public BuildingFeature findSelectedBuilding() {
    return context.getActiveSituation().findBuilding(getSelectedBuildingId());
  }

  @Watch("buildingInfluence")
  public void onBuildingInfluenceChange(final boolean neww) {
    if (characteristics == null) {
      return;
    }

    if (neww) {
      // Make sure not to override if there is already an object in there
      if (!characteristics.isBuildingInfluence()) {
        characteristics.setBuildingGmlId(defaultBuildingId);
      }
    } else {
      // If removing the building influence, save it for later and reset actual
      if (characteristics.isBuildingInfluence()) {
        defaultBuildingId = characteristics.getBuildingGmlId();
      }

      characteristics.setBuildingGmlId(null);
    }
  }

  @Computed("isBuildingInfluenceInvalid")
  public boolean isBuildingInfluenceInvalid() {
    return show && buildingInfluence && characteristics != null && !characteristics.isBuildingInfluence();
  }

  @Computed("isBuildingInfluenceError")
  public boolean isBuildingInfluenceError() {
    return displayProblems && isBuildingInfluenceInvalid();
  }

  @Watch(value = "source", isImmediate = true)
  public void watchSource() {
    characteristics = source == null ? null : Js.uncheckedCast(source.getCharacteristics());

    if (characteristics != null) {
      buildingInfluence = characteristics.isBuildingInfluence();
    }
  }
}
