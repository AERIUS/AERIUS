/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.manure;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.wui.vue.ReactivityUtil;

/**
 * Client side implementation of StandardManureStorage props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class StandardManureStorage extends ManureStorage {

  private String manureStorageCode;

  public static final @JsOverlay StandardManureStorage create() {
    final StandardManureStorage props = Js.uncheckedCast(JsPropertyMap.of());
    init(props);
    return props;
  }

  public static final @JsOverlay void init(final StandardManureStorage props) {
    ManureStorage.initManureStorage(props, ManureStorageType.STANDARD);
    ReactivityUtil.ensureJsPropertyAsNull(props, "manureStorageCode", props::setManureStorageCode);
  }

  @JsOverlay
  public final String getManureStorageCode() {
    return manureStorageCode;
  }

  @JsOverlay
  public final void setManureStorageCode(final String manureStorageCode) {
    this.manureStorageCode = manureStorageCode;
  }

}
