/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import javax.inject.Singleton;

@Singleton
public class CalculationJobValidationContext {
  private boolean calculationJobSettingsWarning = false;
  private boolean developmentPressureSearchWarning = false;
  private boolean meteorologicalSettingsWarning = false;
  private boolean advancedSettingsWarning = false;

  private boolean nameError = false;
  private boolean calculationJobSettingsError = false;
  private boolean developmentPressureSearchError = false;
  private boolean meteorologicalSettingsError = false;
  private boolean advancedSettingsError = false;

  public boolean hasCalculationJobSettingsWarning() {
    return calculationJobSettingsWarning;
  }

  public void setCalculationJobSettingsWarning(final boolean calculationJobSettingsWarning) {
    this.calculationJobSettingsWarning = calculationJobSettingsWarning;
  }

  public boolean hasDevelopmentPressureSearchWarning() {
    return developmentPressureSearchWarning;
  }

  public void setDevelopmentPressureSearchWarning(final boolean developmentPressureSearchWarning) {
    this.developmentPressureSearchWarning = developmentPressureSearchWarning;
  }

  public boolean hasMeteorologicalSettingsWarning() {
    return meteorologicalSettingsWarning;
  }

  public void setMeteorologicalSettingsWarning(final boolean meteorologicalSettingsWarning) {
    this.meteorologicalSettingsWarning = meteorologicalSettingsWarning;
  }

  public boolean hasAdvancedSettingsWarning() {
    return advancedSettingsWarning;
  }

  public void setAdvancedSettingsWarning(final boolean advancedSettingsWarning) {
    this.advancedSettingsWarning = advancedSettingsWarning;
  }

  public boolean hasNameError() {
    return nameError;
  }

  public void setNameError(final boolean nameError) {
    this.nameError = nameError;
  }

  public boolean hasCalculationJobSettingsError() {
    return calculationJobSettingsError;
  }

  public void setCalculationJobSettingsError(final boolean calculationJobSettingsError) {
    this.calculationJobSettingsError = calculationJobSettingsError;
  }

  public boolean hasDevelopmentPressureSearchError() {
    return developmentPressureSearchError;
  }

  public void setDevelopmentPressureSearchError(final boolean developmentPressureSearchError) {
    this.developmentPressureSearchError = developmentPressureSearchError;
  }

  public boolean hasMeteorologicalSettingsError() {
    return meteorologicalSettingsError;
  }

  public void setMeteorologicalSettingsError(final boolean meteorologicalSettingsError) {
    this.meteorologicalSettingsError = meteorologicalSettingsError;
  }

  public boolean hasAdvancedSettingsError() {
    return advancedSettingsError;
  }

  public void setAdvancedSettingsError(final boolean advancedSettingsError) {
    this.advancedSettingsError = advancedSettingsError;
  }

  public boolean hasWarnings() {
    return calculationJobSettingsWarning
        || developmentPressureSearchWarning
        || meteorologicalSettingsWarning
        || advancedSettingsWarning;
  }

  public boolean hasErrors(final boolean expectsDevelopmentPressureSearch) {
    return nameError
        || calculationJobSettingsError
        || (expectsDevelopmentPressureSearch && developmentPressureSearchError)
        || meteorologicalSettingsError
        || advancedSettingsError;
  }

  public void reset() {
    nameError = false;
    calculationJobSettingsError = false;
    developmentPressureSearchError = false;
    meteorologicalSettingsError = false;
    advancedSettingsError = false;
  }
}
