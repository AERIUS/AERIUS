/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.navigation;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import elemental2.dom.DomGlobal;
import elemental2.dom.HTMLElement;

import jsinterop.base.Js;

import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.event.PlaceChangeEvent;
import nl.aerius.wui.place.Place;
import nl.overheid.aerius.wui.application.command.misc.DisplayHotkeysCommand;
import nl.overheid.aerius.wui.application.command.misc.HeaderVisibilityToggleCommand;
import nl.overheid.aerius.wui.application.command.misc.ManualPanelToggleCommand;
import nl.overheid.aerius.wui.application.command.misc.NavigateExternalContentViewCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.NavigationContext;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.place.BuildingPlace;
import nl.overheid.aerius.wui.application.place.CalculatePlace;
import nl.overheid.aerius.wui.application.place.CalculationPointsPlace;
import nl.overheid.aerius.wui.application.place.EmissionSourcePlace;
import nl.overheid.aerius.wui.application.place.ExportPlace;
import nl.overheid.aerius.wui.application.place.HomePlace;
import nl.overheid.aerius.wui.application.place.ResultsPlace;
import nl.overheid.aerius.wui.application.place.ScenarioInputListPlace;
import nl.overheid.aerius.wui.application.place.TimeVaryingProfilePlace;
import nl.overheid.aerius.wui.application.ui.pages.externalcontent.ExternalContentViewFactory;

public class NavigationDaemon extends BasicEventComponent {
  private static final NavigationDaemonEventBinder EVENT_BINDER = GWT.create(NavigationDaemonEventBinder.class);

  interface NavigationDaemonEventBinder extends EventBinder<NavigationDaemon> {}

  @Inject private ApplicationContext appContext;
  @Inject private NavigationContext context;

  private final Map<Class<?>, Supplier<String>> titleBehaviour = new HashMap<>();

  public NavigationDaemon() {
    titleBehaviour.put(HomePlace.class,
        () -> M.messages().placeTitleThemeLaunch(universalTitle()));
    titleBehaviour.put(EmissionSourcePlace.class,
        () -> M.messages().placeTitleEmissionSource(universalTitle()));
    titleBehaviour.put(ScenarioInputListPlace.class,
        () -> M.messages().placeTitleScenarioInputs(universalTitle()));
    titleBehaviour.put(CalculationPointsPlace.class,
        () -> M.messages().placeTitleCalculationPoints(universalTitle()));
    titleBehaviour.put(CalculatePlace.class,
        () -> M.messages().placeTitleCalculate(universalTitle()));
    titleBehaviour.put(BuildingPlace.class,
        () -> M.messages().placeTitleBuilding(universalTitle()));
    titleBehaviour.put(TimeVaryingProfilePlace.class,
        () -> M.messages().placeTitleTimeVaryingProfile(universalTitle()));
    titleBehaviour.put(ResultsPlace.class,
        () -> M.messages().placeTitleResults(universalTitle()));
    titleBehaviour.put(ExportPlace.class,
        () -> M.messages().placeTitleExport(universalTitle()));
  }

  private String universalTitle() {
    return M.messages().placeTitleUniversal(appContext.getTheme(), appContext.getProductProfile());
  }

  @EventHandler
  public void onPlaceChangeEvent(final PlaceChangeEvent e) {
    updatePageTitle(e.getValue());
    context.deactivatePopout();
    eventBus.fireEvent(new DisplayHotkeysCommand(false));
  }

  @EventHandler
  public void onManualPanelToggleCommand(final ManualPanelToggleCommand c) {
    context.toggleManual();
  }

  @EventHandler
  public void onHeaderVisibilityToggleCommand(final HeaderVisibilityToggleCommand c) {
    DomGlobal.document.querySelector(".govuk-header").classList.toggle("visually-hidden");
    DomGlobal.document.querySelector(".govuk-phase-banner").classList.toggle("visually-hidden");

    final HTMLElement documentElement = Js.uncheckedCast(DomGlobal.document.documentElement);
    if (documentElement.style.getPropertyValue("--aux-panel-height").equals("0px")) {
      documentElement.style.removeProperty("--aux-panel-height");
      documentElement.style.removeProperty("--external-header");
    } else {
      documentElement.style.setProperty("--aux-panel-height", "0px");
      documentElement.style.setProperty("--external-header", "0px");
    }
  }

  @EventHandler
  public void onNavigateExternalContentViewCommand(final NavigateExternalContentViewCommand c) {
    context.activatePopout(ExternalContentViewFactory.get().getComponentTagName(), c.getValue());
  }

  private void updatePageTitle(final Place place) {
    final String title = Optional.ofNullable(titleBehaviour.get(place.getClass()))
        .orElse(
            () -> M.messages().placeTitleThemeLaunch(universalTitle()))
        .get();
    DomGlobal.document.title = title;
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
