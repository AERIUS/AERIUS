/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context.calculation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.inject.Singleton;

import jsinterop.annotations.JsProperty;

@Singleton
public class CalculationContext {
  private String activeInternalCalculationId = null;

  /**
   * List of calculations using internal calculation IDs (i.e. these calculations may not be initialized or known to the server yet)
   */
  private final @JsProperty Map<String, CalculationExecutionContext> calculationsMap = new LinkedHashMap<>();
  private final @JsProperty List<CalculationExecutionContext> calculationsLst = new ArrayList<>();

  /**
   * List of initialized calculations (i.e. these calculations will have a jobKey and be fully initialized)
   */
  private final @JsProperty Map<String, CalculationExecutionContext> initializedCalculation = new HashMap<>();

  public CalculationExecutionContext getActiveCalculation() {
    return getCalculation(activeInternalCalculationId);
  }

  public boolean hasActiveCalculation() {
    return getCalculation(activeInternalCalculationId) != null;
  }

  public CalculationExecutionContext getInitializedCalculation(final String jobKey) {
    return Optional.ofNullable(initializedCalculation.get(jobKey))
        .orElse(null);
  }

  public List<CalculationExecutionContext> getCalculations() {
    return calculationsLst;
  }

  /**
   * Return the active job id, NOTE, this value is not guaranteed to be set.
   */
  public String getActiveJobId() {
    return Optional.ofNullable(getCalculation(activeInternalCalculationId))
        .map(v -> v.getJobKey())
        .orElse(null);
  }

  public boolean hasCalculations() {
    return !calculationsMap.isEmpty();
  }

  public CalculationExecutionContext getCalculation(final String inputHash) {
    return Optional.ofNullable(calculationsMap.get(inputHash))
        .orElse(null);
  }

  public void addCalculation(final CalculationExecutionContext calculation) {
    calculationsLst.add(calculation);
    calculationsMap.put(calculation.getInternalCalculationId(), calculation);
    // Also intialize if it has a calculation code
    if (calculation.getJobKey() != null) {
      initializeCalculation(calculation);
    }
  }

  public void initializeCalculation(final CalculationExecutionContext calculation) {
    initializedCalculation.put(calculation.getJobKey(), calculation);
  }

  public void setActiveCalculation(final CalculationExecutionContext calculation) {
    activeInternalCalculationId = calculation == null
        ? null
        : calculation.getInternalCalculationId();
  }

  public void removeAllJobs() {
    calculationsLst.clear();
    calculationsMap.clear();
    initializedCalculation.clear();
    reset();
  }

  /**
   * Reset the active calculation state
   */
  public void reset() {
    activeInternalCalculationId = null;
  }

  /**
   * Reset the active calculation state if the given calculation is active
   */
  public void reset(final CalculationExecutionContext calculation) {
    if (isActiveCalculation(calculation)) {
      activeInternalCalculationId = null;
    }
  }

  public boolean isActiveCalculation(final CalculationExecutionContext calculation) {
    return calculation.equals(getActiveCalculation());
  }

  public void removeCalculation(final CalculationExecutionContext calculation) {
    calculationsLst.remove(calculation);
    final String internalCalculationId = calculation.getInternalCalculationId();

    calculationsMap.remove(internalCalculationId);
    if (internalCalculationId != null && internalCalculationId.equals(activeInternalCalculationId)) {
      activeInternalCalculationId = null;
    }
    if (calculation.getJobKey() != null) {
      initializedCalculation.remove(calculation.getJobKey());
    }
  }
}
