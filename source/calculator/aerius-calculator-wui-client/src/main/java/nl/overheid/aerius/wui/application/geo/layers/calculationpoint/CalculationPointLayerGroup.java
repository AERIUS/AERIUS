/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.calculationpoint;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.google.inject.assistedinject.Assisted;

import ol.Collection;
import ol.layer.Base;
import ol.layer.Group;
import ol.layer.LayerGroupOptions;

import nl.aerius.geo.domain.IsLayer;
import nl.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.wui.application.geo.layers.IsMarkerLayer;
import nl.overheid.aerius.wui.application.geo.layers.LayerFactory;
import nl.overheid.aerius.wui.application.i18n.M;

public class CalculationPointLayerGroup implements IsMarkerLayer<Group> {

  private final LayerInfo info;
  private final Group layer;

  private final List<IsLayer<?>> markerLayers = new ArrayList<>();

  @Inject
  public CalculationPointLayerGroup(final LayerFactory layerFactory, @Assisted final int zIndex) {
    this.info = new LayerInfo();
    this.info.setName(this.getClass().getCanonicalName());
    this.info.setTitle(M.messages().calculationPointsLayerTitle());

    int layersZIndex = 0;

    final Collection<Base> layers = new Collection<>();
    markerLayers.add(layerFactory.createSelectedCalculationPointMarkerLayer(layersZIndex++));
    markerLayers.add(layerFactory.createCalculationPointMarkerLayer(layersZIndex++));
    markerLayers.forEach(ml -> layers.push((Base) ml.asLayer()));

    layers.push(layerFactory.createCalculationPointGeometryLayer(layersZIndex++).asLayer());

    final LayerGroupOptions groupOptions = new LayerGroupOptions();
    groupOptions.setLayers(layers);

    layer = new Group(groupOptions);
    layer.setZIndex(zIndex);
  }

  @Override
  public Group asLayer() {
    return layer;
  }

  @Override
  public LayerInfo getInfo() {
    return info;
  }

  @Override
  public List<IsLayer<?>> getMarkerLayers() {
    return markerLayers;
  }
}
