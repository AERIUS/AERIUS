/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.results;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.directives.VectorDirective;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.result.ResultView;
import nl.overheid.aerius.shared.domain.summary.OverlappingHexagonType;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.command.result.ChangeSituationResultKeyCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.tooltip.TooltipComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.context.result.ResultSummaryContext;
import nl.overheid.aerius.wui.application.context.result.ResultViewContext;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationInfo;
import nl.overheid.aerius.wui.application.domain.calculation.JobProgress;
import nl.overheid.aerius.wui.application.domain.info.AssessmentArea;
import nl.overheid.aerius.wui.application.domain.result.CustomCalculationPointResult;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsAreaSummary;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsSummary;
import nl.overheid.aerius.wui.application.event.ResultsViewSelectionCommand;
import nl.overheid.aerius.wui.application.ui.pages.results.areas.OmittedAreasView;
import nl.overheid.aerius.wui.application.ui.pages.results.critical.ResultPercentageCriticalLoadView;
import nl.overheid.aerius.wui.application.ui.pages.results.critical.ResultPercentageCriticalLoadViewFactory;
import nl.overheid.aerius.wui.application.ui.pages.results.custompoints.CustomPointsView;
import nl.overheid.aerius.wui.application.ui.pages.results.custompoints.CustomPointsViewFactory;
import nl.overheid.aerius.wui.application.ui.pages.results.graph.ResultGraphView;
import nl.overheid.aerius.wui.application.ui.pages.results.graph.ResultGraphViewFactory;
import nl.overheid.aerius.wui.application.ui.pages.results.habitat.ResultHabitatView;
import nl.overheid.aerius.wui.application.ui.pages.results.habitat.ResultHabitatViewFactory;
import nl.overheid.aerius.wui.application.ui.pages.results.habitat.ResultsHabitatListView;
import nl.overheid.aerius.wui.application.ui.pages.results.key.SituationResultKeyCompositor;
import nl.overheid.aerius.wui.application.ui.pages.results.procurementpolicy.ProcurementPolicyThresholdView;
import nl.overheid.aerius.wui.application.ui.pages.results.procurementpolicy.ProcurementPolicyThresholdViewFactory;
import nl.overheid.aerius.wui.application.ui.pages.results.progress.CalculationProgressView;
import nl.overheid.aerius.wui.application.ui.pages.results.receptors.EdgeEffectReceptorsView;
import nl.overheid.aerius.wui.application.ui.pages.results.receptors.EdgeEffectReceptorsViewFactory;
import nl.overheid.aerius.wui.application.ui.pages.results.stats.ResultsStatisticsView;
import nl.overheid.aerius.wui.application.ui.pages.results.tab.TabViewComponent;
import nl.overheid.aerius.wui.application.ui.pages.results.table.ResultTableView;
import nl.overheid.aerius.wui.application.ui.pages.results.table.ResultTableViewFactory;
import nl.overheid.aerius.wui.application.util.ResultDisplaySetUtil;
import nl.overheid.aerius.wui.vue.BasicVueEventComponent;

@Component(components = {
    ButtonIcon.class,
    CalculationProgressView.class,
    CollapsiblePanel.class,
    ResultsHabitatListView.class,
    ResultsStatisticsView.class,
    SimplifiedListBoxComponent.class,
    VerticalCollapse.class,
    TooltipComponent.class,
    SituationResultKeyCompositor.class,

    TabViewComponent.class,
    ResultGraphView.class,
    ResultTableView.class,
    ResultHabitatView.class,
    CustomPointsView.class,
    OmittedAreasView.class,
    EdgeEffectReceptorsView.class,
    ResultPercentageCriticalLoadView.class,
    ProcurementPolicyThresholdView.class,
}, directives = {
    VectorDirective.class,
})
public class ScenarioResultsView extends BasicVueEventComponent implements HasMounted {
  private static final List<ResultStatisticType> PROJECT_RESULTS = Arrays.asList(ResultStatisticType.MAX_TOTAL, ResultStatisticType.MAX_DECREASE,
      ResultStatisticType.MAX_INCREASE);
  private static final List<ResultStatisticType> SITUATION_RESULTS = Arrays.asList(ResultStatisticType.MAX_CONTRIBUTION,
      ResultStatisticType.MAX_TOTAL);

  @Prop EventBus eventBus;

  @Inject @Data ApplicationContext applicationContext;
  @Inject @Data ScenarioContext scenarioContext;
  @Inject @Data CalculationContext calculationContext;
  @Inject @Data ResultSummaryContext resultSummaryContext;

  @Inject @Data ResultViewContext viewContext;

  @JsMethod
  public void selectSituationResultKey(final SituationResultsKey key) {
    eventBus.fireEvent(new ChangeSituationResultKeyCommand(key));
  }

  @Computed
  public List<ResultView> getResultsViewTabs() {
    if (getResultContext().getHexagonType() == SummaryHexagonType.CUSTOM_CALCULATION_POINTS) {
      return Collections.emptyList();
    } else if (getResultContext().getOverlappingHexagonType() == OverlappingHexagonType.NON_OVERLAPPING_HEXAGONS_ONLY) {
      return Collections.emptyList();
    } else if (getResultContext().getResultType() == ScenarioResultType.DEPOSITION_SUM) {
      return Collections.singletonList(ResultView.PROCUREMENT_POLICY_THRESHOLD);
    } else if (getResultContext().getHexagonType() == SummaryHexagonType.EXTRA_ASSESSMENT_HEXAGONS) {
      return applicationContext.getConfiguration().getResultViews().stream()
          .map(x -> x == ResultView.HABITAT_TYPES ? ResultView.EXTRA_ASSESSMENT_HABITAT_TYPES : x)
          .collect(Collectors.toList());
    } else {
      return applicationContext.getConfiguration().getResultViews();
    }
  }

  @Watch(value = "getResultsViewTabs()", isImmediate = true)
  public void onResultsViewTabsChange(final List<ResultView> views) {
    // If the list of available types changes, set an appropriate result view
    final ResultView currentTab = viewContext.getResultViewTab();

    // Because HABITAT_TYPES and EXTRA_ASSESSMENT_HABITAT_TYPES are the same tab, but on different pages the select
    // should remain on a habitat types panel when switching.
    final boolean selectHabitat = selectHabitat(views, ResultView.HABITAT_TYPES, ResultView.EXTRA_ASSESSMENT_HABITAT_TYPES)
        || selectHabitat(views, ResultView.EXTRA_ASSESSMENT_HABITAT_TYPES, ResultView.HABITAT_TYPES);
    if (!selectHabitat && !views.contains(currentTab)) {
      views.stream().findFirst().ifPresent(this::selectTab);
    }
  }

  /**
   * If the alternative is not in the views list, but is the current results view tab, switch to the switchTo tab.
   *
   * @param views list if views
   * @param switchTo switch to this tab if conditions met
   * @param alternative alternative to check if not in the list but if it's the current tab
   * @return true if tab changed
   */
  private boolean selectHabitat(final List<ResultView> views, final ResultView switchTo, final ResultView alternative) {
    if (views.contains(switchTo) && !views.contains(alternative) && viewContext.getResultViewTab() == alternative) {
      selectTab(switchTo);
      return true;
    }
    return false;
  }

  @Override
  public void mounted() {
    Optional.ofNullable(calculationContext.getActiveCalculation())
        .map(CalculationExecutionContext::getResultContext)
        .map(ResultSummaryContext::getSituationResultsKey)
        .ifPresent(key -> eventBus.fireEvent(new ChangeSituationResultKeyCommand(key)));
  }

  @JsMethod
  public String getResultViewComponent(final ResultView tab) {
    if (getResultContext().getHexagonType() == SummaryHexagonType.CUSTOM_CALCULATION_POINTS) {
      return CustomPointsViewFactory.get().getComponentTagName();
    }
    if (getResultContext().getOverlappingHexagonType() == OverlappingHexagonType.NON_OVERLAPPING_HEXAGONS_ONLY) {
      return EdgeEffectReceptorsViewFactory.get().getComponentTagName();
    }

    if (tab == null) {
      return "";
    }

    switch (tab) {
    case DEPOSITION_DISTRIBUTION:
      return ResultGraphViewFactory.get().getComponentTagName();
    case MARKERS:
      return ResultTableViewFactory.get().getComponentTagName();
    case HABITAT_TYPES:
      return ResultHabitatViewFactory.get().getComponentTagName();
    case EXTRA_ASSESSMENT_HABITAT_TYPES:
      return ResultHabitatViewFactory.get().getComponentTagName();
    case PERCENTAGE_CRITICAL_LEVELS:
      return ResultPercentageCriticalLoadViewFactory.get().getComponentTagName();
    case PROCUREMENT_POLICY_THRESHOLD:
      return ProcurementPolicyThresholdViewFactory.get().getComponentTagName();
    default:
      return null;
    }
  }

  @Computed
  public boolean isShowSituationResults() {
    final JobState state = getJobProgress().getState();

    return state.isSuccessState() || state == JobState.CALCULATING || state == JobState.POST_PROCESSING;
  }

  @Computed
  public List<ResultStatisticType> getHabitatStatisticTypes() {
    return isProjectCalculation() ? PROJECT_RESULTS : SITUATION_RESULTS;
  }

  @Computed
  public AssessmentArea[] getOmittedAreas() {
    return getResultsSummary() != null ? getResultsSummary().getOmittedAreas() : null;
  }

  @JsMethod
  public boolean hasOmittedAreas() {
    if (getResultContext().getOverlappingHexagonType() == OverlappingHexagonType.NON_OVERLAPPING_HEXAGONS_ONLY) {
      return false;
    }
    return isProjectCalculation() && getOmittedAreas() != null && getOmittedAreas().length > 0;
  }

  @JsMethod
  public void selectTab(final ResultView tab) {
    eventBus.fireEvent(new ResultsViewSelectionCommand(tab));
  }

  @JsMethod
  public boolean hasAnyResultValue() {
    return ResultDisplaySetUtil.couldHaveResults(getResultContext().getSituationResultsKey())
        && Optional.ofNullable(getResultsSummary())
            .map(SituationResultsSummary::getStatistics)
            .isPresent();
  }

  @JsMethod
  public boolean hasResults() {
    if (!ResultDisplaySetUtil.couldHaveResults(getResultContext().getSituationResultsKey())) {
      return false;
    }
    final Optional<SituationResultsAreaSummary[]> areaResults = Optional.ofNullable(getResultsSummary())
        .map(SituationResultsSummary::getAreaStatistics);

    final Optional<CustomCalculationPointResult[]> pointResults = Optional.ofNullable(getResultsSummary())
        .map(SituationResultsSummary::getCustomPointResults);

    return areaResults.isPresent() && areaResults.get().length > 0 ||
        pointResults.isPresent() && pointResults.get().length > 0;
  }

  @Computed
  public CalculationExecutionContext getCalculationJob() {
    return calculationContext.getActiveCalculation();
  }

  @Computed
  public CalculationInfo getCalculationInfo() {
    return Optional.ofNullable(getCalculationJob())
        .map(CalculationExecutionContext::getCalculationInfo)
        .orElseThrow(() -> new RuntimeException("Could not retrieve calculation info."));
  }

  @Computed
  public JobProgress getJobProgress() {
    return Optional.ofNullable(getCalculationInfo())
        .map(CalculationInfo::getJobProgress)
        .orElseGet(JobProgress::new);
  }

  @Computed
  public ResultSummaryContext getResultContext() {
    return Optional.ofNullable(getCalculationJob())
        .map(CalculationExecutionContext::getResultContext)
        .orElseThrow(() -> new RuntimeException("Could not retrieve result context."));
  }

  @Computed
  public SituationResultsSummary getResultsSummary() {
    return Optional.ofNullable(getCalculationJob())
        .map(CalculationExecutionContext::getResultContext)
        .map(ResultSummaryContext::getActiveResultSummary)
        .orElse(null);
  }

  private boolean isProjectCalculation() {
    return getResultContext().getResultType() == ScenarioResultType.PROJECT_CALCULATION;
  }

  @JsMethod
  protected String noResultsText() {
    return ResultDisplaySetUtil.noCalculationResultsText(getResultContext());
  }
}
