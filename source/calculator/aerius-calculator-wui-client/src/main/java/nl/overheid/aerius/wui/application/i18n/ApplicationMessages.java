/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.i18n;

import com.google.gwt.i18n.client.Messages;

import nl.overheid.aerius.shared.domain.ProductProfile;
import nl.overheid.aerius.shared.domain.Theme;

/**
 * Interface to all texts in the application. All texts that are visible to the
 * user and need to be available in all supported languages need to be present
 * in this interface.
 */
public interface ApplicationMessages
    extends Messages, BaseMessages, MenuMessages, ViewMessages, BuildingMessages, TimeVaryingProfileMessages, MooringMessages, EmissionSourceMessages,
    SourceCharacteristicsMessages, SRM2CharacteristicsMessages, AdmsCharacteristicsMessages, ResultsMessages, TextMessages, MiscellaneousMessages,
    NavigationMessages, WarningMessages, ErrorMessages, LayerPanelMessages, InfoPanelMessages, CalculationSourceMessages, CalculateMessages,
    UnitMessages, PdfMessages, AriaMessages, ExportMessages, PreferencesMessages, CalculationPointMessages, HotKeyMessages, ApiKeyLoginMessages,
    NextStepMessages, ManualMessages, LegendMessages, NcaPrintMessages, DecisionFrameworkMessages {

  String applicationVersionOutdated(String local, String remote);

  String dateFormat();

  String booleanYes();
  String booleanNo();

  String nullDateDefault();

  String placeTitleUniversal(@Select Theme theme, @Select ProductProfile productProfile);

  String placeTitleThemeLaunch(@Optional String brand);
  String placeTitleCalculationPoints(@Optional String brand);
  String placeTitleEmissionSource(@Optional String brand);
  String placeTitleScenarioInputs(@Optional String brand);
  String placeTitleCalculate(@Optional String brand);
  String placeTitleResults(@Optional String brand);
  String placeTitleBuilding(@Optional String brand);
  String placeTitleTimeVaryingProfile(@Optional String brand);
  String placeTitleExport(@Optional String brand);

}
