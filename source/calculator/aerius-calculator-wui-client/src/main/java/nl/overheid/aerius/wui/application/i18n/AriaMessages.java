/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.i18n;

public interface AriaMessages {
  String ariaMainNavigation();
  String ariaMainLogo();
  String ariaSkipLink();
  String ariaSkipLinkLabel();
  String ariaCalculateScenarioSummaryTable();
  String ariaDescription();

  String ariaLayerLabel();
  String ariaInfoLabel();
  String ariaLoginPanelLabel();
  String ariaSearchLabel();
  String ariaMeasureLabel();
  String ariaFeatureHeading();

  String ariaErrorIcon(String title);
  String ariaWarningIcon(String title);

  String ariaLabelClose();
  String ariaLabelDontShowAgain();
  String ariaLabelSpinner();

  String ariaSelectSituation();
  String ariaSituationNameLabel();

  String ariaFarmAHAdditionalTechniqueLabel();
  String ariaFarmLodgingAdditionalMeasureLabel();

  String ariaNotificationIdx(int num);

  String ariaSituationSummaryTable();

  String ariaNumberOfAnimals();
  String ariaNumberOfDays();
  String ariaNumberOfApplications();
  String ariaMetersCubed();

  String ariaArchiveProjectsTable();
  String ariaCalculatedScenariosTable();

  String ariaTonnes();
}
