/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.criticallevel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.geo.domain.legend.ColorLabelsLegend;
import nl.aerius.geo.domain.legend.Legend;
import nl.aerius.geo.domain.legend.LegendType;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.legend.ColorRange;
import nl.overheid.aerius.shared.domain.legend.ColorRangeType;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.event.LegendFilterEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;

public class TotalPercentageCriticalLevelResultLayer extends BasePCLResultLayer {

  private static final TotalPercentageCriticalLevelEventBinder EVENT_BINDER = GWT.create(TotalPercentageCriticalLevelEventBinder.class);

  interface TotalPercentageCriticalLevelEventBinder extends EventBinder<TotalPercentageCriticalLevelResultLayer> {}

  private static final String TOTAL_PERCENTAGE_CL_NAME = "total_percentage_cl";

  private final List<ColorRange> hideRanges = new ArrayList<>();

  private String[] labels;
  private List<String> labelsList;
  private List<ColorRange> colorRanges;

  @Inject
  public TotalPercentageCriticalLevelResultLayer(@Assisted final ApplicationConfiguration themeConfiguration, final EventBus eventBus,
      final EnvironmentConfiguration configuration, final CalculationContext calculationContext) {
    super("_total_percentage_cl_", M.messages().layerTotalPercentageCL(M.messages().resultsTotalName(themeConfiguration.getTheme())),
        themeConfiguration, eventBus, configuration, calculationContext);
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Override
  protected Legend createLegend() {
    // createLegend is called during constructor of super, therefore initialize here.
    labels = new String[] {
        M.messages().layerTotalPercentageCLItemLabelNearingExceedance(M.messages().resultsTotalName(themeConfiguration.getTheme())),
        M.messages().layerTotalPercentageCLItemLabelExceedance(M.messages().resultsTotalName(themeConfiguration.getTheme()))
    };
    labelsList = Arrays.asList(labels);
    colorRanges = themeConfiguration.getColorRange(ColorRangeType.MAX_TOTAL_AS_PERCENTAGE_CRITICAL_LOAD);
    final String[] colors = colorRanges.stream().map(ColorRange::getColor).toArray(String[]::new);
    final ColorLabelsLegend legend = new ColorLabelsLegend(
        labels,
        colors,
        emissionResultKey.getEmissionResultType() == EmissionResultType.DEPOSITION ? LegendType.CIRCLE : LegendType.CIRCLE_OUTLINE,
        M.messages().layerTotalPercentageCLLegendNote(M.messages().resultsTotalName(themeConfiguration.getTheme())));

    legend.setFilterables(labels);
    return legend;
  }

  @Override
  protected String buildCqlFilter() {
    String cqlFilter = super.buildCqlFilter();

    if (!hideRanges.isEmpty() && !cqlFilter.isEmpty()) {
      // Super uses OR, so use brackets for that part to filter accordingly.
      cqlFilter = "(" + cqlFilter + ") AND ";
    }
    cqlFilter += hideRanges.stream()
        .map(this::getCQLPart)
        .map(bound -> "NOT (" + bound + ")")
        .collect(Collectors.joining(" AND "));

    return cqlFilter;
  }

  @EventHandler
  public void onLegendFilterEvent(final LegendFilterEvent e) {
    if (!labelsList.contains(e.getValue())) {
      return;
    }

    final ColorRange filtered = colorRanges.get(labelsList.indexOf(e.getValue()));
    if (e.isVisible()) {
      hideRanges.remove(filtered);
    } else {
      hideRanges.add(filtered);
    }
    updateLayer();
  }

  private String getCQLPart(final ColorRange colorRange) {
    final double lowerValue = colorRange.getLowerValue();
    final double upperValue = getUpperValue(colorRange);
    if (Double.isInfinite(lowerValue)) {
      return TOTAL_PERCENTAGE_CL_NAME + " < " + upperValue;
    } else if (Double.isInfinite(upperValue)) {
      return TOTAL_PERCENTAGE_CL_NAME + " >= " + lowerValue;
    } else {
      return TOTAL_PERCENTAGE_CL_NAME + " >= " + lowerValue + " AND " + TOTAL_PERCENTAGE_CL_NAME + " < " + upperValue;
    }
  }

  private double getUpperValue(final ColorRange colorRange) {
    final int index = colorRanges.indexOf(colorRange);
    return index + 1 == colorRanges.size() ? Double.POSITIVE_INFINITY : colorRanges.get(index).getLowerValue();
  }

}
