/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.info;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasDestroyed;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import elemental2.dom.HTMLElement;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;
import nl.overheid.aerius.wui.application.command.geo.InfoPopupHiddenCommand;
import nl.overheid.aerius.wui.application.command.geo.InfoPopupResetCommand;
import nl.overheid.aerius.wui.application.command.geo.InfoPopupVisibleCommand;
import nl.overheid.aerius.wui.application.components.map.InfoPanelDockChangeCommand;
import nl.overheid.aerius.wui.application.components.map.InfoPanelDockRemoveCommand;
import nl.overheid.aerius.wui.application.components.modal.PanelizedModalComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.InfoMarkerContext;
import nl.overheid.aerius.wui.application.context.InfoPanelContext;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.util.ScreenUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    InfoPanel.class,
    PanelizedModalComponent.class
})
public class InfoPanelModal extends BasicVueComponent implements IsVueComponent, HasCreated, HasDestroyed {
  private static final InfoPanelModalEventBinder EVENT_BINDER = GWT.create(InfoPanelModalEventBinder.class);

  interface InfoPanelModalEventBinder extends EventBinder<InfoPanelModal> {}

  @Prop EventBus eventBus;

  @Inject @Data ApplicationContext applicationContext;
  @Inject @Data InfoPanelContext infoPanelContext;
  @Inject @Data InfoMarkerContext infoMarkerContext;

  @Ref PanelizedModalComponent modal;

  private HandlerRegistration handlers;
  private ReceptorUtil recUtil;

  @Data HTMLElement dock;

  @Override
  public void created() {
    handlers = EVENT_BINDER.bindEventHandlers(this, eventBus);
    recUtil = new ReceptorUtil(applicationContext.getConfiguration().getReceptorGridSettings());
  }

  @EventHandler
  public void onInfoPanelDockRemoveCommand(final InfoPanelDockRemoveCommand c) {
    if (dock == c.getValue()) {
      dock = null;
    }
  }

  @EventHandler
  public void onInfoPanelDockChangeCommand(final InfoPanelDockChangeCommand c) {
    dock = c.getValue();
    dock.parentNode.insertBefore(modal.vue().$el(), dock.nextSibling);
    SchedulerUtil.delay(() -> {
      ScreenUtil.attachElementLeft(dock, modal.vue().$el(), ScreenUtil.OFFSET_LEFT);
    });
  }

  @JsMethod
  public void close() {
    eventBus.fireEvent(new InfoPopupHiddenCommand());
  }

  @JsMethod
  public String getInactiveText() {
    if (infoMarkerContext.hasSelection()) {
      final Point point = recUtil.getPointFromReceptorId(infoMarkerContext.getReceptor());
      return "x:" + Math.round(point.getX()) + " y:" + Math.round(point.getY()) + ", " + M.messages().infoPanelReceptorIdLabel() + ": "
          + infoMarkerContext.getReceptor();
    } else {
      return i18n.infoPanelNoSelectionInactiveText();
    }
  }

  @EventHandler
  public void onInfoPopupResetCommand(final InfoPopupResetCommand c) {
    modal.reset();
    eventBus.fireEvent(new InfoPopupVisibleCommand());
  }

  @Override
  public void destroyed() {
    if (handlers != null) {
      handlers.removeHandler();
    }
  }
}
