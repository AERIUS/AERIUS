/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.characteristics;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.aerius.vuelidate.ValidationOptions;
import nl.aerius.vuelidate.Validations;
import nl.aerius.vuelidate.ValidatorBuilder;
import nl.overheid.aerius.shared.domain.ops.OPSLimits;

/**
 * Validators for {@link OPSCharacteristicsEditorComponent}.
 */
class OPSCharacteristicsEditorValidators extends ValidationOptions<OPSCharacteristicsEditorComponent> {

  @JsType(isNative = true, namespace = JsPackage.GLOBAL)
  static class OPSCharacteristicsEditorValidations extends Validations {
    public @JsProperty Validations spreadV;
  }

  @Override
  protected void constructValidators(final Object options, final ValidatorInstaller v) {
    final OPSCharacteristicsEditorComponent instance = Js.uncheckedCast(options);

    v.install("spreadV", () -> instance.spreadV = null
        , ValidatorBuilder.create()
        .required()
        .decimal()
        .minValue(OPSLimits.SOURCE_SPREAD_MINIMUM)
        .maxValue(OPSLimits.SOURCE_SPREAD_MAXIMUM)
        );
  }
}
