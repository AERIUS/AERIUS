/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.print.common.scenario;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.gwt.i18n.client.LocaleInfo;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.v2.characteristics.CustomTimeVaryingProfileType;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.source.TimeVaryingProfile;
import nl.overheid.aerius.wui.application.print.common.PrintSection;
import nl.overheid.aerius.wui.application.ui.nca.pages.emisionsourceList.timevarying.MonthlyProfileStatsView;
import nl.overheid.aerius.wui.application.ui.nca.pages.emisionsourceList.timevarying.ThreeDayProfileStatsView;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

/**
 * Basic component outputting a table displaying summarized time varying profiles
 */
@Component(components = {
    PrintSection.class,
    ThreeDayProfileStatsView.class,
    MonthlyProfileStatsView.class,
})
public class PrintScenarioTimeVaryingProfilesComponent extends BasicVueComponent {
  @Prop SituationContext situation;

  @Computed
  public String getLocale() {
    return LocaleInfo.getCurrentLocale().getLocaleName();
  }

  @JsMethod
  public Double[] getTimeVaryingProfileData(final TimeVaryingProfile profile) {
    return profile.getValues().toArray(new Double[profile.getValues().size()]);
  }

  @JsMethod
  public String getTimeVaryingProfileType(final TimeVaryingProfile profile) {
    return profile.getType().name();
  }

  @JsMethod
  boolean isThreeDayProfile(final TimeVaryingProfile profile) {
    return profile.getType() == CustomTimeVaryingProfileType.THREE_DAY;
  }

  @JsMethod
  boolean isMonthlyProfile(final TimeVaryingProfile profile) {
    return profile.getType() == CustomTimeVaryingProfileType.MONTHLY;
  }
}
