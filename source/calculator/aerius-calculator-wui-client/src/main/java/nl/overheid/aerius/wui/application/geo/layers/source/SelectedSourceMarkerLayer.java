/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source;

import java.util.Collections;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.wui.application.command.source.TemporaryEmissionSourceAddCommand;
import nl.overheid.aerius.wui.application.command.source.TemporaryEmissionSourceClearCommand;
import nl.overheid.aerius.wui.application.context.MapConfigurationContext;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.event.situation.SituationSwitchEvent;
import nl.overheid.aerius.wui.application.geo.layers.LabelStyle;
import nl.overheid.aerius.wui.application.geo.layers.base.BaseMarkerLayer;
import nl.overheid.aerius.wui.application.geo.layers.base.LabelStyleCreator;
import nl.overheid.aerius.wui.application.geo.layers.base.VectorFeaturesWrapper;

/**
 * Layer to show the marker of the {@link EmissionSourceFeature} on the map that is being edited.
 */
public class SelectedSourceMarkerLayer extends BaseMarkerLayer<EmissionSourceFeature> {

  interface SourceMarkerLayerEventBinder extends EventBinder<SelectedSourceMarkerLayer> {}

  private static final SourceMarkerLayerEventBinder EVENT_BINDER = GWT.create(SourceMarkerLayerEventBinder.class);

  private final VectorFeaturesWrapper vectorObject = new VectorFeaturesWrapper();

  private final MapConfigurationContext context;

  @Inject
  public SelectedSourceMarkerLayer(@Assisted final LayerInfo info, final EventBus eventBus, @Assisted final int zIndex,
      final MapConfigurationContext mapConfigurationContext) {
    super(info, zIndex, new LabelStyleCreator<EmissionSourceFeature>(mapConfigurationContext::getIconScale, true, LabelStyle.EMISSION_SOURCE));
    setFeatures(vectorObject, Collections.emptyList());
    this.context = mapConfigurationContext;
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  public void onTemporaryEmissionSourceAddCommand(final TemporaryEmissionSourceAddCommand c) {
    if (context.isMarked(c.getValue())) {
      vectorObject.addFeature(c.getValue());
    }
  }

  @EventHandler
  public void onTemporaryEmissionSourceClearCommand(final TemporaryEmissionSourceClearCommand c) {
    vectorObject.removeFeature(c.getValue());
  }

  @EventHandler
  public void onSituationSwitch(final SituationSwitchEvent e) {
    vectorObject.clear();
    refreshLayer();
  }
}
