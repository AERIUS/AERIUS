/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.modal;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Emit;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;
import com.google.web.bindery.event.shared.EventBus;

import elemental2.dom.DomGlobal;
import elemental2.dom.HTMLElement;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.directives.VectorDirective;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.context.SimplePanelContext;
import nl.overheid.aerius.wui.application.daemon.geo.PanelActivateCommand;
import nl.overheid.aerius.wui.application.daemon.geo.PanelDockCommand;
import nl.overheid.aerius.wui.application.daemon.geo.PanelUndockCommand;
import nl.overheid.aerius.wui.application.util.DragUtil;
import nl.overheid.aerius.wui.application.util.VerticalResizeUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    ButtonIcon.class,
}, directives = {
    VectorDirective.class
})
public class PanelizedModalComponent extends BasicVueComponent implements IsVueComponent, HasMounted {
  @Prop boolean canResize;
  @Prop String debugId;

  @Prop EventBus eventBus;
  @Prop SimplePanelContext context;

  @Prop String title;
  @Prop String inactiveText;

  @Ref HTMLElement modal;
  @Ref HTMLElement dragHandle;

  @Ref HTMLElement panel;
  @Ref HTMLElement resizer;

  @Data boolean docked = true;

  @Computed
  public String getActiveText() {
    return isPanelInactive() ? inactiveText : title;
  }

  @Computed("isShowing")
  public boolean isShowing() {
    return context.isShowing();
  }

  @Override
  public void mounted() {
    DragUtil.draggable(modal, dragHandle, () -> dock(), () -> undock());
    VerticalResizeUtil.resizable(resizer, panel);
    final double contentHeight = DomGlobal.window.innerHeight - 150D;
    if (contentHeight < 500) {
      panel.style.setProperty("--modal-content-height", contentHeight + "px");
    } else {
      panel.style.setProperty("--modal-content-height", "100%");
    }
  }

  private void dock() {
    eventBus.fireEvent(new PanelDockCommand(context));
    docked = true;
  }

  private void undock() {
    eventBus.fireEvent(new PanelUndockCommand(context));
    docked = false;
  }

  @JsMethod
  public void setActive() {
    eventBus.fireEvent(new PanelActivateCommand(context));
  }

  @Computed("isPanelInactive")
  public boolean isPanelInactive() {
    return !context.isActive() && docked;
  }

  /**
   * Emits when the component wants to close through means other than user
   * interaction with the close button
   */
  @Emit
  @JsMethod
  public void autoClose() {}

  @Emit
  @JsMethod
  public void close() {}

  public void reset() {
    DragUtil.reset(modal);
    VerticalResizeUtil.reset(panel);
    docked = true;
  }
}
