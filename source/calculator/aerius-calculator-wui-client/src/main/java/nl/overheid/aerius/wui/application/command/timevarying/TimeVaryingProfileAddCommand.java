/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.command.timevarying;

import nl.aerius.wui.command.SimpleGenericCommand;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.source.TimeVaryingProfile;
import nl.overheid.aerius.wui.application.event.timevarying.TimeVaryingProfileAddedEvent;

/**
 * Command to add a new {@link TimeVaryingProfile} object.
 */
public class TimeVaryingProfileAddCommand extends SimpleGenericCommand<TimeVaryingProfile, TimeVaryingProfileAddedEvent> {
  private SituationContext situation;

  public TimeVaryingProfileAddCommand(final TimeVaryingProfile profile) {
    this(profile, null);
  }

  public TimeVaryingProfileAddCommand(final TimeVaryingProfile profile, final SituationContext situation) {
    super(profile);
    this.situation = situation;
  }

  public SituationContext getSituation() {
    return situation;
  }

  public void setSituation(final SituationContext situation) {
    this.situation = situation;
  }

  @Override
  protected TimeVaryingProfileAddedEvent createEvent(final TimeVaryingProfile value) {
    return new TimeVaryingProfileAddedEvent(value, situation);
  }

}
