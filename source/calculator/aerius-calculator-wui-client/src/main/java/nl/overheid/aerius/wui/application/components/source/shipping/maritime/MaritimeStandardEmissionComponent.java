/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.shipping.maritime;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.sector.category.MaritimeShippingCategory;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.maritime.MaritimeStandardValidators.MaritimeStandardValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.shipping.maritime.StandardMaritimeShipping;
import nl.overheid.aerius.wui.vue.DebugDirective;

@Component(name = "aer-maritime-standard", customizeOptions = {
    MaritimeStandardValidators.class
}, directives = {
    ValidateDirective.class,
    DebugDirective.class
}, components = {
    LabeledInputComponent.class,
    MaritimeMovementComponent.class,
    ValidationBehaviour.class,
})
public class MaritimeStandardEmissionComponent extends ErrorWarningValidator implements HasCreated,
    HasValidators<MaritimeStandardValidations> {
  @Prop @JsProperty StandardMaritimeShipping standardMaritime;
  @Data String descriptionV;
  @Data String shipCodeV;

  @Data @Inject ApplicationContext applicationContext;

  @JsProperty(name = "$v") MaritimeStandardValidations validation;

  @Computed
  public MaritimeStandardValidations getV() {
    return validation;
  }

  @Watch(value = "standardMaritime", isImmediate = true)
  public void onStandardMaritimeChange() {
    descriptionV = standardMaritime.getDescription();
    shipCodeV = standardMaritime.getShipCode();
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Computed
  protected String getDescription() {
    return standardMaritime.getDescription();
  }

  @Computed
  protected void setDescription(final String description) {
    standardMaritime.setDescription(description);
    this.descriptionV = description;
  }

  @Computed
  public List<MaritimeShippingCategory> getShipCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getMaritimeShippingCategories();
  }

  @Computed
  public String getMaritimeShippingCategory() {
    return standardMaritime.getShipCode();
  }

  @Computed
  public void setMaritimeShippingCategory(final String shipCode) {
    standardMaritime.setShipCode(shipCode);
    shipCodeV = shipCode;
  }

  @Computed("isInvalid")
  protected boolean isInvalid() {
    return validation.descriptionV.invalid || validation.shipCodeV.invalid;
  }

  @JsMethod
  protected String descriptionError() {
    return i18n.errorDescriptionRequired();
  }

  @JsMethod
  protected String shipCategoryRequiredError() {
    return i18n.errorShipCategoryRequired();
  }
}
