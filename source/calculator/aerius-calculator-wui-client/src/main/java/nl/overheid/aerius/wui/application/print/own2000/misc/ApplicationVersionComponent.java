/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.print.own2000.misc;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;

import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component
public class ApplicationVersionComponent extends BasicVueComponent {
  @Inject EnvironmentConfiguration cfg;
  @Inject ApplicationContext applicationContext;

  @Computed
  public String getReleaseDetailsUrl() {
    return applicationContext.getConfiguration().getSettings().getSetting(SharedConstantsEnum.RELEASE_DETAILS_URL);
  }

  @Computed
  public String getAeriusVersion() {
    return cfg.getApplicationVersion();
  }

  @Computed
  public String getDatabaseVersion() {
    return applicationContext.getConfiguration().getSettings().getSetting(SharedConstantsEnum.DATABASE_VERSION);
  }
}
