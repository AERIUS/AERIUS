/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.file;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import elemental2.dom.File;

import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.util.NotificationUtil;
import nl.overheid.aerius.js.file.SituationStats;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobAddCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationJobImportResultsCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointsAddCommand;
import nl.overheid.aerius.wui.application.command.importer.ImportFilesCommand;
import nl.overheid.aerius.wui.application.command.situation.SituationRefreshEmissionsCommand;
import nl.overheid.aerius.wui.application.components.importer.content.ImportFilterGroupSelectionCommand;
import nl.overheid.aerius.wui.application.components.importer.domain.ImportAction;
import nl.overheid.aerius.wui.application.components.importer.domain.ImportAction.ActionType;
import nl.overheid.aerius.wui.application.components.importer.domain.VirtualSituation;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ExportContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.context.importer.FileContext;
import nl.overheid.aerius.wui.application.daemon.scenario.FileImportPoller;
import nl.overheid.aerius.wui.application.daemon.scenario.SituationDaemon;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.application.domain.importer.ImportFilter;
import nl.overheid.aerius.wui.application.domain.importer.ImportFilters;
import nl.overheid.aerius.wui.application.domain.importer.ImportParcel;
import nl.overheid.aerius.wui.application.domain.importer.ImportParcelConverter;
import nl.overheid.aerius.wui.application.domain.importer.ImportParcelInitializer;
import nl.overheid.aerius.wui.application.domain.importer.ImportParcelSituation;
import nl.overheid.aerius.wui.application.domain.importer.ScenarioMetaData;
import nl.overheid.aerius.wui.application.domain.importer.SituationWithFileId;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;
import nl.overheid.aerius.wui.application.event.ImportCompleteEvent;
import nl.overheid.aerius.wui.application.event.ImportFailureEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;

public class ImportDaemon extends BasicEventComponent {
  private static final ImportDaemonEventBinder EVENT_BINDER = GWT.create(ImportDaemonEventBinder.class);

  interface ImportDaemonEventBinder extends EventBinder<ImportDaemon> {}

  private static final int MAX_NAME_LENGTH = 128;

  private static final ScenarioMetaData EMPTY_METADATA = ScenarioMetaData.create();

  @Inject private FileContext filesContext;
  @Inject private FileImportPoller poller;

  @Inject private ApplicationContext applicationContext;
  @Inject private ExportContext exportContext;
  @Inject private ImportParcelConverter converter;
  @Inject private SituationDaemon situationDaemon;

  @EventHandler
  public void onImportFilterSelectionCommand(final ImportFilterGroupSelectionCommand c) {
    final FileUploadStatus file = c.getValue();

    filesContext.getFiles().stream()
        .filter(v -> v.getFile().equals(file.getFile()))
        .forEach(v -> {
          if (c.getSelect()) {
            v.getFilters().add(c.getFilter());
          } else {
            v.getFilters().remove(c.getFilter());
          }
        });
  }

  @EventHandler
  public void onImportFailureEvent(final ImportFailureEvent e) {
    NotificationUtil.broadcastError(eventBus, new AeriusException(AeriusExceptionReason.IMPORT_FAILURE, e.getValue().getMessage()));
  }

  @EventHandler
  public void onImportFilesCommand(final ImportFilesCommand command) {
    poller.start(filesContext.getFiles().stream()
        .filter(v -> v.getImportAction().getType() != ActionType.IGNORE)
        .collect(Collectors.toList()), parcels -> processParcels(parcels));
  }

  private void processParcels(final Map<FileUploadStatus, ImportParcel> parcels) {
    try {
      parcels.forEach((k, v) -> process(k, v));

      tryProcessCalculationJobs(parcels);
      tryProcessMetaData(parcels, exportContext);

      notifyImportResult(parcels);

      eventBus.fireEvent(new ImportCompleteEvent());
    } catch (final RuntimeException e) {
      eventBus.fireEvent(new ImportFailureEvent(e));
    }
  }

  private void tryProcessMetaData(final Map<FileUploadStatus, ImportParcel> parcels, final ExportContext exportContext) {
    final boolean isMetaDataEmpty = EMPTY_METADATA.equals(exportContext.getScenarioMetaData());
    if (!isMetaDataEmpty) {
      return;
    }

    final long numberDifferentMetaData = filterOutIgnored(parcels)
        .map(v -> v.getValue())
        .filter(v -> v.getImportedMetaData() != null)
        .map(v -> v.getImportedMetaData())
        .map(v -> v.toJSONString())
        .distinct()
        .count();

    if (numberDifferentMetaData < 1) {
      // Do nothing
    } else if (numberDifferentMetaData > 1) {
      // Warn
      NotificationUtil.broadcastWarning(eventBus, M.messages().importNoticeMetaDataNotImported());
    } else {
      filterOutIgnored(parcels)
          .map(v -> v.getValue())
          .filter(v -> v.getImportedMetaData() != null)
          .findFirst()
          .ifPresent(parcel -> exportContext.setScenarioMetaData(parcel.getImportedMetaData()));
    }
  }

  private void tryProcessCalculationJobs(final Map<FileUploadStatus, ImportParcel> parcels) {
    parcels.entrySet().stream()
        .collect(Collectors.groupingBy(v -> v.getKey().getFile(), Collectors.toMap(v -> v.getKey(), v -> v.getValue())))
        .forEach((file, value) -> tryImportCalculationJob(file, value));
  }

  private void notifyImportResult(final Map<FileUploadStatus, ImportParcel> parcels) {
    final int count = (int) filterOutIgnored(parcels)
        .map(v -> v.getKey())
        .map(v -> v.getFile())
        .distinct().count();

    NotificationUtil.broadcastMessage(eventBus, M.messages().scenarioImportComplete(count));
  }

  private int tryImportCalculationJob(final File file, final Map<FileUploadStatus, ImportParcel> lst) {
    if (ImportDaemonUtil.skipImportCalculationJob(applicationContext, lst)) {
      return 0;
    }

    // Check whether calculation settings conflict within the group, warn and skip if so
    // This check can only be done at this stage of the import process, so we cannot warn earlier
    if (lst.entrySet().stream()
        .filter(v -> v.getKey().getImportAction().getType() == ActionType.ADD_TO_VIRTUAL_SITUATION)
        .map(v -> v.getValue())
        .map(v -> v.getCalculationSetOptions())
        .filter(v -> v != null)
        .peek(v -> CalculationSetOptions.init(v))
        .map(v -> v.toJSONString())
        .collect(Collectors.toSet())
        .size() != 1) {
      NotificationUtil.broadcastWarning(eventBus, M.messages().importNoticeCalculationTaskNotCreated(file.name));
      return 0;
    }

    // We can take the first because at this point we know they are identical for each child-file
    final CalculationSetOptions calculationOptions = lst.values().iterator().next().getCalculationSetOptions();

    eventBus.fireEvent(new CalculationJobAddCommand(job -> onJobAdded(file, lst, calculationOptions, job)));
    return 1;
  }

  private void onJobAdded(final File file, final Map<FileUploadStatus, ImportParcel> lst, final CalculationSetOptions calculationOptions,
      final CalculationJobContext job) {
    // Set calculation options if any, otherwise leave the default
    if (calculationOptions != null) {
      ImportParcelInitializer.postProcessCalculationOptions(calculationOptions, applicationContext);
      final CalculationSetOptions copy = calculationOptions.copy();
      job.setCalculationOptions(copy);
    }

    // Derive the situation composition and set it
    final SituationComposition comp = new SituationComposition();
    lst.keySet().stream()
        .map(v -> v.getImportAction())
        .filter(v -> v.getType() == ActionType.ADD_TO_VIRTUAL_SITUATION)
        .map(v -> v.getVirtualSituation())
        .forEach(v -> comp.addSituation(v.getNewlyExistingSituation()));
    job.setSituationComposition(comp);

    job.setName(sanitizeJobName(job, file));

    if (hasAnyResultsToImport(lst.keySet())) {
      handleResultImport(lst, job);
    }
  }

  private static String sanitizeJobName(final CalculationJobContext job, final File file) {
    final String newJobName = job.getName() + " - " + stripExtension(file.name);

    return newJobName.length() > MAX_NAME_LENGTH
        ? newJobName.substring(0, MAX_NAME_LENGTH)
        : newJobName;
  }

  private static String stripExtension(final String filename) {
    if (filename == null) {
      return filename;
    }
    final int lastIndexOf = filename.lastIndexOf('.');

    return lastIndexOf < 0 ? filename : filename.substring(0, lastIndexOf);
  }

  private void handleResultImport(final Map<FileUploadStatus, ImportParcel> lst, final CalculationJobContext job) {
    final List<SituationWithFileId> situationWithFileIds = lst.keySet().stream()
        .filter(x -> x.getCreatedSituationId() != null)
        .map(x -> {
          final SituationWithFileId situationWithFileId = new SituationWithFileId();
          situationWithFileId.setSituationId(x.getCreatedSituationId());
          situationWithFileId.setFileId(x.getFileCode());
          return situationWithFileId;
        })
        .collect(Collectors.toList());

    final String archiveContributionFileId = lst.keySet().stream()
        .filter(x -> x.getImportAction().getType() == ActionType.ADD_ARCHIVE_CONTRIBUTION)
        .map(x -> x.getFileCode())
        .findFirst().orElse(null);
    if (archiveContributionFileId != null) {
      job.getCalculationOptions().setUseInCombinationArchive(true);
    }

    eventBus.fireEvent(new CalculationJobImportResultsCommand(job, situationWithFileIds, archiveContributionFileId));
  }

  private void process(final FileUploadStatus file, final ImportParcel parcel) {
    final ImportAction action = file.getImportAction();
    if (action == null) {
      // Bug out because this should be validated for
      throw new RuntimeException("Cannot import file without a chosen action.");
    }

    switch (action.getType()) {
    case ADD_CALCULATION_POINTS:
      addCalculationPoints(file.getFilters(), parcel.getCalculationPoints());
      break;
    case ADD_TO_EXISTING_SITUATION:
      handleExistingSituationImport(file, action.getExistingSituation(), parcel.getSituation());
      break;
    case ADD_TO_VIRTUAL_SITUATION:
      handleVirtualSituationImport(file, parcel);
      break;
    case ADD_ARCHIVE_CONTRIBUTION:
    case IGNORE:
      // Do nothing
      break;
    case ADD_NEW:
      throw new RuntimeException("Unsupported action type: " + action.getType());
    }
  }

  private void addCalculationPoints(final ImportFilters filters, final Object points) {
    if (filters.isDisallowed(ImportFilter.CALCULATION_POINTS)) {
      return;
    }

    final CalculationPointFeature[] calculationPoints = converter.getCalculationPoints(points);
    if (calculationPoints.length > 0) {
      eventBus.fireEvent(new CalculationPointsAddCommand(Arrays.asList(calculationPoints)));
    }
  }

  private void handleExistingSituationImport(final FileUploadStatus file, final SituationContext situationContext,
      final ImportParcelSituation importParcelSituation) {
    converter.addParcelToSituation(importParcelSituation, situationContext, file.getFilters());
    if (importParcelSituation.getYear() != situationContext.getYear()) {
      // As the import might contain sources with emissions that are year-dependant, ensure these are refreshed.
      eventBus.fireEvent(new SituationRefreshEmissionsCommand(situationContext));
    }
  }

  private void handleVirtualSituationImport(final FileUploadStatus file, final ImportParcel parcel) {
    final ImportAction action = file.getImportAction();

    final ImportParcelSituation parcelSituation = parcel.getSituation();
    final VirtualSituation virtualSituation = action.getVirtualSituation();
    final SituationContext situation = virtualSituation.getNewlyExistingSituation();
    if (situation != null) {
      converter.addParcelToSituation(parcelSituation, situation, file.getFilters());
      tryHandleSituationParcel(action, file, parcel);
    } else {
      situationDaemon.createAndAddSituation(newSituation -> {
        // By tracking the 'newlyExistingSituation', files may be added and processed out-of-order without issue
        virtualSituation.setNewlyExistingSituation(newSituation);

        converter.addParcelToSituation(parcelSituation, newSituation, file.getFilters());
        tryHandleSituationParcel(action, file, parcel);
      });
    }
  }

  /**
   * Try to set situation properties and whatever else for the given file/parcel -- does not do anything if this is not the "base" file
   */
  private void tryHandleSituationParcel(final ImportAction action, final FileUploadStatus file, final ImportParcel parcel) {
    final VirtualSituation virtualSituation = action.getVirtualSituation();
    if (!VirtualSituation.isSameBaseFile(virtualSituation, file)) {
      // Abort if virtual file handle is not the file being processed, i.e. not the base file
      return;
    }

    file.setCreatedSituationId(virtualSituation.getNewlyExistingSituation().getId());
    addSituationBaseProperties(parcel.getSituation(), virtualSituation);
  }

  private void addSituationBaseProperties(final ImportParcelSituation parcelSituation, final VirtualSituation virtualSituation) {
    final SituationContext situation = virtualSituation.getNewlyExistingSituation();
    situation.setName(this.getTruncatedSituationName(virtualSituation.getName()));
    SituationContext.safeSetSituationType(situation, virtualSituation.getSituationType());
    if (parcelSituation.getYear() != 0) {
      situation.setYear(parcelSituation.getYear());
    }
    final Double nettingFactor = parcelSituation.getNettingFactor();
    if (nettingFactor == null) {
      situation.setNettingFactor(applicationContext.getSetting(SharedConstantsEnum.DEFAULT_NETTING_FACTOR));
    } else {
      situation.setNettingFactor(parcelSituation.getNettingFactor());
    }
  }

  private String getTruncatedSituationName(final String name) {
    return name.length() > MAX_NAME_LENGTH ? name.substring(0, MAX_NAME_LENGTH) : name;
  }

  private static Stream<Entry<FileUploadStatus, ImportParcel>> filterOutIgnored(final Map<FileUploadStatus, ImportParcel> parcels) {
    return parcels.entrySet().stream()
        .filter(x -> x.getKey().getImportAction().getType() != ActionType.IGNORE);
  }

  private static boolean hasAnyResultsToImport(final Collection<FileUploadStatus> uploadStatuses) {
    return uploadStatuses.stream()
        .filter(x -> x.getFilters().isAllowed(ImportFilter.RESULTS))
        .map(FileUploadStatus::getSituationStats)
        .map(SituationStats::getCalculationResults)
        .anyMatch(x -> x > 0);
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
