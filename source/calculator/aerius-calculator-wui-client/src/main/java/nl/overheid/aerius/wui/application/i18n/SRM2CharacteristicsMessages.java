/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.i18n;

import com.google.gwt.i18n.client.Messages.Select;

import nl.overheid.aerius.shared.domain.v2.source.road.RoadElevation;
import nl.overheid.aerius.shared.domain.v2.source.road.SRM2RoadSideBarrierType;

/**
 * Text specific for SRM2 characteristics.
 */
public interface SRM2CharacteristicsMessages {
  String sourceRoadTunnelFactorCheckboxLabel();
  String sourceRoadTunnelFactorLabel();
  String sourceRoadElevationTypeLabel();
  String sourceRoadElevationType(@Select RoadElevation roadElevation);
  String sourceRoadElevationTypeShort(@Select RoadElevation roadElevation);
  String sourceRoadHeight();
  String sourceRoadPorositeit();
  String sourceRoadBarrierTitle();
  String sourceRoadBarrierType();
  String sourceRoadBarrierLeftTitle();
  String sourceRoadBarrierRightTitle();
  String sourceRoadsideBarrierNone();
  String sourceRoadsideBarrierType(@Select SRM2RoadSideBarrierType roadSideBarrierType);
  String sourceRoadBarrierHeight();
  String sourceRoadBarrierLeftHeight();
  String sourceRoadBarrierRightHeight();
  String sourceRoadBarrierDistance();
  String sourceRoadBarrierLeftDistance();
  String sourceRoadBarrierRightDistance();
}
