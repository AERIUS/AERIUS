/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.place;

import java.util.function.Supplier;

import nl.aerius.wui.place.PlaceTokenizer;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.context.InputTypeViewMode;

/**
 * Place to display the list of theme sources.
 */
public class ScenarioInputListPlace extends MainThemePlace {

  private static final String PLACE_KEY_SOURCES = "inputs";

  public static class Tokenizer extends MainThemePlace.Tokenizer<ScenarioInputListPlace> {
    public Tokenizer(final Supplier<ScenarioInputListPlace> provider, final Theme theme, final String... postfix) {
      super(provider, theme, postfix);
    }
  }

  private InputTypeViewMode mode;

  public ScenarioInputListPlace(final Theme theme) {
    this(theme, null);
  }

  public ScenarioInputListPlace(final Theme theme, final InputTypeViewMode mode) {
    this(createTokenizer(theme), mode);
  }

  public <P extends MainThemePlace> ScenarioInputListPlace(final PlaceTokenizer<P> tokenizer) {
    super(tokenizer);
  }

  public <P extends MainThemePlace> ScenarioInputListPlace(final PlaceTokenizer<P> tokenizer, final InputTypeViewMode mode) {
    super(tokenizer);
    this.mode = mode;
  }

  public static PlaceTokenizer<ScenarioInputListPlace> createTokenizer(final Theme theme) {
    return new Tokenizer(() -> new ScenarioInputListPlace(theme), theme, PLACE_KEY_SOURCES);
  }

  @Override
  public <T> T visit(final PlaceVisitor<T> visitor) {
    return visitor.apply(this);
  }

  public InputTypeViewMode getMode() {
    return mode;
  }

  public void setMode(final InputTypeViewMode mode) {
    this.mode = mode;
  }
}
