/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.resources.images.markers;

import java.util.Optional;
import java.util.function.Supplier;

import com.google.gwt.resources.client.DataResource;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.wui.application.resources.R;

public class MarkerResourceUtil {

  private static final double[] ANCHOR_DEFAULT_BOTTOM_CENTER = new double[] {0.5, 1.0};
  private static final double[] ANCHOR_CENTER = new double[] {0.5, 0.5};

  public static final MarkerResource getHcon(final Theme theme, final ScenarioResultType type) {
    return getOwN2000OrNca(theme,
        () -> getResultTypeResource(type,
            () -> R.images().srHbij(),
            null,
            null,
            null,
            null,
            null),
        () -> getResultTypeResource(type,
            () -> R.images().scHcon(),
            null,
            null,
            null,
            null,
            null));
  }

  public static final MarkerResource getHconHcum(final Theme theme, final ScenarioResultType type) {
    return getOwN2000OrNca(theme,
        () -> getResultTypeResource(type,
            null,
            null,
            null,
            null,
            null,
            null),
        () -> getResultTypeResource(type,
            null,
            () -> R.images().pcHconHcum(),
            null,
            () -> R.images().icHconHcum(),
            () -> R.images().acHconHcum(),
            null));
  }

  public static final MarkerResource getHinc(final Theme theme, final ScenarioResultType type) {
    return getOwN2000OrNca(theme,
        () -> getResultTypeResource(type,
            null,
            () -> R.images().pbHtoe(),
            null,
            () -> R.images().cbHtoe(),
            () -> R.images().acHinc(),
            null),
        () -> getResultTypeResource(type,
            null,
            () -> R.images().pcHinc(),
            null,
            () -> R.images().icHinc(),
            () -> R.images().acHinc(),
            null));
  }

  public static final MarkerResource getHincHcum(final Theme theme, final ScenarioResultType type) {
    return getOwN2000OrNca(theme,
        () -> getResultTypeResource(type,
            null,
            null,
            null,
            null,
            null,
            null),
        () -> getResultTypeResource(type,
            null,
            () -> R.images().pcHincHcum(),
            null,
            () -> R.images().icHincHcum(),
            () -> R.images().acHincHcum(),
            null));
  }

  public static final MarkerResource getHdec(final Theme theme, final ScenarioResultType type) {
    return getOwN2000OrNca(theme,
        () -> getResultTypeResource(type,
            null,
            () -> R.images().pbHafn(),
            null,
            () -> R.images().cbHafn(),
            () -> R.images().acHdec(),
            null),
        () -> getResultTypeResource(type,
            null,
            () -> R.images().pcHdec(),
            null,
            () -> R.images().icHdec(),
            () -> R.images().acHdec(),
            null));
  }

  public static final MarkerResource getHcum(final Theme theme, final ScenarioResultType type) {
    return getOwN2000OrNca(theme,
        () -> getResultTypeResource(type,
            null,
            null,
            null,
            null,
            null,
            null),
        () -> getResultTypeResource(type,
            null,
            () -> R.images().pcHcum(),
            null,
            () -> R.images().icHcum(),
            () -> R.images().acHcum(),
            null));
  }

  public static final MarkerResource getHdecHcum(final Theme theme, final ScenarioResultType type) {
    return getOwN2000OrNca(theme,
        () -> getResultTypeResource(type,
            null,
            null,
            null,
            null,
            null,
            null),
        () -> getResultTypeResource(type,
            null,
            () -> R.images().pcHdecHcum(),
            null,
            () -> R.images().icHdecHcum(),
            () -> R.images().acHdecHcum(),
            null));
  }

  public static final MarkerResource getHmax(final Theme theme, final ScenarioResultType type) {
    return getOwN2000OrNca(theme,
        () -> getResultTypeResource(type,
            null,
            null,
            () -> R.images().mtHmax(),
            null,
            null,
            null),
        () -> getResultTypeResource(type,
            null,
            null,
            () -> R.images().mtHmax(),
            null,
            null,
            null));
  }

  public static final MarkerResource getHtot(final Theme theme, final ScenarioResultType type) {
    return getOwN2000OrNca(theme,
        () -> getResultTypeResource(type,
            () -> R.images().srHtot(),
            () -> R.images().pbHtot(),
            () -> R.images().mtHtot(),
            () -> R.images().cbHtot(),
            () -> R.images().acHtot(),
            null),
        () -> getResultTypeResource(type,
            () -> R.images().scHtot(),
            () -> R.images().pcHtot(),
            () -> R.images().mtHtot(),
            () -> R.images().icHtot(),
            () -> R.images().acHtot(),
            null));
  }

  public static final MarkerResource getHtotHcumHinc(final Theme theme, final ScenarioResultType type) {
    return getOwN2000OrNca(theme,
        () -> getResultTypeResource(type,
            null,
            null,
            null,
            null,
            null,
            null),
        () -> getResultTypeResource(type,
            null,
            () -> R.images().pcHtotHcumHinc(),
            null,
            () -> R.images().icHtotHcumHinc(),
            () -> R.images().acHtotHcumHinc(),
            null));
  }

  public static final MarkerResource getHtotHcumHdec(final Theme theme, final ScenarioResultType type) {
    return getOwN2000OrNca(theme,
        () -> getResultTypeResource(type,
            null,
            null,
            null,
            null,
            null,
            null),
        () -> getResultTypeResource(type,
            null,
            () -> R.images().pcHtotHcumHdec(),
            null,
            () -> R.images().icHtotHcumHdec(),
            () -> R.images().acHtotHcumHdec(),
            null));
  }

  public static final MarkerResource getHconHtot(final Theme theme, final ScenarioResultType type) {
    return getOwN2000OrNca(theme,
        () -> getResultTypeResource(type,
            () -> R.images().srHbijHtot(),
            null,
            null,
            null,
            null,
            null),
        () -> getResultTypeResource(type,
            () -> R.images().scHconHtot(),
            null,
            null,
            null,
            null,
            null));
  }

  public static final MarkerResource getHincHtot(final Theme theme, final ScenarioResultType type) {
    return getOwN2000OrNca(theme,
        () -> getResultTypeResource(type,
            null,
            () -> R.images().pbHtoeHtot(),
            null,
            () -> R.images().cbHtoeHtot(),
            () -> R.images().acHincHtot(),
            null),
        () -> getResultTypeResource(type,
            null,
            () -> R.images().pcHincHtot(),
            null,
            () -> R.images().icHincHtot(),
            () -> R.images().acHincHtot(),
            null));
  }

  public static final MarkerResource getHdecHtot(final Theme theme, final ScenarioResultType type) {
    return getOwN2000OrNca(theme,
        () -> getResultTypeResource(type,
            null,
            () -> R.images().pbHafnHtot(),
            null,
            () -> R.images().cbHafnHtot(),
            () -> R.images().acHdecHtot(),
            null),
        () -> getResultTypeResource(type,
            null,
            () -> R.images().pcHdecHtot(),
            null,
            () -> R.images().icHdecHtot(),
            () -> R.images().acHdecHtot(),
            null));
  }

  public static final MarkerResource getHtotHcum(final Theme theme, final ScenarioResultType type) {
    return getOwN2000OrNca(theme,
        () -> getResultTypeResource(type,
            null,
            null,
            null,
            null,
            null,
            null),
        () -> getResultTypeResource(type,
            null,
            () -> R.images().pcHtotHcum(),
            null,
            () -> R.images().icHtotHcum(),
            () -> R.images().acHtotHcum(),
            null));
  }

  public static final MarkerResource getHmaxHtot(final Theme theme, final ScenarioResultType type) {
    return getOwN2000OrNca(theme,
        () -> getResultTypeResource(type,
            null,
            null,
            () -> R.images().mtHmaxHtot(),
            null,
            null,
            null),
        () -> getResultTypeResource(type,
            null,
            null,
            () -> R.images().mtHmaxHtot(),
            null,
            null,
            null));
  }

  public static final MarkerResource getHtotHconHcum(final Theme theme, final ScenarioResultType type) {
    return getOwN2000OrNca(theme,
        () -> getResultTypeResource(type,
            null,
            null,
            null,
            null,
            null,
            null),
        () -> getResultTypeResource(type,
            null,
            () -> R.images().pcHtotHconHcum(),
            null,
            () -> R.images().icHtotHconHcum(),
            () -> R.images().acHtotHconHcum(),
            null));
  }

  public static final MarkerResource getDsProcurementPolicyPass(final Theme theme, final ScenarioResultType type) {
    return getOwN2000OrNca(theme,
        () -> getResultTypeResource(type,
            null,
            null,
            null,
            null,
            null,
            () -> R.images().dsProcurementPolicyPass()),
        () -> getResultTypeResource(type,
            null,
            null,
            null,
            null,
            null,
            null),
        ANCHOR_CENTER);
  }

  public static final MarkerResource getDsProcurementPolicyFail(final Theme theme, final ScenarioResultType type) {
    return getOwN2000OrNca(theme,
        () -> getResultTypeResource(type,
            null,
            null,
            null,
            null,
            null,
            () -> R.images().dsProcurementPolicyFail()),
        () -> getResultTypeResource(type,
            null,
            null,
            null,
            null,
            null,
            null),
        ANCHOR_CENTER);
  }

  public static final MarkerResource getSiteLocationMarker() {
    return new MarkerResource(R.images().siteLocationMarker(), ANCHOR_DEFAULT_BOTTOM_CENTER);
  }

  private static final DataResource getResultTypeResource(final ScenarioResultType type, final Supplier<DataResource> srSupplier,
      final Supplier<DataResource> pcSupplier, final Supplier<DataResource> mtSupplier, final Supplier<DataResource> icSupplier,
      final Supplier<DataResource> acSupplier, final Supplier<DataResource> dsSupplier) {
    final Optional<DataResource> res;
    switch (type) {
    case SITUATION_RESULT:
      res = Optional.ofNullable(srSupplier)
          .map(v -> v.get());
      break;
    case PROJECT_CALCULATION:
      res = Optional.ofNullable(pcSupplier)
          .map(v -> v.get());
      break;
    case MAX_TEMPORARY_EFFECT:
      res = Optional.ofNullable(mtSupplier)
          .map(v -> v.get());
      break;
    case IN_COMBINATION:
      res = Optional.ofNullable(icSupplier)
          .map(v -> v.get());
      break;
    case ARCHIVE_CONTRIBUTION:
      res = Optional.ofNullable(acSupplier)
          .map(v -> v.get());
      break;
    case DEPOSITION_SUM:
      res = Optional.ofNullable(dsSupplier)
          .map(v -> v.get());
      break;
    default:
      res = Optional.empty();
    }

    return res.orElse(null);
  }

  private static final MarkerResource getOwN2000OrNca(final Theme theme, final Supplier<DataResource> own2000Supplier,
      final Supplier<DataResource> ncaSupplier, final double[] anchor) {
    DataResource res;
    switch (theme) {
    case NCA:
      res = ncaSupplier.get();
      break;
    case OWN2000:
      res = own2000Supplier.get();
      break;
    case RBL:
    default:
      throw new RuntimeException("No resource for theme: " + theme);
    }
    return new MarkerResource(res, anchor);
  }

  private static final MarkerResource getOwN2000OrNca(final Theme theme, final Supplier<DataResource> ownSupplier,
      final Supplier<DataResource> ncaSupplier) {
    return getOwN2000OrNca(theme, ownSupplier, ncaSupplier, ANCHOR_DEFAULT_BOTTOM_CENTER);
  }
}
