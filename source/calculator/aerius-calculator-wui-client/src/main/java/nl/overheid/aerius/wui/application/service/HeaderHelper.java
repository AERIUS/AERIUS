/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

import com.google.gwt.i18n.client.LocaleInfo;
import com.google.inject.Singleton;

import nl.overheid.aerius.wui.application.context.UserContext;

/**
 * Class that can help with HTTP request headers to be used.
 * Contains methods to obtain (or fill) a map with default headers.
 */
@Singleton
public class HeaderHelper {

  private static final String ACCEPT_LANGUAGE = "Accept-Language";
  private static final String API_KEY = "api-key";

  private @Inject UserContext userContext;

  /**
   * Return a (new) map with default header values.
   */
  public Map<String, String> defaultHeaders() {
    return defaultHeaders(null);
  }

  public Map<String, String> defaultHeaders(final String apiKey) {
    final Map<String, String> headers = new HashMap<>();
    addDefaultHeaders(headers, apiKey);
    return headers;
  }

  /**
   * Adds default headers to the supplied header map.
   */
  public void addDefaultHeaders(final Map<String, String> headers, final String apiKey) {
    headers.put(ACCEPT_LANGUAGE, LocaleInfo.getCurrentLocale().getLocaleName());
    addApiKey(headers);
    Optional.ofNullable(apiKey).ifPresent(key -> headers.put(API_KEY, key));
  }

  private void addApiKey(final Map<String, String> headers) {
    if (userContext.isLoggedIn()) {
      headers.put(API_KEY, userContext.getApiKey());
    }
  }
}
