/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.results;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasDestroyed;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import elemental2.dom.HTMLElement;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.wui.application.command.result.ProcessContributionsLegendPanelDockRemoveCommand;
import nl.overheid.aerius.wui.application.command.result.ProcessContributionsLegendPopupHiddenCommand;
import nl.overheid.aerius.wui.application.command.result.ProcessContributionsLegendPopupResetCommand;
import nl.overheid.aerius.wui.application.command.result.ProcessContributionsLegendPopupToggleCommand;
import nl.overheid.aerius.wui.application.command.result.ProcessContributionsLegendPopupVisibleCommand;
import nl.overheid.aerius.wui.application.components.modal.PanelizedModalComponent;
import nl.overheid.aerius.wui.application.context.ProcessContributionsLegendPanelContext;
import nl.overheid.aerius.wui.application.util.ScreenUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    ProcessContributionsLegendPanel.class,
    PanelizedModalComponent.class
})
public class ProcessContributionsLegendPanelModal extends BasicVueComponent implements IsVueComponent, HasCreated, HasDestroyed {
  private static final LegendPanelModalEventBinder EVENT_BINDER = GWT.create(LegendPanelModalEventBinder.class);

  interface LegendPanelModalEventBinder extends EventBinder<ProcessContributionsLegendPanelModal> {}

  @Prop EventBus eventBus;

  @Data @Inject ProcessContributionsLegendPanelContext context;

  @Ref PanelizedModalComponent modal;

  private HandlerRegistration handlers;

  @Data HTMLElement dock;

  @EventHandler
  public void onLegendPanelDockRemoveCommand(final ProcessContributionsLegendPanelDockRemoveCommand c) {
    if (dock == c.getValue()) {
      dock = null;
    }
  }

  @EventHandler
  public void onProcessContributionsLegendPopupToggleCommand(final ProcessContributionsLegendPopupToggleCommand c) {
    SchedulerUtil.delay(() -> {
      ScreenUtil.attachElementLeft(c.getValue(), modal.vue().$el(), ScreenUtil.OFFSET_LEFT);
    });
  }

  @JsMethod
  public void close() {
    eventBus.fireEvent(new ProcessContributionsLegendPopupHiddenCommand());
  }

  @EventHandler
  public void onProcessContributionsLegendPopupResetCommand(final ProcessContributionsLegendPopupResetCommand c) {
    modal.reset();
    eventBus.fireEvent(new ProcessContributionsLegendPopupVisibleCommand());
  }

  @Override
  public void created() {
    handlers = EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Override
  public void destroyed() {
    if (handlers != null) {
      handlers.removeHandler();
    }
  }
}
