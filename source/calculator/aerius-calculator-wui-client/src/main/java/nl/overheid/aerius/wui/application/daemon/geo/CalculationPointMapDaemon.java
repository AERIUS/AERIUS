/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.daemon.geo;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.geom.GeometryType;

import nl.aerius.geo.command.InteractionAddedCommand;
import nl.aerius.geo.command.InteractionRemoveCommand;
import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.event.PlaceChangeEvent;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointDrawCommand;
import nl.overheid.aerius.wui.application.geo.interactions.FeatureDrawInteraction;
import nl.overheid.aerius.wui.application.place.CalculationPointsPlace;

public class CalculationPointMapDaemon extends BasicEventComponent implements Daemon {
  private static final CalculationPointMapDaemonEventBinder EVENT_BINDER = GWT.create(CalculationPointMapDaemonEventBinder.class);

  interface CalculationPointMapDaemonEventBinder extends EventBinder<CalculationPointMapDaemon> {}

  private FeatureDrawInteraction drawInteraction;

  @EventHandler
  public void onCalculationPointDrawCommand(final CalculationPointDrawCommand c) {
    if (drawInteraction != null) {
      eventBus.fireEvent(new InteractionRemoveCommand(drawInteraction));
    }

    if (c.getValue() != null) {
      drawInteraction = new FeatureDrawInteraction(GeometryType.Point, c.getValue());
      eventBus.fireEvent(new InteractionAddedCommand(drawInteraction));
    }
  }

  @EventHandler
  public void onPlaceChange(final PlaceChangeEvent e) {
    if (!(e.getValue() instanceof CalculationPointsPlace) && drawInteraction != null) {
      eventBus.fireEvent(new CalculationPointDrawCommand(null));
    }
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
