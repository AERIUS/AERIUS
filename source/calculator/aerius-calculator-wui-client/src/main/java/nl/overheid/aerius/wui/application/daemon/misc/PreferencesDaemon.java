/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.misc;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.shared.domain.v2.point.AssessmentCategory;
import nl.overheid.aerius.wui.application.daemon.flags.LocalStorageUtil;
import nl.overheid.aerius.wui.application.ui.pages.preferences.PreferencesView.ImportMode;

@Singleton
public class PreferencesDaemon extends BasicEventComponent implements Daemon {
  private static final PreferencesDaemonEventBinder EVENT_BINDER = GWT.create(PreferencesDaemonEventBinder.class);

  interface PreferencesDaemonEventBinder extends EventBinder<PreferencesDaemon> {}

  public static final String SELECTED_PROJECTION = "setting_defaultProjectionConversion";
  public static final String ADVANCED_USER_IMPORT = "setting_advancedUserImport";
  public static final String HIDE_PRIVACY_MESSAGE = "setting_hidePrivacyWarning";
  public static final String HIDE_SECTOR_HINT_MESSAGE = "setting_hideSectorHintWarning";
  public static final String LAYER_AUTO_SWITCH = "setting_layerAutoSwitch";
  public static final String ENABLE_LOGIN = "setting_enableLogin";
  public static final String MANUAL_AUTO_SWITCH = "setting_manualAutoSwitch";
  public static final String ASSESSMENT_POINT_CATEGORY = "setting_assessmentPointCategory";
  public static final String COLLAPSED_MENU = "setting_collapsedMenu";

  @Inject private PersistablePreferencesContext preferencesContext;

  private final Map<String, BiConsumer<PersistablePreferencesContext, Object>> persistableSettings = new HashMap<>();

  public PreferencesDaemon() {
    persistableSettings.put(SELECTED_PROJECTION,
        (con, v) -> con.setSelectedProjection(v == null ? null : String.valueOf(v)));
    persistableSettings.put(ADVANCED_USER_IMPORT,
        (con, v) -> con.setImportMode(v == null ? null : ImportMode.safeValueOf(String.valueOf(v))));
    persistableSettings.put(HIDE_PRIVACY_MESSAGE,
        (con, v) -> con.setHidePrivacyWarning(v != null && Boolean.parseBoolean(String.valueOf(v))));
    persistableSettings.put(HIDE_SECTOR_HINT_MESSAGE,
        (con, v) -> con.setHideSectorWarning(v != null && Boolean.parseBoolean(String.valueOf(v))));
    persistableSettings.put(LAYER_AUTO_SWITCH,
        (con, v) -> con.setLayerAutoSwitch(v != null && Boolean.parseBoolean(String.valueOf(v))));
    persistableSettings.put(ENABLE_LOGIN,
        (con, v) -> con.setLoginEnabledSwitch(v != null && Boolean.parseBoolean(String.valueOf(v))));
    persistableSettings.put(MANUAL_AUTO_SWITCH,
        (con, v) -> con.setManualAutoSwitch(v != null && Boolean.parseBoolean(String.valueOf(v))));
    persistableSettings.put(ASSESSMENT_POINT_CATEGORY,
        (con, v) -> con.setAssessmentCategory(v == null ? null : AssessmentCategory.safeValueOf(String.valueOf(v))));
    persistableSettings.put(COLLAPSED_MENU,
        (con, v) -> con.setCollapsedMenu(v != null && Boolean.parseBoolean(String.valueOf(v))));
  }

  @Override
  public void init() {
    init(SELECTED_PROJECTION);
    init(ADVANCED_USER_IMPORT, ImportMode.BASIC);
    init(HIDE_PRIVACY_MESSAGE);
    init(HIDE_SECTOR_HINT_MESSAGE);
    init(LAYER_AUTO_SWITCH, "true");
    init(ENABLE_LOGIN);
    init(MANUAL_AUTO_SWITCH, "true");
    init(ASSESSMENT_POINT_CATEGORY, null);
    init(COLLAPSED_MENU);
  }

  public void init(final String key) {
    LocalStorageUtil.tryGet(key)
        .ifPresent(v -> Optional.ofNullable(persistableSettings.get(key))
            .ifPresent(consumer -> consumer.accept(preferencesContext, v)));
  }

  public void init(final String key, final Object defaultt) {
    Optional.ofNullable(persistableSettings.get(key))
        .ifPresent(consumer -> consumer.accept(preferencesContext, LocalStorageUtil.tryGet(key)
            .orElse(defaultt == null ? null : String.valueOf(defaultt))));
  }

  @EventHandler
  public void onPersistUserSetting(final UserSettingChangeCommand c) {
    LocalStorageUtil.trySet(c.getKey(), c.getValue());
    Optional.ofNullable(persistableSettings.get(c.getKey()))
        .ifPresent(v -> v.accept(preferencesContext, c.getValue()));
  }

  @EventHandler
  public void onUserSettingRemoveCommand(final UserSettingRemoveCommand c) {
    LocalStorageUtil.tryRemove(c.getKey());
    Optional.ofNullable(persistableSettings.get(c.getKey()))
        .ifPresent(v -> v.accept(preferencesContext, ""));
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }

}
