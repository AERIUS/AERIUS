/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.nca.pages.calculate.options;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.backgrounddata.MetSite;
import nl.overheid.aerius.shared.domain.backgrounddata.MetSiteDataset;
import nl.overheid.aerius.shared.domain.calculation.MetDatasetType;
import nl.overheid.aerius.wui.application.components.input.InputHintComponent;
import nl.overheid.aerius.wui.application.components.input.InputWarningComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.components.toggle.ToggleButtons;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationComposition;
import nl.overheid.aerius.wui.application.domain.calculation.ADMSOptions;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.ui.nca.pages.calculate.options.MetSiteSelectionValidators.MetSiteSelectionValidations;
import nl.overheid.aerius.wui.vue.StyleDirective;

@Component(customizeOptions = {
    MetSiteSelectionValidators.class,
}, directives = {
    ValidateDirective.class,
    StyleDirective.class,
}, components = {
    ValidationBehaviour.class,
    MetSiteSearchComponent.class,
    MetSiteSuggestComponent.class,
    ToggleButtons.class,
    LabeledInputComponent.class,
    SimplifiedListBoxComponent.class,
    InputHintComponent.class,
    InputWarningComponent.class,
})
public class CalculationJobMeteorologicalSettingsComponent extends ErrorWarningValidator
    implements HasCreated, HasValidators<MetSiteSelectionValidations> {
  private static final String DATASET_TYPE_OBS = "OBS";
  private static final String DATASET_TYPE_NWP = "NWP";

  protected static final String MET_SITE_SUGGEST = "SUGGEST";
  protected static final String MET_SITE_SEARCH = "SEARCH";

  protected static final String RECENT_YEARS = "recent";
  protected static final int MIN_RECENT_YEARS = 2;
  protected static final int MAX_RECENT_YEARS = 5;

  private static final Map<String, List<MetDatasetType>> DATASET_SELECTION_FLOWS = new HashMap<>();
  static {
    DATASET_SELECTION_FLOWS.put(DATASET_TYPE_NWP,
        Arrays.stream(MetDatasetType.values())
            .filter(v -> v.isNwp())
            .collect(Collectors.toList()));
    DATASET_SELECTION_FLOWS.put(DATASET_TYPE_OBS,
        Arrays.stream(MetDatasetType.values())
            .filter(v -> v.isObserved())
            .collect(Collectors.toList()));
  }

  @Data String datasetTypeFlow;
  @Data String metSiteFlow = MET_SITE_SUGGEST;

  @Prop(required = true) CalculationSetOptions options;
  @Prop(required = true) SituationComposition composition;

  @Data MetDatasetType metDatasetTypeV;
  @Data String metSiteYearV;

  @Data String metSiteFilterText;
  @Data String recentYears = RECENT_YEARS;

  @Inject @Data ApplicationContext applicationContext;

  @JsProperty(name = "$v") MetSiteSelectionValidations validation;

  @Data @JsProperty Map<MetDatasetType, List<MetSite>> siteLocationsCache = new HashMap<>();

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Override
  @Computed
  public MetSiteSelectionValidations getV() {
    return validation;
  }

  @Computed
  public List<String> getDatasetTypeFlowOptions() {
    return Arrays.asList(DATASET_TYPE_OBS, DATASET_TYPE_NWP);
  }

  @JsMethod
  public void handleDatasetTypeFlowSelection(final String datasetTypeFlow) {
    this.datasetTypeFlow = datasetTypeFlow;
    final List<MetDatasetType> metDatasetTypes = getMetDatasetTypes(datasetTypeFlow);
    if (metDatasetTypes.size() == 1) {
      selectMetDatasetType(metDatasetTypes.get(0));
    } else if (!metDatasetTypes.contains(metDatasetTypeV)) {
      selectMetDatasetType(null);
    }
  }

  @JsMethod
  public List<MetDatasetType> getMetDatasetTypes(final String flow) {
    return DATASET_SELECTION_FLOWS.get(flow);
  }

  @JsMethod
  public void selectMetDatasetType(final MetDatasetType metDatasetType) {
    this.metDatasetTypeV = metDatasetType;
    final ADMSOptions admsOptions = getAdmsOptions();
    admsOptions.setMetDatasetType(metDatasetType);
  }

  @Computed
  public List<String> getMetSiteFlowOptions() {
    return Arrays.asList(MET_SITE_SUGGEST, MET_SITE_SEARCH);
  }

  @JsMethod
  public void handleMetSiteFlowSelection(final String metSiteFlow) {
    this.metSiteFlow = metSiteFlow;
  }

  @Computed("isSuggestFlow")
  public boolean isSuggestFlow() {
    return MET_SITE_SUGGEST.equals(this.metSiteFlow);
  }

  @Computed("isSearchFlow")
  public boolean isSearchFlow() {
    return MET_SITE_SEARCH.equals(this.metSiteFlow);
  }

  @Watch(value = "options", isImmediate = true)
  public void onOptionsChange(final CalculationSetOptions neww) {
    final ADMSOptions admsOptions = getAdmsOptions();
    if (admsOptions == null) {
      return;
    }

    // Initialize cache for all dataset types
    Arrays.stream(MetDatasetType.values())
        .forEach(type -> siteLocationsCache.put(type, getSiteLocationsForType(type)));

    metDatasetTypeV = admsOptions.getMetDatasetType();
    metSiteYearV = Optional.ofNullable(admsOptions.getMetYears())
        .map(v -> v.stream().findFirst().orElse(null))
        .orElse(null);

    // Set the dataset type flow to a correct initial state
    handleDatasetTypeFlowSelection(metDatasetTypeV.isNwp() ? DATASET_TYPE_NWP : DATASET_TYPE_OBS);

    final List<MetSiteWithDistance> suggestedList = TemporaryGeometryUtil.getSuggestedSiteLocations(getSiteLocations(),
        options.getCalculationJobType(),
        composition);

    // Use "suggest" if no option is selected (or the ID does not work for the current dataset type)
    // or if the selected option is one of the suggestions; otherwise, use "search".
    final boolean unknownSiteId = admsOptions.getMetSiteId() == 0
        || getSiteLocations().stream().noneMatch(v -> v.getId() == admsOptions.getMetSiteId());
    handleMetSiteFlowSelection(
        unknownSiteId || suggestedList.stream().anyMatch(v -> v.getSite().getId() == admsOptions.getMetSiteId())
            ? MET_SITE_SUGGEST
            : MET_SITE_SEARCH);
  }

  @Computed
  public List<MetSite> getSiteLocations() {
    return Optional.ofNullable(metDatasetTypeV)
        .map(type -> siteLocationsCache.getOrDefault(type, new ArrayList<>()))
        .orElse(new ArrayList<>());
  }

  private List<MetSite> getSiteLocationsForType(final MetDatasetType metDatasetType) {
    return applicationContext.getConfiguration().getMetSites()
        .stream()
        .filter(v -> v.getDatasets().stream()
            .anyMatch(dataset -> dataset.getDatasetType() == metDatasetType))
        .collect(Collectors.toList());
  }

  @Computed
  public String getMeteoSiteLocation() {
    return String.valueOf(getAdmsOptions().getMetSiteId());
  }

  @Computed
  public String getMeteoSiteYear() {
    final List<String> lst = Optional.ofNullable(getAdmsOptions().getMetYears())
        .orElse(new ArrayList<>());
    if (hasMultipleMeteoYears() && lst.size() > 1) {
      return RECENT_YEARS;
    }

    return lst.isEmpty() ? "" : lst.get(0);
  }

  @Computed("meteoSiteYears")
  protected List<String> getMeteoSiteYears() {
    final ADMSOptions admsOptions = getAdmsOptions();
    final List<String> meteoYears = getSiteLocations().stream()
        .filter(v -> v.getId() == admsOptions.getMetSiteId())
        .findFirst()
        .map(v -> v.getDatasets().stream()
            .filter(dataset -> dataset.getDatasetType() == admsOptions.getMetDatasetType())
            .map(MetSiteDataset::getYear)
            .sorted()
            .collect(Collectors.toList()))
        .orElse(null);

    return meteoYears;
  }

  @Computed("hasMeteoYears")
  public boolean hasMeteoYears() {
    final List<String> meteoSiteYears = getMeteoSiteYears();
    return meteoSiteYears != null && !meteoSiteYears.isEmpty();
  }

  @Computed("hasMultipleMeteoYears")
  public boolean hasMultipleMeteoYears() {
    final List<String> meteoSiteYears = getMeteoSiteYears();
    return meteoSiteYears != null && meteoSiteYears.size() > 1;
  }

  @Watch("getMeteoSiteYears()")
  public void onMeteoSiteYearsChange(final List<String> newYears, final List<String> oldYears) {
    if (newYears == null || !newYears.contains(metSiteYearV)) {
      selectSiteYear(null);
    }
  }

  @JsMethod
  public String getRecentYearsText() {
    final int numberOfYears = Math.min(Math.max(getMeteoSiteYears().size(), MIN_RECENT_YEARS), MAX_RECENT_YEARS);
    return M.messages().calculateMetYearSelectRecents(numberOfYears);
  }

  @JsMethod
  public void selectSiteYear(final String siteYear) {
    if (siteYear == null) {
      selectSingleSiteYear(null);
      return;
    }

    // Catch unknown site year
    if (!getMeteoSiteYears().contains(siteYear) && !RECENT_YEARS.equals(siteYear)) {
      return;
    }

    if (!RECENT_YEARS.equals(siteYear)) {
      selectSingleSiteYear(siteYear);
    } else if (hasMultipleMeteoYears()) {
      selectMultipleSiteYears();
    } else {
      selectSingleSiteYear(null);
    }
  }

  private void selectSingleSiteYear(final String siteYear) {
    metSiteYearV = siteYear == null ? "" : siteYear;
    if (siteYear == null || "".equals(siteYear)) {
      getAdmsOptions().setMetYears(null);
    } else {
      getAdmsOptions().setMetYears(new ArrayList<>(Arrays.asList(siteYear)));
    }
  }

  private void selectMultipleSiteYears() {
    metSiteYearV = RECENT_YEARS;

    final List<String> allYears = getMeteoSiteYears();
    allYears.sort((a, b) -> Integer.compare(Integer.parseInt(b), Integer.parseInt(a)));
    final List<String> recentYears = allYears.stream().limit(MAX_RECENT_YEARS).collect(Collectors.toList());
    getAdmsOptions().setMetYears(recentYears);
  }

  private ADMSOptions getAdmsOptions() {
    return options.getNcaCalculationOptions().getAdmsOptions();
  }

  @SuppressWarnings("rawtypes")
  @Computed("entryKey")
  public Function<Object, Object> entryKey(final Object obj) {
    return v -> String.valueOf(((Entry) v).getKey());
  }

  @SuppressWarnings("rawtypes")
  @Computed("entryValue")
  public Function<Object, String> entryValue(final Object obj) {
    return v -> String.valueOf(((Entry) v).getValue());
  }

  @Computed("isMetDatasetTypeInvalid")
  public boolean isMetDatasetTypeInvalid() {
    return validation.metDatasetType.invalid;
  }

  @Computed("isMetYearInvalid")
  public boolean isMetYearInvalid() {
    return validation.metSiteYear.invalid
        || (!getSiteLocations().stream()
            .anyMatch(v -> v.getId() == getAdmsOptions().getMetSiteId())
            && !RECENT_YEARS.equals(metSiteYearV));
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return isMetDatasetTypeInvalid()
        || isMetYearInvalid();
  }

  @Computed("remarks")
  public String getRemarks() {
    final ADMSOptions admsOptions = getAdmsOptions();
    if (admsOptions.getMetSiteId() == 0 || admsOptions.getMetYears() == null || admsOptions.getMetYears().size() != 1) {
      return M.messages().emptySign();
    }
    final String metYear = admsOptions.getMetYears().get(0);
    return getSiteLocations().stream()
        .filter(v -> v.getId() == admsOptions.getMetSiteId())
        .findFirst()
        .flatMap(v -> v.getDatasets().stream()
            .filter(dataset -> dataset.getDatasetType() == admsOptions.getMetDatasetType())
            .filter(dataset -> metYear.equals(dataset.getYear()))
            .map(MetSiteDataset::getRemarks)
            .filter(x -> x != null)
            .findFirst())
        .orElse(M.messages().emptySign());
  }
}
