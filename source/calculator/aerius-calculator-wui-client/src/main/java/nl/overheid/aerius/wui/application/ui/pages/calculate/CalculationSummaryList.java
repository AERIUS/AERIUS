/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages.calculate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.place.PlaceController;
import nl.aerius.wui.vue.transition.HorizontalCollapse;
import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.wui.application.command.calculation.CalculationCancelCommand;
import nl.overheid.aerius.wui.application.command.calculation.CalculationDeleteCommand;
import nl.overheid.aerius.wui.application.components.button.IconComponent;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationExecutionContext;
import nl.overheid.aerius.wui.application.place.ResultsPlace;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    VerticalCollapseGroup.class,
    HorizontalCollapse.class,
    CollapsiblePanel.class,
    IconComponent.class,
})
public class CalculationSummaryList extends BasicVueComponent implements IsVueComponent {
  @Prop EventBus eventBus;

  @Inject @Data CalculationContext calculationContext;
  @Inject @Data ApplicationContext appContext;

  @Inject PlaceController placeController;

  @Computed
  public List<CalculationExecutionContext> getCalculations() {
    final ArrayList<CalculationExecutionContext> lst = new ArrayList<>(calculationContext.getCalculations());
    Collections.reverse(lst);
    return lst;
  }

  @JsMethod
  public String getJobName(final CalculationExecutionContext calculation) {
    return calculation.getJobContext().getName() + " -  " + calculation.getInternalCalculationId();
  }

  @JsMethod
  public void goToResults() {
    placeController.goTo(new ResultsPlace(appContext.getTheme()));
  }

  @JsMethod
  public void removeCalculation(final CalculationExecutionContext job) {
    eventBus.fireEvent(new CalculationDeleteCommand(job));
  }

  @JsMethod
  public void cancelCalculation(final CalculationExecutionContext job) {
    eventBus.fireEvent(new CalculationCancelCommand(job));
  }

  @JsMethod
  public boolean isInitialized(final CalculationExecutionContext context) {
    return context.getJobKey() != null;
  }

  @JsMethod
  public boolean isNotFinished(final CalculationExecutionContext context) {
    return context.getJobKey() == null || !context.getJobState().isFinalState();
  }

  @JsMethod
  public boolean isActiveCalculation(final CalculationExecutionContext context) {
    return calculationContext.isActiveCalculation(context);
  }

  @JsMethod
  public void setActiveCalculation(final CalculationExecutionContext context) {
    calculationContext.setActiveCalculation(context);
  }

  @JsMethod
  public String getCalculatedHa(final CalculationExecutionContext calc) {
    return Optional.ofNullable(calc.getCalculationInfo())
        .map(v -> v.getJobProgress())
        .map(v -> v.getNumberOfPointsCalculated())
        .orElse(0) + i18n.unitSingularHectares();
  }

  @JsMethod
  public boolean isNotUndefined(final CalculationExecutionContext calc) {
    return calc.getJobState() != JobState.UNDEFINED;
  }

  @JsMethod
  public boolean isCancelState(final CalculationExecutionContext calc) {
    return calc.getJobState() == JobState.CANCELLED;
  }

  @JsMethod
  public boolean isErrorState(final CalculationExecutionContext calc) {
    return calc.getJobState() == JobState.ERROR;
  }

  @JsMethod
  public boolean isFinishedState(final CalculationExecutionContext calc) {
    return calc.isCalculationCompleted();
  }

  @JsMethod
  public boolean isRunning(final CalculationExecutionContext calc) {
    return calc.getJobState() != null && !calc.getJobState().isFinalState();
  }
}
