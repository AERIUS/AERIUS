/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.info.habitat;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.ImaerConstants;
import nl.overheid.aerius.shared.domain.info.HabitatSurfaceType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.application.components.info.BasicInfoRowComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.info.HabitatInfo;
import nl.overheid.aerius.wui.application.domain.result.CriticalLevels;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    BasicInfoRowComponent.class
})
public class HabitatInfoPanel extends BasicVueComponent {
  @Prop @JsProperty HabitatInfo habitatInfo;
  @Prop boolean habitatTypePopupShowing;
  @Prop boolean showAllFields;

  @Inject @Data ApplicationContext applicationContext;

  @JsMethod
  public List<EmissionResultKey> getCriticalLevelKeys() {
    return applicationContext.getConfiguration().getEmissionResultKeys();
  }

  @JsMethod
  public double getRelevantMapped() {
    return habitatInfo.getAdditionalSurface(HabitatSurfaceType.RELEVANT_MAPPED) / ImaerConstants.M2_TO_HA;
  }

  @JsMethod
  public double getRelevantCartographic() {
    return habitatInfo.getAdditionalSurface(HabitatSurfaceType.RELEVANT_CARTOGRAPHIC) / ImaerConstants.M2_TO_HA;
  }

  @JsMethod
  public String getCriticalLevel(final EmissionResultKey criticalLevelKey) {
    final CriticalLevels criticalLevels = habitatInfo.getCriticalLevels();
    if (criticalLevels.hasKey(criticalLevelKey)) {
      final double criticalLevel = criticalLevels.getEmissionResultValue(criticalLevelKey);
      return MessageFormatter.formatCLWithUnit(criticalLevel, criticalLevelKey,
          applicationContext.getConfiguration().getEmissionResultValueDisplaySettings());
    } else {
      return i18n.valueNotApplicable();
    }
  }

}
