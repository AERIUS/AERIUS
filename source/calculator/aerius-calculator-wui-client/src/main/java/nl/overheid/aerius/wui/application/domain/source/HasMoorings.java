/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "*")
public interface HasMoorings {
  @JsOverlay
  default String retrieveMooringAId() {
    if (this instanceof MaritimeShippingESFeature) {
      return ((MaritimeShippingESFeature) this).getMooringAId();
    } else if (this instanceof InlandShippingESFeature) {
      return ((InlandShippingESFeature) this).getMooringAId();
    } else {
      return null;
    }
  }

  @JsOverlay
  default void persistMooringAId(final String mooringAId) {
    if (this instanceof MaritimeShippingESFeature) {
      ((MaritimeShippingESFeature) this).setMooringAId(mooringAId);
    } else if (this instanceof InlandShippingESFeature) {
      ((InlandShippingESFeature) this).setMooringAId(mooringAId);
    }
  }

  @JsOverlay
  default String retrieveMooringBId() {
    if (this instanceof MaritimeShippingESFeature) {
      return ((MaritimeShippingESFeature) this).getMooringBId();
    } else if (this instanceof InlandShippingESFeature) {
      return ((InlandShippingESFeature) this).getMooringBId();
    } else {
      return null;
    }
  }

  @JsOverlay
  default void persistMooringBId(final String mooringBId) {
    if (this instanceof MaritimeShippingESFeature) {
      ((MaritimeShippingESFeature) this).setMooringBId(mooringBId);
    } else if (this instanceof InlandShippingESFeature) {
      ((InlandShippingESFeature) this).setMooringBId(mooringBId);
    }
  }
}
