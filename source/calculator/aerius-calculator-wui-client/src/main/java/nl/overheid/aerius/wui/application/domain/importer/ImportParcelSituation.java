/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.importer;

import static jsinterop.annotations.JsPackage.GLOBAL;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import elemental2.core.JsArray;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.domain.source.TimeVaryingProfile;

/**
 * Data object to transfer a situation within a ImportParcel to the client.
 *
 * Does not contain all properties available (yet).
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class ImportParcelSituation {

  private String id;
  private int year;
  private String name;
  private String type;
  private Object emissionSources;
  private Object buildings;
  private Object definitions;
  private double nettingFactor;

  public final @JsOverlay String getId() {
    return id;
  }

  public final @JsOverlay int getYear() {
    return year;
  }

  public final @JsOverlay String getName() {
    return name;
  }

  public final @JsOverlay SituationType getType() {
    return SituationType.safeValueOf(type);
  }

  /**
   * @return The FeatureCollection of sources
   */
  public final @JsOverlay Object getEmissionSources() {
    return emissionSources;
  }

  /**
   * @return The FeatureCollection of buildings
   */
  public final @JsOverlay Object getBuildings() {
    return buildings;
  }

  public final @JsOverlay Double getNettingFactor() {
    return nettingFactor;
  }

  public final @JsOverlay Object getDefinitions() {
    return definitions;
  }

  public final @JsOverlay void setDefinitions(final Object definitions) {
    this.definitions = definitions;
  }

  public final @JsOverlay List<TimeVaryingProfile> getTimeVaryingProfiles() {
    if (definitions == null) {
      return new ArrayList<>();
    }
    final JsPropertyMap<?> definitionsMap = Js.uncheckedCast(definitions);
    final JsArray<JsPropertyMap<?>> timeVaryingEmissionProfiles = Js.uncheckedCast(definitionsMap.get("customTimeVaryingProfiles"));
    return timeVaryingEmissionProfiles.asList().stream()
        .map(TimeVaryingProfile::fromJSObject).collect(Collectors.toList());
  }
}
