/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class WKTUtilTest {
  @ParameterizedTest
  @MethodSource("wktGeometryDirtyConversionsProviderThreeDecimals")
  void testCleanWktInputThreeDecimals(final String input, final String expected, final String txt) {
    assertEquals(input, WKTUtil.cleanWktInput(input, 3), txt);
    // TODO AER-2361: enable next line and remove previous line when powers that be decide it is time.
    //assertEquals(expected, WKTUtil.cleanWktInput(input, 3), txt);
  }

  @ParameterizedTest
  @MethodSource("wktGeometryDirtyConversionsProviderTwoDecimals")
  void testCleanWktInputTwoDecimals(final String input, final String expected, final String txt) {
    assertEquals(input, WKTUtil.cleanWktInput(input, 2), txt);
    // TODO AER-2361: enable next line and remove previous line when powers that be decide it is time.
    //assertEquals(expected, WKTUtil.cleanWktInput(input, 2), txt);
  }

  static Stream<Arguments> wktGeometryDirtyConversionsProviderThreeDecimals() {
    return Stream.of(
        Arguments.of("POINT(10 10)", "POINT(10 10)", "Unchanged simple point"),
        Arguments.of("POINT(10.121 10.120)", "POINT(10.121 10.12)", "Unchanged simple point"),
        Arguments.of("POINT(10.0 10)", "POINT(10 10)", "Remove .0 from x-coordinate"),
        Arguments.of("POINT(10 10.0)", "POINT(10 10)", "Remove .0 from y-coordinate"),
        Arguments.of("POINT(10.00000 10.00000)", "POINT(10 10)", "Remove trailing zeros"),
        Arguments.of("POINT(10.1 10.1)", "POINT(10.1 10.1)", "Unchanged decimal coordinates"),
        Arguments.of("POINT(10.10 10.1)", "POINT(10.1 10.1)", "Remove trailing zero from x-coordinate"),
        Arguments.of("POINT(10.1 10.10)", "POINT(10.1 10.1)", "Remove trailing zero from y-coordinate"),
        Arguments.of("POINT(10.10000 10.10000)", "POINT(10.1 10.1)", "Remove all trailing zeros"),
        Arguments.of("POINT(10.12345 10.12340)", "POINT(10.123 10.123)", "Fix to 3 decimals"),
        Arguments.of("LINESTRING(10.10 10.0,10.0 10.10)", "LINESTRING(10.1 10,10 10.1)", "Linestring trailing zeros removed"));
  }

  static Stream<Arguments> wktGeometryDirtyConversionsProviderTwoDecimals() {
    return Stream.of(
        Arguments.of("POINT(10.12345 10.12340)", "POINT(10.12 10.12)", "Fix to 2 decimals rounded down"),
        Arguments.of("POINT(10.12345 10.12560)", "POINT(10.12 10.13)", "Fix to 2 decimals rounded up"));
  }
}
