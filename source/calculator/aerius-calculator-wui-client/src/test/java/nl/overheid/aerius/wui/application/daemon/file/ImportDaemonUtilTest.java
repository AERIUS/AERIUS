/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.file;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.withSettings;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.quality.Strictness;

import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.wui.application.components.importer.domain.ImportAction;
import nl.overheid.aerius.wui.application.components.importer.domain.ImportAction.ActionType;
import nl.overheid.aerius.wui.application.components.importer.domain.VirtualSituation;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.application.domain.importer.ImportFilter;
import nl.overheid.aerius.wui.application.domain.importer.ImportFilters;
import nl.overheid.aerius.wui.application.domain.importer.ImportParcel;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;

/**
 *
 */
@ExtendWith(MockitoExtension.class)
class ImportDaemonUtilTest {

  @Mock ApplicationContext applicationContext;
  @Mock ApplicationConfiguration appThemeConfiguration;

  @BeforeEach
  public void setUp() {
    lenient().when(applicationContext.getConfiguration()).thenReturn(appThemeConfiguration);
    lenient().when(appThemeConfiguration.isAllowed(CalculationJobType.MAX_TEMPORARY_EFFECT)).thenReturn(true);
  }

  @Test
  void testSkipImportCalculationJobEmptyMap() {
    assertTrue(ImportDaemonUtil.skipImportCalculationJob(applicationContext, new HashMap<>()), "Skip when supplied map is empty");
  }

  @Test
  void testSkipImportCalculationJobValid() {
    final Map<FileUploadStatus, ImportParcel> mapToTest = new HashMap<>();

    mapToTest.put(mockValidStatus(), mockValidParcel());
    mapToTest.put(mockValidStatus(), mockValidParcel());

    assertFalse(ImportDaemonUtil.skipImportCalculationJob(applicationContext, mapToTest), "Don't skip valid case");
  }

  @Test
  void testSkipImportCalculationJobInvalidFilter() {
    final Map<FileUploadStatus, ImportParcel> mapToTest = new HashMap<>();

    final FileUploadStatus status = mockValidStatus();
    final ImportFilters filters = mock(ImportFilters.class);
    when(filters.isDisallowed(ImportFilter.CALCULATION_JOB)).thenReturn(true);
    when(status.getFilters()).thenReturn(filters);

    mapToTest.put(mockValidStatus(), mockValidParcel());
    mapToTest.put(status, mockValidParcel());

    assertTrue(ImportDaemonUtil.skipImportCalculationJob(applicationContext, mapToTest), "Skip when importing calculation job is filtered out");
  }

  @Test
  void testSkipImportCalculationJobSomeInvalidActions() {
    final Map<FileUploadStatus, ImportParcel> mapToTest = new HashMap<>();

    final FileUploadStatus status2 = mockValidStatus();
    final ImportAction importAction2 = mock(ImportAction.class);
    when(importAction2.getType()).thenReturn(ActionType.ADD_CALCULATION_POINTS);
    when(status2.getImportAction()).thenReturn(importAction2);

    mapToTest.put(mockValidStatus(), mockValidParcel());
    mapToTest.put(status2, mockValidParcel());

    assertFalse(ImportDaemonUtil.skipImportCalculationJob(applicationContext, mapToTest), "Don't skip when at least 1 import action is correct");
  }

  @Test
  void testSkipImportCalculationJobInvalidActions() {
    final Map<FileUploadStatus, ImportParcel> mapToTest = new HashMap<>();

    final FileUploadStatus status1 = mockValidStatus();
    final ImportAction importAction1 = mock(ImportAction.class);
    when(importAction1.getType()).thenReturn(ActionType.ADD_CALCULATION_POINTS);
    when(status1.getImportAction()).thenReturn(importAction1);

    final FileUploadStatus status2 = mockValidStatus();
    final ImportAction importAction2 = mock(ImportAction.class);
    when(importAction2.getType()).thenReturn(ActionType.ADD_CALCULATION_POINTS);
    when(status2.getImportAction()).thenReturn(importAction2);

    mapToTest.put(status1, mockValidParcel());
    mapToTest.put(status2, mockValidParcel());

    assertTrue(ImportDaemonUtil.skipImportCalculationJob(applicationContext, mapToTest), "Skip when all import action are incorrect");
  }

  @Test
  void testSkipImportCalculationJobWithoutSomeOptions() {
    final Map<FileUploadStatus, ImportParcel> mapToTest = new HashMap<>();

    final ImportParcel parcel = mockValidParcel();
    when(parcel.getCalculationSetOptions()).thenReturn(null);

    mapToTest.put(mockValidStatus(), mockValidParcel());
    mapToTest.put(mockValidStatus(), parcel);

    assertFalse(ImportDaemonUtil.skipImportCalculationJob(applicationContext, mapToTest), "Don't skip when some options are null");
  }

  @Test
  void testSkipImportCalculationJobWithoutAnyOptions() {
    final Map<FileUploadStatus, ImportParcel> mapToTest = new HashMap<>();

    final ImportParcel parcel1 = mockValidParcel();
    when(parcel1.getCalculationSetOptions()).thenReturn(null);

    final ImportParcel parcel2 = mockValidParcel();
    when(parcel2.getCalculationSetOptions()).thenReturn(null);

    mapToTest.put(mockValidStatus(), parcel1);
    mapToTest.put(mockValidStatus(), parcel2);

    assertTrue(ImportDaemonUtil.skipImportCalculationJob(applicationContext, mapToTest), "Skip when all options are null");
  }

  @Test
  void testSkipImportCalculationJobDisallowedJobType() {
    final Map<FileUploadStatus, ImportParcel> mapToTest = new HashMap<>();

    final FileUploadStatus status = mockValidStatus();
    when(status.getCalculationJobType()).thenReturn(CalculationJobType.DEPOSITION_SUM);

    mapToTest.put(mockValidStatus(), mockValidParcel());
    mapToTest.put(status, mockValidParcel());

    assertTrue(ImportDaemonUtil.skipImportCalculationJob(applicationContext, mapToTest), "Skip when job type isn't allowed");
  }

  private FileUploadStatus mockValidStatus() {
    final FileUploadStatus status = mock(FileUploadStatus.class, withSettings().strictness(Strictness.LENIENT));
    when(status.getFileCode()).thenReturn("SomeFileCode");
    final ImportFilters filters = mock(ImportFilters.class, withSettings().strictness(Strictness.LENIENT));
    when(filters.isDisallowed(ImportFilter.CALCULATION_JOB)).thenReturn(false);
    when(status.getFilters()).thenReturn(filters);
    final ImportAction importAction = mock(ImportAction.class, withSettings().strictness(Strictness.LENIENT));
    when(importAction.getType()).thenReturn(ActionType.ADD_TO_VIRTUAL_SITUATION);
    final VirtualSituation virtualSituation = mock(VirtualSituation.class, withSettings().strictness(Strictness.LENIENT));
    when(importAction.getVirtualSituation()).thenReturn(virtualSituation);
    when(virtualSituation.getFileHandle()).thenReturn(status);
    when(status.getImportAction()).thenReturn(importAction);
    when(status.getCalculationJobType()).thenReturn(CalculationJobType.MAX_TEMPORARY_EFFECT);
    return status;
  }

  private ImportParcel mockValidParcel() {
    final ImportParcel parcel = mock(ImportParcel.class, withSettings().strictness(Strictness.LENIENT));
    final CalculationSetOptions options = mock(CalculationSetOptions.class, withSettings().strictness(Strictness.LENIENT));
    when(parcel.getCalculationSetOptions()).thenReturn(options);
    return parcel;
  }

}
