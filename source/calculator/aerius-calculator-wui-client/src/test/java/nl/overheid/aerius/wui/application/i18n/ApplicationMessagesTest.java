/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.i18n;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Tests if the {@link ApplicationMessages} interface is in sync with the language specific properties files,
 * and if the properties files are in sync.
 */
class ApplicationMessagesTest {

  // Ignore these enum types as not all are relevant, deprecated or not yet supported.
  // This list is for keys that have lots of exceptions to missing keys. Preferable this kind of filtering should be handled with the
  // {#IGNORED_INCOMPLETE_KEYS}.
  private static final Set<Class<?>> IGNORED_ENUMS = Set.of(Theme.class, EmissionResultKey.class);

  // Ignore these keys to report on missing keys values, because they
  private static final Set<String> IGNORED_INCOMPLETE_KEYS = Set.of(
      // Only 2 custom pdfDisclaimer, others falls back to generic disclaimer. Would have reported 4 keys as missing
      "pdfDisclaimer",
      // Only COMBINATION_* are relevant for this key. If not filtered would have reported 5 keys as missing.
      "situationTypeInCombinationBase");
  // pdfDisclaimer 5 missing keys ignored,
  // situationTypeInCombinationBase 5 missing keys ignored.
  private static final int NUMBER_OF_IGNORED_INCOMPLETE_KEYS = 10;

  // Fuzzy matcher that matches method names. Assumes character before method name is not a char,
  // and character after method is not a char, number or underscore.
  private static final Pattern METHOD_NAME_MATCHER = Pattern.compile("[^\\W][\\w\\d_]+[^\\W\\d]+");

  /**
   * Tests if all enums are present as key in the properties file.
   */
  @Test
  void testMissingEnumKeysFromInterface() throws IOException, URISyntaxException {
    final Map<String, Method> methods = getMethods();
    final Set<String> props = getLanguageProperties("");
    final List<String> missingKeys = new ArrayList<>();

    for (final Entry<String, Method> entry : methods.entrySet()) {
      final List<Class<?>> enumParams = Stream.of(entry.getValue().getParameterTypes()).filter(Class::isEnum).collect(Collectors.toList());
      if (!ignoreExceptions(enumParams)) {
        for (final String enumKey : combinations(List.of(), enumParams, 0)) {
          final String methodEnum = entry.getKey() + '[' + enumKey + ']';

          if (!props.contains(methodEnum)) {
            missingKeys.add(methodEnum);
          }
        }
      }
    }
    filterIgnoredKeys(missingKeys);
    print("Missing Keys", missingKeys);
    assertEquals(List.of(), missingKeys, "ApplicationMessages.properties misses enum values.");
  }

  private static void filterIgnoredKeys(final List<String> missingKeys) {
    final List<String> removeKeys =
        IGNORED_INCOMPLETE_KEYS.stream().flatMap(iik -> missingKeys.stream().filter(k -> k.startsWith(iik))).collect(Collectors.toList());

    assertEquals(NUMBER_OF_IGNORED_INCOMPLETE_KEYS, removeKeys.size(),
        "There are more missing keys filtered as expected. Check if it shouldn't have added these keys or if the number of filtered must be adapted");
    removeKeys.forEach(missingKeys::remove);
  }

  /**
   * Tests if all methods in the interface are actually used in java or html files.
   */
  @Test
  void testIfKeysFromInterfaceAreUsed() throws IOException, URISyntaxException {
    final Map<String, Method> methods = getMethods();
    final Map<String, AtomicInteger> counter = new HashMap<>();
    final Path root = Path.of(new File(getClass().getResource("/").toURI()).getParentFile().getParentFile().getAbsolutePath(),
        "src", "main", "java");

    Files.walkFileTree(root, new SimpleFileVisitor<>() {
      @Override
      public FileVisitResult visitFile(final Path arg0, final BasicFileAttributes arg1) throws IOException {
        final String name = arg0.toFile().getName();
        if (name.endsWith("java") || name.endsWith("html")) {
          final String content = new String(Files.readAllBytes(arg0), StandardCharsets.UTF_8);

          methods.entrySet().stream().filter(e -> matchMethodName(content, e.getKey())).forEach(
              entry -> counter.computeIfAbsent(entry.getKey(), k -> new AtomicInteger()).incrementAndGet());
        }
        return super.visitFile(arg0, arg1);
      }
    });
    final List<String> unusedKeys = counter.entrySet().stream().filter(e -> e.getValue().get() < 2).map(e -> e.getKey()).collect(Collectors.toList());

    print("Unused Keys", unusedKeys);
    assertEquals(List.of(), unusedKeys, "There are " + unusedKeys.size() + " keys that are not used in code.");
  }

  /**
   * Checks if the methodName is in the content. Uses pattern matching to check for the exact method name, and not method names that overlap.

   * @param content content to check
   * @param methodName method name to find
   * @return true if method is found in the content
   */
  private boolean matchMethodName(final String content, final String methodName) {
    int index;
    String nextContent = content;
    while ((index = nextContent.indexOf(methodName)) > 0) {
      final Matcher matcher = METHOD_NAME_MATCHER.matcher(nextContent.substring(index - 2, index + methodName.length() + 1));

      if (matcher.find()) {
        if (matcher.group().equals(methodName)) {
          return true;
        }
      }
      nextContent = nextContent.substring(index + methodName.length());
    }
    return false;
  }

  /**
   * Test if there are keys in the default properties file that are not present in the {@link ApplicationMessages} interface.
   */
  @Test
  void testObsoleteProperties() throws IOException, URISyntaxException {
    final Map<String, Method> methods = getMethods();
    final Set<String> props = getLanguageProperties("");
    final List<String> obsoleteKeys = new ArrayList<>();

    for (final String key : props) {
      final int indexOf = key.indexOf('[');
      final String actualKey = indexOf > 0 ? key.substring(0, indexOf) : key;

      if (!methods.containsKey(actualKey)) {
        obsoleteKeys.add(actualKey);
      }
    }
    print("Obsolete Keys", obsoleteKeys);
    assertEquals(List.of(), obsoleteKeys, "ApplicationMessages.properties contains obsolete keys that are not used anymore.");
  }

  /**
   * Test if there are methods on the {@link ApplicationMessages} interface that are not present in the properties file.
   */
  @Test
  void testObsoleteMethods() throws IOException, URISyntaxException {
    final Map<String, Method> methods = getMethods();
    final Set<String> props = getLanguageProperties("");
    final List<String> obsoleteKeys = new ArrayList<>();

    for (final Entry<String, Method> method : methods.entrySet()) {
      if (!props.contains(method.getKey())) {
        obsoleteKeys.add(method.getKey());
      }
    }
    print("Obsolete methods", obsoleteKeys);
    assertEquals(List.of(), obsoleteKeys, "In Java there are methods that don't have a key in the properties file.");
  }

  @ParameterizedTest
  @ValueSource(strings = {"en"})
  void testLanguagesMissingKeys(final String language) throws IOException, URISyntaxException {
    final Set<String> defaultProps = getLanguageProperties("");
    final Set<String> langProps = getLanguageProperties(language);
    assertLanguageKeys(defaultProps, langProps, language, "language " + language + " misses keys present in the default language file.");
  }

  @ParameterizedTest
  @ValueSource(strings = {"en"})
  void testLanguagesObsoleteKeys(final String language) throws IOException, URISyntaxException {
    final Set<String> langProps = getLanguageProperties(language);
    final Set<String> defaultProps = getLanguageProperties("");
    assertLanguageKeys(langProps, defaultProps, language, "language " + language + " contains obsolete keys that are not used anymore.");
  }

  @ParameterizedTest
  @ValueSource(strings = {"", "en"})
  void testSorting(final String language) throws IOException, URISyntaxException {
    final List<String> content = readLanguageProperties(language);
    final List<String> sorted = content.stream().sorted().collect(Collectors.toList());
    final List<String> misMatches = new ArrayList<>();
    final List<String> actuals = new ArrayList<>();
    final List<String> expecteds = new ArrayList<>();

    for (int i = 0; i < sorted.size(); i++) {
      final String expected = sorted.get(i);
      final String actual = content.get(i);

      if (!expected.equals(actual)) {
        misMatches.add("Expected: " + keyMap(expected) + ", found: " + keyMap(actual));
        if (misMatches.size() > 10) {
          expecteds.add("...");
          actuals.add("...");
          // If we have more than a number of mismatches we can break here, to avoid getting a very large list that is probably not helpfull.
          break;
        } else {
          expecteds.add(expected);
          actuals.add(actual);
        }
      }
    }
    if (!misMatches.isEmpty()) {
      print("Sorting mismatch for language '" + language + "'", misMatches);
    }
    assertEquals(String.join(", ", expecteds), String.join(", ", actuals), "Language property " + language + " file should be sorted.");
  }

  private static boolean ignoreExceptions(final List<Class<?>> enumParams) {
    return enumParams.stream().anyMatch(e -> IGNORED_ENUMS.contains(e));
  }

  private static List<String> combinations(final List<String> enums, final List<Class<?>> enumParameters, final int idx) {
    final boolean last = idx == enumParameters.size();
    if (last) {
      return enums;
    }
    final Class<?> enumParam = enumParameters.get(idx);
    final List<String> newEnumKeys = new ArrayList<>();

    for (final Object enumValue : enumParam.getEnumConstants()) {
      final String name = ((Enum<?>) enumValue).name();

      if (enums.isEmpty()) {
        newEnumKeys.add(name);
      } else {
        for (final String enumKey : enums) {
          newEnumKeys.add(enumKey + '|' + name);
        }
      }
    }
    return combinations(newEnumKeys, enumParameters, idx + 1);
  }

  private static void assertLanguageKeys(final Set<String> langProps, final Set<String> defaultProps,
      final String language, final String message) {
    final List<String> missingKeys = new ArrayList<>();

    for (final String propKey : langProps) {
      if (!defaultProps.contains(propKey)) {
        missingKeys.add(propKey);
      }
    }
    print("Missing Keys for " + language, missingKeys);
    assertEquals(List.of(), missingKeys, "ApplicationMessages_" + language + ".properties " + message);
  }

  private static Map<String, Method> getMethods() {
    return Stream.of(ApplicationMessages.class.getMethods()).collect(Collectors.toMap(m -> m.getName(), Function.identity()));
  }

  private static Set<String> getLanguageProperties(final String language) throws IOException, URISyntaxException {
    final List<String> content = readLanguageProperties(language);

    return content.stream().filter(s -> !s.trim().isBlank()).map(ApplicationMessagesTest::keyMap).distinct().collect(Collectors.toSet());
  }

  private static List<String> readLanguageProperties(final String language) throws IOException, URISyntaxException {
    final String fileName = ApplicationMessages.class.getSimpleName() + (language.isEmpty() ? "" : "_" + language) + ".properties";
    final List<String> content = Files.readAllLines(Path.of(ApplicationMessagesTest.class.getResource(fileName).toURI()));
    return content;
  }

  private static String keyMap(final String s) {
    final int optionsIndex = s.indexOf(']'); // if blocks used in key, it can contain an =, and therefore split on = won't work.
    final int equalsIndex = s.indexOf("=");
    return equalsIndex < 0 ? s : s.substring(0, optionsIndex > 0 && optionsIndex < equalsIndex ? optionsIndex + 1 : s.indexOf('=')).trim();
  }

  private static void print(final String title, final List<String> keys) {
    if (keys.isEmpty()) {
      return;
    }
    final String sep = "-".repeat(10);
    System.out.println(sep + title + sep);
    keys.stream().sorted().forEach(System.out::println);
  }
}
