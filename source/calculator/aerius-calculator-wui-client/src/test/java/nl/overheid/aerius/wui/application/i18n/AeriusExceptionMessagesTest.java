/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.i18n;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.exception.ImaerExceptionReason;
import nl.overheid.aerius.shared.i18n.AeriusExceptionMessages;

/**
 * Test class for interface {@link AeriusExceptionMessages}.
 */
class AeriusExceptionMessagesTest {

  /**
   * Test to check if the reasons from {@link nl.overheid.aerius.shared.exception.AeriusException} has methods on the
   * interface of {@link AeriusExceptionMessages}
   */
  @Test
  void testReasonsMatch() {
    final List<String> missingMethods = new ArrayList<>();
    final Set<String> allReasonErrorCodes = allReasonErrorCodes();
    int countMatches = 0;

    for (final Method method : AeriusExceptionMessages.class.getDeclaredMethods()) {
      final String methodName = method.getName().substring(1);

      if (allReasonErrorCodes.contains(methodName)) {
        countMatches++;
      } else {
        missingMethods.add(methodName);
        System.out.println(methodName);
      }
    }
    assertTrue(countMatches > 0, "Safety check we have at least found anything");
    assertTrue(missingMethods.isEmpty(),
        "Interface methods for errors codes: " + String.join(",", missingMethods) + " not present in "
            + AeriusExceptionReason.class.getCanonicalName());
  }

  /**
   * Test to check if the reasons from {@link nl.overheid.aerius.shared.exception.AeriusException} has methods on the
   * interface of {@link AeriusExceptionMessages}
   */
  @Test
  void testMethodMatch() {
    final Set<String> methodNames = Stream.of(AeriusExceptionMessages.class.getDeclaredMethods())
        .map(m -> m.getName().substring(1))
        .collect(Collectors.toSet());

    for (final String reasonErrorCode : allReasonErrorCodes()) {
      assertTrue(methodNames.contains(reasonErrorCode),
          "Reason " + reasonErrorCode + " not present in interface " + AeriusExceptionMessages.class.getCanonicalName());
    }
  }

  /**
   * Test to check if the reasons have unique error codes.
   */
  @Test
  void testReasonsUnique() {
    final Map<Integer, List<Reason>> groupedByErrorCode =
        Stream.concat(Stream.of(ImaerExceptionReason.values()), Stream.of(AeriusExceptionReason.values()))
            .collect(Collectors.groupingBy(x -> Integer.valueOf(x.getErrorCode())));
    groupedByErrorCode.entrySet().stream()
        // INTERNAL_ERROR is specified in both ImaerExceptionReason and AeriusExceptionReason, ignore for this test
        .filter(entry -> entry.getKey() != ImaerExceptionReason.INTERNAL_ERROR.getErrorCode())
        .forEach(entry -> assertEquals(1, entry.getValue().size(),
            "reason code had multiple reasons: " + entry.getKey() + ". Reasons: " + entry.getValue()));
  }

  private static Set<String> allReasonErrorCodes() {
    return Stream.concat(Stream.of(ImaerExceptionReason.values()), Stream.of(AeriusExceptionReason.values()))
        .map(r -> Integer.toString(r.getErrorCode()))
        .collect(Collectors.toSet());
  }

}
