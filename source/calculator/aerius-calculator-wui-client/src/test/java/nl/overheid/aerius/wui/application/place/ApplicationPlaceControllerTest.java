/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.place;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.shared.domain.ProductProfile;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;

/**
 *
 */
@ExtendWith(MockitoExtension.class)
class ApplicationPlaceControllerTest {

  @Mock ApplicationContext applicationContext;
  @Mock ScenarioContext scenarioContext;

  @InjectMocks ApplicationPlaceController controller;

  @Test
  void testIsPermanentlyDisabled() {
    assertFalse(controller.isPermanentlyDisabled(ApplicationPlaceControllerTest.class), "Unknown class shouldn't be disabled");
    assertFalse(controller.isPermanentlyDisabled(CalculatePlace.class), "Calculate place shouldn't be disabled by default");
    assertFalse(controller.isPermanentlyDisabled(CalculationPointsPlace.class), "Calculation points place shouldn't be disabled by default");

    when(applicationContext.getProductProfile()).thenReturn(ProductProfile.LBV_POLICY);

    assertTrue(controller.isPermanentlyDisabled(CalculationPointsPlace.class), "Calculation points place should be disabled for LBV");
  }

  @Test
  void testIsEnabled() {
    assertTrue(controller.isEnabled(ApplicationPlaceControllerTest.class), "Unknown class should be enabled (no conditions implemented)");
    assertFalse(controller.isEnabled(CalculatePlace.class), "Calculate place shouldn't be enabled by default");
    assertFalse(controller.isEnabled(CalculationPointsPlace.class), "Calculation points place shouldn't be enabled by default");

    when(scenarioContext.hasSituations()).thenReturn(true);

    assertTrue(controller.isEnabled(CalculatePlace.class), "Calculate place should be enabled when there are situations");
    assertTrue(controller.isEnabled(CalculationPointsPlace.class), "Calculation points place should be enabled when there are situations");

    when(applicationContext.getProductProfile()).thenReturn(ProductProfile.LBV_POLICY);

    assertTrue(controller.isEnabled(CalculatePlace.class), "Calculate place should be enabled when there are situations");
    assertFalse(controller.isEnabled(CalculationPointsPlace.class), "Calculation points place shouldn't be enabled when it is permanently disabled");
  }

}
