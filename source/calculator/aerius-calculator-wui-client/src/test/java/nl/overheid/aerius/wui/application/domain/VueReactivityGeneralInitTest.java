/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain;

import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.reflections.Reflections;
import org.reflections.scanners.Scanners;

import com.github.javaparser.ParserConfiguration;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.Parameter;
import com.github.javaparser.ast.type.Type;
import com.github.javaparser.resolution.types.ResolvedType;
import com.github.javaparser.symbolsolver.JavaSymbolSolver;
import com.github.javaparser.symbolsolver.model.resolution.TypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.ClassLoaderTypeSolver;
import com.github.javaparser.utils.CodeGenerationUtils;
import com.github.javaparser.utils.SourceRoot;
import com.machinezoo.noexception.Exceptions;

import nl.overheid.aerius.wui.application.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.application.domain.importer.ScenarioMetaData;
import nl.overheid.aerius.wui.application.domain.source.AbstractSubSource;
import nl.overheid.aerius.wui.application.domain.source.TimeVaryingProfile;
import nl.overheid.aerius.wui.application.domain.source.SourceCharacteristics;
import nl.overheid.aerius.wui.vue.ReactivityUtil;
import nl.overheid.aerius.wui.vue.SkipReactivityValidations;

/**
 * <p>
 * This test tries to confirm that a @JsType class initializes its fields such
 * that they will be reactive when fed into a vue.js component.
 *
 * <p>
 * The reason this test is necessary is because when vue.js goes to make a
 * JSObject reactive, it will iterate over all fields in that object and add
 * observers to them. For this to work, all fields must be initialized with some
 * value, even if that value is null. Due to the way GWT and JavaScript conjuct,
 * a JSObject can in practice have undefined`fields (i.e. these are non-existing
 * fields, which can not be 'found'/iterated over) while in the Java
 * understanding of such an object they would exist and be `null`.
 *
 * <p>
 * Failure to correctly initialize field values results in discrepant behaviour
 * depending on circumstances, which leads to bugs that are routinely discovered
 * at too late a stage in the development process.
 *
 * <p>
 * Usually, this is easy to resolve by simply initializing field values to an
 * explicit null and forget about it. However in cases where an object is
 * a @JsType, and is wrapping a JSObject directly, these initializations are
 * easily skipped or neglected. This test aims to rehabilitate those specific
 * class types, so as to make them 'rigid' as it were, squelching future bugs.
 *
 * <p>
 * This test suite enforces the following pattern, for eligible classes:
 * <ul>
 * <li>Class must have a public static create method, or be abstract
 * <li>Class must have an init method (a method that starts with init*)
 * <li>The init method must have a parameter with type of the class being tested
 * <li>If there are multiple create or init methods, the one(s) not used in the
 * pattern enforced by this unit test must be annotated with the below
 * annotation, otherwise only the first occurrence will be tested
 * <li>The init method must call the init method on their parent
 * class
 * <li>All public getters must have their setters set in this init method
 * <li>Ideally, existing values if any are not overridden, however this is not
 * validated for as of yet. Convenience methods exist in {@link ReactivityUtil}
 * to canonicalize this practice.
 * <li>All return types of getters are also validated
 * <li>Future art: the setter call must in some way be guarded by an if
 * statement
 * </ul>
 *
 * <p>
 * Unfortunately, we can't be helped by an interface here because generalized
 * methods such as getters must be a @JsOverlay for @JsType's, and those cannot
 * have overrides. As a result, implementation for the entire inheritance tree
 * would need to be implemented in the interface's default method, which is
 * impracticable.
 *
 * <p>
 * If, for whatever reason, a class or create/init method, or getter method must
 * be skipped, the class or the method can be annotated
 * with @SkipReactivityValidations
 *
 * <p>
 * The implementation of getters and setters is not scrutinized and could be
 * arbitrarily complex, but it is encouraged they be sane.
 *
 * <p>
 * Code example of a valid pattern:
 *
 * <pre>
 * public class SRM2RoadESFeature extends RoadESFeature {
 *   public static @JsOverlay SRM2RoadESFeature create() {
 *     final SRM2RoadESFeature feature = new SRM2RoadESFeature();
 *     init(feature);
 *     return feature;
 *   }
 *
 *   public static @JsOverlay void init(final SRM2RoadESFeature feature) {
 *     RoadESFeature.initRoadFeature(feature, null, EmissionSourceType.SRM2_ROAD);
 *     ReactivityUtil.ensureDefault(feature::getElevation, feature::setElevation, RoadElevation.NORMAL);
 *     ReactivityUtil.ensureDefault(feature::getElevationHeight, feature::setElevationHeight, 0);
 *     ReactivityUtil.ensureJsProperty(feature, "porosity", feature::setPorosity, 0D);
 *     ReactivityUtil.ensureDefault(feature::getBarrierLeft, feature::setBarrierLeft);
 *     ReactivityUtil.ensureInitialized(feature::getBarrierLeft, SRM2RoadSideBarrier::init);
 *     ReactivityUtil.ensureDefault(feature::getBarrierRight, feature::setBarrierRight);
 *     ReactivityUtil.ensureInitialized(feature::getBarrierRight, SRM2RoadSideBarrier::init);
 *     ReactivityUtil.ensureJsProperty(feature, "partialChanges", feature::setPartialChanges, new JsArray<SRM2LinearReference>());
 *   }
 * }
 * </pre>
 */
public class VueReactivityGeneralInitTest {
  private static final String SRC_MAIN_JAVA = "src/main/java";
  private static final String PACKAGE_BASE = "nl.overheid.aerius.wui";

  private static Reflections reflections;
  private static SourceRoot sourceRoot;

  @BeforeAll
  static void setup() {
    final TypeSolver solver = new ClassLoaderTypeSolver(VueReactivityGeneralInitTest.class.getClassLoader());
    final ParserConfiguration parserConfig = new ParserConfiguration()
        .setSymbolResolver(new JavaSymbolSolver(solver));
    sourceRoot = new SourceRoot(CodeGenerationUtils.mavenModuleRoot(VueReactivityGeneralInitTest.class)
        .resolve(SRC_MAIN_JAVA), parserConfig);

    reflections = new Reflections(PACKAGE_BASE);
  }

  @TestFactory
  Collection<DynamicTest> testAbstractSubSource() {
    return testHierarchy(AbstractSubSource.class);
  }

  @TestFactory
  Collection<DynamicTest> testTimeVaryingProfile() {
    return testHierarchy(TimeVaryingProfile.class);
  }

  @TestFactory
  Collection<DynamicTest> testScenarioMetaData() {
    return testHierarchy(ScenarioMetaData.class);
  }

  @TestFactory
  Collection<DynamicTest> testCalculationSetOptions() {
    return testHierarchy(CalculationSetOptions.class);
  }

  @TestFactory
  Collection<DynamicTest> testSourceCharacteristics() {
    return testHierarchy(SourceCharacteristics.class);
  }

  @TestFactory
  Collection<DynamicTest> testImaerFeatures() {
    return testHierarchy(ImaerFeature.class);
  }

  Collection<DynamicTest> testHierarchy(final Class<?> clss) {
    final Set<Class<?>> subTypes = reflections.get(Scanners.SubTypes.of(clss).asClass());

    return Stream.concat(Stream.of(clss), subTypes.stream())
        .filter(v -> v.getAnnotation(SkipReactivityValidations.class) == null)
        .map(v -> explodeInner(v, SRC_MAIN_JAVA))
        .flatMap(Collection::stream)
        .distinct()
        .map(v -> dynamicTestOf(v, SRC_MAIN_JAVA))
        .sorted((o1, o2) -> o1.getDisplayName().compareTo(o2.getDisplayName()))
        .collect(Collectors.toList());
  }

  private Collection<Class<?>> explodeInner(final Class<?> clss, final String path) {
    final CompilationUnit cu = sourceRoot.parse(clss.getPackageName(), clss.getSimpleName() + ".java");
    final List<MethodDeclaration> getterMethods = cu.findAll(MethodDeclaration.class, VueReactivityGeneralInitTest::isValidatableGetterMethod);

    // Explode the getters and add them to the list of class types to test
    final Set<Class<?>> exploded = getterMethods.stream()
        .map(v -> toValidatableClass(v.getType()))
        .filter(Optional::isPresent)
        .map(Optional::get)
        .collect(Collectors.toSet());

    // Recurse on the result - this may cause a stackoverflow if there is a
    // cycle in object references, in which case the developer at this point
    // will have to annotate one or the other with @SkipReactivityValidations
    final Stream<Class<?>> explodedInner = exploded.stream()
        .map(v -> explodeInner(v, path))
        .flatMap(Collection::stream);

    // Add them all to a pile and return
    return Stream.concat(Stream.of(clss), Stream.concat(exploded.stream(), explodedInner))
        .collect(Collectors.toSet());
  }

  private DynamicTest dynamicTestOf(final Class<?> v, final String path) {
    return DynamicTest.dynamicTest("Type: " + v.getSimpleName(), () -> testClass(v, path));
  }

  private void testClass(final Class<?> clss, final String path) {
    final CompilationUnit cu = sourceRoot.parse(clss.getPackageName(), clss.getSimpleName() + ".java");

    testCompilationUnit(clss, cu);
  }

  private void testCompilationUnit(final Class<?> clss, final CompilationUnit cu) {
    final Optional<MethodDeclaration> createMethod = cu.findFirst(MethodDeclaration.class, VueReactivityGeneralInitTest::isTestableCreateMethod);
    final Optional<MethodDeclaration> initMethodOpt = cu.findFirst(MethodDeclaration.class, VueReactivityGeneralInitTest::isTestableInitMethod);

    final List<MethodDeclaration> getterMethods = cu.findAll(MethodDeclaration.class, VueReactivityGeneralInitTest::isValidatableGetterMethod);

    Assertions.assertTrue(Modifier.isAbstract(clss.getModifiers()) || !createMethod.isEmpty(),
        "Class type " + clss.getSimpleName() + " should include a public static create, or be abstract");

    Assertions.assertFalse(initMethodOpt.isEmpty(), "Class type " + clss.getSimpleName() + " does not include an init method");

    final MethodDeclaration initMethod = initMethodOpt.get();
    final String initBody = initMethod.getBody().get().toString();
    final Optional<Parameter> initObjParamOpt = initMethod.getParameters().stream()
        .filter(p -> p.getType().toString().replaceAll("<.*>", "").equals(clss.getSimpleName()))
        .findFirst();

    Assertions.assertTrue(initObjParamOpt.isPresent(), "A parameter with type " + clss.getSimpleName() + " must be passed to the init method");

    final String initObjParamName = initObjParamOpt.get().getNameAsString();
    testParentInit(clss, initBody, initObjParamName);
    testGetterMethodDefaults(getterMethods, initBody, initObjParamName);
    testGetterMethodInits(getterMethods, initBody);
  }

  private void testParentInit(final Class<?> clss, final String initBody, final String initObjParamName) {
    if (!clss.getSuperclass().getName().startsWith(PACKAGE_BASE)) {
      return;
    }

    final String superStatementRegex = String.format("%s\\.init(.*)\\((.*)%s(.*)\\);", clss.getSuperclass().getSimpleName(), initObjParamName);
    final boolean containsPattern = Pattern.compile(superStatementRegex)
        .matcher(initBody)
        .find();
    Assertions.assertTrue(containsPattern,
        "Text matching " + superStatementRegex + " should be found in the init body of " + clss.getSimpleName());
  }

  private void testGetterMethodDefaults(final List<MethodDeclaration> getters, final String body, final String initObjParamName) {
    // First, quickly validate the default is set
    getters.stream()
        .map(MethodDeclaration::getNameAsString)
        .forEach(getter -> {
          final String setterName = toSetterName(getter);
          final String setterOptionA = initObjParamName + "::" + setterName + ",";
          final String setterOptionB = initObjParamName + "::" + setterName + ")";
          final String setterOptionC = initObjParamName + "." + setterName + "(";

          Assertions.assertTrue(body.contains(setterOptionA)
              || body.contains(setterOptionB)
              || body.contains(setterOptionC),
              "Getter \"" + getter + "\" must have its setter set through"
                  + "[" + setterOptionA + "] or "
                  + "[" + setterOptionB + "] or "
                  + "[" + setterOptionC + "]");
        });
  }

  private void testGetterMethodInits(final List<MethodDeclaration> getters, final String body) {
    // Iterate again to validate if each value is initialized
    getters.stream()
        .map(v -> toValidatableClass(v.getType()))
        .filter(Optional::isPresent)
        .map(Optional::get)
        .forEach(clss -> {
          final String initOptionA = clss.getSimpleName() + "::init(.*)";
          final String initOptionB = clss.getSimpleName() + ".init(.*)\\((.*)%s(.*)\\)";

          final boolean containsPatternA = Pattern.compile(initOptionA)
              .matcher(body)
              .find();
          final boolean containsPatternB = Pattern.compile(initOptionB)
              .matcher(body)
              .find();
          Assertions.assertTrue(containsPatternA || containsPatternB,
              String.format("Complex object of type " + clss.getSimpleName() + " must be initialized using pattern %s or %s", initOptionA,
                  initOptionB));
        });

  }

  private String toSetterName(final String getterName) {
    if (getterName.startsWith("get")) {
      return getterName.replaceFirst("get", "set");
    } else {
      return getterName.replaceFirst("is", "set");
    }
  }

  /**
   * Any public static create method not annotated by SkipReactivityValidations
   */
  private static boolean isTestableCreateMethod(final MethodDeclaration method) {
    return method.getNameAsString().startsWith("create")
        && method.isStatic()
        && method.isPublic()
        && method.getAnnotationByClass(SkipReactivityValidations.class).isEmpty();
  }

  /**
   * Any public static init method not annotated by SkipReactivityValidations
   */
  public static boolean isTestableInitMethod(final MethodDeclaration method) {
    return method.getNameAsString().startsWith("init")
        && method.isStatic()
        && method.getAnnotationByClass(SkipReactivityValidations.class).isEmpty();
  }

  /**
   * Any public getter method not annotated by SkipReactivityValidations
   */
  private static boolean isValidatableGetterMethod(final MethodDeclaration method) {
    final String name = method.getName().toString();
    return (name.startsWith("get") || name.startsWith("is"))
        && method.isPublic()
        && !method.isStatic()
        && method.getAnnotationByClass(SkipReactivityValidations.class).isEmpty();
  }

  private static Optional<Class<?>> toValidatableClass(final Type type) {
    return Optional.of(type)
        .filter(Type::isClassOrInterfaceType)
        .map(Type::resolve)
        .map(ResolvedType::asReferenceType)
        .filter(v -> v.getQualifiedName().startsWith(PACKAGE_BASE))
        .map(Exceptions.sneak().function(v -> Class.forName(v.getQualifiedName())))
        .filter(v -> !v.isEnum())
        .map(v -> v);
  }
}
