/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class LruCacheTest {

  private int computations;
  private LruCache<Integer, Integer> cache;

  @Test
  void testCachedComputation() {
    computations = 0;
    cache = new LruCache<>(10);
    cache.computeIfAbsent(1, this::compute);
    cache.computeIfAbsent(2, this::compute);
    cache.computeIfAbsent(3, this::compute);
    cache.computeIfAbsent(1, this::compute);
    assertEquals(3, computations, "Should be 3 computation for 3 keys");
  }

  @Test
  void testEvictionOrder() {
    cache = new LruCache<>(3);
    cache.computeIfAbsent(1, this::compute);
    cache.computeIfAbsent(2, this::compute);
    cache.computeIfAbsent(3, this::compute);
    cache.computeIfAbsent(3, this::compute); // should not recompute/evict
    cache.computeIfAbsent(1, this::compute); // should not recompute/evict, but reset the access order of key = 1
    cache.computeIfAbsent(4, this::compute); // should evict key = 2
    assertTrue(cache.containsKey(1), "Should contain key = 1");
    assertFalse(cache.containsKey(2), "Should not contain key = 2");
    assertTrue(cache.containsKey(3), "Should contain key = 3");
    assertTrue(cache.containsKey(4), "Should contain key = 4");
  }

  private int compute(final Integer key) {
    computations++;
    return key * key;
  }

}
