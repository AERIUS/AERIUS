/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.icons;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.junit.jupiter.api.Test;

import nl.aerius.geo.icon.DynamicSvgIcon;
import nl.overheid.aerius.wui.application.geo.icons.labels.Label;
import nl.overheid.aerius.wui.application.geo.icons.labels.LabelIconStyle;
import nl.overheid.aerius.wui.application.geo.icons.labels.LabelSvgIconBuilder;

class LabelSvgIconBuilderTest {

  private static final String RESULT_FILE_BASE_PATH = "nl/overheid/aerius/wui/application/geo/icons/";

  final LabelSvgIconBuilder iconBuilder = new LabelSvgIconBuilderJvm();

  @Test
  void testEmissionSource() throws IOException {
    final DynamicSvgIcon svgIcon = iconBuilder.getSvgIcon(
        List.of(new Label("Test", "#699DCD", "#ffffff")),
        false, LabelIconStyle.EMISSION_SOURCE);
    assertEquals(getResultFileContent("emission-source.svg"), svgIcon.getSvgContent());
  }

  @Test
  void testEmissionSourceWithOutline() throws IOException {
    final DynamicSvgIcon svgIcon = iconBuilder.getSvgIcon(
        List.of(new Label("Test", "#699DCD", "#ffffff")),
        true, LabelIconStyle.EMISSION_SOURCE);
    assertEquals(getResultFileContent("emission-source-with-outline.svg"), svgIcon.getSvgContent());
  }

  @Test
  void testCalculationPoint() throws IOException {
    final DynamicSvgIcon svgIcon = iconBuilder.getSvgIcon(
        List.of(new Label("Test & test < 1km ✔ \uD83D\uDE42", "#FFFE7E", "#000000")),
        false, LabelIconStyle.CALCULATION_POINT);
    assertEquals(getResultFileContent("calculation-point.svg"), svgIcon.getSvgContent());
  }

  @Test
  void testBuilding() throws IOException {
    final DynamicSvgIcon svgIcon = iconBuilder.getSvgIcon(
        List.of(new Label("Test", "#ffffff", "#222222")),
        false, LabelIconStyle.BUILDING);
    assertEquals(getResultFileContent("building.svg"), svgIcon.getSvgContent());
  }

  @Test
  void testEmissionSourceCluster() throws IOException {
    final DynamicSvgIcon svgIcon = iconBuilder.getSvgIcon(
        List.of(
            new Label("1", "#699DCD", "#ffffff"),
            new Label("2", "#6B15CB", "#ffffff")),
        false, LabelIconStyle.EMISSION_SOURCE);
    assertEquals(getResultFileContent("emission-source-cluster.svg"), svgIcon.getSvgContent());
  }

  @Test
  void testEmissionSourceWithIndicators() throws IOException {
    final DynamicSvgIcon svgIcon = iconBuilder.getSvgIcon(
        List.of(new Label("Test", "#699DCD", "#FFFFFF", new Character[] {'T', 'P'})),
        false, LabelIconStyle.EMISSION_SOURCE);
    assertEquals(getResultFileContent("emission-source-with-indicators.svg"), svgIcon.getSvgContent());
  }

  @Test
  void testEmissionSourceWithIndicatorsAndOutline() throws IOException {
    final DynamicSvgIcon svgIcon = iconBuilder.getSvgIcon(
        List.of(new Label("Test", "#699DCD", "#FFFFFF", new Character[] {'T', 'P'})),
        true, LabelIconStyle.EMISSION_SOURCE);
    assertEquals(getResultFileContent("emission-source-with-indicators-and-outline.svg"), svgIcon.getSvgContent());
  }

  @Test
  void testEmissionSourceClusterWithIndicators() throws IOException {
    final DynamicSvgIcon svgIcon = iconBuilder.getSvgIcon(
        List.of(
            new Label("1", "#699DCD", "#FFFFFF", new Character[] {'T', 'P'}),
            new Label("2", "#6B15CB", "#FFFFFF", new Character[] {'P'})),
        false, LabelIconStyle.EMISSION_SOURCE);
    assertEquals(getResultFileContent("emission-source-cluster-with-indicators.svg"), svgIcon.getSvgContent());
  }

  @Test
  void testBuildingWithIndicators() throws IOException {
    final DynamicSvgIcon svgIcon = iconBuilder.getSvgIcon(
        List.of(new Label("Test", "#FFFFFF", "#000000", new Character[] {'T', 'P'})),
        false, LabelIconStyle.BUILDING);
    assertEquals(getResultFileContent("building-with-indicators.svg"), svgIcon.getSvgContent());
  }

  @Test
  void testArchiveProject() throws IOException {
    final DynamicSvgIcon svgIcon = iconBuilder.getSvgIcon(
        List.of(new Label("1", "#f15b29", "#ffffff")),
        false, LabelIconStyle.ARCHIVE_PROJECT);
    assertEquals(getResultFileContent("archive-project.svg"), svgIcon.getSvgContent());
  }

  private String getResultFileContent(final String file) throws IOException {
    try (final InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(RESULT_FILE_BASE_PATH + file)) {
      assertNotNull(stream);
      final byte[] byteContent = stream.readAllBytes();
      return new String(byteContent, StandardCharsets.UTF_8);
    }
  }

}
