/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class StringUtilsTest {

  @Test
  void testEncodeHtmlEntities() {
    assertEquals("a", StringUtils.encodeHtmlEntities("a"), "Basic ASCII text should work");
    assertEquals("&#60;test&#62;", StringUtils.encodeHtmlEntities("<test>"), "HTML Entities should be encoded");
    assertEquals("&#38;&#34;&#39;", StringUtils.encodeHtmlEntities("&\"'"), "HTML Entities should be encoded");
    assertEquals("&#10003;", StringUtils.encodeHtmlEntities("✓"), "Unicode Plane 0 should be encoded");
    assertEquals("&#128526;", StringUtils.encodeHtmlEntities("\uD83D\uDE0E"), "Other Unicode Planes should be encoded");
  }

}
