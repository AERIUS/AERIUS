/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain;

import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.reflections.Reflections;
import org.reflections.scanners.Scanners;

import com.github.javaparser.ParserConfiguration;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.Parameter;
import com.github.javaparser.symbolsolver.JavaSymbolSolver;
import com.github.javaparser.symbolsolver.model.resolution.TypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.ClassLoaderTypeSolver;
import com.github.javaparser.utils.CodeGenerationUtils;
import com.github.javaparser.utils.SourceRoot;

import elemental2.core.JsArray;

import nl.overheid.aerius.wui.application.domain.source.EmissionSubSourceFeature;
import nl.overheid.aerius.wui.vue.SkipReactivityValidations;

/**
 * <p>
 * This test enforces a pattern where end types of emission sources that have sub sources, must initialize their subsources.
 *
 * <p>
 * This test is separate from {@link VueReactivityGeneralInitTest} because it specifically targets sub sources, which maintain a collection through a
 * {@link JsArray} and not a {@link Collection}, which is kind of a unique case that we shouldn't clog the other test with.
 */
public class VueReactivitySubSourceInitTest {
  private static final String SRC_MAIN_JAVA = "src/main/java";
  private static final String PACKAGE_BASE = "nl.overheid.aerius.wui";

  private static Reflections reflections;
  private static SourceRoot sourceRoot;

  @BeforeAll
  static void setup() {
    final TypeSolver solver = new ClassLoaderTypeSolver(VueReactivitySubSourceInitTest.class.getClassLoader());
    final ParserConfiguration parserConfig = new ParserConfiguration()
        .setSymbolResolver(new JavaSymbolSolver(solver));
    sourceRoot = new SourceRoot(CodeGenerationUtils.mavenModuleRoot(VueReactivitySubSourceInitTest.class)
        .resolve(SRC_MAIN_JAVA), parserConfig);

    reflections = new Reflections(PACKAGE_BASE);
  }

  @TestFactory
  Collection<DynamicTest> testAbstractSubSource() {
    return testHierarchyLeaves(EmissionSubSourceFeature.class);
  }

  Collection<DynamicTest> testHierarchyLeaves(final Class<?> clss) {
    final Set<Class<?>> subTypes = reflections.get(Scanners.SubTypes.of(clss).asClass());

    return Stream.concat(Stream.of(clss), subTypes.stream())
        .filter(v -> v.getAnnotation(SkipReactivityValidations.class) == null)
        .filter(v -> !Modifier.isAbstract(v.getModifiers()))
        .map(v -> dynamicTestOf(v, SRC_MAIN_JAVA))
        .sorted((o1, o2) -> o1.getDisplayName().compareTo(o2.getDisplayName()))
        .collect(Collectors.toList());
  }

  private DynamicTest dynamicTestOf(final Class<?> v, final String path) {
    return DynamicTest.dynamicTest("Type: " + v.getSimpleName(), () -> testClassSubSources(v, path));
  }

  private void testClassSubSources(final Class<?> clss, final String path) {
    final CompilationUnit cu = sourceRoot.parse(clss.getPackageName(), clss.getSimpleName() + ".java");

    testCompilationUnit(clss, cu);
  }

  private void testCompilationUnit(final Class<?> clss, final CompilationUnit cu) {
    final Optional<MethodDeclaration> initMethodOpt = cu.findFirst(MethodDeclaration.class, VueReactivityGeneralInitTest::isTestableInitMethod);

    Assertions.assertFalse(initMethodOpt.isEmpty(), "Class type " + clss.getSimpleName() + " does not include an init method");

    final MethodDeclaration initMethod = initMethodOpt.get();
    final String initBody = initMethod.getBody().get().toString();
    final Optional<Parameter> initObjParamOpt = initMethod.getParameters().stream()
        .filter(p -> p.getType().toString().replaceAll("<.*>", "").equals(clss.getSimpleName()))
        .findFirst();

    Assertions.assertTrue(initObjParamOpt.isPresent(), "A parameter with type " + clss.getSimpleName() + " must be passed to the init method");

    final String initObjParamName = initObjParamOpt.get().getNameAsString();
    testSubSourceInit(clss, initBody, initObjParamName);
  }

  private void testSubSourceInit(final Class<?> clss, final String initBody, final String initObjParamName) {
    if (!clss.getSuperclass().getName().startsWith(PACKAGE_BASE)) {
      return;
    }

    final String superStatementRegex = String.format("EmissionSubSourceFeature\\.initSubSources\\(%s(.*)\\);", initObjParamName);
    final boolean containsPattern = Pattern.compile(superStatementRegex)
        .matcher(initBody)
        .find();
    Assertions.assertTrue(containsPattern,
        "Text matching " + superStatementRegex + " should be found in the init body of " + clss.getSimpleName());
  }

}
