/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.domain;

import java.util.List;

import nl.overheid.aerius.shared.domain.ServerUsage;
import nl.overheid.aerius.shared.domain.calculation.JobProgress;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculation;
import nl.overheid.aerius.shared.domain.v2.archive.ArchiveMetaData;

/**
 * Wrapper for objects that should be returned as part of the calculation info for UI.
 */
public class UiCalculationInfoResponse {

  private final JobProgress jobProgress;
  private final ServerUsage serverUsage;
  private final List<SituationCalculation> situationCalculations;
  private final ArchiveMetaData archiveMetaData;

  public UiCalculationInfoResponse(final JobProgress jobProgress, final ServerUsage serverUsage,
      final List<SituationCalculation> situationCalculations, final ArchiveMetaData archiveMetaData) {
    this.jobProgress = jobProgress;
    this.serverUsage = serverUsage;
    this.situationCalculations = situationCalculations;
    this.archiveMetaData = archiveMetaData;
  }

  public JobProgress getJobProgress() {
    return jobProgress;
  }

  public ServerUsage getServerUsage() {
    return serverUsage;
  }

  public List<SituationCalculation> getSituationCalculations() {
    return situationCalculations;
  }

  public ArchiveMetaData getArchiveMetaData() {
    return archiveMetaData;
  }

}
