/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.util.LocaleDBUtils;

/**
 * Our own locale service, that ensures we end up with a known locale.
 */
@Service
public class LocaleService {

  private final PMF pmf;

  @Autowired
  LocaleService(final PMF pmf) {
    this.pmf = pmf;
  }

  public Locale getDefaultLocale() {
    return LocaleDBUtils.getDefaultLocale(pmf);
  }

  public Locale getLocale() {
    // LocaleContextHolder should already be configured with the correct supported locales and default.
    // Reason to use this method: better for mocking, and if it turns out this doesn't always work...
    return LocaleContextHolder.getLocale();
  }

}
