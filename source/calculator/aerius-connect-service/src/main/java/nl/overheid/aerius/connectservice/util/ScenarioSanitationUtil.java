/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.util;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

import nl.overheid.aerius.shared.domain.v2.geojson.Crs;
import nl.overheid.aerius.shared.domain.v2.geojson.Crs.CrsContent;
import nl.overheid.aerius.shared.domain.v2.geojson.FeatureCollection;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.IsFeature;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.geo.EPSG;

public final class ScenarioSanitationUtil {
  /**
   * Refer to the AERIUS Calculator Handbook 2023, page 207, for details on coordinate accuracy.
   * See: https://nexus.aerius.nl/repository/website-resources/calculator/handboek_aerius_calculator_2023.pdf
   */
  private static final int MAX_DECIMAL_PLACES = 3;
  private static final double DECIMAL_FACTOR = Math.pow(10, MAX_DECIMAL_PLACES);

  private ScenarioSanitationUtil() {}

  /**
   * Sanitizes the given scenario.
   *
   * - Round all geometries on a flat 1-by-1-meter plane to 3 decimals (mm) (i.e. don't round WGS / degree-based situations)
   * - Scrub results that were present, if needed, the worker should calculate the results again when exporting.
   */
  public static void sanitizeScenario(final Scenario scenario) {
    // TODO AER-2361: Enable the rounding behaviour below pending decision to endure the consequent calculation result changes that will happen in certain cases
    // We may possibly consider flooring/truncating the decimals, instead of rounding, to eliminate some edge cases when rounding a 2nd time
    // Also enable tests

    // Round geometries
    // scenario.getSituations().stream()
    // .forEach(ScenarioSanitationUtil::sanitizeSituationGeometries);
    // sanitizeFeatureGeometries(scenario.getCustomPoints());

    // Scrub results
    scenario.getCustomPointsList().stream()
        .filter(point -> point.getProperties() != null && point.getProperties().getResults() != null)
        .forEach(point -> point.getProperties().getResults().clear());
  }

  @SuppressWarnings("unused")
  private static void sanitizeSituationGeometries(final ScenarioSituation situation) {
    sanitizeFeatureGeometries(situation.getSources());
    sanitizeFeatureGeometries(situation.getBuildings());
  }

  private static void sanitizeFeatureGeometries(final FeatureCollection<? extends IsFeature> featureCollection) {
    if (featureCollection == null || determineSrid(featureCollection) == EPSG.WGS84) {
      return;
    }

    featureCollection.getFeatures().stream()
        .map(IsFeature::getGeometry).forEach(ScenarioSanitationUtil::sanitizeGeometry);
  }

  private static EPSG determineSrid(final FeatureCollection<? extends IsFeature> featureCollection) {
    return Optional.ofNullable(featureCollection)
        .map(FeatureCollection::getCrs)
        .map(Crs::getProperties)
        .map(CrsContent::getName)
        .map(EPSG::getEnumByEpsg)
        .orElse(null);
  }

  static void sanitizeGeometry(final Geometry geometry) {
    switch (geometry.type()) {
    case POINT:
      final Point pointGeometry = (Point) geometry;
      pointGeometry.setX(roundCoordinate(pointGeometry.getX()));
      pointGeometry.setY(roundCoordinate(pointGeometry.getY()));
      break;
    case LINESTRING:
      final LineString lineGeometry = (LineString) geometry;
      lineGeometry.setCoordinates(roundCoordinates(lineGeometry.getCoordinates()));
      break;
    case POLYGON:
      final Polygon polygonGeometry = (Polygon) geometry;
      polygonGeometry.setCoordinates(roundCoordinates(polygonGeometry.getCoordinates()));
      break;
    }
  }

  private static double[][][] roundCoordinates(final double[][][] coordinates) {
    return Stream.of(coordinates).map(ScenarioSanitationUtil::roundCoordinates).collect(Collectors.toList()).toArray(new double[][][] {});
  }

  private static double[][] roundCoordinates(final double[][] coordinates) {
    return Stream.of(coordinates).map(ScenarioSanitationUtil::roundCoordinates).collect(Collectors.toList()).toArray(new double[][] {});
  }

  private static double[] roundCoordinates(final double[] coordinates) {
    return DoubleStream.of(coordinates).map(ScenarioSanitationUtil::roundCoordinate).toArray();
  }

  /**
   * Round the given value to the given number of decimal places.
   */
  private static double roundCoordinate(final double value) {
    return Math.round(value * DECIMAL_FACTOR) / DECIMAL_FACTOR;
  }
}
