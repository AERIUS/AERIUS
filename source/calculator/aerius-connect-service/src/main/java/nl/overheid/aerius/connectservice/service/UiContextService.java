/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.common.configuration.ApplicationConfigurationInitializer;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.AeriusCustomer;
import nl.overheid.aerius.shared.domain.ProductProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

@Service
public class UiContextService {

  private static final Logger LOGGER = LoggerFactory.getLogger(UiContextService.class);

  private final PMF pmf;

  @Autowired
  public UiContextService(final PMF pmf) {
    this.pmf = pmf;
  }

  /**
   * Retrieve the application configuration for a given product profile and locale
   *
   * @param productProfile the product profile
   * @param locale the locale
   * @return the application configuration
   * @throws AeriusException
   */
  @Cacheable(cacheNames = "contextCache", key = "#productProfile.name() + '-' + #locale.toString()")
  public ApplicationConfiguration getContext(final ProductProfile productProfile, final Locale locale) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      final DBMessagesKey messagesKey = new DBMessagesKey(locale);
      final AeriusCustomer customer = AeriusCustomer.safeValueOf(ConstantRepository.getString(con, ConstantsEnum.CUSTOMER));

      return ApplicationConfigurationInitializer.createApplicationConfiguration(con, messagesKey, customer, productProfile);
    } catch (final SQLException e) {
      LOGGER.error("Could not construct context: {}", e.getMessage(), e);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }
}
