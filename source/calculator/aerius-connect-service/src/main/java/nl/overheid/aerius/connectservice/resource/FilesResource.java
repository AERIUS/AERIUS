/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.resource;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.server.ResponseStatusException;

import reactor.core.publisher.Mono;

import nl.overheid.aerius.connectservice.api.v8.FilesApiDelegate;
import nl.overheid.aerius.connectservice.service.ProxyFileService;
import nl.overheid.aerius.connectservice.service.ResponseService;
import nl.overheid.aerius.shared.FileServerFile;

/**
 * Resource to interact with the file server.
 */
@Service
public class FilesResource implements FilesApiDelegate {

  private static final Logger LOG = LoggerFactory.getLogger(FilesResource.class);

  private final ProxyFileService proxyFileService;
  private final ResponseService responseService;

  @Autowired
  FilesResource(final ProxyFileService proxyFileService, final ResponseService responseService) {
    this.proxyFileService = proxyFileService;
    this.responseService = responseService;
  }

  @Override
  public ResponseEntity<Resource> getFile(final String jobKey, final String name) {
    return getFile(FileServerFile.FREE_FORMAT, jobKey, name);
  }

  public ResponseEntity<Resource> getFile(final FileServerFile fileServerFile, final String... pathVariables) {
    try {
      return proxyFileService.retrieveFile(fileServerFile, this::responseHandler, pathVariables);
    } catch (final RuntimeException e) {
      LOG.warn("getFile failed with RuntimeException: {}", e.getMessage());
      return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
  }

  /**
   * Handles the response from the file server. If the status is a 3xx it will redirect the file to the requested. If the response is a normal
   * ok. It will retrieve the file and stream it to the requester. Other stati will be handled as errors.
   *
   * @param response response object from the file sever
   * @return response to be passed to the caller
   */
  private Mono<ResponseEntity<Resource>> responseHandler(final ClientResponse response) {
    if (response.statusCode().is3xxRedirection()) {
      return Mono.just(new ResponseEntity<>(response.headers().asHttpHeaders(), response.statusCode()));
    } else if (HttpStatus.OK == response.statusCode()) {
      return response.bodyToMono(DataBuffer.class).map(buffer -> returnFile(response, buffer));
    } else {
      return response.createException().flatMap(we -> Mono.error(ProxyFileService.handleError(we)));
    }
  }

  private ResponseEntity<Resource> returnFile(final ClientResponse response, final DataBuffer dataBuffer) {
    if (dataBuffer == null) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    final HttpHeaders headers = new HttpHeaders();
    final ContentDisposition disposition = ContentDisposition.parse(response.headers().header(HttpHeaders.CONTENT_DISPOSITION).get(0));
    headers.setContentDisposition(disposition);

    return responseService.toOkResponse(new InputStreamResource(dataBuffer.asInputStream(true)), headers);
  }

  @Override
  public Optional<NativeWebRequest> getRequest() {
    return FilesApiDelegate.super.getRequest();
  }
}
