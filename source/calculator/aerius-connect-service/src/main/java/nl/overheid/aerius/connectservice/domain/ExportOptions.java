/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.connectservice.domain;

import java.util.Set;

import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.export.ExportAppendix;
import nl.overheid.aerius.shared.domain.export.ExportType;

public class ExportOptions {

  private JobType jobType;
  private String name;
  private String email;
  private boolean protectJob;
  private ExportType exportType;
  private Set<ExportAppendix> appendices;
  private boolean demoMode;

  public JobType getJobType() {
    return jobType;
  }

  public void setJobType(final JobType jobType) {
    this.jobType = jobType;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(final String email) {
    this.email = email;
  }

  public boolean isProtectJob() {
    return protectJob;
  }

  public void setProtectJob(final boolean protectJob) {
    this.protectJob = protectJob;
  }

  public ExportType getExportType() {
    return exportType;
  }

  public void setExportType(final ExportType exportType) {
    this.exportType = exportType;
  }

  public Set<ExportAppendix> getAppendices() {
    return appendices;
  }

  public void setAppendices(final Set<ExportAppendix> appendices) {
    this.appendices = appendices;
  }

  public boolean isDemoMode() {
    return demoMode;
  }

  public void setDemoMode(final boolean demoMode) {
    this.demoMode = demoMode;
  }
}
