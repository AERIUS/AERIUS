/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import nl.aerius.taskmanager.client.TaskManagerClientSender;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.i18n.AeriusExceptionMessages;
import nl.overheid.aerius.importer.ValidationFactory;
import nl.overheid.aerius.importer.domain.ImportTaskInput;
import nl.overheid.aerius.importer.domain.ImporterInput;
import nl.overheid.aerius.importer.domain.SaveImportedResultsTaskInput;
import nl.overheid.aerius.importer.domain.SituationWithFileId;
import nl.overheid.aerius.shared.FileServerExpireTag;
import nl.overheid.aerius.shared.FileServerFile;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.importer.ImportProperties;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.validation.FileValidationStatus;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Service to send import related tasks to the importer worker.
 */
@Service
public class UiImportService {

  private static final Logger LOG = LoggerFactory.getLogger(UiImportService.class);

  private final ProxyFileService fileService;
  private final JobRepositoryBean jobRepository;
  private final TaskManagerClientSender taskManagerClientSender;

  @Autowired
  public UiImportService(final ProxyFileService fileService, final JobRepositoryBean jobRepository,
      final TaskManagerClientSender taskManagerClientSender) {
    this.fileService = fileService;
    this.jobRepository = jobRepository;
    this.taskManagerClientSender = taskManagerClientSender;
  }

  /**
   * Sends the import task to the import worker.
   *
   * @param uuid uuid under which the file is stored on the file server, and the ui queries for the file
   * @param importFilename the name of the file is stored in the file server
   * @param originalFilename original filename as provided with the upload
   * @param importProperties import options
   * @param expire the tag to indicate how long the import file should be stored
   * @throws IOException
   */
  public void sendImportToWorker(final String uuid, final String importFilename, final String originalFilename,
      final ImportProperties importProperties, final FileServerExpireTag expire) throws IOException {
    final Locale locale = LocaleContextHolder.getLocale();
    final ImporterInput importerInput = new ImportTaskInput(uuid, importFilename, originalFilename, importProperties, expire, locale);

    taskManagerClientSender.sendTask(importerInput, uuid, uuid, null, WorkerType.IMPORTER.type());
  }

  /**
   * Send a task to the import worker to save previously imported results files.
   *
   * @param scenario The scenario to import, should contain at least the situations with proper IDs and types.
   * @param situationFileIds The list of scenarios to import results for, along with their file IDs.
   * The file IDs should correspond to what the scenario was originally imported with.
   * @param archiveContributionFileId The optional file ID for the archive contribution file that was imported and should be included as well.
   * @return The job key to track the import with, similar to a calculations.
   * @throws IOException
   * @throws AeriusException
   */
  public String sendSaveResultsToWorker(final Scenario scenario, final List<SituationWithFileId> situationFileIds,
      final String archiveContributionFileId) throws IOException, AeriusException {
    final String jobKey = jobRepository.createJob(JobType.INSERT_RESULTS);
    final ImporterInput importerInput = new SaveImportedResultsTaskInput(jobKey, scenario, situationFileIds, archiveContributionFileId);

    taskManagerClientSender.sendTask(importerInput, jobKey, jobKey, null, WorkerType.IMPORTER.type());
    return jobKey;
  }

  /**
   * On Exception when something failed trying to send the import to the import worker this exception handler should be called to write validation
   * status to the file server to let the user interface know the validation failed.
   *
   * @param uuid uuid to store the validation status
   * @param e exception thrown by the import
   * @param expire the tag to indicate how long the import file should be stored
   */
  public void onException(final String uuid, final Exception e, final FileServerExpireTag expire) {
    LOG.debug("Error while importing file", e);
    final Locale locale = LocaleContextHolder.getLocale();
    final FileValidationStatus status = ValidationFactory.createFailedFileValidationStatus(new AeriusExceptionMessages(locale), uuid, e);

    writeAndSaveResultValidation(uuid, status, expire);
  }

  private void writeAndSaveResultValidation(final String fileCode, final FileValidationStatus status, final FileServerExpireTag expire) {
    ValidationFactory.updateValidationStatus(status);
    fileService.writeJson(fileCode, FileServerFile.VALIDATION, expire, status);
  }
}
