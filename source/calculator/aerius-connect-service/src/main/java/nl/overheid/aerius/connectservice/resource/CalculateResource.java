/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.resource;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import nl.overheid.aerius.connectservice.auth.AuthenticationService;
import nl.overheid.aerius.connectservice.model.CalculateResponse;
import nl.overheid.aerius.connectservice.model.UploadFile;
import nl.overheid.aerius.connectservice.service.CalculateService;
import nl.overheid.aerius.connectservice.service.ObjectValidatorService;
import nl.overheid.aerius.connectservice.service.ResponseService;
import nl.overheid.aerius.connectservice.service.ScenarioConstruction;
import nl.overheid.aerius.connectservice.service.ScenarioService;
import nl.overheid.aerius.importer.ImportOption;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

class CalculateResource<T> {

  protected final ResponseService responseService;
  protected final AuthenticationService authentication;
  private final CalculateService<T> calculateService;
  private final ScenarioService scenarioService;
  private final ObjectValidatorService validatorService;

  public CalculateResource(final AuthenticationService authenticationService, final ResponseService responseService,
      final ScenarioService scenarioService, final ObjectValidatorService validatorService, final CalculateService<T> calculateService) {
    this.authentication = authenticationService;
    this.responseService = responseService;
    this.calculateService = calculateService;
    this.scenarioService = scenarioService;
    this.validatorService = validatorService;
  }

  protected ResponseEntity<CalculateResponse> calculate(final List<UploadFile> files, final List<MultipartFile> fileParts, final Theme theme,
      final Set<ImportOption> additionalImportOptions, final T options) {
    return calculate(calculateService, files, fileParts, theme, additionalImportOptions, options);
  }

  protected ResponseEntity<CalculateResponse> calculate(final CalculateService<T> calculateService, final List<UploadFile> files,
      final List<MultipartFile> fileParts, final Theme theme, final Set<ImportOption> additionalImportOptions, final T options) {
    validatorService.validate(options);
    validatorService.validateAll(files);
    try {
      // Validate user
      final ConnectUser user = authentication.getCurrentUserWithValidatingJobLimits();
      // Validate options
      validateOptions(options);
      // Link uploadfiles (metadata) and fileparts (actual files)
      final Map<UploadFile, MultipartFile> linkedFiles = linkFiles(files, fileParts);

      final ScenarioConstruction scenarioConstruction = scenarioService.constructScenario(theme, linkedFiles, additionalImportOptions);
      calculateService.validateInput(scenarioConstruction);
      final CalculateResponse response = new CalculateResponse();
      response.setErrors(responseService.convertToValidation(scenarioConstruction.getErrors()));
      response.setWarnings(responseService.convertToValidation(scenarioConstruction.getWarnings()));

      if (scenarioConstruction.isSuccesful()) {
        final String jobKey = calculateService.calculate(user, scenarioConstruction.getScenario(), options);
        response.setSuccessful(true);
        response.setJobKey(jobKey);
      } else {
        response.setSuccessful(false);
      }

      return responseService.toOkResponse(response);
    } catch (final AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  protected void validateOptions(final T options) throws AeriusException {
    // NO-OP by default
  }

  protected static Map<UploadFile, MultipartFile> linkFiles(final List<UploadFile> files, final List<MultipartFile> fileParts) throws AeriusException {
    validateFileNames(files, fileParts);
    final Map<UploadFile, MultipartFile> linkedFiles = new LinkedHashMap<>();

    for (final UploadFile uploadFile : files) {
      final MultipartFile linkedFile = fileParts.stream()
          .filter(filePart -> filePart.getOriginalFilename().endsWith(uploadFile.getFileName()))
          .findFirst().orElseThrow(() -> new AeriusException(AeriusExceptionReason.IMPORT_FILE_NOT_SUPPLIED));
      linkedFiles.put(uploadFile, linkedFile);
    }
    return linkedFiles;
  }

  private static void validateFileNames(final List<UploadFile> files, final List<MultipartFile> fileParts) throws AeriusException {
    if (files.size() != fileParts.size()) {
      throw new AeriusException(AeriusExceptionReason.IMPORT_FILE_NOT_SUPPLIED);
    }
    final Map<String, Integer> uniqueFilenames = new HashMap<>();
    files.forEach(up -> uniqueFilenames.merge(up.getFileName(), 1, Integer::sum));

    if (uniqueFilenames.size() != files.size()) {
      final List<String> duplicateNames = uniqueFilenames.entrySet().stream().filter(e -> e.getValue() > 1).map(Map.Entry::getKey)
          .collect(Collectors.toList());

      throw new AeriusException(AeriusExceptionReason.IMPORTED_FILE_DUPLICATES, String.join(",", duplicateNames));
    }
  }

}
