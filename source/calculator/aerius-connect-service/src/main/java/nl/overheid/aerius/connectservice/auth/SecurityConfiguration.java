/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.header.writers.ReferrerPolicyHeaderWriter;

import nl.overheid.aerius.shared.Constants;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration {

  private final ApiKeyFilter apiKeyFilter;

  @Autowired
  SecurityConfiguration(final ApiKeyFilter apiKeyFilter) {
    this.apiKeyFilter = apiKeyFilter;
  }

  @Bean
  protected SecurityFilterChain securityFilterChain(final HttpSecurity http) throws Exception {
    commonFilterChainOptions(http);
    http.authorizeRequests()
        .antMatchers("/**/jobs/**", "/**/receptorSets/**", "/**/wnb/**", "/**/own2000/**", "/**/rbl/**", "/**/lbv/**", "/**/analyse/**", "/**/register/**", "/**/user/validateApiKey")
        .authenticated().and()
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    return http.build();
  }

  @Bean
  protected SecurityFilterChain uiFilterChain(final HttpSecurity http) throws Exception {
    commonFilterChainOptions(http);
    http.authorizeRequests()
        .antMatchers("/**/ui/**")
        .permitAll();
    return http.build();
  }

  /**
   * Common filter chain options, to apply to both the secured endpoints and optionally secured endpoints.
   */
  private void commonFilterChainOptions(final HttpSecurity http) throws Exception {
    http
        .addFilter(apiKeyFilter)
        .csrf().disable()
        .headers()
        .frameOptions().deny()
        .referrerPolicy().policy(ReferrerPolicyHeaderWriter.ReferrerPolicy.NO_REFERRER).and()
        .contentTypeOptions().and()
        .httpStrictTransportSecurity().maxAgeInSeconds(Constants.HSTS_MAX_AGE);
  }
}
