/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.overheid.aerius.connectservice.model.JobStatus;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.processmonitor.ProcessMonitorClientBean;
import nl.overheid.aerius.shared.domain.calculation.JobProgress;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

@Service
public class JobsService {

  private final JobRepositoryBean jobRepository;
  private final ProcessMonitorClientBean processMonitorClient;

  @Autowired
  public JobsService(final JobRepositoryBean jobRepository, final ProcessMonitorClientBean processMonitorClient) {
    this.jobRepository = jobRepository;
    this.processMonitorClient = processMonitorClient;
  }

  public Optional<JobStatus> getJob(final ConnectUser user, final String jobKey) throws AeriusException {
    return jobRepository.getProgressForUserAndKey(user, jobKey).map(JobsService::convert);
  }

  public List<JobStatus> getJobsForUser(final ConnectUser user) throws AeriusException {
    return jobRepository.getProgressForUser(user).stream()
        .map(JobsService::convert)
        .toList();
  }

  public List<JobStatus> getJobsForUserAndJobStates(final ConnectUser user, final List<String> status) throws AeriusException {
    final Set<JobState> jobStatesToFilter = status.stream().flatMap(JobsService::convert).collect(Collectors.toSet());

    return jobRepository.getProgressForUserAndJobStates(user, jobStatesToFilter).stream()
        .map(JobsService::convert)
        .toList();
  }

  private static JobStatus convert(final JobProgress progress) {
    return new JobStatus()
        .jobKey(progress.getKey())
        .name(progress.getName())
        .status(convert(progress.getState()))
        .numberOfPoints((long) progress.getNumberOfPoints())
        .numberOfPointsCalculated(Math.round(progress.getNumberOfPointsCalculated()))
        .startDateTime(convert(progress.getStartDateTime()))
        .endDateTime(convert(progress.getEndDateTime()))
        .resultUrl(progress.getResultUrl())
        .errorMessage(progress.getErrorMessage());
  }

  /**
   * Convert new JobStates to old states to keep Connect API backward compatible.
   *
   * @param state job State to convert
   * @return String representing the old job state
   */
  private static String convert(final JobState state) {
    return switch (state) {
      case PREPARING, CALCULATING, POST_PROCESSING -> "RUNNING";
      case CANCELLED, COMPLETED, DELETED, ERROR, UNDEFINED ->  state.name();
      case QUEUING -> "INITIALIZED";
      default -> state.name();
    };
  }

  private static OffsetDateTime convert(final Date date) {
    if (date == null) {
      return null;
    } else {
      return date.toInstant().atOffset(ZoneOffset.UTC);
    }
  }

  /**
   * Convert an old Job State to a list of new Job States to keep Connect API backward compatible.
   * It's a list because the old state RUNNING has been split up into 3 new states.
   *
   * @param oldJobState old job state to convert
   * @return Stream of new JobStates
   */
  private static Stream<JobState> convert(final String oldJobState) {
    return switch (oldJobState) {
      case "INITIALIZED" -> Stream.of(JobState.QUEUING);
      case "RUNNING" -> Stream.of(JobState.PREPARING, JobState.CALCULATING, JobState.POST_PROCESSING);
      default -> Stream.of(JobState.safeValueOf(oldJobState));
    };
  }

  public void cancelJob(final ConnectUser user, final String jobKey) throws AeriusException {
    if (jobRepository.isJobFromUser(user, jobKey)) {
      jobRepository.cancelJob(jobKey);
      processMonitorClient.cancelJob(jobKey);
    } else {
      throw new AeriusException(AeriusExceptionReason.CONNECT_USER_JOBKEY_DOES_NOT_EXIST, jobKey);
    }
  }

  public void deleteJob(final ConnectUser user, final String jobKey) throws AeriusException {
    if (jobRepository.isJobFromUser(user, jobKey)) {
      jobRepository.deleteJob(jobKey);
      processMonitorClient.cancelJob(jobKey);
    } else {
      throw new AeriusException(AeriusExceptionReason.CONNECT_USER_JOBKEY_DOES_NOT_EXIST, jobKey);
    }
  }

}
