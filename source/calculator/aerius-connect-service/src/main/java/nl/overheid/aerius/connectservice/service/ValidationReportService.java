/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import nl.overheid.aerius.connectservice.domain.ImportParcelAggregate;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.validation.FileValidationStatus;
import nl.overheid.aerius.shared.domain.validation.ValidationError;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.OSUtils;

/**
 * Service to generate .csv validation reports from {@code ImportParcel}s
 */
@Service
public class ValidationReportService {

  private static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

  // Convenience local constant.
  private static final String SNL = OSUtils.LNL;

  private static final String[] CSV_HEADERS = {"Reference", "Time", "Type", "Code", "Message"};

  private final MessagesService messagesService;

  public ValidationReportService(final MessagesService messagesService) {
    this.messagesService = messagesService;
  }

  /**
   * Generate a .csv validation report from an {@code ImportParcelAggregate}
   *
   * @param aggregate
   * @param generationTime
   * @param outputStream
   * @throws IOException
   */
  public void report(final ImportParcelAggregate aggregate, final LocalDateTime generationTime, final OutputStream outputStream) throws IOException {
    report(new ImportParcelAggregateInput(aggregate), generationTime, outputStream);
  }

  /**
   * Generate a .csv validation report from an {@code List<FileValidationStatus>}
   *
   * @param aggregate
   * @param generationTime
   * @param outputStream
   * @throws IOException
   */
  public void report(final List<FileValidationStatus> aggregate, final LocalDateTime generationTime, final OutputStream outputStream)
      throws IOException {
    report(new FileValidationStatusInput(aggregate), generationTime, outputStream);
  }

  private <F, M> void report(final ReportInput<F, M> input, final LocalDateTime generationTime, final OutputStream outputStream)
      throws IOException {
    try (final Writer writer = new BufferedWriter(new OutputStreamWriter(outputStream, StandardCharsets.UTF_8))) {
      writeRow(writer, CSV_HEADERS);
      for (final F file : input.getFiles()) {
        writeMessageRow(writer, input.getReference(file), generationTime, "ERROR", input, input.getErrors(file));
      }
      for (final F file : input.getFiles()) {
        writeMessageRow(writer, input.getReference(file), generationTime, "WARNING", input, input.getWarnings(file));
      }
    }
  }

  private <F, M> void writeMessageRow(final Writer writer, final String reference, final LocalDateTime generationTime, final String messageType,
      final ReportInput<F, M> input, final List<M> messages) throws IOException {
    if (!messages.isEmpty()) {
      for (final M message : messages) {
        final String[] fields = {
            reference,
            generationTime.format(DATETIME_FORMATTER),
            messageType,
            String.valueOf(input.getMessageCode(message)),
            input.getMessage(message)
        };
        writeRow(writer, fields);
      }
    }
  }

  private static void writeRow(final Writer writer, final String[] fields) throws IOException {
    writer.append(Arrays.stream(fields).map(ValidationReportService::escapeSpecialCharacters).collect(Collectors.joining(",")));
    writer.append(SNL);
  }

  private static String escapeSpecialCharacters(String data) {
    data = data.replaceAll("[\\r\\n]", " "); // remove newlines
    if (data.contains(",") || data.contains("\"") || data.contains("'")) {
      data = "\"" + data.replace("\"", "\"\"") + "\"";
    }
    return data;
  }

  interface ReportInput<F, M> {

    List<F> getFiles();

    String getReference(final F file);

    List<M> getErrors(final F file);

    List<M> getWarnings(final F file);

    String getMessage(final M message);

    Integer getMessageCode(final M message);

  }

  class ImportParcelAggregateInput implements ReportInput<ImportParcel, AeriusException> {

    private final ImportParcelAggregate importParcelAggregate;

    ImportParcelAggregateInput(final ImportParcelAggregate importParcelAggregate) {
      this.importParcelAggregate = importParcelAggregate;
    }

    @Override
    public List<ImportParcel> getFiles() {
      return importParcelAggregate.getImportParcels();
    }

    @Override
    public String getReference(final ImportParcel file) {
      return file.getSituation().getReference();
    }

    @Override
    public List<AeriusException> getErrors(final ImportParcel file) {
      return file.getExceptions();
    }

    @Override
    public List<AeriusException> getWarnings(final ImportParcel file) {
      return file.getWarnings();
    }

    @Override
    public String getMessage(final AeriusException message) {
      return messagesService.getExceptionMessage(message);
    }

    @Override
    public Integer getMessageCode(final AeriusException message) {
      return message.getReason().getErrorCode();
    }

  }

  static class FileValidationStatusInput implements ReportInput<FileValidationStatus, ValidationError> {

    private final List<FileValidationStatus> fileValidationStatuses;

    FileValidationStatusInput(final List<FileValidationStatus> fileValidationStatuses) {
      this.fileValidationStatuses = fileValidationStatuses;
    }

    @Override
    public List<FileValidationStatus> getFiles() {
      return fileValidationStatuses;
    }

    @Override
    public String getReference(final FileValidationStatus file) {
      return file.getReference();
    }

    @Override
    public List<ValidationError> getErrors(final FileValidationStatus file) {
      return file.getErrors();
    }

    @Override
    public List<ValidationError> getWarnings(final FileValidationStatus file) {
      return file.getWarnings();
    }

    @Override
    public String getMessage(final ValidationError message) {
      return message.getMessage();
    }

    @Override
    public Integer getMessageCode(final ValidationError message) {
      return message.getReason();
    }

  }
}
