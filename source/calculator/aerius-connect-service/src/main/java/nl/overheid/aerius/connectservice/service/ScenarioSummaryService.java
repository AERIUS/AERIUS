/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import nl.aerius.taskmanager.client.TaskManagerClientSender;
import nl.overheid.aerius.importer.domain.ImporterInput;
import nl.overheid.aerius.importer.domain.SummarizeTaskInput;
import nl.overheid.aerius.importer.summary.ImportSummaryGenerator;
import nl.overheid.aerius.shared.FileServerExpireTag;
import nl.overheid.aerius.shared.domain.importer.ImportProperties;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Service to create a summary of an imported file
 */
@Service
public class ScenarioSummaryService {

  private final TaskManagerClientSender taskManagerClientSender;
  private final ImportSummaryGenerator importSummaryGenerator = new ImportSummaryGenerator();

  public ScenarioSummaryService(final TaskManagerClientSender taskManagerClientSender) {
    this.taskManagerClientSender = taskManagerClientSender;
  }

  /**
   * Directly convert an ImportParcel to a summary
   * @param locale the locale of the error messages
   * @param parcels the parcels to convert
   * @return the summary
   * @throws AeriusException
   */
  public Object generateSummaryResponse(final Locale locale, final List<ImportParcel> parcels) throws AeriusException {
    return importSummaryGenerator.generateImportSummary(locale, parcels);
  }

  /**
   * Sends the import task to the import worker.
   *
   * @param importFilename the name of the file is stored in the file server
   * @param jobKey jobKey where the import is processed under
   * @param originalFilename original filename as provided with the upload
   * @param importProperties import options
   * @param expire the tag to indicate how long the import file should be stored
   * @param maximumResultsDistance maximum distance of included results
   * @param decisionFramework Indication if decision framework related information has to be included
   * @throws IOException
   */
  public void sendSummaryRequestToWorker(final String importFilename, final String jobKey, final String originalFilename,
      final ImportProperties importProperties, final FileServerExpireTag expire, final Integer maximumResultsDistance,
      final boolean decisionFramework) throws IOException {
    final Locale locale = LocaleContextHolder.getLocale();
    final ImporterInput importerInput = new SummarizeTaskInput(jobKey, importFilename, originalFilename, importProperties, expire, locale,
        maximumResultsDistance, decisionFramework);

    taskManagerClientSender.sendTask(importerInput, jobKey, jobKey, null, WorkerType.IMPORTER.type());
  }

}
