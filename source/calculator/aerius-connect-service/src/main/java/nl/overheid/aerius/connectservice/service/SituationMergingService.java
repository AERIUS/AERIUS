/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import nl.overheid.aerius.shared.domain.v2.building.Building;
import nl.overheid.aerius.shared.domain.v2.building.BuildingFeature;
import nl.overheid.aerius.shared.domain.v2.geojson.FeatureCollection;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKMeasure;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKMeasureFeature;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.InlandShippingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.MaritimeShippingEmissionSource;

/**
 * Service class to merge 2 situations.
 *
 * This class takes care of unique aspects of a situation, like GML IDs that should be unique.
 * If for instance a GML ID was already present in the target situation, the ID of the object in the situation to add will be altered.
 * Any references to the object (if any) will be updated as well.
 */
@Service
public class SituationMergingService {

  private static final Pattern VERSION_PATTERN = Pattern.compile("^(.*_)([0-9]+)$");

  public void merge(final ScenarioSituation situation, final ScenarioSituation situationToAdd) {
    updateCrsIfNeeded(situation, situationToAdd);
    if ((situation.getName() == null || situation.getName().isEmpty())) {
      situation.setName(situationToAdd.getName());
    }
    addAndRemapSourcesIfNeeded(situation.getEmissionSourcesList(), situationToAdd);
    addAndRemapBuildingsIfNeeded(situation.getBuildingsList(), situationToAdd);
    situation.getCimlkDispersionLinesList().addAll(situationToAdd.getCimlkDispersionLinesList());
    addAndRemapMeasuresIfNeeded(situation.getCimlkMeasuresList(), situationToAdd);
    situation.getCimlkCorrections().addAll(situationToAdd.getCimlkCorrections());
    if (situation.getYear() == 0) {
      situation.setYear(situationToAdd.getYear());
    }
    if (situation.getNettingFactor() == null) {
      situation.setNettingFactor(situationToAdd.getNettingFactor());
    }
  }

  private static void updateCrsIfNeeded(final ScenarioSituation situation, final ScenarioSituation situationToAdd) {
    updateCrsIfNeeded(situation.getSources(), situationToAdd.getSources());
    updateCrsIfNeeded(situation.getBuildings(), situationToAdd.getBuildings());
    updateCrsIfNeeded(situation.getCimlkDispersionLines(), situationToAdd.getCimlkDispersionLines());
    updateCrsIfNeeded(situation.getCimlkMeasures(), situationToAdd.getCimlkMeasures());
  }

  private static <T extends Serializable> void updateCrsIfNeeded(final FeatureCollection<T> target, final FeatureCollection<T> origin) {
    if (target.getCrs() == null) {
      target.setCrs(origin.getCrs());
    }
  }

  private static void addAndRemapSourcesIfNeeded(final List<EmissionSourceFeature> sources, final ScenarioSituation situationToAdd) {
    final Set<String> existingGmlIds = sources.stream()
        .map(feature -> feature.getProperties().getGmlId())
        .filter(Objects::nonNull)
        .collect(Collectors.toSet());
    situationToAdd.getEmissionSourcesList().forEach(feature -> {
      checkRemappingSource(feature.getProperties(), existingGmlIds, situationToAdd);
      sources.add(feature);
      existingGmlIds.add(feature.getProperties().getGmlId());
    });
  }

  private static void checkRemappingSource(final EmissionSource source, final Set<String> existingGmlIds, final ScenarioSituation situationToAdd) {
    final String currentId = source.getGmlId();
    if (currentId != null && existingGmlIds.contains(currentId)) {
      final String newId = produceUniqueNewKey(currentId, existingGmlIds);
      source.setGmlId(newId);
      handleRemappedSourceId(currentId, newId, situationToAdd);
    }
  }

  private static void handleRemappedSourceId(final String oldId, final String newId, final ScenarioSituation situationToAdd) {
    situationToAdd.getEmissionSourcesList().stream()
        .map(EmissionSourceFeature::getProperties)
        .forEach(source -> checkRemappedSourceId(oldId, newId, source));
  }

  private static void checkRemappedSourceId(final String oldId, final String newId, final EmissionSource existingSource) {
    if (existingSource instanceof InlandShippingEmissionSource) {
      final InlandShippingEmissionSource inlandSource = (InlandShippingEmissionSource) existingSource;
      if (oldId.equals(inlandSource.getMooringAId())) {
        inlandSource.setMooringAId(newId);
      }
      // A and B can point to the same source, so don't use else.
      if (oldId.equals(inlandSource.getMooringBId())) {
        inlandSource.setMooringBId(newId);
      }
    } else if (existingSource instanceof MaritimeShippingEmissionSource) {
      final MaritimeShippingEmissionSource maritimeSource = (MaritimeShippingEmissionSource) existingSource;
      if (oldId.equals(maritimeSource.getMooringAId())) {
        maritimeSource.setMooringAId(newId);
      }
      // A and B can point to the same source, so don't use else.
      if (oldId.equals(maritimeSource.getMooringBId())) {
        maritimeSource.setMooringBId(newId);
      }
    }
  }

  private static void addAndRemapBuildingsIfNeeded(final List<BuildingFeature> buildings, final ScenarioSituation situationToAdd) {
    // First determine existing GML ID's
    final Set<String> existingGmlIds = buildings.stream()
        .map(feature -> feature.getProperties().getGmlId())
        .filter(Objects::nonNull)
        .collect(Collectors.toSet());
    situationToAdd.getBuildingsList().forEach(feature -> {
      checkRemappingBuilding(feature.getProperties(), existingGmlIds, situationToAdd);
      buildings.add(feature);
      existingGmlIds.add(feature.getProperties().getGmlId());
    });
  }

  private static void checkRemappingBuilding(final Building building, final Set<String> existingGmlIds,
      final ScenarioSituation situationToAdd) {
    final String currentId = building.getGmlId();
    if (existingGmlIds.contains(currentId)) {
      final String newId = produceUniqueNewKey(currentId, existingGmlIds);
      building.setGmlId(newId);
      handleRemappedBuildingId(currentId, newId, situationToAdd);
    }
  }

  private static void handleRemappedBuildingId(final String oldId, final String newId, final ScenarioSituation situationToAdd) {
    situationToAdd.getEmissionSourcesList().stream()
        .map(EmissionSourceFeature::getProperties)
        .forEach(source -> checkRemappedBuildingId(oldId, newId, source));
  }

  private static void checkRemappedBuildingId(final String oldId, final String newId, final EmissionSource existingSource) {
    if (existingSource.getCharacteristics() != null && oldId.equals(existingSource.getCharacteristics().getBuildingId())) {
      existingSource.getCharacteristics().setBuildingId(newId);
    }
  }

  private static void addAndRemapMeasuresIfNeeded(final List<CIMLKMeasureFeature> measures, final ScenarioSituation situationToAdd) {
    final Set<String> existingGmlIds = measures.stream()
        .map(feature -> feature.getProperties().getGmlId())
        .filter(Objects::nonNull)
        .collect(Collectors.toSet());
    situationToAdd.getCimlkMeasuresList().forEach(feature -> {
      checkRemappingMeasure(feature.getProperties(), existingGmlIds);
      measures.add(feature);
      existingGmlIds.add(feature.getProperties().getGmlId());
    });
  }

  private static void checkRemappingMeasure(final CIMLKMeasure measure, final Set<String> existingGmlIds) {
    final String currentId = measure.getGmlId();
    if (existingGmlIds.contains(currentId)) {
      final String newId = produceUniqueNewKey(currentId, existingGmlIds);
      measure.setGmlId(newId);
    }
  }

  /**
   * Recursively increment the key with a remapping postfix in order to ensure a
   * unique key is produced.
   *
   * Uses pretty much the same version pattern as used in InternalGMLWriter: some-key_with_otherstuff_[version]
   *
   * @return a unique key
   */
  private static String produceUniqueNewKey(final String key, final Set<String> existingKeys) {
    final String newKey = smartIncrementedRemapping(key);
    if (existingKeys.contains(newKey)) {
      return produceUniqueNewKey(newKey, existingKeys);
    } else {
      return newKey;
    }
  }

  private static String smartIncrementedRemapping(final String key) {
    final Matcher matcher = VERSION_PATTERN.matcher(key);
    if (matcher.matches()) {
      try {
        final int num = Integer.parseInt(matcher.group(2));
        final String labelBare = matcher.group(1);
        return labelBare + (num + 1);
      } catch (final NumberFormatException e) {
        return key + "_1";
      }
    } else {
      return key + "_1";
    }
  }

}
