/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.domain;

public class UIInfoMarkerRequest {

  private int receptorId;
  private int calculationYear;
  private String jobKey;

  public int getReceptorId() {
    return receptorId;
  }

  public void setReceptorId(final int receptorId) {
    this.receptorId = receptorId;
  }

  public int getCalculationYear() {
    return calculationYear;
  }

  public void setCalculationYear(final int callculationYear) {
    this.calculationYear = callculationYear;
  }

  public String getJobKey() {
    return jobKey;
  }

  public void setJobKey(final String jobKey) {
    this.jobKey = jobKey;
  }
}
