/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import nl.overheid.aerius.connectservice.api.v8.ReceptorSetsApiDelegate;
import nl.overheid.aerius.connectservice.auth.AuthenticationService;
import nl.overheid.aerius.connectservice.domain.ImportParcelAggregate;
import nl.overheid.aerius.connectservice.model.ReceptorSet;
import nl.overheid.aerius.connectservice.model.ValidateResponse;
import nl.overheid.aerius.connectservice.model.ValidationMessage;
import nl.overheid.aerius.connectservice.service.ObjectValidatorService;
import nl.overheid.aerius.connectservice.service.ReceptorSetsService;
import nl.overheid.aerius.connectservice.service.ResponseService;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.exception.AeriusException;

@Service
@Profile({"nl", "uk"})
public class ReceptorSetsResource implements ReceptorSetsApiDelegate {

  private final AuthenticationService authentication;
  private final ResponseService responseService;
  private final ReceptorSetsService receptorSetsService;
  private final ObjectValidatorService validatorService;

  @Autowired
  ReceptorSetsResource(final AuthenticationService authenticationService, final ResponseService responseService,
      final ReceptorSetsService receptorSetsService, final ObjectValidatorService validatorService) {
    this.authentication = authenticationService;
    this.responseService = responseService;
    this.receptorSetsService = receptorSetsService;
    this.validatorService = validatorService;
  }

  @Override
  public ResponseEntity<ValidateResponse> addReceptorSet(final ReceptorSet receptorSet, final MultipartFile filePart) {
    validatorService.validate(receptorSet);
    try {
      final ConnectUser connectUser = authentication.getCurrentUser();
      final ImportParcelAggregate importResult = receptorSetsService.addReceptorSet(connectUser, receptorSet, filePart);
      return responseService.toOkResponse(convert(importResult));
    } catch (final AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<Void> deleteReceptorSet(final String name) {
    try {
      final ConnectUser connectUser = authentication.getCurrentUser();
      receptorSetsService.deleteReceptorSet(connectUser, name);
      return responseService.toOkResponse();
    } catch (final AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<List<ReceptorSet>> listReceptorSets() {
    try {
      final ConnectUser connectUser = authentication.getCurrentUser();
      final List<ReceptorSet> receptorSets = receptorSetsService.getReceptorSets(connectUser);
      return responseService.toOkResponse(receptorSets);
    } catch (final AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  private ValidateResponse convert(final ImportParcelAggregate result) {
    final List<ValidationMessage> errors = responseService.convertToValidation(result.getExceptions());
    final List<ValidationMessage> warnings = responseService.convertToValidation(result.getWarnings());
    return new ValidateResponse()
        .successful(result.getExceptions().isEmpty())
        .errors(errors)
        .warnings(warnings);
  }

  @Override
  public Optional<NativeWebRequest> getRequest() {
    return ReceptorSetsApiDelegate.super.getRequest();
  }
}
