/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.resource;

import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.NativeWebRequest;

import nl.overheid.aerius.connectservice.api.v8.UserApiDelegate;
import nl.overheid.aerius.connectservice.model.GenerateApiKey;
import nl.overheid.aerius.connectservice.service.EmailMessageSenderService;
import nl.overheid.aerius.connectservice.service.LocaleService;
import nl.overheid.aerius.connectservice.service.ResponseService;
import nl.overheid.aerius.connectservice.service.UserService;
import nl.overheid.aerius.email.message.EmailMessage;
import nl.overheid.aerius.shared.Constants;
import nl.overheid.aerius.shared.domain.connect.ApiKeyExportData;
import nl.overheid.aerius.shared.domain.email.EmailMessagesEnum;
import nl.overheid.aerius.shared.domain.email.EmailType;
import nl.overheid.aerius.shared.domain.email.ReplacementToken;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

@Service
@Profile({"nl", "uk"})
public class UserResource implements UserApiDelegate {

  private static final Pattern EMAIL_PATTERN = Pattern.compile(Constants.VALID_EMAIL_ADDRESS_REGEX, Pattern.CASE_INSENSITIVE);

  private final ResponseService responseService;
  private final UserService userService;
  private final LocaleService localeService;
  private final EmailMessageSenderService emailMessageBuilderService;

  @Autowired
  UserResource(final ResponseService responseService, final UserService userService, final LocaleService localeService,
      final EmailMessageSenderService emailMessageSenderService) {
    this.responseService = responseService;
    this.userService = userService;
    this.localeService = localeService;
    this.emailMessageBuilderService = emailMessageSenderService;
  }

  @Override
  public ResponseEntity<Void> generateApiKey(final GenerateApiKey generateApiKey) {
    try {
      generateApiKey(generateApiKey.getEmail());
      return responseService.toOkResponse();
    } catch (final AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<Void> validateApiKey() {
    return responseService.toOkResponse();
  }

  private void generateApiKey(final String email) throws AeriusException {
    validateEmail(email);

    final String apiKey = userService.generateAPIKey(email);

    sendMail(email, apiKey);
  }

  /**
   * Validates the email address.
   * @param emailAddress email address
   * @throws AeriusException throws exception in case of validation errors
   */
  private static void validateEmail(final String emailAddress) throws AeriusException {
    if (emailAddress == null || emailAddress.length() == 0 || !EMAIL_PATTERN.matcher(emailAddress.trim()).matches()) {
      throw new AeriusException(AeriusExceptionReason.CONNECT_NO_VALID_EMAIL_SUPPLIED, emailAddress);
    }
  }

  private void sendMail(final String email, final String apiKey) throws AeriusException {
    final ApiKeyExportData data = new ApiKeyExportData(apiKey);

    data.setLocale(localeService.getLocale().getLanguage());
    data.setEmailAddress(email);
    emailMessageBuilderService.sendMailToUser(new ConnectApiKeyEmailMessage(data));
  }

  private static class ConnectApiKeyEmailMessage extends EmailMessage<ApiKeyExportData> {

    public ConnectApiKeyEmailMessage(final ApiKeyExportData data) {
      super(EmailType.CONNECT_API_KEY, data);
    }

    @Override
    public void populateStringReplacementTokens(final Map<ReplacementToken, String> map) {
      map.put(ReplacementToken.CONNECT_APIKEY, getData().getApiKey());
    }

    @Override
    public EmailMessagesEnum getMailContent() {
      return EmailMessagesEnum.CONNECT_APIKEY_CONFIRM_BODY;
    }

    @Override
    public EmailMessagesEnum getMailSubject() {
      return EmailMessagesEnum.CONNECT_APIKEY_CONFIRM_SUBJECT;
    }
  }

  @Override
  public Optional<NativeWebRequest> getRequest() {
    return UserApiDelegate.super.getRequest();
  }
}
