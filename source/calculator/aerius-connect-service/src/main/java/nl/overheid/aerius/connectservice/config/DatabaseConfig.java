/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.config;

import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import nl.overheid.aerius.calculation.NCACalculationOptionsUtil;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.CalculationInfoRepositoryBean;
import nl.overheid.aerius.db.calculator.CalculatorLimitsRepository;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.calculator.ShippingRepositoryBean;
import nl.overheid.aerius.db.common.AreaInformationSupplier;
import nl.overheid.aerius.db.common.ConstantRepositoryBean;
import nl.overheid.aerius.db.common.DataSourcePMF;
import nl.overheid.aerius.db.common.GeneralRepositoryBean;
import nl.overheid.aerius.db.common.MeteoRepositoryBean;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.db.common.ReceptorInfoRepositoryBean;
import nl.overheid.aerius.db.common.decision.DecisionFrameworkRepository;
import nl.overheid.aerius.db.common.procurement.ProcurementRepository;
import nl.overheid.aerius.db.common.results.ResultsSummaryRepository;
import nl.overheid.aerius.db.common.sector.SectorRepositoryBean;
import nl.overheid.aerius.db.connect.ConnectCalculationPointSetsRepositoryBean;
import nl.overheid.aerius.db.connect.ConnectUserRepositoryBean;
import nl.overheid.aerius.db.i18n.DBMessages;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

@Configuration
public class DatabaseConfig {

  private static final Logger LOG = LoggerFactory.getLogger(DatabaseConfig.class);

  private static final int RETRY_TIME_SECONDS = 15;
  // Retry for an arbitrary number of times (a few days)
  private static final int RETRIES = 50000;

  @Bean
  public PMF pmf(final DataSource dataSource) {
    final PMF pmf = new DataSourcePMF(dataSource);
    initializeMessages(pmf);
    return pmf;
  }

  @Bean
  public ConstantRepositoryBean constantRepository(final PMF pmf) {
    return new ConstantRepositoryBean(pmf);
  }

  @Bean
  public ConnectUserRepositoryBean connectUserRepository(final PMF pmf) {
    return new ConnectUserRepositoryBean(pmf);
  }

  @Bean
  public JobRepositoryBean jobRepository(final PMF pmf) {
    return new JobRepositoryBean(pmf);
  }

  @Bean
  public ConnectCalculationPointSetsRepositoryBean calculationPointSetsRepository(final PMF pmf) {
    return new ConnectCalculationPointSetsRepositoryBean(pmf);
  }

  @Bean
  public MeteoRepositoryBean meteoRepository(final PMF pmf) {
    return new MeteoRepositoryBean(pmf);
  }

  @Bean
  public CalculatorLimitsRepository calculatorLimitsRepository(final PMF pmf) {
    return new CalculatorLimitsRepository(pmf);
  }

  @Bean
  public ReceptorUtil receptorUtil(final PMF pmf) throws SQLException {
    final ReceptorGridSettings rgs = ReceptorGridSettingsRepository.getReceptorGridSettings(pmf);
    return new ReceptorUtil(rgs);
  }

  @Bean
  public ResultsSummaryRepository resultsSummaryRepository(final PMF pmf, final ReceptorUtil receptorUtil) {
    return new ResultsSummaryRepository(pmf, receptorUtil);
  }

  @Bean
  public ReceptorInfoRepositoryBean receptorInfoRepositor(final PMF pmf) {
    return new ReceptorInfoRepositoryBean(pmf);
  }

  @Bean
  public CalculationInfoRepositoryBean calculationInfoRepositor(final PMF pmf) {
    return new CalculationInfoRepositoryBean(pmf);
  }

  @Bean
  public ShippingRepositoryBean shippingRepository(final PMF pmf) {
    return new ShippingRepositoryBean(pmf);
  }

  @Bean
  public GeneralRepositoryBean generalRepository(final PMF pmf) {
    return new GeneralRepositoryBean(pmf);
  }

  @Bean
  public SectorRepositoryBean sectorRepository(final PMF pmf) {
    return new SectorRepositoryBean(pmf);
  }

  @Bean
  public NCACalculationOptionsUtil ncaCalculationOptionsUtil(final PMF pmf) {
    return new NCACalculationOptionsUtil(pmf);
  }

  @Bean
  public ProcurementRepository procurementRepository(final PMF pmf, final AreaInformationSupplier areaInformationSupplier) {
    return new ProcurementRepository(pmf, areaInformationSupplier);
  }

  @Bean
  public DecisionFrameworkRepository decisionFrameworkRepository(final PMF pmf, final AreaInformationSupplier areaInformationSupplier) {
    return new DecisionFrameworkRepository(pmf, areaInformationSupplier);
  }

  private static void initializeMessages(final PMF pmf) {
    boolean initialized = false;
    int retry = 0;
    while (!initialized && retry < RETRIES) {
      try {
        DBMessages.init(pmf);
        initialized = true;
      } catch (final RuntimeException e) {
        if (retry == 0) {
          LOG.error("Error initializing database messages, will keep on retrying", e);
        } else {
          LOG.debug("Still an error while initializing database messages", e);
        }
        retry++;
        try {
          LOG.info("Retrying initializing in {} seconds", RETRY_TIME_SECONDS);
          TimeUnit.SECONDS.sleep(RETRY_TIME_SECONDS);
        } catch (final InterruptedException e1) {
          LOG.error("Interrupted while trying to initialize");
          Thread.currentThread().interrupt();
        }
      }
    }
    if (!initialized) {
      LOG.error("Done with retrying, initializing failed!");
    }
  }

}
