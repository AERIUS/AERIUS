/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import nl.overheid.aerius.connectservice.model.ValidationMessage;
import nl.overheid.aerius.shared.exception.AeriusException;

@Service
public class ResponseService {

  private static final Logger LOG = LoggerFactory.getLogger(ResponseService.class);

  private final MessagesService messagesService;

  @Autowired
  public ResponseService(final MessagesService messagesService) {
    this.messagesService = messagesService;
  }

  public ResponseEntity<Void> toOkResponse() {
    return ResponseEntity
        .status(HttpStatus.OK)
        .build();
  }

  public <T> ResponseEntity<T> toOkResponse(final T result) {
    return toOkResponse(result, new HttpHeaders());
  }

  public <T> ResponseEntity<T> toOkResponse(final T result, final HttpHeaders headers) {
    return ResponseEntity
        .status(HttpStatus.OK)
        .headers(headers)
        .body(result);
  }

  public <T> ResponseEntity<T> toOkResponseOptional(final Optional<T> result, final String reasonWhenNotPresent) {
    return result.map(res -> ResponseEntity
        .status(HttpStatus.OK)
        .body(res)).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, reasonWhenNotPresent));
  }

  public ResponseStatusException toResponseStatusException(final Exception e) {
    final HttpStatus status = e instanceof AeriusException && !((AeriusException) e).isInternalError()
        ? HttpStatus.BAD_REQUEST
        : HttpStatus.INTERNAL_SERVER_ERROR;
    if (status == HttpStatus.INTERNAL_SERVER_ERROR) {
      LOG.error("Returning internal server error to user", e);
    } else {
      LOG.info("Bad request by user: {}", e.getMessage());
    }
    final String reference = e instanceof AeriusException ? (" (" + ((AeriusException) e).getReference() + ")") : "";
    return new ResponseStatusException(status, messagesService.getExceptionMessage(e) + reference);
  }

  public List<ValidationMessage> convertToValidation(final List<AeriusException> toBeConverted) {
    return toBeConverted.stream().map(this::convertToValidation).collect(Collectors.toList());
  }

  public ValidationMessage convertToValidation(final AeriusException e) {
    return new ValidationMessage()
        .code(e.getReason().getErrorCode())
        .message(messagesService.getExceptionMessage(e));
  }

}
