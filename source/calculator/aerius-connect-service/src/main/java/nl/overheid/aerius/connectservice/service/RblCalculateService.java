/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.overheid.aerius.connectservice.model.RblCalculationOptions;
import nl.overheid.aerius.connectservice.model.RblCalculationOptions.NationalSrm2NetworkVersionEnum;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.ConstantRepositoryBean;
import nl.overheid.aerius.db.connect.ConnectCalculationPointSetsRepositoryBean;
import nl.overheid.aerius.server.service.export.ExportTaskClientBean;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.QueueEnum;
import nl.overheid.aerius.validation.RblCohesionValidator;

/**
 * Service implementation for RBL API.
 */
@Service
public class RblCalculateService extends AbstractCalculateService<RblCalculationOptions, Void> {

  @Autowired
  RblCalculateService(final ExportTaskClientBean exportTaskclient, final JobRepositoryBean jobRepository,
      final ConnectCalculationPointSetsRepositoryBean pointSetsRepository, final ConstantRepositoryBean constantRepository,
      final ProxyFileService fileService, final LocaleService localeService, final EmissionsSourcesService emissionsSourcesService) {
    super(exportTaskclient, jobRepository, pointSetsRepository, constantRepository, fileService, localeService, emissionsSourcesService);
  }

  @Override
  public void validateInput(final ScenarioConstruction scenarioConstruction) {
    new RblCohesionValidator().checkCohesion(scenarioConstruction.getScenario(), scenarioConstruction.getErrors(),
        scenarioConstruction.getWarnings());
  }

  @Override
  protected JobType getJobType(final RblCalculationOptions options) {
    return JobType.CALCULATION;
  }

  @Override
  protected Optional<String> getJobName(final RblCalculationOptions options) {
    return Optional.of(options.getName());
  }

  @Override
  protected boolean useCustomPoints(final RblCalculationOptions options) {
    return true;
  }

  @Override
  protected String getReceptorSetName(final RblCalculationOptions options) {
    return null;
  }

  @Override
  protected Optional<Integer> getOverrideYear(final RblCalculationOptions options) {
    return Optional.ofNullable(options.getCalculationYear());
  }

  @Override
  protected void validate(final Scenario scenario, final RblCalculationOptions options, final Void additionalData)
      throws AeriusException {
    // NO-OP
  }

  @Override
  protected void updateScenarioOptions(final Scenario scenario, final RblCalculationOptions options) throws AeriusException {
    final NationalSrm2NetworkVersionEnum monitorVersion = options.getNationalSrm2NetworkVersion();

    if (monitorVersion != null) {
      scenario.getOptions().getRblCalculationOptions().setMonitorSrm2Year(monitorVersion.getValue().intValue());
    }
  }

  @Override
  protected void setExportProperties(final ExportProperties exportProperties, final RblCalculationOptions options) {
    exportProperties.setExportType(ExportType.CIMLK_CSV);
    exportProperties.setName(options.getName());
  }

  @Override
  protected QueueEnum determineQueue(final RblCalculationOptions options) {
    return QueueEnum.CONNECT_GML_EXPORT;
  }

  @Override
  protected boolean shouldSendEmail(final RblCalculationOptions options) {
    return Boolean.TRUE.equals(options.getSendEmail());
  }
}
