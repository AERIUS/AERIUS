/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.domain;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;

/**
 * Wrapper class for objects that should be supplied for a UI calculation.
 */
public class UiCalculationRequest {

  private Theme theme;
  private Scenario scenario;
  private ExportOptions exportOptions;
  private String jobKey;

  public Theme getTheme() {
    return theme;
  }

  public void setTheme(final Theme theme) {
    this.theme = theme;
  }

  public Scenario getScenario() {
    return scenario;
  }

  public void setScenario(final Scenario scenario) {
    this.scenario = scenario;
  }

  public ExportOptions getExportOptions() {
    return exportOptions;
  }

  public void setExportOptions(final ExportOptions exportOptions) {
    this.exportOptions = exportOptions;
  }

  public String getJobKey() {
    return jobKey;
  }

  public void setJobKey(final String jobKey) {
    this.jobKey = jobKey;
  }
}
