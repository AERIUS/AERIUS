/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.config;

import java.sql.SQLException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import nl.overheid.aerius.controller.ImaerController;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.gml.GMLWriter;
import nl.overheid.aerius.importer.PermitGMLWriter;

@Configuration
public class ImaerConfig {

  @Bean
  public ImaerController imaerController(final PMF pmf) {
    return new ImaerController(pmf);
  }

  @Bean
  public GMLWriter gmlWriter(final PMF pmf) throws SQLException {
    return new PermitGMLWriter(pmf);
  }

}
