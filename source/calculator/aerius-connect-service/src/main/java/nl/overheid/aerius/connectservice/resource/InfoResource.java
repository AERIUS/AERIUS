/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.resource;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import nl.overheid.aerius.AeriusVersion;
import nl.overheid.aerius.connectservice.api.v8.InfoApiDelegate;
import nl.overheid.aerius.connectservice.model.VersionInfoResponse;
import nl.overheid.aerius.connectservice.service.ResponseService;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

@Service
public class InfoResource implements InfoApiDelegate {

  private static final Logger LOG = LoggerFactory.getLogger(InfoResource.class);

  private final ResponseService responseService;
  private final PMF pmf;

  @Autowired
  InfoResource(final ResponseService responseService, final PMF pmf) {
    this.responseService = responseService;
    this.pmf = pmf;
  }

  @Override
  public ResponseEntity<VersionInfoResponse> getInfoVersion() {
    final VersionInfoResponse versionInfo = new VersionInfoResponse();

    versionInfo.setAeriusVersion(AeriusVersion.getVersionNumber());
    try {
      versionInfo.setDatabaseVersion(pmf.getDatabaseVersion());
    } catch (final SQLException e) {
      LOG.error("Could not get database version", e);
      throw responseService.toResponseStatusException(new AeriusException(AeriusExceptionReason.SQL_ERROR));
    }
    return responseService.toOkResponse(versionInfo);
  }
}
