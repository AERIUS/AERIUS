/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.auth;

import java.util.Optional;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import nl.overheid.aerius.connectservice.service.UserService;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.exception.AeriusException;

@Component
public class ApiKeyManager implements AuthenticationManager {

  private final UserService userService;

  public ApiKeyManager(final UserService userService) {
    this.userService = userService;
  }

  @Override
  public Authentication authenticate(final Authentication authentication) {
    final String principal = (String) authentication.getPrincipal();

    Optional<ConnectUser> connectUser;
    try {
      connectUser = userService.getUser(principal);
    } catch (final AeriusException e) {
      throw new InternalAuthenticationServiceException("The API key could not be validated at this moment");
    }
    if (connectUser.isEmpty()) {
      throw new BadCredentialsException("The API key was not found.");
    } else if (!connectUser.get().isEnabled()) {
      throw new DisabledException("The API key was disabled");
    } else {
      authentication.setAuthenticated(true);
      return authentication;

    }
  }

}
