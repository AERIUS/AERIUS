/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.util.Locale;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.overheid.aerius.calculation.OwN2000CalculationOptionsUtil;
import nl.overheid.aerius.connectservice.model.Calculation;
import nl.overheid.aerius.connectservice.model.CalculationResult;
import nl.overheid.aerius.connectservice.model.CalculationResultSet;
import nl.overheid.aerius.connectservice.model.OwN2000RegisterCalculationOptions;
import nl.overheid.aerius.connectservice.model.PermitArea;
import nl.overheid.aerius.connectservice.model.RegisterCalculationResults;
import nl.overheid.aerius.connectservice.model.ResultType;
import nl.overheid.aerius.connectservice.util.CalculationJobTypeValidator;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.ConstantRepositoryBean;
import nl.overheid.aerius.db.connect.ConnectCalculationPointSetsRepositoryBean;
import nl.overheid.aerius.server.service.export.ExportTaskClientBean;
import nl.overheid.aerius.shared.FileServerExpireTag;
import nl.overheid.aerius.shared.FileServerFile;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.register.RegisterCalculation;
import nl.overheid.aerius.shared.domain.register.RegisterCalculationResult;
import nl.overheid.aerius.shared.domain.register.RegisterCalculationResultSet;
import nl.overheid.aerius.shared.domain.register.RegisterPermitAreaResult;
import nl.overheid.aerius.shared.domain.register.RegisterResults;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.taskmanager.client.QueueEnum;

@Service
public class RegisterOwN2000PermitService extends AbstractCalculateService<OwN2000RegisterCalculationOptions, RegisterCalculationResults> {

  @Autowired
  RegisterOwN2000PermitService(final ExportTaskClientBean exportTaskclient, final JobRepositoryBean jobRepository,
      final ConnectCalculationPointSetsRepositoryBean pointSetsRepository, final ConstantRepositoryBean constantRepository,
      final ProxyFileService fileService, final LocaleService localeService, final EmissionsSourcesService emissionsSourcesService) {
    super(exportTaskclient, jobRepository, pointSetsRepository, constantRepository, fileService, localeService, emissionsSourcesService);
  }

  @Override
  public String calculate(final Scenario scenario, final OwN2000RegisterCalculationOptions options) throws AeriusException {
    // Should not call this specific method for OwN2000 calculations
    throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
  }

  @Override
  protected JobType getJobType(final OwN2000RegisterCalculationOptions options) {
    return OwN2000RegisterCalculationOptions.OutputTypeEnum.JSON == options.getOutputType() ? JobType.CALCULATION : JobType.REPORT;
  }

  @Override
  protected Optional<String> getJobName(final OwN2000RegisterCalculationOptions options) {
    return Optional.ofNullable(options.getName());
  }

  @Override
  protected boolean useCustomPoints(final OwN2000RegisterCalculationOptions options) {
    // Default for OwN2000 calculation is not using custom points, but letting AERIUS determine receptors.
    return false;
  }

  @Override
  protected String getReceptorSetName(final OwN2000RegisterCalculationOptions options) {
    // Default for OwN2000 calculation is not using custom points, but letting AERIUS determine receptors.
    return "";
  }

  @Override
  protected Optional<Integer> getOverrideYear(final OwN2000RegisterCalculationOptions options) {
    return Optional.ofNullable(options.getCalculationYear());
  }

  @Override
  protected void updateScenarioOptions(final Scenario scenario, final OwN2000RegisterCalculationOptions options) throws AeriusException {
    final CalculationSetOptions cso = OwN2000CalculationOptionsUtil.createOwN2000CalculationSetOptions();

    cso.setCalculationMethod(CalculationMethod.FORMAL_ASSESSMENT);
    cso.setCalculationJobType(CalculationJobType.PROCESS_CONTRIBUTION);
    scenario.setOptions(cso);
  }

  @Override
  protected void setExportProperties(final ExportProperties exportProperties, final OwN2000RegisterCalculationOptions options) {
    final ExportType exportType;
    if (options.getOutputType() == OwN2000RegisterCalculationOptions.OutputTypeEnum.JSON) {
      exportType = ExportType.REGISTER_JSON_PROJECT_CALCULATION;
    } else {
      exportType = ExportType.REGISTER_PDF_PAA_DEMAND;
    }
    exportProperties.setName(options.getName());
    exportProperties.setExportType(exportType);
  }

  @Override
  protected boolean shouldSendEmail(final OwN2000RegisterCalculationOptions options) {
    return false;
  }

  @Override
  protected QueueEnum determineQueue(final OwN2000RegisterCalculationOptions options) {
    return QueueEnum.CONNECT_REGISTER_EXPORT;
  }

  public String saveCalculation(final ConnectUser connectUser, final Scenario scenario, final OwN2000RegisterCalculationOptions options,
      final RegisterCalculationResults calculationResults) throws AeriusException {
    if (calculationResults.getCalculations() == null) {
      throw new AeriusException(AeriusExceptionReason.CALCULATION_PAA_PERMIT_CALCULATION_RESULTS_MISSING);
    }
    return calculate(Optional.of(connectUser), scenario, options, calculationResults);
  }

  @Override
  protected void sendTask(final String jobKey, final ExportProperties exportProperties, final Scenario scenario,
      final OwN2000RegisterCalculationOptions options, final RegisterCalculationResults calculationResults) throws AeriusException {
    if (calculationResults != null) {
      final RegisterResults registerResults = convert(calculationResults);
      fileService.writeJson(jobKey, FileServerFile.RESULTS, FileServerExpireTag.SHORT, registerResults);
    }
    super.sendTask(jobKey, exportProperties, scenario, options, calculationResults);
  }

  private static RegisterResults convert(final RegisterCalculationResults calculationResults) {
    return new RegisterResults(
        calculationResults.getCalculations().stream()
            .map(RegisterOwN2000PermitService::convertCalculation)
            .toList(),
        calculationResults.getPermitAreas().stream()
            .map(RegisterOwN2000PermitService::convertAreaResult)
            .toList());
  }

  private static RegisterCalculation convertCalculation(final Calculation calculation) {
    return new RegisterCalculation(calculation.getScenario(),
        calculation.getCalculationResultSets().stream()
            .map(RegisterOwN2000PermitService::convertCalculationResultSet)
            .toList());
  }

  private static RegisterCalculationResultSet convertCalculationResultSet(final CalculationResultSet resultSet) {
    final Substance substance = Optional.ofNullable(resultSet.getSubstance())
        .map(nl.overheid.aerius.connectservice.model.Substance::name)
        .map(Substance::safeValueOf)
        .orElse(null);
    final EmissionResultType resultType = Optional.ofNullable(resultSet.getResultType())
        .map(ResultType::name)
        .map(EmissionResultType::safeValueOf)
        .orElse(null);
    return new RegisterCalculationResultSet(substance, resultType,
        resultSet.getCalculationResults().stream()
            .map(RegisterOwN2000PermitService::convertCalculationResult)
            .toList());
  }

  private static RegisterCalculationResult convertCalculationResult(final CalculationResult result) {
    return new RegisterCalculationResult(result.getReceptorId().intValue(), result.getResult());
  }

  private static RegisterPermitAreaResult convertAreaResult(final PermitArea permitArea) {
    return new RegisterPermitAreaResult(permitArea.getAreaId().intValue(), permitArea.getDemandCheck().doubleValue());
  }

  @Override
  protected void validate(final Scenario scenario, final OwN2000RegisterCalculationOptions options,
      final RegisterCalculationResults calculationResults) throws AeriusException {
    CalculationJobTypeValidator.validate(scenario,
        scenario.getSituations().size() == 1 ? CalculationJobType.SINGLE_SCENARIO : CalculationJobType.PROCESS_CONTRIBUTION);

    if (calculationResults != null) {
      validateCalculationResults(calculationResults);
    }
  }

  private static void validateCalculationResults(final RegisterCalculationResults calculationResults) throws AeriusException {
    if (calculationResults.getCalculations().isEmpty()) {
      throw new AeriusException(AeriusExceptionReason.CALCULATION_PAA_PERMIT_CALCULATION_RESULTS_MISSING);
    }

    final long proposedResults = calculationResults.getCalculations().stream()
        .filter(situation -> situation.getScenario().toUpperCase(Locale.ROOT).equals(SituationType.PROPOSED.name())).count();
    final long referenceResults = calculationResults.getCalculations().stream()
        .filter(situation -> situation.getScenario().toUpperCase(Locale.ROOT).equals(SituationType.REFERENCE.name())).count();
    final long offSiteReductionResults = calculationResults.getCalculations().stream()
        .filter(situation -> situation.getScenario().toUpperCase(Locale.ROOT).equals(SituationType.OFF_SITE_REDUCTION.name())).count();

    if (proposedResults > 1 || referenceResults > 1 || offSiteReductionResults > 1) {
      throw new AeriusException(AeriusExceptionReason.CALCULATION_PAA_PERMIT_CALCULATION_RESULTS_INCORRECT_SITUATIONS_COUNT, "1");
    }
  }
}
