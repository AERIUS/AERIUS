/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.util;

import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import nl.overheid.aerius.connectservice.domain.UiCalculationRequest;
import nl.overheid.aerius.connectservice.resource.FilesResource;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.shared.FileServerFile;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculation;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioResults;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituationResults;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.ScenarioObjectMapperUtil;

/**
 * Component with util methods to get data related to a scenario.
 */
@Component
public class ScenarioUtil {

  private static final Logger LOGGER = LoggerFactory.getLogger(ScenarioUtil.class);

  private final JobRepositoryBean jobRepository;
  private final FilesResource filesResource;

  public ScenarioUtil(final JobRepositoryBean jobRepository, final FilesResource filesResource) {
    this.jobRepository = jobRepository;
    this.filesResource = filesResource;
  }

  /**
   * Gets the scenario for the given job from the file server.
   *
   * @param options
   * @return Scenario retrieved from file server
   * @throws AeriusException
   */
  public Scenario getScenario(final UiCalculationRequest options) throws AeriusException {
    final ResponseEntity<Resource> file = filesResource.getFile(FileServerFile.DATA, options.getJobKey());
    final Resource body = file.getBody();

    if (file.getStatusCode() == HttpStatus.OK && body != null) {
      try (InputStream fsis = body.getInputStream()) {
        return ScenarioObjectMapperUtil.getScenarioObjectMapper().readValue(fsis, Scenario.class);
      } catch (final IOException e) {
        LOGGER.warn("Could not get scenario for job key {} from file server", options.getJobKey(),  e);
      }
    } else {
      LOGGER.debug("Could not get scenario for job key {} from file server. Status code: {}", options.getJobKey(), file.getStatusCode());
    }
    return null;
  }

  /**
   * Gets the ScenarioResult information for the give job key from the database.
   *
   * @param jobKey
   * @return
   * @throws AeriusException
   */
  public ScenarioResults getScenarioResults(final String jobKey) throws AeriusException {
    final SituationCalculations situationCalculations = jobRepository.getSituationCalculations(jobKey);
    final ScenarioResults scenarioResults = new ScenarioResults();

    for (final SituationCalculation situationCalculation : situationCalculations) {
      final ScenarioSituationResults situationResults = new ScenarioSituationResults();

      situationResults.setCalculationId(situationCalculation.getCalculationId());
      scenarioResults.getResultsPerSituation().put(situationCalculation.getSituationId(), situationResults);
    }
    return scenarioResults;
  }
}
