/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.overheid.aerius.db.common.decision.DecisionFrameworkRepository;
import nl.overheid.aerius.db.common.procurement.ProcurementRepository;
import nl.overheid.aerius.db.common.results.ResultsSummaryRepository;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.sector.category.SimpleCategory;
import nl.overheid.aerius.shared.domain.summary.AssessmentAreaThresholdResults;
import nl.overheid.aerius.shared.domain.summary.DecisionFrameworkResults;
import nl.overheid.aerius.shared.domain.summary.JobSummary;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SituationResultsSummary;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.domain.summary.SummaryRequest;
import nl.overheid.aerius.shared.domain.summary.ThresholdResult;
import nl.overheid.aerius.shared.exception.AeriusException;

@Service
public class UiResultsService {

  private static final String UNKNOWN = "Unknown";

  private final ResultsSummaryRepository resultsSummaryRepository;
  private final AreaInformationService areaInformationService;
  private final ProcurementRepository procurementRepository;
  private final DecisionFrameworkRepository decisionFrameworkRepository;

  @Autowired
  public UiResultsService(final ResultsSummaryRepository resultsSummaryRepository, final AreaInformationService areaInformationService,
      final ProcurementRepository procurementRepository, final DecisionFrameworkRepository decisionFrameworkRepository) {
    this.resultsSummaryRepository = resultsSummaryRepository;
    this.areaInformationService = areaInformationService;
    this.procurementRepository = procurementRepository;
    this.decisionFrameworkRepository = decisionFrameworkRepository;
  }

  public SituationResultsSummary determineSituationResultsSummary(final SituationCalculations situationCalculations,
      final SummaryRequest summaryRequest) throws AeriusException {
    SituationResultsSummary summary;
    if (summaryRequest.getHexagonType() == SummaryHexagonType.CUSTOM_CALCULATION_POINTS) {
      summary = determineCustomCalculationPointResults(situationCalculations, summaryRequest);
    } else if (summaryRequest.getResultType() == ScenarioResultType.DEPOSITION_SUM) {
      summary = determineDepositionSumResults(situationCalculations, summaryRequest);
    } else {
      summary = determineReceptorResults(situationCalculations, summaryRequest);
    }
    return summary;
  }

  private SituationResultsSummary determineReceptorResults(final SituationCalculations situationCalculations,
      final SummaryRequest summaryRequest) throws AeriusException {
    return resultsSummaryRepository.determineReceptorResultsSummary(situationCalculations, summaryRequest,
        areaInformationService);

  }

  private SituationResultsSummary determineDepositionSumResults(final SituationCalculations situationCalculations,
      final SummaryRequest summaryRequest) throws AeriusException {
    final SummaryRequest situationResultRequest = new SummaryRequest(
        ScenarioResultType.SITUATION_RESULT,
        summaryRequest.getJobId(),
        summaryRequest.getCalculationId(),
        summaryRequest.getHexagonType(),
        summaryRequest.getOverlappingHexagonType(),
        summaryRequest.getEmissionResultKey(),
        null);
    final SituationResultsSummary summary = resultsSummaryRepository.determineReceptorResultsSummary(situationCalculations, situationResultRequest,
        areaInformationService);
    if (summaryRequest.getProcurementPolicy() != null) {
      summary.getMarkers().clear();
      procurementRepository.addProcurementStatistics(summary, summaryRequest.getProcurementPolicy());
    }
    return summary;
  }

  private SituationResultsSummary determineCustomCalculationPointResults(final SituationCalculations situationCalculations,
      final SummaryRequest summaryRequest) throws AeriusException {
    return resultsSummaryRepository.determineCustomCalculationPointResultsSummary(situationCalculations, summaryRequest);
  }

  public JobSummary determineJobSummary(final int jobId) throws AeriusException {
    return resultsSummaryRepository.determineJobSummary(jobId, areaInformationService);
  }

  public DecisionFrameworkResults determineDecisionFrameworkResults(final int jobId, final int proposedCalculationId) throws AeriusException {
    final Map<EmissionResultKey, ThresholdResult> dmtResults =
        decisionFrameworkRepository.getDecisionMakingThresholdResults(jobId, proposedCalculationId);
    final SimpleCategory developmentPressureClass = decisionFrameworkRepository.getDevelopmentPressureClass(jobId);
    final List<AssessmentAreaThresholdResults> srtResults =
        decisionFrameworkRepository.getAssessmentAreaThresholdResults(jobId, proposedCalculationId);
    return new DecisionFrameworkResults(dmtResults, developmentPressureClass == null ? UNKNOWN : developmentPressureClass.getDescription(),
        srtResults);
  }

}
