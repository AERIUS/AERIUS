/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.domain;

import java.util.List;

import nl.overheid.aerius.importer.domain.SituationWithFileId;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;

/**
 * Wrapper class for objects that should be supplied for a UI import results job.
 */
public class UiImportResultsRequest {

  private Theme theme;
  private Scenario scenario;
  private List<SituationWithFileId> situationWithFileIds;
  private String archiveContributionFileId;

  public Theme getTheme() {
    return theme;
  }

  public void setTheme(final Theme theme) {
    this.theme = theme;
  }

  public Scenario getScenario() {
    return scenario;
  }

  public void setScenario(final Scenario scenario) {
    this.scenario = scenario;
  }

  public List<SituationWithFileId> getSituationWithFileIds() {
    return situationWithFileIds;
  }

  public void setSituationWithFileIds(final List<SituationWithFileId> situationWithFileIds) {
    this.situationWithFileIds = situationWithFileIds;
  }

  public String getArchiveContributionFileId() {
    return archiveContributionFileId;
  }

  public void setArchiveContributionFileId(final String archiveContributionFileId) {
    this.archiveContributionFileId = archiveContributionFileId;
  }

}
