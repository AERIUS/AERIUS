/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import org.springframework.stereotype.Component;

import nl.aerius.taskmanager.client.mq.RabbitMQWorkerMonitor.RabbitMQWorkerObserver;
import nl.overheid.aerius.shared.domain.ServerUsage;
import nl.overheid.aerius.taskmanager.client.WorkerType;

@Component
public class ServerUsageService implements RabbitMQWorkerObserver {

  private static final double VERY_HIGH_THRESHOLD = 0.9;
  private static final double ABOVE_AVERAGE_THRESHOLD = 0.75;

  private ServerUsage currentServerUsage = ServerUsage.UNKNOWN;

  @Override
  public void updateWorkers(final String workerQueueName, final int size, final int utilisation) {
    if (WorkerType.CONNECT.type().getWorkerQueueName().equalsIgnoreCase(workerQueueName)) {
      if (size == 0) {
        currentServerUsage = ServerUsage.UNKNOWN;
      } else if (utilisation >= size * VERY_HIGH_THRESHOLD) {
        currentServerUsage = ServerUsage.VERY_HIGH;
      } else if (utilisation >= size * ABOVE_AVERAGE_THRESHOLD) {
        currentServerUsage = ServerUsage.ABOVE_AVERAGE;
      } else {
        currentServerUsage = ServerUsage.NORMAL;
      }
    }
  }

  public ServerUsage getCurrentServerUsage() {
    return currentServerUsage;
  }

}
