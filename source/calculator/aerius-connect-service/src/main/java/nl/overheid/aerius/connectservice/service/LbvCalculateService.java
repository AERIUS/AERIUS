/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.connectservice.service;

import java.util.Collections;
import java.util.Optional;

import org.springframework.stereotype.Service;

import nl.overheid.aerius.calculation.OwN2000CalculationOptionsUtil;
import nl.overheid.aerius.connectservice.model.LbvCalculationOptions;
import nl.overheid.aerius.connectservice.util.CalculationJobTypeValidator;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.ConstantRepositoryBean;
import nl.overheid.aerius.db.connect.ConnectCalculationPointSetsRepositoryBean;
import nl.overheid.aerius.server.service.export.ExportTaskClientBean;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.QueueEnum;

@Service
public class LbvCalculateService extends AbstractCalculateService<LbvCalculationOptions, Void> {
  LbvCalculateService(final ExportTaskClientBean exportTaskclient, final JobRepositoryBean jobRepository,
      final ConnectCalculationPointSetsRepositoryBean pointSetsRepository,
      final ConstantRepositoryBean constantRepository, final ProxyFileService fileService, final LocaleService localeService,
      final EmissionsSourcesService emissionsSourcesService) {
    super(exportTaskclient, jobRepository, pointSetsRepository, constantRepository, fileService, localeService, emissionsSourcesService);
  }

  @Override
  protected JobType getJobType(final LbvCalculationOptions options) {
    return JobType.REPORT;
  }

  @Override
  protected Optional<String> getJobName(final LbvCalculationOptions options) {
    return Optional.ofNullable(options.getName());
  }

  @Override
  protected boolean useCustomPoints(final LbvCalculationOptions options) {
    return false;
  }

  @Override
  protected String getReceptorSetName(final LbvCalculationOptions options) {
    return null;
  }

  @Override
  protected Optional<Integer> getOverrideYear(final LbvCalculationOptions options) {
    return Optional.ofNullable(options.getCalculationYear());
  }

  @Override
  protected void validate(final Scenario scenario, final LbvCalculationOptions options, final Void additionalData)
      throws AeriusException {
    CalculationJobTypeValidator.validate(scenario);
  }

  @Override
  protected void updateScenarioOptions(final Scenario scenario, final LbvCalculationOptions options) throws AeriusException {
    final CalculationSetOptions calculationSetOptions = OwN2000CalculationOptionsUtil.createOwN2000CalculationSetOptions();
    calculationSetOptions.setCalculationJobType(CalculationJobType.DEPOSITION_SUM);
    scenario.setOptions(calculationSetOptions);
  }

  @Override
  protected void setExportProperties(final ExportProperties exportProperties, final LbvCalculationOptions options) {
    exportProperties.setExportType(ExportType.PDF_PROCUREMENT_POLICY);
    exportProperties.setName(options.getName());
    exportProperties.setAppendices(Collections.emptySet());
  }

  @Override
  protected boolean shouldSendEmail(final LbvCalculationOptions options) {
    return options.getSendEmail();
  }

  @Override
  protected QueueEnum determineQueue(final LbvCalculationOptions options) {
    return QueueEnum.CALCULATOR_PP_EXPORT;
  }
}
