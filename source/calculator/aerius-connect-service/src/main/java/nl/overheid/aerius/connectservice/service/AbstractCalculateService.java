/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.io.IOException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.connectservice.util.ScenarioSanitationUtil;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.ConstantRepositoryBean;
import nl.overheid.aerius.db.connect.ConnectCalculationPointSetsRepositoryBean;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.server.service.export.ExportTaskClientBean;
import nl.overheid.aerius.shared.FileServerExpireTag;
import nl.overheid.aerius.shared.FileServerFile;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.domain.export.ExportData.ExportAdditionalOptions;
import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.taskmanager.client.QueueEnum;
import nl.overheid.aerius.util.LineSimplifier;

/**
 * Abstract base class for CalculateService implementations.
 * @param <T> Type of the options
 * @param <S> Type of additional data to process
 */
abstract class AbstractCalculateService<T, S> implements CalculateService<T> {

  private static final Logger LOG = LoggerFactory.getLogger(AbstractCalculateService.class);

  protected final ConstantRepositoryBean constantRepository;
  protected final ExportTaskClientBean exportTaskclient;
  protected final JobRepositoryBean jobRepository;
  private final ConnectCalculationPointSetsRepositoryBean pointSetsRepository;
  protected final ProxyFileService fileService;
  private final LocaleService localeService;

  private final EmissionsSourcesService emissionsSourcesService;

  AbstractCalculateService(final ExportTaskClientBean exportTaskclient, final JobRepositoryBean jobRepository,
      final ConnectCalculationPointSetsRepositoryBean pointSetsRepository, final ConstantRepositoryBean constantRepository,
      final ProxyFileService fileService, final LocaleService localeService, final EmissionsSourcesService emissionsSourcesService) {
    this.exportTaskclient = exportTaskclient;
    this.jobRepository = jobRepository;
    this.pointSetsRepository = pointSetsRepository;
    this.constantRepository = constantRepository;
    this.fileService = fileService;
    this.localeService = localeService;
    this.emissionsSourcesService = emissionsSourcesService;
  }

  public String calculate(final Scenario scenario, final T options) throws AeriusException {
    return calculate(Optional.empty(), scenario, options, null);
  }

  @Override
  public String calculate(final ConnectUser connectUser, final Scenario scenario, final T options) throws AeriusException {
    return calculate(Optional.of(connectUser), scenario, options, null);
  }

  protected String calculate(final Optional<ConnectUser> connectUser, final Scenario scenario, final T options,
      final S additionalData) throws AeriusException {
    final ExportProperties exportProperties = toExportProperties(connectUser, options);

    updateScenario(scenario, options);
    updateScenarioOptions(scenario, options);
    updateCustomPoints(scenario, connectUser, options);
    validate(scenario, options);
    validate(scenario, options, additionalData);

    final String jobKey = createJob(connectUser, options);
    return sendTask(scenario, options, additionalData, exportProperties, jobKey);
  }

  protected String sendTask(final Scenario scenario, final T options, final S additionalData, final ExportProperties exportProperties,
      final String jobKey) throws AeriusException {
    try {
      sendTask(jobKey, exportProperties, scenario, options, additionalData);
      return jobKey;
    } catch (final RuntimeException e) {
      jobRepository.setErrorMessage(jobKey, e.getCause() == null ? e.getMessage() : e.getCause().getMessage());
      LOG.warn("Sending job {} failed with RuntimeException.", jobKey, e);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    } catch (final AeriusException e) {
      jobRepository.setErrorMessage(jobKey, e.getMessage());
      LOG.warn("Sending job {} failed with AeriusException.", jobKey, e);
      throw e;
    }
  }

  /**
   * Method to return the type of the job derived from the options.
   *
   * @param options calculation options
   * @return job type
   */
  protected abstract JobType getJobType(T options);

  /**
   * Method to return an optional name of the job.
   *
   * @param options calculation options
   * @return optional job name
   */
  protected abstract Optional<String> getJobName(T options);

  /**
   * Method to indicate if a Job should be created to be protected from automatic deletion. Default is false.
   *
   * @param options calculation options
   * @return true if job should be protected form automatic deletion
   */
  protected boolean isProtectJob(final T options) {
    return false;
  }

  /**
   * Method to indicate if custom calculation points are to be included in the calculation.
   *
   * @param options calculation options
   * @return true if custom points should be processed
   */
  protected abstract boolean useCustomPoints(final T options);

  /**
   * Method to indicate if custom receptors are to be included in the calculation.
   *
   * @param options calculation options
   * @return true if custom receptors should be processed
   */
  protected boolean useCustomReceptors(final T options) {
    return false;
  }

  /**
   * Returns the name of the receptor set if a receptor set is given.
   *
   * @param options calculation options
   * @return name of the receptor set if a receptor set is given
   */
  protected abstract String getReceptorSetName(final T options);

  /**
   * Optional return the calculation year if the year is passed as parameter in the options.
   *
   * @param options options containing optional the calculation year
   * @return Optional overriding calculation year
   */
  protected abstract Optional<Integer> getOverrideYear(final T options);

  /**
   * Perform additional validations of the input options, in combination with the scenario.
   *
   * @param scenario scenario
   * @param options options to check
   * @param additionalData additional data to process
   * @throws AeriusException throws an exception if validation of options fails.
   */
  protected abstract void validate(final Scenario scenario, final T options, S additionalData) throws AeriusException;

  /**
   * Set the input options in the scenario.
   *
   * @param scenario scenario to set the options in
   * @param options input options to set in the scenario
   * @throws AeriusException
   */
  protected abstract void updateScenarioOptions(final Scenario scenario, final T options) throws AeriusException;

  /**
   * Return true if an email should be sent to the user when job completed
   *
   * @param options options containing information related to sending email
   * @return true if email should be send
   */
  protected abstract boolean shouldSendEmail(T options);

  /**
   * Set additional export properties based on information in the given options.
   *
   * @param exportProperties export properties to enhance
   * @param options options from service
   */
  protected abstract void setExportProperties(final ExportProperties exportProperties, final T options);

  /**
   * Returns the queue the task should be put on.
   *
   * @param options options to use to determine queue name
   * @return name of the queue the task should be put on
   */
  protected abstract QueueEnum determineQueue(final T options);

  protected String createJob(final Optional<ConnectUser> connectUser, final T options) throws AeriusException {
    final String jobKey;
    if (connectUser.isPresent()) {
      jobKey = jobRepository.createJob(connectUser.get(), getJobType(options), getJobName(options), isProtectJob(options));
    } else {
      jobKey = jobRepository.createJob(getJobType(options));
    }
    return jobKey;
  }

  private void updateScenario(final Scenario scenario, final T options) throws AeriusException {
    final Optional<Integer> overrideYear = getOverrideYear(options);
    if (overrideYear.isPresent()) {
      scenario.getSituations().forEach(situation -> situation.setYear(overrideYear.get()));
    }
    for (final ScenarioSituation situation : scenario.getSituations()) {
      emissionsSourcesService.refreshEmissions(situation.getEmissionSourcesList(), situation.getYear());
    }
    if (scenario.getTheme() == Theme.NCA) {
      LineSimplifier.cleanupLineSources(scenario);
    }
  }

  private void updateCustomPoints(final Scenario scenario, final Optional<ConnectUser> connectUser, final T options) throws AeriusException {
    // Default for analysis: custom points
    if (useCustomPoints(options) || useCustomReceptors(options)) {
      final String receptorSetName = getReceptorSetName(options);

      if (connectUser.isPresent() && receptorSetName != null && !receptorSetName.isBlank()) {
        if (pointSetsRepository.getCalculationPointSetByName(connectUser.get(), receptorSetName) == null) {
          throw new AeriusException(AeriusExceptionReason.CONNECT_USER_CALCULATION_POINT_SET_DOES_NOT_EXIST, receptorSetName);
        }
        scenario.getCustomPointsList().clear();
        scenario.getCustomPointsList().addAll(pointSetsRepository.getCalculationPointsByName(connectUser.get(), receptorSetName));
      }
      if (scenario.getCustomPointsList().isEmpty()) {
        throw new AeriusException(AeriusExceptionReason.CONNECT_NO_CUSTOM_POINTS);
      }
    } else {
      scenario.getCustomPointsList().clear();
    }
  }

  private void validate(final Scenario scenario, final T options) throws AeriusException {
    for (final ScenarioSituation situation : scenario.getSituations()) {
      validateYear(situation);
    }
  }

  private void validateYear(final ScenarioSituation situation) throws AeriusException {
    final int minYear = constantRepository.getInteger(ConstantsEnum.MIN_YEAR);
    final int maxYear = constantRepository.getInteger(ConstantsEnum.MAX_YEAR);
    final int situationYear = situation.getYear();
    if (situationYear < minYear || situationYear > maxYear) {
      throw new AeriusException(AeriusExceptionReason.CONNECT_INCORRECT_CALCULATIONYEAR,
          String.valueOf(situationYear), String.valueOf(minYear), String.valueOf(maxYear));
    }
  }

  protected ExportProperties toExportProperties(final Optional<ConnectUser> user, final T options) {
    final ExportProperties exportProperties = new ExportProperties();
    setExportProperties(exportProperties, options);
    exportProperties.setExpire(getExpire(user, options));
    exportProperties.setLocale(localeService.getLocale().getLanguage());
    if (shouldSendEmail(options)) {
      user.ifPresent(connectUser -> exportProperties.setEmailAddress(connectUser.getEmailAddress()));
    } else {
      exportProperties.getAdditionalOptions().remove(ExportAdditionalOptions.EMAIL_USER);
    }
    return exportProperties;
  }

  private FileServerExpireTag getExpire(final Optional<ConnectUser> user, final T options) {
    return user.isPresent() && isProtectJob(options) ? FileServerExpireTag.NEVER : FileServerExpireTag.LEGAL;
  }

  protected void sendTask(final String jobKey, final ExportProperties exportProperties, final Scenario scenario, final T options,
      final S additionalData) throws AeriusException {
    final QueueEnum queue = determineQueue(options);

    try {
      ScenarioSanitationUtil.sanitizeScenario(scenario);

      fileService.writeJson(jobKey, FileServerFile.DATA, FileServerExpireTag.NEVER, scenario);
      exportTaskclient.startExport(queue, exportProperties, jobKey);
      jobRepository.updateJobStatus(jobKey, JobState.QUEUING);
    } catch (final IOException e) {
      LOG.error("Error when starting export", e);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }
}
