/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import nl.overheid.aerius.db.common.AreaInformationSupplier;
import nl.overheid.aerius.db.common.GeneralRepositoryBean;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.HabitatType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

@Service
public class AreaInformationService implements AreaInformationSupplier {

  private final GeneralRepositoryBean generalRepository;

  public AreaInformationService(final GeneralRepositoryBean generalRepository) {
    this.generalRepository = generalRepository;
  }

  @Override
  public AssessmentArea determineAssessmentArea(final int assessmentAreaId) {
    final Optional<AssessmentArea> area = generalRepository.determineAssessmentArea(assessmentAreaId);
    return area.orElseGet(() -> {
      final AssessmentArea fakeArea = new AssessmentArea();
      fakeArea.setId(assessmentAreaId);
      fakeArea.setName("Gebied " + assessmentAreaId);
      return fakeArea;
    });
  }

  @Override
  public HabitatType determineHabitatType(final int habitatTypeId) {
    final Optional<HabitatType> habitatType = generalRepository.determineHabitatType(habitatTypeId);
    return habitatType.orElseGet(() -> {
      final HabitatType fakeHabitatType = new HabitatType();
      fakeHabitatType.setId(habitatTypeId);
      fakeHabitatType.setName("Habitat " + habitatTypeId);
      return fakeHabitatType;
    });
  }

  @Override
  public boolean determineHabitatTypeSensitiveness(final int habitatTypeId, final EmissionResultKey emissionResultKey) {
    return generalRepository.determineHabitatTypeSensitiveness(habitatTypeId)
        .map(sensitivenessMap -> sensitivenessMap.get(emissionResultKey))
        .orElse(false);
  }

  @Override
  public Double determineHabitatCriticalLevel(final int habitatTypeId, final EmissionResultKey emissionResultKey) {
    return generalRepository.determineHabitatTypeCriticalLevels(habitatTypeId)
        .filter(criticalLevels -> criticalLevels.hasResult(emissionResultKey))
        .map(criticalLevels -> criticalLevels.get(emissionResultKey))
        .orElse(null);
  }

}
