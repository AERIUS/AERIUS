/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.overheid.aerius.calculation.OwN2000CalculationOptionsUtil;
import nl.overheid.aerius.connectservice.model.AppendixType;
import nl.overheid.aerius.connectservice.model.OwN2000CalculationOptions;
import nl.overheid.aerius.connectservice.model.OwN2000CalculationOptions.CalculationPointsTypeEnum;
import nl.overheid.aerius.connectservice.model.OwN2000CalculationOptions.OutputTypeEnum;
import nl.overheid.aerius.connectservice.util.CalculationJobTypeValidator;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.ConstantRepositoryBean;
import nl.overheid.aerius.db.connect.ConnectCalculationPointSetsRepositoryBean;
import nl.overheid.aerius.server.service.export.ExportTaskClientBean;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.export.ExportAppendix;
import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.taskmanager.client.QueueEnum;

@Service
public class OwN2000CalculateService extends AbstractCalculateService<OwN2000CalculationOptions, Void> {

  @Autowired
  OwN2000CalculateService(final ExportTaskClientBean exportTaskclient, final JobRepositoryBean jobRepository,
      final ConnectCalculationPointSetsRepositoryBean pointSetsRepository, final ConstantRepositoryBean constantRepository,
      final ProxyFileService fileService, final LocaleService localeService, final EmissionsSourcesService emissionsSourcesService) {
    super(exportTaskclient, jobRepository, pointSetsRepository, constantRepository, fileService, localeService, emissionsSourcesService);
  }

  @Override
  public String calculate(final Scenario scenario, final OwN2000CalculationOptions options) throws AeriusException {
    // Should not call this specific method for OwN2000 calculations
    throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
  }

  @Override
  protected JobType getJobType(final OwN2000CalculationOptions options) {
    return options.getOutputType() == OutputTypeEnum.PDF ? JobType.REPORT : JobType.CALCULATION;
  }

  @Override
  protected Optional<String> getJobName(final OwN2000CalculationOptions options) {
    return Optional.ofNullable(options.getName());
  }

  @Override
  protected boolean useCustomPoints(final OwN2000CalculationOptions options) {
    // Default for OwN2000 calculation is not using custom points, but letting AERIUS determine receptors.
    return options.getCalculationPointsType() != null && options.getCalculationPointsType() == CalculationPointsTypeEnum.CUSTOM_POINTS;
  }

  @Override
  protected String getReceptorSetName(final OwN2000CalculationOptions options) {
    return options.getReceptorSetName();
  }

  @Override
  protected Optional<Integer> getOverrideYear(final OwN2000CalculationOptions options) {
    return Optional.ofNullable(options.getCalculationYear());
  }

  @Override
  protected void validate(final Scenario scenario, final OwN2000CalculationOptions options, final Void additionalData)
      throws AeriusException {
    if (options.getOutputType() == OutputTypeEnum.PDF) {
      scenario.getOptions().setCalculationJobType(CalculationJobType.PROCESS_CONTRIBUTION);
      CalculationJobTypeValidator.validate(scenario);
    }
  }

  @Override
  protected void updateScenarioOptions(final Scenario scenario, final OwN2000CalculationOptions options) throws AeriusException {
    final CalculationSetOptions cso = OwN2000CalculationOptionsUtil.createOwN2000CalculationSetOptions();

    cso.setCalculationMethod(useCustomPoints(options) ? CalculationMethod.CUSTOM_POINTS : CalculationMethod.FORMAL_ASSESSMENT);
    scenario.setOptions(cso);
  }

  @Override
  protected void setExportProperties(final ExportProperties exportProperties, final OwN2000CalculationOptions options) {
    final ExportType exportType = options.getOutputType() == OutputTypeEnum.PDF ? ExportType.PDF_PAA : ExportType.GML_WITH_RESULTS;
    exportProperties.setName(options.getName());
    exportProperties.setExportType(exportType);
    exportProperties.setAppendices(convertAppendices(options.getAppendices()));
    addRequiredAppendices(exportProperties, exportType);
  }

  private static Set<ExportAppendix> convertAppendices(final List<AppendixType> connectAppendices) {
    if (connectAppendices == null || connectAppendices.isEmpty()) {
      return new HashSet<>();
    }
    return connectAppendices.stream()
        .map(OwN2000CalculateService::convertAppendix)
        .filter(Objects::nonNull)
        .collect(Collectors.toCollection(() -> new HashSet<>()));
  }

  private static ExportAppendix convertAppendix(final AppendixType appendixType) {
    ExportAppendix converted;
    if (appendixType == AppendixType.EDGE_EFFECT_REPORT) {
      converted = ExportAppendix.EDGE_EFFECT_REPORT;
    } else {
      // Either throw error for the user or filter out, went with filtering out in this case.
      converted = null;
    }
    return converted;
  }

  private static void addRequiredAppendices(final ExportProperties exportProperties, final ExportType exportType) {
    if (exportType == ExportType.PDF_PAA) {
      exportProperties.getAppendices().add(ExportAppendix.EXTRA_ASSESSMENT_REPORT);
    }
  }

  @Override
  protected boolean shouldSendEmail(final OwN2000CalculationOptions options) {
    // If getSendEmail() is null it is the default and means it should send an e-mail.
    return options.getSendEmail() == null || options.getSendEmail();
  }

  @Override
  protected QueueEnum determineQueue(final OwN2000CalculationOptions options) {
    return options.getOutputType() == OutputTypeEnum.PDF ? QueueEnum.CONNECT_PAA_EXPORT : QueueEnum.CONNECT_GML_EXPORT;
  }

}
