/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.resource;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import nl.overheid.aerius.connectservice.api.v8.UtilityApiDelegate;
import nl.overheid.aerius.connectservice.domain.ImportParcelAggregate;
import nl.overheid.aerius.connectservice.model.FileResponse;
import nl.overheid.aerius.connectservice.model.ValidateResponse;
import nl.overheid.aerius.connectservice.model.ValidationMessage;
import nl.overheid.aerius.connectservice.service.ExportService;
import nl.overheid.aerius.connectservice.service.ImportService;
import nl.overheid.aerius.connectservice.service.ResponseService;
import nl.overheid.aerius.connectservice.service.ValidationReportService;
import nl.overheid.aerius.importer.ImportOption;
import nl.overheid.aerius.shared.domain.importer.ImportProperties;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.exception.AeriusException;

@Service
@Profile({"nl", "uk"})
public class UtilityResource implements UtilityApiDelegate {

  private static final DateTimeFormatter DATETIME_FORMATTER_FILE = DateTimeFormatter.ofPattern("yyyy-MM-dd+HH_mm_ss");

  private static final Set<ImportOption> DEFAULT_OPTIONS = EnumSet.of(
      ImportOption.USE_IMPORTED_LANDUSES,
      ImportOption.INCLUDE_SOURCES,
      ImportOption.INCLUDE_CIMLK_MEASURES,
      ImportOption.INCLUDE_CIMLK_DISPERSION_LINES,
      ImportOption.INCLUDE_CIMLK_CORRECTIONS,
      ImportOption.USE_VALID_SECTORS,
      ImportOption.VALIDATE_AGAINST_SCHEMA,
      ImportOption.VALIDATE_SOURCES);

  private final ImportService importService;
  private final ResponseService responseService;
  private final ValidationReportService validationReportService;
  private final ExportService exportService;

  public UtilityResource(final ImportService importService, final ResponseService responseService,
      final ValidationReportService validationReportService, final ExportService exportService) {
    this.importService = importService;
    this.responseService = responseService;
    this.validationReportService = validationReportService;
    this.exportService = exportService;
  }

  @Override
  public ResponseEntity<Resource> convert(final MultipartFile filePart) {
    try {
      final Set<ImportOption> options = convertOptions();
      final List<ImportParcel> result = importService.importFile(filePart, options, new ImportProperties());
      final Resource response = convertConvert(result);
      return responseService.toOkResponse(response);
    } catch (final AeriusException | IOException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<FileResponse> validateAndConvert(final MultipartFile filePart) {
    try {
      final Set<ImportOption> options = convertOptions();
      final List<ImportParcel> result = importService.importFile(filePart, options, new ImportProperties());
      final FileResponse response = convertValidateAndConvert(result);
      return responseService.toOkResponse(response);
    } catch (final AeriusException | IOException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<ValidateResponse> validate(final MultipartFile filePart, final Boolean strict) {
    try {
      final Set<ImportOption> options = validationOptions(strict);
      final List<ImportParcel> result = importService.importFile(filePart, options, new ImportProperties());
      return responseService.toOkResponse(convertValidate(result));
    } catch (final AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<Resource> validateReport(final MultipartFile filePart, final Boolean strict) {
    try {
      final Set<ImportOption> options = validationOptions(strict);
      final LocalDateTime generationTime = LocalDateTime.now();
      final List<ImportParcel> result = importService.importFile(filePart, options, new ImportProperties());
      final ByteArrayResource resource;
      try (final ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
        validationReportService.report(new ImportParcelAggregate(result), generationTime, outputStream);
        resource = new ByteArrayResource(outputStream.toByteArray());
      }
      final HttpHeaders headers = new HttpHeaders();
      final String filename = String.format("validation-report-%s.csv", generationTime.format(DATETIME_FORMATTER_FILE));
      headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename);
      return responseService.toOkResponse(resource, headers);
    } catch (final AeriusException | IOException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  private Set<ImportOption> convertOptions() {
    final Set<ImportOption> options = new HashSet<>();
    options.addAll(DEFAULT_OPTIONS);
    options.add(ImportOption.INCLUDE_RESULTS);
    return options;
  }

  private Set<ImportOption> validationOptions(final Boolean strict) {
    final Set<ImportOption> options = new HashSet<>();
    options.addAll(DEFAULT_OPTIONS);
    if (strict != null && strict.booleanValue()) {
      options.add(ImportOption.VALIDATE_STRICT);
    }
    return options;
  }

  private ValidateResponse convertValidate(final List<ImportParcel> result) {
    final ImportParcelAggregate aggregate = new ImportParcelAggregate(result);
    final List<ValidationMessage> errors = responseService.convertToValidation(aggregate.getExceptions());
    final List<ValidationMessage> warnings = responseService.convertToValidation(aggregate.getWarnings());
    return new ValidateResponse()
        .successful(aggregate.getExceptions().isEmpty())
        .errors(errors)
        .warnings(warnings);
  }

  private FileResponse convertValidateAndConvert(final List<ImportParcel> result) throws AeriusException, IOException {
    final ImportParcelAggregate aggregate = new ImportParcelAggregate(result);
    final List<ValidationMessage> errors = responseService.convertToValidation(aggregate.getExceptions());
    final List<ValidationMessage> warnings = responseService.convertToValidation(aggregate.getWarnings());
    final boolean success = aggregate.getExceptions().isEmpty();
    final FileResponse response = new FileResponse()
        .successful(success)
        .errors(errors)
        .warnings(warnings);
    if (success) {
      response.setFilePart(exportService.exportAsZip(result));
    }
    return response;
  }

  private Resource convertConvert(final List<ImportParcel> result) throws AeriusException, IOException {
    final ImportParcelAggregate aggregate = new ImportParcelAggregate(result);
    final List<ValidationMessage> errors = responseService.convertToValidation(aggregate.getExceptions());
    final boolean success = aggregate.getExceptions().isEmpty();
    if (success) {
      return new ByteArrayResource(exportService.exportAsZip(result));
    } else {
      final String errorMessage = errors.stream()
          .map(ValidationMessage::getMessage)
          .collect(Collectors.joining(""));
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, errorMessage);
    }
  }

  @Override
  public Optional<NativeWebRequest> getRequest() {
    return UtilityApiDelegate.super.getRequest();
  }
}
