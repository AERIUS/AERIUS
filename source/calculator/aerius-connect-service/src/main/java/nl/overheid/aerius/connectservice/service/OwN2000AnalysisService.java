/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.overheid.aerius.calculation.OwN2000CalculationOptionsUtil;
import nl.overheid.aerius.connectservice.model.OpsOptions;
import nl.overheid.aerius.connectservice.model.OwN2000AnalysisOptions;
import nl.overheid.aerius.connectservice.model.OwN2000AnalysisOptions.CalculationPointsTypeEnum;
import nl.overheid.aerius.connectservice.model.OwN2000AnalysisOptions.OutputTypeEnum;
import nl.overheid.aerius.connectservice.model.OwN2000AnalysisOptions.RoadOPSEnum;
import nl.overheid.aerius.connectservice.model.OwN2000AnalysisOutputOptions;
import nl.overheid.aerius.connectservice.model.ResultType;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.ConstantRepositoryBean;
import nl.overheid.aerius.db.common.MeteoRepositoryBean;
import nl.overheid.aerius.db.connect.ConnectCalculationPointSetsRepositoryBean;
import nl.overheid.aerius.server.service.export.ExportTaskClientBean;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.CalculationRoadOPS;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.calculation.OPSOptions;
import nl.overheid.aerius.shared.domain.calculation.OwN2000CalculationOptions;
import nl.overheid.aerius.shared.domain.calculation.SubReceptorsMode;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.meteo.Meteo;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.theme.own2000.OwN2000SubReceptorConstants;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CustomCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.taskmanager.client.QueueEnum;

@Service
public class OwN2000AnalysisService extends AbstractCalculateService<OwN2000AnalysisOptions, Void> {

  private final MeteoRepositoryBean meteoRepository;

  @Autowired
  OwN2000AnalysisService(final ExportTaskClientBean exportTaskclient, final JobRepositoryBean jobRepository,
      final ConnectCalculationPointSetsRepositoryBean pointSetsRepository, final ConstantRepositoryBean constantRepository,
      final ProxyFileService fileService, final MeteoRepositoryBean meteoRepository, final LocaleService localeService,
      final EmissionsSourcesService emissionsSourcesService) {
    super(exportTaskclient, jobRepository, pointSetsRepository, constantRepository, fileService, localeService, emissionsSourcesService);
    this.meteoRepository = meteoRepository;
  }

  @Override
  public String calculate(final Scenario scenario, final OwN2000AnalysisOptions options) throws AeriusException {
    // Should not call this specific method for analysis
    throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
  }

  public String analyseOwN2000(final ConnectUser connectUser, final Scenario scenario, final OwN2000AnalysisOptions options)
      throws AeriusException {
    return calculate(connectUser, scenario, options);
  }

  @Override
  protected Optional<String> getJobName(final OwN2000AnalysisOptions options) {
    return Optional.ofNullable(options.getName());
  }

  @Override
  protected JobType getJobType(final OwN2000AnalysisOptions options) {
    return JobType.CALCULATION;
  }

  @Override
  protected boolean useCustomPoints(final OwN2000AnalysisOptions options) {
    // Default for analysis is using custom point
    return options.getCalculationPointsType() == null || options.getCalculationPointsType() == CalculationPointsTypeEnum.CUSTOM_POINTS;
  }

  @Override
  protected boolean useCustomReceptors(final OwN2000AnalysisOptions options) {
    return options.getCalculationPointsType() == CalculationPointsTypeEnum.CUSTOM_RECEPTORS;
  }

  @Override
  protected String getReceptorSetName(final OwN2000AnalysisOptions options) {
    return options.getReceptorSetName();
  }

  @Override
  protected Optional<Integer> getOverrideYear(final OwN2000AnalysisOptions options) {
    return Optional.ofNullable(options.getCalculationYear());
  }

  @Override
  protected void validate(final Scenario scenario, final OwN2000AnalysisOptions options, final Void additionalData) throws AeriusException {
    validateCustomPointHeights(scenario, options);
  }

  @Override
  protected void updateScenarioOptions(final Scenario scenario, final OwN2000AnalysisOptions options) throws AeriusException {
    final CalculationSetOptions scenarioOptions = scenario.getOptions();
    final CalculationMethod calculationMethod;
    if (useCustomPoints(options)) {
      calculationMethod = CalculationMethod.CUSTOM_POINTS;
    } else if (useCustomReceptors(options)) {
      calculationMethod = CalculationMethod.CUSTOM_RECEPTORS;
    } else {
      calculationMethod = CalculationMethod.FORMAL_ASSESSMENT;
    }

    scenarioOptions.setCalculationMethod(calculationMethod);
    // compare options that are default true by checking if false and invert; because if value is null it would mean it should get value true.
    scenarioOptions.setStacking(!Boolean.FALSE.equals(options.getStacking()));

    final OwN2000CalculationOptions own2000Options = scenarioOptions.getOwN2000CalculationOptions();
    own2000Options.setUseMaxDistance(true);
    // if the current or legacy max distance setting is false, set to false (default is true)
    if (Boolean.FALSE.equals(options.getWithOwN2000MaxDistance())) {
      own2000Options.setUseMaxDistance(false);
    }
    own2000Options.setRoadOPS(toDomain(options.getRoadOPS()));
    own2000Options.setForceAggregation(Boolean.TRUE.equals(options.getAggregate()));
    own2000Options.setMeteo(toMeteo(options.getMeteoYear()));
    own2000Options.setUseReceptorHeights(Boolean.TRUE.equals(options.getUseReceptorHeight()));
    own2000Options.setSubReceptorsMode(Optional.ofNullable(options.getSubReceptors())
        .map(sr -> SubReceptorsMode.valueOf(sr.getValue())).orElse(SubReceptorsMode.DISABLED));
    own2000Options.setSubReceptorZoomLevel(Optional.ofNullable(options.getSubReceptorZoomLevel()).orElse(1));
    own2000Options.setSplitSubReceptorWork(!Boolean.FALSE.equals(options.getSplitSubReceptorWork()));
    own2000Options.setSplitSubReceptorWorkDistance(
        Optional.ofNullable(options.getSplitSubReceptorWorkDistance()).orElse((int) OwN2000SubReceptorConstants.SUB_RECEPTOR_FARTHEST_DISTANCE));
    updateOPSOptions(scenarioOptions.getOwN2000CalculationOptions(), options);
    updateOptionsSubstance(scenarioOptions, options);
  }

  @Override
  protected void setExportProperties(final ExportProperties exportProperties, final OwN2000AnalysisOptions options) {
    final ExportType exportType = determineExportType(options.getOutputType(), options.getOutputOptions());
    exportProperties.setName(options.getName());
    exportProperties.setExportType(exportType);
  }

  @Override
  protected QueueEnum determineQueue(final OwN2000AnalysisOptions options) {
    return options.getOutputType() == OutputTypeEnum.BRN ? QueueEnum.INPUT_EXPORT : QueueEnum.CONNECT_GML_EXPORT;
  }

  @Override
  protected boolean shouldSendEmail(final OwN2000AnalysisOptions options) {
    return Boolean.TRUE.equals(options.getSendEmail());
  }

  private static void updateOPSOptions(final OwN2000CalculationOptions scenarioOptions, final OwN2000AnalysisOptions options) {
    if (options.getOpsOptions() != null) {
      final OPSOptions opsOptions = new OPSOptions();

      opsOptions.setRawInput(Boolean.TRUE.equals(options.getOpsOptions().getRawInput()));
      opsOptions.setYear(options.getOpsOptions().getYear());
      opsOptions.setCompCode(options.getOpsOptions().getCompCode());
      opsOptions.setMolWeight(options.getOpsOptions().getMolWeight());
      opsOptions.setPhase(options.getOpsOptions().getPhase());
      opsOptions.setLoss(options.getOpsOptions().getLoss());
      opsOptions.setDiffCoeff(options.getOpsOptions().getDiffCoeff());
      opsOptions.setWashout(options.getOpsOptions().getWashout());
      opsOptions.setConvRate(options.getOpsOptions().getConvRate());
      opsOptions.setRoads(options.getOpsOptions().getRoads());
      opsOptions.setRoughness(options.getOpsOptions().getRoughness());
      opsOptions.setChemistry(toDomain(options.getOpsOptions().getChemistry()));
      scenarioOptions.setOpsOptions(opsOptions);
    }
  }

  private static OPSOptions.Chemistry toDomain(final OpsOptions.ChemistryEnum connectValue) {
    if (connectValue != null) {
      return OPSOptions.Chemistry.valueOf(connectValue.name());
    }
    return OPSOptions.Chemistry.PROGNOSIS;
  }

  private static CalculationRoadOPS toDomain(final RoadOPSEnum connectValue) {
    if (connectValue == null) {
      return CalculationRoadOPS.DEFAULT;
    }
    return switch (connectValue) {
      case OPS_ROAD -> CalculationRoadOPS.OPS_ROAD;
      case OPS_ALL -> CalculationRoadOPS.OPS_ALL;
      default -> CalculationRoadOPS.DEFAULT;
    };
  }

  private Meteo toMeteo(final String meteoYear) throws AeriusException {
    final Meteo meteo;
    // Only apply if it is a custom points calculation
    if (!StringUtils.isEmpty(meteoYear)) {
      final Map<String, Meteo> supportedMeteoOptions = meteoRepository.getMeteoDefinitions();
      meteo = supportedMeteoOptions.get(meteoYear);
      if (meteo == null) {
        throw new AeriusException(AeriusExceptionReason.CONNECT_INVALID_METEO, meteoYear);
      }
    } else {
      meteo = null;
    }
    return meteo;
  }

  private static void updateOptionsSubstance(final CalculationSetOptions scenarioOptions, final OwN2000AnalysisOptions options) {
    if (options.getSubstances() == null || options.getSubstances().isEmpty()) {
      OwN2000CalculationOptionsUtil.PERMIT_SUBSTANCES.forEach(scenarioOptions.getSubstances()::add);
    } else {
      options.getSubstances().stream().map(s -> Substance.safeValueOf(s.name())).forEach(scenarioOptions.getSubstances()::add);
    }
    final OwN2000AnalysisOutputOptions outputOptions = options.getOutputOptions();

    if (outputOptions == null || outputOptions.getResultTypes() == null || outputOptions.getResultTypes().isEmpty()) {
      scenarioOptions.getEmissionResultKeys().add(EmissionResultKey.NH3_DEPOSITION);
      scenarioOptions.getEmissionResultKeys().add(EmissionResultKey.NOX_DEPOSITION);
    } else {
      for (final ResultType resultType : outputOptions.getResultTypes()) {
        scenarioOptions.getEmissionResultKeys().addAll(EmissionResultKey.getEmissionResultKeys(
            scenarioOptions.getSubstances(), toDomain(resultType)));
      }
    }
  }

  private static EmissionResultType toDomain(final ResultType connectValue) {
    if (connectValue == null) {
      return null;
    }
    return switch (connectValue) {
      case CONCENTRATION -> EmissionResultType.CONCENTRATION;
      case DEPOSITION -> EmissionResultType.DEPOSITION;
      case DRY_DEPOSITION -> EmissionResultType.DRY_DEPOSITION;
      case WET_DEPOSITION -> EmissionResultType.WET_DEPOSITION;
      default -> throw new IllegalArgumentException("Can't convert connect result type '" + connectValue + "' to a AERIUS result type");
    };
  }

  private static ExportType determineExportType(final OutputTypeEnum outputType, final OwN2000AnalysisOutputOptions outputOptions) {
    final ExportType exportType;
    if (outputType == OutputTypeEnum.CSV) {
      exportType = ExportType.CSV;
    } else if (outputType == OutputTypeEnum.BRN) {
      exportType = ExportType.OPS;
    } else {
      // Even though the user is required to supply the output type, we can use GML as default
      exportType = includeSectorResults(outputOptions)
          ? ExportType.GML_WITH_SECTORS_RESULTS
          : ExportType.GML_WITH_RESULTS;
    }
    return exportType;
  }

  private static boolean includeSectorResults(final OwN2000AnalysisOutputOptions outputOptions) {
    return outputOptions != null && Boolean.TRUE.equals(outputOptions.getSectorOutput());
  }

  private static void validateCustomPointHeights(final Scenario scenario, final OwN2000AnalysisOptions options) throws AeriusException {
    if (options.getUseReceptorHeight() != null && options.getUseReceptorHeight()) {
      validateResultTypesForHeightCalculation(options);
      validateCalculationPointsForHeightCalculation(scenario);
    }
  }

  private static void validateResultTypesForHeightCalculation(final OwN2000AnalysisOptions options) throws AeriusException {
    final List<ResultType> resultTypes = options.getOutputOptions().getResultTypes();

    if (resultTypes == null || resultTypes.isEmpty()) {
      // If results type is empty it implies the default, which is DEPOSITION, and that is illegal.
      throw new AeriusException(AeriusExceptionReason.CONNECT_INVALID_OUTPUTTYPE, ResultType.DEPOSITION.toString());
    }
    for (final ResultType resultType : resultTypes) {
      if (resultType != ResultType.CONCENTRATION) {
        throw new AeriusException(AeriusExceptionReason.CONNECT_INVALID_OUTPUTTYPE, resultType.toString());
      }
    }
  }

  private static void validateCalculationPointsForHeightCalculation(final Scenario scenario) throws AeriusException {
    if (scenario.getCustomPointsList().stream().anyMatch(OwN2000AnalysisService::hasNoHeight)) {
      throw new AeriusException(AeriusExceptionReason.CONNECT_MISSING_RECEPTOR_HEIGHT);
    }
  }

  private static boolean hasNoHeight(final CalculationPointFeature pointFeature) {
    if (pointFeature.getProperties() instanceof final CustomCalculationPoint customPoint) {
      return customPoint.getHeight() == null;
    } else {
      return true;
    }
  }
}
