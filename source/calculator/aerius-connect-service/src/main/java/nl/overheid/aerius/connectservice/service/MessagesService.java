/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.overheid.aerius.i18n.AeriusExceptionMessages;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Service to cache and return translated messages corresponding to {@link Throwable}.
 */
@Service
public class MessagesService {

  private final Map<Locale, AeriusExceptionMessages> messages;
  private final LocaleService localeService;

  @Autowired
  public MessagesService(final LocaleService localeService) {
    this.localeService = localeService;
    messages = LocaleUtils.getLocales().stream()
        .collect(Collectors.toMap(Function.identity(), AeriusExceptionMessages::new));
  }

  /**
   * Gets the message corresponding to the Throwable e
   * @param e the {@link Throwable}
   * @return the message or {@code null} if the language for the current request is not supported.
   */
  public String getExceptionMessage(final Throwable e) {
    return getExceptionMessages().map(aem -> aem.getString(e)).orElse(null);
  }

  private Optional<AeriusExceptionMessages> getExceptionMessages() {
    return Optional.ofNullable(messages.get(localeService.getLocale()));
  }
}
