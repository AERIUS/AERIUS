/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 *
 */
public class ScenarioConstruction {

  private final Scenario scenario;
  private final List<AeriusException> errors = new ArrayList<>();
  private final List<AeriusException> warnings = new ArrayList<>();

  private boolean metadataAdded;

  public ScenarioConstruction(final Scenario scenario) {
    this.scenario = scenario;
  }

  public Scenario getScenario() {
    return scenario;
  }

  public void addErrors(final List<AeriusException> errors) {
    this.errors.addAll(errors);
  }

  public void addError(final AeriusException error) {
    this.errors.add(error);
  }

  public List<AeriusException> getErrors() {
    return errors;
  }

  public void addWarnings(final List<AeriusException> warnings) {
    this.warnings.addAll(warnings);
  }

  public List<AeriusException> getWarnings() {
    return warnings;
  }

  public boolean isSuccesful() {
    return errors.isEmpty();
  }

  public boolean isMetadataAdded() {
    return metadataAdded;
  }

  public void setMetadataAdded(final boolean metadataAdded) {
    this.metadataAdded = metadataAdded;
  }

}
