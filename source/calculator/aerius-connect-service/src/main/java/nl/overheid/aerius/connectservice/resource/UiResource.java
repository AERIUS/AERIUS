/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.resource;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.databind.ObjectMapper;

import nl.overheid.aerius.connectservice.api.v8.UiApiDelegate;
import nl.overheid.aerius.connectservice.auth.AuthenticationService;
import nl.overheid.aerius.connectservice.domain.UIInfoMarkerRequest;
import nl.overheid.aerius.connectservice.domain.UiCalculationInfoResponse;
import nl.overheid.aerius.connectservice.domain.UiCalculationRequest;
import nl.overheid.aerius.connectservice.domain.UiDetermineCalculationPointsRequest;
import nl.overheid.aerius.connectservice.domain.UiDetermineCalculationPointsResult;
import nl.overheid.aerius.connectservice.domain.UiImportResultsRequest;
import nl.overheid.aerius.connectservice.domain.UiRefreshEmissionsRequest;
import nl.overheid.aerius.connectservice.service.EmissionsSourcesService;
import nl.overheid.aerius.connectservice.service.ImportService;
import nl.overheid.aerius.connectservice.service.InfoService;
import nl.overheid.aerius.connectservice.service.ProxyFileService;
import nl.overheid.aerius.connectservice.service.ResponseService;
import nl.overheid.aerius.connectservice.service.ServerUsageService;
import nl.overheid.aerius.connectservice.service.UiCalculateService;
import nl.overheid.aerius.connectservice.service.UiCalculationPointService;
import nl.overheid.aerius.connectservice.service.UiContextService;
import nl.overheid.aerius.connectservice.service.UiImportService;
import nl.overheid.aerius.connectservice.service.UiJobsService;
import nl.overheid.aerius.connectservice.service.UiResultsService;
import nl.overheid.aerius.connectservice.service.ValidationReportService;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.importer.domain.ImporterInput;
import nl.overheid.aerius.shared.FileServerExpireTag;
import nl.overheid.aerius.shared.FileServerFile;
import nl.overheid.aerius.shared.domain.FileFormat;
import nl.overheid.aerius.shared.domain.ProductProfile;
import nl.overheid.aerius.shared.domain.ServerUsage;
import nl.overheid.aerius.shared.domain.calculation.JobProgress;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculation;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.domain.importer.ImportProperties;
import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.summary.DecisionFrameworkResults;
import nl.overheid.aerius.shared.domain.summary.JobSummary;
import nl.overheid.aerius.shared.domain.summary.OverlappingHexagonType;
import nl.overheid.aerius.shared.domain.summary.ProcurementPolicy;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SituationResultsSummary;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.domain.summary.SummaryRequest;
import nl.overheid.aerius.shared.domain.v2.archive.ArchiveMetaData;
import nl.overheid.aerius.shared.domain.v2.geojson.FeatureCollection;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.InlandWaterway;
import nl.overheid.aerius.shared.domain.validation.FileValidationStatus;
import nl.overheid.aerius.shared.domain.validation.ValidationStatus;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.LocaleUtils;

@Service
@Profile({"( nl | uk | lbv ) & !prerelease"})
public class UiResource implements UiApiDelegate {
  private static final DateTimeFormatter DATETIME_FORMATTER_FILE = DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss");
  private static final String NO_JOB_FOUND_WITH_THIS_KEY = "No job found with this key";

  private static final Logger LOG = LoggerFactory.getLogger(UiResource.class);

  private final AuthenticationService authenticationService;
  private final ResponseService responseService;
  private final ProxyFileService proxyFileService;
  private final UiImportService uiImportService;
  private final UiCalculateService calculateService;
  private final UiJobsService jobsService;
  private final UiResultsService resultsService;
  private final EmissionsSourcesService sourcesService;
  private final UiContextService contextService;
  private final UiCalculationPointService calculationPointService;
  private final ServerUsageService serverUsageService;
  private final InfoService infoService;
  private final ValidationReportService validationReportService;
  private final JobRepositoryBean jobRepository;
  private final ObjectMapper objectMapper;

  @Autowired
  UiResource(final AuthenticationService authenticationService, final ResponseService responseService, final ProxyFileService proxyFileService,
      final UiImportService uiImportService, final UiCalculateService calculateService, final UiJobsService jobsService,
      final UiResultsService resultsService, final EmissionsSourcesService sourcesService, final UiContextService contextService,
      final UiCalculationPointService calculationPointService, final ServerUsageService serverUsageService, final InfoService infoService,
      final ValidationReportService validationReportService, final JobRepositoryBean jobRepository, final ObjectMapper objectMapper) {
    this.authenticationService = authenticationService;
    this.responseService = responseService;
    this.proxyFileService = proxyFileService;
    this.uiImportService = uiImportService;
    this.calculateService = calculateService;
    this.jobsService = jobsService;
    this.resultsService = resultsService;
    this.sourcesService = sourcesService;
    this.contextService = contextService;
    this.calculationPointService = calculationPointService;
    this.serverUsageService = serverUsageService;
    this.infoService = infoService;
    this.validationReportService = validationReportService;
    this.jobRepository = jobRepository;
    this.objectMapper = objectMapper;
  }

  @Override
  public ResponseEntity<Boolean> deleteFile(final String uuid) {
    deleteImportedFiles(uuid);
    return responseService.toOkResponse(true);
  }

  /**
   * Imports the uploaded file by sending the file to the file server and schedule an import task.
   * If the file is not a supported file type the import will fail with an error.
   * If the file is supported, but not a zip file. The file is zipped and the zipped content is sent to the file server.
   * The filename under which the file is sent to the file server is a random uuid that is generated in this method and passed to the import.
   * A random name used here because when using the original file it would be possible to retrieve the file via the file service api as
   * the name could be constructed using the returned uuid by this api call and filename, creating a security issue.
   *
   * This method should always write a validation result so the UI can present the error with the file instead of as a notification.
   *
   * @param filePart uploaded file part
   * @param optImportProperties optional {@link ImportProperties} as json string
   */
  @Override
  public ResponseEntity<String> importFile(final MultipartFile filePart, final String optImportProperties) {
    final String uuid = FileServerFile.createId(ImporterInput.IMPORT_PREFIX);

    try {
      final String originalFilename = filePart.getOriginalFilename();
      final ImportType importType = validateFilename(uuid, originalFilename, FileServerExpireTag.SHORT);

      if (importType == null) {
        // If import type == null, file is not supported, written to validation and abort here.
        return responseService.toOkResponse(uuid);
      }
      // Store the imported file under a unique random id so it can't be retrieved via the files api.
      final String importFilename = UUID.randomUUID().toString();

      try (InputStream fileInputStream = ImportService.toZipInputSteam(filePart.getInputStream(), importType, originalFilename)) {
        proxyFileService.write(FileServerFile.FREE_FORMAT, fileInputStream, FileServerExpireTag.SHORT, uuid, importFilename);
      }
      // If the data was zipped create a new file name by appending .zip. This is needed because importer will need file extension to read the file.
      final String filename = originalFilename + (importType == ImportType.ZIP ? "" : FileFormat.ZIP.getDottedExtension());
      // Start the validation on a different thread by subscribing on another scheduler.
      // This validation also splits the file into multiple ImportParcels if multiple situations are present
      uiImportService.sendImportToWorker(uuid, importFilename, filename,
          objectMapper.readValue(optImportProperties, ImportProperties.class), FileServerExpireTag.SHORT);
    } catch (final IOException e) {
      LOG.debug("IOException importing file", e);
      uiImportService.onException(uuid, new AeriusException(AeriusExceptionReason.INTERNAL_ERROR), FileServerExpireTag.SHORT);
    } catch (final RuntimeException e) {
      LOG.error("RuntimeException importing file", e);
      uiImportService.onException(uuid, new AeriusException(AeriusExceptionReason.INTERNAL_ERROR), FileServerExpireTag.SHORT);
    }
    return responseService.toOkResponse(uuid);
  }

  /**
   * Checks if the uploaded file has an supported file extension and if not throw a file not supported response exception.
   *
   * @param uuid uuid the validation should be stored under
   * @param filename filename to check
   * @param expire
   * @return returns import type if supported
   */
  private ImportType validateFilename(final String uuid, final String filename, final FileServerExpireTag expire) {
    final ImportType importType = ImportType.determineByFilename(filename);

    if (importType == null) {
      uiImportService.onException(uuid, new AeriusException(AeriusExceptionReason.IMPORT_FILE_UNSUPPORTED, filename), expire);
    }
    return importType;
  }

  @Override
  public ResponseEntity<Object> retrieveValidationResult(final String uuid) {
    try {
      return getFileResponse(uuid, FileServerFile.VALIDATION);
    } catch (final RuntimeException e) {
      LOG.debug("Error retrieving validation result", e);
      final FileValidationStatus status = new FileValidationStatus(uuid, "", ValidationStatus.PENDING, "", SituationType.UNKNOWN, null, List.of(),
          null);

      return responseService.toOkResponse(status);
    }
  }

  @Override
  public ResponseEntity<Resource> retrieveValidationReport(final List<String> fileCodes) {
    try {
      final LocalDateTime generationTime = LocalDateTime.now();
      final List<FileValidationStatus> validationStatuses = new ArrayList<>();
      for (final String fileCode : fileCodes) {
        validationStatuses.add(proxyFileService.retrieveFile(FileServerFile.VALIDATION, (fn, is) -> readFileValidationStatus(is), fileCode));
      }

      final ByteArrayResource resource;
      try (final ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
        validationReportService.report(validationStatuses, generationTime, outputStream);
        resource = new ByteArrayResource(outputStream.toByteArray());
      }

      final HttpHeaders headers = new HttpHeaders();
      final String filename = String.format("validation-report-%s.csv", generationTime.format(DATETIME_FORMATTER_FILE));
      headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename);
      return responseService.toOkResponse(resource, headers);
    } catch (final IOException | WebClientResponseException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  private FileValidationStatus readFileValidationStatus(final InputStream is) throws IOException {
    try (is) {
      return objectMapper.readValue(is, FileValidationStatus.class);
    }
  }

  @Override
  public ResponseEntity<Object> retrieveJson(final String uuid, final Boolean keepFile) {
    try {
      final ResponseEntity<Object> fileResponse = getFileResponse(uuid, FileServerFile.DATA);

      // Delete imported files from the fileserver when keepFile is not true.
      if (!Boolean.TRUE.equals(keepFile)) {
        deleteImportedFiles(uuid);
      }
      return fileResponse;
    } catch (final WebClientResponseException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  private void deleteImportedFiles(final String fileCode) {
    proxyFileService.deleteFilesForId(fileCode);
  }

  private ResponseEntity<Object> getFileResponse(final String uuid, final FileServerFile fileServerFile) {
    final InputStreamResource result = proxyFileService.retrieveFile(fileServerFile, UiResource::inputStream2Resource, uuid);

    return responseService.toOkResponse(result);
  }

  private static InputStreamResource inputStream2Resource(final String filename, final InputStream inputStream) {
    return new InputStreamResource(inputStream);
  }

  @Override
  public ResponseEntity<String> importResults(final String body) {
    try {
      final UiImportResultsRequest request = objectMapper.readValue(body, UiImportResultsRequest.class);
      final Scenario scenario = request.getScenario();
      scenario.setTheme(request.getTheme());
      return responseService.toOkResponse(
          uiImportService.sendSaveResultsToWorker(scenario, request.getSituationWithFileIds(), request.getArchiveContributionFileId()));
    } catch (final IOException | AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<String> startCalculation(final String body) {
    try {
      final UiCalculationRequest request = objectMapper.readValue(body, UiCalculationRequest.class);
      sanityCheck(request);
      final Scenario scenario = request.getScenario();
      scenario.setTheme(request.getTheme());
      final Optional<ConnectUser> currentUser = authenticationService.getOptionalCurrentUser();
      if (currentUser.isPresent()) {
        return responseService.toOkResponse(calculateService.calculate(currentUser.get(), scenario, request));
      }
      return responseService.toOkResponse(calculateService.calculate(scenario, request));
    } catch (final IOException | AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<String> cancelCalculation(final String jobKey) {
    try {
      final Optional<JobProgress> jobProgress = jobsService.getJob(jobKey);
      if (jobProgress.isEmpty()) {
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, NO_JOB_FOUND_WITH_THIS_KEY);
      } else {
        jobsService.cancelJob(jobKey);
        return responseService.toOkResponse(jobKey);
      }
    } catch (final AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<Void> deleteCalculation(final String jobKey) {
    try {
      jobsService.deleteJob(jobKey);
      return responseService.toOkResponse();
    } catch (final AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<Void> deleteCalculationViaPost(final String jobKey) {
    return deleteCalculation(jobKey);
  }

  @Override
  public ResponseEntity<Object> retrieveCalculationInfo(final String jobKey) {
    try {
      final Optional<JobProgress> jobProgress = jobsService.getJob(jobKey);
      if (jobProgress.isEmpty()) {
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, NO_JOB_FOUND_WITH_THIS_KEY);
      } else {
        final ServerUsage serverUsage = serverUsageService.getCurrentServerUsage();
        final List<SituationCalculation> situationCalculations = jobsService.getSituationCalculations(jobKey);
        final ArchiveMetaData archiveMetadata = jobsService.getArchiveMetaData(jobKey);
        return responseService.toOkResponse(new UiCalculationInfoResponse(jobProgress.get(), serverUsage, situationCalculations, archiveMetadata));
      }
    } catch (final AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<Object> retrieveSituationResultsSummary(final String jobKey, final String situationId, final String resultType,
      final String summaryHexagonType, final String optOverlappingHexagonType, final String optEmissionResultKey, final String optProcurementPolicy) {
    try {
      final SituationCalculations situationCalculations = jobsService.getSituationCalculations(jobKey);
      final Optional<Integer> calculationId = situationCalculations.getCalculationId(situationId);
      if (calculationId.isEmpty()) {
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No job/situation combination found with supplied values");
      } else {
        final int jobId = jobsService.getJobId(jobKey);
        final ScenarioResultType resultTypeType = Optional.ofNullable(resultType).map(ScenarioResultType::valueOf)
            .orElse(ScenarioResultType.SITUATION_RESULT);
        final SummaryHexagonType hexagonType = Optional.ofNullable(summaryHexagonType).map(SummaryHexagonType::valueOf)
            .orElse(SummaryHexagonType.RELEVANT_HEXAGONS);
        final OverlappingHexagonType overlappingHexagonType = Optional.ofNullable(optOverlappingHexagonType).map(OverlappingHexagonType::valueOf)
            .orElse(OverlappingHexagonType.ALL_HEXAGONS);
        final EmissionResultKey emissionResultKey = Optional.ofNullable(optEmissionResultKey).map(EmissionResultKey::valueOf)
            .orElse(EmissionResultKey.NOXNH3_DEPOSITION);
        final ProcurementPolicy procurementPolicy = Optional.ofNullable(optProcurementPolicy).map(ProcurementPolicy::valueOf)
            .orElse(null);
        final SummaryRequest summaryRequest = new SummaryRequest(resultTypeType, jobId, calculationId.get(), hexagonType, overlappingHexagonType,
            emissionResultKey, procurementPolicy);
        final SituationResultsSummary result = resultsService
            .determineSituationResultsSummary(situationCalculations, summaryRequest);
        return responseService.toOkResponse(result);
      }
    } catch (final AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<Object> retrieveJobSummary(final String jobKey) {
    try {
      final int jobId = jobsService.getJobId(jobKey);
      if (jobId == 0) {
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, NO_JOB_FOUND_WITH_THIS_KEY);
      } else {
        final JobSummary result = resultsService.determineJobSummary(jobId);
        return responseService.toOkResponse(result);
      }
    } catch (final AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<Object> retrieveDecisionFrameworkResults(final String jobKey) {
    try {
      final int jobId = jobsService.getJobId(jobKey);
      if (jobId == 0) {
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, NO_JOB_FOUND_WITH_THIS_KEY);
      }
      final SituationCalculations situationCalculations = jobsService.getSituationCalculations(jobKey);
      final Optional<Integer> calculationId = situationCalculations.getCalculationIdOfSingleSituationType(SituationType.PROPOSED);
      if (calculationId.isEmpty()) {
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No job/situation combination found that matches decision framework results");
      } else {
        final DecisionFrameworkResults result = resultsService.determineDecisionFrameworkResults(jobId, calculationId.get());
        return responseService.toOkResponse(result);
      }
    } catch (final AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<Object> retrieveStaticInfoSummary(final String body) {
    try {
      final UIInfoMarkerRequest infoMarker = objectMapper.readValue(body, UIInfoMarkerRequest.class);
      final Object result = infoService.retrieveStaticInfoSummary(
          infoMarker.getReceptorId(),
          infoMarker.getCalculationYear());
      return responseService.toOkResponse(result);
    } catch (final IOException | AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<Object> retrieveJobInfoSummary(final String body) {
    try {
      final UIInfoMarkerRequest infoMarker = objectMapper.readValue(body, UIInfoMarkerRequest.class);
      final Object result = infoService.retrieveJobInfoSummary(
          infoMarker.getReceptorId(),
          infoMarker.getJobKey());
      return responseService.toOkResponse(result);
    } catch (final IOException | AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  private void sanityCheck(final UiCalculationRequest request) throws AeriusException {
    if (request == null) {
      // not sure if this can happen, but better check for it.
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
    // If the job is not completed a new job should be started.
    if (request.getJobKey() != null
        && jobRepository.getProgressForKey(request.getJobKey()).map(p -> p.getState() != JobState.COMPLETED).orElse(Boolean.FALSE)) {
      request.setJobKey(null);
    }
    if (request.getJobKey() == null) {
      final Scenario scenario = request.getScenario();

      if (scenario == null || scenario.getSituations() == null || scenario.getSituations().isEmpty()) {
        throw new AeriusException(AeriusExceptionReason.CONNECT_NO_SOURCES);
      }
    }
    if (request.getTheme() == null) {
      throw new AeriusException(AeriusExceptionReason.CONNECT_CALCULATION_METHOD_SUPPLIED_NOT_SUPPORTED);
    }
  }

  @Override
  public ResponseEntity<Object> refreshEmissions(final String body) {
    try {
      final UiRefreshEmissionsRequest request = objectMapper.readValue(body, UiRefreshEmissionsRequest.class);
      final FeatureCollection<EmissionSourceFeature> sources = request.getSources();
      sourcesService.validateSources(sources.getFeatures());
      sourcesService.refreshEmissions(sources.getFeatures(), request.getYear());
      return responseService.toOkResponse(sources);
    } catch (final IOException | AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<Object> determineCalculationPoints(final String body) {
    try {
      final UiDetermineCalculationPointsRequest request = objectMapper.readValue(body, UiDetermineCalculationPointsRequest.class);
      final FeatureCollection<EmissionSourceFeature> sources = request.getSources();

      final UiDetermineCalculationPointsResult result = calculationPointService.determineCalculationPoints(sources.getFeatures(),
          request.isPointsAbroad(),
          request.getRadiusInland());
      return responseService.toOkResponse(result);
    } catch (final IOException | AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<Object> suggestWaterway(final String body) {
    try {
      final Geometry geometry = objectMapper.readValue(body, Geometry.class);
      final List<InlandWaterway> waterways = sourcesService.suggestInlandShippingWaterway(geometry);
      return responseService.toOkResponse(waterways);
    } catch (final IOException | AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<Object> getContext(final String productProfile, final String locale) {
    try {
      final Locale localeObj = LocaleUtils.getLocale(locale);
      final ProductProfile productProfileObj = ProductProfile.safeValueOf(productProfile);
      return responseService.toOkResponse(contextService.getContext(productProfileObj, localeObj));
    } catch (final AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public Optional<NativeWebRequest> getRequest() {
    return UiApiDelegate.super.getRequest();
  }
}
