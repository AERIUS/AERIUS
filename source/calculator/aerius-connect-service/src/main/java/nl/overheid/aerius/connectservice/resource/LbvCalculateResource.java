/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.connectservice.resource;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import nl.overheid.aerius.connectservice.api.v8.LbvApiDelegate;
import nl.overheid.aerius.connectservice.auth.AuthenticationService;
import nl.overheid.aerius.connectservice.model.CalculateResponse;
import nl.overheid.aerius.connectservice.model.LbvCalculationOptions;
import nl.overheid.aerius.connectservice.model.UploadFile;
import nl.overheid.aerius.connectservice.service.CalculateService;
import nl.overheid.aerius.connectservice.service.ObjectValidatorService;
import nl.overheid.aerius.connectservice.service.ResponseService;
import nl.overheid.aerius.connectservice.service.ScenarioService;
import nl.overheid.aerius.importer.ImportOption;
import nl.overheid.aerius.shared.domain.Theme;

@Service
@Profile("lbv")
public class LbvCalculateResource extends CalculateResource<LbvCalculationOptions> implements LbvApiDelegate {
  public LbvCalculateResource(final AuthenticationService authenticationService,
      final ResponseService responseService,
      final ScenarioService scenarioService,
      final ObjectValidatorService validatorService,
      final CalculateService<LbvCalculationOptions> calculateService) {
    super(authenticationService, responseService, scenarioService, validatorService, calculateService);
  }

  @Override
  public ResponseEntity<CalculateResponse> calculateLbv(final LbvCalculationOptions options, final MultipartFile file) {
    final UploadFile uploadFile = new UploadFile();
    uploadFile.setFileName(file.getOriginalFilename());
    uploadFile.setSituation(UploadFile.SituationEnum.DEFINED_BY_FILE);
    return calculate(List.of(uploadFile), List.of(file), Theme.OWN2000, Set.of(ImportOption.VALIDATE_AGAINST_SCHEMA), options);
  }

  @Override
  public Optional<NativeWebRequest> getRequest() {
    return LbvApiDelegate.super.getRequest();
  }
}
