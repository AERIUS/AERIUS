/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.auth;

import java.util.Optional;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import nl.overheid.aerius.connectservice.service.UserService;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.exception.AeriusException;

@Component
public class AuthenticationService {

  private static final String ANONYMOUS_USER_PRINCIPAL = "anonymousUser";
  private final UserService userService;

  AuthenticationService(final UserService userService) {
    this.userService = userService;
  }

  public String getCurrentApiKey() {
    return (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
  }

  public Optional<ConnectUser> getOptionalCurrentUser() throws AeriusException {
    String apiKey = getCurrentApiKey();
    if (ANONYMOUS_USER_PRINCIPAL.equals(apiKey)) {
      return Optional.empty();
    }

    return Optional.of(userService.getUserWithoutValidatingJobLimits(apiKey));
  }

  /**
   * Fetch the current user.
   * Will NOT validate any limits on jobs.
   */
  public ConnectUser getCurrentUser() throws AeriusException {
    return userService.getUserWithoutValidatingJobLimits(getCurrentApiKey());
  }

  /**
   * Fetch the current user with given API key.
   * Will validate limits on jobs.
   */
  public ConnectUser getCurrentUserWithValidatingJobLimits() throws AeriusException {
    return userService.getUserWithValidatingJobLimits(getCurrentApiKey());
  }
}
