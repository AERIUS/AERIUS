/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.resource;

import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import nl.overheid.aerius.connectservice.api.v8.AnalysisApiDelegate;
import nl.overheid.aerius.connectservice.auth.AuthenticationService;
import nl.overheid.aerius.connectservice.model.CalculateResponse;
import nl.overheid.aerius.connectservice.model.OwN2000AnalysisOptions;
import nl.overheid.aerius.connectservice.model.UploadFile;
import nl.overheid.aerius.connectservice.service.ObjectValidatorService;
import nl.overheid.aerius.connectservice.service.OwN2000AnalysisService;
import nl.overheid.aerius.connectservice.service.ResponseService;
import nl.overheid.aerius.connectservice.service.ScenarioService;
import nl.overheid.aerius.importer.ImportOption;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Service class for starting a OwN2000 theme analysis calculation.
 */
@Service
@Profile("nl")
public class OwN2000AnalysisResource extends CalculateResource<OwN2000AnalysisOptions> implements AnalysisApiDelegate {

  @Autowired
  OwN2000AnalysisResource(final AuthenticationService authenticationService, final ResponseService responseService,
      final ScenarioService scenarioService, final ObjectValidatorService validatorService, final OwN2000AnalysisService calculateService) {
    super(authenticationService, responseService, scenarioService, validatorService, calculateService);
  }

  @Override
  public Optional<NativeWebRequest> getRequest() {
    return AnalysisApiDelegate.super.getRequest();
  }

  @Override
  public ResponseEntity<CalculateResponse> analyseOwN2000(final OwN2000AnalysisOptions options, final List<UploadFile> files,
      final List<MultipartFile> fileParts) {
    final Set<ImportOption> additionaImportOptions = determineImportOptions(options);
    return calculate(files, fileParts, Theme.OWN2000, additionaImportOptions, options);
  }

  @Override
  protected void validateOptions(final OwN2000AnalysisOptions options) throws AeriusException {
    if (isOpsRawInput(options) && options.getOutputType() == OwN2000AnalysisOptions.OutputTypeEnum.GML) {
      throw new AeriusException(AeriusExceptionReason.CONNECT_OPTION_COMBINATION_NOT_ALLOWED, "opsOptions.rawInput=true", "outputType=GML");
    }
    validateSplitSubReceptorWorkValidation(options);
  }

  private static Set<ImportOption> determineImportOptions(final OwN2000AnalysisOptions options) {
    final Set<ImportOption> additionalOptions = EnumSet.noneOf(ImportOption.class);
    if (!Boolean.FALSE.equals(options.getValidateAgainstSchema())) {
      additionalOptions.add(ImportOption.VALIDATE_AGAINST_SCHEMA);
    }
    if (isOpsRawInput(options)) {
      additionalOptions.add(ImportOption.IMPORT_SOURCE_DIAMETER);
    }
    return additionalOptions;
  }

  private static boolean isOpsRawInput(final OwN2000AnalysisOptions options) {
    return options.getOpsOptions() != null && Boolean.TRUE.equals(options.getOpsOptions().getRawInput());
  }

  private static void validateSplitSubReceptorWorkValidation(final OwN2000AnalysisOptions options) throws AeriusException {
    if (options.getSubReceptors() == OwN2000AnalysisOptions.SubReceptorsEnum.DISABLED && Boolean.TRUE.equals(options.getSplitSubReceptorWork())) {
      throw new AeriusException(AeriusExceptionReason.CONNECT_OPTION_COMBINATION_NOT_ALLOWED, "splitSubReceptorWork=true", "subReceptors=DISABLED");
    }
  }
}
