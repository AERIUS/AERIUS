/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.overheid.aerius.connectservice.domain.UiDetermineCalculationPointsResult;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.calculationpoint.CalculationPointRepository;
import nl.overheid.aerius.db.calculator.calculationpoint.DeterminedCalculationPointsCollector;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.v2.geojson.FeatureCollection;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CustomCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Service for requests related to calculation points
 */
@Service
public class UiCalculationPointService {

  private static final Logger LOGGER = LoggerFactory.getLogger(UiCalculationPointService.class);

  private final PMF pmf;

  @Autowired
  public UiCalculationPointService(final PMF pmf) {
    this.pmf = pmf;
  }

  /**
   * Determine calculation points for a set of sources
   *
   * @param sources
   * @param pointsAbroad whether to include points in areas abroad
   * @param radiusInland radius of inland area points, null of no points in inland
   * @return calculation points and the number of assessment areas
   * @throws AeriusException
   */
  public UiDetermineCalculationPointsResult determineCalculationPoints(final List<EmissionSourceFeature> sources, final boolean pointsAbroad,
      final Double radiusInland) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {

      final DeterminedCalculationPointsCollector determinationResult = new DeterminedCalculationPointsCollector();
      if (pointsAbroad) {
        CalculationPointRepository.determinePointsAbroad(con, determinationResult, sources);
      }
      if (radiusInland != null) {
        final int maxRadius = ConstantRepository.getInteger(con, SharedConstantsEnum.DEFAULT_CALCULATION_POINTS_PLACEMENT_RADIUS);
        final int distanceFromSource = (int) Math.round(Math.min(radiusInland, maxRadius));

        CalculationPointRepository.determinePointsInlandAssessmentAreas(con, determinationResult, sources, distanceFromSource);
        CalculationPointRepository.determinePointsInlandHabitats(con, determinationResult, sources, distanceFromSource);
      }

      final FeatureCollection<CalculationPointFeature> points = new FeatureCollection<>();
      determinationResult.getPoints().stream().map(p -> {
        final CalculationPointFeature feature = new CalculationPointFeature();
        feature.setGeometry(new Point(p.getX(), p.getY()));
        feature.setProperties(new CustomCalculationPoint());
        feature.getProperties().setLabel(p.getLabel());
        return feature;
      }).forEach(p -> points.getFeatures().add(p));

      return new UiDetermineCalculationPointsResult(points, determinationResult.getAmountOfAssessmentAreas());
    } catch (final SQLException e) {
      LOGGER.error("SQL Error determining calculation points", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

}
