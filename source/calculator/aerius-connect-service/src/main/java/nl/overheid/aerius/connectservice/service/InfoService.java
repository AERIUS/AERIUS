/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.overheid.aerius.db.calculator.CalculationInfoRepositoryBean;
import nl.overheid.aerius.db.common.ReceptorInfoRepositoryBean;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculation;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.domain.info.JobReceptorInfo;
import nl.overheid.aerius.shared.domain.info.ScenarioEmissionResults;
import nl.overheid.aerius.shared.domain.info.StaticReceptorInfo;
import nl.overheid.aerius.shared.domain.result.EmissionResults;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

@Service
public class InfoService {
  private static final Logger LOG = LoggerFactory.getLogger(InfoService.class);

  private final ReceptorInfoRepositoryBean receptorInfoRepository;
  private final CalculationInfoRepositoryBean calculationInfoRepository;
  private final UiJobsService jobsService;
  private final LocaleService localeService;
  private final ReceptorUtil receptorUtil;

  @Autowired
  public InfoService(final ReceptorInfoRepositoryBean receptorInfoRepository, final CalculationInfoRepositoryBean calculationInfoRepository,
      final UiJobsService jobsService, final LocaleService localeService, final ReceptorUtil receptorUtil) {
    this.receptorInfoRepository = receptorInfoRepository;
    this.calculationInfoRepository = calculationInfoRepository;
    this.jobsService = jobsService;
    this.localeService = localeService;
    this.receptorUtil = receptorUtil;
  }

  public StaticReceptorInfo retrieveStaticInfoSummary(final int receptorId, final int calculationYear) throws AeriusException {
    LOG.debug("Retrieving info summary for receptor: {}, calculationYear: {}", receptorId, calculationYear);
    final StaticReceptorInfo info = new StaticReceptorInfo();

    if (receptorId < 0) {
      return info;
    }

    final DBMessagesKey dbMessagesKey = new DBMessagesKey(localeService.getLocale());

    receptorInfoRepository.fillReceptorInfoForNature(info, receptorId, dbMessagesKey);

    info.setHexagonTypes(receptorInfoRepository.getHexagonTypes(receptorId));
    final Point receptorPoint = receptorUtil.getPointFromReceptorId(receptorId);
    info.setBackgroundEmissionResults(receptorInfoRepository.getBackgroundEmissionResult(receptorPoint, receptorId, calculationYear));

    return info;
  }

  public JobReceptorInfo retrieveJobInfoSummary(final int receptorId, final String jobKey) throws AeriusException {
    LOG.debug("Retrieving info summary for receptor for job: {}, jobKey: {}", receptorId, jobKey);
    final JobReceptorInfo info = new JobReceptorInfo();

    if (receptorId < 0) {
      return info;
    }

    addJobInformation(info, receptorId, jobKey);
    return info;
  }

  private void addJobInformation(final JobReceptorInfo jobReceptorInfo, final int receptorId, final String jobKey) throws AeriusException {
    if (jobKey != null) {
      final SituationCalculations situationCalculations = jobsService.getSituationCalculations(jobKey);

      for (final SituationCalculation calculation : situationCalculations) {
        addCalculationInformation(jobReceptorInfo, receptorId, calculation.getSituationId(), calculation.getCalculationId());
      }
      addScenarioResultInformation(jobReceptorInfo, receptorId, jobKey);
    }
  }

  private void addCalculationInformation(final JobReceptorInfo jobReceptorInfo, final int receptorId, final String situationId,
      final int calculationId) throws AeriusException {
    final EmissionResults calculationResults = calculationInfoRepository.getEmissionResults(calculationId, receptorId);

    if (!calculationResults.isEmpty()) {
      jobReceptorInfo.putSituationsEmissionResults(situationId, calculationResults);
    }
  }

  private void addScenarioResultInformation(final JobReceptorInfo jobReceptorInfo, final int receptorId, final String jobKey) throws AeriusException {
    final Map<ScenarioResultType, ScenarioEmissionResults> scenarioResults = calculationInfoRepository.getScenarioResults(jobKey, receptorId);

    if (!scenarioResults.isEmpty()) {
      for (final Entry<ScenarioResultType, ScenarioEmissionResults> entry : scenarioResults.entrySet()) {
        jobReceptorInfo.putScenarioEmissionResults(entry.getKey(), entry.getValue());
      }
    }
  }
}
