/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.ShippingRepositoryBean;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.common.sector.SectorRepositoryBean;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.gml.base.GMLHelper;
import nl.overheid.aerius.importer.GMLHelperImpl;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.InlandWaterway;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.geometry.GeometryCalculator;
import nl.overheid.aerius.util.LocaleUtils;
import nl.overheid.aerius.validation.CategoryBasedValidationHelper;
import nl.overheid.aerius.validation.ScenarioSituationValidator;
import nl.overheid.aerius.validation.ValidationHelper;

@Service
public class EmissionsSourcesService {

  private final SectorCategories categories;
  private final ShippingRepositoryBean shippingRepository;
  private final GMLHelper gmlHelper;
  private final ValidationHelper validationHelper;

  @Autowired
  public EmissionsSourcesService(final PMF pmf, final SectorRepositoryBean sectorRepository, final ShippingRepositoryBean shippingRepository,
      final GeometryCalculator geometryCalculator) throws AeriusException {
    // Cache the categories instead of retrieving them for every operation
    // Locale should not matter either, since (at the moment) it is only used for determining emissions.
    this.categories = sectorRepository.getSectorCategories(LocaleUtils.getDefaultLocale());
    this.shippingRepository = shippingRepository;
    gmlHelper = new GMLHelperImpl(pmf, categories);
    final CharacteristicsType characteristicsType = ConstantRepository.getEnum(pmf, ConstantsEnum.CHARACTERISTICS_TYPE, CharacteristicsType.class);
    validationHelper = new CategoryBasedValidationHelper(categories, characteristicsType);
  }

  public void validateSources(final List<EmissionSourceFeature> sources) throws AeriusException {
    ScenarioSituationValidator.validateSources(sources, validationHelper);
  }

  public void validateSituation(final ScenarioSituation situation) throws AeriusException {
    ScenarioSituationValidator.validateSituation(situation, validationHelper);
  }

  public void refreshEmissions(final List<EmissionSourceFeature> sources, final int year) throws AeriusException {
    gmlHelper.enforceEmissions(year, sources);
  }

  public List<InlandWaterway> suggestInlandShippingWaterway(final Geometry geometry) throws AeriusException {
    return shippingRepository.suggestInlandShippingWaterway(geometry);
  }

}
