/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.domain;

import nl.overheid.aerius.shared.domain.v2.geojson.FeatureCollection;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;

/**
 * Wrapper class for objects that should be supplied when refreshing emissions for a (set of) sources for the UI.
 */
public class UiRefreshEmissionsRequest {

  private FeatureCollection<EmissionSourceFeature> sources;
  private int year;

  public FeatureCollection<EmissionSourceFeature> getSources() {
    return sources;
  }

  public void setSources(final FeatureCollection<EmissionSourceFeature> sources) {
    this.sources = sources;
  }

  public int getYear() {
    return year;
  }

  public void setYear(final int year) {
    this.year = year;
  }

}
