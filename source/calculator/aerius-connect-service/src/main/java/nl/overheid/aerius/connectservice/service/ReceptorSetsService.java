/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import nl.overheid.aerius.connectservice.domain.ImportParcelAggregate;
import nl.overheid.aerius.connectservice.model.ReceptorSet;
import nl.overheid.aerius.db.connect.ConnectCalculationPointSetsRepositoryBean;
import nl.overheid.aerius.importer.ImportOption;
import nl.overheid.aerius.shared.domain.connect.ConnectCalculationPointSetMetadata;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.domain.importer.ImportProperties;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.CustomPointsUtil;

@Service
public class ReceptorSetsService {

  private static final Set<ImportOption> DEFAULT_OPTIONS = EnumSet.of(
      ImportOption.INCLUDE_CALCULATION_POINTS,
      ImportOption.USE_IMPORTED_LANDUSES,
      ImportOption.VALIDATE_AGAINST_SCHEMA);

  private final ConnectCalculationPointSetsRepositoryBean pointSetsRepository;
  private final ImportService importService;

  @Autowired
  public ReceptorSetsService(final ConnectCalculationPointSetsRepositoryBean pointSetsRepository,
      final ImportService importService) {
    this.pointSetsRepository = pointSetsRepository;
    this.importService = importService;
  }

  public ImportParcelAggregate addReceptorSet(final ConnectUser user, final ReceptorSet receptorSet, final MultipartFile filePart)
      throws AeriusException {
    final String name = receptorSet.getName();

    if (name == null || name.isBlank()) {
      throw new AeriusException(AeriusExceptionReason.CONNECT_USER_CALCULATION_POINT_SET_EMPTY_NAME);
    }
    if (pointSetsRepository.getCalculationPointSetByName(user, name) != null) {
      throw new AeriusException(AeriusExceptionReason.CONNECT_USER_CALCULATION_POINT_SET_ALREADY_EXISTS, name);
    }
    return addCalculationPointSet(filePart, user, receptorSet);
  }

  public void deleteReceptorSet(final ConnectUser user, final String name) throws AeriusException {
    final ConnectCalculationPointSetMetadata existingSet = pointSetsRepository.getCalculationPointSetByName(user, name);
    if (existingSet == null) {
      throw new AeriusException(AeriusExceptionReason.CONNECT_USER_CALCULATION_POINT_SET_DOES_NOT_EXIST, name);
    }
    pointSetsRepository.deleteCalculationPointSet(existingSet.getSetId());
  }

  public List<ReceptorSet> getReceptorSets(final ConnectUser user) throws AeriusException {
    final List<ConnectCalculationPointSetMetadata> metadatas = pointSetsRepository.getCalculationPointSets(user);
    return metadatas.stream()
        .map(ReceptorSetsService::convert)
        .collect(Collectors.toList());
  }

  private ImportParcelAggregate addCalculationPointSet(final MultipartFile filePart, final ConnectUser user, final ReceptorSet receptorSet)
      throws AeriusException {
    final ConnectCalculationPointSetMetadata metadata = new ConnectCalculationPointSetMetadata();
    metadata.setUserId(user.getId());
    metadata.setName(receptorSet.getName());
    metadata.setDescription(receptorSet.getDescription());
    final List<ImportParcel> importResult = importService.importFile(filePart, determineOptions(receptorSet), new ImportProperties());
    final ImportParcelAggregate importAggregate = new ImportParcelAggregate(importResult);
    if (importAggregate.getExceptions().isEmpty()) {
      persistCalculationPointSet(metadata, importResult);
    }
    return importAggregate;
  }

  private static Set<ImportOption> determineOptions(final ReceptorSet receptorSet) {
    final Set<ImportOption> options = new HashSet<>();
    options.addAll(DEFAULT_OPTIONS);
    if (Boolean.TRUE.equals(receptorSet.getExpectRcpHeight())) {
      options.add(ImportOption.USE_IMPORTED_RECEPTOR_HEIGHT);
    }
    return options;
  }

  private void persistCalculationPointSet(final ConnectCalculationPointSetMetadata metadata, final List<ImportParcel> importParcels)
      throws AeriusException {
    final List<CalculationPointFeature> customPointList = new ArrayList<>();
    for (final ImportParcel parcel : importParcels) {
      customPointList.addAll(parcel.getCalculationPointsList());
    }
    if (customPointList.isEmpty()) {
      throw new AeriusException(AeriusExceptionReason.CONNECT_NO_RECEPTORS_IN_PARAMETERS);
    }
    CustomPointsUtil.ensureProperIds(customPointList, false);
    pointSetsRepository.insertCalculationPointSet(metadata, customPointList);
  }

  private static ReceptorSet convert(final ConnectCalculationPointSetMetadata metadata) {
    return new ReceptorSet()
        .name(metadata.getName())
        .description(metadata.getDescription());
  }
}
