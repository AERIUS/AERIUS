/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.domain;

import java.io.Serializable;

import nl.overheid.aerius.shared.domain.v2.geojson.FeatureCollection;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;

public class UiDetermineCalculationPointsResult implements Serializable {

  private final FeatureCollection<CalculationPointFeature> computedCalculationPoints;

  private final int assessmentAreas;

  public UiDetermineCalculationPointsResult(final FeatureCollection<CalculationPointFeature> computedCalculationPoints, final int assessmentAreas) {
    this.computedCalculationPoints = computedCalculationPoints;
    this.assessmentAreas = assessmentAreas;
  }

  public final FeatureCollection<CalculationPointFeature> getComputedCalculationPoints() {
    return computedCalculationPoints;
  }

  public final int getAssessmentAreas() {
    return assessmentAreas;
  }

}
