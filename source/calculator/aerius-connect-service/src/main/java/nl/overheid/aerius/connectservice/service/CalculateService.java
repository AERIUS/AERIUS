/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Generic interface for CalculateService. This allows for the CalculateResource to use a generic interface being passed as calculate service.
 * @param <T> Type of calculate service options.
 */
public interface CalculateService<T> {

  /**
   * Starts a calculation.
   *
   * @param connectUser
   * @param scenario
   * @param options
   * @return
   * @throws AeriusException
   */
  String calculate(final ConnectUser connectUser, final Scenario scenario, final T options) throws AeriusException;

  /**
   * Additional method to validate the constructed scenario before sending it to the worker.
   *
   * @param scenarioConstruction
   */
  default void validateInput(final ScenarioConstruction scenarioConstruction) {
    // Default no additional input validations.
  }
}
