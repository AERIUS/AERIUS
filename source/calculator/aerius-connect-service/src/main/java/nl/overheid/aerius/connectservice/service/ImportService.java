/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import nl.overheid.aerius.controller.ImaerController;
import nl.overheid.aerius.importer.ImportOption;
import nl.overheid.aerius.io.ImportableFile;
import nl.overheid.aerius.shared.domain.importer.ImportProperties;
import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.exception.AeriusException;

@Service
public class ImportService {

  private final ImaerController imaerController;
  private final LocaleService localeService;

  @Autowired
  public ImportService(final ImaerController imaerController, final LocaleService localeService) {
    this.imaerController = imaerController;
    this.localeService = localeService;
  }

  public List<ImportParcel> importFile(final MultipartFile filePart, final Set<ImportOption> options, final ImportProperties importProperties)
      throws AeriusException {
    final ImportableFile file = new MultipartImportableFile(filePart);
    return importFile(file, options, importProperties);
  }

  public List<ImportParcel> importFile(final ImportableFile importableFile, final Set<ImportOption> options, final ImportProperties importProperties)
      throws AeriusException {
    return imaerController.processFile(importableFile, options, importProperties, localeService.getLocale());
  }

  /**
   * Returns the input stream to the imported file. If the data is not a zip file, it will first put the file into a zip and return a stream to
   * the zipped content.
   *
   * @param inputStream input stream to the data of the upload file
   * @param importType file type of the file upload
   * @param filename original file name of the file uploaded
   * @throws IOException
   */
  public static InputStream toZipInputSteam(final InputStream inputStream, final ImportType importType, final String filename) throws IOException {
    if (importType == ImportType.ZIP) {
      return inputStream;
    } else {
      try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
        // Close zip output stream before converting to byte array otherwise it won't create valid zip content.
        try (ZipOutputStream zipOutput = new ZipOutputStream(out)) {
          zipOutput.putNextEntry(new ZipEntry(filename));
          inputStream.transferTo(zipOutput);
          zipOutput.closeEntry();
        }
        return new ByteArrayInputStream(out.toByteArray());
      } finally {
        inputStream.close();
      }
    }
  }

}
