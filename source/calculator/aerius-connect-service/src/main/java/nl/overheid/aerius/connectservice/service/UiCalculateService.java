/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.overheid.aerius.calculation.NCACalculationOptionsUtil;
import nl.overheid.aerius.calculation.OwN2000CalculationOptionsUtil;
import nl.overheid.aerius.connectservice.domain.ExportOptions;
import nl.overheid.aerius.connectservice.domain.UiCalculationRequest;
import nl.overheid.aerius.connectservice.util.CalculationJobTypeValidator;
import nl.overheid.aerius.connectservice.util.ScenarioUtil;
import nl.overheid.aerius.db.calculator.CalculatorLimitsRepository;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.ConstantRepositoryBean;
import nl.overheid.aerius.db.connect.ConnectCalculationPointSetsRepositoryBean;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.server.service.export.ExportTaskClientBean;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.domain.export.ExportAppendix;
import nl.overheid.aerius.shared.domain.export.ExportData.ExportAdditionalOptions;
import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioResults;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.geometry.EmissionSourceLimits;
import nl.overheid.aerius.shared.util.ApplicationSectionsUtil;
import nl.overheid.aerius.taskmanager.client.QueueEnum;
import nl.overheid.aerius.validation.EmissionSourceLimitValidator;

@Service
public class UiCalculateService extends AbstractCalculateService<UiCalculationRequest, Void> {
  private static final Logger LOGGER = LoggerFactory.getLogger(UiCalculateService.class);

  /**
   * Defines the default number of sources to determine if to run or small or large calculations queue.
   */
  private static final int DEFAULT_CALCULATOR_LARGE_CALCULATIONS_SPLIT = 100;

  private final EmissionsSourcesService sourcesService;
  private final CalculatorLimitsRepository calculatorLimitsRepository;
  private final NCACalculationOptionsUtil ncaCalculationOptionsUtil;
  private final ScenarioUtil scenarioUtil;

  @Autowired
  UiCalculateService(final ExportTaskClientBean exportTaskclient, final JobRepositoryBean jobRepository,
      final ConnectCalculationPointSetsRepositoryBean pointSetsRepository, final ConstantRepositoryBean constantRepository,
      final ProxyFileService fileService, final EmissionsSourcesService emissionsSourcesService, final ScenarioUtil scenarioUtil,
      final CalculatorLimitsRepository calculatorLimitsRepository, final LocaleService localeService,
      final NCACalculationOptionsUtil ncaCalculationOptionsUtil) {
    super(exportTaskclient, jobRepository, pointSetsRepository, constantRepository, fileService, localeService, emissionsSourcesService);
    this.sourcesService = emissionsSourcesService;
    this.scenarioUtil = scenarioUtil;
    this.calculatorLimitsRepository = calculatorLimitsRepository;
    this.ncaCalculationOptionsUtil = ncaCalculationOptionsUtil;
  }

  @Override
  protected JobType getJobType(final UiCalculationRequest options) {
    return Optional.ofNullable(options.getExportOptions())
        .map(ExportOptions::getJobType)
        .orElse(JobType.CALCULATION);
  }

  @Override
  protected Optional<String> getJobName(final UiCalculationRequest options) {
    return Optional.empty();
  }

  @Override
  protected boolean isProtectJob(final UiCalculationRequest options) {
    return Optional.ofNullable(options.getExportOptions())
        .map(ExportOptions::isProtectJob)
        .orElse(false);
  }

  @Override
  protected boolean useCustomPoints(final UiCalculationRequest request) {
    // Unless explicitly avoid using custom points.
    // It's either the calculation type is custom points,
    // in which case there should always be custom points supplied (checked in calling method).
    // Or just use/add the custom points that were supplied, if any.
    final CalculationSetOptions options = request.getScenario().getOptions();
    final ExportType exportType = request.getExportOptions().getExportType();

    return !explicitlyAvoidCustomPoints(options, exportType)
        && (explicitlyUseCustomPoints(options, exportType) || !request.getScenario().getCustomPointsList().isEmpty());
  }

  private static boolean explicitlyUseCustomPoints(final CalculationSetOptions options, final ExportType exportType) {
    return options != null
        && exportType != ExportType.PDF_PAA
        && exportType != ExportType.GML_SOURCES_ONLY
        && options.getCalculationMethod() == CalculationMethod.CUSTOM_POINTS;
  }

  /**
   * Avoid custom points if no custom points are used in a calculation
   * or if the export is a model export (OPS/ADMS) and the calculation method isn't custom points.
   * Ignore for model export because it supports either receptor export or custom points, not both.
   *
   * @param options
   * @param exportType
   * @return true if custom points should be avoided.
   */
  private static boolean explicitlyAvoidCustomPoints(final CalculationSetOptions options, final ExportType exportType) {
    return options != null
        && (!ApplicationSectionsUtil.isCustomPointsUsed(options.getCalculationJobType())
            || (options.getCalculationMethod() != CalculationMethod.CUSTOM_POINTS
                && (exportType == ExportType.OPS || exportType == ExportType.ADMS)));
  }

  @Override
  protected String getReceptorSetName(final UiCalculationRequest options) {
    return null;
  }

  @Override
  protected Optional<Integer> getOverrideYear(final UiCalculationRequest options) {
    return Optional.empty();
  }

  @Override
  protected void validate(final Scenario scenario, final UiCalculationRequest options, final Void additionalData)
      throws AeriusException {
    final EmissionSourceLimits limits = calculatorLimitsRepository.getCalculatorLimits();
    if (!options.getExportOptions().getExportType().isSourceExport()) {
      CalculationJobTypeValidator.validate(scenario);

      final int numberOfSources = scenario.getSituations().stream().mapToInt(s -> s.getEmissionSourcesList().size()).sum();

      EmissionSourceLimitValidator.check(numberOfSources, limits);
    }

    for (final ScenarioSituation situation : scenario.getSituations()) {
      if (!options.getExportOptions().getExportType().isSourceExport()) {
        final List<AeriusException> exceptions = EmissionSourceLimitValidator.checkGeometries(situation.getEmissionSourcesList(), limits);

        if (!exceptions.isEmpty()) {
          throw exceptions.get(0);
        }
      }
      sourcesService.validateSituation(situation);
    }
  }

  @Override
  protected void updateScenarioOptions(final Scenario scenario, final UiCalculationRequest request) throws AeriusException {
    scenario.setTheme(request.getTheme());
    final CalculationSetOptions options = request.getScenario().getOptions();
    final ExportType exportType = request.getExportOptions().getExportType();
    switch (request.getTheme()) {
    case OWN2000:
      final CalculationSetOptions cso = OwN2000CalculationOptionsUtil.createOwN2000CalculationSetOptions();

      final CalculationJobType calculationJobType = Optional.ofNullable(options.getCalculationJobType())
          .orElse(CalculationJobType.PROCESS_CONTRIBUTION);
      cso.setCalculationJobType(calculationJobType);
      cso.setCalculationMethod(
          explicitlyUseCustomPoints(options, exportType) ? CalculationMethod.CUSTOM_POINTS : CalculationMethod.FORMAL_ASSESSMENT);
      cso.setCalculationJobType(options.getCalculationJobType());
      scenario.setOptions(cso);
      break;
    case NCA:
      scenario.setOptions(ncaCalculationOptionsUtil.createCalculationSetOptions(options,
          explicitlyUseCustomPoints(options, exportType), request.getExportOptions().isDemoMode()));
      break;
    case RBL:
    default:
      // Not yet implemented theme
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }

  @Override
  protected void setExportProperties(final ExportProperties exportProperties, final UiCalculationRequest options) {
    Optional.ofNullable(options.getExportOptions())
        .ifPresent(requestOptions -> {
          exportProperties.setName(requestOptions.getName());
          exportProperties.setExportType(requestOptions.getExportType());
          exportProperties.setEmailAddress(requestOptions.getEmail());
          exportProperties.setAppendices(requestOptions.getAppendices());
        });

    if (options.getExportOptions().isDemoMode()) {
      exportProperties.getAdditionalOptions().add(ExportAdditionalOptions.DEMO_MODE);
    }
    addRequiredAppendices(exportProperties, options);
  }

  private static void addRequiredAppendices(final ExportProperties exportProperties, final UiCalculationRequest options) {
    Optional.ofNullable(options.getExportOptions())
        .map(ExportOptions::getExportType)
        .ifPresent(exportType -> {
          if (options.getTheme() == Theme.OWN2000
              && (exportType == ExportType.PDF_PAA || exportType == ExportType.PDF_SINGLE_SCENARIO)) {
            if (exportProperties.getAppendices() == null) {
              exportProperties.setAppendices(new HashSet<>());
            }
            exportProperties.getAppendices().add(ExportAppendix.EXTRA_ASSESSMENT_REPORT);
          }
        });
  }

  @Override
  protected String calculate(final Optional<ConnectUser> connectUser, final Scenario scenario, final UiCalculationRequest options,
      final Void additionalData) throws AeriusException {
    // If no job key of job is not completed start a new calculation.
    if (options.getJobKey() == null) {
      return super.calculate(connectUser, scenario, options, additionalData);
    } else {
      return exportWithExistingResults(connectUser, scenario, options);
    }
  }

  /**
   * Fast export on existing calculation by creating a new job, copy scenario object related to results with new meta data,
   * and link calculation results in the database. But if no scenario found on the file server, or no results start a new calculation.
   *
   * @param connectUser
   * @param uiScenario the scenario passed from the UI
   * @param options
   * @return new job key
   * @throws AeriusException
   */
  private String exportWithExistingResults(final Optional<ConnectUser> connectUser, final Scenario uiScenario, final UiCalculationRequest options)
      throws AeriusException {
    final Scenario scenario = scenarioUtil.getScenario(options);
    // If no scenario found start as a new calculation.
    if (scenario != null) {
      final String baseJobKey = options.getJobKey();
      final ScenarioResults scenarioResults = scenarioUtil.getScenarioResults(baseJobKey);
      // If no results available start as a new calculation.
      if (!scenarioResults.getResultsPerSituation().isEmpty()) {
        // Copy the existing scenario and update the scenario meta data.
        final String newJobKey = createJob(connectUser, options);

        scenario.setMetaData(options.getScenario().getMetaData());

        options.getExportOptions().setName(newJobKey);
        final ExportProperties exportProperties = toExportProperties(connectUser, options);
        // Link the existing calculation results to this new job.
        exportProperties.setScenarioResults(scenarioResults);
        jobRepository.copyJobResults(baseJobKey, newJobKey);

        // Check if results were actually copied.
        final ScenarioResults sanityCheckScenarioResults = scenarioUtil.getScenarioResults(newJobKey);
        // If we have no calculation results here we will start an new calculation anyway
        if (sanityCheckScenarioResults.getResultsPerSituation().isEmpty()) {
          LOGGER.info("No fast export, because no results, will just restart the calculation");
          jobRepository.deleteJob(newJobKey);
        } else {
          LOGGER.info("Fast export derived from job {} with new job key: {}", baseJobKey, newJobKey);
          sendTask(scenario, options, null, exportProperties, newJobKey);
          return newJobKey;
        }
      }
    }
    return super.calculate(connectUser, uiScenario, options, null);
  }

  @Override
  protected boolean shouldSendEmail(final UiCalculationRequest options) {
    return options.getExportOptions() != null;
  }

  @Override
  protected QueueEnum determineQueue(final UiCalculationRequest options) {
    final ExportType exportType = options.getExportOptions() == null ? ExportType.CALCULATION_UI : options.getExportOptions().getExportType();

    return switch (exportType) {
      case ADMS, OPS, GML_SOURCES_ONLY -> QueueEnum.INPUT_EXPORT;
      case CALCULATION_UI -> determineCalculationUiQueue(options);
      case PDF_SINGLE_SCENARIO, PDF_PAA -> QueueEnum.CALCULATOR_PAA_EXPORT;
      case PDF_PROCUREMENT_POLICY -> QueueEnum.CALCULATOR_PP_EXPORT;
      case GML_WITH_RESULTS -> QueueEnum.CALCULATOR_GML_EXPORT;
      default -> throw new IllegalArgumentException("Export type '" + exportType + "' not expected from a UI calculation");
    };
  }

  private QueueEnum determineCalculationUiQueue(final UiCalculationRequest options) {
    final int size = options.getScenario().getSituations().stream().mapToInt(s -> s.getEmissionSourcesList().size()).sum();
    int configuredSize = -1;

    try {
      configuredSize = constantRepository.getInteger(ConstantsEnum.CALCULATOR_LARGE_CALCULATIONS_SPLIT);
    } catch (final AeriusException e) {
      LOGGER.error("Error determining CALCULATOR_LARGE_CALCULATIONS_SPLIT", e);
    }

    final int maxSize = configuredSize > 0 ? configuredSize : DEFAULT_CALCULATOR_LARGE_CALCULATIONS_SPLIT;
    return size > maxSize ? QueueEnum.CALCULATOR_UI_LARGE : QueueEnum.CALCULATOR_UI_SMALL;
  }

}
