/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
public class CorsConfig {

  /**
   * CORS filter to allow client calls to be made on a different port.
   */
  @Bean
  public FilterRegistrationBean<?> corsFilter(final CorsProperties properties) {
    final CorsConfiguration config = new CorsConfiguration();

    config.setAllowedOriginPatterns(properties.getAllowedOriginPatterns());
    config.addAllowedMethod(CorsConfiguration.ALL);
    config.applyPermitDefaultValues();
    final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();

    source.registerCorsConfiguration("/**", config);
    final FilterRegistrationBean<?> bean = new FilterRegistrationBean<>(new CorsFilter(source));

    bean.setOrder(0);
    return bean;
  }
}
