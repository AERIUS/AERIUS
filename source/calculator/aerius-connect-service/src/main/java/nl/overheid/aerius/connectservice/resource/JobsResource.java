/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.NativeWebRequest;

import nl.overheid.aerius.connectservice.api.v8.JobsApiDelegate;
import nl.overheid.aerius.connectservice.auth.AuthenticationService;
import nl.overheid.aerius.connectservice.model.JobStatus;
import nl.overheid.aerius.connectservice.service.JobsService;
import nl.overheid.aerius.connectservice.service.ResponseService;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.exception.AeriusException;

@Service
public class JobsResource implements JobsApiDelegate {

  private final AuthenticationService authentication;
  private final ResponseService responseService;
  private final JobsService jobsService;

  @Autowired
  JobsResource(final AuthenticationService authenticationService, final ResponseService responseService, final JobsService jobsService) {
    this.authentication = authenticationService;
    this.responseService = responseService;
    this.jobsService = jobsService;
  }

  @Override
  public ResponseEntity<JobStatus> getJobStatus(final String jobKey) {
    try {
      final ConnectUser connectUser = authentication.getCurrentUser();
      final Optional<JobStatus> job = jobsService.getJob(connectUser, jobKey);
      return responseService.toOkResponseOptional(job, "No job found with this key");
    } catch (final AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<List<JobStatus>> listJobs(final List<String> status) {
    try {
      final ConnectUser connectUser = authentication.getCurrentUser();
      final List<JobStatus> jobs;
      if (status == null || status.isEmpty()) {
        jobs = jobsService.getJobsForUser(connectUser);
      } else {
        jobs = jobsService.getJobsForUserAndJobStates(connectUser, status);
      }
      return responseService.toOkResponse(jobs);
    } catch (final AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<Void> cancelJob(final String jobKey) {
    try {
      final ConnectUser connectUser = authentication.getCurrentUser();
      jobsService.cancelJob(connectUser, jobKey);
      return responseService.toOkResponse();
    } catch (final AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<Void> deleteJob(final String jobKey) {
    try {
      final ConnectUser connectUser = authentication.getCurrentUser();
      jobsService.deleteJob(connectUser, jobKey);
      return responseService.toOkResponse();
    } catch (final AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public Optional<NativeWebRequest> getRequest() {
    return JobsApiDelegate.super.getRequest();
  }
}
