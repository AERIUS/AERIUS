/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.resource;

import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import nl.overheid.aerius.connectservice.api.v8.RblApiDelegate;
import nl.overheid.aerius.connectservice.auth.AuthenticationService;
import nl.overheid.aerius.connectservice.model.CalculateResponse;
import nl.overheid.aerius.connectservice.model.RblCalculationOptions;
import nl.overheid.aerius.connectservice.model.UploadFile;
import nl.overheid.aerius.connectservice.service.ObjectValidatorService;
import nl.overheid.aerius.connectservice.service.RblCalculateService;
import nl.overheid.aerius.connectservice.service.ResponseService;
import nl.overheid.aerius.connectservice.service.ScenarioService;
import nl.overheid.aerius.importer.ImportOption;
import nl.overheid.aerius.shared.domain.Theme;

/**
 * Resource implementation for RBL API.
 */
@Service
@Profile("rbl")
public class RblCalculateResource extends CalculateResource<RblCalculationOptions> implements RblApiDelegate {

  @Autowired
  RblCalculateResource(final AuthenticationService authenticationService, final ResponseService responseService,
      final ScenarioService scenarioService, final ObjectValidatorService validatorService, final RblCalculateService calculateService) {
    super(authenticationService, responseService, scenarioService, validatorService, calculateService);
  }

  @Override
  public ResponseEntity<CalculateResponse> calculateRbl(final RblCalculationOptions options, final List<UploadFile> files,
      final List<MultipartFile> fileParts) {
    final Set<ImportOption> additionaImportOptions = EnumSet.of(
        ImportOption.VALIDATE_AGAINST_SCHEMA,
        ImportOption.INCLUDE_CIMLK_MEASURES,
        ImportOption.INCLUDE_CIMLK_DISPERSION_LINES,
        ImportOption.INCLUDE_CIMLK_CORRECTIONS,
        ImportOption.VALIDATE_CIMLK_COHESION);
    return calculate(files, fileParts, Theme.CIMLK, additionaImportOptions, options);
  }

  @Override
  public Optional<NativeWebRequest> getRequest() {
    return RblApiDelegate.super.getRequest();
  }
}
