/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.ConstantRepositoryBean;
import nl.overheid.aerius.db.connect.ConnectUserRepositoryBean;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.domain.calculation.JobProgress;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

@Service
public class UserService {

  private static final int JOB_RATE_LIMIT_PERIOD_MINUTES = 60 * 24;

  private final ConstantRepositoryBean constantRepository;
  private final ConnectUserRepositoryBean connectUserRepository;
  private final JobRepositoryBean jobRepository;

  @Autowired
  public UserService(final ConstantRepositoryBean constantRepository, final ConnectUserRepositoryBean connectUserRepository,
      final JobRepositoryBean jobRepository) {
    this.constantRepository = constantRepository;
    this.connectUserRepository = connectUserRepository;
    this.jobRepository = jobRepository;
  }

  public String generateAPIKey(final String emailAddress) throws AeriusException {
    validateGenerateKeyEnabled();

    return createOrResetKey(emailAddress).getApiKey();
  }

  private void validateGenerateKeyEnabled() throws AeriusException {
    if (!constantRepository.getBoolean(ConstantsEnum.CONNECT_GENERATING_APIKEY_ENABLED)) {
      throw new AeriusException(AeriusExceptionReason.USER_API_KEY_GENERATION_DISABLED);
    }
  }

  private ConnectUser createOrResetKey(final String email) throws AeriusException {
    ConnectUser user = connectUserRepository.getUserByEmailAddress(email);

    if (user == null) {
      user = createUser(email);
    } else {
      user = resetAPIKey(user);
    }

    return user;
  }

  private ConnectUser createUser(final String email) throws AeriusException {
    final ConnectUser createUser = new ConnectUser();
    createUser.setApiKey(generateAPIKeyString());
    createUser.setEmailAddress(email);
    createUser.setEnabled(true);

    connectUserRepository.createUser(createUser);
    return connectUserRepository.getUserByEmailAddress(email);
  }

  private ConnectUser resetAPIKey(final ConnectUser user) throws AeriusException {
    user.setApiKey(generateAPIKeyString());
    connectUserRepository.updateApiKeyForUser(user);

    return connectUserRepository.getUserByEmailAddress(user.getEmailAddress());
  }

  private String generateAPIKeyString() {
    return UUID.randomUUID().toString().replace("-", "");
  }

  /**
   * Fetch the user with given API key. Will also validate the maximum concurrent jobs. Do this for actions that create jobs, like calculations.
   * @param con The database connection.
   * @param apiKey The API key of the user.
   * @return The user with the matching API key
   * @throws AeriusException Thrown if user not found, account is disabled or the max concurrent jobs is reached.
   */
  public ConnectUser getUserWithValidatingJobLimits(final String apiKey) throws AeriusException {
    final ConnectUser user = getValidatedUser(apiKey);
    checkLimitsForUser(user);
    return user;
  }

  /**
   * Fetch the user with given API key. Will NOT validate the maximum concurrent jobs. Do this for actions which do no create jobs.
   * @param con The database connection.
   * @param apiKey The API key of the user.
   * @return The user with the matching API key
   * @throws AeriusException Thrown if user not found or the account is disabled.
   */
  public ConnectUser getUserWithoutValidatingJobLimits(final String apiKey) throws AeriusException {
    return getValidatedUser(apiKey);
  }

  /**
   * Fetch the user with given API key.
   * @param apiKey The API key of the user.
   * @return The user with the matching API key
   * @throws AeriusException Thrown if user not found or account is disabled.
   */
  private ConnectUser getValidatedUser(final String apiKey) throws AeriusException {
    final ConnectUser user = connectUserRepository.getUserByApiKey(apiKey);

    if (user == null) {
      throw new AeriusException(AeriusExceptionReason.USER_INVALID_API_KEY, apiKey);
    }

    if (!user.isEnabled()) {
      throw new AeriusException(AeriusExceptionReason.USER_ACCOUNT_DISABLED);
    }

    return user;
  }

  /**
   * Fetch the user with given API key.
   * @param apiKey The API key of the user.
   * @return The user with the matching API key, empty if not found
   * @throws AeriusException Thrown in case of database errors.
   */
  public Optional<ConnectUser> getUser(final String apiKey) throws AeriusException {
    return Optional.ofNullable(connectUserRepository.getUserByApiKey(apiKey));
  }

  /**
   * Check the limits for the user.
   * @param user The user to check.
   * @throws AeriusException Thrown if any of the limits are reached.
   */
  private void checkLimitsForUser(final ConnectUser user) throws AeriusException {
    final List<JobProgress> allJobs = jobRepository.getProgressForUser(user);
    final List<JobProgress> activeJobs = allJobs.stream()
        .filter(job -> !job.getState().isFinalState())
        .collect(Collectors.toList());

    if (activeJobs.size() >= user.getMaxConcurrentJobs()) {
      throw new AeriusException(AeriusExceptionReason.USER_MAX_CONCURRENT_JOB_LIMIT_REACHED, String.valueOf(user.getMaxConcurrentJobs()));
    }

    final Date currentPeriodStart = Date.from(ZonedDateTime.now().minusMinutes(JOB_RATE_LIMIT_PERIOD_MINUTES).toInstant());

    final List<JobProgress> periodJobs = allJobs.stream()
        .filter(job -> job.getStartDateTime() != null && job.getStartDateTime().after(currentPeriodStart))
        .collect(Collectors.toList());

    if (periodJobs.size() >= user.getPeriodJobRateLimit()) {
      throw new AeriusException(AeriusExceptionReason.USER_PERIOD_JOB_RATE_LIMIT_REACHED, String.valueOf(user.getPeriodJobRateLimit()));
    }
  }

}
