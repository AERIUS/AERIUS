/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.resource;

import java.io.IOException;
import java.io.InputStream;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import nl.overheid.aerius.connectservice.api.v8.RegisterApiDelegate;
import nl.overheid.aerius.connectservice.auth.AuthenticationService;
import nl.overheid.aerius.connectservice.model.CalculateResponse;
import nl.overheid.aerius.connectservice.model.OwN2000RegisterCalculationOptions;
import nl.overheid.aerius.connectservice.model.RegisterCalculationResults;
import nl.overheid.aerius.connectservice.model.UploadFile;
import nl.overheid.aerius.connectservice.service.CalculateService;
import nl.overheid.aerius.connectservice.service.ImportService;
import nl.overheid.aerius.connectservice.service.ObjectValidatorService;
import nl.overheid.aerius.connectservice.service.ProxyFileService;
import nl.overheid.aerius.connectservice.service.RegisterOwN2000PermitService;
import nl.overheid.aerius.connectservice.service.ResponseService;
import nl.overheid.aerius.connectservice.service.ScenarioConstruction;
import nl.overheid.aerius.connectservice.service.ScenarioService;
import nl.overheid.aerius.connectservice.service.ScenarioSummaryService;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.ConstantRepositoryBean;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.importer.ImportOption;
import nl.overheid.aerius.shared.FileServerExpireTag;
import nl.overheid.aerius.shared.FileServerFile;
import nl.overheid.aerius.shared.domain.AeriusCustomer;
import nl.overheid.aerius.shared.domain.FileFormat;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.domain.importer.ImportProperties;
import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.LocaleUtils;

@Service
@Profile({"nl", "uk"})
public class RegisterResource extends CalculateResource<OwN2000RegisterCalculationOptions> implements RegisterApiDelegate {

  private static final Logger LOG = LoggerFactory.getLogger(RegisterResource.class);

  private static final Set<ImportOption> REGISTER_IMPORT_OPTIONS = EnumSet.of(
      ImportOption.INCLUDE_SOURCES,
      ImportOption.INCLUDE_RESULTS,
      ImportOption.USE_VALID_SECTORS,
      ImportOption.VALIDATE_AGAINST_SCHEMA,
      ImportOption.VALIDATE_SOURCES,
      ImportOption.WARNING_ON_CALCULATION_POINTS);

  private static final EnumSet<ImportType> REGISTER_IMPORT_TYPES = EnumSet.of(
      ImportType.GML,
      ImportType.ZIP,
      ImportType.PAA);

  private final ImportService importService;
  private final ScenarioSummaryService scenarioSummaryService;
  private final RegisterOwN2000PermitService registerOwN2000PermitService;
  private final ProxyFileService proxyFileService;
  private final JobRepositoryBean jobRepository;
  private final ConstantRepositoryBean constantRepository;

  @Autowired
  RegisterResource(final AuthenticationService authenticationService, final ResponseService responseService,
      final ScenarioService scenarioService, final ObjectValidatorService validatorService,
      final ImportService importService, final ScenarioSummaryService scenarioSummaryService,
      final CalculateService<OwN2000RegisterCalculationOptions> calculateService, final RegisterOwN2000PermitService registerOwN2000PermitService,
      final ProxyFileService proxyFileService, final JobRepositoryBean jobRepository, final ConstantRepositoryBean constantRepository) {
    super(authenticationService, responseService, scenarioService, validatorService, calculateService);
    this.importService = importService;
    this.scenarioSummaryService = scenarioSummaryService;
    this.registerOwN2000PermitService = registerOwN2000PermitService;
    this.proxyFileService = proxyFileService;
    this.jobRepository = jobRepository;
    this.constantRepository = constantRepository;
  }

  @Override
  public Optional<NativeWebRequest> getRequest() {
    return RegisterApiDelegate.super.getRequest();
  }

  @Override
  public ResponseEntity<String> requestSummary(final MultipartFile file, final Integer maximumResultsDistance, final Boolean decisionFramework) {
    try {
      final String originalFilename = file.getOriginalFilename();
      final ImportType importType = ImportType.determineByFilename(originalFilename);
      if (importType == null) {
        throw responseService.toResponseStatusException(new AeriusException(AeriusExceptionReason.IMPORT_FILE_UNSUPPORTED));
      }

      // Store the imported file under a unique random id so it can't be retrieved via the files api.
      final String importFilename = UUID.randomUUID().toString();
      final ConnectUser connectUser = authentication.getCurrentUser();
      final String jobKey = jobRepository.createJob(connectUser, JobType.INSERT_RESULTS, Optional.empty(), false);
      try {
        try (final InputStream fileInputStream = ImportService.toZipInputSteam(file.getInputStream(), importType, originalFilename)) {
          proxyFileService.write(FileServerFile.FREE_FORMAT, fileInputStream, FileServerExpireTag.SHORT, jobKey, importFilename);
        }
        // If the data was zipped create a new file name by appending .zip. This is needed because importer will need file extension to read the file.
        final String filename = originalFilename + (importType == ImportType.ZIP ? "" : FileFormat.ZIP.getDottedExtension());
        final AeriusCustomer customer = constantRepository.getEnum(ConstantsEnum.CUSTOMER, AeriusCustomer.class);
        final Theme theme = customer == AeriusCustomer.JNCC ? Theme.NCA : Theme.OWN2000;
        scenarioSummaryService.sendSummaryRequestToWorker(importFilename, jobKey,
            filename, new ImportProperties(null, null, REGISTER_IMPORT_TYPES, theme),
            FileServerExpireTag.SHORT, maximumResultsDistance, Boolean.TRUE.equals(decisionFramework));

        return responseService.toOkResponse(jobKey);
      } catch (final RuntimeException e) {
        jobRepository.setErrorMessage(jobKey, e.getCause() == null ? e.getMessage() : e.getCause().getMessage());
        throw e;
      }
    } catch (final AeriusException | IOException | RuntimeException e) {
      LOG.error("Exception importing file", e);
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity summarizeFile(final MultipartFile file, final String acceptLanguage) {
    try {
      final List<ImportParcel> parcels = importService.importFile(file, REGISTER_IMPORT_OPTIONS,
          new ImportProperties(null, null, REGISTER_IMPORT_TYPES));

      final Object summaryResponse = scenarioSummaryService.generateSummaryResponse(LocaleUtils.getSupportedLocaleOrDefault(acceptLanguage), parcels);
      return responseService.toOkResponse(summaryResponse);
    } catch (final AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<CalculateResponse> generateOwN2000Permit(final OwN2000RegisterCalculationOptions options, final List<UploadFile> files,
      final List<MultipartFile> fileParts, final RegisterCalculationResults calculationResults) {
    final RegisterPermitCalculateService service = new RegisterPermitCalculateService(registerOwN2000PermitService, calculationResults);
    return calculate(service, files, fileParts, Theme.OWN2000, Set.of(ImportOption.VALIDATE_AGAINST_SCHEMA), options);
  }

  @Override
  public ResponseEntity<CalculateResponse> registerOwN2000(final OwN2000RegisterCalculationOptions options, final List<UploadFile> files,
      final List<MultipartFile> fileParts, final RegisterCalculationResults calculationResults) {
    return calculate(files, fileParts, Theme.OWN2000, Set.of(ImportOption.VALIDATE_AGAINST_SCHEMA), options);
  }

  private static class RegisterPermitCalculateService implements CalculateService<OwN2000RegisterCalculationOptions> {
    private final RegisterOwN2000PermitService registerOwN2000PermitService;
    private final RegisterCalculationResults calculationResults;

    public RegisterPermitCalculateService(final RegisterOwN2000PermitService registerOwN2000PermitService,
        final RegisterCalculationResults calculationResults) {
      this.registerOwN2000PermitService = registerOwN2000PermitService;
      this.calculationResults = calculationResults;
    }

    @Override
    public String calculate(final ConnectUser connectUser, final Scenario scenario, final OwN2000RegisterCalculationOptions options)
        throws AeriusException {
      return registerOwN2000PermitService.saveCalculation(connectUser, scenario, options, calculationResults);
    }

    @Override
    public void validateInput(final ScenarioConstruction scenarioConstruction) {
      registerOwN2000PermitService.validateInput(scenarioConstruction);
    }
  }
}
