/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.util;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Utility class to check the situation composition of a scenario, based on its CalculationJobType.
 */
public final class CalculationJobTypeValidator {

  private CalculationJobTypeValidator() {
  }

  /**
   * Validate the scenario, based on the CalculationJobType present in the options.
   *
   * @param scenario a Scenario
   * @throws AeriusException when the scenario has no situations,
   *                         or the situation types do not match with the requirements posed by the CalculationJobType
   */
  public static void validate(final Scenario scenario) throws AeriusException {
    validate(scenario, scenario.getOptions().getCalculationJobType());
  }

  /**
   * Validate the scenario, based on the supplied CalculationJobType.
   *
   * @param scenario a Scenario
   * @param jobType The job type to validate the scenario for.
   * @throws AeriusException when the scenario has no situations,
   *                         or the situation types do not match with the requirements posed by the CalculationJobType
   */
  public static void validate(final Scenario scenario, final CalculationJobType jobType) throws AeriusException {
    final List<SituationType> missingSituationTypes = missingRequiredSituations(scenario, jobType);
    if (scenario.getSituations().isEmpty() || !missingSituationTypes.isEmpty()) {
      throw new AeriusException(AeriusExceptionReason.CALCULATION_JOB_TYPE_MISSING_REQUIRED_SITUATION,
          jobType.name(), join(missingSituationTypes));
    }

    final List<SituationType> illegalSituationTypes = illegalSituations(scenario, jobType);
    if (!illegalSituationTypes.isEmpty()) {
      throw new AeriusException(AeriusExceptionReason.CALCULATION_JOB_TYPE_INVALID_SITUATION_TYPE,
          jobType.name(), join(illegalSituationTypes));
    }
    final List<SituationType> tooManySituationsOfType = tooManySituationsOfType(scenario, jobType);
    if (!tooManySituationsOfType.isEmpty()) {
      throw new AeriusException(AeriusExceptionReason.CALCULATION_JOB_TYPE_TOO_MANY_SITUATIONS,
          jobType.name(), join(tooManySituationsOfType));
    }
  }

  private static List<SituationType> missingRequiredSituations(final Scenario scenario, final CalculationJobType calculationJobType) {
    return calculationJobType.getRequiredSituationTypes().stream()
        .filter(st -> scenario.getSituations().stream().noneMatch(sc -> sc.getType() == st))
        .toList();
  }

  private static List<SituationType> illegalSituations(final Scenario scenario, final CalculationJobType calculationJobType) {
    return scenario.getSituations().stream().map(ScenarioSituation::getType).filter(calculationJobType::isIllegal).collect(Collectors.toList());
  }

  private static List<SituationType> tooManySituationsOfType(final Scenario scenario, final CalculationJobType calculationJobType) {
    final Map<SituationType, List<ScenarioSituation>> situationsByType = scenario.getSituations().stream()
        .collect(Collectors.groupingBy(ScenarioSituation::getType));

    return situationsByType.keySet().stream()
        .filter(type -> situationsByType.get(type).size() > 1 && !calculationJobType.isPlural(type))
        .toList();
  }

  private static String join(final List<SituationType> situationTypes) {
    return situationTypes.stream().map(SituationType::name).collect(Collectors.joining(", "));
  }
}
