/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.processmonitor.ProcessMonitorClientBean;
import nl.overheid.aerius.shared.domain.calculation.JobProgress;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.domain.v2.archive.ArchiveMetaData;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

@Service
public class UiJobsService {

  private final JobRepositoryBean jobRepository;
  private final ProcessMonitorClientBean processMonitorClient;

  @Autowired
  public UiJobsService(final JobRepositoryBean jobRepository, final ProcessMonitorClientBean processMonitorClient) {
    this.jobRepository = jobRepository;
    this.processMonitorClient = processMonitorClient;
  }

  public int getJobId(final String jobKey) throws AeriusException {
    return jobRepository.getJobId(jobKey);
  }

  public Optional<JobProgress> getJob(final String jobKey) throws AeriusException {
    return jobRepository.getProgressForKey(jobKey);
  }

  public SituationCalculations getSituationCalculations(final String jobKey) throws AeriusException {
    return jobRepository.getSituationCalculations(jobKey);
  }

  public ArchiveMetaData getArchiveMetaData(final String jobKey) throws AeriusException {
    return jobRepository.getArchiveMetaData(jobKey);
  }

  public void cancelJob(final String jobKey) throws AeriusException {
    if (jobRepository.isJobWithoutUser(jobKey)) {
      jobRepository.cancelJob(jobKey);
      processMonitorClient.cancelJob(jobKey);
    } else {
      throw new AeriusException(AeriusExceptionReason.CONNECT_USER_JOBKEY_DOES_NOT_EXIST, jobKey);
    }
  }

  public void deleteJob(final String jobKey) throws AeriusException {
    if (jobRepository.isJobWithoutUser(jobKey)) {
      jobRepository.deleteJob(jobKey);
      processMonitorClient.cancelJob(jobKey);
    } else {
      throw new AeriusException(AeriusExceptionReason.CONNECT_USER_JOBKEY_DOES_NOT_EXIST, jobKey);
    }
  }

}
