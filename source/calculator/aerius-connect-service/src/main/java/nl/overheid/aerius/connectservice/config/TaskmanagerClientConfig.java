/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.config;

import java.io.IOException;
import java.util.concurrent.Executors;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.aerius.taskmanager.client.TaskManagerClientSender;
import nl.aerius.taskmanager.client.configuration.ConnectionConfiguration;
import nl.aerius.taskmanager.client.mq.RabbitMQWorkerMonitor;
import nl.overheid.aerius.connectservice.service.ServerUsageService;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.email.message.EmailMessageTaskSender;
import nl.overheid.aerius.processmonitor.ProcessMonitorClientBean;
import nl.overheid.aerius.server.service.export.ExportTaskClientBean;
import nl.overheid.aerius.taskmanager.client.WorkerType;

@Configuration
public class TaskmanagerClientConfig {

  @Bean
  public BrokerConnectionFactory brokerConnectionFactory(final RabbitMQBrokerProperties brokerProperties) {
    final ConnectionConfiguration.Builder builder = ConnectionConfiguration.builder();
    builder.brokerHost(brokerProperties.getHost());
    builder.brokerPort(brokerProperties.getPort());
    builder.brokerUsername(brokerProperties.getUsername());
    builder.brokerPassword(brokerProperties.getPassword());
    return new BrokerConnectionFactory(Executors.newCachedThreadPool(), builder.build());
  }

  @Bean
  public TaskManagerClientSender taskManagerClient(final BrokerConnectionFactory factory) {
    return new TaskManagerClientSender(factory);
  }

  @Bean
  public EmailMessageTaskSender emailMessageTaskSender(final TaskManagerClientSender client, final PMF pmf) {
    return new EmailMessageTaskSender(client, pmf);
  }

  @Bean
  public ExportTaskClientBean exportTaskClient(final TaskManagerClientSender client) {
    return new ExportTaskClientBean(client, WorkerType.CONNECT);
  }

  @Bean
  public RabbitMQWorkerMonitor workerMonitor(final BrokerConnectionFactory factory, final ServerUsageService serverUsageService)
      throws IOException {
    final RabbitMQWorkerMonitor monitor = new RabbitMQWorkerMonitor(factory);
    monitor.addObserver(serverUsageService);
    monitor.start();
    return monitor;
  }

  @Bean
  public ProcessMonitorClientBean processMonitorClientBean(final BrokerConnectionFactory factory) {
    return new ProcessMonitorClientBean(factory);
  }
}
