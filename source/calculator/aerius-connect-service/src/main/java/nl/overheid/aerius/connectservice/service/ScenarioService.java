/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import nl.overheid.aerius.connectservice.model.UploadFile;
import nl.overheid.aerius.connectservice.model.UploadFile.SituationEnum;
import nl.overheid.aerius.importer.ImportOption;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.importer.ImportProperties;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

@Service
public class ScenarioService {

  /**
   * Helper class to support constructing a scenario.
   */
  private static class ObjectsToAddTo {

    private final ScenarioSituation situation;
    private boolean yearOverwritten;
    private boolean nettingFactorOverwritten;

    ObjectsToAddTo(final SituationType situationType) {
      this.situation = new ScenarioSituation();
      this.situation.setType(situationType);
    }

    void setYear(final Integer overwriteYear) {
      if (!yearOverwritten && overwriteYear != null) {
        situation.setYear(overwriteYear.intValue());
        yearOverwritten = true;
      }
    }

    void setNettingFactor(final Double nettingFactor) {
      if (situation.getType() == SituationType.OFF_SITE_REDUCTION && !nettingFactorOverwritten && nettingFactor != null) {
        situation.setNettingFactor(nettingFactor);
        nettingFactorOverwritten = true;
      }
    }

    public ScenarioSituation getSituation() {
      return situation;
    }

  }

  private static class SituationKey {
    private final SituationType situationType;
    private final Integer groupId;

    SituationKey(final SituationType situationType, final Integer groupId) {
      this.situationType = situationType;
      this.groupId = groupId;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
      result = prime * result + ((situationType == null) ? 0 : situationType.hashCode());
      return result;
    }

    @Override
    public boolean equals(final Object obj) {
      boolean equal = false;
      if (this == obj) {
        equal = true;
      } else if (obj != null && getClass() == obj.getClass()) {
        final SituationKey other = (SituationKey) obj;
        equal = situationType == other.situationType &&
            ((groupId == null && other.groupId == null) ||
                (groupId != null && groupId.equals(other.groupId)));
      }
      return equal;
    }

  }

  private static final double DEFAULT_NETTING_FACTOR = 0.3;

  private static final Set<ImportOption> DEFAULT_OPTIONS = EnumSet.of(
      ImportOption.USE_IMPORTED_LANDUSES,
      ImportOption.INCLUDE_SOURCES,
      ImportOption.USE_VALID_SECTORS,
      ImportOption.VALIDATE_SOURCES);

  private final ImportService importService;
  private final SituationMergingService mergingService;

  public ScenarioService(final ImportService importService, final SituationMergingService mergingService) {
    this.importService = importService;
    this.mergingService = mergingService;
  }

  public ScenarioConstruction constructScenario(final Theme theme, final Map<UploadFile, MultipartFile> linkedFiles,
      final Set<ImportOption> additionalImportOptions) throws AeriusException {
    final Map<SituationEnum, List<UploadFile>> groupedFiles = groupFiles(linkedFiles.keySet());
    final Set<ImportOption> importOptions = determineImportOptions(additionalImportOptions);
    return construct(theme, groupedFiles, linkedFiles, importOptions);
  }

  private static Set<ImportOption> determineImportOptions(final Set<ImportOption> additionalImportOptions) {
    return Stream.of(DEFAULT_OPTIONS, additionalImportOptions).flatMap(Set::stream).collect(Collectors.toSet());
  }

  private static Map<SituationEnum, List<UploadFile>> groupFiles(final Collection<UploadFile> uploadFiles) {
    return uploadFiles.stream().collect(Collectors.groupingBy(UploadFile::getSituation));
  }

  private ScenarioConstruction construct(final Theme theme, final Map<SituationEnum, List<UploadFile>> groupedFiles,
      final Map<UploadFile, MultipartFile> filesMap, final Set<ImportOption> importOptions) throws AeriusException {
    final Scenario scenario = new Scenario(theme);
    final ScenarioConstruction construction = new ScenarioConstruction(scenario);
    final Map<SituationKey, ObjectsToAddTo> situationConstructions = new HashMap<>();
    //First add proposed, prefer to use metadata from proposed if available
    handleReferenceAndProposedZipFiles(groupedFiles.get(SituationEnum.REFERENCE_AND_PROPOSED_ZIP), filesMap, construction,
        situationConstructions, importOptions);
    handleFiles(groupedFiles.get(SituationEnum.PROPOSED), filesMap, construction, situationConstructions, SituationType.PROPOSED, importOptions);
    handleFiles(groupedFiles.get(SituationEnum.REFERENCE), filesMap, construction, situationConstructions, SituationType.REFERENCE, importOptions);
    handleFiles(groupedFiles.get(SituationEnum.TEMPORARY), filesMap, construction, situationConstructions, SituationType.TEMPORARY, importOptions);
    handleFiles(groupedFiles.get(SituationEnum.OFF_SITE_REDUCTION), filesMap, construction, situationConstructions, SituationType.OFF_SITE_REDUCTION,
        importOptions);
    handleFilesSelfDefined(groupedFiles.get(SituationEnum.DEFINED_BY_FILE), filesMap, construction, situationConstructions, importOptions);
    if (situationConstructions.isEmpty()) {
      // When nothing is added so far (for example, only ALL files), ensure there is at least one scenario to add to.
      situationConstructions.putIfAbsent(new SituationKey(SituationType.PROPOSED, null), new ObjectsToAddTo(SituationType.PROPOSED));
    }
    handleFilesForAll(groupedFiles.get(SituationEnum.ALL), filesMap, construction, situationConstructions.values(), importOptions);
    if (construction.isSuccesful()) {
      for (final ObjectsToAddTo situationConstruction : situationConstructions.values()) {
        final ScenarioSituation situation = situationConstruction.getSituation();
        ensureNettingFactor(situation);
        scenario.getSituations().add(situation);
      }
    }

    return construction;
  }

  private void handleFiles(final List<UploadFile> uploadFiles, final Map<UploadFile, MultipartFile> filesMap, final ScenarioConstruction construction,
      final Map<SituationKey, ObjectsToAddTo> situationConstructions, final SituationType situationType, final Set<ImportOption> importOptions)
      throws AeriusException {
    if (uploadFiles != null) {
      for (final UploadFile file : uploadFiles) {
        final SituationKey keyReference = new SituationKey(situationType, file.getGroupId());
        final ObjectsToAddTo objectsToAddTo = situationConstructions.computeIfAbsent(keyReference, ScenarioService::newObjectsToAddTo);
        final List<ImportParcel> importParcels = importFile(file, filesMap.get(file), importOptions);
        for (final ImportParcel importParcel : importParcels) {
          construction.addErrors(importParcel.getExceptions());
          construction.addWarnings(importParcel.getWarnings());
          if (construction.isSuccesful()) {
            objectsToAddTo.setYear(file.getCalculationYear());
            objectsToAddTo.setNettingFactor(file.getNettingFactor());
            addImportResult(importParcel, construction, objectsToAddTo);
          }
        }
      }
    }
  }

  private void handleFilesSelfDefined(final List<UploadFile> uploadFiles, final Map<UploadFile, MultipartFile> filesMap,
      final ScenarioConstruction construction, final Map<SituationKey, ObjectsToAddTo> situationConstructions, final Set<ImportOption> importOptions)
      throws AeriusException {
    if (uploadFiles != null) {
      for (final UploadFile file : uploadFiles) {
        final List<ImportParcel> importParcels = importFile(file, filesMap.get(file), importOptions);
        for (final ImportParcel importParcel : importParcels) {
          if (importParcel.getSituation().getType() == null) {
            construction.addError(new AeriusException(AeriusExceptionReason.CONNECT_NO_SITUATION_TYPE_DETECTED, file.getFileName()));
          }
          construction.addErrors(importParcel.getExceptions());
          construction.addWarnings(importParcel.getWarnings());
          if (construction.isSuccesful()) {
            final SituationKey keyReference = new SituationKey(importParcel.getSituation().getType(), file.getGroupId());
            final ObjectsToAddTo objectsToAddTo = situationConstructions.computeIfAbsent(keyReference, ScenarioService::newObjectsToAddTo);
            objectsToAddTo.setYear(file.getCalculationYear());
            objectsToAddTo.setNettingFactor(file.getNettingFactor());
            addImportResult(importParcel, construction, objectsToAddTo);
          }
        }
      }
    }
  }

  private void handleFilesForAll(final List<UploadFile> uploadFiles, final Map<UploadFile, MultipartFile> filesMap,
      final ScenarioConstruction construction, final Collection<ObjectsToAddTo> objectsToAddTo, final Set<ImportOption> importOptions)
      throws AeriusException {
    if (uploadFiles != null) {
      for (final UploadFile file : uploadFiles) {
        final List<ImportParcel> importParcels = importFile(file, filesMap.get(file), importOptions);
        for (final ImportParcel importParcel : importParcels) {
          construction.addErrors(importParcel.getExceptions());
          construction.addWarnings(importParcel.getWarnings());
          if (construction.isSuccesful()) {
            for (final ObjectsToAddTo objectToAddTo : objectsToAddTo) {
              objectToAddTo.setYear(file.getCalculationYear());
              addImportResult(importParcel, construction, objectToAddTo);
            }
          }
        }
      }
    }
  }

  private void addImportResult(final ImportParcel importParcel, final ScenarioConstruction construction, final ObjectsToAddTo objectsToAddTo) {
    addMetadataIfNeeded(construction, importParcel);
    mergingService.merge(objectsToAddTo.getSituation(), importParcel.getSituation());
    addCalculationPoints(construction, importParcel);
  }

  private void handleReferenceAndProposedZipFiles(final List<UploadFile> uploadFiles, final Map<UploadFile, MultipartFile> filesMap,
      final ScenarioConstruction construction, final Map<SituationKey, ObjectsToAddTo> situationConstructions, final Set<ImportOption> importOptions)
      throws AeriusException {
    if (uploadFiles != null) {
      for (final UploadFile file : uploadFiles) {
        final SituationKey keyReference = new SituationKey(SituationType.REFERENCE, file.getGroupId());
        final SituationKey keyProposed = new SituationKey(SituationType.PROPOSED, file.getGroupId());
        final ObjectsToAddTo reference = situationConstructions.computeIfAbsent(keyReference, ScenarioService::newObjectsToAddTo);
        final ObjectsToAddTo proposed = situationConstructions.computeIfAbsent(keyProposed, ScenarioService::newObjectsToAddTo);
        handleReferenceAndProposedZipFile(file, filesMap, construction, reference, proposed, importOptions);
      }
    }
  }

  private static ObjectsToAddTo newObjectsToAddTo(final SituationKey key) {
    return new ObjectsToAddTo(key.situationType);
  }

  private void handleReferenceAndProposedZipFile(final UploadFile file, final Map<UploadFile, MultipartFile> filesMap,
      final ScenarioConstruction construction, final ObjectsToAddTo reference, final ObjectsToAddTo proposed, final Set<ImportOption> importOptions)
      throws AeriusException {
    final List<ImportParcel> importParcels = importFile(file, filesMap.get(file), importOptions);
    importParcels.forEach(parcel -> construction.addErrors(parcel.getExceptions()));
    importParcels.forEach(parcel -> construction.addWarnings(parcel.getWarnings()));
    if (importParcels.size() < 2) {
      // Could perhaps use a more appropriate reason here
      construction.addError(new AeriusException(AeriusExceptionReason.ZIP_WITHOUT_USABLE_FILES, file.getFileName()));
    } else if (importParcels.size() > 2) {
      construction.addError(new AeriusException(AeriusExceptionReason.ZIP_TOO_MANY_USABLE_FILES_ERROR, file.getFileName()));
    }
    if (construction.isSuccesful()) {
      addMetadataIfNeeded(construction, importParcels.get(0));
      addMetadataIfNeeded(construction, importParcels.get(1));
      mergingService.merge(reference.getSituation(), importParcels.get(0).getSituation());
      mergingService.merge(proposed.getSituation(), importParcels.get(1).getSituation());
      addCalculationPoints(construction, importParcels.get(0));
      addCalculationPoints(construction, importParcels.get(1));
    }
  }

  private static void addMetadataIfNeeded(final ScenarioConstruction construction, final ImportParcel importResult) {
    if (!construction.isMetadataAdded() && importResult.getImportedMetaData() != null) {
      construction.getScenario().setMetaData(importResult.getImportedMetaData());
      construction.setMetadataAdded(true);
    }
  }

  private static void addCalculationPoints(final ScenarioConstruction construction, final ImportParcel importResult) {
    construction.getScenario().getCustomPointsList().addAll(importResult.getCalculationPointsList());
  }

  private List<ImportParcel> importFile(final UploadFile uploadFile, final MultipartFile filePart, final Set<ImportOption> importOptions)
      throws AeriusException {
    final Optional<Substance> substance = Optional.ofNullable(uploadFile.getSubstance())
        .map(uploadedSubstance -> Substance.safeValueOf(uploadedSubstance.name()));
    final ImportProperties properties = new ImportProperties(substance.orElse(null), null);
    return importService.importFile(filePart, importOptions, properties);
  }

  /**
   * In some combinations it's possible to end up with a off site reduction situation without a netting factor.
   * Example: original GML file not an off site reduction, options supplied force that situation type but without netting factor.
   *
   * This method fixes that by setting the default.
   *
   * Can't do this upfront, because then GML imports that have a netting factor won't work correctly.
   */
  private static void ensureNettingFactor(final ScenarioSituation situation) {
    if (situation.getType() == SituationType.OFF_SITE_REDUCTION && situation.getNettingFactor() == null) {
      situation.setNettingFactor(DEFAULT_NETTING_FACTOR);
    }
  }

}
