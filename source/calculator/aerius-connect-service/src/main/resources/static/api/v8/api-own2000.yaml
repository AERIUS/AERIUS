#
# Copyright the State of the Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/.
#

openapi: 3.0.1
info:
  title: AERIUS connect service
  description: |
    ## OWN2000
    This specification contains methods to perform calculations in the context of nitrogen deposition for Natura 2000-activities.

    ## Common specification
    This specification is part of a collection of specifications.
    The part [api-common.yaml](api-common.yaml) contains the common methods that are not tied to a specific application.
    That part contains, among other things, the methods and description of how to obtain an api-key required for the methods within this specification.
  termsOfService: https://link.aerius.nl/documenten/calculator/handboek_2024.pdf
  contact:
    name: AERIUS - Bij12 helpdesk
    url: https://link.aerius.nl/helpdesk
  license:
    name: Apache License, version 2.0
    url: https://www.apache.org/licenses/LICENSE-2.0
  version: 8.0.0
servers:
  - url: /api/v8
    description: Base URL relative to this page
tags:
  - name: calculation
    description: Calculation related methods
paths:
  /own2000/calculate:
    post:
      summary: Calculation in the context of nitrogen deposition for Natura 2000-activities
      description: |
        This function performs an AERIUS calculation and returns a jobKey with which the calculation can be followed.
        In addition, it is possible to receive the result via the email address linked to the API-key.
        Depending on the options, this can provide one or more GML files with results or a PDF file.

        The fields __files__ and __fileParts__ contain a list of 1 or more source files that together form the input of the calculation.

        If the input could not be validated, it is returned in the same way as with the validation function.
        If the result indicates that it was successful, the calculation task has been submitted to the AERIUS Calculator.
        In this case, a jobKey is also returned, which can be used to follow the progress of the calculation and obtain the result.
        Depending on the options, the user will receive an email when the calculation task is finished.
        The email contains a download link that can be used to download the result.
      operationId: calculateOwn2000
      tags:
        - calculation
      security:
        - ApiKeyAuth: []
      requestBody:
        required: true
        content:
          multipart/form-data:
            schema:
              $ref: '#/components/schemas/OwN2000CalculationRequest'
      responses:
        200:
          $ref: 'api-common.yaml#/components/responses/CalculationStarted'
        400:
          $ref: 'api-common.yaml#/components/responses/ClientError'
        440:
          $ref: 'api-common.yaml#/components/responses/ValidationError'
components:
  securitySchemes:
    ApiKeyAuth:
      type: apiKey
      in: header
      name: api-key
  schemas:
    OwN2000CalculationRequest:
      type: object
      required:
        - options
        - files
        - fileParts
      properties:
        options:
          $ref: '#/components/schemas/OwN2000CalculationOptions'
        files:
          $ref: 'api-common.yaml#/components/schemas/UploadFiles'
        fileParts:
          $ref: 'api-common.yaml#/components/schemas/FileParts'
    OwN2000CalculationOptions:
      allOf:
        - $ref: 'api-common.yaml#/components/schemas/AbstractCalculationOptions'
        - description: Calculation options for OwN2000.
        - type: object
          required:
            - outputType
          properties:
            outputType:
              type: string
              enum:
                - GML
                - PDF
              description: Indicates what type of result is requested.
            calculationPointsType:
              type: string
              default: OWN2000_RECEPTORS
              enum:
                - OWN2000_RECEPTORS
                - CUSTOM_POINTS
              description: |
                This option can be used to indicate which calculation points need to be calculated.
                The following options are supported:
                ___OWN2000_RECEPTORS:___ Calculate with the standard OwN2000 receptors, whereby the receptors to be calculated are determined by the system based on the sources.
                ___CUSTOM_POINTS:___  Calculate with own calculation points.

                In the case of OWN2000_RECEPTORS, the receptors to be calculated are determined by the system based on the sources.
                In the case of CUSTOM_POINTS, the calculation points from the supplied files are used, or the calculation points from a receptor set uploaded via '/receptorSets' are used.

                If this option is not supplied, OWN2000_RECEPTORS will be assumed.
            receptorSetName:
              type: string
              description: |
                Optional: the name of a receptor set saved by the user that should be used.

                If supplied, and if the `calculationPointsType` option is CUSTOM_POINTS, the calculation points supplied via this call are ignored.
                If not supplied, depending on the type of calculation, the calculation points occurring in the supplied files will be used.
            appendices:
              type: array
              description: |
                Indicates which additional appendices should also be generated.
                Depending on the outputType, this will result in additional appendices being added to the result file.
                If this option is not supplied or is an empty array, no additional appendices will be generated.
              items:
                $ref: 'api-common.yaml#/components/schemas/AppendixType'
