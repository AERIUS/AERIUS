/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.connectservice.model.JobStatus;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.processmonitor.ProcessMonitorClientBean;
import nl.overheid.aerius.shared.domain.calculation.JobProgress;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

@ExtendWith(MockitoExtension.class)
class JobsServiceTest {

  private static final String TEST_JOB_KEY = "Not_very_important";

  @Mock JobRepositoryBean jobRepository;
  @Mock ProcessMonitorClientBean processMonitorClient;

  @InjectMocks JobsService jobsService;

  @Test
  void testGetJobNotPresent() throws AeriusException {
    final ConnectUser user = mock(ConnectUser.class);

    when(jobRepository.getProgressForUserAndKey(user, TEST_JOB_KEY)).thenReturn(Optional.empty());

    final Optional<JobStatus> returned = jobsService.getJob(user, TEST_JOB_KEY);

    assertTrue(returned.isEmpty(), "No job status should be returned");
  }

  @Test
  void testGetJobPresent() throws AeriusException {
    final ConnectUser user = mock(ConnectUser.class);
    final JobProgress progress = mock(JobProgress.class);
    when(progress.getKey()).thenReturn(TEST_JOB_KEY);
    when(progress.getState()).thenReturn(JobState.COMPLETED);
    final LocalDateTime startDateTime = LocalDateTime.of(2020, 3, 25, 21, 55);
    final Date startDate = Date.from(startDateTime.toInstant(ZoneOffset.UTC));
    when(progress.getStartDateTime()).thenReturn(startDate);
    final int pointsCalculated = 230;
    when(progress.getNumberOfPointsCalculated()).thenReturn((double) pointsCalculated);
    final String testUrl = "someUrl";
    when(progress.getResultUrl()).thenReturn(testUrl);
    final String testErrorMessage = "someErrorMessage";
    when(progress.getErrorMessage()).thenReturn(testErrorMessage);

    when(jobRepository.getProgressForUserAndKey(user, TEST_JOB_KEY)).thenReturn(Optional.of(progress));

    final Optional<JobStatus> returned = jobsService.getJob(user, TEST_JOB_KEY);

    assertTrue(returned.isPresent(), "A job status should be returned");
    final JobStatus statusReturned = returned.get();
    assertEquals(TEST_JOB_KEY, statusReturned.getJobKey(), "Correct job should be returned");
    assertEquals(JobState.COMPLETED.name(), statusReturned.getStatus(), "Correct job status should be returned");
    assertEquals(OffsetDateTime.of(startDateTime, ZoneOffset.UTC), statusReturned.getStartDateTime(),
        "Correct job start date time should be returned");
    assertNull(statusReturned.getEndDateTime(), "Correct job end date time should be returned");
    assertEquals(pointsCalculated, statusReturned.getNumberOfPointsCalculated(), "Correct number of points calculated should be returned");
    assertEquals(testUrl, statusReturned.getResultUrl(), "Correct result URL should be returned");
    assertEquals(testErrorMessage, statusReturned.getErrorMessage(), "Correct error message should be returned");
  }

  @Test
  void testGetAllJobs() throws AeriusException {
    final ConnectUser user = mock(ConnectUser.class);
    final JobProgress job1 = mock(JobProgress.class);
    final String job1Key = UUID.randomUUID().toString();
    when(job1.getKey()).thenReturn(job1Key);
    when(job1.getState()).thenReturn(JobState.COMPLETED);
    final JobProgress job2 = mock(JobProgress.class);
    final String job2Key = UUID.randomUUID().toString();
    when(job2.getKey()).thenReturn(job2Key);
    when(job2.getState()).thenReturn(JobState.CANCELLED);

    when(jobRepository.getProgressForUser(user)).thenReturn(List.of(job1, job2));

    final List<JobStatus> returned = jobsService.getJobsForUser(user);

    assertEquals(2, returned.size(), "A job status should be returned");
    assertEquals(job1Key, returned.get(0).getJobKey(), "Correct job should be returned first");
    assertEquals(JobState.COMPLETED.name(), returned.get(0).getStatus(), "Correct job status should be returned first");
    assertEquals(job2Key, returned.get(1).getJobKey(), "Correct job should be returned second");
    assertEquals(JobState.CANCELLED.name(), returned.get(1).getStatus(), "Correct job status should be returned second");
  }

  @Test
  void testGetAllJobsWithJobStates() throws AeriusException {
    final ConnectUser user = mock(ConnectUser.class);
    final JobProgress job1 = mock(JobProgress.class);
    when(job1.getState()).thenReturn(JobState.CALCULATING);
    final JobProgress job2 = mock(JobProgress.class);
    when(job2.getState()).thenReturn(JobState.CANCELLED);

    when(jobRepository.getProgressForUserAndJobStates(user,
        Set.of(JobState.PREPARING, JobState.CALCULATING, JobState.POST_PROCESSING, JobState.CANCELLED))).thenReturn(List.of(job1, job2));

    final List<JobStatus> returned = jobsService.getJobsForUserAndJobStates(user, List.of("RUNNING", "CANCELLED"));
    assertEquals(2, returned.size(), "A job status should be returned");
  }

  @Test
  void testCancelJob() throws AeriusException {
    final ConnectUser user = mock(ConnectUser.class);
    when(jobRepository.isJobFromUser(user, TEST_JOB_KEY)).thenReturn(true);

    jobsService.cancelJob(user, TEST_JOB_KEY);

    verify(jobRepository).cancelJob(TEST_JOB_KEY);
    verifyNoMoreInteractions(jobRepository);
  }

  @Test
  void testCancelJobNotFound() throws AeriusException {
    final ConnectUser user = mock(ConnectUser.class);
    when(jobRepository.isJobFromUser(user, TEST_JOB_KEY)).thenReturn(false);

    final AeriusException e = assertThrows(AeriusException.class,
        () -> jobsService.cancelJob(user, TEST_JOB_KEY));

    assertEquals(AeriusExceptionReason.CONNECT_USER_JOBKEY_DOES_NOT_EXIST, e.getReason(), "Reason should be that user and jobkey do not match");

    verifyNoMoreInteractions(jobRepository);
  }

  @Test
  void testDeleteJob() throws AeriusException {
    final ConnectUser user = mock(ConnectUser.class);
    when(jobRepository.isJobFromUser(user, TEST_JOB_KEY)).thenReturn(true);

    jobsService.deleteJob(user, TEST_JOB_KEY);

    verify(jobRepository).deleteJob(TEST_JOB_KEY);
    verifyNoMoreInteractions(jobRepository);
  }

  @Test
  void testDeleteJobNotFound() throws AeriusException {
    final ConnectUser user = mock(ConnectUser.class);
    when(jobRepository.isJobFromUser(user, TEST_JOB_KEY)).thenReturn(false);

    final AeriusException e = assertThrows(AeriusException.class,
        () -> jobsService.deleteJob(user, TEST_JOB_KEY));

    assertEquals(AeriusExceptionReason.CONNECT_USER_JOBKEY_DOES_NOT_EXIST, e.getReason(), "Reason should be that user and jobkey do not match");

    verifyNoMoreInteractions(jobRepository);
  }

}
