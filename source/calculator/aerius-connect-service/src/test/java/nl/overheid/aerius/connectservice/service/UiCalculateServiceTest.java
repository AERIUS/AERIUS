/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import static nl.overheid.aerius.connectservice.util.TestScenarioUtil.TEST_YEAR;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atMostOnce;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.function.BiPredicate;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.verification.VerificationMode;

import nl.overheid.aerius.connectservice.domain.ExportOptions;
import nl.overheid.aerius.connectservice.domain.UiCalculationRequest;
import nl.overheid.aerius.connectservice.util.ScenarioUtil;
import nl.overheid.aerius.connectservice.util.TestScenarioUtil;
import nl.overheid.aerius.db.adms.MetSiteRepository;
import nl.overheid.aerius.db.calculator.CalculatorLimitsRepository;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.ConstantRepositoryBean;
import nl.overheid.aerius.db.connect.ConnectCalculationPointSetsRepositoryBean;
import nl.overheid.aerius.server.service.export.ExportTaskClientBean;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.export.ExportData.ExportAdditionalOptions;
import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioMetaData;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioResults;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituationResults;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.exception.ImaerExceptionReason;
import nl.overheid.aerius.shared.geometry.EmissionSourceLimits;
import nl.overheid.aerius.taskmanager.client.QueueEnum;

/**
 * Test class for {@link UiCalculateService}.
 */
@ExtendWith(MockitoExtension.class)
class UiCalculateServiceTest {

  private static final String TEST_JOB_KEY = "CalculateThis";
  private static final String NEW_JOB_KEY = "\"CalculateThat";
  private static final String TEST_EMAIL = "MyFakeEmail@example.org";

  private @Mock ExportTaskClientBean exportTaskClient;
  private @Mock JobRepositoryBean jobRepository;
  private @Mock ConnectCalculationPointSetsRepositoryBean pointSetsRepository;
  private @Mock ConstantRepositoryBean constantRepository;
  private @Mock ProxyFileService fileService;
  private @Mock EmissionsSourcesService sourcesService;
  private @Mock ScenarioUtil scenarioUtil;
  private @Mock CalculatorLimitsRepository calculatorLimitsRepository;
  private @Mock LocaleService localeService;
  private @Mock MetSiteRepository metSiteSurfaceCharacteristicsRepository;

  private @Captor ArgumentCaptor<ExportProperties> exportPropertiesCaptor;
  private @Captor ArgumentCaptor<QueueEnum> queueNameCaptor;
  private @Captor ArgumentCaptor<CalculationSetOptions> optionsCaptor;

  private @InjectMocks UiCalculateService calculateService;;

  @BeforeEach
  void beforeEach() throws AeriusException {
    TestScenarioUtil.mockConstantRepositoryYears(constantRepository);
    lenient().when(localeService.getLocale()).thenReturn(Locale.ENGLISH);
    final EmissionSourceLimits calculatorLimits = new EmissionSourceLimits();

    calculatorLimits.setMaxSources(5000);
    lenient().when(calculatorLimitsRepository.getCalculatorLimits()).thenReturn(calculatorLimits);
  }

  @ParameterizedTest
  @MethodSource("provideTestCalculateParameters")
  void testCalculate(final int size, final QueueEnum expectedQueue) throws AeriusException, IOException {
    final Scenario scenario = mockScenario(size);
    when(scenario.getOptions().getCalculationJobType()).thenReturn(CalculationJobType.PROCESS_CONTRIBUTION);
    when(scenario.getSituations().get(0).getType()).thenReturn(SituationType.PROPOSED);
    final UiCalculationRequest options = mockOptions(scenario);

    when(jobRepository.createJob(any())).thenReturn(TEST_JOB_KEY);

    final String jobKey = calculateService.calculate(scenario, options);

    assertEquals(TEST_JOB_KEY, jobKey, "Job key shouldn't be null");

    verify(jobRepository).createJob(JobType.CALCULATION);
    verify(exportTaskClient).startExport(queueNameCaptor.capture(), exportPropertiesCaptor.capture(), eq(TEST_JOB_KEY));

    verify(scenario.getSituations().get(0), never()).setYear(TEST_YEAR);

    final ExportProperties capturedProperties = exportPropertiesCaptor.getValue();
    assertEquals(ExportType.CALCULATION_UI, capturedProperties.getExportType(), "UI Calculation");
    assertNull(capturedProperties.getName(), "Should not have name");
    assertTrue(capturedProperties.getAdditionalOptions().contains(ExportAdditionalOptions.EMAIL_USER), "Should mail user");
    assertEquals(TEST_EMAIL, capturedProperties.getEmailAddress(), "Should contain user email address");

    assertEquals(expectedQueue, queueNameCaptor.getValue(), "Should send to " + expectedQueue + " calculation queue");
  }

  @Test
  void testExportWithExistingResults() throws AeriusException, IOException {
    final Scenario scenarioOrg = mockScenario(1);
    final Scenario scenarioNew = mockScenario(1);
    final UiCalculationRequest options = mockOptions(scenarioOrg);
    when(options.getJobKey()).thenReturn(TEST_JOB_KEY);
    final ScenarioMetaData metaData = new ScenarioMetaData();
    when(scenarioOrg.getMetaData()).thenReturn(metaData);
    when(jobRepository.createJob(any())).thenReturn(NEW_JOB_KEY);
    when(scenarioUtil.getScenario(options)).thenReturn(scenarioNew);
    final ScenarioResults scenarioResults = new ScenarioResults();
    scenarioResults.getResultsPerSituation().put("1", new ScenarioSituationResults());
    when(scenarioUtil.getScenarioResults(TEST_JOB_KEY)).thenReturn(scenarioResults);
    when(scenarioUtil.getScenarioResults(NEW_JOB_KEY)).thenReturn(scenarioResults);
    final String jobKey = calculateService.calculate(scenarioOrg, options);

    verify(jobRepository).createJob(JobType.CALCULATION);
    verify(scenarioUtil).getScenario(options);
    verify(scenarioUtil).getScenarioResults(TEST_JOB_KEY);
    verify(jobRepository).copyJobResults(TEST_JOB_KEY, NEW_JOB_KEY);
    verify(scenarioNew).setMetaData(metaData);
    assertEquals(NEW_JOB_KEY, jobKey, "Not the expected job key");
    verify(exportTaskClient).startExport(any(), exportPropertiesCaptor.capture(), eq(NEW_JOB_KEY));
    final ExportProperties capturedProperties = exportPropertiesCaptor.getValue();
    assertEquals(scenarioResults, capturedProperties.getScenarioResults(), "ScenarioResults should be set.");
  }

  @ParameterizedTest
  @CsvSource({
    "false,false", // Test that there was no file on the file server, results could be in the database.
    "false,true", // Test that there was no file on the file server, but no results in the database.
    "true,false", // Test that there was a file on the file server, but no results in the database
    "true,true", // Test that there was a file on the file server, and result in the database, but when coping results there were not results
  })
  void testExportWithExistingResultWithMissingScenario(final boolean hasScenario, final boolean hasResults) throws AeriusException, IOException {
    final Scenario scenario = mockScenario(1);
    final UiCalculationRequest options = mockOptions(scenario);
    when(options.getJobKey()).thenReturn(TEST_JOB_KEY);
    when(jobRepository.createJob(any())).thenReturn(NEW_JOB_KEY);
    when(scenarioUtil.getScenario(options)).thenReturn(hasScenario ? scenario : null);
    final ScenarioResults scenarioResults = new ScenarioResults();
    if (hasResults) {
      scenarioResults.getResultsPerSituation().put("1", new ScenarioSituationResults());
    }
    lenient().when(scenarioUtil.getScenarioResults(TEST_JOB_KEY)).thenReturn(scenarioResults);
    lenient().when(scenarioUtil.getScenarioResults(NEW_JOB_KEY)).thenReturn(new ScenarioResults());

    calculateService.calculate(scenario, options);

    verify(scenarioUtil).getScenario(options);
    verify(scenarioUtil, hasScenario ? atMostOnce() : never()).getScenarioResults(TEST_JOB_KEY);
    final VerificationMode modeForBothScenarioAndResults = hasScenario && hasResults ? atMostOnce() : never();
    verify(scenarioUtil, modeForBothScenarioAndResults).getScenarioResults(NEW_JOB_KEY);
    verify(jobRepository, modeForBothScenarioAndResults).copyJobResults(any(), any());
    verify(exportTaskClient).startExport(any(), exportPropertiesCaptor.capture(), eq(NEW_JOB_KEY));
    final ExportProperties capturedProperties = exportPropertiesCaptor.getValue();
    assertNull(capturedProperties.getScenarioResults(), "ScenarioResults should not be set.");
  }

  @Test
  void testLimitSourcesExceeded() throws AeriusException {
    final Scenario scenario = mockScenario(50_000);
    when(scenario.getOptions().getCalculationJobType()).thenReturn(CalculationJobType.PROCESS_CONTRIBUTION);
    when(scenario.getSituations().get(0).getType()).thenReturn(SituationType.PROPOSED);
    final UiCalculationRequest options = mockOptions(scenario);

    final AeriusException exception = assertThrows(AeriusException.class, () -> calculateService.calculate(scenario, options),
        "Should have thrown an AeriusException");

    assertEquals(ImaerExceptionReason.LIMIT_SOURCES_EXCEEDED, exception.getReason(), "Should have thrown LIMIT_SOURCES_EXCEEDED.");
    verify(jobRepository, never()).createJob(any());
    verify(jobRepository, never()).createJob(any(), any(), any(), eq(false));
  }

  @Test
  void testCalculateCustomPointsEmpty() throws AeriusException, IOException {
    assertCustomPoints(ExportType.GML_SOURCES_ONLY, CalculationMethod.FORMAL_ASSESSMENT, new ArrayList<>(), 0);
  }

  @Test
  void testCalculateCustomPointsNotEmpty() throws AeriusException, IOException {
    assertCustomPoints(ExportType.GML_SOURCES_ONLY, CalculationMethod.FORMAL_ASSESSMENT, List.of(mock(CalculationPointFeature.class)), 1);
  }

  @ParameterizedTest
  @ValueSource(strings = {"OPS", "ADMS"})
  void testCalculateOwN2000ExportModelEmptyCustomPoints(final String exportType) throws AeriusException, IOException {
    assertCustomPoints(ExportType.valueOf(exportType), CalculationMethod.FORMAL_ASSESSMENT, createSinglePointList(), 0);
  }

  @ParameterizedTest
  @ValueSource(strings = {"GML_SOURCES_ONLY", "PDF_PAA"})
  void testGMLExportExplicityUseExplicitCustomPoints(final String exportType) throws AeriusException, IOException {
    assertCustomPoints(SituationType.PROPOSED, ExportType.valueOf(exportType), CalculationMethod.CUSTOM_POINTS,
        CalculationJobType.PROCESS_CONTRIBUTION, createSinglePointList(), 1, CalculationMethod.FORMAL_ASSESSMENT);
  }

  @Test
  void testCalculateCustomPointsNotEmptyForCustomPointsCalculation() throws AeriusException, IOException {
    assertCustomPoints(ExportType.CALCULATION_UI, CalculationMethod.CUSTOM_POINTS, createSinglePointList(), 1);
  }

  private static List<CalculationPointFeature> createSinglePointList() {
    final CalculationPointFeature point = mock(CalculationPointFeature.class);
    final List<CalculationPointFeature> points = new ArrayList<>();

    points.add(point);
    return points;
  }

  @Test
  void testCalculateCustomPointsEmptyForDepositionSumCalculation() throws AeriusException, IOException {
    assertCustomPoints(SituationType.REFERENCE, ExportType.CALCULATION_UI, CalculationMethod.FORMAL_ASSESSMENT, CalculationJobType.DEPOSITION_SUM,
        createSinglePointList(), 0, CalculationMethod.FORMAL_ASSESSMENT);
  }

  private void assertCustomPoints(final ExportType exportType, final CalculationMethod calculationMethod, final List<CalculationPointFeature> points,
      final int expectedSize) throws AeriusException, IOException {
    assertCustomPoints(SituationType.PROPOSED, exportType, calculationMethod, CalculationJobType.PROCESS_CONTRIBUTION, points, expectedSize,
        calculationMethod);
  }

  private void assertCustomPoints(final SituationType situationType, final ExportType exportType, final CalculationMethod calculationMethod,
      final CalculationJobType calculationJobType, final List<CalculationPointFeature> points, final int expectedSize,
      final CalculationMethod expectedCalculationMethod) throws AeriusException, IOException {
    final Scenario scenario = mockScenario(1, situationType);
    final CalculationSetOptions setOptions = mock(CalculationSetOptions.class);
    when(scenario.getOptions()).thenReturn(setOptions);
    when(setOptions.getCalculationMethod()).thenReturn(calculationMethod);
    when(setOptions.getCalculationJobType()).thenReturn(calculationJobType);
    when(scenario.getCustomPointsList()).thenReturn(points);
    final UiCalculationRequest request = mockOptions(scenario);
    request.getExportOptions().setExportType(exportType);
    when(jobRepository.createJob(any())).thenReturn(TEST_JOB_KEY);

    final String jobKey = calculateService.calculate(scenario, request);

    assertEquals(TEST_JOB_KEY, jobKey, "Job key shouldn't be null");

    verify(jobRepository).createJob(JobType.CALCULATION);
    verify(exportTaskClient).startExport(queueNameCaptor.capture(), exportPropertiesCaptor.capture(), eq(TEST_JOB_KEY));
    verify(scenario).setOptions(optionsCaptor.capture());
    final CalculationSetOptions optionsWhenCalculating = optionsCaptor.getValue();
    assertNotEquals(setOptions, optionsWhenCalculating, "Should be a new options object set");
    assertEquals(expectedCalculationMethod, optionsWhenCalculating.getCalculationMethod(), "Type should be set be passed calculation method.");
    assertEquals(expectedSize, scenario.getCustomPointsList().size(), "List of points should have expected size");
  }

  @Test
  void testCalculateCustomPointsEmptyForCustomPointsCalculation() throws AeriusException, IOException {
    final Scenario scenario = mockScenario(1);
    final CalculationSetOptions setOptions = mock(CalculationSetOptions.class);
    when(scenario.getOptions()).thenReturn(setOptions);
    when(setOptions.getCalculationMethod()).thenReturn(CalculationMethod.CUSTOM_POINTS);
    when(setOptions.getCalculationJobType()).thenReturn(CalculationJobType.PROCESS_CONTRIBUTION);
    when(scenario.getCustomPointsList()).thenReturn(new ArrayList<>());
    final UiCalculationRequest request = mockOptions(scenario);

    final AeriusException exception = assertThrows(AeriusException.class, () -> calculateService.calculate(scenario, request),
        "Should throw an error when trying to calculate CUSTOM_POINTS without custom points");

    assertEquals(AeriusExceptionReason.CONNECT_NO_CUSTOM_POINTS, exception.getReason(), "Reason returned");

    verify(jobRepository, never()).createJob(JobType.CALCULATION);
    verifyNoInteractions(exportTaskClient);
  }

  @ParameterizedTest
  @MethodSource("provideCalculationJobTypeInvalidSituations")
  void testCalculationJobTypeInvalidSituations(final CalculationJobType calculationJobType, final SituationType type,
      final SituationType requiredSituationType) {
    final Scenario scenario = mockScenario(1);
    final List<ScenarioSituation> situations = new ArrayList<>(scenario.getSituations());
    final ScenarioSituation requiredSituation = mock(ScenarioSituation.class);

    when(requiredSituation.getYear()).thenReturn(TEST_YEAR);
    when(requiredSituation.getType()).thenReturn(requiredSituationType);
    situations.add(requiredSituation);
    when(scenario.getSituations()).thenReturn(situations);

    final CalculationSetOptions setOptions = mock(CalculationSetOptions.class);
    when(scenario.getOptions()).thenReturn(setOptions);
    when(setOptions.getCalculationJobType()).thenReturn(calculationJobType);
    final UiCalculationRequest request = mockOptions(scenario);
    when(scenario.getSituations().get(0).getType()).thenReturn(type);

    final AeriusException aeriusException = assertThrows(AeriusException.class, () -> calculateService.calculate(scenario, request),
        "Calculate should throw exception for illegal situation type " + type.name());
    assertEquals(AeriusExceptionReason.CALCULATION_JOB_TYPE_INVALID_SITUATION_TYPE, aeriusException.getReason(),
        "Reason should be invalid situation type");
  }

  private static Collection<Arguments> provideCalculationJobTypeInvalidSituations() {
    final Collection<Arguments> arguments = new ArrayList<>();
    for (final CalculationJobType calculationJobType : CalculationJobType.values()) {
      final Optional<SituationType> requiredSituationType = Arrays.stream(SituationType.values())
          .filter(calculationJobType::isRequired)
          .findFirst();

      if (requiredSituationType.isPresent()) {
        for (final SituationType situationType : Arrays.stream(SituationType.values())
            .filter(calculationJobType::isIllegal)
            .toList()) {
          arguments.add(Arguments.of(calculationJobType, situationType, requiredSituationType.get()));
        }
      }
    }
    return arguments;
  }

  @ParameterizedTest
  @MethodSource("provideCalculationJobTypeRequiredSituations")
  void testCalculationJobTypeValidSituations(final CalculationJobType calculationJobType, final SituationType type) {
    final Scenario scenario = mockScenario(1);
    final CalculationSetOptions setOptions = mock(CalculationSetOptions.class);
    when(scenario.getOptions()).thenReturn(setOptions);
    when(setOptions.getCalculationJobType()).thenReturn(calculationJobType);
    final UiCalculationRequest request = mockOptions(scenario);
    when(scenario.getSituations().get(0).getType()).thenReturn(type);

    assertDoesNotThrow(() -> calculateService.calculate(scenario, request),
        "Calculate should not throw exception for valid and required situation type " + type.name());
  }

  private static Collection<Arguments> provideCalculationJobTypeRequiredSituations() {
    return provideCalculationJobTypeSituations((situationType, calculationJobType) -> calculationJobType.isRequired(situationType));
  }

  @ParameterizedTest
  @MethodSource("provideCalculationJobTypeMissingRequiredSituations")
  void testCalculationJobTypeMissingRequiredSituations(final CalculationJobType calculationJobType, final SituationType type) {
    final Scenario scenario = mockScenario(1);
    final CalculationSetOptions setOptions = mock(CalculationSetOptions.class);
    when(scenario.getOptions()).thenReturn(setOptions);
    when(setOptions.getCalculationJobType()).thenReturn(calculationJobType);
    final UiCalculationRequest request = mockOptions(scenario);
    when(scenario.getSituations().get(0).getType()).thenReturn(type);

    final AeriusException aeriusException = assertThrows(AeriusException.class, () -> calculateService.calculate(scenario, request),
        "Calculate should throw exception for missing required situation type " + type.name());
    assertEquals(AeriusExceptionReason.CALCULATION_JOB_TYPE_MISSING_REQUIRED_SITUATION, aeriusException.getReason(),
        "Reason should be missing required situation type");
  }

  private static Collection<Arguments> provideCalculationJobTypeMissingRequiredSituations() {
    return provideCalculationJobTypeSituations((situationType, calculationJobType) -> !calculationJobType.getRequiredSituationTypes().isEmpty()
        && !calculationJobType.isRequired(situationType) && !calculationJobType.isIllegal(
            situationType));
  }

  @ParameterizedTest
  @MethodSource("provideCalculationJobTypeTooManySituationsOfType")
  void testCalculationJobTypeTooManySituationsOfType(final CalculationJobType calculationJobType, final SituationType type) {
    final Scenario scenario = mockScenario(1);
    final CalculationSetOptions setOptions = mock(CalculationSetOptions.class);
    when(scenario.getOptions()).thenReturn(setOptions);
    when(setOptions.getCalculationJobType()).thenReturn(calculationJobType);
    final UiCalculationRequest request = mockOptions(scenario);
    final ArrayList<ScenarioSituation> situations = new ArrayList<>(scenario.getSituations());
    when(situations.get(0).getType()).thenReturn(type);
    final ScenarioSituation secondSituation = mock(ScenarioSituation.class);
    when(secondSituation.getYear()).thenReturn(TEST_YEAR);
    when(secondSituation.getType()).thenReturn(type);
    situations.add(secondSituation);
    when(scenario.getSituations()).thenReturn(situations);

    final AeriusException aeriusException = assertThrows(AeriusException.class, () -> calculateService.calculate(scenario, request),
        "Calculate should throw exception for situation type " + type.name());
    assertEquals(AeriusExceptionReason.CALCULATION_JOB_TYPE_TOO_MANY_SITUATIONS, aeriusException.getReason(),
        "Reason should be too many situations of type");
  }

  private static Collection<Arguments> provideCalculationJobTypeTooManySituationsOfType() {
    return provideCalculationJobTypeSituations(
        (situationType, calculationJobType) -> !calculationJobType.isPlural(situationType) && calculationJobType.isRequired(situationType));
  }

  private static Collection<Arguments> provideCalculationJobTypeSituations(final BiPredicate<SituationType, CalculationJobType> filterFunction) {
    final Collection<Arguments> arguments = new ArrayList<>();
    for (final CalculationJobType calculationJobType : CalculationJobType.values()) {
      for (final SituationType situationType : Arrays.stream(SituationType.values())
          .filter(situationType -> filterFunction.test(situationType, calculationJobType))
          .toList()) {

        arguments.add(Arguments.of(calculationJobType, situationType));
      }
    }
    return arguments;
  }

  private static Stream<Arguments> provideTestCalculateParameters() {
    return Stream.of(Arguments.of(10, QueueEnum.CALCULATOR_UI_SMALL), Arguments.of(110, QueueEnum.CALCULATOR_UI_LARGE));
  }

  private static UiCalculationRequest mockOptions(final Scenario scenario) {
    final UiCalculationRequest options = mock(UiCalculationRequest.class);
    final ExportOptions eo = new ExportOptions();
    eo.setExportType(ExportType.CALCULATION_UI);
    eo.setEmail(TEST_EMAIL);
    when(options.getTheme()).thenReturn(Theme.OWN2000);
    lenient().when(options.getScenario()).thenReturn(scenario);
    when(options.getExportOptions()).thenReturn(eo);
    return options;
  }

  private static Scenario mockScenario(final int size) {
    return mockScenario(size, SituationType.PROPOSED);
  }

  private static Scenario mockScenario(final int size, final SituationType situationType) {
    final Scenario scenario = TestScenarioUtil.mockScenario();

    final List<EmissionSourceFeature> sourcesList = mock(ArrayList.class);
    lenient().when(sourcesList.size()).thenReturn(size);
    lenient().when(sourcesList.iterator()).thenReturn(new ArrayList<EmissionSourceFeature>().iterator());
    final List<ScenarioSituation> situations = scenario.getSituations();
    lenient().when(situations.get(0).getEmissionSourcesList()).thenReturn(sourcesList);
    lenient().when(situations.get(0).getType()).thenReturn(situationType);
    return scenario;
  }
}
