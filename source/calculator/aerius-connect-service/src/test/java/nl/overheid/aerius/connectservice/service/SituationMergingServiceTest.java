/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.v2.building.Building;
import nl.overheid.aerius.shared.domain.v2.building.BuildingFeature;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKCorrection;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKDispersionLineFeature;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKMeasure;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKMeasureFeature;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.InlandShippingEmissionSource;

class SituationMergingServiceTest {

  SituationMergingService mergingService;

  @BeforeEach
  void beforeEach() {
    mergingService = new SituationMergingService();
  }

  @Test
  void testMergeName() {
    final ScenarioSituation targetSituation = new ScenarioSituation();
    final ScenarioSituation situationToAdd = new ScenarioSituation();
    situationToAdd.setName("Some name");

    mergingService.merge(targetSituation, situationToAdd);

    assertEquals("Some name", targetSituation.getName(), "Name if there was no name yet");

    final ScenarioSituation anotherSituationToAdd = new ScenarioSituation();
    anotherSituationToAdd.setName("Some different name");

    mergingService.merge(targetSituation, anotherSituationToAdd);

    assertEquals("Some name", targetSituation.getName(), "Name should stay once set");
  }

  @Test
  void testMergeYear() {
    final ScenarioSituation targetSituation = new ScenarioSituation();
    final ScenarioSituation situationToAdd = new ScenarioSituation();
    situationToAdd.setYear(2020);

    mergingService.merge(targetSituation, situationToAdd);

    assertEquals(2020, targetSituation.getYear(), "Year if there was no year yet");

    final ScenarioSituation anotherSituationToAdd = new ScenarioSituation();
    anotherSituationToAdd.setYear(2030);

    mergingService.merge(targetSituation, anotherSituationToAdd);

    assertEquals(2020, targetSituation.getYear(), "Year should stay once set");
  }

  @Test
  void testMergeNettingFactor() {
    final ScenarioSituation targetSituation = new ScenarioSituation();
    final ScenarioSituation situationToAdd = new ScenarioSituation();
    situationToAdd.setNettingFactor(0.4);

    mergingService.merge(targetSituation, situationToAdd);

    assertEquals(0.4, targetSituation.getNettingFactor(), "Netting factor if there was no netting factor yet");

    final ScenarioSituation anotherSituationToAdd = new ScenarioSituation();
    anotherSituationToAdd.setNettingFactor(0.7);

    mergingService.merge(targetSituation, anotherSituationToAdd);

    assertEquals(0.4, targetSituation.getNettingFactor(), "Netting factor should stay once set");
  }

  @Test
  void testMergeSources() {
    final String duplicateId = "SomeID";
    final ScenarioSituation targetSituation = new ScenarioSituation();
    final ScenarioSituation situationToAdd = new ScenarioSituation();
    final EmissionSource source = new GenericEmissionSource();
    source.setGmlId(duplicateId);
    situationToAdd.getEmissionSourcesList().add(mockFeature(source));

    mergingService.merge(targetSituation, situationToAdd);

    assertEquals(1, targetSituation.getEmissionSourcesList().size(), "Sources when merged");

    final ScenarioSituation anotherSituationToAdd = new ScenarioSituation();
    anotherSituationToAdd.getEmissionSourcesList().add(mockFeature(mockSource("Some other ID")));
    final EmissionSource anotherSourceWithSameID = new GenericEmissionSource();
    anotherSourceWithSameID.setGmlId(duplicateId);
    anotherSituationToAdd.getEmissionSourcesList().add(mockFeature(anotherSourceWithSameID));

    mergingService.merge(targetSituation, anotherSituationToAdd);

    assertEquals(3, targetSituation.getEmissionSourcesList().size(), "Sources when merged with more");

    final ScenarioSituation andAnotherWithReferences = new ScenarioSituation();
    final EmissionSource andAnotherWithSameID = new GenericEmissionSource();
    andAnotherWithSameID.setGmlId(duplicateId);
    andAnotherWithReferences.getEmissionSourcesList().add(mockFeature(andAnotherWithSameID));
    final InlandShippingEmissionSource shippingSource = new InlandShippingEmissionSource();
    shippingSource.setGmlId("DoesNotMatter");
    shippingSource.setMooringAId(duplicateId);
    shippingSource.setMooringBId(duplicateId);
    andAnotherWithReferences.getEmissionSourcesList().add(mockFeature(shippingSource));

    mergingService.merge(targetSituation, andAnotherWithReferences);

    assertEquals(5, targetSituation.getEmissionSourcesList().size(), "Sources when merged with even more");
    assertEquals("SomeID", source.getGmlId(), "Initial source ID should stay the same");
    assertEquals("SomeID_1", anotherSourceWithSameID.getGmlId(), "second should have a version affix");
    assertEquals("SomeID_2", andAnotherWithSameID.getGmlId(), "third should have the version affix, but one version up");
    assertEquals("SomeID_2", shippingSource.getMooringAId(), "Reference in third situation should be updated");
    assertEquals("SomeID_2", shippingSource.getMooringBId(), "Reference in third situation should be updated");

    final ScenarioSituation andAnotherWithNullIds = new ScenarioSituation();
    andAnotherWithNullIds.getEmissionSourcesList().add(mockFeature(mockSource(null)));
    andAnotherWithNullIds.getEmissionSourcesList().add(mockFeature(mockSource(null)));
    mergingService.merge(targetSituation, andAnotherWithNullIds);

    assertEquals(7, targetSituation.getEmissionSourcesList().size(), "Sources when merged with even more");
  }

  @Test
  void testMergeBuildings() {
    final String duplicateId = "BuildingId_0_8_32";
    final ScenarioSituation targetSituation = new ScenarioSituation();
    final ScenarioSituation situationToAdd = new ScenarioSituation();
    final Building building = new Building();
    building.setGmlId(duplicateId);
    situationToAdd.getBuildingsList().add(mockFeature(building));

    mergingService.merge(targetSituation, situationToAdd);

    assertEquals(1, targetSituation.getBuildingsList().size(), "Buildings when merged");

    final ScenarioSituation anotherSituationToAdd = new ScenarioSituation();
    anotherSituationToAdd.getBuildingsList().add(mockFeature(mockBuilding("Some other ID")));
    final Building anotherBuildingWithSameID = new Building();
    anotherBuildingWithSameID.setGmlId(duplicateId);
    anotherSituationToAdd.getBuildingsList().add(mockFeature(anotherBuildingWithSameID));

    mergingService.merge(targetSituation, anotherSituationToAdd);

    assertEquals(3, targetSituation.getBuildingsList().size(), "Buildings when merged with more");

    final ScenarioSituation andAnotherWithReferences = new ScenarioSituation();
    final Building andAnotherWithSameID = new Building();
    andAnotherWithSameID.setGmlId(duplicateId);
    andAnotherWithReferences.getBuildingsList().add(mockFeature(andAnotherWithSameID));
    final GenericEmissionSource source = new GenericEmissionSource();
    source.setGmlId("DoesNotMatter");
    source.setCharacteristics(new OPSSourceCharacteristics());
    source.getCharacteristics().setBuildingId(duplicateId);
    andAnotherWithReferences.getEmissionSourcesList().add(mockFeature(source));

    mergingService.merge(targetSituation, andAnotherWithReferences);

    assertEquals(4, targetSituation.getBuildingsList().size(), "Buildings when merged with even more");
    assertEquals("BuildingId_0_8_32", building.getGmlId(), "Initial source ID should stay the same");
    assertEquals("BuildingId_0_8_33", anotherBuildingWithSameID.getGmlId(), "second should have a version affix");
    assertEquals("BuildingId_0_8_34", andAnotherWithSameID.getGmlId(), "third should have the version affix, but one version up");
    assertEquals("BuildingId_0_8_34", source.getCharacteristics().getBuildingId(), "Reference in third situation should be updated");
  }

  @Test
  void testMergeMeasures() {
    final String duplicateId = "some_original_1_id";
    final ScenarioSituation targetSituation = new ScenarioSituation();
    final ScenarioSituation situationToAdd = new ScenarioSituation();
    final CIMLKMeasure measure = new CIMLKMeasure();
    measure.setGmlId(duplicateId);
    situationToAdd.getCimlkMeasuresList().add(mockFeature(measure));

    mergingService.merge(targetSituation, situationToAdd);

    assertEquals(1, targetSituation.getCimlkMeasuresList().size(), "Measures when merged");

    final ScenarioSituation anotherSituationToAdd = new ScenarioSituation();
    anotherSituationToAdd.getCimlkMeasuresList().add(mockFeature(mockMeasure("Some other ID")));
    final CIMLKMeasure anotherMeasureWithSameID = new CIMLKMeasure();
    anotherMeasureWithSameID.setGmlId(duplicateId);
    anotherSituationToAdd.getCimlkMeasuresList().add(mockFeature(anotherMeasureWithSameID));

    mergingService.merge(targetSituation, anotherSituationToAdd);

    assertEquals(3, targetSituation.getCimlkMeasuresList().size(), "Measures when merged with more");

    final ScenarioSituation andAnotherWithReferences = new ScenarioSituation();
    final CIMLKMeasure andAnotherWithSameID = new CIMLKMeasure();
    andAnotherWithSameID.setGmlId(duplicateId);
    andAnotherWithReferences.getCimlkMeasuresList().add(mockFeature(andAnotherWithSameID));

    mergingService.merge(targetSituation, andAnotherWithReferences);

    assertEquals(4, targetSituation.getCimlkMeasuresList().size(), "Measures when merged with even more");
    assertEquals("some_original_1_id", measure.getGmlId(), "Initial measure ID should stay the same");
    assertEquals("some_original_1_id_1", anotherMeasureWithSameID.getGmlId(), "measure should have a version affix");
    assertEquals("some_original_1_id_2", andAnotherWithSameID.getGmlId(), "measure should have the version affix, but one version up");
  }

  @Test
  void testMergeDispersionLines() {
    final ScenarioSituation targetSituation = new ScenarioSituation();
    final ScenarioSituation situationToAdd = new ScenarioSituation();
    situationToAdd.getCimlkDispersionLinesList().add(mock(CIMLKDispersionLineFeature.class));

    mergingService.merge(targetSituation, situationToAdd);

    assertEquals(1, targetSituation.getCimlkDispersionLinesList().size(), "dispersion lines when merged");

    final ScenarioSituation anotherSituationToAdd = new ScenarioSituation();
    anotherSituationToAdd.getCimlkDispersionLinesList().add(mock(CIMLKDispersionLineFeature.class));
    anotherSituationToAdd.getCimlkDispersionLinesList().add(mock(CIMLKDispersionLineFeature.class));

    mergingService.merge(targetSituation, anotherSituationToAdd);

    assertEquals(3, targetSituation.getCimlkDispersionLinesList().size(), "dispersion lines when merged with more");
  }

  @Test
  void testMergeCorrections() {
    final ScenarioSituation targetSituation = new ScenarioSituation();
    final ScenarioSituation situationToAdd = new ScenarioSituation();
    situationToAdd.getCimlkCorrections().add(mock(CIMLKCorrection.class));

    mergingService.merge(targetSituation, situationToAdd);

    assertEquals(1, targetSituation.getCimlkCorrections().size(), "Corrections when merged");

    final ScenarioSituation anotherSituationToAdd = new ScenarioSituation();
    anotherSituationToAdd.getCimlkCorrections().add(mock(CIMLKCorrection.class));
    anotherSituationToAdd.getCimlkCorrections().add(mock(CIMLKCorrection.class));

    mergingService.merge(targetSituation, anotherSituationToAdd);

    assertEquals(3, targetSituation.getCimlkCorrections().size(), "Corrections when merged with more");
  }

  private EmissionSource mockSource(final String gmlId) {
    final EmissionSource mock = mock(EmissionSource.class);
    when(mock.getGmlId()).thenReturn(gmlId);
    return mock;
  }

  private EmissionSourceFeature mockFeature(final EmissionSource source) {
    final EmissionSourceFeature mock = mock(EmissionSourceFeature.class);
    when(mock.getProperties()).thenReturn(source);
    return mock;
  }

  private Building mockBuilding(final String gmlId) {
    final Building mock = mock(Building.class);
    when(mock.getGmlId()).thenReturn(gmlId);
    return mock;
  }

  private BuildingFeature mockFeature(final Building building) {
    final BuildingFeature mock = mock(BuildingFeature.class);
    when(mock.getProperties()).thenReturn(building);
    return mock;
  }

  private CIMLKMeasure mockMeasure(final String gmlId) {
    final CIMLKMeasure mock = mock(CIMLKMeasure.class);
    when(mock.getGmlId()).thenReturn(gmlId);
    return mock;
  }

  private CIMLKMeasureFeature mockFeature(final CIMLKMeasure building) {
    final CIMLKMeasureFeature mock = mock(CIMLKMeasureFeature.class);
    when(mock.getProperties()).thenReturn(building);
    return mock;
  }

}
