/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.connectservice.model.OpsOptions;
import nl.overheid.aerius.connectservice.model.OpsOptions.ChemistryEnum;
import nl.overheid.aerius.connectservice.model.OwN2000AnalysisOptions;
import nl.overheid.aerius.connectservice.model.OwN2000AnalysisOptions.CalculationPointsTypeEnum;
import nl.overheid.aerius.connectservice.model.OwN2000AnalysisOptions.OutputTypeEnum;
import nl.overheid.aerius.connectservice.model.OwN2000AnalysisOptions.RoadOPSEnum;
import nl.overheid.aerius.connectservice.model.OwN2000AnalysisOutputOptions;
import nl.overheid.aerius.connectservice.model.ResultType;
import nl.overheid.aerius.connectservice.util.TestScenarioUtil;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.ConstantRepositoryBean;
import nl.overheid.aerius.db.common.MeteoRepositoryBean;
import nl.overheid.aerius.db.connect.ConnectCalculationPointSetsRepositoryBean;
import nl.overheid.aerius.server.service.export.ExportTaskClientBean;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.CalculationRoadOPS;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.calculation.OPSOptions;
import nl.overheid.aerius.shared.domain.calculation.OPSOptions.Chemistry;
import nl.overheid.aerius.shared.domain.calculation.OwN2000CalculationOptions;
import nl.overheid.aerius.shared.domain.calculation.SubReceptorsMode;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.domain.export.ExportData.ExportAdditionalOptions;
import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.meteo.Meteo;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.v2.point.CustomCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.taskmanager.client.QueueEnum;

/**
 * Test class for  {@link OwN2000AnalysisService}.
 */
@ExtendWith(MockitoExtension.class)
class OwN2000AnalysisServiceTest {

  private static final int METEO_YEAR = 2020;
  private static final String METEO_YEAR_STRING = String.valueOf(METEO_YEAR);
  private static final String JOB_KEY = "job_key_123";

  private @Mock ExportTaskClientBean exportTaskClient;
  private @Mock JobRepositoryBean jobRepository;
  private @Mock ConnectCalculationPointSetsRepositoryBean pointSetsRepository;
  private @Mock ConstantRepositoryBean constantRepository;
  private @Mock ProxyFileService fileService;
  private @Mock MeteoRepositoryBean meteoRepository;
  private @Mock LocaleService localeService;
  private @Mock EmissionsSourcesService emissionsSourcesService;

  private @Mock ConnectUser connectUser;

  private @Captor ArgumentCaptor<QueueEnum> queueCaptor;
  private @Captor ArgumentCaptor<ExportProperties> exportPropertiesCaptor;
  private @Captor ArgumentCaptor<JobType> jobTypeCaptor;
  private @Captor ArgumentCaptor<Optional<String>> jobNameCaptor;
  private @Captor ArgumentCaptor<Boolean> protectedJobCaptor;

  private Scenario scenario;
  private OwN2000AnalysisService service;
  private OwN2000AnalysisOptions analysisOptions;
  private CalculationSetOptions actualOptions;

  @BeforeEach
  void beforeEach() throws AeriusException {
    TestScenarioUtil.mockConstantRepositoryYears(constantRepository);
    scenario = TestScenarioUtil.mockScenario();
    actualOptions = new CalculationSetOptions();
    analysisOptions = new OwN2000AnalysisOptions();
    analysisOptions.setOutputOptions(new OwN2000AnalysisOutputOptions());
    doReturn(actualOptions).when(scenario).getOptions();
    lenient().doReturn(Locale.ENGLISH).when(localeService).getLocale();
    lenient().doReturn(JOB_KEY).when(jobRepository).createJob(eq(connectUser), any(), any(), anyBoolean());
    service = new OwN2000AnalysisService(exportTaskClient, jobRepository, pointSetsRepository, constantRepository, fileService, meteoRepository,
        localeService, emissionsSourcesService);
  }

  //  Calculation Method tests

  @ParameterizedTest
  @CsvSource({"null", "CUSTOM_POINTS", "CUSTOM_RECEPTORS"})
  void testCalculationMehod(final String calculationMethod) throws AeriusException, IOException {

    final boolean nullValue = "null".equals(calculationMethod);
    final CalculationPointsTypeEnum cpte = nullValue ? null : CalculationPointsTypeEnum.valueOf(calculationMethod);
    analysisOptions.setCalculationPointsType(cpte);
    assertAnalyzeOwN2000();
    final CalculationMethod ct = nullValue ? CalculationMethod.CUSTOM_POINTS : CalculationMethod.valueOf(calculationMethod);

    assertEquals(ct, actualOptions.getCalculationMethod(), "Should be set to Custom points");
  }

  // Boolean Options tests

  @ParameterizedTest
  @MethodSource("booleanOptionsData")
  void testBooleanOptions(final String type, final Boolean value, final CalculationSetOptions reference, final boolean sendEmail)
      throws AeriusException, IOException {
    if (value != null) {
      analysisOptions.withOwN2000MaxDistance(value);
      analysisOptions.setStacking(value);
      analysisOptions.setAggregate(value);
      analysisOptions.setSendEmail(value);
    }
    assertAnalyzeOwN2000();
    final OwN2000CalculationOptions referenceOwN2000bOpts = reference.getOwN2000CalculationOptions();
    final OwN2000CalculationOptions actualOwN2000Opts = actualOptions.getOwN2000CalculationOptions();

    assertEquals(referenceOwN2000bOpts.isUseMaxDistance(), actualOwN2000Opts.isUseMaxDistance(), "Not expected OwN2000 Max Distance value");
    assertEquals(reference.isStacking(), actualOptions.isStacking(), "Not expected Stacking value");

    assertEquals(referenceOwN2000bOpts.isForceAggregation(), actualOwN2000Opts.isForceAggregation(), "Not expected Force Aggregation value");
    assertEquals(sendEmail, exportPropertiesCaptor.getValue().getAdditionalOptions().contains(ExportAdditionalOptions.EMAIL_USER),
        "Should set option that e-mail will be sent or not");

  }

  private static List<Arguments> booleanOptionsData() {
    return List.of(
        Arguments.of("DEFAULT", null, defaultOptions(), true),
        Arguments.of("TRUE", Boolean.TRUE, createOptions(true), true),
        Arguments.of("FASLE", Boolean.FALSE, createOptions(false), false));
  }

  private static CalculationSetOptions defaultOptions() {
    final CalculationSetOptions opts = new CalculationSetOptions();
    final OwN2000CalculationOptions own2000Opts = opts.getOwN2000CalculationOptions();
    own2000Opts.setUseMaxDistance(true);
    opts.setStacking(true);
    own2000Opts.setForceAggregation(false);
    own2000Opts.setSubReceptorsMode(SubReceptorsMode.DISABLED);

    return opts;
  }

  private static CalculationSetOptions createOptions(final boolean value) {
    final CalculationSetOptions opts = new CalculationSetOptions();
    final OwN2000CalculationOptions own2000Opts = opts.getOwN2000CalculationOptions();
    own2000Opts.setUseMaxDistance(value);
    opts.setStacking(value);
    own2000Opts.setForceAggregation(value);
    return opts;
  }

  // Use Receptor Height tests

  @ParameterizedTest
  @CsvSource({"null", "FALSE"})
  void testReceptorHeight(final String value) throws AeriusException, IOException {
    // Test default value for receptor Heights
    assertAnalyzeOwN2000();
    assertEquals(Boolean.valueOf(value), actualOptions.getOwN2000CalculationOptions().isUseReceptorHeights(), "Should not Use Receptor Heights");
  }

  @Test
  void testUseReceptorHeightNoHeight() {
    // Test use receptor Heights
    analysisOptions.setUseReceptorHeight(true);
    analysisOptions.getOutputOptions().setResultTypes(List.of(ResultType.CONCENTRATION));
    final AeriusException ae = assertThrows(AeriusException.class, this::assertAnalyzeOwN2000,
        "Should throw exception if not Concentration output");
    assertEquals(AeriusExceptionReason.CONNECT_MISSING_RECEPTOR_HEIGHT, ae.getReason(), "Should get Reason CONNECT_MISSING_RECEPTOR_HEIGHT");
  }

  @Test
  void testUseReceptorHeightNoConcentration() {
    // Test use receptor Heights
    analysisOptions.setUseReceptorHeight(true);
    final CustomCalculationPoint point = new CustomCalculationPoint();
    point.setHeight(10.0);
    scenario.getCustomPointsList().get(0).setProperties(point);
    final AeriusException ae = assertThrows(AeriusException.class, this::assertAnalyzeOwN2000,
        "Should throw exception if not Concentration output");
    assertEquals(AeriusExceptionReason.CONNECT_INVALID_OUTPUTTYPE, ae.getReason(), "Should get Reason CONNECT_INVALID_OUTPUTTYPE");
  }

  @Test
  void testUseReceptorHeightValid() {
    // Test use receptor Heights
    analysisOptions.setUseReceptorHeight(true);
    analysisOptions.getOutputOptions().setResultTypes(List.of(ResultType.CONCENTRATION));
    final CustomCalculationPoint point = new CustomCalculationPoint();
    point.setHeight(10.0);
    scenario.getCustomPointsList().get(0).setProperties(point);
    assertDoesNotThrow(this::assertAnalyzeOwN2000, "Should not throw exception Concentration output");
    assertTrue(actualOptions.getOwN2000CalculationOptions().isUseReceptorHeights(), "Should set Use Receptor Height option");
  }

  // Test MeteoYear option

  @Test
  void testNoMeteoYear() {
    assertDoesNotThrow(this::assertAnalyzeOwN2000, "Should not throw exception when no meteo year available");
    assertNull(actualOptions.getOwN2000CalculationOptions().getMeteo(), "Expected no meteo object");
  }

  @Test
  void testValidMeteoYear() throws AeriusException, IOException {
    final Meteo meteo = new Meteo(METEO_YEAR);
    doReturn(Map.of(METEO_YEAR_STRING, meteo)).when(meteoRepository).getMeteoDefinitions();

    analysisOptions.setMeteoYear(METEO_YEAR_STRING);
    assertAnalyzeOwN2000();
    assertEquals(meteo, actualOptions.getOwN2000CalculationOptions().getMeteo(), "Expected meteo object to be returned");
  }

  @Test
  void testUnsupportedMeteoYear() throws AeriusException {
    doReturn(Map.of()).when(meteoRepository).getMeteoDefinitions();

    analysisOptions.setMeteoYear("invalid");
    final AeriusException ae = assertThrows(AeriusException.class, this::assertAnalyzeOwN2000, "Should throw exception on unknown meteo year");
    assertEquals(AeriusExceptionReason.CONNECT_INVALID_METEO, ae.getReason(), "Should get Reason CONNECT_INVALID_METEO");
  }

  // Test Substances

  @Test
  void testDefaultSubstances() throws AeriusException, IOException {
    assertAnalyzeOwN2000();
    assertTrue(Stream.of(Substance.NH3, Substance.NOX, Substance.NO2).allMatch(actualOptions.getSubstances()::contains),
        "Should contain all expected substances");
  }

  @Test
  void testCustomSubstances() throws AeriusException, IOException {
    analysisOptions.setSubstances(List.of(nl.overheid.aerius.connectservice.model.Substance.NH3));
    assertAnalyzeOwN2000();
    assertEquals(List.of(Substance.NH3), actualOptions.getSubstances(), "Should contain only the set substance");
  }

  // Test Emission Result Types

  @Test
  void testDefaultResultTypes() throws AeriusException, IOException {
    assertAnalyzeOwN2000();
    assertEquals(Set.of(EmissionResultKey.NH3_DEPOSITION, EmissionResultKey.NOX_DEPOSITION), Set.of(actualOptions.getEmissionResultKeys().toArray()),
        "Should contain the default emission result types");
  }

  @Test
  void testCustomResultTypes() throws AeriusException, IOException {
    analysisOptions.setSubstances(List.of(nl.overheid.aerius.connectservice.model.Substance.PM10));
    analysisOptions.getOutputOptions().setResultTypes(List.of(ResultType.CONCENTRATION));
    assertAnalyzeOwN2000();
    assertEquals(Set.of(EmissionResultKey.PM10_CONCENTRATION), Set.of(actualOptions.getEmissionResultKeys().toArray()),
        "Should only contain custom set emission result type");
  }

  // Test Export types, including if results per sector are expected.

  @ParameterizedTest
  @MethodSource("exportTypeData")
  void testExportTypes(final OutputTypeEnum ot, final Boolean sectorResults, final ExportType expectedExportType)
      throws AeriusException, IOException {
    analysisOptions.setOutputType(ot);
    if (sectorResults != null) {
      analysisOptions.getOutputOptions().setSectorOutput(sectorResults);
    }
    assertAnalyzeOwN2000();
    assertEquals(expectedExportType, exportPropertiesCaptor.getValue().getExportType(), "Should have expected export type");
  }

  private static List<Arguments> exportTypeData() {
    return List.of(
        Arguments.of(OutputTypeEnum.CSV, null, ExportType.CSV),
        Arguments.of(OutputTypeEnum.GML, null, ExportType.GML_WITH_RESULTS),
        Arguments.of(OutputTypeEnum.GML, Boolean.FALSE, ExportType.GML_WITH_RESULTS),
        Arguments.of(OutputTypeEnum.GML, Boolean.TRUE, ExportType.GML_WITH_SECTORS_RESULTS));
  }

  // Test custom OPS options

  @Test
  void testOpsOptions() throws AeriusException, IOException {
    final OpsOptions opsOptions = new OpsOptions();
    opsOptions.setRawInput(Boolean.TRUE);
    opsOptions.setYear(METEO_YEAR);
    opsOptions.setCompCode(1);
    opsOptions.setMolWeight(2.0);
    opsOptions.setPhase(3);
    opsOptions.setLoss(4);
    opsOptions.setDiffCoeff("5");
    opsOptions.setWashout("6");
    opsOptions.setConvRate("7");
    opsOptions.setRoads("8");
    opsOptions.setRoughness(9.0);
    opsOptions.setChemistry(ChemistryEnum.PROGNOSIS);

    analysisOptions.setOpsOptions(opsOptions);
    assertAnalyzeOwN2000();
    final OPSOptions actualOpsOptions = actualOptions.getOwN2000CalculationOptions().getOpsOptions();

    assertTrue(actualOpsOptions.isRawInput(), "Expects raw input");
    assertEquals(METEO_YEAR, actualOpsOptions.getYear(), "Expect OPS year set");
    assertEquals(1, actualOpsOptions.getCompCode(), "Expect OPS Comp Code set");
    assertEquals(2.0, actualOpsOptions.getMolWeight(), "Expect OPS Mol Weight set");
    assertEquals(3, actualOpsOptions.getPhase(), "Expect OPS Phase set");
    assertEquals(4, actualOpsOptions.getLoss(), "Expect OPS Loss set");
    assertEquals("5", actualOpsOptions.getDiffCoeff(), "Expect OPS Diff Coeff set");
    assertEquals("6", actualOpsOptions.getWashout(), "Expect OPS Washout set");
    assertEquals("7", actualOpsOptions.getConvRate(), "Expect OPS Conv Rage test");
    assertEquals("8", actualOpsOptions.getRoads(), "Expect OPS Roads test");
    assertEquals(9.0, actualOpsOptions.getRoughness(), "Expect OPS Roughness set");
    assertEquals(Chemistry.PROGNOSIS, actualOpsOptions.getChemistry(), "Expect OPS Chemistry set");
  }

  // Test OPS road options

  @ParameterizedTest
  @CsvSource({"DEFAULT", "OPS_ROAD", "OPS_ALL"})
  void testOpsRoad(final String opsRoad) throws AeriusException, IOException {
    analysisOptions.setRoadOPS(RoadOPSEnum.valueOf(opsRoad));
    assertAnalyzeOwN2000();
    assertEquals(CalculationRoadOPS.valueOf(opsRoad), actualOptions.getOwN2000CalculationOptions().getRoadOPS(),
        "Should have expected OPS road options");
  }

  private void assertAnalyzeOwN2000() throws AeriusException, IOException {
    analysisOptions.setCalculationYear(TestScenarioUtil.TEST_YEAR);
    assertNotNull(service.analyseOwN2000(connectUser, scenario, analysisOptions), "Should return an jobKey");
    verify(exportTaskClient).startExport(queueCaptor.capture(), exportPropertiesCaptor.capture(), anyString());
    verify(jobRepository).createJob(eq(connectUser), jobTypeCaptor.capture(), jobNameCaptor.capture(), protectedJobCaptor.capture());
  }
}
