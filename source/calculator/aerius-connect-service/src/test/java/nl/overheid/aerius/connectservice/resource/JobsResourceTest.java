/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.resource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import nl.overheid.aerius.connectservice.auth.AuthenticationService;
import nl.overheid.aerius.connectservice.model.JobStatus;
import nl.overheid.aerius.connectservice.service.JobsService;
import nl.overheid.aerius.connectservice.service.ResponseService;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.exception.AeriusException;

@ExtendWith(MockitoExtension.class)
class JobsResourceTest {

  private static final String TEST_JOB_KEY = "abcdfg";
  private static final List<String> TEST_JOB_STATES = List.of("CALCULATING", "QUEUING");

  @Mock AuthenticationService authenticationService;
  @Mock ResponseService responseService;
  @Mock JobsService jobsService;

  @Mock ConnectUser connectUser;
  @Mock ResponseEntity<JobStatus> mockStatusResponse;
  @Mock ResponseEntity<List<JobStatus>> mockStatusesResponse;
  @Mock ResponseEntity<Void> mockVoidResponse;

  @Captor ArgumentCaptor<Optional<JobStatus>> jobStatusCaptor;
  @Captor ArgumentCaptor<List<JobStatus>> jobStatusesCaptor;
  @Captor ArgumentCaptor<List<String>> jobStatesCaptor;

  JobsResource jobsResource;

  @BeforeEach
  void beforeEach() throws AeriusException {
    when(authenticationService.getCurrentUser()).thenReturn(connectUser);
    jobsResource = new JobsResource(authenticationService, responseService, jobsService);
  }

  @Test
  void testGetJobStatus() throws AeriusException {
    final Optional<JobStatus> mockJobStatus = Optional.of(mock(JobStatus.class));
    when(jobsService.getJob(connectUser, TEST_JOB_KEY)).thenReturn(mockJobStatus);

    when(responseService.toOkResponseOptional(jobStatusCaptor.capture(), any())).thenReturn(mockStatusResponse);

    final ResponseEntity<JobStatus> response = jobsResource.getJobStatus(TEST_JOB_KEY);
    assertEquals(mockStatusResponse, response, "Response should come from response service");
    assertEquals(mockJobStatus, jobStatusCaptor.getValue(), "Content of response should match the job status retrieved");
  }

  @Test
  void testGetJobStatusException() throws AeriusException {
    final AeriusException aeriusException = mock(AeriusException.class);
    when(jobsService.getJob(connectUser, TEST_JOB_KEY)).thenThrow(aeriusException);

    final ResponseStatusException mockStatusException = mock(ResponseStatusException.class);
    when(responseService.toResponseStatusException(aeriusException)).thenReturn(mockStatusException);

    final ResponseStatusException thrownException = assertThrows(ResponseStatusException.class, () -> jobsResource.getJobStatus(TEST_JOB_KEY));
    assertEquals(mockStatusException, thrownException, "Thrown exception should come from response service");
  }

  @Test
  void testListJobs() throws AeriusException {
    final List<JobStatus> statuses = List.of(mock(JobStatus.class), mock(JobStatus.class));
    when(jobsService.getJobsForUserAndJobStates(eq(connectUser), jobStatesCaptor.capture())).thenReturn(statuses);

    when(responseService.toOkResponse(jobStatusesCaptor.capture())).thenReturn(mockStatusesResponse);

    final ResponseEntity<List<JobStatus>> response = jobsResource.listJobs(TEST_JOB_STATES);
    assertEquals(mockStatusesResponse, response, "Response should come from response service");
    assertEquals(TEST_JOB_STATES, jobStatesCaptor.getValue(), "Should be called with the correct job states");
    assertEquals(statuses, jobStatusesCaptor.getValue(), "Content of response should match the list of jobs retrieved");
  }

  @Test
  void testListJobsException() throws AeriusException {
    final AeriusException aeriusException = mock(AeriusException.class);
    when(jobsService.getJobsForUserAndJobStates(eq(connectUser), jobStatesCaptor.capture())).thenThrow(aeriusException);

    final ResponseStatusException mockStatusException = mock(ResponseStatusException.class);
    when(responseService.toResponseStatusException(aeriusException)).thenReturn(mockStatusException);

    final ResponseStatusException thrownException = assertThrows(ResponseStatusException.class, () -> jobsResource.listJobs(TEST_JOB_STATES));
    assertEquals(mockStatusException, thrownException, "Thrown exception should come from response service");
  }

  @Test
  void testCancelJob() throws AeriusException {
    when(responseService.toOkResponse()).thenReturn(mockVoidResponse);

    final ResponseEntity<Void> response = jobsResource.cancelJob(TEST_JOB_KEY);
    assertEquals(mockVoidResponse, response, "Response should come from response service");

    verify(jobsService).cancelJob(connectUser, TEST_JOB_KEY);
  }

  @Test
  void testCancelJobException() throws AeriusException {
    final AeriusException aeriusException = mock(AeriusException.class);
    doThrow(aeriusException).when(jobsService).cancelJob(connectUser, TEST_JOB_KEY);

    final ResponseStatusException mockStatusException = mock(ResponseStatusException.class);
    when(responseService.toResponseStatusException(aeriusException)).thenReturn(mockStatusException);

    final ResponseStatusException thrownException = assertThrows(ResponseStatusException.class, () -> jobsResource.cancelJob(TEST_JOB_KEY));
    assertEquals(mockStatusException, thrownException, "Thrown exception should come from response service");
  }

  @Test
  void testDeleteJob() throws AeriusException {
    when(responseService.toOkResponse()).thenReturn(mockVoidResponse);

    final ResponseEntity<Void> response = jobsResource.deleteJob(TEST_JOB_KEY);
    assertEquals(mockVoidResponse, response, "Response should come from response service");

    verify(jobsService).deleteJob(connectUser, TEST_JOB_KEY);
  }

  @Test
  void testDeleteJobException() throws AeriusException {
    final AeriusException aeriusException = mock(AeriusException.class);
    doThrow(aeriusException).when(jobsService).deleteJob(connectUser, TEST_JOB_KEY);

    final ResponseStatusException mockStatusException = mock(ResponseStatusException.class);
    when(responseService.toResponseStatusException(aeriusException)).thenReturn(mockStatusException);

    final ResponseStatusException thrownException = assertThrows(ResponseStatusException.class, () -> jobsResource.deleteJob(TEST_JOB_KEY));
    assertEquals(mockStatusException, thrownException, "Thrown exception should come from response service");
  }

}
