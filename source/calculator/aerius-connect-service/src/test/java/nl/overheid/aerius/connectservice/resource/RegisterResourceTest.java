/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.resource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import nl.overheid.aerius.connectservice.auth.AuthenticationService;
import nl.overheid.aerius.connectservice.model.CalculateResponse;
import nl.overheid.aerius.connectservice.model.OwN2000RegisterCalculationOptions;
import nl.overheid.aerius.connectservice.model.SummaryResponse;
import nl.overheid.aerius.connectservice.model.UploadFile;
import nl.overheid.aerius.connectservice.model.ValidationMessage;
import nl.overheid.aerius.connectservice.service.CalculateService;
import nl.overheid.aerius.connectservice.service.ImportService;
import nl.overheid.aerius.connectservice.service.ObjectValidatorService;
import nl.overheid.aerius.connectservice.service.ProxyFileService;
import nl.overheid.aerius.connectservice.service.RegisterOwN2000PermitService;
import nl.overheid.aerius.connectservice.service.ResponseService;
import nl.overheid.aerius.connectservice.service.ScenarioConstruction;
import nl.overheid.aerius.connectservice.service.ScenarioService;
import nl.overheid.aerius.connectservice.service.ScenarioSummaryService;
import nl.overheid.aerius.importer.ImportOption;
import nl.overheid.aerius.importer.summary.ImportSummaryGenerator;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.exception.AeriusException;

@ExtendWith(MockitoExtension.class)
class RegisterResourceTest {

  private static final String CORRECT_FILE_NAME = "original_filename.notreally";

  @Mock AuthenticationService authenticationService;
  @Mock ResponseService responseService;
  @Mock ScenarioService scenarioService;
  @Mock ObjectValidatorService validatorService;
  @Mock ImportService importService;
  @Mock ScenarioSummaryService scenarioSummaryService;
  @Mock RegisterOwN2000PermitService registerOwN2000PermitService;
  @Mock CalculateService<OwN2000RegisterCalculationOptions> calculateService;
  @Mock ProxyFileService proxyFileService;
  @Mock ObjectMapper objectMapper;
  @Mock ImportSummaryGenerator importSummaryGenerator;

  @Mock ConnectUser connectUser;
  @Mock ResponseEntity<SummaryResponse> summaryResponse;
  @Mock ResponseEntity<CalculateResponse> mockResponse;

  @InjectMocks RegisterResource registerResource;

  @Captor ArgumentCaptor<SummaryResponse> summaryCaptor;
  @Captor ArgumentCaptor<Set<ImportOption>> importOptionsCaptor;
  @Captor ArgumentCaptor<CalculateResponse> calculateResponseCaptor;

  @SuppressWarnings("unchecked")
  @BeforeEach
  void beforeEach() throws AeriusException {
    lenient().when(responseService.convertToValidation(any(List.class))).thenAnswer((invocation) -> {
      final List<AeriusException> exceptions = (List<AeriusException>) invocation.getArgument(0);
      return exceptions.stream().map(e -> mock(ValidationMessage.class)).collect(Collectors.toList());
    });
  }

  @Test
  void testGenerateOwN2000Permit() throws AeriusException {
    final OwN2000RegisterCalculationOptions options = mock(OwN2000RegisterCalculationOptions.class);
    final UploadFile uploadFile1 = mock(UploadFile.class);
    when(uploadFile1.getFileName()).thenReturn(CORRECT_FILE_NAME);
    final MultipartFile multipartFile1 = mock(MultipartFile.class);
    when(multipartFile1.getOriginalFilename()).thenReturn(CORRECT_FILE_NAME);

    when(authenticationService.getCurrentUserWithValidatingJobLimits()).thenReturn(connectUser);
    final ScenarioConstruction constructedScenario = mock(ScenarioConstruction.class);
    when(constructedScenario.isSuccesful()).thenReturn(true);
    when(scenarioService.constructScenario(same(Theme.OWN2000), any(), any())).thenReturn(constructedScenario);

    when(responseService.toOkResponse(calculateResponseCaptor.capture())).thenReturn(mockResponse);

    final ResponseEntity<CalculateResponse> response = registerResource.generateOwN2000Permit(options, List.of(uploadFile1), List.of(multipartFile1), null);
    assertEquals(mockResponse, response, "Returned response should come from responseService");

    final CalculateResponse capturedResponse = calculateResponseCaptor.getValue();
    assertNotNull(capturedResponse, "Content of response shouldn't be null");
    assertTrue(capturedResponse.getSuccessful(), "It should have been succesful");
  }

}
