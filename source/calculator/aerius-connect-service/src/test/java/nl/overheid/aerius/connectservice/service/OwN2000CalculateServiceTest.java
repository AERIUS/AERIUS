/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import static nl.overheid.aerius.connectservice.util.TestScenarioUtil.MAX_YEAR;
import static nl.overheid.aerius.connectservice.util.TestScenarioUtil.MIN_YEAR;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.connectservice.model.AppendixType;
import nl.overheid.aerius.connectservice.model.OwN2000CalculationOptions;
import nl.overheid.aerius.connectservice.model.OwN2000CalculationOptions.OutputTypeEnum;
import nl.overheid.aerius.connectservice.util.TestScenarioUtil;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.ConstantRepositoryBean;
import nl.overheid.aerius.db.connect.ConnectCalculationPointSetsRepositoryBean;
import nl.overheid.aerius.server.service.export.ExportTaskClientBean;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.domain.export.ExportAppendix;
import nl.overheid.aerius.shared.domain.export.ExportData.ExportAdditionalOptions;
import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Test class for {@link OwN2000CalculateService}.
 */
@ExtendWith(MockitoExtension.class)
class OwN2000CalculateServiceTest {

  private static final String TEST_JOB_KEY = "CalculateThis";
  private static final String TEST_NAME = "StandardNaming";
  private static final String TEST_EMAIL = "MyFakeEmail@example.org";

  private @Mock ExportTaskClientBean exportTaskClient;
  private @Mock JobRepositoryBean jobRepository;
  private @Mock ConnectCalculationPointSetsRepositoryBean pointSetsRepository;
  private @Mock ConstantRepositoryBean constantRepository;
  private @Mock ProxyFileService fileService;
  private @Mock LocaleService localeService;
  private @Mock EmissionsSourcesService emissionsSourcesService;

  private @Mock ConnectUser connectUser;

  private @Captor ArgumentCaptor<ExportProperties> exportPropertiesCaptor;

  OwN2000CalculateService calculateService;

  @BeforeEach
  void beforeEach() throws AeriusException {
    lenient().when(connectUser.getEmailAddress()).thenReturn(TEST_EMAIL);
    TestScenarioUtil.mockConstantRepositoryYears(constantRepository);
    lenient().when(localeService.getLocale()).thenReturn(Locale.ENGLISH);
    calculateService = new OwN2000CalculateService(exportTaskClient, jobRepository, pointSetsRepository, constantRepository, fileService, localeService,
        emissionsSourcesService);
  }

  @Test
  void testCalculate() throws AeriusException, IOException {
    final MockData data = mockScenarioData(OutputTypeEnum.GML, TEST_NAME, TestScenarioUtil.TEST_YEAR, true);
    final String jobKey = calculateService.calculate(connectUser, data.scenario(), data.options());

    verify(data.scenario().getSituations().get(0)).setYear(TestScenarioUtil.TEST_YEAR);
    verifyCalls(data, jobKey, TEST_NAME, JobType.CALCULATION, ExportType.GML_WITH_RESULTS, true);
  }

  @Test
  void testCalculateWithoutYearInOptions() throws AeriusException, IOException {
    final MockData data = mockScenarioData(OutputTypeEnum.GML, TEST_NAME, TestScenarioUtil.TEST_YEAR, true);
    when(data.options().getCalculationYear()).thenReturn(null);
    final String jobKey = calculateService.calculate(connectUser, data.scenario(), data.options());

    verify(data.scenario().getSituations().get(0), times(0)).setYear(anyInt());
    verifyCalls(data, jobKey, TEST_NAME, JobType.CALCULATION, ExportType.GML_WITH_RESULTS, true);
  }

  @Test
  void testCalculateLowerLimitYear() throws AeriusException {
    verifyExceptionThrownIncorrectCalculationYear(MIN_YEAR - 1);
  }

  @Test
  void testCalculateUpperLimitYear() throws AeriusException {
    verifyExceptionThrownIncorrectCalculationYear(MAX_YEAR + 1);
  }

  private void verifyExceptionThrownIncorrectCalculationYear(final int year) throws AeriusException {
    final MockData data = mockScenarioData(OutputTypeEnum.GML, TEST_NAME, year, true);
    when(data.options().getCalculationYear()).thenReturn(null);

    final AeriusException e = assertThrows(AeriusException.class, () -> calculateService.calculate(connectUser, data.scenario(), data.options()),
        "Expects an AeriusException to be thrown about incorrect year.");

    assertEquals(AeriusExceptionReason.CONNECT_INCORRECT_CALCULATIONYEAR, e.getReason(), "AeriusException reason");
    assertArrayEquals(new String[] {String.valueOf(year), String.valueOf(MIN_YEAR), String.valueOf(MAX_YEAR)}, e.getArgs(),
        "AeriusException arguments expected");
  }

  @ParameterizedTest
  @MethodSource("casesForPdf")
  void testCalculatePdf(final List<AppendixType> inputAppendices, final Set<ExportAppendix> expectedAppendicesInProperties)
      throws AeriusException, IOException {
    final MockData data = mockScenarioData(OutputTypeEnum.PDF, TEST_NAME, TestScenarioUtil.TEST_YEAR, true);
    when(data.options().getAppendices()).thenReturn(inputAppendices);
    final String jobKey = calculateService.calculate(connectUser, data.scenario(), data.options());

    verifyCalls(data, jobKey, TEST_NAME, JobType.REPORT, ExportType.PDF_PAA, true);
    assertEquals(expectedAppendicesInProperties, exportPropertiesCaptor.getValue().getAppendices(), "Expected appendices");
  }

  private static Stream<Arguments> casesForPdf() {
    return Stream.of(
        Arguments.of(null, Set.of(ExportAppendix.EXTRA_ASSESSMENT_REPORT)),
        Arguments.of(List.of(), Set.of(ExportAppendix.EXTRA_ASSESSMENT_REPORT)),
        Arguments.of(List.of(AppendixType.EDGE_EFFECT_REPORT), Set.of(ExportAppendix.EXTRA_ASSESSMENT_REPORT, ExportAppendix.EDGE_EFFECT_REPORT)),
        // Don't really have a unknown type, so fake that by using null.
        // Other option would be to mock it I guess, but mocking enums is a bit of a hassle...
        Arguments.of(Arrays.asList((AppendixType) null), Set.of(ExportAppendix.EXTRA_ASSESSMENT_REPORT)));
  }

  @Test
  void testCalculateNoName() throws AeriusException, IOException {
    final MockData data = mockScenarioData(OutputTypeEnum.GML, null, TestScenarioUtil.TEST_YEAR, true);
    final String jobKey = calculateService.calculate(connectUser, data.scenario(), data.options());

    verifyCalls(data, jobKey, null, JobType.CALCULATION, ExportType.GML_WITH_RESULTS, true);
  }

  @Test
  void testCalculateNoSendEmail() throws AeriusException, IOException {
    final MockData data = mockScenarioData(OutputTypeEnum.GML, TEST_NAME, TestScenarioUtil.TEST_YEAR, null);
    final String jobKey = calculateService.calculate(connectUser, data.scenario(), data.options());

    verifyCalls(data, jobKey, TEST_NAME, JobType.CALCULATION, ExportType.GML_WITH_RESULTS, true);
  }

  @Test
  void testCalculateSendEmailFalse() throws AeriusException, IOException {
    final MockData data = mockScenarioData(OutputTypeEnum.GML, TEST_NAME, TestScenarioUtil.TEST_YEAR, false);
    final String jobKey = calculateService.calculate(connectUser, data.scenario(), data.options());

    verifyCalls(data, jobKey, TEST_NAME, JobType.CALCULATION, ExportType.GML_WITH_RESULTS, false);
  }

  @ParameterizedTest
  @ValueSource(classes = {IOException.class, RuntimeException.class})
  void testCalculateException(final Class<? extends Throwable> clazz) throws AeriusException, IOException {
    final MockData data = mockScenarioData(OutputTypeEnum.PDF, TEST_NAME, TestScenarioUtil.TEST_YEAR, null);
    doThrow(clazz).when(exportTaskClient).startExport(any(), any(), any());

    final AeriusException exception = assertThrows(AeriusException.class,
        () -> calculateService.calculate(connectUser, data.scenario(), data.options()), "Expected Internal AeriusException error");

    assertEquals(AeriusExceptionReason.INTERNAL_ERROR, exception.getReason(), "Reason should be an internal error");
    verify(jobRepository).setErrorMessage(notNull(), any());
  }

  private void verifyCalls(final MockData data, final String jobKey, final String name, final JobType jobType, final ExportType exportType,
      final boolean expectsEmail) throws AeriusException, IOException {
    verifyCalls(data, jobKey, jobType, name);
    verifyExportProperties(exportType, name);
    verifyEmail(expectsEmail);
  }

  private void verifyCalls(final MockData data, final String jobKey, final JobType jobType, final String name) throws AeriusException, IOException {
    assertEquals(TEST_JOB_KEY, jobKey, "Job key shouldn't be null");
    verify(jobRepository).createJob(connectUser, jobType, Optional.ofNullable(name), false);
    verify(exportTaskClient).startExport(any(), exportPropertiesCaptor.capture(), eq(TEST_JOB_KEY));
  }

  private void verifyExportProperties(final ExportType exportType, final String name) {
    final ExportProperties capturedProperties = exportPropertiesCaptor.getValue();
    assertEquals(exportType, capturedProperties.getExportType(), "Not the expected export type");
    if (name == null) {
      assertNull(capturedProperties.getName(), "Shouldn't contain name");
    } else {
      assertEquals(TEST_NAME, capturedProperties.getName(), "Should contain name");
    }
  }

  private void verifyEmail(final boolean expectsEmail) {
    final ExportProperties capturedProperties = exportPropertiesCaptor.getValue();
    if (expectsEmail) {
      assertTrue(capturedProperties.getAdditionalOptions().contains(ExportAdditionalOptions.EMAIL_USER), "Should mail user");
      assertEquals(TEST_EMAIL, capturedProperties.getEmailAddress(), "Should contain user email address");
    } else {
      assertFalse(capturedProperties.getAdditionalOptions().contains(ExportAdditionalOptions.EMAIL_USER), "Shouldn't mail user");
      assertNull(capturedProperties.getEmailAddress(), "Shouldn't contain user email address");
    }
  }

  private MockData mockScenarioData(final OutputTypeEnum outputType, final String name, final int year, final Boolean sendEmail)
      throws AeriusException {
    final Scenario scenario = TestScenarioUtil.mockScenario(year);
    final OwN2000CalculationOptions options = mock(OwN2000CalculationOptions.class);
    when(options.getOutputType()).thenReturn(outputType);
    when(options.getName()).thenReturn(name);
    when(options.getSendEmail()).thenReturn(sendEmail);
    when(options.getCalculationYear()).thenReturn(TestScenarioUtil.TEST_YEAR);

    lenient().when(jobRepository.createJob(any(), any(), any(), eq(false))).thenReturn(TEST_JOB_KEY);
    return new MockData(scenario, options);
  }

  private record MockData(Scenario scenario, OwN2000CalculationOptions options) {
  }
}
