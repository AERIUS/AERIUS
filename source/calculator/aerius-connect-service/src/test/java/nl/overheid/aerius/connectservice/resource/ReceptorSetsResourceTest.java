/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.resource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import nl.overheid.aerius.connectservice.auth.AuthenticationService;
import nl.overheid.aerius.connectservice.domain.ImportParcelAggregate;
import nl.overheid.aerius.connectservice.model.ReceptorSet;
import nl.overheid.aerius.connectservice.model.ValidateResponse;
import nl.overheid.aerius.connectservice.model.ValidationMessage;
import nl.overheid.aerius.connectservice.service.ObjectValidatorService;
import nl.overheid.aerius.connectservice.service.ReceptorSetsService;
import nl.overheid.aerius.connectservice.service.ResponseService;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.exception.AeriusException;

@ExtendWith(MockitoExtension.class)
class ReceptorSetsResourceTest {

  @Mock ResponseService responseService;
  @Mock AuthenticationService authenticationService;
  @Mock ReceptorSetsService receptorSetsService;
  @Mock ObjectValidatorService validatorService;

  @Mock ConnectUser connectUser;

  @Mock ResponseEntity<ValidateResponse> mockValidateResponse;
  @Mock ResponseEntity<Void> mockVoidResponse;
  @Mock ResponseEntity<List<ReceptorSet>> mockListReceptorsResponse;

  @Captor ArgumentCaptor<ValidateResponse> validateResponseCaptor;

  ReceptorSetsResource receptorSetsResource;

  @SuppressWarnings("unchecked")
  @BeforeEach
  void beforeEach() throws AeriusException {
    when(authenticationService.getCurrentUser()).thenReturn(connectUser);
    lenient().when(responseService.convertToValidation(any(List.class))).thenAnswer((invocation) -> {
      final List<AeriusException> exceptions = (List<AeriusException>) invocation.getArgument(0);
      return exceptions.stream().map(e -> mock(ValidationMessage.class)).collect(Collectors.toList());
    });
    receptorSetsResource = new ReceptorSetsResource(authenticationService, responseService, receptorSetsService, validatorService);
  }

  @Test
  void testAddReceptorSet() throws AeriusException {
    final ReceptorSet receptorSet = mock(ReceptorSet.class);
    final MultipartFile filePart = mock(MultipartFile.class);
    final ImportParcelAggregate importAggregate = mock(ImportParcelAggregate.class);

    when(receptorSetsService.addReceptorSet(connectUser, receptorSet, filePart)).thenReturn(importAggregate);

    when(responseService.toOkResponse(validateResponseCaptor.capture())).thenReturn(mockValidateResponse);

    final ResponseEntity<ValidateResponse> returnedResponse = receptorSetsResource.addReceptorSet(receptorSet, filePart);

    assertEquals(mockValidateResponse, returnedResponse, "Response should come from response service");
    final ValidateResponse actualResponse = validateResponseCaptor.getValue();
    assertTrue(actualResponse.getSuccessful(), "No errors on import, so should be OK");
    assertEquals(0, actualResponse.getWarnings().size(), "No warnings should magically appear");
    assertEquals(0, actualResponse.getErrors().size(), "No errors should magically appear");
  }

  @Test
  void testAddReceptorSetWithWarnings() throws AeriusException {
    final ReceptorSet receptorSet = mock(ReceptorSet.class);
    final MultipartFile filePart = mock(MultipartFile.class);
    final ImportParcelAggregate importAggregate = mock(ImportParcelAggregate.class);
    final List<AeriusException> warnings = List.of(mock(AeriusException.class), mock(AeriusException.class));
    when(importAggregate.getWarnings()).thenReturn(warnings);

    when(receptorSetsService.addReceptorSet(connectUser, receptorSet, filePart)).thenReturn(importAggregate);

    when(responseService.toOkResponse(validateResponseCaptor.capture())).thenReturn(mockValidateResponse);

    final ResponseEntity<ValidateResponse> returnedResponse = receptorSetsResource.addReceptorSet(receptorSet, filePart);

    assertEquals(mockValidateResponse, returnedResponse, "Response should come from response service");
    final ValidateResponse actualResponse = validateResponseCaptor.getValue();
    assertTrue(actualResponse.getSuccessful(), "No errors on import, so should be OK");
    assertEquals(warnings.size(), actualResponse.getWarnings().size(), "No warnings should magically appear");
    assertEquals(0, actualResponse.getErrors().size(), "No errors should magically appear");
  }

  @Test
  void testAddReceptorSetWithErrors() throws AeriusException {
    final ReceptorSet receptorSet = mock(ReceptorSet.class);
    final MultipartFile filePart = mock(MultipartFile.class);
    final ImportParcelAggregate importAggregate = mock(ImportParcelAggregate.class);
    final List<AeriusException> errors = List.of(mock(AeriusException.class), mock(AeriusException.class), mock(AeriusException.class));
    when(importAggregate.getExceptions()).thenReturn(errors);

    when(receptorSetsService.addReceptorSet(connectUser, receptorSet, filePart)).thenReturn(importAggregate);

    when(responseService.toOkResponse(validateResponseCaptor.capture())).thenReturn(mockValidateResponse);

    final ResponseEntity<ValidateResponse> returnedResponse = receptorSetsResource.addReceptorSet(receptorSet, filePart);

    assertEquals(mockValidateResponse, returnedResponse, "Response should come from response service");
    final ValidateResponse actualResponse = validateResponseCaptor.getValue();
    assertFalse(actualResponse.getSuccessful(), "There are errors on import, so shouldn't be OK");
    assertEquals(0, actualResponse.getWarnings().size(), "No warnings should magically appear");
    assertEquals(errors.size(), actualResponse.getErrors().size(), "No errors should magically appear");
  }

  @Test
  void testAddReceptorSetException() throws AeriusException {
    final ReceptorSet receptorSet = mock(ReceptorSet.class);
    final MultipartFile filePart = mock(MultipartFile.class);

    final AeriusException aeriusException = mock(AeriusException.class);
    when(receptorSetsService.addReceptorSet(connectUser, receptorSet, filePart)).thenThrow(aeriusException);

    final ResponseStatusException mockStatusException = mock(ResponseStatusException.class);
    when(responseService.toResponseStatusException(aeriusException)).thenReturn(mockStatusException);

    final ResponseStatusException thrownException = assertThrows(ResponseStatusException.class,
        () -> receptorSetsResource.addReceptorSet(receptorSet, filePart));

    assertEquals(mockStatusException, thrownException, "Response should come from response service");
  }

  @Test
  void testDeleteReceptorSet() throws AeriusException {
    final String deleteName = "SomeNameToDelete";

    when(responseService.toOkResponse()).thenReturn(mockVoidResponse);

    final ResponseEntity<Void> returnedResponse = receptorSetsResource.deleteReceptorSet(deleteName);
    assertEquals(mockVoidResponse, returnedResponse, "Response should come from response service");

    verify(receptorSetsService).deleteReceptorSet(connectUser, deleteName);
  }

  @Test
  void testDeleteReceptorSetException() throws AeriusException {
    final String deleteName = "SomeNameToDelete";

    final AeriusException aeriusException = mock(AeriusException.class);
    doThrow(aeriusException).when(receptorSetsService).deleteReceptorSet(connectUser, deleteName);

    final ResponseStatusException mockStatusException = mock(ResponseStatusException.class);
    when(responseService.toResponseStatusException(aeriusException)).thenReturn(mockStatusException);

    final ResponseStatusException thrownException = assertThrows(ResponseStatusException.class,
        () -> receptorSetsResource.deleteReceptorSet(deleteName));

    assertEquals(mockStatusException, thrownException, "Response should come from response service");
  }

  @Test
  void testListReceptorSets() throws AeriusException {
    final List<ReceptorSet> receptorSets = List.of(mock(ReceptorSet.class), mock(ReceptorSet.class));

    when(receptorSetsService.getReceptorSets(connectUser)).thenReturn(receptorSets);

    when(responseService.toOkResponse(receptorSets)).thenReturn(mockListReceptorsResponse);

    final ResponseEntity<List<ReceptorSet>> returnedResponse = receptorSetsResource.listReceptorSets();
    assertEquals(mockListReceptorsResponse, returnedResponse, "Response should come from response service");
  }

  @Test
  void testListReceptorSetsException() throws AeriusException {
    final AeriusException aeriusException = mock(AeriusException.class);

    when(receptorSetsService.getReceptorSets(connectUser)).thenThrow(aeriusException);

    final ResponseStatusException mockStatusException = mock(ResponseStatusException.class);
    when(responseService.toResponseStatusException(aeriusException)).thenReturn(mockStatusException);

    final ResponseStatusException thrownException = assertThrows(ResponseStatusException.class,
        () -> receptorSetsResource.listReceptorSets());

    assertEquals(mockStatusException, thrownException, "Response should come from response service");
  }

}
