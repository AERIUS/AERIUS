/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import static nl.overheid.aerius.connectservice.util.TestScenarioUtil.MAX_YEAR;
import static nl.overheid.aerius.connectservice.util.TestScenarioUtil.MIN_YEAR;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Locale;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.connectservice.model.LbvCalculationOptions;
import nl.overheid.aerius.connectservice.util.TestScenarioUtil;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.ConstantRepositoryBean;
import nl.overheid.aerius.db.connect.ConnectCalculationPointSetsRepositoryBean;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.server.service.export.ExportTaskClientBean;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.domain.export.ExportData;
import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;

@ExtendWith(MockitoExtension.class)
class LbvCalculateServiceTest {

  private static final String TEST_EMAIL = "test@example.org";
  private static final String TEST_JOB_KEY = "1234";

  @Mock private ExportTaskClientBean exportTaskClient;
  @Mock private JobRepositoryBean jobRepository;
  @Mock private ConnectCalculationPointSetsRepositoryBean pointSetsRepository;
  @Mock private ConstantRepositoryBean constantRepository;
  @Mock private ProxyFileService fileService;
  @Mock private LocaleService localeService;
  @Mock private EmissionsSourcesService emissionsSourcesService;

  @InjectMocks LbvCalculateService calculateService;

  @Mock private ConnectUser connectUser;

  @Captor ArgumentCaptor<ExportProperties> exportPropertiesArgumentCaptor;

  @BeforeEach
  void setup() throws AeriusException {
    when(localeService.getLocale()).thenReturn(Locale.ENGLISH);
    lenient().when(constantRepository.getInteger(ConstantsEnum.MIN_YEAR)).thenReturn(MIN_YEAR);
    lenient().when(constantRepository.getInteger(ConstantsEnum.MAX_YEAR)).thenReturn(MAX_YEAR);
    lenient().when(connectUser.getEmailAddress()).thenReturn(TEST_EMAIL);
  }

  @Test
  void testCalculate() throws AeriusException, IOException {
    final Scenario scenario = TestScenarioUtil.mockScenario();
    final ScenarioSituation mockSituation = scenario.getSituations().get(0);
    when(mockSituation.getType()).thenReturn(SituationType.REFERENCE);
    when(scenario.getOptions().getCalculationJobType()).thenReturn(CalculationJobType.DEPOSITION_SUM);

    final LbvCalculationOptions options = new LbvCalculationOptions();
    options.sendEmail(true);

    when(jobRepository.createJob(any(), any(), any(), eq(false))).thenReturn(TEST_JOB_KEY);

    final String jobKey = calculateService.calculate(connectUser, scenario, options);

    assertEquals(TEST_JOB_KEY, jobKey, "Starting an Lbv(-plus) calculation should return the jobkey provided by the jobRepository");

    verify(exportTaskClient).startExport(any(), exportPropertiesArgumentCaptor.capture(), eq(TEST_JOB_KEY));
    final ExportProperties exportProperties = exportPropertiesArgumentCaptor.getValue();
    assertEquals(ExportType.PDF_PROCUREMENT_POLICY, exportProperties.getExportType(), "export type should always be procurement policy");
    assertTrue(exportProperties.getAdditionalOptions().contains(ExportData.ExportAdditionalOptions.EMAIL_USER), "Should send email");
    assertEquals(TEST_EMAIL, exportProperties.getEmailAddress(), "Export email should be that of the connect user.");
  }
}
