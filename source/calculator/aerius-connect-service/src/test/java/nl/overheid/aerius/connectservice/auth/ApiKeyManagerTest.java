/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.auth;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.description;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.Authentication;

import nl.overheid.aerius.connectservice.service.UserService;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.exception.AeriusException;

@ExtendWith(MockitoExtension.class)
class ApiKeyManagerTest {

  private static final String PRINCIPAL = "principal";
  @Mock private Authentication authentication;
  @Mock private UserService userService;

  @InjectMocks private ApiKeyManager apiKeyManager;

  @BeforeEach
  public void setup() {
    doReturn(PRINCIPAL).when(authentication).getPrincipal();
  }

  @Test
  void testAuthenticate() throws AeriusException {
    final ConnectUser user = mock(ConnectUser.class);
    doReturn(true).when(user).isEnabled();
    doReturn(Optional.of(user)).when(userService).getUser(PRINCIPAL);

    final Authentication result = apiKeyManager.authenticate(authentication);

    assertEquals(authentication, result, "The result should be the same object as the one passed to the authenticate method");
    verify(authentication, description("User should be authenticated")).setAuthenticated(true);
  }

  @Test
  void testAuthenticateUserNotEnabled() throws AeriusException {
    final ConnectUser user = mock(ConnectUser.class);
    doReturn(false).when(user).isEnabled();
    doReturn(Optional.of(user)).when(userService).getUser(PRINCIPAL);

    assertThrows(DisabledException.class, () -> apiKeyManager.authenticate(authentication),
        "If user is not enabled, DisabledException should be thrown");
  }

  @Test
  void testAuthenticateApiKeyNotFound() throws AeriusException {
    doReturn(Optional.empty()).when(userService).getUser(PRINCIPAL);

    assertThrows(BadCredentialsException.class, () -> apiKeyManager.authenticate(authentication),
        "If no user is found, BadCredentialsException should be thrown");
  }

  @Test
  void testAuthenticateUserServiceException() throws AeriusException {
    doThrow(new AeriusException()).when(userService).getUser(any());

    assertThrows(InternalAuthenticationServiceException.class, () -> apiKeyManager.authenticate(authentication),
        "If the userService is unavailable, InternalAuthenticationServiceException should be thrown");
  }
}
