/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.resource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import nl.overheid.aerius.connectservice.domain.ImportParcelAggregate;
import nl.overheid.aerius.connectservice.model.FileResponse;
import nl.overheid.aerius.connectservice.model.ValidateResponse;
import nl.overheid.aerius.connectservice.model.ValidationMessage;
import nl.overheid.aerius.connectservice.service.ExportService;
import nl.overheid.aerius.connectservice.service.ImportService;
import nl.overheid.aerius.connectservice.service.ResponseService;
import nl.overheid.aerius.connectservice.service.ValidationReportService;
import nl.overheid.aerius.importer.ImportOption;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.exception.AeriusException;

@ExtendWith(MockitoExtension.class)
class UtilityResourceTest {

  @Mock ImportService importService;
  @Mock ResponseService responseService;
  @Mock ValidationReportService validationReportService;
  @Mock ExportService exportService;

  @Mock ResponseEntity<FileResponse> mockFileResponse;
  @Mock ResponseEntity<ValidateResponse> mockValidateResponse;
  @Mock ResponseEntity<Resource> mockResourceResponse;

  @Captor ArgumentCaptor<FileResponse> fileResponseCaptor;
  @Captor ArgumentCaptor<ValidateResponse> validateResponseCaptor;
  @Captor ArgumentCaptor<Resource> resourceCaptor;
  @Captor ArgumentCaptor<HttpHeaders> headersCaptor;
  @Captor ArgumentCaptor<Set<ImportOption>> importOptionsCaptor;

  UtilityResource utilityResource;

  @SuppressWarnings("unchecked")
  @BeforeEach
  void beforeEach() throws AeriusException {
    lenient().when(responseService.convertToValidation(any(List.class))).thenAnswer((invocation) -> {
      final List<AeriusException> exceptions = (List<AeriusException>) invocation.getArgument(0);
      return exceptions.stream().map(e -> mock(ValidationMessage.class)).collect(Collectors.toList());
    });
    utilityResource = new UtilityResource(importService, responseService, validationReportService, exportService);
  }

  @Test
  void testConvert() throws AeriusException, IOException {
    final MultipartFile filePart = mock(MultipartFile.class);
    final ImportParcel importResult = mock(ImportParcel.class);
    final List<ImportParcel> importResults = List.of(importResult);

    when(importService.importFile(eq(filePart), importOptionsCaptor.capture(), any())).thenReturn(importResults);

    final byte[] mockedFileResponse = new byte[] {2, 5};
    when(exportService.exportAsZip(importResults)).thenReturn(mockedFileResponse);

    when(responseService.toOkResponse(fileResponseCaptor.capture())).thenReturn(mockFileResponse);

    final ResponseEntity<FileResponse> returnedResponse = utilityResource.validateAndConvert(filePart);

    assertEquals(mockFileResponse, returnedResponse, "Response should come from response service");
    final FileResponse actualResponse = fileResponseCaptor.getValue();
    assertTrue(actualResponse.getSuccessful(), "No errors on import, so should be OK");
    assertEquals(mockedFileResponse, actualResponse.getFilePart(),
        "Response should contain the generated file from export service");
    assertEquals(0, actualResponse.getWarnings().size(), "No warnings should magically appear");
    assertEquals(0, actualResponse.getErrors().size(), "No errors should magically appear");
    assertFalse(ImportOption.VALIDATE_STRICT.in(importOptionsCaptor.getValue()), "Shouldn't validate strict");
  }

  @Test
  void testConvertWithWarnings() throws AeriusException, IOException {
    final MultipartFile filePart = mock(MultipartFile.class);
    final ImportParcel importResult = mock(ImportParcel.class);
    final List<ImportParcel> importResults = List.of(importResult);
    final ArrayList<AeriusException> warnings = new ArrayList<>(List.of(mock(AeriusException.class), mock(AeriusException.class)));
    when(importResult.getWarnings()).thenReturn(warnings);

    when(importService.importFile(eq(filePart), any(), any())).thenReturn(importResults);

    final byte[] mockedFileResponse = new byte[] {2, 5};
    when(exportService.exportAsZip(importResults)).thenReturn(mockedFileResponse);

    when(responseService.toOkResponse(fileResponseCaptor.capture())).thenReturn(mockFileResponse);

    final ResponseEntity<FileResponse> returnedResponse = utilityResource.validateAndConvert(filePart);

    assertEquals(mockFileResponse, returnedResponse, "Response should come from response service");
    final FileResponse actualResponse = fileResponseCaptor.getValue();
    assertTrue(actualResponse.getSuccessful(), "No errors on import, so should be OK");
    assertEquals(mockedFileResponse, actualResponse.getFilePart(),
        "Response should contain the generated file from export service");
    assertEquals(warnings.size(), actualResponse.getWarnings().size(), "No warnings should magically appear");
    assertEquals(0, actualResponse.getErrors().size(), "No errors should magically appear");
  }

  @Test
  void testConvertWithErrors() throws AeriusException, IOException {
    final MultipartFile filePart = mock(MultipartFile.class);
    final ImportParcel importResult = mock(ImportParcel.class);
    final List<ImportParcel> importResults = List.of(importResult);
    final ArrayList<AeriusException> errors = new ArrayList<>(
        List.of(mock(AeriusException.class), mock(AeriusException.class), mock(AeriusException.class)));
    when(importResult.getExceptions()).thenReturn(errors);

    when(importService.importFile(eq(filePart), any(), any())).thenReturn(importResults);

    when(responseService.toOkResponse(fileResponseCaptor.capture())).thenReturn(mockFileResponse);

    final ResponseEntity<FileResponse> returnedResponse = utilityResource.validateAndConvert(filePart);

    assertEquals(mockFileResponse, returnedResponse, "Response should come from response service");
    final FileResponse actualResponse = fileResponseCaptor.getValue();
    assertFalse(actualResponse.getSuccessful(), "There are errors on import, so shouldn't be OK");
    assertNull(actualResponse.getFilePart(), "Response should contain the generated file from export service");
    assertEquals(0, actualResponse.getWarnings().size(), "No warnings should magically appear");
    assertEquals(errors.size(), actualResponse.getErrors().size(), "No errors should magically appear");
  }

  @Test
  void testConvertAeriusException() throws AeriusException, IOException {
    final MultipartFile filePart = mock(MultipartFile.class);
    final AeriusException aeriusException = mock(AeriusException.class);

    when(importService.importFile(eq(filePart), any(), any())).thenThrow(aeriusException);

    final ResponseStatusException mockStatusException = mock(ResponseStatusException.class);
    when(responseService.toResponseStatusException(aeriusException)).thenReturn(mockStatusException);

    final ResponseStatusException thrownException = assertThrows(ResponseStatusException.class, () -> utilityResource.convert(filePart));

    assertEquals(mockStatusException, thrownException, "Response should come from response service");
  }

  @Test
  void testConvertIOException() throws AeriusException, IOException {
    final MultipartFile filePart = mock(MultipartFile.class);
    final ImportParcel importResult = mock(ImportParcel.class);
    final List<ImportParcel> importResults = List.of(importResult);

    when(importService.importFile(eq(filePart), any(), any())).thenReturn(importResults);

    final IOException ioException = mock(IOException.class);
    when(exportService.exportAsZip(importResults)).thenThrow(ioException);

    final ResponseStatusException mockStatusException = mock(ResponseStatusException.class);
    when(responseService.toResponseStatusException(ioException)).thenReturn(mockStatusException);

    final ResponseStatusException thrownException = assertThrows(ResponseStatusException.class, () -> utilityResource.convert(filePart));

    assertEquals(mockStatusException, thrownException, "Response should come from response service");
  }

  @Test
  void testValidate() throws AeriusException {
    final MultipartFile filePart = mock(MultipartFile.class);
    final ImportParcel importResult = mock(ImportParcel.class);
    final List<ImportParcel> importResults = List.of(importResult);

    when(importService.importFile(eq(filePart), importOptionsCaptor.capture(), any())).thenReturn(importResults);

    when(responseService.toOkResponse(validateResponseCaptor.capture())).thenReturn(mockValidateResponse);

    final ResponseEntity<ValidateResponse> returnedResponse = utilityResource.validate(filePart, false);

    assertEquals(mockValidateResponse, returnedResponse, "Response should come from response service");
    final ValidateResponse actualResponse = validateResponseCaptor.getValue();
    assertTrue(actualResponse.getSuccessful(), "No errors on import, so should be OK");
    assertEquals(0, actualResponse.getWarnings().size(), "No warnings should magically appear");
    assertEquals(0, actualResponse.getErrors().size(), "No errors should magically appear");
    assertFalse(ImportOption.VALIDATE_STRICT.in(importOptionsCaptor.getValue()), "Shouldn't validate strict");
  }

  @Test
  void testValidateStrict() throws AeriusException {
    final MultipartFile filePart = mock(MultipartFile.class);
    final ImportParcel importResult = mock(ImportParcel.class);
    final List<ImportParcel> importResults = List.of(importResult);

    when(importService.importFile(eq(filePart), importOptionsCaptor.capture(), any())).thenReturn(importResults);

    when(responseService.toOkResponse(validateResponseCaptor.capture())).thenReturn(mockValidateResponse);

    final ResponseEntity<ValidateResponse> returnedResponse = utilityResource.validate(filePart, true);

    assertEquals(mockValidateResponse, returnedResponse, "Response should come from response service");
    final ValidateResponse actualResponse = validateResponseCaptor.getValue();
    assertTrue(actualResponse.getSuccessful(), "No errors on import, so should be OK");
    assertEquals(0, actualResponse.getWarnings().size(), "No warnings should magically appear");
    assertEquals(0, actualResponse.getErrors().size(), "No errors should magically appear");
    assertTrue(ImportOption.VALIDATE_STRICT.in(importOptionsCaptor.getValue()), "Should validate strict");
  }

  @Test
  void testValidateWithWarnings() throws AeriusException {
    final MultipartFile filePart = mock(MultipartFile.class);
    final ImportParcel importResult = mock(ImportParcel.class);
    final List<ImportParcel> importResults = List.of(importResult);
    final ArrayList<AeriusException> warnings = new ArrayList<>(List.of(mock(AeriusException.class), mock(AeriusException.class)));
    when(importResult.getWarnings()).thenReturn(warnings);

    when(importService.importFile(eq(filePart), any(), any())).thenReturn(importResults);

    when(responseService.toOkResponse(validateResponseCaptor.capture())).thenReturn(mockValidateResponse);

    final ResponseEntity<ValidateResponse> returnedResponse = utilityResource.validate(filePart, true);

    assertEquals(mockValidateResponse, returnedResponse, "Response should come from response service");
    final ValidateResponse actualResponse = validateResponseCaptor.getValue();
    assertTrue(actualResponse.getSuccessful(), "No errors on import, so should be OK");
    assertEquals(warnings.size(), actualResponse.getWarnings().size(), "No warnings should magically appear");
    assertEquals(0, actualResponse.getErrors().size(), "No errors should magically appear");
  }

  @Test
  void testValidateWithErrors() throws AeriusException {
    final MultipartFile filePart = mock(MultipartFile.class);
    final ImportParcel importResult = mock(ImportParcel.class);
    final List<ImportParcel> importResults = List.of(importResult);
    final ArrayList<AeriusException> errors = new ArrayList<>(
        List.of(mock(AeriusException.class), mock(AeriusException.class), mock(AeriusException.class)));
    when(importResult.getExceptions()).thenReturn(errors);

    when(importService.importFile(eq(filePart), any(), any())).thenReturn(importResults);

    when(responseService.toOkResponse(validateResponseCaptor.capture())).thenReturn(mockValidateResponse);

    final ResponseEntity<ValidateResponse> returnedResponse = utilityResource.validate(filePart, true);

    assertEquals(mockValidateResponse, returnedResponse, "Response should come from response service");
    final ValidateResponse actualResponse = validateResponseCaptor.getValue();
    assertFalse(actualResponse.getSuccessful(), "There are errors on import, so shouldn't be OK");
    assertEquals(0, actualResponse.getWarnings().size(), "No warnings should magically appear");
    assertEquals(errors.size(), actualResponse.getErrors().size(), "No errors should magically appear");
  }

  @Test
  void testValidateException() throws AeriusException {
    final MultipartFile filePart = mock(MultipartFile.class);
    final AeriusException aeriusException = mock(AeriusException.class);

    when(importService.importFile(eq(filePart), any(), any())).thenThrow(aeriusException);

    final ResponseStatusException mockStatusException = mock(ResponseStatusException.class);
    when(responseService.toResponseStatusException(aeriusException)).thenReturn(mockStatusException);

    final ResponseStatusException thrownException = assertThrows(ResponseStatusException.class, () -> utilityResource.validate(filePart, true));

    assertEquals(mockStatusException, thrownException, "Response should come from response service");
  }

  @Test
  void testValidateReport() throws AeriusException, IOException {
    final MultipartFile filePart = mock(MultipartFile.class);
    final ImportParcel importResult = mock(ImportParcel.class);
    final List<ImportParcel> importResults = List.of(importResult);

    when(importService.importFile(eq(filePart), importOptionsCaptor.capture(), any())).thenReturn(importResults);

    when(responseService.toOkResponse(resourceCaptor.capture(), headersCaptor.capture())).thenReturn(mockResourceResponse);

    final ResponseEntity<Resource> returnedResponse = utilityResource.validateReport(filePart, false);

    assertEquals(mockResourceResponse, returnedResponse, "Response should come from response service");
    final Resource resource = resourceCaptor.getValue();
    assertTrue(resource instanceof ByteArrayResource, "Should be a byte array resource");
    final HttpHeaders headers = headersCaptor.getValue();
    assertTrue(headers.containsKey(HttpHeaders.CONTENT_DISPOSITION), "Should contain content disposition");
    assertFalse(ImportOption.VALIDATE_STRICT.in(importOptionsCaptor.getValue()), "Shouldn't validate strict");

    verify(validationReportService).report(any(ImportParcelAggregate.class), any(), any());
  }

  @Test
  void testValidateReportStrict() throws AeriusException, IOException {
    final MultipartFile filePart = mock(MultipartFile.class);
    final ImportParcel importResult = mock(ImportParcel.class);
    final List<ImportParcel> importResults = List.of(importResult);

    when(importService.importFile(eq(filePart), importOptionsCaptor.capture(), any())).thenReturn(importResults);

    when(responseService.toOkResponse(resourceCaptor.capture(), headersCaptor.capture())).thenReturn(mockResourceResponse);

    final ResponseEntity<Resource> returnedResponse = utilityResource.validateReport(filePart, true);

    assertEquals(mockResourceResponse, returnedResponse, "Response should come from response service");
    final Resource resource = resourceCaptor.getValue();
    assertTrue(resource instanceof ByteArrayResource, "Should be a byte array resource");
    final HttpHeaders headers = headersCaptor.getValue();
    assertTrue(headers.containsKey(HttpHeaders.CONTENT_DISPOSITION), "Should contain content disposition");
    assertTrue(ImportOption.VALIDATE_STRICT.in(importOptionsCaptor.getValue()), "Should validate strict");

    verify(validationReportService).report(any(ImportParcelAggregate.class), any(), any());
  }

  @Test
  void testValidateReportException() throws AeriusException, IOException {
    final MultipartFile filePart = mock(MultipartFile.class);
    final ImportParcel importResult = mock(ImportParcel.class);
    final List<ImportParcel> importResults = List.of(importResult);

    when(importService.importFile(eq(filePart), any(), any())).thenReturn(importResults);

    final IOException ioException = mock(IOException.class);
    doThrow(ioException).when(validationReportService).report(any(ImportParcelAggregate.class), any(), any());

    final ResponseStatusException mockStatusException = mock(ResponseStatusException.class);
    when(responseService.toResponseStatusException(ioException)).thenReturn(mockStatusException);

    final ResponseStatusException thrownException = assertThrows(ResponseStatusException.class,
        () -> utilityResource.validateReport(filePart, true));

    assertEquals(mockStatusException, thrownException, "Response should come from response service");
  }

}
