/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.connectservice.model.Calculation;
import nl.overheid.aerius.connectservice.model.OwN2000RegisterCalculationOptions;
import nl.overheid.aerius.connectservice.model.RegisterCalculationResults;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.ConstantRepositoryBean;
import nl.overheid.aerius.db.connect.ConnectCalculationPointSetsRepositoryBean;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.server.service.export.ExportTaskClientBean;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Test class for {@link RegisterOwN2000PermitService}.
 */
@ExtendWith(MockitoExtension.class)
class RegisterOwN2000PermitServiceTest {

  private static final int MIN_YEAR = 1300;
  private static final int MAX_YEAR = 1500;
  private static final int TEST_YEAR = 1400;
  private static final String TEST_EMAIL = "MyFakeEmail@example.org";
  private static final String TEST_NAME = "StandardNaming";
  private static final String TEST_JOB_KEY = "someJobKey";

  private @Mock ExportTaskClientBean exportTaskClient;
  private @Mock JobRepositoryBean jobRepository;
  private @Mock ConnectCalculationPointSetsRepositoryBean pointSetsRepository;
  private @Mock ConstantRepositoryBean constantRepository;
  private @Mock ProxyFileService fileService;
  private @Mock LocaleService localeService;
  private @Mock EmissionsSourcesService emissionsSourcesService;

  private @Mock ConnectUser connectUser;

  private @Captor ArgumentCaptor<ExportProperties> exportPropertiesCaptor;

  private RegisterOwN2000PermitService registerOwN2000PermitService;

  @BeforeEach
  void beforeEach() throws AeriusException {
    lenient().when(connectUser.getEmailAddress()).thenReturn(TEST_EMAIL);
    lenient().when(constantRepository.getInteger(ConstantsEnum.MIN_YEAR)).thenReturn(MIN_YEAR);
    lenient().when(constantRepository.getInteger(ConstantsEnum.MAX_YEAR)).thenReturn(MAX_YEAR);
    lenient().when(localeService.getLocale()).thenReturn(Locale.ENGLISH);
    lenient().when(jobRepository.createJob(any(), any(), any(), anyBoolean())).thenReturn(TEST_JOB_KEY);
    registerOwN2000PermitService = new RegisterOwN2000PermitService(exportTaskClient, jobRepository, pointSetsRepository,
        constantRepository, fileService, localeService, emissionsSourcesService);
  }

  @Test
  void testCalculate() throws AeriusException {
    final Scenario scenario = mockScenario();
    final OwN2000RegisterCalculationOptions options = mock(OwN2000RegisterCalculationOptions.class);
    when(options.getName()).thenReturn(TEST_NAME);
    when(options.getCalculationYear()).thenReturn(TEST_YEAR);

    final String result = registerOwN2000PermitService.calculate(connectUser, scenario, options);

    verify(fileService, times(1)).writeJson(any(), any(), any(), any());
    assertEquals(TEST_JOB_KEY, result, "It should be valid to use both a proposed and reference situation");
  }


  @Test
  void testProposedAndReference() throws AeriusException {
    final Scenario scenario = mockScenario();
    final OwN2000RegisterCalculationOptions options = mock(OwN2000RegisterCalculationOptions.class);
    when(options.getName()).thenReturn(TEST_NAME);
    when(options.getCalculationYear()).thenReturn(TEST_YEAR);
    final RegisterCalculationResults calculationResults = mockCalculationResults("proposed");

    final String result = registerOwN2000PermitService.saveCalculation(connectUser, scenario, options, calculationResults);

    assertEquals(TEST_JOB_KEY, result, "It should be valid to use both a proposed and reference situation");
    verify(fileService, times(2)).writeJson(any(), any(), any(), any());
  }

  @ParameterizedTest
  @MethodSource("casesForSingleSituation")
  void testSingleSituation(final SituationType situationType) throws AeriusException {
    final Scenario scenario = mockSingleScenario(situationType);
    final OwN2000RegisterCalculationOptions options = mock(OwN2000RegisterCalculationOptions.class);
    when(options.getName()).thenReturn(TEST_NAME);
    when(options.getCalculationYear()).thenReturn(TEST_YEAR);
    final RegisterCalculationResults calculationResults = mockCalculationResults(situationType.name().toLowerCase());

    final String result = registerOwN2000PermitService.saveCalculation(connectUser, scenario, options, calculationResults);

    assertEquals(TEST_JOB_KEY, result, "It should be valid to use just a reference situation");
  }

  private static Stream<Arguments> casesForSingleSituation() {
    return Stream.of(
        Arguments.of(SituationType.REFERENCE),
        Arguments.of(SituationType.OFF_SITE_REDUCTION),
        Arguments.of(SituationType.PROPOSED));
  }

  @Test
  void testNotSupportedSituations() {
    final Scenario scenario = mockNotSupportedSituations();
    final OwN2000RegisterCalculationOptions options = mock(OwN2000RegisterCalculationOptions.class);
    when(options.getName()).thenReturn(TEST_NAME);
    when(options.getCalculationYear()).thenReturn(TEST_YEAR);
    final RegisterCalculationResults calculationResults = mockEmptyCalculationResults();

    final AeriusException e = assertThrows(AeriusException.class,
        () -> registerOwN2000PermitService.saveCalculation(connectUser, scenario, options, calculationResults),
        "Saving a calculation should throw an exception when scenario contains an unsupported situation.");

    assertEquals(AeriusExceptionReason.CALCULATION_JOB_TYPE_INVALID_SITUATION_TYPE, e.getReason(),
        "Reason should be that temporary situation is not expected");
  }

  @Test
  void testIncorrectSituations() {
    final Scenario scenario = mockIncorrectSituations();
    final OwN2000RegisterCalculationOptions options = mock(OwN2000RegisterCalculationOptions.class);
    when(options.getName()).thenReturn(TEST_NAME);
    when(options.getCalculationYear()).thenReturn(TEST_YEAR);
    final RegisterCalculationResults calculationResults = mockEmptyCalculationResults();

    final AeriusException e = assertThrows(AeriusException.class,
        () -> registerOwN2000PermitService.saveCalculation(connectUser, scenario, options, calculationResults),
        "Saving a calculation should throw an exception when scenario contains two reference situations.");

    assertEquals(AeriusExceptionReason.CALCULATION_JOB_TYPE_TOO_MANY_SITUATIONS, e.getReason(),
        "Reason should be that scenario contains too many reference situations");
  }

  @Test
  void testShouldContainCalculationResults() {
    final Scenario scenario = mockScenario();
    final OwN2000RegisterCalculationOptions options = mock(OwN2000RegisterCalculationOptions.class);
    when(options.getName()).thenReturn(TEST_NAME);
    when(options.getCalculationYear()).thenReturn(TEST_YEAR);
    final RegisterCalculationResults calculationResults = mockEmptyCalculationResults();

    final AeriusException e = assertThrows(AeriusException.class,
        () -> registerOwN2000PermitService.saveCalculation(connectUser, scenario, options, calculationResults),
        "Saving a calculation should throw an exception when calculationResults contains no results.");

    assertEquals(AeriusExceptionReason.CALCULATION_PAA_PERMIT_CALCULATION_RESULTS_MISSING, e.getReason(),
        "Reason should be that calculation results is expected");
  }

  @Test
  void testShouldContainCalculationResultsForOneScenarioOfEachType() {
    final Scenario scenario = mockScenario();
    final OwN2000RegisterCalculationOptions options = mock(OwN2000RegisterCalculationOptions.class);
    when(options.getName()).thenReturn(TEST_NAME);
    when(options.getCalculationYear()).thenReturn(TEST_YEAR);
    final RegisterCalculationResults calculationResults = mockDoubleCalculationResults();

    final AeriusException e = assertThrows(AeriusException.class,
        () -> registerOwN2000PermitService.saveCalculation(connectUser, scenario, options, calculationResults),
        "Saving a calculation should throw an exception when calculationResults contains duplicate situation types.");

    assertEquals(AeriusExceptionReason.CALCULATION_PAA_PERMIT_CALCULATION_RESULTS_INCORRECT_SITUATIONS_COUNT, e.getReason(),
        "Reason should be that calculation results should contain at most one of each type");
  }

  private Scenario mockScenario() {
    return mockScenario(TEST_YEAR);
  }

  private Scenario mockScenario(final int year) {
    final Scenario scenario = mock(Scenario.class);
    final ScenarioSituation situation1 = mock(ScenarioSituation.class);
    lenient().when(situation1.getType()).thenReturn(SituationType.PROPOSED);
    when(situation1.getYear()).thenReturn(year);
    final ScenarioSituation situation2 = mock(ScenarioSituation.class);
    lenient().when(situation2.getType()).thenReturn(SituationType.REFERENCE);
    when(situation2.getYear()).thenReturn(year);
    when(scenario.getSituations()).thenReturn(List.of(situation1, situation2));
    return scenario;
  }

  private Scenario mockIncorrectSituations() {
    final Scenario scenario = mock(Scenario.class);
    final ScenarioSituation situation1 = mock(ScenarioSituation.class);
    lenient().when(situation1.getType()).thenReturn(SituationType.PROPOSED);
    when(situation1.getYear()).thenReturn(TEST_YEAR);
    final ScenarioSituation situation2 = mock(ScenarioSituation.class);
    lenient().when(situation2.getType()).thenReturn(SituationType.REFERENCE);
    when(situation2.getYear()).thenReturn(TEST_YEAR);
    final ScenarioSituation situation3 = mock(ScenarioSituation.class);
    lenient().when(situation3.getType()).thenReturn(SituationType.OFF_SITE_REDUCTION);
    when(situation3.getYear()).thenReturn(TEST_YEAR);
    final ScenarioSituation situation4 = mock(ScenarioSituation.class);
    lenient().when(situation4.getType()).thenReturn(SituationType.REFERENCE);
    when(situation4.getYear()).thenReturn(TEST_YEAR);
    when(scenario.getSituations()).thenReturn(List.of(situation1, situation2, situation3, situation4));
    return scenario;
  }

  private Scenario mockNotSupportedSituations() {
    final Scenario scenario = mock(Scenario.class);
    final ScenarioSituation situation1 = mock(ScenarioSituation.class);
    lenient().when(situation1.getType()).thenReturn(SituationType.PROPOSED);
    when(situation1.getYear()).thenReturn(TEST_YEAR);
    final ScenarioSituation situation2 = mock(ScenarioSituation.class);
    lenient().when(situation2.getType()).thenReturn(SituationType.TEMPORARY);
    when(situation2.getYear()).thenReturn(TEST_YEAR);
    when(scenario.getSituations()).thenReturn(List.of(situation1, situation2));
    return scenario;
  }

  private Scenario mockSingleScenario(final SituationType situationType) {
    final Scenario scenario = mock(Scenario.class);
    final ScenarioSituation situation1 = mock(ScenarioSituation.class);
    lenient().when(situation1.getType()).thenReturn(situationType);
    when(situation1.getYear()).thenReturn(TEST_YEAR);
    when(scenario.getSituations()).thenReturn(List.of(situation1));
    return scenario;
  }

  private RegisterCalculationResults mockCalculationResults(final String... situationTypes) {
    final RegisterCalculationResults results = mock(RegisterCalculationResults.class);
    final List<Calculation> calculations = new ArrayList<>();
    for (final String situationType : situationTypes) {
      final Calculation mockCalculation = mock(Calculation.class);
      when(mockCalculation.getScenario()).thenReturn(situationType);
      calculations.add(mockCalculation);
    }

    when(results.getCalculations()).thenReturn(calculations);

    return results;
  }

  private RegisterCalculationResults mockEmptyCalculationResults() {
    final RegisterCalculationResults registerCalculationResults = mock(RegisterCalculationResults.class);
    return registerCalculationResults;
  }

  private RegisterCalculationResults mockDoubleCalculationResults() {
    return mockCalculationResults("proposed", "proposed");
  }
}
