/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.util;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.shared.domain.v2.geojson.Crs;
import nl.overheid.aerius.shared.domain.v2.geojson.Crs.CrsContent;
import nl.overheid.aerius.shared.domain.v2.geojson.FeatureCollection;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.geo.EPSG;

class ScenarioSanitationUtilTest {
  static Stream<Arguments> pointProvider() {
    return Stream.of(
        Arguments.of("Rounding up positive", new Point(1.0001, -0.9999), "1.0", "-1.0"),
        Arguments.of("Rounding down negative", new Point(-0.9999, 1.0001), "-1.0", "1.0"),
        Arguments.of("Edge case rounding up", new Point(0.9995, -0.9995), "1.0", "-0.999"),
        Arguments.of("Edge case rounding down", new Point(0.9994, -0.9994), "0.999", "-0.999"),
        Arguments.of("Precision floats with rounding", new Point(5.9999999, -5.9999999), "6.0", "-6.0"),
        Arguments.of("Floating-point errors", new Point(0.1 + 0.2 - 0.3, 1.0 - 0.9), "0.0", "0.1"),
        Arguments.of("Zero value input", new Point(0.0, 0.0), "0.0", "0.0"),
        Arguments.of("Integer inputs converted to float", new Point(2, 3), "2.0", "3.0"),
        Arguments.of("Point Geometry", new Point(1.2345, -4.56789), "1.235", "-4.568"));
  }

  static Stream<Arguments> lineProvider() {
    return Stream.of(
        Arguments.of("LineString Geometry",
            createLineString(new double[][] {{1.1111, 2.2222}, {3.3333, 4.4444}, {5.5555, 6.6666}}),
            new double[][] {{1.111, 2.222}, {3.333, 4.444}, {5.556, 6.667}}));
  };

  static Stream<Arguments> polygonProvider() {
    return Stream.of(
        Arguments.of("Polygon Geometry",
            createPolygon(new double[][][] {{{1.1111, 2.2222}, {3.3333, 4.4444}, {5.5555, 6.6666}, {1.1111, 2.2222}}}),
            new double[][][] {{{1.111, 2.222}, {3.333, 4.444}, {5.556, 6.667}, {1.111, 2.222}}}));
  }

  static Stream<Arguments> epsgProvider() {
    return Stream.of(
        Arguments.of(1.000001, 0.999999, 1.000001, 0.999999, EPSG.WGS84),
        // TODO AER-2361:Rounding is not enabled so we are not currently expecting geometries in scenarios to change
        // When rounding is enabled again, the commented-out cases below should be expected instead
        Arguments.of(1.000001, 0.999999, 1.000001, 0.999999, EPSG.RDNEW),
        Arguments.of(1.000001, 0.999999, 1.000001, 0.999999, EPSG.BNG),
        Arguments.of(1.000001, 0.999999, 1.000001, 0.999999, EPSG.TM65),
        Arguments.of(1.000001, 0.999999, 1.000001, 0.999999, EPSG.TM65));
    // Arguments.of(1.000001, 0.999999, 1.0, 1.0, EPSG.RDNEW),
    // Arguments.of(1.000001, 0.999999, 1.0, 1.0, EPSG.BNG),
    // Arguments.of(1.000001, 0.999999, 1.0, 1.0, EPSG.TM65),
    // Arguments.of(1.000001, 0.999999, 1.0, 1.0, EPSG.TM65));
  }

  @ParameterizedTest(name = "{4} - {0}:{1} should be {2}:{3}")
  @MethodSource("epsgProvider")
  void testSrids(final double originalX, final double originalY, final double expectedX, final double expectedY, final EPSG epsg) {
    final Scenario scenario = mock(Scenario.class);
    final ScenarioSituation situation = mock(ScenarioSituation.class);
    final EmissionSourceFeature source = mock(EmissionSourceFeature.class);
    final Crs mockCrs = mock(Crs.class);
    final CrsContent mockProperties = mock(CrsContent.class);
    final Point point = new Point(originalX, originalY);
    final FeatureCollection<EmissionSourceFeature> sources = new FeatureCollection<>();
    sources.setCrs(mockCrs);
    sources.setFeatures(List.of(source));

    when(scenario.getSituations()).thenReturn(List.of(situation));
    when(situation.getBuildings()).thenReturn(new FeatureCollection<>());
    when(scenario.getCustomPoints()).thenReturn(new FeatureCollection<>());
    when(source.getGeometry()).thenReturn(point);
    when(situation.getSources()).thenReturn(sources);
    when(mockCrs.getProperties()).thenReturn(mockProperties);
    when(mockProperties.getName()).thenReturn(epsg.getEpsgCode());

    ScenarioSanitationUtil.sanitizeScenario(scenario);

    final Point resultPoint = (Point) scenario.getSituations().get(0).getSources().getFeatures().get(0).getGeometry();
    assertEquals(expectedX, resultPoint.getX(), "X coordinate mismatch for " + epsg.getEpsgCode());
    assertEquals(expectedY, resultPoint.getY(), "Y coordinate mismatch for " + epsg.getEpsgCode());

    // TODO AER-2361: Rounding is not enabled so we are not currently touching the following, but once enabled it should be the verified again.
    // verify(scenario, atLeastOnce()).getSituations();
    // verify(situation, atLeastOnce()).getSources();
    // verify(source, atLeastOnce()).getGeometry();
    // verify(mockCrs, atLeastOnce()).getProperties();
    // verify(mockProperties, atLeastOnce()).getName();
  }

  @ParameterizedTest(name = "{0}")
  @MethodSource("pointProvider")
  void testPoints(final String description, final Geometry inputGeometry, final String expectedX, final String expectedY) {
    ScenarioSanitationUtil.sanitizeGeometry(inputGeometry);
    assertTrue(inputGeometry instanceof Point, "Sanity check, geometry is point");
    final Point point = (Point) inputGeometry;
    assertEquals(expectedX, String.valueOf(point.getX()), "X coordinate mismatch for Point");
    assertEquals(expectedY, String.valueOf(point.getY()), "Y coordinate mismatch for Point");
  }

  @ParameterizedTest(name = "{0}")
  @MethodSource("lineProvider")
  void testLines(final String description, final Geometry inputGeometry, final double[][] expectedCoordinates) {
    ScenarioSanitationUtil.sanitizeGeometry(inputGeometry);
    assertTrue(inputGeometry instanceof LineString, "Sanity check, geometry is line");
    final LineString line = (LineString) inputGeometry;

    assertArrayEquals(expectedCoordinates, line.getCoordinates(), "Coordinates mismatch for LineString");
  }

  @ParameterizedTest(name = "{0}")
  @MethodSource("polygonProvider")
  void testPolygon(final String description, final Geometry inputGeometry, final double[][][] expectedCoordinates) {
    ScenarioSanitationUtil.sanitizeGeometry(inputGeometry);
    assertTrue(inputGeometry instanceof Polygon, "Sanity check, geometry is polygon");
    final Polygon polygon = (Polygon) inputGeometry;

    assertArrayEquals(expectedCoordinates, polygon.getCoordinates(), "Coordinates mismatch for Polygon");
  }

  private static LineString createLineString(final double[][] ds) {
    final LineString ls = new LineString();
    ls.setCoordinates(ds);
    return ls;
  }

  private static Polygon createPolygon(final double[][][] ds) {
    final Polygon ls = new Polygon();
    ls.setCoordinates(ds);
    return ls;
  }
}
