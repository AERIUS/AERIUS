/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import nl.overheid.aerius.connectservice.model.SituationResults;
import nl.overheid.aerius.connectservice.model.SituationSummary;
import nl.overheid.aerius.connectservice.model.SourcesSummary;
import nl.overheid.aerius.connectservice.model.SummaryResponse;
import nl.overheid.aerius.importer.summary.ImportSummaryGenerator;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.importer.summary.ImportSummary;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.ReceptorPoint;
import nl.overheid.aerius.shared.domain.v2.point.SubPoint;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioMetaData;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituationResults;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;

@ExtendWith(MockitoExtension.class)
class ScenarioSummaryServiceTest {

  private static final int FARM_LODGE_SECTOR_ID = 4110;
  private static final String REFERENCE = "reference";
  private static final String CORPORATION = "corporation";
  private static final String PROJECT_NAME = "projectName";
  private static final String DESCRIPTION = "description";
  private static final String STREET_ADDRESS = "streetAddress";
  private static final String POSTCODE = "postcode";
  private static final String CITY = "city";
  private static final int SITUATION_YEAR = 2022;

  ImportSummaryGenerator importSummaryGenerator = new ImportSummaryGenerator();
  ScenarioMetaData scenarioMetaData = new ScenarioMetaData();

  @Mock ImportParcel importParcel;
  @Mock ScenarioSituation scenarioSituation;
  @Mock ScenarioSituationResults scenarioSituationResults;
  @Mock EmissionSourceFeature emissionSourceFeature;
  @Mock CalculationPointFeature calculationPointFeature1;
  @Mock CalculationPointFeature calculationPointFeature2;
  @Mock ReceptorPoint receptorPoint;
  @Mock SubPoint subPoint;

  private final ObjectMapper objectMapper = new ObjectMapper();

  @BeforeEach
  void beforeEach() throws AeriusException {
    when(importParcel.getSituation()).thenReturn(scenarioSituation);
    when(importParcel.getImportedMetaData()).thenReturn(scenarioMetaData);
    when(importParcel.getSituationResults()).thenReturn(scenarioSituationResults);

    when(scenarioSituation.getType()).thenReturn(SituationType.PROPOSED);
    when(scenarioSituation.getYear()).thenReturn(SITUATION_YEAR);
    when(scenarioSituation.getEmissionSourcesList())
        .thenReturn(Arrays.asList(emissionSourceFeature, emissionSourceFeature, emissionSourceFeature));
    when(scenarioSituation.getReference()).thenReturn(REFERENCE);

    scenarioMetaData.setCorporation(CORPORATION);
    scenarioMetaData.setProjectName(PROJECT_NAME);
    scenarioMetaData.setDescription(DESCRIPTION);
    scenarioMetaData.setStreetAddress(STREET_ADDRESS);
    scenarioMetaData.setPostcode(POSTCODE);
    scenarioMetaData.setCity(CITY);

    when(receptorPoint.getReceptorId()).thenReturn(12);
    when(receptorPoint.getResults()).thenReturn(Map.of(EmissionResultKey.NOX_DEPOSITION, 50.0, EmissionResultKey.NH3_DEPOSITION, 100.0));

    when(subPoint.getReceptorId()).thenReturn(18);
    when(subPoint.getSubPointId()).thenReturn(42);
    when(subPoint.getResults()).thenReturn(Map.of(EmissionResultKey.NOX_CONCENTRATION, 50.0, EmissionResultKey.NH3_CONCENTRATION, 100.0));

    when(calculationPointFeature1.getProperties()).thenReturn(receptorPoint);
    when(calculationPointFeature2.getProperties()).thenReturn(subPoint);
    when(scenarioSituationResults.getResults()).thenReturn(Arrays.asList(calculationPointFeature1, calculationPointFeature2));

    when(emissionSourceFeature.getProperties()).thenReturn(mock(EmissionSource.class));
    when(emissionSourceFeature.getProperties().getEmissions()).thenReturn(Map.of(Substance.NH3, 10.0));
    when(emissionSourceFeature.getProperties().getSectorId()).thenReturn(FARM_LODGE_SECTOR_ID);
    when(emissionSourceFeature.getGeometry()).thenReturn(new Point(100, 200));
  }

  private SummaryResponse getSummaryResponse() throws AeriusException, JsonProcessingException {
    final ImportSummary importSummary = importSummaryGenerator.generateImportSummary(Locale.ENGLISH, List.of(importParcel));
    final String generatedJson = objectMapper.writeValueAsString(importSummary);
    return objectMapper.readValue(generatedJson, SummaryResponse.class);
  }

  @Test
  void testResponse() throws AeriusException, JsonProcessingException {
    final SummaryResponse summaryResponse = getSummaryResponse();

    assertTrue(summaryResponse.getSuccessful(), "Should succeed");
    assertEquals(0, summaryResponse.getErrors().size(), "Should be no errors");
    assertEquals(0, summaryResponse.getWarnings().size(), "Should be no warnings");
    assertEquals(1, summaryResponse.getSituations().size(), "Should be exactly one situation");
  }

  @Test
  void testSituation() throws AeriusException, JsonProcessingException {
    final SummaryResponse summaryResponse = getSummaryResponse();
    final SituationSummary situationSummary = summaryResponse.getSituations().get(0);

    assertEquals("PROPOSED", situationSummary.getType(), "Should return situation type");
    assertEquals(SITUATION_YEAR, situationSummary.getYear().intValue(), "Should return year");
  }

  @Test
  void testSourcesSummary() throws AeriusException, JsonProcessingException {
    final SummaryResponse summaryResponse = getSummaryResponse();
    final SituationSummary situationSummary = summaryResponse.getSituations().get(0);
    final SourcesSummary sources = situationSummary.getSources();

    assertEquals(30, sources.getTotalEmissions().get("NH3").intValue(), "Should sum emissions");
    assertEquals(3, sources.getNumSources().intValue(), "Should return number of sources");

    assertEquals(100, sources.getCentroidX().intValue(), "Should return centroid");
    assertEquals(200, sources.getCentroidY().intValue(), "Should return centroid");

    assertEquals(FARM_LODGE_SECTOR_ID, sources.getMainSectorId().intValue(), "Should return main sector id");
    assertEquals(1, sources.getNumSectors().intValue(), "Should return multiple sectors");
  }

  @Test
  void testMetadata() throws AeriusException, JsonProcessingException {
    final SummaryResponse summaryResponse = getSummaryResponse();
    final SituationSummary situationSummary = summaryResponse.getSituations().get(0);

    assertNotNull(situationSummary.getMetadata(), "Should return metadata");
    assertEquals(7, situationSummary.getMetadata().size(), "Should have expected number of metadata fields: reference + ScenarioMetadata fields");
    situationSummary.getMetadata().forEach((key, value) ->
        assertEquals(value, key, "Example metadata has key = value")
    );
  }

  @Test
  void testResults() throws AeriusException, JsonProcessingException {
    final SummaryResponse summaryResponse = getSummaryResponse();
    final SituationSummary situationSummary = summaryResponse.getSituations().get(0);

    assertNotNull(situationSummary.getResults(), "Should return results");
    assertEquals(2, situationSummary.getResults().size(), "Should return number of results");

    final SituationResults firstResult = situationSummary.getResults().get(0);
    assertEquals(100.0, firstResult.getResults().get("NH3").doubleValue(), "Should return NH3 result");
    assertEquals(50.0, firstResult.getResults().get("NOx").doubleValue(), "Should return NOx result");
    assertEquals("DEPOSITION", firstResult.getResultType().name(), "Should return result type");
    assertEquals(12, firstResult.getReceptorId(), "Should return receptor id");
    assertNull(firstResult.getSubPointId(), "Should not return sub point id");

    final SituationResults secondResult = situationSummary.getResults().get(1);
    assertEquals(100.0, secondResult.getResults().get("NH3").doubleValue(), "Should return NH3 result");
    assertEquals(50.0, secondResult.getResults().get("NOx").doubleValue(), "Should return NOx result");
    assertEquals("CONCENTRATION", secondResult.getResultType().name(), "Should return result type");
    assertEquals(18, secondResult.getReceptorId(), "Should return receptor id");
    assertEquals(42, secondResult.getSubPointId(), "Should not return sub point id");
  }
}
