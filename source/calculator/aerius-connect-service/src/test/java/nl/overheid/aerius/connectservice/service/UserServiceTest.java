/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.ConstantRepositoryBean;
import nl.overheid.aerius.db.connect.ConnectUserRepositoryBean;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.domain.calculation.JobProgress;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Unit test for {@link UserService}.
 */
@ExtendWith(MockitoExtension.class)
class UserServiceTest {

  private static final String TEST_EMAIL = "something@example.org";
  private static final String TEST_API_KEY = "Not_too_important";

  @Mock ConstantRepositoryBean constantRepository;
  @Mock ConnectUserRepositoryBean connectUserRepository;
  @Mock JobRepositoryBean jobRepository;

  @Captor ArgumentCaptor<ConnectUser> userCaptor;

  UserService userService;

  @BeforeEach
  void beforeEach() {
    userService = new UserService(constantRepository, connectUserRepository, jobRepository);
  }

  @Test
  void testGenerateAPIKeyExistingUser() throws AeriusException {
    when(constantRepository.getBoolean(ConstantsEnum.CONNECT_GENERATING_APIKEY_ENABLED)).thenReturn(true);
    final ConnectUser mockUser = new ConnectUser();
    when(connectUserRepository.getUserByEmailAddress(any())).thenReturn(mockUser);

    final String apiKey = userService.generateAPIKey(TEST_EMAIL);
    assertNotNull(apiKey, "Returned api key shouldn't be null");

    verify(connectUserRepository).updateApiKeyForUser(mockUser);
  }

  @Test
  void testGenerateAPIKeyNewUser() throws AeriusException {
    when(constantRepository.getBoolean(ConstantsEnum.CONNECT_GENERATING_APIKEY_ENABLED)).thenReturn(true);
    // First return the original value, then return the object that was created.
    when(connectUserRepository.getUserByEmailAddress(any())).thenReturn(null).thenAnswer((email) -> userCaptor.getValue());
    doNothing().when(connectUserRepository).createUser(userCaptor.capture());

    final String apiKey = userService.generateAPIKey(TEST_EMAIL);
    assertNotNull(apiKey, "Returned api key shouldn't be null");
  }

  @Test
  void testGenerateAPIKeyDisabled() throws AeriusException {
    when(constantRepository.getBoolean(ConstantsEnum.CONNECT_GENERATING_APIKEY_ENABLED)).thenReturn(false);

    final AeriusException e = assertThrows(AeriusException.class, () -> userService.generateAPIKey(TEST_EMAIL));
    assertEquals(AeriusExceptionReason.USER_API_KEY_GENERATION_DISABLED, e.getReason(), "Reason should be that api key generation is disabled");
  }

  @Test
  void testGetUserWithoutValidatingJobLimitsUnknown() throws AeriusException {
    when(connectUserRepository.getUserByApiKey(any())).thenReturn(null);

    final AeriusException e = assertThrows(AeriusException.class, () -> userService.getUserWithoutValidatingJobLimits(TEST_API_KEY));
    assertEquals(AeriusExceptionReason.USER_INVALID_API_KEY, e.getReason(), "Reason should be that api key is invalid");
  }

  @Test
  void testGetUserWithoutValidatingJobLimitsDisabled() throws AeriusException {
    final ConnectUser user = mock(ConnectUser.class);
    when(user.isEnabled()).thenReturn(false);
    when(connectUserRepository.getUserByApiKey(TEST_API_KEY)).thenReturn(user);

    final AeriusException e = assertThrows(AeriusException.class, () -> userService.getUserWithoutValidatingJobLimits(TEST_API_KEY));
    assertEquals(AeriusExceptionReason.USER_ACCOUNT_DISABLED, e.getReason(), "Reason should be that user account is disabled");
  }

  @Test
  void testGetUserWithoutValidatingJobLimits() throws AeriusException {
    final ConnectUser user = mock(ConnectUser.class);
    when(user.isEnabled()).thenReturn(true);
    when(connectUserRepository.getUserByApiKey(TEST_API_KEY)).thenReturn(user);

    final ConnectUser returned = userService.getUserWithoutValidatingJobLimits(TEST_API_KEY);
    assertEquals(user, returned, "Correct user should be returned");
    verifyNoInteractions(jobRepository);
  }

  @Test
  void testGetUserWithValidatingJobLimitsUnknown() throws AeriusException {
    when(connectUserRepository.getUserByApiKey(any())).thenReturn(null);

    final AeriusException e = assertThrows(AeriusException.class, () -> userService.getUserWithValidatingJobLimits(TEST_API_KEY));
    assertEquals(AeriusExceptionReason.USER_INVALID_API_KEY, e.getReason(), "Reason should be that api key is invalid");
  }

  @Test
  void testGetUserWithValidatingJobLimitsDisabled() throws AeriusException {
    final ConnectUser user = mock(ConnectUser.class);
    when(user.isEnabled()).thenReturn(false);
    when(connectUserRepository.getUserByApiKey(TEST_API_KEY)).thenReturn(user);

    final AeriusException e = assertThrows(AeriusException.class, () -> userService.getUserWithValidatingJobLimits(TEST_API_KEY));
    assertEquals(AeriusExceptionReason.USER_ACCOUNT_DISABLED, e.getReason(), "Reason should be that user account is disabled");
  }

  @Test
  void testGetUserWithValidatingJobLimitsOverMaxConcurrent() throws AeriusException {
    final ConnectUser user = mock(ConnectUser.class);
    when(user.isEnabled()).thenReturn(true);
    when(user.getMaxConcurrentJobs()).thenReturn(1);
    when(connectUserRepository.getUserByApiKey(TEST_API_KEY)).thenReturn(user);
    final JobProgress jobProgress = mock(JobProgress.class);
    when(jobProgress.getState()).thenReturn(JobState.CALCULATING);
    when(jobRepository.getProgressForUser(user)).thenReturn(List.of(jobProgress));

    final AeriusException e = assertThrows(AeriusException.class, () -> userService.getUserWithValidatingJobLimits(TEST_API_KEY));
    assertEquals(AeriusExceptionReason.USER_MAX_CONCURRENT_JOB_LIMIT_REACHED, e.getReason(),
        "Reason should be that max concurrent job limit is reached");
  }

  @Test
  void testGetUserWithValidatingJobLimitsWithCompletedJob() throws AeriusException {
    final ConnectUser user = mock(ConnectUser.class);
    when(user.isEnabled()).thenReturn(true);
    when(user.getMaxConcurrentJobs()).thenReturn(1);
    when(user.getPeriodJobRateLimit()).thenReturn(10);
    when(connectUserRepository.getUserByApiKey(TEST_API_KEY)).thenReturn(user);
    final JobProgress jobProgress = mock(JobProgress.class);
    when(jobProgress.getState()).thenReturn(JobState.COMPLETED);
    when(jobProgress.getStartDateTime()).thenReturn(new Date());
    when(jobRepository.getProgressForUser(user)).thenReturn(List.of(jobProgress));

    final ConnectUser returned = userService.getUserWithValidatingJobLimits(TEST_API_KEY);
    assertEquals(user, returned, "Correct user should be returned");
    verify(jobRepository).getProgressForUser(user);
  }

  @Test
  void testGetUserWithValidatingJobLimitsPeriodRateLimit() throws AeriusException {
    final ConnectUser user = mock(ConnectUser.class);
    when(user.isEnabled()).thenReturn(true);
    when(user.getMaxConcurrentJobs()).thenReturn(10);
    when(user.getPeriodJobRateLimit()).thenReturn(1);
    when(connectUserRepository.getUserByApiKey(TEST_API_KEY)).thenReturn(user);
    final JobProgress jobProgress = mock(JobProgress.class);
    when(jobProgress.getState()).thenReturn(JobState.COMPLETED);
    when(jobProgress.getStartDateTime()).thenReturn(new Date());
    when(jobRepository.getProgressForUser(user)).thenReturn(List.of(jobProgress));

    final AeriusException e = assertThrows(AeriusException.class, () -> userService.getUserWithValidatingJobLimits(TEST_API_KEY));
    assertEquals(AeriusExceptionReason.USER_PERIOD_JOB_RATE_LIMIT_REACHED, e.getReason(),
        "Reason should be that period job rate limit is reached");
  }

  @Test
  void testGetUserWithValidatingJobLimitsWithOldJob() throws AeriusException {
    final ConnectUser user = mock(ConnectUser.class);
    when(user.isEnabled()).thenReturn(true);
    when(user.getMaxConcurrentJobs()).thenReturn(10);
    when(user.getPeriodJobRateLimit()).thenReturn(1);
    when(connectUserRepository.getUserByApiKey(TEST_API_KEY)).thenReturn(user);
    final JobProgress jobProgress = mock(JobProgress.class);
    when(jobProgress.getState()).thenReturn(JobState.COMPLETED);
    final Date oldDate = Date.from(ZonedDateTime.now().minusDays(2).toInstant());
    when(jobProgress.getStartDateTime()).thenReturn(oldDate);
    when(jobRepository.getProgressForUser(user)).thenReturn(List.of(jobProgress));

    final ConnectUser returned = userService.getUserWithValidatingJobLimits(TEST_API_KEY);
    assertEquals(user, returned, "Correct user should be returned");
    verify(jobRepository).getProgressForUser(user);
  }

  @Test
  void testGetUserWithValidatingJobLimits() throws AeriusException {
    final ConnectUser user = mock(ConnectUser.class);
    when(user.isEnabled()).thenReturn(true);
    when(user.getMaxConcurrentJobs()).thenReturn(1);
    when(user.getPeriodJobRateLimit()).thenReturn(1);
    when(connectUserRepository.getUserByApiKey(TEST_API_KEY)).thenReturn(user);

    final ConnectUser returned = userService.getUserWithValidatingJobLimits(TEST_API_KEY);
    assertEquals(user, returned, "Correct user should be returned");
    verify(jobRepository).getProgressForUser(user);
  }

  @Test
  void testGetUserPresent() throws AeriusException {
    final ConnectUser user = mock(ConnectUser.class);
    when(connectUserRepository.getUserByApiKey(TEST_API_KEY)).thenReturn(user);

    final Optional<ConnectUser> returned = userService.getUser(TEST_API_KEY);
    assertTrue(returned.isPresent(), "A user should be returned");
    assertEquals(user, returned.get(), "Correct user should be returned");
  }

  @Test
  void testGetUserNotPresent() throws AeriusException {
    when(connectUserRepository.getUserByApiKey(TEST_API_KEY)).thenReturn(null);

    final Optional<ConnectUser> returned = userService.getUser(TEST_API_KEY);
    assertTrue(returned.isEmpty(), "No user should be returned");
  }

}
