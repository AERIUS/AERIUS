/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.multipart.MultipartFile;

import nl.overheid.aerius.connectservice.model.Substance;
import nl.overheid.aerius.connectservice.model.UploadFile;
import nl.overheid.aerius.connectservice.model.UploadFile.SituationEnum;
import nl.overheid.aerius.importer.ImportOption;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.importer.ImportProperties;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioMetaData;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

@ExtendWith(MockitoExtension.class)
class ScenarioServiceTest {

  private static final Set<ImportOption> IMPORT_OPTIONS = Set.of(ImportOption.VALIDATE_AGAINST_SCHEMA);

  @Mock ImportService importService;
  @Mock SituationMergingService mergingService;

  ScenarioService scenarioService;

  @BeforeEach
  void beforeEach() {
    scenarioService = new ScenarioService(importService, mergingService);
  }

  @Test
  void testConstructScenarioOwN2000WithErrors() throws AeriusException {
    final Map<UploadFile, MultipartFile> linkedFiles = new HashMap<>();
    final UploadFile uploadFile = mockUploadFile(SituationEnum.PROPOSED);
    final MultipartFile multipartFile = mock(MultipartFile.class);
    linkedFiles.put(uploadFile, multipartFile);
    final ImportParcel importResult = mockImportParcel(true);
    final List<ImportParcel> importResults = List.of(importResult);
    when(importService.importFile(eq(multipartFile), any(), any())).thenReturn(importResults);

    final ScenarioConstruction construction = scenarioService.constructScenario(Theme.OWN2000, linkedFiles, IMPORT_OPTIONS);

    assertFalse(construction.isSuccesful(), "Should succesfully have crafted a scenario");
    assertNotNull(construction.getScenario(), "Scenario will still not be null");
  }

  @Test
  void testConstructScenarioOwN2000WithoutMetadata() throws AeriusException {
    final Map<UploadFile, MultipartFile> linkedFiles = new HashMap<>();
    final UploadFile uploadFile = mockUploadFile(SituationEnum.PROPOSED);
    final MultipartFile multipartFile = mock(MultipartFile.class);
    linkedFiles.put(uploadFile, multipartFile);
    final ImportParcel importResult = mockImportParcel(false);
    final List<ImportParcel> importResults = List.of(importResult);
    when(importService.importFile(eq(multipartFile), any(), any())).thenReturn(importResults);

    final ScenarioConstruction construction = scenarioService.constructScenario(Theme.OWN2000, linkedFiles, IMPORT_OPTIONS);

    assertTrue(construction.isSuccesful(), "Should succesfully have crafted a scenario");
    assertFalse(construction.isMetadataAdded(), "Metadata should be added");
    assertNotNull(construction.getScenario(), "Scenario shouldn't be null");
    assertEquals(1, construction.getScenario().getSituations().size(), "Scenario should be a single scenario");
  }

  @ParameterizedTest
  @MethodSource("singleSituationData")
  void testConstructScenarioOwN2000SingleSituation(final SituationEnum situationEnum, final SituationType expected,
      final Consumer<UploadFile> extraPreparations, final Consumer<ScenarioSituation> extraValidations) throws AeriusException {
    final Map<UploadFile, MultipartFile> linkedFiles = new HashMap<>();
    final UploadFile uploadFile = mockUploadFile(situationEnum);
    extraPreparations.accept(uploadFile);
    final MultipartFile multipartFile = mock(MultipartFile.class);
    linkedFiles.put(uploadFile, multipartFile);
    final ImportParcel importResult = mockImportParcel(false);
    final ScenarioMetaData metadata = mock(ScenarioMetaData.class);
    when(importResult.getImportedMetaData()).thenReturn(metadata);
    final List<ImportParcel> importResults = List.of(importResult);
    when(importService.importFile(eq(multipartFile), any(), any())).thenReturn(importResults);

    final ScenarioConstruction construction = scenarioService.constructScenario(Theme.OWN2000, linkedFiles, IMPORT_OPTIONS);

    assertTrue(construction.isSuccesful(), "Should succesfully have crafted a scenario");
    assertTrue(construction.isMetadataAdded(), "Metadata should be added");
    assertNotNull(construction.getScenario(), "Scenario shouldn't be null");
    assertEquals(1, construction.getScenario().getSituations().size(), "Scenario should be a single scenario");
    final ScenarioSituation createdSituation = construction.getScenario().getSituations().get(0);
    assertEquals(expected, createdSituation.getType(), "Scenario should be correct type");
    extraValidations.accept(createdSituation);
  }

  private static List<Arguments> singleSituationData() {
    return List.of(
        Arguments.of(SituationEnum.PROPOSED, SituationType.PROPOSED, defaultPreparation(), defaultValidations()),
        Arguments.of(SituationEnum.REFERENCE, SituationType.REFERENCE, defaultPreparation(), defaultValidations()),
        Arguments.of(SituationEnum.TEMPORARY, SituationType.TEMPORARY, defaultPreparation(), defaultValidations()),
        Arguments.of(SituationEnum.OFF_SITE_REDUCTION, SituationType.OFF_SITE_REDUCTION, nettingPreparation(null), nettingValidations(0.3)),
        Arguments.of(SituationEnum.OFF_SITE_REDUCTION, SituationType.OFF_SITE_REDUCTION, nettingPreparation(4.2), nettingValidations(4.2)));
  }

  private static Consumer<UploadFile> defaultPreparation() {
    return new Consumer<UploadFile>() {

      @Override
      public void accept(final UploadFile t) {
      }
    };
  }

  private static Consumer<ScenarioSituation> defaultValidations() {
    return new Consumer<ScenarioSituation>() {

      @Override
      public void accept(final ScenarioSituation t) {
      }
    };
  }

  private static Consumer<UploadFile> nettingPreparation(final Double nettingFactor) {
    return new Consumer<UploadFile>() {

      @Override
      public void accept(final UploadFile t) {
        when(t.getNettingFactor()).thenReturn(nettingFactor);
      }
    };
  }

  private static Consumer<ScenarioSituation> nettingValidations(final double expectedNettingFactor) {
    return new Consumer<ScenarioSituation>() {

      @Override
      public void accept(final ScenarioSituation t) {
        assertEquals(expectedNettingFactor, t.getNettingFactor(), "Expected netting factor");
      }
    };
  }

  @Test
  void testConstructScenarioOwN2000WithCalculationPoints() throws AeriusException {
    final Map<UploadFile, MultipartFile> linkedFiles = new HashMap<>();
    final UploadFile uploadFile = mockUploadFile(SituationEnum.REFERENCE);
    final MultipartFile multipartFile = mock(MultipartFile.class);
    linkedFiles.put(uploadFile, multipartFile);
    final ImportParcel importResult = mockImportParcel(false);
    final ScenarioMetaData metadata = mock(ScenarioMetaData.class);
    when(importResult.getImportedMetaData()).thenReturn(metadata);
    final List<CalculationPointFeature> calculationPoints = mockCalculationPointList();
    when(importResult.getCalculationPointsList()).thenReturn(calculationPoints);
    final List<ImportParcel> importResults = List.of(importResult);
    when(importService.importFile(eq(multipartFile), any(), any())).thenReturn(importResults);

    final ScenarioConstruction construction = scenarioService.constructScenario(Theme.OWN2000, linkedFiles, IMPORT_OPTIONS);

    assertTrue(construction.isSuccesful(), "Should succesfully have crafted a scenario");
    assertTrue(construction.isMetadataAdded(), "Metadata should be added");
    assertNotNull(construction.getScenario(), "Scenario shouldn't be null");
    assertEquals(1, construction.getScenario().getSituations().size(), "Scenario should be a single scenario");
    assertEquals(1, construction.getScenario().getCustomPointsList().size(), "Scenario should contain the calculation points");
  }

  @Test
  void testConstructScenarioOwN2000Comparison() throws AeriusException {
    final Map<UploadFile, MultipartFile> linkedFiles = new HashMap<>();

    final UploadFile uploadFileCurrent = mockUploadFile(SituationEnum.REFERENCE);
    final MultipartFile multipartFileCurrent = mock(MultipartFile.class);
    linkedFiles.put(uploadFileCurrent, multipartFileCurrent);
    final ImportParcel importResultCurrent = mockImportParcel(false);
    when(importService.importFile(eq(multipartFileCurrent), any(), any())).thenReturn(List.of(importResultCurrent));

    final UploadFile uploadFileProposed = mockUploadFile(SituationEnum.PROPOSED);
    final MultipartFile multipartFileProposed = mock(MultipartFile.class);
    linkedFiles.put(uploadFileProposed, multipartFileProposed);
    final ImportParcel importResultProposed = mockImportParcel(false);
    final ScenarioMetaData metadata = mock(ScenarioMetaData.class);
    when(importResultProposed.getImportedMetaData()).thenReturn(metadata);
    when(importService.importFile(eq(multipartFileProposed), any(), any())).thenReturn(List.of(importResultProposed));

    final ScenarioConstruction construction = scenarioService.constructScenario(Theme.OWN2000, linkedFiles, IMPORT_OPTIONS);

    assertTrue(construction.isSuccesful(), "Should succesfully have crafted a scenario");
    assertTrue(construction.isMetadataAdded(), "Metadata should be added");
    assertNotNull(construction.getScenario(), "Scenario shouldn't be null");
    assertEquals(2, construction.getScenario().getSituations().size(), "Scenario should be a comparison scenario");
    for (final ScenarioSituation situation : construction.getScenario().getSituations()) {
      assertEquals(0, situation.getYear(), "Should have the default year (0)");
    }
  }

  @Test
  void testConstructScenarioOwN2000ComparisonWithYears() throws AeriusException {
    final Map<UploadFile, MultipartFile> linkedFiles = new HashMap<>();

    final UploadFile uploadFileCurrent = mockUploadFile(SituationEnum.REFERENCE);
    when(uploadFileCurrent.getCalculationYear()).thenReturn(2020);
    final MultipartFile multipartFileCurrent = mock(MultipartFile.class);
    linkedFiles.put(uploadFileCurrent, multipartFileCurrent);
    final ImportParcel importResultCurrent = mockImportParcel(false);
    when(importService.importFile(eq(multipartFileCurrent), any(), any())).thenReturn(List.of(importResultCurrent));

    final UploadFile uploadFileProposed = mockUploadFile(SituationEnum.PROPOSED);
    when(uploadFileProposed.getCalculationYear()).thenReturn(2030);
    final MultipartFile multipartFileProposed = mock(MultipartFile.class);
    linkedFiles.put(uploadFileProposed, multipartFileProposed);
    final ImportParcel importResultProposed = mockImportParcel(false);
    final ScenarioMetaData metadata = mock(ScenarioMetaData.class);
    when(importResultProposed.getImportedMetaData()).thenReturn(metadata);
    when(importService.importFile(eq(multipartFileProposed), any(), any())).thenReturn(List.of(importResultProposed));

    final ScenarioConstruction construction = scenarioService.constructScenario(Theme.OWN2000, linkedFiles, IMPORT_OPTIONS);

    assertTrue(construction.isSuccesful(), "Should succesfully have crafted a scenario");
    assertTrue(construction.isMetadataAdded(), "Metadata should be added");
    assertNotNull(construction.getScenario(), "Scenario shouldn't be null");
    assertEquals(2, construction.getScenario().getSituations().size(), "Scenario should be a comparison scenario");
    for (final ScenarioSituation situation : construction.getScenario().getSituations()) {
      if (situation.getType() == SituationType.REFERENCE) {
        assertEquals(2020, situation.getYear(), "Reference should have correct year");
      } else if (situation.getType() == SituationType.PROPOSED) {
        assertEquals(2030, situation.getYear(), "Reference should have correct year");
      } else {
        fail("Unexpected situation type: " + situation.getType());
      }
    }
  }

  @Test
  void testConstructScenarioOwN2000CurrentAndProposedTooFewSourceLists() throws AeriusException {
    final Map<UploadFile, MultipartFile> linkedFiles = new HashMap<>();

    final UploadFile uploadFile = mockUploadFile(SituationEnum.REFERENCE_AND_PROPOSED_ZIP);
    final MultipartFile multipartFile = mock(MultipartFile.class);
    linkedFiles.put(uploadFile, multipartFile);
    final List<ImportParcel> importResults = mockImportParcels(1, "current_and_proposed");
    when(importService.importFile(eq(multipartFile), any(), any())).thenReturn(importResults);

    final ScenarioConstruction construction = scenarioService.constructScenario(Theme.OWN2000, linkedFiles, IMPORT_OPTIONS);

    assertFalse(construction.isSuccesful(), "Should fail to create scenario");
    assertEquals(1, construction.getErrors().size(), "Number of errors");
    assertEquals(AeriusExceptionReason.ZIP_WITHOUT_USABLE_FILES, construction.getErrors().get(0).getReason(), "Reason should indicate too few files");
  }

  @Test
  void testConstructScenarioOwN2000CurrentAndProposedTooManySources() throws AeriusException {
    final Map<UploadFile, MultipartFile> linkedFiles = new HashMap<>();

    final UploadFile uploadFile = mockUploadFile(SituationEnum.REFERENCE_AND_PROPOSED_ZIP);
    when(uploadFile.getSubstance()).thenReturn(Substance.NOX);
    final MultipartFile multipartFile = mock(MultipartFile.class);
    linkedFiles.put(uploadFile, multipartFile);
    final List<ImportParcel> importResults = mockImportParcels(3, "current_and_proposed");
    final ArgumentCaptor<ImportProperties> propertiesCaptor = ArgumentCaptor.forClass(ImportProperties.class);
    when(importService.importFile(eq(multipartFile), any(), propertiesCaptor.capture()))
        .thenReturn(importResults);

    final ScenarioConstruction construction = scenarioService.constructScenario(Theme.OWN2000, linkedFiles, IMPORT_OPTIONS);

    assertFalse(construction.isSuccesful(), "Should fail to create scenario");
    assertEquals(1, construction.getErrors().size(), "Number of errors");
    assertEquals(AeriusExceptionReason.ZIP_TOO_MANY_USABLE_FILES_ERROR, construction.getErrors().get(0).getReason(),
        "Reason should indicate too many files");
    assertEquals(nl.overheid.aerius.shared.domain.Substance.NOX, propertiesCaptor.getValue().getSubstance(),
        "Substance should be provided when importing");
  }

  @Test
  void testConstructScenarioOwN2000CurrentAndProposed() throws AeriusException {
    final Map<UploadFile, MultipartFile> linkedFiles = new HashMap<>();

    final UploadFile uploadFile = mockUploadFile(SituationEnum.REFERENCE_AND_PROPOSED_ZIP);
    final MultipartFile multipartFile = mock(MultipartFile.class);
    linkedFiles.put(uploadFile, multipartFile);
    final ScenarioMetaData metadata = mock(ScenarioMetaData.class);
    final List<ImportParcel> importResults = mockImportParcels(2, "current_and_proposed");
    when(importResults.get(0).getImportedMetaData()).thenReturn(metadata);
    when(importService.importFile(eq(multipartFile), any(), any())).thenReturn(importResults);

    final ScenarioConstruction construction = scenarioService.constructScenario(Theme.OWN2000, linkedFiles, IMPORT_OPTIONS);

    assertTrue(construction.isSuccesful(), "Should succesfully have crafted a scenario");
    assertTrue(construction.isMetadataAdded(), "Metadata should be added");
    assertNotNull(construction.getScenario(), "Scenario shouldn't be null");
    assertEquals(2, construction.getScenario().getSituations().size(), "Scenario should be a comparison scenario");
  }

  @Test
  void testConstructScenarioOwN2000OnlyAll() throws AeriusException {
    final Map<UploadFile, MultipartFile> linkedFiles = new HashMap<>();

    final UploadFile uploadFile = mockUploadFile(SituationEnum.ALL);
    final MultipartFile multipartFile = mock(MultipartFile.class);
    linkedFiles.put(uploadFile, multipartFile);
    final ImportParcel importResult = mockImportParcel(false);
    final ScenarioMetaData metadata = mock(ScenarioMetaData.class);
    when(importResult.getImportedMetaData()).thenReturn(metadata);
    final List<ImportParcel> importResults = List.of(importResult);
    when(importService.importFile(eq(multipartFile), any(), any())).thenReturn(importResults);

    final ScenarioConstruction construction = scenarioService.constructScenario(Theme.OWN2000, linkedFiles, IMPORT_OPTIONS);

    assertTrue(construction.isSuccesful(), "Should succesfully have crafted a scenario");
    assertTrue(construction.isMetadataAdded(), "Metadata should be added");
    assertNotNull(construction.getScenario(), "Scenario shouldn't be null");
    assertEquals(1, construction.getScenario().getSituations().size(), "Scenario should be a single scenario");
  }

  @Test
  void testConstructScenarioOwN2000() throws AeriusException {
    final Map<UploadFile, MultipartFile> linkedFiles = new HashMap<>();

    final UploadFile uploadFileCurrent = mockUploadFile(SituationEnum.REFERENCE);
    final MultipartFile multipartFileCurrent = mock(MultipartFile.class);
    linkedFiles.put(uploadFileCurrent, multipartFileCurrent);
    final List<ImportParcel> importResultsCurrent = mockImportParcels(4, "current");
    when(importService.importFile(eq(multipartFileCurrent), any(), any())).thenReturn(importResultsCurrent);

    final UploadFile uploadFileProposed = mockUploadFile(SituationEnum.PROPOSED);
    final MultipartFile multipartFileProposed = mock(MultipartFile.class);
    linkedFiles.put(uploadFileProposed, multipartFileProposed);
    final List<ImportParcel> importResultsProposed = mockImportParcels(8, "proposed");
    when(importService.importFile(eq(multipartFileProposed), any(), any())).thenReturn(importResultsProposed);

    final UploadFile uploadFileAll = mockUploadFile(SituationEnum.ALL);
    final MultipartFile multipartFileAll = mock(MultipartFile.class);
    linkedFiles.put(uploadFileAll, multipartFileAll);
    final List<ImportParcel> importResultsAll = mockImportParcels(3, "all");
    when(importService.importFile(eq(multipartFileAll), any(), any())).thenReturn(importResultsAll);

    final UploadFile uploadFileCurrentAndProposed = mockUploadFile(SituationEnum.REFERENCE_AND_PROPOSED_ZIP);
    final MultipartFile multipartFileCurrentAndProposed = mock(MultipartFile.class);
    linkedFiles.put(uploadFileCurrentAndProposed, multipartFileCurrentAndProposed);
    final ScenarioMetaData metadata = mock(ScenarioMetaData.class);
    final List<ImportParcel> importResultsCurrentAndProposed = mockImportParcels(2, "current_and_proposed");
    when(importResultsCurrentAndProposed.get(0).getImportedMetaData()).thenReturn(metadata);
    when(importService.importFile(eq(multipartFileCurrentAndProposed), any(), any())).thenReturn(importResultsCurrentAndProposed);

    final ScenarioConstruction construction = scenarioService.constructScenario(Theme.OWN2000, linkedFiles, IMPORT_OPTIONS);

    assertTrue(construction.isSuccesful(), "Should succesfully have crafted a scenario");
    assertTrue(construction.isMetadataAdded(), "Metadata should be added");
    assertNotNull(construction.getScenario(), "Scenario shouldn't be null");
    assertEquals(2, construction.getScenario().getSituations().size(), "Scenario should be a comparison scenario");
    final Optional<ScenarioSituation> situationCurrent = construction.getScenario().getSituations().stream()
        .filter(situation -> situation.getType() == SituationType.REFERENCE)
        .findFirst();
    final Optional<ScenarioSituation> situationProposed = construction.getScenario().getSituations().stream()
        .filter(situation -> situation.getType() == SituationType.PROPOSED)
        .findFirst();
    assertTrue(situationCurrent.isPresent(), "Should have a current situation");
    assertTrue(situationProposed.isPresent(), "Should have a proposed situation");
    final ArgumentCaptor<ScenarioSituation> mergedIntoCaptor = ArgumentCaptor.forClass(ScenarioSituation.class);
    final ArgumentCaptor<ScenarioSituation> mergedFromCaptor = ArgumentCaptor.forClass(ScenarioSituation.class);
    // Times the merging should be called:
    // 4 current + 3 all + 1 current_and_proposed = 8 times for current situation
    // 8 proposed + 3 all + 1 current_and_proposed = 12 times for proposed situation
    verify(mergingService, times(20)).merge(mergedIntoCaptor.capture(), mergedFromCaptor.capture());
    final List<ScenarioSituation> mergedIntos = mergedIntoCaptor.getAllValues();
    final List<ScenarioSituation> mergedFroms = mergedFromCaptor.getAllValues();
    assertEquals(20, mergedIntos.size(), "Should have captured mergedInto 20 times");
    assertEquals(20, mergedFroms.size(), "Should have captured mergedFrom 20 times");

    // Check if all CURRENT situations were merged into the same situation
    int foundSituations = 0;
    ScenarioSituation mergedIntoCurrent = null;
    for (final ImportParcel parcel : importResultsCurrent) {
      assertTrue(mergedFroms.contains(parcel.getSituation()), "Current situation should be added");
      final ScenarioSituation correspondingMergedInto = mergedIntos.get(mergedFroms.indexOf(parcel.getSituation()));
      if (mergedIntoCurrent == null) {
        mergedIntoCurrent = correspondingMergedInto;
        foundSituations++;
      } else {
        assertEquals(mergedIntoCurrent, correspondingMergedInto, "Should be added to the same situation");
        foundSituations++;
      }
    }
    // Check if all PROPOSED situations were merged into the same situation
    ScenarioSituation mergedIntoProposed = null;
    for (final ImportParcel parcel : importResultsProposed) {
      assertTrue(mergedFroms.contains(parcel.getSituation()), "Proposed situation should be added");
      final ScenarioSituation correspondingMergedInto = mergedIntos.get(mergedFroms.indexOf(parcel.getSituation()));
      if (mergedIntoProposed == null) {
        mergedIntoProposed = correspondingMergedInto;
        foundSituations++;
      } else {
        assertEquals(mergedIntoProposed, correspondingMergedInto, "Should be added to the same situation");
        foundSituations++;
      }
    }
    // Check if all ALL situations were merged into both the current and the proposed.
    for (final ImportParcel parcel : importResultsAll) {
      boolean inCurrent = false;
      boolean inProposed = false;
      for (int i = 0; i < mergedFroms.size(); i++) {
        if (mergedFroms.get(i).equals(parcel.getSituation())) {
          if (mergedIntos.get(i).equals(mergedIntoCurrent)) {
            inCurrent = true;
            foundSituations++;
          } else if (mergedIntos.get(i).equals(mergedIntoProposed)) {
            inProposed = true;
            foundSituations++;
          } else {
            fail("Found an ALL situation merged into an unexpected situation");
          }
        }
      }
      assertTrue(inCurrent, "Did not find an ALL situation merged into current situation");
      assertTrue(inProposed, "Did not find an ALL situation merged into proposed situation");
    }
    // Check if the CURRENT_AND_PROPOSED situations were merged into the correct current and proposed situation.
    assertEquals(mergedIntoCurrent, mergedIntos.get(mergedFroms.indexOf(importResultsCurrentAndProposed.get(0).getSituation())),
        "Did not find the current situation from CURRENT_AND_PROPOSED merged into current situation");
    foundSituations++;
    assertEquals(mergedIntoProposed, mergedIntos.get(mergedFroms.indexOf(importResultsCurrentAndProposed.get(1).getSituation())),
        "Did not find the proposed situation from CURRENT_AND_PROPOSED merged into proposed situation");
    foundSituations++;
    assertEquals(20, foundSituations, "Sanity check to see if we did actually check all situations");
  }

  private UploadFile mockUploadFile(final SituationEnum situation) {
    final UploadFile uploadFile = mock(UploadFile.class);
    when(uploadFile.getSituation()).thenReturn(situation);
    return uploadFile;
  }

  private ImportParcel mockImportParcel(final boolean hasErrors) {
    final ImportParcel importResult = mock(ImportParcel.class);
    final ArrayList<AeriusException> errors = new ArrayList<>(hasErrors ? List.of(mock(AeriusException.class)) : List.of());
    when(importResult.getExceptions()).thenReturn(errors);
    final ScenarioSituation situation = mockSituation("");
    lenient().when(importResult.getSituation()).thenReturn(situation);
    return importResult;
  }

  private List<ImportParcel> mockImportParcels(final int numberOfParcels, final String name) {
    final List<ImportParcel> parcels = new ArrayList<>();
    for (int i = 0; i < numberOfParcels; i++) {
      final ImportParcel parcel = mockImportParcel(false);
      final ScenarioSituation situation = mockSituation(name + " " + i);
      lenient().when(parcel.getSituation()).thenReturn(situation);
      parcels.add(parcel);
    }
    return parcels;
  }

  private ScenarioSituation mockSituation(final String name) {
    // Not really mocking in this case, but using an actual situation
    final ScenarioSituation situation = new ScenarioSituation();
    situation.setName(name);
    situation.getEmissionSourcesList().addAll(mockEmissionSourceList());
    return situation;
  }

  private List<EmissionSourceFeature> mockEmissionSourceList() {
    final List<EmissionSourceFeature> list = new ArrayList<>();
    list.add(mock(EmissionSourceFeature.class));
    return list;
  }

  private List<CalculationPointFeature> mockCalculationPointList() {
    return List.of(mock(CalculationPointFeature.class));
  }

}
