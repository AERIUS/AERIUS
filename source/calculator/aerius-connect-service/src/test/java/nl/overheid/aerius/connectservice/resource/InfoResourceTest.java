/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.resource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;

import java.sql.SQLException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.AeriusVersion;
import nl.overheid.aerius.connectservice.model.VersionInfoResponse;
import nl.overheid.aerius.connectservice.service.ResponseService;
import nl.overheid.aerius.db.PMF;

/**
 * Test class for {@link InfoResource}.
 */
@ExtendWith(MockitoExtension.class)
class InfoResourceTest {

  private @Mock ResponseService responseService;
  private @Mock PMF pmf;
  private @Captor ArgumentCaptor<VersionInfoResponse> infoCapture;

  @Test
  void testGet() throws SQLException {
    doReturn("123").when(pmf).getDatabaseVersion();
    doReturn(null).when(responseService).toOkResponse(infoCapture.capture());
    final InfoResource ir = new InfoResource(responseService, pmf);
    ir.getInfoVersion();

    final VersionInfoResponse versionInfo = infoCapture.getValue();
    assertEquals(AeriusVersion.getVersionNumber(), versionInfo.getAeriusVersion(), "Returned expected AERIUS version");
    assertEquals("123", versionInfo.getDatabaseVersion(), "Returned expected database version");
  }
}
