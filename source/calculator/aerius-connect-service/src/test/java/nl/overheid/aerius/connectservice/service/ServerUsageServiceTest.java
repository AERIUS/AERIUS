/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.ServerUsage;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 *
 */
class ServerUsageServiceTest {

  ServerUsageService serverUsageService;

  @BeforeEach
  void beforeEach() {
    serverUsageService = new ServerUsageService();
  }

  @Test
  void testInitial() {
    assertEquals(ServerUsage.UNKNOWN, serverUsageService.getCurrentServerUsage(), "Should start with unknown state");
  }

  @Test
  void testUpdateWorkersNoneUsed() {
    final int numberOfWorkers = 10;
    final int workersInUse = 0;
    serverUsageService.updateWorkers(WorkerType.CONNECT.type().getWorkerQueueName(), numberOfWorkers, workersInUse);
    assertEquals(ServerUsage.NORMAL, serverUsageService.getCurrentServerUsage(), "Should be normal if nothing is in use");
  }

  @Test
  void testUpdateWorkersNormal() {
    final int numberOfWorkers = 10;
    final int workersInUse = 5;
    serverUsageService.updateWorkers(WorkerType.CONNECT.type().getWorkerQueueName(), numberOfWorkers, workersInUse);
    assertEquals(ServerUsage.NORMAL, serverUsageService.getCurrentServerUsage(), "Should be normal with some in use");
  }

  @Test
  void testUpdateWorkersAboveAverage() {
    final int numberOfWorkers = 10;
    final int workersInUse = 8;
    serverUsageService.updateWorkers(WorkerType.CONNECT.type().getWorkerQueueName(), numberOfWorkers, workersInUse);
    assertEquals(ServerUsage.ABOVE_AVERAGE, serverUsageService.getCurrentServerUsage(), "Should be above average with 80% in use");
  }

  @Test
  void testUpdateWorkersVeryHigh() {
    final int numberOfWorkers = 10;
    final int workersInUse = 9;
    serverUsageService.updateWorkers(WorkerType.CONNECT.type().getWorkerQueueName(), numberOfWorkers, workersInUse);
    assertEquals(ServerUsage.VERY_HIGH, serverUsageService.getCurrentServerUsage(), "Should be very high with 90% in use");
  }

  @Test
  void testUpdateWorkersZeroSize() {
    final int numberOfWorkers = 0;
    final int workersInUse = 0;
    serverUsageService.updateWorkers(WorkerType.CONNECT.type().getWorkerQueueName(), numberOfWorkers, workersInUse);
    assertEquals(ServerUsage.UNKNOWN, serverUsageService.getCurrentServerUsage(), "If 0 workers available we don't really know the usage");
  }

  @Test
  void testUpdateWorkersDifferentQueue() {
    final int numberOfWorkers =  10;
    final int workersInUse = 5;
    serverUsageService.updateWorkers(WorkerType.MESSAGE.type().getWorkerQueueName(), numberOfWorkers, workersInUse);
    assertEquals(ServerUsage.UNKNOWN, serverUsageService.getCurrentServerUsage(), "Messages for other queues don't matter to us");
  }

}
