/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.resource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import nl.overheid.aerius.connectservice.auth.AuthenticationService;
import nl.overheid.aerius.connectservice.model.CalculateResponse;
import nl.overheid.aerius.connectservice.model.OwN2000CalculationOptions;
import nl.overheid.aerius.connectservice.model.UploadFile;
import nl.overheid.aerius.connectservice.model.ValidationMessage;
import nl.overheid.aerius.connectservice.service.ObjectValidatorService;
import nl.overheid.aerius.connectservice.service.ResponseService;
import nl.overheid.aerius.connectservice.service.ScenarioConstruction;
import nl.overheid.aerius.connectservice.service.ScenarioService;
import nl.overheid.aerius.connectservice.service.OwN2000CalculateService;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

@ExtendWith(MockitoExtension.class)
class CalculateResourceTest {

  private static final String CORRECT_FILE_NAME = "original_filename.notreally";
  private static final String TEST_JOB_KEY = "key_to_the_safe";

  @Mock AuthenticationService authenticationService;
  @Mock ResponseService responseService;
  @Mock OwN2000CalculateService owN2000CalculateService;
  @Mock ScenarioService scenarioService;
  @Mock ObjectValidatorService validatorService;

  @Mock ConnectUser connectUser;
  @Mock ResponseEntity<CalculateResponse> mockResponse;

  @Captor ArgumentCaptor<AeriusException> exceptionCaptor;
  @Captor ArgumentCaptor<CalculateResponse> calculateResponseCaptor;

  OwN2000CalculateResource calculateResource;

  @BeforeEach
  void beforeEach() {
    calculateResource = new OwN2000CalculateResource(authenticationService, responseService, scenarioService, validatorService, owN2000CalculateService);
  }

  @Test
  void testCalculateOwN2000NonMatchingFilesWithUploadedFiles() throws AeriusException {
    final UploadFile uploadFile = mock(UploadFile.class);
    final List<UploadFile> uploadFiles = List.of(uploadFile);
    final MultipartFile multipartFile = mock(MultipartFile.class);
    final List<MultipartFile> multipartFiles = List.of(multipartFile, multipartFile);

    assertFileValidation(uploadFiles, multipartFiles, AeriusExceptionReason.IMPORT_FILE_NOT_SUPPLIED);
  }

  @Test
  void testCalculateOwN2000DuplicateFiles() throws AeriusException {
    final UploadFile uploadFile = mockUploadFile(CORRECT_FILE_NAME);
    final List<UploadFile> uploadFiles = List.of(uploadFile, uploadFile);
    final MultipartFile multipartFile = mock(MultipartFile.class);
    final List<MultipartFile> multipartFiles = List.of(multipartFile, multipartFile);

    assertFileValidation(uploadFiles, multipartFiles, AeriusExceptionReason.IMPORTED_FILE_DUPLICATES);
  }

  @Test
  void testCalculateOwN2000FileMismatch() throws AeriusException {
    final UploadFile uploadFile = mockUploadFile("SomeOtherName");
    final MultipartFile multipartFile = mockMultipartFile(CORRECT_FILE_NAME);
    final List<UploadFile> uploadFiles = List.of(uploadFile);
    final List<MultipartFile> multipartFiles = List.of(multipartFile);

    assertFileValidation(uploadFiles, multipartFiles, AeriusExceptionReason.IMPORT_FILE_NOT_SUPPLIED);
  }

  private void assertFileValidation(final List<UploadFile> uploadFiles, final List<MultipartFile> multipartFiles, final Reason reason)
      throws AeriusException {
    final OwN2000CalculationOptions options = mock(OwN2000CalculationOptions.class);

    when(authenticationService.getCurrentUserWithValidatingJobLimits()).thenReturn(connectUser);

    final ResponseStatusException mockException = mock(ResponseStatusException.class);
    when(responseService.toResponseStatusException(exceptionCaptor.capture())).thenReturn(mockException);

    final ResponseStatusException thrownException = assertThrows(ResponseStatusException.class,
        () -> calculateResource.calculateOwn2000(options, uploadFiles, multipartFiles), "Expected the service to throw a ResponseStatusException");

    assertEquals(mockException, thrownException, "Thrown exception should come from response service");
    final AeriusException capturedException = exceptionCaptor.getValue();
    assertEquals(reason, capturedException.getReason(),
        "Reason should be that an import file is not supplied");
  }

  @SuppressWarnings("unchecked")
  @Test
  void testCalculateOwN2000FailedConstruction() throws AeriusException {
    final OwN2000CalculationOptions options = mock(OwN2000CalculationOptions.class);
    final UploadFile uploadFile = mockUploadFile(CORRECT_FILE_NAME);
    final MultipartFile multipartFile = mockMultipartFile(CORRECT_FILE_NAME);

    when(authenticationService.getCurrentUserWithValidatingJobLimits()).thenReturn(connectUser);
    final ScenarioConstruction constructedScenario = mock(ScenarioConstruction.class);
    when(constructedScenario.isSuccesful()).thenReturn(false);
    final List<AeriusException> errors = List.of(mock(AeriusException.class), mock(AeriusException.class));
    final List<AeriusException> warnings = List.of(mock(AeriusException.class));
    when(constructedScenario.getErrors()).thenReturn(errors);
    when(constructedScenario.getWarnings()).thenReturn(warnings);
    when(scenarioService.constructScenario(same(Theme.OWN2000), any(), any())).thenReturn(constructedScenario);

    when(responseService.toOkResponse(calculateResponseCaptor.capture())).thenReturn(mockResponse);
    when(responseService.convertToValidation(any(List.class))).thenAnswer((invocation) -> {
      final List<AeriusException> exceptions = (List<AeriusException>) invocation.getArgument(0);
      return exceptions.stream().map(e -> mock(ValidationMessage.class)).collect(Collectors.toList());
    });

    final ResponseEntity<CalculateResponse> response = calculateResource.calculateOwn2000(options, List.of(uploadFile), List.of(multipartFile));

    assertEquals(mockResponse, response, "Returned response should come from responseService");
    final CalculateResponse capturedResponse = calculateResponseCaptor.getValue();
    assertNotNull(capturedResponse, "Content of response shouldn't be null");
    assertFalse(capturedResponse.getSuccessful(), "It should have been succesful");
    assertEquals(errors.size(), capturedResponse.getErrors().size(), "Errors should be same size");
    assertEquals(warnings.size(), capturedResponse.getWarnings().size(), "Warnings should be same size");

    verify(owN2000CalculateService, times(1)).validateInput(any());
    verify(owN2000CalculateService, never()).calculate(any(), any(), any());
  }

  @Test
  void testCalculateOwN2000() throws AeriusException {
    final OwN2000CalculationOptions options = mock(OwN2000CalculationOptions.class);
    final UploadFile uploadFile = mockUploadFile(CORRECT_FILE_NAME);
    final MultipartFile multipartFile = mockMultipartFile(CORRECT_FILE_NAME);

    when(authenticationService.getCurrentUserWithValidatingJobLimits()).thenReturn(connectUser);
    final ScenarioConstruction constructedScenario = mock(ScenarioConstruction.class);
    when(constructedScenario.isSuccesful()).thenReturn(true);
    when(scenarioService.constructScenario(same(Theme.OWN2000), any(), any())).thenReturn(constructedScenario);

    when(owN2000CalculateService.calculate(any(), any(), any())).thenReturn(TEST_JOB_KEY);

    when(responseService.toOkResponse(calculateResponseCaptor.capture())).thenReturn(mockResponse);

    final ResponseEntity<CalculateResponse> response = calculateResource.calculateOwn2000(options, List.of(uploadFile), List.of(multipartFile));

    assertEquals(mockResponse, response, "Returned response should come from responseService");
    final CalculateResponse capturedResponse = calculateResponseCaptor.getValue();
    assertNotNull(capturedResponse, "Content of response shouldn't be null");
    assertTrue(capturedResponse.getSuccessful(), "It should have been succesful");
    assertEquals(TEST_JOB_KEY, capturedResponse.getJobKey(), "Should return the same job key");
  }

  private static UploadFile mockUploadFile(final String filename) {
    final UploadFile uploadFile = mock(UploadFile.class);

    when(uploadFile.getFileName()).thenReturn(filename);
    return uploadFile;
  }

  private static MultipartFile mockMultipartFile(final String filename) {
    final MultipartFile multipartFile = mock(MultipartFile.class);

    when(multipartFile.getOriginalFilename()).thenReturn(filename);
    return multipartFile;
  }
}
