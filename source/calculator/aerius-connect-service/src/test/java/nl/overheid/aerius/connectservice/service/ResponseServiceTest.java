/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import nl.overheid.aerius.connectservice.model.ValidationMessage;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

@ExtendWith(MockitoExtension.class)
class ResponseServiceTest {

  private static final String NOT_PRESENT = "wasn't present";
  private static final String TYPICAL_ERROR_MESSAGE = "not informative";

  @Mock MessagesService messagesService;

  ResponseService responseService;

  @BeforeEach
  void beforeEach() {
    responseService = new ResponseService(messagesService);
  }

  @Test
  void testToOkResponseVoid() {
    final ResponseEntity<Void> result = responseService.toOkResponse();

    assertEquals(HttpStatus.OK, result.getStatusCode(), "Status should be OK");
    assertNull(result.getBody(), "Body should be the same as supplied");
  }

  @Test
  void testToOkResponse() {
    final String response = "Something";

    final ResponseEntity<String> result = responseService.toOkResponse(response);

    assertEquals(HttpStatus.OK, result.getStatusCode(), "Status should be OK");
    assertEquals(response, result.getBody(), "Body should be the same as supplied");
  }

  @Test
  void testToOkResponseList() {
    final List<String> response = List.of("Something", "other", "dubadooo");

    final ResponseEntity<List<String>> result = responseService.toOkResponse(response);

    assertEquals(HttpStatus.OK, result.getStatusCode(), "Status should be OK");
    assertEquals(response, result.getBody(), "Body should be the same as supplied");
  }

  @Test
  void testToOkResponseOptionalPresent() {
    final String response = "Something";

    final ResponseEntity<String> result = responseService.toOkResponseOptional(Optional.of(response), NOT_PRESENT);

    assertEquals(HttpStatus.OK, result.getStatusCode(), "Status should be OK");
    assertEquals(response, result.getBody(), "Body should be the same as supplied");
  }

  @Test
  void testToOkResponseOptionalNotPresent() {
    final Optional<String> response = Optional.empty();

    final ResponseStatusException e = assertThrows(ResponseStatusException.class,
        () -> responseService.toOkResponseOptional(response, NOT_PRESENT));

    assertEquals(HttpStatus.NOT_FOUND, e.getStatus(), "Http status when not present");
    assertEquals(NOT_PRESENT, e.getReason(), "Message when not present");
  }

  @Test
  void testToResponseStatusExceptionGeneric() {
    final Exception testException = new RuntimeException("just testing");
    when(messagesService.getExceptionMessage(testException)).thenReturn(TYPICAL_ERROR_MESSAGE);

    final ResponseStatusException result = responseService.toResponseStatusException(testException);

    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatus(), "Http status for generic exception");
    assertEquals(TYPICAL_ERROR_MESSAGE, result.getReason(), "Reason for generic exception");
  }

  @Test
  void testToResponseStatusExceptionAeriusInternal() {
    final AeriusException testException = new AeriusException(AeriusExceptionReason.INTERNAL_ERROR, 123);
    when(messagesService.getExceptionMessage(testException)).thenReturn(TYPICAL_ERROR_MESSAGE);

    final ResponseStatusException result = responseService.toResponseStatusException(testException);

    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatus(), "Http status for generic exception");
    assertEquals(TYPICAL_ERROR_MESSAGE + " (123)", result.getReason(), "Reason for generic exception");
  }

  @Test
  void testToResponseStatusExceptionAeriusNotInternal() {
    final AeriusException testException = mock(AeriusException.class);
    when(testException.isInternalError()).thenReturn(false);
    when(testException.getReference()).thenReturn(321L);
    when(messagesService.getExceptionMessage(testException)).thenReturn(TYPICAL_ERROR_MESSAGE);

    final ResponseStatusException result = responseService.toResponseStatusException(testException);

    assertEquals(HttpStatus.BAD_REQUEST, result.getStatus(), "Http status for generic exception");
    assertEquals(TYPICAL_ERROR_MESSAGE + " (321)", result.getReason(), "Reason for generic exception");
  }

  @Test
  void testConvertToValidation() {
    final AeriusException testException = mock(AeriusException.class);
    final Reason reason = mock(Reason.class);
    when(reason.getErrorCode()).thenReturn(230);
    when(testException.getReason()).thenReturn(reason);
    when(messagesService.getExceptionMessage(testException)).thenReturn(TYPICAL_ERROR_MESSAGE);

    final ValidationMessage result = responseService.convertToValidation(testException);

    assertEquals(230, result.getCode(), "Code should be based on reason");
    assertEquals(TYPICAL_ERROR_MESSAGE, result.getMessage(), "Error message should be from service");
  }

  @Test
  void testConvertToValidations() {
    final AeriusException testException1 = mock(AeriusException.class);
    final Reason reason1 = mock(Reason.class);
    when(reason1.getErrorCode()).thenReturn(230);
    when(testException1.getReason()).thenReturn(reason1);
    when(messagesService.getExceptionMessage(testException1)).thenReturn(TYPICAL_ERROR_MESSAGE);
    final AeriusException testException2 = mock(AeriusException.class);
    final Reason reason2 = mock(Reason.class);
    when(reason2.getErrorCode()).thenReturn(600);
    when(testException2.getReason()).thenReturn(reason2);
    when(messagesService.getExceptionMessage(testException2)).thenReturn("My Odd Message");
    final AeriusException testException3 = mock(AeriusException.class);
    final Reason reason3 = mock(Reason.class);
    when(reason3.getErrorCode()).thenReturn(42);
    when(testException3.getReason()).thenReturn(reason3);
    when(messagesService.getExceptionMessage(testException3)).thenReturn("Now for something completely different");

    final List<AeriusException> testExceptions = List.of(testException1, testException2, testException3);

    final List<ValidationMessage> results = responseService.convertToValidation(testExceptions);

    assertEquals(3, results.size(), "Http status for generic exception");
    assertEquals(230, results.get(0).getCode(), "Code should be based on reason");
    assertEquals(TYPICAL_ERROR_MESSAGE, results.get(0).getMessage(), "Error message should be from service");
  }

}
