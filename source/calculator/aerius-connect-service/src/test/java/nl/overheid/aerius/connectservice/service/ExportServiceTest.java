/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.db.common.ConstantRepositoryBean;
import nl.overheid.aerius.gml.GMLWriter;
import nl.overheid.aerius.gml.base.MetaDataInput;
import nl.overheid.aerius.shared.domain.scenario.IsScenario;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioMetaData;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;

@ExtendWith(MockitoExtension.class)
class ExportServiceTest {

  @TempDir Path tempDir;

  @Mock ConstantRepositoryBean constantRepository;
  @Mock GMLWriter gmlWriter;

  @Captor ArgumentCaptor<IsScenario> scenarioCaptor;
  @Captor ArgumentCaptor<MetaDataInput> metadataCaptor;

  ExportService exportService;

  @BeforeEach
  void beforeEach() {
    exportService = new ExportService(constantRepository, gmlWriter);
  }

  @Test
  void testExportAsZip() throws AeriusException, IOException {
    final ImportParcel importResult1 = mock(ImportParcel.class);
    final ImportParcel importResult2 = mock(ImportParcel.class);
    final List<ImportParcel> importResults = List.of(importResult1, importResult2);

    final int year = 2021;
    final ScenarioSituation situation1 = mock(ScenarioSituation.class);
    when(importResult1.getSituation()).thenReturn(situation1);
    when(situation1.getYear()).thenReturn(year);
    final List<EmissionSourceFeature> sources1 = new ArrayList<>();
    when(situation1.getEmissionSourcesList()).thenReturn(sources1);
    final ScenarioSituation situation2 = mock(ScenarioSituation.class);
    when(importResult2.getSituation()).thenReturn(situation2);
    when(situation2.getYear()).thenReturn(year);
    final List<EmissionSourceFeature> sources2 = new ArrayList<>();
    when(situation2.getEmissionSourcesList()).thenReturn(sources2);

    final ScenarioMetaData metaData = mock(ScenarioMetaData.class);
    when(importResult1.getImportedMetaData()).thenReturn(metaData);
    when(importResult2.getImportedMetaData()).thenReturn(metaData);

    when(gmlWriter.writeToFile(any(), scenarioCaptor.capture(), metadataCaptor.capture())).thenAnswer(this::generateRandomFile);

    final byte[] export = exportService.exportAsZip(importResults);

    assertNotNull(export, "Export shouldn't be null");
    assertTrue(export.length > 0, "Export shouldn't be empty");

    assertEquals(2, scenarioCaptor.getAllValues().size(), "");
    final IsScenario firstScenario = scenarioCaptor.getAllValues().get(0);
    final IsScenario secondScenario = scenarioCaptor.getAllValues().get(1);
    assertEquals(sources1, firstScenario.getSources(), "First scenario should contain first sourcelist");
    assertEquals(sources2, secondScenario.getSources(), "Second scenario should contain second sourcelist");

    assertEquals(2, metadataCaptor.getAllValues().size(), "Metadata shouldn't be null");
    for (final MetaDataInput metaDataInput : metadataCaptor.getAllValues()) {
      assertEquals(year, metaDataInput.getYear(), "Year in metadata should be the one imported");
      assertEquals(metaData, metaDataInput.getScenarioMetaData(), "Scenario meta data should be the one imported");
    }
  }

  private File generateRandomFile(final InvocationOnMock invocationOnMock) {
    final Path generatedFile = tempDir.resolve(UUID.randomUUID().toString() + ".txt");
    try (BufferedWriter writer = Files.newBufferedWriter(generatedFile)) {
      writer.write("This should end up in a zip file.");
      writer.newLine();
    } catch (final IOException e) {
      throw new UncheckedIOException(e);
    }
    return generatedFile.toFile();
  }

}
