/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.aerius.taskmanager.client.TaskManagerClientSender;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.importer.domain.ImportTaskInput;
import nl.overheid.aerius.importer.domain.ImporterInput;
import nl.overheid.aerius.importer.domain.SaveImportedResultsTaskInput;
import nl.overheid.aerius.importer.domain.SituationWithFileId;
import nl.overheid.aerius.shared.FileServerExpireTag;
import nl.overheid.aerius.shared.FileServerFile;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.validation.FileValidationStatus;
import nl.overheid.aerius.shared.domain.validation.ValidationStatus;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Test class for {@link UiImportService}.
 */
@ExtendWith(MockitoExtension.class)
class UiImportServiceTest {

  private static final FileServerExpireTag SHORT = FileServerExpireTag.SHORT;
  private static final String UUID = "uuid";
  private static final String IMPORT_FILENAME = "importFilename";

  private @Mock ProxyFileService fileService;
  private @Mock JobRepositoryBean jobRepository;
  private @Mock TaskManagerClientSender taskManagerClientSender;

  private @Captor ArgumentCaptor<FileValidationStatus> validationStatusCaptor;
  private @Captor ArgumentCaptor<ImporterInput> importerInputCaptor;

  private UiImportService uiImportService;

  @BeforeEach
  void before() {
    uiImportService = new UiImportService(fileService, jobRepository, taskManagerClientSender);
  }

  /**
   * Test sending import to importer worker.
   */
  @Test
  void testImport() throws AeriusException, IOException {
    final String originalFilename = "originalFilename";

    uiImportService.sendImportToWorker(UUID, IMPORT_FILENAME, originalFilename, null, SHORT);

    verify(taskManagerClientSender).sendTask(importerInputCaptor.capture(), eq(UUID), eq(UUID), isNull(), eq(WorkerType.IMPORTER.type()));
    final ImporterInput importerInput = importerInputCaptor.getValue();

    assertTrue(importerInput instanceof ImportTaskInput, "Should be correct class");
    final ImportTaskInput importTaskInput = (ImportTaskInput) importerInput;
    assertEquals(IMPORT_FILENAME, importTaskInput.getImportFilename(), "Expected the importFilename");
    assertEquals(originalFilename, importTaskInput.getOriginalFilename(), "Expected the originalFilename");
    assertEquals(SHORT, importTaskInput.getExpire(), "Expected SHORT expire tag");
  }

  /**
   * Test sending save results to importer worker.
   */
  @Test
  void testSaveResults() throws AeriusException, IOException {
    final Scenario scenario = mock(Scenario.class);
    final List<SituationWithFileId> situationsWithFileIds = List.of(new SituationWithFileId("someSituationId", "someFileId"));
    final String mockJobKey = "someJobKey";
    when(jobRepository.createJob(JobType.INSERT_RESULTS)).thenReturn(mockJobKey);
    final String archiveContributionFileId = "someArchiveFileId";

    final String jobKey = uiImportService.sendSaveResultsToWorker(scenario, situationsWithFileIds, "someArchiveFileId");

    assertEquals(mockJobKey, jobKey, "Job key returned should be the one created by repository");
    verify(taskManagerClientSender).sendTask(importerInputCaptor.capture(), eq(mockJobKey), eq(mockJobKey), isNull(), eq(WorkerType.IMPORTER.type()));
    final ImporterInput importerInput = importerInputCaptor.getValue();
    assertTrue(importerInput instanceof SaveImportedResultsTaskInput, "Should be correct class");
    final SaveImportedResultsTaskInput saveImportedResultsTaskInput = (SaveImportedResultsTaskInput) importerInput;
    assertEquals(jobKey, saveImportedResultsTaskInput.getJobKey(), "Expected the generated job key");
    assertEquals(scenario, saveImportedResultsTaskInput.getScenario(), "Expected the supplied scenario");
    assertEquals(situationsWithFileIds, saveImportedResultsTaskInput.getSituationFileIds(), "Expected the supplied list");
    assertEquals(archiveContributionFileId, saveImportedResultsTaskInput.getArchiveContributionFileId(), "Expected the archive contribution file ID");
  }

  @Test
  void testOnException() {
    final IllegalArgumentException exception = new IllegalArgumentException();

    uiImportService.onException(UUID, exception, SHORT);

    verify(fileService).writeJson(eq(UUID), eq(FileServerFile.VALIDATION), eq(SHORT), validationStatusCaptor.capture());

    final FileValidationStatus status = validationStatusCaptor.getValue();
    assertEquals(ValidationStatus.INVALID, status.getValidationStatus(), "Expected failed validation status");
    assertEquals(AeriusExceptionReason.INTERNAL_ERROR.getErrorCode(), status.getErrors().get(0).getReason(), "Expected internal error code");
  }
}
