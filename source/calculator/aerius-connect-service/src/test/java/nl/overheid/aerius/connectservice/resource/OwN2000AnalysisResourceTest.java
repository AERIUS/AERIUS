/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.resource;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import nl.overheid.aerius.connectservice.auth.AuthenticationService;
import nl.overheid.aerius.connectservice.model.CalculateResponse;
import nl.overheid.aerius.connectservice.model.OpsOptions;
import nl.overheid.aerius.connectservice.model.OwN2000AnalysisOptions;
import nl.overheid.aerius.connectservice.model.OwN2000AnalysisOptions.OutputTypeEnum;
import nl.overheid.aerius.connectservice.model.OwN2000AnalysisOptions.SubReceptorsEnum;
import nl.overheid.aerius.connectservice.model.UploadFile;
import nl.overheid.aerius.connectservice.service.ObjectValidatorService;
import nl.overheid.aerius.connectservice.service.OwN2000AnalysisService;
import nl.overheid.aerius.connectservice.service.ResponseService;
import nl.overheid.aerius.connectservice.service.ScenarioConstruction;
import nl.overheid.aerius.connectservice.service.ScenarioService;
import nl.overheid.aerius.importer.ImportOption;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Test class for {@link OwN2000AnalysisResource}.
 */
@ExtendWith(MockitoExtension.class)
class OwN2000AnalysisResourceTest {

  private static final String TEST_JOB_KEY = "some special key";

  @Mock AuthenticationService authenticationService;
  @Mock ResponseService responseService;
  @Mock ScenarioService scenarioService;
  @Mock ObjectValidatorService validatorService;
  @Mock OwN2000AnalysisService calculateService;

  @Mock ConnectUser connectUser;
  @Mock ResponseEntity<CalculateResponse> mockResponse;

  private @InjectMocks OwN2000AnalysisResource owN2000AnalysisResource;

  @Captor ArgumentCaptor<Set<ImportOption>> importOptionsCaptor;
  @Captor ArgumentCaptor<CalculateResponse> calculateResponseCaptor;
  @Captor ArgumentCaptor<AeriusException> exceptionCaptor;

  @ParameterizedTest
  @MethodSource("casesForAnalysis")
  void testAnalyseOwN2000(final String description, final OwN2000AnalysisOptions inputOptions,
      final Consumer<Set<ImportOption>> importOptionsValidation)
          throws AeriusException {
    mockCalculateCall();

    final List<UploadFile> files = List.of();
    final List<MultipartFile> fileParts = List.of();
    owN2000AnalysisResource.analyseOwN2000(inputOptions, files, fileParts);

    final Set<ImportOption> importOptions = importOptionsCaptor.getValue();
    importOptionsValidation.accept(importOptions);

    final CalculateResponse capturedResponse = calculateResponseCaptor.getValue();
    assertNotNull(capturedResponse, "Content of response shouldn't be null");
    assertTrue(capturedResponse.getSuccessful(), "It should have been succesful");

    verify(calculateService, times(1)).validateInput(any());
    assertEquals(TEST_JOB_KEY, capturedResponse.getJobKey(), "Result should be calculated");
  }

  @Test
  void testAnalyseOwN2000InvalidRawInputWithGMLOutput() throws AeriusException {
    final Consumer<OwN2000AnalysisOptions> mockInvalidOptions = options -> {
      when(options.getOutputType()).thenReturn(OutputTypeEnum.GML);
      final OpsOptions opsOptions = mock(OpsOptions.class);
      when(options.getOpsOptions()).thenReturn(opsOptions);
      when(opsOptions.getRawInput()).thenReturn(true);
    };

    final List<Consumer<OwN2000AnalysisOptions>> validCombinations = List.of(options -> {
      when(options.getOutputType()).thenReturn(OutputTypeEnum.GML);
      final OpsOptions opsOptions = mock(OpsOptions.class);
      when(options.getOpsOptions()).thenReturn(opsOptions);
      when(opsOptions.getRawInput()).thenReturn(false);
    }, options -> {
      when(options.getOutputType()).thenReturn(OutputTypeEnum.CSV);
      final OpsOptions opsOptions = mock(OpsOptions.class);
      when(options.getOpsOptions()).thenReturn(opsOptions);
      when(opsOptions.getRawInput()).thenReturn(true);
    });

    final AeriusException capturedException = assertInvalidCombination(validCombinations, mockInvalidOptions);
    assertEquals(AeriusExceptionReason.CONNECT_OPTION_COMBINATION_NOT_ALLOWED, capturedException.getReason(), "Reason");
    assertArrayEquals(new String[] {"opsOptions.rawInput=true", "outputType=GML"}, capturedException.getArgs(), "Arguments");
  }

  @Test
  void testAnalyseOwN2000InvalidSplitWorkWithSubreceptorsDisabled() throws AeriusException {
    final Consumer<OwN2000AnalysisOptions> mockInvaliOptions = options -> {
      when(options.getSubReceptors()).thenReturn(SubReceptorsEnum.DISABLED);
      lenient().when(options.getSplitSubReceptorWork()).thenReturn(Boolean.TRUE);
    };
    final List<Consumer<OwN2000AnalysisOptions>> validCombinations = List.of(
        options -> {
          when(options.getSubReceptors()).thenReturn(SubReceptorsEnum.DISABLED);
          when(options.getSplitSubReceptorWork()).thenReturn(Boolean.FALSE);
        },
        options -> {
          when(options.getSubReceptors()).thenReturn(SubReceptorsEnum.ENABLED);
          when(options.getSplitSubReceptorWork()).thenReturn(Boolean.TRUE);
        });

    final AeriusException capturedException = assertInvalidCombination(validCombinations, mockInvaliOptions);
    assertEquals(AeriusExceptionReason.CONNECT_OPTION_COMBINATION_NOT_ALLOWED, capturedException.getReason(), "Reason");
    assertArrayEquals(new String[] {"splitSubReceptorWork=true", "subReceptors=DISABLED"}, capturedException.getArgs(), "Arguments");
  }

  private AeriusException assertInvalidCombination(final List<Consumer<OwN2000AnalysisOptions>> validCombinations,
      final Consumer<OwN2000AnalysisOptions> mockInvalidOption) throws AeriusException {
    when(authenticationService.getCurrentUserWithValidatingJobLimits()).thenReturn(connectUser);

    final OwN2000AnalysisOptions options = mock(OwN2000AnalysisOptions.class);
    final OpsOptions opsOptions = mock(OpsOptions.class);
    when(options.getOpsOptions()).thenReturn(opsOptions);
    final List<UploadFile> files = List.of();
    final List<MultipartFile> fileParts = List.of();

    mockCalculateCall();
    // First check if valid combinations
    final ResponseStatusException mockException = mock(ResponseStatusException.class);
    when(responseService.toResponseStatusException(exceptionCaptor.capture())).thenReturn(mockException);

    for (final Consumer<OwN2000AnalysisOptions> validCombination : validCombinations) {
      validCombination.accept(options);
      assertDoesNotThrow(() -> owN2000AnalysisResource.analyseOwN2000(options, files, fileParts), "This call should not fail as it has valid options.");
    }
    // Second apply the invalid option and check if the expected expection is thrown.
    mockInvalidOption.accept(options);
    final ResponseStatusException thrown =
        assertThrows(ResponseStatusException.class, () -> owN2000AnalysisResource.analyseOwN2000(options, files, fileParts),
            "Should throw an exception when this combination is used.");

    assertEquals(mockException, thrown, "Thrown exception should be the one from the responseService");
    final AeriusException capturedException = exceptionCaptor.getValue();
    return capturedException;
  }

  private void mockCalculateCall() throws AeriusException {
    when(authenticationService.getCurrentUserWithValidatingJobLimits()).thenReturn(connectUser);
    final ScenarioConstruction constructedScenario = mock(ScenarioConstruction.class);
    lenient().when(constructedScenario.isSuccesful()).thenReturn(true);
    lenient().when(scenarioService.constructScenario(same(Theme.OWN2000), any(), importOptionsCaptor.capture())).thenReturn(constructedScenario);
    lenient().when(calculateService.calculate(any(), any(), any())).thenReturn(TEST_JOB_KEY);
    lenient().when(responseService.toOkResponse(calculateResponseCaptor.capture())).thenReturn(mockResponse);
  }

  private static Stream<Arguments> casesForAnalysis() {
    return Stream.of(
        Arguments.of("Validate schema: null", optionsValidateSchema(null), expectedValidateSchema(true)),
        Arguments.of("Validate schema: false", optionsValidateSchema(false), expectedValidateSchema(false)),
        Arguments.of("Validate schema: true", optionsValidateSchema(true), expectedValidateSchema(true)),
        Arguments.of("OPS raw input: null", optionsOpsRawInput(null), expectedOpsRawInput(false)),
        Arguments.of("OPS raw input: false", optionsOpsRawInput(false), expectedOpsRawInput(false)),
        Arguments.of("OPS raw input: true", optionsOpsRawInput(true), expectedOpsRawInput(true)));
  }

  private static OwN2000AnalysisOptions optionsValidateSchema(final Boolean value) {
    final OwN2000AnalysisOptions options = mock(OwN2000AnalysisOptions.class);
    when(options.getValidateAgainstSchema()).thenReturn(value);
    return options;
  }

  private static Consumer<Set<ImportOption>> expectedValidateSchema(final boolean expectedToBePresent) {
    return (options) -> assertEquals(expectedToBePresent, options.contains(ImportOption.VALIDATE_AGAINST_SCHEMA), "Schema validation presence");
  }

  private static OwN2000AnalysisOptions optionsOpsRawInput(final Boolean value) {
    final OwN2000AnalysisOptions options = mock(OwN2000AnalysisOptions.class);
    final OpsOptions opsOptions = mock(OpsOptions.class);
    when(options.getOpsOptions()).thenReturn(opsOptions);
    when(opsOptions.getRawInput()).thenReturn(value);
    return options;
  }

  private static Consumer<Set<ImportOption>> expectedOpsRawInput(final boolean expectedToBePresent) {
    return (options) -> assertEquals(expectedToBePresent, options.contains(ImportOption.IMPORT_SOURCE_DIAMETER), "Import source diameter presence");
  }

}
