/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.connectservice.domain.ImportParcelAggregate;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;

@ExtendWith(MockitoExtension.class)
class ValidationReportServiceTest {

  @Mock MessagesService messagesService;

  ValidationReportService reportService;

  @BeforeEach
  void beforeEach() {
    reportService = new ValidationReportService(messagesService);
  }

  @Test
  void testReport() throws IOException {
    final ImportParcelAggregate importResult = mock(ImportParcelAggregate.class);
    final LocalDateTime testDateTime = LocalDateTime.of(2021, 05, 26, 21, 54);
    final AeriusException error1 = mock(AeriusException.class);
    final AeriusException error2 = mock(AeriusException.class);

    final AeriusException warning1 = mock(AeriusException.class);
    final AeriusException warning2 = mock(AeriusException.class);

    final ImportParcel importParcel1 = mock(ImportParcel.class);
    final ScenarioSituation situation1 = mock(ScenarioSituation.class);
    when(importParcel1.getSituation()).thenReturn(situation1);
    when(importParcel1.getExceptions()).thenReturn(new ArrayList<>(List.of(error1)));
    when(importParcel1.getWarnings()).thenReturn(new ArrayList<>(List.of(warning1)));
    when(situation1.getReference()).thenReturn("Reference1");

    final ImportParcel importParcel2 = mock(ImportParcel.class);
    final ScenarioSituation situation2 = mock(ScenarioSituation.class);
    when(importParcel2.getSituation()).thenReturn(situation2);
    when(importParcel2.getExceptions()).thenReturn(new ArrayList<>(List.of(error2)));
    when(importParcel2.getWarnings()).thenReturn(new ArrayList<>(List.of(warning2)));
    when(situation2.getReference()).thenReturn("Reference2");

    when(importResult.getImportParcels()).thenReturn(new ArrayList<>(List.of(importParcel1, importParcel2)));

    final AeriusException.Reason mockedReason1 = mock(AeriusException.Reason.class);
    when(mockedReason1.getErrorCode()).thenReturn(1);
    final AeriusException.Reason mockedReason2 = mock(AeriusException.Reason.class);
    when(mockedReason1.getErrorCode()).thenReturn(2);

    when(messagesService.getExceptionMessage(error1)).thenReturn("Error message 1");
    when(error1.getReason()).thenReturn(mockedReason1);
    when(messagesService.getExceptionMessage(error2)).thenReturn("Error message the \"second\"");
    when(error2.getReason()).thenReturn(mockedReason2);
    when(messagesService.getExceptionMessage(warning1)).thenReturn("Some cute warning");
    when(warning1.getReason()).thenReturn(mockedReason1);
    when(messagesService.getExceptionMessage(warning2)).thenReturn("Another warning");
    when(warning2.getReason()).thenReturn(mockedReason2);

    String result;
    try (final ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
      reportService.report(importResult, testDateTime, outputStream);
      result = outputStream.toString(StandardCharsets.UTF_8);
    }

    assertEquals("Reference,Time,Type,Code,Message\n"
            + "Reference1,2021-05-26 21:54:00,ERROR,2,Error message 1\n"
            + "Reference2,2021-05-26 21:54:00,ERROR,0,\"Error message the \"\"second\"\"\"\n"
            + "Reference1,2021-05-26 21:54:00,WARNING,2,Some cute warning\n"
            + "Reference2,2021-05-26 21:54:00,WARNING,0,Another warning\n",
        result);
  }

}
