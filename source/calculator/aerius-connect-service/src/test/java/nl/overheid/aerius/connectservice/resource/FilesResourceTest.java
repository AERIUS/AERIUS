/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.resource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.function.Function;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.reactivestreams.Publisher;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ClientResponse.Headers;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.Builder;
import org.springframework.web.reactive.function.client.WebClient.RequestHeadersUriSpec;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import nl.overheid.aerius.connectservice.config.FileServerProperties;
import nl.overheid.aerius.connectservice.service.ProxyFileService;
import nl.overheid.aerius.connectservice.service.ResponseService;
import nl.overheid.aerius.shared.exception.AeriusException;

import reactor.core.publisher.Mono;

/**
 * Unit test for {@link FilesResource}.
 */
@ExtendWith(MockitoExtension.class)
public class FilesResourceTest {

  private static final String TEST_JOB_KEY = "abcdfg";
  private static final String TEST_JOB_NAME = "test.zip";

  private @Mock ResponseService responseService;
  private @Mock WebClient mockWebClient;
  private @Mock Builder webClientBuilder;
  private @Mock ResponseEntity<Resource> mockResponseEntity;
  private @Mock RequestHeadersUriSpec<?> requestHeadersUriSpec;
  private @Mock ClientResponse clientResponse;
  private @Mock Headers headers;
  private @Mock HttpHeaders httpHeaders;
  private @Mock Mono<?> exchangeMono;

  private @Mock Function<? super Publisher<Object>, ? extends Publisher<Object>> onEachOperatorMock;
  private @Mock Mono<?> mono;
  private @Mock DataBuffer dataBuffer;

  private @Captor ArgumentCaptor<InputStreamResource> inputStreamResource;

  private FilesResource fileServerResource;

  @BeforeEach
  void beforeEach() throws AeriusException {
    final FileServerProperties fileServerProperties = new FileServerProperties();
    when(webClientBuilder.baseUrl(any())).thenReturn(webClientBuilder);
    when(webClientBuilder.clientConnector(any())).thenReturn(webClientBuilder);
    when(webClientBuilder.build()).thenReturn(mockWebClient);
    final ProxyFileService proxyFileService = new ProxyFileService(webClientBuilder, fileServerProperties);
    fileServerResource = new FilesResource(proxyFileService, responseService);

    doReturn(requestHeadersUriSpec).when(mockWebClient).get();
    doReturn(requestHeadersUriSpec).when(requestHeadersUriSpec).uri(any(), any(Function.class));
    doAnswer(a -> ((Function) a.getArgument(0)).apply(clientResponse)).when(requestHeadersUriSpec).exchangeToMono(any(Function.class));
  }

  @Test
  void testRedirectResult() throws WebClientResponseException {
    doReturn(HttpStatus.TEMPORARY_REDIRECT).when(clientResponse).statusCode();
    doReturn(headers).when(clientResponse).headers();
    doReturn(new HttpHeaders()).when(headers).asHttpHeaders();
    final ResponseEntity<Resource> response = fileServerResource.getFile(TEST_JOB_KEY, TEST_JOB_NAME);

    assertEquals(HttpStatus.TEMPORARY_REDIRECT, response.getStatusCode(), "Status code should be redirect");
  }

  @Test
  void testRetrieveResult() throws WebClientResponseException, IOException {
    final byte[] bytes = "test".getBytes();

    try (final InputStream inputStream = new ByteArrayInputStream(bytes)) {
      doReturn(HttpStatus.OK).when(clientResponse).statusCode();
      doReturn(headers).when(clientResponse).headers();
      doReturn(List.of("url")).when(headers).header(HttpHeaders.CONTENT_DISPOSITION);
      doReturn(Mono.just(dataBuffer)).when(clientResponse).bodyToMono(any(Class.class));
      doReturn(inputStream).when(dataBuffer).asInputStream(anyBoolean());
      doReturn(mockResponseEntity).when(responseService).toOkResponse(inputStreamResource.capture(), any());
      final ResponseEntity<Resource> response = fileServerResource.getFile(TEST_JOB_KEY, TEST_JOB_NAME);

      assertEquals(mockResponseEntity, response, "Response should come from response service");
      assertEquals(inputStream, inputStreamResource.getValue().getInputStream(), "Content of response should match the data from mock file service");
    }
  }
}
