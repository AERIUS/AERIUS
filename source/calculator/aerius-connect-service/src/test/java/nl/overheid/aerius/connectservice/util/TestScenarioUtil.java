/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.util;

import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.db.common.ConstantRepositoryBean;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Util class to generate a mock data, like scenario.
 */
public final class TestScenarioUtil {

  public static final int MIN_YEAR = 2000;
  public static final int MAX_YEAR = 2100;
  public static final int TEST_YEAR = 2030;

  private TestScenarioUtil() {
    // Util class
  }

  /**
   * Create mocking for min and max years as database constants.
   *
   * @param constantRepository
   */
  public static void mockConstantRepositoryYears(final ConstantRepositoryBean constantRepository) throws AeriusException {
    lenient().when(constantRepository.getInteger(ConstantsEnum.MIN_YEAR)).thenReturn(MIN_YEAR);
    lenient().when(constantRepository.getInteger(ConstantsEnum.MAX_YEAR)).thenReturn(MAX_YEAR);
  }

  public static Scenario mockScenario() {
    return mockScenario(TEST_YEAR);
  }

  public static Scenario mockScenario(final int year) {
    final Scenario scenario = mock(Scenario.class);
    final ScenarioSituation situation = mock(ScenarioSituation.class);
    final CalculationSetOptions options = mock(CalculationSetOptions.class);
    final CalculationPointFeature mockPoint = new CalculationPointFeature();
    final List<CalculationPointFeature> customPointsList = newModifiableList(mockPoint);

    lenient().when(situation.getYear()).thenReturn(year);
    lenient().when(situation.getType()).thenReturn(SituationType.PROPOSED);
    lenient().when(scenario.getCustomPointsList()).thenReturn(customPointsList);
    when(scenario.getSituations()).thenReturn(List.of(situation));
    lenient().when(scenario.getOptions()).thenReturn(options);
    lenient().when(options.getCalculationJobType()).thenReturn(CalculationJobType.PROCESS_CONTRIBUTION);
    return scenario;
  }

  private static <T> List<T> newModifiableList(final T entry) {
    final List<T> list = new ArrayList<>();

    list.add(entry);
    return list;
  }
}
