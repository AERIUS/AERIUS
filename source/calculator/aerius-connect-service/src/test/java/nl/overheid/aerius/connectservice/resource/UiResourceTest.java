/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.resource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import nl.overheid.aerius.connectservice.auth.AuthenticationService;
import nl.overheid.aerius.connectservice.domain.UiCalculationRequest;
import nl.overheid.aerius.connectservice.domain.UiImportResultsRequest;
import nl.overheid.aerius.connectservice.service.EmissionsSourcesService;
import nl.overheid.aerius.connectservice.service.InfoService;
import nl.overheid.aerius.connectservice.service.ProxyFileService;
import nl.overheid.aerius.connectservice.service.ResponseService;
import nl.overheid.aerius.connectservice.service.ServerUsageService;
import nl.overheid.aerius.connectservice.service.UiCalculateService;
import nl.overheid.aerius.connectservice.service.UiCalculationPointService;
import nl.overheid.aerius.connectservice.service.UiImportService;
import nl.overheid.aerius.connectservice.service.UiJobsService;
import nl.overheid.aerius.connectservice.service.UiResultsService;
import nl.overheid.aerius.connectservice.service.ValidationReportService;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.importer.domain.SituationWithFileId;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.JobProgress;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Test class for {@link UiResource}.
 */
@ExtendWith(MockitoExtension.class)
class UiResourceTest {

  private static final String JOB_KEY = "JOB_KEY";
  private static final String IMPORT_RESULTS_REQUEST_AS_STRING = "importResultsRequestAsString";
  private static final String CALCULATION_REQUEST_AS_STRING = "calculationRequestAsString";

  private @Mock AuthenticationService authenticationService;
  private @Mock ResponseService responseService;
  private @Mock ProxyFileService proxyFileService;
  private @Mock UiImportService uiImportService;
  private @Mock UiCalculateService calculateService;
  private @Mock UiJobsService jobsService;
  private @Mock UiResultsService resultsService;
  private @Mock EmissionsSourcesService sourcesService;
  private @Mock UiCalculationPointService calculationPointService;
  private @Mock ServerUsageService serverUsageService;
  private @Mock InfoService infoService;
  private @Mock JobRepositoryBean jobRepository;
  private @Mock ValidationReportService validationReportService;
  private @Mock ObjectMapper objectMapper;
  private @Mock ResponseEntity<String> mockedResponse;

  private @InjectMocks UiResource uiResource;

  private @Captor ArgumentCaptor<String> uuidCaptor;
  private @Captor ArgumentCaptor<String> fileServerFilename;
  private @Captor ArgumentCaptor<String> filenameCaptor;
  private @Captor ArgumentCaptor<AeriusException> exceptionCaptor;

  @BeforeEach
  void beforeEach() throws IOException {
    lenient().doAnswer(a -> {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, a.getArguments()[0].toString());
    }).when(responseService).toResponseStatusException(any());
    lenient().doAnswer(x -> null).when(uiImportService).onException(any(), exceptionCaptor.capture(), any());
    lenient().doAnswer(x -> null).when(uiImportService).sendImportToWorker(uuidCaptor.capture(), fileServerFilename.capture(),
        filenameCaptor.capture(), any(), any());
  }

  @Test
  void testImportFile() throws IOException {
    final String fileName = "file.gml";
    assertImport(fileName, fileName + ".zip");
  }

  @Test
  void testImportFileZip() throws IOException {
    final String fileName = "file.zip";
    assertImport(fileName, fileName);
  }

  @Test
  void testImportUnsupportedFile() throws IOException {
    final String fileName = "file.c";
    callMockedImportFile(fileName);
    verify(uiImportService, never()).sendImportToWorker(any(), any(), any(), any(), any());
    verify(uiImportService).onException(any(), any(), any());
    assertEquals(AeriusExceptionReason.IMPORT_FILE_UNSUPPORTED, exceptionCaptor.getValue().getReason(), "Expected IMPORT_FILE_UNSUPPORTED reason.");
  }

  @Test
  void testImportResults() throws IOException, AeriusException {
    final String jobKey = "someJobKey";
    when(responseService.toOkResponse(jobKey)).thenReturn(mockedResponse);
    final UiImportResultsRequest mockedRequest = mock(UiImportResultsRequest.class);
    final List<SituationWithFileId> situationsWithFileIds = List.of();
    final Scenario scenario = mock(Scenario.class);
    when(mockedRequest.getScenario()).thenReturn(scenario);
    when(mockedRequest.getSituationWithFileIds()).thenReturn(situationsWithFileIds);
    when(objectMapper.readValue(IMPORT_RESULTS_REQUEST_AS_STRING, UiImportResultsRequest.class)).thenReturn(mockedRequest);
    when(uiImportService.sendSaveResultsToWorker(scenario, situationsWithFileIds, null)).thenReturn(jobKey);

    final ResponseEntity<String> response = uiResource.importResults(IMPORT_RESULTS_REQUEST_AS_STRING);

    assertEquals(mockedResponse, response, "Response should be returned by resource");
  }

  @Test
  void testCalculateWithoutAuthentication() throws AeriusException, JsonProcessingException {
    final Scenario scenario = getScenario();
    final UiCalculationRequest uiCalculationRequest = getUiCalculationRequest(scenario);

    doReturn(Optional.empty()).when(authenticationService).getOptionalCurrentUser();

    uiResource.startCalculation(CALCULATION_REQUEST_AS_STRING);

    verify(calculateService, never()).calculate(any(), any(), any());
    verify(calculateService).calculate(scenario, uiCalculationRequest);
  }

  @Test
  void testCalculateWithAuthentication() throws JsonProcessingException, AeriusException {
    final Scenario scenario = getScenario();
    final UiCalculationRequest uiCalculationRequest = getUiCalculationRequest(scenario);

    final ConnectUser user = mock(ConnectUser.class);
    doReturn(Optional.of(user)).when(authenticationService).getOptionalCurrentUser();

    uiResource.startCalculation(CALCULATION_REQUEST_AS_STRING);

    verify(calculateService).calculate(eq(user), any(), any());
    verify(calculateService, never()).calculate(any(), any());
  }

  @ParameterizedTest
  @CsvSource({"CREATED,", "COMPLETED,JOB_KEY"})
  void testWithJobKeyUnfinished(final JobState state, final String expectedJobKey) throws JsonProcessingException, AeriusException {
    final Scenario scenario = getScenario();
    final UiCalculationRequest uiCalculationRequest = getUiCalculationRequest(scenario);
    uiCalculationRequest.setJobKey(JOB_KEY);
    final JobProgress jobProgres = new JobProgress();
    jobProgres.setState(state);
    doReturn(Optional.of(jobProgres)).when(jobRepository).getProgressForKey(JOB_KEY);

    uiResource.startCalculation(CALCULATION_REQUEST_AS_STRING);

    verify(jobRepository).getProgressForKey(JOB_KEY);
    assertEquals(expectedJobKey, uiCalculationRequest.getJobKey(), "Job key value is not what was expected.");
  }

  private UiCalculationRequest getUiCalculationRequest(final Scenario scenario) throws JsonProcessingException {
    final UiCalculationRequest uiCalculationRequest = new UiCalculationRequest();
    uiCalculationRequest.setScenario(scenario);
    uiCalculationRequest.setTheme(Theme.OWN2000);
    doReturn(uiCalculationRequest).when(objectMapper).readValue(CALCULATION_REQUEST_AS_STRING, UiCalculationRequest.class);
    return uiCalculationRequest;
  }

  private static Scenario getScenario() {
    final Scenario scenario = new Scenario();
    scenario.getSituations().add(new ScenarioSituation());
    return scenario;
  }

  private void assertImport(final String fileName, final String expectedFilename) throws IOException {
    callMockedImportFile(fileName);
    verify(uiImportService).sendImportToWorker(any(), any(), any(), any(), any());
    verify(uiImportService, never()).onException(any(), any(), any());
    assertNotEquals(uuidCaptor.getValue(), fileServerFilename.getValue(), "Generated uuid should not match importFilename");
    assertNotEquals(filenameCaptor.getValue(), fileServerFilename.getValue(), "Passed filename should not match importFilename");
    assertEquals(expectedFilename, filenameCaptor.getValue(), "Filename passed should match name with extension .zip");
  }

  private void callMockedImportFile(final String fileName) throws IOException {
    final MultipartFile multiPart = mock(MultipartFile.class);
    doReturn(fileName).when(multiPart).getOriginalFilename();
    lenient().doReturn(new ByteArrayInputStream("data".getBytes())).when(multiPart).getInputStream();
    uiResource.importFile(multiPart, null);
  }
}
