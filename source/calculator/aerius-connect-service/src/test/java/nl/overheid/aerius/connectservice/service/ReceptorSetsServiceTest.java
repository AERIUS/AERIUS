/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.multipart.MultipartFile;

import nl.overheid.aerius.connectservice.domain.ImportParcelAggregate;
import nl.overheid.aerius.connectservice.model.ReceptorSet;
import nl.overheid.aerius.db.connect.ConnectCalculationPointSetsRepositoryBean;
import nl.overheid.aerius.shared.domain.connect.ConnectCalculationPointSetMetadata;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

@ExtendWith(MockitoExtension.class)
class ReceptorSetsServiceTest {

  private static final int TEST_ID = 43;
  private static final String TEST_NAME = "SomeIdName";
  private static final String TEST_DESCRIPTION = "Something that does not matter";

  private @Mock ConnectCalculationPointSetsRepositoryBean calculationPointSetsRepository;
  private @Mock ImportService importService;

  private @Mock ConnectUser user;
  private @Mock ReceptorSet receptorSet;
  private @Mock MultipartFile filePart;

  private @Captor ArgumentCaptor<ConnectCalculationPointSetMetadata> metadataCaptor;

  ReceptorSetsService receptorSetsService;

  @BeforeEach
  void beforeEach() {
    receptorSetsService = new ReceptorSetsService(calculationPointSetsRepository, importService);
  }

  @Test
  void testAddReceptorSetExisting() throws AeriusException {
    when(receptorSet.getName()).thenReturn(TEST_NAME);
    when(calculationPointSetsRepository.getCalculationPointSetByName(user, TEST_NAME))
        .thenReturn(mock(ConnectCalculationPointSetMetadata.class));

    final AeriusException e = assertThrows(AeriusException.class, () -> receptorSetsService.addReceptorSet(user, receptorSet, filePart));
    assertEquals(AeriusExceptionReason.CONNECT_USER_CALCULATION_POINT_SET_ALREADY_EXISTS, e.getReason(),
        "Reason should be that the set with that name already exists");
  }

  @Test
  void testAddReceptorEmptyName() throws AeriusException {
    when(receptorSet.getName()).thenReturn(" ");

    final AeriusException e = assertThrows(AeriusException.class, () -> receptorSetsService.addReceptorSet(user, receptorSet, filePart));
    assertEquals(AeriusExceptionReason.CONNECT_USER_CALCULATION_POINT_SET_EMPTY_NAME, e.getReason(),
        "Reason should be that the name is empty");
  }

  @Test
  void testAddReceptorSetWithoutCalculationPoints() throws AeriusException {
    when(receptorSet.getName()).thenReturn(TEST_NAME);

    when(calculationPointSetsRepository.getCalculationPointSetByName(user, TEST_NAME)).thenReturn(null);

    final ImportParcel importResult = mock(ImportParcel.class);
    final List<ImportParcel> importResults = List.of(importResult);
    final List<CalculationPointFeature> calculationPoints = new ArrayList<>();
    when(importResult.getCalculationPointsList()).thenReturn(calculationPoints);
    when(importService.importFile(eq(filePart), any(), any())).thenReturn(importResults);

    final AeriusException e = assertThrows(AeriusException.class, () -> receptorSetsService.addReceptorSet(user, receptorSet, filePart));
    assertEquals(AeriusExceptionReason.CONNECT_NO_RECEPTORS_IN_PARAMETERS, e.getReason(),
        "Reason should be that the set with that name already exists");
  }

  @Test
  void testAddReceptorSet() throws AeriusException {
    when(receptorSet.getName()).thenReturn(TEST_NAME);
    when(receptorSet.getDescription()).thenReturn(TEST_DESCRIPTION);
    when(calculationPointSetsRepository.getCalculationPointSetByName(user, TEST_NAME)).thenReturn(null);

    final ImportParcel importResult = mock(ImportParcel.class);
    when(importResult.getExceptions()).thenReturn(List.of());
    final List<ImportParcel> importResults = List.of(importResult);
    final List<CalculationPointFeature> calculationPoints = new ArrayList<>();
    final CalculationPointFeature feature = mock(CalculationPointFeature.class);
    when(feature.getProperties()).thenReturn(mock(CalculationPoint.class));
    calculationPoints.add(feature);
    when(importResult.getCalculationPointsList()).thenReturn(calculationPoints);
    when(importService.importFile(eq(filePart), any(), any())).thenReturn(importResults);

    final ImportParcelAggregate addResult = receptorSetsService.addReceptorSet(user, receptorSet, filePart);

    assertEquals(importResult.getExceptions(), addResult.getExceptions(), "The import results should be returned aggregated");
    verify(calculationPointSetsRepository).insertCalculationPointSet(metadataCaptor.capture(), eq(calculationPoints));
    final ConnectCalculationPointSetMetadata insertedMetada = metadataCaptor.getValue();
    assertEquals(TEST_NAME, insertedMetada.getName(), "Name should be inserted");
    assertEquals(TEST_DESCRIPTION, insertedMetada.getDescription(), "Description should be inserted");
  }

  @Test
  void testDeleteReceptorSetNotExisting() throws AeriusException {
    when(calculationPointSetsRepository.getCalculationPointSetByName(user, TEST_NAME))
        .thenReturn(null);

    final AeriusException e = assertThrows(AeriusException.class, () -> receptorSetsService.deleteReceptorSet(user, TEST_NAME));
    assertEquals(AeriusExceptionReason.CONNECT_USER_CALCULATION_POINT_SET_DOES_NOT_EXIST, e.getReason(),
        "Reason should be that the set with that name does not exists");
  }

  @Test
  void testDeleteReceptorSet() throws AeriusException {
    final ConnectCalculationPointSetMetadata existing = mock(ConnectCalculationPointSetMetadata.class);
    when(existing.getSetId()).thenReturn(TEST_ID);
    when(calculationPointSetsRepository.getCalculationPointSetByName(user, TEST_NAME))
        .thenReturn(existing);

    receptorSetsService.deleteReceptorSet(user, TEST_NAME);

    verify(calculationPointSetsRepository).deleteCalculationPointSet(TEST_ID);
  }

  @Test
  void testGetReceptorSets() throws AeriusException {
    final ConnectCalculationPointSetMetadata existing = mock(ConnectCalculationPointSetMetadata.class);
    when(existing.getName()).thenReturn(TEST_NAME);
    when(existing.getDescription()).thenReturn(TEST_DESCRIPTION);
    when(calculationPointSetsRepository.getCalculationPointSets(user))
        .thenReturn(List.of(existing, mock(ConnectCalculationPointSetMetadata.class)));

    final List<ReceptorSet> retrieved = receptorSetsService.getReceptorSets(user);

    assertEquals(2, retrieved.size(), "Retrieved size list");
    assertEquals(TEST_NAME, retrieved.get(0).getName(), "Name of first set");
    assertEquals(TEST_DESCRIPTION, retrieved.get(0).getDescription(), "Description of first set");
  }

}
