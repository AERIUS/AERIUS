/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.resource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Locale;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import nl.overheid.aerius.connectservice.model.GenerateApiKey;
import nl.overheid.aerius.connectservice.service.EmailMessageSenderService;
import nl.overheid.aerius.connectservice.service.LocaleService;
import nl.overheid.aerius.connectservice.service.ResponseService;
import nl.overheid.aerius.connectservice.service.UserService;
import nl.overheid.aerius.email.message.EmailMessage;
import nl.overheid.aerius.shared.domain.connect.ApiKeyExportData;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Unit test for {@link UserResource}.
 */
@ExtendWith(MockitoExtension.class)
class UserResourceTest {

  private static final String VALID_EMAIL = "valid@example.org";

  @Mock ResponseService responseService;
  @Mock UserService userService;
  @Mock LocaleService localeService;
  @Mock ResponseEntity<Void> mockResponse;
  @Mock EmailMessageSenderService emailMessageSenderService;

  @Captor ArgumentCaptor<AeriusException> exceptionCaptor;
  @Captor ArgumentCaptor<EmailMessage<ApiKeyExportData>> messageCaptor;

  UserResource userResource;

  @BeforeEach
  void beforeEach() throws AeriusException, SQLException {
    userResource = new UserResource(responseService, userService, localeService, emailMessageSenderService);
    lenient().when(localeService.getLocale()).thenReturn(Locale.ENGLISH);
  }

  @Test
  void testGenerateApiKey() throws AeriusException {
    when(responseService.toOkResponse()).thenReturn(mockResponse);
    doAnswer(answer -> null).when(emailMessageSenderService).sendMailToUser(messageCaptor.capture());

    final GenerateApiKey generateApiKey = mock(GenerateApiKey.class);
    when(generateApiKey.getEmail()).thenReturn(VALID_EMAIL);
    final ResponseEntity<Void> returnedResponse = userResource.generateApiKey(generateApiKey);

    assertEquals(mockResponse, returnedResponse, "Response should come from response service");
    final EmailMessage<ApiKeyExportData> message = messageCaptor.getValue();
    assertEquals(VALID_EMAIL, message.getData().getEmailAddress());
  }

  @Test
  void testGenerateApiKeyNullEmail() {
    final ResponseStatusException mockStatusException = mock(ResponseStatusException.class);
    when(responseService.toResponseStatusException(exceptionCaptor.capture())).thenReturn(mockStatusException);

    final GenerateApiKey generateApiKey = mock(GenerateApiKey.class);
    when(generateApiKey.getEmail()).thenReturn(null);
    final ResponseStatusException thrownException = assertThrows(ResponseStatusException.class, () -> userResource.generateApiKey(generateApiKey));

    assertEquals(mockStatusException, thrownException, "Response should come from response service");
    final AeriusException capturedException = exceptionCaptor.getValue();
    assertEquals(AeriusExceptionReason.CONNECT_NO_VALID_EMAIL_SUPPLIED, capturedException.getReason(),
        "Reason should be an invalid email");
  }

  @Test
  void testGenerateApiKeyInvalidEmail() {
    final ResponseStatusException mockStatusException = mock(ResponseStatusException.class);
    when(responseService.toResponseStatusException(exceptionCaptor.capture())).thenReturn(mockStatusException);

    final GenerateApiKey generateApiKey = mock(GenerateApiKey.class);
    when(generateApiKey.getEmail()).thenReturn("not_an_email");
    final ResponseStatusException thrownException = assertThrows(ResponseStatusException.class, () -> userResource.generateApiKey(generateApiKey));

    assertEquals(mockStatusException, thrownException, "Response should come from response service");
    final AeriusException capturedException = exceptionCaptor.getValue();
    assertEquals(AeriusExceptionReason.CONNECT_NO_VALID_EMAIL_SUPPLIED, capturedException.getReason(),
        "Reason should be an invalid email");
  }

  @Test
  void testGenerateApiKeyIOException() throws IOException, AeriusException {
    final AeriusException mockIOException = mock(AeriusException.class);
    doThrow(mockIOException).when(emailMessageSenderService).sendMailToUser(any());

    final ResponseStatusException mockStatusException = mock(ResponseStatusException.class);
    when(responseService.toResponseStatusException(any())).thenReturn(mockStatusException);

    final GenerateApiKey generateApiKey = mock(GenerateApiKey.class);
    when(generateApiKey.getEmail()).thenReturn(VALID_EMAIL);
    final ResponseStatusException thrownException = assertThrows(ResponseStatusException.class, () -> userResource.generateApiKey(generateApiKey));

    assertEquals(mockStatusException, thrownException, "Response should come from response service");
  }

}
