/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.export;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.db.calculator.JobRepository;
import nl.overheid.aerius.export.PdfAttachZipFileProcessor;
import nl.overheid.aerius.export.PdfGenerator;
import nl.overheid.aerius.export.util.PdfTask;
import nl.overheid.aerius.shared.domain.AeriusCustomer;
import nl.overheid.aerius.shared.domain.PdfProductType;
import nl.overheid.aerius.shared.domain.ReleaseType;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.WorkerUtil;

/**
 * Test class for {@link PdfExportHandler} in combination with {@link CalculatorExportWorker}.
 */
@ExtendWith(MockitoExtension.class)
class PdfExportHandlerTest extends AbstractTestExportWorker {

  private static final String WEBSERVER_URL = "http://localhost";

  private @Mock PdfGenerator pdfGenerator;
  private @Mock PdfAttachZipFileProcessor pdfAttachFileProcessor;
  private @Captor ArgumentCaptor<PdfTask> pdfTaskCaptor;
  private @Captor ArgumentCaptor<String> urlCaptor;
  private @Captor ArgumentCaptor<String> referenceCaptor;

  private File folderForJob;

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    setUpWorkerConnectionHelper();
    setUpHttpClientManager();
    setUpJobIdentifier();
    folderForJob = WorkerUtil.getFolderForJob(jobIdentifier);
    JobRepository.getProgress(getConnection(), jobIdentifier.getJobKey());
  }

  @ParameterizedTest
  @MethodSource("pdfExportTypes")
  void testSuccessfulExports(final ExportType exportType, final SituationType requiredType) throws Exception {
    final List<PdfTask> pdfTasks = List
        .of(new PdfTask(AeriusCustomer.RIVM, WEBSERVER_URL, jobIdentifier, PdfProductType.OWN2000_CALCULATOR, ReleaseType.PRODUCTION));
    final PdfExportHandler pdfExportHandler = new PdfExportHandler(pdfGenerator, pdfAttachFileProcessor, pdfTasks, folderForJob);
    setUp(new ExportEmailHandler(workerConnectionHelper, new ExportZipProcessor(pdfExportHandler, fileServerProxy), gmlProvider));
    final CalculationInputData inputData = createExampleCalculationInputData(jobIdentifier);
    lenient().when(calculator.calculate()).then(a -> {
      assertTrue(JobRepository.updateJobStatus(getConnection(), jobIdentifier.getJobKey(), JobState.POST_PROCESSING),
          "Should have set state to POST_PROCESING");
      return calculatorJob;
    });
    inputData.getScenario().getSituations().get(0).setType(requiredType);
    inputData.setExportType(exportType);

    assertExport(inputData, 1);
    verify(pdfGenerator).generate(any(), referenceCaptor.capture(), pdfTaskCaptor.capture(), urlCaptor.capture(), any(), any());
    assertEquals(pdfTasks.get(0), pdfTaskCaptor.getValue(), "Task should contain origional pdfTask");

    final String url = urlCaptor.getValue();
    assertTrue(url.startsWith(WEBSERVER_URL), "Should start with initialized webserver url");
    assertTrue(url.contains("/print?internal=true"), "Url should contain print and internal");
    assertTrue(url.contains("jobKey=" + jobIdentifier.getJobKey()), "Url should contain jobKey");
    assertTrue(url.contains("reference=" + referenceCaptor.getValue()), "Url should contain reference");
    assertTrue(url.contains("proposedSituationId=" + inputData.getScenario().getSituations().get(0).getId()), "Url should contain situation id");
    assertTrue(url.contains("pdfProductType=" + PdfProductType.OWN2000_CALCULATOR), "Url should conain pdfProductType");
    assertTrue(url.contains("locale=nl"), "Url should contain locale");
  }

  private static Stream<Arguments> pdfExportTypes() {
    return Stream.of(ExportType.values())
        .filter(ExportType::isPdfExportType)
        .map(exportType -> Arguments.of(exportType, getRequiredSituationType(exportType)));
  }

  private static SituationType getRequiredSituationType(final ExportType exportType) {
    return exportType == ExportType.PDF_PROCUREMENT_POLICY ? SituationType.REFERENCE : SituationType.PROPOSED;
  }

  private void assertExport(final CalculationInputData inputData, final int expectedFiles) throws AeriusException, URISyntaxException, Exception {
    worker.run(inputData, jobIdentifier, null);
    assertJobState(JobState.POST_PROCESSING);
  }
}
