/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.importer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.mock;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.calculator.CalculationInfoRepository;
import nl.overheid.aerius.db.calculator.JobRepository;
import nl.overheid.aerius.db.common.AreaInformationSupplier;
import nl.overheid.aerius.db.common.results.ResultsSummaryRepository;
import nl.overheid.aerius.db.test.BaseTestDatabase;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.summary.JobSummary;
import nl.overheid.aerius.shared.domain.v2.archive.ArchiveMetaData;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.ReceptorPoint;
import nl.overheid.aerius.shared.domain.v2.point.SubPoint;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituationResults;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link ResultsInserter}.
 */
class ResultsInserterTest extends BaseTestDatabase {

  //Receptors in Binnenveld which are both relevant and exceeding
  private static final int TEST_RECEPTOR_ID = 4263743;
  private static final int TEST_RECEPTOR_ID_2 = 4265272;
  private static final double TEST_RESULT_NOX = 24.2;
  private static final double TEST_RESULT_NH3 = 55.1;

  @Test
  void testInsertResults() throws SQLException, AeriusException {
    final ResultsInserter resultsInserter = new ResultsInserter(getPMF());

    final String jobKey = createJob();

    final List<SituationResultsImport> resultsToInsert = new ArrayList<>();
    final ScenarioSituation situation = createSituation(SituationType.PROPOSED);
    final Scenario scenario = createScenario(List.of(situation));
    final ScenarioSituationResults situationResults = new ScenarioSituationResults();
    situationResults.setResults(createResults(1));
    resultsToInsert.add(new SituationResultsImport(situation.getId(), situationResults, "TestVersion"));

    resultsInserter.insertResults(jobKey, scenario, resultsToInsert, null);

    // Verify calculation is made and results are in
    final Optional<Integer> calculationId = JobRepository.getSituationCalculation(getConnection(), jobKey, situation.getId());
    assertTrue(calculationId.isPresent(), "Calculation should be present");

    final List<CalculationPointFeature> resultsInDb = CalculationInfoRepository.getCalculationResults(getConnection(), calculationId.get());
    assertReceptorOnlyResults(resultsInDb, 1);

    assertSummary(jobKey);
  }

  @Test
  void testInsertResultsArchiveContribution() throws SQLException, AeriusException {
    final ResultsInserter resultsInserter = new ResultsInserter(getPMF());

    final String jobKey = createJob();

    final List<SituationResultsImport> resultsToInsert = new ArrayList<>();
    final ScenarioSituation situation = createSituation(SituationType.PROPOSED);
    final Scenario scenario = createScenario(List.of(situation));
    final ScenarioSituationResults situationResults = new ScenarioSituationResults();
    situationResults.setResults(createResults(1));
    resultsToInsert.add(new SituationResultsImport(situation.getId(), situationResults, "TestVersion"));

    final ArchiveMetaData archiveMetaData = new ArchiveMetaData();
    final ArchiveResultsImport archiveResults = new ArchiveResultsImport(archiveMetaData, createResults(2));

    resultsInserter.insertResults(jobKey, scenario, resultsToInsert, archiveResults);

    // Verify calculation is made and results are in
    final Optional<Integer> calculationId = JobRepository.getSituationCalculation(getConnection(), jobKey, situation.getId());
    assertTrue(calculationId.isPresent(), "Calculation should be present");

    final List<CalculationPointFeature> resultsInDb = CalculationInfoRepository.getCalculationResults(getConnection(), calculationId.get());
    assertReceptorOnlyResults(resultsInDb, 1);
    final List<CalculationPointFeature> archiveResultsInDb = CalculationInfoRepository.getArchiveContributions(getConnection(), calculationId.get());
    assertReceptorOnlyResults(archiveResultsInDb, 2);

    assertSummary(jobKey);
  }

  @Test
  void testInsertResultsSubPoints() throws SQLException, AeriusException {
    final ResultsInserter resultsInserter = new ResultsInserter(getPMF());

    final String jobKey = createJob();

    final List<SituationResultsImport> resultsToInsert = new ArrayList<>();
    final ScenarioSituation situation = createSituation(SituationType.PROPOSED);
    final Scenario scenario = createScenario(List.of(situation));
    scenario.setTheme(Theme.NCA);
    final ScenarioSituationResults situationResults = new ScenarioSituationResults();
    situationResults.setResults(List.of(createBinnenveldResult(1), createBinnenveldSubPointResult(1)));
    resultsToInsert.add(new SituationResultsImport(situation.getId(), situationResults, "TestVersion"));

    resultsInserter.insertResults(jobKey, scenario, resultsToInsert, null);

    // Verify calculation is made and results are in
    final Optional<Integer> calculationId = JobRepository.getSituationCalculation(getConnection(), jobKey, situation.getId());
    assertTrue(calculationId.isPresent(), "Calculation should be present");

    final List<CalculationPointFeature> resultsInDb = CalculationInfoRepository.getCalculationResults(getConnection(), calculationId.get());
    assertReceptorAndSubPointResults(resultsInDb, 1);

    assertSummary(jobKey);
  }

  private void assertReceptorOnlyResults(final List<CalculationPointFeature> resultsInDb, final int factor) {
    assertEquals(1, resultsInDb.size(), "Should be 1 point");
    assertTrue(resultsInDb.get(0).getProperties() instanceof ReceptorPoint, "Should be a receptor point");
    assertReceptorResult((ReceptorPoint) resultsInDb.get(0).getProperties(), factor);
  }

  private void assertReceptorAndSubPointResults(final List<CalculationPointFeature> resultsInDb, final int factor) {
    assertEquals(2, resultsInDb.size(), "Should be 2 point");
    for (final CalculationPointFeature feature : resultsInDb) {
      if (feature.getProperties() instanceof ReceptorPoint) {
        assertReceptorResult((ReceptorPoint) feature.getProperties(), factor);
      } else if (feature.getProperties() instanceof SubPoint) {
        assertSubPointResult((SubPoint) feature.getProperties(), factor);
      } else {
        fail("Unexpected result in database: " + feature);
      }
    }
  }

  private void assertReceptorResult(final ReceptorPoint receptorInDb, final int factor) {
    assertEquals(TEST_RECEPTOR_ID, receptorInDb.getReceptorId(), "Receptor ID");
    assertEquals(3, receptorInDb.getResults().size(), "Nr of results");
    assertEquals(factor * TEST_RESULT_NOX, receptorInDb.getResults().get(EmissionResultKey.NOX_DEPOSITION), 1E-3, "NOX deposition");
    assertEquals(factor * TEST_RESULT_NH3, receptorInDb.getResults().get(EmissionResultKey.NH3_DEPOSITION), 1E-3, "NH3 deposition");
    assertEquals(factor * (TEST_RESULT_NOX + TEST_RESULT_NH3), receptorInDb.getResults().get(EmissionResultKey.NOXNH3_DEPOSITION), 1E-3,
        "Nitrogen deposition");
  }

  private void assertSubPointResult(final SubPoint subPointId, final int factor) {
    assertEquals(2, subPointId.getSubPointId(), "Sub point ID");
    assertEquals(TEST_RECEPTOR_ID_2, subPointId.getReceptorId(), "Sub point receptor ID");
    assertEquals(1, subPointId.getLevel(), "Sub point level");
    assertEquals(3, subPointId.getResults().size(), "Nr of results");
    assertEquals(5 * factor * TEST_RESULT_NOX, subPointId.getResults().get(EmissionResultKey.NOX_DEPOSITION), 1E-3, "NOX deposition");
    assertEquals(5 * factor * TEST_RESULT_NH3, subPointId.getResults().get(EmissionResultKey.NH3_DEPOSITION), 1E-3, "NH3 deposition");
    assertEquals(5 * factor * (TEST_RESULT_NOX + TEST_RESULT_NH3), subPointId.getResults().get(EmissionResultKey.NOXNH3_DEPOSITION), 1E-3,
        "Nitrogen deposition");
  }

  private void assertSummary(final String jobKey) throws SQLException, AeriusException {
    // Also check if summary is made
    final ResultsSummaryRepository summaryRepository = new ResultsSummaryRepository(getPMF());
    final int jobId = JobRepository.getJobId(getConnection(), jobKey);
    // Could use determineReceptorResultsSummary to check if summary is made, but this one requires less arguments.
    final JobSummary jobSummary = summaryRepository.determineJobSummary(jobId, mock(AreaInformationSupplier.class));
    // Just check if there's a project record, no need to check anything else.
    assertEquals(1, jobSummary.getProjectRecords().size(), "project records");
  }

  private String createJob() throws SQLException, AeriusException {
    return JobRepository.createJob(getConnection(), JobType.INSERT_RESULTS, false);
  }

  private ScenarioSituation createSituation(final SituationType situationType) {
    final ScenarioSituation situation = new ScenarioSituation();
    situation.setType(situationType);
    situation.setYear(2030);
    return situation;
  }

  private Scenario createScenario(final List<ScenarioSituation> situations) {
    final Scenario scenario = new Scenario();
    scenario.setSituations(situations);
    scenario.getOptions().setCalculationJobType(CalculationJobType.IN_COMBINATION_PROCESS_CONTRIBUTION);
    return scenario;
  }

  private List<CalculationPointFeature> createResults(final int factor) {
    final CalculationPointFeature feature = createBinnenveldResult(factor);
    return List.of(feature);
  }

  private CalculationPointFeature createBinnenveldResult(final int factor) {
    final CalculationPointFeature feature = new CalculationPointFeature();
    feature.setGeometry(new Point(3, 4));
    final ReceptorPoint receptorPoint = new ReceptorPoint();
    // Receptor in Binnenveld which is both relevant and exceeding
    final int receptorId = TEST_RECEPTOR_ID;
    receptorPoint.setReceptorId(receptorId);
    receptorPoint.setResults(Map.of(
        EmissionResultKey.NOX_DEPOSITION, TEST_RESULT_NOX * factor,
        EmissionResultKey.NH3_DEPOSITION, TEST_RESULT_NH3 * factor));
    feature.setProperties(receptorPoint);
    return feature;
  }

  private CalculationPointFeature createBinnenveldSubPointResult(final int factor) {
    final CalculationPointFeature feature = new CalculationPointFeature();
    feature.setGeometry(new Point(6, 7));
    final SubPoint subPoint = new SubPoint();
    // Receptor in Binnenveld which is both relevant and exceeding
    final int receptorId = TEST_RECEPTOR_ID_2;
    subPoint.setSubPointId(2);
    subPoint.setReceptorId(receptorId);
    subPoint.setLevel(1);
    subPoint.setResults(Map.of(
        EmissionResultKey.NOX_DEPOSITION, 5 * TEST_RESULT_NOX * factor,
        EmissionResultKey.NH3_DEPOSITION, 5 * TEST_RESULT_NH3 * factor));
    feature.setProperties(subPoint);
    return feature;
  }

}
