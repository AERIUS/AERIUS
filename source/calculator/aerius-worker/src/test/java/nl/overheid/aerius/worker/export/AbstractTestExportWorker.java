/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.export;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.junit.jupiter.api.AfterEach;
import org.mockito.Mock;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.aerius.taskmanager.client.TaskManagerClientSender;
import nl.overheid.aerius.calculation.Calculator;
import nl.overheid.aerius.calculation.CalculatorBuildDirector;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.db.calculator.JobRepository;
import nl.overheid.aerius.db.test.BaseTestDatabase;
import nl.overheid.aerius.http.FileServerProxy;
import nl.overheid.aerius.http.MockHttpClientProxy;
import nl.overheid.aerius.http.MockHttpClientProxy.HttpUriRequestCaptor;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.JobProgress;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.ops.DiurnalVariation;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.test.TestDomain;
import nl.overheid.aerius.util.HttpClientManager;
import nl.overheid.aerius.util.ScenarioFileServerUtil;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.WorkerConnectionHelper;

/**
 * Test class for specific ExportHandler implementations wrapping them in a {@link CalculatorExportWorker}.
 */
abstract class AbstractTestExportWorker extends BaseTestDatabase {

  protected static final TestDomain TEST_DOMAIN = new TestDomain();
  protected static final String FILE_CODE = "1234";

  private static final String DUMMY_EMAIL_ADDRESS = "aerius@example.com";

  protected final MockHttpClientProxy httpClientProxy = new MockHttpClientProxy();
  protected CalculatorExportWorker worker;
  protected HttpClientManager httpClientManager;
  protected String entityContent;
  protected HttpUriRequestCaptor httpUriRequestCaptor = new HttpUriRequestCaptor() {
    @Override
    public void set(final HttpUriRequest httpUriRequest) {
      try {
        final byte[] bytes = ((HttpPut) httpUriRequest).getEntity().getContent().readAllBytes();
        entityContent = new String(bytes, StandardCharsets.UTF_8);
      } catch (UnsupportedOperationException | IOException e) {
        new RuntimeException(e);
      }
      super.set(httpUriRequest);
    }
  };

  protected @Mock ScenarioFileServerUtil scenarioFileServiceUtil;
  protected @Mock CalculatorBuildDirector calculatorBuildDirector;
  protected @Mock WorkerConnectionHelper workerConnectionHelper;
  protected @Mock CalculationJob calculatorJob;
  protected @Mock Calculator calculator;
  protected @Mock RegisterResultsInserter registerResultsInserter;

  private @Mock ScenarioCalculations scenarioCalculations;
  private @Mock TaskManagerClientSender taskmanagerClient;

  protected File zipFilesDirectory;
  protected FileServerProxy fileServerProxy;
  protected JobIdentifier jobIdentifier;
  protected GMLProvider gmlProvider;

  protected void setUpWorkerConnectionHelper() throws SQLException, IOException {
    when(workerConnectionHelper.getPMF()).thenReturn(getPMF());
    final BrokerConnectionFactory brokerConnectionFactory = mock(BrokerConnectionFactory.class);
    final Connection connection = mock(Connection.class);
    lenient().when(connection.createChannel()).thenReturn(mock(Channel.class));
    lenient().when(brokerConnectionFactory.getConnection()).thenReturn(connection);
    lenient().when(workerConnectionHelper.getBrokerConnectionFactory()).thenReturn(brokerConnectionFactory);
    lenient().when(workerConnectionHelper.createTaskManagerClient()).thenReturn(taskmanagerClient);
    gmlProvider = new GMLProvider(getPMF());
  }

  protected void setUpHttpClientManager() throws IOException {
    httpClientManager = httpClientProxy.mockStringResponse("", httpUriRequestCaptor);
    fileServerProxy = new FileServerProxy(httpClientManager, "http://localhost");
    lenient().when(workerConnectionHelper.createFileServerProxy(any())).thenReturn(fileServerProxy);
  }

  protected void setUpJobIdentifier() throws SQLException, AeriusException {
    jobIdentifier = new JobIdentifier(JobRepository.createJob(getConnection(), JobType.REPORT, false));
  }

  protected void setUp(final ExportEmailHandler exportEmailHandler) throws Exception {
    zipFilesDirectory = exportEmailHandler.setKeepFiles();
    setUpWorkerConnectionHelper();
    lenient().when(calculatorJob.getScenarioCalculations()).thenReturn(scenarioCalculations);
    lenient().when(calculator.calculate()).thenReturn(calculatorJob);
    lenient().when(calculatorBuildDirector.construct(any(), any())).thenReturn(calculator);
    worker = new CalculatorExportWorker(calculatorBuildDirector, workerConnectionHelper, exportEmailHandler, scenarioFileServiceUtil,
        registerResultsInserter);
  }

  @AfterEach
  void afterEach() {
    Stream.of(zipFilesDirectory.listFiles()).forEach(File::delete);
  }

  protected void assertJobState(final JobState expectedState) throws SQLException {
    final JobProgress progress = JobRepository.getProgress(getConnection(), jobIdentifier.getJobKey());

    assertEquals(expectedState, progress.getState(), "Should have expected job state");
  }

  protected CalculationInputData createExampleCalculationInputData(final JobIdentifier jobIdentifier) throws AeriusException {
    final CalculationInputData inputData = new CalculationInputData();
    inputData.setEmailAddress(DUMMY_EMAIL_ADDRESS);
    inputData.setExportType(ExportType.GML_WITH_RESULTS);
    final Scenario scenario = new Scenario(Theme.OWN2000);
    inputData.setScenario(scenario);
    scenario.getOptions().getSubstances().addAll(Substance.NOXNH3.hatch());
    scenario.getOptions().getEmissionResultKeys().add(EmissionResultKey.NOXNH3_DEPOSITION);

    final ScenarioSituation situation = new ScenarioSituation();
    scenario.getSituations().add(situation);
    situation.setType(SituationType.PROPOSED);
    situation.getEmissionSourcesList().addAll(getExampleEmissionSources());

    lenient().when(scenarioFileServiceUtil.retrieveScenario(eq(jobIdentifier.getJobKey()))).thenReturn(inputData.getScenario());

    return inputData;
  }

  protected void assertResults(final int expectedFiles, final File folderForJob) throws IOException, ZipException {
    assertEquals(expectedFiles, folderForJob.list().length, "Should have number of expected files");
    try (final ZipFile zipFile = new ZipFile(new File(zipFilesDirectory, zipFilesDirectory.list()[0]))) {
      int countZipContent = 0;

      for (final Enumeration<? extends ZipEntry> entries = zipFile.entries(); entries.hasMoreElements(); entries.nextElement()) {
        countZipContent++;
      }
      assertEquals(expectedFiles, countZipContent, "Should have number of expected files in zip");
    }
  }

  private static List<EmissionSourceFeature> getExampleEmissionSources() {
    final List<EmissionSourceFeature> sources = new ArrayList<>();

    sources.add(createExampleEmissionSources(TestDomain.SECTOR_DEFAULT));
    return sources;
  }

  private static EmissionSourceFeature createExampleEmissionSources(final Sector sector) {
    //source x: 122442 y: 473298
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    final int xCoord = 122442;
    final int yCoord = 473298;
    feature.setGeometry(new Point(xCoord, yCoord));
    feature.setId("1");
    final GenericEmissionSource source = new GenericEmissionSource();
    source.setLabel("SomeSource" + sector.getSectorId());
    source.setSectorId(sector.getSectorId());
    final OPSSourceCharacteristics characteristics = new OPSSourceCharacteristics();
    characteristics.setHeatContent(0.22);
    characteristics.setEmissionHeight(40.0);
    characteristics.setSpread(20.0);
    characteristics.setDiurnalVariation(DiurnalVariation.INDUSTRIAL_ACTIVITY);
    characteristics.setParticleSizeDistribution(1);
    source.setCharacteristics(characteristics);
    source.getEmissions().put(Substance.NOX, 1000.0);
    source.getEmissions().put(Substance.NH3, 1000.0);
    feature.setProperties(source);
    return feature;
  }

}
