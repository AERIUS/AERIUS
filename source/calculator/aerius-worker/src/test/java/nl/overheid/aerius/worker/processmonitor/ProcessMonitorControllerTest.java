/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.processmonitor;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.rabbitmq.client.AMQP.Queue.DeclareOk;
import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.DeliverCallback;

import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.processmonitor.ProcessMonitorService;
import nl.overheid.aerius.worker.WorkerConnectionHelper;

/**
 * Test class for {@link ProcessMonitorController}.
 */
@ExtendWith(MockitoExtension.class)
class ProcessMonitorControllerTest {

  private @Mock ScheduledExecutorService scheduler;
  private @Mock BrokerConnectionFactory connectionFactory;
  private @Mock ProcessMonitorService processMonitorService;
  private @Mock WorkerConnectionHelper workerConnectionHelper;
  private @Captor ArgumentCaptor<Runnable> cleanUpCaptor;

  private ProcessMonitorController processMonitorController;
  private Channel channel;

  @BeforeEach
  void beforeEach() throws IOException {
    final Connection connection = mock(Connection.class);
    channel = mock(Channel.class);
    final DeclareOk declareOk = mock(DeclareOk.class);
    doReturn(declareOk).when(channel).queueDeclare();
    doReturn(channel).when(connection).createChannel();
    doReturn(connection).when(connectionFactory).getConnection();
    doReturn(connectionFactory).when(workerConnectionHelper).getBrokerConnectionFactory();
    doReturn(processMonitorService).when(workerConnectionHelper).getProcessMonitorService();
    processMonitorController = new ProcessMonitorController(workerConnectionHelper, scheduler);
  }

  @Test
  void testRun() throws IOException {
    final ScheduledFuture<?> future = mock(ScheduledFuture.class);

    doReturn(future).when(scheduler).scheduleWithFixedDelay(cleanUpCaptor.capture(), anyLong(), anyLong(), any());
    processMonitorController.run();
    verify(scheduler).scheduleWithFixedDelay(any(Runnable.class), anyLong(), anyLong(), any());
    verify(channel).basicConsume(any(), eq(true), any(DeliverCallback.class), any(CancelCallback.class));
    // Mock call to cleanup task to verify if expected calls are made
    cleanUpCaptor.getValue().run();
    verify(processMonitorService).killAllCancelledJobs();
    verify(processMonitorService).cleanCache(anyLong());
  }
}
