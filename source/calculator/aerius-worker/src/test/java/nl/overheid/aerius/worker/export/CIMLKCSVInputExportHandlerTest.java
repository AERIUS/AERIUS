/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.export;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.WorkerUtil;

/**
 * Test class for {@link CIMLKCSVInputExportHandler} and {@link GMLInputExportHandler}.
 * Test both in single unit test because they're used to together in CIMLK export.
 */
@ExtendWith(MockitoExtension.class)
class CIMLKCSVInputExportHandlerTest extends AbstractTestExportWorker {

  // Expect 3 files for CIMLKCSVInputExportHandler, and 1 for GMLInputExportHandler.
  private static final int EXPECTED_NUMBER_OF_FILES = 3 + 1;

  private static final String TEST_DATE_STRING = "20150701000000";
  private static final String EXPECTED_ZIP_FILENAME = "AERIUS_CIMLK_TEST_" + TEST_DATE_STRING + ".zip";
  private static final Date TEST_DATE = new Date(115, 6, 1, 0, 0, 0);
  private File folderForJob;

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    setUpWorkerConnectionHelper();
    setUpHttpClientManager();
    setUpJobIdentifier();
    folderForJob = WorkerUtil.getFolderForJob(jobIdentifier);
    final DirectoryExportHandler directoryExportHandler = new DirectoryExportHandler(folderForJob, "CIMLK_TEST",
        new CIMLKCSVInputExportHandler(), new GMLInputExportHandler(getPMF()));
    setUp(new ExportEmailHandler(workerConnectionHelper, new ExportZipProcessor(directoryExportHandler, fileServerProxy), gmlProvider));
  }

  @Test
  void testSuccesfullExport() throws Exception {
    final CalculationInputData inputData = createExampleCalculationInputData(jobIdentifier);
    inputData.setCreationDate(TEST_DATE);
    assertExport(inputData, EXPECTED_NUMBER_OF_FILES);
  }

  private void assertExport(final CalculationInputData inputData, final int expectedFiles) throws AeriusException, URISyntaxException, Exception {
    worker.run(inputData, jobIdentifier, null);
    assertResults(expectedFiles, folderForJob, inputData);
    assertJobState(JobState.POST_PROCESSING);
  }

  private void assertResults(final int expectedFiles, final File folderForJob2, final CalculationInputData inputData)
      throws IOException, ZipException {
    assertEquals(expectedFiles, folderForJob.list().length, "Should have number of expected files");

    try (final ZipFile zipFile = new ZipFile(new File(zipFilesDirectory, zipFilesDirectory.list()[0]))) {
      assertEquals(EXPECTED_ZIP_FILENAME, processFile(zipFile.getName()), "Expected name of the zip file.");
      int countZipContent = 0;

      final Enumeration<? extends ZipEntry> entries = zipFile.entries();
      final List<String> names = new ArrayList<>();

      while (entries.hasMoreElements()) {
        names.add(processFile(entries.nextElement().getName()));
        countZipContent++;
      }
      assertEquals(expectedFiles, countZipContent, "Should have number of expected files in zip");
      final String id = inputData.getScenario().getSituations().get(0).getId();
      final List<String> expectedContentNames = List.of(
          String.format("%s_cimlk_calculation_points_%s.csv", id, TEST_DATE_STRING),
          String.format("%s_cimlk_srm1_roads_%s.csv", id, TEST_DATE_STRING),
          String.format("%s_cimlk_srm2_roads_%s.csv", id, TEST_DATE_STRING), String.format("AERIUS_%s_0.gml", TEST_DATE_STRING));
      assertEquals(expectedContentNames, names.stream().sorted().toList(), "Expected names of the zip file.");
    }
  }

  private String processFile(final String name) {
    return new File(name).getName();
  }
}
