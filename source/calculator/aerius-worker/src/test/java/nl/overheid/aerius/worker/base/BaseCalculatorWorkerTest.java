/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.base;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.db.calculator.JobRepository;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.ScenarioFileServerUtil;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.RequeueTaskException;
import nl.overheid.aerius.worker.WorkerConnectionHelper;

/**
 * Test class for {@link BaseCalculatorWorker}.
 */
@ExtendWith(MockitoExtension.class)
class BaseCalculatorWorkerTest extends BaseDBTest {

  private static final int EXPECTED_REQUEUES = 10;

  private @Mock ScenarioFileServerUtil scenarioFileServerUtil;
  private @Mock CalculationInputData inputData;
  private @Mock WorkerConnectionHelper workerConnectionHelper;
  private String jobKey;

  @BeforeEach
  void beforeEach() throws SQLException, AeriusException, IOException {
    doReturn(getPMF()).when(workerConnectionHelper).getPMF();
    final BrokerConnectionFactory brokerConnectionFactory = mock(BrokerConnectionFactory.class);
    lenient().doReturn(brokerConnectionFactory).when(workerConnectionHelper).getBrokerConnectionFactory();
    final Connection rabbitMQConnection = mock(Connection.class);
    lenient().doReturn(rabbitMQConnection).when(brokerConnectionFactory).getConnection();
    lenient().doReturn(mock(Channel.class)).when(rabbitMQConnection).createChannel();
    jobKey = JobRepository.createJob(getConnection(), JobType.CALCULATION, false);
  }

  @Test
  void testBasic() throws Exception {
    final BaseCalculatorWorker worker = new BaseCalculatorWorker(workerConnectionHelper, scenarioFileServerUtil) {

      @Override
      protected boolean process(final CalculationInputData inputData, final JobIdentifier jobIdentifier) throws Exception {
        return true;
      }
    };

    final JobIdentifier jobIdentifier = new JobIdentifier(jobKey);
    assertTrue(worker.run(inputData, jobIdentifier, null), "Worker process should return true when returning true in implementation");
    assertTrue(worker.requeueAttempts.isEmpty(), "Nothing requeued");
  }

  @Test
  void testBasicProcessFalse() throws Exception {
    final BaseCalculatorWorker worker = new BaseCalculatorWorker(workerConnectionHelper, scenarioFileServerUtil) {

      @Override
      protected boolean process(final CalculationInputData inputData, final JobIdentifier jobIdentifier) throws Exception {
        // False in this case indicates that the work is not completed successfully. The task should still be considered finished however.
        return false;
      }
    };

    final JobIdentifier jobIdentifier = new JobIdentifier(jobKey);
    assertTrue(worker.run(inputData, jobIdentifier, null), "Worker process should return true even when returning false in implementation");
    assertTrue(worker.requeueAttempts.isEmpty(), "Nothing requeued");
  }

  @Test
  void testRequeueOnce() throws Exception {
    final BaseCalculatorWorker worker = new BaseCalculatorWorker(workerConnectionHelper, scenarioFileServerUtil) {

      boolean requeued;

      @Override
      protected boolean process(final CalculationInputData inputData, final JobIdentifier jobIdentifier) throws Exception {
        if (!requeued) {
          requeued = true;
          throw new RequeueTaskException("Mocking requeue");
        } else {
          return true;
        }
      }
    };

    final JobIdentifier jobIdentifier = new JobIdentifier(jobKey);
    assertThrows(RequeueTaskException.class, () -> worker.run(inputData, jobIdentifier, null), "Should requeue at first attempt");
    // When requeuing, the scenario shouldn't be removed.
    verify(scenarioFileServerUtil, never()).removeScenario(jobKey);
    assertEquals(1, worker.requeueAttempts.size(), "1 job requeued");

    assertTrue(worker.run(inputData, jobIdentifier, null), "Should work OK 2nd time");
    assertTrue(worker.requeueAttempts.isEmpty(), "Nothing requeued (should be removed again)");
  }

  @Test
  void testRequeueTooManyTimes() throws Exception {
    final BaseCalculatorWorker worker = new BaseCalculatorWorker(workerConnectionHelper, scenarioFileServerUtil) {

      @Override
      protected boolean process(final CalculationInputData inputData, final JobIdentifier jobIdentifier) throws Exception {
        throw new RequeueTaskException("Mocking requeue");
      }
    };

    final JobIdentifier jobIdentifier = new JobIdentifier(jobKey);
    for (int i = 1; i <= EXPECTED_REQUEUES; i++) {
      assertThrows(RequeueTaskException.class, () -> worker.run(inputData, jobIdentifier, null), "Should requeue at attempt " + i);
    }

    // When requeuing, the scenario should never be removed.
    verify(scenarioFileServerUtil, never()).removeScenario(jobKey);
    assertEquals(1, worker.requeueAttempts.size(), "1 job requeued");

    final JobIdentifier someOtherJob = new JobIdentifier(JobRepository.createJob(getConnection(), JobType.CALCULATION, false));
    assertThrows(RequeueTaskException.class, () -> worker.run(inputData, someOtherJob, null), "Should requeue another job");
    assertEquals(2, worker.requeueAttempts.size(), "after different job requeued");

    assertThrows(AeriusException.class, () -> worker.run(inputData, jobIdentifier, null), "Should not requeue at attempt " + (EXPECTED_REQUEUES + 1));
    verify(scenarioFileServerUtil, times(1)).removeScenario(jobKey);
    assertEquals(1, worker.requeueAttempts.size(), "First job should now be removed from the map, to avoid hogging memory");
  }

}
