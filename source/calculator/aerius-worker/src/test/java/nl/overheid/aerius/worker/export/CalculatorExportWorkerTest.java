/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.export;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.util.HashSet;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.db.calculator.JobRepository;
import nl.overheid.aerius.email.message.EmailMessage;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioResults;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituationResults;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Test class {@link CalculatorExportWorker} using a mocked {@link ExportEmailHandler}.
 */
@ExtendWith(MockitoExtension.class)
class CalculatorExportWorkerTest extends AbstractTestExportWorker {

  private @Mock ExportEmailHandler exportEmailHandler;
  private @TempDir File tempZipfileDir;
  private @Captor ArgumentCaptor<EmailMessage<?>> captor;

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    setUpJobIdentifier();
    when(exportEmailHandler.setKeepFiles()).thenReturn(tempZipfileDir);
    setUp(exportEmailHandler);
  }

  @ParameterizedTest
  @CsvSource({"true", "false"})
  void testSuccesfullRun(final boolean sendEmail) throws Exception {
    final CalculationInputData inputData = createExampleCalculationInputData(jobIdentifier);
    if (!sendEmail) {
      inputData.setAdditionalOptions(new HashSet<>());
    }
    assertRun(inputData);
    assertJobState(sendEmail ? JobState.POST_PROCESSING : JobState.COMPLETED);
  }

  @Test
  void testHandleWorkLoadGMLInputCancelledJob() throws Exception {
    final CalculationInputData inputData = createExampleCalculationInputData(jobIdentifier);
    final AeriusException cancelledException = new AeriusException(AeriusExceptionReason.CONNECT_JOB_CANCELLED);

    when(scenarioFileServiceUtil.retrieveScenario(any())).then(input -> {
      JobRepository.updateJobStatus(getConnection(), jobIdentifier.getJobKey(), JobState.CANCELLED);
      throw cancelledException;
    });
    final AeriusException thrownException = assertThrows(AeriusException.class, () -> worker.run(inputData, jobIdentifier, null),
        "Run should throw a cancelled job exception.");
    assertEquals(AeriusExceptionReason.CONNECT_JOB_CANCELLED, thrownException.getReason(), "Throw should have a cancelled job reason.");
    assertJobState(JobState.CANCELLED);
  }

  @Test
  void testFailedScenarioRetrieval() throws Exception {
    final CalculationInputData inputData = createExampleCalculationInputData(jobIdentifier);

    when(scenarioFileServiceUtil.retrieveScenario(any())).then(input -> {
      throw new AeriusException(AeriusExceptionReason.IMPORTED_FILE_NOT_FOUND);
    });
    final Exception exception = assertThrows(AeriusException.class, () -> worker.run(inputData, jobIdentifier, null),
        "Should throw AeriusException with Imported file not found reason.");
    verify(exportEmailHandler).emailException(any(), eq(jobIdentifier), eq(exception));
    assertJobState(JobState.ERROR);
  }

  @Test
  void testExportGMLSourcesOnly() throws Exception {
    final CalculationInputData inputData = createExampleCalculationInputData(ExportType.GML_SOURCES_ONLY);

    assertRun(inputData);
    verify(calculatorBuildDirector, never()).construct(any(), any());
    verify(scenarioFileServiceUtil).removeScenario(jobIdentifier.getJobKey());
  }

  @Test
  void testExportGMLWithResultsProvided() throws Exception {
    final CalculationInputData inputData = createExampleCalculationInputData(ExportType.GML_WITH_RESULTS);
    setScenarioResults(inputData);
    assertRun(inputData);
    verify(calculatorBuildDirector, never()).construct(any(), any());
    verify(scenarioFileServiceUtil).removeScenario(jobIdentifier.getJobKey());
  }

  @Test
  void testExportPDFWithResultsProvided() throws Exception {
    final CalculationInputData inputData = createExampleCalculationInputData(ExportType.PDF_PAA);
    setScenarioResults(inputData);
    assertRun(inputData);
    verify(calculatorBuildDirector, never()).construct(any(), any());
    verify(scenarioFileServiceUtil, never()).removeScenario(jobIdentifier.getJobKey());
  }

  private static void setScenarioResults(final CalculationInputData inputData) {
    final ScenarioResults scenarioResults = new ScenarioResults();
    final ScenarioSituationResults scenarioSituationResult = new ScenarioSituationResults();
    scenarioSituationResult.setCalculationId(1);
    scenarioResults.getResultsPerSituation().put("1", scenarioSituationResult);
    inputData.setScenarioResults(scenarioResults);
  }

  @Test
  void testExportPDF() throws Exception {
    final CalculationInputData inputData = createExampleCalculationInputData(ExportType.PDF_PAA);
    assertRun(inputData);
    verify(calculatorBuildDirector).construct(any(), any());
    verify(scenarioFileServiceUtil, never()).removeScenario(jobIdentifier.getJobKey());
  }

  @Test
  void testExportRegisterPdf() throws Exception {
    final CalculationInputData inputData = createExampleCalculationInputData(ExportType.REGISTER_PDF_PAA_DEMAND);
    assertRun(inputData);
    verify(registerResultsInserter).insertRegisterResults(jobIdentifier.getJobKey(), inputData.getScenario());
    verify(calculatorBuildDirector, never()).construct(any(), any());
    verify(scenarioFileServiceUtil, never()).removeScenario(jobIdentifier.getJobKey());
  }

  private CalculationInputData createExampleCalculationInputData(final ExportType exportType) throws AeriusException {
    final CalculationInputData inputData = createExampleCalculationInputData(jobIdentifier);

    inputData.setExportType(exportType);
    return inputData;
  }

  private void assertRun(final CalculationInputData inputData) throws Exception {
    assertEquals(Boolean.TRUE, worker.run(inputData, jobIdentifier, null), "Expected worker to return true");

  }
}
