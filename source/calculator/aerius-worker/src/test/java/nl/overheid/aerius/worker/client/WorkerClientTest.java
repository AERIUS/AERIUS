/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.client;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

import com.rabbitmq.client.ShutdownSignalException;

import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.aerius.taskmanager.test.MockConnection;
import nl.overheid.aerius.taskmanager.client.WorkerHandler;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Test class for {@link WorkerClientPool}.
 */
class WorkerClientTest {

  private static final WorkerType WORKER_TYPE_TEST = WorkerType.TEST;

  private static ExecutorService executor;
  private WorkerClientPool workerClient;
  private BrokerConnectionFactory factory;

  @BeforeAll
  static void setupClass() {
    executor = Executors.newFixedThreadPool(4);
  }

  @AfterAll
  static void afterClass() {
    executor.shutdown();
  }

  @BeforeEach
  void setUp() throws Exception {
    factory = new BrokerConnectionFactory(executor) {
      @Override
      protected com.rabbitmq.client.Connection createNewConnection() throws IOException {
        return new MockConnection();
      };
    };
  }

  @AfterEach
  void tearDown() throws Exception {
    if (workerClient != null) {
      workerClient.stopAllConsuming();
    }
  }

  /**
   * Test method for {@link nl.overheid.aerius.worker.client.WorkerClientPool#startConsuming(int)}.
   */
  @Test
  @Timeout(value = 5, unit = TimeUnit.SECONDS)
  void testStartConsuming() throws IOException {
    workerClient = createPool(1);
    assertEquals(1, workerClient.getNumberOfWorkers(), "Number of workers that should be there");
    sleepAfterStartConsuming();
    int retry = 10;
    while (retry > 0 && workerClient.getCurrentNumberOfWorkers() != 1) {
      try {
        Thread.sleep(100);
      } catch (final InterruptedException e) {
      }
      retry--;
    }
    assertEquals(1, workerClient.getCurrentNumberOfWorkers(), "Number of current workers");
    assertEquals(1, workerClient.getNumberOfWorkers(), "Number of workers that should be there");
  }

  /**
   * Test method for {@link nl.overheid.aerius.worker.client.WorkerClientPool#startConsuming(int)}.
   */
  @Test
  @Timeout(value = 5, unit = TimeUnit.SECONDS)
  void testShutdownSignal() throws IOException {
    testStartConsuming();
    workerClient.shutdownCompleted(new ShutdownSignalException(true, false, null, null));
    assertEquals(0, workerClient.getCurrentNumberOfWorkers(), "Number of current workers right away after shutdown");
    assertEquals(1, workerClient.getNumberOfWorkers(), "Number of workers that should be there");
    sleepAfterStartConsuming();
    assertEquals(1, workerClient.getCurrentNumberOfWorkers(), "Number of current workers some time after shutdown");
  }

  @Test
  @Timeout(value = 3, unit = TimeUnit.SECONDS)
  void testWorkerClientWithoutWorkerHandler() throws IOException {
    assertThrows(IllegalArgumentException.class, () -> new WorkerClientPool(executor, factory, null, WORKER_TYPE_TEST, 1),
        "Should throw an exception when creating a WorkerClientPool without WorkHandler");
  }

  @Test
  @Timeout(value = 3, unit = TimeUnit.SECONDS)
  void testWorkerClientWithoutQueueNaming() throws IOException {
    final WorkerHandler workerHandler = new MockWorkerHandler();

    assertThrows(IllegalArgumentException.class, () -> new WorkerClientPool(executor, factory, workerHandler, null, 1),
        "Should throw an exception when creating a WorkHandler without queue name");
  }

  private void sleepAfterStartConsuming() {
    try {
      //start consuming happens on different thread.
      Thread.sleep(2000);
    } catch (final InterruptedException e) {
    }
  }

  private WorkerClientPool createPool(final int workers) {
    final WorkerClientPool client = new WorkerClientPool(executor, factory, new MockWorkerHandler(), WORKER_TYPE_TEST, workers, Optional.of(1));

    executor.execute(client);
    return client;
  }

}
