/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.export;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.aerius.taskmanager.client.TaskManagerClientSender;
import nl.overheid.aerius.calculation.Calculator;
import nl.overheid.aerius.calculation.CalculatorBuildDirector;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.http.FileServerProxy;
import nl.overheid.aerius.http.MockHttpClientProxy;
import nl.overheid.aerius.http.MockHttpClientProxy.HttpUriRequestCaptor;
import nl.overheid.aerius.shared.Constants;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationMethod;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.export.ExportData.ExportAdditionalOptions;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.test.TestDomain;
import nl.overheid.aerius.util.HttpClientManager;
import nl.overheid.aerius.util.ScenarioFileServerUtil;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.WorkerConnectionHelper;

/**
 * Test class for {@link ExportEmailHandler}.
 */
@ExtendWith(MockitoExtension.class)
class ExportEmailHandlerTest extends BaseDBTest {

  private static final String DUMMY_EMAIL_ADDRESS = "aerius@example.com";
  private static final Pattern ZIP_FILENAME_PATTERN = Pattern.compile(".*/(.*\\.zip)");
  private static final String ZIP_FILENAME = "zipfilename.zip";
  private static final String EXTERNAL_DOWNLOAD_URL = "http://localhost/";

  private final MockHttpClientProxy httpClientProxy = new MockHttpClientProxy();
  private CalculatorExportWorker worker;
  private HttpClientManager httpClientManager;
  private HttpPut httpPutRequest;
  private final HttpUriRequestCaptor httpUriRequestCaptor = new HttpUriRequestCaptor() {
    @Override
    public void set(final HttpUriRequest httpUriRequest) {
      try {
        httpPutRequest = (HttpPut) httpUriRequest;
      } catch (final UnsupportedOperationException e) {
        new RuntimeException(e);
      }
      super.set(httpUriRequest);
    };
  };
  private @Mock ExportHandler exportHandler;
  private @Mock GMLProvider gmlProvider;
  private @Mock ScenarioFileServerUtil scenarioFileServiceUtil;
  private @Mock ExportEmailHandler exportEmailHandler;
  private @Mock CalculatorBuildDirector calculatorBuildDirector;
  private @Mock WorkerConnectionHelper workerConnectionHelper;
  private @Mock TaskManagerClientSender taskManagerClient;
  private @Mock Calculator calculator;
  private @Mock CalculationJob calculatorJob;
  private @Mock ScenarioCalculations scenarioCalculations;
  private @TempDir File tempDir;
  private TestDomain testDomain;
  private FileServerProxy fileServerProxy;

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    super.setUp();
    testDomain = new TestDomain(getCategories(getPMF()));

    when(workerConnectionHelper.getPMF()).thenReturn(getPMF());
    httpClientManager = httpClientProxy.mockStringResponse("", httpUriRequestCaptor);
    fileServerProxy = new FileServerProxy(httpClientManager, EXTERNAL_DOWNLOAD_URL);
    when(workerConnectionHelper.createTaskManagerClient()).thenReturn(taskManagerClient);
    when(exportHandler.createFileContent(any(), any())).thenReturn(tempDir);
  }

  @Test
  void testRun() throws Exception {
    final CalculationInputData inputData = createExportData();
    when(exportHandler.getZipFileName(any())).thenReturn(ZIP_FILENAME);
    final ExportEmailHandler handler = new ExportEmailHandler(workerConnectionHelper, new ExportZipProcessor(exportHandler, fileServerProxy),
        gmlProvider);

    handler.process(inputData, new JobIdentifier("SomeCorrelation"), null);

    final Matcher matcher = ZIP_FILENAME_PATTERN.matcher(httpPutRequest.getURI().toString());
    matcher.find();
    assertEquals(ZIP_FILENAME, matcher.group(1), "Entity posted to the file server should contain the zip file.");
    verify(taskManagerClient, times(1)).sendTask(any(), any(), any(), any(), any(), any());
  }

  @Test
  void testRunWithoutEmail() throws Exception {
    final CalculationInputData inputData = createExportData();
    inputData.getAdditionalOptions().remove(ExportAdditionalOptions.EMAIL_USER);
    when(exportHandler.getZipFileName(any())).thenReturn(ZIP_FILENAME);
    final ExportEmailHandler handler = new ExportEmailHandler(workerConnectionHelper, new ExportZipProcessor(exportHandler, fileServerProxy),
        gmlProvider);

    handler.process(inputData, new JobIdentifier("SomeCorrelation"), null);

    final Matcher matcher = ZIP_FILENAME_PATTERN.matcher(httpPutRequest.getURI().toString());
    matcher.find();
    assertEquals(ZIP_FILENAME, matcher.group(1), "Entity posted to the file server should contain the zip file.");
    verify(taskManagerClient, never()).sendTask(any(), any(), any(), any(), any(), any());
  }

  private CalculationInputData createExportData() throws SQLException {
    final CalculationInputData inputData = new CalculationInputData();
    inputData.setEmailAddress(DUMMY_EMAIL_ADDRESS);
    inputData.setLocale(Constants.LOCALE_NL);
    inputData.setExportType(ExportType.GML_WITH_RESULTS);
    inputData.setQueueName("Never_gonna_find_me");

    final Scenario scenario = new Scenario(Theme.OWN2000);
    final ScenarioSituation situation = new ScenarioSituation();
    scenario.getSituations().add(situation);
    situation.getEmissionSourcesList().add(TestDomain.getSource("1", new Point(100, 200), "farm 1", testDomain.getFarmEmissionSource()));
    situation.getEmissionSourcesList().add(TestDomain.getSource("2", new Point(300, 400), "farm 2", testDomain.getFarmEmissionSource()));
    situation.setName("TestSituation");
    situation.setYear(TestDomain.YEAR);
    situation.setType(SituationType.PROPOSED);
    scenario.getOptions().setCalculationMethod(CalculationMethod.FORMAL_ASSESSMENT);
    scenario.getOptions().getSubstances().add(Substance.NOXNH3);
    scenario.getOptions().getEmissionResultKeys().add(EmissionResultKey.NOXNH3_DEPOSITION);
    inputData.setScenario(scenario);
    return inputData;
  }
}
