/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.export;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.db.calculator.JobRepository;
import nl.overheid.aerius.shared.domain.calculation.JobProgress;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.util.HttpClientManager;

/**
 * Test class for {@link ModelExportHandler} in combination with {@link CalculatorExportWorker}.
 */
@ExtendWith(MockitoExtension.class)
class ModelExportHandlerTest extends AbstractTestExportWorker {

  private static final String EXTERNAL_FILESERVICE_URL = "http://localhost";

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    setUpWorkerConnectionHelper();
    httpClientManager = Mockito.mock(HttpClientManager.class);
    setUpJobIdentifier();
    setUp(new ExportEmailHandler(workerConnectionHelper, new ExportZipProcessor(new ModelExportHandler(), fileServerProxy), gmlProvider));
  }

  @Test
  void testModelExport() throws Exception {
    final CalculationInputData inputData = createExampleCalculationInputData(jobIdentifier);
    when(calculatorJob.getResultUrl()).thenReturn(EXTERNAL_FILESERVICE_URL);

    worker.run(inputData, jobIdentifier, null);
    final JobProgress progress = JobRepository.getProgressWithoutDeleted(getConnection(), jobIdentifier.getJobKey());

    verifyNoInteractions(httpClientManager);
    assertEquals(EXTERNAL_FILESERVICE_URL, progress.getResultUrl(), "Url to download should be stored in the database");
    assertJobState(JobState.POST_PROCESSING);
  }
}
