/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Properties;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * Test class for {@link CalculatorWorkerFactory}.
 */
@ExtendWith(MockitoExtension.class)
class CalculatorWorkerFactoryTest {

  private static final String ARCHIVESERVER_URL = "https://archiveserver.url";

  @Test
  void testConfigurationBuilder() throws Exception {
    final CalculatorWorkerFactory factory = new CalculatorWorkerFactory();
    final Properties props = new Properties();
    props.put("chunker.processes", "1");
    props.put("archiveserver.url", ARCHIVESERVER_URL);
    props.put("archiveserver.timeout", "200");
    final CalculatorConfiguration config = factory.configurationBuilder(props).buildAndValidate().get();

    assertEquals(1, config.getProcesses(), "Should have 1 Chunker process");
    assertEquals(ARCHIVESERVER_URL, config.getArchiveConfiguration().getArchiveUrl(), "Should have Archive server url");
    assertEquals(200, config.getArchiveConfiguration().getArchiveTimeout(), "Should have Archive timeout");
  }
}
