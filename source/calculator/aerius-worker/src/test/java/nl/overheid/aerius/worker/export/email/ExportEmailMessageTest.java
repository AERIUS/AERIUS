/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.export.email;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.aerius.taskmanager.client.TaskManagerClientSender;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.email.message.EmailMessage;
import nl.overheid.aerius.email.message.EmailMessageTaskSender;
import nl.overheid.aerius.shared.domain.AeriusCustomer;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.email.EmailMessagesEnum;
import nl.overheid.aerius.shared.domain.email.MailMessageData;
import nl.overheid.aerius.shared.domain.email.ReplacementToken;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioMetaData;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.QueueEnum;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Test class for Export {@link EmailMessage} instances.
 */
@ExtendWith(MockitoExtension.class)
class ExportEmailMessageTest {

  private static final String LOCALE = "en";
  private static final String FILE_DOWNLOAD_EXTERN_URL = "http://url";
  private static final String PROJECT_NAME = "Project Name";
  private static final String REFERENCE = "1234";
  private static final Date CREATION_DATE = new Date(0);
  private static final String CREATION_DATE_STRING = "Thursday 01 January 1970";
  private static final String CREATION_TIME_STRING = "01:00";

  private static final Map<ReplacementToken, String> DEFAULT_STRING_REPLACEMENTS = Map.of(
      ReplacementToken.CREATION_DATE, CREATION_DATE_STRING, ReplacementToken.CREATION_TIME, CREATION_TIME_STRING);

  private @Mock TaskManagerClientSender taskManagerClient;
  private @Captor ArgumentCaptor<MailMessageData> captor;

  @ParameterizedTest
  @MethodSource("data")
  void testExportEmailMessage(final CalculationInputData data, final Map<ReplacementToken, Enum<?>> expectedEnumReplacementTokens,
      final Map<ReplacementToken, String> expectedReplacements) throws AeriusException, IOException {
    final EmailMessage<CalculationInputData> emailMessage = ExportEmailMessageFactory.create(data, new JobIdentifier(REFERENCE));

    if (expectedReplacements.isEmpty()) {
      assertNull(emailMessage, "If not expected tokens in replace input no email is expected and emailMessage should be null");
      return;
    }
    final EmailMessageTaskSender sender = new EmailMessageTaskSender(taskManagerClient, AeriusCustomer.RIVM);
    sender.sendMailToUser(emailMessage, FILE_DOWNLOAD_EXTERN_URL, "1234");
    verify(taskManagerClient).sendTask(captor.capture(), any(), any(), isNull(), eq(WorkerType.MESSAGE.type()), eq(QueueEnum.MESSAGE.getQueueName()));
    final MailMessageData actualMessage = captor.getValue();

    assertEquals(expectedEnumReplacementTokens.keySet(), actualMessage.getEnumReplacementTokens().keySet(),
        "Expected enum tokens should be the same");
    assertEquals(expectedReplacements, actualMessage.getReplacements(), "Expected replace enum tokens should be the same");
  }

  private static List<Object[]> data() {
    final List<Object[]> objects = new ArrayList<>();
    // PDF export
    objects.add(input(ExportType.PDF_PAA, PROJECT_NAME, Theme.OWN2000, Map.of(
        ReplacementToken.EXPORT_TYPE_FILE, EmailMessagesEnum.EXPORT_TYPE_FILE_PAA,
        ReplacementToken.ADDITIONAL_DOWNLOAD_INFO_1, EmailMessagesEnum.DOWNLOAD_IMAER_IMPORT,
        ReplacementToken.ADDITIONAL_DOWNLOAD_INFO_2, EmailMessagesEnum.DOWNLOAD_PDF_NO_ENGLISH,
        ReplacementToken.MAIL_SUBJECT, EmailMessagesEnum.DOWNLOAD_PAA_SUBJECT,
        ReplacementToken.MAIL_CONTENT,
        EmailMessagesEnum.DOWNLOAD_CONTENT),
        Map.of(ReplacementToken.DOWNLOAD_LINK, FILE_DOWNLOAD_EXTERN_URL, ReplacementToken.JOB, PROJECT_NAME,
            ReplacementToken.PROJECT_NAME, PROJECT_NAME, ReplacementToken.REFERENCE, REFERENCE)));
    // GML export
    objects.add(input(ExportType.GML_WITH_RESULTS, "", Theme.OWN2000, Map.of(
        ReplacementToken.EXPORT_TYPE_FILE, EmailMessagesEnum.EXPORT_TYPE_FILE_GML,
        ReplacementToken.ADDITIONAL_DOWNLOAD_INFO_1, EmailMessagesEnum.DOWNLOAD_IMAER_IMPORT,
        ReplacementToken.MAIL_SUBJECT, EmailMessagesEnum.DOWNLOAD_SUBJECT,
        ReplacementToken.MAIL_CONTENT,
        EmailMessagesEnum.DOWNLOAD_CONTENT),
        Map.of(ReplacementToken.DOWNLOAD_LINK, FILE_DOWNLOAD_EXTERN_URL, ReplacementToken.JOB, "", ReplacementToken.PROJECT_NAME, "",
            ReplacementToken.REFERENCE, REFERENCE)));
    objects.add(input(ExportType.GML_WITH_RESULTS, PROJECT_NAME, Theme.OWN2000, Map.of(
        ReplacementToken.EXPORT_TYPE_FILE, EmailMessagesEnum.EXPORT_TYPE_FILE_GML,
        ReplacementToken.ADDITIONAL_DOWNLOAD_INFO_1, EmailMessagesEnum.DOWNLOAD_IMAER_IMPORT,
        ReplacementToken.MAIL_SUBJECT, EmailMessagesEnum.DOWNLOAD_SUBJECT_JOB,
        ReplacementToken.MAIL_CONTENT,
        EmailMessagesEnum.DOWNLOAD_CONTENT),
        Map.of(ReplacementToken.DOWNLOAD_LINK, FILE_DOWNLOAD_EXTERN_URL, ReplacementToken.JOB, PROJECT_NAME, ReplacementToken.PROJECT_NAME,
            PROJECT_NAME, ReplacementToken.REFERENCE, REFERENCE)));
    // CSV export
    objects.add(input(ExportType.CSV, "", Theme.OWN2000, Map.of(
        ReplacementToken.EXPORT_TYPE_FILE, EmailMessagesEnum.EXPORT_TYPE_FILE_CSV,
        ReplacementToken.MAIL_SUBJECT, EmailMessagesEnum.DOWNLOAD_SUBJECT,
        ReplacementToken.MAIL_CONTENT,
        EmailMessagesEnum.DOWNLOAD_CONTENT),
        Map.of(ReplacementToken.DOWNLOAD_LINK, FILE_DOWNLOAD_EXTERN_URL, ReplacementToken.JOB, "", ReplacementToken.PROJECT_NAME, "",
            ReplacementToken.REFERENCE, REFERENCE)));
    objects.add(input(ExportType.CSV, PROJECT_NAME, Theme.OWN2000, Map.of(
        ReplacementToken.EXPORT_TYPE_FILE, EmailMessagesEnum.EXPORT_TYPE_FILE_CSV,
        ReplacementToken.MAIL_SUBJECT, EmailMessagesEnum.DOWNLOAD_SUBJECT_JOB,
        ReplacementToken.MAIL_CONTENT,
        EmailMessagesEnum.DOWNLOAD_CONTENT),
        Map.of(ReplacementToken.DOWNLOAD_LINK, FILE_DOWNLOAD_EXTERN_URL, ReplacementToken.JOB, PROJECT_NAME, ReplacementToken.PROJECT_NAME,
            PROJECT_NAME, ReplacementToken.REFERENCE, REFERENCE)));
    // CIMLK CSV export
    objects.add(input(ExportType.CIMLK_CSV, "", Theme.CIMLK, Map.of(
        ReplacementToken.EXPORT_TYPE_FILE, EmailMessagesEnum.EXPORT_TYPE_FILE_CSV,
        ReplacementToken.MAIL_SUBJECT, EmailMessagesEnum.DOWNLOAD_SUBJECT,
        ReplacementToken.MAIL_CONTENT, EmailMessagesEnum.DOWNLOAD_CONTENT),
        Map.of(ReplacementToken.DOWNLOAD_LINK, FILE_DOWNLOAD_EXTERN_URL, ReplacementToken.JOB, "", ReplacementToken.PROJECT_NAME, "",
            ReplacementToken.REFERENCE, REFERENCE)));
    // OPS export
    objects.add(input(ExportType.OPS, PROJECT_NAME, Theme.OWN2000, Map.of(
        ReplacementToken.EXPORT_TYPE_FILE, EmailMessagesEnum.EXPORT_TYPE_FILE_ENGINE_INPUT,
        ReplacementToken.MAIL_SUBJECT, EmailMessagesEnum.DOWNLOAD_SUBJECT,
        ReplacementToken.MAIL_CONTENT, EmailMessagesEnum.DOWNLOAD_CONTENT),
        Map.of(ReplacementToken.DOWNLOAD_LINK, FILE_DOWNLOAD_EXTERN_URL, ReplacementToken.JOB, PROJECT_NAME, ReplacementToken.PROJECT_NAME,
            PROJECT_NAME, ReplacementToken.REFERENCE, REFERENCE)));
    // ADMS export
    objects.add(input(ExportType.ADMS, PROJECT_NAME, Theme.OWN2000, Map.of(
        ReplacementToken.EXPORT_TYPE_FILE, EmailMessagesEnum.EXPORT_TYPE_FILE_ENGINE_INPUT,
        ReplacementToken.MAIL_SUBJECT, EmailMessagesEnum.DOWNLOAD_SUBJECT,
        ReplacementToken.MAIL_CONTENT, EmailMessagesEnum.DOWNLOAD_CONTENT),
        Map.of(ReplacementToken.DOWNLOAD_LINK, FILE_DOWNLOAD_EXTERN_URL, ReplacementToken.JOB, PROJECT_NAME, ReplacementToken.PROJECT_NAME,
            PROJECT_NAME, ReplacementToken.REFERENCE, REFERENCE)));
    // UI export
    objects.add(new Object[] {createCalculationInputData(ExportType.CALCULATION_UI, "", Theme.OWN2000), Map.of(), Map.of()});
    return objects;
  }

  private static Object[] input(final ExportType exportType, final String projectName, final Theme theme,
      final Map<ReplacementToken, Enum<?>> expectedEnumReplacementTokens, final Map<ReplacementToken, String> expectedReplacements) {
    final CalculationInputData inputData = createCalculationInputData(exportType, projectName, theme);

    return new Object[] {inputData, expectedEnumReplacementTokens, merge(DEFAULT_STRING_REPLACEMENTS, expectedReplacements)};
  }

  private static CalculationInputData createCalculationInputData(final ExportType exportType, final String projectName, final Theme theme) {
    final CalculationInputData inputData = new CalculationInputData();
    final Scenario scenario = new Scenario();
    final ScenarioSituation situation = new ScenarioSituation();
    final ScenarioMetaData metaData = new ScenarioMetaData();
    scenario.setTheme(theme);

    metaData.setProjectName(projectName);
    scenario.setMetaData(metaData);
    situation.setReference(REFERENCE);
    scenario.getSituations().add(situation);
    inputData.setName(projectName);
    inputData.setCreationDate(CREATION_DATE);
    inputData.setLocale(LOCALE);
    inputData.setScenario(scenario);
    inputData.setExportType(exportType);
    return inputData;
  }

  private static Map<ReplacementToken, ?> merge(final Map<ReplacementToken, ?> map1, final Map<ReplacementToken, ?> map2) {
    return Stream.of(map1, map2).flatMap(m -> m.entrySet().stream()).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
  }
}
