/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.processmonitor;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Properties;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.worker.processmonitor.ProcessMonitorConfigurationBuilder.ProcessMonitorWorkerConfiguration;

/**
 * Test class for {@link ProcessMonitorConfigurationBuilder}
 */
class ProcessMonitorConfigurationBuilderTest {

  @Test
  void buildConfiguration() {
    final Properties properties = new Properties();
    final ProcessMonitorConfigurationBuilder builder = new ProcessMonitorConfigurationBuilder(properties);
    final ProcessMonitorWorkerConfiguration configuration = builder.buildAndValidate().get();

    assertEquals(1, configuration.getProcesses(), "Should be initialzed with 1 process");
  }
}
