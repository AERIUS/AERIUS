/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.pdf;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.io.Serializable;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.aerius.taskmanager.client.TaskManagerClientSender;
import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.db.calculator.JobRepository;
import nl.overheid.aerius.db.test.BaseTestDatabase;
import nl.overheid.aerius.export.PdfGenerator;
import nl.overheid.aerius.export.util.PdfTask;
import nl.overheid.aerius.http.FileServerProxy;
import nl.overheid.aerius.shared.domain.PdfProductType;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.Worker;
import nl.overheid.aerius.worker.WorkerConnectionHelper;
import nl.overheid.aerius.worker.export.paa.GMLGenerator;

/**
 * Test class for {@link PdfCalculatorWorkerFactory}.
 */
@ExtendWith(MockitoExtension.class)
class PdfCalculatorWorkerFactoryTest extends BaseTestDatabase {

  private @Mock WorkerConnectionHelper workerConnectionHelper;
  private @Mock FileServerProxy fileServerProxy;
  private @Mock TaskManagerClientSender taskManagerClient;
  private @Mock PdfGenerator pdfGenerator;
  private @Mock GMLGenerator gmlGenerator;
  private @Mock StringDataSource gmlDataSource;

  private @Captor ArgumentCaptor<String> filenameCaptor;
  private @Captor ArgumentCaptor<PdfTask> pdfTaskCaptor;

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    super.setUp();
    PROPS.put("pdf.processes", "1");
    PROPS.put("pdf.webserver.url", "http://localhost");
    doReturn(getPMF()).when(workerConnectionHelper).getPMF();
    doReturn(taskManagerClient).when(workerConnectionHelper).createTaskManagerClient();
    doReturn(fileServerProxy).when(workerConnectionHelper).createFileServerProxy(any());
  }

  @Test
  void testRun() throws Exception {
    final PdfCalculatorWorkerFactory factory = new PdfCalculatorWorkerFactory();
    final Optional<PdfConfiguration> config = factory.configurationBuilder(PROPS).buildAndValidate();
    final Worker<Serializable, Serializable> worker = factory.createWorkerHandlerWithPMF(config.get(), workerConnectionHelper, pdfGenerator);
    final CalculationInputData inputData = new CalculationInputData();

    inputData.setExportType(ExportType.PDF_PAA);
    final String scenarioJson = "{ \"theme\": \"OWN2000\", \"situations\": [ { \"id\": \"1\", \"type\":\"PROPOSED\" }]}";
    doReturn(scenarioJson).when(fileServerProxy).getContent(any());
    doReturn("").when(fileServerProxy).processFile(any(), filenameCaptor.capture(), any(File.class), any());
    doReturn(new File("")).when(pdfGenerator).generate(any(), any(), pdfTaskCaptor.capture(), any(), any(), any());
    final JobIdentifier jobIdentifier = new JobIdentifier(JobRepository.createJob(getConnection(), JobType.REPORT, false));

    worker.run(inputData, jobIdentifier, null);
    final String fn = filenameCaptor.getValue();
    assertEquals(".zip", fn.substring(fn.length() - 4), "Should have zip file name");
    final String jobKey = jobIdentifier.getJobKey();
    assertEquals(jobKey, pdfTaskCaptor.getValue().jobIdentifier().getJobKey(), "Should have called pdf generator for id");
    assertEquals(PdfProductType.OWN2000_CALCULATOR, pdfTaskCaptor.getValue().pdfProductType(), "Should have called pdf generator for id");
    assertEquals(JobState.POST_PROCESSING, JobRepository.getProgress(getConnection(), jobKey).getState(),
        "Should have job state POST_PROCESSING");
    verify(fileServerProxy, times(3)).deleteRemoteContent(argThat(s -> s.startsWith(jobKey)));
  }
}
