/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.export;

import java.io.File;
import java.net.URISyntaxException;
import java.util.function.UnaryOperator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.WorkerUtil;

/**
 * Test class for {@link GMLExportHandler} in combination with {@link CalculatorExportWorker}.
 */
@ExtendWith(MockitoExtension.class)
class GMLExportHandlerTest extends AbstractTestExportWorker {

  private File folderForJob;

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    setUpWorkerConnectionHelper();
    setUpHttpClientManager();
    setUpJobIdentifier();
    folderForJob = WorkerUtil.getFolderForJob(jobIdentifier);
    setUp(new ExportEmailHandler(workerConnectionHelper,
        new ExportZipProcessor(new GMLExportHandler(getPMF(), folderForJob, UnaryOperator.identity()), fileServerProxy), gmlProvider));
  }

  @Test
  void testSuccesfullExport() throws Exception {
    final CalculationInputData inputData = createExampleCalculationInputData(jobIdentifier);

    assertExport(inputData, 1);
  }

  @Test
  void testSuccesfullExportSectors() throws Exception {
    final CalculationInputData inputData = createExampleCalculationInputData(jobIdentifier);

    inputData.setExportType(ExportType.GML_WITH_SECTORS_RESULTS);
    assertExport(inputData, 2);
  }

  private void assertExport(final CalculationInputData inputData, final int expectedFiles) throws AeriusException, URISyntaxException, Exception {
    worker.run(inputData, jobIdentifier, null);
    assertResults(expectedFiles, folderForJob);
    assertJobState(JobState.POST_PROCESSING);
  }
}
