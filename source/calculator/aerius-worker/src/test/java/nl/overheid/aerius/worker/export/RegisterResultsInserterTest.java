/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.export;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import com.fasterxml.jackson.databind.ObjectMapper;

import nl.overheid.aerius.db.calculator.CalculationRepositoryBean;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.results.SaveCalculationResultsRepositoryBean;
import nl.overheid.aerius.http.FileServerProxy;
import nl.overheid.aerius.http.HttpException;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.register.RegisterCalculation;
import nl.overheid.aerius.shared.domain.register.RegisterCalculationResult;
import nl.overheid.aerius.shared.domain.register.RegisterCalculationResultSet;
import nl.overheid.aerius.shared.domain.register.RegisterPermitAreaResult;
import nl.overheid.aerius.shared.domain.register.RegisterResults;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 *
 */
@ExtendWith(MockitoExtension.class)
class RegisterResultsInserterTest {

  private static final String JOB_KEY = "someKey";
  private static final int CALCULATION_ID = 101;

  private @Mock FileServerProxy fileServerProxy;
  private @Mock JobRepositoryBean jobRepository;
  private @Mock SaveCalculationResultsRepositoryBean saveCalculationResultsRepository;
  private @Mock CalculationRepositoryBean calculationRepository;

  private @Mock Scenario scenario;
  private @Mock ScenarioSituation situation;

  @Test
  void testInsertRegisterResults() throws AeriusException, IOException, HttpException, URISyntaxException {
    final RegisterResultsInserter inserter =
        new RegisterResultsInserter(fileServerProxy, jobRepository, saveCalculationResultsRepository, calculationRepository);
    when(scenario.getSituations()).thenReturn(List.of(situation));
    when(situation.getType()).thenReturn(SituationType.PROPOSED);

    final RegisterCalculationResultSet resultSet = new RegisterCalculationResultSet(Substance.NOX, EmissionResultType.DEPOSITION,
        List.of(new RegisterCalculationResult(9292, 4.1), new RegisterCalculationResult(3534, 3.6)));
    final RegisterCalculation calculation = new RegisterCalculation("PROPOSED",
        List.of(resultSet));
    final RegisterResults registerResults = new RegisterResults(
        List.of(calculation),
        List.of(new RegisterPermitAreaResult(57, 0.0), new RegisterPermitAreaResult(65, 1.0)));

    final ObjectMapper mapper = new ObjectMapper();
    when(fileServerProxy.getContentStream(any()))
        .thenReturn(new ByteArrayInputStream(mapper.writeValueAsBytes(registerResults)));

    doAnswer(new Answer<>() {

      @Override
      public Object answer(final InvocationOnMock invocation) throws Throwable {
        final ScenarioCalculations calculations = invocation.getArgument(0);
        calculations.getCalculations().get(0).setCalculationId(CALCULATION_ID);
        return null;
      }
    }).when(saveCalculationResultsRepository).insertCalculations(any());

    when(calculationRepository.getCalculationResultIdSets(CALCULATION_ID)).thenReturn(List.of(3));

    inserter.insertRegisterResults(JOB_KEY, scenario);

    verify(saveCalculationResultsRepository).insertCalculationResults(eq(CALCULATION_ID), any());
    verify(saveCalculationResultsRepository).insertResultsSummaries(anyInt(), any(), any());
    verify(saveCalculationResultsRepository).insertPermitDemand(anyInt(), anyInt(), eq(57), eq(0.0));
    verify(saveCalculationResultsRepository).insertPermitDemand(anyInt(), anyInt(), eq(65), eq(1.0));
  }

}
