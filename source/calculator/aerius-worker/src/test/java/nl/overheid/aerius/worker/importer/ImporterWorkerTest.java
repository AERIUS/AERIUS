/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.importer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.verification.VerificationMode;

import com.fasterxml.jackson.databind.ObjectMapper;

import nl.overheid.aerius.controller.ImaerController;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.http.FileServerProxy;
import nl.overheid.aerius.http.HttpException;
import nl.overheid.aerius.importer.domain.ImportTaskInput;
import nl.overheid.aerius.importer.domain.ImporterInput;
import nl.overheid.aerius.importer.domain.SaveImportedResultsTaskInput;
import nl.overheid.aerius.importer.domain.SituationWithFileId;
import nl.overheid.aerius.io.FileServiceImportableFile;
import nl.overheid.aerius.shared.FileServerExpireTag;
import nl.overheid.aerius.shared.FileServerFile;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.ops.OPSCustomCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituationResults;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.validation.FileValidationStatus;
import nl.overheid.aerius.shared.domain.validation.ValidationStatus;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.LocaleUtils;
import nl.overheid.aerius.util.PatternMatcher;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.JobStateHelper;

/**
 * Test class for {@link ImporterWorker}.
 */
@ExtendWith(MockitoExtension.class)
class ImporterWorkerTest {

  private @Mock FileServerProxy fileServerProxy;
  private @Mock ImaerController imaerController;
  private @Mock ResultsInserter resultsInserter;
  private @Mock JobStateHelper jobStateHelper;
  private @Mock ScenarioSummaryFactory scenarioSummaryFactory;
  private @Mock PMF pmf;

  private @Captor ArgumentCaptor<String> putFileAsString;
  private @Captor ArgumentCaptor<String> pathSegmentCaptor;
  private @Captor ArgumentCaptor<List<SituationResultsImport>> situationResultsImportCaptor;

  private final ObjectMapper objectMapper = new ObjectMapper();

  private ImporterWorker importerWorker;

  @BeforeEach
  void before() throws IOException, HttpException, URISyntaxException, SQLException, AeriusException {
    lenient().doReturn("").when(fileServerProxy).putFile(any(), any(), any(String.class), any());
    importerWorker = new ImporterWorker(fileServerProxy, imaerController, resultsInserter, jobStateHelper, scenarioSummaryFactory, new PatternMatcher(""));
  }

  /**
   * Test with no import parcels in data.
   */
  @Test
  void testValidateAndImportFileEmpty() throws Exception {
    assertImportValidation(List.of(), times(1), ValidationStatus.FAILED);
  }

  /**
   * Test with one import parcels in data.
   */
  @Test
  void testValidateAndImportFile() throws Exception {
    assertImportValidation(List.of(new ImportParcel()), times(1), ValidationStatus.VALID);
  }

  /**
   * Test with 2 import parcels in data.
   */
  @Test
  void testValidateAndImportMultipleFile() throws Exception {
    assertImportValidation(List.of(new ImportParcel(), new ImportParcel()), times(2), ValidationStatus.VALID);
  }

  /**
   * Test an import with calculation points and sources should result in 2 parcels.
   */
  @Test
  void testValidateWithPointsAndSourcesFile() throws Exception {
    final ImportParcel importParcel = createImportParcelWithCustomPoints();
    importParcel.getSituation().getEmissionSourcesList().add(new EmissionSourceFeature());

    assertImportValidation(List.of(importParcel), times(2), ValidationStatus.VALID);
  }

  /**
   * Test an import with only calculation points should result in 1 parcel.
   */
  @Test
  void testValidateWithPointsFile() throws Exception {
    final ImportParcel importParcel = createImportParcelWithCustomPoints();

    assertImportValidation(List.of(importParcel), times(1), ValidationStatus.VALID);
  }

  private ImportParcel createImportParcelWithCustomPoints() {
    final ImportParcel importParcel = new ImportParcel();
    final CalculationPointFeature cp = new CalculationPointFeature();
    cp.setProperties(new OPSCustomCalculationPoint());
    importParcel.getCalculationPoints().getFeatures().add(cp);
    return importParcel;
  }

  /**
   * Test with exception thrown during parsing import.
   */
  @Test
  void testValidateAndImportFileException() throws Exception {
    doThrow(new AeriusException(AeriusExceptionReason.INTERNAL_ERROR)).when(imaerController).processFile(any(FileServiceImportableFile.class),
        anySet(), any(), any());
    runImporterWorker(times(1));
    assertEquals(ValidationStatus.INVALID, readFileValidationStatus().getValidationStatus(), "");
  }

  private void assertImportValidation(final List<ImportParcel> parcels, final VerificationMode times, final ValidationStatus validationStatus)
      throws Exception {
    importFile(parcels, times);
    assertEquals(validationStatus, readFileValidationStatus().getValidationStatus(), "");
  }

  private FileValidationStatus readFileValidationStatus() throws Exception {
    return objectMapper.readValue(putFileAsString.getValue(), FileValidationStatus.class);
  }

  private void importFile(final List<ImportParcel> parcels, final VerificationMode times) throws Exception {
    final List<ImportParcel> importParcels = new ArrayList<>();

    importParcels.addAll(parcels);
    doReturn(importParcels).when(imaerController).processFile(any(FileServiceImportableFile.class), anySet(), any(), any());

    final String uuid = runImporterWorker(times);
    assertEquals(uuid, readFileValidationStatus().getFileCode(), "");
  }

  private String runImporterWorker(final VerificationMode times) throws Exception {
    final String uuid = "123";
    final String originalFilename = "filename";
    final JobIdentifier jobIdentifier = new JobIdentifier(uuid);
    final ImporterInput input =
        new ImportTaskInput(uuid, uuid, originalFilename, null, FileServerExpireTag.SHORT, LocaleUtils.getDefaultLocale());

    importerWorker.run(input, jobIdentifier, null);
    verify(fileServerProxy, times).putFile(any(), eq(FileServerFile.VALIDATION.getFilename()), putFileAsString.capture(),
        eq(FileServerExpireTag.SHORT));
    return uuid;
  }

  /**
   * Test for importing results.
   */
  @Test
  void testSaveResults() throws Exception {
    final String uuid = "123";
    final String situationId = "someSituationId";
    final String fileId = "someFileId";
    final String version = "SomeVersion";
    final JobIdentifier jobIdentifier = new JobIdentifier(uuid);
    final Scenario scenario = mock(Scenario.class);
    final SituationWithFileId situationWithFileId = new SituationWithFileId(situationId, fileId);
    final ScenarioSituationResults results = new ScenarioSituationResults();
    // rather than faking 1 or more calculation point feature, just set the calculation ID to check if deserializing works.
    results.setCalculationId(23);
    final ImportParcel parcel = new ImportParcel();
    parcel.setVersion(version);
    final ObjectMapper mapper = new ObjectMapper();
    when(fileServerProxy.getContentStream(pathSegmentCaptor.capture()))
        .thenReturn(new ByteArrayInputStream(mapper.writeValueAsBytes(parcel)))
        .thenReturn(new ByteArrayInputStream(mapper.writeValueAsBytes(results)));
    final ImporterInput input = new SaveImportedResultsTaskInput(uuid, scenario, List.of(situationWithFileId), null);

    importerWorker.run(input, jobIdentifier, null);

    assertEquals("someFileId/data.json", pathSegmentCaptor.getAllValues().get(0), "Path used when obtaining parcel");
    assertEquals("someFileId/result_data.json", pathSegmentCaptor.getAllValues().get(1), "Path used when obtaining results");
    verify(resultsInserter).insertResults(eq(uuid), eq(scenario), situationResultsImportCaptor.capture(), isNull());
    final List<SituationResultsImport> captured = situationResultsImportCaptor.getValue();
    assertEquals(1, captured.size(), "Number of situations in captured map");
    final SituationResultsImport imported = captured.get(0);
    assertEquals(situationId, imported.getSituationId(), "Situation ID");
    assertEquals(23, imported.getResults().getCalculationId(), "Calculation ID");
    assertEquals(version, imported.getVersion(), "version");

    verify(jobStateHelper).setJobStatus(uuid, JobState.COMPLETED);
  }
}
