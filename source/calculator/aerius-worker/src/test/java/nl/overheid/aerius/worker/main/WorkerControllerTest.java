/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.main;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.nio.file.Path;
import java.util.List;

import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.worker.WorkerConfigurationFactory;
import nl.overheid.aerius.worker.WorkerExitCode;

/**
 * Test class for {@link WorkerController}.
 */
class WorkerControllerTest {

  @ParameterizedTest
  @Timeout(1000)
  @MethodSource("withConfig")
  void testErrors(final String filename, final WorkerExitCode expectedExitCode) throws Exception {
    final WorkerController controller = new WorkerController(new WorkerConfigurationFactory<?>[0]);
    final String propertiesFile = Path.of(getClass().getResource(filename).toURI()).toFile().getAbsolutePath();

    assertEquals(expectedExitCode, controller.start(new String[] {"-config", propertiesFile}), "Should stop with expected exit code.");
  }

  private static List<Arguments> withConfig() {
    return List.of(
        Arguments.of("empty.properties", WorkerExitCode.VALIDATION_ERRORS),
        Arguments.of("no_processes.properties", WorkerExitCode.NOTHING_TO_DO));
  }

  @ParameterizedTest
  @Timeout(1000)
  @MethodSource("withInfo")
  void testInfoStartup(final String option, final WorkerExitCode expectedExitCode) throws Exception {
    final WorkerController controller = new WorkerController(new WorkerConfigurationFactory<?>[0]);

    assertEquals(expectedExitCode, controller.start(new String[] {option}), "Should stop with expected exit code.");
  }

  private static List<Arguments> withInfo() {
    return List.of(
        Arguments.of("-help", WorkerExitCode.SUCCESS),
        Arguments.of("-version", WorkerExitCode.SUCCESS));
  }
}
