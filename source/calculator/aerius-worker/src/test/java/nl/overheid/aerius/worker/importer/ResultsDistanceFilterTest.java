/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.importer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.ReceptorPoint;
import nl.overheid.aerius.shared.domain.v2.point.SubPoint;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituationResults;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.geo.EPSG;

class ResultsDistanceFilterTest {

  private static final HexagonZoomLevel ZOOM_LEVEL_1 = new HexagonZoomLevel(1, 10_000);
  private static final BBox RECEPTOR_BBOX = new BBox(3604, 296800, 287959, 629300);
  private static final ReceptorGridSettings RGS = new ReceptorGridSettings(RECEPTOR_BBOX, EPSG.RDNEW, 1529,
      new ArrayList<>(List.of(ZOOM_LEVEL_1)));

  private static final List<Integer> TEST_RECEPTOR_IDS = List.of(4615264, 4616793, 4618323);

  private static final List<CalculationPointFeature> RECEPTORS = TEST_RECEPTOR_IDS.stream().map(ap -> {
        final CalculationPointFeature cpf = new CalculationPointFeature();
        final ReceptorPoint receptorPoint = new ReceptorPoint();
        receptorPoint.setReceptorId(ap);
        cpf.setProperties(receptorPoint);
        return cpf;
      }
  ).toList();
  private static final List<CalculationPointFeature> SUB_POINTS = TEST_RECEPTOR_IDS.stream().map(ap -> {
        final CalculationPointFeature cpf = new CalculationPointFeature();
        final SubPoint subPoint = new SubPoint();
        subPoint.setReceptorId(ap);
        subPoint.setSubPointId(ap * 100);
        cpf.setProperties(subPoint);
        return cpf;
      }
  ).toList();

  private final ResultsDistanceFilter RESULTS_DISTANCE_FILTER = new ResultsDistanceFilter(RGS);

  @Test
  void testDistanceFilter() throws AeriusException {
    final ImportParcel importParcel = new ImportParcel();

    final EmissionSourceFeature esf = new EmissionSourceFeature();
    esf.setGeometry(new Point(141427, 458899));
    esf.setProperties(new GenericEmissionSource());
    importParcel.getSituation().getEmissionSourcesList().add(esf);

    importParcel.setSituationResults(new ScenarioSituationResults());
    importParcel.getSituationResults().getResults().addAll(RECEPTORS);
    importParcel.getSituationResults().getResults().addAll(SUB_POINTS);

    RESULTS_DISTANCE_FILTER.filter(300, List.of(importParcel));

    // 2 receptors + 2 sub points in range
    assertEquals(4, importParcel.getSituationResults().getResults().size(), "Should return correct number of points");
  }

}
