/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.base;

import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.processmonitor.ProcessMonitorService;
import nl.overheid.aerius.util.HttpClientManager;
import nl.overheid.aerius.worker.WorkerConnectionHelper;

/**
 * Default implementation of the {@link WorkerConnectionHelper} for all workers.
 */
public class WorkerConnectionHelperImpl implements WorkerConnectionHelper {

  private final BrokerConnectionFactory brokerConnectionFactory;
  private final HttpClientManager httpClientManager = new HttpClientManager();
  private final ProcessMonitorService processMonitorService = new ProcessMonitorService();
  private PMF pmf;

  public WorkerConnectionHelperImpl(final BrokerConnectionFactory brokerConnectionFactory) {
    this.brokerConnectionFactory = brokerConnectionFactory;
  }

  @Override
  public BrokerConnectionFactory getBrokerConnectionFactory() {
    return brokerConnectionFactory;
  }

  @Override
  public HttpClientManager getHttpClientManager() {
    return httpClientManager;
  }

  @Override
  public ProcessMonitorService getProcessMonitorService() {
    return processMonitorService;
  }

  @Override
  public PMF getPMF() {
    return pmf;
  }

  @Override
  public void setPmf(final PMF pmf) {
    this.pmf = pmf;
  }
}
