/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.export;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.util.FileProcessor;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.ZipAndProcessChainer;

/**
 * Runs the export handlers to create the file content, puts the content (files in a temp directory) in a zip file, and passes the zip file to
 * a processor.
 */
public class ExportZipProcessor {

  // Files are all added to the root of the zip file.
  private static final boolean INCLUDE_DIRECTORY_IN_ZIP = false;

  private static final String TEMP_ZIP_DIRECTORY_PREFIX = "aerius_tmp_zip";

  private final ExportHandler exportHandler;
  private final FileProcessor zipPostProcessor;
  private final File tmpDirectory;

  public ExportZipProcessor(final ExportHandler exportHandler, final FileProcessor zipPostProcessor) throws IOException {
    this.exportHandler = exportHandler;
    this.zipPostProcessor = zipPostProcessor;
    this.tmpDirectory = Files.createTempDirectory(TEMP_ZIP_DIRECTORY_PREFIX).toFile();
  }

  /**
   * Zips the data from the exportHandler and return the zip file. If keepFiles was set it will not deleted all files.
   *
   * @param inputData
   * @param jobIdentifier
   * @param keepFiles true if the genated files should not be clean up after generation
   * @return returns the name of the zip file
   * @throws Exception
   */
  public String zipAndProcess(final CalculationInputData inputData, final JobIdentifier jobIdentifier, final boolean keepFiles) throws Exception {
    final File folderWithFilesToZip = exportHandler.createFileContent(inputData, jobIdentifier);
    final File zipFile = new File(tmpDirectory, exportHandler.getZipFileName(inputData));

    try (final ZipAndProcessChainer chainer = new ZipAndProcessChainer(zipPostProcessor, zipFile, keepFiles)) {
      chainer.addDirectoryContent(folderWithFilesToZip, INCLUDE_DIRECTORY_IN_ZIP);
      return chainer.put(jobIdentifier.getJobKey(), inputData.getExpire());
    }
  }

  public File getZipDirectory() {
    return tmpDirectory;
  }
}
