/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.importer;

import java.util.Properties;

import nl.overheid.aerius.worker.ConfigurationBuilder;
import nl.overheid.aerius.worker.DatabaseConfigurationBuilder;
import nl.overheid.aerius.worker.FileServerConfigurationBuilder;
import nl.overheid.aerius.worker.base.ArchiveServerConfigurationBuilder;

class ImporterConfigurationBuilder extends ConfigurationBuilder<ImporterConfiguration> {

  private static final String IMPORTER_PREFIX = "importer";

  public ImporterConfigurationBuilder(final Properties properties) {
    super(properties, IMPORTER_PREFIX);
    addBuilder(new DatabaseConfigurationBuilder<>(properties));
    addBuilder(new FileServerConfigurationBuilder<>(properties));
    addBuilder(new ArchiveServerConfigurationBuilder<>(properties));
  }

  @Override
  protected ImporterConfiguration create() {
    return new ImporterConfiguration(workerProperties.getProcesses());
  }

  @Override
  protected boolean isActive() {
    return workerProperties.isActive();
  }

  @Override
  protected void build(final ImporterConfiguration configuration) {
    // No additional properties to set
  }
}
