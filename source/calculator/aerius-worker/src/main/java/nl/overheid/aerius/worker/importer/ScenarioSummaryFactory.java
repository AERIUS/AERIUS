/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.importer;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import nl.overheid.aerius.archive.ArchiveProxy;
import nl.overheid.aerius.calculation.theme.nca.DevelopmentPressureSearchHandler;
import nl.overheid.aerius.controller.ImaerController;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.db.common.decision.DecisionFrameworkRepository;
import nl.overheid.aerius.http.FileServerProxy;
import nl.overheid.aerius.http.HttpException;
import nl.overheid.aerius.importer.ImportOption;
import nl.overheid.aerius.importer.domain.SummarizeTaskInput;
import nl.overheid.aerius.importer.summary.ImportSummaryGenerator;
import nl.overheid.aerius.io.FileServiceImportableFile;
import nl.overheid.aerius.shared.FileServerFile;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.importer.ImportProperties;
import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.domain.importer.summary.ImportSummary;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.ScenarioObjectMapperUtil;

/**
 * Imports a file and create a summary based on the resulting parcels.
 */
public class ScenarioSummaryFactory {

  private static final Logger LOG = LoggerFactory.getLogger(ScenarioSummaryFactory.class);

  private static final Set<ImportOption> SUMMARY_IMPORT_OPTIONS = EnumSet.of(
      ImportOption.INCLUDE_SOURCES,
      ImportOption.INCLUDE_RESULTS,
      ImportOption.USE_VALID_SECTORS,
      ImportOption.VALIDATE_AGAINST_SCHEMA,
      ImportOption.VALIDATE_SOURCES,
      ImportOption.WARNING_ON_CALCULATION_POINTS);

  private static final EnumSet<ImportType> SUMMARY_IMPORT_TYPES = EnumSet.of(
      ImportType.GML,
      ImportType.ZIP,
      ImportType.PAA);

  private final ImaerController imaerController;
  private final FileServerProxy fileServerProxy;
  private final ResultsInserter resultsInserter;
  private final DecisionFrameworkRepository decisionFrameworkRepository;
  private final JobRepositoryBean jobRepository;
  private final ObjectMapper objectMapper = ScenarioObjectMapperUtil.getScenarioObjectMapper();
  private final ImportSummaryGenerator importSummaryGenerator = new ImportSummaryGenerator();
  private final ResultsDistanceFilter resultsDistanceFilter;
  private final DevelopmentPressureSearchHandler developmentPressureSearchHandler;

  public ScenarioSummaryFactory(final PMF pmf, final ImaerController imaerController, final FileServerProxy fileServerProxy,
      final ArchiveProxy archiveProxy)
      throws SQLException {
    this.resultsDistanceFilter = new ResultsDistanceFilter(ReceptorGridSettingsRepository.getReceptorGridSettings(pmf));
    this.imaerController = imaerController;
    this.fileServerProxy = fileServerProxy;
    this.resultsInserter = new ResultsInserter(pmf);
    this.decisionFrameworkRepository = new DecisionFrameworkRepository(pmf, null);
    this.jobRepository = new JobRepositoryBean(pmf);
    this.developmentPressureSearchHandler = new DevelopmentPressureSearchHandler(decisionFrameworkRepository, jobRepository, archiveProxy);
  }

  /**
   * Generate a summary for the given input and place it on the file server
   * @param importerInput the input to summarize
   * @throws AeriusException
   */
  public void summarize(final SummarizeTaskInput importerInput) throws AeriusException {
    try {
      final List<ImportParcel> result = getImportParcels(importerInput.getJobKey(), importerInput.getImportFilename(),
          importerInput.getOriginalFilename(), new ImportProperties(null, null,
              SUMMARY_IMPORT_TYPES, importerInput.getImportProperties().getTheme()),
          importerInput.getLocale());

      if (importerInput.getMaximumResultsDistance() != null) {
        resultsDistanceFilter.filter(importerInput.getMaximumResultsDistance(), result);
      }

      final ImportSummary importSummary = importSummaryGenerator.generateImportSummary(importerInput.getLocale(), result);

      processDecisionFramework(importerInput, result, importSummary);

      fileServerProxy.putFile(importerInput.getJobKey(), FileServerFile.SUMMARY.getFilename(), objectMapper.writeValueAsString(importSummary),
          importerInput.getExpire());
    } catch (final HttpException e) {
      LOG.error("HttpException while generating import summary", e);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    } catch (final URISyntaxException e) {
      LOG.error("URISyntaxException while generating import summary", e);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    } catch (final IOException e) {
      LOG.error("IOException while generating import summary", e);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }

  private void processDecisionFramework(final SummarizeTaskInput importerInput, final List<ImportParcel> importParcels,
      final ImportSummary importSummary) throws AeriusException {
    if (importerInput.isDecisionFramework()) {
      // Decision framework so far is only for UK.
      final Scenario scenario = new Scenario(Theme.NCA);
      scenario.setSituations(importParcels.stream().map(ImportParcel::getSituation).toList());
      final List<SituationResultsImport> situationResultImports =
          importParcels.stream().map(ScenarioSummaryFactory::retrieveSituationResults).toList();
      final ScenarioCalculations scenarioCalculations =
          resultsInserter.insertResults(importerInput.getJobKey(), scenario, situationResultImports, null);
      final int jobId = jobRepository.getJobId(importerInput.getJobKey());
      final int proposedCalculationId =
          scenarioCalculations.getCalculationForSituationType(SituationType.PROPOSED).map(Calculation::getCalculationId).orElse(0);
      importSummary.setAboveDecisionMakingThreshold(decisionFrameworkRepository.isAboveDecisionMakingThreshold(jobId, proposedCalculationId));
      final Optional<ImportParcel> proposedSituationParcel = importParcels.stream()
          .filter(x -> Optional.ofNullable(x.getSituation()).map(ScenarioSituation::getType).isPresent())
          .filter(x -> x.getSituation().getType() == SituationType.PROPOSED)
          .findFirst();
      if (proposedSituationParcel.isPresent()) {
        developmentPressureSearchHandler.handleSearch(proposedSituationParcel.get().getCalculationSetOptions(), jobId,
            proposedSituationParcel.get().getSituation());
        importSummary.setAboveSiteRelevantThreshold(decisionFrameworkRepository.isAboveAssessmentAreaThreshold(jobId, proposedCalculationId));
      }
    }
  }

  private static SituationResultsImport retrieveSituationResults(final ImportParcel resultsParcel) {
    return new SituationResultsImport(resultsParcel.getSituation().getId(), resultsParcel.getSituationResults(), resultsParcel.getVersion());
  }

  private List<ImportParcel> getImportParcels(final String uuid, final String importFilename, final String originalFilename,
      final ImportProperties importProperties, final Locale locale) throws IOException, HttpException, URISyntaxException, AeriusException {
    try (final InputStream fsis = fileServerProxy.getContentStream(FileServerFile.FREE_FORMAT.format(uuid, importFilename))) {
      return imaerController.processFile(new FileServiceImportableFile(originalFilename, fsis), SUMMARY_IMPORT_OPTIONS, importProperties,
          locale);
    }
  }
}
