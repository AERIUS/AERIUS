/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.processmonitor;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;

import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.ConfigurationBuilder;
import nl.overheid.aerius.worker.ScheduledWorkerFactory;
import nl.overheid.aerius.worker.WorkerConnectionHelper;
import nl.overheid.aerius.worker.processmonitor.ProcessMonitorConfigurationBuilder.ProcessMonitorWorkerConfiguration;

/**
 * ScheduledWorkerFactory to start the {@link ProcessMonitorController}.
 */
public class ProcessMonitorFactory implements ScheduledWorkerFactory<ProcessMonitorWorkerConfiguration> {

  @Override
  public ConfigurationBuilder<ProcessMonitorWorkerConfiguration> configurationBuilder(final Properties properties) {
    return new ProcessMonitorConfigurationBuilder(properties);
  }

  @Override
  public Runnable createScheduledWorker(final ProcessMonitorWorkerConfiguration configuration, final WorkerConnectionHelper workerConnectionHelper)
      throws IOException, AeriusException, SQLException {
    return new ProcessMonitorController(workerConnectionHelper);
  }
}
