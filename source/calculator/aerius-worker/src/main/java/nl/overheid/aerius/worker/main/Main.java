/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.adms.ADMSWorkerFactory;
import nl.overheid.aerius.email.worker.EmailMessageWorkerFactory;
import nl.overheid.aerius.ops.OPSWorkerFactory;
import nl.overheid.aerius.srm.worker.SRMWorkerFactory;
import nl.overheid.aerius.worker.WorkerConfigurationFactory;
import nl.overheid.aerius.worker.WorkerExitCode;
import nl.overheid.aerius.worker.calculator.CalculatorWorkerFactory;
import nl.overheid.aerius.worker.importer.ImporterWorkerFactory;
import nl.overheid.aerius.worker.jobcleanup.JobCleanupWorkerFactory;
import nl.overheid.aerius.worker.pdf.PdfCalculatorWorkerFactory;
import nl.overheid.aerius.worker.processmonitor.ProcessMonitorFactory;

import uk.gov.apas.email.worker.NotifyEmailWorkerFactory;

/**
 * Main class for all workers.
 */
public class Main {
  private static final Logger LOG = LoggerFactory.getLogger(Main.class);

  private static final WorkerConfigurationFactory<?>[] WORKER_FACTORIES = {
      new ADMSWorkerFactory(),
      new OPSWorkerFactory(),
      new SRMWorkerFactory(),
      new CalculatorWorkerFactory(),
      new PdfCalculatorWorkerFactory(),
      new EmailMessageWorkerFactory(),
      new NotifyEmailWorkerFactory(),
      new ImporterWorkerFactory(),
      new JobCleanupWorkerFactory(),
      new ProcessMonitorFactory(),
  };

  public static void main(final String[] args) {
    WorkerExitCode exitCode;

    try {
      final WorkerController worker = new WorkerController(WORKER_FACTORIES);

      exitCode = worker.start(args);
    } catch (final Exception e) {
      LOG.error("Exception in main. ", e);
      exitCode = WorkerExitCode.FAILURE;
    }
    System.exit(exitCode.getExitCode());
  }
}
