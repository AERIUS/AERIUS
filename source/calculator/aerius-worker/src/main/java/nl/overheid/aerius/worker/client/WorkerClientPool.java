/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.client;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ShutdownListener;
import com.rabbitmq.client.ShutdownSignalException;

import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.taskmanager.client.WorkerHandler;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.WorkerThreadUtil;

/**
 * Client to be used to communicate with the taskmanager (more importantly, receive tasks from the taskmanager).
 * Usage example (to start 1 worker):
 * <pre>
 * WorkerHandler workerHandler = new SomeWorkerHandlerImpl();
 * WorkerClient client = new WorkerClient(connectionConfiguration, handler, workerQueueName);
 * client.startConsuming(1);
 * </pre>
 */
public class WorkerClientPool implements ShutdownListener, Runnable {

  private static final Logger LOG = LoggerFactory.getLogger(WorkerClientPool.class);
  private static final int DEFAULT_RETRY_SECONDS = 60;

  private final ExecutorService executor;
  private final WorkerHandler handler;
  private final Queue<WorkerClient> workerConsumers = new LinkedList<>();
  private final WorkerType workerType;
  private final AtomicInteger numberOfWorkers = new AtomicInteger();
  private final int retryWaitTimeSeconds;

  private final BrokerConnectionFactory connectionFactory;

  private int index;
  private boolean running;

  /**
   * Creates a worker client pool with a number of workers. Each worker can have at most 1 task to handle.
   * For instance, if there's capacity for 8 workers (because of 8 cores or something), the numberOfWorkers should be 8.
   * Each worker will run on it's own thread that is started by RabbitMQ and will have it's own channel.
   *
   * @param executor executor to start worker threads with
   * @param connectionFactory Configuration used for communicating with the MQ broker.
   * @param handler The object that will handle the actual work and should compile a result object to be returned to sender.
   * @param workerType The naming strategy used for queues.
   * @param numberOfWorkers Number of workers to start.
   */
  public WorkerClientPool(final ExecutorService executor, final BrokerConnectionFactory connectionFactory, final WorkerHandler handler,
      final WorkerType workerType, final int numberOfWorkers) {
    this(executor, connectionFactory, handler, workerType, numberOfWorkers, Optional.empty());
  }

  /**
   * Creates a worker client pool with a number of workers. Each worker can have at most 1 task to handle.
   * For instance, if there's capacity for 8 workers (because of 8 cores or something), the numberOfWorkers should be 8.
   * Each worker will run on it's own thread that is started by RabbitMQ and will have it's own channel.
   *
   * @param executor executor to start worker threads with
   * @param connectionFactory Configuration used for communicating with the MQ broker.
   * @param handler The object that will handle the actual work and should compile a result object to be returned to sender.
   * @param workerType The naming strategy used for queues.
   * @param numberOfWorkers Number of workers to start.
   * @param retryWaitTimeSeconds Wait time before retrying to start workers in case of connection problems.
   */
  public WorkerClientPool(final ExecutorService executor, final BrokerConnectionFactory connectionFactory, final WorkerHandler handler,
      final WorkerType workerType, final int numberOfWorkers, final Optional<Integer> retryWaitTimeSeconds) {
    if (connectionFactory == null || handler == null || workerType == null) {
      throw new IllegalArgumentException("No arguments are allowed to be null.");
    }
    this.executor = executor;
    this.connectionFactory = connectionFactory;
    this.handler = handler;
    this.workerType = workerType;
    // if input <= 0, no workers will be started.
    running = this.numberOfWorkers.addAndGet(Math.max(0, numberOfWorkers)) > 0;
    this.retryWaitTimeSeconds = retryWaitTimeSeconds.orElse(DEFAULT_RETRY_SECONDS);
  }

  @Override
  public void run() {
    LOG.debug("Workers started for: {}", workerType);
    WorkerThreadUtil.setThreadName("WorkerPool", workerType.name());
    while (running) {
      final Connection connection = connectionFactory.getConnection();
      connection.addShutdownListener(this);
      try {
        checkWorkers();
        startWorkers();
      } catch (final IOException e) {
        try {
          LOG.error("IOException while starting workers of '{}', restarting.", workerType, e);
          shutDown(true);
        } catch (final IOException e1) {
          LOG.error("IOException while restarting when specificially said it shouldn't throw. Should not happen, but it happens.", e1);
        }
      }
      try {
        Thread.sleep(TimeUnit.SECONDS.toMillis(retryWaitTimeSeconds));
      } catch (final InterruptedException e) {
        running = false;
        Thread.currentThread().interrupt();
      }
    }
    LOG.debug("Workers stopped for: {}", workerType);
  }

  private void checkWorkers() {
    final List<WorkerClient> shutDownWorkers = workerConsumers.stream().filter(wc -> !wc.isAlive()).collect(Collectors.toList());

    shutDownWorkers.forEach(wc -> {
      if (wc.getException() != null) {
        numberOfWorkers.decrementAndGet();
        LOG.debug("WorkerClient stopped with exception: {}", wc.getException().getMessage());
      }
      workerConsumers.remove(wc);
    });
    if (!shutDownWorkers.isEmpty()) {
      LOG.info("Stopped #{} handlers for worker type: {}, total active: {}", shutDownWorkers.size(), workerType, workerConsumers.size());
    }

  }

  private void startWorkers() throws IOException {
    int cnt = 0;
    while (numberOfWorkers.get() > workerConsumers.size()) {
      cnt++;
      final WorkerClient client = new WorkerClient(executor, connectionFactory, handler, workerType.type(), ++index);
      client.run();
      workerConsumers.add(client);
    }
    if (cnt > 0) {
      LOG.info("Started #{} workers for worker type: {}", cnt, workerType);
    }
    running = getNumberOfWorkers() > 0;
  }

  /**
   * @return The number of workers this client has at the moment.
   * Starting workers happens on different thread, so this might be different from getNumberOfWorkers.
   */
  public int getCurrentNumberOfWorkers() {
    return workerConsumers.size();
  }

  /**
   * @return The number of workers this client *should* have.
   */
  public int getNumberOfWorkers() {
    return numberOfWorkers.get();
  }

  /**
   * Try to stop all consuming work at the moment.
   * Exception might occur, but this will be saved until all workers are attempted to be closed.
   * @throws IOException When an error occurred while communicating with the MQ broker.
   */
  public void stopAllConsuming() throws IOException {
    shutDown(false);
  }

  @Override
  public void shutdownCompleted(final ShutdownSignalException cause) {
    //all workers are now useless, stop em all WITHOUT setting number of workers to 0.
    try {
      shutDown(true);
    } catch (final IOException e) {
      LOG.trace("Exception during #shutdownCompleted, but ignored", e);
    }
  }

  private void shutDown(final boolean restart) throws IOException {
    if (!restart) {
      numberOfWorkers.set(0);
    }
    WorkerClient workerConsumer;
    while ((workerConsumer = workerConsumers.poll()) != null) {
      workerConsumer.shutdown();
    }
    if (!restart) {
      running = false;
    }
  }
}
