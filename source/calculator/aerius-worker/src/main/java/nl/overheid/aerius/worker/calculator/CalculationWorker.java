/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.calculator;

import nl.overheid.aerius.calculation.CalculatorBuildDirector;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.util.ScenarioFileServerUtil;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.WorkerConnectionHelper;
import nl.overheid.aerius.worker.base.BaseCalculatorWorker;

/**
 * Worker to perform a complete calculation. It chunks the task, calls the calculation engine workers and collects the results in the database and
 * send the results back.
 */
public class CalculationWorker extends BaseCalculatorWorker {

  private final CalculatorBuildDirector calculatorBuildDirector;

  public CalculationWorker(final CalculatorBuildDirector calculatorBuildDirector, final WorkerConnectionHelper workerConnectionHelper,
      final ScenarioFileServerUtil scenarioFileServiceUtil) {
    super(workerConnectionHelper, scenarioFileServiceUtil);
    this.calculatorBuildDirector = calculatorBuildDirector;
  }

  @Override
  protected boolean process(final CalculationInputData inputData, final JobIdentifier jobIdentifier) throws Exception {
    final String jobKey = jobIdentifier.getJobKey();

    cleanUpCalculations(jobKey, true);
    jobStateHelper.setJobStatus(jobKey, JobState.PREPARING);
    calculatorBuildDirector.construct(inputData, jobIdentifier).calculate();
    return true;
  }
}
