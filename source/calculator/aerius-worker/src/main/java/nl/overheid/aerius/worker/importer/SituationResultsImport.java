/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.importer;

import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituationResults;

/**
 *
 */
class SituationResultsImport {

  private final String situationId;
  private final ScenarioSituationResults results;
  private final String version;

  public SituationResultsImport(final String situationId, final ScenarioSituationResults results, final String version) {
    this.situationId = situationId;
    this.results = results;
    this.version = version;
  }

  public String getSituationId() {
    return situationId;
  }

  public ScenarioSituationResults getResults() {
    return results;
  }

  public String getVersion() {
    return version;
  }

}
