/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.main;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.exec.ExecuteWrapper;
import nl.overheid.aerius.worker.ScheduledWorkerFactory;
import nl.overheid.aerius.worker.WorkerConfigurationFactory;
import nl.overheid.aerius.worker.WorkerConnectionHelper;
import nl.overheid.aerius.worker.WorkerExitCode;
import nl.overheid.aerius.worker.WorkerFactory;
import nl.overheid.aerius.worker.base.WorkerConnectionHelperImpl;

/**
 * Worker Main class.
 */
final class WorkerController {

  private static final Logger LOG = LoggerFactory.getLogger(WorkerController.class);

  private final WorkerConfigurationFactory<?>[] workerFactories;

  public WorkerController(final WorkerConfigurationFactory<?>[] workerFactories) {
    this.workerFactories = workerFactories;
  }

  /**
   * When this main method is used, the worker will be started.
   *
   * @param args no arguments needed, but if supplied, they should fit the description given by -help.
   * @throws Exception Exceptions during startup
   */
  public WorkerExitCode start(final String[] args) throws Exception {
    final CmdOptions cmdOpts = new CmdOptions(args);

    if (cmdOpts.printIfInfoOption()) {
      return WorkerExitCode.SUCCESS;
    }
    Thread.setDefaultUncaughtExceptionHandler((t, e) -> LOG.error("Uncaught exception from {}", t, e));
    // Read the configuration and validate the configuration
    final WorkerControllerInitializer initializer = new WorkerControllerInitializer(cmdOpts.getConfigFile());

    final List<WorkerFactoryPack<?>> workerPacks = initializer.init(workerFactories);
    if (initializer.printValidationStatus()) {
      // if there where validations errors stop here.
      return WorkerExitCode.VALIDATION_ERRORS;
    }
    // Configuration initialized, start the workers.
    final ExecutorService executor = Executors.newCachedThreadPool();
    final BrokerConnectionFactory factory = new BrokerConnectionFactory(executor, initializer.getBrokerConfig().buildConnectionConfiguration());
    final WorkerConnectionHelper workerConnectionHelper = new WorkerConnectionHelperImpl(factory);
    final WorkerClientPoolContainer container = new WorkerClientPoolContainer(executor, workerConnectionHelper);

    try {
      final List<CompletableFuture<?>> futures = startWorkers(executor, workerConnectionHelper, container, workerPacks);

      if (futures.isEmpty()) {
        LOG.error("No processes started. Maybe a configuration error?");
        return WorkerExitCode.NOTHING_TO_DO;
      } else {
        CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()])).join();
        return WorkerExitCode.SUCCESS;
      }
    } finally {
      try {
        container.stopWorkers();
        Stream.of(workerFactories).forEach(WorkerConfigurationFactory::shutdown);
      } catch (final RuntimeException e) {
        LOG.debug("Exception while stopping worker", e);
      }
      factory.shutdown();
      executor.shutdownNow();
      ExecuteWrapper.EXECUTOR.shutdownNow();
    }
  }

  /**
   * Starts all workers of the worker factories.
   *
   * @param executor executor to start the worker threads on
   * @param workerConnectionHelper Helper class to get connections to external services
   * @param properties configuration properties
   * @return array of CompletableFutures for all processes started
   * @throws Exception Exceptions during startup
   */
  @SuppressWarnings({"unchecked", "rawtypes"})
  private static List<CompletableFuture<?>> startWorkers(final ExecutorService executor, final WorkerConnectionHelper workerConnectionHelper,
      final WorkerClientPoolContainer container, final List<WorkerFactoryPack<?>> workerPacks) throws Exception {
    final List<CompletableFuture<?>> futures = new ArrayList<>();

    for (final WorkerFactoryPack<?> workerPack : workerPacks) {
      final WorkerConfigurationFactory<?> workerFactory = workerPack.getFactory();
      final Runnable runner;

      if (workerFactory instanceof final WorkerFactory wf) {
        runner = container.createWorkerClientPool(wf, workerPack.getConfiguration());
      } else if (workerFactory instanceof final ScheduledWorkerFactory swf) {
        runner = swf.createScheduledWorker(workerPack.getConfiguration(), workerConnectionHelper);
      } else {
        runner = null;
      }
      if (runner != null) {
        futures.add(CompletableFuture.runAsync(runner, executor));
      }
    }
    return futures;
  }
}
