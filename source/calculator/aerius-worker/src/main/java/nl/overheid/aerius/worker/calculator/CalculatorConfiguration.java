/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.calculator;

import nl.overheid.aerius.worker.DatabaseConfiguration;
import nl.overheid.aerius.worker.base.ArchiveConfiguration;
import nl.overheid.aerius.worker.base.HasArchiveConfiguration;

/**
 * Configuration class for calculator/chunker worker.
 */
class CalculatorConfiguration extends DatabaseConfiguration implements HasArchiveConfiguration {

  private ArchiveConfiguration archiveConfiguration = new ArchiveConfiguration();

  public CalculatorConfiguration(final int processes) {
    super(processes);
  }

  @Override
  public ArchiveConfiguration getArchiveConfiguration() {
    return archiveConfiguration;
  }

  public void setArchiveConfiguration(final ArchiveConfiguration archiveConfiguration) {
    this.archiveConfiguration = archiveConfiguration;
  }

}
