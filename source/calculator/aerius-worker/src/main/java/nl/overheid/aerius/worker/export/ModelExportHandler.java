/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.export;

import java.io.File;
import java.util.UUID;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Export handler for Model exports.
 *
 * The model worker will store the file in the file server, and this is stored in the database by the chunker worker.
 * The old export handled the file itself, but that is not needed here so no implementation needed here.
 * The export needs to be refactored in general so all export handlers will be using the file service.
 */
public class ModelExportHandler implements ExportHandler {

  @Override
  public File createFileContent(final CalculationInputData inputData, final JobIdentifier jobIdentifier) {
    throw new UnsupportedOperationException("Model files are generated by the model workers, and not in here. Therefor should never be called.");
  }

  @Override
  public String getZipFileName(final CalculationInputData inputData) {
    return UUID.randomUUID().toString();
  }

}
