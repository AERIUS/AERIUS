/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.export.email;

import java.util.Map;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.shared.domain.email.EmailMessagesEnum;
import nl.overheid.aerius.shared.domain.email.EmailType;
import nl.overheid.aerius.shared.domain.email.ReplacementToken;

/**
 * Provide OwN2000 PDF output specific email message text.
 */
class OwN2000PdfEmailMessage extends FileDownloadEmailMessage {

  public OwN2000PdfEmailMessage(final CalculationInputData data) {
    super(EmailType.PDF_DOWNLOAD, data);
  }

  @Override
  protected void populateEnumReplacementTokens(final Map<ReplacementToken, Enum<?>> map) {
    map.put(ReplacementToken.EXPORT_TYPE_FILE, EmailMessagesEnum.EXPORT_TYPE_FILE_PAA);
    map.put(ReplacementToken.ADDITIONAL_DOWNLOAD_INFO_1, EmailMessagesEnum.DOWNLOAD_IMAER_IMPORT);
    map.put(ReplacementToken.ADDITIONAL_DOWNLOAD_INFO_2, EmailMessagesEnum.DOWNLOAD_PDF_NO_ENGLISH);
  }

  @Override
  public EmailMessagesEnum getMailSubject() {
    return EmailMessagesEnum.DOWNLOAD_PAA_SUBJECT;
  }
}
