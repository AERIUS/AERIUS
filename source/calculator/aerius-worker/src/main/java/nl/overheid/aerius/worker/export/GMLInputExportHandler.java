/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.export;

import java.io.File;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import nl.overheid.aerius.AeriusVersion;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.gml.GMLScenarioHelper;
import nl.overheid.aerius.gml.base.MetaDataInput;
import nl.overheid.aerius.importer.PermitGMLWriter;
import nl.overheid.aerius.shared.domain.FileFormat;
import nl.overheid.aerius.shared.domain.ProductProfile;
import nl.overheid.aerius.shared.domain.scenario.IsScenario;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.FilenameUtil;
import nl.overheid.aerius.worker.export.DirectoryExportHandler.InputExportHandler;

/**
 * Exports GML files specific for RBL theme.
 */
public class GMLInputExportHandler implements InputExportHandler {

  private final String databaseVersion;
  private final PermitGMLWriter gmlWriter;

  /**
   * @param pmf The Persistence Manager Factory to use.
   * @throws SQLException
   */
  public GMLInputExportHandler(final PMF pmf) throws SQLException {
    this.databaseVersion = pmf.getDatabaseVersion();
    this.gmlWriter = new PermitGMLWriter(pmf);
  }

  @Override
  public void exportInput(final CalculationInputData inputData, final File targetDirectory) {
    try {
      writeToFiles(targetDirectory, inputData.getScenario(), inputData.getCreationDate());
    } catch (final AeriusException e) {
      writeErrorInFile(targetDirectory, e, "Error writing GML to file");
    }
  }

  private void writeToFiles(final File targetDirectory, final Scenario scenario, final Date creationDate) throws AeriusException {
    gmlWriter.ensureAllSituationsHaveNewReferences(scenario, ProductProfile.CALCULATOR);
    final List<ScenarioSituation> situations = scenario.getSituations();
    for (final ScenarioSituation situation : situations) {
      final MetaDataInput metaDataInput = constructMetaData(scenario, situation);
      final List<CalculationPointFeature> receptorPoints = scenario.getCustomPointsList();
      final IsScenario gmlScenario = GMLScenarioHelper.constructScenario(situation, receptorPoints);
      final Path filename = Path.of(targetDirectory.getAbsolutePath(), FilenameUtil.getFilename(scenario.getTheme(), FileFormat.GML.getExtension(),
          creationDate, String.valueOf(situations.indexOf(situation))));

      gmlWriter.writeToFile(filename, gmlScenario, metaDataInput);
    }
  }

  private MetaDataInput constructMetaData(final Scenario scenario, final ScenarioSituation situation) {
    return GMLScenarioHelper.constructMetaData(scenario, situation, () -> true, AeriusVersion.getVersionNumber(), databaseVersion);
  }
}
