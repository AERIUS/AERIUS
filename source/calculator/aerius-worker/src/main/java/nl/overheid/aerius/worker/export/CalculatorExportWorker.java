/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.export;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.CalculatorBuildDirector;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.ScenarioFileServerUtil;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.WorkerConnectionHelper;
import nl.overheid.aerius.worker.base.BaseCalculatorWorker;

/**
 * Export worker can handle PDF, GML and CSV exports based on worker.
 */
public class CalculatorExportWorker extends BaseCalculatorWorker {

  private static final Logger LOGGER = LoggerFactory.getLogger(CalculatorExportWorker.class);

  private final CalculatorBuildDirector calculatorBuildDirector;
  private final ExportPostProcessor exportPostProcessor;
  private final RegisterResultsInserter registerResultsInserter;

  public CalculatorExportWorker(final CalculatorBuildDirector calculatorBuildDirector, final WorkerConnectionHelper workerConnectionHelper,
      final ExportPostProcessor exportPostProcessor, final ScenarioFileServerUtil scenarioFileServiceUtil,
      final RegisterResultsInserter registerResultsInserter) {
    super(workerConnectionHelper, scenarioFileServiceUtil);
    this.calculatorBuildDirector = calculatorBuildDirector;
    this.exportPostProcessor = exportPostProcessor;
    this.registerResultsInserter = registerResultsInserter;
  }

  @Override
  protected boolean process(final CalculationInputData inputData, final JobIdentifier jobIdentifier) throws Exception {
    final ExportType exportType = inputData.getExportType();

    if (exportType == null) {
      LOGGER.error("Export type was null. This should not happen: {}", jobIdentifier);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
    final String jobKey = jobIdentifier.getJobKey();
    final boolean workDone;
    final boolean deleteResults;

    jobStateHelper.setJobStatus(jobKey, JobState.PREPARING);
    if (isExportOnly(inputData)) {
      LOGGER.info("Export without calculation of {} for {}", inputData.getExportType(), jobIdentifier);
      postProcess(inputData, jobIdentifier, null);
      workDone = !inputData.isEmailUser();
      deleteResults = !inputData.getExportType().isPdfExportType();
    } else if (exportType.isPdfExportType()) {
      // PAA_DEMAND (or Bijlage Bij Besluit) gets results from Register, so don't re-calculate but import instead.
      if (exportType == ExportType.REGISTER_PDF_PAA_DEMAND) {
        LOGGER.info("Export without calculation Register job for {}", jobIdentifier);
        registerResultsInserter.insertRegisterResults(jobKey, inputData.getScenario());
      } else {
        calculate(inputData, jobIdentifier, jobKey);
      }
      postProcess(inputData, jobIdentifier, null);
      deleteResults = false;
      workDone = false;
    } else {
      final CalculationJob calculationJob = calculate(inputData, jobIdentifier, jobKey);

      postProcess(inputData, jobIdentifier, calculationJob.getResultUrl());
      workDone = !inputData.isEmailUser();
      deleteResults = true;
    }
    if (deleteResults && cleanUpCalculations(jobKey, false)) {
      scenarioFileServiceUtil.removeScenario(jobKey);
    }
    return workDone;
  }

  private static boolean isExportOnly(final CalculationInputData inputData) {
    return inputData.getExportType() == ExportType.GML_SOURCES_ONLY || inputData.getScenarioResults().isPresent();
  }

  private CalculationJob calculate(final CalculationInputData inputData, final JobIdentifier jobIdentifier, final String jobKey) throws Exception {
    cleanUpCalculations(jobKey, true);
    final CalculationJob calculationJob = calculatorBuildDirector.construct(inputData, jobIdentifier).calculate();

    inputData.setScenarioResults(calculationJob.getScenarioCalculations().getScenarioResults());
    return calculationJob;
  }

  private void postProcess(final CalculationInputData inputData, final JobIdentifier jobIdentifier, final String resultUrl) throws Exception {
    jobStateHelper.setJobStatus(jobIdentifier.getJobKey(), JobState.POST_PROCESSING);
    exportPostProcessor.process(inputData, jobIdentifier, resultUrl);
  }

  @Override
  protected void additionalProcessException(final CalculationInputData inputData, final JobIdentifier jobIdentifier, final Exception exception) {
    // This method is called when a task will be dropped from the queue without action from the user (cancellation), so inform the user about it.
    exportPostProcessor.emailException(inputData, jobIdentifier, exception);
  }
}
