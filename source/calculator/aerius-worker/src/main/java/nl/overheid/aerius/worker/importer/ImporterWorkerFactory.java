/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.importer;

import java.io.Serializable;
import java.util.Properties;

import nl.overheid.aerius.archive.ArchiveProxy;
import nl.overheid.aerius.controller.ImaerController;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.http.FileServerProxy;
import nl.overheid.aerius.http.HttpClientProxy;
import nl.overheid.aerius.importer.domain.ImporterInput;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.util.PatternMatcher;
import nl.overheid.aerius.worker.ConfigurationBuilder;
import nl.overheid.aerius.worker.DatabaseWorkerFactory;
import nl.overheid.aerius.worker.JobStateHelper;
import nl.overheid.aerius.worker.Worker;
import nl.overheid.aerius.worker.WorkerConnectionHelper;

public class ImporterWorkerFactory extends DatabaseWorkerFactory<ImporterConfiguration> {

  public ImporterWorkerFactory() {
    super(WorkerType.IMPORTER);
  }

  @Override
  public ConfigurationBuilder<ImporterConfiguration> configurationBuilder(final Properties properties) {
    return new ImporterConfigurationBuilder(properties);
  }

  @Override
  protected Worker<ImporterInput, Serializable> createWorkerHandlerWithPMF(final ImporterConfiguration configuration,
      final WorkerConnectionHelper workerConnectionHelper) throws Exception {
    final PMF pmf = workerConnectionHelper.getPMF();
    final PatternMatcher resultsAllowedVersions = new PatternMatcher(
        ConstantRepository.getString(pmf, ConstantsEnum.IMPORT_RESULTS_ALLOWED_VERSIONS)
    );
    final ImaerController imaerController = new ImaerController(pmf);
    final FileServerProxy fileServerProxy = workerConnectionHelper.createFileServerProxy(configuration);
    final ArchiveProxy archiveProxy = new ArchiveProxy(
        new HttpClientProxy(workerConnectionHelper.getHttpClientManager(), configuration.getArchiveConfiguration(). getArchiveTimeout()),
        configuration.getArchiveConfiguration().getArchiveUrl(), configuration.getArchiveConfiguration().getArchiveApiKey());
    final ScenarioSummaryFactory scenarioSummaryFactory = new ScenarioSummaryFactory(pmf, imaerController, fileServerProxy, archiveProxy);
    return new ImporterWorker(fileServerProxy, imaerController,
        new ResultsInserter(pmf), new JobStateHelper(pmf), scenarioSummaryFactory, resultsAllowedVersions);
  }
}
