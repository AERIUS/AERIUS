/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.export;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.RequeueTaskException;

/**
 * Handler preparing and exporting data to a zip file.
 */
public interface ExportHandler {

  /**
   * Create the content for the zip file.
   *
   * @param inputData
   * @param jobIdentifier
   * @return
   */
  File createFileContent(CalculationInputData inputData, JobIdentifier jobIdentifier)
      throws IOException, SQLException, AeriusException, RequeueTaskException;

  /**
   * Returns the name of the zip file.
   *
   * @param inputData
   * @return
   */
  String getZipFileName(CalculationInputData inputData);
}
