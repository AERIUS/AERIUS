/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.export;

import java.sql.SQLException;

import nl.aerius.taskmanager.client.TaskManagerClientSender;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.WorkerConnectionHelper;

/**
 * Implementation of the export postprocessor for generating PDF exports.
 * PDF exports are handled by a separate worker. Therefore this handler sends the task to generate to PDF to the PDF worker queue directly.
 */
public class ExportToPdfWorkerHandler extends ExportPostProcessor {

  private final TaskManagerClientSender client;

  public ExportToPdfWorkerHandler(final WorkerConnectionHelper workerConnectionHelper, final GMLProvider gmlProvider) throws SQLException {
    super(workerConnectionHelper, gmlProvider);
    client = workerConnectionHelper.createTaskManagerClient();
  }

  @Override
  public void process(final CalculationInputData inputData, final JobIdentifier jobIdentifier, final String resultUrl) throws Exception {
    client.sendTask(inputData, jobIdentifier.getJobKey(), jobIdentifier.getLocalId(), null, WorkerType.PDF.type());
  }
}
