/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.export;

import java.sql.SQLException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.ProductProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.worker.export.paa.GMLGenerator;

/**
 * Provides the GML objects to be sent to the user in case of an error as attachment.
 */
public class GMLProvider {
  private static final Logger LOGGER = LoggerFactory.getLogger(GMLProvider.class);

  private final GMLGenerator gmlGenerator;

  public GMLProvider(final PMF pmf) throws SQLException {
    gmlGenerator = new GMLGenerator(pmf);
  }

  /**
   * Returns a map of GML objects with situation id as key.
   *
   * @param inputData
   * @param productProfile
   * @return
   * @throws AeriusException
   */
  public Map<String, StringDataSource> getGMLs(final CalculationInputData inputData, final ProductProfile productProfile) throws AeriusException {
    return getGMLs(gmlGenerator, inputData, productProfile);
  }

  /**
   * Use a GML generator to get GMLs based on the supplied input data.
   * @param gmlGenerator The GMLGenerator used to generate files.
   * @param inputData The data that has to be converted to GML.
   * @param productProfile
   * @return The gmls for each situation in the scenario.
   * @throws AeriusException Exception indicating something went wrong with generating the GML.
   */
  public static Map<String, StringDataSource> getGMLs(final GMLGenerator gmlGenerator, final CalculationInputData inputData,
      final ProductProfile productProfile) throws AeriusException {
    try {
      return gmlGenerator.createGMLs(inputData.getScenario(), inputData.getCreationDate(), productProfile);
    } catch (final SQLException e) {
      LOGGER.error("SQL Exception generating GML files: ", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }
}
