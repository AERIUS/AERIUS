/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.processmonitor;

import java.util.Properties;

import nl.overheid.aerius.worker.ConfigurationBuilder;
import nl.overheid.aerius.worker.WorkerConfiguration;
import nl.overheid.aerius.worker.processmonitor.ProcessMonitorConfigurationBuilder.ProcessMonitorWorkerConfiguration;

/**
 * Builder for {@link ProcessMonitorWorkerConfiguration}.
 * The ProcessMonitorService is started as a scheduled worker task.
 * This process is always activated, but not all workers make use of it.
 * While there are no specific configuration options for this worker a Builder/Configuration pattern is used which allows for the
 * ProcessMonitorService to be implemented as a ScheduledWorkerTask.
 */
class ProcessMonitorConfigurationBuilder extends ConfigurationBuilder<ProcessMonitorWorkerConfiguration> {

  private static final int ALWAYS_1_PROCESS_RUNNING = 1;

  public ProcessMonitorConfigurationBuilder(final Properties properties) {
    super(properties, "");
  }

  @Override
  protected ProcessMonitorWorkerConfiguration create() {
    return new ProcessMonitorWorkerConfiguration();
  }

  @Override
  protected void build(final ProcessMonitorWorkerConfiguration configuration) {
    // No additional configuration present.
  }

  public static class ProcessMonitorWorkerConfiguration extends WorkerConfiguration {

    public ProcessMonitorWorkerConfiguration() {
      super(ALWAYS_1_PROCESS_RUNNING);
    }
  }
}
