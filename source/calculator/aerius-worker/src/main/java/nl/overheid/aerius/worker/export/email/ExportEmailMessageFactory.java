/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.export.email;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.email.message.EmailMessage;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Factory class to get a export type specific {@link EmailMessage} object.
 */
public final class ExportEmailMessageFactory {

  private ExportEmailMessageFactory() {
    // Util class
  }

  public static EmailMessage<CalculationInputData> create(final CalculationInputData data, final JobIdentifier jobIdentifier) {
    final boolean isProtected = jobIdentifier.isProtected();

    return switch (data.getExportType()) {
      case PDF_PAA,
           PDF_SINGLE_SCENARIO,
           PDF_PROCUREMENT_POLICY -> isProtected ? new ProtectedOwN2000PdfEmailMessage(data, jobIdentifier) : new OwN2000PdfEmailMessage(data);
      case GML_WITH_RESULTS,
           GML_WITH_SECTORS_RESULTS,
           GML_SOURCES_ONLY -> isProtected ? new ProtectedGMLEmailMessage(data, jobIdentifier) : new GMLEmailMessage(data);
      case CSV, CIMLK_CSV -> new CSVEmailMessage(data);
      case ADMS, OPS -> new EngineInputEmailMessage(data);
      case REGISTER_PDF_PAA_DEMAND,
           REGISTER_JSON_PROJECT_CALCULATION,
           CALCULATION_UI -> null;
      default -> null;// no email needs to be send.
    };
  }
}
