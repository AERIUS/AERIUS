/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.pdf;

import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;

/**
 * Class to filter out only the relevant situations to use in PDF generation.
 */
class PdfSituationFilter implements UnaryOperator<List<ScenarioSituation>> {
  // ToDo: this should probably use information from the CalculationJobType, or just assume the right situations are present in the calculation job.
  // Currently this is kind of relevant, because the assumption still exists that multiple PDF's could be generated for a calculation.
  private static final Set<SituationType> OWN2000_SITUATION_TYPES = EnumSet.of(
      SituationType.REFERENCE,
      SituationType.OFF_SITE_REDUCTION,
      SituationType.PROPOSED);

  private final ExportType exportType;

  public PdfSituationFilter(final ExportType exportType) {
    this.exportType = exportType;
  }

  /**
   * Returns the list of relevant gmls for each proposed situation in the scenario.
   */
  @Override
  public List<ScenarioSituation> apply(final List<ScenarioSituation> situations) {
    if (exportType == ExportType.PDF_PAA || exportType == ExportType.REGISTER_PDF_PAA_DEMAND) {
      return filterSituationsForEachProjectCalculation(situations);
    } else if (exportType == ExportType.PDF_PROCUREMENT_POLICY) {
      return filterSituations(situations, s -> s.getType() == SituationType.REFERENCE);
    } else if (exportType == ExportType.PDF_SINGLE_SCENARIO) {
      return filterSituations(situations, s -> true);
    } else {
      return situations;
    }
  }

  /**
   * Get a list of relevant gmls for each proposed situation in the scenario.
   */
  public List<ScenarioSituation> filterSituationsForEachProjectCalculation(final List<ScenarioSituation> situations) {
    return getProposedSituation(situations).map(proposedSituation -> situations.stream()
        .filter(situation -> isRelevantForOwN2000Export(situation, proposedSituation.getId()))
        .toList()).orElseGet(List::of);
  }

  private static Optional<ScenarioSituation> getProposedSituation(final List<ScenarioSituation> situations) {
    return situations.stream().filter(scenarioSituation -> scenarioSituation.getType() == SituationType.PROPOSED).findAny();
  }

  /**
   * Build GML dataSources for all relevant situations in the scenario
   *
   * @param scenario a scenario
   * @param date creation date of the scenario
   * @param productProfile product profile, relevant for reference generation
   * @return GMLs of all OwN2000 relevant situations in the input scenario by their situation id.
   */
  public static List<ScenarioSituation> filterSituations(final List<ScenarioSituation> situations, final Predicate<ScenarioSituation> filter) {
    return situations.stream().filter(PdfSituationFilter::isRelevantForOwN2000Export).filter(filter).toList();
  }

  private static boolean isRelevantForOwN2000Export(final ScenarioSituation situation, final String proposedSituationId) {
    if (situation.getType() == SituationType.PROPOSED && !situation.getId().equals(proposedSituationId)) {
      return false;
    }
    return isRelevantForOwN2000Export(situation);
  }

  private static boolean isRelevantForOwN2000Export(final ScenarioSituation situation) {
    return OWN2000_SITUATION_TYPES.contains(situation.getType());
  }

}
