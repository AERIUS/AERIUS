/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.export;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.shared.domain.FileFormat;
import nl.overheid.aerius.util.FilenameUtil;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Worker to ZIP and mail the files that were generated and written in a specific directory.
 */
public class DirectoryExportHandler implements ExportHandler {

  public interface InputExportHandler {
    void exportInput(CalculationInputData inputData, File targetDirectory);

    default void writeErrorInFile(final File targetDirectory, final Exception e, final String message) {
      LOG.error(message, e);
      try {
        FileUtils.writeStringToFile(new File(targetDirectory, "error.log"),
            message + ": " + ExceptionUtils.getStackTrace(e), true);
      } catch (final IOException e1) {
        LOG.trace("Error writing log to file", e1);
      }
    }
  }

  private static final Logger LOG = LoggerFactory.getLogger(DirectoryExportHandler.class);

  private static final String FILENAME_FORMAT = "AERIUS_%s";

  private final File filesToExportDirectory;
  private final String filenamePrefix;
  private final List<InputExportHandler> inputExportHandlers = new ArrayList<>();

  public DirectoryExportHandler(final File filesToExportDirectory, final String filenamePrefix, final InputExportHandler... inputExportHandlers) {
    this.filesToExportDirectory = filesToExportDirectory;
    this.filenamePrefix = String.format(FILENAME_FORMAT, filenamePrefix);
    this.inputExportHandlers.addAll(Arrays.asList(inputExportHandlers));
  }

  @Override
  public File createFileContent(final CalculationInputData inputData, final JobIdentifier jobIdentifier) {
    generateInputExports(inputData);
    LOG.info("Generating ZIP for files, reference: {}", inputData.getScenario().getReference());
    return filesToExportDirectory;
  }

  @Override
  public String getZipFileName(final CalculationInputData inputData) {
    return FilenameUtil.getFilename(filenamePrefix, FileFormat.ZIP.getExtension(), inputData.getCreationDate());
  }

  private void generateInputExports(final CalculationInputData inputData) {
    for (final InputExportHandler inputExportHandler : inputExportHandlers) {
      inputExportHandler.exportInput(inputData, filesToExportDirectory);
    }
  }

}
