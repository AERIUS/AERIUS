/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.importer;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.CalculationRepository;
import nl.overheid.aerius.db.calculator.JobRepository;
import nl.overheid.aerius.db.common.decision.DecisionFrameworkRepository;
import nl.overheid.aerius.db.common.results.ResultsSummaryRepository;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculatedSnapshotValues;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculationsImpl;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.AeriusSubPoint;
import nl.overheid.aerius.shared.domain.geo.IsSubPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.geojson.Feature;
import nl.overheid.aerius.shared.domain.v2.geojson.IsFeature;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.point.CIMLKCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointVisitor;
import nl.overheid.aerius.shared.domain.v2.point.CustomCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.NcaCustomCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.ReceptorPoint;
import nl.overheid.aerius.shared.domain.v2.point.SubPoint;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituationResults;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Class to do the result inserts when importing a file with results.
 */
class ResultsInserter {

  /**
   * Convenience class to avoid having to supply multiple parameters for all methods.
   */
  private class ResultsContext {

    private final String jobKey;
    private final ScenarioCalculations scenarioCalculations;
    private final Map<String, ScenarioSituationResults> situationsWithResults;
    private final Map<String, String> situationsWithVersions;
    private final boolean persistSubPoints;
    private final Set<Integer> receptorsWithSubPoints;

    private ResultsContext(final String jobKey, final ScenarioCalculations scenarioCalculations,
        final Map<String, ScenarioSituationResults> situationsWithResults, final Map<String, String> situationsWithVersions,
        final boolean persistSubPoints, final Set<Integer> receptorsWithSubPoints) {
      this.jobKey = jobKey;
      this.scenarioCalculations = scenarioCalculations;
      this.situationsWithResults = situationsWithResults;
      this.situationsWithVersions = situationsWithVersions;
      this.persistSubPoints = persistSubPoints;
      this.receptorsWithSubPoints = receptorsWithSubPoints;
    }

  }

  private static final Logger LOGGER = LoggerFactory.getLogger(ResultsInserter.class);

  private final PMF pmf;
  private final ResultsSummaryRepository summaryRepository;
  private final DecisionFrameworkRepository decisionFrameworkRepository;

  ResultsInserter(final PMF pmf) {
    this.pmf = pmf;
    this.summaryRepository = new ResultsSummaryRepository(pmf);
    this.decisionFrameworkRepository = new DecisionFrameworkRepository(pmf, null);
  }

  public ScenarioCalculations insertResults(final String jobKey, final Scenario scenario, final List<SituationResultsImport> situationResultImports,
      final ArchiveResultsImport archiveResultsImport) throws AeriusException {
    final ScenarioCalculations scenarioCalculations = new ScenarioCalculationsImpl(scenario);
    final boolean persistSubPoints = scenarioCalculations.getScenario().getTheme() == Theme.NCA;
    final Map<String, ScenarioSituationResults> situationsWithResults = situationResultImports.stream()
        .collect(Collectors.toMap(SituationResultsImport::getSituationId, SituationResultsImport::getResults));
    final Map<String, String> situationsWithVersions = situationResultImports.stream()
        .collect(Collectors.toMap(SituationResultsImport::getSituationId, SituationResultsImport::getVersion));
    final Set<Integer> receptorsWithSubPoints = determineReceptorsWithSubPoints(situationsWithResults);
    final ResultsContext resultsContext = new ResultsContext(jobKey, scenarioCalculations, situationsWithResults, situationsWithVersions,
        persistSubPoints, receptorsWithSubPoints);
    try (final Connection connection = pmf.getConnection()) {
      insertCalculations(connection, resultsContext);
      insertArchiveResults(connection, resultsContext, archiveResultsImport);
      persistCalculatedSnapshotValues(connection, jobKey, scenario.getOptions().getCalculatedSnapshotValues());
      determineResultsSummaries(connection, jobKey, scenario.getOptions().getCalculationJobType());
    } catch (final SQLException | IOException e) {
      LOGGER.error("Error while trying to insert imported results", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
    return scenarioCalculations;
  }

  private void insertCalculations(final Connection con, final ResultsContext resultsContext) throws SQLException, AeriusException {
    CalculationRepository.insertCalculations(con, resultsContext.scenarioCalculations, true, resultsContext.persistSubPoints);
    JobRepository.attachCalculations(con, resultsContext.jobKey, resultsContext.scenarioCalculations.getCalculations(),
        resultsContext.situationsWithVersions);
    if (resultsContext.persistSubPoints) {
      final Collection<IsSubPoint> subPointsInCalculations = toSubPoints(resultsContext);
      persistSubPoints(con, resultsContext.scenarioCalculations.getCalculationPointSetTracker().getCalculationSubPointSetId(),
          subPointsInCalculations);
    }
    for (final Entry<String, ScenarioSituationResults> entry : resultsContext.situationsWithResults.entrySet()) {
      insertCalculationResults(con, entry.getKey(), entry.getValue(), resultsContext);
    }
  }

  private Set<Integer> determineReceptorsWithSubPoints(final Map<String, ScenarioSituationResults> situationsWithResults) {
    return situationsWithResults.values().stream()
        .flatMap(x -> x.getResults().stream())
        .filter(x -> x.getProperties() instanceof SubPoint)
        .map(x -> (SubPoint) x.getProperties())
        .map(x -> x.getReceptorId())
        .collect(Collectors.toSet());
  }

  private Collection<IsSubPoint> toSubPoints(final ResultsContext resultsContext) {
    final Map<String, IsSubPoint> subPointMap = new HashMap<>();
    for (final ScenarioSituationResults results : resultsContext.situationsWithResults.values()) {
      for (final CalculationPointFeature pointFeature : results.getResults()) {
        if (pointFeature.getProperties() instanceof SubPoint) {
          // Convert all sub points as they are.
          final SubPoint subPoint = (SubPoint) pointFeature.getProperties();
          final String subPointKey = subPointKey(subPoint.getReceptorId(), subPoint.getSubPointId());
          subPointMap.computeIfAbsent(subPointKey, key -> new AeriusSubPoint(pointFeature, subPoint));
        } else if (pointFeature.getProperties() instanceof ReceptorPoint) {
          // Add a subpoint for a receptor only if it had no sub points.
          // This is similar to what is done in UK calculations.
          final ReceptorPoint receptor = (ReceptorPoint) pointFeature.getProperties();
          if (!resultsContext.receptorsWithSubPoints.contains(receptor.getId())) {
            final String subPointKey = subPointKey(receptor.getReceptorId(), 0);
            subPointMap.computeIfAbsent(subPointKey, key -> new AeriusSubPoint(pointFeature, receptor));
          }
        } else {
          // Ignore other types, not expected to be subpoints.
        }
      }
    }
    return subPointMap.values();
  }

  private String subPointKey(final int receptorId, final int subPointId) {
    return receptorId + "-" + subPointId;
  }

  private void persistSubPoints(final Connection con, final Integer calculationSubPointSetId, final Collection<IsSubPoint> pointsToPersist)
      throws SQLException {
    if (calculationSubPointSetId != null) {
      CalculationRepository.insertCalculationSubPointsForSet(con, calculationSubPointSetId, new ArrayList<>(pointsToPersist));
    }
  }

  private void insertCalculationResults(final Connection con, final String situationId, final ScenarioSituationResults situationResults,
      final ResultsContext resultsContext) throws SQLException, AeriusException {
    final ScenarioSituation situation = resultsContext.scenarioCalculations.getScenario().getSituations().stream()
        .filter(x -> situationId.equals(x.getId()))
        .findFirst()
        .orElseThrow(() -> new IllegalArgumentException("Did not find situation in scenario for ID " + situationId));
    final int calculationId = resultsContext.scenarioCalculations.getCalculationForSituation(situation)
        .map(Calculation::getCalculationId)
        .orElseThrow(() -> new AeriusException(AeriusExceptionReason.INTERNAL_ERROR));
    final List<AeriusResultPoint> resultPoints = toResultPoints(situationResults.getResults(), resultsContext);
    CalculationRepository.insertCalculationResults(con, calculationId, resultPoints);
    CalculationRepository.updateCalculationState(con, calculationId, CalculationState.COMPLETED);
  }

  private void insertArchiveResults(final Connection con, final ResultsContext resultsContext,
      final ArchiveResultsImport archiveResultsImport)
      throws SQLException, AeriusException, IOException {
    final Optional<Calculation> proposedCalculation = resultsContext.scenarioCalculations.getCalculationForSituationType(SituationType.PROPOSED);
    if (archiveResultsImport != null && proposedCalculation.isPresent()) {
      CalculationRepository.insertArchiveContribution(con, proposedCalculation.get().getCalculationId(),
          toResultPoints(archiveResultsImport.getResults(), resultsContext));
      CalculationRepository.insertArchiveMetadata(con, proposedCalculation.get().getCalculationId(),
          archiveResultsImport.getMetadata());
    }
  }

  private List<AeriusResultPoint> toResultPoints(final List<CalculationPointFeature> features, final ResultsContext resultsContext)
      throws AeriusException {
    final FeatureToResultPointVisitor visitor = new FeatureToResultPointVisitor(resultsContext.scenarioCalculations.getScenario());
    final List<AeriusResultPoint> resultPoints = new ArrayList<>();
    for (final CalculationPointFeature feature : features) {
      resultPoints.add(feature.accept(visitor));
      // Ensure a result sub point is added for receptors that do not have any sub points.
      if (resultsContext.persistSubPoints && feature.getProperties() instanceof ReceptorPoint) {
        final ReceptorPoint receptor = (ReceptorPoint) feature.getProperties();
        if (!resultsContext.receptorsWithSubPoints.contains(receptor.getId())) {
          resultPoints.add(visitor.toResultPoint(0, receptor.getReceptorId(), AeriusPointType.SUB_RECEPTOR, feature, receptor));
        }
      }
    }
    return resultPoints;
  }

  private void persistCalculatedSnapshotValues(final Connection connection, final String jobKey, final CalculatedSnapshotValues snapshotValues)
      throws SQLException, AeriusException {
    if (snapshotValues != null
        && snapshotValues.getDevelopmentPressureClassification() != null
        && !snapshotValues.getDevelopmentPressureClassification().isEmpty()) {
      final int jobId = JobRepository.getJobId(connection, jobKey);
      decisionFrameworkRepository.insertDevelopmentPressureClass(jobId, snapshotValues.getDevelopmentPressureClassification());
    }
  }

  private void determineResultsSummaries(final Connection connection, final String jobKey, final CalculationJobType calculationJobType)
      throws SQLException, AeriusException {
    final SituationCalculations situationCalculations = JobRepository.getSituationCalculations(connection, jobKey);
    final int jobId = JobRepository.getJobId(connection, jobKey);
    summaryRepository.insertResultsSummaries(jobId, situationCalculations, calculationJobType);
  }

  class FeatureToResultPointVisitor implements CalculationPointVisitor<AeriusResultPoint> {

    private final Scenario scenario;

    private FeatureToResultPointVisitor(final Scenario scenario) {
      this.scenario = scenario;
    }

    @Override
    public AeriusResultPoint visit(final ReceptorPoint calculationPoint, final IsFeature feature) throws AeriusException {
      return toResultPoint(calculationPoint.getReceptorId(), null, AeriusPointType.RECEPTOR, feature, calculationPoint);
    }

    @Override
    public AeriusResultPoint visit(final CustomCalculationPoint calculationPoint, final IsFeature feature) throws AeriusException {
      return toResultPoint(determineId(calculationPoint), null, AeriusPointType.POINT, feature, calculationPoint);
    }

    @Override
    public AeriusResultPoint visit(final NcaCustomCalculationPoint calculationPoint, final IsFeature feature) throws AeriusException {
      return toResultPoint(determineId(calculationPoint), null, AeriusPointType.POINT, feature, calculationPoint);
    }

    @Override
    public AeriusResultPoint visit(final CIMLKCalculationPoint calculationPoint, final IsFeature feature) throws AeriusException {
      return toResultPoint(determineId(calculationPoint), null, AeriusPointType.POINT, feature, calculationPoint);
    }

    @Override
    public AeriusResultPoint visit(final SubPoint calculationPoint, final IsFeature feature) throws AeriusException {
      return toResultPoint(calculationPoint.getSubPointId(), calculationPoint.getReceptorId(), AeriusPointType.SUB_RECEPTOR, feature,
          calculationPoint);
    }

    /*
     * Custom points should have been re-ID-ed by the frontend.
     * This method tries to map the point based on the GML ID, which should still be the same, to get the correct ID.
     */
    private int determineId(final CustomCalculationPoint customPoint) {
      if (customPoint.getGmlId() != null) {
        // try to map by GML ID
        return scenario.getCustomPointsList().stream()
            .map(Feature::getProperties)
            .filter(CustomCalculationPoint.class::isInstance)
            .map(CustomCalculationPoint.class::cast)
            .filter(x -> customPoint.getGmlId().equals(x.getGmlId()))
            .map(CustomCalculationPoint::getId)
            .findFirst().orElse(customPoint.getId());
      } else {
        return customPoint.getId();
      }
    }

    private AeriusResultPoint toResultPoint(final int id, final Integer parentId, final AeriusPointType type, final IsFeature feature,
        final CalculationPoint originalPoint) {
      final Point point = getPoint(feature);
      final AeriusResultPoint resultPoint = new AeriusResultPoint(id, parentId, type, point.getX(), point.getY());
      originalPoint.getResults().forEach(resultPoint::setEmissionResult);
      return resultPoint;
    }

    private Point getPoint(final IsFeature feature) {
      // The geometry should be a point in all cases. If not, don't bother freaking out, as the ID is what we want anyway.
      return feature.getGeometry() instanceof final Point point ? point : new Point();
    }

  }

}
