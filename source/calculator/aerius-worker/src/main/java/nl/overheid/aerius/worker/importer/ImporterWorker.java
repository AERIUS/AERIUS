/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.importer;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.AeriusVersion;
import nl.overheid.aerius.controller.ImaerController;
import nl.overheid.aerius.http.FileServerProxy;
import nl.overheid.aerius.http.HttpException;
import nl.overheid.aerius.i18n.AeriusExceptionMessages;
import nl.overheid.aerius.importer.ImportOption;
import nl.overheid.aerius.importer.ValidationFactory;
import nl.overheid.aerius.importer.domain.ImportTaskInput;
import nl.overheid.aerius.importer.domain.ImporterInput;
import nl.overheid.aerius.importer.domain.SaveImportedResultsTaskInput;
import nl.overheid.aerius.importer.domain.SituationWithFileId;
import nl.overheid.aerius.importer.domain.SummarizeTaskInput;
import nl.overheid.aerius.io.FileServiceImportableFile;
import nl.overheid.aerius.shared.FileServerExpireTag;
import nl.overheid.aerius.shared.FileServerFile;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.importer.ImportProperties;
import nl.overheid.aerius.shared.domain.ops.OPSCustomCalculationPoint;
import nl.overheid.aerius.shared.domain.scenario.SituationStats;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CustomCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituationResults;
import nl.overheid.aerius.shared.domain.validation.FileValidationStatus;
import nl.overheid.aerius.shared.domain.validation.ValidationStatus;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.util.PatternMatcher;
import nl.overheid.aerius.util.ScenarioObjectMapperUtil;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.JobStateHelper;
import nl.overheid.aerius.worker.Worker;
import nl.overheid.aerius.worker.WorkerTimeLogger;

/**
 * Worker to validate imported files, and convert them to json that can be read by the user interface.
 */
class ImporterWorker implements Worker<ImporterInput, Serializable> {

  private static final Logger LOG = LoggerFactory.getLogger(ImporterWorker.class);

  private static final int FILE_NOT_FOUND = 404;

  private static final Set<ImportOption> DEFAULT_OPTIONS = EnumSet.of(
      ImportOption.INCLUDE_CALCULATION_POINTS,
      ImportOption.INCLUDE_CIMLK_CORRECTIONS,
      ImportOption.INCLUDE_CIMLK_DISPERSION_LINES,
      ImportOption.INCLUDE_CIMLK_MEASURES,
      ImportOption.INCLUDE_RESULTS,
      ImportOption.INCLUDE_SOURCES,
      ImportOption.USE_IMPORTED_LANDUSES,
      ImportOption.USE_VALID_SECTORS,
      ImportOption.VALIDATE_AGAINST_SCHEMA,
      ImportOption.VALIDATE_SOURCES,
      ImportOption.WARNING_ON_GEOMETRY_LIMITS);

  private final FileServerProxy fileServerProxy;
  private final ImaerController imaerController;
  private final ResultsInserter resultsInserter;
  private final JobStateHelper jobStateHelper;
  private final PatternMatcher resultsAllowedVersions;
  private final ObjectMapper objectMapper = ScenarioObjectMapperUtil.getScenarioObjectMapper();
  private final ScenarioSummaryFactory scenarioSummaryFactory;

  public ImporterWorker(final FileServerProxy fileServerProxy,
      final ImaerController imaerController, final ResultsInserter resultsInserter, final JobStateHelper jobStateHelper,
      final ScenarioSummaryFactory scenarioSummaryFactory, final PatternMatcher resultsAllowedVersions) {
    this.imaerController = imaerController;
    this.fileServerProxy = fileServerProxy;
    this.resultsInserter = resultsInserter;
    this.jobStateHelper = jobStateHelper;
    this.resultsAllowedVersions = resultsAllowedVersions;
    this.scenarioSummaryFactory = scenarioSummaryFactory;
  }

  @Override
  public Serializable run(final ImporterInput importerInput, final JobIdentifier jobIdentifier,
      final WorkerIntermediateResultSender workerIntermediateResultSender) throws Exception {
    jobStateHelper.failIfJobCancelledByUser(jobIdentifier);
    final WorkerTimeLogger timeLogger = WorkerTimeLogger.start(WorkerType.IMPORTER.name());

    try {
      if (importerInput instanceof final ImportTaskInput importInput) {
        validateAndImportFile(importInput.getUuid(), importInput.getImportFilename(), importInput.getOriginalFilename(),
            importInput.getImportProperties(), importInput.getExpire(), importInput.getLocale());
      } else if (importerInput instanceof final SaveImportedResultsTaskInput sirti) {
        saveResults(sirti);
      } else if (importerInput instanceof final SummarizeTaskInput sti) {
        summarizeImport(sti);
      } else {
        throw new UnsupportedOperationException("Worker does not know how to handle the input:"
            + importerInput.getClass().getName());
      }
      importerInput.fileIdsToCleanupOnSuccess().stream().forEach(this::cleanupFileId);
      return null;
    } catch (final RuntimeException | AeriusException e) {
      // When a runtime or aerius Exception is thrown the job should be set to error and an endtime should be set.
      // Other exceptions mean the job will get back on the queue, and can be picked up again, so no endtime is set.
      if (JobStateHelper.isNotCancelledJob(e)) {
        processException(jobIdentifier, e);
      }
      importerInput.fileIdsToCleanupOnError().stream().forEach(this::cleanupFileId);
      throw e;
    } finally {
      timeLogger.logDuration(jobIdentifier);
    }
  }

  /**
   * Processes the import file by retrieving the file from the file server and converting them to {@link ImportParcel} objects.
   * If import processing failed a validation error status is written.
   * If the import contains multiple parcels the first parcel will be used to store the file codes to the other parcels.
   * Each parcel will get it's own validation status.
   *
   * @param uuid uuid under which the file is stored on the file server, and the ui queries for the file
   * @param importFilename the name of the file is stored in the file server
   * @param originalFilename original filename as provided with the upload
   * @param importProperties import options
   * @param expire the tag to indicate how long the import file should be stored
   * @param locale
   */
  private void validateAndImportFile(final String uuid, final String importFilename, final String originalFilename,
      final ImportProperties importProperties, final FileServerExpireTag expire, final Locale locale)
      throws AeriusException {
    List<ImportParcel> parcels;

    try {
      parcels = getImportParcels(uuid, importFilename, originalFilename, importProperties, locale);
    } catch (final RuntimeException | AeriusException e) {
      onException(uuid, e, expire, locale);
      return;
    }

    if (parcels.isEmpty()) {
      writeAndSaveResultValidation(uuid, new FileValidationStatus(uuid, "", ValidationStatus.FAILED, "", null, null, null, null), expire);
    } else {
      filterResultsForParcels(parcels);
      // Loop through every parcel except the first one
      final List<String> childFileCodes = handleChildParcels(parcels, expire, locale);

      // Update the folder of the first (parent) parcel
      final ImportParcel parcel = parcels.get(0);
      final FileValidationStatus parentStatus = makeFileValidationStatusForParcel(uuid, parcel, childFileCodes, locale);

      writeAndSaveResult(uuid, parcel, expire);
      writeAndSaveResultValidation(uuid, parentStatus, expire);
    }
  }

  private List<String> handleChildParcels(final List<ImportParcel> parcels, final FileServerExpireTag expire, final Locale locale)
      throws AeriusException {
    // Loop through every parcel except the first one
    final List<String> childFileCodes = new ArrayList<>();
    for (final ImportParcel parcel : parcels) {
      if (parcels.indexOf(parcel) == 0) {
        continue;
      }
      childFileCodes.add(addChildFile(parcel, expire, locale));
    }
    return childFileCodes;
  }

  private String addChildFile(final ImportParcel parcel, final FileServerExpireTag expire, final Locale locale) throws AeriusException {
    final String fileCode = FileServerFile.createId(ImporterInput.IMPORT_PREFIX);
    final FileValidationStatus status = makeFileValidationStatusForParcel(fileCode, parcel, Collections.emptyList(), locale);

    writeAndSaveResult(fileCode, parcel, expire);
    writeAndSaveResultValidation(fileCode, status, expire);
    return fileCode;
  }

  private List<ImportParcel> getImportParcels(final String uuid, final String importFilename, final String originalFilename,
      final ImportProperties importProperties, final Locale locale) throws AeriusException {
    try (InputStream fsis = fileServerProxy.getContentStream(FileServerFile.FREE_FORMAT.format(uuid, importFilename))) {
      return readImportParcels(originalFilename, fsis, importProperties, locale);
    } catch (final IOException | HttpException | URISyntaxException e) {
      LOG.error("Error while reading import parcels", e);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }

  private List<ImportParcel> readImportParcels(final String originalFilename, final InputStream fsis, final ImportProperties importProperties,
      final Locale locale) throws IOException, AeriusException {
    final List<ImportParcel> importParcels;
    try (fsis) {
      importParcels = imaerController.processFile(new FileServiceImportableFile(originalFilename, fsis), DEFAULT_OPTIONS, importProperties, locale);
    }
    importParcels.addAll(extractCalculationPointParcels(importParcels));
    importParcels.stream().flatMap(parcel -> parcel.getCalculationPointsList().stream()).forEach(ImporterWorker::toCorrectType);
    return importParcels;

  }

  /**
   * On Exception during importing parcels write a Failed status message to the file server for the given uuid.
   *
   * @param uuid uuid to store the validation status
   * @param e exception thrown by the import
   * @param expire the tag to indicate how long the import file should be stored
   * @param locale
   * @throws URISyntaxException
   * @throws HttpException
   * @throws IOException
   */
  public void onException(final String uuid, final Exception e, final FileServerExpireTag expire, final Locale locale)
      throws AeriusException {
    LOG.debug("Error while importing file", e);
    final FileValidationStatus status = ValidationFactory.createFailedFileValidationStatus(new AeriusExceptionMessages(locale), uuid, e);

    writeAndSaveResultValidation(uuid, status, expire);
  }

  private void writeAndSaveResult(final String fileCode, final ImportParcel parcel, final FileServerExpireTag expire)
      throws AeriusException {
    // Clear warnings and exceptions
    parcel.getExceptions().clear();
    parcel.getWarnings().clear();
    if (parcel.getSituationResults() != null) {
      putObjectAsJson(fileCode, FileServerFile.RESULTS, parcel.getSituationResults(), expire);
      parcel.setSituationResults(null);
    }
    putObjectAsJson(fileCode, FileServerFile.DATA, parcel, expire);
  }

  private void filterResultsForParcels(final List<ImportParcel> parcels) {
    parcels.forEach(parcel -> {
      // First filter out any custom calculation points in the situation results before checking compatibility
      parcel.setSituationResults(filterActualResults(parcel.getSituationResults()));
      parcel.setSituationResults(filterCompatibleResults(parcel));
    });
  }

  /*
   * Custom calculation points are always added to the situation results.
   * However, they don't necessarily have to contain results, for instance when a PDF with custom calculation points is imported.
   * This function ensures parcels only end up with actual results (that is, points that have at least 1 result).
   */
  private static ScenarioSituationResults filterActualResults(final ScenarioSituationResults importedResults) {
    if (importedResults == null) {
      return null;
    }
    final ScenarioSituationResults filtered = new ScenarioSituationResults();
    filtered.setResults(importedResults.getResults().stream()
        .filter(point -> !point.getProperties().getResults().isEmpty())
        .toList());
    return filtered.getResults().isEmpty() ? null : filtered;
  }

  private ScenarioSituationResults filterCompatibleResults(final ImportParcel parcel) {
    if (parcel.getSituationResults() == null || parcel.getSituationResults().getResults().isEmpty()) {
      return parcel.getSituationResults();
    }
    if (AeriusVersion.getVersionNumber().equals(parcel.getVersion()) || resultsAllowedVersions.matches(parcel.getVersion())) {
      return parcel.getSituationResults();
    } else {
      parcel.getWarnings().add(new AeriusException(AeriusExceptionReason.IMPORTED_CALCULATION_RESULTS_VERSION_NOT_ALLOWED));
      return null;
    }
  }

  private void writeAndSaveResultValidation(final String fileCode, final FileValidationStatus status, final FileServerExpireTag expire)
      throws AeriusException {
    ValidationFactory.updateValidationStatus(status);
    putObjectAsJson(fileCode, FileServerFile.VALIDATION, status, expire);
  }

  private void putObjectAsJson(final String fileCode, final FileServerFile fileServerFile, final Object objectToWrite,
      final FileServerExpireTag expire) throws AeriusException {
    try {
      fileServerProxy.putFile(fileCode, fileServerFile.getFilename(), objectMapper.writeValueAsString(objectToWrite), expire);
    } catch (final IOException | HttpException | URISyntaxException e) {
      LOG.error("Error while writing object to fileserver", e);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }

  private static FileValidationStatus makeFileValidationStatusForParcel(final String uuid, final ImportParcel parcel,
      final List<String> childFileCodes, final Locale locale) {
    final SituationStats stats = new SituationStats();
    stats.setSources(parcel.getSituation().getSources().getFeatures().size());
    stats.setBuildings(parcel.getSituation().getBuildings().getFeatures().size());
    stats.setCalculationPoints(parcel.getCalculationPoints().getFeatures().size());
    stats.setCalculationResults(parcel.getSituationResults() == null ? 0 : parcel.getSituationResults().getResults().size());
    stats.setTimeVaryingProfiles(Optional.ofNullable(parcel.getSituation().getDefinitions())
        .map(x -> x.getCustomTimeVaryingProfiles())
        .map(x -> x.size()).orElse(0));
    stats.setArchive(parcel.getArchiveMetaData() != null);

    final String fileReference = Optional.ofNullable(parcel.getSituation().getReference()).orElse("");
    final CalculationJobType calculationJobType = Optional.ofNullable(parcel.getCalculationSetOptions())
        .map(CalculationSetOptions::getCalculationJobType)
        .orElse(null);

    final FileValidationStatus status = new FileValidationStatus(uuid, fileReference, ValidationStatus.PENDING, parcel.getSituation().getName(),
        parcel.getSituation().getType(), calculationJobType, childFileCodes, stats);

    final AeriusExceptionMessages aem = new AeriusExceptionMessages(locale);
    status.getErrors().addAll(parcel.getExceptions().stream()
        .map(e -> ValidationFactory.mapAeriusExceptionToValidationError(aem, e))
        .toList());
    status.getWarnings().addAll(parcel.getWarnings().stream()
        .map(e -> ValidationFactory.mapAeriusExceptionToValidationError(aem, e))
        .toList());

    return status;
  }

  /**
   * Extract calculation points to new parcels
   */
  private static List<ImportParcel> extractCalculationPointParcels(final List<ImportParcel> importParcels) {
    return importParcels.stream()
        .filter(ImporterWorker::shouldExtractCalculationPoints)
        .map(parcel -> {
          final ImportParcel newParcel = createCalculationPointParcel(parcel);
          parcel.getCalculationPoints().getFeatures().clear();
          return newParcel;
        })
        .toList();
  }

  /**
   *  Only extract calculation points from a parcel if it also contains emission sources or buildings.
   */
  private static boolean shouldExtractCalculationPoints(final ImportParcel parcel) {
    return !parcel.getCalculationPointsList().isEmpty() &&
        (!parcel.getSituation().getEmissionSourcesList().isEmpty() || !parcel.getSituation().getBuildingsList().isEmpty());
  }

  /**
   * Create a new ImportParcel with just the calculation points and import status data from the input parcel.
   */
  private static ImportParcel createCalculationPointParcel(final ImportParcel importParcel) {
    final ImportParcel copy = new ImportParcel();
    copy.setDatabaseVersion(importParcel.getDatabaseVersion());
    copy.setVersion(importParcel.getVersion());
    copy.getCalculationPoints().setFeatures(new ArrayList<>(importParcel.getCalculationPointsList()));
    copy.setCalculationSetOptions(importParcel.getCalculationSetOptions());
    copy.getWarnings().addAll(importParcel.getWarnings());
    copy.getExceptions().addAll(importParcel.getExceptions());
    return copy;
  }

  private static void toCorrectType(final CalculationPointFeature feature) {
    if (feature.getProperties() instanceof final OPSCustomCalculationPoint originalProperties) {
      // OPSCustomCalculationPoint will be converted to JSON with type 'OPSCustomCalculationPoint' by jackson
      // Since this is unknown in the base class CalculationPoint, it won't work well when the points are sent back for calculations/exports
      // 2 options:
      // - move OPSCustomCalculationPoint (or it's properties) to IMAER-java
      // - copy the properties to a type that is converted properly.
      // This is that second option.
      final CustomCalculationPoint copiedProperties = new CustomCalculationPoint();
      copiedProperties.setGmlId(originalProperties.getGmlId());
      copiedProperties.setLabel(originalProperties.getLabel());
      copiedProperties.setDescription(originalProperties.getDescription());
      copiedProperties.setJurisdictionId(originalProperties.getJurisdictionId());
      copiedProperties.setCustomPointId(originalProperties.getCustomPointId());
      copiedProperties.setHeight(originalProperties.getHeight());
      feature.setProperties(copiedProperties);
    }
  }

  private void summarizeImport(final SummarizeTaskInput input) throws AeriusException {
    final String jobKey = input.getJobKey();
    jobStateHelper.setJobStatus(jobKey, JobState.POST_PROCESSING);
    scenarioSummaryFactory.summarize(input);
    jobStateHelper.setJobStatus(jobKey, JobState.COMPLETED);
  }

  private void saveResults(final SaveImportedResultsTaskInput saveResultsImport) throws AeriusException {
    final String jobKey = saveResultsImport.getJobKey();
    jobStateHelper.setJobStatus(jobKey, JobState.POST_PROCESSING);
    addCalculatedSnapshotValues(saveResultsImport);
    final List<SituationResultsImport> situationResultImports = new ArrayList<>();
    for (final SituationWithFileId situationWithFileId : saveResultsImport.getSituationFileIds()) {
      situationResultImports.add(retrieveSituationResults(situationWithFileId));
    }
    final ArchiveResultsImport archiveResults = retrieveArchiveResults(saveResultsImport.getArchiveContributionFileId());
    resultsInserter.insertResults(saveResultsImport.getJobKey(), saveResultsImport.getScenario(), situationResultImports, archiveResults);
    jobStateHelper.setJobStatus(jobKey, JobState.COMPLETED);
  }

  private SituationResultsImport retrieveSituationResults(final SituationWithFileId situationWithFileId)
      throws AeriusException {
    final ImportParcel resultsParcel = retrieveParcel(situationWithFileId.getFileId());
    final ScenarioSituationResults results = retrieveResults(situationWithFileId.getFileId());
    return new SituationResultsImport(situationWithFileId.getSituationId(), results, resultsParcel.getVersion());
  }

  private void addCalculatedSnapshotValues(final SaveImportedResultsTaskInput saveResultsImport)
      throws AeriusException {
    if (saveResultsImport.getScenario().getTheme() != Theme.NCA || saveResultsImport.getSituationFileIds().isEmpty()) {
      return;
    }
    final ImportParcel importParcel = retrieveParcel(saveResultsImport.getSituationFileIds().get(0).getFileId());
    saveResultsImport.getScenario().getOptions().getCalculatedSnapshotValues().setDevelopmentPressureClassification(
        importParcel.getCalculationSetOptions().getCalculatedSnapshotValues().getDevelopmentPressureClassification());
  }

  private ArchiveResultsImport retrieveArchiveResults(final String archiveContributionFileId) throws AeriusException {
    if (archiveContributionFileId == null) {
      return null;
    }
    final ImportParcel archiveParcel = retrieveParcel(archiveContributionFileId);
    final List<CalculationPointFeature> results = retrieveResults(archiveContributionFileId).getResults();
    return new ArchiveResultsImport(archiveParcel.getArchiveMetaData(), results);
  }

  private ScenarioSituationResults retrieveResults(final String fileId) throws AeriusException {
    try (InputStream fsis = fileServerProxy.getContentStream(FileServerFile.RESULTS.format(fileId))) {
      return objectMapper.readValue(fsis, ScenarioSituationResults.class);
    } catch (final HttpException e) {
      // 404 can happen if there were no results, for example an archive contribution file with no relevant projects.
      if (e.getStatusCode() == FILE_NOT_FOUND) {
        return new ScenarioSituationResults();
      } else {
        LOG.error("Error while retrieving results", e);
        throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
      }
    } catch (final IOException | URISyntaxException e) {
      LOG.error("Error while retrieving results", e);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }

  private ImportParcel retrieveParcel(final String fileId) throws AeriusException {
    try (InputStream fsis = fileServerProxy.getContentStream(FileServerFile.DATA.format(fileId))) {
      return objectMapper.readValue(fsis, ImportParcel.class);
    } catch (final IOException | HttpException | URISyntaxException e) {
      LOG.error("Error while reading import parcel", e);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }

  /**
   * When a runtime or AERIUS Exception is thrown the job is considered finished (though in an error state),
   * an endtime should be set and the corresponding scenario is cleaned from fileservice.
   * Other exceptions mean the job will get back on the queue, and can be picked up again, so then this method should not be called.
   */
  private void processException(final JobIdentifier jobIdentifier, final Exception exception) {
    final String jobKey = jobIdentifier.getJobKey();
    jobStateHelper.setErrorStateAndMessageJob(jobKey, exception);
  }

  private void cleanupFileId(final String fileId) {
    try {
      fileServerProxy.deleteRemoteContent(FileServerFile.ALL.format(fileId));
    } catch (RuntimeException | IOException | HttpException | URISyntaxException e) {
      LOG.warn("Failed to clean up after save result import job for file id {}: {}", fileId, e.getMessage());
    }
  }
}
