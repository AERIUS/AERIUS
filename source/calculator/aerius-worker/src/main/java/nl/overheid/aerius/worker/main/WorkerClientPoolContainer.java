/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.main;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.WorkerConfiguration;
import nl.overheid.aerius.worker.WorkerConnectionHelper;
import nl.overheid.aerius.worker.WorkerFactory;
import nl.overheid.aerius.worker.WorkerHandlerImpl;
import nl.overheid.aerius.worker.WorkerProperties;
import nl.overheid.aerius.worker.client.WorkerClientPool;

/**
 * Helper class to start and manage and workers.
 */
class WorkerClientPoolContainer {

  private static final Logger LOG = LoggerFactory.getLogger(WorkerClientPoolContainer.class);

  private final ExecutorService executor;
  private final WorkerConnectionHelper workerConnectionHelper;
  private final List<WorkerClientPool> pools = new ArrayList<>();

  public WorkerClientPoolContainer(final ExecutorService executor, final WorkerConnectionHelper workerConnectionHelper) {
    this.executor = executor;
    this.workerConnectionHelper = workerConnectionHelper;
  }

  /**
   * Creates a WorkerClientPool Start a number of workers if the specific workers are configured.
   *
   * @param workerFactory factory to create worker configuration and runner
   * @param configuration The configuration that tells us how many workers should be started
   * @return returns a newly created {@link WorkerClientPool} that should be started by calling run or returns null if #workers is 0
   * @throws IOException In case of startup problems.
   * @throws AeriusException exception
   * @throws SQLException
   */
  public <C extends WorkerConfiguration> WorkerClientPool createWorkerClientPool(final WorkerFactory<C> workerFactory, final C configuration)
      throws Exception {
    final WorkerClientPool client;
    final int workers = determineNumberOfprocesses(configuration.getProcesses());
    // if no workers defined or no worker queuename is defined, don't start any worker consumers.
    if (workers == 0) {
      return null;
    }
    LOG.debug("Creating {} worker clients for worker {}", workers, workerFactory.getWorkerType());
    final WorkerType type = workerFactory.getWorkerType();
    final BrokerConnectionFactory brokerConnectionFactory = workerConnectionHelper.getBrokerConnectionFactory();

    client = new WorkerClientPool(executor, brokerConnectionFactory,
        new WorkerHandlerImpl<>(workerFactory.createWorkerHandler(configuration, workerConnectionHelper), type,
            workerConnectionHelper.getProcessMonitorService()),
        type, workers, Optional.of(brokerConnectionFactory.getConnectionConfiguration().getBrokerRetryWaitTime()));
    pools.add(client);
    return client;
  }

  /**
   * Determines the actual number of processes to start. If auto configure base configuration on available cores.
   *
   * @param configuredProcesses Number of configured processes
   * @return derived number of processes to start
   */
  private int determineNumberOfprocesses(final int configuredProcesses) {
    return configuredProcesses == WorkerProperties.AUTO_PROCESSES ? (Runtime.getRuntime().availableProcessors() - 1) : configuredProcesses;
  }

  /**
   * Stop any workers currently consuming.
   */
  public void stopWorkers() {
    for (final WorkerClientPool pool : pools) {
      try {
        pool.stopAllConsuming();
      } catch (final IOException e) {
        LOG.debug("Exception while stopping worker client pool", e);
      }
    }
    pools.clear();
  }
}
