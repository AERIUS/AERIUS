/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.base;

public class ArchiveConfiguration {
  /**
   * Url to the Archive server.
   */
  private String archiveUrl;
  /**
   * API-Key for the Archive server.
   */
  private String archiveApiKey;
  /**
   * Timeout to the Archive server.
   */
  private int archiveTimeout;

  public String getArchiveUrl() {
    return archiveUrl;
  }

  public void setArchiveUrl(final String archiveUrl) {
    this.archiveUrl = archiveUrl;
  }

  public String getArchiveApiKey() {
    return archiveApiKey;
  }

  public void setArchiveApiKey(final String archiveApiKey) {
    this.archiveApiKey = archiveApiKey;
  }

  public int getArchiveTimeout() {
    return archiveTimeout;
  }

  public void setArchiveTimeout(final int archiveTimeout) {
    this.archiveTimeout = archiveTimeout;
  }
}
