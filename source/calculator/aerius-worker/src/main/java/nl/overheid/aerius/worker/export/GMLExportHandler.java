/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.export;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.BooleanSupplier;
import java.util.function.UnaryOperator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.AeriusVersion;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.CalculationInfoRepository;
import nl.overheid.aerius.db.calculator.CalculationRepository;
import nl.overheid.aerius.db.calculator.JobRepository;
import nl.overheid.aerius.db.common.decision.DecisionFrameworkRepository;
import nl.overheid.aerius.gml.GMLScenarioHelper;
import nl.overheid.aerius.gml.base.MetaDataInput;
import nl.overheid.aerius.importer.PermitGMLWriter;
import nl.overheid.aerius.shared.domain.FileFormat;
import nl.overheid.aerius.shared.domain.ProductProfile;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.scenario.IsScenario;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.sector.category.SimpleCategory;
import nl.overheid.aerius.shared.domain.v2.archive.ArchiveMetaData;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioResults;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituationResults;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.CalculationPointDuplicator;
import nl.overheid.aerius.util.FilenameUtil;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Handler converts sources and custom calculation points to GML.
 * When calculation ID supplied is not null, results for that calculation are also converted.
 * Sends generic mails containing the generated .gml file after it is done generating files (or when an error occurred).
 */
public class GMLExportHandler implements ExportHandler {

  private static final Logger LOG = LoggerFactory.getLogger(GMLExportHandler.class);

  private final PMF pmf;
  private final DecisionFrameworkRepository decisionFrameworkRepository;
  private final PermitGMLWriter writer;
  private final UnaryOperator<List<ScenarioSituation>> filterSituations;
  private final File folderForJob;

  /**
   * @param pmf The Persistence Manager Factory to use.
   * @param folderForJob
   * @param filterSituations filter method to export only specific situations
   * @throws SQLException
   */
  public GMLExportHandler(final PMF pmf, final File folderForJob, final UnaryOperator<List<ScenarioSituation>> filterSituations) throws SQLException {
    this.folderForJob = folderForJob;
    this.pmf = pmf;
    this.decisionFrameworkRepository = new DecisionFrameworkRepository(pmf, null);
    this.filterSituations = filterSituations;
    writer = new PermitGMLWriter(pmf);
    writer.setFormattedOutput(Boolean.FALSE);
  }

  /**
   * @see PermitGMLWriter#setNoReceptorRepresentation()
   */
  public void setNoReceptorRepresentation() {
    writer.setNoReceptorRepresentation();
  }

  @Override
  public final File createFileContent(final CalculationInputData inputData, final JobIdentifier jobIdentifier)
      throws IOException, AeriusException, SQLException {
    writer.ensureAllSituationsHaveNewReferences(inputData.getScenario(), ProductProfile.CALCULATOR);
    LOG.debug("Generating ZIP for project, reference: {}", inputData.getScenario().getReference());
    createGMLs(folderForJob, inputData, jobIdentifier);
    return folderForJob;
  }

  private void createGMLs(final File tempDir, final CalculationInputData inputData, final JobIdentifier jobIdentifier)
      throws AeriusException, SQLException {
    final Scenario scenario = inputData.getScenario();
    addCalculatedSnapshotValues(jobIdentifier, scenario);
    final List<ScenarioSituation> situations = filterSituations.apply(inputData.getScenario().getSituations());
    final Optional<ScenarioResults> scenarioResults = inputData.getScenarioResults();
    defaultGmlExport(tempDir, scenario, situations, inputData.getExportType().isPdfExportType(), scenarioResults, inputData.getCreationDate());

    // Add individual sector result gml files
    if (inputData.isSectorOutput()) {
      sectorsGmlExport(tempDir, scenario, situations, scenarioResults, inputData.getCreationDate());
    }
  }

  private void addCalculatedSnapshotValues(final JobIdentifier jobIdentifier, final Scenario scenario) throws AeriusException, SQLException {
    // Clear any existing values
    scenario.getOptions().getCalculatedSnapshotValues().setDevelopmentPressureClassification(null);
    try (final Connection con = pmf.getConnection()) {
      final int jobId = JobRepository.getJobId(con, jobIdentifier.getJobKey());
      final SimpleCategory developmentPressureClass = decisionFrameworkRepository.getDevelopmentPressureClass(jobId);
      if (developmentPressureClass != null) {
        scenario.getOptions().getCalculatedSnapshotValues().setDevelopmentPressureClassification(developmentPressureClass.getCode());
      }
    }
  }

  private void defaultGmlExport(final File tempDir, final Scenario scenario, final List<ScenarioSituation> situations,
      final boolean forceIncludeCalculationOptions, final Optional<ScenarioResults> results, final Date date) throws AeriusException, SQLException {
    exportGML(tempDir, scenario, situations, results, forceIncludeCalculationOptions, this::getResults, Optional.empty(), date);
  }

  private void sectorsGmlExport(final File tempDir, final Scenario scenario, final List<ScenarioSituation> situations,
      final Optional<ScenarioResults> results, final Date date) throws AeriusException, SQLException {

    for (final int sectorId : determineSectors(situations)) {
      sectorGmlExport(tempDir, scenario, situations, results, sectorId, date);
    }
  }

  private static List<Integer> determineSectors(final List<ScenarioSituation> situations) {
    final List<Integer> sectorList = new ArrayList<>();
    // create unique sector list
    for (final ScenarioSituation situation : situations) {
      for (final EmissionSourceFeature emissionSource : situation.getSources().getFeatures()) {
        final int sectorId = emissionSource.getProperties().getSectorId();
        if (!sectorList.contains(sectorId)) {
          sectorList.add(sectorId);
        }
      }
    }
    return sectorList;
  }

  private void sectorGmlExport(final File tempDir, final Scenario scenario, final List<ScenarioSituation> situations,
      final Optional<ScenarioResults> results, final int sectorId, final Date date) throws AeriusException, SQLException {
    exportGML(tempDir, scenario, situations, results, false, (calculationId, customPoints) -> getSectorResults(calculationId, sectorId, customPoints),
        Optional.of(new SectorFilter(sectorId)), date);
  }

  private void exportGML(final File tempDir, final Scenario scenario, final List<ScenarioSituation> situations,
      final Optional<ScenarioResults> results, final boolean forceIncludeCalculationOptions,
      final BiFunction<Integer, List<CalculationPointFeature>, List<CalculationPointFeature>> calculationResultRetrieval,
      final Optional<SectorFilter> filter, final Date date) throws AeriusException, SQLException {

    for (final ScenarioSituation situation : situations) {
      createSituationGmls(tempDir, scenario, results, calculationResultRetrieval, forceIncludeCalculationOptions, filter, situation, date);
    }
  }

  private void createSituationGmls(final File tempDir, final Scenario scenario, final Optional<ScenarioResults> results,
      final BiFunction<Integer, List<CalculationPointFeature>, List<CalculationPointFeature>> calculationResultRetrieval,
      final boolean forceIncludeCalculationOptions, final Optional<SectorFilter> filter, final ScenarioSituation situation, final Date date)
      throws AeriusException, SQLException {
    final MetaDataInput metaDataInput = constructMetaData(scenario, situation, forceIncludeCalculationOptions, results);
    final Optional<ScenarioSituationResults> scenarioResults = results.map(r -> r.getResultsPerSituation().get(situation.getId()));
    final Optional<Integer> calculationId = scenarioResults.map(ScenarioSituationResults::getCalculationId);
    final List<CalculationPointFeature> receptorPoints = getReceptors(calculationId, scenario, calculationResultRetrieval);
    final IsScenario gmlScenario = GMLScenarioHelper.constructScenario(situation, receptorPoints);
    final String additionalName;

    if (filter.isPresent()) {
      final SectorFilter sourceFilter = filter.get();
      final List<EmissionSourceFeature> sources = gmlScenario.getSources().stream()
          .filter(sourceFilter::include)
          .toList();
      gmlScenario.getSources().clear();
      gmlScenario.getSources().addAll(sources);
      additionalName = sourceFilter.getOptionalFileName();
    } else {
      additionalName = null;
    }

    final Path filename = Path.of(tempDir.getAbsolutePath(), FilenameUtil.situationFilename(scenario.getTheme(), situation, date, additionalName));

    writer.writeToFile(filename, gmlScenario, metaDataInput);
    createIncombinationArchiveGml(tempDir, scenario, situation, calculationId, date);
  }

  private MetaDataInput constructMetaData(final Scenario scenario, final ScenarioSituation situation, final boolean forceIncludeCalculationOptions,
      final Optional<ScenarioResults> results) throws SQLException {
    return GMLScenarioHelper.constructMetaData(scenario, situation,
        includeCalculationOptions(forceIncludeCalculationOptions, situation, results),
        AeriusVersion.getVersionNumber(), pmf.getDatabaseVersion());
  }

  private static BooleanSupplier includeCalculationOptions(final boolean forceIncludeCalculationOptions, final ScenarioSituation situation,
      final Optional<ScenarioResults> results) {
    return () -> forceIncludeCalculationOptions ||
        (results.isPresent() && results.get().getResultsPerSituation().containsKey(situation.getId()));
  }

  private void createIncombinationArchiveGml(final File tempDir, final Scenario scenario, final ScenarioSituation situation,
      final Optional<Integer> calculationId, final Date date) throws SQLException, AeriusException {
    if (scenario.getOptions().getCalculationJobType() == CalculationJobType.IN_COMBINATION_PROCESS_CONTRIBUTION
        && scenario.getOptions().isUseInCombinationArchive()
        // Archive contributions should be stored under the proposed calculation.
        && situation.getType() == SituationType.PROPOSED
        && calculationId.isPresent()) {
      final List<CalculationPointFeature> archiveContributions = getArchiveContributions(calculationId.get());
      final ArchiveMetaData archiveMetaData = retrieveArchiveMetadata(calculationId.get());
      final MetaDataInput metaDataInput = GMLScenarioHelper.constructArchiveMetaData(scenario, situation.getYear(), archiveMetaData,
          AeriusVersion.getVersionNumber(), pmf.getDatabaseVersion());
      final Path filename = Path.of(tempDir.getAbsolutePath(), FilenameUtil.archiveContributionsFilename(scenario.getTheme(), date));
      writer.writeArchiveContributionsToFile(filename, archiveContributions, metaDataInput);
    }
  }

  /**
   * Get the receptors from the scenario: results if calculation id is valid, custom calculation point otherwise (and available).
   */
  private List<CalculationPointFeature> getReceptors(final Optional<Integer> calculationId, final Scenario scenario,
      final BiFunction<Integer, List<CalculationPointFeature>, List<CalculationPointFeature>> calculationResultRetrieval) {
    final List<CalculationPointFeature> receptors = new ArrayList<>();
    if (calculationId.isEmpty() || calculationId.get() == 0) {
      receptors.addAll(scenario.getCustomPointsList());
    } else {
      receptors.addAll(calculationResultRetrieval.apply(calculationId.get(), scenario.getCustomPointsList()));
      addEdgeEffect(calculationId.get(), scenario, receptors);
    }
    return receptors;
  }

  private List<CalculationPointFeature> getResults(final int calculationId, final List<CalculationPointFeature> customPoints) {
    try (final Connection connection = pmf.getConnection()) {
      final List<CalculationPointFeature> allResults = CalculationInfoRepository.getCalculationResults(connection, calculationId);
      final Map<Integer, Map<EmissionResultKey, Double>> results = CalculationInfoRepository.getCustomPointsCalculationResults(connection,
          calculationId);
      allResults.addAll(mergeCustomPointResults(results, customPoints));
      return allResults;
    } catch (final SQLException | AeriusException e) {
      throw new RuntimeException("Exception retrieving results for GML export", e);
    }
  }

  private List<CalculationPointFeature> getSectorResults(final int calculationId, final int sectorId,
      final List<CalculationPointFeature> customPoints) {
    try (final Connection connection = pmf.getConnection()) {
      final List<CalculationPointFeature> allResults = CalculationInfoRepository.getCalculationSectorResults(connection, calculationId, sectorId);
      final Map<Integer, Map<EmissionResultKey, Double>> results = CalculationInfoRepository.getCustomPointsCalculationSectorResults(connection,
          calculationId, sectorId);
      allResults.addAll(mergeCustomPointResults(results, customPoints));
      return allResults;
    } catch (final SQLException | AeriusException e) {
      throw new RuntimeException("Exception retrieving sector results for GML export", e);
    }
  }

  private List<CalculationPointFeature> getArchiveContributions(final int calculationId) {
    try (final Connection connection = pmf.getConnection()) {
      return CalculationInfoRepository.getArchiveContributions(connection, calculationId);
    } catch (final SQLException e) {
      throw new RuntimeException("Exception retrieving results for GML export", e);
    }
  }

  private void addEdgeEffect(final int calculationId, final Scenario scenario, final List<CalculationPointFeature> calculationPointFeatures) {
    if (hasPotentialEdgeEffect(scenario)) {
      try (final Connection connection = pmf.getConnection()) {
        CalculationRepository.addEdgeEffectInformation(connection, calculationId, calculationPointFeatures);
      } catch (final SQLException e) {
        throw new RuntimeException("SQL exception retrieving edge effect information for GML export", e);
      }
    }
  }

  private ArchiveMetaData retrieveArchiveMetadata(final int calculationId) {
    try (final Connection connection = pmf.getConnection()) {
      return CalculationRepository.getArchiveMetadata(connection, calculationId);
    } catch (final SQLException | IOException e) {
      throw new RuntimeException("Exception retrieving archive metadata for GML export", e);
    }
  }

  private static boolean hasPotentialEdgeEffect(final Scenario scenario) {
    return scenario.getOptions() != null && scenario.getOptions().getOwN2000CalculationOptions() != null
        && scenario.getOptions().getOwN2000CalculationOptions().isUseMaxDistance();
  }

  private static List<CalculationPointFeature> mergeCustomPointResults(final Map<Integer, Map<EmissionResultKey, Double>> databaseResultsMap,
      final List<CalculationPointFeature> originalCustomPoints) throws AeriusException {
    final List<CalculationPointFeature> featuresWithResults = new ArrayList<>();
    final CalculationPointDuplicator duplicator = new CalculationPointDuplicator();
    for (final CalculationPointFeature originalFeature : originalCustomPoints) {
      // Merge by creating duplicates: the originals are per scenario, while the results are per situation.
      // So we can't just add the results to the originals and be done with it, as that would mean impacting other situations.
      final CalculationPointFeature duplicateFeature = originalFeature.accept(duplicator);
      final int calculationPointId = originalFeature.getProperties().getId();
      if (databaseResultsMap.containsKey(calculationPointId)) {
        duplicateFeature.getProperties().setResults(databaseResultsMap.get(calculationPointId));
      }
      featuresWithResults.add(duplicateFeature);
    }
    return featuresWithResults;
  }

  @Override
  public String getZipFileName(final CalculationInputData inputData) {
    return FilenameUtil.jobFilename(inputData, FileFormat.ZIP, inputData.getScenario().getReference());
  }

  private static record SectorFilter(int sectorId) {

    public String getOptionalFileName() {
      return String.valueOf(sectorId);
    }

    public boolean include(final EmissionSourceFeature emissionSource) {
      return emissionSource.getProperties().getSectorId() == sectorId;
    }
  }
}
