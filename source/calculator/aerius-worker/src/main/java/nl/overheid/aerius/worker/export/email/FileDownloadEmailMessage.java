/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.export.email;

import java.util.Map;
import java.util.Optional;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.email.message.EmailMessage;
import nl.overheid.aerius.shared.domain.email.EmailMessagesEnum;
import nl.overheid.aerius.shared.domain.email.EmailType;
import nl.overheid.aerius.shared.domain.email.ReplacementToken;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;

/**
 * Abstract base class for email messages related to downloading of files.
 */
abstract class FileDownloadEmailMessage extends EmailMessage<CalculationInputData> {

  protected FileDownloadEmailMessage(final EmailType emailType, final CalculationInputData data) {
    super(emailType, data);
  }

  @Override
  public void populateStringReplacementTokens(final Map<ReplacementToken, String> map) {
    final Scenario scenario = getData().getScenario();

    map.put(ReplacementToken.JOB, Optional.ofNullable(getData().getName()).orElse(""));
    map.put(ReplacementToken.PROJECT_NAME, Optional.ofNullable(scenario.getMetaData().getProjectName()).orElse(""));
    map.put(ReplacementToken.REFERENCE, Optional.ofNullable(scenario.getReference()).orElse(""));
  }

  @Override
  public EmailMessagesEnum getMailContent() {
    return EmailMessagesEnum.DOWNLOAD_CONTENT;
  }

  @Override
  public EmailMessagesEnum getMailSubject() {
    return EmailMessagesEnum.DOWNLOAD_SUBJECT;
  }
}
