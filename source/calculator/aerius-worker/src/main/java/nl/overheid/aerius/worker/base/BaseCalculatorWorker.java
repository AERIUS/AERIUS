/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.base;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.db.calculator.JobRepository;
import nl.overheid.aerius.processmonitor.ProcessMonitorClientBean;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.LocaleUtils;
import nl.overheid.aerius.util.ScenarioFileServerUtil;
import nl.overheid.aerius.worker.AbstractDBWorker;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.JobStateHelper;
import nl.overheid.aerius.worker.RequeueTaskException;
import nl.overheid.aerius.worker.WorkerConnectionHelper;

/**
 * Base class for running a calculation worker.
 */
public abstract class BaseCalculatorWorker extends AbstractDBWorker<CalculationInputData, Boolean> {

  private static final Logger LOGGER = LoggerFactory.getLogger(BaseCalculatorWorker.class);

  private static final int MAX_REQUEUE_ATTEMPTS = 10;

  protected final ScenarioFileServerUtil scenarioFileServiceUtil;
  protected final JobStateHelper jobStateHelper;

  final Map<String, Integer> requeueAttempts = new HashMap<>();

  private final ProcessMonitorClientBean processMonitorClient;

  protected BaseCalculatorWorker(final WorkerConnectionHelper workerConnectionHelper, final ScenarioFileServerUtil scenarioFileServiceUtil) {
    super(workerConnectionHelper.getPMF());
    this.processMonitorClient = new ProcessMonitorClientBean(workerConnectionHelper.getBrokerConnectionFactory());
    this.scenarioFileServiceUtil = scenarioFileServiceUtil;
    this.jobStateHelper = new JobStateHelper(workerConnectionHelper.getPMF());
  }

  @Override
  public final Boolean run(final CalculationInputData inputData, final JobIdentifier jobIdentifier,
      final WorkerIntermediateResultSender workerIntermediateResultSender) throws Exception {
    final String jobKey = jobIdentifier.getJobKey();
    LOGGER.info("Starting calculation job with type '{}' for {}", inputData.getExportType(), jobIdentifier);
    try {
      jobStateHelper.failIfJobCancelledByUser(jobIdentifier);
      final Scenario scenario = scenarioFileServiceUtil.retrieveScenario(jobKey);

      inputData.setScenario(scenario);
      final boolean workDone = process(inputData, jobIdentifier);

      jobStateHelper.setJobStatus(jobIdentifier.getJobKey(), workDone ? JobState.COMPLETED : JobState.POST_PROCESSING);
      requeueAttempts.remove(jobIdentifier.getJobKey());
      return Boolean.TRUE;
    } catch (final RequeueTaskException e) {
      if (shouldRequeue(jobIdentifier)) {
        throw e;
      } else {
        processException(inputData, jobIdentifier, e);
        LOGGER.error("Requeued the same job in this worker too many times, no longer requeuing, {}", jobIdentifier);
        throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
      }
    } catch (final RuntimeException | AeriusException e) {
      // When a runtime or aerius Exception is thrown the job is cancelled, an endtime should be set and scenario is cleaned from fileservice.
      // Other exceptions mean the job will get back on the queue, and can be picked up again, so no endtime is set.
      if (JobStateHelper.isNotCancelledJob(e)) {
        processException(inputData, jobIdentifier, e);
      }
      throw e;
    }
  }

  private boolean shouldRequeue(final JobIdentifier jobIdentifier) {
    final String jobKey = jobIdentifier.getJobKey();
    final int requeuedSoFar = requeueAttempts.getOrDefault(jobKey, 0);
    if (requeuedSoFar < MAX_REQUEUE_ATTEMPTS) {
      requeueAttempts.put(jobKey, requeuedSoFar + 1);
      return true;
    } else {
      requeueAttempts.remove(jobKey);
      return false;
    }
  }

  /**
   * Perform the actual calculation process.
   *
   * @param inputData calculation input data
   * @param jobIdentifier id of the job
   * @return state the calculation finished with.
   * @throws Exception
   */
  protected abstract boolean process(CalculationInputData inputData, JobIdentifier jobIdentifier) throws Exception;

  /**
   * When a calculation crashed and was set back on the queue it could mean there are still calculation results left in the database.
   * This call removes these results if any are present before the calculation starts.
   * @param jobKey job key to remove results for.
   */
  protected boolean cleanUpCalculations(final String jobKey, final boolean force) {
    try (final Connection con = getPMF().getConnection()) {
      return JobRepository.removeJobCalculations(con, jobKey, force);
    } catch (final SQLException e) {
      LOGGER.error("Error occurred while trying clean up a job {}", jobKey, e);
      return true;
    }
  }

  /**
   * When a runtime or AERIUS Exception is thrown the job is considered finished (though in an error state),
   * an endtime should be set and the corresponding scenario is cleaned from fileservice.
   * Other exceptions mean the job will get back on the queue, and can be picked up again, so then this method should not be called.
   */
  private void processException(final CalculationInputData inputData, final JobIdentifier jobIdentifier, final Exception exception) {
    final String jobKey = jobIdentifier.getJobKey();
    jobStateHelper.setErrorStateAndMessageJob(jobKey, getLocale(inputData), exception);
    additionalProcessException(inputData, jobIdentifier, exception);
    // Also remove the scenario from the file service, as it has become useless
    scenarioFileServiceUtil.removeScenario(jobKey);
    processMonitorClient.cancelJob(jobKey);
  }

  protected void additionalProcessException(final CalculationInputData inputData, final JobIdentifier jobIdentifier, final Exception exception) {
    // No default additional exception processing
  }

  private static Locale getLocale(final CalculationInputData inputData) {
    return LocaleUtils.getLocale(inputData.getLocale());
  }
}
