/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.calculator;

import java.util.List;
import java.util.Properties;

import nl.overheid.aerius.worker.ConfigurationBuilder;
import nl.overheid.aerius.worker.ConfigurationValidator;
import nl.overheid.aerius.worker.DatabaseConfigurationBuilder;
import nl.overheid.aerius.worker.FileServerConfigurationBuilder;
import nl.overheid.aerius.worker.base.ArchiveServerConfigurationBuilder;

/**
 * Builder for calculator worker configuration.
 */
class CalculatorConfigurationBuilder extends ConfigurationBuilder<CalculatorConfiguration> {

  private static final String WORKER_PREFIX = "chunker";

  public CalculatorConfigurationBuilder(final Properties properties) {
    super(properties, WORKER_PREFIX);
    addBuilder(new DatabaseConfigurationBuilder<>(properties));
    addBuilder(new FileServerConfigurationBuilder<>(properties));
    addBuilder(new ArchiveServerConfigurationBuilder(properties));
  }

  @Override
  protected boolean isActive() {
    return workerProperties.isActive();
  }

  @Override
  protected CalculatorConfiguration create() {
    return new CalculatorConfiguration(workerProperties.getProcesses());
  }

  @Override
  protected void build(final CalculatorConfiguration configuration) {
    // No additional properties to set
  }

  @Override
  protected List<String> validate(final CalculatorConfiguration configuration) {
    final ConfigurationValidator validator = new ConfigurationValidator();

    return validator.getValidationErrors();
  }
}
