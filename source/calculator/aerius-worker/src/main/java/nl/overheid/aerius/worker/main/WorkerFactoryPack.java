/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.main;

import nl.overheid.aerius.worker.WorkerConfiguration;
import nl.overheid.aerius.worker.WorkerConfigurationFactory;

/**
 * Package that contains the factory with it's configuration to build a specific worker.
 */
class WorkerFactoryPack<C extends WorkerConfiguration> {

  private final WorkerConfigurationFactory<C> factory;
  private final C configuration;

  public WorkerFactoryPack(final WorkerConfigurationFactory<C> factory, final C configuration) {
    this.factory = factory;
    this.configuration = configuration;
  }

  public WorkerConfigurationFactory<C> getFactory() {
    return factory;
  }

  public C getConfiguration() {
    return configuration;
  }
}
