/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.pdf;

import java.util.List;
import java.util.Properties;

import nl.overheid.aerius.export.chrome.ChromeConfigurationBuilder;
import nl.overheid.aerius.worker.ConfigurationBuilder;
import nl.overheid.aerius.worker.ConfigurationValidator;
import nl.overheid.aerius.worker.DatabaseConfigurationBuilder;
import nl.overheid.aerius.worker.FileServerConfigurationBuilder;

/**
 * Builds the configuration object for the PDF worker.
 */
class PdfConfigurationBuilder extends ConfigurationBuilder<PdfConfiguration> {

  private static final String WORKER_PREFIX = "pdf";
  private static final String WEBSERVER_URL = "webserver.url";

  public PdfConfigurationBuilder(final Properties properties) {
    super(properties, WORKER_PREFIX);
    addBuilder(new DatabaseConfigurationBuilder<>(properties));
    addBuilder(new FileServerConfigurationBuilder<>(properties));
    addBuilder(new ChromeConfigurationBuilder<>(properties));
  }

  @Override
  protected boolean isActive() {
    return workerProperties.isActive();
  }

  @Override
  protected PdfConfiguration create() {
    return new PdfConfiguration(workerProperties.getProcesses());
  }

  @Override
  protected void build(final PdfConfiguration configuration) {
    configuration.setWebserverUrl(workerProperties.getProperty(WEBSERVER_URL));
  }

  @Override
  protected List<String> validate(final PdfConfiguration configuration) {
    final ConfigurationValidator validator = new ConfigurationValidator();

    validator.validateRequiredProperty(workerProperties.getFullPropertyName(WEBSERVER_URL), configuration.getWebserverUrl());
    return validator.getValidationErrors();
  }
}
