/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.calculator;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.Properties;
import java.util.function.UnaryOperator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.archive.ArchiveProxy;
import nl.overheid.aerius.calculation.CalculatorBuildDirector;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.http.FileServerProxy;
import nl.overheid.aerius.http.HttpClientProxy;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.util.ScenarioFileServerUtil;
import nl.overheid.aerius.worker.ConfigurationBuilder;
import nl.overheid.aerius.worker.DatabaseWorkerFactory;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.JobStateHelper;
import nl.overheid.aerius.worker.Worker;
import nl.overheid.aerius.worker.WorkerConnectionHelper;
import nl.overheid.aerius.worker.WorkerTimeLogger;
import nl.overheid.aerius.worker.WorkerUtil;
import nl.overheid.aerius.worker.export.CIMLKCSVInputExportHandler;
import nl.overheid.aerius.worker.export.CalculatorExportWorker;
import nl.overheid.aerius.worker.export.DirectoryExportHandler;
import nl.overheid.aerius.worker.export.ExportEmailHandler;
import nl.overheid.aerius.worker.export.ExportHandler;
import nl.overheid.aerius.worker.export.ExportPostProcessor;
import nl.overheid.aerius.worker.export.ExportToPdfWorkerHandler;
import nl.overheid.aerius.worker.export.ExportZipProcessor;
import nl.overheid.aerius.worker.export.GMLExportHandler;
import nl.overheid.aerius.worker.export.GMLInputExportHandler;
import nl.overheid.aerius.worker.export.GMLProvider;
import nl.overheid.aerius.worker.export.ModelExportHandler;
import nl.overheid.aerius.worker.export.RegisterResultsInserter;

/**
 * Factory to create calculator worker.
 */
public class CalculatorWorkerFactory extends DatabaseWorkerFactory<CalculatorConfiguration> {

  private static final Logger LOG = LoggerFactory.getLogger(CalculatorWorkerFactory.class);

  public CalculatorWorkerFactory() {
    super(WorkerType.CONNECT);
  }

  @Override
  public ConfigurationBuilder<CalculatorConfiguration> configurationBuilder(final Properties properties) {
    return new CalculatorConfigurationBuilder(properties);
  }

  @Override
  protected Worker<Serializable, Serializable> createWorkerHandlerWithPMF(final CalculatorConfiguration configuration,
      final WorkerConnectionHelper workerConnectionHelper) throws Exception {
    return new CalculatorWorker(configuration, workerConnectionHelper);
  }

  /**
   * Handler for the calculator worker.
   */
  private static class CalculatorWorker implements Worker<Serializable, Serializable> {
    private final PMF pmf;
    private final CalculatorBuildDirector calculatorBuildDirector;
    private final ScenarioFileServerUtil scenarioFileServiceUtil;
    private final WorkerConnectionHelper workerConnectionHelper;
    private final FileServerProxy fileServerProxy;
    private final JobStateHelper jobStateHelper;
    private final RegisterResultsInserter registerResultsInserter;

    /**
     * @param configuration The database configuration to use for setting up connections with the DB and such
     * @param workerConnectionHelper helper class to get external connections
     */
    public CalculatorWorker(final CalculatorConfiguration configuration, final WorkerConnectionHelper workerConnectionHelper)
        throws SQLException, IOException, AeriusException {
      this.workerConnectionHelper = workerConnectionHelper;
      this.pmf = workerConnectionHelper.getPMF();
      this.fileServerProxy = workerConnectionHelper.createFileServerProxy(configuration);
      this.scenarioFileServiceUtil = new ScenarioFileServerUtil(fileServerProxy);
      final ArchiveProxy archiveProxy = new ArchiveProxy(
          new HttpClientProxy(workerConnectionHelper.getHttpClientManager(), configuration.getArchiveConfiguration().getArchiveTimeout()),
          configuration.getArchiveConfiguration().getArchiveUrl(), configuration.getArchiveConfiguration().getArchiveApiKey());
      this.calculatorBuildDirector = new CalculatorBuildDirector(workerConnectionHelper.getPMF(), workerConnectionHelper.getBrokerConnectionFactory(),
          configuration.getProcesses(), archiveProxy);
      this.jobStateHelper = new JobStateHelper(pmf);
      this.registerResultsInserter = new RegisterResultsInserter(fileServerProxy, pmf);
    }

    @Override
    public Serializable run(final Serializable input, final JobIdentifier jobIdentifier,
        final WorkerIntermediateResultSender workerIntermediateResultSender) throws Exception {
      jobStateHelper.failIfJobCancelledByUser(jobIdentifier);
      if (input instanceof final CalculationInputData cid) {
        final WorkerTimeLogger timeLogger = WorkerTimeLogger.start(WorkerType.CONNECT.name() + "." + cid.getExportType().name());

        try {
          return handleCalculationExport((CalculationInputData) input, jobIdentifier, workerIntermediateResultSender);
        } catch (final SQLException e) {
          LOG.error("SQLException for {}", jobIdentifier, e);
          return new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
        } finally {
          timeLogger.logDuration(jobIdentifier);
        }
      } else {
        throw new UnsupportedOperationException("Worker does not know how to handle the input:" + input.getClass().getName());
      }
    }

    private Serializable handleCalculationExport(final CalculationInputData input, final JobIdentifier jobIdentifier,
        final WorkerIntermediateResultSender workerIntermediateResultSender) throws Exception {
      final Worker<CalculationInputData, ?> worker;

      if (input.getExportType() == ExportType.CALCULATION_UI) {
        worker = new CalculationWorker(calculatorBuildDirector, workerConnectionHelper, scenarioFileServiceUtil);
      } else {
        worker = new CalculatorExportWorker(calculatorBuildDirector, workerConnectionHelper, createExportPostProcessor(input, jobIdentifier),
            scenarioFileServiceUtil, registerResultsInserter);
      }
      return worker.run(input, jobIdentifier, workerIntermediateResultSender);
    }

    /**
     * Creates the processor that should be run when the export is complete. If the export is a pdf the task is send to the pdf worker.
     * In that case the pdf worker will sent the task to the email worker when done.
     * When the export is something else the task can be sent directly to the email worker when done.
     */
    private ExportPostProcessor createExportPostProcessor(final CalculationInputData input, final JobIdentifier jobIdentifier)
        throws IOException, SQLException {
      final GMLProvider gmlProvider = new GMLProvider(pmf);

      if (input.getExportType().isPdfExportType()) {
        return new ExportToPdfWorkerHandler(workerConnectionHelper, gmlProvider);
      } else {
        return new ExportEmailHandler(workerConnectionHelper, new ExportZipProcessor(getExportHandler(input, jobIdentifier), fileServerProxy),
            gmlProvider);
      }
    }

    private ExportHandler getExportHandler(final CalculationInputData input, final JobIdentifier jobIdentifier) throws SQLException {
      final ExportType exportType = input.getExportType();
      final File folderForJob = WorkerUtil.getFolderForJob(jobIdentifier);

      return switch (exportType) {
        case PDF_PROCUREMENT_POLICY, PDF_PAA, REGISTER_PDF_PAA_DEMAND, PDF_SINGLE_SCENARIO ->
            throw new UnsupportedOperationException("Worker should send pdf to PDF worker for export type: " + exportType);
        case GML_WITH_RESULTS, GML_WITH_SECTORS_RESULTS, GML_SOURCES_ONLY ->
            new GMLExportHandler(pmf, folderForJob, UnaryOperator.identity());
        case CIMLK_CSV ->
            new DirectoryExportHandler(folderForJob, "CSV", new GMLInputExportHandler(pmf), new CIMLKCSVInputExportHandler());
        case CSV ->
            new DirectoryExportHandler(folderForJob, "CSV");
        case ADMS, OPS ->
            new ModelExportHandler();
        case REGISTER_JSON_PROJECT_CALCULATION ->
            new DirectoryExportHandler(folderForJob, "JSON");
        case CALCULATION_UI ->
            throw new UnsupportedOperationException("Worker does not know how to handle the export type: " + exportType);
        default ->
            throw new UnsupportedOperationException("Worker does not know how to handle the export type: " + exportType);
      };
    }
  }
}
