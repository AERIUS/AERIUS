/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.importer;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.domain.EmissionSourceListSTRTree;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.geo.SridPoint;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.ReceptorPoint;
import nl.overheid.aerius.shared.domain.v2.point.SubPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

/**
 * Filters results based on the distance to the nearest sources,
 * in any situation.
 */
public class ResultsDistanceFilter {

  private static final Logger LOG = LoggerFactory.getLogger(ResultsDistanceFilter.class);

  private final int srid;
  private final ReceptorUtil receptorUtil;

  public ResultsDistanceFilter(final ReceptorGridSettings rgs) {
    this.receptorUtil = new ReceptorUtil(rgs);
    this.srid = rgs.getEPSG().getSrid();
  }

  /**
   * Filters results based on the distance to the nearest sources, in any situation.
   *
   * @param maximumDistance the maximum distance for filtering
   * @param importParcels the list of import parcels containing the situation results to filter
   * @throws AeriusException
   */
  public void filter(final double maximumDistance, final List<ImportParcel> importParcels) throws AeriusException {
    final EmissionSourceListSTRTree esTree = new EmissionSourceListSTRTree();
    for (final ImportParcel importParcel : importParcels) {
      esTree.add(importParcel.getSituation().getEmissionSourcesList());
    }
    esTree.build();

    for (final ImportParcel importParcel : importParcels) {
      final List<CalculationPointFeature> filteredFeatures = importParcel.getSituationResults().getResults().stream().filter(
          cpf -> {

            final SridPoint point;
            if (cpf.getProperties() instanceof ReceptorPoint) {
              point = getSridPoint((ReceptorPoint) cpf.getProperties());
            } else if (cpf.getProperties() instanceof SubPoint) {
              point = getSridPoint((SubPoint) cpf.getProperties());
            } else {
              return false;
            }

            try {
              return esTree.findShortestDistance(point) <= maximumDistance;
            } catch (final AeriusException e) {
              LOG.error("AeriusException while filtering results on distance", e);
              // RuntimeException because the Stream API does not support checked exceptions
              throw new RuntimeException(e);
            }
          }
      ).collect(Collectors.toList());
      importParcel.getSituationResults().setResults(filteredFeatures);
    }
  }

  private SridPoint getSridPoint(final ReceptorPoint receptorPoint) {
    return toSridPoint(receptorUtil.getPointFromReceptorId(receptorPoint.getReceptorId()));
  }

  private SridPoint getSridPoint(final SubPoint subPoint) {
    return toSridPoint(receptorUtil.getPointFromReceptorId(subPoint.getReceptorId()));
  }

  private SridPoint toSridPoint(final Point point) {
    return new SridPoint(point.getX(), point.getY(), srid);
  }
}
