/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.pdf;

import java.io.File;
import java.io.Serializable;
import java.nio.file.Files;
import java.util.EnumSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.export.PdfAttachZipFileProcessor;
import nl.overheid.aerius.export.PdfGenerator;
import nl.overheid.aerius.export.chrome.ChromeRunner;
import nl.overheid.aerius.export.karate.KaratePdfGenerator;
import nl.overheid.aerius.export.util.PdfContentGenerator;
import nl.overheid.aerius.export.util.PdfTask;
import nl.overheid.aerius.http.FileServerProxy;
import nl.overheid.aerius.shared.domain.AeriusCustomer;
import nl.overheid.aerius.shared.domain.PdfProductType;
import nl.overheid.aerius.shared.domain.ReleaseType;
import nl.overheid.aerius.shared.domain.export.ExportAppendix;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.util.LocaleUtils;
import nl.overheid.aerius.util.ScenarioFileServerUtil;
import nl.overheid.aerius.worker.ConfigurationBuilder;
import nl.overheid.aerius.worker.DatabaseWorkerFactory;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.JobStateHelper;
import nl.overheid.aerius.worker.Worker;
import nl.overheid.aerius.worker.WorkerConnectionHelper;
import nl.overheid.aerius.worker.WorkerTimeLogger;
import nl.overheid.aerius.worker.WorkerUtil;
import nl.overheid.aerius.worker.base.BaseCalculatorWorker;
import nl.overheid.aerius.worker.export.ExportEmailHandler;
import nl.overheid.aerius.worker.export.ExportPostProcessor;
import nl.overheid.aerius.worker.export.ExportZipProcessor;
import nl.overheid.aerius.worker.export.GMLExportHandler;
import nl.overheid.aerius.worker.export.GMLProvider;
import nl.overheid.aerius.worker.export.PdfExportHandler;

/**
 * Factory class to create PDF generating workers.
 * The worker takes the calculated input, and calls the export processor to generate a pdf using chrome headless.
 */
public class PdfCalculatorWorkerFactory extends DatabaseWorkerFactory<PdfConfiguration> {

  private static final String TEMP_GML_DIRECTORY_PREFIX = "pdf-gmls";

  public PdfCalculatorWorkerFactory() {
    super(WorkerType.PDF);
  }

  @Override
  public ConfigurationBuilder<PdfConfiguration> configurationBuilder(final Properties properties) {
    return new PdfConfigurationBuilder(properties);
  }

  @Override
  protected Worker<Serializable, Serializable> createWorkerHandlerWithPMF(final PdfConfiguration configuration,
      final WorkerConnectionHelper workerConnectionHelper) throws Exception {
    final PdfContentGenerator generator;
    switch (configuration.getMode()) {
      case HEADLESS:
        generator = new ChromeRunner(configuration);
        break;
      case KARATE:
      default:
        generator = new KaratePdfGenerator();
    }
    return createWorkerHandlerWithPMF(configuration, workerConnectionHelper, new PdfGenerator(workerConnectionHelper.getPMF(), generator));
  }

  /**
   * Handler creator that injects the {@link PdfGeneratorFactory}. Intended for mocking pdf generation in unit tests.
   */
  public Worker<Serializable, Serializable> createWorkerHandlerWithPMF(final PdfConfiguration configuration,
      final WorkerConnectionHelper workerConnectionHelper, final PdfGenerator pdfGenerator) {
    final ScenarioFileServerUtil scenarioFileServiceUtil = new ScenarioFileServerUtil(workerConnectionHelper.createFileServerProxy(configuration));

    return new PdfWorkerHandler(configuration, workerConnectionHelper, scenarioFileServiceUtil, pdfGenerator);
  }

  private static class PdfWorkerHandler implements Worker<Serializable, Serializable> {
    private final WorkerConnectionHelper workerConnectionHelper;
    private final FileServerProxy fileServerProxy;
    private final ScenarioFileServerUtil scenarioFileServiceUtil;
    private final String webserverUrl;
    private final ReleaseType releaseType;
    private final AeriusCustomer customer;
    private final PMF pmf;
    private final PdfGenerator pdfGenerator;
    private final JobStateHelper jobStateHelper;

    public PdfWorkerHandler(final PdfConfiguration configuration, final WorkerConnectionHelper workerConnectionHelper,
        final ScenarioFileServerUtil scenarioFileServiceUtil, final PdfGenerator pdfGenerator) {
      this.workerConnectionHelper = workerConnectionHelper;
      this.scenarioFileServiceUtil = scenarioFileServiceUtil;
      this.fileServerProxy = workerConnectionHelper.createFileServerProxy(configuration);
      this.webserverUrl = configuration.getWebserverUrl();
      this.pmf = workerConnectionHelper.getPMF();
      this.pdfGenerator = pdfGenerator;
      this.releaseType = ConstantRepository.getEnum(pmf, ConstantsEnum.RELEASE, ReleaseType.class);
      this.customer = ConstantRepository.getEnum(pmf, ConstantsEnum.CUSTOMER, AeriusCustomer.class);
      this.jobStateHelper = new JobStateHelper(pmf);
    }

    @Override
    public Serializable run(final Serializable input, final JobIdentifier jobIdentifier,
        final WorkerIntermediateResultSender workerIntermediateResultSender) throws Exception {
      jobStateHelper.failIfJobCancelledByUser(jobIdentifier);
      final WorkerTimeLogger timeLogger = WorkerTimeLogger.start(WorkerType.PDF.name());

      try {
        if (input instanceof final CalculationInputData inputData) {
          final ExportType exportType = inputData.getExportType();

          if (exportType.isPdfExportType()) {
            return generatePdf(jobIdentifier, inputData, exportType, workerIntermediateResultSender);
          }
          throw new UnsupportedOperationException("PDF Worker does not know how to handle the export type: " + exportType);
        }
        throw new UnsupportedOperationException("Worker does not know how to handle the input: " + input.getClass().getName());
      } finally {
        timeLogger.logDuration(jobIdentifier);
      }
    }

    private Serializable generatePdf(final JobIdentifier jobIdentifier, final CalculationInputData inputData, final ExportType exportType,
        final WorkerIntermediateResultSender workerIntermediateResultSender) throws Exception {
      try {
        final List<PdfTask> pdfTasks = getPdfTasks(jobIdentifier, inputData, exportType);
        final File gmlTempDirectory = Files.createTempDirectory(TEMP_GML_DIRECTORY_PREFIX).toFile();
        final GMLExportHandler exportHandler = new GMLExportHandler(pmf, gmlTempDirectory, new PdfSituationFilter(exportType));
        exportHandler.setNoReceptorRepresentation();
        final PdfAttachZipFileProcessor pdfAttachmentGenerator = fileProcessor -> new ExportZipProcessor(exportHandler, fileProcessor)
            .zipAndProcess(inputData, jobIdentifier, false);
        final PdfExportHandler handler = new PdfExportHandler(pdfGenerator, pdfAttachmentGenerator, pdfTasks,
            WorkerUtil.getFolderForJob(jobIdentifier));
        final ExportPostProcessor exportPostProcessor = new ExportEmailHandler(workerConnectionHelper,
            new ExportZipProcessor(handler, fileServerProxy), new GMLProvider(pmf));
        final PdfWorker worker = new PdfWorker(workerConnectionHelper, exportPostProcessor, scenarioFileServiceUtil);

        return worker.run(inputData, jobIdentifier, workerIntermediateResultSender);
      } catch (final Exception e) {
        jobStateHelper.setErrorStateAndMessageJob(jobIdentifier.getJobKey(), LocaleUtils.getLocale(inputData.getLocale()), e);
        throw e;
      }
    }

    private List<PdfTask> getPdfTasks(final JobIdentifier jobIdentifier, final CalculationInputData inputData, final ExportType exportType) {
      final Set<PdfProductType> pdfProductTypes = getPdfProductTypes(customer, exportType, inputData.getAppendices());

      return pdfProductTypes.stream().map(pdt -> new PdfTask(customer, webserverUrl, jobIdentifier, pdt, releaseType)).toList();
    }

    private static Set<PdfProductType> getPdfProductTypes(final AeriusCustomer customer, final ExportType exportType,
        final Set<ExportAppendix> appendices) {
      final Set<PdfProductType> pdfProducts = EnumSet.noneOf(PdfProductType.class);

      if (customer == AeriusCustomer.JNCC) {
        pdfProducts.add(PdfProductType.NCA_CALCULATOR);
      } else if (customer == AeriusCustomer.RIVM) {
        if (exportType == ExportType.REGISTER_PDF_PAA_DEMAND) {
          pdfProducts.add(PdfProductType.OWN2000_PERMIT);
        } else if (exportType == ExportType.PDF_PROCUREMENT_POLICY) {
          pdfProducts.add(PdfProductType.OWN2000_LBV_POLICY);
        } else if (exportType == ExportType.PDF_SINGLE_SCENARIO) {
          pdfProducts.add(PdfProductType.OWN2000_SINGLE_SCENARIO);
        } else {
          pdfProducts.add(PdfProductType.OWN2000_CALCULATOR);
          if (appendices != null && appendices.contains(ExportAppendix.EDGE_EFFECT_REPORT)) {
            pdfProducts.add(PdfProductType.OWN2000_EDGE_EFFECT_APPENDIX);
          }
        }
        if (appendices != null && appendices.contains(ExportAppendix.EXTRA_ASSESSMENT_REPORT)) {
          pdfProducts.add(PdfProductType.OWN2000_EXTRA_ASSESSMENT_APPENDIX);
        }
      }
      return pdfProducts;
    }
  }

  private static class PdfWorker extends BaseCalculatorWorker {
    private final ExportPostProcessor exportPostProcessor;

    public PdfWorker(final WorkerConnectionHelper workerConnectionHelper, final ExportPostProcessor exportPostProcessor,
        final ScenarioFileServerUtil scenarioFileServiceUtil) {
      super(workerConnectionHelper, scenarioFileServiceUtil);
      this.exportPostProcessor = exportPostProcessor;
    }

    @Override
    protected boolean process(final CalculationInputData inputData, final JobIdentifier jobIdentifier) throws Exception {
      final String jobKey = jobIdentifier.getJobKey();
      exportPostProcessor.process(inputData, jobIdentifier, null);

      if (cleanUpCalculations(jobKey, false)) {
        scenarioFileServiceUtil.removeScenario(jobKey);
      }
      return !inputData.isEmailUser();
    }

    @Override
    protected void additionalProcessException(final CalculationInputData inputData, final JobIdentifier jobIdentifier, final Exception exception) {
      exportPostProcessor.emailException(inputData, jobIdentifier, exception);
    }
  }
}
