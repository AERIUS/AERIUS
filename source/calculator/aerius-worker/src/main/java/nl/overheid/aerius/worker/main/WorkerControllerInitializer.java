/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.taskmanager.client.configuration.BrokerConfiguration;
import nl.overheid.aerius.worker.ConfigurationBuilder;
import nl.overheid.aerius.worker.PropertiesUtil;
import nl.overheid.aerius.worker.WorkerConfiguration;
import nl.overheid.aerius.worker.WorkerConfigurationFactory;

/**
 * Initializes the worker controller. Builds the configurations, stops when validation fails.
 * If all is good it will start up all configured workers.
 */
class WorkerControllerInitializer {
  private static final Logger LOG = LoggerFactory.getLogger(WorkerControllerInitializer.class);

  private final Properties properties;
  private final List<String> validationErrors = new ArrayList<>();

  private BrokerConfiguration brokerConfig;

  public WorkerControllerInitializer(final String configurationFile) throws IOException {
    LOG.debug("Reading configuration file: {}", configurationFile);
    properties = PropertiesUtil.getFromPropertyFile(configurationFile);
  }

  /**
   * Initializes all configured workers and returns a factory pach containing a factory and configuration.
   *
   * @param workerFactories factories to build configuration if activated
   * @return list of factory and configuration packets.
   */
  public List<WorkerFactoryPack<?>> init(final WorkerConfigurationFactory<?>[] workerFactories) {
    brokerConfig = new BrokerConfiguration(properties);
    validationErrors.addAll(brokerConfig.getValidationErrors());
    return initWorkers(workerFactories);
  }

  private List<WorkerFactoryPack<?>> initWorkers(final WorkerConfigurationFactory<?>[] workerFactories) {
    final List<WorkerFactoryPack<?>> packs = new ArrayList<>();

    for (final WorkerConfigurationFactory<?> workerFactory : workerFactories) {
      init(workerFactory, properties).ifPresent(packs::add);
    }
    return packs;
  }

  private <C extends WorkerConfiguration> Optional<WorkerFactoryPack<C>> init(final WorkerConfigurationFactory<C> workerFactory,
      final Properties properties) {
    final ConfigurationBuilder<C> builder = workerFactory.configurationBuilder(properties);

    return builder.buildAndValidate().map(configuration -> {
      validationErrors.addAll(builder.getValidationErrors());
      return Optional.of(new WorkerFactoryPack<C>(workerFactory, configuration));
    }).orElse(Optional.empty());
  }

  public BrokerConfiguration getBrokerConfig() {
    return brokerConfig;
  }

  /**
   * Prints validation errors to error log if any validations present or if validation succeeded. Returns true is there where validation errors.
   * @return true if there are validation errors.
   */
  public boolean printValidationStatus() {
    if (validationErrors.isEmpty()) {
      LOG.info("All configurations read and validated.");
      return false;
    } else {
      LOG.error("One or more validations failed: {}", String.join(", ", validationErrors));
      return true;
    }
  }
}
