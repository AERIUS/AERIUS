/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.client;

import java.io.IOException;
import java.io.Serializable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;

import nl.aerius.taskmanager.client.WorkerResultSender;
import nl.aerius.taskmanager.client.util.QueueHelper;
import nl.overheid.aerius.taskmanager.client.WorkerHandler;
import nl.overheid.aerius.worker.RequeueTaskException;
import nl.overheid.aerius.worker.WorkerThreadUtil;

/**
 * Class for workers to consume tasks from the queue and send results back to the clients. The actual work is done by the passed
 * {@link WorkerHandler}.
 */
class WorkerConsumer extends DefaultConsumer {

  private static final Logger LOG = LoggerFactory.getLogger(WorkerConsumer.class);

  private final ExecutorService executor;
  // Use semaphore to handle only 1 task at a time.
  private final Semaphore sequentialProcessing = new Semaphore(0);

  private final WorkerHandler workerHandler;
  private final String workerTypeName;
  private final boolean returnResults;
  private final int consumerIndex;

  private Future<?> runningTask;
  private boolean alive = true;
  private Throwable exception;

  WorkerConsumer(final ExecutorService executor, final Channel channel, final WorkerHandler workerHandler, final String workerTypeName,
      final int consumerIndex) {
    this(executor, channel, workerHandler, workerTypeName, consumerIndex, true);
  }

  WorkerConsumer(final ExecutorService executor, final Channel channel, final WorkerHandler workerHandler, final String workerTypeName,
      final int consumerIndex, final boolean returnResults) {
    super(channel);
    if (workerHandler == null) {
      throw new IllegalArgumentException("Workerhandler not allowed to be null.");
    }
    this.executor = executor;
    this.workerHandler = workerHandler;
    this.workerTypeName = workerTypeName;
    this.consumerIndex = consumerIndex;
    this.returnResults = returnResults;
  }

  @Override
  public void handleDelivery(final String consumerTag, final Envelope envelope, final BasicProperties properties, final byte[] body)
      throws IOException {
    if (!alive) {
      shutdown(null);
    }
    WorkerThreadUtil.setThreadName("Channel-" + consumerIndex, workerTypeName, properties.getCorrelationId());
    LOG.debug("Worker Consumer handle delivery {}: {}", workerTypeName, properties.getCorrelationId());
    final Channel channel = getChannel();

    runningTask = executor.submit(() -> {
      try {
        handleDelivery(properties, body, channel, true, envelope.getDeliveryTag());
      } finally {
        sequentialProcessing.release();
      }
    });
    try {
      sequentialProcessing.acquire();
      if (alive) {
        LOG.debug("Worker Consumer delivery handled {}: {}", workerTypeName, properties.getCorrelationId());
      } else {
        LOG.debug("Worker Consumer died during delivery {}: {}", workerTypeName, properties.getCorrelationId());
      }
    } catch (final InterruptedException e) {
      Thread.currentThread().interrupt();
    }
  }

  /**
   * Shutdown this worker consumer and cancels any running task. Triggered when the connection with RabbitMQ is lost.
   *
   * @param e reason for shutdown
   */
  public void shutdown(final ShutdownSignalException e) {
    if (runningTask != null) {
      LOG.warn("Worker Consumer {} got ShutdownSignalException: {}", workerTypeName, (e == null ? "" : e.getMessage()));
      alive = false;
      runningTask.cancel(true);
      sequentialProcessing.release();
    }
  }

  public void handleDelivery(final BasicProperties properties, final byte[] body, final Channel channel, final boolean ack, final long deliveryTag) {
    try {
      final Serializable workLoad = QueueHelper.bytesToObject(body);
      final WorkerResultSender resultHandler = new WorkerResultSender(channel, properties);
      // let the workerHandler do its job.
      final Serializable result = workerHandler.handleWorkLoad(workLoad, properties.getCorrelationId(), resultHandler);

      if (!alive) {
        return;
      }
      if (result instanceof Exception) {
        LOG.info("Worker task {} failed with an exception: {}", properties.getCorrelationId(), ((Exception) result).getMessage());
      }
      if (returnResults) {
        //send the result back
        resultHandler.sendFinalResult(result);
      }
      if (ack) {
        // acknowledge the message, deleting it from the worker queue.
        channel.basicAck(deliveryTag, false);
      }
    } catch (final RequeueTaskException e) {
      // Requeue message, but don't shut down (unless requeuing failed)
      try {
        channel.basicNack(deliveryTag, false, true);
      } catch (final IOException e2) {
        shutdownDuringDelivery(e2);
      }
    } catch (final InterruptedException e) {
      LOG.debug("This worker is shutdown");
      Thread.currentThread().interrupt();
    } catch (final Throwable e) {
      shutdownDuringDelivery(e);
    } finally {
      if (!alive) {
        try {
          WorkerThreadUtil.setThreadName("Worker", workerTypeName, "UNUSED");
          channel.basicNack(deliveryTag, false, true);
          channel.close();
        } catch (final IOException | TimeoutException e) {
          LOG.debug("IOException in channel while already marked as not running: {}", e.getMessage());
        }
      }
    }
  }

  private void shutdownDuringDelivery(final Throwable e) {
    if (alive) {
      LOG.error("Exception worthy of shutdown during delivery: Shutting down this worker handler.", e);
      exception = e;
      alive = false;
    } else {
      LOG.info("Shutting down during delivery, but was already dead! Exception: {}", e.getMessage());
    }
  }

  /**
   * @return Returns true if this process is still alive and running.
   */
  public boolean isAlive() {
    return alive;
  }

  public Throwable getException() {
    return exception;
  }

}
