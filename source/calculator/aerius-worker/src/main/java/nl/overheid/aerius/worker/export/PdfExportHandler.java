/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.export;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.export.PdfAttachZipFileProcessor;
import nl.overheid.aerius.export.PdfGenerator;
import nl.overheid.aerius.export.util.PdfTask;
import nl.overheid.aerius.export.util.PdfUrlBuilder;
import nl.overheid.aerius.shared.domain.FileFormat;
import nl.overheid.aerius.shared.domain.PdfProductType;
import nl.overheid.aerius.shared.domain.ProductProfile;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.reference.ReferenceUtil;
import nl.overheid.aerius.util.FilenameUtil;
import nl.overheid.aerius.util.LocaleUtils;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.RequeueTaskException;

/**
 * PDF export Handler.
 * Used to generate a PDF for all pdf ExportTypes (indicated by {@link nl.overheid.aerius.shared.domain.export.ExportType#isPdfExportType()}
 */
public class PdfExportHandler implements ExportHandler {

  private static final Logger LOG = LoggerFactory.getLogger(PdfExportHandler.class);

  private final PdfGenerator pdfGenerator;
  private final PdfAttachZipFileProcessor pdfAttachFileProcessor;
  private final File folderForJob;

  private final List<PdfTask> pdfTasks;
  private final ProductProfile productProfile;
  private final String reference;

  public PdfExportHandler(final PdfGenerator pdfGenerator, final PdfAttachZipFileProcessor pdfAttachFileProcessor, final List<PdfTask> pdfTasks,
      final File folderForJob) {
    this.pdfGenerator = pdfGenerator;
    this.pdfAttachFileProcessor = pdfAttachFileProcessor;
    this.pdfTasks = pdfTasks;
    this.folderForJob = folderForJob;
    productProfile = pdfTasks.stream().anyMatch(t -> t.pdfProductType() == PdfProductType.OWN2000_LBV_POLICY) ? ProductProfile.LBV_POLICY
        : ProductProfile.CALCULATOR;
    reference = ReferenceUtil.generateReference(productProfile);
  }

  // ToDo: this still assumes that multiple PDF's can be generated for each input. But I don't think that's relevant anymore?
  @Override
  public File createFileContent(final CalculationInputData inputData, final JobIdentifier jobIdentifier) throws AeriusException, IOException, RequeueTaskException {
    LOG.debug("Generating PDF for project, reference: {}", reference);

    for (final ScenarioSituation situation : inputData.getScenario().getSituations()) {
      if (shouldGeneratePdfForSituation(situation, inputData.getExportType())) {

        for (final PdfTask pdfTask : pdfTasks) {
          final PdfProductType pdfProductType = pdfTask.pdfProductType();
          final String pdfFilename = getPDFFilename(inputData, pdfProductType, situation, reference);
          final File pdfFile = new File(folderForJob, pdfFilename);
          final String situationId = situation.getId();
          final Locale locale = getLocale(inputData);
          final PdfUrlBuilder pdfUrlBuilder = new PdfUrlBuilder(pdfTask.webserverUrl())
              .setJobKey(pdfTask.jobIdentifier().getJobKey())
              .setReference(reference)
              .setProposedSituationId(situationId)
              .setPdfProductType(pdfProductType)
              .setLocale(locale);

          writeSinglePDF(pdfFile, reference, pdfTask, pdfUrlBuilder.build(), locale);
        }
      }
    }
    return folderForJob;
  }

  @Override
  public String getZipFileName(final CalculationInputData inputData) {
    return FilenameUtil.jobFilename(inputData, FileFormat.ZIP, reference);
  }

  private static Locale getLocale(final CalculationInputData inputData) {
    return inputData.getScenario().getTheme() == Theme.NCA ? Locale.ENGLISH : LocaleUtils.DUTCH;
  }

  private static String getPDFFilename(final CalculationInputData inputData, final PdfProductType pdfProductType,
      final ScenarioSituation proposedSituation, final String reference) {
    return FilenameUtil.getFilename(pdfProductType.getFilePrefix(), FileFormat.PDF.getExtension(), inputData.getCreationDate(), reference,
        proposedSituation.getName());
  }

  private static boolean shouldGeneratePdfForSituation(final ScenarioSituation scenarioSituation, final ExportType exportType) {
    if (exportType == ExportType.PDF_PAA || exportType == ExportType.REGISTER_PDF_PAA_DEMAND) {
      return scenarioSituation.getType() == SituationType.PROPOSED;
    }
    if (exportType == ExportType.PDF_PROCUREMENT_POLICY) {
      return scenarioSituation.getType() == SituationType.REFERENCE;
    }
    return exportType == ExportType.PDF_SINGLE_SCENARIO;
  }

  private void writeSinglePDF(final File pdfFile, final String reference, final PdfTask pdfTask, final String url, final Locale locale)
      throws AeriusException {
    try {
      pdfGenerator.generate(pdfFile, reference, pdfTask, url, pdfAttachFileProcessor, locale);
    } catch (final Exception e) {
      LOG.error("Error writing PDF: {}", e.getMessage(), e);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }
}
