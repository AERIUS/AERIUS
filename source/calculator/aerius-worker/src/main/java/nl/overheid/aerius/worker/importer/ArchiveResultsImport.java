/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.importer;

import java.util.List;

import nl.overheid.aerius.shared.domain.v2.archive.ArchiveMetaData;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;

/**
 *
 */
class ArchiveResultsImport {

  private final ArchiveMetaData metadata;
  private final List<CalculationPointFeature> results;

  public ArchiveResultsImport(final ArchiveMetaData metadata, final List<CalculationPointFeature> results) {
    this.metadata = metadata;
    this.results = results;
  }

  public ArchiveMetaData getMetadata() {
    return metadata;
  }

  public List<CalculationPointFeature> getResults() {
    return results;
  }

}
