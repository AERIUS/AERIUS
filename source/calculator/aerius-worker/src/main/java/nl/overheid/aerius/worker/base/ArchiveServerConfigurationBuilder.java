/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.base;

import java.util.Properties;

import nl.overheid.aerius.worker.ConfigurationBuilder;

/**
 * Configuration builder for the Archive server URL
 */
public class ArchiveServerConfigurationBuilder<T extends HasArchiveConfiguration> extends ConfigurationBuilder<T> {

  private static final String ARCHIVE_SERVER_PREFIX = "archiveserver";
  private static final String URL_KEY = "url";
  private static final String API_KEY_KEY = "apikey";
  private static final String TIMEOUT_KEY = "timeout";

  private static final int TIMEOUT_DEFAULT = 60;

  public ArchiveServerConfigurationBuilder(final Properties properties) {
    super(properties, ARCHIVE_SERVER_PREFIX);
  }

  @Override
  protected T create() {
    throw new IllegalStateException("ArchiveServerConfigurationBuilder should not be used as separated configuration.");
  }

  @Override
  protected void build(final T configuration) {
    configuration.getArchiveConfiguration().setArchiveUrl(workerProperties.getProperty(URL_KEY));
    configuration.getArchiveConfiguration().setArchiveApiKey(workerProperties.getProperty(API_KEY_KEY));
    configuration.getArchiveConfiguration().setArchiveTimeout(workerProperties.getPropertyOrDefault(TIMEOUT_KEY, TIMEOUT_DEFAULT));
  }
}
