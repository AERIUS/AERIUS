/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.jobcleanup;

import java.util.List;
import java.util.Properties;

import nl.overheid.aerius.worker.ConfigurationBuilder;
import nl.overheid.aerius.worker.DatabaseConfigurationBuilder;

/**
 * Builder class to build {@link JobCleanupConfiguration}.
 */
class JobCleanupConfigurationBuilder extends ConfigurationBuilder<JobCleanupConfiguration> {

  private static final String JOBCLEANUP_PREFIX = "jobcleanup";
  private static final String JOBCLEANUP_WORKER_ENABLED = "enabled";
  private static final String JOBCLEANUP_WORKER_AFTER_DAYS = "after_days";

  public JobCleanupConfigurationBuilder(final Properties properties) {
    super(properties, JOBCLEANUP_PREFIX);
    addBuilder(new DatabaseConfigurationBuilder<JobCleanupConfiguration>(properties));
  }

  @Override
  protected boolean isActive() {
    return workerProperties.getPropertyBooleanSafe(JOBCLEANUP_WORKER_ENABLED);
  }

  @Override
  protected JobCleanupConfiguration create() {
    return new JobCleanupConfiguration();
  }

  @Override
  protected void build(final JobCleanupConfiguration configuration) {
    configuration.setJobCleanupAfterDays(getJobCleanupAfterDays());
  }

  @Override
  protected List<String> validate(final JobCleanupConfiguration configuration) {
    if (configuration.getJobCleanupAfterDays() <= 0) {
      return List.of("The amount of days to clean jobs after isn't configured properly for the job cleanup worker, "
          + "even though the worker is enabled. Worker disabled for this run.");
    }
    return List.of();
  }

  private int getJobCleanupAfterDays() {
    return workerProperties.getPropertyIntSafe(JOBCLEANUP_WORKER_AFTER_DAYS);
  }
}
