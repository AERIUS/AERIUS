/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.processmonitor;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Delivery;

import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.processmonitor.ProcessMonitorService;
import nl.overheid.aerius.worker.ProcessMonitorConstants;
import nl.overheid.aerius.worker.WorkerConnectionHelper;

/**
 * The ProcessMonitorController performs 3 tasks:
 *
 * <dl>
 * <dt>Listen to the processmonitor RabbitMQ exchange</dt>
 * <dd>Via the exchange the messages are sent to cancel running jobs.
 *     The Controller receives these messages and passes the jobKey related to the cancelled job to the {@link ProcessMonitorService}.</dd>
 * <dt>With a scheduled task trigger the {@link ProcessMonitorService} to kill any (still) running cancelled processes.</dt>
 * <dd>The moment a cancel command is received via the queue, might not sync with the moment a process is running.
 *     Therefore it is needed to regularly trigger the {@link ProcessMonitorService} to kill possible tasks running for cancelled tasks.</dd>
 * <dt>Clean up cancelled job cache.</dt>
 * <dd>Cancelled jobs are kept in a cache because there still might be tasks on the queue that arrive after the job was cancelled.
 *      This means the cache keeps on growing. By clean up the cache after a time frame with a wide margin the cache is clean up,
 *      while still being sure no tasks will be missed that should have actually be killed.</dd>
 * </dl>
 */
class ProcessMonitorController implements Runnable {
  private static final Logger LOG = LoggerFactory.getLogger(ProcessMonitorController.class);

  private static final long WAIT_TIME_IN_MINUTES = 1;
  private static final long CLEAN_OLD_ENTRIES_AFTER_DAYS = 30;

  private final ScheduledExecutorService scheduler;
  private final BrokerConnectionFactory connectionFactory;
  private final ProcessMonitorService processMonitorService;

  public ProcessMonitorController(final WorkerConnectionHelper workerConnectionHelper) {
    this(workerConnectionHelper, Executors.newScheduledThreadPool(1));
  }

  ProcessMonitorController(final WorkerConnectionHelper workerConnectionHelper, final ScheduledExecutorService scheduler) {
    this.connectionFactory = workerConnectionHelper.getBrokerConnectionFactory();
    this.processMonitorService = workerConnectionHelper.getProcessMonitorService();
    this.scheduler = scheduler;
  }

  @Override
  public void run() {
    final Channel channel = startConsume();
    LOG.info(
        "Scheduling process monitor clean up worker to run every {} minute(s). Will delete jobs older than {} days.",
        WAIT_TIME_IN_MINUTES, CLEAN_OLD_ENTRIES_AFTER_DAYS);
    final ScheduledFuture<?> future = scheduler.scheduleWithFixedDelay(this::cleanupTask, WAIT_TIME_IN_MINUTES, WAIT_TIME_IN_MINUTES,
        TimeUnit.MINUTES);

    // Wait for the future to finish (which should only be in case of exceptions).
    try {
      future.get();
    } catch (final InterruptedException e) {
      Thread.currentThread().interrupt();
    } catch (final ExecutionException e) {
      LOG.error("Exception in process monitoring clean up scheduled task.", e);
    }
    scheduler.shutdown();
    closeChannel(channel);
  }

  private Channel startConsume() {
    try {
      return startConsume(ProcessMonitorConstants.PROCESS_MONITOR_EXCHANGE);
    } catch (final IOException e) {
      LOG.error("Exception in process monitoring start.", e);
      return null;
    }
  }

  private Channel startConsume(final String exchangeName) throws IOException {
    final Channel channel = connectionFactory.getConnection().createChannel();
    channel.exchangeDeclare(exchangeName, "fanout");
    final String queueName = channel.queueDeclare().getQueue();
    channel.queueBind(queueName, exchangeName, "");
    channel.basicConsume(queueName, true, this::callback, consumerTag -> { });
    return channel;
  }

  private void callback(final String consumerTag, final Delivery delivery) {
    processMonitorService.killCancelledJob(new String(delivery.getBody(), StandardCharsets.UTF_8));
  }

  private void cleanupTask() {
    processMonitorService.killAllCancelledJobs();
    processMonitorService.cleanCache(CLEAN_OLD_ENTRIES_AFTER_DAYS);
  }

  private static void closeChannel(final Channel channel) {
    if (channel != null) {
      try {
        channel.close();
      } catch (IOException | TimeoutException  e) {
        // Eat close error.
      }
    }
  }
}
