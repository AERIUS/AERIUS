/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.export;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.CalculationRepositoryBean;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.results.SaveCalculationResultsRepositoryBean;
import nl.overheid.aerius.http.FileServerProxy;
import nl.overheid.aerius.http.HttpException;
import nl.overheid.aerius.shared.FileServerFile;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationJobType;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculationsImpl;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.register.RegisterCalculation;
import nl.overheid.aerius.shared.domain.register.RegisterCalculationResult;
import nl.overheid.aerius.shared.domain.register.RegisterCalculationResultSet;
import nl.overheid.aerius.shared.domain.register.RegisterPermitAreaResult;
import nl.overheid.aerius.shared.domain.register.RegisterResults;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.ScenarioObjectMapperUtil;

/**
 * Class to help with inserting register results into the database.
 *
 * These results are supplied by Register to the api, which are then serialized into a file.
 * This class will retrieve that file, deserialize it, and insert the appropriate data.
 * Calculations and summaries are created, to ensure a decision PDF can be created once this process is complete.
 */
public class RegisterResultsInserter {

  private static final Logger LOG = LoggerFactory.getLogger(RegisterResultsInserter.class);

  private final FileServerProxy fileServerProxy;
  private final ObjectMapper objectMapper = ScenarioObjectMapperUtil.getScenarioObjectMapper();
  private final JobRepositoryBean jobRepository;
  private final SaveCalculationResultsRepositoryBean saveCalculationResultsRepository;
  private final CalculationRepositoryBean calculationRepository;

  public RegisterResultsInserter(final FileServerProxy fileServerProxy, final PMF pmf) {
    this(fileServerProxy, new JobRepositoryBean(pmf), new SaveCalculationResultsRepositoryBean(pmf), new CalculationRepositoryBean(pmf));
  }

  RegisterResultsInserter(final FileServerProxy fileServerProxy, final JobRepositoryBean jobRepository,
      final SaveCalculationResultsRepositoryBean saveCalculationResultsRepository, final CalculationRepositoryBean calculationRepository) {
    this.fileServerProxy = fileServerProxy;
    this.jobRepository = jobRepository;
    this.saveCalculationResultsRepository = saveCalculationResultsRepository;
    this.calculationRepository = calculationRepository;
  }

  /**
   * Insert register results based on/for a job key.
   *
   * @param jobKey The job key to obtain results for and save the results for.
   * @param scenario The scenario associated with the results.
   * @throws AeriusException In case of an exception.
   */
  public void insertRegisterResults(final String jobKey, final Scenario scenario) throws AeriusException {
    final int jobId = jobRepository.getJobId(jobKey);
    final RegisterResults registerResults = retrieveResults(jobKey);
    final ScenarioCalculations scenarioCalculations = new ScenarioCalculationsImpl(scenario);
    saveCalculationResultsRepository.insertCalculations(scenarioCalculations);
    jobRepository.attachCalculations(jobKey, scenarioCalculations.getCalculations());
    saveCalculationResults(scenarioCalculations, registerResults);
    saveCalculationResultsRepository.updateCalculationState(scenarioCalculations, CalculationState.COMPLETED);
    final SituationCalculations situationCalculations = jobRepository.getSituationCalculations(jobKey);
    determineSummary(jobId, scenarioCalculations, situationCalculations, CalculationJobType.PROCESS_CONTRIBUTION,
        registerResults.permitAreaResults());
  }

  private RegisterResults retrieveResults(final String jobKey) throws AeriusException {
    try (InputStream fsis = fileServerProxy.getContentStream(FileServerFile.RESULTS.format(jobKey))) {
      return objectMapper.readValue(fsis, RegisterResults.class);
    } catch (IOException | HttpException | URISyntaxException e) {
      LOG.error("Error retrieving Register results from file server", e);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }

  private void determineSummary(final int jobId, final ScenarioCalculations scenarioCalculations, final SituationCalculations situationCalculations,
      final CalculationJobType jobType, final List<RegisterPermitAreaResult> permitAreas) throws AeriusException {
    // First determine normal summary, then add area permit demand checks.
    saveCalculationResultsRepository.insertResultsSummaries(jobId, situationCalculations, jobType);

    for (final RegisterPermitAreaResult permitArea : permitAreas) {
      persistPermitAreaResult(jobId, scenarioCalculations, permitArea);
    }
  }

  private void persistPermitAreaResult(final int jobId, final ScenarioCalculations scenarioCalculations, final RegisterPermitAreaResult permitArea)
      throws AeriusException {
    for (final Calculation storedCalcs : scenarioCalculations.getCalculations()) {
      final int calcId = storedCalcs.getCalculationId();
      for (final Integer resultSet : calculationRepository.getCalculationResultIdSets(calcId)) {
        saveCalculationResultsRepository.insertPermitDemand(resultSet, jobId, permitArea.areaId(), permitArea.demandCheck());
      }
    }
  }

  private void saveCalculationResults(final ScenarioCalculations scenarioCalculations, final RegisterResults registerResults)
      throws AeriusException {
    for (final Calculation storedCalculation : scenarioCalculations.getCalculations()) {
      final RegisterCalculation results = getCalculationResultOnType(storedCalculation.getSituationType().name(),
          registerResults.calculations());

      saveCalculationResults(storedCalculation, results);
    }
  }

  private void saveCalculationResults(final Calculation calculation, final RegisterCalculation results) throws AeriusException {
    final Map<Integer, AeriusResultPoint> resultPointMap = new HashMap<>();
    for (final RegisterCalculationResultSet resultSet : results.calculationResultSets()) {
      final EmissionResultKey emissionResultKey = EmissionResultKey.valueOf(resultSet.substance());
      for (final RegisterCalculationResult calculationResult : resultSet.calculationResults()) {
        final AeriusResultPoint resultPoint = resultPointMap.computeIfAbsent(calculationResult.receptorId(),
            receptorId -> new AeriusResultPoint(new AeriusPoint(receptorId)));

        resultPoint.setEmissionResult(emissionResultKey, calculationResult.result());
      }
    }
    saveCalculationResultsRepository.insertCalculationResults(calculation.getCalculationId(), new ArrayList<>(resultPointMap.values()));
  }

  private RegisterCalculation getCalculationResultOnType(final String situationType,
      final List<RegisterCalculation> calculations) throws AeriusException {
    for (final RegisterCalculation calculation : calculations) {
      if (calculation.situationType().contains(situationType)) {
        return calculation;
      }
    }
    LOG.error("Register supplied results situation name does not match source situation type {}", situationType);
    throw new AeriusException(AeriusExceptionReason.IMPORTED_CALCULATION_RESULTS_DOES_NOT_MATCH_SOURCE_SITUATION, situationType);
  }

}
