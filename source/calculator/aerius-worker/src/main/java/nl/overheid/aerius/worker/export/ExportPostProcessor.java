/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.export;

import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.email.message.EmailMessageTaskSender;
import nl.overheid.aerius.shared.domain.ProductProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.WorkerConnectionHelper;

/**
 * Abstract class for doing the export after a calculation.
 */
public abstract class ExportPostProcessor {

  private static final Logger LOGGER = LoggerFactory.getLogger(ExportPostProcessor.class);

  private final EmailMessageTaskSender messageSender;
  private final GMLProvider gmlProvider;

  protected ExportPostProcessor(final WorkerConnectionHelper workerConnectionHelper, final GMLProvider gmlProvider) {
    this.gmlProvider = gmlProvider;
    messageSender = new EmailMessageTaskSender(workerConnectionHelper.createTaskManagerClient(), workerConnectionHelper.getPMF());
  }

  /**
   * Performs the post processing action.
   *
   * @param inputData
   * @param jobIdentifier
   * @param resultUrl optional url to the results
   */
  public abstract void process(final CalculationInputData inputData, final JobIdentifier jobIdentifier, final String resultUrl) throws Exception;

  /**
   * Email an exception to the user.
   *
   * @param inputData
   * @param jobIdentifier
   * @param exception
   * @param backupAttachments
   */
  public void emailException(final CalculationInputData inputData, final JobIdentifier jobIdentifier, final Exception exception) {
    if (inputData.isEmailUser()) {
      LOGGER.info("Sending error mail for {}", jobIdentifier);
      try {
        final Collection<StringDataSource> gmls = inputData.getScenario() == null ? List.of()
            : gmlProvider.getGMLs(inputData, ProductProfile.CALCULATOR).values();

        messageSender.sendMailToUserOnError(inputData, exception, jobIdentifier.getJobKey(), gmls);
        LOGGER.debug("Error mail sent to user for {}", jobIdentifier);
      } catch (final AeriusException e) {
        LOGGER.error("Problem sending error email to the user: {}, {}", e.getMessage(), jobIdentifier, e);
      }
    }
  }

  protected EmailMessageTaskSender getMessageSender() {
    return messageSender;
  }
}
