/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.client;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabbitmq.client.Channel;

import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.aerius.taskmanager.client.WorkerQueueType;
import nl.overheid.aerius.taskmanager.client.WorkerHandler;

/**
 * Receiving end of the Worker. Programs reading from the worker queue should use this class to listen to the worker
 * queue.
 */
public class WorkerClient {

  private static final Logger LOG = LoggerFactory.getLogger(WorkerClient.class);

  private final ExecutorService executor;
  private final BrokerConnectionFactory connectionFactory;
  private final WorkerHandler handler;
  private final String workerTypeName;
  private final String queueName;
  private final int consumerIndex;

  private WorkerConsumer workerConsumer;
  private boolean alive = true;

  public WorkerClient(final ExecutorService executor, final BrokerConnectionFactory connectionFactory, final WorkerHandler handler,
      final WorkerQueueType workerType, final int consumerIndex) {
    this(executor, connectionFactory, handler, workerType.propertyName(), workerType.getWorkerQueueName(), consumerIndex);
  }

  public WorkerClient(final ExecutorService executor, final BrokerConnectionFactory connectionFactory, final WorkerHandler handler,
      final String workerTypeName, final String queueName, final int consumerIndex) {
    this.executor = executor;
    this.connectionFactory = connectionFactory;
    this.handler = handler;
    this.workerTypeName = workerTypeName;
    this.queueName = queueName;
    this.consumerIndex = consumerIndex;
  }

  public void run() throws IOException {
    run(queueName, true, true);
    LOG.debug("Worker queue {} started", queueName);
  }

  /**
   * Starts a consuming process that waits for tasks to arrive on the queue.
   *
   * @param queueName name of the queue
   * @param durable if the queue created must be durable
   * @param sendResults sends results back via the reply queue if true
   * @return returns the channel used to listen on
   * @throws IOException
   */
  public Channel run(final String queueName, final boolean durable, final boolean sendResults) throws IOException {
    //create the channel to get messages on and start consuming.
    final Channel channel = connectionFactory.getConnection().createChannel();
    channel.basicQos(1);
    workerConsumer = new WorkerConsumer(executor, channel, handler, workerTypeName, consumerIndex, sendResults);
    channel.addShutdownListener(e -> {
      workerConsumer.shutdown(e);
      shutdown();
    });
    channel.basicConsume(queueName, false, queueName, workerConsumer);
    return channel;
  }

  /**
   * @return Returns true if the WorkerClient hasn't been shutdown and is alive.
   */
  public boolean isAlive() {
    // if workerConsumer is not yet set it is seen as this worker client is running (and running is also true).
    return alive && (workerConsumer == null || workerConsumer.isAlive());
  }

  /**
   * @return Returns the exception in case the worker consumer crashed with an exception otherwise it returns null.
   */
  public Throwable getException() {
    return workerConsumer == null ? null : workerConsumer.getException();
  }

  /**
   * Closes down the queue connections.
   */
  public void shutdown() {
    if (workerConsumer != null) {
      alive = false;
      workerConsumer.shutdown(null);
      if (workerConsumer.getChannel().isOpen()) {
        try {
          workerConsumer.getChannel().basicCancel(queueName);
          workerConsumer.getChannel().close();
        } catch (final IOException |TimeoutException e) {
          LOG.trace("Exception during shutdown worker client: {}", e.getMessage());
        }
      }
    }
  }
}
