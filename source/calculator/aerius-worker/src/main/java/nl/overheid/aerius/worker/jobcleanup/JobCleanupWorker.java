/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.jobcleanup;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.JobRepository;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.WorkerPMFFactory;

/**
 * Job cleanup Worker.
 */
class JobCleanupWorker implements Runnable {

  private static final Logger LOG = LoggerFactory.getLogger(JobCleanupWorker.class);

  // This time will be used as the wait time to start running after the worker is started up and also between runs.
  // Between runs means after a run has finished it will be rescheduled again so it cannot run multiple times if a run takes to long.
  private static final int WAIT_TIME_IN_MINUTES = 60;

  private final PMF calculatorPMF;
  private final int cleanupAfterAmountOfDays;
  private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

  /**
   * @param configuration The configuration containing DB config and start time for this worker.
   * @throws AeriusException
   */
  public JobCleanupWorker(final JobCleanupConfiguration configuration) throws AeriusException {
    this.calculatorPMF = WorkerPMFFactory.createPMF(configuration);

    cleanupAfterAmountOfDays = configuration.getJobCleanupAfterDays();
  }

  @Override
  public void run() {
    LOG.info(
        "Scheduling job cleanup worker to run every {} minute(s). Will delete jobs older than {} days.",
        WAIT_TIME_IN_MINUTES,
        cleanupAfterAmountOfDays);
    final ScheduledFuture<?> future = scheduler.scheduleWithFixedDelay(this::jobCleanupScheduledTask, WAIT_TIME_IN_MINUTES, WAIT_TIME_IN_MINUTES,
        TimeUnit.MINUTES);

    // Wait for the future to finish (which should only be in case of exceptions).
    try {
      future.get();
    } catch (final InterruptedException e) {
      Thread.currentThread().interrupt();
    } catch (final ExecutionException e) {
      LOG.error("Exception in worker clean up scheduled task.", e);
    }
    LOG.error("JobCleanupWorker stopped unexpectedly");
    scheduler.shutdown();
  }

  private void jobCleanupScheduledTask() {
    LOG.info("Starting to clean up jobs.");
    try (final Connection con = calculatorPMF.getConnection()) {
      final int amountRemoved = JobRepository.removeJobsWithMinAge(con, cleanupAfterAmountOfDays);
      LOG.info("Done cleaning up jobs. Amount removed: Connect Jobs : {}", amountRemoved);
    } catch (final SQLException e) {
      LOG.error("Error while cleaning up jobs.", e);
    }
  }
}
