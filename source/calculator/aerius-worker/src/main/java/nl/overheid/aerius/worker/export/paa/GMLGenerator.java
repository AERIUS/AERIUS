/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.export.paa;

import java.sql.SQLException;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import nl.overheid.aerius.AeriusVersion;
import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.gml.GMLScenarioHelper;
import nl.overheid.aerius.gml.base.MetaDataInput;
import nl.overheid.aerius.importer.PermitGMLWriter;
import nl.overheid.aerius.shared.domain.ProductProfile;
import nl.overheid.aerius.shared.domain.scenario.IsScenario;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.FilenameUtil;

/**
 * Generates the GML to be sent to the user in case of an error.
 */
public class GMLGenerator {
  // ToDo: this should probably use information from the CalculationJobType, or just assume the right situations are present in the calculation job.
  // Currently this is kind of relevant, because the assumption still exists that multiple PDF's could be generated for a calculation.
  private static final Set<SituationType> OWN2000_SITUATION_TYPES = EnumSet.of(
      SituationType.REFERENCE,
      SituationType.OFF_SITE_REDUCTION,
      SituationType.PROPOSED
      );

  private final PMF pmf;
  private final PermitGMLWriter permitGMLWriter;

  public GMLGenerator(final PMF pmf) throws SQLException {
    this.pmf = pmf;
    permitGMLWriter = new PermitGMLWriter(pmf);
    permitGMLWriter.setFormattedOutput(Boolean.FALSE);
  }

  /**
   * Build GML dataSources for all relevant situations in the scenario
   *
   * @param scenario a scenario
   * @param date creation date of the scenario
   * @param productProfile product profile, relevant for reference generation
   * @return GMLs of all OwN2000 relevant situations in the input scenario by their situation id.
   */
  public Map<String, StringDataSource> createGMLs(final Scenario scenario, final Date date, final ProductProfile productProfile)
      throws SQLException, AeriusException {
    final Map<String, StringDataSource> gmls = new HashMap<>();
    permitGMLWriter.ensureAllSituationsHaveNewReferences(scenario, productProfile);

    for (final ScenarioSituation situation : scenario.getSituations()) {
      if (isRelevantForOwN2000Export(situation)) {
        final IsScenario gmlScenario = GMLScenarioHelper.constructScenario(situation, scenario.getCustomPointsList());
        final MetaDataInput gmlMetaData = GMLScenarioHelper
            .constructMetaData(scenario, situation, () -> true, AeriusVersion.getVersionNumber(), pmf.getDatabaseVersion());
        final String filename = FilenameUtil.situationFilename(scenario.getTheme(), situation, date, null);

        gmls.put(situation.getId(), permitGMLWriter.writeToString(gmlScenario, gmlMetaData, filename));
      }
    }

    return gmls;
  }

  private static boolean isRelevantForOwN2000Export(final ScenarioSituation situation) {
    return OWN2000_SITUATION_TYPES.contains(situation.getType());
  }
}
