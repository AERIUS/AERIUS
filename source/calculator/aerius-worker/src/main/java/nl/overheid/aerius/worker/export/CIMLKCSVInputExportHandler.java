/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.export;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

import org.apache.commons.io.FileUtils;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.shared.domain.FileFormat;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.srm.io.CIMLKCalculationPointWriter;
import nl.overheid.aerius.srm.io.CIMLKCorrectionWriter;
import nl.overheid.aerius.srm.io.CIMLKDispersionLineWriter;
import nl.overheid.aerius.srm.io.CIMLKMeasureWriter;
import nl.overheid.aerius.srm.io.CIMLKSRM1RoadWriter;
import nl.overheid.aerius.srm.io.CIMLKSRM2RoadWriter;
import nl.overheid.aerius.util.FilenameUtil;
import nl.overheid.aerius.worker.export.DirectoryExportHandler.InputExportHandler;

/**
 *
 */
public class CIMLKCSVInputExportHandler implements InputExportHandler {

  private static final int SCALE = 10;

  private static final String FILENAME_SOURCES_SRM1 = "%s_cimlk_srm1_roads";
  private static final String FILENAME_SOURCES_SRM2 = "%s_cimlk_srm2_roads";
  private static final String FILENAME_POINTS = "%s_cimlk_calculation_points";
  private static final String FILENAME_DISPERSION_LINES = "%s_cimlk_dispersion_lines";
  private static final String FILENAME_CORRECTIONS = "%s_cimlk_corrections";
  private static final String FILENAME_MEASURES = "%s_cimlk_measures";

  private final CIMLKSRM1RoadWriter srm1Writer = new CIMLKSRM1RoadWriter(SCALE);
  private final CIMLKSRM2RoadWriter srm2Writer = new CIMLKSRM2RoadWriter(SCALE);
  private final CIMLKCalculationPointWriter pointWriter = new CIMLKCalculationPointWriter();
  private final CIMLKDispersionLineWriter dispersionLineWriter = new CIMLKDispersionLineWriter();
  private final CIMLKCorrectionWriter correctionWriter = new CIMLKCorrectionWriter(SCALE);
  private final CIMLKMeasureWriter measureWriter = new CIMLKMeasureWriter(SCALE);

  @Override
  public void exportInput(final CalculationInputData inputData, final File targetDirectory) {
    final Scenario scenario = inputData.getScenario();

    for (final ScenarioSituation situation : scenario.getSituations()) {
      exportSrm1Sources(situation, targetDirectory, inputData);
      exportSrm2Sources(situation, targetDirectory, inputData);
      exportPoints(situation, scenario, targetDirectory, inputData);
      exportDispersionLines(situation, targetDirectory, inputData);
      exportCorrections(situation, targetDirectory, inputData);
      exportMeasures(situation, targetDirectory, inputData);
    }

  }

  private void exportSrm1Sources(final ScenarioSituation situation, final File targetDirectory, final CalculationInputData inputData) {
    final File targetFile = targetFile(targetDirectory, FILENAME_SOURCES_SRM1, situation, inputData);
    try {
      srm1Writer.write(targetFile, situation.getEmissionSourcesList());
    } catch (final IOException | RuntimeException e) {
      writeErrorInFile(targetDirectory, e, "Error writing CIMLK SRM1 sources to file");
      FileUtils.deleteQuietly(targetFile);
    }
  }

  private void exportSrm2Sources(final ScenarioSituation situation, final File targetDirectory, final CalculationInputData inputData) {
    final File targetFile = targetFile(targetDirectory, FILENAME_SOURCES_SRM2, situation, inputData);
    try {
      srm2Writer.write(targetFile, situation.getEmissionSourcesList());
    } catch (final IOException | RuntimeException e) {
      writeErrorInFile(targetDirectory, e, "Error writing CIMLK SRM2 sources to file");
      FileUtils.deleteQuietly(targetFile);
    }
  }

  private void exportPoints(final ScenarioSituation situation, final Scenario scenario, final File targetDirectory,
      final CalculationInputData inputData) {
    final File targetFile = targetFile(targetDirectory, FILENAME_POINTS, situation, inputData);
    try {
      pointWriter.write(targetFile, scenario.getCustomPointsList());
    } catch (final IOException | RuntimeException e) {
      writeErrorInFile(targetDirectory, e, "Error writing CIMLK points to file");
      FileUtils.deleteQuietly(targetFile);
    }
  }

  private void exportDispersionLines(final ScenarioSituation situation, final File targetDirectory, final CalculationInputData inputData) {
    final File targetFile = targetFile(targetDirectory, FILENAME_DISPERSION_LINES, situation, inputData);
    try {
      if (!situation.getCimlkDispersionLinesList().isEmpty()) {
        dispersionLineWriter.write(targetFile, situation.getCimlkDispersionLinesList());
      }
    } catch (final IOException | RuntimeException e) {
      writeErrorInFile(targetDirectory, e, "Error writing CIMLK dispersion lines to file");
      FileUtils.deleteQuietly(targetFile);
    }
  }

  private void exportCorrections(final ScenarioSituation situation, final File targetDirectory, final CalculationInputData inputData) {
    final File targetFile = targetFile(targetDirectory, FILENAME_CORRECTIONS, situation, inputData);
    try {
      if (!situation.getCimlkCorrections().isEmpty()) {
        correctionWriter.write(targetFile, situation.getCimlkCorrections());
      }
    } catch (final IOException | RuntimeException e) {
      writeErrorInFile(targetDirectory, e, "Error writing CIMLK corrections to file");
      FileUtils.deleteQuietly(targetFile);
    }
  }

  private void exportMeasures(final ScenarioSituation situation, final File targetDirectory, final CalculationInputData inputData) {
    final File targetFile = targetFile(targetDirectory, FILENAME_MEASURES, situation, inputData);
    try {
      if (!situation.getCimlkMeasuresList().isEmpty()) {
        measureWriter.write(targetFile, situation.getCimlkMeasuresList());
      }
    } catch (final IOException | RuntimeException e) {
      writeErrorInFile(targetDirectory, e, "Error writing CIMLK measures to file");
      FileUtils.deleteQuietly(targetFile);
    }
  }

  private static File targetFile(final File targetDirectory, final String fileTypeFormat, final ScenarioSituation situation,
      final CalculationInputData inputData) {
    final String scenarioName = inputData.getScenario().getMetaData().getProjectName();
    final Date exportDate = inputData.getCreationDate();
    final String prefix = String.format(fileTypeFormat, situation.getId());
    final String filename = FilenameUtil.getFilename(prefix, FileFormat.CSV.getExtension(), exportDate, scenarioName);
    final Path openFile = Paths.get(targetDirectory.getAbsolutePath(), filename);
    return openFile.toFile();
  }

}
