################################################################################
# Example of the worker.properties file.
# Use this file (uncommented and altered) with the worker to get the right
################################################################################

# RabbitMQ configuration. Commented out configuration has a default value.
broker.host = [host]
# broker.port = 5672
broker.username = [username]
broker.password = [password]
# broker.virtualhost = /
# broker.management.port = 15672
# broker.management.refresh = 60

################################################################################

# Properties for file server
# These properties need to be set for the Chunker worker, and ADMS and OPS workers.

# Internal url to the file server
fileserver.internal.url = http://localhost:8083
# Url for external access to the file server. This is the url to connect (without /api/*)
fileserver.external.url = http://localhost:8080
# timeout in seconds
fileserver.timeout = 60

################################################################################

# OPS configuration
# number of OPS workers running on this server (or set to auto)
ops.processes = 4
# Alternative configuration to start number of OPS workers based on number of cores: Results is #cores - 1
# ops.processes = auto
# root directory of OPS installation. This directory must contain a folder with the OPS version bounded with the version
# in AERIUS worker. The version number folder must contain the OPS installation.
ops.root = [path/ops]
# directory to store generated configuration files for ops
ops.runfiles.directory = [path/runfiles]
# if set to true the files in the temporary directory will be deleted, unless ops fails.
# In production this should not set to true.
ops.keepgeneratedfiles = false
# Set a different meteo file to be used by ops. Only used if the use case requires a specific meteo file
#ops.settings.meteofile = m095104c.002
# Set a different landuse prefix to be used by ops. Only used in uk environment
#ops.settings.landuse = lcm7
# Url where model data can be found.
ops.model.data.url = https://nexus.aerius.nl/repository/resources
# comma seperated list of versions to preload on startup. If none are present, or the chunker is configured for a different version
# then the model data is loaded at the first calculation performed by this worker.
ops.model.preload.versions = latest

################################################################################

# AERIUS SRM configuration
# number of SRM processes. It is recommended to set this to 1 because a single SRM calculation will already take all available cores.
srm.processes = 3
# Set to true to force to use the CPU. if false a GPU will be used for SRM2 calculation when installed.
#srm.force_cpu = true

# Comma separated list to enable themes that should be activated.
srm.themes = OWN2000,RBL

# For srm calculations the configuration must be specified per profile.
# If the srm.root configuration is left out that specific profile is not enabled

# Directory of the SRM data files needed by AERIUS
# The directory structure must follow a specific pattern
# [srm-root]/aerius-presm-[aerius-presrm-version]
# [srm-root]/presm-[presrm-version]/lu
# For SRM1 calculations:
# [srm-root]/windvelden-[aerius-windvelden-version]
# For Depostion calculation
# [srm-root]/depositionvelocity

# Directory to store data
srm.root = [path]

srm.model.data.url = https://nexus.aerius.nl/repository/resources

# Comma-seperated lists of model data versions to load on startup for srm.
# srm.preload.versions = SRM-2023

# Set to true to force to use the CPU. if false a GPU will be used for SRM2 calculation when installed.
#srm.force_cpu = true

################################################################################

# ADMS Worker configuration properties.

# adms.processes = 1
# adms.root =
# adms.runfiles.directory =
# adms.keepgeneratedfiles = false
# adms.model.preload.versions = 5.0.0.1
# adms.model.data.url = https://nexus.aerius.nl/repository/resources
# adms.license.base64 =
# Interval in seconds at which intermediate progress results are send back to the chunker (Optional).
# adms.progress.interval =

################################################################################

# Database worker configuration

# Database connection username and password
database.username = [db username]
database.password = [db password]
# Database connection url to the AERIUS Calculator PostgreSql database
database.url = jdbc:postgresql://localhost/calculator

################################################################################

# Number of processes for AERIUS chunker workers. If 0 no chunker tasks will be processed.
chunker.processes = 1

# Configuration for Archive server used in Chunker to get results during calculation from the Archive server.
# archiveserver.url = [archive server]
# archiveserver.timeout = [optional timeout to archive server in seconds]

################################################################################

# Number of processes for AERIUS pdf generation workers. If 0 no pdf tasks will be processed.
# In general this should be set to 1 process to sequentially generate pdfs.
pdf.processes = 1

# URL needed for pdf export
pdf.webserver.url = http://localhost:8080/

# Set Chrome mode:
# HEADLESS: run local Chrome executable.
# KARATE: use Chrome running as server
chrome.mode = [HEADLESS|KARATE]
# If configured as Chrome executable set directory to Chrome executable.
chrome.root= [directory to chrome executable]
# Set new headless mode (Available since Chrome 112). Default is true.
#chrome.newheadless=false
# Set a timeout in milliseconds for chrome (will use virtual timeout option, default 15 seconds.
#chrome.timeout=

################################################################################

# Number of processes for importer worker.
importer.processes = 1

################################################################################

# Number of processes for AERIUS messages related database tasks. If 0 no Messages tasks will be processed.
email.processes = 1

################################################################################

# If set to true this worker will clean up jobs in the connect database.
# jobcleanup.enabled = true
# If connect.jobcleanup.enabled is enabled this specifies after how many days jobs should be cleaned up.
# jobcleanup.after_days = 14

################################################################################

# Base URL to connect to Archive (should be up to and including the version part of the URL).
archiveserver.url = http://localhost:8281/api/v1
archiveserver.apikey = [apikey]
