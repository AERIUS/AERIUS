/*
 * Copyright the State of the Netherlands
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.io.IOException;
import java.util.function.Consumer;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;

import nl.aerius.taskmanager.client.util.QueueHelper;
import nl.aerius.taskmanager.test.MockConnection;
import nl.aerius.taskmanager.test.MockedChannelFactory;
import nl.overheid.aerius.calculation.EngineInputData.CommandType;
import nl.overheid.aerius.shared.domain.result.ExportResult;

/**
 * Mock Connection for a command Export task.
 */
public class ExportMockConnection extends MockConnection {

  private final ExportResult mockExportResult;
  private final Consumer<EngineInputData<?, ?>> inputAsserter;

  /**
   * Constructor.
   *
   * @param mockExportResult The result that should be returned as the export result.
   * @param inputAsserter consumer that is passed the input and can be used to assert the input.
   */
  public ExportMockConnection(final ExportResult mockExportResult, final Consumer<EngineInputData<?, ?>> inputAsserter) {
    this.mockExportResult = mockExportResult;
    this.inputAsserter = inputAsserter;
  }

  @Override
  public Channel createChannel() throws IOException {
    return MockedChannelFactory.create(this::mockResults);
  }


  /**
   * Mocks a Channel to return results that with each run decrease in value. This can be used to mock deposition results in unit tests. Given the
   * points are with each run at a large distance to the sources.
   */
  private byte[] mockResults(final BasicProperties properties, final byte[] body) {
    synchronized (this) {
      try {
        final EngineInputData<?, ?> input = (EngineInputData<?, ?>) QueueHelper.bytesToObject(body);
        inputAsserter.accept(input);
        if (input.getCommandType() == CommandType.EXPORT) {
          return QueueHelper.objectToBytes(mockExportResult);
        } else {
          return null;
        }
      } catch (final IOException | ClassNotFoundException e) {
        return null;
      }
    }
  }
}
