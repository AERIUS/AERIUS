/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.ShippingNode;
import nl.overheid.aerius.shared.domain.sector.category.FarmAdditionalLodgingSystemCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategories;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingFodderMeasureCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingFodderMeasureCategory.AmmoniaProportion;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingSystemDefinition;
import nl.overheid.aerius.shared.domain.sector.category.FarmReductiveLodgingSystemCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmlandCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategories;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.shared.domain.sector.category.MaritimeShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.OnRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategories;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategory;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategory.RoadEmissionCategoryKey;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;

/**
 *
 */
class TestSectorCategories {

  private static final String AUTO_BUS = "AUTO_BUS";
  private static final String HEAVY_FREIGHT = "HEAVY_FREIGHT";
  private static final String LIGHT_TRAFFIC = "LIGHT_TRAFFIC";
  private static final String NORMAL_FREIGHT = "NORMAL_FREIGHT";

  private static final String FREEWAY = "FREEWAY";
  private static final String NON_URBAN_ROAD_GENERAL = "NON_URBAN_ROAD_GENERAL";
  private static final String NON_URBAN_ROAD_NATIONAL = "NON_URBAN_ROAD_NATIONAL";
  private static final String URBAN_ROAD_NORMAL = "URBAN_ROAD_NORMAL";
  private static final String URBAN_ROAD_FREE_FLOW = "URBAN_ROAD_FREE_FLOW";
  private static final String URBAN_ROAD_STAGNATING = "URBAN_ROAD_STAGNATING";

  private static final List<Integer> SECTORS = Arrays.asList(
      1400,
      2100,
      3100,
      3111,
      3112,
      3113,
      3210,
      3220,
      3230,
      4110,
      4140,
      4150,
      7510,
      7530,
      7610,
      7620,
      9000);

  private static final String FARM_ANIMAL_CODE = "D3";

  private static final List<FarmConstructHelper> FARM_LODGING_CATEGORIES = Arrays.asList(
      new FarmConstructHelper("A1.4", 9.2),
      new FarmConstructHelper("B1.100", 0.7),
      new FarmConstructHelper("C1.100", 1.9),
      new FarmConstructHelper("D3.1", 4.5, "BWL2001.21"),
      new FarmConstructHelper("D1.3.3", 2.5),
      new FarmConstructHelper("D3.2.7.2.1", 1.5),
      new FarmConstructHelper("F4.4", 0.2),
      new FarmConstructHelper("A4.2", 1.1, "BWL2011.12"),
      new FarmConstructHelper("A3.100", 4.4),
      new FarmConstructHelper("A2.100", 4.1),
      new FarmConstructHelper("A1.1", 5.7),
      new FarmConstructHelper("D1.1.2", 0.24),
      new FarmConstructHelper("E2.12.1", 0.068),
      new FarmConstructHelper("A1.2", 10.2));

  private static final List<FarmConstructHelper> FARM_ADDITIONAL_SYSTEM_CATEGORIES = Arrays.asList(
      new FarmConstructHelper("E6.1.a", 0.01),
      new FarmConstructHelper("E6.5.b", 0.015));

  private static final List<FarmConstructHelper> FARM_REDUCTIVE_SYSTEM_CATEGORIES = Arrays.asList(
      new FarmConstructHelper("A4.3", 0.7),
      new FarmConstructHelper("G2.1.2", 0.7),
      new FarmConstructHelper("E2.10", 0.9));

  private static final List<FarmFodderConstructHelper> FARM_FODDER_MEASURE_CATEGORIES = Arrays.asList(
      new FarmFodderConstructHelper("PAS2015.01-01", 0.16, 0.16, 0.16, 0.3, 0.7, animalCode -> true),
      new FarmFodderConstructHelper("PAS2015.05-01", 0.2, 0.2, 0.2, 0.3, 0.7, animalCode -> true),
      new FarmFodderConstructHelper("PAS2015.04-01", 0.1, 0.1, 0.1, 0.3, 0.7, animalCode -> animalCode.startsWith("D")),
      new FarmFodderConstructHelper("PAS2015.03-01", 0.35, 0.16, 0.4, 0.1, 0.9, animalCode -> true));

  private static final List<String> FARMLAND_CATEGORIES = Arrays.asList(
      "PASTURE",
      "MANURE");

  private static final List<String> INLAND_SHIPPING_TYPES = Arrays.asList(
      "BI",
      "BII-1",
      "BII-6L",
      "C1B",
      "C3B",
      "M1",
      "M2",
      "M8");

  private static final List<String> INLAND_WATERWAYS = Arrays.asList(
      "CEMT_VIb",
      "CEMT_Vb",
      "CEMT_Va",
      "CEMT_II",
      "IJssel");

  private static final List<String> MARITIME_SHIPPING_TYPES = Arrays.asList(
      "GC3000",
      "GC5000",
      "KV10000",
      "OO100",
      "OO1600",
      "OO10000");

  private static final List<OffRoadConstructHelper> OFF_ROAD_MOBILE_SOURCE_CATEGORIES = Arrays.asList(
      new OffRoadConstructHelper("SI75560DSN", new EmissionHelper(0.026066116, 0.000008371), new EmissionHelper(13.9, 0.003439), null),
      new OffRoadConstructHelper("SII75560DSN", new EmissionHelper(0.017333332, 0.000008371), new EmissionHelper(13.9, 0.003439), null),
      new OffRoadConstructHelper("B4T", null, new EmissionHelper(0.000003162449, 0.000714), null),
      new OffRoadConstructHelper("SIIIA75560DSN", new EmissionHelper(0.017436975, 0.000008371), new EmissionHelper(14.2, 0.003308), null),
      new OffRoadConstructHelper("SIV75560DSJ", new EmissionHelper(0.003086777, 0.000008351), new EmissionHelper(10, 0.003149),
          new EmissionHelper(10, 0.003149)),
      new OffRoadConstructHelper("SI56DSN", new EmissionHelper(0.024580769, 0.000008371), new EmissionHelper(13.9, 0.003439), null),
      new OffRoadConstructHelper("SIIIA56DSN", new EmissionHelper(0.025440613, 0.000008332), new EmissionHelper(14.2, 0.003293), null));

  // road categories returned dynamic as it seems to cause mock to cause issues when running with maven command line.
  private static final List<RoadConstructHelper> ROAD_CATEGORIES = Arrays.asList(
      new RoadConstructHelper("",
          new EmissionHelper(0.1062, 0.0486, 0.0188, 0.0169),
          80, FREEWAY, LIGHT_TRAFFIC, false, 0,
          new EmissionHelper(0.1453, 0.0502, 0.0376, 0.029)),
      new RoadConstructHelper("",
          new EmissionHelper(0.0895, 0.045, 0.0203, 0.0169),
          80, FREEWAY, LIGHT_TRAFFIC, true, 0,
          new EmissionHelper(0.1453, 0.0502, 0.0376, 0.029)),
      new RoadConstructHelper("",
          new EmissionHelper(0.1021, 0.0466, 0.0261, 0.017),
          100, FREEWAY, LIGHT_TRAFFIC, false, 0,
          new EmissionHelper(0.1453, 0.0502, 0.0376, 0.029)),
      new RoadConstructHelper("",
          new EmissionHelper(0.0971, 0.046, 0.0235, 0.017),
          100, FREEWAY, LIGHT_TRAFFIC, true, 0,
          new EmissionHelper(0.1453, 0.0502, 0.0376, 0.029)),
      new RoadConstructHelper("",
          new EmissionHelper(0.1168, 0.0474, 0.0294, 0.017),
          120, FREEWAY, LIGHT_TRAFFIC, false, 0,
          new EmissionHelper(0.1453, 0.0502, 0.0376, 0.029)),
      new RoadConstructHelper("",
          new EmissionHelper(0.124, 0.0476, 0.0315, 0.017),
          130, FREEWAY, LIGHT_TRAFFIC, false, 0,
          new EmissionHelper(0.1453, 0.0502, 0.0376, 0.029)),
      new RoadConstructHelper("",
          new EmissionHelper(1.168, 0.0525, 0.1151, 0.0868),
          80, FREEWAY, NORMAL_FREIGHT, false, 0,
          new EmissionHelper(4.129, 0.0525, 0.1201, 0.146)),
      new RoadConstructHelper("",
          new EmissionHelper(1.1584, 0.0525, 0.1151, 0.0868),
          80, FREEWAY, NORMAL_FREIGHT, true, 0,
          new EmissionHelper(4.129, 0.0525, 0.1201, 0.146)),
      new RoadConstructHelper("",
          new EmissionHelper(1.1584, 0.0525, 0.1151, 0.0868),
          100, FREEWAY, NORMAL_FREIGHT, false, 0,
          new EmissionHelper(4.129, 0.0525, 0.1201, 0.146)),
      new RoadConstructHelper("",
          new EmissionHelper(1.4714, 0.0786, 0.1177, 0.0794),
          80, FREEWAY, HEAVY_FREIGHT, false, 0,
          new EmissionHelper(6.0702, 0.0786, 0.2166, 0.1566)),
      new RoadConstructHelper("",
          new EmissionHelper(1.4749, 0.0786, 0.1177, 0.0794),
          80, FREEWAY, HEAVY_FREIGHT, true, 0,
          new EmissionHelper(6.0702, 0.0786, 0.2166, 0.1566)),
      new RoadConstructHelper("",
          new EmissionHelper(1.4749, 0.0786, 0.1177, 0.0794),
          100, FREEWAY, HEAVY_FREIGHT, false, 0,
          new EmissionHelper(6.0702, 0.0786, 0.2166, 0.1566)),
      new RoadConstructHelper("",
          new EmissionHelper(1.4749, 0.0786, 0.1177, 0.0794),
          100, FREEWAY, HEAVY_FREIGHT, true, 0,
          new EmissionHelper(6.0702, 0.0786, 0.2166, 0.1566)),
      new RoadConstructHelper("",
          new EmissionHelper(1.1584, 0.0525, 0.1151, 0.0868),
          120, FREEWAY, AUTO_BUS, false, 0,
          new EmissionHelper(4.129, 0.0525, 0.1201, 0.146)),
      new RoadConstructHelper("",
          new EmissionHelper(0.1037, 0.0266, 0.0214, 0.0147),
          80, NON_URBAN_ROAD_NATIONAL, LIGHT_TRAFFIC, false, 0,
          new EmissionHelper(0.1037, 0.0266, 0.0214, 0.0147)),
      new RoadConstructHelper("",
          new EmissionHelper(1.2802, 0.0511, 0.1284, 0.0834),
          80, NON_URBAN_ROAD_NATIONAL, NORMAL_FREIGHT, false, 0,
          new EmissionHelper(1.2802, 0.0511, 0.1284, 0.0834)),
      new RoadConstructHelper("",
          new EmissionHelper(2.9868, 0.0971, 0.1395, 0.0774),
          80, NON_URBAN_ROAD_NATIONAL, HEAVY_FREIGHT, false, 0,
          new EmissionHelper(2.9868, 0.0971, 0.1395, 0.0774)),
      new RoadConstructHelper("",
          new EmissionHelper(0.9613, 0.0038, 0.1477, 0.0704),
          80, NON_URBAN_ROAD_NATIONAL, AUTO_BUS, false, 0,
          new EmissionHelper(0.9613, 0.0038, 0.1477, 0.0704)),
      new RoadConstructHelper("",
          new EmissionHelper(0.1037, 0.0266, 0.0214, 0.0147),
          0, NON_URBAN_ROAD_GENERAL, LIGHT_TRAFFIC, false, 0,
          new EmissionHelper(0.1884, 0.018, 0.0369, 0.0289)),
      new RoadConstructHelper("",
          new EmissionHelper(1.2802, 0.0511, 0.1284, 0.0834),
          0, NON_URBAN_ROAD_GENERAL, NORMAL_FREIGHT, false, 0,
          new EmissionHelper(4.5993, 0.0594, 0.199, 0.1696)),
      new RoadConstructHelper("",
          new EmissionHelper(2.9868, 0.0971, 0.1395, 0.0774),
          0, NON_URBAN_ROAD_GENERAL, HEAVY_FREIGHT, false, 0,
          new EmissionHelper(5.5221, 0.0772, 0.351, 0.1639)),
      new RoadConstructHelper("",
          new EmissionHelper(0.9613, 0.0038, 0.1477, 0.0704),
          0, NON_URBAN_ROAD_GENERAL, AUTO_BUS, false, 0,
          new EmissionHelper(3.0753, 0.0055, 0.4524, 0.1441)),
      new RoadConstructHelper("",
          new EmissionHelper(0.1425, 0.0146, 0.0227, 0.0288),
          0, URBAN_ROAD_FREE_FLOW, LIGHT_TRAFFIC, false, 0,
          new EmissionHelper(0.1884, 0.018, 0.0369, 0.0289)),
      new RoadConstructHelper("",
          new EmissionHelper(1.6864, 0.0594, 0.1134, 0.1379),
          0, URBAN_ROAD_FREE_FLOW, NORMAL_FREIGHT, false, 0,
          new EmissionHelper(4.5993, 0.0594, 0.199, 0.1696)),
      new RoadConstructHelper("",
          new EmissionHelper(3.5597, 0.0772, 0.1612, 0.1318),
          0, URBAN_ROAD_FREE_FLOW, HEAVY_FREIGHT, false, 0,
          new EmissionHelper(5.5221, 0.0772, 0.351, 0.1639)),
      new RoadConstructHelper("",
          new EmissionHelper(1.421, 0.0055, 0.2019, 0.1116),
          0, URBAN_ROAD_FREE_FLOW, AUTO_BUS, false, 0,
          new EmissionHelper(3.0753, 0.0055, 0.4524, 0.1441)),
      new RoadConstructHelper("",
          new EmissionHelper(0.1501, 0.016, 0.0276, 0.0287),
          0, URBAN_ROAD_NORMAL, LIGHT_TRAFFIC, false, 0,
          new EmissionHelper(0.1884, 0.018, 0.0369, 0.0289)),
      new RoadConstructHelper("",
          new EmissionHelper(2.5086, 0.0594, 0.135, 0.1481),
          0, URBAN_ROAD_NORMAL, NORMAL_FREIGHT, false, 0,
          new EmissionHelper(4.5993, 0.0594, 0.199, 0.1696)),
      new RoadConstructHelper("",
          new EmissionHelper(4.4918, 0.0772, 0.2471, 0.1423),
          0, URBAN_ROAD_NORMAL, HEAVY_FREIGHT, false, 0,
          new EmissionHelper(5.5221, 0.0772, 0.351, 0.1639)),
      new RoadConstructHelper("",
          new EmissionHelper(1.8332, 0.0055, 0.2828, 0.1217),
          0, URBAN_ROAD_NORMAL, AUTO_BUS, false, 0,
          new EmissionHelper(3.0753, 0.0055, 0.4524, 0.1441)),
      new RoadConstructHelper("",
          new EmissionHelper(0.1884, 0.018, 0.0369, 0.0289),
          0, URBAN_ROAD_STAGNATING, LIGHT_TRAFFIC, false, 0,
          new EmissionHelper(0.1884, 0.018, 0.0369, 0.0289)),
      new RoadConstructHelper("",
          new EmissionHelper(4.5993, 0.0594, 0.199, 0.1696),
          0, URBAN_ROAD_STAGNATING, NORMAL_FREIGHT, false, 0,
          new EmissionHelper(4.5993, 0.0594, 0.199, 0.1696)),
      new RoadConstructHelper("",
          new EmissionHelper(5.5221, 0.0772, 0.351, 0.1639),
          0, URBAN_ROAD_STAGNATING, HEAVY_FREIGHT, false, 0,
          new EmissionHelper(5.5221, 0.0772, 0.351, 0.1639)),
      new RoadConstructHelper("",
          new EmissionHelper(3.0753, 0.0055, 0.4524, 0.1441),
          0, URBAN_ROAD_STAGNATING, AUTO_BUS, false, 0,
          new EmissionHelper(3.0753, 0.0055, 0.4524, 0.1441)));

  private static final List<String> ON_ROAD_MOBILE_SOURCE_CATEGORIES = Arrays.asList(
      "BA-B-E3",
      "BA-D-E6-ZW",
      "BA-L-E5");

  static SectorCategories construct() {
    final SectorCategories categories = new SectorCategories();

    mockSectors(categories);
    mockFarmLodging(categories);
    mockFarmland(categories);
    mockRoad(categories);
    mockOffRoadMobileSources(categories);
    mockOnRoadMobileSources(categories);
    mockMaritimeShipping(categories);
    mockInlandShipping(categories);
    mockShippingSnappableNodes(categories);

    return categories;
  }

  private static void mockSectors(final SectorCategories categories) {
    final ArrayList<Sector> sectors = new ArrayList<>();
    categories.setSectors(sectors);

    for (final int sectorId : SECTORS) {
      final Sector sector = mock(Sector.class);
      final OPSSourceCharacteristics characteristics = mock(OPSSourceCharacteristics.class);
      when(sector.getSectorId()).thenReturn(sectorId);
      when(sector.getDefaultCharacteristics()).thenReturn(characteristics);
      when(characteristics.copyTo(any())).thenAnswer(invocation -> invocation.getArgument(0));
      sectors.add(sector);
    }
  }

  private static void mockFarmLodging(final SectorCategories categories) {
    final FarmLodgingCategories farmLodgingCategories = new FarmLodgingCategories();
    categories.setFarmLodgingCategories(farmLodgingCategories);

    final FarmAnimalCategory animalCategory = mock(FarmAnimalCategory.class);
    when(animalCategory.getCode()).thenReturn(FARM_ANIMAL_CODE);

    final ArrayList<FarmLodgingCategory> lodgingCategories = new ArrayList<>();

    for (final FarmConstructHelper helper : FARM_LODGING_CATEGORIES) {
      final FarmLodgingCategory lodgingCategory = mock(FarmLodgingCategory.class);
      when(lodgingCategory.getCode()).thenReturn(helper.code);
      when(lodgingCategory.getEmissionFactor()).thenReturn(helper.emissionFactor);
      final List<FarmLodgingSystemDefinition> systemDefinitions = helper.systemDefinitions.stream().map(systemDefinitionCode -> {
        final FarmLodgingSystemDefinition systemDefinition = mock(FarmLodgingSystemDefinition.class);
        when(systemDefinition.getCode()).thenReturn(systemDefinitionCode);
        return systemDefinition;
      }).toList();
      when(lodgingCategory.getFarmLodgingSystemDefinitions()).thenReturn(systemDefinitions);
      when(lodgingCategory.getAnimalCategory()).thenReturn(animalCategory);
      lodgingCategories.add(lodgingCategory);
    }

    farmLodgingCategories.setFarmLodgingSystemCategories(lodgingCategories);

    final ArrayList<FarmAdditionalLodgingSystemCategory> additionalSystems = new ArrayList<>();

    for (final FarmConstructHelper helper : FARM_ADDITIONAL_SYSTEM_CATEGORIES) {
      final FarmAdditionalLodgingSystemCategory additionalSystem = mock(FarmAdditionalLodgingSystemCategory.class);
      when(additionalSystem.getCode()).thenReturn(helper.code);
      when(additionalSystem.getEmissionFactor()).thenReturn(helper.emissionFactor);
      additionalSystems.add(additionalSystem);
    }

    farmLodgingCategories.setFarmAdditionalLodgingSystemCategories(additionalSystems);

    final ArrayList<FarmReductiveLodgingSystemCategory> reductiveSystems = new ArrayList<>();

    for (final FarmConstructHelper helper : FARM_REDUCTIVE_SYSTEM_CATEGORIES) {
      final FarmReductiveLodgingSystemCategory reductiveSystem = mock(FarmReductiveLodgingSystemCategory.class);
      when(reductiveSystem.getCode()).thenReturn(helper.code);
      when(reductiveSystem.getReductionFactor()).thenReturn(helper.emissionFactor);
      reductiveSystems.add(reductiveSystem);
    }

    farmLodgingCategories.setFarmReductiveLodgingSystemCategories(reductiveSystems);

    final ArrayList<FarmLodgingFodderMeasureCategory> fodderMeasures = new ArrayList<>();

    for (final FarmFodderConstructHelper helper : FARM_FODDER_MEASURE_CATEGORIES) {
      final FarmLodgingFodderMeasureCategory fodderMeasure = mock(FarmLodgingFodderMeasureCategory.class);
      when(fodderMeasure.getCode()).thenReturn(helper.code);
      when(fodderMeasure.getReductionFactorTotal()).thenReturn(helper.reductionTotal);
      when(fodderMeasure.getReductionFactorFloor()).thenReturn(helper.reductionFloor);
      when(fodderMeasure.getReductionFactorCellar()).thenReturn(helper.reductionCellar);
      final AmmoniaProportion ammoniaProportion = mock(AmmoniaProportion.class);
      when(ammoniaProportion.getProportionFloor()).thenReturn(helper.proportionFloor);
      when(ammoniaProportion.getProportionCellar()).thenReturn(helper.proportionCellar);
      final Map<FarmAnimalCategory, AmmoniaProportion> ammoniaProportions = Collections.singletonMap(animalCategory, ammoniaProportion);
      when(fodderMeasure.getAmmoniaProportions()).thenReturn(ammoniaProportions);
      when(fodderMeasure.canApplyToFarmLodgingCategory(any()))
          .thenAnswer(invocation -> helper.matchLodging.apply(invocation.getArgument(0, FarmLodgingCategory.class).getCode()));
      fodderMeasures.add(fodderMeasure);
    }

    farmLodgingCategories.setFarmLodgingFodderMeasureCategories(fodderMeasures);
  }

  private static void mockFarmland(final SectorCategories categories) {
    final ArrayList<FarmlandCategory> farmlandCategories = new ArrayList<>();
    categories.setFarmlandCategories(farmlandCategories);

    for (final String code : FARMLAND_CATEGORIES) {
      final FarmlandCategory farmland = mock(FarmlandCategory.class);
      when(farmland.getCode()).thenReturn(code);
      farmlandCategories.add(farmland);
    }

  }

  private static void mockRoad(final SectorCategories categories) {
    final RoadEmissionCategories roadEmissionCategories = new RoadEmissionCategories();
    categories.setRoadEmissionCategories(roadEmissionCategories);

    for (final RoadConstructHelper helper : ROAD_CATEGORIES) {
      final RoadEmissionCategoryKey key = new RoadEmissionCategoryKey("NL", helper.roadType, helper.vehicleType);
      final RoadEmissionCategory category = spy(new RoadEmissionCategory(key,
          helper.strictEnforcement, helper.maximumSpeed, helper.gradient));
      when(category.getEmissionFactor(any())).thenAnswer(invocation -> toResult(helper.emissions, (EmissionValueKey) invocation.getArgument(0)));
      when(category.getStagnatedEmissionFactor(any()))
          .thenAnswer(invocation -> toResult(helper.stagnatedEmissions, (EmissionValueKey) invocation.getArgument(0)));
      roadEmissionCategories.add(category);
    }
  }

  private static void mockOffRoadMobileSources(final SectorCategories categories) {
    final ArrayList<OffRoadMobileSourceCategory> offRoadMobileSources = new ArrayList<>();
    categories.setOffRoadMobileSourceCategories(offRoadMobileSources);

    for (final OffRoadConstructHelper helper : OFF_ROAD_MOBILE_SOURCE_CATEGORIES) {
      final OffRoadMobileSourceCategory offRoadType = mock(OffRoadMobileSourceCategory.class);
      when(offRoadType.getCode()).thenReturn(helper.code);
      when(offRoadType.getEmissionFactorPerLiterFuel(any(Substance.class)))
          .thenAnswer(invocation -> toResult(helper.emissionFactorsPerLiterFuel, (Substance) invocation.getArgument(0)));
      when(offRoadType.expectsLiterFuel()).thenReturn(helper.emissionFactorsPerLiterFuel != null);
      when(offRoadType.getEmissionFactorPerOperatingHour(any(Substance.class)))
          .thenAnswer(invocation -> toResult(helper.emissionFactorsPerOperatingHour, (Substance) invocation.getArgument(0)));
      when(offRoadType.expectsOperatingHours()).thenReturn(helper.emissionFactorsPerOperatingHour != null);
      when(offRoadType.getEmissionFactorPerLiterAdBlue(any(Substance.class)))
          .thenAnswer(invocation -> toResult(helper.emissionFactorsPerLiterAdBlue, (Substance) invocation.getArgument(0)));
      when(offRoadType.expectsLiterAdBlue()).thenReturn(helper.emissionFactorsPerLiterAdBlue != null);
      offRoadMobileSources.add(offRoadType);
    }
  }

  private static void mockOnRoadMobileSources(final SectorCategories categories) {
    final ArrayList<OnRoadMobileSourceCategory> onRoadMobileSources = new ArrayList<>();
    categories.setOnRoadMobileSourceCategories(onRoadMobileSources);

    for (final String code : ON_ROAD_MOBILE_SOURCE_CATEGORIES) {
      final OnRoadMobileSourceCategory onRoadMobileSource = mock(OnRoadMobileSourceCategory.class);
      when(onRoadMobileSource.getCode()).thenReturn(code);

      onRoadMobileSources.add(onRoadMobileSource);
    }
  }

  private static void mockMaritimeShipping(final SectorCategories categories) {
    final ArrayList<MaritimeShippingCategory> maritimeShippingCategories = new ArrayList<>();
    categories.setMaritimeShippingCategories(maritimeShippingCategories);

    for (final String code : MARITIME_SHIPPING_TYPES) {
      final MaritimeShippingCategory shipType = mock(MaritimeShippingCategory.class);
      when(shipType.getCode()).thenReturn(code);
      maritimeShippingCategories.add(shipType);
    }
  }

  private static void mockInlandShipping(final SectorCategories categories) {
    final InlandShippingCategories inlandShippingCategories = new InlandShippingCategories();
    categories.setInlandShippingCategories(inlandShippingCategories);

    final ArrayList<InlandShippingCategory> shipTypes = new ArrayList<>();

    for (final String code : INLAND_SHIPPING_TYPES) {
      final InlandShippingCategory shipType = mock(InlandShippingCategory.class);
      when(shipType.getCode()).thenReturn(code);
      shipTypes.add(shipType);
    }

    inlandShippingCategories.setShipCategories(shipTypes);

    final ArrayList<InlandWaterwayCategory> waterways = new ArrayList<>();

    for (final String code : INLAND_WATERWAYS) {
      final InlandWaterwayCategory waterway = mock(InlandWaterwayCategory.class);
      when(waterway.getCode()).thenReturn(code);
      waterways.add(waterway);

      for (final InlandShippingCategory shipType : shipTypes) {
        inlandShippingCategories.addWaterwayForShip(shipType, waterway);
      }
    }

    inlandShippingCategories.setWaterwayCategories(waterways);
  }

  private static void mockShippingSnappableNodes(final SectorCategories categories) {
    final ArrayList<ShippingNode> shippingSnappableNodes = new ArrayList<>();
    categories.setShippingSnappableNodes(shippingSnappableNodes);
  }

  private static double toResult(final EmissionHelper helper, final EmissionValueKey key) {
    return toResult(helper, key.getSubstance());
  }

  private static double toResult(final EmissionHelper helper, final Substance substance) {
    return switch (substance) {
      case NOX -> helper.emissionFactorNOx;
      case NH3 -> helper.emissionFactorNH3;
      case NO2 -> helper.emissionFactorNO2;
      case PM10 -> helper.emissionFactorPM10;
      default -> 0.0;
    };
  }

  private static class EmissionHelper {
    final double emissionFactorNOx;
    final double emissionFactorNH3;
    final double emissionFactorNO2;
    final double emissionFactorPM10;

    EmissionHelper(final double emissionFactorNOx, final double emissionFactorNH3) {
      this(emissionFactorNOx, emissionFactorNH3, 0.0, 0.0);
    }

    EmissionHelper(final double emissionFactorNOx, final double emissionFactorNH3, final double emissionFactorNO2, final double emissionFactorPM10) {
      this.emissionFactorNOx = emissionFactorNOx;
      this.emissionFactorNH3 = emissionFactorNH3;
      this.emissionFactorNO2 = emissionFactorNO2;
      this.emissionFactorPM10 = emissionFactorPM10;
    }

  }

  private static class GenericConstructHelper {

    final String code;
    final EmissionHelper emissions;

    GenericConstructHelper(final String code, final EmissionHelper emissions) {
      this.code = code;
      this.emissions = emissions;
    }
  }

  private static class OffRoadConstructHelper {

    final String code;
    final EmissionHelper emissionFactorsPerLiterFuel;
    final EmissionHelper emissionFactorsPerOperatingHour;
    final EmissionHelper emissionFactorsPerLiterAdBlue;

    public OffRoadConstructHelper(final String code, final EmissionHelper emissionFactorsPerLiterFuel,
        final EmissionHelper emissionFactorsPerOperatingHour, final EmissionHelper emissionFactorsPerLiterAdBlue) {
      this.code = code;
      this.emissionFactorsPerLiterFuel = emissionFactorsPerLiterFuel;
      this.emissionFactorsPerOperatingHour = emissionFactorsPerOperatingHour;
      this.emissionFactorsPerLiterAdBlue = emissionFactorsPerLiterAdBlue;
    }

  }

  private static class RoadConstructHelper extends GenericConstructHelper {

    final int maximumSpeed;
    final String roadType;
    final String vehicleType;
    final boolean strictEnforcement;
    final int gradient;
    final EmissionHelper stagnatedEmissions;

    public RoadConstructHelper(final String code, final EmissionHelper emissions, final int maximumSpeed, final String roadType,
        final String vehicleType, final boolean strictEnforcement, final int gradient, final EmissionHelper stagnatedEmissions) {
      super(code, emissions);
      this.maximumSpeed = maximumSpeed;
      this.roadType = roadType;
      this.vehicleType = vehicleType;
      this.strictEnforcement = strictEnforcement;
      this.gradient = gradient;
      this.stagnatedEmissions = stagnatedEmissions;
    }

  }

  private static class FarmConstructHelper {

    final String code;
    final double emissionFactor;
    final List<String> systemDefinitions;

    FarmConstructHelper(final String code, final double emissionFactor, final String... systemDefinitions) {
      this.code = code;
      this.emissionFactor = emissionFactor;
      this.systemDefinitions = Arrays.asList(systemDefinitions);
    }
  }

  private static class FarmFodderConstructHelper {

    final String code;
    final double reductionTotal;
    final double reductionFloor;
    final double reductionCellar;
    final double proportionFloor;
    final double proportionCellar;
    final Function<String, Boolean> matchLodging;

    FarmFodderConstructHelper(final String code, final double reductionTotal, final double reductionFloor, final double reductionCellar,
        final double proportionFloor, final double proportionCellar, final Function<String, Boolean> matchLodging) {
      this.code = code;
      this.reductionTotal = reductionTotal;
      this.reductionFloor = reductionFloor;
      this.reductionCellar = reductionCellar;
      this.proportionFloor = proportionFloor;
      this.proportionCellar = proportionCellar;
      this.matchLodging = matchLodging;
    }
  }

}
