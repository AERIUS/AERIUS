/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;

import nl.aerius.taskmanager.client.util.QueueHelper;
import nl.aerius.taskmanager.test.MockConnection;
import nl.aerius.taskmanager.test.MockedChannelFactory;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Mocking connection for OPS related workers.
 * Any OPSInputData sent through will result in random results for each receptor up to a maximum value.
 */
public class OwN2000MockConnection extends MockConnection {

  private final Map<String, Double> currentValues = new HashMap<>();
  private final double initialValue;
  private final double resultDecrementFactor;
  private final Consumer<EngineInputData<?, ?>> inputAsserter;

  public OwN2000MockConnection(final double initialValue, final double resultDecrementFactor,
      final Consumer<EngineInputData<?, ?>> inputAsserter) {
    this.initialValue = initialValue;
    this.resultDecrementFactor = resultDecrementFactor;
    this.inputAsserter = inputAsserter;
  }

  @Override
  public Channel createChannel() throws IOException {
    return MockedChannelFactory.create(this::mockResults);
  }

  /**
   * Mocks a Channel to return results that with each run decrease in value. This can be used to mock deposition results in unit tests. Given the
   * points are with each run at a large distance to the sources.
   */
  private byte[] mockResults(final BasicProperties properties, final byte[] body) {
    synchronized (this) {
      try {
        final EngineInputData<?, ?> input = (EngineInputData<?, ?>) QueueHelper.bytesToObject(body);
        inputAsserter.accept(input);
        final CalculationResult cr = new CalculationResult(input.getChunkStats(), input.getEngineDataKey());

        for (final int sectorId : input.getEmissionSources().keySet()) {
          cr.put(sectorId, input.getReceptors().stream()
              .map(r -> toResultPoint(r, updateValue(properties.getMessageId()), input.getEmissionResultKeys())).collect(Collectors.toList()));
        }

        return QueueHelper.objectToBytes(cr);
      } catch (final IOException | ClassNotFoundException e) {
        return null;
      }
    }
  }

  private double updateValue(final String correlationId) {
    final Double oldValue = currentValues.computeIfAbsent(correlationId, cid -> initialValue);

    currentValues.put(correlationId, Math.max(0.0, oldValue - resultDecrementFactor));
    return oldValue;
  }

  private static AeriusResultPoint toResultPoint(final AeriusPoint receptor, final double resultFactor, final Set<EmissionResultKey> keys) {
    final AeriusResultPoint resultPoint = new AeriusResultPoint(receptor);

    for (final EmissionResultKey resultKey : keys) {
      resultPoint.setEmissionResult(resultKey, resultFactor);
    }
    return resultPoint;
  }
}
