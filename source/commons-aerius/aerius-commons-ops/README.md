# OPS Worker


# Unit testing

Unit test can be run with and without running OPS.
By default the unit tests run without actually running OPS.
This because we're not testing OPS itself, just how input and output of OPS is handled.

The unit tests use output files in the `src/test/resources/nl/overheid/aerius/ops/mock_results` directory to mock the OPS output.
When new files need to generated, set the `AERIUS_OPS_GENERATE_MOCK_RESULTS=1` before running maven.
This can be done when new OPS version is set and changes need to be checked.
