/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.exception.OPSInvalidVersionException;
import nl.overheid.aerius.ops.util.OPSSplitProvidedUtil;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class to test if a OPS run with option SPLIT_PROVIDED is correctly run.
 */
@ExtendWith(MockitoExtension.class)
class RunOPSSplitProvidedTest extends OPSRunnerTestBase {

  private static final String BRN = "emissions";
  private static final String BRN_LOW_EMISSIONS = "low_emissions";

  private static final int ID_1 = 4697681;
  private static final double RESULT_NO_SUB_4697681 = 0.02703;
  private static final double RESULT_NO_SUB_4697689 = 0.03351;

  private static final double RESULT_SUB_4697681 = 0.23147;
  private static final double RESULT_SUB_4697689 = 18.52;

  /**
   * Tests if a run of OPS with SPLIT_PROVIDED has calculated the expected results.
   * It should have merged the results of the 2 runs.
   */
  @ParameterizedTest
  @ValueSource(strings = {BRN, BRN_LOW_EMISSIONS})
  void testSplitProvidedRun(final String brn) throws IOException, AeriusException, OPSInvalidVersionException {
    mockOPSOutputFilesWithCounter("SplitProvidedRun-" + brn);
    final OPSInputData inputData = OPSSplitProvidedUtil.createInputData(YEAR, brn);
    inputData.setMaxDistance(true);
    final List<AeriusResultPoint> results = runOPS.run(inputData).getResults().get(1);

    assertEquals(2, results.size(), "Should have 2 results");
    assertResults(results, BRN_LOW_EMISSIONS.equals(brn));
  }

  /**
   * Tests if a run of OPS with SPLIT_PROVIDED has calculated the expected results.
   * But this only has receptors, no subreceptors. Therefore it doesn't need to merge as there is no run with the subreceptors.
   */
  @Test
  void testSplitProvidedNoSubreceptorsRun() throws IOException, AeriusException, OPSInvalidVersionException {
    mockOPSOutputFiles("SplitProvidedNoSubreceptorsRun");
    final OPSInputData inputData = OPSSplitProvidedUtil.createInputData(YEAR, BRN);
    OPSSplitProvidedUtil.replaceReceptorFarAwayOnly(inputData.getReceptors());

    final List<AeriusResultPoint> results = runOPS.run(inputData).getResults().get(1);
    assertEquals(1, results.size(), "Should have 1 result");
    assertEquals(ID_1, results.get(0).getId(), "Should have only result for id " + ID_1);
    assertResults(results, false);
  }

  private static void assertResults(final List<AeriusResultPoint> results, final boolean lowEmissions) {
    results.forEach(r -> {
      final double calcResult = r.getEmissionResult(EmissionResultKey.NOX_DEPOSITION);
      final double expectedResult;

      if (r.getId() == ID_1) {
        expectedResult = RESULT_NO_SUB_4697681 + (lowEmissions ? 0 : RESULT_SUB_4697681);
      } else {
        expectedResult = RESULT_NO_SUB_4697689 + (lowEmissions ? 0 : RESULT_SUB_4697689);
      }
      assertEquals(expectedResult, calcResult, 1.0E-4, "Not the expected calculated result");
    });
  }
}
