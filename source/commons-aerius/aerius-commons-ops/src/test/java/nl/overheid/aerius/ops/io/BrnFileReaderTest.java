/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.domain.OPSSource.OPSSourceOutflowDirection;
import nl.overheid.aerius.ops.util.OPSTestUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.SridPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.geo.EPSG;

/**
 * Test class for BrnFileReaders.
 */
class BrnFileReaderTest {

  private static final double EPSILON = 1E-5;

  private static final String RESOURCES_DIRECTORY = "io/source/";
  private static final String USED_EXTENSION = ".brn";
  private static final String BRN_V0_STYLE_FILE = "emission_v0";
  private static final String BRN_V1_STYLE_FILE = "emission_v1";
  private static final String BRN_V4_STYLE_FILE = "emission_v4";
  private static final String EMISSION_FAIL_FILE = "emission_fail";

  @Test
  void testParseOPSv0File() throws IOException {
    assertParseRegularOPSFile(new BrnFileReader(Substance.NH3), BRN_V0_STYLE_FILE);
  }

  @Test
  void testParseOPSv1File() throws IOException {
    assertParseRegularOPSFile(new BrnFileReader(Substance.NH3), BRN_V1_STYLE_FILE);
  }

  @Test
  void testParseOPSv4File() throws IOException {
    assertParseRegularOPSFile(new BrnFileReader(Substance.NH3), BRN_V4_STYLE_FILE);
    assertPauseV4SpecificFile(new BrnFileReader(Substance.NH3), BRN_V4_STYLE_FILE);
  }

  @Test
  void testParseRegularOPSFileFail1() throws IOException {
    // first fail file is a file where a line has been cut in half.
    final LineReaderResult<OPSSource> result = readBrnFile(EMISSION_FAIL_FILE + 1);

    assertEquals(2, result.getExceptions().size(), "Number of exceptions");
    // Fail line: "   0  139123  434456 2.500E-01  0.000   2.5      0   0.2   4   "
    final AeriusException e1 = result.getExceptions().get(0);
    assertEquals(AeriusExceptionReason.IO_EXCEPTION_NOT_ENOUGH_FIELDS, e1.getReason(), "Error not enough fields");
    assertEquals("2", e1.getArgs()[0], "Error line number not enough fields");
    // Fail line: "   1   1   0  17"
    final AeriusException e2 = result.getExceptions().get(1);
    assertEquals(AeriusExceptionReason.IO_EXCEPTION_NUMBER_FORMAT, e2.getReason(), "Error reason");
    assertEquals("3", e2.getArgs()[0], "Error line number");
    assertEquals("x(m)", e2.getArgs()[1], "Error column name");
    assertEquals("1   0", e2.getArgs()[2], "Error column content");
  }

  @Test
  void testParseRegularOPSFileFail2() throws IOException {
    // second fail file is a file where a line contains a word.
    final LineReaderResult<OPSSource> result = readBrnFile(EMISSION_FAIL_FILE + 2);

    assertEquals(1, result.getExceptions().size(), "Number of exceptions");
    // Fail line: "   0  139123  434456 2.500E-01  0.000   two      0   0.2   4   1   1   0  17"
    final AeriusException e = result.getExceptions().get(0);
    assertEquals(AeriusExceptionReason.IO_EXCEPTION_NUMBER_FORMAT, e.getReason(), "Error reason");
    assertEquals("2", e.getArgs()[0], "Error line number");
    assertEquals("h(m)", e.getArgs()[1], "Error column name");
    assertEquals("two", e.getArgs()[2], "Error column content");
  }

  @Test
  void testParseRegularOPSFileFail3() throws IOException {
    // third is an empty file, which won't cause an exception, parsing just returns empty collection.

    final LineReaderResult<OPSSource> result = readBrnFile(EMISSION_FAIL_FILE + 3);
    final List<OPSSource> sources = result.getObjects();

    assertNotNull(sources, "Check sources for fail3 test should not be null");
    assertEquals(0, sources.size(), "Sources parsed");
  }

  @Test
  void testParseRegularOPSFileFail4() throws IOException {
    // 4rd fail file is a file which has a line where proper format isn't used (space separated instead of fixed width).
    final LineReaderResult<OPSSource> result = readBrnFile(EMISSION_FAIL_FILE + 4);

    assertEquals(1, result.getExceptions().size(), "Number of exceptions");
    // Fail line: " 0 139123 434456 2.500E-01 0.000 2.5 0 0.2 4 1 1 0 17",
    final AeriusException e = result.getExceptions().get(0);
    assertEquals(AeriusExceptionReason.IO_EXCEPTION_NUMBER_FORMAT, e.getReason(), "Error reason");
    assertEquals("2", e.getArgs()[0], "Error line number");
    assertEquals("snr", e.getArgs()[1], "Error column name");
    assertEquals("0 1", e.getArgs()[2], "Error column content");
  }

  private void assertParseRegularOPSFile(final BrnFileReader reader, final String filename) throws IOException {
    final LineReaderResult<OPSSource> result = readBrnFile(reader, filename);
    final List<OPSSource> sources = result.getObjects();

    assertNotNull(sources, "Sources for regual file should not be null");
    assertEquals(7, sources.size(), "Sources parsed");
    // ID's are descending from 7
    int id = 7;
    for (final OPSSource source : sources) {
      assertEquals(id--, source.getId(), "Source ID");
    }
    // header    : snr    x(m)    y(m)    q(g/s) hc(MW)  h(m)   d(m)  s(m)  dv cat area sd  comp
    // first line:   7  139123  434456 2.500E-01  2.100   2.5      3   0.2   4   2   2   2  17
    assertEquals(139123, sources.get(0).getPoint().getX(), EPSILON, "X coord");
    assertEquals(434456, sources.get(0).getPoint().getY(), EPSILON, "Y coord");
    assertEquals(2.500E-01 * Substance.EMISSION_IN_G_PER_S_FACTOR, sources.get(0).getEmission(Substance.NH3), EPSILON, "Emission");
    assertEquals(2.100, sources.get(0).getHeatContent(), 0.000001, "Heat content");
    assertEquals(2.5, sources.get(0).getEmissionHeight(), EPSILON, "Height");
    assertEquals(3, sources.get(0).getDiameter(), "Diameter");
    assertEquals(0.2, sources.get(0).getSpread(), EPSILON, "Spread");
    assertEquals(4, sources.get(0).getDiurnalVariation(), "Diurnal variation");
    assertEquals(2, sources.get(0).getParticleSizeDistribution(), "Particle size distribution");
    assertEquals(2, sources.get(0).getCat(), "Category number");
    assertEquals(2, sources.get(0).getArea(), "Area");
    assertEquals("17", sources.get(0).getComp(), "Comp");

    // Test for EPSG 4326 coordinate.
    assertPoint(sources.get(1).getPoint(), 13.123, 43.456, EPSG.WGS84.getSrid());
  }

  private void assertPauseV4SpecificFile(final BrnFileReader reader, final String filename) throws IOException {
    final LineReaderResult<OPSSource> result = readBrnFile(reader, filename);
    final List<OPSSource> sources = result.getObjects();

    assertEquals(1.10, sources.get(0).getOutflowDiameter(), EPSILON, "Exit surface");
    assertEquals(25.94, sources.get(0).getOutflowVelocity(), EPSILON, "Exit velocity");
    assertEquals(OPSSourceOutflowDirection.VERTICAL, sources.get(0).getOutflowDirection(), "Exit direction");
    assertEquals(50.00, sources.get(0).getEmissionTemperature(), EPSILON, "Emission temperature");

    // second source should have a negative velocity which means horizontal exit direction, but the method should return the absolute value
    assertEquals(25.94, sources.get(1).getOutflowVelocity(), EPSILON, "Exit velocity");
    assertEquals(OPSSourceOutflowDirection.HORIZONTAL, sources.get(1).getOutflowDirection(), "Exit direction");
  }

  private void assertPoint(final SridPoint point, final double x, final double y, final int srid) {
    assertEquals(x, point.getX(), EPSILON, "X coord");
    assertEquals(y, point.getY(), EPSILON, "Y coord");
    assertEquals(srid, point.getSrid(), "SRID");
  }

  private LineReaderResult<OPSSource> readBrnFile(final String fileName) throws IOException {
    return OPSTestUtil.readBrnFile(Substance.NH3, RESOURCES_DIRECTORY + fileName + USED_EXTENSION);
  }

  private LineReaderResult<OPSSource> readBrnFile(final BrnFileReader reader, final String fileName) throws IOException {
    return OPSTestUtil.readBrnFile(reader, RESOURCES_DIRECTORY + fileName + USED_EXTENSION);
  }
}
