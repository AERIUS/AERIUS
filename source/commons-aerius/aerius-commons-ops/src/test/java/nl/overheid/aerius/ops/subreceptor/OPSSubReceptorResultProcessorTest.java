/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.subreceptor;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.calculation.EngineInputData.CommandType;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.io.ControlFileOptions;
import nl.overheid.aerius.ops.version.OPSVersion;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.geo.EPSG;

/**
 * Test class for {@link RecalculateOPSSubReceptorResultProcessor}.
 */
class OPSSubReceptorResultProcessorTest {

  private static final double NOX_DEP = 333.0;

  private final RecalculateOPSSubReceptorResultProcessor postProcessor = new RecalculateOPSSubReceptorResultProcessor();

  @Test
  void testMergeResults() {
    final AeriusResultPoint originalResult = createResult(1, 1010, 1010, 200, 300);
    final List<AeriusResultPoint> subResults = List.of(
        createResult(1, 1010, 1010, 100, 150), // original recalculated point
        createResult(2, 1050, 1050, 40, 60), // sub point 2
        createResult(3, 1050, 1050, 20, 80), // sub point 3
        createResult(3, 1011, 1011, 300, 400)); // sub point 4, but should not be included

    replaceResults(originalResult, subResults);
    // Results should be original (200) - new org point (100) + new average (40 + 20) / 2
    assertEquals(130.0, originalResult.getEmissionResult(EmissionResultKey.NH3_DEPOSITION), 0.0, "Expected new deposition");
    // Results should be original (300) - new org point (150) + new average (60 + 80) / 2
    assertEquals(220.0, originalResult.getEmissionResult(EmissionResultKey.NH3_CONCENTRATION), 0.0, "Expected new concentration");
    assertEquals(NOX_DEP, originalResult.getEmissionResult(EmissionResultKey.NOX_DEPOSITION), 0.0, "NOX deposition should not be changed.");
  }

  private void replaceResults(final AeriusResultPoint originalResult, final List<AeriusResultPoint> subResults) {
    final OPSSource source = createSource();
    final OPSInputData inputData = new OPSInputData(OPSVersion.LATEST, CommandType.EXPORT, 1);
    final ControlFileOptions cfo = new ControlFileOptions(inputData, Substance.NH3, true,
        EnumSet.of(EmissionResultKey.NH3_CONCENTRATION, EmissionResultKey.NH3_DEPOSITION), Set.of(3100));
    postProcessor.replaceResults(source, originalResult, cfo, subResults);
  }

  private static OPSSource createSource() {
    return new OPSSource(1, EPSG.RDNEW.getSrid(), 1000, 1000);
  }

  private static AeriusResultPoint createResult(final int id, final int x, final int y, final double dep, final double con) {
    final AeriusResultPoint rp = new AeriusResultPoint(id, null, AeriusPointType.RECEPTOR, x, y);

    rp.setEmissionResult(EmissionResultKey.NOX_DEPOSITION, NOX_DEP);
    rp.setEmissionResult(EmissionResultKey.NH3_DEPOSITION, dep);
    rp.setEmissionResult(EmissionResultKey.NH3_CONCENTRATION, con);
    return rp;
  }
}
