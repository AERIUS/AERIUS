/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.importer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.io.ImportReader;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Test class for {@link BrnImportReader}.
 */
class BrnImportReaderTest {

  private static final String FILENAME_VALID = "BrnImportReaderValid.brn";
  private static final String FILENAME_WITH_ERROR = "BrnImportReaderWithError.brn";
  private final ImportReader ir = new BrnImportReader(true, false);

  private final SectorCategories categories = new SectorCategories();

  @Test
  void detectTest() {
    assertTrue(ir.detect("sources.brn"), "Detect extension correcty");
    assertTrue(ir.detect("sources.BRN"), "Detect extension case insane");
    assertFalse(ir.detect("sources"), "Not detect other extensions");
  }

  @Test
  void testBRNWithoutSubstance() throws IOException, SQLException {
    try (InputStream inputStream = getClass().getResourceAsStream(FILENAME_VALID)) {
      final AeriusException e = assertThrows(AeriusException.class, () -> ir.read(FILENAME_VALID, inputStream, categories, null, new ImportParcel()));

      assertEquals(AeriusExceptionReason.BRN_WITHOUT_SUBSTANCE, e.getReason(), "Exception reason BRN without substance");
    }
  }

  @Test
  void testBRNUnsupportedSubstance() throws IOException, SQLException {
    try (InputStream inputStream = getClass().getResourceAsStream(FILENAME_VALID)) {
      final AeriusException e = assertThrows(AeriusException.class,
          () -> ir.read(FILENAME_VALID, inputStream, categories, Substance.NOXNH3, new ImportParcel()));

      assertEquals(AeriusExceptionReason.BRN_SUBSTANCE_NOT_SUPPORTED, e.getReason(), "Exception reason BRN unsupported substance");
    }
  }

  @Test
  void testBRNValid() throws IOException, AeriusException {
    final ImportParcel result = new ImportParcel();
    try (InputStream inputStream = getClass().getResourceAsStream(FILENAME_VALID)) {
      ir.read(FILENAME_VALID, inputStream, categories, Substance.NH3, result);
    }
    assertEquals(6, result.getSituation().getEmissionSourcesList().size(), "Should have read # rows correctly");
    assertTrue(result.getExceptions().isEmpty(), "Should have read 0 errors");
    assertEquals(1, result.getWarnings().size(), "Should have read # warnings");
    assertEquals(0,
        ((OPSSourceCharacteristics) result.getSituation().getEmissionSourcesList().get(2).getProperties().getCharacteristics()).getDiameter(),
        "Diameter for source 2 should have been 0");
  }

  @Test
  void testBRNValidReadingDiameter() throws IOException, AeriusException {
    final ImportParcel result = new ImportParcel();
    final ImportReader irImportingDiameter = new BrnImportReader(true, true);
    try (InputStream inputStream = getClass().getResourceAsStream(FILENAME_VALID)) {
      irImportingDiameter.read(FILENAME_VALID, inputStream, categories, Substance.NH3, result);
    }
    assertEquals(6, result.getSituation().getEmissionSourcesList().size(), "Should have read # rows correctly");
    assertTrue(result.getExceptions().isEmpty(), "Should have read 0 errors");
    assertTrue(result.getWarnings().isEmpty(), "Should have read 0 warnings");
    assertEquals(5000,
        ((OPSSourceCharacteristics) result.getSituation().getEmissionSourcesList().get(2).getProperties().getCharacteristics()).getDiameter(),
        "Proper diameter should have been read for source 2");
  }

  @Test
  void testBRNWithErrors() throws IOException, AeriusException {
    final ImportParcel result = new ImportParcel();
    try (InputStream inputStream = getClass().getResourceAsStream(FILENAME_WITH_ERROR)) {
      ir.read(FILENAME_WITH_ERROR, inputStream, categories, Substance.NH3, result);
    }
    assertEquals(6, result.getSituation().getEmissionSourcesList().size(), "Should have read # rows correctly");
    assertEquals(1, result.getExceptions().size(), "Should have read # errors");
  }
}
