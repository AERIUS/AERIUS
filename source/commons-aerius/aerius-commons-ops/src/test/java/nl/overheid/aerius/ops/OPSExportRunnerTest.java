/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops;

import static nl.overheid.aerius.ops.util.OPSSplitProvidedUtil.DISTANCE_BETWEEN;
import static nl.overheid.aerius.ops.util.OPSSplitProvidedUtil.DISTANCE_LARGE;
import static nl.overheid.aerius.ops.util.OPSSplitProvidedUtil.DISTANCE_SMALL;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.calculation.EngineInputData.CommandType;
import nl.overheid.aerius.calculation.EngineInputData.SubReceptorCalculation;
import nl.overheid.aerius.http.FileServerProxy;
import nl.overheid.aerius.http.HttpException;
import nl.overheid.aerius.io.AbstractLineReader;
import nl.overheid.aerius.ops.background.OPSData;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.io.BrnFileReader;
import nl.overheid.aerius.ops.util.OPSSplitProvidedUtil;
import nl.overheid.aerius.ops.util.OPSTestUtil;
import nl.overheid.aerius.ops.version.OPSVersion;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.OPSOptions;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.ops.OPSSubReceptor;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.ExportResult;

/**
 * Test class for {@link OPSExportRunner}.
 */
@ExtendWith(MockitoExtension.class)
class OPSExportRunnerTest extends OPSRunnerTestBase {

  private static final String FILE_SERVER_EXTERNAL_URL_BASEEXTERNAL = "http://localhost/external";
  private static final String FILE_CODE = "1234";

  private @Captor ArgumentCaptor<File> zipfileCapture;
  private @Mock FileServerProxy fileServerProxy;
  private @TempDir Path tempDir;

  @ParameterizedTest
  @CsvSource({
      "LATEST,PROVIDED,8", // Latest is 4 sectors * 2 substances
      "STABLE,PROVIDED,8", // Stable is 4 sectors * 2 substances
  })
  void testExportOPS(final OPSVersion opsVersion, final SubReceptorCalculation subReceptorCalculation, final int expectedCount) throws Exception {
    config.setKeepGeneratedFiles(true);
    final OPSExportRunner exporter = createOPSExportRunner(fileServerProxy);
    final OPSInputData input = OPSTestUtil.getInputTestData(CommandType.EXPORT, EmissionResultKey.NOX_DEPOSITION, YEAR, OPSOptions.Chemistry.ACTUAL,
        opsVersion);

    // Add emissions for NH3 to sources to generated both runs for NOx and NH3
    input.setSubReceptorCalculation(subReceptorCalculation);
    input.setSubstances(Substance.NOXNH3.hatch());
    input.getEmissionSources().forEach((k, v) -> v.stream().forEach(s -> s.setEmission(Substance.NH3, 20.0)));
    addClosePoint(input);

    final ExportResult result = exporter.run(input, FILE_CODE);
    assertZip(expectedCount, result);
  }

  @ParameterizedTest
  @MethodSource("testSplitProvidedData")
  void testSplitProvided(final boolean subreceptors, final int splitDistance, final int expectedFilesInZip, final int numberOfSources,
      final String receptorFilePattern) throws Exception {
    config.setKeepGeneratedFiles(true);
    final OPSInputData input = OPSSplitProvidedUtil.createInputData(YEAR, "emissions");
    input.setSplitDistance(splitDistance);
    final OPSExportRunner exporter = createOPSExportRunner(fileServerProxy);
    if (!subreceptors) {
      OPSSplitProvidedUtil.replaceReceptorsOnly(input.getReceptors());
    }
    final ExportResult result = exporter.run(input, FILE_CODE);

    assertZip(expectedFilesInZip, result);
    Files.walk(tempDir)
        .filter(p -> p.toFile().isDirectory() && !p.equals(tempDir))
        .map(Path::toFile)
        .forEach(f -> assertExportedFiles(f, numberOfSources, receptorFilePattern));
  }

  @Test
  void testExportOPSWithoutKeepingGeneratedFiles() throws Exception {
    config.setKeepGeneratedFiles(false);
    final OPSExportRunner exporter = createOPSExportRunner(fileServerProxy);
    final OPSInputData input = createOPSInputData();

    assertExportRun(exporter, input);
  }

  @Test
  void testExportOPSRawInput() throws Exception {
    final OPSExportRunner exporter = createOPSExportRunner(fileServerProxy);
    final OPSInputData input = createOPSInputData();
    final OPSOptions optOptions = new OPSOptions();

    optOptions.setRawInput(true);
    input.setOpsOptions(optOptions);

    assertExportRun(exporter, input);
  }

  private OPSInputData createOPSInputData() throws IOException {
    final OPSInputData input = OPSTestUtil.getInputTestData(CommandType.EXPORT, EmissionResultKey.NOX_DEPOSITION, YEAR, OPSOptions.Chemistry.ACTUAL,
        OPSVersion.LATEST);

    // Add emissions for NH3 to sources to generated both runs for NOx and NH3
    input.setSubReceptorCalculation(SubReceptorCalculation.PROVIDED);
    input.setSubstances(Substance.NOXNH3.hatch());
    input.getEmissionSources().forEach((k, v) -> v.stream().forEach(s -> s.setEmission(Substance.NH3, 20.0)));
    addClosePoint(input);
    return input;
  }

  private void assertExportRun(final OPSExportRunner exporter, final OPSInputData input) throws Exception {
    final ExportResult result = exporter.run(input, FILE_CODE);
    assertEquals(FILE_SERVER_EXTERNAL_URL_BASEEXTERNAL, result.getFileUrl(), "Should have return external expected url");

    assertFalse(zipfileCapture.getValue().exists(), "File should have been removed at this point");
  }

  private static List<Arguments> testSplitProvidedData() {
    return List.of(
        Arguments.of(true, DISTANCE_LARGE, 4, 2, "with_sub"),
        Arguments.of(false, DISTANCE_LARGE, 4, 2, "without_sub"),
        Arguments.of(true, DISTANCE_SMALL, 4, 2, "plain_"),
        Arguments.of(false, DISTANCE_SMALL, 4, 2, "plain_"),
        Arguments.of(true, DISTANCE_BETWEEN, 8, 1, null), // split calculation, each should contain 1 source
        Arguments.of(false, DISTANCE_BETWEEN, 4, 2, "plain_"));
  }

  private void assertZip(final int expectedCount, final ExportResult result) throws IOException, HttpException, URISyntaxException {
    verify(fileServerProxy).processFile(eq(FILE_CODE), any(), any(File.class), any());
    final int count = zipfileCapture.getAllValues().stream().mapToInt(f -> countZipContent(tempDir.toFile().getAbsolutePath(), f)).sum();

    assertEquals(FILE_SERVER_EXTERNAL_URL_BASEEXTERNAL, result.getFileUrl(), "Should have return external expected url");
    assertEquals(expectedCount, count, "Should have expected number of files in the zip");
  }

  private void assertExportedFiles(final File directory, final int numberOfSources, final String receptorFilePattern) {
    try {
      final List<OPSSource> emissions = readFile(new BrnFileReader(Substance.NOX), new File(directory, "emission.brn"));
      assertEquals(numberOfSources, emissions.size(), "Should only contain " + numberOfSources + " source(s)");

      final String prefix = receptorFilePattern == null ? (emissions.get(0).getPoint().getY() == 461826 ? "with_sub" : "plain_")
          : receptorFilePattern;
      final String withSubreceptors = prefix + "receptors.rcp";

      final List<String> expectedReceptors = sortedLines(OPSTestUtil.getFile("subreceptors/split_provided/export/" + withSubreceptors));
      final List<String> exportedReceptors = sortedLines(new File(directory, "receptors.rcp"));

      assertArrayEquals(expectedReceptors.toArray(new String[0]), exportedReceptors.toArray(new String[0]),
          "Should have same content in exported receptor file");
    } catch (final IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  private static List<String> sortedLines(final File file) throws IOException {
    return Files.readAllLines(file.toPath()).stream().sorted().toList();
  }

  private static <L> List<L> readFile(final AbstractLineReader<L> reader, final File file) throws IOException {
    try (InputStream inputStream = new FileInputStream(file)) {
      return reader.readObjects(inputStream).getObjects();
    }
  }

  private OPSExportRunner createOPSExportRunner(final FileServerProxy fileServerProxy) throws Exception {
    doReturn(FILE_SERVER_EXTERNAL_URL_BASEEXTERNAL).when(fileServerProxy).processFile(eq(FILE_CODE), any(), zipfileCapture.capture(), any());

    return new OPSExportRunner(config, fileServerProxy, new OPSData());
  }

  /**
   *Add receptor point close to the first source to trigger generating subreceptor generation code
   */
  private static void addClosePoint(final OPSInputData input) {
    if (input.getSubReceptorCalculation() == SubReceptorCalculation.PROVIDED) {
      input.getReceptors().add(new OPSSubReceptor(new AeriusPoint(1000, AeriusPointType.RECEPTOR), AeriusPointType.RECEPTOR, 0));
      final OPSSubReceptor opsSubReceptor = new OPSSubReceptor(new AeriusPoint(1001, AeriusPointType.RECEPTOR), AeriusPointType.SUB_RECEPTOR, 0);
      opsSubReceptor.setId(1000);
      input.getReceptors().add(opsSubReceptor);
    }
  }

  private static int countZipContent(final String tempDir, final File zipFile) {
    int count = 0;
    System.out.println("tempDir:" + tempDir);
    System.out.println("zipFile:" + zipFile);
    try (final ZipInputStream zipStream = new ZipInputStream(new FileInputStream(zipFile))) {
      ZipEntry nextEntry;
      while ((nextEntry = zipStream.getNextEntry()) != null) {
        final Path file = Path.of(tempDir, nextEntry.getName());

        file.toFile().getParentFile().mkdir();
        Files.copy(zipStream, file);
        count++;
      }
    } catch (final IOException e) {
      throw new UncheckedIOException(e);
    }
    return count;
  }
}
