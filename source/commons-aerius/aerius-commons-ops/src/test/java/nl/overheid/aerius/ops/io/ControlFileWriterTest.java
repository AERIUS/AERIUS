/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.CALLS_REAL_METHODS;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.withSettings;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import nl.overheid.aerius.calculation.EngineInputData.CommandType;
import nl.overheid.aerius.ops.domain.OPSConfiguration;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.util.OPSTestUtil;
import nl.overheid.aerius.ops.version.OPSVersion;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.OPSOptions;
import nl.overheid.aerius.shared.domain.meteo.Meteo;
import nl.overheid.aerius.util.OSUtils;

/**
 * Test class for {@link ControlFileWriter}.
 */
class ControlFileWriterTest {

  private static final String SETTINGS10 = "settings_10_latest.ctr";
  private static final String LINUX_SETTINGS10 = "linux_settings_10_latest.ctr";
  private static final String RUN_ID10 = "RAY310";

  private static final String SETTINGS11 = "settings_11_latest.ctr";
  private static final String LINUX_SETTINGS11 = "linux_settings_11_latest.ctr";
  private static final String RUN_ID11 = "AB305";

  private static final String SETTINGS17 = "settings_17_latest.ctr";
  private static final String LINUX_SETTINGS17 = "linux_settings_17_latest.ctr";
  private static final String RUN_ID17 = "14AK31";

  private static final String SETTINGS11_CUSTOM_ROADS = "settings_11_custom_roads_latest.ctr";
  private static final String LINUX_SETTINGS11_CUSTOM_ROADS = "linux_settings_11_custom_roads_latest.ctr";

  private static final String SETTINGS17_FUTURE = "settings_17_future_latest.ctr";
  private static final String LINUX_SETTINGS17_FUTURE = "linux_settings_17_future_latest.ctr";

  private static final String SETTINGS17_CUSTOM_METEO = "settings_17_custom_meteo_latest.ctr";
  private static final String LINUX_SETTINGS17_CUSTOM_METEO = "linux_settings_17_custom_meteo_latest.ctr";
  private static final String CUSTOM_METEO_FILE = "the_meteo_file";

  private static final String SETTINGS17_OPS_OPTIONS = "settings_17_ops_options_latest.ctr";
  private static final String LINUX_SETTINGS17_OPS_OPTIONS = "linux_settings_17_ops_options_latest.ctr";

  private static final String SETTINGS17_CUSTOM_CHEMISTRY = "settings_17_custom_chemistry_latest.ctr";
  private static final String LINUX_SETTINGS17_CUSTOM_CHEMISTRY = "linux_settings_17_custom_chemistry_latest.ctr";

  private static final String SETTINGS17_STABLE = "settings_17_stable.ctr";
  private static final String LINUX_SETTINGS17_STABLE = "linux_settings_17_stable.ctr";

  private static final File OPS_ROOT = new File(OSUtils.isWindows() ? "C:\\OPS\\" : "/opt/aerius/ops/");
  private static final File OPS_RUN = new File(OSUtils.isWindows() ? "C:\\OPS\\run" : "//tmp/ops/");

  private final int YEAR = 2010;
  private final OPSConfiguration cfo = mock(OPSConfiguration.class, withSettings().useConstructor(1).defaultAnswer(CALLS_REAL_METHODS));
  private OPSOptions opsOptions = null;
  private Set<Integer> roadsCats;

  @TempDir
  File tempDir;

  @BeforeEach
  void resetSettings() {
    doReturn(OPS_ROOT).when(cfo).getVersionsRoot();
    doReturn(new File(OPS_ROOT, "LATEST")).when(cfo).getOpsRoot(OPSVersion.LATEST);
    doReturn(new File(OPS_ROOT, "STABLE")).when(cfo).getOpsRoot(OPSVersion.STABLE);
    roadsCats = Set.of(3100);
  }

  @Test
  void testWriteFile10() throws IOException {
    assertControlFile(RUN_ID10, Substance.PM10, YEAR, SETTINGS10, LINUX_SETTINGS10, OPSVersion.LATEST);
  }

  @Test
  void testWriteFile11() throws IOException {
    assertControlFile(RUN_ID11, Substance.NOX, YEAR, SETTINGS11, LINUX_SETTINGS11, OPSVersion.LATEST);
  }

  @Test
  void testWriteFile17() throws IOException {
    assertControlFile(RUN_ID17, Substance.NH3, YEAR, SETTINGS17, LINUX_SETTINGS17, OPSVersion.LATEST);
  }

  @Test
  void testWriteCustomMeteo() throws IOException {
    doReturn(CUSTOM_METEO_FILE).when(cfo).getSettingsMeteoFile();

    assertControlFile(RUN_ID17, Substance.NH3, YEAR, SETTINGS17_CUSTOM_METEO, LINUX_SETTINGS17_CUSTOM_METEO, OPSVersion.LATEST);
  }

  @Test
  void testWriteEmptyOpsOptions() throws IOException {
    opsOptions = new OPSOptions();
    assertControlFile(RUN_ID17, Substance.NH3, YEAR, SETTINGS17, LINUX_SETTINGS17, OPSVersion.LATEST);
  }

  @Test
  void testWriteCustomOpsOptions() throws IOException {
    opsOptions = new OPSOptions();
    opsOptions.setYear(1994);
    opsOptions.setCompCode(2);
    opsOptions.setMolWeight(3.3);
    opsOptions.setPhase(4);
    opsOptions.setLoss(9);
    opsOptions.setDiffCoeff("5.2");
    opsOptions.setWashout("7");
    opsOptions.setConvRate("Some conv rate");
    opsOptions.setRoads("1 2 3");
    opsOptions.setRoughness(234.2);
    assertControlFile(RUN_ID17, Substance.NH3, YEAR, SETTINGS17_OPS_OPTIONS, LINUX_SETTINGS17_OPS_OPTIONS, OPSVersion.LATEST);
  }

  @Test
  void testWriteCustomChemistry() throws IOException {
    opsOptions = new OPSOptions();
    opsOptions.setChemistry(OPSOptions.Chemistry.PROGNOSIS);
    opsOptions.setYear(2025);

    assertControlFile(RUN_ID17, Substance.NH3, YEAR, SETTINGS17_CUSTOM_CHEMISTRY, LINUX_SETTINGS17_CUSTOM_CHEMISTRY, OPSVersion.LATEST);
  }

  @Test
  void testWriteChemistryNull() throws IOException {
    opsOptions = new OPSOptions();
    opsOptions.setChemistry(null);

    assertControlFile(RUN_ID17, Substance.NH3, YEAR, SETTINGS17, LINUX_SETTINGS17, OPSVersion.LATEST);
  }

  @Test
  void testToOpsMeteoFile() throws IOException {
    assertNull(ControlFileWriter.toOpsMeteoFile(null), "Absent meteo");
    assertEquals("a005105c.*", ControlFileWriter.toOpsMeteoFile(new Meteo(2005)), "Single year meteo");
    assertEquals("m005114c.*", ControlFileWriter.toOpsMeteoFile(new Meteo(2005, 2014)), "Multi year meteo");
  }

  @Test
  void testStableControlFile() throws IOException {
    opsOptions = new OPSOptions();
    opsOptions.setChemistry(OPSOptions.Chemistry.PROGNOSIS);

    assertControlFile(RUN_ID17, Substance.NH3, YEAR, SETTINGS17_STABLE, LINUX_SETTINGS17_STABLE, OPSVersion.STABLE);
  }

  @Test
  void testCustomRoads() throws IOException {
    roadsCats = Set.of(1, 2, 3);
    assertControlFile(RUN_ID11, Substance.NOX, YEAR, SETTINGS11_CUSTOM_ROADS, LINUX_SETTINGS11_CUSTOM_ROADS, OPSVersion.LATEST);
  }

  @Test
  void testFutureYear() throws IOException {
    assertControlFile(RUN_ID17, Substance.NH3, 2025, SETTINGS17_FUTURE, LINUX_SETTINGS17_FUTURE, OPSVersion.LATEST);
  }

  private void assertControlFile(final String runId, final Substance substance, final int year, final String refFileWin, final String refFileLinux,
      final OPSVersion opsVersion) throws IOException {
    final OPSInputData input = new OPSInputData(opsVersion, CommandType.CALCULATE, OPSTestUtil.ZOOM_LEVEL_1_SURFACE);
    input.setYear(year);
    input.setOpsOptions(opsOptions);
    final ControlFileOptions options = new ControlFileOptions(input, substance, false, null, roadsCats);
    ControlFileWriter.writeFile(cfo, tempDir, runId, options, null);
    //If linux, we can't really compare these files: either we make them equal and OPS won't work properly
    //or we make OPS work and the test will fail (reference doesn't match the output).
    final String referenceFile = OSUtils.isWindows() ? refFileWin : refFileLinux;

    final String output = getOutputFile(tempDir, runId);
    final String reference = OPSTestUtil.readFile("io/" + referenceFile);

    assertEquals(reference, output, "WriteFile Test Substance " + substance);
  }

  private String getOutputFile(final File path, final String runId) throws IOException {
    final String content = OPSTestUtil.readFile(new File(path, ControlFileWriter.getControlFileName()));
    return content.replace(path.toString(), OPS_RUN + File.separator + runId);
  }
}
