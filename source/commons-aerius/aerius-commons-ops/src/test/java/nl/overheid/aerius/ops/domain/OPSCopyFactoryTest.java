/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.List;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.ops.conversion.OPSBuildingTracker;
import nl.overheid.aerius.ops.domain.OPSSource.OPSSourceOutflowDirection;
import nl.overheid.aerius.ops.io.BrnConstants;
import nl.overheid.aerius.shared.domain.v2.building.Building;
import nl.overheid.aerius.shared.domain.v2.building.BuildingFeature;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.geo.EPSG;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Test class for {@link OPSCopyFactory}
 */
class OPSCopyFactoryTest {

  private static final int TEST_DVS_ID = 1;
  private static final String TEST_BUILDING_ID = "Some weird building ID";

  @Test
  void testFullCircleWithBuilding() throws AeriusException {
    final OPSSource originalSource = createOPSSourceWithBuilding();
    final OPSSource resultSource = performFullCircle(originalSource);
    assertEquals(originalSource, resultSource, "Original OPSSource Object and result should be equal.");
    // Checking the above assert not just succeeding because of lack of building included in the equals method
    assertNotEquals(createOPSSourceWithoutBuilding(), createOPSSourceWithBuilding(), "OPSSource with and without building should not be equal.");
  }

  @Test
  void testFullCircleWithoutBuilding() throws AeriusException {
    final OPSSource originalSource = createOPSSourceWithoutBuilding();
    final OPSSource resultSource = performFullCircle(originalSource);
    assertEquals(originalSource, resultSource, "Original OPSSource Object and result should be equal.");
  }

  private OPSSource performFullCircle(final OPSSource originalSource) throws AeriusException {
    final BuildingFeature buildingFeature = new BuildingFeature();
    final Point point = new Point(originalSource.getPoint().getX(), originalSource.getPoint().getY());
    final OPSSourceCharacteristics characteristics = OPSCopyFactory.toSourceCharacteristics(originalSource,
        (length, width, height, orientation) -> {
          buildingFeature.setGeometry(GeometryUtil.constructPolygonFromDimensions(point, length, width, orientation));
          final Building building = new Building();
          building.setGmlId(TEST_BUILDING_ID);
          building.setHeight(height);
          buildingFeature.setProperties(building);
          return TEST_BUILDING_ID;
        });
    List<BuildingFeature> buildings;
    if (buildingFeature.getGeometry() == null) {
      assertNull(characteristics.getBuildingId(), "Returned building ID");
      buildings = List.of();
    } else {
      assertEquals(TEST_BUILDING_ID, characteristics.getBuildingId(), "Returned building ID");
      buildings = List.of(buildingFeature);
    }
    final OPSBuildingTracker buildingTracker = new OPSBuildingTracker(buildings);
    final OPSSource resultSource = createAndPrepareResultOPSSource(originalSource);
    OPSCopyFactory.toOpsSource(characteristics, resultSource, buildingTracker);
    return resultSource;
  }

  /**
   * Creates the OPSSource and transfers the fields the OPSCopyFactory does not transfer full circle.
   * @param originalSource
   * @return
   */
  private OPSSource createAndPrepareResultOPSSource(final OPSSource originalSource) {
    final OPSSource resultSource = new OPSSource();
    resultSource.setId(originalSource.getId());
    resultSource.getPoint().setSrid(originalSource.getPoint().getSrid());
    resultSource.getPoint().setX(originalSource.getPoint().getX());
    resultSource.getPoint().setY(originalSource.getPoint().getY());
    return resultSource;
  }

  private OPSSource createOPSSourceWithBuilding() {
    final OPSSource src = createBaselineOPSSource();
    src.setBuildingWidth(10);
    src.setBuildingLength(20);
    src.setBuildingHeight(5);
    src.setBuildingOrientation(36);
    return src;
  }

  private OPSSource createOPSSourceWithoutBuilding() {
    final OPSSource src = createBaselineOPSSource();
    src.setBuildingWidth(BrnConstants.BUILDING_WIDTH_NOT_APPLICABLE);
    src.setBuildingLength(BrnConstants.BUILDING_LENGTH_NOT_APPLICABLE);
    src.setBuildingHeight(BrnConstants.BUILDING_HEIGHT_NOT_APPLICABLE);
    src.setBuildingOrientation(BrnConstants.BUILDING_ORIENTATION_NOT_APPLICABLE);
    return src;
  }

  private OPSSource createBaselineOPSSource() {
    final OPSSource src = new OPSSource(3265, EPSG.RDNEW.getSrid(), 195500.0, 431500.0);
    src.setHeatContent(0.0);
    src.setEmissionHeight(1.0);
    src.setDiameter(1000);
    src.setSpread(0.5);
    src.setDiurnalVariation(TEST_DVS_ID);
    src.setParticleSizeDistribution(0);
    src.setEmissionTemperature(18.5);
    src.setOutflowDiameter(2.0);
    src.setOutflowVelocity(5);
    src.setOutflowDirection(OPSSourceOutflowDirection.VERTICAL);
    src.setCat(1);
    src.setArea(580);
    src.setComp("Commentaar");
    return src;
  }

}
