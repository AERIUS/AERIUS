/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.util;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import nl.overheid.aerius.calculation.EngineInputData.CommandType;
import nl.overheid.aerius.calculation.EngineInputData.SubReceptorCalculation;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.version.OPSVersion;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.OPSOptions;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.ops.OPSSubReceptor;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Utility class to create input for doing SPLIT_PROVIDED calculations.
 */
public class OPSSplitProvidedUtil {

  public static final int RECEPTOR_FAR_WAY_ID = 4697681;
  public static final int RECEPTOR_CLOSE_BY_ID = 4697689;
  public static final int DISTANCE_LARGE = 100_000;
  public static final int DISTANCE_SMALL = 1;
  public static final int DISTANCE_BETWEEN = 300;

  private static final OPSSubReceptor RECEPTOR_CLOSE_BY = create(RECEPTOR_CLOSE_BY_ID, 0, AeriusPointType.RECEPTOR, 115277.0, 461854.0, 0);
  private static final OPSSubReceptor RECEPTOR_CLOSE_BY_0 = create(0, RECEPTOR_CLOSE_BY_ID, AeriusPointType.SUB_RECEPTOR, 115277.0, 461854.0, 0);
  private static final OPSSubReceptor RECEPTOR_CLOSE_BY_1 = create(1, RECEPTOR_CLOSE_BY_ID, AeriusPointType.SUB_RECEPTOR, 115218.0, 461856.0, 3);
  private static final OPSSubReceptor RECEPTOR_CLOSE_BY_2 = create(2, RECEPTOR_CLOSE_BY_ID, AeriusPointType.SUB_RECEPTOR, 115222.0, 461858.0, 3);
  private static final OPSSubReceptor RECEPTOR_FAR_WAY = create(RECEPTOR_FAR_WAY_ID, 0, AeriusPointType.RECEPTOR, 113788.0, 461854.0, 0);
  private static final OPSSubReceptor RECEPTOR_FAR_WAY_0 = create(0, RECEPTOR_FAR_WAY_ID, AeriusPointType.SUB_RECEPTOR, 113788.0, 461854.0, 0);

  public static OPSInputData createInputData(final int year, final String brn) throws IOException {
    final OPSInputData input = OPSTestUtil.getInputTestData(CommandType.CALCULATE, EmissionResultKey.NOX_DEPOSITION, year,
        OPSOptions.Chemistry.ACTUAL, OPSVersion.LATEST);
    final String emissionFile = "subreceptors/split_provided/" + brn + ".brn";

    input.setEmissionSources(1, OPSTestUtil.readBrnFile(Substance.NOX, emissionFile).getObjects());
    replaceReceptorsWithSubreceptors(input.getReceptors());
    input.setSubReceptorCalculation(SubReceptorCalculation.SPLIT_PROVIDED);
    input.setSplitDistance(300);

    return input;
  }

  /**
   * Replace the receptors with 2 receptors, 1 with no subreceptors (only level) and a 1 with 2 sub receptors.
   */
  private static void replaceReceptorsWithSubreceptors(final Collection<OPSReceptor> receptors) {
    receptors.clear();
    receptors.addAll(List.of(RECEPTOR_CLOSE_BY, RECEPTOR_CLOSE_BY_1, RECEPTOR_CLOSE_BY_2, RECEPTOR_FAR_WAY, RECEPTOR_FAR_WAY_0));
  }

  /**
   * Replaces the receptors with only a receptor, but no subreceptors. But receptor are subreceptor classes.
   */
  public static void replaceReceptorsOnly(final Collection<OPSReceptor> receptors) {
    receptors.clear();
    receptors.addAll(List.of(RECEPTOR_CLOSE_BY, RECEPTOR_CLOSE_BY_0, RECEPTOR_FAR_WAY, RECEPTOR_FAR_WAY_0));
  }

  /**
   * Replaces the receptors with only a single receptor, but no subreceptors. But receptor are subreceptor classes.
   */
  public static void replaceReceptorFarAwayOnly(final Collection<OPSReceptor> receptors) {
    receptors.clear();
    receptors.addAll(List.of(RECEPTOR_FAR_WAY, RECEPTOR_FAR_WAY_0));
  }

  private static OPSSubReceptor create(final int id, final int parentId, final AeriusPointType pointType, final double x,
      final double y, final int level) {
    return new OPSSubReceptor(new AeriusPoint(id, parentId, pointType, x, y), pointType, level);
  }
}
