/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.conversion;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.ops.domain.OPSCopyFactory;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.OrientedEnvelope;
import nl.overheid.aerius.shared.domain.ops.DiurnalVariation;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.building.Building;
import nl.overheid.aerius.shared.domain.v2.building.BuildingFeature;
import nl.overheid.aerius.shared.domain.v2.characteristics.HeatContentType;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.geojson.Crs;
import nl.overheid.aerius.shared.domain.v2.geojson.Crs.CrsContent;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.geo.EPSG;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Test class for {@link EmissionSourceOPSConverter}.
 */
class EmissionSourceOPSConverterTest {

  private static final int SOURCE_ID = 12;
  private static final int SOURCE_X = 101;
  private static final int SOURCE_Y = 201;
  private static final double SOURCE_X_WGS84 = 5.9375;
  private static final double SOURCE_Y_WGS84 = 50.59375;
  private static final String SOURCE_LABEL = "SomeLabel";
  private static final int SECTOR_ID = 323;
  private static final double EMISSION = 231.234;
  private static final OPSSourceCharacteristics SOURCE_CHARACTERISTICS = defaultCharacteristics();

  @Test
  void testConvertEmissionSource() throws AeriusException {
    final GenericEmissionSource source = createGenericEmissionSource();
    final Point point = new Point(SOURCE_X, SOURCE_Y);

    final OPSBuildingTracker buildingTracker = new OPSBuildingTracker(List.of());

    final EmissionSourceOPSConverter converter = new EmissionSourceOPSConverter();
    final OPSSource opsSource = converter.convert(source, point, source.getEmissions(), SOURCE_ID, List.of(Substance.NOX), buildingTracker, null)
        .get(0);
    assertEquals(SOURCE_ID, opsSource.getId(), "ID");
    assertEquals(SOURCE_X, opsSource.getPoint().getX(), 0.00001, "X coord");
    assertEquals(SOURCE_Y, opsSource.getPoint().getY(), 0.00001, "Y coord");
    assertEquals(EPSG.RDNEW.getSrid(), opsSource.getPoint().getSrid(), "SRID");
    assertEquals(EMISSION, opsSource.getEmission(Substance.NOX), 0.00001, "Emission");
    validateCharacteristics(SOURCE_CHARACTERISTICS, OPSCopyFactory.toSourceCharacteristics(opsSource, null));
    assertEquals(1, opsSource.getArea(), "Area");
    assertEquals("", opsSource.getComp(), "Comp");
  }

  @Test
  void testConvertEmissionSourceWithBuilding() throws AeriusException {
    final GenericEmissionSource source = createGenericEmissionSource();
    final OPSSourceCharacteristics sourceCharacteristics = new OPSSourceCharacteristics();
    sourceCharacteristics.setBuildingId(SOURCE_LABEL);
    source.setCharacteristics(sourceCharacteristics);
    final Point point = new Point(SOURCE_X, SOURCE_Y);

    final BuildingFeature buildingFeature = new BuildingFeature();
    final Building building = new Building();
    building.setGmlId(SOURCE_LABEL);
    buildingFeature.setProperties(building);
    final Polygon polygon = new Polygon();
    polygon.setCoordinates(new double[][][] {{{0.0, 0.0}, {5.0, 0.0}, {0.0, 10.0}, {5.0, 10.0}, {0.0, 0.0}}});
    buildingFeature.setGeometry(polygon);
    final OPSBuildingTracker buildingTracker = new OPSBuildingTracker(List.of(buildingFeature));

    final EmissionSourceOPSConverter converter = new EmissionSourceOPSConverter();
    final OPSSource opsSource = converter.convert(source, point, source.getEmissions(), SOURCE_ID, List.of(Substance.NOX), buildingTracker, null)
        .get(0);
    assertEquals(5, opsSource.getBuildingWidth(), 1E-5, "Building Width");
    assertEquals(10, opsSource.getBuildingLength(), 1E-5, "Building Length");
    assertEquals(90, opsSource.getBuildingOrientation(), 1E-5, "Building Orientation");
  }

  @Test
  void testConvertEmissionSourceDiurnalCorrection() throws AeriusException {
    final GenericEmissionSource source = createGenericEmissionSource();
    source.getEmissions().put(Substance.NH3, EMISSION);
    source.setCharacteristics(SOURCE_CHARACTERISTICS.copyTo(new OPSSourceCharacteristics()));
    final Point point = new Point(SOURCE_X, SOURCE_Y);

    final OPSBuildingTracker buildingTracker = new OPSBuildingTracker(List.of());

    final EmissionSourceOPSConverter converter = new EmissionSourceOPSConverter();
    ((OPSSourceCharacteristics) source.getCharacteristics()).setDiurnalVariation(DiurnalVariation.TRAFFIC);
    assertEquals(3, converter.convert(source, point, source.getEmissions(), SOURCE_ID, List.of(Substance.NOX), buildingTracker, null).get(0)
        .getDiurnalVariation(),
        "Diurnal variation to be the same");

    ((OPSSourceCharacteristics) source.getCharacteristics()).setDiurnalVariation(DiurnalVariation.ANIMAL_HOUSING);
    assertEquals(4, converter.convert(source, point, source.getEmissions(), SOURCE_ID, List.of(Substance.NOX), buildingTracker, null).get(0)
        .getDiurnalVariation(),
        "Same diurnal variation for 4 and NOx");
    ((OPSSourceCharacteristics) source.getCharacteristics()).setDiurnalVariation(DiurnalVariation.ANIMAL_HOUSING);
    assertEquals(4, converter.convert(source, point, source.getEmissions(), SOURCE_ID, List.of(Substance.NH3), buildingTracker, null).get(0)
        .getDiurnalVariation(),
        "Same diurnal variation for 4 and NH3");

    ((OPSSourceCharacteristics) source.getCharacteristics()).setDiurnalVariation(DiurnalVariation.FERTILISER);
    assertEquals(5, converter.convert(source, point, source.getEmissions(), SOURCE_ID, List.of(Substance.NOX), buildingTracker, null).get(0)
        .getDiurnalVariation(),
        "Same diurnal variation for 5 and NOx");
    ((OPSSourceCharacteristics) source.getCharacteristics()).setDiurnalVariation(DiurnalVariation.FERTILISER);
    assertEquals(5, converter.convert(source, point, source.getEmissions(), SOURCE_ID, List.of(Substance.NH3), buildingTracker, null).get(0)
        .getDiurnalVariation(),
        "Same diurnal variation for 5 and NH3");
  }

  @Test
  void testConvertEmissionSourceWgs84() throws AeriusException {
    final GenericEmissionSource source = createGenericEmissionSource();
    final Point point = new Point(SOURCE_X_WGS84, SOURCE_Y_WGS84);

    final ScenarioSituation situation = new ScenarioSituation();
    setSrc(situation, EPSG.WGS84);

    final OPSBuildingTracker buildingTracker = new OPSBuildingTracker(List.of());

    final EmissionSourceOPSConverter converter = new EmissionSourceOPSConverter();
    final OPSSource opsSource = converter.convert(source, point, source.getEmissions(), SOURCE_ID, List.of(Substance.NOX), buildingTracker,
        situation).get(0);
    assertEquals(SOURCE_ID, opsSource.getId(), "ID");
    assertEquals(SOURCE_X_WGS84, opsSource.getPoint().getX(), 0.00001, "X coord");
    assertEquals(SOURCE_Y_WGS84, opsSource.getPoint().getY(), 0.00001, "Y coord");
    assertEquals(EPSG.WGS84.getSrid(), opsSource.getPoint().getSrid(), "SRID");
  }

  @ParameterizedTest
  @MethodSource("geometryValidation")
  void testGeometryValidation(final int x, final int y, final EPSG epsg, final boolean throwException) throws AeriusException {
    final GenericEmissionSource source = createGenericEmissionSource();
    final Point point = new Point(x, y);

    final ScenarioSituation situation = new ScenarioSituation();
    setSrc(situation, epsg);

    final OPSBuildingTracker buildingTracker = new OPSBuildingTracker(List.of());
    final EmissionSourceOPSConverter converter = new EmissionSourceOPSConverter();
    final Executable function = () -> converter
        .convert(source, point, source.getEmissions(), SOURCE_ID, List.of(Substance.NOX), buildingTracker, situation).get(0);

    if (throwException) {
      assertThrows(AeriusException.class, function, "Should throw an exception");
    } else {
      assertDoesNotThrow(function, "Should not throw an exception");
    }
  }

  private static List<Arguments> geometryValidation() {
    return List.of(
        Arguments.of(1, 1, EPSG.WGS84, false),
        Arguments.of(1, 90, EPSG.RDNEW, false),
        Arguments.of(1, 1, EPSG.RDNEW, true));
  }

  private static void setSrc(final ScenarioSituation situation, final EPSG epsg) {
    final Crs crs = new Crs();
    final CrsContent crsContent = new CrsContent();
    crsContent.setName(epsg.getEpsgCode());
    crs.setProperties(crsContent);
    situation.getSources().setCrs(crs);
  }

  @Test
  void testConvertOPSSourceBuilding() throws AeriusException {
    final SectorCategories categories = new SectorCategories();
    final Substance substance = Substance.NOX;
    final OPSSource opsSource = getStandardOPSSource(substance);
    opsSource.setBuildingWidth(5.0);
    opsSource.setBuildingLength(10.0);
    opsSource.setBuildingOrientation(90);
    opsSource.setBuildingHeight(1.0);
    final BuildingFeature buildingFeature = EmissionSourceOPSConverter.convert(opsSource, Substance.NOX, categories, true).getBuilding();
    final OrientedEnvelope envelop = GeometryUtil.determineOrientedEnvelope((Polygon) buildingFeature.getGeometry());
    assertEquals(5.0, envelop.getWidth(), 1E-5, "Width should be the same as ops source");
    assertEquals(10.0, envelop.getLength(), 1E-5, "Height should be the same as ops source");
    assertEquals(0.0, envelop.getOrientation(), 1E-5, "Orientation should be North clockwise");
  }

  @Test
  void testConvertOPSSourceSubstanceNullSectors() throws AeriusException {
    final SectorCategories categories = new SectorCategories();
    final Substance substance = Substance.NOX;
    final OPSSource opsSource = getStandardOPSSource(substance);
    final EmissionSourceFeature sourceFeature = EmissionSourceOPSConverter.convert(opsSource, Substance.NOX, categories, true).getSource();
    final Sector sectorUndefined = Sector.SECTOR_DEFAULT;

    assertEquals("12", sourceFeature.getId(), "ID");
    assertTrue(sourceFeature.getGeometry() instanceof Point, "Should be a point");
    final Point point = (Point) sourceFeature.getGeometry();
    assertEquals(SOURCE_X, point.getX(), 0.00001, "X coord");
    assertEquals(SOURCE_Y, point.getY(), 0.00001, "Y coord");

    final EmissionSource source = sourceFeature.getProperties();
    assertEquals("12", source.getGmlId(), "GML ID");
    assertEquals(EMISSION, source.getEmissions().get(substance), 0.00001, "Emission");
    assertTrue(source.getCharacteristics() instanceof OPSSourceCharacteristics, "Characteristics should be OPS");
    validateCharacteristics(SOURCE_CHARACTERISTICS, (OPSSourceCharacteristics) source.getCharacteristics());
    assertEquals(sectorUndefined.getSectorId(), source.getSectorId(), "Category number");
    assertEquals(opsSource.getComp() + EmissionSourceOPSConverter.OPS_ID_PREFIX + opsSource.getId()
        + EmissionSourceOPSConverter.OPS_ID_POSTFIX, source.getLabel(), "Comp");

    final EmissionSourceFeature sourceWithFakeSectorFeature = EmissionSourceOPSConverter.convert(opsSource, Substance.NOX, categories, false)
        .getSource();

    assertEquals("12", sourceWithFakeSectorFeature.getId(), "ID");
    assertTrue(sourceWithFakeSectorFeature.getGeometry() instanceof Point, "Should be a point");
    final Point pointFakeSector = (Point) sourceFeature.getGeometry();
    assertEquals(SOURCE_X, pointFakeSector.getX(), 0.00001, "X coord");
    assertEquals(SOURCE_Y, pointFakeSector.getY(), 0.00001, "Y coord");

    final EmissionSource sourceWithFakeSector = sourceWithFakeSectorFeature.getProperties();
    assertEquals(EMISSION, sourceWithFakeSector.getEmissions().get(substance), 0.00001, "Emission");
    assertTrue(sourceWithFakeSector.getCharacteristics() instanceof OPSSourceCharacteristics, "Characteristics should be OPS");
    validateCharacteristics(SOURCE_CHARACTERISTICS, (OPSSourceCharacteristics) sourceWithFakeSector.getCharacteristics());
    assertEquals(SECTOR_ID, sourceWithFakeSector.getSectorId(), "Category number");
    assertEquals(opsSource.getComp() + EmissionSourceOPSConverter.OPS_ID_PREFIX + opsSource.getId()
        + EmissionSourceOPSConverter.OPS_ID_POSTFIX, sourceWithFakeSector.getLabel(), "Comp");
  }

  @Test
  void testConvertOPSSourceSubstanceWithSectors() throws AeriusException {
    final SectorCategories categories = new SectorCategories();
    final OPSSource opsSource = getStandardOPSSource(Substance.NOX);
    final ArrayList<Sector> sectors = new ArrayList<>();
    sectors.add(new Sector(901, SectorGroup.INDUSTRY, ""));
    sectors.add(new Sector(SECTOR_ID, SectorGroup.INDUSTRY, ""));
    sectors.add(new Sector(161, SectorGroup.INDUSTRY, ""));
    categories.setSectors(sectors);

    final Substance key = Substance.NOX;
    final EmissionSourceFeature sourceFeature = EmissionSourceOPSConverter.convert(opsSource, Substance.NOX, categories, true).getSource();
    assertEquals("12", sourceFeature.getId(), "ID");
    assertTrue(sourceFeature.getGeometry() instanceof Point, "Should be a point");
    final Point point = (Point) sourceFeature.getGeometry();
    assertEquals(SOURCE_X, point.getX(), 0.00001, "X coord");
    assertEquals(SOURCE_Y, point.getY(), 0.00001, "Y coord");
    final EmissionSource source = sourceFeature.getProperties();
    assertEquals("12", source.getGmlId(), "GML ID");
    assertEquals(EMISSION, source.getEmissions().get(key), 0.00001, "Emission");
    assertTrue(source.getCharacteristics() instanceof OPSSourceCharacteristics, "Characteristics should be OPS");
    validateCharacteristics(SOURCE_CHARACTERISTICS, (OPSSourceCharacteristics) source.getCharacteristics());
    assertEquals(SECTOR_ID, source.getSectorId(), "Category number");
    assertEquals(opsSource.getComp() + EmissionSourceOPSConverter.OPS_ID_PREFIX + opsSource.getId()
        + EmissionSourceOPSConverter.OPS_ID_POSTFIX, source.getLabel(), "Comp");
  }

  @Test
  void testToOPSSources() throws AeriusException {
    final EmissionSourceOPSConverter esOPSConverter = new EmissionSourceOPSConverter();
    List<OPSSource> opsSources = null;

    final GenericEmissionSource source = new GenericEmissionSource();
    source.setSectorId(748);
    source.getEmissions().put(Substance.NOX, 9.8);
    final Point point = new Point(30000, 400000);

    final ArrayList<EmissionSource> sources = new ArrayList<>();
    sources.add(source);

    final OPSBuildingTracker buildingTracker = new OPSBuildingTracker(List.of());

    opsSources = esOPSConverter.convert(source, point, source.getEmissions(), 1, List.of(Substance.PM10), buildingTracker, null);
    assertTrue(opsSources.isEmpty(), "Sources list pm10 should be empty."); // Because source has NOx emission, and conversion done for PM10 only.

    opsSources = esOPSConverter.convert(source, point, source.getEmissions(), 1, List.of(Substance.NOX), buildingTracker, null);
    assertEquals(1, opsSources.size(), "List should have 1 source.");
    OPSSource opsSource = opsSources.get(0);
    assertEquals(point.getX(), opsSource.getPoint().getX(), 0.001, "Source x");
    assertEquals(point.getY(), opsSource.getPoint().getY(), 0.001, "Source y");
    assertEquals(source.getEmissions().get(Substance.NOX), opsSource.getEmission(Substance.NOX), 0.001, "Source emission");
    assertEquals(748, opsSource.getCat(), "Sector passed to OPS");

    opsSources = esOPSConverter.convert(source, point, source.getEmissions(), 1, Substance.NOXNH3.hatch(), buildingTracker, null);
    assertEquals(1, opsSources.size(), "List should have 1 source.");
    opsSource = opsSources.get(0);
    assertEquals(point.getX(), opsSource.getPoint().getX(), 0.001, "Source x");
    assertEquals(point.getY(), opsSource.getPoint().getY(), 0.001, "Source y");
    assertEquals(source.getEmissions().get(Substance.NOX), opsSource.getEmission(Substance.NOX), 0.001, "Source emission");
    assertEquals(748, opsSource.getCat(), "Sector passed to OPS");

    source.getEmissions().put(Substance.NH3, 8.28);

    opsSources = esOPSConverter.convert(source, point, source.getEmissions(), 1, Substance.NOXNH3.hatch(), buildingTracker, null);
    opsSource = opsSources.get(0);
    assertEquals(1, countEmissionResults(opsSources, Substance.NOX), "List should have 1 source.");
    assertEquals(source.getEmissions().get(Substance.NOX), opsSource.getEmission(Substance.NOX), 0.001, "Source emission");
    assertEquals(1, countEmissionResults(opsSources, Substance.NH3), "List should have 1 source.");
    assertEquals(source.getEmissions().get(Substance.NH3), opsSource.getEmission(Substance.NH3), 0.001, "Source emission");
    assertEquals(748, opsSource.getCat(), "Sector passed to OPS");
  }

  private void validateCharacteristics(final OPSSourceCharacteristics expected, final OPSSourceCharacteristics actual) {
    assertEquals(expected.getHeatContentType(), actual.getHeatContentType(), "Heat content type");
    assertEquals(expected.getHeatContent(), actual.getHeatContent(), "Heat content");
    assertEquals(expected.getEmissionHeight(), actual.getEmissionHeight(), 1E-5, "Emission height");
    assertEquals(expected.getSpread(), actual.getSpread(), 1E-5, "Spread");
    assertEquals(expected.getDiameter(), actual.getDiameter(), "Diameter");
    assertEquals(expected.getDiurnalVariation(), actual.getDiurnalVariation(), "Diurnal variation");
    assertEquals(expected.getParticleSizeDistribution(), actual.getParticleSizeDistribution(), "Particle size distribution");
  }

  private int countEmissionResults(final List<OPSSource> sources, final Substance substance) {
    int cnt = 0;
    for (final OPSSource source : sources) {
      cnt += source.getEmission(substance) > 0 ? 1 : 0;
    }
    return cnt;
  }

  private GenericEmissionSource createGenericEmissionSource() {
    final GenericEmissionSource source = new GenericEmissionSource();
    source.getEmissions().put(Substance.NOX, EMISSION);
    source.setLabel(SOURCE_LABEL);
    source.setCharacteristics(SOURCE_CHARACTERISTICS);
    source.setSectorId(323);
    return source;
  }

  private OPSSource getStandardOPSSource(final Substance substance) throws AeriusException {
    final OPSSource opsSource = new OPSSource(SOURCE_ID, EPSG.RDNEW.getSrid(), SOURCE_X, SOURCE_Y);
    opsSource.setEmission(substance, EMISSION);
    OPSCopyFactory.toOpsSource(SOURCE_CHARACTERISTICS, opsSource, null);
    opsSource.setCat(SECTOR_ID);
    opsSource.setComp(SOURCE_LABEL);
    return opsSource;
  }

  private static OPSSourceCharacteristics defaultCharacteristics() {
    final OPSSourceCharacteristics characteristics = new OPSSourceCharacteristics();
    characteristics.setHeatContentType(HeatContentType.NOT_FORCED);
    characteristics.setHeatContent(2.0);
    characteristics.setEmissionHeight(1.0);
    characteristics.setDiameter(3);
    characteristics.setSpread(2.0);
    characteristics.setDiurnalVariation(DiurnalVariation.INDUSTRIAL_ACTIVITY);
    characteristics.setParticleSizeDistribution(2);
    return characteristics;
  }

}
