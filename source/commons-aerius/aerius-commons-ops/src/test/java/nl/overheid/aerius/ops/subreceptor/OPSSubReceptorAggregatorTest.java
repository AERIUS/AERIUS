/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.subreceptor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.ops.OPSSubReceptor;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Test class for {@link OPSSubReceptorAggregator}.
 *
 * While most testing is done on the base class AbstractSubReceptorAggregator, these tests validate OPS specific behaviour:
 * Only returning a list of non-(sub)receptor point results and aggregated receptor results.
 */
class OPSSubReceptorAggregatorTest {

  @ParameterizedTest
  @MethodSource("pointTypes")
  void testAggregation(final AeriusPointType parentType, final AeriusPointType subPointType) {
    final Collection<OPSReceptor> receptors = createPoints(parentType, subPointType);
    final Set<EmissionResultKey> erks = EnumSet.of(EmissionResultKey.NOX_CONCENTRATION);
    final OPSSubReceptorAggregator aggregator = new OPSSubReceptorAggregator(receptors, erks, true);
    final List<AeriusResultPoint> originalResults = createResults(receptors, parentType);

    assertEquals(10, originalResults.size(), "originalResults list should now contain 10 results");
    final List<AeriusResultPoint> aggregate = aggregator.aggregate(originalResults);

    assertEquals(10, originalResults.size(),
        "originalResults list shouldn't change");
    assertEquals(1, aggregate.size(),
        "Returned list should only contain aggregated result (and any non-receptor results)");
    final Optional<AeriusResultPoint> receptor = aggregate.stream().filter(p -> p.getPointType() == parentType).findAny();

    assertTrue(receptor.isPresent(), "Results should contain the receptor again");
    assertEquals(5.5, receptor.get().getEmissionResult(EmissionResultKey.NOX_CONCENTRATION), 0.1, "Should have aggregated concentration");

    aggregate.stream().filter(p -> p.getPointType().isSubReceptor())
        .forEach(x -> assertEquals(x.getId(), x.getEmissionResult(EmissionResultKey.NOX_CONCENTRATION), 0.1,
            "Should have original concentration"));
  }

  private static List<Arguments> pointTypes() {
    return List.of(
        Arguments.of(AeriusPointType.RECEPTOR, AeriusPointType.SUB_RECEPTOR),
        Arguments.of(AeriusPointType.POINT, AeriusPointType.SUB_POINT));
  }

  private Collection<OPSReceptor> createPoints(final AeriusPointType parentType, final AeriusPointType subPointType) {
    final OPSSubReceptor center = createOPSReceptor(1000, 0, 0, parentType);
    final List<OPSReceptor> points = new ArrayList<>();

    points.add(center);
    for (int i = 0; i < 10; i++) {
      final OPSSubReceptor rp = createOPSReceptor(i + 1, i, i, subPointType);
      rp.setParent(center);
      points.add(rp);
    }
    return points;
  }

  private List<AeriusResultPoint> createResults(final Collection<OPSReceptor> receptors, final AeriusPointType pointType) {
    return receptors.stream().filter(p -> p.getPointType() != pointType).map(p -> {
      final AeriusResultPoint rp = new AeriusResultPoint(p);

      rp.setEmissionResult(EmissionResultKey.NOX_CONCENTRATION, Double.valueOf(p.getId()));
      return rp;
    }).collect(Collectors.toList());
  }

  private OPSSubReceptor createOPSReceptor(final int id, final double x, final double y, final AeriusPointType type) {
    return new OPSSubReceptor(new AeriusPoint(id, null, type, x, y), type, 1);
  }
}
