/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.conversion;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

import java.util.List;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.ops.conversion.OPSBuildingTracker.BuildingDimensionsApplier;
import nl.overheid.aerius.shared.domain.v2.building.Building;
import nl.overheid.aerius.shared.domain.v2.building.BuildingFeature;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.ImaerExceptionReason;

/**
 * Test class for {@link OPSOPSBuildingTracker}.
 */
class OPSOPSBuildingTrackerTest {

  private static final String TEST_GML_ID = "someId";

  @Test
  void testApplyBuildingDimensionsNullList() {
    final OPSBuildingTracker tracker = new OPSBuildingTracker(null);
    final BuildingDimensionsApplier buildingDimensions = mock(BuildingDimensionsApplier.class);
    final AeriusException exceptionThrown = assertThrows(AeriusException.class,
        () -> tracker.applyBuildingDimensions(TEST_GML_ID, buildingDimensions));
    assertEquals(ImaerExceptionReason.COHESION_REFERENCE_MISSING_BUILDING, exceptionThrown.getReason());
    assertEquals(1, exceptionThrown.getArgs().length);
    assertEquals(TEST_GML_ID, exceptionThrown.getArgs()[0]);
    verifyNoInteractions(buildingDimensions);
  }

  @Test
  void testApplyBuildingDimensionsEmptyList() {
    final OPSBuildingTracker tracker = new OPSBuildingTracker(List.of());
    final BuildingDimensionsApplier buildingDimensions = mock(BuildingDimensionsApplier.class);
    final AeriusException exceptionThrown = assertThrows(AeriusException.class,
        () -> tracker.applyBuildingDimensions(TEST_GML_ID, buildingDimensions));
    assertEquals(ImaerExceptionReason.COHESION_REFERENCE_MISSING_BUILDING, exceptionThrown.getReason());
    assertEquals(1, exceptionThrown.getArgs().length);
    assertEquals(TEST_GML_ID, exceptionThrown.getArgs()[0]);
    verifyNoInteractions(buildingDimensions);
  }

  @Test
  void testApplyBuildingDimensions() throws AeriusException {
    final BuildingFeature existingBuilding = new BuildingFeature();
    final Building building = new Building();
    building.setGmlId(TEST_GML_ID);
    building.setHeight(2.0);
    existingBuilding.setProperties(building);
    final Polygon geometry = new Polygon();
    geometry.setCoordinates(new double[][][] {{{0.0, 0.0}, {5.0, 0.0}, {0.0, 10.0}, {5.0, 10.0}, {0.0, 0.0}}});
    existingBuilding.setGeometry(geometry);
    final BuildingFeature otherExistingBuilding = new BuildingFeature();
    final Building otherBuilding = new Building();
    otherBuilding.setGmlId("AnotherID");
    otherBuilding.setHeight(3.0);
    otherExistingBuilding.setProperties(otherBuilding);
    final Polygon otherGeometry = new Polygon();
    otherGeometry.setCoordinates(new double[][][] {{{0.0, 0.0}, {2.0, 0.0}, {0.0, 15.0}, {9.0, 15.0}, {0.0, 0.0}}});
    otherExistingBuilding.setGeometry(otherGeometry);
    final OPSBuildingTracker tracker = new OPSBuildingTracker(List.of(existingBuilding, otherExistingBuilding));

    final BuildingDimensionsApplier buildingDimensions = mock(BuildingDimensionsApplier.class);
    tracker.applyBuildingDimensions(TEST_GML_ID, buildingDimensions);
    verify(buildingDimensions).setDimensions(10.0, 5.0, 2.0, 0);

    final BuildingDimensionsApplier buildingDimensionsUnknown = mock(BuildingDimensionsApplier.class);
    final AeriusException exceptionThrown = assertThrows(AeriusException.class,
        () -> tracker.applyBuildingDimensions("UnknownID", buildingDimensionsUnknown));
    assertEquals(ImaerExceptionReason.COHESION_REFERENCE_MISSING_BUILDING, exceptionThrown.getReason());
    assertEquals(1, exceptionThrown.getArgs().length);
    assertEquals("UnknownID", exceptionThrown.getArgs()[0]);
    verifyNoInteractions(buildingDimensionsUnknown);

    // Same ID, different object should have same effect as the first (and a thrown exception shouldn't affect use)
    final BuildingDimensionsApplier buildingDimensions2 = mock(BuildingDimensionsApplier.class);
    tracker.applyBuildingDimensions(TEST_GML_ID, buildingDimensions2);
    verify(buildingDimensions).setDimensions(10.0, 5.0, 2.0, 0);
  }
}
