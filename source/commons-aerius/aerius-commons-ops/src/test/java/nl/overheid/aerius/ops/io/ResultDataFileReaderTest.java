/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.ops.exception.OPSRetryException;
import nl.overheid.aerius.ops.util.ResultDataFileReaderHelper;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Test class for {@link ResultDataFileReader}.
 */
class ResultDataFileReaderTest {

  private static final String RESULT_NH3 = "result/result.tab";
  private static final String RESULTS_PM10 = "result_pm10/result.tab";
  private static final String RETRY_FILE = "retryrun/settings.err";
  private static final String ERROR_FILE = "errorrun/settings.err";

  @Test
  void readResultsTest() throws IOException {
    final ResultDataFileReader reader = new ResultDataFileReader();
    ResultDataFileReaderHelper.assertResults(ResultDataFileReaderTest.class, "NOxNH3ResultsDeposition", reader, RESULT_NH3,
        EmissionResultKey.NH3_DEPOSITION, createDepositionReference(), true);
    ResultDataFileReaderHelper.assertResults(ResultDataFileReaderTest.class, "NOxNH3ResultsConcentration", reader, RESULT_NH3,
        EmissionResultKey.NH3_CONCENTRATION, createConcentrationReference(), true);
    ResultDataFileReaderHelper.assertResults(ResultDataFileReaderTest.class, "NOxNH3ResultsDryDeposition", reader, RESULT_NH3,
        EmissionResultKey.NH3_DRY_DEPOSITION, createDryReference(), true);
    ResultDataFileReaderHelper.assertResults(ResultDataFileReaderTest.class, "NOxNH3ResultsWetDeposition", reader, RESULT_NH3,
        EmissionResultKey.NH3_WET_DEPOSITION, createWetReference(), true);

    final RcpPM10FileReader pm10Reader = new RcpPM10FileReader();
    pm10Reader.setResultSubstance(Substance.PM10);
    ResultDataFileReaderHelper.assertResults(ResultDataFileReaderTest.class, "PM10ResultsConcentration", pm10Reader, RESULTS_PM10,
        EmissionResultKey.PM10_CONCENTRATION, createPM10Reference(), true);
    pm10Reader.setResultSubstance(Substance.PM25);
    ResultDataFileReaderHelper.assertResults(ResultDataFileReaderTest.class, "PM25ResultsConcentration", pm10Reader, RESULTS_PM10,
        EmissionResultKey.PM25_CONCENTRATION, createPM25Reference(), true);
  }

  @Test
  void testCheckForErrorsWithoutErrors() throws IOException, AeriusException {
    final File file = new File(ResultDataFileReaderTest.class.getResource(RESULT_NH3).getFile()).getParentFile();
    ResultDataFileReader.checkForErrors(file);
    //shouldn't throw exception
  }

  @Test
  void testCheckForErrorsWithRetry() throws IOException, AeriusException {
    final File file = new File(ResultDataFileReaderTest.class.getResource(RETRY_FILE).getFile()).getParentFile();
    assertThrows(OPSRetryException.class, () -> ResultDataFileReader.checkForErrors(file),
        "Should throw retry error when error indicates it can be retried.");
  }

  @Test
  void testCheckForErrorsWithErrors() throws IOException, AeriusException {
    final File file = new File(ResultDataFileReaderTest.class.getResource(ERROR_FILE).getFile()).getParentFile();
    try {
      ResultDataFileReader.checkForErrors(file);
      fail("Expected exception");
    } catch (final AeriusException e) {
      if (e instanceof OPSRetryException) {
        fail("Didn't expect a retry");
      } else {
        assertEquals(AeriusExceptionReason.OPS_INTERNAL_EXCEPTION, e.getReason(), "Reason for exception");
      }
    }
  }

  static ArrayList<AeriusResultPoint> createDepositionReference() {
    final ArrayList<AeriusResultPoint> result = new ArrayList<>();

    result.add(createData(37094, 193000, 432000, EmissionResultKey.NH3_DEPOSITION, 0.03141));
    result.add(createData(37095, 193250, 432000, EmissionResultKey.NH3_DEPOSITION, 0.4041E-01));
    result.add(createData(37096, 193500, 432000, EmissionResultKey.NH3_DEPOSITION, 0.4861E-01));
    result.add(createData(37097, 193750, 432000, EmissionResultKey.NH3_DEPOSITION, 0));
    return result;
  }

  static ArrayList<AeriusResultPoint> createConcentrationReference() {
    final ArrayList<AeriusResultPoint> result = new ArrayList<>();

    result.add(createData(37094, 193000, 432000, EmissionResultKey.NH3_CONCENTRATION, 0.9011E-04));
    result.add(createData(37095, 193250, 432000, EmissionResultKey.NH3_CONCENTRATION, 0.1031E-03));
    result.add(createData(37096, 193500, 432000, EmissionResultKey.NH3_CONCENTRATION, 0.1041E-03));
    result.add(createData(37097, 193750, 432000, EmissionResultKey.NH3_CONCENTRATION, 0));
    return result;
  }

  static ArrayList<AeriusResultPoint> createDryReference() {
    final ArrayList<AeriusResultPoint> result = new ArrayList<>();

    result.add(createData(37094, 193000, 432000, EmissionResultKey.NH3_DRY_DEPOSITION, 0.9991E-02));
    result.add(createData(37095, 193250, 432000, EmissionResultKey.NH3_DRY_DEPOSITION, 0.1731E-01));
    result.add(createData(37096, 193500, 432000, EmissionResultKey.NH3_DRY_DEPOSITION, 0.2431E-01));
    result.add(createData(37097, 193750, 432000, EmissionResultKey.NH3_DRY_DEPOSITION, 0));
    return result;
  }

  static ArrayList<AeriusResultPoint> createWetReference() {
    final ArrayList<AeriusResultPoint> result = new ArrayList<>();

    result.add(createData(37094, 193000, 432000, EmissionResultKey.NH3_WET_DEPOSITION, 0.2141E-01));
    result.add(createData(37095, 193250, 432000, EmissionResultKey.NH3_WET_DEPOSITION, 0.2311E-01));
    result.add(createData(37096, 193500, 432000, EmissionResultKey.NH3_WET_DEPOSITION, 0.2431E-01));
    result.add(createData(37097, 193750, 432000, EmissionResultKey.NH3_WET_DEPOSITION, 0));
    return result;
  }

  static ArrayList<AeriusResultPoint> createPM10Reference() {
    final ArrayList<AeriusResultPoint> result = new ArrayList<>();

    result.add(createData(37094, 193000, 432000, EmissionResultKey.PM10_CONCENTRATION, 0.1721E-02));
    result.add(createData(37095, 193250, 432000, EmissionResultKey.PM10_CONCENTRATION, 0.1711E-02));
    result.add(createData(37096, 193500, 432000, EmissionResultKey.PM10_CONCENTRATION, 0.1681E-02));
    return result;
  }

  static ArrayList<AeriusResultPoint> createPM25Reference() {
    final ArrayList<AeriusResultPoint> result = new ArrayList<>();

    result.add(createData(37094, 193000, 432000, EmissionResultKey.PM25_CONCENTRATION, 0.1551E-02));
    result.add(createData(37095, 193250, 432000, EmissionResultKey.PM25_CONCENTRATION, 0.1541E-02));
    result.add(createData(37096, 193500, 432000, EmissionResultKey.PM25_CONCENTRATION, 0.1511E-02));
    return result;
  }

  private static AeriusResultPoint createData(final int id, final int x, final int y, final EmissionResultKey key, final double totalNHx) {
    final AeriusResultPoint r = new AeriusResultPoint(id, null, AeriusPointType.POINT, x, y);

    r.setEmissionResult(key, totalNHx);
    return r;
  }

}
