/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.runner;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.exec.ExecuteWrapper;
import nl.overheid.aerius.ops.domain.OPSConfiguration;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Test class for {@link OPSSingleSubstanceRunner}.
 */
@ExtendWith(MockitoExtension.class)
class OPSSingleSubstanceRunnerTest {

  private @Mock ExecuteWrapper mockExecuteWrapper;
  private @Mock OPSStreamGobblers mockGobblers;

  private OPSSingleSubstanceRunner runner;
  private @TempDir File tempDir;

  @Test
  void testErrorFileRun() throws IOException, AeriusException {
    final OPSConfiguration config = createConfiguration();
    runner = new OPSSingleSubstanceRunner(config);
    Files.write(Path.of(tempDir.getAbsolutePath(), "settings.err"), "error".getBytes());

    final AeriusException exception = assertThrows(AeriusException.class, () -> runner.performRun("1", tempDir, mockExecuteWrapper, mockGobblers),
        "Should throw an exception if an OPS error file is present");
    assertEquals(AeriusExceptionReason.OPS_INTERNAL_EXCEPTION, exception.getReason(), "Expected an OPS internal error exception on error file.");
    verify(mockGobblers, never()).getError();
  }

  @Test
  void testRetryRun() throws IOException, AeriusException, InterruptedException {
    runner = new OPSSingleSubstanceRunner(createConfiguration());
    // run should fail the first time, but then retry. In retry error file is deleted, therefor second run should succeed.
    Files.write(Path.of(tempDir.getAbsolutePath(), "settings.err"), "Io-status = 30".getBytes());

    assertDoesNotThrow(() -> runner.performRun("1", tempDir, mockExecuteWrapper, mockGobblers), "Should not throw an exception when retrying");
    verify(mockExecuteWrapper, times(2)).run(anyString(), any(File.class));
    verify(mockGobblers).getError();
  }

  @Test
  void testCrashRun() throws IOException, AeriusException {
    runner = new OPSSingleSubstanceRunner(createConfiguration());
    doReturn(new AeriusException()).when(mockGobblers).getError();
    assertThrows(AeriusException.class, () -> runner.performRun("1", tempDir, mockExecuteWrapper, mockGobblers),
        "Should throw an exception if OPS crash in std error output");
  }

  private OPSConfiguration createConfiguration() {
    final OPSConfiguration config = new OPSConfiguration(1);
    config.setVersionsRoot(tempDir);
    return config;
  }
}
