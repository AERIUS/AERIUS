/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.EngineInputData.SubReceptorCalculation;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.domain.OPSSubReceptorCreator;
import nl.overheid.aerius.ops.exception.OPSInvalidVersionException;
import nl.overheid.aerius.ops.io.ResultDataFileReader;
import nl.overheid.aerius.ops.util.OPSTestUtil;
import nl.overheid.aerius.ops.util.ResultDataFileReaderHelper;
import nl.overheid.aerius.ops.version.OPSVersion;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.ops.OPSSubReceptor;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.test.TestDomain;

/**
 * Test class to check if sources near by receptors are correctly calculated with OPS.
 */
@ExtendWith(MockitoExtension.class)
class RunOPSSubreceptorsTest extends OPSRunnerTestBase {
  private static final Logger LOG = LoggerFactory.getLogger(RunOPSSubreceptorsTest.class);

  private static final String RESOURCES_DIRECTORY = "subreceptors/";
  private static final String INPUT_DIRECTORY = RESOURCES_DIRECTORY + "input/";
  private static final String RECEPTOR_FILE = INPUT_DIRECTORY + "receptors.rcp";
  private static final String EMISSION_SOURCE_FILE = "/emissions.brn";
  private static final String EXPECTED_RESULT_FILE = "/" + ResultDataFileReader.FILE_NAME;

  static List<Arguments> dataLatest() {
    return data("latest");
  }

  private static List<Arguments> data(final String versionFolder) {
    return List.of(
        getParameters("hogebron_NOx", versionFolder, AeriusPointType.POINT),
        getParameters("hogebron_NOx", versionFolder, AeriusPointType.RECEPTOR),
        getParameters("hogebron_verplaatst_NOx", versionFolder, AeriusPointType.POINT),
        getParameters("lagebron_NOx", versionFolder, AeriusPointType.POINT),
        getParameters("lijnbron_NOx", versionFolder, AeriusPointType.POINT),
        getParameters("lijnbron_kort_NOx", versionFolder, AeriusPointType.POINT));
  }

  private static Arguments getParameters(final String folder, final String versionFolder, final AeriusPointType originalPointType) {
    return Arguments.of(folder + '-' + originalPointType.name(),
        INPUT_DIRECTORY + folder + EMISSION_SOURCE_FILE,
        RESOURCES_DIRECTORY + versionFolder + "/" + folder + EXPECTED_RESULT_FILE,
        originalPointType);
  }

  @ParameterizedTest
  @MethodSource("dataLatest")
  void testRunOPSWithNearbySourcesLatest(final String name, final String emissionFile, final String resultFile,
      final AeriusPointType originalPointType)
          throws IOException, AeriusException, OPSInvalidVersionException {
    mockOPSOutputFiles("RunOPSWithNearbySourcesLatest-" + name);
    final List<OPSReceptor> receptorsToCalculate = getProvidedSubReceptors(originalPointType);
    final String message = "RunOPS for small receptor-source distances, LATEST.";

    assertRun(OPSVersion.LATEST, emissionFile, resultFile, originalPointType, receptorsToCalculate, SubReceptorCalculation.PROVIDED, message);
  }

  static List<Arguments> dataDisabled() {
    return data("latest_disabled");
  }

  @ParameterizedTest
  @MethodSource("dataDisabled")
  void testRunOPSWithNearbySourcesLatestDisabled(final String name, final String emissionFile, final String resultFile,
      final AeriusPointType originalPointType)
          throws IOException, AeriusException, OPSInvalidVersionException {
    mockOPSOutputFiles("RunOPSWithNearbySourcesLatestDisabled-" + name);
    final String message = "RunOPS for small receptor-source distances, LATEST with disabled sub receptors.";

    assertRun(OPSVersion.LATEST, emissionFile, resultFile, originalPointType, OPSTestUtil.readRcpFile(RECEPTOR_FILE).getObjects(),
        SubReceptorCalculation.DISABLED, message);
  }

  static List<Arguments> dataStable() {
    return data("stable");
  }

  @ParameterizedTest
  @MethodSource("dataStable")
  void testRunOPSWithNearbySourcesStable(final String name, final String emissionFile, final String resultFile,
      final AeriusPointType originalPointType)
          throws IOException, AeriusException, OPSInvalidVersionException {
    mockOPSOutputFiles("RunOPSWithNearbySourcesStable-" + name);
    final String message = "RunOPS for small receptor-source distances, STABLE.";

    assertRun(OPSVersion.STABLE, emissionFile, resultFile, originalPointType, getProvidedSubReceptors(originalPointType),
        SubReceptorCalculation.PROVIDED, message);
  }

  private void assertRun(final OPSVersion opsVersion, final String emissionFile, final String resultFile, final AeriusPointType originalPointType,
      final List<OPSReceptor> receptorsToCalculate, final SubReceptorCalculation subReceptorCalculation, final String message)
          throws IOException, AeriusException, OPSInvalidVersionException {
    LOG.debug("Run test on {} with resultfile {}", emissionFile, resultFile);
    final EmissionResultKey erk = EmissionResultKey.NOX_DEPOSITION;
    final ResultDataFileReader resultReader = new ResultDataFileReader();
    final File resultDirectory = config.getRunFilesDirectory();
    final int numberOfFilesBeforeRun = resultDirectory.listFiles().length;
    final OPSInputData inputData = getInputTestData(erk, opsVersion);

    inputData.setSubReceptorCalculation(subReceptorCalculation);
    inputData.setReceptors(receptorsToCalculate);
    inputData.setEmissionSources(1, OPSTestUtil.readBrnFile(Substance.NOX, emissionFile).getObjects());

    final CalculationResult result = runOPS.run(inputData);
    ResultDataFileReaderHelper.assertResults(OPSProcessRunnerTest.class, message, resultReader, resultFile, erk, result.getResults().get(1), false);
    assertEquals(numberOfFilesBeforeRun, resultDirectory.listFiles().length, "Number of files/directories in" + resultDirectory);
  }

  @Test
  void testRunOPSWithoutSubReceptors() throws IOException, AeriusException, OPSInvalidVersionException {
    mockOPSOutputFiles("RunOPSWithoutSubReceptors");
    final String emissionFile = INPUT_DIRECTORY + "hogebron_NOx" + EMISSION_SOURCE_FILE;
    final EmissionResultKey erk = EmissionResultKey.NOX_DEPOSITION;
    final OPSInputData inputData = getInputTestData(erk, OPSVersion.LATEST);
    // Use the same normal but filter out the actual subpoints, so you get left with only center receptors.
    final List<OPSReceptor> receptorsToCalculate = getProvidedSubReceptors(AeriusPointType.POINT)
        .stream()
        .filter(p -> p.getPointType() != AeriusPointType.SUB_POINT)
        .toList();

    inputData.setSubReceptorCalculation(SubReceptorCalculation.PROVIDED);
    inputData.setReceptors(receptorsToCalculate);
    inputData.setEmissionSources(1, OPSTestUtil.readBrnFile(Substance.NOX, emissionFile).getObjects());

    final CalculationResult result = runOPS.run(inputData);
    assertFalse(result.getResults().isEmpty(), "Results shouldn't be empty");
    for (final Entry<Integer, List<AeriusResultPoint>> entry : result.getResults().entrySet()) {
      assertTrue(entry.getValue().isEmpty(), "Result list should be empty, as we didn't actually calculate anything");
    }
  }

  private List<OPSReceptor> getProvidedSubReceptors(final AeriusPointType originalPointType) throws IOException, AeriusException {
    final List<OPSReceptor> receptorsToCalculate = new ArrayList<>();
    final List<OPSReceptor> receptors = OPSTestUtil.readRcpFile(RECEPTOR_FILE).getObjects();
    final OPSSubReceptorCreator subReceptorCreator = new OPSSubReceptorCreator(
        originalPointType == AeriusPointType.RECEPTOR ? AeriusPointType.SUB_RECEPTOR : AeriusPointType.SUB_POINT,
        TestDomain.getExampleGridSettings().getZoomLevel1().getHexagonRadius());
    for (final OPSReceptor receptor : receptors) {
      // Normally we're using points of type RECEPTOR, but when read from rcp they are considered POINT
      // Fake them being receptors by setting the type.
      receptor.setPointType(originalPointType);
      final OPSSubReceptor centerReceptor = subReceptorCreator.copyToSubReceptorPoint(receptor, receptor.getPointType(), 0);
      receptorsToCalculate.add(centerReceptor);
      final Collection<OPSSubReceptor> subReceptors = subReceptorCreator.computePoints(centerReceptor, 3, null);
      receptorsToCalculate.addAll(subReceptors);
    }
    return receptorsToCalculate;
  }
}
