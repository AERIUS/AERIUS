/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.ops.util.OPSTestUtil;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Test for {@link RcpFileReader} class.
 */
class RcpFileReaderTest {

  private static final String RESOURCES_DIRECTORY = "io/receptor/";
  private static final String USED_EXTENSION = ".rcp";
  private static final String RECEPTOR_FILE = "receptor";
  private static final String RECEPTOR_FILE_FREEFORMAT = "receptor_freeformat";
  private static final String RECEPTOR_FAIL_FILE = "receptor_fail";
  private static final String RECEPTOR_WITH_HEIGHT = "receptor_height";

  @Test
  void testParseRegularOPSFile() throws IOException {
    final LineReaderResult<OPSReceptor> result = readRcpFile(RECEPTOR_FILE);

    assertNotNull(result, "Check if has receptors.");
    testExpectedReceptors(result);
  }

  @Test
  void testParseFreeFormatOPSFile() throws IOException {
    final LineReaderResult<OPSReceptor> result = readRcpFile(RECEPTOR_FILE_FREEFORMAT);

    assertNotNull(result, "Check if has receptors.");
    testExpectedReceptors(result);
  }

  private void testExpectedReceptors(final LineReaderResult<OPSReceptor> input) {
    final List<OPSReceptor> receptors = input.getObjects();
    assertEquals(7, receptors.size(), "Receptors parsed");
    // ID's are descending from 7
    for (final OPSReceptor receptor : receptors) {
      assertNotEquals(0, receptor.getId(), "receptor ID");
    }
    // header    : snr    x(m)    y(m)    q(g/s) hc(MW)  h(m)   d(m)  s(m)  dv cat area sd  comp
    // first line:   7  139123  434456 2.500E-01  2.100   2.5      3   0.2   4   2   2   2  17
    assertEquals(106206, receptors.get(0).getX(), 0.00001, "X coord");
    assertEquals(419024, receptors.get(0).getY(), 0.00001, "Y coord");
    assertEquals("words", receptors.get(0).getLabel(), "Name");
    assertTrue(input.getExceptions().isEmpty(), "Check if no exceptions present");
  }

  @Test
  void testParseRegularOPSFileFail1() throws IOException {
    // first fail file is a file where a line has been cut in half.
    final LineReaderResult<OPSReceptor> result = readRcpFile(RECEPTOR_FAIL_FILE + 1);

    assertEquals(2, result.getExceptions().size(), "Number of exceptions");
    // Fail line: "   7 3310760       "
    final AeriusException e1 = result.getExceptions().get(0);
    assertEquals(AeriusExceptionReason.IO_EXCEPTION_NOT_ENOUGH_FIELDS, e1.getReason(), "Error reason");
    assertEquals("2", e1.getArgs()[0], "Error line number");
    // Fail line: "   106206  419024"
    final AeriusException e2 = result.getExceptions().get(1);
    assertEquals(AeriusExceptionReason.IO_EXCEPTION_NOT_ENOUGH_FIELDS, e2.getReason(), "Error reason");
    assertEquals("3", e2.getArgs()[0], "Error line number");
  }

  @Test
  void testParseRegularOPSFileFail2() throws IOException {
    // second fail file is a file where a line contains a word as ID (should be a number).
    final LineReaderResult<OPSReceptor> result = readRcpFile(RECEPTOR_FAIL_FILE + 2);

    assertEquals(1, result.getExceptions().size(), "Number of exceptions");
    // Fail line: " one 3310760       106206  419024"
    final AeriusException e = result.getExceptions().get(0);
    assertEquals(AeriusExceptionReason.IO_EXCEPTION_NUMBER_FORMAT, e.getReason(), "Error reason");
    assertEquals("2", e.getArgs()[0], "Error line number");
    assertEquals("Nr", e.getArgs()[1], "Error column name");
    assertEquals("one", e.getArgs()[2], "Error column content");
  }

  @Test
  void testParseOPSFileWithHeights() throws IOException {
    final RcpFileReader reader = new RcpFileReader();
    reader.setReadTerrainData(true);
    reader.setReadHeight(true);
    final LineReaderResult<OPSReceptor> result = OPSTestUtil.readFile(reader, RESOURCES_DIRECTORY + RECEPTOR_WITH_HEIGHT + USED_EXTENSION);

    testExpectedReceptors(result);
    for (final OPSReceptor receptor : result.getObjects()) {
      if (receptor.getId() > 2) {
        assertNotNull(receptor.getHeight(), "receptor height");
        assertNotEquals(0.0, receptor.getHeight(), 1E-5, "receptor height");
      } else {
        assertNull(receptor.getHeight(), "receptor height");
      }
    }
  }

  private LineReaderResult<OPSReceptor> readRcpFile(final String fileName) throws IOException {
    return OPSTestUtil.readRcpFile(RESOURCES_DIRECTORY + fileName + USED_EXTENSION);
  }
}
