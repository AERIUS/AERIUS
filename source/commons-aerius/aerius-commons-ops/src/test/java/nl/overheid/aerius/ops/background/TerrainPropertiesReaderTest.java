/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.background;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.ops.util.OPSTestUtil;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.ops.LandUse;
import nl.overheid.aerius.shared.domain.ops.OPSTerrainProperties;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;
import nl.overheid.aerius.test.TestDomain;

/**
 * Test class for TerrainPropertiesReader.
 */
class TerrainPropertiesReaderTest {

  private static final String VALID_FILE = "background/test_ops_terrain_properties.txt";

  private static final String HEADER = "receptor_id\taverage_roughness\tdominant_land_use\tland_uses";

  @Test
  void testReadFile() throws IOException {
    final ReceptorGridSettings gridSettings = TestDomain.getExampleGridSettings();
    TerrainPropertiesMap map;
    try (InputStream inputStream = OPSTestUtil.getFileInputStream(VALID_FILE)) {
      map = TerrainPropertiesReader.read(inputStream, gridSettings);
    }
    assertEquals(9, map.size(), "Number of records in the map");
  }

  @Test
  void testReadHeaderOnly() throws IOException {
    final ReceptorGridSettings gridSettings = TestDomain.getExampleGridSettings();
    final String testContent = HEADER + "\n";
    TerrainPropertiesMap map;
    try (InputStream inputStream = new ByteArrayInputStream(testContent.getBytes())) {
      map = TerrainPropertiesReader.read(inputStream, gridSettings);
    }
    assertEquals(0, map.size(), "Number of records in the map");
  }

  @Test
  void testRead1Row() throws IOException {
    final ReceptorGridSettings gridSettings = TestDomain.getExampleGridSettings();
    final ReceptorUtil receptorUtil = new ReceptorUtil(gridSettings);
    final String testContent = HEADER + "\n"
        + "101\t0.202\tloofbos\t{1,2,3,4,5,6,7,8,9}";
    final Point expectedPoint = receptorUtil.getPointFromReceptorId(101);

    TerrainPropertiesMap map;
    try (InputStream inputStream = new ByteArrayInputStream(testContent.getBytes())) {
      map = TerrainPropertiesReader.read(inputStream, gridSettings);
    }

    assertEquals(1, map.size(), "Number of records in the map");
    final Optional<OPSTerrainProperties> terrainProperties = map.getTerrainProperties(expectedPoint);
    assertTrue(terrainProperties.isPresent(), "Expected point should be present");
    assertEquals(0.202, terrainProperties.get().getAverageRoughness(), 1E-5, "Average roughness");
    assertEquals(LandUse.DECIDUOUS_FOREST, terrainProperties.get().getLandUse(), "Dominant land use");
    assertArrayEquals(new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9}, terrainProperties.get().getLandUses(), "land uses");
  }

  @ParameterizedTest(name = "{index} {0}")
  @MethodSource("invalidRows")
  void testReadInvalidRows(final String description, final String row) throws IOException {
    final ReceptorGridSettings gridSettings = TestDomain.getExampleGridSettings();
    final String testContent = HEADER + "\n"
        + row;

    try (InputStream inputStream = new ByteArrayInputStream(testContent.getBytes())) {
      assertThrows(IOException.class, () -> TerrainPropertiesReader.read(inputStream, gridSettings),
          "Expecting an IOException for this case");
    }
  }

  private static List<Arguments> invalidRows() {
    return List.of(
        Arguments.of("Incomplete row", "101\t0.202\tloofbos"),
        Arguments.of("Incorrect separator", "101;0.202;loofbos;{1,2,3,4,5,6,7,8,9}"),
        Arguments.of("Receptor incorrect", "x\t0.202\tloofbos\t{1,2,3,4,5,6,7,8,9}"),
        Arguments.of("Roughness incorrect", "101\tavg\tloofbos\t{1,2,3,4,5,6,7,8,9}"),
        Arguments.of("Unknown landuse", "101\t0.202\tsprieten\t{1,2,3,4,5,6,7,8,9}"),
        Arguments.of("Too few landuses", "101\t0.202\tloofbos\t{1,2,3,4,5,6,7,8}"),
        Arguments.of("Too many landuses", "101\t0.202\tloofbos\t{1,2,3,4,5,6,7,8,9,10}"),
        Arguments.of("No number for a landuse", "101\t0.202\tloofbos\t{1,2,3,4,5,6,7,8,rest}"));
  }

}
