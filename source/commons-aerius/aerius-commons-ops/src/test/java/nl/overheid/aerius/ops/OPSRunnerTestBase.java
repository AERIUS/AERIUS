/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.io.TempDir;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.EngineInputData.CommandType;
import nl.overheid.aerius.http.HttpClientProxy;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.ops.background.OPSData;
import nl.overheid.aerius.ops.domain.OPSConfiguration;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.exception.OPSInvalidVersionException;
import nl.overheid.aerius.ops.io.BrnFileReaderV0;
import nl.overheid.aerius.ops.io.RcpFileReader;
import nl.overheid.aerius.ops.io.ResultDataFileReader;
import nl.overheid.aerius.ops.util.OPSTestUtil;
import nl.overheid.aerius.ops.util.ResultDataFileReaderHelper;
import nl.overheid.aerius.ops.version.OPSVersion;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.OPSOptions;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.util.HttpClientManager;
import nl.overheid.aerius.worker.PropertiesUtil;

/**
 * Base class for running OPS tests.
 */
public abstract class OPSRunnerTestBase {

  private static final Logger LOG = LoggerFactory.getLogger(OPSRunnerTestBase.class);

  // Static boolean to avoid retrying download for every test.
  private static boolean attemptedDownload;

  protected static final int YEAR = 2030;

  protected OPSProcessRunner runOPS;
  protected OPSConfiguration config;
  protected boolean useMockResults;

  private boolean keepGeneratedFiles;

  @TempDir
  private File tempResultsDirectory;

  private Process mockedProcess;
  private Runtime mockedRuntime;

  @BeforeEach
  public void setUp() throws Exception {
    config = getConfig();
    // Set environment variable AERIUS_OPS_GENERATE_REFERENCE to any value to generate reference files
    useMockResults = System.getenv("AERIUS_OPS_GENERATE_MOCK_RESULTS") == null;
    keepGeneratedFiles = true; //set to true before assume, if assume returns false tearDown() will not be run.
    if (!useMockResults) {
      tryDownload(config);
    }
    assumeTrue(!useMockResults || checkOPSAvailability(config), "OPS not installed, skipping test.");
    config.setRunFilesDirectory(tempResultsDirectory);
    runOPS = new OPSProcessRunner(config, new OPSData());
    mockedRuntime = mock(Runtime.class);
    runOPS.getRunner().setRuntime(mockedRuntime);
    mockedProcess = mock(Process.class);
    lenient().doReturn(new ByteArrayInputStream("".getBytes(StandardCharsets.UTF_8))).when(mockedProcess).getErrorStream();
    lenient().doReturn(new ByteArrayInputStream(" ".getBytes(StandardCharsets.UTF_8))).when(mockedProcess).getInputStream();
    lenient().doReturn(0).when(mockedProcess).waitFor();
    if (useMockResults) {
      LOG.info("Unit test will mock OPS run.");
    } else {
      LOG.info("Unit test will run OPS.");
    }
  }

  /**
   * Method to mock for multiple runs in the same test. Each time OPS is run the reference input file taken is appended with an incremented number,
   * only for the first increment nothing is appended. This for convenience to let test run without multiple runs use the same logic, without the
   * number 1 being appended.
   *
   * @param orgReferenceFilename
   */
  protected void mockOPSOutputFilesWithCounter(final String orgReferenceFilename) throws IOException {
    final AtomicInteger counter = new AtomicInteger();

    mockOPSOutputFiles(orgReferenceFilename, () -> counter.incrementAndGet() == 1 ? "" : String.valueOf(counter.get()));
  }

  protected void mockOPSOutputFiles(final String orgReferenceFilename) throws IOException {
    mockOPSOutputFiles(orgReferenceFilename, () -> "");
  }

  private void mockOPSOutputFiles(final String orgReferenceFilename, final Supplier<String> counter) throws IOException {
    lenient().doAnswer(invocation -> {
      synchronized (counter) {
        final String referenceFilename = getClass().getSimpleName() + '-' + orgReferenceFilename.replace(" ", "-") + counter.get();

        final File outputDir = invocation.getArgument(2);
        final Path opsRunResultTab = Path.of(outputDir.getAbsolutePath(), "result.tab");
        final Path opsRunSettingsErr = Path.of(outputDir.getAbsolutePath(), "settings.err");
        final Path opsReferenceResultTab = referenceFileToPath(referenceFilename + ".tab");
        final Path opsReferenceSettingsError = referenceFileToPath(referenceFilename + ".err");

        if (useMockResults) {
          copyFileIfExists(opsReferenceResultTab, opsRunResultTab);
          copyFileIfExists(opsReferenceSettingsError, opsRunSettingsErr);
          return mockedProcess;
        } else {
          final Process runProcess = Runtime.getRuntime().exec((String[]) invocation.getArgument(0), null, outputDir);
          runProcess.waitFor();
          copyFileIfExists(opsRunResultTab, opsReferenceResultTab);
          copyFileIfExists(opsRunSettingsErr, opsReferenceSettingsError);
          return runProcess;
        }
      }
    }).when(mockedRuntime).exec(any(String[].class), isNull(), any(File.class));
  }

  private static void copyFileIfExists(final Path referenceFile, final Path targetRerenceFile) throws IOException {
    if (referenceFile.toFile().exists()) {
      Files.copy(referenceFile, targetRerenceFile, StandardCopyOption.REPLACE_EXISTING);
    }
  }

  /**
   * Creates a path name in the directory : ../aerius-commons-ops/src/test/resources/nl/overheid/aerius/ops/mock_results
   *
   * @param referenceFilename
   * @return
   */
  private Path referenceFileToPath(final String referenceFilename) throws URISyntaxException {
    return Path.of(
        Path.of(getClass().getClassLoader().getResource(".").toURI()).toFile().getParentFile().getParentFile().getAbsolutePath(),
        "src", "test", "resources", "nl", "overheid", "aerius", "ops", "mock_results", referenceFilename);
  }

  // Synchronized to avoid issues when running in parallel
  private static synchronized void tryDownload(final OPSConfiguration config) throws Exception {
    if (!attemptedDownload) {
      try {
        new OPSModelDataUtil(new HttpClientProxy(new HttpClientManager()))
        .ensureModelDataPresence(config);
      } catch (final Exception e) {
        LOG.error("Tried to download OPS for testing purposes, but got an exception", e);
      }
      attemptedDownload = true;
    }
  }

  @AfterEach
  public void tearDown() {
    if (!keepGeneratedFiles) {
      for (final File file : config.getRunFilesDirectory().listFiles()) {
        try {
          UUID.fromString(file.getName());
          FileUtil.removeDirectoryRecursively(file.toPath());
        } catch (final IllegalArgumentException e) {
          // If argument for UUID.fromString does not follow UUID rules, it'll throw an IllegalArgumentException
          // When that's the case, we don't want to delete (we didn't generate it).
          // Should make results where runFilesPath is defined as C:\\ less dramatic.
        }
      }
    }
  }

  protected OPSConfiguration getConfig() throws IOException {
    final OPSWorkerFactory factory = new OPSWorkerFactory();
    final Properties properties = PropertiesUtil.getFromTestPropertiesFile("ops");
    if (useMockResults) {
      properties.remove("ops.model.preload.versions");
    }
    return factory.configurationBuilder(properties).buildAndValidate().get();
  }

  private static boolean checkOPSAvailability(final OPSConfiguration config) {
    final File opsRoot = config.getOpsRoot(OPSVersion.LATEST);
    final boolean available = opsRoot != null && opsRoot.exists();
    if (!available) {
      LOG.warn("Could not find OPS root directory ({}). {}", opsRoot, Thread.currentThread().getStackTrace()[2]);
    }
    return available;
  }

  protected List<AssertionError> assertRunOPS(final ResultDataFileReader reader, final EmissionResultKey erk, final String referenceFile,
      final boolean failOnResultDiff) throws IOException, AeriusException, OPSInvalidVersionException {
    final File resultDirectory = config.getRunFilesDirectory();
    final int numberOfFilesBeforeRun = resultDirectory.listFiles().length;
    final CalculationResult result = runOPS.run(getInputTestData(erk));
    final String message = "RunOPS: Input " + erk + ". reference file: " + referenceFile;
    final List<AssertionError> errors = ResultDataFileReaderHelper.assertResults(getClass(), message, reader, referenceFile, erk,
        result.getResults().get(1), failOnResultDiff);

    assertEquals(numberOfFilesBeforeRun, resultDirectory.listFiles().length, "Number of files/directories in" + resultDirectory);
    return errors;
  }

  protected OPSInputData getInputTestData(final EmissionResultKey key) throws IOException {
    return OPSTestUtil.getInputTestData(key, YEAR, OPSOptions.Chemistry.ACTUAL);
  }

  protected OPSInputData getInputTestData(final EmissionResultKey key, final OPSVersion opsVersion) throws IOException {
    return OPSTestUtil.getInputTestData(CommandType.CALCULATE, key, YEAR, OPSOptions.Chemistry.ACTUAL, opsVersion);
  }

  protected void setSources(final OPSInputData inputData, final String sourcesFile, final Substance substance) throws IOException {
    try (InputStream inputStream = getFileInputStream(sourcesFile)) {
      final BrnFileReaderV0 emissionFileReader = new BrnFileReaderV0(substance);
      final LineReaderResult<OPSSource> results = emissionFileReader.readObjects(inputStream);
      inputData.setEmissionSources(1, new ArrayList<>(results.getObjects()));
    }
  }

  protected void setReceptors(final OPSInputData inputData, final String receptorFile) throws IOException {
    try (InputStream inputStream = getFileInputStream(receptorFile)) {
      final RcpFileReader opsReceptorFileReader = new RcpFileReader();
      opsReceptorFileReader.setReadTerrainData(true);
      final LineReaderResult<OPSReceptor> receptors = opsReceptorFileReader.readObjects(inputStream);

      assertNotNull(receptors, "Check if has receptors.");
      inputData.setReceptors(new ArrayList<>(receptors.getObjects()));
    }
  }

  protected InputStream getFileInputStream(final String fileName) throws FileNotFoundException {
    final File file = new File(getClass().getResource(fileName).getFile());

    return new FileInputStream(file);
  }

}
