/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.CALLS_REAL_METHODS;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.withSettings;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import nl.overheid.aerius.ops.domain.OPSConfiguration;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.exception.OPSInvalidVersionException;
import nl.overheid.aerius.ops.util.OPSTestUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.OPSOptions;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.util.FileUtil;

/**
 * Test class for {@link OPSFileWriter}.
 */
class OPSFileWriterTest {

  @TempDir
  File tempDir;

  @Test
  void testWriteFiles() throws IOException, OPSInvalidVersionException {
    final OPSConfiguration cfo = mock(OPSConfiguration.class, withSettings().useConstructor(1).defaultAnswer(CALLS_REAL_METHODS));

    doReturn(new File("[OPS_root]")).when(cfo).getVersionsRoot();
    doReturn(tempDir).when(cfo).getRunFilesDirectory();

    final OPSFileWriter fileWriter = new OPSFileWriter(cfo);
    final String runId = "test";
    final OPSInputData data = OPSTestUtil.getInputTestData(EmissionResultKey.NH3_CONCENTRATION, 2020, OPSOptions.Chemistry.ACTUAL);
    final ControlFileOptions options = new ControlFileOptions(data, Substance.NH3, false, null, Set.of(3100));
    final File runDirectory = FileUtil.createDirectory(cfo.getRunFilesDirectory(), null);

    fileWriter.writeFiles(runId, runDirectory, data.getEmissionSources().get(1), data.getReceptors(), options);
    assertTrue(runDirectory.exists(), "Run ID directory should exist.");
    assertEquals(3, runDirectory.listFiles().length, "Should be 3 files created");
  }
}
