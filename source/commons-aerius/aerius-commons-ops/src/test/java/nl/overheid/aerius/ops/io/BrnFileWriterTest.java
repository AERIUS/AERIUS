/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.domain.OPSSource.OPSSourceOutflowDirection;
import nl.overheid.aerius.ops.util.OPSTestUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.geo.EPSG;

/**
 * Test class for {@link BrnFileWriter}
 */
class BrnFileWriterTest {

  private static final String EMISSION4 = "io/source/emission_v4_%s.brn";
  private static final String WITH_HEAT_CONTENT_AND_STACK = "with_heat_content_and_stack";
  private static final String NO_HEAT_CONTENT_WITH_STACK = "no_heat_content_with_stack";
  private static final String WITH_HEAT_CONTENT_NO_STACK = "with_heat_content_no_stack";

  @TempDir
  File tempDir;

  static List<Object[]> data() throws FileNotFoundException {
    return Arrays.asList(
        new Object[] {WITH_HEAT_CONTENT_AND_STACK},
        new Object[] {NO_HEAT_CONTENT_WITH_STACK},
        new Object[] {WITH_HEAT_CONTENT_NO_STACK});
  }

  @ParameterizedTest
  @MethodSource("data")
  void writeFileTest(final String name) throws IOException {
    BrnFileWriter.writeFile(tempDir, createOPSSourceData(name), Substance.NOX, null);
    final String output = OPSTestUtil.readFile(new File(tempDir, BrnFileWriter.getFileName()));
    final String reference = OPSTestUtil.readFile(String.format(EMISSION4, name));

    assertEquals(reference, output, "Check writing and then reading file results in same data");
  }

  private static List<OPSSource> createOPSSourceData(final String name) {
    switch (name) {
    case WITH_HEAT_CONTENT_AND_STACK:
      return dataWithHeatContentAndStack();
    case NO_HEAT_CONTENT_WITH_STACK:
      return dataNoHeatContentWithStack();
    case WITH_HEAT_CONTENT_NO_STACK:
      return dataWithHeatContentNoStack();
    }
    return Collections.emptyList();
  }

  private static OPSSource create() {
    return create(139123, 434456, 2.5E-01, 3.5f, 0, 0.2f, 4, 0, Substance.NOX);
  }

  private static List<OPSSource> dataWithHeatContentAndStack() {
    final OPSSource source = create();

    setHeatConcent(source);
    setStack(source, OPSSourceOutflowDirection.HORIZONTAL);
    return Collections.singletonList(source);
  }

  private static List<OPSSource> dataNoHeatContentWithStack() {
    final OPSSource source1 = create();
    final OPSSource source2 = create();

    source1.setHeatContent(BrnConstants.HEAT_CONTENT_NOT_APPLICABLE);
    source2.setHeatContent(BrnConstants.HEAT_CONTENT_NOT_APPLICABLE);
    setStack(source1, OPSSourceOutflowDirection.HORIZONTAL);
    setStack(source2, OPSSourceOutflowDirection.VERTICAL);
    return Arrays.asList(source1, source2);
  }

  private static List<OPSSource> dataWithHeatContentNoStack() {
    final OPSSource source = create();

    setHeatConcent(source);
    return Collections.singletonList(source);
  }

  private static void setHeatConcent(final OPSSource source) {
    source.setHeatContent(8.5E-01);
  }

  private static void setStack(final OPSSource source, final OPSSourceOutflowDirection direction) {
    source.setOutflowDiameter(3.3);
    source.setOutflowVelocity(4.4);
    source.setEmissionTemperature(20.1);
    source.setOutflowDirection(direction);
  }

  private static OPSSource create(final int x, final int y, final double q, final float h, final int d,
      final float s, final int dv, final int psd, final Substance substance) {
    final OPSSource e = new OPSSource(0, EPSG.RDNEW.getSrid(), x, y);

    // Emission in test data is in g/s, while expected in kg/j therefore
    // multiply with EMISSION_IN_G_PER_S_FACTOR.
    e.setEmission(substance, q * Substance.EMISSION_IN_G_PER_S_FACTOR);
    e.setDiameter(d);
    e.setEmissionHeight(h);
    e.setSpread(s);
    e.setDiurnalVariation(dv);
    e.setParticleSizeDistribution(psd);
    e.setComp(substance.getName());
    return e;
  }
}
