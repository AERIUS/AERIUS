/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.ops.runner;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.ops.domain.OPSConfiguration;
import nl.overheid.aerius.ops.exception.OPSInvalidVersionException;
import nl.overheid.aerius.ops.version.OPSVersion;

/**
 * Test OPSVersion check.
 */
class OPSVersionTest {

  @Test
  void testInvalidVersion() throws IOException {
    final OPSConfiguration config = new OPSConfiguration(1);
    config.setVersionsRoot(new File(""));
    final OPSSingleSubstanceRunner runner = new OPSSingleSubstanceRunner(config);
    final Runtime runtime = mock(Runtime.class);
    final Process process = mock(Process.class);
    final InputStream inputStream = new ByteArrayInputStream("no version".getBytes());
    final InputStream errorStream = new ByteArrayInputStream(new byte[0]);

    doReturn(inputStream).when(process).getInputStream();
    doReturn(errorStream).when(process).getErrorStream();
    runner.setRuntime(runtime);
    doReturn(process).when(runtime).exec((String[]) any(), any(), any());

    assertThrows(OPSInvalidVersionException.class, () -> runner.versionCheck(OPSVersion.LATEST),
        "Should thrown an OPSInvalidVersionException on invalid OPS version.");
  }
}
