/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.ops.exception.OPSInvalidVersionException;
import nl.overheid.aerius.ops.io.ResultDataFileReader;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 *  Test class for RunOPS on several different substances.
 *  All tests will be ignored when OPS is not installed in the right location
 *  (see worker.properties file in src/test/resources), they are just won't test anything.
 */
@ExtendWith(MockitoExtension.class)
class RunOPSSubstancesTest extends OPSRunnerTestBase {

  private static final String EXPECTED_NOX = "runopsNOx/" + ResultDataFileReader.FILE_NAME;
  private static final String EXPECTED_NH3 = "runopsNH3/" + ResultDataFileReader.FILE_NAME;

  static List<Arguments> data() {
    return List.of(
        //without terrain stuff tests:
        Arguments.of(EmissionResultKey.NOX_DEPOSITION, EXPECTED_NOX),
        Arguments.of(EmissionResultKey.NOX_CONCENTRATION, EXPECTED_NOX),
        Arguments.of(EmissionResultKey.NOX_DRY_DEPOSITION, EXPECTED_NOX),
        Arguments.of(EmissionResultKey.NOX_WET_DEPOSITION, EXPECTED_NOX),

        Arguments.of(EmissionResultKey.NH3_DEPOSITION, EXPECTED_NH3),
        Arguments.of(EmissionResultKey.NH3_CONCENTRATION, EXPECTED_NH3),
        Arguments.of(EmissionResultKey.NH3_DRY_DEPOSITION, EXPECTED_NH3),
        Arguments.of(EmissionResultKey.NH3_WET_DEPOSITION, EXPECTED_NH3));
  }

  @ParameterizedTest
  @MethodSource("data")
  void testRunOPS(final EmissionResultKey key, final String expected) throws IOException, AeriusException, OPSInvalidVersionException {
    mockOPSOutputFiles(key.getSubstance().name() + '-' + key.getEmissionResultType().name());
    final ResultDataFileReader reader = new ResultDataFileReader();
    assertRunOPS(reader, key, expected, true);
  }
}
