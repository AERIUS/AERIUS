/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.calculation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.sql.SQLException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.ops.LandUse;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.ops.OPSTerrainProperties;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link OPSReceptorFactory}.
 */
class OPSReceptorFactoryTest {

  private OPSReceptor point;

  @BeforeEach
  void before() {
    point = new OPSReceptor(1, 1000, 2000);
  }

  @Test
  void testCreateEmpty() throws SQLException, AeriusException {
    point = OPSReceptorFactory.createReceptor(point);
    assertEquals(1000, point.getRoundedX(), "Receptor X value.");
    assertEquals(2000, point.getRoundedY(), "Receptor Y value.");
    assertEquals(1, point.getId(), "Receptor ID.");
    assertNull(point.getTerrainProperties(), "Recepter has no terrain properties.");
    assertNull(point.getHeight(), "Receptor has no height");
  }

  @Test
  void testCreateWithOnlyAvRoughness() throws SQLException, AeriusException {
    //converted avg roughness and landuse should be null when supplied avg roughness and/or landuse supplied are null
    point.setTerrainProperties(new OPSTerrainProperties(1.1, null));
    point = OPSReceptorFactory.createReceptor(point);
    assertNull(point.getTerrainProperties(), "Recepter has no terrain properties.");
    assertNull(point.getHeight(), "Receptor has no height");
  }

  @Test
  void testCreateWithOnlyLandUse() throws SQLException, AeriusException {
    point.setTerrainProperties(new OPSTerrainProperties(null, LandUse.DECIDUOUS_FOREST));
    point = OPSReceptorFactory.createReceptor(point);
    assertNull(point.getTerrainProperties(), "Recepter has no terrain properties.");
    assertNull(point.getHeight(), "Receptor has no height");
  }

  @Test
  void testCreateWithLandUseAndAv() throws SQLException, AeriusException {
    point.setTerrainProperties(new OPSTerrainProperties(1.1, LandUse.DECIDUOUS_FOREST));
    point = OPSReceptorFactory.createReceptor(point);
    assertEquals(1.1, point.getTerrainProperties().getAverageRoughness().doubleValue(), 0.0001, "Receptor average roughness.");
    assertEquals(LandUse.DECIDUOUS_FOREST.getOption(), point.getTerrainProperties().getLandUse().getOption(), "Receptor dominant land use.");
    assertNull(point.getTerrainProperties().getLandUses(), "Receptor has no land use fractions.");
    assertNull(point.getHeight(), "Receptor has no height");
  }

  @Test
  void testCreateWithOnlyHeight() throws SQLException, AeriusException {
    point.setHeight(32.1);
    point = OPSReceptorFactory.createReceptor(point);
    assertNull(point.getTerrainProperties(), "Recepter has no terrain properties.");
    assertEquals(32.1, point.getHeight(), 0.0001, "Receptor height");
  }

  @Test
  void testCreateFull() throws SQLException, AeriusException {
    point.setTerrainProperties(new OPSTerrainProperties(1.1, LandUse.DECIDUOUS_FOREST, new int[] {20, 20, 30, 30, 0, 0, 0, 0, 0}));
    point.setHeight(32.1);
    point = OPSReceptorFactory.createReceptor(point);
    assertEquals(1.1, point.getTerrainProperties().getAverageRoughness().doubleValue(), 0.0001, "Receptor average roughness.");
    assertEquals(LandUse.DECIDUOUS_FOREST.getOption(), point.getTerrainProperties().getLandUse().getOption(), "Receptor dominant land use.");
    assertEquals(20, point.getTerrainProperties().getLandUses()[0], "Receptor first land use fraction.");
    assertEquals(0, point.getTerrainProperties().getLandUses()[8], "Receptor last land use fraction.");
    assertEquals(32.1, point.getHeight(), 0.0001, "Receptor height");
  }
}
