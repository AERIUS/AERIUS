/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import nl.overheid.aerius.ops.util.OPSTestUtil;
import nl.overheid.aerius.shared.domain.ops.LandUse;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.ops.OPSTerrainProperties;

/**
 * Test class for {@link RcpFileWriter}
 *
 */
class RcpFileWriterTest {

  private static final String RECEPTORS1 = "io/receptor/receptor_1.rcp";
  private static final String RECEPTORS2 = "io/receptor/receptor_2.rcp";
  private static final String RECEPTORS_WITH_HEIGHT = "io/receptor/receptor_3.rcp";
  private static final String RECEPTORS_WITH_HEIGHT_AND_TERRAIN = "io/receptor/receptor_4.rcp";

  @TempDir File tempDir;

  @Test
  void writeFileTest() throws IOException {
    final ArrayList<OPSReceptor> receptors = createReceptorData();

    clearHeights(receptors);
    clearTerrains(receptors);

    RcpFileWriter.writeFile(tempDir, receptors);
    final String output = OPSTestUtil.readFile(new File(tempDir, RcpFileWriter.getFileName()));
    final String reference = OPSTestUtil.readFile(RECEPTORS1);

    assertEquals(reference, output, "Read receptors file should be same as reference file.");
  }

  @Test
  void writeFileWithHeightTest() throws IOException {
    final ArrayList<OPSReceptor> receptors = createReceptorData();

    clearTerrains(receptors);

    RcpFileWriter.writeFile(tempDir, receptors);
    final String output = OPSTestUtil.readFile(new File(tempDir, RcpFileWriter.getFileName()));
    final String reference = OPSTestUtil.readFile(RECEPTORS_WITH_HEIGHT);

    assertEquals(reference, output, "Read receptors file should be same as reference file.");
  }

  @Test
  void writeFileWithTerrainTest() throws IOException {
    final ArrayList<OPSReceptor> receptors = createReceptorData();

    clearHeights(receptors);

    RcpFileWriter.writeFile(tempDir, receptors);
    final String output = OPSTestUtil.readFile(new File(tempDir, RcpFileWriter.getFileName()));
    final String reference = OPSTestUtil.readFile(RECEPTORS2);

    assertEquals(reference, output, "Read receptors file with terrain data should be same as reference file.");
  }

  @Test
  void writeFileWithTerrainAndHeightTest() throws IOException {
    final ArrayList<OPSReceptor> receptors = createReceptorData();

    RcpFileWriter.writeFile(tempDir, receptors);
    final String output = OPSTestUtil.readFile(new File(tempDir, RcpFileWriter.getFileName()));
    final String reference = OPSTestUtil.readFile(RECEPTORS_WITH_HEIGHT_AND_TERRAIN);

    assertEquals(reference, output, "Read receptors file with terrain data should be same as reference file.");
  }

  private ArrayList<OPSReceptor> createReceptorData() {
    final ArrayList<OPSReceptor> data = new ArrayList<>();

    create(data, 3310760, 106206, 419024);
    create(data, 3324315, 106485, 419507);
    create(data, 3303231, 106485, 418756);
    create(data, 3325821, 106392, 419561);
    create(data, 3316786, 106578, 419239);
    create(data, 3304735, 106020, 418809);
    create(data, 3304739, 106764, 418809);
    double height = 9.87654321;
    double z0 = 1.23456789;
    int landuse = 0;
    for (final OPSReceptor receptor : data) {
      final OPSTerrainProperties terrainProperties;
      if (landuse == 0) {
        terrainProperties = new OPSTerrainProperties(z0, LandUse.values()[landuse++ % LandUse.values().length],
            new int[] {60, 0, 0, 0, 0, 0, 0, 0, 40});
      } else {
        terrainProperties = new OPSTerrainProperties(z0, LandUse.values()[landuse++ % LandUse.values().length]);
      }
      receptor.setTerrainProperties(terrainProperties);
      z0 += z0;
      receptor.setHeight(height);
      height += height;
    }
    return data;
  }

  private void clearHeights(final ArrayList<OPSReceptor> receptors) {
    receptors.forEach(receptor -> receptor.setHeight(null));
  }

  private void clearTerrains(final ArrayList<OPSReceptor> receptors) {
    receptors.forEach(receptor -> {
      receptor.setTerrainProperties(null);
    });
  }

  private static void create(final ArrayList<OPSReceptor> data, final int id, final int x, final int y) {
    data.add(new OPSReceptor(id, x, y));
  }

}
