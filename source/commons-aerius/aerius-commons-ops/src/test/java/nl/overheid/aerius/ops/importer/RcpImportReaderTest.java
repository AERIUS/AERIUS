/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.importer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.io.ImportReader;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link RcpImportReader}.
 */
class RcpImportReaderTest {

  private static final String FILENAME = "RcpImportReader.rcp";
  private InputStream inputStream;

  @BeforeEach
  void before() {
    inputStream = getClass().getResourceAsStream(FILENAME);
  }

  @AfterEach
  void after() throws IOException {
    inputStream.close();
  }

  @Test
  void detectTest() {
    final ImportReader ir = new RcpImportReader(false, false);
    assertTrue(ir.detect("receptors.rcp"), "Detect extension correcty");
    assertTrue(ir.detect("RECEPTORS.RCP"), "Detect extension case insane");
    assertFalse(ir.detect("receptors"), "Not detect other extensions");
  }

  @Test
  void testRCPWithErrors() throws IOException, AeriusException {
    final ImportReader ir = new RcpImportReader(false, false);
    final ImportParcel result = new ImportParcel();
    ir.read(FILENAME, inputStream, null, null, result);
    assertEquals(6, result.getCalculationPointsList().size(), "Should have read # rows correctly");
    assertEquals(1, result.getExceptions().size(),  "Should have read # errors");
    assertEquals(2, result.getCalculationPointsList().get(1).getProperties().getId(), "Should have autonumbered id's");
  }

  @Test
  void testRCPWithNameAsId() throws IOException, AeriusException {
    final ImportReader ir = new RcpImportReader(true, false);
    final ImportParcel result = new ImportParcel();
    ir.read(FILENAME, inputStream, null, null, result);
    assertEquals(3324315, result.getCalculationPointsList().get(1).getProperties().getId(), "Should have read name as id");
  }

}
