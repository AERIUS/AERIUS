/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops;

import static nl.overheid.aerius.ops.util.ResultDataFileReaderHelper.assertResults;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.exception.OPSInvalidVersionException;
import nl.overheid.aerius.ops.io.ResultDataFileReader;
import nl.overheid.aerius.ops.util.OPSTestUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.OPSOptions;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class to run OPS with WGS84 coordinates.
 */
@ExtendWith(MockitoExtension.class)
class RunOPSWGS84Test extends OPSRunnerTestBase {
  private static final String RESOURCES_DIRECTORY = "runops_wgs84/";
  private static final String RECEPTOR_FILE = "receptors.rcp";
  private static final String EMISSION_SOURCE_FILE = "emissions.brn";
  private static final String EXPECTED_RESULT_FILE = ResultDataFileReader.FILE_NAME;

  static List<Arguments> data() {
    return List.of(
        Arguments.of("testcase_road_11", Substance.NOX),
        Arguments.of("testcase_farm_17", Substance.NH3));
  }

  @ParameterizedTest
  @MethodSource("data")
  void testRunWGS84Points(final String testFolder, final Substance substance) throws IOException, AeriusException, OPSInvalidVersionException {
    mockOPSOutputFiles(testFolder);
    final String testResourcesPath = RESOURCES_DIRECTORY + testFolder + "/";
    final String sourcesFile = testResourcesPath + EMISSION_SOURCE_FILE;
    final String receptorFile = testResourcesPath + RECEPTOR_FILE;
    final EmissionResultKey erk = EmissionResultKey.valueOf(substance);
    final OPSInputData inputData = OPSTestUtil.getInputTestData(erk, YEAR, OPSOptions.Chemistry.ACTUAL, sourcesFile, receptorFile);

    setSources(inputData, sourcesFile, substance);
    setReceptors(inputData, receptorFile);
    final CalculationResult result = runOPS.run(inputData);
    final String message = "EPSG";
    final String resultFile = testResourcesPath + EXPECTED_RESULT_FILE;
    assertResults(RunOPSWGS84Test.class, message, new ResultDataFileReader(), resultFile, erk, result.getResults().get(1), true);
  }
}
