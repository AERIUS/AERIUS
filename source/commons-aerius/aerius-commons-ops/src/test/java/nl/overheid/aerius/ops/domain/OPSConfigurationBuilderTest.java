/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import nl.overheid.aerius.ops.OPSWorkerFactory;
import nl.overheid.aerius.ops.version.OPSVersion;
import nl.overheid.aerius.worker.ConfigurationBuilder;
import nl.overheid.aerius.worker.PropertiesUtil;

/**
 * Test class for {@link OPSConfigurationBuilder}.
 */
class OPSConfigurationBuilderTest {

  @TempDir
  Path tempDir;

  @Test
  void testBuilder() throws IOException {
    final OPSWorkerFactory factory = new OPSWorkerFactory();
    final Properties props = PropertiesUtil.getFromTestPropertiesFile("ops");
    // Ensure ops.root directory exists by using temp dir as location.
    props.put("ops.root", tempDir.toString());
    final ConfigurationBuilder<OPSConfiguration> builder = factory.configurationBuilder(props);
    final Optional<OPSConfiguration> opt = builder.buildAndValidate();

    assertTrue(opt.isPresent(), "Configuration has processes, therefor should be active");
    final OPSConfiguration configuration = opt.get();

    assertEquals(4, configuration.getProcesses(), "Configuration should have expected number of processes");
    assertEquals(List.of(), builder.getValidationErrors(), "Should not have validation errors.");
    assertNotNull(configuration.getOpsRoot(OPSVersion.LATEST), "Should return a none null root");
    assertNotNull(configuration.getRunFilesDirectory(), "Should return a run directory");
  }
}
