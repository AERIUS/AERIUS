/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.geo.EPSG;

/**
 * Test class for {@link OPSSource}
 */
class OPSSourceTest {

  @Test
  void testHashCodeEquals() {
    final OPSSource s1 = createOPSSource(3265, 195500.0, 431500.0, 0.0, 1.0, 1000, 0.5, 0, 0);
    final OPSSource s2 = createOPSSource(3265, 195500.0, 431500.0, 0.0, 1.0, 1000, 0.5, 0, 0);
    assertEquals(s1.hashCode(), s2.hashCode(), "Hashcode should be the same");
  }

  @Test
  void testOPSSourceEquals() {
    final OPSSource s1 = createOPSSource(3265, 195500.0, 431500.0, 0.0, 1.0, 1000, 0.5, 0, 0);
    final OPSSource s2 = createOPSSource(3265, 195500.0, 431500.0, 0.0, 1.0, 1000, 0.5, 0, 0);
    assertEquals(s1, s2, "Equals should be the same");
  }

  @Test
  void testHashCodeNotEquals() {
    final OPSSource s1 = createOPSSource(3265, 195500.0, 431500.0, 0.0, 1.0, 1000, 0.5, 0, 0);
    final OPSSource s2 = createOPSSource(6014, 197500.0, 307500.0, 0.0, 1.0, 1000, 0.5, 0, 0);
    assertNotEquals(s1.hashCode(), s2.hashCode(), "Hashcode should not be the same");
  }

  @Test
  void testOPSSourceNotEquals() {
    final OPSSource s1 = createOPSSource(3265, 195500.0, 431500.0, 0.0, 1.0, 1000, 0.5, 0, 0);
    final OPSSource s2 = createOPSSource(6014, 197500.0, 307500.0, 0.0, 1.0, 1000, 0.5, 0, 0);
    assertNotEquals(s1, s2, "Equals should not be the same");
  }

  private OPSSource createOPSSource(final int id, final double x, final double y, final double hc, final double h, final int d, final double s,
      final int dv, final int psd) {
    final OPSSource src = new OPSSource(id, EPSG.RDNEW.getSrid(), x, y);
    src.setHeatContent(hc);
    src.setEmissionHeight(h);
    src.setDiameter(d);
    src.setSpread(s);
    src.setDiurnalVariation(dv);
    src.setParticleSizeDistribution(psd);
    return src;
  }
}
