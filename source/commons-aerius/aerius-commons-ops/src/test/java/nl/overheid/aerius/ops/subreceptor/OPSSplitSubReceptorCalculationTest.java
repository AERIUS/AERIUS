/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.subreceptor;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;

/**
 * Test class for {@link OPSSplitSubReceptorCalculation}.
 */
class OPSSplitSubReceptorCalculationTest {

  @ParameterizedTest()
  @MethodSource("data")
  void testSplit(final int y1, final int y2, final int splitDistance, final boolean withSub, final int sizeSourcesNoSub,
      final int sizeSourcesWithSub, final int diameter) {
    final List<OPSSource> sources = List.of(
        new OPSSource(1, 0, y1, 0),
        new OPSSource(2, 0, y2, 0));
    sources.stream().forEach(s -> s.setDiameter(diameter));
    final List<OPSReceptor> receptors = new ArrayList<>();
    receptors.add(new OPSReceptor(new AeriusPoint(123, 0, AeriusPointType.RECEPTOR, 10, 0)));
    receptors.add(new OPSReceptor(new AeriusPoint(0, 123, AeriusPointType.SUB_RECEPTOR, 10, 0)));
    if (withSub) {
      receptors.add(new OPSReceptor(new AeriusPoint(4, 123, AeriusPointType.SUB_RECEPTOR, 11, 0)));
    }
    final OPSSplitSubReceptorCalculation splitter = OPSSplitSubReceptorCalculation.split(sources, receptors, splitDistance);

    assertEquals(1, splitter.getReceptorsWithOutSubreceptors().size(), "Only the receptor should remain");
    assertEquals(123, ((List<OPSReceptor>) splitter.getReceptorsWithOutSubreceptors()).get(0).getId(), "The receptor itself should be in the list");

    assertEquals(sizeSourcesNoSub, splitter.getSourcesNoSubReceptors().size(),
        "Only " + sizeSourcesNoSub + " source should be in the NO subreceptors list");

    assertEquals(sizeSourcesWithSub, splitter.getSourcesWithSubReceptors().size(),
        "Only " + sizeSourcesWithSub + " source should be in the WITH subreceptors list");
  }

  static List<Arguments> data() {
    //               y1  y2  distance, sources_no_sub, source_with_sub, diameter
    return List.of(
        // All sources within split distance
        // There are subreceptors -> All sources should be in sourcesWithSubReceptors
        Arguments.of(100, 400, 700, true, 0, 2, 0),
        // There are NO subreceptors -> All sources should be in sourcesWithSubReceptors
        Arguments.of(400, 400, 700, false, 0, 2, 0),

        // All sources beyond the split distance
        // There are subreceptors -> All sourcesWithoutSubReceptors
        Arguments.of(100, 400, 70, true, 2, 0, 0),
        // There are NO subreceptors -> All sourcesWithoutSubReceptors
        Arguments.of(400, 400, 70, false, 2, 0, 0),

        // Some sources within distance, others beyond distance
        // There are subreceptors -> split into 2 groups
        Arguments.of(100, 1000, 700, true, 1, 1, 0),
        // There are NO subreceptors -> split into 2 groups
        Arguments.of(100, 1000, 700, false, 1, 1, 0),

        // Source with diameter are all within distance
        // There are subreceptors -> All sources should be in sourcesWithSubReceptors
        Arguments.of(100, 1000, 700, true, 0, 2, 400),
        // There are NO subreceptors -> All sources should be in sourcesWithSubReceptors
        Arguments.of(100, 1000, 700, false, 0, 2, 400));
  }
}
