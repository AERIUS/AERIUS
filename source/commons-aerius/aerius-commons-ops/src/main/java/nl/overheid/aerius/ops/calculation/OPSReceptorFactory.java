/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.calculation;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;

/**
 * Factory class to create an OPSReceptor from a given receptor.
 */
public final class OPSReceptorFactory {

  private OPSReceptorFactory() {
    // Util class
  }

  public static OPSReceptor createReceptor(final AeriusPoint receptor) {
    final boolean isOpsReceptor = receptor instanceof OPSReceptor;
    final OPSReceptor opsReceptor = isOpsReceptor ? (OPSReceptor) receptor : new OPSReceptor(receptor);

    if (isOpsReceptor) {
      cleanIfIncomplete(opsReceptor);
    }
    return opsReceptor;
  }

  private static void cleanIfIncomplete(final OPSReceptor opsReceptor) {
    if (opsReceptor.hasNoTerrainData()) {
      opsReceptor.setTerrainProperties(null);
    }
  }
}
