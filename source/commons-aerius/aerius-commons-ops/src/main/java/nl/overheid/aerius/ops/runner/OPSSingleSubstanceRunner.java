/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.runner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.exec.ExecuteParameters;
import nl.overheid.aerius.exec.ExecuteWrapper;
import nl.overheid.aerius.exec.ExecuteWrapper.StreamGobblers;
import nl.overheid.aerius.ops.domain.OPSConfiguration;
import nl.overheid.aerius.ops.exception.OPSInvalidVersionException;
import nl.overheid.aerius.ops.exception.OPSRetryException;
import nl.overheid.aerius.ops.io.ResultDataFileReader;
import nl.overheid.aerius.ops.version.OPSVersion;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.OSUtils;

/**
 * Performs a OPS run.
 */
public class OPSSingleSubstanceRunner {

  private static final Logger LOGGER = LoggerFactory.getLogger(OPSSingleSubstanceRunner.class);

  private static final String VERSION_FLAG = "-r";

  private static final int MAX_RUN_RETRIES = 5;
  private static final int RETRY_WAIT_MILLISECONDS = 10;

  private static final String OPS_BIN = "bin";
  private static final String EXECUTABLE_NAME = OSUtils.isWindows() ? "ops.exe" : "ops";

  private final OPSConfiguration configuration;

  /**
   * Set runtime for testing purposes.
   */
  private Runtime runtime;


  public OPSSingleSubstanceRunner(final OPSConfiguration configuration) {
    this.configuration = configuration;
  }

  public void setRuntime(final Runtime runtime) {
    this.runtime = runtime;
  }

  /**
   * Returns true if the OPS binary executable is executable.
   *
   * @param opsRootDir OPS root directory
   * @return true if executable
   */
  public boolean isExecutable(final Path opsRootDir) {
    return Files.isExecutable(opsRootDir.resolve(OPS_BIN).resolve(EXECUTABLE_NAME));
  }

  /**
   * Runs OPS and checks if the binary returns the version number we expect.
   *
   * @throws IOException
   * @throws InterruptedException
   * @throws OPSInvalidVersionException
   */
  public void versionCheck(final OPSVersion opsVersion) throws IOException, InterruptedException, OPSInvalidVersionException {
    final OPSVersionStreamGobblers streamGobblers = new OPSVersionStreamGobblers(opsVersion);
    final ExecuteWrapper wrapper = createWrapper(List.of(VERSION_FLAG), streamGobblers, opsVersion);

    if (runtime != null) { // set test runtime if present
      wrapper.setRuntime(runtime);
    }
    wrapper.run("1", null);
    if (streamGobblers.getVersionException() != null) {
      throw streamGobblers.getVersionException();
    }
  }

  /**
   * Performs the actual run of OPS. Including creating a directory for the
   * configuration and output files, generating the configuration files,
   * reading the OPS output and cleaning up the files if execution was
   * successful.
   *
   * @param sources the sources to run
   * @param receptors the receptors to run
   * @param erks the emission result keys to get the results for
   * @param controlFileOptions
   * @param runnerParameters
   * @param opsVersion the version of OPS to run
   * @return output data of this run
   * @throws IOException when reading/writing files failed
   * @throws AeriusException
   */
  public void run(final String runId, final File targetDirectory, final List<String> parameters, final OPSVersion opsVersion)
      throws IOException, AeriusException {
    // construct the execute worker.
    final OPSStreamGobblers gobblers = new OPSStreamGobblers();
    final ExecuteWrapper executeWrapper = createWrapper(parameters, gobblers, opsVersion);

    if (runtime != null) { // set test runtime if present
      executeWrapper.setRuntime(runtime);
    }
    // perform the actual ops run.
    performRun(runId, targetDirectory, executeWrapper, gobblers);
  }

  private ExecuteWrapper createWrapper(final List<String> parameters, final StreamGobblers streamGobblers, final OPSVersion opsVersion) {
    final ExecuteParameters executeParameters = new ExecuteParameters(EXECUTABLE_NAME, parameters.toArray(new String[parameters.size()]));
    final File opsBinDir = new File(configuration.getOpsRoot(opsVersion), OPS_BIN);
    return new ExecuteWrapper(executeParameters, opsBinDir.getAbsolutePath(), streamGobblers);
  }

  protected void performRun(final String runId, final File physicalRunFilesDirectory, final ExecuteWrapper executeWrapper,
      final OPSStreamGobblers gobblers) throws IOException, AeriusException {
    int runTries = 1;
    try {
      //When run concurrently, OPS can run into troubles reading it's own files.
      //To avoid crashing the calculation, we're retrying a couple of times.
      while (runTries <= MAX_RUN_RETRIES) {
        try {
          executeWrapper.run(runId, physicalRunFilesDirectory);
          // Check if there are any error files.
          ResultDataFileReader.checkForErrors(physicalRunFilesDirectory);
          // Check if there were any errors in the std error
          checkGobblerErrors(gobblers.getError());
          break;
        } catch (final OPSRetryException e) {
          LOGGER.error("Error while running OPS. This was try: {}. Errormsg: {}", runTries, e.getArgs());
          if (runTries >= MAX_RUN_RETRIES) {
            LOGGER.error("Finished trying, throwing an IOException so this worker dies.");
            //throwing an IOException instead so the worker dies instead of the calculation. Another worker can probably handle it.
            throw new IOException(e);
          } else {
            ResultDataFileReader.clearErrorFile(physicalRunFilesDirectory);
            Thread.sleep(TimeUnit.MILLISECONDS.toMillis(RETRY_WAIT_MILLISECONDS));
            runTries++;
          }
        }
      }
    } catch (final InterruptedException e) {
      Thread.currentThread().interrupt();
      LOGGER.error("RunOPS#performRun", e);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }

  /**
   * If exception is not null throw the exception.
   *
   * @param exception exception to check
   * @throws AeriusException
   */
  private static void checkGobblerErrors(final AeriusException exception) throws AeriusException {
    if (exception != null) {
      throw exception;
    }
  }
}
