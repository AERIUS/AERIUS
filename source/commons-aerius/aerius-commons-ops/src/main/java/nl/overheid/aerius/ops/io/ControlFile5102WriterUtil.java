/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import java.util.Map;
import java.util.Optional;

import nl.overheid.aerius.ops.version.OPSVersion;
import nl.overheid.aerius.shared.domain.calculation.OPSOptions;

/**
 * Specific control file writing functionality for OPS version 5.1.0.2
 */
public class ControlFile5102WriterUtil implements ControlFileWriterUtil {

  private static final String DEFAULT_METEO_FILE = "m005114c.*";

  @Override
  public String getDefaultLandUse() {
    return "lgn2020";
  }

  @Override
  public String getDefaultMeteoFile() {
    return DEFAULT_METEO_FILE;
  }

  @Override
  public boolean writeOPSSetting(final OPSSetting setting, Map<OPSSetting, String> settings) {
    return setting != OPSSetting.ROADSopt;
  }

  @Override
  public String getYear(final int year, final boolean isOwN2000, final OPSVersion opsVersion, final OPSOptions.Chemistry chemistry) {
    int opsYear = year;
    if (isOwN2000 && opsYear >= opsVersion.getCurrentYear()) {
      opsYear = opsVersion.getPrognoseYear();
    }

    final OPSOptions.Chemistry chemistryToUse = Optional.ofNullable(chemistry)
        .orElse(OPSOptions.Chemistry.PROGNOSIS);

    return chemistryToUse == OPSOptions.Chemistry.ACTUAL
        ? String.valueOf(opsYear)
        : String.join(" ", String.valueOf(opsYear), chemistryToUse.getControlFileLabel());
  }
}
