/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.exception;


/**
 * Exception to be thrown if a directory exists where it shouldn't have in an OPS run.
 */
public class OPSRunDirectoryExistsException extends RuntimeException {

  private static final long serialVersionUID = 7108897802373722304L;

  /**
   * @param message @see {@link OPSException}
   */
  public OPSRunDirectoryExistsException(final String message) {
    super(message);
  }
}
