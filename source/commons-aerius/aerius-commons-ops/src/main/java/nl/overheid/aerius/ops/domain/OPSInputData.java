/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.domain;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import nl.overheid.aerius.calculation.EngineInputData;
import nl.overheid.aerius.ops.version.OPSVersion;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.OPSOptions;
import nl.overheid.aerius.shared.domain.meteo.Meteo;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;

/**
 * Data class to combine all data needed for a single OPS run.
 */
public class OPSInputData extends EngineInputData<OPSSource, OPSReceptor> {

  private static final long serialVersionUID = 6L;

  private Meteo meteo;
  private final int hexagonSurfaceLevel1;
  private OPSOptions opsOptions;
  /**
   * If set it will pass the max distance option to OPS. This will limit calculation of source/receptors to 25km.
   */
  private boolean maxDistance;
  /**
   * If set the terrain properties will be read in the OPS worker. Otherwise, it depends if terrain properties are already present on the receptor.
   */
  private boolean forceTerrainProperties;
  /**
   * If set it will pass the min distance option to OPS. This will not calculate of source/receptors within 5km.
   */
  private final Set<Integer> minDistanceSet = new HashSet<>();
  /**
   * Sets the OPS categories (which matches the sector id) which should
   * be calculated with OPS ROADS option
   */
  private final Set<Integer> roadsCats = new HashSet<>();

  public OPSInputData(final OPSVersion opsVersion, final CommandType commandType, final int hexagonSurfaceLevel1) {
    super(Theme.OWN2000, CalculationEngine.OPS, opsVersion, commandType); // At this moment only OwN2000 is supported with OPS
    this.hexagonSurfaceLevel1 = hexagonSurfaceLevel1;
  }

  public Meteo getMeteo() {
    return meteo;
  }

  public void setMeteo(final Meteo meteo) {
    this.meteo = meteo;
  }

  public boolean isMaxDistance() {
    return maxDistance;
  }

  public void setMaxDistance(final boolean maxDistance) {
    this.maxDistance = maxDistance;
  }

  public boolean isForceTerrainProperties() {
    return forceTerrainProperties;
  }

  public void setForceTerrainProperties(final boolean forceTerrainProperties) {
    this.forceTerrainProperties = forceTerrainProperties;
  }

  public void addMinDistanceGroupId(final Integer groupId) {
    minDistanceSet.add(groupId);
  }

  public boolean minDistanceContains(final Integer groupId) {
    return minDistanceSet.contains(groupId);
  }

  public Set<Integer> getRoadsCats() {
    return roadsCats;
  }

  @Override
  public Collection<OPSReceptor> getReceptors() {
    return super.getReceptors();
  }

  public int getHexagonSurfaceLevel1() {
    return hexagonSurfaceLevel1;
  }

  public OPSOptions getOpsOptions() {
    return opsOptions;
  }

  public void setOpsOptions(final OPSOptions opsOptions) {
    this.opsOptions = opsOptions;
  }
}
