/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import nl.overheid.aerius.ops.io.RcpPM10FileReader;
import nl.overheid.aerius.ops.io.ResultDataFileReader;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Class to collect OPS output results from OPS output into the AERIUS internal data structure.
 */
final class OPSResultCollector {

  private OPSResultCollector() {
    // Util class.
  }

  /**
   * Collects results from the results file in the given directory.
   *
   * @param directory directory containing the results file
   * @param substance The substance this results where generated for
   * @param erks the emission result key values to obtain from the results
   * @return A list of result points containing the read results
   * @throws IOException
   */
  public static List<AeriusResultPoint> collectResults(final File directory, final Substance substance, final Set<EmissionResultKey> erks)
      throws IOException {
    final List<AeriusResultPoint> results;
    if (substance == Substance.PM10) {
      results = collectPMResults(directory, erks);
    } else { // NOx || NH3
      final ResultDataFileReader reader = new ResultDataFileReader();
      results = reader.readObjects(directory, substance, erks).getObjects();
    }
    return results;
  }

  private static List<AeriusResultPoint> collectPMResults(final File physicalRunFilesDirectory, final Set<EmissionResultKey> erks)
      throws IOException {
    //PM10 results are a bit weird and have to be handled differently.
    final RcpPM10FileReader reader = new RcpPM10FileReader();
    final Map<Substance, List<AeriusResultPoint>> pmMap = new EnumMap<>(Substance.class);

    if (erks.contains(EmissionResultKey.PM10_CONCENTRATION)) {
      pmMap.put(Substance.PM10, reader.readObjects(physicalRunFilesDirectory, Substance.PM10, EmissionResultKey.PM10_CONCENTRATION).getObjects());
    }
    if (erks.contains(EmissionResultKey.PM25_CONCENTRATION)) {
      reader.setResultSubstance(Substance.PM25);
      pmMap.put(Substance.PM25, reader.readObjects(physicalRunFilesDirectory, Substance.PM25, EmissionResultKey.PM25_CONCENTRATION).getObjects());
    }
    return mergeResults(pmMap);
  }

  /**
   * Merges the map of substances into one list with a results for the substances in 1 List, where each result point contains the results for all
   * available substance results.
   *
   * @param results Map of results per substance
   * @return List of result points with results for all available substances
   */
  public static List<AeriusResultPoint> mergeResults(final Map<Substance, List<AeriusResultPoint>> results) {
    final Map<AeriusResultPoint, AeriusResultPoint> points = new HashMap<>();
    final ArrayList<AeriusResultPoint> resultPoints = new ArrayList<>();

    for (final Entry<Substance, List<AeriusResultPoint>> entry : results.entrySet()) {
      for (final AeriusResultPoint arp : entry.getValue()) {
        if (points.containsKey(arp)) {
          points.get(arp).merge(arp);
        } else {
          resultPoints.add(arp);
          points.put(arp, arp);
        }
      }
    }
    return resultPoints;
  }
}
