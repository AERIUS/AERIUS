/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.version;

import nl.overheid.aerius.calculation.EngineVersion;
import nl.overheid.aerius.ops.io.ControlFile5102WriterUtil;
import nl.overheid.aerius.ops.io.ControlFile5300WriterUtil;
import nl.overheid.aerius.ops.io.ControlFileWriterUtil;

/**
 * Enum with the different versions that are supported.
 */
public enum OPSVersion implements EngineVersion {
  STABLE("5.1.2.0-1", "OPS-version: L-5.1.2.0 ; Release date: 15 nov 2023", "OPS-version: W-5.1.2.0 ; Release date: 15 nov 2023",
      2022, 2030, new ControlFile5102WriterUtil(), "ops_terrain_properties_20220128", false),
  LATEST("5.3.1.0-2", "OPS-version: L-5.3.1.0 ; Release date: 19 dec 2024", "OPS-version: W-5.3.1.0 ; Release date: 19 dec 2024",
      2024, 2030, new ControlFile5300WriterUtil(), "ops_terrain_properties_20220128", true),
  CHECK_STABLE("5.1.0.2", "OPS-version: L-5.1.0.2 ; Release date: 14 jul 2022", "OPS-version: W-5.1.0.2 ; Release date: 14 jul 2022",
      2022, 2030, new ControlFile5102WriterUtil(), "ops_terrain_properties_20220128", false);

  private final String version;
  private final String linuxVersion;
  private final String windowsVersion;
  /**
   * Sets the year after which OPS should switch to a fixed prognose year
   */
  private final int currentYear;
  /**
   * Sets the fixed prognose year to be used for calculations in the future
   */
  private final int prognoseYear;
  private final ControlFileWriterUtil controlFileWriterUtil;
  private final String terrainPropertiesVersion;
  private final boolean supportSigZ0PointSourceOption;

  OPSVersion(final String version, final String linuxVersion, final String windowsVersion, final int currentYear, final int prognoseYear,
      final ControlFileWriterUtil controlFileWriterUtil, final String terrainPropertiesVersion, final boolean supportSigZ0PointSourceOption) {
    this.version = version;
    this.linuxVersion = linuxVersion;
    this.windowsVersion = windowsVersion;
    this.currentYear = currentYear;
    this.prognoseYear = prognoseYear;
    this.controlFileWriterUtil = controlFileWriterUtil;
    this.terrainPropertiesVersion = terrainPropertiesVersion;
    this.supportSigZ0PointSourceOption = supportSigZ0PointSourceOption;
  }

  @Override
  public String getVersion() {
    return version;
  }

  public String getLinuxVersion() {
    return linuxVersion;
  }

  public String getWindowsVersion() {
    return windowsVersion;
  }

  public int getCurrentYear() {
    return currentYear;
  }

  public int getPrognoseYear() {
    return prognoseYear;
  }

  public ControlFileWriterUtil getControlFileUtil() {
    return controlFileWriterUtil;
  }

  public String getTerrainPropertiesVersion() {
    return terrainPropertiesVersion;
  }

  public boolean isSupportSigZ0PointSourceOption() {
    return supportSigZ0PointSourceOption;
  }

  /**
   * Map a versionLabel (explicit version or latest|stable) to OPSVersion type.
   * @param versionLabel the version label
   */
  public static OPSVersion getOPSVersionByLabel(final String versionLabel) {
    return EngineVersion.getByVersionLabel(OPSVersion.values(), "OPS", versionLabel);
  }
}
