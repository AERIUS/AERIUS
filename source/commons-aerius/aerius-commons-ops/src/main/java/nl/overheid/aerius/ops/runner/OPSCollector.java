/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.runner;

import java.util.Map;

import nl.overheid.aerius.shared.domain.Substance;

/**
 * Interface to implement to handle the returned results by an OPS run as returned by the {@link OPSRunner}.
 *
 * @param <T> type of data as returned by the {@link OPSRunner} run process.
 */
public interface OPSCollector<T> {

  /**
   * Collect results from multiple substance runs of OPS
   *
   * @param sourceKey source key of the sources
   * @param results results map with results per substance of the {@link OPSRunner}.
   */
  default void collect(final Integer sourceKey, final Map<Substance, T> results) {
  }
}
