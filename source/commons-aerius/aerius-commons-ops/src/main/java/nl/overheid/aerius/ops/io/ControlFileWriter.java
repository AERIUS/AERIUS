/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.ops.domain.GridSize;
import nl.overheid.aerius.ops.domain.OPSConfiguration;
import nl.overheid.aerius.ops.version.OPSVersion;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.OPSOptions;
import nl.overheid.aerius.shared.domain.meteo.Meteo;
import nl.overheid.aerius.util.OSUtils;

/**
 * Writes the OPS global configuration file to disk.
 */
final class ControlFileWriter {

  public static final String NO_DEFAULT = "";

  private static final Logger LOG = LoggerFactory.getLogger(ControlFileWriter.class);

  private static final int SECTION_WIDTH = 48;
  private static final int SETTING_KEY_WIDTH = 15;
  private static final String CONTROL_FILE_NAME = "settings.ctr";
  private static final String NL = OSUtils.NL;

  private static final String DATA_FOLDER = "data";
  private static final String METEO_FOLDER = "meteo";

  private static final GridSize DEFAULT_GRID_SIZE = GridSize.SIZE_250;

  private static final Locale FORMAT_LOCALE = Locale.ENGLISH;
  private static final String FORMAT_MOLWEIGHT = "%2.1f";
  private static final String FORMAT_ROUGHNESS = "%2.1f";
  private static final String FORMAT_METEO_SINGLE_YEAR = "a0%02d1%02dc.*";
  private static final String FORMAT_METEO_MULTI_YEAR = "m0%02d1%02dc.*";
  private static final int METEO_YEAR_MODULO = 100;
  private static final String ROADSOPT_DEAULT = "0 1";

  private enum ControlFileSection {

    DIRECTORY("directory layer",
        new OPSSetting[] {OPSSetting.DATADIR}),

    IDENTIFICATION("identification layer",
        new OPSSetting[] {OPSSetting.PROJECT, OPSSetting.RUNID, OPSSetting.YEAR,}),

    SUBSTANCE("substance layer",
        new OPSSetting[] {OPSSetting.COMPCODE, OPSSetting.COMPNAME, OPSSetting.MOLWEIGHT, OPSSetting.PHASE, OPSSetting.LOSS, OPSSetting.DDSPECTYPE,
            OPSSetting.DDPARVALUE, OPSSetting.WDSPECTYPE, OPSSetting.WDPARVALUE, OPSSetting.DIFFCOEFF, OPSSetting.WASHOUT, OPSSetting.CONVRATE,
            OPSSetting.LDCONVRATE,}),

    EMISSION("emission layer",
        new OPSSetting[] {OPSSetting.EMFILE, OPSSetting.USDVEFILE, OPSSetting.USPSDFILE, OPSSetting.EMCORFAC, OPSSetting.TARGETGROUP,
            OPSSetting.COUNTRY, OPSSetting.ROADS, OPSSetting.ROADSopt}),

    RECEPTOR("receptor layer",
        new OPSSetting[] {OPSSetting.RECEPTYPE, OPSSetting.XCENTER, OPSSetting.YCENTER, OPSSetting.NCOLS, OPSSetting.NROWS, OPSSetting.RESO,
            OPSSetting.OUTER, OPSSetting.RCPFILE,}),

    METEO_SURFACE("meteo & surface char layer",
        new OPSSetting[] {OPSSetting.ROUGHNESS, OPSSetting.Z0FILE, OPSSetting.LUFILE, OPSSetting.METEOTYPE, OPSSetting.MTFILE,}),

    OUTPUT("output layer",
        new OPSSetting[] {OPSSetting.DEPUNIT, OPSSetting.PLTFILE, OPSSetting.PRNFILE, OPSSetting.INCLUDE, OPSSetting.GUIMADE,});

    private final String description;
    private final OPSSetting[] settings;

    ControlFileSection(final String description, final OPSSetting[] settings) {
      this.description = description;
      this.settings = settings;
    }

    public String getDescription() {
      return description;
    }

    public OPSSetting[] getSettings() {
      return settings;
    }

  }

  private ControlFileWriter() {
  }

  public static String getControlFileName() {
    return CONTROL_FILE_NAME;
  }

  /**
   * Creates the OPS settings.ctr file and writes it to disk.
   *
   * @param configuration global OPS options
   * @param runFilesPath Location to put the file
   * @param runId Id identifying the ops run
   * @param controlFileOptions options relevant for the OPS control file
   * @param meteoFile meteo file to use
   * @throws IOException throws IOException if it was not possible to create the configuration file
   */
  public static void writeFile(final OPSConfiguration configuration, final File runFilesPath, final String runId,
      final ControlFileOptions controlFileOptions, final String meteoFile) throws IOException {

    final Map<OPSSetting, String> settings = getStandardSettings(configuration, controlFileOptions.getOpsVersion(), runFilesPath, runId,
        controlFileOptions.getYear(), Optional.ofNullable(controlFileOptions.getOpsOptions()).map(OPSOptions::getChemistry).orElse(null),
        meteoFile, controlFileOptions.getRoadsCats());

    addSubstanceSettings(controlFileOptions.getSubstance(), settings);
    setCustomOpsOptions(settings, controlFileOptions.getOpsOptions(), controlFileOptions.getOpsVersion());

    try (final Writer writer = Files.newBufferedWriter(new File(runFilesPath, CONTROL_FILE_NAME).toPath(), StandardCharsets.UTF_8)) {
      for (final ControlFileSection section : ControlFileSection.values()) {
        writer.write(getSectionLine(section));
        for (final OPSSetting setting : section.getSettings()) {
          if (controlFileOptions.getOpsVersion().getControlFileUtil().writeOPSSetting(setting, settings)) {
            writer.write(getSettingLine(setting, settings.containsKey(setting) ? settings.get(setting) : setting.getDefaultValue()));
          }
        }
      }
    }
  }

  public static String controlFileLocation(final File virtualRunFilesPath) {
    return new File(virtualRunFilesPath, CONTROL_FILE_NAME).toString();
  }

  /**
   * @param meteo The meteo to get the corresponding OPS file for.
   * @return The OPS specific meteo file
   */
  public static String toOpsMeteoFile(final Meteo meteo) {
    if (meteo == null) {
      return null;
    }
    return String.format(meteo.isSingleYear() ? FORMAT_METEO_SINGLE_YEAR : FORMAT_METEO_MULTI_YEAR,
        meteo.getStartYear() % METEO_YEAR_MODULO,
        meteo.getEndYear() % METEO_YEAR_MODULO);
  }

  private static Map<OPSSetting, String> getStandardSettings(final OPSConfiguration options, final OPSVersion opsVersion,
      final File runFilesPath, final String runId, final int year, final OPSOptions.Chemistry chemistry, final String meteoFile,
      final String roadsCats) {
    final Map<OPSSetting, String> settings = new HashMap<>();
    final File opsRoot = options.getOpsRoot(opsVersion);
    settings.put(OPSSetting.DATADIR, getFileLocation(opsRoot, DATA_FOLDER) + File.separator);
    final String displayId = "RUN" + runId;
    settings.put(OPSSetting.PROJECT, displayId);
    settings.put(OPSSetting.RUNID, displayId);
    settings.put(OPSSetting.YEAR, getYearWithChemistryFlag(year, true, chemistry, opsVersion));
    settings.put(OPSSetting.EMFILE, getFileLocation(runFilesPath, BrnFileWriter.getFileName()));
    settings.put(OPSSetting.RCPFILE, getFileLocation(runFilesPath, RcpFileWriter.getFileName()));
    final GridSize gridSize = options.getSettingsGridSize(DEFAULT_GRID_SIZE);
    final String landUsePart = getLandUse(options.getSettingsLandUse(), opsVersion);
    settings.put(OPSSetting.Z0FILE, getFileLocation(opsRoot, DATA_FOLDER, "z0_jr_" + gridSize + "_" + landUsePart + ".ops"));
    settings.put(OPSSetting.LUFILE, getFileLocation(opsRoot, DATA_FOLDER, "lu_" + gridSize + "_" + landUsePart + ".ops"));
    settings.put(OPSSetting.MTFILE, getFileLocation(opsRoot, METEO_FOLDER) + File.separator
        + getMeteoFile(opsVersion, meteoFile == null ? options.getSettingsMeteoFile() : meteoFile));
    settings.put(OPSSetting.PLTFILE, getFileLocation(runFilesPath, ResultDataFileReader.FILE_NAME));
    settings.put(OPSSetting.PRNFILE, getFileLocation(runFilesPath, "result.lpt"));
    settings.put(OPSSetting.ROADS, roadsCats);
    settings.put(OPSSetting.ROADSopt, ROADSOPT_DEAULT);
    return settings;
  }

  private static String getYearWithChemistryFlag(final int year, final boolean isOwN2000, final OPSOptions.Chemistry chemistry,
      final OPSVersion opsVersion) {
    return opsVersion.getControlFileUtil().getYear(year, isOwN2000, opsVersion, chemistry);
  }

  private static String getMeteoFile(final OPSVersion opsVersion, final String configMeteoFile) {
    final String meteoFile;
    if (configMeteoFile == null) {
      meteoFile = opsVersion.getControlFileUtil().getDefaultMeteoFile();
    } else {
      meteoFile = configMeteoFile;
      LOG.debug("OPS is running with a custom meteo file: {}", meteoFile);
    }
    return meteoFile;
  }

  private static String getLandUse(final String configLandUse, final OPSVersion opsVersion) {
    final String landUse;
    if (configLandUse == null) {
      landUse = opsVersion.getControlFileUtil().getDefaultLandUse();
    } else {
      landUse = configLandUse;
      LOG.debug("OPS is running with a custom landuse : {}", landUse);
    }
    return landUse;
  }

  private static void addSubstanceSettings(final Substance substance, final Map<OPSSetting, String> settings) {
    switch (substance) {
    case NOX:
      settings.put(OPSSetting.COMPCODE, "2");
      settings.put(OPSSetting.COMPNAME, "NOx (nitrogen oxides) - gas.");
      settings.put(OPSSetting.MOLWEIGHT, "46.0");
      settings.put(OPSSetting.PHASE, "1");
      settings.put(OPSSetting.DIFFCOEFF, ".0");
      settings.put(OPSSetting.WASHOUT, "0");
      break;
    case NH3:
      settings.put(OPSSetting.COMPCODE, "3");
      settings.put(OPSSetting.COMPNAME, "NH3 (ammonium) - gas.");
      settings.put(OPSSetting.MOLWEIGHT, "17.0");
      settings.put(OPSSetting.PHASE, "1");
      settings.put(OPSSetting.DIFFCOEFF, ".240");
      settings.put(OPSSetting.WASHOUT, "0");
      break;
    case PM10:
      settings.put(OPSSetting.COMPCODE, "24");
      settings.put(OPSSetting.COMPNAME, "PM10 - aer.");
      settings.put(OPSSetting.MOLWEIGHT, "1.0");
      settings.put(OPSSetting.PHASE, "0");
      break;
    default:
      throw new IllegalArgumentException("Substance " + substance + " not supported by OPS configuration.");
    }
  }

  private static void setCustomOpsOptions(final Map<OPSSetting, String> settings, final OPSOptions opsOptions,
      final OPSVersion opsVersion) {
    if (opsOptions != null) {
      if (opsOptions.getYear() != null) {
        settings.put(OPSSetting.YEAR, getYearWithChemistryFlag(opsOptions.getYear(), false, opsOptions.getChemistry(), opsVersion));
      }
      if (opsOptions.getCompCode() != null) {
        settings.put(OPSSetting.COMPCODE, Integer.toString(opsOptions.getCompCode()));
      }
      if (opsOptions.getMolWeight() != null) {
        settings.put(OPSSetting.MOLWEIGHT, String.format(FORMAT_LOCALE, FORMAT_MOLWEIGHT, opsOptions.getMolWeight()));
      }
      if (opsOptions.getPhase() != null) {
        settings.put(OPSSetting.PHASE, Integer.toString(opsOptions.getPhase()));
      }
      if (opsOptions.getLoss() != null) {
        settings.put(OPSSetting.LOSS, Integer.toString(opsOptions.getLoss()));
      }
      if (opsOptions.getDiffCoeff() != null) {
        settings.put(OPSSetting.DIFFCOEFF, opsOptions.getDiffCoeff());
      }
      if (opsOptions.getWashout() != null) {
        settings.put(OPSSetting.WASHOUT, opsOptions.getWashout());
      }
      if (opsOptions.getConvRate() != null) {
        settings.put(OPSSetting.CONVRATE, opsOptions.getConvRate());
      }
      if (opsOptions.getRoads() != null) {
        settings.put(OPSSetting.ROADS, opsOptions.getRoads());
      }
      if (opsOptions.getRoads() != null) {
        settings.put(OPSSetting.ROADS, opsOptions.getRoads());
      }
      if (opsOptions.getRoughness() != null) {
        settings.put(OPSSetting.ROUGHNESS, String.format(FORMAT_LOCALE, FORMAT_ROUGHNESS, opsOptions.getRoughness()));
      }
    }
  }

  /**
   * Format the filename and add args to the startPath. It will make sure the
   * file separator is OS compatible.
   * @param startPath The starting point of the location (like C:\OPS\)
   * @param args The folder(s) and filename to add to the location (like "data", "z0FileName.ops")
   * @return the string to use as location for the file.
   */
  private static String getFileLocation(final File startPath, final String... args) {
    final StringBuilder fileLocationBuilder = new StringBuilder(startPath.getAbsolutePath());
    for (final String arg : args) {
      fileLocationBuilder.append(File.separator);
      fileLocationBuilder.append(arg);
    }
    return fileLocationBuilder.toString();
  }

  private static String getSectionLine(final ControlFileSection section) {
    return "*-----------------------" + StringUtils.rightPad(section.getDescription(), SECTION_WIDTH, '-') + "*" + NL;
  }

  private static String getSettingLine(final OPSSetting key, final String value) {
    return StringUtils.rightPad(key.name(), SETTING_KEY_WIDTH) + value + NL;
  }
}
