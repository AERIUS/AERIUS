/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.background;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.ops.OPSTerrainProperties;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

public class TerrainPropertiesMap {
  private final ReceptorGridSettings receptorGridSettings;
  private final ReceptorUtil receptorUtil;
  private final Map<Integer, OPSTerrainProperties> receptorTerrainProperties = new HashMap<>();

  public TerrainPropertiesMap(final ReceptorGridSettings receptorGridSettings) {
    this.receptorUtil = new ReceptorUtil(receptorGridSettings);
    this.receptorGridSettings = receptorGridSettings;
  }

  public boolean put(final int receptorId, final OPSTerrainProperties value) {
    return receptorTerrainProperties.put(receptorId, value) == null;
  }

  /**
   * Finds the terrain properties for the level 1 hexagon nearest the given point.
   * Will only return properties for points within the bounding box.
   *
   * @param point The point.
   * @return The terrain properties, or empty if they could not be found.
   */
  public Optional<OPSTerrainProperties> getTerrainProperties(final Point point) {
    if (receptorGridSettings.getBoundingBox().isPointWithinBoundingBox(point)) {
      return getTerrainPropertiesById(receptorUtil.getReceptorIdFromPoint(point));
    } else {
      return Optional.empty();
    }
  }

  private Optional<OPSTerrainProperties> getTerrainPropertiesById(final int receptorId) {
    return Optional.ofNullable(receptorTerrainProperties.get(receptorId));
  }

  public int size() {
    return receptorTerrainProperties.size();
  }
}
