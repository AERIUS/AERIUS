/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.background;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.ops.domain.OPSConfiguration;
import nl.overheid.aerius.ops.domain.OPSConstants;
import nl.overheid.aerius.ops.version.OPSVersion;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;

/**
 * Util class to populate {@link OPSData} background data.
 */
public final class OPSDataBuilder {

  private static final Logger LOG = LoggerFactory.getLogger(OPSDataBuilder.class);

  private OPSDataBuilder() {
  }

  /**
   * Create a new and populated instance of {@link OPSData}
   * @param configuration configuration of the OPS worker
   * @return a new and populated instance of {@link OPSData}
   * @throws IOException
   */
  public static OPSData build(final OPSConfiguration configuration) throws IOException {
    final OPSData opsData = new OPSData();
    for (final OPSVersion opsVersion : configuration.getModelPreloadVersions()) {
      loadAdditionalData(configuration, opsVersion, opsData);
    }
    return opsData;
  }

  /**
   * Check if more data is needed, and load into the passed instance of {@link OPSData}
   * @param config configuration of the OPS worker
   * @param opsVersion the opsVersion for which to load data
   * @param opsData the opsData to load data into
   * @return a new and populated instance of {@link OPSData}
   */
  public static synchronized void loadAdditionalData(final OPSConfiguration config, final OPSVersion opsVersion, final OPSData opsData)
      throws IOException {
    loadTerrainProperties(opsVersion, opsData, config.getTerrainPropertiesFile(opsVersion));
  }

  private static void loadTerrainProperties(final OPSVersion opsVersion, final OPSData data, final File terrainPropertiesFile)
      throws IOException {
    if (!data.containsTerrainPropertiesMap(opsVersion)) {
      LOG.info("Loading terrain properties data from {}.", terrainPropertiesFile);
      final ReceptorGridSettings gridSettings = OPSConstants.getReceptorGridSettings();
      try (final InputStream is = new FileInputStream(terrainPropertiesFile)) {
        data.addTerrainPropertiesMap(opsVersion, TerrainPropertiesReader.read(is, gridSettings));
      }
    }
  }

}
