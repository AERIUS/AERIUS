/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.runner;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.exec.ExecuteWrapper.StreamGobblers;
import nl.overheid.aerius.exec.StreamGobbler;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Stream Gobblers for OPS executable.
 */
class OPSStreamGobblers implements StreamGobblers {

  private static final Logger LOG = LoggerFactory.getLogger(OPSStreamGobblers.class);

  /**
   * Some calculation methods give some useless error messages, because it tries to execute a clear command that isn't present,
   *  tries to use a UI object while running via the console (wine) etc. It results in clobbering the error stream.
   * Here are the complete strings of those error messages so they can be filtered out.
   */
  private static final List<String> IGNORE_ERROR_LINES = List.of(
      "clear wordt niet herkend als een interne", // Dutch windows version of the error message by OPS - part I
      "of externe opdracht, programma of batchbestand.", // Dutch windows version of the error message by OPS - part II
      "'clear' is not recognized as an internal or external command,", // Windows english version version of the error message by OPS - part I
      "operable program or batch file.", // Windows english version of the error message by OPS - part II
      "'unknown': unknown terminal type.", // Linux version of the error message by OPS.
      "'unknown': I need something more specific.", // Linux version of the error message by OPS.
      "Make sure that your X server is running and that $DISPLAY is set correctly.", // Wine default error message on UI object creation
      "Application tried to create a window, but no driver could be loaded.", // Wine error message on UI window creation
      "err:systray:initialize_systray Could not create tray window", // Wine error message on systray usage
      "TERM environment variable not set." // Linux version TERM environment warnings
  );

  /**
   * Pattern of an OPS crash report
   */
  private static final String FORRTL_ERROR = "forrtl: severe";

  private AeriusException error;

  @Override
  public StreamGobbler errorStreamGobbler(final String type, final String parentId) {
    return new StreamGobbler(type, parentId, this::ignoreErrorKnownLines);
  }

  @Override
  public StreamGobbler outputStreamGobbler(final String type, final String parentId) {
    return new StreamGobbler(type, parentId, OPSStreamGobblers::ignoreKnownLines) {
      @Override
      public void run() {
        // We ignore the stdout as it doesn't contain useful information
        try (final BufferedInputStream br = new BufferedInputStream(getInputStream())) {
          while (br.read() != -1) {
            // read and ignore till the end of the stream has been reached
          }
        } catch (final IOException ioe) {
          LOG.error("Error while reading stdout while running external.", ioe);
        }
      }
    };
  }

  /**
   * @return Returns the exception if an error was present in the error log or null if no errors.
   */
  public AeriusException getError() {
    return error;
  }

  private boolean ignoreErrorKnownLines(final String line) {
    if (line.contains(FORRTL_ERROR)) {
      error = new AeriusException(AeriusExceptionReason.OPS_INTERNAL_EXCEPTION, line);
    }
    return OPSStreamGobblers.ignoreKnownLines(line);
  }

  /**
   * Returns true if the line doesn't contain any of messages to ignore
   * @param line line to check
   * @return true if line contains error.
   */
  private static boolean ignoreKnownLines(final String line) {
    return !IGNORE_ERROR_LINES.contains(line);
  }
}
