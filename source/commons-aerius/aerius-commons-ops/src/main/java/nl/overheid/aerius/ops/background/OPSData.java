/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.background;

import java.util.EnumMap;
import java.util.Map;

import nl.overheid.aerius.ops.version.OPSVersion;

/**
 * Object to store background data loaded to be used with OPS.
 */
public class OPSData {

  private final Map<OPSVersion, TerrainPropertiesMap> terrainProperties = new EnumMap<>(OPSVersion.class);

  public void addTerrainPropertiesMap(final OPSVersion opsVersion, final TerrainPropertiesMap terrainPropertiesMap) {
    this.terrainProperties.put(opsVersion, terrainPropertiesMap);
  }

  public boolean containsTerrainPropertiesMap(final OPSVersion opsVersion) {
    return terrainProperties.containsKey(opsVersion);
  }

  public TerrainPropertiesMap getTerrainPropertiesMap(final OPSVersion opsVersion) {
    return terrainProperties.get(opsVersion);
  }
}
