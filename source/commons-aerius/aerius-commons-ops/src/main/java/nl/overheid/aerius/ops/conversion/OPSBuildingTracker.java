/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.conversion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import nl.overheid.aerius.building.BuildingTracker;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.geo.OrientedEnvelope;
import nl.overheid.aerius.shared.domain.v2.building.Building;
import nl.overheid.aerius.shared.domain.v2.building.BuildingFeature;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.ImaerExceptionReason;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * {@link BuildingTracker} implementation for {@link OPSSource} objects.
 */
public class OPSBuildingTracker implements BuildingTracker {

  public interface BuildingDimensionsApplier {

    /**
     * @param length The length of the building in meters
     * @param width The width of the building in meters
     * @param height The height of the building in meters
     * @param orientation The orientation (or angle) North clockwise
     */
    void setDimensions(final double length, double width, double height, double orientation);
  }

  private static class BuildingDimensions {
    double length;
    double width;
    double orientation;
    double height;

    public BuildingDimensions(final double length, final double width, final double orientation, final double height) {
      this.length = length;
      this.width = width;
      this.orientation = orientation;
      this.height = height;
    }
  }

  private final Map<String, BuildingFeature> originalBuildingsMap;
  private final Map<String, BuildingDimensions> buildingDimensionsMap = new HashMap<>();

  public OPSBuildingTracker(final List<BuildingFeature> possibleBuildings) {
    originalBuildingsMap = possibleBuildings == null
        ? Map.of()
        : possibleBuildings.stream().collect(Collectors.toMap(feature -> feature.getProperties().getGmlId(), Function.identity()));
  }

  public void applyBuildingDimensions(final String buildingGmlId, final BuildingDimensionsApplier buildingDimensionsApplier) throws AeriusException {
    ensureDimensionsExist(buildingGmlId);
    final BuildingDimensions buildingDimensions = buildingDimensionsMap.get(buildingGmlId);
    buildingDimensionsApplier.setDimensions(
        buildingDimensions.length,
        buildingDimensions.width,
        buildingDimensions.height,
        buildingDimensions.orientation);
  }

  private void ensureDimensionsExist(final String buildingGmlId) throws AeriusException {
    if (!buildingDimensionsMap.containsKey(buildingGmlId)) {
      if (originalBuildingsMap.containsKey(buildingGmlId)) {
        final BuildingDimensions dimensions = convert(originalBuildingsMap.get(buildingGmlId));
        buildingDimensionsMap.put(buildingGmlId, dimensions);
      } else {
        throw new AeriusException(ImaerExceptionReason.COHESION_REFERENCE_MISSING_BUILDING, buildingGmlId);
      }
    }
  }

  private static BuildingDimensions convert(final BuildingFeature feature) throws AeriusException {
    final Geometry geometry = feature.getGeometry();
    final Building properties = feature.getProperties();

    if (geometry instanceof Polygon) {
      final OrientedEnvelope orientedEnvelope = GeometryUtil.determineOrientedEnvelope((Polygon) geometry);

      return new BuildingDimensions(
          orientedEnvelope.getLength(),
          orientedEnvelope.getWidth(),
          orientedEnvelope.getOrientation(),
          properties.getHeight());
    } else if (properties.isCircle()) {
      return new BuildingDimensions(properties.getDiameter(), properties.getDiameter(), 0, properties.getHeight());
    } else {
      throw new AeriusException(ImaerExceptionReason.GEOMETRY_INVALID, feature.getGeometry().toString());
    }
  }
}

