/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.importer;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;

import nl.overheid.aerius.io.ImportReader;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.ops.conversion.CalculationPointOPSConverter;
import nl.overheid.aerius.ops.io.RcpFileReader;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Import reader for OPS receptor files.
 */
public class RcpImportReader implements ImportReader {

  private static final String RCP_FILE_EXTENSION = ".rcp";

  private final boolean useImportedLandUses;
  private final boolean useImportedHeights;

  public RcpImportReader(final boolean useImportedLandUses, final boolean useImportedHeights) {
    this.useImportedLandUses = useImportedLandUses;
    this.useImportedHeights = useImportedHeights;
  }

  @Override
  public boolean detect(final String filename) {
    return filename.toLowerCase(Locale.ENGLISH).endsWith(RCP_FILE_EXTENSION);
  }

  @Override
  public void read(final String filename, final InputStream inputStream, final SectorCategories categories, final Substance substance,
      final ImportParcel importResult) throws IOException, AeriusException {
    final RcpFileReader reader = new RcpFileReader();
    reader.setReadTerrainData(useImportedLandUses);
    reader.setReadHeight(useImportedHeights);

    final LineReaderResult<OPSReceptor> results = reader.readObjects(inputStream);
    final List<OPSReceptor> receptors = results.getObjects();
    ensureCorrectIds(receptors);

    final List<CalculationPointFeature> calculationPoints = importResult.getCalculationPointsList();

    for (final OPSReceptor opsReceptor : receptors) {
      calculationPoints.add(CalculationPointOPSConverter.convert(opsReceptor, useImportedLandUses || useImportedHeights));
    }
    importResult.getExceptions().addAll(results.getExceptions());
  }

  private void ensureCorrectIds(final List<OPSReceptor> receptors) {
    // For importing land uses the ID's would be maintained as-is. It's assumed they are consecutive
    // For other cases, the ID would be numbered to be consecutive.
    if (!useImportedLandUses) {
      int i = 1;
      for (final OPSReceptor r : receptors) {
        r.setId(i++);
      }
    }
  }
}
