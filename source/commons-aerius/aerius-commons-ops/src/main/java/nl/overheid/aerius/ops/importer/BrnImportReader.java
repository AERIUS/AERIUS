/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.importer;

import java.io.IOException;
import java.io.InputStream;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

import nl.overheid.aerius.io.ImportReader;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.ops.conversion.EmissionSourceOPSConverter;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.domain.SourceBuildingCombination;
import nl.overheid.aerius.ops.io.BrnFileReader;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.geojson.Crs;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.geo.EPSG;

/**
 * Import reader for OPS brn source files. It supports reading the fixed width version 0 and the space delimited v1 and v2 of OPS source files.
 */
public class BrnImportReader implements ImportReader {

  private static final String BRN_FILE_EXTENSION = ".brn";
  private static final Set<Substance> SUPPORTED_SUBSTANCES = EnumSet.of(Substance.NH3, Substance.NOX, Substance.PM10);
  private final boolean useValidSectors;
  private final boolean importDiameter;

  public BrnImportReader(final boolean useValidSectors, final boolean importDiameter) {
    this.useValidSectors = useValidSectors;
    this.importDiameter = importDiameter;
  }

  @Override
  public boolean detect(final String filename) {
    return filename.toLowerCase(Locale.ENGLISH).endsWith(BRN_FILE_EXTENSION);
  }

  @Override
  public void read(final String filename, final InputStream inputStream, final SectorCategories categories, final Substance substance,
      final ImportParcel importResult) throws IOException, AeriusException {
    if (substance == null) {
      throw new AeriusException(AeriusExceptionReason.BRN_WITHOUT_SUBSTANCE);
    }
    if (!SUPPORTED_SUBSTANCES.contains(substance)) {
      throw new AeriusException(AeriusExceptionReason.BRN_SUBSTANCE_NOT_SUPPORTED, substance.getName(), supportedSubstancesAsString());
    }
    final LineReaderResult<OPSSource> results = new BrnFileReader(substance).readObjects(inputStream);
    final List<OPSSource> sources = results.getObjects();
    setDiameterOnSources(sources, importResult.getWarnings());

    // Convert the OPS sources to an EmissionSource list
    final ScenarioSituation situation = importResult.getSituation();
    final List<EmissionSourceFeature> emissionSources = situation.getEmissionSourcesList();

    for (final OPSSource src : sources) {
      final SourceBuildingCombination combination = EmissionSourceOPSConverter.convert(src, substance, categories, useValidSectors);
      emissionSources.add(combination.getSource());
      if (combination.getBuilding() != null) {
        situation.getBuildingsList().add(combination.getBuilding());
      }
    }
    setCrsBasedOnSources(sources, situation);
    importResult.getExceptions().addAll(results.getExceptions());
  }

  private void setDiameterOnSources(final List<OPSSource> sources, final List<AeriusException> warnings) {
    if (!importDiameter) {
      final List<OPSSource> sourcesWithNonZeroDiameter = sources.stream()
          .filter(s -> s.getDiameter() != 0)
          .collect(Collectors.toList());
      if (!sourcesWithNonZeroDiameter.isEmpty()) {
        warnings.add(new AeriusException(AeriusExceptionReason.BRN_DIAMETER_NOT_IMPORTED));
        sourcesWithNonZeroDiameter.forEach(x -> x.setDiameter(0));
      }
    }
  }

  private static void setCrsBasedOnSources(final List<OPSSource> sources, final ScenarioSituation situation) {
    if (sources.stream().anyMatch(x -> x.getPoint().getSrid() == EPSG.WGS84.getSrid())) {
      final Crs crs = new Crs();
      final Crs.CrsContent crsContent = new Crs.CrsContent();
      crsContent.setName(EPSG.WGS84.getEpsgCode());
      crs.setProperties(crsContent);
      situation.getSources().setCrs(crs);
    }
  }

  private static String supportedSubstancesAsString() {
    return SUPPORTED_SUBSTANCES.stream().map(Substance::getName).collect(Collectors.joining(", "));
  }
}
