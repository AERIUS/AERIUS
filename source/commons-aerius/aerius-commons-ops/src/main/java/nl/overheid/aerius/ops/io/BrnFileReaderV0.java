/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import nl.overheid.aerius.io.AbstractLineReader;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.domain.OPSSource.OPSSourceOutflowDirection;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.geo.EPSG;

/**
 * Class to read the fixed with OPS brn source file.
 */
public class BrnFileReaderV0 extends AbstractLineReader<OPSSource> {

  /**
   * Enum with all columns in an OPS source file.
   */
  public enum Column {
    SOURCE("snr"),
    X("x(m)"),
    Y("y(m)"),
    EMISSION("q(g/s)"),
    HEAT("hc(MW)"),
    HEIGHT("h(m)"),
    DIAMETER("d(m)"),
    SPREAD("s(m)"),
    OUTFLOW_SURFACE("D_stack(m)"),
    OUTFLOW_VELOCITY("V_stack(m/s)"),
    EMISSION_TEMPERATURE("Ts_stack(C)"),
    DIURNAL_VARIATION("dv"),
    CATEGORY("cat"),
    AREA("area"),
    PARTICLE_SIZE_DISTRIBUTION("psd"),
    BUILDING_LENGTH("length(m)"),
    BUILDING_WIDTH("width(m)"),
    BUILDING_HEIGHT("height(m)"),
    BUILDING_ORIENTATION("orientation(degrees)"),
    COMP("component");

    private final String name;

    Column(final String name) {
      this.name = name;
    }

    public String getName() {
      return name;
    }
  }

  private static final int SOURCE_ID_OFFSET = 0;
  private static final int X_OFFSET = 4;
  private static final int Y_OFFSET = 12;
  private static final int EMISSION_OFFSET = 20;
  private static final int HEAT_CONTENT_OFFSET = 30;
  private static final int HEIGHT_OFFSET = 37;
  private static final int DIAMETER_OFFSET = 43;
  private static final int SPREAD_OFFSET = 50;
  private static final int DIURNAL_VARIATION_OFFSET = 56;
  private static final int CATEGORY_OFFSET = 60;
  private static final int AREA_OFFSET = 64;
  private static final int PARTICLE_SIZE_DISTRIBUTION_OFFSET = 68;
  private static final int COMP_OFFSET = 74;

  private static final int SOURCE_ID_SIZE = 4;
  private static final int X_SIZE = 8;
  private static final int Y_SIZE = 8;
  private static final int EMISSION_SIZE = 10;
  private static final int HEAT_CONTENT_SIZE = 7;
  private static final int HEIGHT_SIZE = 6;
  private static final int DIAMETER_SIZE = 7;
  private static final int SPREAD_SIZE = 6;
  private static final int DIURNAL_VARIATION_SIZE = 4;
  private static final int CATEGORY_SIZE = 4;
  private static final int AREA_SIZE = 4;
  private static final int PARTICLE_SIZE_DISTRIBUTION_SIZE = 4;

  private static final Map<Column, ColumnPosition> COLUMNS_MAP;
  static {
    final Map<Column, ColumnPosition> map = new EnumMap<>(Column.class);
    map.put(Column.SOURCE, new ColumnPosition(SOURCE_ID_OFFSET, SOURCE_ID_SIZE));
    map.put(Column.X, new ColumnPosition(X_OFFSET, X_SIZE));
    map.put(Column.Y, new ColumnPosition(Y_OFFSET, Y_SIZE));
    map.put(Column.EMISSION, new ColumnPosition(EMISSION_OFFSET, EMISSION_SIZE));
    map.put(Column.HEAT, new ColumnPosition(HEAT_CONTENT_OFFSET, HEAT_CONTENT_SIZE));
    map.put(Column.HEIGHT, new ColumnPosition(HEIGHT_OFFSET, HEIGHT_SIZE));
    map.put(Column.DIAMETER, new ColumnPosition(DIAMETER_OFFSET, DIAMETER_SIZE));
    map.put(Column.SPREAD, new ColumnPosition(SPREAD_OFFSET, SPREAD_SIZE));
    map.put(Column.DIURNAL_VARIATION, new ColumnPosition(DIURNAL_VARIATION_OFFSET, DIURNAL_VARIATION_SIZE));
    map.put(Column.CATEGORY, new ColumnPosition(CATEGORY_OFFSET, CATEGORY_SIZE));
    map.put(Column.AREA, new ColumnPosition(AREA_OFFSET, AREA_SIZE));
    map.put(Column.PARTICLE_SIZE_DISTRIBUTION, new ColumnPosition(PARTICLE_SIZE_DISTRIBUTION_OFFSET, PARTICLE_SIZE_DISTRIBUTION_SIZE));
    map.put(Column.COMP, new ColumnPosition(COMP_OFFSET, 0));
    COLUMNS_MAP = Collections.unmodifiableMap(map);
  }

  private final Map<Column, ColumnPosition> columns;
  private final Substance substance;

  public BrnFileReaderV0(final Substance substance) {
    this(COLUMNS_MAP, substance);
  }

  public BrnFileReaderV0(final Map<Column, ColumnPosition> columns, final Substance substance) {
    this.columns = columns;
    this.substance = substance;
  }

  @Override
  public LineReaderResult<OPSSource> readObjects(final InputStream inputStream) throws IOException {
    try (InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8)) {
      return super.readObjects(reader, 1);
    }
  }

  // i4,2i8,e10.3,f7.3,f6.1,f7,f6.1,4i4,2x,a12
  @Override
  protected OPSSource parseLine(final String line, final List<AeriusException> warnings) {
    final OPSSource source = new OPSSource(getSourceId(), getSRS(), getX(), getY());
    source.setEmission(substance, getEmission() * Substance.EMISSION_IN_G_PER_S_FACTOR);
    source.setHeatContent(getHeatContent());
    source.setEmissionHeight(getHeight());
    source.setDiameter(getDiameter());
    source.setSpread(getSpread());
    source.setOutflowDiameter(getOutflowSurface());
    source.setOutflowVelocity(getOutflowVelocity());
    source.setOutflowDirection(getOutflowDirection());
    source.setEmissionTemperature(getEmissionTemperature());
    source.setDiurnalVariation(getDiurnalVariation());
    source.setParticleSizeDistribution(getParticleSizeDistribution());
    source.setCat(getCategoryNumber());
    source.setArea(getAreaNumber());
    source.setBuildingHeight(getBuildingHeight());
    source.setBuildingWidth(getBuildingWidth());
    source.setBuildingLength(getBuildingLength());
    source.setBuildingOrientation(getBuildingOrientation());
    source.setComp(getComp());
    return source;
  }

  private int getSourceId() {
    return getInt(Column.SOURCE);
  }

  /**
   * If coordinate contains a dot it's a 4326 srid. This is how ops works.
   * @param source to set the value on.
   */
  private int getSRS() {
    return (getString(Column.X).contains(".") ? EPSG.WGS84 : EPSG.RDNEW).getSrid();
  }

  private double getX() {
    return getDouble(Column.X);
  }

  private double getY() {
    return getDouble(Column.Y);
  }

  private double getEmission() {
    return getDouble(Column.EMISSION);
  }

  private double getHeatContent() {
    return getDouble(Column.HEAT);
  }

  private double getHeight() {
    return getDouble(Column.HEIGHT);
  }

  private int getDiameter() {
    return getInt(Column.DIAMETER);
  }

  private double getSpread() {
    return getDouble(Column.SPREAD);
  }

  private double getOutflowSurface() {
    return getDouble(Column.OUTFLOW_SURFACE, BrnConstants.OUTFLOW_DIAMETER_NOT_APPLICABLE);
  }

  private double getOutflowVelocity() {
    final double outflowVelocity = getDouble(Column.OUTFLOW_VELOCITY, BrnConstants.OUTFLOW_VELOCITY_NOT_APPLICABLE);

    return outflowVelocity > BrnConstants.OUTFLOW_VELOCITY_NOT_APPLICABLE ? Math.abs(outflowVelocity) : outflowVelocity;
  }

  private OPSSourceOutflowDirection getOutflowDirection() {
    if (columns.containsKey(Column.OUTFLOW_VELOCITY)) {
      return getDouble(Column.OUTFLOW_VELOCITY) > 0 ? OPSSourceOutflowDirection.VERTICAL : OPSSourceOutflowDirection.HORIZONTAL;
    }

    return OPSSourceOutflowDirection.HORIZONTAL;
  }

  private double getEmissionTemperature() {
    return getDouble(Column.EMISSION_TEMPERATURE, BrnConstants.EMISSION_TEMPERATURE_NOT_APPLICABLE);
  }

  private int getDiurnalVariation() {
    return getInt(Column.DIURNAL_VARIATION);
  }

  private int getCategoryNumber() {
    return getInt(Column.CATEGORY);
  }

  private int getAreaNumber() {
    return getInt(Column.AREA);
  }

  private int getParticleSizeDistribution() {
    return getInt(Column.PARTICLE_SIZE_DISTRIBUTION);
  }

  private double getBuildingHeight() {
    return getDouble(Column.BUILDING_HEIGHT, BrnConstants.BUILDING_HEIGHT_NOT_APPLICABLE);
  }

  private double getBuildingWidth() {
    return getDouble(Column.BUILDING_WIDTH, BrnConstants.BUILDING_WIDTH_NOT_APPLICABLE);
  }

  private double getBuildingLength() {
    return getDouble(Column.BUILDING_LENGTH, BrnConstants.BUILDING_LENGTH_NOT_APPLICABLE);
  }

  private int getBuildingOrientation() {
    return getInt(Column.BUILDING_ORIENTATION, BrnConstants.BUILDING_ORIENTATION_NOT_APPLICABLE);
  }

  private String getComp() {
    return getString(Column.COMP.getName(), columns.get(Column.COMP).getOffset());
  }

  private double getDouble(final Column column, final double defaultValue) {
    return columns.containsKey(column) ? getDouble(column) : defaultValue;
  }

  private double getDouble(final Column column) {
    return getDouble(column.getName(), columns.get(column).getOffset(), columns.get(column).getSize());
  }

  private int getInt(final Column column) {
    return getInt(column.getName(), columns.get(column).getOffset(), columns.get(column).getSize());
  }

  private int getInt(final Column column, final int defaultValue) {
    return columns.containsKey(column) ? getInt(column.getName(), columns.get(column).getOffset(), columns.get(column).getSize()) : defaultValue;
  }

  private String getString(final Column column) {
    return getString(column.getName(), columns.get(column).getOffset(), columns.get(column).getSize());
  }

  /**
   * Data class containing offset and size of data in a ops sources file row.
   */
  protected static class ColumnPosition {
    private final int offset;
    private final int size;

    public ColumnPosition(final int offset, final int size) {
      this.offset = offset;
      this.size = size;
    }

    public int getOffset() {
      return offset;
    }

    public int getSize() {
      return size;
    }
  }
}
