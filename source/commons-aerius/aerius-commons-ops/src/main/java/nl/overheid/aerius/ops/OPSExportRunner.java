/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import nl.overheid.aerius.http.FileServerProxy;
import nl.overheid.aerius.ops.background.OPSData;
import nl.overheid.aerius.ops.domain.OPSConfiguration;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.io.ControlFileOptions;
import nl.overheid.aerius.ops.runner.OPSCollector;
import nl.overheid.aerius.ops.runner.OPSPerformer;
import nl.overheid.aerius.ops.runner.OPSRunner;
import nl.overheid.aerius.ops.version.OPSVersion;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.ExportResult;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.util.FilenameUtil;
import nl.overheid.aerius.worker.ZipAndProcessChainer;

/**
 * Generates all OPS input files and zips them, posts the zip to the internal file server and returns the fileCode to the chunker worker.
 */
class OPSExportRunner {

  private static final OPSCollector<File> EMPTY_COLLECTOR = new OPSCollector<>() {
  };
  private static final String PREFIX = "aerius_ops";
  private static final String CMD_TXT = "cmd.txt";

  /**
   * Any validation error should be written to file and returned in the export zip.
   */
  private static final boolean VALIDATION_ERRORS_TO_FILE = true;
  /**
   * Generated files should be kept by the processor as they are deleted by this class after adding them to the zip.
   */
  private static final boolean KEEP_GENERATED_FILES_PROCESSOR = true;

  private final OPSConfiguration configuration;
  private final FileServerProxy fileServerProxy;
  private final boolean keepGeneratedFiles;
  private final OPSData opsData;

  public OPSExportRunner(final OPSConfiguration configuration, final FileServerProxy fileServerProxy, final OPSData opsData) {
    this.configuration = configuration;
    keepGeneratedFiles = configuration.isKeepGeneratedFiles();
    this.fileServerProxy = fileServerProxy;
    this.opsData = opsData;
  }

  /**
   * Generates the OPS input files, sends them to the file server and returns the fileCode.
   *
   * @param input input to generate input files from
   * @param jobKey job key
   * @return result with fileCode of zipped OPS input files sent to the file server
   * @throws Exception
   */
  public ExportResult run(final OPSInputData input, final String jobKey) throws Exception {
    final File zipOutputDirectory = FileUtil.createDirectory(configuration.getRunFilesDirectory(), PREFIX);

    try (final ZipAndProcessChainer chainer = new ZipAndProcessChainer(fileServerProxy, FilenameUtil.modelZipFilename(zipOutputDirectory, PREFIX),
        keepGeneratedFiles)) {
      final OPSRunner<File> runner = new OPSRunner<>() {
        @Override
        public File run(final String runId, final File runDirectory, final List<String> parameters, final ControlFileOptions cfo)
            throws IOException, AeriusException {
          writeCmdOptionsToFile(runDirectory, cfo.getOpsVersion(), parameters);
          addFile(chainer, runDirectory);
          return null;
        }

        @Override
        public File emptyResultsForSubstance(final Collection<OPSReceptor> receptors, final Set<EmissionResultKey> erks) {
          return null;
        }
      };
      final OPSPerformer<File> processor = new OPSPerformer<>(runner, EMPTY_COLLECTOR, opsData, configuration, VALIDATION_ERRORS_TO_FILE,
          KEEP_GENERATED_FILES_PROCESSOR);

      processor.run(input);
      return new ExportResult(chainer.put(jobKey, input.getExpire()));
    }
  }

  private static void addFile(final ZipAndProcessChainer chainer, final File directory) {
    if (directory != null) {
      chainer.addDirectoryContent(directory, true);
    }
  }

  /**
   * Write the OPS command line options to a file to include in the export
   *
   * @param targetDirectory directory to store the file in
   * @param opsVersion OPS version this is generated for
   * @param parameters command line parameters
   * @throws IOException
   */
  private static void writeCmdOptionsToFile(final File targetDirectory, final OPSVersion opsVersion, final List<String> parameters)
      throws IOException {
    final File file = new File(targetDirectory, CMD_TXT);

    Files.writeString(file.toPath(), opsVersion.getVersion() + "/bin/ops " + String.join(" ", parameters));
  }
}
