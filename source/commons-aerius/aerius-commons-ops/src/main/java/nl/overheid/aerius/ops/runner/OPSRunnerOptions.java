/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.runner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import nl.overheid.aerius.calculation.EngineVersion;
import nl.overheid.aerius.ops.version.OPSVersion;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;

/**
 * OPS runtime command line options constructor.
 */
class OPSRunnerOptions {

  private static final String VERBOSE_FLAG = "-v";
  private static final String CONTROL_FILE_FLAG = "-i";
  private static final String TERRAIN_DATA_FLAG = "-perc";
  private static final String HEIGHT_DATA_FLAG = "-varz";
  private static final String MAX_DISTANCE_FLAG = "-maxdist";
  private static final String MIN_DISTANCE_FLAG = "-mindist";
  private static final String ALLOW_SIGZ0_POINT_SOURCE = "-allow_sigz0_point_source";

  private final boolean maxDistance;
  private final boolean hasTerrainData;
  private final boolean hasHeight;
  private final boolean withAllowSigZ0PointSourceOption;

  /**
   * Constructs command line options object.
   *
   * @param receptors receptors to do run for
   * @param maxDistance true if maxDistance flag should be used
   * @param opsVersion OPS version
   */
  public OPSRunnerOptions(final Collection<OPSReceptor> receptors, final boolean maxDistance, final EngineVersion opsVersion) {
    this.maxDistance = maxDistance;
    final Optional<OPSReceptor> firstReceptor = receptors.stream().findFirst();

    if (firstReceptor.isPresent()) {
      final OPSReceptor receptor = firstReceptor.get();
      hasTerrainData = receptor.hasTerrainData();
      hasHeight = receptor.hasHeight();
    } else {
      hasTerrainData = false;
      hasHeight = false;
    }
    withAllowSigZ0PointSourceOption = opsVersion instanceof final OPSVersion opsv &&  opsv.isSupportSigZ0PointSourceOption();
  }

  /**
   * Returns the list of OPS command line options to use for a run.
   *
   * @param settingsFile path the the OPS settings file
   * @param minDistance true if minDistance parameters must be passed
   * @return List of command line options
   */
  public List<String> constructParameters(final String settingsFile, final boolean minDistance) {
    final List<String> parameters = new ArrayList<>();
    //Order here is important: OPS expects the arguments in a certain order (switching terrain flag with verbose flag for instance won't work).
    parameters.add(VERBOSE_FLAG);
    parameters.add(CONTROL_FILE_FLAG);
    parameters.add(settingsFile);
    if (hasTerrainData) {
      parameters.add(TERRAIN_DATA_FLAG);
    }
    if (hasHeight) {
      parameters.add(HEIGHT_DATA_FLAG);
    }
    if (maxDistance) {
      parameters.add(MAX_DISTANCE_FLAG);
    }
    if (minDistance) {
      parameters.add(MIN_DISTANCE_FLAG);
    }
    if (withAllowSigZ0PointSourceOption) {
      parameters.add(ALLOW_SIGZ0_POINT_SOURCE);
    }
    return parameters;
  }
}
