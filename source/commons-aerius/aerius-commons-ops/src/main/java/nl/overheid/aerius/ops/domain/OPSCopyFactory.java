/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.domain;

import nl.overheid.aerius.ops.conversion.OPSBuildingTracker;
import nl.overheid.aerius.ops.domain.OPSSource.OPSSourceOutflowDirection;
import nl.overheid.aerius.ops.io.BrnConstants;
import nl.overheid.aerius.shared.domain.ops.DiurnalVariation;
import nl.overheid.aerius.shared.domain.ops.OutflowDirectionType;
import nl.overheid.aerius.shared.domain.v2.characteristics.HeatContentType;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.characteristics.SourceCharacteristics;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Copy factor class to copy from {@link OPSSourceCharacteristics} to {@link OPSSource} and back.
 */
public final class OPSCopyFactory {

  public interface ImportBuildingHandler {

    /**
     *
     * @param length The length of the building in meters
     * @param width The width of the building in meters
     * @param height The height of the building in meters
     * @param orientation The orientation (or angle) North clockwise
     * @return returns building id
     * @throws AeriusException
     */
    String handle(double length, double width, double height, int orientation) throws AeriusException;

  }

  private OPSCopyFactory() {
    // util
  }

  /**
   * Copies the content of {@link OPSSourceCharacteristics} to the {@link OPSSource} object.
   * @param characteristics input {@link OPSSourceCharacteristics}
   * @param opsSource output {@link OPSSource}
   * @param buildingTracker tracker containing buildings
   * @throws AeriusException
   */
  public static void toOpsSource(final SourceCharacteristics original, final OPSSource opsSource, final OPSBuildingTracker buildingTracker)
      throws AeriusException {
    if (original instanceof OPSSourceCharacteristics characteristics) {
      opsSource.setOutflowDiameter(safeDouble(characteristics.getOutflowDiameter()));
      opsSource.setOutflowVelocity(safeDouble(characteristics.getOutflowVelocity()));
      opsSource.setEmissionTemperature(characteristics.getEmissionTemperature() == null
          ? BrnConstants.AVERAGE_SURROUNDING_TEMPERATURE
          : characteristics.getEmissionTemperature());
      opsSource.setOutflowDirection(characteristics.getOutflowDirection() == OutflowDirectionType.HORIZONTAL
          ? OPSSourceOutflowDirection.HORIZONTAL
          : OPSSourceOutflowDirection.VERTICAL);
      opsSource.setHeatContent(characteristics.getHeatContentType() == HeatContentType.FORCED
          ? BrnConstants.HEAT_CONTENT_NOT_APPLICABLE
          : safeDouble(characteristics.getHeatContent()));
      opsSource.setDiameter(safeInt(characteristics.getDiameter()));
      opsSource.setDiurnalVariation(
          (characteristics.getDiurnalVariation() == null ? DiurnalVariation.UNKNOWN : characteristics.getDiurnalVariation()).getId());
      opsSource.setEmissionHeight(characteristics.getEmissionHeight());
      opsSource.setSpread(safeDouble(characteristics.getSpread()));
      opsSource.setParticleSizeDistribution(characteristics.getParticleSizeDistribution());
      if (characteristics.getBuildingId() != null && buildingTracker != null) {
        buildingTracker.applyBuildingDimensions(characteristics.getBuildingId(), (length, width, height, orientation) -> {
          opsSource.setBuildingWidth(width);
          opsSource.setBuildingLength(length);
          opsSource.setBuildingHeight(height);
          opsSource.setBuildingOrientation((int) Math.round(GeometryUtil.orientationNorthToX(orientation)));
        });
      } else {
        opsSource.setBuildingWidth(BrnConstants.BUILDING_WIDTH_NOT_APPLICABLE);
        opsSource.setBuildingLength(BrnConstants.BUILDING_LENGTH_NOT_APPLICABLE);
        opsSource.setBuildingHeight(BrnConstants.BUILDING_HEIGHT_NOT_APPLICABLE);
        opsSource.setBuildingOrientation(BrnConstants.BUILDING_ORIENTATION_NOT_APPLICABLE);
      }
    }
  }

  private static int safeInt(final Integer intValue) {
    return intValue == null ? 0 : intValue;
  }

  private static double safeDouble(final Double doubleValue) {
    return doubleValue == null ? 0.0 : doubleValue;
  }

  /**
   * Returns a new {@link OPSSourceCharacteristics} objects from the {@link OPSSource} object.
   * @param dvs list of {@link DiurnalVariationSpecification} objects.
   * @param opsSource input {@link OPSSource}.
   * @return new {@link OPSSourceCharacteristics} object.
   * @throws AeriusException
   */
  public static OPSSourceCharacteristics toSourceCharacteristics(final OPSSource opsSource,
      final ImportBuildingHandler importBuildingHandler) throws AeriusException {
    final OPSSourceCharacteristics characteristics = new OPSSourceCharacteristics();
    characteristics.setDiameter(opsSource.getDiameter());
    characteristics.setDiurnalVariation(DiurnalVariation.safeValueOf(opsSource.getDiurnalVariation()));
    characteristics.setHeatContentType(opsSource.getHeatContent() < 0 ? HeatContentType.FORCED : HeatContentType.NOT_FORCED);
    characteristics.setHeatContent(opsSource.getHeatContent());
    characteristics.setEmissionHeight(opsSource.getEmissionHeight());

    characteristics.setOutflowDiameter(opsSource.getOutflowDiameter());
    characteristics.setOutflowVelocity(opsSource.getOutflowVelocity());
    characteristics.setEmissionTemperature(opsSource.getEmissionTemperature());
    characteristics.setOutflowDirection(
        opsSource.getOutflowDirection() == OPSSourceOutflowDirection.HORIZONTAL
            ? OutflowDirectionType.HORIZONTAL
            : OutflowDirectionType.VERTICAL);
    if (opsSource.getBuildingHeight() > 0 && opsSource.getBuildingLength() > 0 && opsSource.getBuildingWidth() > 0) {
      final String buildingId = importBuildingHandler.handle(
          opsSource.getBuildingLength(),
          opsSource.getBuildingWidth(),
          opsSource.getBuildingHeight(),
          (int) GeometryUtil.orientationXToNorth(opsSource.getBuildingOrientation()));
      characteristics.setBuildingId(buildingId);
    }
    characteristics.setSpread(opsSource.getSpread());
    characteristics.setParticleSizeDistribution(opsSource.getParticleSizeDistribution());

    return characteristics;
  }
}
