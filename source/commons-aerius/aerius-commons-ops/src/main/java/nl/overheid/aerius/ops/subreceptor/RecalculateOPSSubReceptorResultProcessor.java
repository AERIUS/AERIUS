/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.subreceptor;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.io.ControlFileOptions;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Merges the results on the receptor that was recalculated by the subreceptor calculation.
 *
 * Subreceptor recalculation is done by first calculating the result by all sources.
 * Than the single source that is to be recalculated is recalculated with the receptors within the hexagon.
 * The new result is calculated by substracting the result by the single source on the original receptor coordinate
 * and to this the average of the subreceptors is added (minus receptors within 20 meter of the original source).
 */
public class RecalculateOPSSubReceptorResultProcessor {
  private static final String PRECISION_FORMAT = "0.###E0";
  private static final double MIN_SOURCE_RECEPTOR_DISTANCE = 20.0;

  private static final Logger LOG = LoggerFactory.getLogger(RecalculateOPSSubReceptorResultProcessor.class);

  /**
   * Replace and recalculate the results on the recalculated subreceptor.
   *
   * @param inHexagonSource source that was recalculated
   * @param originalResultPoint original result run
   * @param cfo single run OPS configuration
   * @param subResults results of the subreceptor calculation
   */
  public void replaceResults(final OPSSource inHexagonSource, final AeriusResultPoint originalResultPoint, final ControlFileOptions cfo,
      final List<AeriusResultPoint> subResults) {
    //replace the results
    for (final Entry<EmissionResultKey, Double> originalResultEntry : originalResultPoint.getEmissionResults().entrySet()) {
      if (originalResultEntry.getKey().getSubstance() == cfo.getSubstance()) {
        recalculateSubstanceResult(inHexagonSource, originalResultPoint.getId(), subResults, originalResultEntry);
      }
    }
  }

  private static void recalculateSubstanceResult(final OPSSource inHexagonSource, final Integer originalId, final List<AeriusResultPoint> subResults,
      final Entry<EmissionResultKey, Double> originalResultEntry) {
    double midPointResult = 0.0;
    double totalResult = 0.0;
    int pointsUsed = 0;
    for (final AeriusResultPoint subResult : subResults) {
      if (shouldIncludeResult(inHexagonSource, subResult)) {
        totalResult += subResult.getEmissionResult(originalResultEntry.getKey());
        pointsUsed++;
      }
      if (subResult.getId() == originalId) {
        midPointResult = subResult.getEmissionResult(originalResultEntry.getKey());
      }
    }
    //result should be the original value - the value of the point with this source (to obtain the results of all the other sources in the
    // calculation) + the average of the sub calculation.
    final double newValue = pointsUsed == 0 ? 0
        : roundToProperPrecision(originalResultEntry.getValue() - midPointResult + totalResult / pointsUsed);
    if (LOG.isTraceEnabled() && pointsUsed > 0) {
      LOG.trace("Original value {} source only recalulated to {} and total subpoints results {} averaged to {} resulting in new value {}",
          originalResultEntry.getValue(), midPointResult, totalResult, totalResult / pointsUsed, newValue);
    }
    originalResultEntry.setValue(newValue);
  }

  private static double roundToProperPrecision(final double value) {
    final NumberFormat formatter = new DecimalFormat(PRECISION_FORMAT, DecimalFormatSymbols.getInstance(Locale.ENGLISH));
    return Double.parseDouble(formatter.format(value));
  }

  /**
   * Determine if the results for the source should be included for a specific point.
   * @param source The source to determine for.
   * @param point The point to determine for.
   * @return True if the result for the combination should be added, false if not.
   */
  private static boolean shouldIncludeResult(final OPSSource source, final AeriusPoint point) {
    return point.distance(source.getPoint()) > MIN_SOURCE_RECEPTOR_DISTANCE;
  }
}
