/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.domain;

import java.io.File;

import nl.overheid.aerius.ops.version.OPSVersion;
import nl.overheid.aerius.worker.EngineWorkerConfiguration;

/**
 * Configuration file for OPS.
 */
public class OPSConfiguration extends EngineWorkerConfiguration<OPSVersion> {

  private String settingsMeteoFile;
  private String settingsLandUse;
  private GridSize gridSize;

  public OPSConfiguration(final int processes) {
    super(processes);
  }

  /**
   * @return The directory path containing the OPS installation.
   */
  public File getOpsRoot(final OPSVersion opsVersion) {
    final File versionsRoot = getVersionsRoot();

    return versionsRoot == null ? null : new File(versionsRoot, opsVersion.getVersion());
  }

  public GridSize getSettingsGridSize(final GridSize defaultGridSize) {
    return gridSize == null ? defaultGridSize : gridSize;
  }

  public String getSettingsMeteoFile() {
    return settingsMeteoFile;
  }

  void setSettingsMeteoFile(final String settingsMeteoFile) {
    this.settingsMeteoFile = settingsMeteoFile;
  }

  public String getSettingsLandUse() {
    return settingsLandUse;
  }

  void setSettingsLandUse(final String settingsLandUse) {
    this.settingsLandUse = settingsLandUse;
  }

  public File getTerrainPropertiesFile(final OPSVersion opsVersion) {
    return getDataFilePath(opsVersion.getTerrainPropertiesVersion(), ".txt");
  }

  private File getDataFilePath(final String name, final String extension) {
    return getDataFilePath(name, name, extension);
  }

  private File getDataFilePath(final String directory, final String name, final String extension) {
    return getVersionsRoot() == null ? null : new File(new File(getVersionsRoot(), directory), name + extension);
  }
}
