/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.runner;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import nl.overheid.aerius.ops.io.ControlFileOptions;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Interface for OPS processes
 *
 * @param <T> type of the data returned by the process
 */
public interface OPSRunner<T> {

  default void init(final Collection<OPSReceptor> receptors, final Set<EmissionResultKey> erks) {}

  /**
   * Method to perform the actual task.
   *
   * @param runId unique id for the run
   * @param runDirectory directory where the input files are located
   * @param parameters OPS command line parameters
   * @param cfo OPS run configuration
   * @return result of the operation
   * @throws Exception throws exception in case something fails
   */
  T run(String runId, File runDirectory, List<String> parameters, ControlFileOptions cfo) throws IOException, AeriusException;

  /**
   * Returns the set of receptors that should be use to check for recalculation subreceptors.
   *
   * @param keys emission results keys
   * @param receptors original receptors
   * @param result from Runner run
   *
   * @return set of receptors to use for sub receptor recalculation
   */

  default T mergeResults(final Set<EmissionResultKey> keys, final T results1, final T results2) {
    return results1;
  }

  default T aggregate(final T results) {
    return results;
  }

  /**
   * Result to return when there is no emission for a given substance.
   *
   * @param receptors receptors that are to be calculated
   * @param erks emission result keys results are expected for
   * @return Adapater result
   */
  T emptyResultsForSubstance(Collection<OPSReceptor> receptors, Set<EmissionResultKey> erks);
}
