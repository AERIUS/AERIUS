/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.background;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.ops.LandUse;
import nl.overheid.aerius.shared.domain.ops.OPSTerrainProperties;

/**
 * Utility class for reading OPS terrain properties files.
 */
public final class TerrainPropertiesReader {

  // Land use is pretty specific: array of 9 values.
  private static final Pattern LAND_USE_PATTERN = Pattern.compile("\\{(\\d+),(\\d+),(\\d+),(\\d+),(\\d+),(\\d+),(\\d+),(\\d+),(\\d+)\\}");
  private static final Pattern SPLIT_PATTERN = Pattern.compile("\t");
  // The expected columns: receptor_id average_roughness dominant_land_use land_uses
  private static final int COLUMNS = 4;

  private static final int RECEPTOR_INDEX = 0;
  private static final int ROUGHNESS_INDEX = 1;
  private static final int DOMINANT_LAND_USE_INDEX = 2;
  private static final int LAND_USES_INDEX = 3;

  private TerrainPropertiesReader() {
    // util class.
  }

  /**
   * Reads the deposition velocity data from the given input stream.
   *
   * @param inputStream
   *          The input stream containing the deposition velocities.
   * @param hexHor Number of hexagons on a receptor grid row for zoomlevel 1.
   * @return A mapping from receptor id to deposition velocity.
   * @throws IOException
   *           Thrown if an error occurred while reading the stream.
   */
  public static TerrainPropertiesMap read(final InputStream inputStream, final ReceptorGridSettings receptorGridSettings) throws IOException {
    final TerrainPropertiesMap terrainPropertiesMap = new TerrainPropertiesMap(receptorGridSettings);

    try (final InputStreamReader is = new InputStreamReader(inputStream, StandardCharsets.UTF_8.name());
        final LineNumberReader reader = new LineNumberReader(is)) {
      // Discard first line as it contains the headers.
      final String header = reader.readLine();
      if (header != null) {
        for (String line = reader.readLine(); line != null; line = reader.readLine()) {
          parseLine(terrainPropertiesMap, reader, line);
        }
      }
    }

    return terrainPropertiesMap;
  }

  private static void parseLine(final TerrainPropertiesMap terrainPropertiesMap,
      final LineNumberReader reader, final String line) throws IOException {
    final String[] parts = SPLIT_PATTERN.split(line);
    if (parts.length != COLUMNS) {
      throw new IOException("Invalid number of columns for line " + reader.getLineNumber()
          + ". Expected " + COLUMNS + ", actual " + parts.length);
    }
    final int receptorId = parseReceptorId(reader, parts);
    final double averageRoughness = parseAverageRoughness(reader, parts);
    final LandUse dominantLandUse = parseDominantLanduse(reader, parts);
    final int[] landUses = parseLandUses(reader, parts);

    final OPSTerrainProperties terrainProperties = new OPSTerrainProperties(averageRoughness, dominantLandUse, landUses);
    terrainPropertiesMap.put(receptorId, terrainProperties);
  }

  private static int parseReceptorId(final LineNumberReader reader, final String... parts) throws IOException {
    final String part = parts[RECEPTOR_INDEX];
    final int receptorId;
    try {
      receptorId = Integer.parseInt(part);
    } catch (final NumberFormatException e) {
      throw createException(part, "receptor id", reader, e);
    }
    return receptorId;
  }

  private static double parseAverageRoughness(final LineNumberReader reader, final String... parts) throws IOException {
    final String part = parts[ROUGHNESS_INDEX];
    final double averageRoughness;
    try {
      averageRoughness = Double.parseDouble(part);
    } catch (final NumberFormatException e) {
      throw createException(part, "average roughness", reader, e);
    }
    return averageRoughness;
  }

  private static LandUse parseDominantLanduse(final LineNumberReader reader, final String... parts) throws IOException {
    final String part = parts[DOMINANT_LAND_USE_INDEX];
    final LandUse dominantLandUse = LandUse.safeValueOf(part);
    if (dominantLandUse == null) {
      throw createException(part, "dominant land use", reader);
    }
    return dominantLandUse;
  }

  private static int[] parseLandUses(final LineNumberReader reader, final String... parts) throws IOException {
    final String part = parts[LAND_USES_INDEX];
    final Matcher matcher = LAND_USE_PATTERN.matcher(part);
    if (!matcher.find()) {
      throw createException(part, "land uses", reader);
    }
    final int[] landUses = new int[matcher.groupCount()];
    for (int i = 0; i < matcher.groupCount(); i++) {
      landUses[i] = Integer.parseInt(matcher.group(i + 1));
    }
    return landUses;
  }

  private static IOException createException(final String part, final String description, final LineNumberReader reader, final Exception cause) {
    return new IOException("Error parsing " + description + " (" + part + ")  on line " + reader.getLineNumber(), cause);
  }

  private static IOException createException(final String part, final String description, final LineNumberReader reader) {
    return new IOException("Error parsing " + description + " (" + part + ")  on line " + reader.getLineNumber());
  }

}
