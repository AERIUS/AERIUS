/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import nl.overheid.aerius.ops.domain.OPSConfiguration;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;

/**
 * Writes all input files for a single OPS run into a temporary directory.
 */
public class OPSFileWriter {

  private final OPSConfiguration configuration;

  /**
   * Create an OPSFileWriter to generate OPS files with.
   *
   * @param configuration global ops options.
   */
  public OPSFileWriter(final OPSConfiguration configuration) {
    this.configuration = configuration;
  }

  /**
   * Create and write all configuration files to disk.
   *
   * @param runId The run ID to use. Will be used as sub-directory of the base directory to write all files to on disk.
   * @param runDirectory directory to store ops files in.
   * @param sources The source data to use.
   * @param receptors the receptors to use.
   * @param controlFileOptions options to use
   * @throws IOException In case writing a file fails.
   * @return the 'virtual' location of the settings file to be used by OPS.
   * @throws IOException In case writing a file fails.
   */
  public String writeFiles(final String runId, final File runDirectory, final Collection<OPSSource> sources, final Collection<OPSReceptor> receptors,
      final ControlFileOptions controlFileOptions) throws IOException {
    BrnFileWriter.writeFile(runDirectory, sources, controlFileOptions.getSubstance(), controlFileOptions.getOpsOptions());
    RcpFileWriter.writeFile(runDirectory, receptors);

    final String meteoFile = ControlFileWriter.toOpsMeteoFile(controlFileOptions.getMeteo());
    ControlFileWriter.writeFile(configuration, runDirectory, runId, controlFileOptions, meteoFile);
    return ControlFileWriter.controlFileLocation(runDirectory);
  }
}
