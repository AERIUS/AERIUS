/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.version;

import java.util.Arrays;
import java.util.stream.Collectors;

import nl.overheid.aerius.ops.exception.OPSInvalidVersionException;
import nl.overheid.aerius.util.OSUtils;

/**
 * Class that retrieves the configured OPS version and contains checks to determine if the binary used is version expected.
 */
public final class OPSVersionUtil {

  private OPSVersionUtil() {
  }

  /**
   * Validates if the given OPS version matches an expected version by this product. If it doesn't match it throws an
   * {@link OPSInvalidVersionException}.
   *
   * @param versionString version to check
   * @throws OPSInvalidVersionException in case OPS version doesn't match
   */
  public static void validateVersion(final String versionString) throws OPSInvalidVersionException {
    if (Arrays.stream(OPSVersion.values()).noneMatch(opsVersion -> opsVersion.getVersion().equals(versionString))) {
      throw new OPSInvalidVersionException(
          "Data received with version number '" + versionString + "', expecting one of: " + getValidVersionStrings() + ".");
    }
  }

  /**
   * Validates if the version returned by OPS matches any of the expected version information pattern.
   * If it doesn't match it throws an {@link OPSInvalidVersionException}.
   *
   * @param opsVersion The ops version to test
   * @param opsOutput ops output to match.
   * @return true if version is supported by the application
   * @throws OPSInvalidVersionException if it doesn't match else returns null.
   */
  public static boolean validateOPSVersion(final OPSVersion opsVersion, final String opsOutput) throws OPSInvalidVersionException {
    final String trimmedOpsOutput = opsOutput.trim();
    final String patternToMatch = OSUtils.isWindows() ? opsVersion.getWindowsVersion() : opsVersion.getLinuxVersion();

    if (patternToMatch.equals(trimmedOpsOutput)) {
      return true;
    }
    throw new OPSInvalidVersionException(
        "Invalid OPS version configured. Expected one of " + getValidOSVersionStrings() + ", but found: " + trimmedOpsOutput);
  }

  private static String getValidVersionStrings() {
    return Arrays.stream(OPSVersion.values())
        .map(opsVersion -> String.format("'%s'", opsVersion.getVersion()))
        .collect(Collectors.joining(", "));
  }

  private static String getValidOSVersionStrings() {
    return Arrays.stream(OPSVersion.values())
        .map(opsVersion -> String.format("'%s'", OSUtils.isWindows() ? opsVersion.getWindowsVersion() : opsVersion.getLinuxVersion()))
        .collect(Collectors.joining(", "));
  }
}
