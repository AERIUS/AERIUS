/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.domain;

import nl.overheid.aerius.receptor.HexagonEquilateralTrianglesMap;
import nl.overheid.aerius.receptor.SubReceptorCreator;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.ops.OPSSubReceptor;
import nl.overheid.aerius.shared.domain.theme.own2000.OwN2000SubReceptorConstants;

/**
 * Class to build a {@link HexagonEquilateralTrianglesMap} specific for creating sub receptors to use with OPS.
 */
public class OPSSubReceptorCreator extends SubReceptorCreator<OPSSubReceptor> {

  /**
   * Constructs the sub receptor creator.
   *
   * @param aeriusPointType point type of the sub receptor
   * @param hexagonRadius radius of the hexagon
   */
  public OPSSubReceptorCreator(final AeriusPointType aeriusPointType, final double hexagonRadius) {
    super(aeriusPointType, hexagonRadius, OwN2000SubReceptorConstants.SUB_RECEPTOR_DISTANCES, OwN2000SubReceptorConstants.LEVEL_OFFSET);
  }

  @Override
  public OPSSubReceptor copyToSubReceptorPoint(final AeriusPoint point, final AeriusPointType pointType, final int level) {
    final OPSSubReceptor opsSubReceptor = new OPSSubReceptor(point, pointType, level);

    if (point instanceof final OPSReceptor opsReceptor) {
      opsSubReceptor.setTerrainProperties(opsReceptor.getTerrainProperties());
    }
    return opsSubReceptor;
  }

}
