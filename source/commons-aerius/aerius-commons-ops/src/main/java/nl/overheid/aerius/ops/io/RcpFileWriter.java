/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.function.Function;

import org.apache.commons.lang3.StringUtils;

import nl.overheid.aerius.ops.runner.OPSRunner;
import nl.overheid.aerius.shared.ImaerConstants;
import nl.overheid.aerius.shared.domain.ops.LandUse;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.ops.OPSTerrainProperties;
import nl.overheid.aerius.util.AeriusPointUtil;
import nl.overheid.aerius.util.OSUtils;

/**
 * Class to write OPS receptors to a receptors.rcp file.
 */
final class RcpFileWriter {

  /**
   * @see OPSRunner#createSourceInputFile
   */
  private static final String FILENAME = "receptors.rcp";
  private static final int HEIGHT_RESOLUTION = 3;
  private static final int AVERAGE_ROUGHNESS_RESOLUTION = 6;
  private static final String HEADER = "Nr Name X-coor Y-coor";
  private static final String HEADER_WITH_HEIGHT = HEADER + " Height";
  private static final String HEADER_TERRAIN_PART = " AVG-z0 DOM-LU LU1 LU2 LU3 LU4 LU5 LU6 LU7 LU8 LU9";
  private static final String HEADER_WITH_TERRAIN = HEADER + HEADER_TERRAIN_PART;
  private static final String HEADER_WITH_HEIGHT_AND_TERRAIN = HEADER_WITH_HEIGHT + HEADER_TERRAIN_PART;
  private static final String STRING_FORMAT_BASE = "%d %s %d %d";
  // Format: 0 ID Xcoord Ycoord
  private static final String STRING_FORMAT = STRING_FORMAT_BASE + OSUtils.NL;
  private static final String STRING_FORMAT_WITH_HEIGHT_BASE = STRING_FORMAT_BASE + " %s";
  //Format: 0 ID Xcoord Ycoord height
  private static final String STRING_FORMAT_WITH_HEIGHT = STRING_FORMAT_WITH_HEIGHT_BASE + OSUtils.NL;
  private static final String STRING_FORMAT_TERRAIN_PART = " %s %d %s";
  //Format: 0 ID Xcoord Ycoord z0 Landuse Landuses
  private static final String STRING_FORMAT_WITH_TERRAIN = STRING_FORMAT_BASE + STRING_FORMAT_TERRAIN_PART + OSUtils.NL;
  //Format: 0 ID Xcoord Ycoord height z0 Landuse Landuses
  private static final String STRING_FORMAT_WITH_HEIGHT_AND_TERRAIN = STRING_FORMAT_WITH_HEIGHT_BASE
      + STRING_FORMAT_TERRAIN_PART + OSUtils.NL;

  private RcpFileWriter() {
  }

  public static String getFileName() {
    return FILENAME;
  }

  /**
   * @param path File containing the path to create the receptor file in.
   * @param opsReceptors The receptors to write in the file (according to OPS-format).
   * @throws IOException When an error occurs writing to the file.
   */
  public static void writeFile(final File path, final Collection<OPSReceptor> opsReceptors) throws IOException {
    try (final Writer writer = Files.newBufferedWriter(new File(path, FILENAME).toPath(), StandardCharsets.UTF_8)) {
      if (opsReceptors.isEmpty()) {
        writer.write(HEADER_WITH_TERRAIN + OSUtils.NL);
        return;
      }

      final boolean hasNoTerrain = opsReceptors.iterator().next().hasNoTerrainData();
      final boolean hasNoHeight = opsReceptors.iterator().next().hasNoHeight();

      if (hasNoHeight && hasNoTerrain) {
        writeNoHeightAndNoTerrain(writer, opsReceptors);
      } else if (hasNoTerrain) {
        writeHeightAndNoTerrain(writer, opsReceptors);
      } else if (hasNoHeight) {
        writeNoHeightAndTerrain(writer, opsReceptors);
      } else {
        writeHeightAndTerrain(writer, opsReceptors);
      }
    }
  }

  private static void writeNoHeightAndNoTerrain(final Writer writer, final Collection<OPSReceptor> opsReceptors) throws IOException {
    writer.write(HEADER + OSUtils.NL);
    for (final OPSReceptor opsReceptor : opsReceptors) {
      writer.write(String.format(Locale.US, STRING_FORMAT, 0,
          AeriusPointUtil.encodeName(opsReceptor),
          opsReceptor.getRoundedX(), opsReceptor.getRoundedY()));
    }
  }

  private static void writeHeightAndNoTerrain(final Writer writer, final Collection<OPSReceptor> opsReceptors) throws IOException {
    writer.write(HEADER_WITH_HEIGHT + OSUtils.NL);
    for (final OPSReceptor opsReceptor : opsReceptors) {
      writer.write(String.format(Locale.US, STRING_FORMAT_WITH_HEIGHT, 0,
          AeriusPointUtil.encodeName(opsReceptor),
          opsReceptor.getRoundedX(), opsReceptor.getRoundedY(),
          getHeightAsString(opsReceptor)));
    }
  }

  private static void writeNoHeightAndTerrain(final Writer writer, final Collection<OPSReceptor> opsReceptors) throws IOException {
    writer.write(HEADER_WITH_TERRAIN + OSUtils.NL);
    for (final OPSReceptor opsReceptor : opsReceptors) {
      writer.write(String.format(Locale.US, STRING_FORMAT_WITH_TERRAIN, 0,
          AeriusPointUtil.encodeName(opsReceptor),
          opsReceptor.getRoundedX(), opsReceptor.getRoundedY(),
          getAverageRoughnessAsString(opsReceptor),
          getLandUsesOption(opsReceptor),
          getLandUsesAsString(opsReceptor)));
    }
  }

  private static void writeHeightAndTerrain(final Writer writer, final Collection<OPSReceptor> opsReceptors) throws IOException {
    writer.write(HEADER_WITH_HEIGHT_AND_TERRAIN + OSUtils.NL);
    for (final OPSReceptor opsReceptor : opsReceptors) {
      writer.write(String.format(Locale.US, STRING_FORMAT_WITH_HEIGHT_AND_TERRAIN, 0,
          AeriusPointUtil.encodeName(opsReceptor),
          opsReceptor.getRoundedX(), opsReceptor.getRoundedY(),
          getHeightAsString(opsReceptor),
          getAverageRoughnessAsString(opsReceptor),
          getLandUsesOption(opsReceptor),
          getLandUsesAsString(opsReceptor)));
    }
  }

  private static int getLandUsesOption(final OPSReceptor opsReceptor) {
    final LandUse landUse = Optional.ofNullable(opsReceptor.getTerrainProperties())
        .map(OPSTerrainProperties::getLandUse)
        .orElse(LandUse.OTHER_NATURE);
    return landUse.getOption();
  }

  private static String getHeightAsString(final OPSReceptor opsReceptor) {
    return getAsString(opsReceptor, OPSReceptor::getHeight, HEIGHT_RESOLUTION);
  }

  private static String getAverageRoughnessAsString(final OPSReceptor opsReceptor) {
    return getAsString(opsReceptor,
        r -> Optional.ofNullable(r).map(OPSReceptor::getTerrainProperties).map(OPSTerrainProperties::getAverageRoughness).orElse(null),
        AVERAGE_ROUGHNESS_RESOLUTION);
  }

  private static String getAsString(final OPSReceptor opsReceptor, final Function<OPSReceptor, Double> function, final int scale) {
    final Double value = function.apply(opsReceptor);
    return value == null ? "0" : BigDecimal.valueOf(value).setScale(scale, RoundingMode.HALF_UP).toString();
  }

  private static String getLandUsesAsString(final OPSReceptor opsReceptor) {
    int[] landUses = Optional.ofNullable(opsReceptor.getTerrainProperties())
        .map(OPSTerrainProperties::getLandUses)
        .orElse(null);
    if (landUses == null) {
      // Creates an array with 9 occurrences of 0.
      landUses = new int[LandUse.OPS_LAND_USE_CLASSES];
      //set the correct column to represent 100% of the landuse. Landuse starts at 1 however.
      landUses[getLandUsesOption(opsReceptor) - 1] = ImaerConstants.PERCENTAGE_TO_FRACTION;
    }

    final List<String> percentages = new ArrayList<>(landUses.length);
    for (final int landUse : landUses) {
      percentages.add(String.valueOf(landUse));
    }

    return StringUtils.join(percentages, ' ');
  }
}
