/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import nl.overheid.aerius.ops.background.OPSData;
import nl.overheid.aerius.ops.domain.OPSConfiguration;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.exception.OPSInvalidVersionException;
import nl.overheid.aerius.ops.io.ControlFileOptions;
import nl.overheid.aerius.ops.runner.OPSCollector;
import nl.overheid.aerius.ops.runner.OPSPerformer;
import nl.overheid.aerius.ops.runner.OPSRunner;
import nl.overheid.aerius.ops.runner.OPSSingleSubstanceRunner;
import nl.overheid.aerius.ops.subreceptor.OPSSubReceptorAggregator;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.IsSubPoint;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResults;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Handles a full runs of OPS. Includes writing configuration files,
 * instrumenting OPS and reading output from OPS output files. This class can
 * be reused to run multiple runs. In the constructor the location where OPS
 * installation(s) are expected is initialized and each run is started via
 * {@link #run(OPSInputData)}.
 *
 * This class is thread safe.
 */
class OPSProcessRunner {

  /**
   * Validation error should be thrown.
   */
  private static final boolean VALIDATION_ERRORS_TO_FILE = false;

  private final OPSConfiguration configuration;
  private final OPSSingleSubstanceRunner runner;
  private final OPSData opsData;

  /**
   * Initializes the class with OPS location information.
   *
   * @param configuration OPS configuration
   */
  public OPSProcessRunner(final OPSConfiguration configuration, final OPSData opsData) {
    this.configuration = configuration;
    this.opsData = opsData;
    final File opsVersionsRoot = configuration.getVersionsRoot();
    if (opsVersionsRoot == null || configuration.getRunFilesDirectory() == null) {
      throw new IllegalArgumentException("No argument allowed to be null or empty");
    }
    runner = new OPSSingleSubstanceRunner(configuration);
  }

  OPSSingleSubstanceRunner getRunner() {
    return runner;
  }

  /**
   * Perform a OPS run for each substance present in the input data.
   * @param inputData all input values used in a OPS calculation
   * @return the result of an OPS calculation
   * @throws IOException IOException
   * @throws AeriusException AeriusException
   * @throws OPSInvalidVersionException
   */
  public CalculationResult run(final OPSInputData input) throws IOException, AeriusException, OPSInvalidVersionException {
    final CalculationResult cr = new CalculationResult(input.getChunkStats(), input.getEngineDataKey());
    final OPSCollector<List<AeriusResultPoint>> collector = new OPSCollector<>() {

      @Override
      public void collect(final Integer sourceKey, final Map<Substance, List<AeriusResultPoint>> results) {
        cr.put(sourceKey, OPSResultCollector.mergeResults(results));
      }
    };
    final OPSPerformer<List<AeriusResultPoint>> performer = new OPSPerformer<>(new OPSRunnerImpl(runner, input.isMaxDistance()), collector, opsData,
        configuration, VALIDATION_ERRORS_TO_FILE, configuration.isKeepGeneratedFiles());

    performer.run(input);
    return cr;
  }

  private static class OPSRunnerImpl implements OPSRunner<List<AeriusResultPoint>> {
    private final OPSSingleSubstanceRunner runner;
    private OPSSubReceptorAggregator aggregator;
    private final boolean maxDistance;

    public OPSRunnerImpl(final OPSSingleSubstanceRunner runner, final boolean maxDistance) {
      this.runner = runner;
      this.maxDistance = maxDistance;
    }

    @Override
    public void init(final Collection<OPSReceptor> receptors, final Set<EmissionResultKey> erks) {
      aggregator = new OPSSubReceptorAggregator(receptors, erks, maxDistance);
    }

    @Override
    public List<AeriusResultPoint> run(final String runId, final File runDirectory, final List<String> parameters, final ControlFileOptions cfo)
        throws IOException, AeriusException {
      runner.run(runId, runDirectory, parameters, cfo.getOpsVersion());
      return OPSResultCollector.collectResults(runDirectory, cfo.getSubstance(), cfo.getEmissionResultKeys());
    }

    @Override
    public List<AeriusResultPoint> aggregate(final List<AeriusResultPoint> results) {
      return aggregator.aggregate(results);
    }

    @Override
    public List<AeriusResultPoint> mergeResults(final Set<EmissionResultKey> keys, final List<AeriusResultPoint> results1,
        final List<AeriusResultPoint> results2) {
      final Map<AeriusResultPoint, AeriusResultPoint> results1Map = results1.stream()
          .collect(Collectors.toMap(Function.identity(), Function.identity()));
      results2.forEach(result2 -> {
        final EmissionResults r1 = results1Map.get(result2).getEmissionResults();

        keys.forEach(key -> result2.getEmissionResults().add(key, r1.get(key)));
      });
      return results2;
    }

    /**
     * This method assumes all receptors are sub receptor points. It will derive the receptor point from the sub receptor for the center point
     * and return a list receptor points with zero values for emission keys.
     */
    @Override
    public List<AeriusResultPoint> emptyResultsForSubstance(final Collection<OPSReceptor> receptors, final Set<EmissionResultKey> erks) {
      final List<AeriusResultPoint> emptyResults = new ArrayList<>();
      for (final OPSReceptor opsr : receptors) {
        if (opsr instanceof final IsSubPoint opsSP && opsSP.getPointType().isSubReceptor()) {
          continue;
        }
        final AeriusResultPoint rp = new AeriusResultPoint(opsr.getId(), opsr.getParentId(), opsr.getPointType(), Math.round(opsr.getX()),
            Math.round(opsr.getY()));

        for (final EmissionResultKey erk : erks) {
          rp.setEmissionResult(erk, 0.0);
        }
        emptyResults.add(rp);
      }
      return emptyResults;
    }
  }
}
