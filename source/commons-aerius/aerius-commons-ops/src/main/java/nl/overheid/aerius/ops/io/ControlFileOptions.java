/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import java.util.Set;
import java.util.stream.Collectors;

import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.version.OPSVersion;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.OPSOptions;
import nl.overheid.aerius.shared.domain.meteo.Meteo;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Dynamic OPS global options related to a single OPS run.
 */
public class ControlFileOptions {

  private final OPSVersion opsVersion;
  private final Substance substance;
  private final int year;
  private final Meteo meteo;
  private final OPSOptions opsOptions;
  private final boolean minDistance;
  private final Set<EmissionResultKey> erks;
  private final String roadsCats;

  public ControlFileOptions(final OPSInputData input, final Substance substance, final boolean minDistance, final Set<EmissionResultKey> erks,
      final Set<Integer> roadsCats) {
    opsVersion = (OPSVersion) input.getEngineVersion();
    this.substance = substance;
    this.year = input.getYear();
    this.meteo = input.getMeteo();
    this.opsOptions = input.getOpsOptions();
    this.minDistance = minDistance;
    this.erks = erks;
    this.roadsCats = roadsCats.stream().sorted().map(Object::toString).collect(Collectors.joining(" "));
  }

  public OPSVersion getOpsVersion() {
    return opsVersion;
  }

  public Substance getSubstance() {
    return substance;
  }

  public int getYear() {
    return year;
  }

  public Meteo getMeteo() {
    return meteo;
  }

  public OPSOptions getOpsOptions() {
    return opsOptions;
  }

  public boolean isMinDistance() {
    return minDistance;
  }

  public Set<EmissionResultKey> getEmissionResultKeys() {
    return erks;
  }

  public String getRoadsCats() {
    return roadsCats;
  }
}
