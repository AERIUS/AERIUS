/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.domain;

import static nl.overheid.aerius.shared.geo.EPSG.RDNEW;

import java.util.ArrayList;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.geo.EPSG;

/**
 * Constants for OPS worker.
 */
public final class OPSConstants {

  private static final EPSG EPSG = RDNEW;
  private static final int HEX_HOR = 1529;
  private static final double MIN_X = 3604.0;
  private static final double MAX_X = 287959.0;
  private static final double MIN_Y = 296800.0;
  private static final double MAX_Y = 629300.0;

  private static final HexagonZoomLevel ZOOM_LEVEL_1 = new HexagonZoomLevel(1, 10_000);

  private OPSConstants() {
  }

  /**
   * @return a new ReceptorGridSettings object
   */
  public static ReceptorGridSettings getReceptorGridSettings() {
    final ArrayList<HexagonZoomLevel> hexagonZoomLevels = new ArrayList<>();
    hexagonZoomLevels.add(ZOOM_LEVEL_1);
    final BBox boundingBox = new BBox(MIN_X, MIN_Y, MAX_X, MAX_Y);
    return new ReceptorGridSettings(boundingBox, EPSG, HEX_HOR, hexagonZoomLevels);
  }
}
