/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.domain;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import nl.overheid.aerius.shared.domain.EngineBuilding;
import nl.overheid.aerius.shared.domain.EngineEmissionSource;
import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.shared.domain.ops.OPSLimits;

/**
 * Data class of ops emission data. emission (q in ops) - emission or source strength (kg/y). The worker expects kg/y, OPS however uses g/s. Correct
 * factor is applied by the worker.
 */
public class OPSSource extends EngineEmissionSource implements EngineBuilding, HasId {

  private static final long serialVersionUID = 2L;

  public enum OPSSourceOutflowDirection {
    VERTICAL, HORIZONTAL;
  }

  /**
   * snr >- source identification number (not essential)
   */
  private int id;

  private final OPSPoint point;

  /**
   * heatContent - heat content (MW)
   */
  private double heatContent;

  /**
   * emissionHeight - source emission height (m)
   */
  private double emissionHeight;

  /**
   * diameter - source diameter (m)
   */
  private int diameter;

  /**
   * spread - vertical spread of source height(s) (m)
   */
  private double spread;

  /**
   * D_stack - Inner diameter stack (m)
   */
  private double outflowDiameter;

  /**
   * V_stack - exit velocity (m/s)
   */
  private double outflowVelocity;

  /**
   * part of V_stack - outflow direction (horizontal/vertical). If horizontal V_stack should be made negative.
   */
  private OPSSourceOutflowDirection outflowDirection;

  /**
   * Ts_stack - Temperature effluent gas (C)
   */
  private double emissionTemperature;

  /**
   * diurnalVariation - specification for diurnal variation
   */
  private int diurnalVariation;

  /**
   * particleSizeDistribution - code for distribution of particle sizes (0 for gasses, > 0 for particulate matter)
   */
  private int particleSizeDistribution;

  /**
   * cat - source category number (0-9999) (for administrative purposes only)
   */
  private int cat = 1;

  /**
   * area - country or area number (0-9999) (for administrative purposes only)
   */
  private int area = 1; // Netherlands

  /**
   * buildingLength
   */
  private double buildingLength;

  /**
   * buildingWidth
   */
  private double buildingWidth;

  /**
   * buildingHeight
   */
  private double buildingHeight;

  /**
   * buildingOrientation relative to X-axis and counter-clockwise.
   */
  private double buildingOrientation;

  /**
   * comp - name of the substance (not essential)
   */
  private String comp;

  /**
   * Constructor.
   */
  public OPSSource() {
    point = new OPSPoint();
  }

  public OPSSource(final int id) {
    this();

    this.id = id;
  }

  public OPSSource(final int id, final int srid, final double x, final double y) {
    this(id);

    point.setSrid(srid);
    point.setX(x);
    point.setY(y);
  }

  public OPSSource copy() {
    final OPSSource copy = new OPSSource(id, point.getSrid(), point.getX(), point.getY());

    copy.heatContent = heatContent;
    copy.emissionHeight = emissionHeight;
    copy.diameter = diameter;
    copy.spread = spread;
    copy.outflowDiameter = outflowDiameter;
    copy.outflowVelocity = outflowVelocity;
    copy.outflowDirection = outflowDirection;
    copy.emissionTemperature = emissionTemperature;
    copy.diurnalVariation = diurnalVariation;
    copy.particleSizeDistribution = particleSizeDistribution;
    copy.cat = cat;
    copy.area = area;
    copy.buildingWidth = buildingWidth;
    copy.buildingLength = buildingLength;
    copy.buildingHeight = buildingHeight;
    copy.buildingOrientation = buildingOrientation;
    copyEmissionsTo(copy);
    return copy;
  }

  @Override
  public int hashCode() {
    return Objects.hash(
        diameter, diurnalVariation, heatContent, emissionHeight, particleSizeDistribution, spread, outflowDiameter, outflowVelocity, outflowDirection,
        emissionTemperature, buildingWidth, buildingHeight, buildingLength, buildingOrientation, point);
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    final OPSSource other = (OPSSource) obj;
    if (point == null) {
      if (other.point != null) {
        return false;
      }
    } else if (!point.equals(other.point)) {
      return false;
    }
    return diameter == other.diameter
        && diurnalVariation == other.diurnalVariation
        && Double.doubleToLongBits(heatContent) == Double.doubleToLongBits(other.heatContent)
        && Double.doubleToLongBits(emissionHeight) == Double.doubleToLongBits(other.emissionHeight)
        && particleSizeDistribution == other.particleSizeDistribution
        && Double.doubleToLongBits(spread) == Double.doubleToLongBits(other.spread)
        && Double.doubleToLongBits(outflowDiameter) == Double.doubleToLongBits(other.outflowDiameter)
        && Double.doubleToLongBits(outflowVelocity) == Double.doubleToLongBits(other.outflowVelocity)
        && outflowDirection == other.outflowDirection
        && Double.doubleToLongBits(emissionTemperature) == Double.doubleToLongBits(other.emissionTemperature)
        && Double.doubleToLongBits(buildingWidth) == Double.doubleToLongBits(other.buildingWidth)
        && Double.doubleToLongBits(buildingLength) == Double.doubleToLongBits(other.buildingLength)
        && Double.doubleToLongBits(buildingHeight) == Double.doubleToLongBits(other.buildingHeight)
        && (int) buildingOrientation == (int) other.buildingOrientation;
  }

  /**
   * @return the id
   */
  // No limit on the ID, OPS uses a default.
  @Override
  public int getId() {
    return id;
  }

  /**
   * @return the HeatContent
   */
  @Min(value = OPSLimits.SOURCE_HEAT_CONTENT_MINIMUM, message = "ops heat_content > " + OPSLimits.SOURCE_HEAT_CONTENT_MINIMUM)
  @Max(value = OPSLimits.SOURCE_HEAT_CONTENT_MAXIMUM, message = "ops heat_content < " + OPSLimits.SOURCE_HEAT_CONTENT_MAXIMUM)
  public double getHeatContent() {
    return heatContent;
  }

  /**
   * @return the diurnal variation
   */
  @Min(value = OPSLimits.SOURCE_DIURNAL_VARIATION_MINIMUM, message = "ops diurnal variation > " + OPSLimits.SOURCE_DIURNAL_VARIATION_MINIMUM)
  @Max(value = OPSLimits.SOURCE_DIURNAL_VARIATION_MAXIMUM, message = "ops diurnal variation < " + OPSLimits.SOURCE_DIURNAL_VARIATION_MAXIMUM)
  public int getDiurnalVariation() {
    return diurnalVariation;
  }

  /**
   * @return the h
   */
  @Min(value = OPSLimits.SOURCE_EMISSION_HEIGHT_MINIMUM, message = "ops height > " + OPSLimits.SOURCE_EMISSION_HEIGHT_MINIMUM)
  @Max(value = OPSLimits.SOURCE_EMISSION_HEIGHT_MAXIMUM, message = "ops height < " + OPSLimits.SOURCE_EMISSION_HEIGHT_MAXIMUM)
  public double getEmissionHeight() {
    return emissionHeight;
  }

  /**
   * @return the s (Source height distribution)
   */
  @Min(value = OPSLimits.SOURCE_SPREAD_MINIMUM, message = "ops spread < " + OPSLimits.SOURCE_SPREAD_MINIMUM)
  @Max(value = OPSLimits.SOURCE_SPREAD_MAXIMUM, message = "ops spread > " + OPSLimits.SOURCE_SPREAD_MAXIMUM)
  public double getSpread() {
    return spread;
  }

  /**
   * @return the outflow diameter
   */
  @Min(value = (int) OPSLimits.SOURCE_OUTFLOW_DIAMETER_MINIMUM, message = "ops outflow_surface > " + OPSLimits.SOURCE_OUTFLOW_DIAMETER_MINIMUM)
  //@Max(value = (int) OPSLimits.SOURCE_OUTFLOW_DIAMETER_MAXIMUM, message = "ops outflow_surface < " + OPSLimits.SOURCE_OUTFLOW_DIAMETER_MAXIMUM)
  public double getOutflowDiameter() {
    return outflowDiameter;
  }

  /**
   * @return the outflow velocity
   */
  @Min(value = (int) OPSLimits.SOURCE_OUTFLOW_VELOCITY_MINIMUM, message = "ops outflow_velocity > " + OPSLimits.SOURCE_OUTFLOW_VELOCITY_MINIMUM)
  //@Max(value = (int) OPSLimits.SOURCE_OUTFLOW_VELOCITY_MAXIMUM, message = "ops outflow_velocity < " + OPSLimits.SOURCE_OUTFLOW_VELOCITY_MAXIMUM)
  public double getOutflowVelocity() {
    return outflowVelocity;
  }

  /**
   * @return the emission temperature
   */
  @Min(value = (int) OPSLimits.SOURCE_EMISSION_TEMPERATURE_MINIMUM, message = "ops emission_temperature > "
      + (int) OPSLimits.SOURCE_EMISSION_TEMPERATURE_MINIMUM)
  @Max(value = (int) OPSLimits.SOURCE_EMISSION_TEMPERATURE_MAXIMUM, message = "ops emission_temperature < "
      + (int) OPSLimits.SOURCE_EMISSION_TEMPERATURE_MAXIMUM)
  public double getEmissionTemperature() {
    return emissionTemperature;
  }

  /**
   * @return the outflow direction
   */
  public OPSSourceOutflowDirection getOutflowDirection() {
    return outflowDirection;
  }

  /**
   * @return the d
   */
  @Min(value = OPSLimits.SOURCE_DIAMETER_MINIMUM, message = "ops diameter > " + OPSLimits.SOURCE_DIAMETER_MINIMUM)
  @Max(value = OPSLimits.SOURCE_DIAMETER_MAXIMUM, message = "ops diameter < " + OPSLimits.SOURCE_DIAMETER_MAXIMUM)
  public int getDiameter() {
    return diameter;
  }

  /**
   * @return the particle-size distribution
   */
  @Min(value = OPSLimits.SOURCE_PARTICLE_SIZE_DISTRIBUTION_MINIMUM, message = "ops particle_size_distribution > "
      + OPSLimits.SOURCE_PARTICLE_SIZE_DISTRIBUTION_MINIMUM)
  @Max(value = OPSLimits.SOURCE_PARTICLE_SIZE_DISTRIBUTION_MAXIMUM, message = "ops particle_size_distribution < "
      + OPSLimits.SOURCE_PARTICLE_SIZE_DISTRIBUTION_MAXIMUM)
  public int getParticleSizeDistribution() {
    return particleSizeDistribution;
  }

  /**
   * @return the category number
   */
  @Min(value = OPSLimits.SOURCE_CAT_MINIMUM)
  @Max(value = OPSLimits.SOURCE_CAT_MAXIMUM)
  public int getCat() {
    return cat;
  }

  /**
   * @return the area number
   */
  @Min(value = OPSLimits.SOURCE_AREA_MINIMUM)
  @Max(value = OPSLimits.SOURCE_AREA_MAXIMUM)
  public int getArea() {
    return area;
  }

  @Override
  public double getBuildingWidth() {
    return buildingWidth;
  }

  @Override
  public double getBuildingLength() {
    return buildingLength;
  }

  @Override
  public double getBuildingHeight() {
    return buildingHeight;
  }

  @Override
  public double getBuildingOrientation() {
    return buildingOrientation;
  }

  /**
   * @return the comp
   */
  // fortran notation: A12 -> max size is 12
  // No limit on the comp however, OPS uses a default.
  public String getComp() {
    return comp == null ? "" : comp;
  }

  /**
   * @return the point
   */
  @NotNull
  @Valid
  public OPSPoint getPoint() {
    return point;
  }

  /**
   * @param id the id to set
   */
  @Override
  public void setId(final int id) {
    this.id = id;
  }

  /**
   * @param heatContent the heat content to set
   */
  public void setHeatContent(final double heatContent) {
    this.heatContent = heatContent;
  }

  /**
   * @param emissionHeight the height to set
   */
  public void setEmissionHeight(final double emissionHeight) {
    this.emissionHeight = emissionHeight;
  }

  /**
   * @param diameter the diameter to set
   */
  public void setDiameter(final int diameter) {
    this.diameter = diameter;
  }

  /**
   * @param spread the spread to set
   */
  public void setSpread(final double spread) {
    this.spread = spread;
  }

  /**
   * @param emissionTemperature the emission temperature to set
   */
  public void setEmissionTemperature(final double emissionTemperature) {
    this.emissionTemperature = emissionTemperature;
  }

  /**
   * @param outflowDiameter the outflow diameter to set
   */
  public void setOutflowDiameter(final double outflowDiameter) {
    this.outflowDiameter = outflowDiameter;
  }

  /**
   * @param outflowVelocity the exit velocity to set
   */
  public void setOutflowVelocity(final double outflowVelocity) {
    this.outflowVelocity = outflowVelocity;
  }

  /**
   * @param outflowDirection the outflow direction to set
   */
  public void setOutflowDirection(final OPSSourceOutflowDirection outflowDirection) {
    this.outflowDirection = outflowDirection;
  }

  public void setDiurnalVariation(final int diurnalVariation) {
    this.diurnalVariation = diurnalVariation;
  }

  /**
   * @param particleSizeDistribution the particle-size distribution to set
   */
  public void setParticleSizeDistribution(final int particleSizeDistribution) {
    this.particleSizeDistribution = particleSizeDistribution;
  }

  /**
   * @param cat the source category number to set
   */
  public void setCat(final int cat) {
    this.cat = cat;
  }

  /**
   * @param area the area number to set
   */
  public void setArea(final int area) {
    this.area = area;
  }

  /**
   * @param the buildingWidth
   */
  @Override
  public void setBuildingWidth(final double buildingWidth) {
    this.buildingWidth = buildingWidth;
  }

  /**
   * @param the buildingLength
   */
  @Override
  public void setBuildingLength(final double buildingLength) {
    this.buildingLength = buildingLength;
  }

  /**
   * @param the buildingHeight
   */
  @Override
  public void setBuildingHeight(final double buildingHeight) {
    this.buildingHeight = buildingHeight;
  }

  @Override
  public void setBuildingOrientation(final double buildingOrientation) {
    this.buildingOrientation = buildingOrientation;
  }

  /**
   * @param comp the comp (remark) to set
   */
  public void setComp(final String comp) {
    this.comp = comp;
  }

  @Override
  public String toString() {
    return "OPSSource [id=" + id + ", point=" + point + ", heatContent=" + heatContent + ", height=" + emissionHeight + ", diameter=" + diameter
        + ", spread=" + spread + ", outflowDiameter=" + outflowDiameter + ", outflowVelocity=" + outflowVelocity + ", outflowDirection="
        + outflowDirection + ", emissionTemperature=" + emissionTemperature + ", diurnalVariation=" + diurnalVariation + ", particleSizeDistribution="
        + particleSizeDistribution + ", cat=" + cat + ", area=" + area + ", buildingWidth=" + buildingWidth + ", buildingLength=" + buildingLength
        + ", buildingHeight=" + buildingHeight  + ", buildingOrientation=" + buildingOrientation + ", comp=" + comp + "]";
  }



}
