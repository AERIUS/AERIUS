/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.runner;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import nl.overheid.aerius.calculation.EngineInputData.CommandType;
import nl.overheid.aerius.calculation.EngineInputData.SubReceptorCalculation;
import nl.overheid.aerius.ops.SubstanceUtil;
import nl.overheid.aerius.ops.background.OPSData;
import nl.overheid.aerius.ops.domain.OPSConfiguration;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.exception.OPSInvalidVersionException;
import nl.overheid.aerius.ops.io.ControlFileOptions;
import nl.overheid.aerius.ops.io.OPSFileWriter;
import nl.overheid.aerius.ops.subreceptor.OPSSplitSubReceptorCalculation;
import nl.overheid.aerius.ops.version.OPSVersion;
import nl.overheid.aerius.ops.version.OPSVersionUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.LandUse;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.ops.OPSTerrainProperties;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.util.OSUtils;
import nl.overheid.aerius.worker.InputValidatorCollector;

/**
 * Class for processing OPS. Based on the runner implementation this will be to either calculate with OPS or export the OPS input files.
 *
 * @param <T> results data type.
 */
public class OPSPerformer<T> {

  private static final double DEFAULT_AVERAGE_ROUGHNESS = 0.1;
  private static final LandUse DEFAULT_LANDUSE = LandUse.OTHER_NATURE;
  private static final OPSTerrainProperties DEFAULT_TERRAIN_PROPERTIES = new OPSTerrainProperties(DEFAULT_AVERAGE_ROUGHNESS, DEFAULT_LANDUSE);

  private final OPSConfiguration configuration;
  private final boolean keepGeneratedFiles;
  private final OPSRunner<T> runner;
  private final InputValidatorCollector<OPSSource> inputValidatorCollector;
  private final OPSFileWriter opsFileWriter;
  private final OPSCollector<T> collector;
  private final OPSData opsData;

  public OPSPerformer(final OPSRunner<T> runner, final OPSCollector<T> collector, final OPSData opsData, final OPSConfiguration configuration,
      final boolean validationErrorsToFile, final boolean keepGeneratedFiles) {
    this.runner = runner;
    this.collector = collector;
    this.opsData = opsData;
    this.configuration = configuration;
    this.keepGeneratedFiles = keepGeneratedFiles;
    opsFileWriter = new OPSFileWriter(configuration);
    inputValidatorCollector = new InputValidatorCollector<>(validationErrorsToFile, AeriusExceptionReason.OPS_INPUT_VALIDATION);
  }

  /**
   * Performs the run iterative over the sources sets by calling the consumer for each list of sources in the input.
   *
   * @param input OPS input
   * @throws AeriusException
   * @throws OPSInvalidVersionException
   * @throws Exception throws exception in case something fails
   */
  public void run(final OPSInputData input) throws AeriusException, OPSInvalidVersionException, IOException {
    // Don't validate raw ops files
    if (input.getOpsOptions() == null || input.getCommandType() == CommandType.EXPORT ||  !input.getOpsOptions().isRawInput()) {
      validateOPSVersion(input);
      inputValidatorCollector.collect(input, input.getEmissionSources());
    }
    ensureTerrainProperties(input);
    final Collection<OPSReceptor> receptors = input.getCalculateReceptors();
    final OPSRunnerOptions runnerOptions = new OPSRunnerOptions(receptors, input.isMaxDistance(), input.getEngineVersion());
    final List<Substance> supportedSubstances = SubstanceUtil.supportedSubstances(input.getSubstances());
    final Map<Substance, EnumSet<EmissionResultKey>> erkMap = SubstanceUtil.emissionResultKeys(supportedSubstances, input.getEmissionResultKeys());

    for (final Entry<Integer, Collection<OPSSource>> entry : input.getEmissionSources().entrySet()) {
      collector.collect(entry.getKey(),
          runSubstances(input, entry.getKey(), entry.getValue(), receptors, supportedSubstances, erkMap, runnerOptions));
    }
  }

  private void validateOPSVersion(final OPSInputData input) throws OPSInvalidVersionException {
    try {
      OPSVersionUtil.validateVersion(input.getEngineVersion().getVersion());
    } catch (final OPSInvalidVersionException e) {
      if (inputValidatorCollector.addGeneralValidationErrors(e)) {
        throw e;
      }
    }
  }

  private void ensureTerrainProperties(final OPSInputData input) {
    input.getReceptors().stream()
        .forEach(receptor -> ensureTerrainProperties(receptor, (OPSVersion) input.getEngineVersion(), input.isForceTerrainProperties()));
  }

  private void ensureTerrainProperties(final OPSReceptor receptor, final OPSVersion version, final boolean forceTerrainProperties) {
    if (forceTerrainProperties || (receptor.hasNoTerrainData() && opsData.getTerrainPropertiesMap(version) != null)) {
      final Optional<OPSTerrainProperties> properties = opsData.getTerrainPropertiesMap(version).getTerrainProperties(receptor);
      if (properties.isPresent()) {
        receptor.setTerrainProperties(properties.get());
      } else {
        receptor.setTerrainProperties(DEFAULT_TERRAIN_PROPERTIES);
      }
    }
  }

  private Map<Substance, T> runSubstances(final OPSInputData input, final Integer sourcesKey, final Collection<OPSSource> sources,
      final Collection<OPSReceptor> receptors, final List<Substance> supportedSubstances, final Map<Substance, EnumSet<EmissionResultKey>> erkMap,
      final OPSRunnerOptions runnerOptions) throws IOException, AeriusException {
    final boolean minDistance = input.minDistanceContains(sourcesKey);
    runner.init(input.getReceptors(), input.getEmissionResultKeys());
    final SubReceptorCalculation subReceptorCalculation = input.getSubReceptorCalculation();
    final Map<Substance, T> results = new EnumMap<>(Substance.class);

    for (final Substance substance : supportedSubstances) {
      final EnumSet<EmissionResultKey> erks = erkMap.get(substance);

      if (SubstanceUtil.hasEmissionForSubstance(sources, substance) && !receptors.isEmpty()) {
        final ControlFileOptions cfo = new ControlFileOptions(input, substance, minDistance, erks, input.getRoadsCats());
        final T singleSubstanceResults = run(subReceptorCalculation, sourcesKey, erks, sources, receptors, input.getReceptors(), runnerOptions, cfo,
            input.getSplitDistance());
        results.put(substance, singleSubstanceResults);
      } else {
        results.put(substance, runner.emptyResultsForSubstance(receptors, erks));
      }
    }
    return results;
  }

  private T run(final SubReceptorCalculation subReceptorCalculation, final Integer sourcesKey, final Set<EmissionResultKey> erks,
      final Collection<OPSSource> sources, final Collection<OPSReceptor> receptors, final Collection<OPSReceptor> orginalReceptors,
      final OPSRunnerOptions runnerOptions, final ControlFileOptions cfo, final double splitDistance) throws IOException, AeriusException {
    return switch (subReceptorCalculation) {
        case DISABLED -> singleRun(sourcesKey, sources, receptors, cfo, runnerOptions);
        case SPLIT_PROVIDED -> splitSubReceptorRun(sourcesKey, erks, sources, receptors, orginalReceptors, cfo, runnerOptions, splitDistance);
        case PROVIDED -> subReceptorRun(sourcesKey, sources, receptors, cfo, runnerOptions);
        default ->  subReceptorRun(sourcesKey, sources, receptors, cfo, runnerOptions);
    };
  }

  /**
   * Performs a run on the source and receptors. It will pass the results just as is. Therefore receptors should not be sub receptors.
   */
  protected T singleRun(final Integer sourcesKey, final Collection<OPSSource> sources, final Collection<OPSReceptor> receptors,
      final ControlFileOptions cfo, final OPSRunnerOptions runnerOptions) throws IOException, AeriusException {
    return runSubstance(sourcesKey, sources, receptors, cfo, runnerOptions);
  }

  /**
   * Performs a run and aggregates the sub receptor results to their original center point.
   */
  protected T subReceptorRun(final Integer sourcesKey, final Collection<OPSSource> sources, final Collection<OPSReceptor> receptors,
      final ControlFileOptions cfo, final OPSRunnerOptions runnerOptions) throws IOException, AeriusException {
    return runner.aggregate(singleRun(sourcesKey, sources, receptors, cfo, runnerOptions));
  }

  /**
   * Performs a split run. Depending on the split distance, presence of sub receptors this could mean either the calculation is split
   * or is a single run.
   */
  private T splitSubReceptorRun(final Integer sourcesKey, final Set<EmissionResultKey> erks, final Collection<OPSSource> sources,
      final Collection<OPSReceptor> receptors, final Collection<OPSReceptor> orginalReceptors, final ControlFileOptions cfo,
      final OPSRunnerOptions runnerOptions, final double splitDistance) throws IOException, AeriusException {
    final OPSSplitSubReceptorCalculation split = OPSSplitSubReceptorCalculation.split(sources, orginalReceptors, splitDistance);
    final Collection<OPSReceptor> withoutSubreceptors = split.getReceptorsWithOutSubreceptors();
    final T result;

    if (split.isAllWithinDistance()) {
      result = subReceptorRun(sourcesKey, sources, receptors, cfo, runnerOptions);
    } else if (split.isAllBeyondDistance() || withoutSubreceptors.size() == receptors.size()) {
      result = singleRun(sourcesKey, sources, withoutSubreceptors, cfo, runnerOptions);
    } else {
      result = runner.mergeResults(erks,
          singleRun(sourcesKey, split.getSourcesNoSubReceptors(), withoutSubreceptors, cfo, runnerOptions),
          subReceptorRun(sourcesKey, split.getSourcesWithSubReceptors(), receptors, cfo, runnerOptions));
    }
    return result;
  }

  private T runSubstance(final Integer sourcesKey, final Collection<OPSSource> sources, final Collection<OPSReceptor> receptors,
      final ControlFileOptions cfo, final OPSRunnerOptions runnerOptions) throws IOException, AeriusException {
    // outputFile is a random filename in a random directory. Each input file type creates a file by appending it's extension.
    final File runDirectory = FileUtil.createDirectory(configuration.getRunFilesDirectory(), dirPrefix(sourcesKey, cfo));
    final String runId = runDirectory.getName();

    inputValidatorCollector.writeErrorsToFile(sourcesKey, runDirectory, OSUtils.NL);
    // write files to run folder
    final String settingsFile = opsFileWriter.writeFiles(runId, runDirectory, sources, receptors, cfo);
    final List<String> parameters = runnerOptions.constructParameters(settingsFile, cfo.isMinDistance());
    final T result = runner.run(runId, runDirectory, parameters, cfo);

    if (!keepGeneratedFiles) {
      FileUtil.removeDirectoryRecursively(runDirectory.toPath());
    }
    return result;
  }

  private static String dirPrefix(final Integer sourcesKey, final ControlFileOptions cfo) {
    return sourcesKey + "-" + cfo.getSubstance().name().toLowerCase(Locale.ROOT);
  }
}
