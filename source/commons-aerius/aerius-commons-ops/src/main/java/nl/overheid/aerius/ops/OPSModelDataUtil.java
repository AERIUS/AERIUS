/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops;

import nl.overheid.aerius.http.HttpClientProxy;
import nl.overheid.aerius.ops.domain.OPSConfiguration;
import nl.overheid.aerius.ops.runner.OPSSingleSubstanceRunner;
import nl.overheid.aerius.ops.version.OPSVersion;
import nl.overheid.aerius.util.ModelDataUtil;
import nl.overheid.aerius.worker.ModelDataUtilPreloader;

/**
 * Preload OPS executable.
 */
class OPSModelDataUtil implements ModelDataUtilPreloader<OPSVersion, OPSConfiguration> {

  private static final String MODEL_DATA_TYPE = "ops";

  private final ModelDataUtil modelDataUtil;

  public OPSModelDataUtil(final HttpClientProxy httpClientProxy) {
    this.modelDataUtil = new ModelDataUtil(httpClientProxy);
  }

  @Override
  public synchronized void ensureModelDataPresence(final OPSConfiguration config) throws Exception {
    final OPSSingleSubstanceRunner runner = new OPSSingleSubstanceRunner(config);

    for (final OPSVersion opsVersion : config.getModelPreloadVersions()) {
      ensureModelDataAvailable(config, runner, opsVersion);
    }
  }

  public synchronized void ensureModelDataAvailable(final OPSConfiguration config, final OPSSingleSubstanceRunner runner, final OPSVersion opsVersion)
      throws Exception {
    modelDataUtil.ensureModelDataAvailable(config.getModelDataUrl(), MODEL_DATA_TYPE, opsVersion.getVersion(),
        config.getVersionsRoot().getAbsolutePath(), runner::isExecutable, () -> runner.versionCheck(opsVersion));
    modelDataUtil.ensureModelDataAvailable(config.getModelDataUrl(), MODEL_DATA_TYPE, opsVersion.getTerrainPropertiesVersion(),
        config.getVersionsRoot().getAbsolutePath(), path -> true, () -> {});
  }
}
