/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops;

import java.io.Serializable;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.calculation.EngineInputData.CommandType;
import nl.overheid.aerius.http.HttpClientProxy;
import nl.overheid.aerius.ops.background.OPSData;
import nl.overheid.aerius.ops.background.OPSDataBuilder;
import nl.overheid.aerius.ops.domain.OPSConfiguration;
import nl.overheid.aerius.ops.domain.OPSConfigurationBuilder;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.exception.OPSRunDirectoryExistsException;
import nl.overheid.aerius.ops.version.OPSVersion;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.util.HttpClientManager;
import nl.overheid.aerius.worker.ConfigurationBuilder;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.Worker;
import nl.overheid.aerius.worker.WorkerConnectionHelper;
import nl.overheid.aerius.worker.WorkerFactory;
import nl.overheid.aerius.worker.WorkerTimeLogger;

/**
 * Worker implementation for OPS workers. This worker can be used to run multiple OPS calls concurrently.
 */
public class OPSWorkerFactory implements WorkerFactory<OPSConfiguration> {

  private static final Logger LOG = LoggerFactory.getLogger(OPSWorkerFactory.class);

  private final OPSModelDataUtil modelDataUtil;

  public OPSWorkerFactory() {
    this(new HttpClientProxy(new HttpClientManager()));
  }

  public OPSWorkerFactory(final HttpClientProxy httpClientProxy) {
    modelDataUtil = new OPSModelDataUtil(httpClientProxy);
  }

  @Override
  public ConfigurationBuilder<OPSConfiguration> configurationBuilder(final Properties properties) {
    return new OPSConfigurationBuilder(properties, modelDataUtil);
  }

  @Override
  public Worker<OPSInputData, Serializable> createWorkerHandler(final OPSConfiguration configuration,
      final WorkerConnectionHelper workerConnectionHelper) throws Exception {
    final OPSData opsData = OPSDataBuilder.build(configuration);
    return new OPSWorker(configuration, workerConnectionHelper, opsData);
  }

  @Override
  public WorkerType getWorkerType() {
    return WorkerType.OPS;
  }

  private final class OPSWorker implements Worker<OPSInputData, Serializable> {
    private final OPSConfiguration configuration;
    private final OPSData opsData;
    private final OPSProcessRunner runOPS;
    private final OPSExportRunner exportOPS;

    private OPSWorker(final OPSConfiguration configuration, final WorkerConnectionHelper workerConnectionHelper, final OPSData opsData) {
      this.configuration = configuration;
      this.opsData = opsData;
      runOPS = new OPSProcessRunner(configuration, opsData);
      exportOPS = new OPSExportRunner(configuration, workerConnectionHelper.createFileServerProxy(configuration), opsData);
    }

    @Override
    public Serializable run(final OPSInputData input, final JobIdentifier jobIdentifier,
        final WorkerIntermediateResultSender workerIntermediateResultSender) throws Exception {
      final WorkerTimeLogger timeLogger = WorkerTimeLogger.start(WorkerType.OPS.name());

      try {
        final OPSVersion version = (OPSVersion) input.getEngineVersion();
        modelDataUtil.ensureModelDataAvailable(configuration, runOPS.getRunner(), version);
        OPSDataBuilder.loadAdditionalData(configuration, version, opsData);
        while (true) {
          try {
            return input.getCommandType() == CommandType.EXPORT ? exportOPS.run(input, jobIdentifier.getJobKey()) : runOPS.run(input);
          } catch (final OPSRunDirectoryExistsException e) {
            LOG.warn("Directory existed, trying again.");
          }
        }
      } finally {
        timeLogger.logDuration(jobIdentifier, input);
      }
    }

    @Override
    public boolean logStartStop() {
      return false;
    }
  }
}
