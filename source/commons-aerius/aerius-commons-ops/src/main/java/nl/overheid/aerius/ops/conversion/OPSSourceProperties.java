/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.conversion;

import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.SridPoint;

public interface OPSSourceProperties {

  default Object getX(final SridPoint p, final boolean epsg4326) {
    return p.getX();
  }

  default Object getY(final SridPoint p, final boolean epsg4326) {
    return p.getY();
  }

  default double getEmission(final OPSSource s, final Substance substance) {
    return s.getEmission(substance);
  }

  default double getHeatContent(final OPSSource s) {
    return s.getHeatContent();
  }

  default double getEmissionHeight(final OPSSource s) {
    return s.getEmissionHeight();
  }

  default int getDiameter(final OPSSource s) {
    return s.getDiameter();
  }

  default double getSpread(final OPSSource s) {
    return s.getSpread();
  }

  default double getOutflowDiameter(final OPSSource s) {
    return s.getOutflowDiameter();
  }

  default double getOutflowVelocity(final OPSSource s) {
    return s.getOutflowVelocity();
  }

  default double getEmissionTemperature(final OPSSource s) {
    return s.getEmissionTemperature();
  }

  default int getDiurnalVariation(final OPSSource s) {
    return s.getDiurnalVariation();
  }

  default int getCat(final OPSSource s) {
    return s.getCat();
  }

  default int getArea(final OPSSource s) {
    return s.getArea();
  }

  default int getParticleSizeDistribution(final OPSSource s) {
    return s.getParticleSizeDistribution();
  }

  default double getBuildingLength(final OPSSource s) {
    return s.getBuildingLength();
  }

  default double getBuildingWidth(final OPSSource s) {
    return s.getBuildingWidth();
  }

  default double getBuildingHeight(final OPSSource s) {
    return s.getBuildingHeight();
  }

  default double getBuildingOrientation(final OPSSource s) {
    return s.getBuildingOrientation();
  }

}
