/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.conversion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import nl.overheid.aerius.ops.domain.OPSCopyFactory;
import nl.overheid.aerius.ops.domain.OPSCopyFactory.ImportBuildingHandler;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.domain.SourceBuildingCombination;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.building.Building;
import nl.overheid.aerius.shared.domain.v2.building.BuildingFeature;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.ImaerExceptionReason;
import nl.overheid.aerius.shared.geo.EPSG;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Converter class to convert EmissionSource related objects to OPSSource related objects.
 */
public final class EmissionSourceOPSConverter {
  static final String OPS_ID_PREFIX = " (";
  static final String OPS_ID_POSTFIX = ")";

  /**
   * Copies the {@link EmissionSource} object to an {@link OPSSource} object for
   * the given {@link Substance} substance emission. It only looks at the
   * x, y coordinates and not at the geometry in {@link EmissionSource}. To
   * pass {@link EmissionSource} with lines and such, these must be converted
   * to point sources first to be used here.
   *
   * @param source {@link EmissionSource} object to convert to {@link OPSSource}
   * @param point the x-y location of the point
   * @param emissions the emissions of the point
   * @param substances substances to get the emission values for from the source
   * @param buildingTracker
   * @param featureCollection The feature collection the source belongs to.
   * @return new {@link OPSSource} object
   * @throws AeriusException
   */
  public static List<OPSSource> convert(final EmissionSource source, final Point point, final Map<Substance, Double> emissions, final int sourceId,
      final List<Substance> substances, final OPSBuildingTracker buildingTracker, final ScenarioSituation situation) throws AeriusException {
    final int srid = determineActualSourceSrid(situation);
    validateCoordinates(point.getX(), point.getY(), srid);
    final OPSSource ops = new OPSSource(sourceId, srid, point.getX(), point.getY());
    ops.setCat(source.getSectorId());

    OPSCopyFactory.toOpsSource(source.getCharacteristics(), ops, buildingTracker);
    for (final Substance substance : substances) {
      final double emission = emissions.getOrDefault(substance, 0.0);
      if (emission > 0) {
        ops.setEmission(substance, emission);
      }
    }
    final List<OPSSource> list = new ArrayList<>();
    if (ops.hasEmission()) {
      list.add(ops);
    }
    return list;
  }

  /**
   * OPS interprets an y-coordinate < 90 as a WGS84 coordinate. Since that would be invalid in case the EPSG is not WGS84 we throw an exception.
   * Some versions of OPS actually crash on this input, therefore make sure we abort here.
   *
   * @param x x-coordinate to check
   * @param y y-coordinate to check
   * @param srid srid of the coordinates
   * @throws AeriusException exception in case of unsupported geometry
   */
  private static void validateCoordinates(final double x, final double y, final int srid) throws AeriusException {
    if (srid != EPSG.WGS84.getSrid() && y < 90) {
      throw new AeriusException(ImaerExceptionReason.GEOMETRY_INVALID, "x: " + x + ", y:" + y);
    }
  }

  private static int determineActualSourceSrid(final ScenarioSituation situation) {
    return (situation != null && situation.getSources() != null && situation.getSources().getCrs() != null
        && EPSG.WGS84.getEpsgCode().equals(situation.getSources().getCrs().getProperties().getName())
            ? EPSG.WGS84
            : EPSG.RDNEW).getSrid();
  }

  /**
   * Copies the {@link OPSSource} object to an {@link EmissionSource} object with the
   * the given {@link substance} as the EmissionValue.
   * The list of sectors is used to set a sector based on the category number of the OPSsource.
   *
   * @param opsSource The OPS Source object.
   * @param substance The substance to use for the emission value.
   * @param categories The object to use for default values. For instance, the list of sectors is used to pick a sector for the source
   *  (based on category number of the opsSource).
   * @param useValidSectors if true use the sectors from the database, if false fake the sector by  setting the category as sector id.
   * @return The EmissionSource representation of the OPSSource.
   * @throws AeriusException In case converting failed.
   */
  public static SourceBuildingCombination convert(final OPSSource opsSource, final Substance substance, final SectorCategories categories,
      final boolean useValidSectors) throws AeriusException {
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    final SourceBuildingCombination sourceBuildingCombination = new SourceBuildingCombination(feature);
    //OPS sources are always point sources.
    final String id = String.valueOf(opsSource.getId());
    final Point point = new Point(opsSource.getPoint().getX(), opsSource.getPoint().getY());
    feature.setGeometry(point);
    feature.setId(id);

    final GenericEmissionSource source = new GenericEmissionSource();
    feature.setProperties(source);
    source.setGmlId(id);
    source.getEmissions().put(substance, opsSource.getEmission(substance));
    source.setCharacteristics(OPSCopyFactory.toSourceCharacteristics(opsSource, new ImportBuildingHandler() {

      @Override
      public String handle(final double length, final double width, final double height, final int orientation) throws AeriusException {
        final BuildingFeature buildingFeature = new BuildingFeature();
        final String buildingId = String.valueOf(opsSource.getId());
        buildingFeature.setId(buildingId);
        final Polygon polygon = GeometryUtil.constructPolygonFromDimensions(point, length, width, orientation);
        buildingFeature.setGeometry(polygon);
        final Building building = new Building();
        building.setGmlId(buildingId);
        building.setHeight(height);
        buildingFeature.setProperties(building);
        sourceBuildingCombination.setBuilding(buildingFeature);
        return buildingId;
      }
    }));
    // based on the category number, try to select a sector for this source.
    source.setSectorId(getSectorId(opsSource.getCat(), categories, useValidSectors));

    // As we don't use the OPSSource's ID in an EmissionSource, we retain its information by moving it to the source's label.
    // Loss of information is impossible - as it would violate the laws of quantum dynamics. Thus if we do not do this,
    //  we would effectively destroy the universe.
    source.setLabel(opsSource.getComp() + OPS_ID_PREFIX + opsSource.getId() + OPS_ID_POSTFIX);
    return sourceBuildingCombination;
  }

  private static int getSectorId(final int category, final SectorCategories categories, final boolean useValidSectors) {
    final Sector sector;
    if (useValidSectors) {
      sector = categories == null ? Sector.SECTOR_DEFAULT : categories.determineSectorById(category);
    } else {
      sector = new Sector(category, SectorGroup.OTHER, "");
    }
    return sector.getSectorId();
  }
}
