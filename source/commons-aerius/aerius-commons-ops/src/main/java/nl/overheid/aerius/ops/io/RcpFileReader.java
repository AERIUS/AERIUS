/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;

import nl.overheid.aerius.io.AbstractLineReader;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.ops.LandUse;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.ops.OPSTerrainProperties;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Reads an OPS receptor file (.rcp). The data is space separated.
 *
 * If the name column contains an integer that value will be used as the the id instead of the field in the id column.
 */
public class RcpFileReader extends AbstractLineReader<OPSReceptor> {

  private static final String NR_COLUMN_NAME = "Nr";
  private static final String NAME_COLUMN_NAME = "Name";
  private static final String X_COLUMN_NAME = "X-coor";
  private static final String Y_COLUMN_NAME = "Y-coor";
  private static final String HEIGHT_COLUMN_NAME = "Height";
  private static final String Z0_COLUMN_NAME = "AVG-z0";
  private static final String LANDUSE_COLUMN_NAME = "DOM-LU";
  private static final String[] LANDUSES_COLUMN_NAMES = new String[] {
      "LU1", "LU2", "LU3", "LU4", "LU5", "LU6", "LU7", "LU8", "LU9",
  };

  private static final int NR_INDEX = 0;
  private static final int NAME_INDEX = 1;
  private static final int X_INDEX = 2;
  private static final int Y_INDEX = 3;
  private static final int HEIGHT_INDEX = 4;
  private static final int Z0_INDEX = 4;
  private static final int LANDUSE_INDEX = 5;
  private static final int[] LANDUSES_INDEXES = new int[] {
      6, 7, 8, 9, 10, 11, 12, 13, 14,
  };

  private String[] headers;
  private String[] splitCurrentLine;
  private boolean readTerrainData;
  private boolean readHeight;

  @Override
  public LineReaderResult<OPSReceptor> readObjects(final InputStream inputStream)
      throws IOException {
    try (InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8)) {
      return super.readObjects(reader, 1);
    }
  }

  @Override
  protected String skipHeaderRow(final BufferedReader reader, final int headerRowCount) throws IOException, AeriusException {
    if (headerRowCount != 1) {
      // Unexpected implementation change.
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
    final String headerRowString = super.skipHeaderRow(reader, headerRowCount);
    headers = headerRowString.trim().split("\\s+");
    if (readHeight && (headers.length <= HEIGHT_INDEX || !HEIGHT_COLUMN_NAME.equalsIgnoreCase(headers[HEIGHT_INDEX]))) {
      throw new AeriusException(AeriusExceptionReason.CONNECT_MISSING_RECEPTOR_HEIGHT);
    }
    if (!readHeight && headers.length > HEIGHT_INDEX && HEIGHT_COLUMN_NAME.equalsIgnoreCase(headers[HEIGHT_INDEX])) {
      throw new AeriusException(AeriusExceptionReason.CONNECT_UNEXPECTED_RECEPTOR_HEIGHT);
    }
    return headerRowString;
  }

  @Override
  protected OPSReceptor parseLine(final String line, final List<AeriusException> warnings) {
    splitCurrentLine = line.trim().split("\\s+");

    final OPSReceptor receptor = new OPSReceptor(getNr(), getX(), getY());
    receptor.setLabel(getName());
    if (readHeight && splitCurrentLine.length >= HEIGHT_INDEX + 1) {
      receptor.setHeight(getHeight());
    }
    if (readTerrainData && splitCurrentLine.length >= LANDUSES_INDEXES[LANDUSES_INDEXES.length - 1] + terrainOffset() + 1) {
      final OPSTerrainProperties terrainProperties = new OPSTerrainProperties(getZ0(), getLandUse(), getLandUses());
      receptor.setTerrainProperties(terrainProperties);
    } else if (readTerrainData && splitCurrentLine.length >= LANDUSE_INDEX + terrainOffset() + 1) {
      final OPSTerrainProperties terrainProperties = new OPSTerrainProperties(getZ0(), getLandUse());
      receptor.setTerrainProperties(terrainProperties);
    }
    return receptor;
  }

  public void setReadTerrainData(final boolean readLandUseAndZ0) {
    this.readTerrainData = readLandUseAndZ0;
  }

  public void setReadHeight(final boolean readHeight) {
    this.readHeight = readHeight;
  }

  private int getNr() {
    final String name = getName();
    return NumberUtils.isDigits(name) ? Integer.parseInt(name) : getInt(NR_COLUMN_NAME, NR_INDEX);
  }

  private String getName() {
    return getString(NAME_COLUMN_NAME, NAME_INDEX);
  }

  private int getX() {
    return getInt(X_COLUMN_NAME, X_INDEX);
  }

  private int getY() {
    return getInt(Y_COLUMN_NAME, Y_INDEX);
  }

  private double getZ0() {
    return getDouble(Z0_COLUMN_NAME, Z0_INDEX + terrainOffset());
  }

  private LandUse getLandUse() {
    return LandUse.getByOption(getInt(LANDUSE_COLUMN_NAME, LANDUSE_INDEX + terrainOffset()));
  }

  private int[] getLandUses() {
    final int[] landUses = new int[LANDUSES_INDEXES.length];
    for (int i = 0; i < LANDUSES_INDEXES.length; i++) {
      landUses[i] = getInt(LANDUSES_COLUMN_NAMES[i], LANDUSES_INDEXES[i] + terrainOffset());
    }
    return landUses;
  }

  private double getHeight() {
    return getDouble(HEIGHT_COLUMN_NAME, HEIGHT_INDEX);
  }

  private int terrainOffset() {
    return readHeight ? 1 : 0;
  }

  @Override
  protected String processColumn(final int index) {
    return splitCurrentLine[index];
  }

  @Override
  protected String processColumn(final int index, final int size) {
    return processColumn(index);
  }
}
