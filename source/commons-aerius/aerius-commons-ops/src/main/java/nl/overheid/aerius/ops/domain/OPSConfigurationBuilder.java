/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.domain;

import java.util.Properties;

import nl.overheid.aerius.ops.version.OPSVersion;
import nl.overheid.aerius.worker.EngineWorkerConfigurationBuilder;
import nl.overheid.aerius.worker.FileServerConfigurationBuilder;
import nl.overheid.aerius.worker.ModelDataUtilPreloader;

/**
 * Builder class for {@link OPSConfiguration}.
 */
public class OPSConfigurationBuilder extends EngineWorkerConfigurationBuilder<OPSVersion, OPSConfiguration> {

  private static final String OPS_WORKER_PREFIX = "ops";

  private static final String OPS_SETTINGS_METEOFILE = "settings.meteofile";
  private static final String OPS_SETTINGS_LANDUSE = "settings.landuse";

  public OPSConfigurationBuilder(final Properties properties, final ModelDataUtilPreloader<OPSVersion, OPSConfiguration> preloader) {
    super(properties, OPS_WORKER_PREFIX, preloader, true);
    addBuilder(new FileServerConfigurationBuilder<>(properties));
  }

  @Override
  protected OPSConfiguration create() {
    return new OPSConfiguration(workerProperties.getProcesses());
  }

  @Override
  protected void buildEngine(final OPSConfiguration configuration) {
    configuration.setSettingsMeteoFile(getSettingsMeteoFile());
    configuration.setSettingsLandUse(getSettingsLandUse());
  }

  private String getSettingsMeteoFile() {
    return workerProperties.getProperty(OPS_SETTINGS_METEOFILE);
  }

  private String getSettingsLandUse() {
    return workerProperties.getProperty(OPS_SETTINGS_LANDUSE);
  }

  @Override
  protected OPSVersion getVersionByLabel(final String version) {
    return OPSVersion.getOPSVersionByLabel(version);
  }
}
