/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.subreceptor;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.io.ControlFileOptions;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Interface to different implementations on how sub receptors are to be calculated.
 * Given the results for a single substance calculation the specific implementation of this interface will compute sub receptor results.
 *
 * Sub receptors are point that are related to a receptor and the receptor result is in some way computed from the results
 * of the related sub receptors.
 */
public interface OPSSubReceptorCalculator {

  /**
   * Recalculates the results for sub receptors and rewrites the results if need to be updated from the sub receptors.
   *
   * @param sourcesKey source key to identify te specific sources
   * @param sources sources that can be used for recalculation if recalculation is necessary
   * @param singleSubstanceResults calculation results receptors
   * @param cfo additional control file options
   */
  void recalculate(Integer sourcesKey, Collection<OPSSource> sources, List<AeriusResultPoint> singleSubstanceResults,
      ControlFileOptions cfo) throws IOException, AeriusException;
}
