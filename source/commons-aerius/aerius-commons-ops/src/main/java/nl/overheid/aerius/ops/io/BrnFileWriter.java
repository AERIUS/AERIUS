/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Collection;
import java.util.Locale;

import nl.overheid.aerius.ops.conversion.AeriusOPSSourceProperties;
import nl.overheid.aerius.ops.conversion.OPSSourceProperties;
import nl.overheid.aerius.ops.conversion.RawOPSSourceProperties;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.runner.OPSRunner;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.OPSOptions;
import nl.overheid.aerius.shared.domain.geo.SridPoint;
import nl.overheid.aerius.shared.geo.EPSG;
import nl.overheid.aerius.util.OSUtils;

/**
 * Writes an OPS sources file.
 *
 * <dl>
 * <dt>snr</dt>
 * <dd>- source identification number (not essential)</dd>
 * <dt>x</dt>
 * <dd>- x-coordinate (m) (or decimal degrees longitude)</dd>
 * <dt>y</dt>
 * <dd>- y-coordinate (m) (or decimal degrees latitude)</dd>
 * <dt>q</dt>
 * <dd>- source strength (g/s)</dd>
 * <dt>hc</dt>
 * <dd>- heat content (MW)</dd>
 * <dt>h</dt>
 * <dd>- source height (m)</dd>
 * <dt>d</dt>
 * <dd>- source diameter (m)</dd>
 * <dt>s</dt>
 * <dd>- vertical spread of source height(s) (m)</dd>
 * <dt>D_stack</dt>
 * <dd>- inner diameter stack (m)</dd>
 * <dt>V_stack</dt>
 * <dd>- exit velocity (m/s)</dd>
 * <dt>Ts_stack</dt>
 * <dd>- temperature effluent gas (C)</dd>
 * <dt>dv</dt>
 * <dd>- code for diurnal variation of emission (-999 to 3)</dd>
 * <dt>cat</dt>
 * <dd>- source category number (0-9999) (for administrative purposes only)</dd>
 * <dt>area</dt>
 * <dd>- country or area number (0-9999) (for administrative purposes only)</dd>
 * <dt>ps</dt>
 * <dd>- particle-size distribution code (-999 to 3)</dd>
 * <dt>length(m)</dt>
 * <dd>- building length in meter (-9999 for not used otherwise range 10.0 to 105.0)</dd>
 * <dt>width(m)</dt>
 * <dd>- building width in meter (-9999 for not used otherwise range 1.5 to 87.15)</dd>
 * <dt>height(m)</dt>
 * <dd>- building height in meter (-9999 for not used otherwise range 3.0 to 8.0)</dd>
 * <dt>orientation(degrees</dt>
 * <dd>- building degrees (-9999 for not used otherwise range 0 to 179)</dd>
 * <dt>comp</dt>
 * <dd>- name of the substance (not essential)</dd>
 * </dl>
 */
public final class BrnFileWriter {

  /**
   * @see OPSRunner#createSourceInputFile original called source.brn
   */
  private static final String FILENAME = "emission.brn";

  private static final String HEADER_VERSION_4 =
      "! BRN-VERSION 4" + OSUtils.NL
      + "! snr    x(m)     y(m)     q(g/s)   hc(MW)   h(m)    r(m)   s(m)   D_stack(m) V_stack(m/s) Ts_stack(C) dv  cat area   ps length(m) width(m) height(m) orientation(degrees) component"
      + OSUtils.NL;

  //Format representation of the following fortran notation:
  // i4,<based on EPSG see below>,e10.3,f7.3,f6.1,i7.0,f6.1,4i4,2x,a12
  private static final String FORMAT_SOURCE =
      "%4d " // i4 snr
      + "%s " // 3.5f  x y lat/lot 1 meter precision
      + "%10.3E " // e10.3 q(g/s)
      + "%7.3f " // f7.3 hc(mw)
      + "%6.1f " // f6.1 h(m)
      + "%7d " // i7.0 d(m)
      + "%6.1f " // f6.1 s(m)
      + "%4.2f " // D_stack(m)
      + "%4.1f " // V_stack(m/s)
      + "%4.2f " // Ts_stack(C)
      + "%4d %4d %4d %4d " // 4i4 dv cat area sd
      + "%6.1f " // length(m)
      + "%6.1f " // width(m)
      + "%6.1f " // height(m)
      + "%6.1f " // orientation(degrees)
      + "%-12s" // a12 comp
      + OSUtils.NL;

  // Location format representation of the following fortran notation for RD New: 2i8.0
  private static final String FORMAT_LOCATION_RDNEW = "%8.0f %8.0f";
  // Location format representation of the following fortran notation for WGS84/EPSG:4326: 2f3.5. lat/lon 1 meter precision.
  private static final String FORMAT_LOCATION_EPSG_4326 = "%3.5f %3.5f";

  private BrnFileWriter() {
  }

  public static String getFileName() {
    return FILENAME;
  }

  /**
   * Writes OPS data for the given substance to a file with a default name: {@link #FILENAME}.
   * @param path path
   * @param data ops data
   * @param substance substance.
   * @throws IOException io exception.
   */
  public static void writeFile(final File path, final Collection<OPSSource> data, final Substance substance, final OPSOptions options)
      throws IOException {
    writeFile(path, FILENAME, data, substance, options);
  }

  /**
   * Writes OPS data for the given substance to a file.
   * @param path path
   * @param fileName name of the file
   * @param data ops data
   * @param substance substance
   * @throws IOException io exception
   */
  private static void writeFile(final File path, final String fileName, final Collection<OPSSource> data, final Substance substance,
      final OPSOptions options)
      throws IOException {

    OPSSourceProperties sourceProperties = new AeriusOPSSourceProperties();
    if (options != null && options.isRawInput()) {
      sourceProperties = new RawOPSSourceProperties();
    }

    try (final Writer writer = Files.newBufferedWriter(new File(path, fileName).toPath(), StandardCharsets.UTF_8)) {
      boolean writeHeader = true;

      for (final OPSSource d : data) {
        if (writeHeader) {
          writer.write(HEADER_VERSION_4);
          writeHeader = false;
        }

        // if emission is 0 don't add to file because ops doesn't allow sources with 0 emissions.
        if (d.getEmission(substance) > 0) {
          writeSingleSource(substance, writer, d, sourceProperties);
        }
      }
    }
  }

  private static void writeSingleSource(final Substance substance, final Writer writer, final OPSSource d, final OPSSourceProperties osa)
      throws IOException {
    //use 0 as default for SNR (or ID) instead of the source ID. It's not used in the result and has quite the limit.
    // Cast spread to a precision factor of 1, otherwise it's rounded down.
    final boolean epsg4326 = d.getPoint().getSrid() == EPSG.WGS84.getSrid();

    writer.write(String.format(Locale.US, FORMAT_SOURCE, 0,
        getLocation(osa, d.getPoint(), epsg4326),
        osa.getEmission(d, substance) / Substance.EMISSION_IN_G_PER_S_FACTOR,
        osa.getHeatContent(d),
        osa.getEmissionHeight(d),
        osa.getDiameter(d),
        osa.getSpread(d),
        osa.getOutflowDiameter(d),
        osa.getOutflowVelocity(d),
        osa.getEmissionTemperature(d),
        osa.getDiurnalVariation(d),
        osa.getCat(d),
        osa.getArea(d),
        osa.getParticleSizeDistribution(d),
        osa.getBuildingLength(d),
        osa.getBuildingWidth(d),
        osa.getBuildingHeight(d),
        Double.valueOf(osa.getBuildingOrientation(d)),
        ""));
  }

  private static String getLocation(final OPSSourceProperties osa, final SridPoint p, final boolean epsg4326) {
    final String formatLocation = epsg4326 ? FORMAT_LOCATION_EPSG_4326 : FORMAT_LOCATION_RDNEW;

    return String.format(Locale.US, formatLocation, osa.getX(p, epsg4326), osa.getY(p, epsg4326));
  }

}
