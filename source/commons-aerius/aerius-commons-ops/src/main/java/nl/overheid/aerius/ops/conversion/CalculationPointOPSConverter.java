/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.conversion;

import nl.overheid.aerius.shared.domain.ops.OPSCustomCalculationPoint;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CustomCalculationPoint;

/**
 * Converter class to convert CalculationPointFeature related objects to OPSReceptor related objects.
 */
public final class CalculationPointOPSConverter {

  /**
   * Copies the {@link CalculationPoint} object to an {@link OPSReceptor} object.
   *
   * @param calculationPoint {@link CalculationPoint} object to convert to {@link OPSReceptor}
   * @param point the x-y location of the point
   * @param calculationPointId the ID the calculation point should get
   * @return new {@link OPSReceptor} object
   */
  public static OPSReceptor convert(final CalculationPoint calculationPoint, final Point point) {
    final OPSReceptor ops = new OPSReceptor(calculationPoint.getId(), point.getX(), point.getY());

    ops.setLabel(calculationPoint.getLabel());
    if (calculationPoint instanceof OPSCustomCalculationPoint) {
      final OPSCustomCalculationPoint opsCustom = (OPSCustomCalculationPoint) calculationPoint;
      ops.setHeight(opsCustom.getHeight());
      ops.setTerrainProperties(opsCustom.getTerrainProperties());
    }
    return ops;
  }

  /**
   * Copies the {@link OPSReceptor} object to an {@link CalculationPointFeature} object.
   *
   * @param opsReceptor The OPS Receptor object.
   * @return The CalculationPointFeature representation of the OPSReceptor.
   */
  public static CalculationPointFeature convert(final OPSReceptor opsReceptor, final boolean asOpsCustomPoint) {
    final CalculationPointFeature feature = new CalculationPointFeature();
    //OPS sources are always point sources.
    feature.setGeometry(new Point(opsReceptor.getX(), opsReceptor.getY()));
    feature.setId(String.valueOf(opsReceptor.getId()));

    final CustomCalculationPoint properties = determineType(opsReceptor, asOpsCustomPoint);
    feature.setProperties(properties);
    properties.setCustomPointId(opsReceptor.getId());
    properties.setHeight(opsReceptor.getHeight());
    properties.setLabel(opsReceptor.getLabel());
    return feature;
  }

  private static CustomCalculationPoint determineType(final OPSReceptor opsReceptor, final boolean asOpsCustomPoint) {
    final CustomCalculationPoint properties;
    if (asOpsCustomPoint) {
      final OPSCustomCalculationPoint opsProperties = new OPSCustomCalculationPoint();
      opsProperties.setTerrainProperties(opsReceptor.getTerrainProperties());
      properties = opsProperties;
    } else {
      properties = new CustomCalculationPoint();
    }
    return properties;
  }
}
