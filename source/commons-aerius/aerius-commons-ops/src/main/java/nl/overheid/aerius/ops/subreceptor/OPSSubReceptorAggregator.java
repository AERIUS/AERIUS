/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.subreceptor;

import java.util.Collection;
import java.util.Set;

import nl.overheid.aerius.receptor.AbstractSubReceptorAggregator;
import nl.overheid.aerius.shared.domain.geo.IsSubPoint;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * OPS specific implementation of {@link AbstractSubReceptorAggregator}
 */
public class OPSSubReceptorAggregator extends AbstractSubReceptorAggregator<AeriusResultPoint, OPSReceptor> {

  /**
   * Initializes the aggregator with the receptors.
   * It filters out all {@link IsSubPoint} points as those points are only relevant for subreceptors.
   * When running OPS with maxDistance results that are beyond the max distance are set to 0 by OPS.and therefore should be ignored.
   * For maxDistance excludeZeroResults should be true.
   *
   * @param receptors all receptors
   * @param erks emission result keys for which results are requested.
   * @param excludeZeroResult Whether or not to exclude receptors where all results are exactly equal to 0
   */
  public OPSSubReceptorAggregator(final Collection<OPSReceptor> receptors, final Set<EmissionResultKey> erks, final boolean excludeZeroResult) {
    super(receptors, erks, excludeZeroResult);
  }

  @Override
  protected Collection<AeriusResultPoint> combine(final Collection<AeriusResultPoint> receptorResults,
      final Collection<AeriusResultPoint> subReceptorResults) {
    return receptorResults;
  }

}
