/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.geo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.Test;

/**
 * Test class for {@link AreaTypeTest}
 */
class AreaTypeTest {

  @Test
  void testToDatabaseObjectName() {
    assertNull(AreaType.HABITAT_TYPE.toDatabaseObjectName(), "Test HABITAT_TYPE");
    assertNull(AreaType.HABITAT_TYPE.toDatabasePluralObjectName(), "Test HABITAT_TYPE plural");
    assertEquals("province_area", AreaType.PROVINCE_AREA.toDatabaseObjectName(), "Test PROVINCE_AREA");
    assertEquals("province_areas", AreaType.PROVINCE_AREA.toDatabasePluralObjectName(), "Test PROVINCE_AREA plural");
  }

  @Test
  void testFromDatabaseObjectName() {
    assertNull(AreaType.fromDatabaseObjectName(null), "Test null");
    assertNull(AreaType.fromDatabaseObjectName("illegal"), "Test illegal input");
    assertSame(AreaType.HABITAT_TYPE, AreaType.fromDatabaseObjectName("habitat_type"), "Test HABITAT_TYPE");
    assertSame(AreaType.HABITAT_TYPE, AreaType.fromDatabaseObjectName("habitat_types"), "Test HABITAT_TYPE plural");
  }
}
