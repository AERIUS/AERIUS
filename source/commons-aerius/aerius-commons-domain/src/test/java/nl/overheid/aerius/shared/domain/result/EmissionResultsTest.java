/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.result;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Test for {@link EmissionResults}.
 */
class EmissionResultsTest {

  @Test
  void testEmissionResults() {
    final EmissionResults emissionResults = new EmissionResults();
    final EmissionResultKey noxKey = EmissionResultKey.NOX_DEPOSITION;
    final EmissionResultKey nh3Key = EmissionResultKey.NH3_DEPOSITION;
    final EmissionResultKey comboKey = EmissionResultKey.NOXNH3_DEPOSITION;

    Assertions.assertTrue(emissionResults.isEmpty(), "Emission results are empty.");
    Assertions.assertEquals(0.0, emissionResults.get(noxKey), 0.0001, "Deposition NOx equals 0.");
    Assertions.assertEquals(0.0, emissionResults.get(nh3Key), 0.0001, "Deposition NH3 equals 0.");
    Assertions.assertEquals(0.0, emissionResults.get(comboKey), 0.0001, "Deposition NH3+NOx equals 0.");
    Assertions.assertFalse(emissionResults.hasResult(noxKey), "result NOx");
    Assertions.assertFalse(emissionResults.hasResult(nh3Key), "result NH3");
    Assertions.assertFalse(emissionResults.hasResult(comboKey), "result combo");

    emissionResults.put(noxKey, 20.5);
    Assertions.assertFalse(emissionResults.isEmpty(), "Emission results are not empty.");
    Assertions.assertTrue(emissionResults.hasResult(noxKey), "result NOx");
    Assertions.assertFalse(emissionResults.hasResult(nh3Key), "result NH3");
    Assertions.assertTrue(emissionResults.hasResult(comboKey), "result combo");
    emissionResults.put(nh3Key, 10.5);
    Assertions.assertFalse(emissionResults.isEmpty(), "Emission results are not empty.");
    Assertions.assertEquals(20.5, emissionResults.get(noxKey), 0.0001, "Deposition NOx equals 20.5.");
    Assertions.assertEquals(10.5, emissionResults.get(nh3Key), 0.0001, "Deposition NH3 equals 10.5.");
    Assertions.assertEquals(31.0, emissionResults.get(comboKey), 0.0001, "Deposition NH3+NOx equals 31.");
    Assertions.assertTrue(emissionResults.hasResult(noxKey), "result NOx");
    Assertions.assertTrue(emissionResults.hasResult(nh3Key), "result NH3");
    Assertions.assertTrue(emissionResults.hasResult(comboKey), "result combo");

    // Test reset
    emissionResults.put(noxKey, 0.0);
    emissionResults.put(nh3Key, 0.0);
    Assertions.assertEquals(0.0, emissionResults.get(noxKey), 0.0001, "Deposition NOx equals 0.");
    Assertions.assertEquals(0.0, emissionResults.get(nh3Key), 0.0001, "Deposition NH3 equals 0.");
    Assertions.assertEquals(0.0, emissionResults.get(comboKey), 0.0001, "Deposition NH3+NOx equals 0.");

    // Test explicit sum post-reset
    emissionResults.put(comboKey, 5.5);
    Assertions.assertFalse(emissionResults.isEmpty(), "Emission results are not empty.");
    Assertions.assertEquals(0.0, emissionResults.get(noxKey), 0.0001, "Deposition NOx equals 0.");
    Assertions.assertEquals(0.0, emissionResults.get(nh3Key), 0.0001, "Deposition NH3 equals 0.");
    Assertions.assertEquals(5.5, emissionResults.get(comboKey), 0.0001, "Deposition NH3+NOx equals 5.5.");
    Assertions.assertTrue(emissionResults.hasResult(noxKey), "result NOx");
    Assertions.assertTrue(emissionResults.hasResult(nh3Key), "result NH3");
    Assertions.assertTrue(emissionResults.hasResult(comboKey), "result combo");
  }

  @Test
  void testResultAddition() {
    final EmissionResults emissionResults = new EmissionResults();
    final EmissionResultKey noxKey = EmissionResultKey.NOX_DEPOSITION;
    final EmissionResultKey nh3Key = EmissionResultKey.NH3_DEPOSITION;
    final EmissionResultKey comboKey = EmissionResultKey.NOXNH3_DEPOSITION;

    emissionResults.put(noxKey, 5.0);
    emissionResults.put(nh3Key, 5.0);

    Assertions.assertEquals(5.0, emissionResults.get(noxKey), 0.0001, "Deposition NOx equals 5.");
    Assertions.assertEquals(5.0, emissionResults.get(nh3Key), 0.0001, "Deposition NH3 equals 5.");
    Assertions.assertEquals(10.0, emissionResults.get(comboKey), 0.0001, "Deposition NH3+NOx equals 10.");
    Assertions.assertTrue(emissionResults.hasResult(noxKey), "result NOx");
    Assertions.assertTrue(emissionResults.hasResult(nh3Key), "result NH3");
    Assertions.assertTrue(emissionResults.hasResult(comboKey), "result combo");

    emissionResults.put(comboKey, 5.0);

    // Must be 5, not 10
    Assertions.assertEquals(5.0, emissionResults.get(comboKey), 0.0001, "Deposition NH3+NOx equals 5");
    Assertions.assertTrue(emissionResults.hasResult(noxKey), "result NOx");
    Assertions.assertTrue(emissionResults.hasResult(nh3Key), "result NH3");
    Assertions.assertTrue(emissionResults.hasResult(comboKey), "result combo");
  }
}
