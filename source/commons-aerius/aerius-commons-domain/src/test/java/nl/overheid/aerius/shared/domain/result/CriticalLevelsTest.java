/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.result;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.info.CriticalLevels;

/**
 * Test for {@link CriticalLevels}.
 */
class CriticalLevelsTest {

  @Test
  void testCriticalLevels() {
    final CriticalLevels criticalLevels = new CriticalLevels();
    final EmissionResultKey noxKey = EmissionResultKey.NOX_DEPOSITION;
    final EmissionResultKey nh3Key = EmissionResultKey.NH3_DEPOSITION;
    final EmissionResultKey comboKey = EmissionResultKey.NOXNH3_DEPOSITION;

    assertTrue(criticalLevels.isEmpty(), "Emission results are empty.");
    assertEquals(0.0, criticalLevels.get(noxKey), 0.0001, "Deposition NOx equals 0.");
    assertEquals(0.0, criticalLevels.get(nh3Key), 0.0001, "Deposition NH3 equals 0.");
    assertEquals(0.0, criticalLevels.get(comboKey), 0.0001, "Deposition NH3+NOx equals 0.");
    assertFalse(criticalLevels.hasResult(noxKey), "result NOx");
    assertFalse(criticalLevels.hasResult(nh3Key), "result NH3");
    assertFalse(criticalLevels.hasResult(comboKey), "result combo");

    criticalLevels.put(noxKey, 20.5);
    assertFalse(criticalLevels.isEmpty(), "Emission results are not empty.");
    assertTrue(criticalLevels.hasResult(noxKey), "result NOx");
    assertFalse(criticalLevels.hasResult(nh3Key), "result NH3");
    assertTrue(criticalLevels.hasResult(comboKey), "result combo");
    criticalLevels.put(nh3Key, 10.5);
    assertFalse(criticalLevels.isEmpty(), "Emission results are not empty.");
    assertEquals(20.5, criticalLevels.get(noxKey), 0.0001, "Deposition NOx equals 20.5.");
    assertEquals(10.5, criticalLevels.get(nh3Key), 0.0001, "Deposition NH3 equals 10.5.");
    assertEquals(31.0, criticalLevels.get(comboKey), 0.0001, "Deposition NH3+NOx equals 31.");
    assertTrue(criticalLevels.hasResult(noxKey), "result NOx");
    assertTrue(criticalLevels.hasResult(nh3Key), "result NH3");
    assertTrue(criticalLevels.hasResult(comboKey), "result combo");

    // Test reset
    criticalLevels.put(noxKey, 0.0);
    criticalLevels.put(nh3Key, 0.0);
    assertEquals(0.0, criticalLevels.get(noxKey), 0.0001, "Deposition NOx equals 0.");
    assertEquals(0.0, criticalLevels.get(nh3Key), 0.0001, "Deposition NH3 equals 0.");
    assertEquals(0.0, criticalLevels.get(comboKey), 0.0001, "Deposition NH3+NOx equals 0.");

    // Test explicit sum post-reset
    criticalLevels.putUnhatched(comboKey, 5.5);
    assertFalse(criticalLevels.isEmpty(), "Emission results are not empty.");
    assertEquals(0.0, criticalLevels.get(noxKey), 0.0001, "Deposition NOx equals 0.");
    assertEquals(0.0, criticalLevels.get(nh3Key), 0.0001, "Deposition NH3 equals 0.");
    assertEquals(5.5, criticalLevels.get(comboKey), 0.0001, "Deposition NH3+NOx equals 5.5.");
    assertTrue(criticalLevels.hasResult(noxKey), "result NOx");
    assertTrue(criticalLevels.hasResult(nh3Key), "result NH3");
    assertTrue(criticalLevels.hasResult(comboKey), "result combo");
  }

  @Test
  void testResultHatching() {
    final CriticalLevels criticalLevels = new CriticalLevels();
    final EmissionResultKey noxKey = EmissionResultKey.NOX_DEPOSITION;
    final EmissionResultKey nh3Key = EmissionResultKey.NH3_DEPOSITION;
    final EmissionResultKey comboKey = EmissionResultKey.NOXNH3_DEPOSITION;

    criticalLevels.put(comboKey, 5.0);

    assertEquals(5, criticalLevels.get(noxKey), 0.0001, "Concentration NOx equals 5.");
    assertEquals(5, criticalLevels.get(nh3Key), 0.0001, "Concentration NH3 equals 5.");
  }

  @Test
  void testResultAddition() {
    final CriticalLevels criticalLevels = new CriticalLevels();
    final EmissionResultKey noxKey = EmissionResultKey.NOX_DEPOSITION;
    final EmissionResultKey nh3Key = EmissionResultKey.NH3_DEPOSITION;
    final EmissionResultKey comboKey = EmissionResultKey.NOXNH3_DEPOSITION;

    criticalLevels.put(noxKey, 5.0);
    criticalLevels.put(nh3Key, 5.0);

    assertEquals(5.0, criticalLevels.get(noxKey), 0.0001, "Deposition NOx equals 5.");
    assertEquals(5.0, criticalLevels.get(nh3Key), 0.0001, "Deposition NH3 equals 5.");
    assertEquals(10.0, criticalLevels.get(comboKey), 0.0001, "Deposition NH3+NOx equals 10.");
    assertTrue(criticalLevels.hasResult(noxKey), "result NOx");
    assertTrue(criticalLevels.hasResult(nh3Key), "result NH3");
    assertTrue(criticalLevels.hasResult(comboKey), "result combo");

    criticalLevels.put(comboKey, 5.0);

    // Must be 5, not 10
    assertEquals(5.0, criticalLevels.get(comboKey), 0.0001, "Deposition NH3+NOx equals 5");
    assertTrue(criticalLevels.hasResult(noxKey), "result NOx");
    assertTrue(criticalLevels.hasResult(nh3Key), "result NH3");
    assertTrue(criticalLevels.hasResult(comboKey), "result combo");
  }
}
