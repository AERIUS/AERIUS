/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.summary;

import java.util.List;
import java.util.Map;

import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Class containing the decision framework results.
 */
public class DecisionFrameworkResults {

  // Overall decision making threshold (DMT) results
  private final Map<EmissionResultKey, ThresholdResult> decisionMakingThresholdResults;

  // The development pressure classification
  private final String developmentPressureClassification;

  // Site relevant threshold (SRT) results
  private final List<AssessmentAreaThresholdResults> assessmentAreaThresholdResults;

  public DecisionFrameworkResults(final Map<EmissionResultKey, ThresholdResult> decisionMakingThresholdResults,
      final String developmentPressureClassification, final List<AssessmentAreaThresholdResults> assessmentAreaThresholdResults) {
    this.decisionMakingThresholdResults = decisionMakingThresholdResults;
    this.developmentPressureClassification = developmentPressureClassification;
    this.assessmentAreaThresholdResults = assessmentAreaThresholdResults;
  }

  public Map<EmissionResultKey, ThresholdResult> getDecisionMakingThresholdResults() {
    return decisionMakingThresholdResults;
  }

  public String getDevelopmentPressureClassification() {
    return developmentPressureClassification;
  }

  public List<AssessmentAreaThresholdResults> getAssessmentAreaThresholdResults() {
    return assessmentAreaThresholdResults;
  }

}
