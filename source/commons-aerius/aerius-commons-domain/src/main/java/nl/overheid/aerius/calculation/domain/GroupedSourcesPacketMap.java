/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.overheid.aerius.shared.domain.EngineDataKey;
import nl.overheid.aerius.shared.domain.EngineSource;

/**
 * Map to store collections of sources by the unique group key.
 */
public class GroupedSourcesPacketMap {

  private final Map<GroupedSourcesKey, Collection<EngineSource>> map = new HashMap<>();

  /**
   * Adds all sources grouped by the group key and calculation engine.
   *
   * @param groupKey key to identify this group of sources
   * @param engineDataKey engine data key identifier
   * @param sources sources to add
   */
  public boolean addAll(final Integer groupKey, final EngineDataKey engineDataKey, final Collection<EngineSource> sources) {
    final GroupedSourcesKey key = new GroupedSourcesKey(engineDataKey, groupKey);

    return map.computeIfAbsent(key, k -> new ArrayList<>()).addAll(sources);
  }

  /**
   * Adds all sources grouped by the group key and calculation engine if there are already sources present.
   *
   * @param groupKey key to identify this group of sources
   * @param engine key to identify chunk
   * @param sources sources to add
   */
  public void addAllIfNotEmpty(final Integer groupKey, final EngineDataKey engineDataKey, final Collection<EngineSource> sources) {
    final GroupedSourcesKey key = new GroupedSourcesKey(engineDataKey, groupKey);

    if (map.containsKey(key)) {
      map.get(key).addAll(sources);
    }
  }

  public Map<GroupedSourcesKey, Collection<EngineSource>> getMap() {
    return map;
  }

  public List<GroupedSourcesPacket> toGroupedSourcesPacket() {
    return map.entrySet().stream().map(e -> new GroupedSourcesPacket(e.getKey(), e.getValue())).toList();
  }
}
