/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import nl.overheid.aerius.shared.FileServerExpireTag;
import nl.overheid.aerius.shared.domain.EngineDataKey;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.IsSubPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Base class for Calculation Engine input data.
 */
public class EngineInputData<T extends EngineSource, R extends AeriusPoint> implements Serializable {

  public enum CommandType {
    /**
     * Perform a pollution calculation given the input.
     */
    CALCULATE,
    /**
     * Generate the input files for the pollution tool.
     */
    EXPORT,
  }

  public enum SubReceptorCalculation {
    /**
     * Sub receptors are not calculated.
     */
    DISABLED,
    /**
     * Sub receptors are provided by the Chunker.
     */
    PROVIDED,
    /**
     * Split subreceptors based on source distance into sources close by that should do subreceptor calculation,
     * and sources farther away that only do receptor points.
     */
    SPLIT_PROVIDED,
  }

  public record ChunkStats(String chunkId, int nrOfPointsToCalculate) implements Serializable {}

  private static final long serialVersionUID = 8L;

  private final Theme theme;
  private final EngineVersion engineVersion;
  private final EngineDataKey engineDataKey;
  private final CommandType commandType;
  private final Map<Integer, Collection<T>> emissionSources = new HashMap<>();
  private SubReceptorCalculation subReceptorCalculation = SubReceptorCalculation.PROVIDED;
  private ChunkStats chunkStats;

  /**
   * Distance to use to split sources into 2 groups when sub receptor calculation is {@link SubReceptorCalculation#SPLIT_PROVIDED}.
   */
  private double splitDistance;
  private int year;
  private Collection<R> receptors = new ArrayList<>();
  private List<Substance> substances;
  private EnumSet<EmissionResultKey> emissionResultKeys;
  private FileServerExpireTag expire;

  protected EngineInputData(final Theme theme, final EngineDataKey engineDataKey, final EngineVersion engineVersion, final CommandType commandType) {
    this.theme = theme;
    this.engineDataKey = engineDataKey;
    this.engineVersion = engineVersion;
    this.commandType = commandType;
  }

  public Theme getTheme() {
    return theme;
  }

  public EngineDataKey getEngineDataKey() {
    return engineDataKey;
  }

  public EngineVersion getEngineVersion() {
    return engineVersion;
  }

  public CommandType getCommandType() {
    return commandType;
  }

  public ChunkStats getChunkStats() {
    return chunkStats;
  }

  public void setChunkStats(final ChunkStats chunkStats) {
    this.chunkStats = chunkStats;
  }

  public int getYear() {
    return year;
  }

  public void setYear(final int year) {
    this.year = year;
  }

  public void setSubReceptorCalculation(final SubReceptorCalculation subReceptorCalculation) {
    this.subReceptorCalculation = subReceptorCalculation;
  }

  public SubReceptorCalculation getSubReceptorCalculation() {
    return subReceptorCalculation;
  }

  public double getSplitDistance() {
    return splitDistance;
  }

  public void setSplitDistance(final double splitDistance) {
    this.splitDistance = splitDistance;
  }

  public FileServerExpireTag getExpire() {
    return expire;
  }

  public void setExpire(final FileServerExpireTag expire) {
    this.expire = expire;
  }

  public int getSourcesSize() {
    int size = 0;
    for (final Entry<Integer, Collection<T>> entry : emissionSources.entrySet()) {
      size += entry.getValue().size();
    }
    return size;
  }

  public Map<Integer, Collection<T>> getEmissionSources() {
    return emissionSources;
  }

  public void setEmissionSources(final Integer sourcesKey, final Collection<T> sectorEmissionSources) {
    this.emissionSources.put(sourcesKey, sectorEmissionSources);
  }

  @NotNull
  @Size(min = 1)
  @Valid
  public Collection<R> getReceptors() {
    return receptors;
  }

  /**
   * @return Returns the points that should be passed to the model. This means sub receptors with point type receptor are filtered out,
   * because these are already included as subreceptors (they only have a different id).
   */
  public Collection<R> getCalculateReceptors() {
    return receptors.stream()
        .filter(p -> !(p instanceof final IsSubPoint sp && !sp.getPointType().isSubReceptor()))
        .toList();
  }

  public void setReceptors(final Collection<R> receptors) {
    this.receptors = receptors;
  }

  public List<Substance> getSubstances() {
    return substances;
  }

  public void setSubstances(final List<Substance> substances) {
    this.substances = substances;
  }

  public EnumSet<EmissionResultKey> getEmissionResultKeys() {
    return emissionResultKeys;
  }

  public void setEmissionResultKeys(final EnumSet<EmissionResultKey> emissionResultKeys) {
    this.emissionResultKeys = emissionResultKeys;
  }

  @Override
  public String toString() {
    return "#receptors=" + receptors + ",#emissionSources=" + emissionSources.size() + ", year=" + year + ", substances=" + substances
        + ", emissionResultKeys=" + emissionResultKeys + ", subReceptorCalculation:" + subReceptorCalculation +
        ", splitDistance:"  + splitDistance + ", expiresHours=" + expire;
  }
}
