/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.archive;

import java.util.List;
import java.util.Map;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;

public class InCombinationProcessResponse {
  private List<ArchiveProject> projects;
  private List<ArchiveResult> results;

  @SuppressWarnings("unused")
  private InCombinationProcessResponse() {
    // Empty constructor for Json serialization.
  }

  public InCombinationProcessResponse(final List<ArchiveProject> projects, final List<ArchiveResult> results) {
    this.projects = projects;
    this.results = results;
  }

  public List<ArchiveProject> getProjects() {
    return projects;
  }

  public List<ArchiveResult> getResults() {
    return results;
  }

  public static class ArchiveProject {
    private String id;
    private String name;
    private String type;
    private String aeriusVersion;
    private String projectStatus;
    private String permitReference;
    private String planningReference;
    private double centroidX;
    private double centroidY;
    private Map<String, Double> netEmissions;

    @SuppressWarnings("unused")
    private ArchiveProject() {
      // Empty constructor for Json serialization.
    }

    public ArchiveProject(final String id, final String name, final String type, final String aeriusVersion, final String projectStatus, final String permitReference, final String planningReference,
        final double centroidX, final double centroidY, final Map<String, Double> netEmissions) {
      this.id = id;
      this.name = name;
      this.type = type;
      this.aeriusVersion = aeriusVersion;
      this.projectStatus = projectStatus;
      this.permitReference = permitReference;
      this.planningReference = planningReference;
      this.centroidX = centroidX;
      this.centroidY = centroidY;
      this.netEmissions = netEmissions;
    }

    public String getId() {
      return id;
    }

    public String getName() {
      return name;
    }

    public String getType() {
      return type;
    }

    public String getAeriusVersion() {
      return aeriusVersion;
    }

    public String getProjectStatus() {
      return projectStatus;
    }

    public String getPermitReference() {
      return permitReference;
    }

    public String getPlanningReference() {
      return planningReference;
    }

    public double getCentroidX() {
      return centroidX;
    }

    public double getCentroidY() {
      return centroidY;
    }

    public Map<String, Double> getNetEmissions() {
      return netEmissions;
    }
  }

  public static class ArchiveResult {
    private int receptorId;
    private int subPointId;
    private int level;
    private List<ArchiveResultValue> results;

    @SuppressWarnings("unused")
    private ArchiveResult() {
      // Empty constructor for Json serialization.
    }

    public ArchiveResult(final int receptorId, final int subPointId, final int level, final List<ArchiveResultValue> results) {
      this.receptorId = receptorId;
      this.subPointId = subPointId;
      this.level = level;
      this.results = results;
    }

    public int getReceptorId() {
      return receptorId;
    }

    public int getSubPointId() {
      return subPointId;
    }

    public int getLevel() {
      return level;
    }

    public List<ArchiveResultValue> getResults() {
      return results;
    }
  }

  public static class ArchiveResultValue {
    private EmissionResultType type;
    private Substance substance;
    private double value;

    @SuppressWarnings("unused")
    private ArchiveResultValue() {
      // Empty constructor for Json serialization.
    }

    public ArchiveResultValue(final EmissionResultType type, final Substance substance, final double value) {
      this.type = type;
      this.substance = substance;
      this.value = value;
    }

    public EmissionResultType getType() {
      return type;
    }

    public Substance getSubstance() {
      return substance;
    }

    public double getValue() {
      return value;
    }
  }
}
