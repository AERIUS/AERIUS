/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.emissions;

import nl.overheid.aerius.shared.domain.v2.source.shipping.maritime.ShippingMovementType;

public class MaritimeRouteKey {
  private final ShippingMovementType movementType;
  private final String shipCode;

  public MaritimeRouteKey(final ShippingMovementType movementType, final String shipCode) {
    this.movementType = movementType;
    this.shipCode = shipCode;
  }

  public ShippingMovementType getMovementType() {
    return movementType;
  }

  public String getShipCode() {
    return shipCode;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((movementType == null) ? 0 : movementType.hashCode());
    result = prime * result + ((shipCode == null) ? 0 : shipCode.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final MaritimeRouteKey other = (MaritimeRouteKey) obj;
    return movementType == other.movementType
        && ((shipCode == null && other.shipCode == null) || (shipCode != null && shipCode.equals(other.shipCode)));
  }
}
