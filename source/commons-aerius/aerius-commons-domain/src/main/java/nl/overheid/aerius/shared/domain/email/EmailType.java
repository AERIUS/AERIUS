/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.email;

/**
 * Enum to identify the type of email that is being sent.
 */
public enum EmailType {
  /**
   * Request a Connect api key.
   */
  CONNECT_API_KEY(false),
  /**
   * Download a CSV file.
   */
  CSV_DOWNLOAD,
  /**
   * Download model input files.
   */
  ENGINE_INPUT,
  /**
   * An error occurred.
   */
  ERROR,
  /**
   * Download of a GML file.
   */
  GML_DOWNLOAD,
  /**
   * Download of a PDF file.
   */
  PDF_DOWNLOAD;

  /**
   * True if the email is related to a calculation job.
   */
  private final boolean job;

  private EmailType(final boolean job) {
    this.job = job;
  }

  private EmailType() {
    job = true;
  }

  /**
   * @return True if the email is related to a calculation job.
   */
  public boolean isJob() {
    return job;
  }
}
