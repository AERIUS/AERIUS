/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.email;

import java.io.IOException;
import java.io.Serializable;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;

/**
 * Data object to store a large String with a filename and mime type.
 */
public class MailAttachment implements Serializable {

  private static final long serialVersionUID = 1L;

  private byte[] dataByte;
  private String fileName;
  private String mimeType;

  /**
   * @param data The data for this datasource.
   * @param fileName The name the file should have.
   * @param mimeType The mimetype of the file.
   */
  public MailAttachment(final String data, final String fileName, final String mimeType) {
    try {
      this.dataByte = data.getBytes(StandardCharsets.UTF_8.name());
    } catch (final IOException e) {
      throw new UncheckedIOException(e);
    }
    this.fileName = fileName;
    this.mimeType = mimeType;
  }

  /**
   * @param dataByte The data as Byte for this datasource.
   * @param fileName The name the file should have.
   * @param mimeType The mimetype of the file.
   */
  public MailAttachment(final byte[] dataByte, final String fileName, final String mimeType) {
    this.dataByte = dataByte;
    this.fileName = fileName;
    this.mimeType = mimeType;
  }

  /**
   * @return The data as bytes.
   */
  public byte[] getBytes() {
    return dataByte;
  }

  public String getFileName() {
    return fileName;
  }

  public String getMimeType() {
    return mimeType;
  }

  /**
   * @param data The string that will be set as data. It will be encoded using UTF-8.
   * @throws IOException In case the string could not be encoded using UTF-8.
   */
  public void setData(final String data) throws IOException {
    this.dataByte = data.getBytes(StandardCharsets.UTF_8.name());
  }

  public void setFileName(final String fileName) {
    this.fileName = fileName;
  }

  public void setMimeType(final String mimeType) {
    this.mimeType = mimeType;
  }
}
