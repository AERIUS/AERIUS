/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.theme.own2000;

import java.util.List;

/**
 * OwN2000 constants related to sub receptor creation.
 */
public final class OwN2000SubReceptorConstants {

  /**
   * The farthest distance of sources to receptors for which sub receptors should be created.
   */
  public static final double SUB_RECEPTOR_FARTHEST_DISTANCE = 300;
  /**
   * Distances to use to determine if subreceptor calculation should be used.
   * Order of the list is from highest distance to lowest.
   * If a source-receptor distance is shorter than the value in the list that index is used.
   *
   * Position in the list determines the sub level index, starting with 3.
   * Thus if distance below distance of item use that level.
   *
   */
  public static final List<Double> SUB_RECEPTOR_DISTANCES = List.of(SUB_RECEPTOR_FARTHEST_DISTANCE, 150D);

  /**
   * The id of the first level in the list of distances.
   */
  public static final int LEVEL_OFFSET = 3;

  /**
   * Minimum distance a source points must be away from a subreceptor point. If within this distance the subreceptor point is ignored.
   */
  public static final double MIN_SOURCE_SUBRECEPTOR_DISTANCE = 20D;

  private OwN2000SubReceptorConstants() {
    // Constants class
  }
}
