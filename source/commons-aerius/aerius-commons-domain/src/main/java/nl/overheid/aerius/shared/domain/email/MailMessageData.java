/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.email;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import nl.overheid.aerius.shared.domain.Theme;

/**
 * Data class containing everything needed to send an email by the message worker.
 */
public class MailMessageData implements Serializable {

  private static final long serialVersionUID = 2L;

  private final EmailType emailType;
  private final Locale locale;
  private final MailTo mailTo;
  private final Map<ReplacementToken, Enum<?>> enumReplacementTokens = new EnumMap<>(ReplacementToken.class);
  private final Map<ReplacementToken, String> stringReplacementTokens = new EnumMap<>(ReplacementToken.class);
  private final List<MailAttachment> attachments = new ArrayList<>();

  private Theme theme;

  public MailMessageData(final EmailType emailType, final Locale locale, final MailTo mailTo) {
    this.emailType = emailType;
    this.locale = locale;
    this.mailTo = mailTo;
  }

  public EmailType getEmailType() {
    return emailType;
  }

  public Theme getTheme() {
    return theme;
  }

  public void setTheme(final Theme theme) {
    this.theme = theme;
  }

  public Locale getLocale() {
    return locale;
  }

  public MailTo getMailTo() {
    return mailTo;
  }

  public void setEnumReplacementToken(final ReplacementToken token, final Enum<?> replaceWith) {
    enumReplacementTokens.put(token, replaceWith);
  }

  public Map<ReplacementToken, Enum<?>> getEnumReplacementTokens() {
    return enumReplacementTokens;
  }

  /**
   * Set the value to replace a replace token with when sending an email, overwriting any previously added value for that token.
   * @param token The token to set the value for.
   * @param value The value that will replace the token in the template.
   * Can be null, in which case the token will not be replaced at all
   * (leaving a [TOKEN] in the mail if it was present in the template).
   */
  public void setReplacement(final ReplacementToken token, final String replaceWith) {
    if (replaceWith == null) {
      stringReplacementTokens.remove(token);
    } else {
      stringReplacementTokens.put(token, replaceWith);
    }
  }

  public Map<ReplacementToken, String> getReplacements() {
    return stringReplacementTokens;
  }

  public void addAttachment(final MailAttachment attachment) {
    attachments.add(attachment);
  }

  public List<MailAttachment> getAttachments() {
    return attachments;
  }

  @Override
  public String toString() {
    return "MailMessageData [templates=" + enumReplacementTokens + ", locale=" + locale + ", mailTo=" + mailTo
        + ", replacements=" + stringReplacementTokens.size() + ", attachments=" + attachments.size() + "]";
  }
}
