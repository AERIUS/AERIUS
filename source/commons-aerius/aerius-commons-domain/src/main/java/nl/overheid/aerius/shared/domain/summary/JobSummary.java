/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.summary;

import java.util.List;

/**
 * Summary of results for a job.
 */
public class JobSummary {

  private final List<JobSummarySituationRecord> situationRecords;
  private final List<JobSummaryProjectRecord> projectRecords;

  public JobSummary(final List<JobSummarySituationRecord> situationRecords, final List<JobSummaryProjectRecord> projectRecords) {
    this.situationRecords = situationRecords;
    this.projectRecords = projectRecords;
  }

  public List<JobSummarySituationRecord> getSituationRecords() {
    return situationRecords;
  }

  public List<JobSummaryProjectRecord> getProjectRecords() {
    return projectRecords;
  }

}
