/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.calculation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioResults;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituationResults;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;

/**
 * This implementation of the ScenarioCalculations interface works entirely on one Scenario object.
 * It will ensure that the calculation objects are created as needed, and keep track of those calculations with respect to situations.
 *
 */
public class ScenarioCalculationsImpl implements ScenarioCalculations {

  public static class CalculationPointSetTrackerImpl implements CalculationPointSetTracker {

    private Integer calculationPointSetId;
    private Integer calculationSubPointSetId;

    @Override
    public Integer getCalculationPointSetId() {
      return calculationPointSetId;
    }

    @Override
    public void setCalculationPointSetId(final Integer calculationPointSetId) {
      this.calculationPointSetId = calculationPointSetId;
    }

    @Override
    public Integer getCalculationSubPointSetId() {
      return calculationSubPointSetId;
    }

    @Override
    public void setCalculationSubPointSetId(final Integer calculationSubPointSetId) {
      this.calculationSubPointSetId = calculationSubPointSetId;
    }

  }

  private final CalculationPointSetTracker calculationPointSetTracker = new CalculationPointSetTrackerImpl();
  private final Scenario scenario;
  private final ScenarioResults results = new ScenarioResults();
  private final List<Calculation> calculations = new ArrayList<>();

  public ScenarioCalculationsImpl(final Scenario scenario) {
    this.scenario = scenario;
    scenario.getSituations().forEach(situation -> calculations.add(createCalculation(situation, results)));
  }

  private static Calculation createCalculation(final ScenarioSituation situation, final ScenarioResults results) {
    final Calculation calculation = new Calculation(situation);

    calculation.setCalculationIdChangeListener(calculationId -> {
      final ScenarioSituationResults situationResults = new ScenarioSituationResults();
      situationResults.setCalculationId(calculationId);
      results.getResultsPerSituation().put(situation.getId(), situationResults);
    });
    return calculation;
  }

  @Override
  public Scenario getScenario() {
    return scenario;
  }

  @Override
  public CalculationSetOptions getOptions() {
    return scenario.getOptions();
  }

  @Override
  public CalculationPointSetTracker getCalculationPointSetTracker() {
    return calculationPointSetTracker;
  }

  @Override
  public List<CalculationPointFeature> getCalculationPoints() {
    return scenario.getCustomPointsList();
  }

  @Override
  public List<Calculation> getCalculations() {
    return calculations;
  }

  @Override
  public List<EmissionSourceFeature> getSources(final int calculationId) {
    final Optional<Calculation> calculation = getCalculation(calculationId);
    return calculation
        .map(c -> scenario.getSituations().get(calculations.indexOf(c)).getEmissionSourcesList())
        .orElse(null);
  }

  @Override
  public Map<Integer, Integer> getCalculationYears() {
    final Map<Integer, Integer> calculationYears = new HashMap<>();
    for (final Calculation calculation : calculations) {
      calculationYears.put(calculation.getCalculationId(), scenario.getSituations().get(calculations.indexOf(calculation)).getYear());
    }
    return calculationYears;
  }

  private Optional<Calculation> getCalculation(final int calculationId) {
    return calculations.stream().filter(c -> c.getCalculationId() == calculationId).findFirst();
  }

  @Override
  public Optional<Calculation> getCalculationForSituationType(final SituationType situationType) {
    final Optional<Calculation> correctCalculation;
    final Optional<String> correspondingSituationId = getSituationForSituationType(situationType).map(ScenarioSituation::getId);
    final Optional<ScenarioSituationResults> situationResults = correspondingSituationId.map(id -> results.getResultsPerSituation().get(id));
    if (situationResults.isPresent()) {
      final int calculationId = situationResults.get().getCalculationId();
      correctCalculation = calculations.stream().filter(calculation -> calculation.getCalculationId() == calculationId).findFirst();
    } else {
      correctCalculation = Optional.empty();
    }
    return correctCalculation;
  }

  @Override
  public Optional<Calculation> getCalculationForSituation(final ScenarioSituation situation) {
    final int index = scenario.getSituations().indexOf(situation);
    return index >= 0 ? Optional.of(calculations.get(index)) : Optional.empty();
  }

  @Override
  public ScenarioResults getScenarioResults() {
    return results;
  }

  private Optional<ScenarioSituation> getSituationForSituationType(final SituationType situationType) {
    // Will only return the first occurrence (if any), be wary.
    return scenario.getSituations().stream()
        .filter(situation -> situation.getType() == situationType)
        .findFirst();
  }

}
