/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.result;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.theme.cimlk.CIMLKCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKMonitorSubstance;

/**
 * Calculation result on a point with CIMLK specific data fields.
 * The basis results are the summed results of srm1, srm2 and background.
 * Each of those results (srm1, srm2, background) is also available as separate results in this class.
 */
public class CIMLKResultPoint extends AeriusResultPoint {

  private static final long serialVersionUID = -6739647179678428136L;

  //calculation background concentrations
  private final EmissionResults backgroundConcentrations = new EmissionResults();
  // GCN concentrations
  private final EmissionResults gcnConcentrations = new EmissionResults();
  // Corrections HWN concentrations
  private final EmissionResults cHwnConcentrations = new EmissionResults();
  private final EmissionResults srm1Results = new EmissionResults();
  private final EmissionResults srm2Results = new EmissionResults();
  private final EmissionResults userCorrections = new EmissionResults();
  private CIMLKMonitorSubstance monitorSubstance;

  /**
   * Windfactor used in SRM1 calculations.
   */
  private double windFactor;

  public CIMLKResultPoint(final AeriusPoint aeriusPoint) {
    super(aeriusPoint);
    if (aeriusPoint instanceof CIMLKCalculationPoint) {
      this.setLabel(aeriusPoint.getLabel());
      this.monitorSubstance = ((CIMLKCalculationPoint) aeriusPoint).getMonitorSubstance();
    }
  }

  public EmissionResults getBackgroundConcentrations() {
    return backgroundConcentrations;
  }

  public EmissionResults getGcnConcentrations() {
    return gcnConcentrations;
  }

  public EmissionResults getCHwnConcentrations() {
    return cHwnConcentrations;
  }

  public EmissionResults getSrm1Results() {
    return srm1Results;
  }

  public EmissionResults getSrm2Results() {
    return srm2Results;
  }

  public EmissionResults getUserCorrections() {
    return userCorrections;
  }

  public double getWindFactor() {
    return windFactor;
  }

  public void setWindFactor(final double windFactor) {
    this.windFactor = windFactor;
  }

  public CIMLKMonitorSubstance getMonitorSubstance() {
    return monitorSubstance;
  }

  public void setMonitorSubstance(final CIMLKMonitorSubstance monitorSubstance) {
    this.monitorSubstance = monitorSubstance;
  }

}
