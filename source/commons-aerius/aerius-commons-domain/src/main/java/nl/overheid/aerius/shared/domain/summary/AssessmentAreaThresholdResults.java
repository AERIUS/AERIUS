/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.summary;

import java.util.HashMap;
import java.util.Map;

import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Class containing the threshold results for an assessment area.
 */
public class AssessmentAreaThresholdResults {

  private final AssessmentArea assessmentArea;
  private final Map<EmissionResultKey, ThresholdResult> results = new HashMap<>();

  public AssessmentAreaThresholdResults(final AssessmentArea assessmentArea) {
    this.assessmentArea = assessmentArea;
  }

  public AssessmentArea getAssessmentArea() {
    return assessmentArea;
  }

  public Map<EmissionResultKey, ThresholdResult> getResults() {
    return results;
  }

}
