/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.summary;

import java.io.Serializable;

import nl.overheid.aerius.shared.domain.v2.geojson.Point;

/**
 * Class to contain result for a custom point.
 */
public class CustomCalculationPointResult implements Serializable {

  private static final long serialVersionUID = 1L;

  private final int customPointId;
  private final Point point;
  private final Double result;

  public CustomCalculationPointResult(final int customPointId, final Point point, final Double result) {
    this.customPointId = customPointId;
    this.point = point;
    this.result = result;
  }

  public int getCustomPointId() {
    return customPointId;
  }

  public Point getPoint() {
    return point;
  }

  public boolean hasResult() {
    return result != null;
  }

  public Double getResult() {
    return result;
  }
}
