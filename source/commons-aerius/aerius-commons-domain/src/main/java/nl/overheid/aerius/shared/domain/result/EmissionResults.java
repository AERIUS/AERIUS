/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.result;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import nl.overheid.aerius.shared.domain.Substance;

/**
 * Data class to contain result values for a single point.
 */
public class EmissionResults implements Serializable {

  private static final long serialVersionUID = 1L;

  private final HashMap<EmissionResultKey, Double> hashMap = new HashMap<>();

  public void clear() {
    hashMap.clear();
  }

  /**
   * Copies the results in the class to the target class.
   *
   * @param target to copy results to
   */
  public void copyTo(final EmissionResults target) {
    for (final Entry<EmissionResultKey, Double> entry : hashMap.entrySet()) {
      target.put(entry.getKey(), entry.getValue());
    }
  }

  /**
   * Adds the value for the give key if a value is already present.
   *
   * @param key The EmissionResultKey to put
   * @param value the value to add.
   */
  public void add(final EmissionResultKey key, final double value) {
     hashMap.merge(key, value, Double::sum);
  }

  /**
   * Put the given key and value in the emission result set. Don't hatch the ERK.
   *
   * @param key The EmissionResultKey to put
   * @param value the value to put.
   */
  public void put(final EmissionResultKey key, final Double value) {
    hashMap.put(key, value);
  }

  /**
   * Assumes that, in a set of emission results, for a given emission result type,
   * if a combined NH3+NOx value exist, no values exist for NOx and/or NH3 separately,
   * and if NOx and/or NH3 values exist separately, no combined NH3+NOx values exists.
   * @param key The key to get the result for.
   * @return Always returns a value; if no value exists for the given key, it returns 0.
   */
  public double get(final EmissionResultKey key) {
    double er = 0.0;
    if (key != null) {
      final Substance substance = key.getSubstance();

      if (substance == Substance.NOXNH3 && !hashMap.containsKey(key) && key.getEmissionResultType() == EmissionResultType.DEPOSITION) {
        for (final EmissionResultKey erk : key.hatch()) {
          er += get(erk);
        }
      } else if (hashMap.containsKey(key)) {
        final Double erd = hashMap.get(key);
        er = erd == null ? 0.0 : erd;
      }
    }
    return er;
  }

  public Double remove(final EmissionResultKey key) {
    return hashMap.remove(key);
  }

  /**
   * @param key The key to determine if a result is available.
   * @return True if there is a result.
   */
  public boolean hasResult(final EmissionResultKey key) {
    boolean hasResult = false;
    if (key != null) {
      final Substance substance = key.getSubstance();

      if (substance == Substance.NOXNH3 && !hashMap.containsKey(key)) {
        for (final EmissionResultKey erk : key.hatch()) {
          hasResult = hasResult || hasResult(erk);
        }
      } else {
        hasResult = hashMap.containsKey(key);
      }
    }
    return hasResult;
  }

  /**
   * @return The entryset containing the results per EmissionResultKey.
   */
  public Set<Entry<EmissionResultKey, Double>> entrySet() {
    return hashMap.entrySet();
  }

  public Map<EmissionResultKey, Double> getHashMap() {
    return hashMap;
  }

  public boolean isEmpty() {
    return hashMap.isEmpty();
  }

  @Override
  public String toString() {
    return "EmissionResults [map:" + hashMap.toString() + "]";
  }
}
