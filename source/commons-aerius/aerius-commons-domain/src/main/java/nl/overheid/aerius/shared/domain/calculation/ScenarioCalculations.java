/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.calculation;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioResults;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;

/**
 * Methods used during calculations of a scenario.
 */
public interface ScenarioCalculations {

  /**
   * Calculation point set tracker.
   * Used to ensure that all calculations in the scenario use the same calculation point set ID.
   * This should always be the same object during scenario calculations.
   */
  public interface CalculationPointSetTracker {
    Integer getCalculationPointSetId();

    void setCalculationPointSetId(Integer calculationPointSetId);

    Integer getCalculationSubPointSetId();

    void setCalculationSubPointSetId(Integer calculationSubPointSetId);
  }

  /**
   * Get the scenario that is being calculated.
   */
  Scenario getScenario();

  /**
   * Get the calculation that is used for a specific situation type, if available.
   * If the scenario contained multiple situations (and multiple calculations) for the same situation type,
   * this will return only one of the applicable calculations.
   */
  Optional<Calculation> getCalculationForSituationType(SituationType situationType);

  /**
   * Get the calculation that is used for a specific situation, if available.
   */
  Optional<Calculation> getCalculationForSituation(ScenarioSituation situation);

  /**
   * The calculation options (to be) used for all calculations.
   */
  CalculationSetOptions getOptions();

  /**
   * @see {@link CalculationPointSetTracker}.
   */
  CalculationPointSetTracker getCalculationPointSetTracker();

  /**
   * The (user defined) calculation points that have to be calculated.
   */
  List<CalculationPointFeature> getCalculationPoints();

  /**
   * The list of calculations that should represent the scenario.
   * Each situation in the scenario should end up being one calculation.
   * These calculations should have been initialized with the correct objects, like sources.
   * The calculation ID should not be set yet, that will be done during the calculation.
   */
  List<Calculation> getCalculations();

  /**
   * Retrieve the sources that have been used for a calculation within this scenario.
   * Can return null, for instance if no calculation with the ID is found in this object.
   */
  List<EmissionSourceFeature> getSources(int calculationId);

  /**
   * The object that keeps track of result related information for a scenario.
   */
  ScenarioResults getScenarioResults();

  /**
   * Retrieve the years for each calculation in this scenario
   *
   * Map<Calculation Id, Year>
   */
  Map<Integer, Integer> getCalculationYears();

}
