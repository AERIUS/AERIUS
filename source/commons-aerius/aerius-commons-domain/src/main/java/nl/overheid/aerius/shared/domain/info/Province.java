/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.info;

import java.io.Serializable;

import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.shared.domain.HasName;

/**
 *
 */
public class Province implements Serializable, Comparable<Province>, HasName, HasId {

  private static final long serialVersionUID = -595898501630017232L;

  private int provinceId;
  private String name;

  public int getProvinceId() {
    return provinceId;
  }

  public void setProvinceId(final int provinceId) {
    this.provinceId = provinceId;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void setName(final String name) {
    this.name = name;
  }

  @Override
  public int compareTo(final Province o) {
    return name.compareTo(o.getName());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + provinceId;
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj != null && obj.getClass() == this.getClass() && this.provinceId == ((Province) obj).provinceId;
  }

  @Override
  public int getId() {
    return provinceId;
  }

  @Override
  public void setId(final int id) {
    this.provinceId = id;
  }
}
