/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.summary;

import java.util.List;

import nl.overheid.aerius.shared.domain.info.AssessmentArea;

public class ProjectSituationResultsSummary extends SituationResultsSummary {

  // Areas that have depositions in the individual situation calculation, but not in the overall project calculation.
  private final List<AssessmentArea> omittedAreas;

  public ProjectSituationResultsSummary(final SituationResultsStatistics statistics,
      final List<SituationResultsAreaSummary> areaStatistics,
      final List<ResultStatisticsMarker> markers,
      final SituationResultsStatisticReceptors statisticReceptors,
      final List<CustomCalculationPointResult> customPointResults,
      final List<AssessmentArea> omittedAreas) {
    super(statistics, areaStatistics, markers, statisticReceptors, customPointResults);
    this.omittedAreas = omittedAreas;
  }

  public List<AssessmentArea> getOmittedAreas() {
    return omittedAreas;
  }
}
