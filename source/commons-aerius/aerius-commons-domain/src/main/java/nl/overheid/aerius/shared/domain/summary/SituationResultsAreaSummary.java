/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.summary;

import java.util.List;

import nl.overheid.aerius.shared.domain.info.AssessmentArea;

/**
 * Summary of results for an assessment area for a single situation.
 */
public class SituationResultsAreaSummary {

  private final AssessmentArea assessmentArea;
  private final boolean sensitive;
  // emission result chart information
  private final List<SurfaceChartResults> emissionResultChartResults;
  // critical load chart information
  private final List<SurfaceChartResults> percentageCriticalLoadChartResults;
  // table information
  private final SituationResultsStatistics statistics = new SituationResultsStatistics();
  // habitat information
  private final List<SituationResultsHabitatSummary> habitatSummaries;
  // receptor information
  private final List<SituationResultsEdgeReceptor> edgeReceptors;

  public SituationResultsAreaSummary(final AssessmentArea assessmentArea, final boolean sensitive,
      final List<SurfaceChartResults> emissionResultChartResults, final List<SurfaceChartResults> percentageCriticalLoadChartResults,
      final List<SituationResultsHabitatSummary> habitatSummaries, final List<SituationResultsEdgeReceptor> edgeReceptors) {
    this.assessmentArea = assessmentArea;
    this.sensitive = sensitive;
    this.emissionResultChartResults = emissionResultChartResults;
    this.percentageCriticalLoadChartResults = percentageCriticalLoadChartResults;
    this.habitatSummaries = habitatSummaries;
    this.edgeReceptors = edgeReceptors;
  }

  public AssessmentArea getAssessmentArea() {
    return assessmentArea;
  }

  public boolean isSensitive() {
    return sensitive;
  }

  public List<SurfaceChartResults> getEmissionResultChartResults() {
    return emissionResultChartResults;
  }

  public List<SurfaceChartResults> getPercentageCriticalLoadChartResults() {
    return percentageCriticalLoadChartResults;
  }

  public SituationResultsStatistics getStatistics() {
    return statistics;
  }

  public List<SituationResultsHabitatSummary> getHabitatSummaries() {
    return habitatSummaries;
  }

  public List<SituationResultsEdgeReceptor> getEdgeReceptors() {
    return edgeReceptors;
  }
}
