/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.importer.summary;

import java.util.List;
import java.util.Map;

/**
 * Represents a summary of a situation as can be requested using Connect.
 * <br><br>
 * Should match API Spec for file summaries.
 * This is tested in `aerius-connect-service`:
 * {@link nl.overheid.aerius.connectservice.service.ScenarioSummaryServiceTest}
 */
public class SituationSummary {

  private String name;
  private int year;
  private String type;
  private Map<String, String> metadata;
  private String version;
  private String databaseVersion;
  private SourcesSummary sources;
  private List<SituationResults> results;

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public int getYear() {
    return year;
  }

  public void setYear(final int year) {
    this.year = year;
  }

  public String getType() {
    return type;
  }

  public void setType(final String type) {
    this.type = type;
  }

  public Map<String, String> getMetadata() {
    return metadata;
  }

  public void setMetadata(final Map<String, String> metadata) {
    this.metadata = metadata;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(final String version) {
    this.version = version;
  }

  public String getDatabaseVersion() {
    return databaseVersion;
  }

  public void setDatabaseVersion(final String databaseVersion) {
    this.databaseVersion = databaseVersion;
  }

  public SourcesSummary getSources() {
    return sources;
  }

  public void setSources(final SourcesSummary sources) {
    this.sources = sources;
  }

  public List<SituationResults> getResults() {
    return results;
  }

  public void setResults(final List<SituationResults> results) {
    this.results = results;
  }
}
