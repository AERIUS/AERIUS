/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.importer.summary;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Represents a row of results as can be requested using Connect.
 * <br><br>
 * Should match API Spec for file summaries.
 * This is tested in `aerius-connect-service`:
 * {@link nl.overheid.aerius.connectservice.service.ScenarioSummaryServiceTest}
 */
public class SituationResults {

  private int receptorId;
  private Integer subPointId;
  private String resultType;
  private Map<String, BigDecimal> results;

  public int getReceptorId() {
    return receptorId;
  }

  public void setReceptorId(final int receptorId) {
    this.receptorId = receptorId;
  }

  public Integer getSubPointId() {
    return subPointId;
  }

  public void setSubPointId(final Integer subPointId) {
    this.subPointId = subPointId;
  }

  public String getResultType() {
    return resultType;
  }

  public void setResultType(final String resultType) {
    this.resultType = resultType;
  }

  public Map<String, BigDecimal> getResults() {
    return results;
  }

  public void setResults(final Map<String, BigDecimal> results) {
    this.results = results;
  }
}
