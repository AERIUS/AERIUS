/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.calculation;

import java.io.Serializable;
import java.util.Date;

public class JobProgress implements Serializable {

  private static final long serialVersionUID = 2L;

  private JobType type;
  private String key;
  private String name;
  private boolean protect;
  private JobState state;
  private int queuePosition;
  private Date startDateTime;
  private Date endDateTime;
  private int numberOfPoints;
  private double numberOfPointsCalculated;
  private int estimatedRemainingCalculationTime;
  private String resultUrl;
  private String errorMessage;

  public JobType getType() {
    return type;
  }

  public void setType(final JobType type) {
    this.type = type;
  }

  public JobState getState() {
    return state;
  }

  public void setState(final JobState state) {
    this.state = state;
  }

  public int getQueuePosition() {
    return queuePosition;
  }

  public void setQueuePosition(final int queuePosition) {
    this.queuePosition = queuePosition;
  }

  public int getNumberOfPoints() {
    return numberOfPoints;
  }

  public void setNumberOfPoints(final int numberOfPoints) {
    this.numberOfPoints = numberOfPoints;
  }

  public double getNumberOfPointsCalculated() {
    return numberOfPointsCalculated;
  }

  public void setNumberOfPointsCalculated(final double numberOfPointsCalculated) {
    this.numberOfPointsCalculated = numberOfPointsCalculated;
  }

  public int getEstimatedRemainingCalculationTime() {
    return estimatedRemainingCalculationTime;
  }

  public void setEstimatedRemainingCalculationTime(final int estimatedRemainingCalculationTime) {
    this.estimatedRemainingCalculationTime = estimatedRemainingCalculationTime;
  }

  public Date getStartDateTime() {
    return startDateTime;
  }

  public void setStartDateTime(final Date startDateTime) {
    this.startDateTime = startDateTime;
  }

  public Date getEndDateTime() {
    return endDateTime;
  }

  public void setEndDateTime(final Date endDateTime) {
    this.endDateTime = endDateTime;
  }

  public String getResultUrl() {
    return resultUrl;
  }

  public void setResultUrl(final String resultUrl) {
    this.resultUrl = resultUrl;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(final String errorMessage) {
    this.errorMessage = errorMessage;
  }

  public String getKey() {
    return key;
  }

  public void setKey(final String key) {
    this.key = key;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public boolean isProtect() {
    return protect;
  }

  public void setProtect(final boolean protect) {
    this.protect = protect;
  }

  @Override
  public String toString() {
    return "JobProgress [type=" + type + ", key=" + key + ", name=" + name + ", protect=" + protect + ", state=" + state + ", queuePosition="
        + queuePosition + ", startDateTime=" + startDateTime + ", endDateTime=" + endDateTime + ", numberOfPoints=" + numberOfPoints
        + ", numberOfPointsCalculated=" + numberOfPointsCalculated + ", estimatedRemainingCalculationTime=" + estimatedRemainingCalculationTime
        + ", resultUrl=" + resultUrl + ", errorMessage=" + errorMessage + "]";
  }
}
