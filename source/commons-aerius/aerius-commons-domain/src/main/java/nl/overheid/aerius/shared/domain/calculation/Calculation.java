/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.calculation;

import java.io.Serializable;
import java.util.List;

import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.building.BuildingFeature;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKCorrection;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKDispersionLineFeature;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKMeasureFeature;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;

/**
 * The {@link Calculation} object contains all data for single calculation for a fixed set of input data consisting of:
 * year, substances and sources. Additional it contains the set of options that define the output.
 */
public class Calculation implements Serializable {

  interface CalculationIdChangeListener {

    void onCalculationIdChanged(int newCalculationId);

  }

  private static final long serialVersionUID = 3L;

  private int calculationId;
  private Integer customCalculationPointSetId;
  private Integer customCalculationSubPointSetId;
  private final ScenarioSituation situation;
  private CalculationState state;
  private int year;

  private CalculationIdChangeListener calculationIdChangeListener;

  public Calculation() {
    this(new ScenarioSituation());
  }

  public Calculation(final ScenarioSituation situation) {
    this.situation = situation;
    this.year = situation.getYear();
  }

  public int getCalculationId() {
    return calculationId;
  }

  public void setCalculationId(final int calculationId) {
    this.calculationId = calculationId;
    if (calculationIdChangeListener != null) {
      calculationIdChangeListener.onCalculationIdChanged(calculationId);
    }
  }

  public Integer getCustomCalculationPointSetId() {
    return customCalculationPointSetId;
  }

  public void setCustomCalculationPointSetId(final Integer customCalculationPointSetId) {
    this.customCalculationPointSetId = customCalculationPointSetId;
  }

  public Integer getCustomCalculationSubPointSetId() {
    return customCalculationSubPointSetId;
  }

  public void setCustomCalculationSubPointSetId(final Integer customCalculationSubPointSetId) {
    this.customCalculationSubPointSetId = customCalculationSubPointSetId;
  }

  public ScenarioSituation getSituation() {
    return situation;
  }

  public String getSituationReference() {
    return situation.getId();
  }

  public SituationType getSituationType() {
    return situation.getType();
  }

  public synchronized CalculationState getState() {
    if (state == null) {
      state = CalculationState.UNDEFINED;
    }
    return state;
  }

  public synchronized void setState(final CalculationState state) {
    // Don't set state completed if it was cancelled
    if (this.state == CalculationState.CANCELLED && state == CalculationState.COMPLETED) {
      return;
    }
    this.state = state;
  }

  public int getYear() {
    return year;
  }

  public void setYear(final int year) {
    this.year = year;
  }

  public List<EmissionSourceFeature> getSources() {
    return situation.getEmissionSourcesList();
  }

  public List<CIMLKMeasureFeature> getCIMLKMeasures() {
    return situation.getCimlkMeasuresList();
  }

  public List<CIMLKCorrection> getCIMLKCorrections() {
    return situation.getCimlkCorrections();
  }

  public List<CIMLKDispersionLineFeature> getCIMLKDispersionLines() {
    return situation.getCimlkDispersionLinesList();
  }

  public List<BuildingFeature> getBuildings() {
    return situation.getBuildingsList();
  }

  void setCalculationIdChangeListener(final CalculationIdChangeListener calculationIdChangeListener) {
    this.calculationIdChangeListener = calculationIdChangeListener;
  }

  @Override
  public String toString() {
    return "CalculationSet [calculationId=" + calculationId + ", state=" + state + ", year=" + year + ", situation=" + situation + "]";
  }
}
