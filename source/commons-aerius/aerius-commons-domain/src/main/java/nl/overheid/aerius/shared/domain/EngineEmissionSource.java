/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Base class for model specific emission source objects.
 */
public class EngineEmissionSource implements EngineSource {

  private static final long serialVersionUID = 1L;

  private final Map<Substance, Double> emissions = new HashMap<>();

  public Map<Substance, Double> getEmissions() {
    return emissions;
  }

  public void clearEmissions() {
    emissions.clear();
  }

  protected void copyEmissionsTo(final EngineEmissionSource copy) {
    for (final Entry<Substance, Double> entry : emissions.entrySet()) {
      copy.setEmission(entry.getKey(), entry.getValue());
    }
  }

  /**
   * Emission.
   *
   * @param substance substance to get emission for
   * @return emission.
   */
  public double getEmission(final Substance substance) {
    final Double value = emissions.get(substance);
    return value == null ? 0.0 : value.doubleValue();
  }

  public ArrayList<Substance> getSubstances() {
    return new ArrayList<>(emissions.keySet());
  }

  @Override
  public int hashCode() {
    return emissions.hashCode();
  }

  @Override
  public boolean equals(final Object obj) {

    return obj != null && getClass() == obj.getClass() && emissions.equals(((EngineEmissionSource) obj).emissions);
  }

  public boolean hasEmission() {
    for (final Entry<Substance, Double> entry : emissions.entrySet()) {
      if (getEmission(entry.getKey()) > 0) {
        return true;
      }
    }
    return false;
  }

  public void setEmission(final Substance substance, final double emission) {
    emissions.put(substance, emission);
  }
}
