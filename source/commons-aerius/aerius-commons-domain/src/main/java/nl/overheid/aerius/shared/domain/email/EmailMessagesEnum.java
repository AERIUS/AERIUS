/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.email;

/**
 * Enum of keys to use as values for ReplacementTokens.
 * The keys here should match keys in the properties files.
 * However, only keys that need to be explicitly set should go into this enum.
 * For example there are multiple subject keys,
 * depending on the context a specific subject is needed and will be used a value.
 */
public enum EmailMessagesEnum {

  /**
   * Subject to use when sending a new generated apikey.
   */
  CONNECT_APIKEY_CONFIRM_SUBJECT,

  /**
   * Body text to use when sending a new generated apikey.
   */
  CONNECT_APIKEY_CONFIRM_BODY(true),
  /**
   * The subject to use when sending a PAA-file.
   */
  DOWNLOAD_PAA_SUBJECT,
  /**
   * The text to download a file that can be re-imported into AERIUS.
   */
  DOWNLOAD_CONTENT(true),

  /**
   * Text to inform the user the delivered file can be re-imported in AERIUS.
   */
  DOWNLOAD_IMAER_IMPORT,

  /**
   * Text to inform the user the delivered file can be re-imported in AERIUS, including the calculation job ID.
   */
  DOWNLOAD_IMAER_IMPORT_JOB_KEY,

  /**
   * Additional text the PDF is not in English due to Dutch legislation.
   */
  DOWNLOAD_PDF_NO_ENGLISH,
  /**
   * The subject to use when sending a file to download.
   */
  DOWNLOAD_SUBJECT,
  /**
   * The subject to use when sending a file to download for a specific job.
   */
  DOWNLOAD_SUBJECT_JOB,
  /**
   * The content of the mail to use when informing the user about an error.
   */
  ERROR_CONTENT(true),
  /**
   * The table header for the second column in an error mail when dealing with an external error
   */
  EXTERNAL_ERROR_MESSAGE_HEADER,
  /**
   * The table header for the second column in an error mail when dealing with an internal error
   */
  INTERNAL_ERROR_MESSAGE_HEADER,
  /**
   * The message to show when an internal error occurred
   */
  INTERNAL_ERROR_MESSAGE,
  /**
   * The subject to use when sending an email informing the user about an error.
   */
  ERROR_SUBJECT,
  /**
   * File type for a CSV file.
   */
  EXPORT_TYPE_FILE_CSV,
  /**
   * File type for engine input files.
   */
  EXPORT_TYPE_FILE_ENGINE_INPUT,
  /**
   * File type for a GML file.
   */
  EXPORT_TYPE_FILE_GML,
  /**
   * File type for a Formal PDF file.
   */
  EXPORT_TYPE_FILE_PAA,
  /**
   * The actual template of body of a mail (containing disclaimer and such).
   */
  TEMPLATE_CONTENT(true);

  /**
   * If true the key is will append the {@link EmailContentType} to the key.
   * This is used to have different keys in HTML and TEXT e-mails, while in code use 1 key.
   */
  private final boolean contextRelevant;

  EmailMessagesEnum() {
    this(false);
  }

  EmailMessagesEnum(final boolean contextRelevant) {
    this.contextRelevant = contextRelevant;
  }

  /**
   * Returns the key name. If the key is 'contextRelevant' the name returned gets the context type appended.
   *
   * @param contextType context type to get the key for
   * @return context type specific key
   */
  public String getKey(final EmailContentType contextType) {
    return name() + (contextRelevant ? ("_" + contextType.name()) : "");
  }
}
