/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.email;

/**
 * Tokens to use in email context to replace with actual content.
 * These keys have no value in the properties file but need to be set depending on the context.
 */
public enum ReplacementToken {

  /**
   * Adds additional text below the download link in the e-mail.
   */
  ADDITIONAL_DOWNLOAD_INFO_1,
  /**
   * Adds an additional second text below the download link in the e-mail.
   */
  ADDITIONAL_DOWNLOAD_INFO_2,
  /**
   * The reference of the request (melding or permit).
   */
  REFERENCE,

  /**
   * The calculation creation date (date only, no time).
   */
  CREATION_DATE,

  /**
   * The calculation creation time (time only, no date).
   */
  CREATION_TIME,

  /**
   * New connect apikey.
   */
  CONNECT_APIKEY,

  /**
   * The full download link.
   */
  DOWNLOAD_LINK,

  /**
   * Name of the file type.
   */
  EXPORT_TYPE_FILE,

  /**
   * The error code.
   */
  ERROR_CODE,

  /**
   * The error message.
   */
  ERROR_MESSAGE,

  /**
   * A label for the error message
   */
  ERROR_MESSAGE_HEADER,

  /**
   * The name of the project.
   */
  PROJECT_NAME,

  /**
   * Job description from the user for the job.
   */
  JOB,

  /**
   * Job key
   */
  JOB_KEY,

  /**
   * Helpdesk url.
   */
  HELPDESK_URL,

  /**
   * The subject of the mail.
   */
  MAIL_SUBJECT,

  /**
   * The content of the mail.
   */
  MAIL_CONTENT,

  /**
   * Template for the email content.
   */
  MAIL_CONTENT_TEMPLATE;
}
