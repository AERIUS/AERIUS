/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.importer.summary;

import java.util.List;

/**
 * Represents a summary of an AERIUS file as can be requested using Connect.
 * <br><br>
 * Should match API Spec for file summaries.
 * This is tested in `aerius-connect-service`:
 * {@link nl.overheid.aerius.connectservice.service.ScenarioSummaryServiceTest}
 */
public class ImportSummary {

  private boolean successful;
  private List<ValidationMessage> errors;
  private List<ValidationMessage> warnings;
  private List<SituationSummary> situations;
  private Boolean aboveDecisionMakingThreshold;
  private Boolean aboveSiteRelevantThreshold;

  public boolean isSuccessful() {
    return successful;
  }

  public void setSuccessful(final boolean successful) {
    this.successful = successful;
  }

  public List<ValidationMessage> getErrors() {
    return errors;
  }

  public void setErrors(final List<ValidationMessage> errors) {
    this.errors = errors;
  }

  public List<ValidationMessage> getWarnings() {
    return warnings;
  }

  public void setWarnings(final List<ValidationMessage> warnings) {
    this.warnings = warnings;
  }

  public List<SituationSummary> getSituations() {
    return situations;
  }

  public void setSituations(final List<SituationSummary> situations) {
    this.situations = situations;
  }

  public Boolean getAboveDecisionMakingThreshold() {
    return aboveDecisionMakingThreshold;
  }

  public void setAboveDecisionMakingThreshold(final Boolean aboveDecisionMakingThreshold) {
    this.aboveDecisionMakingThreshold = aboveDecisionMakingThreshold;
  }

  public Boolean getAboveSiteRelevantThreshold() {
    return aboveSiteRelevantThreshold;
  }

  public void setAboveSiteRelevantThreshold(final Boolean aboveSiteRelevantThreshold) {
    this.aboveSiteRelevantThreshold = aboveSiteRelevantThreshold;
  }

  /**
   * Represents a validation message as is returned from Connect
   * <br><br>
   * Should match API Spec for file summaries.
   * This is tested in `aerius-connect-service`:
   * {@link nl.overheid.aerius.connectservice.service.ScenarioSummaryServiceTest}
   */
  public static class ValidationMessage {

    final int code;
    final String message;

    public ValidationMessage(final int code, final String message) {
      this.code = code;
      this.message = message;
    }

    public int getCode() {
      return code;
    }

    public String getMessage() {
      return message;
    }
  }
}
