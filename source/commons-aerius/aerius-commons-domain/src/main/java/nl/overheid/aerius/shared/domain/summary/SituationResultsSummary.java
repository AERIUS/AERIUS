/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.summary;

import java.util.List;

/**
 * Summary of results for a single situation.
 */
public class SituationResultsSummary {

  // Overall statistics
  private final SituationResultsStatistics statistics;
  // Statistics per assessment area
  private final List<SituationResultsAreaSummary> areaStatistics;
  // marker information
  private final List<ResultStatisticsMarker> markers;
  // statistic receptor information
  private final SituationResultsStatisticReceptors statisticReceptors;
  // Custom point results
  private final List<CustomCalculationPointResult> customPointResults;

  public SituationResultsSummary(final SituationResultsStatistics statistics, final List<SituationResultsAreaSummary> areaStatistics,
      final List<ResultStatisticsMarker> markers, final SituationResultsStatisticReceptors receptors,
      final List<CustomCalculationPointResult> customPointResults) {
    this.statistics = statistics;
    this.areaStatistics = areaStatistics;
    this.markers = markers;
    this.statisticReceptors = receptors;
    this.customPointResults = customPointResults;
  }

  public SituationResultsStatistics getStatistics() {
    return statistics;
  }

  public List<SituationResultsAreaSummary> getAreaStatistics() {
    return areaStatistics;
  }

  public List<ResultStatisticsMarker> getMarkers() {
    return markers;
  }

  public SituationResultsStatisticReceptors getStatisticReceptors() {
    return statisticReceptors;
  }

  public List<CustomCalculationPointResult> getCustomPointResults() {
    return customPointResults;
  }

}
