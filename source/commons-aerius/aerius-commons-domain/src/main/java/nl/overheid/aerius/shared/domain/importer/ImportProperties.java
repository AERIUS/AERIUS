/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.importer;

import java.io.Serializable;
import java.util.EnumSet;
import java.util.Optional;
import java.util.Set;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;

/**
 * Immutable class to contain (optional) properties that can be used when importing.
 */
public class ImportProperties implements Serializable {

  private static final long serialVersionUID = 1L;

  private final Theme theme;
  private final Substance substance;
  private final Integer year;
  private final Set<ImportType> allowedFileTypes;

  /**
   * Constructor where all optional fields are assumed to be null.
   */
  public ImportProperties() {
    this(null, null);
  }

  /**
   * @param substance The substance to use when importing. Can be null.
   * @param year The year to use when importing, to determine emissions for instance. Can be null.
   */
  public ImportProperties(final Substance substance, final Integer year) {
    this(substance, year, EnumSet.allOf(ImportType.class));
  }

  /**
   * @param substance The substance to use when importing. Can be null.
   * @param year The year to use when importing, to determine emissions for instance. Can be null.
   * @param allowedFileTypes set of file types that should be allowed to be imported.
   */
  public ImportProperties(final Substance substance, final Integer year, final Set<ImportType> allowedFileTypes) {
    this(substance, year, allowedFileTypes, Theme.OWN2000);
  }

  /**
   * @param substance The substance to use when importing. Can be null.
   * @param year The year to use when importing, to determine emissions for instance. Can be null.
   * @param allowedFileTypes set of file types that should be allowed to be imported.
   * @param theme the theme to import into.
   */
  public ImportProperties(final Substance substance, final Integer year, final Set<ImportType> allowedFileTypes, final Theme theme) {
    this.substance = substance;
    this.year = year;
    this.allowedFileTypes = allowedFileTypes;
    this.theme = theme;
  }

  /**
   * @return The substance, which can be null.
   */
  public Substance getSubstance() {
    return substance;
  }

  /**
   * @return The substance, wrapped in an optional object.
   */
  public Optional<Substance> getOptSubstance() {
    return Optional.ofNullable(substance);
  }

  /**
   * @return The year, wrapped in an optional object.
   */
  public Optional<Integer> getYear() {
    // If year is 0 it should return as empty as it's not a valid year.
    return year == null || year == 0 ? Optional.empty() : Optional.of(year);
  }

  /**
   * Returns true if the given import type is in the allowed file types set.
   *
   * @param importType import type to check
   * @return true if in allowed file types set
   */
  public boolean containsImportType(final ImportType importType) {
    return allowedFileTypes.contains(importType);
  }

  /**
   * @return the Theme.
   */
  public Theme getTheme() {
    return theme;
  }
}
