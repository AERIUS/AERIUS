/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.summary;

import nl.overheid.aerius.shared.domain.info.HabitatType;

/**
 * Summary of results for a habitat type for a single situation.
 */
public class SituationResultsHabitatSummary {

  private final HabitatType habitatType;
  private final SituationResultsStatistics statistics = new SituationResultsStatistics();
  private final Double minimumCriticalLevel;

  public SituationResultsHabitatSummary(final HabitatType habitatType, final Double minimumCriticalLevel) {
    this.habitatType = habitatType;
    this.minimumCriticalLevel = minimumCriticalLevel;
  }

  public HabitatType getHabitatType() {
    return habitatType;
  }

  public SituationResultsStatistics getStatistics() {
    return statistics;
  }

  public Double getMinimumCriticalLevel() {
    return minimumCriticalLevel;
  }

}
