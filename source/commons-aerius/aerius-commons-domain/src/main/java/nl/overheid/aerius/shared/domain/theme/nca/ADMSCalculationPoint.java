/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.theme.nca;

import java.util.HashMap;
import java.util.Map;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;

/**
 * ADMS calculation point with additional optional data values specific for road NOx to NO2 calculation.
 */
public class ADMSCalculationPoint extends AeriusPoint {

  private static final long serialVersionUID = 3L;

  /**
   * Optional background NOx used in road NOx to NO2 calculation. If 0 the internal background will be used.
   */
  private final Map<Integer, Double> yearBackgroundsNOx = new HashMap<>();
  /**
   * Optional local fNO2 used in road NOx to NO2 calculation. If 0 the internal fNO2 will be used.
   */
  private double roadLocalFNO2;

  public ADMSCalculationPoint() {
  }

  public ADMSCalculationPoint(final AeriusPoint point, final AeriusPointType pointType) {
    super(point.getId(), point.getParentId(), pointType, point.getX(), point.getY(), point.getHeight());
  }

  /**
   * Copies the data specific for an ADMSCalculationPoint from the given point to this instance.
   * @param point point to copy data from
   */
  public void setADMSCalculationPointDataFrom(final ADMSCalculationPoint point) {
    setRoadLocalFNO2(point.getRoadLocalFNO2());
    setHeight(point.getHeight());
    point.yearBackgroundsNOx.entrySet().forEach(e -> this.yearBackgroundsNOx.put(e.getKey(), e.getValue()));
  }

  public double getBackgroundNOx(final int year) {
    return yearBackgroundsNOx.getOrDefault(year, 0.0);
  }

  public void setBackgroundNOx(final int year, final double backgroundNOx) {
    this.yearBackgroundsNOx.put(year, backgroundNOx);
  }

  public double getRoadLocalFNO2() {
    return roadLocalFNO2;
  }

  public void setRoadLocalFNO2(final double roadLocalFNO2) {
    this.roadLocalFNO2 = roadLocalFNO2;
  }
}
