/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.summary;

import java.util.EnumMap;
import java.util.Map;

import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Class to contain information for 1 row in the situation table of the summary tab in the UI.
 */
public class JobSummarySituationRecord {

  private final AssessmentArea assessmentArea;
  private final String calculationReference;
  private final Map<EmissionResultKey, Double> maxContributions = new EnumMap<>(EmissionResultKey.class);

  public JobSummarySituationRecord(final AssessmentArea assessmentArea, final String calculationReference) {
    this.assessmentArea = assessmentArea;
    this.calculationReference = calculationReference;
  }

  public AssessmentArea getAssessmentArea() {
    return assessmentArea;
  }

  public String getCalculationReference() {
    return calculationReference;
  }

  public Map<EmissionResultKey, Double> getMaxContributions() {
    return maxContributions;
  }

}
