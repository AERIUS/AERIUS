/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.theme.nca;

import java.util.List;

/**
 * Util class with NCA constants related to sub receptors.
 */
public final class NcaSubReceptorConstants {

  public NcaSubReceptorConstants() {
    // Util class
  }

  /**
   * The farthest distance of sources to receptors for which sub receptors should be created.
   */
  public static final double SUB_RECEPTOR_FARTHEST_DISTANCE = 1600D;

  /**
   * Distances to use to determine if subreceptor calculation should be used.
   * Order of the list is from highest distance to lowest.
   * If a source-receptor distance is shorter than the value in the list that index is used.
   *
   * Position in the list determines the sub level index, starting with 1.
   * Thus if distance below distance of item use that level.
   *
   */
  public static final List<Double> SUB_RECEPTOR_DISTANCES = List.of(SUB_RECEPTOR_FARTHEST_DISTANCE, 800D, 400D, 200D);
}
