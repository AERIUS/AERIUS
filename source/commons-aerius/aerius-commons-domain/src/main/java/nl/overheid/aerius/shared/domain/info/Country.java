/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.info;

import java.io.Serializable;

import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.shared.domain.HasName;

/**
 *
 */
public class Country implements Serializable, Comparable<Country>, HasId, HasName {

  private static final long serialVersionUID = 2868961604868706431L;

  private int countryId;
  private String code;
  private String name;

  public int getCountryId() {
    return countryId;
  }

  public void setCountryId(final int countryId) {
    this.countryId = countryId;
  }

  public String getCode() {
    return code;
  }

  public void setCode(final String code) {
    this.code = code;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void setName(final String name) {
    this.name = name;
  }

  @Override
  public int getId() {
    return countryId;
  }

  @Override
  public void setId(final int id) {
    this.countryId = id;
  }

  @Override
  public int compareTo(final Country o) {
    return name.compareTo(o.getName());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + countryId;
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj != null && this.getClass() == obj.getClass() && this.countryId == ((Country) obj).countryId;
  }

  @Override
  public String toString() {
    return "Country [countryId=" + countryId + ", code=" + code + ", name=" + name + "]";
  }

}
