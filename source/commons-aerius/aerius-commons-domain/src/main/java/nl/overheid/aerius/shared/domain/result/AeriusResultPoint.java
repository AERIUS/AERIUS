/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.result;

import java.util.Map.Entry;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;

/**
 * AeriusPoint with calculation result values.
 */
public class AeriusResultPoint extends AeriusPoint {

  private static final long serialVersionUID = 5L;

  private final EmissionResults emissionResults = new EmissionResults();

  /**
   * For deposition results this field contains the number of subreceptor points that was used to calculate the combined deposition.
   * If no sub points used the value is 1, representing itself.
   */
  private int numberOfSubPointsUsed = 1;

  /**
   * Constructor to create an AeriusResultPoint from an AeriusPoint.
   *
   * @param point
   */
  public AeriusResultPoint(final AeriusPoint point) {
    this(point.getId(), point.getParentId(), point.getGmlId(), point.getPointType(), point.getX(), point.getY(), point.getHeight());
  }

  /**
   * Creates a AeriusResultPoint.
   *
   * @param id
   * @param parentId
   * @param pointType
   * @param x
   * @param y
   */
  public AeriusResultPoint(final int id, final Integer parentId, final AeriusPointType pointType, final double x, final double y) {
    this(id, parentId, String.valueOf(id), pointType, x, y, null);
  }

  /**
   * Creates a AeriusResultPoint.
   *
   * @param id
   * @param parentId
   * @param gmlId
   * @param pointType
   * @param x
   * @param y
   * @param height
   */
  private AeriusResultPoint(final int id, final Integer parentId, final String gmlId, final AeriusPointType pointType, final double x, final double y,
      final Double height) {
    super(id, parentId == null ? 0 : parentId, pointType, x, y, height);
    this.setGmlId(gmlId);
  }

  /**
   * Stores the results from the given AeriusResultPoint into this one. If a value already exists it's overridden.
   *
   * @param point the AeriusResultPoint that is to be merged into this one
   *
   * @return true if merge is successful
   */
  public boolean merge(final AeriusResultPoint point) {
    final boolean success;
    if (point == null) {
      success = false;
    } else {
      for (final Entry<EmissionResultKey, Double> entry : point.emissionResults.entrySet()) {
        emissionResults.put(entry.getKey(), entry.getValue());
      }
      success = true;
    }
    return success;
  }

  public EmissionResults getEmissionResults() {
    return emissionResults;
  }

  /**
   * Gets the emission result for the specified substance in this receptor.
   *
   * If the given Substance is the combined NH3 + NOx substance, aggregate the results
   * (AeriusPoint emission results in this enum value should always be null)
   *
   * @param key the Substance
   * @return the emission result
   */
  public double getEmissionResult(final EmissionResultKey key) {
    return emissionResults.get(key);
  }

  /**
   * Returns true if this object has a emission result value for the given substance.
   * If the substance is {@link Substance#NOXNH3} it returns true if it contains
   * a result value for either NOX or NH3.
   * @param key substance to check
   * @return true if substance present
   */
  public boolean hasEmissionResult(final EmissionResultKey key) {
    boolean hasResults = false;

    if (key != null) {
      if (key.getSubstance() == Substance.NOXNH3) {
        for (final EmissionResultKey erk : key.hatch()) {
          hasResults |= hasEmissionResult(erk);
        }
      } else {
        hasResults = Double.doubleToLongBits(emissionResults.get(key)) != 0;
      }
    }
    return hasResults;
  }

  /**
   * Returns true if this object has any emission result values other than 0.0.
   *
   * @return true if there is any emission result
   */
  public boolean hasAnyResult() {
    return emissionResults.getHashMap().values().stream()
        .anyMatch(value -> Double.doubleToLongBits(value) != 0);
  }

  /**
   * Set the given emission result for the given Substance.
   *
   * @param key the emission result key
   * @param value the emission result
   */
  public void setEmissionResult(final EmissionResultKey key, final Double value) {
    emissionResults.put(key, value);
  }

  /**
   * Removes the result for the given key.
   *
   * @param key the emission result key
   */
  public void unSetEmissionResult(final EmissionResultKey key) {
    emissionResults.remove(key);
  }

  /**
   * Removes all result data.
   */
  public void clearResults() {
    emissionResults.clear();
  }

  public int getNumberOfSubPointsUsed() {
    return numberOfSubPointsUsed;
  }

  public void setNumberOfSubPointsUsed(final int numberOfSubPointsUsed) {
    this.numberOfSubPointsUsed = numberOfSubPointsUsed;
  }

  @Override
  public String toString() {
    return "AeriusResultPoint [" + super.toString() + ", emissionResults=" + emissionResults + "]";
  }
}
