/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.summary;

import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 *
 */
public class SummaryRequest {

  private final ScenarioResultType resultType;
  private final int jobId;
  private final Integer calculationId;
  private final SummaryHexagonType hexagonType;
  private final OverlappingHexagonType overlappingHexagonType;
  private final EmissionResultKey emissionResultKey;
  private final ProcurementPolicy procurementPolicy;

  public SummaryRequest(final ScenarioResultType resultType, final int jobId, final Integer calculationId, final SummaryHexagonType hexagonType,
      final OverlappingHexagonType overlappingHexagonType, final EmissionResultKey emissionResultKey, final ProcurementPolicy procurementPolicy) {
    this.resultType = resultType;
    this.jobId = jobId;
    this.calculationId = calculationId;
    this.hexagonType = hexagonType;
    this.overlappingHexagonType = overlappingHexagonType;
    this.emissionResultKey = emissionResultKey;
    this.procurementPolicy = procurementPolicy;
  }

  public ScenarioResultType getResultType() {
    return resultType;
  }

  public int getJobId() {
    return jobId;
  }

  public Integer getCalculationId() {
    return calculationId;
  }

  public SummaryHexagonType getHexagonType() {
    return hexagonType;
  }

  public OverlappingHexagonType getOverlappingHexagonType() {
    return overlappingHexagonType;
  }

  public EmissionResultKey getEmissionResultKey() {
    return emissionResultKey;
  }

  public ProcurementPolicy getProcurementPolicy() {
    return procurementPolicy;
  }
}
