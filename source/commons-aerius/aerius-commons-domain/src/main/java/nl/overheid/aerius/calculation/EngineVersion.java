/*
 * Copyright the State of the Netherlands
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.util.Arrays;

/**
 * Interface for supporting different versions of the model.
 */
public interface EngineVersion {
  /**
   * @return Returns the version string this model version can be identified with.
   */
  String getVersion();

  /**
   * Utility method to get the version by label.
   *
   * @param versions versions to match with
   * @param name type of version to use in error message
   * @param label label to check
   * @return version instance
   * @throws IllegalStateException if no version could be matched
   */
  static <T extends Enum<?> & EngineVersion> T getByVersionLabel(final T[] versions, final String name, final String label) {
    return Arrays.stream(versions)
        .filter(version -> label == null || version.name().equalsIgnoreCase(label) || version.getVersion().equals(label))
        .findFirst()
        .orElseThrow(() -> new IllegalStateException("Unsupported version label for " + name + " : " + label));
  }

}
