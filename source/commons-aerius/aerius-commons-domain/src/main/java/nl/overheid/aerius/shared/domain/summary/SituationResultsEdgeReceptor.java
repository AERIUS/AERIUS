/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.summary;

/**
 * Receptor where an edge effect occurs
 */
public class SituationResultsEdgeReceptor {

  private final int receptorId;
  private final double result;
  private final double referenceResult;
  private final double offSiteReductionResult;
  /**
   * Contains the proposed result of a project calculation,
   * or a temporary contribution in case of a temporary effect calculation
   */
  private final double proposedResult;

  public SituationResultsEdgeReceptor(final int receptorId, final double result, final double referenceResult, final double offSiteReductionResult, final double proposedResult) {
    this.receptorId = receptorId;
    this.result = result;
    this.referenceResult = referenceResult;
    this.proposedResult = proposedResult;
    this.offSiteReductionResult = offSiteReductionResult;
  }

  public int getReceptorId() {
    return receptorId;
  }

  public double getResult() {
    return result;
  }

  public double getReferenceResult() {
    return referenceResult;
  }

  public Double getOffSiteReductionResult() {
    return offSiteReductionResult;
  }

  public double getProposedResult() {
    return proposedResult;
  }
}
