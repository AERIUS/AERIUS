/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.importer.summary;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Represents a summary of sources in an AERIUS-file as can be requested using Connect.
 * <br><br>
 * Should match API Spec for file summaries.
 * This is tested in `aerius-connect-service`:
 * {@link nl.overheid.aerius.connectservice.service.ScenarioSummaryServiceTest}
 */
public class SourcesSummary {

  private int numSources;
  private Map<String, BigDecimal> totalEmissions;
  private int numSectors;
  private int mainSectorId;
  private double centroidX;
  private double centroidY;

  public int getNumSources() {
    return numSources;
  }

  public void setNumSources(final int numSources) {
    this.numSources = numSources;
  }

  public Map<String, BigDecimal> getTotalEmissions() {
    return totalEmissions;
  }

  public void setTotalEmissions(final Map<String, BigDecimal> totalEmissions) {
    this.totalEmissions = totalEmissions;
  }

  public int getNumSectors() {
    return numSectors;
  }

  public void setNumSectors(final int numSectors) {
    this.numSectors = numSectors;
  }

  public int getMainSectorId() {
    return mainSectorId;
  }

  public void setMainSectorId(final int mainSectorId) {
    this.mainSectorId = mainSectorId;
  }

  public double getCentroidX() {
    return centroidX;
  }

  public void setCentroidX(final double centroidX) {
    this.centroidX = centroidX;
  }

  public double getCentroidY() {
    return centroidY;
  }

  public void setCentroidY(final double centroidY) {
    this.centroidY = centroidY;
  }
}
