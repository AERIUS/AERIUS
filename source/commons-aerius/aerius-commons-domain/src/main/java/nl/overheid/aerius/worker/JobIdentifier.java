/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.util.UUID;

/**
 * Class to keep track of correlation properties.
 */
public class JobIdentifier {

  /**
   * Prefix protected job uuid with "p"
   */
  public static final String PROTECTED_PREFIX = "p";
  /**
   * Prefix all other none protected job uuid with "t"
   */
  public static final String TEMP_PREFIX = "t";

  private final String jobKey;
  private final String localId;

  public JobIdentifier(final String jobKey) {
    this.jobKey = jobKey;
    this.localId = UUID.randomUUID().toString();
  }

  /**
   * The overarching job key.
   *
   * Can be used in logging to keep track of work over multiple workers.
   */
  public String getJobKey() {
    return jobKey;
  }

  /**
   * The local job ID.
   * This ID is generated per worker, and can be used to track parts locally.
   * Can also be used as a unique part in directories if required.
   */
  public String getLocalId() {
    return localId;
  }

  @Override
  public String toString() {
    return "[jobKey=" + jobKey + ", localId=" + localId + "]";
  }

  public boolean isProtected() {
    return getJobKey().startsWith(PROTECTED_PREFIX);
  }
}
