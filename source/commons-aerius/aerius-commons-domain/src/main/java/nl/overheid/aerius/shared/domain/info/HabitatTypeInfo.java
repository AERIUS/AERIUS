/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.info;

/**
 * Basic bean containing habitat type info.
 */
public class HabitatTypeInfo extends HabitatType {
  private static final long serialVersionUID = 2L;

  private CriticalLevels criticalLevels = new CriticalLevels();

  private HabitatGoal qualityGoal;
  private HabitatGoal extentGoal;

  public CriticalLevels getCriticalLevels() {
    return criticalLevels;
  }

  public void setCriticalLevels(final CriticalLevels criticalLevels) {
    this.criticalLevels = criticalLevels;
  }

  public HabitatGoal getQualityGoal() {
    return qualityGoal;
  }

  public void setQualityGoal(final HabitatGoal qualityGoal) {
    this.qualityGoal = qualityGoal;
  }

  public HabitatGoal getExtentGoal() {
    return extentGoal;
  }

  public void setExtentGoal(final HabitatGoal surfaceGoal) {
    this.extentGoal = surfaceGoal;
  }

}
