/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import java.util.Collection;

import nl.overheid.aerius.shared.domain.EngineDataKey;
import nl.overheid.aerius.shared.domain.EngineSource;

/**
 * A packet of sources and the key identifying the sources.
 */
public class GroupedSourcesPacket {
  private final GroupedSourcesKey key;
  private final Collection<EngineSource> sources;

  public GroupedSourcesPacket(final Integer key, final EngineDataKey engineDataKey, final Collection<EngineSource> sources) {
    this(new GroupedSourcesKey(engineDataKey, key), sources);
  }

  public GroupedSourcesPacket(final GroupedSourcesKey key, final Collection<EngineSource> sources) {
    this.key = key;
    this.sources = sources;
  }

  public GroupedSourcesKey getKey() {
    return key;
  }

  public Collection<EngineSource> getSources() {
    return sources;
  }

}
