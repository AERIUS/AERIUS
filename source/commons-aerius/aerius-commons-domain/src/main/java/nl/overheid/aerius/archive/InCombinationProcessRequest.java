/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.archive;

import java.util.List;

public class InCombinationProcessRequest {

  private final int calculationYear;
  private final List<Receptor> receptorIds;

  public InCombinationProcessRequest(final int calculationYear, final List<Receptor> receptorIds) {
    this.calculationYear = calculationYear;
    this.receptorIds = receptorIds;
  }

  public int getCalculationYear() {
    return calculationYear;
  }

  public List<Receptor> getReceptorIds() {
    return receptorIds;
  }

  @Override
  public String toString() {
    return "InCombinationProcessRequest [calculationYear=" + calculationYear + ", receptorIds=" + receptorIds + "]";
  }

  public static class Receptor {
    private final int receptorId;
    private final int level;

    public Receptor(final int receptorId, final int level) {
      this.receptorId = receptorId;
      this.level = level;
    }

    public int getReceptorId() {
      return receptorId;
    }

    public int getLevel() {
      return level;
    }

    @Override
    public String toString() {
      return "Receptor [receptorId=" + receptorId + ", level=" + level + "]";
    }
  }
}
