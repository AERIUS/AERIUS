/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius;

import java.io.Serializable;

/**
 * Data object to store a large String with a filename and mime type.
 *
 * @param data The data for this datasource.
 * @param filename The name the file should have.
 * @param mimeType The mimetype of the file.
 */
public record StringDataSource(String data, String filename, String mimeType) implements Serializable {

  private static final long serialVersionUID = 1L;
}
