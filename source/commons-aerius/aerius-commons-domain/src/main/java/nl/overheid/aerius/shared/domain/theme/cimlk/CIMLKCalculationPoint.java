/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.theme.cimlk;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKDispersionLine;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKMonitorSubstance;
import nl.overheid.aerius.shared.domain.v2.cimlk.CIMLKRejectionGrounds;

public class CIMLKCalculationPoint extends AeriusPoint implements Serializable {

  private static final long serialVersionUID = 4L;

  private CIMLKMonitorSubstance monitorSubstance;
  private CIMLKRejectionGrounds rejectionGrounds;

  private final List<CIMLKDispersionLine> dispersionLines = new ArrayList<>();

  public CIMLKCalculationPoint() {
    super(AeriusPointType.POINT);
  }

  public CIMLKCalculationPoint(final int id, final String gmlId, final double x, final double y) {
    super(id, null, AeriusPointType.POINT, x, y);
    this.setGmlId(gmlId);
  }

  public List<CIMLKDispersionLine> getDispersionLines() {
    return dispersionLines;
  }

  public CIMLKMonitorSubstance getMonitorSubstance() {
    return monitorSubstance;
  }

  public void setMonitorSubstance(final CIMLKMonitorSubstance monitorSubstance) {
    this.monitorSubstance = monitorSubstance;
  }

  public CIMLKRejectionGrounds getRejectionGrounds() {
    return rejectionGrounds;
  }

  public void setRejectionGrounds(final CIMLKRejectionGrounds rejectionGrounds) {
    this.rejectionGrounds = rejectionGrounds;
  }

}
