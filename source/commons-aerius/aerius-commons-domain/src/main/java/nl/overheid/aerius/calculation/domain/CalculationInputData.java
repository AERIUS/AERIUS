/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.export.ExportAppendix;
import nl.overheid.aerius.shared.domain.export.ExportData;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioResults;

/**
 * Data object with options to perform a calculation.
 */
public class CalculationInputData extends ExportData implements Serializable {

  private static final long serialVersionUID = 5L;

  private String name;
  private transient Scenario scenario;
  private ScenarioResults scenarioResults;
  private String calculationWorkerQueue;
  private ExportType exportType;
  private Set<ExportAppendix> appendices = new HashSet<>();
  // Optional ADMS license
  private byte[] admsLicense;

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public Scenario getScenario() {
    return scenario;
  }

  public void setScenario(final Scenario scenario) {
    this.scenario = scenario;
  }

  public Optional<ScenarioResults> getScenarioResults() {
    return Optional.ofNullable(scenarioResults);
  }

  public void setScenarioResults(final ScenarioResults scenarioResults) {
    this.scenarioResults = scenarioResults;
  }

  public ExportType getExportType() {
    return exportType;
  }

  public void setExportType(final ExportType exportType) {
    this.exportType = exportType;
  }

  public Set<ExportAppendix> getAppendices() {
    return appendices;
  }

  public void setAppendices(final Set<ExportAppendix> appendices) {
    this.appendices = appendices;
  }

  public boolean isSectorOutput() {
    return exportType == ExportType.GML_WITH_SECTORS_RESULTS;
  }

  public boolean isCustomPointsInDatabase() {
    return exportType != ExportType.CSV || exportType != ExportType.CIMLK_CSV;
  }

  public boolean isSubPointsInDatabase() {
    return scenario != null && scenario.getTheme() == Theme.NCA;
  }

  public String getQueueName() {
    return calculationWorkerQueue;
  }

  public void setQueueName(final String queueName) {
    this.calculationWorkerQueue = queueName;
  }

  public boolean isNameEmpty() {
    return name == null || name.isEmpty();
  }

  /**
   * @return Optional License for CERC ADMS tool. Will be passed to ADMS worker.
   */
  public byte[] getAdmsLicense() {
    return admsLicense;
  }

  public void setAdmsLicense(final byte[] admsLicense) {
    this.admsLicense = admsLicense;
  }
}
