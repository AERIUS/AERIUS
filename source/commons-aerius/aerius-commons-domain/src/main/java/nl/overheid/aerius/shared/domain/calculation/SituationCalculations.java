/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.calculation;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import nl.overheid.aerius.shared.domain.scenario.SituationType;

public class SituationCalculations extends ArrayList<SituationCalculation> {

  /**
   * Retrieve the calculation id of the situation with that situation id
   */
  public Optional<Integer> getCalculationId(final String situationId) {
    return stream().filter(s -> s.getSituationId().equals(situationId)).map(SituationCalculation::getCalculationId).findFirst();
  }

  /**
   * Retrieve the calculation ids of situations of that type
   */
  public List<Integer> getCalculationIdsOfSituationType(final SituationType situationType) {
    return stream().filter(s -> s.getSituationType() == situationType).map(SituationCalculation::getCalculationId).collect(Collectors.toList());
  }

  /**
   * Retrieve all calculation ids
   */
  public List<Integer> getCalculationIds() {
    return stream().map(SituationCalculation::getCalculationId).collect(Collectors.toList());
  }

  /**
   * Retrieve the calculation id of a situation of that type if it exists
   * This is meant for situations that may only occur once in the calculation!
   * E.g. off site reduction situations may only appear zero or one time.
   */
  public Optional<Integer> getCalculationIdOfSingleSituationType(final SituationType situationType) {
    final List<Integer> calculationsOfType = getCalculationIdsOfSituationType(situationType);
    assert calculationsOfType.size() <= 1 : "Not supported, multiple situations exists with situation type " + situationType;
    return calculationsOfType.stream().findFirst();
  }

}
