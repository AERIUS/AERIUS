# SRM Worker

The SRM worker calculates pollutants with the  Standaardrekenmethode 1 (SRM1) and Standaardrekenmethode 2 (SRM2) from the "Regeling beoordeling luchtkwaliteit 2007" (models specific for road traffic).
The SRM worker supports calculation of fine dust (PM2.5 and PM10), and Nitrogen (NOx, NO2 and NH3) pollutants.
The SRM worker outputs pollutant concentration and/or deposition results.

SRM1 calculations apply to roads in an urban environment.
In the SRM1 model the effects of a specific road segment are calculated on a specific calculation point.
In the SRM2 model the effects of all roads with a 5km distance to a specific calculation point are calculated.

## Technical

The SRM2 model is implemented in OpenCL.
OpenCL is a framework for writing programs that execute across heterogeneous platforms using either a CPU or GPU.
To run the SRM2 model it is required to have OpenCL drivers installed that either use the CPU or GPU.

## SRM background data

The SRM models use data from several sources.
This data is yearly updated.
The worker expects the different data files in their directory in the worker configured SRM data directory.
The worker supports downloading the files dynamically from website.
If downloading is configured the worker tries to download data file if they are not present in the SRM data directory.
The data used consists of background concentrations, meteorology and landuse data.
The following data sets are used:

### Pre-srm landuse data files

The landuse data from the pre-srm is used in the SRM2 calculation.
This data is delivered with a pre-srm installation and can be found in the `data/lu` directory of an pre-srm installation.
This directory can contain more files than are actually needed for AERIUS.
For AERIUS only the following landuse files are required:
```
z0nl250.asc
z0nl1000.asc
z0nl4000.asc
```
The SRM worker expects the files in a directory named `presrm-lu-<presrm version>`.

### Pre-srm preprocessed data

Pre-srm is used to create data files for background concentration and windrose values.
The preprocessing is done with a custom Pascal application that can be found in the `src/main/pascal` directory of this project.
This process is described in the [readme in the `src/main/pascal`](src/main/pascal/README.md) directory.
The SRM worker expects the files in a directory named `aerius-presrm-<presrm version>-<aerius-id>`.
The `<aerius-id>` is a number starting with `1` that can be used to create supplemental versions of the files of the same pre-srm version.
This can be used in case where a new set of files must be generated, based on the same version of pre-srm.
In that case the `<aerius-id>` is incremented by 1.

### Deposition Velocities

The deposition velocities data is a geographically mapped data set used in the SRM2 model to calculate the deposition from the concentration results.
This data is only relevant when calculating depositions.
The data contains deposition velocities at receptor ids at zoom level 2.
The data is provided by the RIVM.
The SRM worker expects the file in a directory with filename: `depositionvelocity-<dv file version>/<dv file version>.csv`.

### Windvelden

Windvelden data files contain wind speed per square km of the whole Netherlands.
The number per square km is the aggregated wind speed of all wind directions.
This data is only relevant for SRM1 calculations.
The data is published on the INFOMIL (I&W) website: https://www.rijksoverheid.nl/onderwerpen/luchtkwaliteit/vraag-en-antwoord/hoe-kan-ik-luchtvervuiling-berekenen
The download contains the interpolated wind speeds over a number of years and the actual wind speed data of the previous year.
For AERIUS we also need the actual wind speed data of other years, not only the last year.
The actual wind speed data of previous years is provided by INFOMIL in per year.
The SRM worker expects the files in a directory named `windvelden-<year>`.
Where `<year>` is the year of the most actual wind speed data year.

## Annual update

SRM background data is updated annually, after PreSRM is released by TNO (usually around the end of March).
Technically this means that the following steps need to be taken:
1. Generate new background data files with PreSRM, see the steps in the [readme in the `src/main/pascal`](src/main/pascal/README.md) directory.
2. Update the enum in [`src/main/java/nl/overheid/aerius/srm/version/SRMVersion.java`](src/main/java/nl/overheid/aerius/srm/version/SRMVersion.java) to reflect the new version.
3. Update the database system constant(s) in `source/database/src/data/sql/nl-[latest|stable]/system/version_constants.sql`.
