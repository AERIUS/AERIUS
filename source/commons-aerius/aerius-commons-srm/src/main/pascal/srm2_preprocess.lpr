{
 Copyright the State of the Netherlands

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see http://www.gnu.org/licenses/.
}
{%BuildCommand $TargetOS() $(CompPath) $(EdFile)}
program srm2_preprocess;

{$mode objfpc}{$H+}

uses
  math, SysUtils, eventlog, INIFiles, TypInfo,
  modeldefs, pre_srm;

type
  TOutputOptions = Record
    wind, con, o3, pm, nh3, nox : Boolean;
    nrOfSectors    : integer;
    outputFilename : ansistring;
  end;

type
  TInputData = Record
    datapath      : ansistring;
    startX, endX,
    startY, endY  : integer;
    blockSize     : integer;
    blockMargin   : integer;
    diameter      : integer;
    meteoPrognose : Boolean;
    startYear,
    endYear       : integer;
    outputOptions : TOutputOptions;
  end;

const
  AERIUS_DATA_VERSION = '1';
  KM_BLOCK = 1000;
  TAB = char(9);
  NR_WIND = 36;
  PRESRM = 'PRESRM';
  AERIUS = 'AERIUS';
  LOG_DIRECTORY = 'log_%s_%d';
  OUTPUT_DIRECTORY = 'aerius-presrm-%s_%d';
  PRESRM_DATAPATH = 'PRESRM_DATAPATH';
  // project init
  USE_STABILITY = false;
  USER_ROUGHNESS = z0_standaard; // default srm2
  USTAR_LIMIT = -1;
  MONINOBU_LIMIT = -1;
  MULTI_STATION = False;
  DUBBELTELLINGSCORRECTIE = True;
  USE_COMP = [TComponent.O3, TComponent.NO2, TComponent.NH3, TComponent.PM10,
      TComponent.PM25, TComponent.Roet];

var
  EventLog1 : TEventLog;

function AERIUS_PreSRM_Version: ansistring;
begin
  AERIUS_PreSRM_Version := format('%.3f-%s',
      [PreSrmVersion, AERIUS_DATA_VERSION]);
end;

{
  Returns pre_srm version as text string
}
function PreSRM_Version: ansistring;
begin
  PreSRM_Version := format('PreSRM version: %s', [AERIUS_PreSRM_Version]);
end;

{
  Returns
}
function meteoPrognoseStr(fmt: ansistring; year: integer; id: TInputData)
    : ansistring;
begin
  if id.meteoPrognose then
  begin
    if id.outputOptions.wind then meteoPrognoseStr := 'prognose'
    else meteoPrognoseStr := format(fmt, [year, 'prognose']);
  end
  else meteoPrognoseStr := format(fmt, [year, 'year']);
end;

procedure Init_File(year: integer; id: TInputData; var froos: textfile);
var
    outputdir : ansistring;
begin
  outputdir := format(OUTPUT_DIRECTORY, [AERIUS_PreSRM_Version,
      id.outputOptions.nrOfSectors]);
  If Not DirectoryExists(outputdir) then
    CreateDir (outputdir);
  AssignFile (froos, outputdir + '/'
      + format(id.outputOptions.outputFilename,
      [meteoPrognoseStr('%d_%s', year, id)]));
  Rewrite(froos);
  {$i+}
end;

procedure Write_Header_Conc(enabled :boolean; conc: ansistring;
    var froos: textfile);
begin
  if enabled then
  begin
    write(froos, TAB, 'GCN_' + conc);
    write(froos, TAB, 'C_HWN_' + conc);
  end;
end;

{
  Header of the file.
  Writes year in the first row, header of the table in the second row.
}
procedure Write_Header(year: integer; id: TInputData; var froos: textfile);
var
  i     : integer;
  meteo : ansistring;
begin
  writeln(froos, PreSRM_Version);
  writeln(froos, format('year          : %d', [year]));

  if id.meteoPrognose then meteo := 'prognose'
  else meteo := 'year';
  writeln(froos, format('meteo         : %s', [meteo]));
  // Column headers
  write(froos, 'X', TAB, 'Y');
  if id.outputOptions.wind then
  begin
    for i := 1 to id.outputOptions.nrOfSectors do
      write(froos, TAB, format('WF%2.2d', [i]));
    for i := 1 to id.outputOptions.nrOfSectors do
      write(froos, TAB, format('WS%2.2d', [i]));
    write(froos, TAB, 'MFactor');
  end;
  if id.outputOptions.con then
  begin
    if id.outputOptions.o3 then
      for i := 1 to id.outputOptions.nrOfSectors do
        write(froos, TAB, format('C%2.2d', [i]));
    Write_Header_Conc(id.outputOptions.nh3, 'NH3', froos);
    Write_Header_Conc(id.outputOptions.nox, 'NO2', froos);
    Write_Header_Conc(id.outputOptions.pm, 'PM10', froos);
    Write_Header_Conc(id.outputOptions.pm, 'PM25', froos);
    Write_Header_Conc(id.outputOptions.pm, 'EC', froos);
    Write_Header_Conc(id.outputOptions.o3, 'O3', froos);
  end;
  writeln(froos);
end;

{
  Initializes the pre-srm project structure.
}
procedure Init_Project(year, midX, midY, width, height: integer;
    id: TInputData; var proj, conc: Pointer);
var
  dummy     : Pointer;
  datadir   : ansistring;
begin
  dummy := nil;
  datadir := ExtractFilePath(id.datapath);
  EventLog1.Log(format('Use 10 year meteo: %s',
      [meteoPrognoseStr('%d(%s)', year, id)]));
  proj := PS_Proj_Init(year, midX, midY, width, height, pAnsiChar(datadir),
      id.meteoPrognose);
  try
    conc := PS_Conc_Init(proj, USE_COMP, -1, -1);
    EventLog1.log('Concentration initialized');
  except
    PS_Cleanup(proj, dummy, conc, dummy);
    proj := nil;
    conc := nil;
  end;
end;

{
  Returns the wind factor for the given sector.
}
function Get_Wind_Sector(classdata: Pointer; sect: integer) : double;
var
  hpmteo      : array[1..3] of double;
  hpmteoTotal : double;
  wind        : integer;
  freq        : double;
begin
  freq := 0;
  for wind := 1 to 3 do
  begin
    // wind must be a value between 1 .. 3 unlike the doc says of this method.
    // these values represent class 0 .. 2
    PS_Class_Meteo(classdata, sect, wind, freq);
    hpmteo[wind] := freq;
  end;
  hpmteoTotal := hpmteo[1] + hpmteo[2] + hpmteo[3];
  if hpmteoTotal > 0 then
  begin
    Get_Wind_Sector := hpmteoTotal /
        (hpmteo[1] * 0.8 / 1.45 + hpmteo[2] * 1.0 / 4 + hpmteo[3] * 1.1 / 8);
    end
    else
    Get_Wind_Sector := 0;
end;

{
}
procedure Write_Conc(var froos: textfile; conc: Pointer; comp: TComponent;
    meteoX, meteoY: integer; writeConc: Boolean);
var
  dbc : single;
  cnc : single;
begin
  if writeConc then
  begin
    dbc := 0;
    cnc := Max(0, PS_MeanConc(conc, comp, meteoX, meteoY, dbc));
    write(froos, TAB, cnc:0:3);
    write(froos, TAB, dbc:0:3);
    EventLog1.Log(format('%s=%f, dbc=%f',
        [GetEnumName(TypeInfo(TComponent), Integer(comp)), cnc, dbc]));
  end;
end;

{
  Writes the data for a row depending on what output options are given.
  If wind than writes columns per sector: wind factor, wind speed
  if concentration than writes columns per wind sector: O3 concentration and
  and background concentration columns for NH3, NO2, PM10, PM25 and O3.
}
procedure Write_Line(var froos: textfile; proj, conc: Pointer;
    meteoX, meteoY: integer; outputOptions: TOutputOptions);
var
  sect          : integer;
  meteo,
  classdata     : Pointer;
  ws, wf, c_o3  : array[1..36] of double;
  wf_total      : double;
  dbc_o3        : single;
  meteo_factor  : double;
  cncO3 : single;
  dummy         : Pointer;
begin
  dummy := nil;
  dbc_o3 :=0;
  cncO3 := PS_MeanConc(conc, TComponent.O3, meteoX, meteoY, dbc_o3);
  EventLog1.Log(
      format('km x=%d, y=%d, O3=%f, dbc=%f', [meteoX, meteoY, cncO3, dbc_o3]));
  if cncO3 > 0 then
  begin
    // per 1x1 km vak meteo interpoleren en classdata opvragen
   meteo := PS_Meteo_Init(proj, USE_STABILITY, USER_ROUGHNESS, USTAR_LIMIT,
       MONINOBU_LIMIT, meteoX, meteoY, MULTI_STATION);
   try
     if meteo = nil then
     begin
      EventLog1.Log('meteo is nil!');
     end
     else
     begin
       classdata := PS_ClassData_Init(proj, meteo, conc,
           outputOptions.nrOfSectors, outputOptions.nrOfSectors,
           DUBBELTELLINGSCORRECTIE);
       PS_Class_SetCoord(classdata, meteoX, meteoY);

       for sect := 1 to outputOptions.nrOfSectors do
       begin
         if outputOptions.wind then
             ws[sect] := Get_Wind_Sector(classdata, sect - 1);
         PS_Class_Conc(classdata, sect - 1, TComponent.O3,
                 c_o3[sect], wf[sect]);
       end;
       write(froos, meteoX:0, TAB, meteoY:0);
       if outputOptions.wind then
       begin
         wf_total := 0;
         for sect := 1 to outputOptions.nrOfSectors do
         begin
           wf_total := wf_total + wf[sect];
           write(froos, TAB, wf[sect]:0:5);
         end;
         if format('%.6f', [wf_total]) <> '1.000000' then
           EventLog1.Log(
             format('Windfactor (x:%d, y:%d) doesn''t count to 1: %.12f',
             [meteoX, meteoY, wf_total]));
         for sect := 1 to outputOptions.nrOfSectors do
         begin
           write(froos, TAB, ws[sect]:0:3);
         end;
         meteo_factor := PS_MeteoFactor(meteo);
         write(froos, TAB, meteo_factor:0:3);
       end;
       if outputOptions.con then
       begin
         if outputOptions.o3 then
           for sect := 1 to outputOptions.nrOfSectors do
           begin
             write(froos, TAB, c_o3[sect]:0:3);
           end;
         // NH3
         Write_Conc(froos, conc, TComponent.NH3, meteoX, meteoY, outputOptions.nh3);
         // NO2
         Write_Conc(froos, conc, TComponent.NO2, meteoX, meteoY, outputOptions.nox);
         // PM10
         Write_Conc(froos, conc, TComponent.PM10, meteoX, meteoY, outputOptions.pm);
         // PM25
         Write_Conc(froos, conc, TComponent.PM25, meteoX, meteoY, outputOptions.pm);
         // EC
         Write_Conc(froos, conc, TComponent.Roet, meteoX, meteoY, outputOptions.pm);
         // O3
         if outputOptions.o3 then
         begin
             write(froos, TAB, cncO3:0:3);
             write(froos, TAB, dbc_o3:0:3);
         end;
       end;
       writeln(froos);
     end
     finally
      PS_Cleanup(dummy, meteo, dummy, classdata);
     end;
  end
  else
    EventLog1.Log(format('No valid O3 for km x=%d, y=%d', [meteoX, meteoY]));
end;

{
  Writes the lines for a block (for example with a 25 block size this would be
  all values within the 25km area per km area.)
}
procedure Write_Block(var froos: textfile; proj, conc: Pointer;
    startX, endX, startY, endY, diameter: integer;
    outputOptions: TOutputOptions);
var
  x, y         : integer;
  halfDiameter : integer;
begin
  halfDiameter := diameter div 2;
  x := startX;
  while x < endX do
  begin
    y := startY;
    while y < endY do
    begin
      Write_Line(froos, proj, conc, x + halfDiameter, y + halfDiameter,
          outputOptions);
      y := y + diameter;
    end;
    x := x + diameter;
  end;
end;

{
 Writes all blocks for a given year.
}
procedure Write_Blocks(year, diameter: integer; outputOptions: TOutputOptions;
    var froos: textfile; id: TInputData);
var
  bX, bY,
  bStartX, bEndX,
  bStartY, bEndY,
  bAStartX, bAEndX,
  bAStartY, bAEndY,
  bWidth, bHeight,
  projectBWidth, projectBHeight,
  bCenterX, bCenterY : integer;
  proj, conc         : Pointer;
  dummy              : Pointer;
begin
  dummy := nil;
  bStartX := id.startX div id.blockSize;
  bEndX := (id.endX - 1) div id.blockSize;
  bStartY := id.startY div id.blockSize;
  bEndY := (id.endY - 1) div id.blockSize;
  for bX := bStartX to bEndX do
  begin
    for bY := bStartY to bEndY do
    begin
      bAStartX := Max(bX * id.blockSize, id.startX);
      bAEndX := Min((bX + 1) * id.blockSize, id.endX);
      bAStartY := Max(bY * id.blockSize, id.startY);
      bAEndY := Min((bY + 1) * id.blockSize, id.endY);
      bWidth := (bAEndX - bAStartX) * KM_BLOCK;
      bHeight := (bAEndY - bAStartY) * KM_BLOCK;
      bCenterX := bAStartX * KM_BLOCK + bWidth div 2;
      bCenterY := bAStartY * KM_BLOCK + bHeight div 2;
      projectBWidth := bWidth + id.blockMargin * KM_BLOCK;
      projectBHeight := bHeight + id.blockMargin * KM_BLOCK;

      EventLog1.Log(
          format('Block(%d:%d) center(%d:%d), x(%d:%d), Y(%d:%d), w:%d, h:%d',
          [bX, bY, bCenterX, bCenterY, bAStartX, bAEndX, bAStartY, bAEndY,
              projectBWidth, projectBHeight]));
      try
        try
          proj := nil;
          conc := nil;
          Init_Project(year, bCenterX, bCenterY, projectBWidth, projectBHeight,
              id, proj, conc);
          if PS_Errorcode <> 0 then
          begin
            EventLog1.Log('Error in proj init, ignoring '
                + format('(%d, %d)', [bX, bY]));
            continue; // year failed
          end;
          if proj <> nil then
            Write_Block(froos, proj, conc, bAStartX * KM_BLOCK,
                bAEndX * KM_BLOCK, bAStartY * KM_BLOCK, bAEndY * KM_BLOCK,
                diameter, outputOptions);
        finally
          if conc <> nil then
            PS_Cleanup(dummy, dummy, conc, dummy);
          if proj <> nil then
            PS_Cleanup(proj, dummy, dummy, dummy);
        end
      except
        on e: Exception do
        EventLog1.Log(
           format('Block error!:(%d, %d) center:(%d, %d), x:(%d - %d), Y:(%d - %d) - %s',
          [bX, bY, bCenterX, bCenterY, bAStartX, bAEndX, bAStartY, bAEndY, e.Message]));
      end;
    end;
  end;
end;

{
 Writes the data for a give year.
}
procedure Write_Year(id: TInputData; year: integer;
    outputOptions: TOutputOptions);
var
  froos : textfile;
begin
  try
    Init_File(year, id, froos);
    Write_Header(year, id, froos);
    Write_Blocks(year, id.diameter, outputOptions, froos, id);
  finally
    closefile(froos);
  end;
end;

{
  Generates the files for all years specified in the input data.
}
procedure Run_Years(id: TInputData);
var
  year : integer;
begin
  for year := id.startYear to id.endYear do
  begin
    EventLog1.Log(format('Run year: %d', [year]));
    Write_Year(id, year, id.outputOptions);
  end;
end;

function checkMeteoPrognose(meteoPrognose : integer): Boolean;
begin
  if meteoPrognose = 1 then checkMeteoPrognose := True
  else if meteoPrognose = 0 then checkMeteoPrognose := False
  else
  begin
    EventLog1.Log('"meteoPrognose" not set or invalid in ini file.');
    Halt(1);
  end;
end;

{
  Reads the custom values from the configuration .ini file.
}
function Read_Ini(iniFilename: ansistring;
    startYear, endYear, nrOfSectors: integer) : TInputData;
var
  iniFile   : TINIFile;
  inputData : TInputData;
  meteoPrognose : integer;
  presrmPath : ansistring;
begin
  iniFile := TINIFile.Create(iniFilename);
  presrmPath :=
      IncludeTrailingBackslash(GetEnvironmentVariable(PRESRM_DATAPATH));
  if presrmPath = '\' then
  begin
    Writeln('Environment variable "PRESRM_DATAPATH" is not set');
    Halt(1);
  end;
    inputData.datapath := format('%sv%.3f\data\', [presrmPath, PreSrmVersion]);
  if not DirectoryExists(inputData.datapath) then
  begin
    Writeln(format('"PRESRM_DATAPATH" path "%s" doesn''t exists',
        [inputData.datapath]));
    Halt(1);
  end;
  inputData.outputOptions.outputFilename := iniFile.ReadString(AERIUS,
      'OutputFilename', 'noname');
  if inputData.outputOptions.outputFilename = 'noname' then
  begin
    Writeln('Ini file argument missing.');
    Writeln('Run: <program> <config.ini> (<start year>) (<end year>) (<number of windsectors>)');
    Halt(1);
  end;

  meteoPrognose := iniFile.ReadInteger(PRESRM, 'meteoPrognose', -1);
  inputData.meteoPrognose := checkMeteoPrognose(meteoPrognose);

  if startYear = 0 then
      inputData.startYear := iniFile.ReadInteger(AERIUS, 'StartYear', -1)
  else inputData.startYear := startYear;
  if startYear = 0 then
      inputData.endYear := iniFile.ReadInteger(AERIUS, 'EndYear', -1)
  else if endYear = 0 then inputData.endYear := startYear
  else inputData.endYear := endYear;

  inputData.blockSize := iniFile.ReadInteger(AERIUS, 'BlockSize', 25);
  EventLog1.Log(format('BlockSize: %d', [inputData.blockSize]));
  inputData.blockMargin := iniFile.ReadInteger(AERIUS, 'BlockMargin', 10);
  EventLog1.Log(format('BlockMargin: %d', [inputData.blockMargin]));
  inputData.diameter := iniFile.ReadInteger(AERIUS, 'Diameter', 1000);
  EventLog1.Log(format('Diameter: %d', [inputData.diameter]));
  inputData.startX := iniFile.ReadInteger(AERIUS, 'StartX', 6);
  inputData.endX := iniFile.ReadInteger(AERIUS, 'EndX', 278);
  inputData.startY := iniFile.ReadInteger(AERIUS, 'StartY', 306);
  inputData.endY := iniFile.ReadInteger(AERIUS, 'EndY', 620);

  inputData.outputOptions.nrOfSectors := nrOfSectors;
  EventLog1.Log(format('Number of windsectors: %d', [nrOfSectors]));
  inputData.outputOptions.wind := iniFile.ReadBool(AERIUS, 'WriteWind', False);
  inputData.outputOptions.con := iniFile.ReadBool(AERIUS, 'WriteCon', False);
  inputData.outputOptions.nh3 := iniFile.ReadBool(AERIUS, 'WriteNH3', True);
  inputData.outputOptions.nox := iniFile.ReadBool(AERIUS, 'WriteNOx', True);
  inputData.outputOptions.pm := iniFile.ReadBool(AERIUS, 'WritePM', True);
  inputData.outputOptions.o3 := iniFile.ReadBool(AERIUS, 'WriteO3', True);

  EventLog1.Log(format('PRESRM_DATAPATH = %s', [inputData.datapath]));
  Read_Ini := inputData;
end;

{
  Logging.
}
procedure Logging(ini: ansistring; startYear, endYear, nrOfSectors: integer);
var
  logdir : ansistring;
begin
  logdir := format(LOG_DIRECTORY,
      [AERIUS_PreSRM_Version, nrOfSectors]);
  If Not DirectoryExists(logdir) then
    CreateDir (logdir);
  EventLog1 := TEventLog.Create(nil);
  EventLog1.LogType := ltFile;
  EventLog1.FileName := format(logdir + '/%s-%d-%d.log',
      [ini, startYear, endYear]);
  EventLog1.Active := True;
  EventLog1.Log(PreSRM_Version);
end;

{
  Main.
}
procedure Main(iniFile: ansistring; startYearStr: ansistring;
    endYearStr: ansistring; numberOfSectors: ansistring);
var
  startYear, endYear, nrOfSectors: integer;
begin
  PS_HandleExceptions(True);
  DefaultFormatSettings.DecimalSeparator := '.';
  if startYearStr = '' then startYear := 0
  else startYear := strToInt(startYearStr);
  if endYearStr = '' then endYear := 0
  else endYear := strToInt(endYearStr);
  if (endYear > 0) and (endYear < 100) then
  begin
    nrOfSectors := endYear;
    endYear := 0;
  end
  else
    if numberOfSectors = '' then nrOfSectors := NR_WIND
    else nrOfSectors := strToInt(numberOfSectors);

  Logging(iniFile, startYear, endYear, nrOfSectors);
  Run_Years(Read_Ini(iniFile, startYear, endYear, nrOfSectors));
  EventLog1.Log('Finished');
end;

{
 Main <ini file> <start year> <end year> <number of wind sectors>
}
begin
  Main(ParamStr(1), ParamStr(2), ParamStr(3), ParamStr(4));
end.
