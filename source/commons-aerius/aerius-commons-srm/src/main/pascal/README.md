# PRE-SRM Preprocessing

The `srm_preprocessor` tool generates the preprocessed pre-srm files for the AERIUS srm worker.
The pre-srm library is used to generate wind (air), O3, NO2, PM10, PM25 NH3 background values per 1km area for a given coordinate set.

## Building the srm_preprocessor tool

This `srm_preprocessor` tool is written in free pascal and can be build with the open source pascal ide Lazarus.

To build the `srm_preprocessor` tool the pre-srm interface files provided by pre-srm are needed.
This files are `modeldefs.pas` and `pre_srm.pas`.
The files included in the repository here are from the latest pre-srm version used.
With a new version of pre-srm copy the above mentioned files over the files in this repository.
Make sure to check the `modeldefs.pas` file. It should contain the correct pre-srm information.
Check the following lines:
```
  PreSrmVersion : Double = 2.103;
  releasedatayear : integer = 2020; // signifies until which year actual data is
```
*These lines are not always correctly updated by TNO and therefore if not correct manually update them to the correct pre-srm version.*

To run the `srm_preprocessor` tool also needs the pre-srm dll files: `pre_srm.dll` (or `pres_srm_64.dll` if running 64 bit).
Place the dll files from the specific pre-srm version in the directory of the `srm2_preprocess.exe`.

## Running the srm_preprocessor tool

The `srm_preprocessor` requires some configuration options to run.
On the command line there are 2 options:

One options takes 2 years. This option generates files for all years between and including the from and to year.
```
srm2_preprocess.exe <ini file> <from year> <to year>
```

The other option takes 1 year, and generated the file for the given year.

```
srm2_preprocess.exe <ini file> <year>
```

The ini file specifies what data is to be generated.

The srm_preprocessor tool generates the files in an `output-<version>-<nr_windrose>` directory,
and logs in the `log-<version>-<nr_windrose>` directory relative to the directory the tool was started.

### Batch run

In the `batch` directory a set of ini files as well as a windows batch script `run.cmd` is available to generate the needed files.

The batch script `run.cmd` can generates for a single ini file multiple years sequential and/or concurrently.
The script generates files from the given start year to the end year.
The script call is as follows:

```
> run.cmd <ini.file> <start year> <nr sequential> <end year>
```

The parameters of the script are:
* `<ini.file>` the ini file containing the specific data to generate.
* `<start year>` the year to start generating file for.
* `<nr sequential>` the number of files to generate sequential.
* `<end year>` the last year to generating file for.

The script starts runs concurrently derived from the `<nr sequential>` parameter.
The `<nr sequential>` specifies the number of years to generate sequential.
Depending on the CPU available it is best to specify the number of sequential runs derived from the number of available cores/threads.
For example. When the years 2021 to 2030 needs to be generated this are 10 years total.
When 2 cores are available it means 10 divided by 2 means it optimally the `<nr sequential>` should be `5`.
This will result in 2 concurrent runs with each 5 sequential runs.

The batch directory also contains a number of convenience scripts to start the different generations.
These scripts need to be adapted when a new year becomes available.

# Files generated

The files generated are for:
1. Background concentration data
2. Wind factor data
3. Schiphol correction factor data
4. Ijmond correction factor data

For each of those files there are 2 type of files generated
1. Ending `-prognose`: Uses 10-year meteo
2. Ending `-year`: Use specific year meteo
The specific meteo is only used for years in the past.
This means when a new year is passed for that year the configuration and batch need to be updated to generate that new year.

# Steps for generating new pre-srm preprocessed files

1. Install Lazarus.
2. Download and install the needed pre-srm version.
3. Copy the pre-srm `depac.pas`, `modeldefs.pas`, and `pre_srm.pas` files from the pres-srm `Delphi` directory to the `pascal` directory of AERIUS.
4. Check if the `modeldefs.pas` was correctly updated by TNO to the new pre-srm version (otherwise manually update).
5. Copy the pre-srm `pre_srm.dll`, `pres_srm_64.dll` and `depac.dll` files from the pres-srm installation directory to the `pascal` directory of AERIUS.
6. Start lazarus and compile the executable (Ctrl-F9).
7. Set the environment variable `PRESRM_DATAPATH` to directory where pre-srm is installed. The executable will add `v<pre-srm version>\data` to the path (should end with slash /)
8. Run the batch script `run.cmd` in the `batch` directory.
9. After the files are generated convert the files to linux line endings (for example with `dos2unix`).
10. Files should be uploaded to our nexus repository.

# Ini file options

The ini files are windows know ini files.
An ini file contains blocks preceded with the block name in brackets, like `[PRESRM]`.
All parameters below that block belong to that block.

The following parameters can be specified in the ini files:
```
[PRESRM]
; if use 10-year meteo set to 1, if use year set to 0
meteoPrognose = 1

[AERIUS]
; The x-y coordinates of the south-west coordinate (the first block)
StartX = 0
EndX = 300
; The x-y coordinates of the north-east coordinate (the last block)
StartY = 300
EndY = 625

; The size pre-srm uses to interpolate the data around the data we want to generate (in km)
BlockSize = 25
; A margin applied to the block size to create a buffer around the block size of pre-srm (in km)
blockMargin = 10

; Name of the output file, where %s will contain year and if prognose data or not.
OutputFilename = con_%s.txt
; Options to write (1) or not write (0) specific data. Available options are:
WriteWind = 0
WriteCon = 0
WriteNH3 = 1
WriteNOx = 1
WritePM = 1
WriteO3 = 1
```