echo off

set ini=%1
set /A startYear=%2
set /A step=%3
set /A endYear=%4
set /A nrOfSectors=%5

echo Run for %ini% file
echo Run from %startYear% till %endYear% with %step% steps

setlocal enabledelayedexpansion

for /l %%y in (%startYear%, %step%, %endYear%) do (
  set /A endRunYear=%%y + %step% - 1
  if !endRunYear! GTR %endYear% (
    set /A endRunYear=%endYear%
  )
  if %step% EQU 1 (
    start ..\srm2_preprocess.exe %ini% %%y %nrOfSectors%
  ) else if %%y EQU !endRunYear! (
    start ..\srm2_preprocess.exe %ini% %%y %nrOfSectors%
  ) else (
    start ..\srm2_preprocess.exe %ini% %%y !endRunYear! %nrOfSectors%
  )
)
