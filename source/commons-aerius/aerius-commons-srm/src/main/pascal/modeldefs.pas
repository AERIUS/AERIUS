unit modeldefs;

{
  April-June 2012: GvD & AdW  conversion to Delphi XE2
                              cleaning up sources,
                              documentation in source code using Insight
                              splitting objects into units
                              Formatting and correcting create-destroy,
                              bug fixing
  29 July 2012: ATV Final changes for 2012
  25 Mar 2015 Frumau, change releasedatayear, min windspeed=1
  21 Mar 2016 Frumau, change releasedatayear, version
  26 March 2020 Frumau, change meteolongperiod
}

interface

uses
  SysUtils;

type
  EPreSRMError = class(Exception);
  TComponent = (SO2, NO2, O3, PM10, CO, C6H6, Pb, NH3, PM25, NOx, BaP, Roet);
  //DComponent = (NHx,NOy,Ntot,Potz);
  TLandUseClass = (grass, arable, crops, conif, decid, water, urban, other);

const
  PreSrmVersion : Double = 2.304;   // Version 2022 final june..correction december
  year_rbl      : integer = 2030;   // projectyear prognose for RBL
  year_aerius   : integer = 2040;   // projectyear prognose for Aerius...
  releasedatayear : integer = 2022; // signifies until which year actual data is
                                    // available, years beyond this will be
                                    // dealt with as prognosis
  TCompNames: array [TComponent] of string[4] = ('SO2', 'NO2', 'O3', 'PM10',
    'CO', 'C6H6', 'Pb', 'NH3', 'PM25', 'NOx', 'BaP', 'EC');  //roet changed to ec 26/3/2018
  //DCompNames: array [TComponent] of string[4] = ('NHx', 'NOy', 'Ntot', 'Potz');

  MolarMass: array [TComponent] of Double = (64.066, 46.0056, 47.9983, 1, 28,
    80, 207.21, 17.03, 1, 1, 252.31, 1);
  InvalidResult: Double     = -9999.9;
  MeteoLongPeriodNrYears    = 10;
  z0_standaard              = 0.08; //kfa frumau 21/3/2020

var
  MixingHeightStart: Double =350;
  MinimumWindspeed: Double = 1.0;                       // used in TMeteoInterpolated.Create
  MeteoLongPeriodStartYear : Integer = 2005;   // longmeteo start 2005 instead of 1995 from 2020 onwards, before long meteo start 1995

type
  CompSet = set of TComponent;
  TCompArray = array [TComponent] of Double;

  TPoint = record x,y:single end;
  TPointArray = array of TPoint;
//  PPointArray = ^TPointArray;

  TIntegerArray = array of Integer;
//  PIntegerArray = ^ TIntegerArray;

//  TCharArray = array of Char;
//  PCharArray=^TCharArray;

  TByteArray = array of Byte;
//  PByteArray=^TByteArray;

  TTime = record
    year, month, day, hour: Integer;
  end;
  TTimeArray = array of TTime;
//  PTimeArray = ^TTimeArray;

  PSingleArray = ^TSingleArray;
  TSingleArray = Array of Single;

  TMeteoRec = packed record
    Time: TDateTime; // Datetime in days since 1-1-1900
    OWS, WD, // Original windspeed [m/s], Winddir [deg] at z=10m
    TT, RH, // Temperature [Celcius], Relative Humidity [%] (constant=60%)
    NN, GR, // Clouds [octets], Shortwave incoming radiation [W/m2]
    PP, // Precipitation [mm/hr]
    WS60, // Windspeed [m/s] at blending height z=60m
    WS, // Interpolated windspeed [m/s] at z=10m
    Sv, // Std dev of cross wind

    Monin, // Monin Obukhov length [m]
    Sens, // Sensible heatflux [W/m2]
    Ustar, // Friction Velocity [m/s]
    K1, K3, // rate constants for O3/NOx [s-1]
    Tl, // []
    Sw, // Std dev vertical wind [m/s]
    MixH: Double; // PBL height [m]
    Status: Byte;
  end;

  TTimArray = array of Double;
  TMeteoArray = array of TMeteoRec;

  TSingleLU = array [TLandUseClass] of Single;
  TSingle_comp_lu = array [TComponent] of TSingleLU;

  TVd_comp_lu = record
    Vd: TSingle_comp_lu;
    Flux, z0, Freq: TSingleLU;
  end;

const
  HasHourly: CompSet = [CO, NO2, O3, PM10, SO2, NOx];

implementation

end.

